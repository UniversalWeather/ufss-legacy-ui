﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Data;
using System.Collections.Specialized;
/*
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByPassengerTrip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTrip]
GO  
*/
namespace DBScripter
{
    class Program
    {
        static void Main(string[] args)
        {
            String serverName = @"LENOVO1\SQLEXPRESS";
            String dbname = "FSSDEV";
            String inputFile = "toscript.txt";
            String storedProcsOut = "OutputProcedures.txt";
            if (args == null || args.Count() == 0 || args[0] == "/?" || args[0] == "/help" || args[0] == "--help" || args[0] == "-help")
            {
                Console.WriteLine("");
                Console.WriteLine("Arguments List");
                Console.WriteLine("");
                Console.WriteLine("Try the command in the following format");
                Console.WriteLine("DBScripter /s DBServer /db DatabaseName /i inputfile /o outputfile");
                return;
            }
            if(args != null && args.Count() > 0)
            {
                for (int i = 0; i < args.Length; i = i+2) // Loop through array
                {
                    string argument = args[i];
                    switch(argument.ToLower())
                    {
                        case "/s":
                            serverName = args[i + 1];
                            break;
                        case "/db":
                            dbname = args[i + 1];
                            break;
                        case "/i":
                            inputFile = args[i + 1];
                            break;
                        case "/o":
                            storedProcsOut = args[i + 1];
                            break;
                    }
                }
            }

            Console.WriteLine("Server: {0}, Database: {1}, InputFile: {2}, Output: {3}", serverName, dbname, inputFile, storedProcsOut);
            String strDropTemplate = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'P', N'PC'))\n" +
                "DROP PROCEDURE [dbo].[{0}]\n\r" +
                "GO";
            String strScriptStartTemplate = "PRINT 'Updating {0}..'\n\r";
            Server server = new Server(serverName);
            Database db = server.Databases[dbname];
            DataTable dataTable = db.EnumObjects(DatabaseObjectTypes.StoredProcedure, SortOrder.Name);
            Scripter scripter = new Scripter();
            scripter.Server = server;
            scripter.Options.IncludeHeaders = true;
            scripter.Options.SchemaQualify = true;
            scripter.Options.DriAll = true;
            scripter.Options.ToFileOnly = false;

            StreamWriter swClear = new StreamWriter(storedProcsOut, false);
            swClear.Close();
            List<String> scriptedList = new List<string>();
            List<String> fileList = new List<string>();
            using (StreamReader sr = new StreamReader(inputFile))
            {
                while (!sr.EndOfStream)
                {
                    String fileInputSPName = sr.ReadLine();
                    fileList.Add(fileInputSPName);
                }
            }
            foreach (DataRow row in dataTable.Rows)
            {
                string sSchema = (string)row["Schema"];
                if (sSchema == "sys" || sSchema == "INFORMATION_SCHEMA")
                    continue;
                StoredProcedure sp = (StoredProcedure)server.GetSmoObject(
                   new Urn((string)row["Urn"]));
                if (!sp.IsSystemObject)
                {
                    using (StreamWriter sw = new StreamWriter(storedProcsOut, true))
                    {
                        using (StreamReader sr = new StreamReader(inputFile))
                        {
                            while (!sr.EndOfStream)
                            {
                                String fileInputSPName = sr.ReadLine();
                                if (!String.IsNullOrWhiteSpace(fileInputSPName) && fileInputSPName.ToUpper() == sp.Name.ToUpper())
                                {
                                    List<SqlSmoObject> list = new List<SqlSmoObject>();
                                    list.Clear();
                                    list.Add(sp);
                                    String dropOutput = String.Format(strDropTemplate, sp.Name);
                                    Console.WriteLine("Scripting {0}", sp.Name);
                                    StringCollection sc = scripter.Script(list.ToArray());
                                    sw.WriteLine(dropOutput);
                                    sw.WriteLine("");
                                    String strstartout = string.Format(strScriptStartTemplate, sp.Name);
                                    sw.WriteLine(strstartout);
                                    sw.WriteLine("GO");
                                    sw.WriteLine("");
                                    foreach (String strprc in sc)
                                    {
                                        String strprcText = strprc.Replace("SET ANSI_NULLS ON", "\n");
                                        strprcText = strprcText.Replace("SET QUOTED_IDENTIFIER ON", "\n");
                                        sw.WriteLine(strprcText);
                                    }
                                    sw.WriteLine("");
                                    sw.WriteLine("GO");
                                    sw.WriteLine("");
                                    scriptedList.Add(fileInputSPName);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            List<String> unscripted = (from fl in fileList
                                      where !scriptedList.Contains(fl)
                                      select fl).ToList();
            Console.WriteLine("Following items were not scripted");
            foreach(String unsc in unscripted)
            {
                Console.WriteLine(unsc);
            }
        }
    }
}
