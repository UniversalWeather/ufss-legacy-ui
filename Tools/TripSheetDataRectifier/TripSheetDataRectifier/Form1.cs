﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace TripSheetDataRectifier
{
    public partial class frmTripSheetValidator : Form
    {
        public frmTripSheetValidator()
        {
            InitializeComponent();
//            txtCustomerId.Text = "10064";
        }

        private void CalculateColumnDetails(String strTempExpectedTable, int reportNumber, string reporttitle, string RPTTSID )
        {
            try { 
            txtActual.Text = "";
            txtExpectedCount.Text = "";

            txtActualColumns.Text = "";
            txtExpectedColumns.Text = "";

            int expectedColumnsCount, actualColumnsCount;
            List<String> expectedColumns = new List<string>();
            List<String> actualColumns = new List<string>();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportDataContainer"].ConnectionString);

            SqlCommand commandActualData = new SqlCommand("spGetReportPRETSFormatSettings", conn);
            commandActualData.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlCommandBuilder.DeriveParameters(commandActualData);

            commandActualData.Parameters["@ReportHeaderID"].Value = cboTripHeaders.SelectedItem.ToString();
            commandActualData.Parameters["@ReportNumber"].Value = reportNumber;
            commandActualData.Parameters["@TempSettings"].Value = "";

            //Get the Actual Retrieved columns from the stored procedure spGetReportPRETSFormatSettings for this report
            DataTable _dataTableActualData = new DataTable();
            _dataTableActualData.Load(commandActualData.ExecuteReader());

            //Get the hard coded (expected) columns in the stored procedure
            SqlCommand commandHardCodedTableVariable = new SqlCommand(strTempExpectedTable, conn);
            commandHardCodedTableVariable.CommandType = CommandType.Text;

            DataTable _dataTableHardcodedTableVariable = new DataTable();
            _dataTableHardcodedTableVariable.Load(commandHardCodedTableVariable.ExecuteReader());

            String strCurrentMax = String.Format("Select max(TripSheetReportDetailID) from TripSheetReportDetail where CustomerId = {0}", txtCustomerId.Text);
            SqlCommand commandCurrentTripDetailMax = new SqlCommand(strCurrentMax, conn);

            Object objCurrentMax = commandCurrentTripDetailMax.ExecuteScalar();

            conn.Close();

            actualColumnsCount = _dataTableActualData.Columns.Count;
            foreach (DataRow row in _dataTableActualData.Rows)
            {
                foreach (DataColumn dcitem in _dataTableActualData.Columns)
                {
                    actualColumns.Add(dcitem.ColumnName);
                    String myval = row[dcitem.ColumnName].ToString();
                }
            }


            expectedColumnsCount = _dataTableHardcodedTableVariable.Columns.Count;
            foreach (DataRow vrow in _dataTableHardcodedTableVariable.Rows)
            {
                foreach (DataColumn vdcitem in _dataTableHardcodedTableVariable.Columns)
                {
                    expectedColumns.Add(vdcitem.ColumnName);
                    String myval = vrow[vdcitem.ColumnName].ToString();
                }
            }

            var qryAdditional = (from act in actualColumns
                                 where !expectedColumns.Contains(act)
                                 select act).ToList();

            String additionalColumns = String.Join(",", qryAdditional.Select(x => x.ToString()).ToArray());
            txtAdditionalColumns.Text = additionalColumns;

            var qryMissing = (from exp in expectedColumns
                              where !actualColumns.Contains(exp)
                              select exp).ToList();

            String missingColumns = String.Join(",", qryMissing.Select(x => x.ToString()).ToArray());
            txtMissingColumns.Text = missingColumns;

            txtActual.Text = actualColumnsCount.ToString();
            txtExpectedCount.Text = expectedColumnsCount.ToString();

            txtActualColumns.Text = String.Join(",", actualColumns.Select(x => x.ToString()).ToArray());
            txtExpectedColumns.Text = String.Join(",", expectedColumns.Select(x => x.ToString()).ToArray());

            long curMax = 0;
            if (objCurrentMax != null)
                try { 
                curMax = (long)objCurrentMax;
                }
                catch
                {

                }

            String strRemovalSql = GenerateRemovalOfItems(txtCustomerId.Text, cboTripHeaders.SelectedItem.ToString(), reportNumber, qryAdditional);
            txtRemoveAdditional.Text = strRemovalSql;

            string strInsertStatements = GenerateInsertStatements(txtCustomerId.Text, cboTripHeaders.SelectedItem.ToString(), reportNumber, qryMissing,
                curMax, reporttitle, RPTTSID);
            txtInsertMissing.Text = strInsertStatements;
           }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private String GenerateRemovalOfItems(String customerid, String tripsheetreportid, int reportNumber, List<String> removableItems)
        {
//"UPDATE TripSheetReportDetail SET ISDELETED=1 WHERE  CUSTOMERID=10064 AND TripSheetReportHeaderID = 10064116598 AND ReportNumber = 3 AND IsDeleted=0"
            StringBuilder sbUpdateSql = new StringBuilder();
            foreach(String removableItem in removableItems)
            {
                sbUpdateSql.AppendLine(String.Format("UPDATE TripSheetReportDetail SET ISDELETED=1 WHERE  CUSTOMERID={0} AND TripSheetReportHeaderID = {1} AND ReportNumber = {2} AND ParameterVariable = '{3}'  AND IsDeleted=0", customerid, tripsheetreportid, reportNumber, removableItem));
                sbUpdateSql.AppendLine("GO");
                sbUpdateSql.AppendLine("");
            }
            return sbUpdateSql.ToString();
        }

        private String GenerateInsertStatements(String customerid, String tripsheetreportid, int reportNumber, List<String> newItems, long lastMaxId, String reporttitle, String RPTTSID)
        {
            StringBuilder sbUpdateSql = new StringBuilder();
            /*
            INSERT INTO [dbo].[TripSheetReportDetail] ([TripSheetReportDetailID],[CustomerID],[TripSheetReportHeaderID],[ReportNumber],[ReportDescription],[IsSelected]
             ,[Copies],[ReportProcedure],[ParameterVariable],[ParameterDescription],[ParameterType],[ParameterWidth],[ParameterCValue],[ParameterLValue],[ParameterNValue]
             ,[ParameterValue],[ReportOrder],[TripSheetOrder],[LastUpdUID],[LastUpdTS],[IsDeleted],[ParameterSize],[WarningMessage])
                 VALUES(10064117793 ,10064 ,10064116598            
                 ,13,'SUMMARY',1, 1 ,'RPTTS13', 'DCREWHOTEL', 'DEPARTURE CREW HOTEL', 'L' ,0 ,NULL,0 ,NULL,NULL ,3,0,NULL,NULL,0,NULL,NULL)
            GO
             * */
            long maxVal = ++lastMaxId;
            foreach (String addableItem in newItems)
            {
                sbUpdateSql.AppendLine(String.Format(@"INSERT INTO [dbo].[TripSheetReportDetail] ([TripSheetReportDetailID],[CustomerID],[TripSheetReportHeaderID],[ReportNumber],[ReportDescription],[IsSelected]
             ,[Copies],[ReportProcedure],[ParameterVariable],[ParameterDescription],[ParameterType],[ParameterWidth],[ParameterCValue],[ParameterLValue],[ParameterNValue]
             ,[ParameterValue],[ReportOrder],[TripSheetOrder],[LastUpdUID],[LastUpdTS],[IsDeleted],[ParameterSize],[WarningMessage])
                 VALUES({0} ,{1} ,{2} ,{3},'{4}',1, 1 ,'{5}', '{6}', '{7} Description', 'L' ,0 ,NULL,0 ,NULL,NULL ,3,0,NULL,NULL,0,NULL,NULL)",
                ++maxVal, customerid, tripsheetreportid, reportNumber, reporttitle, RPTTSID, addableItem, addableItem));
                sbUpdateSql.AppendLine("GO");
            }
            return sbUpdateSql.ToString();
        }

        private void btnSummary_Click(object sender, EventArgs e)
        {
            //spGetReportPRETSSummaryLogisticsInformation 
            lblSelectedItem.Text = "Summary Selected";
            #region table var str
            string tablevar = @"DECLARE @FormatSettings TABLE (  
                 NOGO BIT  
                , PASSENG BIT  
                , PASSPHONE BIT  
                , PASSCNG BIT  
                , PASSPURP SMALLINT  
                , CREW BIT  
                , LEGNOTES SMALLINT  
                , TRIPALERTS BIT  
                , FBO BIT  
                , FBODEP BIT  
                , AIRARRIV BIT  
                , ANOTES BIT  
                , PAXHOTEL BIT  
                , PAXTRANS BIT  
                , CREWHOTEL BIT  
                , CREWTRANS BIT  
                , MAINHOTEL BIT  
                , DEPCATER BIT  
                , OUTINST BIT  
                , PAXRPTPOS SMALLINT  
                , PASSDEPT BIT  
                , DISPATCHNO BIT  
                , PURPOSE BIT  
                , ZULUTIME BIT  
                , CHRMONTH BIT  
                --,   NOTESF      SMALLINT  
                , LEGNOTESF SMALLINT  
                ,   DISPATCHIN SMALLINT  
                ,   DSPTEMAIL BIT  
                ,   DISPATCHER BIT  
                ,   DSPTPHONE BIT  
                ,	DCREWHOTEL BIT
                --,	DMAINHOTEL BIT
                ) ;
                INSERT INTO @FormatSettings(NOGO) values (NULL)
                SELECT * FROM @FormatSettings";
            #endregion

            CalculateColumnDetails(tablevar, 13, "SUMMARY", "RPTTS13");

        }


        private void btnCustomer_Click(object sender, EventArgs e)
        {
            try { 
            lblSelectedItem.Text = "Nothing Selected";
            if (txtCustomerId.Text == "")
            {
                MessageBox.Show("Enter a customer id");
                return;
            }
            txtActual.Text = "";
            txtExpectedCount.Text = "";

            txtActualColumns.Text = "";
            txtExpectedColumns.Text = "";

            cboTripHeaders.Items.Clear();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportDataContainer"].ConnectionString);
            String sqlreportheaders = @"SELECT TripSheetReportHeaderID ,ReportID ,ReportDescription FROM TripSheetReportHeader where IsDeleted=0 and customerid=" + txtCustomerId.Text;
            SqlCommand command = new SqlCommand(sqlreportheaders, conn);
            command.CommandType = CommandType.Text;
            conn.Open();

            DataTable _dataTableVar = new DataTable();
            _dataTableVar.Load(command.ExecuteReader());
            foreach (DataRow vrow in _dataTableVar.Rows)
            {
                StringBuilder rowValue = new StringBuilder();
                foreach (DataColumn vdcitem in _dataTableVar.Columns)
                {
                    if (vdcitem.ColumnName == "TripSheetReportHeaderID")
                    cboTripHeaders.Items.Add(vrow[vdcitem.ColumnName].ToString());
                }
            }
            if (cboTripHeaders.Items.Count > 0)
            cboTripHeaders.SelectedIndex = 0;

            conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }

        private void btnTripSheet_Click(object sender, EventArgs e)
        {
            //[spGetReportPRETSPAXNoFlyInformation1]
            lblSelectedItem.Text = "Tripsheet Selected";
            #region temp table str
            String temptablestr = @"DECLARE @TripSheet TABLE(   ONELEG BIT -- ONEBITEG BIT , Actual temp table column 
      , GENNOTES SMALLINT      
      , TRIPALERTS BIT      
      , FULLCREW BIT      
      , DEPTAUTH BIT      
      , LEGNOTES SMALLINT      
      , FUELBURN BIT      
      , NOTES SMALLINT      
      , ALERTS BIT      
      , OUTBOUND BIT      
      , PAXMANIF SMALLINT      
      , PAXCODE BIT      
      , PAXDEPT BIT      
      , PAXPHONE BIT      
      , PAXNBR BIT      
      , PAXCNG BIT      
      , ARRFBP BIT      
      , FBOAPHONE BIT      
      , FBOAFREQ BIT      
      , FBOAFAX BIT      
      , FBOAUVAIR BIT      
      , FBOAFUEL BIT      
      , FBOANPRICE BIT      
      , FBOALPRICE BIT      
      , FBOALDATE BIT      
      , FBOACONT BIT      
      , FBOAADDR BIT      
      , FBOAREM BIT      
      , PAXHOTEL BIT      
      , HOTELPHONE BIT      
      , HOTELFAX BIT      
      , HOTELCOMM SMALLINT      
      , PTANAME BIT      
      , PTAPHONE BIT      
      , PTAFAX BIT      
      , PTAREMARKS BIT      
      , CDNAME BIT      
      , CDPHONE BIT      
      , CDFAX BIT      
      , CDREMARKS BIT      
      , CREWHOTEL BIT      
      , CREWHPHONE BIT      
      , CREWHFAX BIT      
      , CREWHNRATE BIT      
      , CREWHADDR BIT      
      , CREWHCOMM SMALLINT      
      , CTANAME BIT      
      , CTAPHONE BIT      
      , CTAFAX BIT      
      , CTARATE BIT      
      , CTAREMARKS BIT      
      --, MAINTHOTEL BIT      
      --, MAINTPHONE BIT      
      --, MAINTFAX BIT      
      --, MAINTNRATE BIT      
      --, MAINTADDR BIT      
      --, MAINTCOMM SMALLINT      
      , PAXRPTPOS SMALLINT      
      , FBOAPHONE2 BIT      
      , PXLRGEFON BIT      
      , HOTELREM BIT      
      , CANAME BIT      
      , CAPHONE BIT      
      , CAFAX BIT      
      , CAREMARKS BIT      
      , CREWHREM BIT      
      , STATECODE BIT      
      , SUPPAXNOTE BIT      
      , SUPCRWNOTE BIT      
      , DISPATCHNO BIT      
      , PURPOSE BIT      
      , PAXNOTES SMALLINT      
      , ACNOTES SMALLINT      
      , FBOAPAYTYP BIT      
      , FBOAPOSTPR BIT      
      , FBOAFUELQT BIT    -- FBOAFUEBITQT BIT , Actual temp table column     
      , AIRFLTPHN BIT      
      , CRMBLPHN SMALLINT   -- CRMBBITPHN SMALLINT, Actual temp table column      
      , DISPATCHER BIT      
      , TERMTOAFBO SMALLINT      
      , ALLOWSHADE BIT      
      , CREWHROOMS BIT      
      , CREWHCONF BIT      
      , HOTELROOMS BIT      
      , HOTELCONF BIT      
      , PTACONFIRM BIT      
      , PTACOMMENT SMALLINT      
      , PTDNAME BIT      
      , PTDPHONE BIT      
      , PTDFAX BIT      
      , PTDREMARKS BIT      
      , PTDCONFIRM BIT      
      , PTDCOMMENT SMALLINT      
      , CTACONFIRM BIT      
      , CTACOMMENT SMALLINT      
      , CTDNAME BIT      
      , CTDPHONE BIT      
      , CTDFAX BIT      
      , CTDRATE BIT      
      , CTDREMARKS BIT      
      , CTDCONFIRM BIT      
      , CTDCOMMENT SMALLINT      
      , CACONFIRM BIT      
      , CACOMMENT SMALLINT      
      , CDCONFIRM BIT      
      , CDCOMMENT SMALLINT      
      , FBOACONF BIT      
      , FBOACOMM SMALLINT      
      , DEPFBP BIT      
  , FBODPHONE BIT      
      , FBODFREQ BIT      
      , FBODFAX BIT      
      , FBODUVAIR BIT      
      , FBODFUEL BIT      
      , FBODNPRICE BIT      
      , FBODLPRICE BIT      
      , FBODLDATE BIT      
      , FBODCONT BIT      
      , FBODADDR BIT      
      , FBODREM BIT      
      , FBODPHONE2 BIT      
      , FBODPAYTYP BIT      
      , FBODPOSTPR BIT      
      , FBODFUELQT BIT-- FBODFUEBITQT BIT, Actual temp table column        
      , TERMTODFBO SMALLINT      
      , FBODCONF BIT      
      , FBODCOMM SMALLINT      
      , FLTCAT BIT      
      , PASSPORT BIT      
      , CITIZEN BIT      
      , PAXDOB BIT      
      , HOTELRATE BIT      
      , PTARATE BIT      
      , PTDRATE BIT      
      , CARATE BIT      
      , CDRATE BIT      
      , CONDOUTBND BIT      
      , PAXALERTS SMALLINT      
      , FARNUM BIT      
      , CNTRYCODE BIT      
      --, MAINTREM BIT      
      --, MAINTCONF BIT      
      , AIRADDINFO BIT      
      , PAXADDINFO BIT      
      , FUELLOAD BIT      
      , AUTHPHONE BIT      
      , LMESSAGE BIT      
      , [MESSAGE] VARCHAR(MAX)      
      , MESSAGEPOS SMALLINT      
      , FLIGHTNO SMALLINT      
      , LINESEP BIT      
      , ARRAIRPORT BIT      
      , DEPAIRPORT BIT      
      , REQUESTOR BIT      
      , SORTBYDATE BIT      
      , CREWLASTLG BIT      
      , PRINTLOGO BIT      
      , PRNFIRSTPG BIT      
      , INTLFLTPHN BIT      
      , PAXVISA BIT      
      , CREWPILOT1 NVARCHAR(200)      
      , CREWPILOT2 NVARCHAR(200)      
      , CREWADDL NVARCHAR(200)      
      , PAXADDLPHN BIT      
      , REQADDLPHN BIT      
      , WINDS BIT      
      , DSPTEMAIL BIT      
      , DSPTPHONE BIT      
      , TRIPNO BIT      
      , [DESC] BIT      
      , TRIPDATES BIT      
      , TMREQ BIT      
      , TAILNMBR BIT      
      , CONTPHONE BIT      
      , TYPECODE BIT      
      , CQDESC SMALLINT      
      , REVISNNO BIT      
      , REVISNDESC BIT      
      , FUELDTLS SMALLINT      
      , FUELVEND SMALLINT      
      , BOLDNOTES SMALLINT      
      , HOTELADDR BIT      
      , PAXORDER BIT      
      , TSASTATUS BIT      
      , VERIFYNO BIT      
      , ACFTRLSE BIT      
      , CBPXMLS BIT      
      , CHRMONTH BIT      
      , DALERTS BIT      
      , ATISFREQ BIT      
      , DEPATIS  BIT      
      , ARRATIS  BIT      
      , CLRNCE  BIT      
      , CRWADLINFO BIT      
      , CRWDOB  BIT      
      , CRWPNUM  BIT      
      , CRWNATION BIT      
      , CRWPDATE BIT      
      , PAXREQDOC SMALLINT      
      , LEGSEP  BIT      
      , CQNAME  BIT      
      , CQCONTACT BIT      
      , PAXNOTESF   SMALLINT      
      --,   CREWHCOMM   SMALLINT      
      ,   DISPATCHIN SMALLINT      
      ,LGMINFUEL BIT    
      ,LGPOWERSET BIT    
      ,LGAIRSPEED BIT    
      ,FBOARRUNIC BIT    
      ,FBOARRARIN BIT    
      ,FBODEPUNIC BIT    
      ,FBODEPARIN BIT    
	  ,DCREWHTL BIT
	  ,DCREWHADDR BIT
	  ,DCREWHCOMM SMALLINT
	  ,DCREWHCONF BIT
	  ,DCREWHFAX BIT
	  ,DCREWHNRTE BIT
	  ,DCREWHROOM BIT
	  ,DCREWHPHON BIT
	  ,DCREWHREM BIT
   --   ,DMAINHOTEL BIT
	  --,DMAINHADD BIT
	  --,DMAINHCOMM SMALLINT
	  --,DMAINHCON BIT
	  --,DMAINHFAX BIT
	  --,DMAINHRATE BIT
	  --,DMAINHPHON BIT
	  --,DMAINHREM BIT
		,DPAXHOTEL BIT
		,DPAXHPHON BIT
		,DPAXHFAX BIT
		,DPAXHCOMM SMALLINT
		,DPAXHREM BIT
		,DPAXHROOMS BIT
		,DPAXHCONF BIT
		,DPAXHRATE BIT
		,DPAXHADDR BIT       
      )      
    INSERT INTO @TripSheet(ONELEG) values (NULL)
    SELECT * FROM @TripSheet";
            #endregion

            CalculateColumnDetails(temptablestr, 2, "TRIPSHEET", "RPTTS02");

        }

        private void btnItinerary_Click(object sender, EventArgs e)
        {
            lblSelectedItem.Text = "Itinerary Selected";
            #region table var
            String strtablevar = @"DECLARE @FormatSettings TABLE (
			LMESSAGE	BIT
		,	MESSAGEPOS	SMALLINT
		,	[MESSAGE]	VARCHAR(MAX)
		,	ONELEG	BIT
		,	LEGNOTES	SMALLINT
		,	LEGCREW	SMALLINT
		,	ETEHRS	SMALLINT
		,	PONEPERLIN	BIT
		,	BLANKLINES	BIT
		,	PCNG	BIT
		,	PBUSPERS	BIT
		,	PDEPTCODE	BIT
		,	PDEPTDESC	BIT
		,	CDNAME	BIT
		,	CDPHONE	BIT
		,	CDREMARKS	BIT
		,	PAXHOTEL	BIT
		,	HOTELPHONE	BIT
		,	HOTELREM	BIT
		,	PTANAME	BIT
		,	PTAPHONE	BIT
		,	PTAREMARKS	BIT
		,	CREWHOTEL	BIT
		,	CREWHPHONE	BIT
		,	CREWHADDR	BIT
		--,	MAINT	BIT
		--,	MAINTPHONE	BIT
		--,	MAINTADDR	BIT
		,	FILTFERRY	BIT
		,	SUPPAXNOTE	BIT
		,	SUPCRWNOTE	BIT
		,	DISPATCHNO	BIT
		,	PURPOSE	BIT
		,	AIRFLTPHN	BIT
		,	ALLOWSHADE	BIT
		,	ARRFBP	BIT
		,	PTACONFIRM	BIT
		,	PTACOMMENT	SMALLINT
		,	PTAFAX	BIT
		,	PTDNAME	BIT
		,	PTDPHONE	BIT
		,	PTDCONFIRM	BIT
		,	PTDCOMMENT	SMALLINT
		,	PTDREMARKS	BIT
		,	PTDFAX	BIT
		,	CTANAME	BIT
		,	CTAPHONE	BIT
		,	CTARATE	BIT
		,	CTACONFIRM	BIT
		,	CTACOMMENT	SMALLINT
		,	CTAREMARKS	BIT
		,	CTAFAX	BIT
		,	CTDNAME	BIT
		,	CTDPHONE	BIT
		,	CTDRATE	BIT
		,	CTDCONFIRM	BIT
		,	CTDCOMMENT	SMALLINT
		,	CTDREMARKS	BIT
		,	CTDFAX	BIT
		,	CDCONFIRM	BIT
		,	CDCOMMENT	SMALLINT
		,	CDFAX	BIT
		,	CANAME	BIT
		,	CAPHONE	BIT
		,	CACONFIRM	BIT
		,	CACOMMENT	SMALLINT
		,	CAREMARKS	BIT
		,	CAFAX	BIT
		,	HOTELROOMS	BIT
		,	HOTELCONF	BIT
		,	HOTELCOMM	SMALLINT
		,	HOTELFAX	BIT
		,	CREWHNRATE	BIT
		,	CREWHROOMS	BIT
		,	CREWHCONF	BIT
		,	CREWHCOMM	SMALLINT
		,	CREWHFAX	BIT
		,	CREWHREM	BIT
		,	OUTBOUND	BIT
		,	TERMTOAFBO	SMALLINT
		,	DEPFBP	BIT
		,	TERMTODFBO	SMALLINT
		--,	MAINTFAX	BIT
		--,	MAINTNRATE	BIT
		--,	MAINTROOMS	BIT
		--,	MAINTCONF	BIT
		--,	MAINTCOMM	SMALLINT
		--,	MAINTREM	BIT
		,	PAXNBR	BIT
		,	PRINTLOGO	BIT
		,	LINESEP	BIT
		,	REQUESTOR	BIT
		,	DEPICAO	BIT
		,	ARRICAO	BIT
		,	PRNFIRSTPG	BIT
		,	INTLFLTPHN	BIT
		,	CREWPILOT1	VARCHAR(100)
		,	CREWADDL	VARCHAR(100)
		,	CREWPILOT2	VARCHAR(100)
		,	FBOALPRICE	BIT
		,	FBOALDATE	BIT
		,	FBOACONT	BIT
		,	FBOAADDR	BIT
		,	FBOAREM	BIT
		,	FBOAPHONE2	BIT
		,	FBOAPAYTYP	BIT
		,	FBOAPOSTPR	BIT
		,	FBOAFUELQT	BIT
		,	FBOACONF	BIT
		,	FBOACOMM	SMALLINT
		,	FBODPHONE	BIT
		,	FBODFREQ	BIT
		,	FBODFAX	BIT
		,	FBODUVAIR	BIT
		,	FBODFUEL	BIT
		,	FBODNPRICE	BIT
		,	FBODLPRICE	BIT
		,	FBODLDATE	BIT
		,	FBODCONT	BIT
		,	FBODADDR	BIT
		,	FBODREM	BIT
		,	FBODPHONE2	BIT
		,	FBODPAYTYP	BIT
		,	FBODPOSTPR	BIT
		,	FBODFUELQT	BIT
		,	FBODCONF	BIT
		,	FBODCOMM	SMALLINT
		,	FBOAPHONE	BIT
		,	FBOAFREQ	BIT
		,	FBOAFAX	BIT
		,	FBOAUVAIR	BIT
		,	FBOAFUEL	BIT
		,	FBOANPRICE	BIT
		,	DISPATCHER	BIT
		,	DSPTEMAIL	BIT
		,	DSPTPHONE	BIT
		,	TRIPNO	BIT
		,	[DESC]	BIT
		,	TRIPDATES	BIT
		,	TMREQ	BIT
		,	TAILNMBR	BIT
		,	CONTPHONE	BIT
		,	TYPECODE	BIT
		,	CQDESC	SMALLINT
		,	REVISNNO	BIT
		,	REVISNDESC	BIT
		,	TIMEFORMAT	BIT
		,	CRMBLPHN	SMALLINT
		,	HOTELADDR	BIT
		,	PAXORDER	BIT
		,	CHRMONTH	BIT
		,	LEGSEP		BIT
		,	LEGNOTESF	SMALLINT
		,	CQNAME		SMALLINT
		,	CQCONTACT	SMALLINT
		,   DISPATCHIN  SMALLINT
		,DCREWHTL BIT
		,DCREWHNRTE BIT
		,DCREWHADDR BIT
		,DCREWHCOMM SMALLINT
		,DCREWHCONF BIT
		,DCREWHFAX BIT
		,DCREWHROOM BIT
		,DCREWHPHON BIT
		,DCREWHREM BIT
		--,DMAINHOTEL BIT
		--,DMAINHADD BIT
		--,DMAINHCOMM SMALLINT
		--,DMAINHCON BIT
		--,DMAINHFAX BIT
		--,DMAINHRATE BIT
		--,DMAINHPHON BIT
		--,DMAINHREM BIT
		--,DMAINHROOM BIT
		,DPAXHOTEL BIT
		,DPAXHPHONE BIT 
		,DPAXHREM BIT
		,DPAXHROOMS BIT
		,DPAXHCONF BIT
		,DPAXHCOMM SMALLINT
		,DPAXHFAX BIT
		,DPAXHADDR BIT
		)
	INSERT INTO @FormatSettings(LMESSAGE) values (NULL)
    SELECT * FROM @FormatSettings";
#endregion

            CalculateColumnDetails(strtablevar, 3, "ITINERARY", "RPTTS03");
        }

        private void btnCheckList_Click(object sender, EventArgs e)
        {
            lblSelectedItem.Text = "Checklist Selected";
            #region table var
            String strtablevar = @"DECLARE @FormatSettings TABLE (
	            ALERTS	BIT
	            ,NOTES	SMALLINT
	            ,LEG	SMALLINT
	            ,DISPATCHNO	BIT
	            ,PAXHOTEL	BIT
	            ,HOTELPHONE	BIT
	            ,HOTELFAX	BIT
	            ,HOTELREM	BIT
	            ,PTANAME	BIT
	            ,PTAPHONE	BIT
	            ,PTAFAX	BIT
	            ,PTAREMARKS	BIT
	            ,CANAME	BIT
	            ,CAPHONE	BIT
	            ,CAFAX	BIT
	            ,CAREMARKS	BIT
	            ,CREWHOTEL	BIT
	            ,CREWHPHONE	BIT
	            ,CREWHFAX	BIT
	            ,CREWHNRATE	BIT
	            ,CREWHADDR	BIT
	            ,CREWHREM	BIT
	            ,CTANAME	BIT
	            ,CTAPHONE	BIT
	            ,CTAFAX	BIT
	            ,CTARATE	BIT
	            ,CTAREMARKS	BIT
	            ,CREWHROOMS	BIT
	            ,CREWHCOMM	SMALLINT
	            ,CREWHCONF	BIT
	            ,HOTELROOMS	BIT
	            ,HOTELCONF	BIT
	            ,HOTELCOMM	SMALLINT
	            ,PTACONFIRM	BIT
	            ,PTACOMMENT	SMALLINT
	            ,PTDNAME	BIT
	            ,PTDPHONE	BIT
	            ,PTDFAX	BIT
	            ,PTDREMARKS	BIT
	            ,PTDCONFIRM	BIT
	            ,PTDCOMMENT	SMALLINT
	            ,CTACONFIRM	BIT
	            ,CTACOMMENT	SMALLINT
	            ,CTDNAME	BIT
	            ,CTDPHONE	BIT
	            ,CTDFAX	BIT
	            ,CTDRATE	BIT
	            ,CTDREMARKS	BIT
	            ,CTDCOMMENT	SMALLINT
	            ,CDNAME	BIT
	            ,CDPHONE	BIT
	            ,CDFAX	BIT
	            ,CDREMARKS	BIT
	            ,CDCONFIRM	BIT
	            ,CDCOMMENT	SMALLINT
	            ,CACONFIRM	BIT
	            ,CACOMMENT	SMALLINT
	            ,HOTELRATE	BIT
	            ,PTARATE	BIT
	            ,PTDRATE	BIT
	            ,CARATE	BIT
	            ,CDRATE	BIT
	            ,CTDCONFIRM	BIT
	            ,ONELEG	BIT
	            --,MAINTHOTEL	BIT
	            --,MAINTPHONE	BIT
	            --,MAINTFAX	BIT
	            --,MAINTNRATE	BIT
	            --,MAINTADDR	BIT
	            --,MAINTCOMM	SMALLINT
	            --,MAINTREM	BIT
	            --,MAINTCONF	BIT
	            --,MAINROOMS	BIT
	            ,NOTESF     SMALLINT
	            ,DISPATCHIN SMALLINT
	            ,DSPTEMAIL BIT
	            ,DISPATCHER BIT
	            ,DSPTPHONE BIT
	            ,DCREWHTL BIT
	            ,DCREWHADDR BIT
	            ,DCREWHCOMM SMALLINT
	            ,DCREWHCONF BIT
	            ,DCREWHFAX BIT
	            ,DCREWHNRTE BIT
	            ,DCREWHROOM BIT
	            ,DCREWHPHON BIT
	            ,DCREWHREM BIT
	            --,DMAINHOTEL BIT
	            --,DMAINHADD BIT
	            --,DMAINHCOMM SMALLINT
	            --,DMAINHCON BIT
	            --,DMAINHFAX BIT
	            --,DMAINHRATE BIT
	            --,DMAINHROOM BIT
	            --,DMAINHPHON BIT
	            --,DMAINHREM BIT
	            ,DPAXHOTEL BIT
	            ,DPAXHPHONE BIT
	            ,DPAXHFAX BIT
	            ,DPAXHREM BIT
	            ,DPAXHROOMS BIT
	            ,DPAXHCONF BIT
	            ,DPAXHCOMM SMALLINT
	            ,DPAXHRATE BIT
            );	
            INSERT INTO @FormatSettings(ALERTS) values (NULL)
            SELECT * FROM @FormatSettings";
            #endregion

            CalculateColumnDetails(strtablevar, 12, "TRIP CHECKLIST", "RPTTS12");

        }
    }
}
