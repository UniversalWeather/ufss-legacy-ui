﻿namespace TripSheetDataRectifier
{
    partial class frmTripSheetValidator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCustomerId = new System.Windows.Forms.TextBox();
            this.btnSummary = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboTripHeaders = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCustomer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtActualColumns = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtExpectedColumns = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMissingColumns = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAdditionalColumns = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtActual = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtExpectedCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTripSheet = new System.Windows.Forms.Button();
            this.btnItinerary = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRemoveAdditional = new System.Windows.Forms.TextBox();
            this.txtInsertMissing = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSelectedItem = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCheckList = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.Location = new System.Drawing.Point(85, 85);
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.Size = new System.Drawing.Size(100, 20);
            this.txtCustomerId.TabIndex = 0;
            // 
            // btnSummary
            // 
            this.btnSummary.Location = new System.Drawing.Point(12, 127);
            this.btnSummary.Name = "btnSummary";
            this.btnSummary.Size = new System.Drawing.Size(75, 23);
            this.btnSummary.TabIndex = 1;
            this.btnSummary.Text = "Summary";
            this.btnSummary.UseVisualStyleBackColor = true;
            this.btnSummary.Click += new System.EventHandler(this.btnSummary_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Customer Id:";
            // 
            // cboTripHeaders
            // 
            this.cboTripHeaders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTripHeaders.FormattingEnabled = true;
            this.cboTripHeaders.Location = new System.Drawing.Point(365, 85);
            this.cboTripHeaders.Name = "cboTripHeaders";
            this.cboTripHeaders.Size = new System.Drawing.Size(121, 21);
            this.cboTripHeaders.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(291, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Trip Headers:";
            // 
            // btnCustomer
            // 
            this.btnCustomer.Location = new System.Drawing.Point(202, 85);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(75, 23);
            this.btnCustomer.TabIndex = 5;
            this.btnCustomer.Text = "Get";
            this.btnCustomer.UseVisualStyleBackColor = true;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(4, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 48);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtActualColumns);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtExpectedColumns);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtMissingColumns);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtAdditionalColumns);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtActual);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtExpectedCount);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(3, 172);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(509, 250);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // txtActualColumns
            // 
            this.txtActualColumns.Location = new System.Drawing.Point(66, 92);
            this.txtActualColumns.Multiline = true;
            this.txtActualColumns.Name = "txtActualColumns";
            this.txtActualColumns.ReadOnly = true;
            this.txtActualColumns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtActualColumns.Size = new System.Drawing.Size(416, 40);
            this.txtActualColumns.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Actual:";
            // 
            // txtExpectedColumns
            // 
            this.txtExpectedColumns.Location = new System.Drawing.Point(65, 43);
            this.txtExpectedColumns.Multiline = true;
            this.txtExpectedColumns.Name = "txtExpectedColumns";
            this.txtExpectedColumns.ReadOnly = true;
            this.txtExpectedColumns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtExpectedColumns.Size = new System.Drawing.Size(416, 40);
            this.txtExpectedColumns.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Expected:";
            // 
            // txtMissingColumns
            // 
            this.txtMissingColumns.Location = new System.Drawing.Point(68, 197);
            this.txtMissingColumns.Multiline = true;
            this.txtMissingColumns.Name = "txtMissingColumns";
            this.txtMissingColumns.ReadOnly = true;
            this.txtMissingColumns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMissingColumns.Size = new System.Drawing.Size(416, 40);
            this.txtMissingColumns.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Missing:";
            // 
            // txtAdditionalColumns
            // 
            this.txtAdditionalColumns.Location = new System.Drawing.Point(67, 147);
            this.txtAdditionalColumns.Multiline = true;
            this.txtAdditionalColumns.Name = "txtAdditionalColumns";
            this.txtAdditionalColumns.ReadOnly = true;
            this.txtAdditionalColumns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAdditionalColumns.Size = new System.Drawing.Size(416, 40);
            this.txtAdditionalColumns.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Extra:";
            // 
            // txtActual
            // 
            this.txtActual.Location = new System.Drawing.Point(234, 16);
            this.txtActual.Name = "txtActual";
            this.txtActual.ReadOnly = true;
            this.txtActual.Size = new System.Drawing.Size(100, 20);
            this.txtActual.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Actual:";
            // 
            // txtExpectedCount
            // 
            this.txtExpectedCount.Location = new System.Drawing.Point(65, 15);
            this.txtExpectedCount.Name = "txtExpectedCount";
            this.txtExpectedCount.ReadOnly = true;
            this.txtExpectedCount.Size = new System.Drawing.Size(100, 20);
            this.txtExpectedCount.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Expected:";
            // 
            // btnTripSheet
            // 
            this.btnTripSheet.Location = new System.Drawing.Point(109, 126);
            this.btnTripSheet.Name = "btnTripSheet";
            this.btnTripSheet.Size = new System.Drawing.Size(75, 23);
            this.btnTripSheet.TabIndex = 8;
            this.btnTripSheet.Text = "TripSheet";
            this.btnTripSheet.UseVisualStyleBackColor = true;
            this.btnTripSheet.Click += new System.EventHandler(this.btnTripSheet_Click);
            // 
            // btnItinerary
            // 
            this.btnItinerary.Location = new System.Drawing.Point(202, 127);
            this.btnItinerary.Name = "btnItinerary";
            this.btnItinerary.Size = new System.Drawing.Size(75, 23);
            this.btnItinerary.TabIndex = 9;
            this.btnItinerary.Text = "Itinerary";
            this.btnItinerary.UseVisualStyleBackColor = true;
            this.btnItinerary.Click += new System.EventHandler(this.btnItinerary_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 426);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Remove Additional:";
            // 
            // txtRemoveAdditional
            // 
            this.txtRemoveAdditional.Location = new System.Drawing.Point(8, 442);
            this.txtRemoveAdditional.Multiline = true;
            this.txtRemoveAdditional.Name = "txtRemoveAdditional";
            this.txtRemoveAdditional.ReadOnly = true;
            this.txtRemoveAdditional.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemoveAdditional.Size = new System.Drawing.Size(499, 101);
            this.txtRemoveAdditional.TabIndex = 11;
            // 
            // txtInsertMissing
            // 
            this.txtInsertMissing.Location = new System.Drawing.Point(7, 565);
            this.txtInsertMissing.Multiline = true;
            this.txtInsertMissing.Name = "txtInsertMissing";
            this.txtInsertMissing.ReadOnly = true;
            this.txtInsertMissing.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInsertMissing.Size = new System.Drawing.Size(499, 101);
            this.txtInsertMissing.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 549);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Insert Missing:";
            // 
            // lblSelectedItem
            // 
            this.lblSelectedItem.AutoSize = true;
            this.lblSelectedItem.Location = new System.Drawing.Point(10, 157);
            this.lblSelectedItem.Name = "lblSelectedItem";
            this.lblSelectedItem.Size = new System.Drawing.Size(89, 13);
            this.lblSelectedItem.TabIndex = 14;
            this.lblSelectedItem.Text = "Nothing Selected";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(47, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(404, 45);
            this.label11.TabIndex = 15;
            this.label11.Text = "This Tool Does not modify anything in Database. \r\nIt requires Table View and Stor" +
    "ed procs Execute  permissions \r\nto function properly";
            // 
            // btnCheckList
            // 
            this.btnCheckList.Location = new System.Drawing.Point(294, 125);
            this.btnCheckList.Name = "btnCheckList";
            this.btnCheckList.Size = new System.Drawing.Size(75, 23);
            this.btnCheckList.TabIndex = 16;
            this.btnCheckList.Text = "CheckList";
            this.btnCheckList.UseVisualStyleBackColor = true;
            this.btnCheckList.Click += new System.EventHandler(this.btnCheckList_Click);
            // 
            // frmTripSheetValidator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 670);
            this.Controls.Add(this.btnCheckList);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblSelectedItem);
            this.Controls.Add(this.txtInsertMissing);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtRemoveAdditional);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnItinerary);
            this.Controls.Add(this.btnTripSheet);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCustomer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboTripHeaders);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSummary);
            this.Controls.Add(this.txtCustomerId);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmTripSheetValidator";
            this.Text = "Trip Sheet Data Rectifier";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCustomerId;
        private System.Windows.Forms.Button btnSummary;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboTripHeaders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCustomer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtExpectedCount;
        private System.Windows.Forms.TextBox txtActual;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAdditionalColumns;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMissingColumns;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtActualColumns;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtExpectedColumns;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnTripSheet;
        private System.Windows.Forms.Button btnItinerary;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtRemoveAdditional;
        private System.Windows.Forms.TextBox txtInsertMissing;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblSelectedItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCheckList;
    }
}

