-- Fix for Bug : 5809 - Wording & Capitalization is not consistent

UPDATE ExceptionTemplate SET ExceptionTemplate = 'Aircraft Type Code is required' WHERE ExceptionTemplateID = 1000
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Arrival ICAO ID is required' WHERE ExceptionTemplateID = 1001
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Departure ICAO ID is required' WHERE ExceptionTemplateID = 1002
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>  Distance beyond aircraft capability' WHERE ExceptionTemplateID = 1003
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Date Range between Current Leg <legnum1> and Previous Leg <legnum2> exceeds system defaults' WHERE ExceptionTemplateID = 1004
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Arrival Date/Time (Local) overlaps next Leg <legnum2> Departure Date/Time' WHERE ExceptionTemplateID = 1005
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Trip must have at least one Leg' WHERE ExceptionTemplateID = 1006
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Arrival Airport Minimum Runway Length below aircraft requirements' WHERE ExceptionTemplateID = 1007
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Arrival Airport is Inactive' WHERE ExceptionTemplateID = 1008
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Departure Airport is Inactive' WHERE ExceptionTemplateID = 1009
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Depart Date is required' WHERE ExceptionTemplateID = 1012
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Local Departure Date is required' WHERE ExceptionTemplateID = 1014
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Local Arrival Date is required' WHERE ExceptionTemplateID = 1015
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Departure Date/Time  (Local) overlaps previous Leg <legnum2> Arrival Date/Time' WHERE ExceptionTemplateID = 1017
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Duty Type for Leg <legnum> not available' WHERE ExceptionTemplateID = 1018
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> time aloft beyond aircraft capability' WHERE ExceptionTemplateID = 1019
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Tail: <tailnum> conflicts  Trip No.: <tripnum>  Departure: <dicaoid> Arrival: <aicaoid>' WHERE ExceptionTemplateID = 1021
UPDATE ExceptionTemplate SET ExceptionTemplate = 'No. of Passengers <PassengerCount> for leg <legnum>  exceeds aircraft seating capacity' WHERE ExceptionTemplateID = 1022
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew  <crewcode>  is not Active and is assigned on Leg ' WHERE ExceptionTemplateID = 1025
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew Checklist conflict exists for <crewcode>' WHERE ExceptionTemplateID = 1026
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew Codes <crewcode> not type rated in Aircraft Type' WHERE ExceptionTemplateID = 1027
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew <crewcode>: conflict in trip exists' WHERE ExceptionTemplateID = 1028
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Pax <paxcode>: conflict in trip exists' WHERE ExceptionTemplateID = 1029
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew Code <crewcode> flying on Leg <legnum> is not qualified under Crew Duty Rule <rulename>' WHERE ExceptionTemplateID = 1031
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew not assigned to Leg <legnum>' WHERE ExceptionTemplateID = 1032
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Crew <crewcode> is also flying on <triporlog> <tripnum> that falls within 24 hour period <trip> Please verify that there are no regulation violations' WHERE ExceptionTemplateID = 1033
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Department is Inactive' WHERE ExceptionTemplateID = 1034
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Authorization is Inactive' WHERE ExceptionTemplateID = 1035
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Department is Inactive' WHERE ExceptionTemplateID = 1036
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Authorization is Inactive' WHERE ExceptionTemplateID = 1037
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Aircraft Tail No. is required' WHERE ExceptionTemplateID = 2000
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Duplicate Log No. exists' WHERE ExceptionTemplateID = 2001
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Departure ICAO ID is required' WHERE ExceptionTemplateID = 2002
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival ICAO ID is required' WHERE ExceptionTemplateID = 2003
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Block Out Time cannot be earlier than Scheduled Time' WHERE ExceptionTemplateID = 2004
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Difference between Scheduled & Block Time exceeds system parameters' WHERE ExceptionTemplateID = 2005
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg Distance beyond aircraft capability' WHERE ExceptionTemplateID = 2006
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg Time Aloft beyond aircraft capability' WHERE ExceptionTemplateID = 2007
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Departure Airport is Inactive' WHERE ExceptionTemplateID = 2008
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival Airport is Inactive' WHERE ExceptionTemplateID = 2009
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Departure ICAO different from last Arrival ICAO' WHERE ExceptionTemplateID = 2010
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Fuel Out greater than aircraft Maximum Fuel amount' WHERE ExceptionTemplateID = 2011
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Fuel In less than aircraft Minimum Fuel amount' WHERE ExceptionTemplateID = 2012
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Invalid Out/Off Times' WHERE ExceptionTemplateID = 2013
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Difference between Out/Off greater than 30 Minutes' WHERE ExceptionTemplateID = 2014
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Invalid On/In Times' WHERE ExceptionTemplateID = 2015
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Difference between On/In greater than 30 Minutes' WHERE ExceptionTemplateID = 2016
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Requestor is missing' WHERE ExceptionTemplateID = 2017
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Department is missing' WHERE ExceptionTemplateID = 2018
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Authorization is missing' WHERE ExceptionTemplateID = 2019
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Flight Category is missing' WHERE ExceptionTemplateID = 2020
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Fuel Out is missing' WHERE ExceptionTemplateID = 2021
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Fuel In is missing' WHERE ExceptionTemplateID = 2022
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Fuel Used is missing' WHERE ExceptionTemplateID = 2023
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Duty Departure Time conflicts with Previous Leg arrive Time' WHERE ExceptionTemplateID = 2024
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Duty Arrival Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2025
UPDATE ExceptionTemplate SET ExceptionTemplate = 'You are using Scheduled Date/Time for Crew Duty calculations' WHERE ExceptionTemplateID = 2026
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Scheduled Date/Time conflicts with Previous Leg Arrival Time' WHERE ExceptionTemplateID = 2027
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Out Date/Time conflicts with Previous Leg Arrival Time' WHERE ExceptionTemplateID = 2028
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Off Date/Time conflicts with Previous Leg Arrival Time' WHERE ExceptionTemplateID = 2029
UPDATE ExceptionTemplate SET ExceptionTemplate = 'On Date/Time conflicts with Previous Leg Arrival Time' WHERE ExceptionTemplateID = 2030
UPDATE ExceptionTemplate SET ExceptionTemplate = 'In Date/Time conflicts with Previous Leg Arrival Time' WHERE ExceptionTemplateID = 2031
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Scheduled Date/Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2032
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Out Date/Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2033
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Off Date/Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2034
UPDATE ExceptionTemplate SET ExceptionTemplate = 'On Date/Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2035
UPDATE ExceptionTemplate SET ExceptionTemplate = 'In Date/Time conflicts with Next Leg Departure Time' WHERE ExceptionTemplateID = 2036
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Takeoff/Landing mismatch' WHERE ExceptionTemplateID = 2037
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Night Takeoffs but no Night Time Hours' WHERE ExceptionTemplateID = 2038
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Night Landings but no Night Time Hours' WHERE ExceptionTemplateID = 2039
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Night Time Hours greater than Flight Hours' WHERE ExceptionTemplateID = 2040
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Instrument Time Hours greater than Flight Hours' WHERE ExceptionTemplateID = 2041
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Account No. is required' WHERE ExceptionTemplateID = 2042
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Year No. is required' WHERE ExceptionTemplateID = 2043
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Trip must have at least one Leg' WHERE ExceptionTemplateID = 2044
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Department is Inactive' WHERE ExceptionTemplateID = 2046
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Authorization is Inactive' WHERE ExceptionTemplateID = 2047
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Department is Inactive' WHERE ExceptionTemplateID = 2048
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Authorization is Inactive' WHERE ExceptionTemplateID = 2049
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Total Quote <MainTotalQuote>  is over the Customers Credit Limit of <FileTotalQuote>' WHERE ExceptionTemplateID = 3000
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Departure Airport is Inactive' WHERE ExceptionTemplateID = 3001
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival Airport is Inactive' WHERE ExceptionTemplateID = 3002
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Arrival Date/Time (Local) overlaps Next Leg <legnum2> Departure Date/Time' WHERE ExceptionTemplateID = 3003
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Departure Date/Time  (Local) overlaps Previous Leg <legnum2> Arrival Date/Time' WHERE ExceptionTemplateID = 3004
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Distance beyond aircraft capability' WHERE ExceptionTemplateID = 3005
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival Airport Minimum Runway Length below aircraft requirements' WHERE ExceptionTemplateID = 3006
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Time Aloft beyond aircraft capability' WHERE ExceptionTemplateID = 3007
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Home Base is required' WHERE ExceptionTemplateID = 3008
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Vendor Code is required' WHERE ExceptionTemplateID = 3009
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Tail No. is required' WHERE ExceptionTemplateID = 3010
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Type Code is required' WHERE ExceptionTemplateID = 3011
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Depart ICAO ID is required' WHERE ExceptionTemplateID = 3012
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival ICAO ID is required' WHERE ExceptionTemplateID = 3013
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Trip Date is required' WHERE ExceptionTemplateID = 3014
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Quote Date is required' WHERE ExceptionTemplateID = 3015
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Depart Date is required' WHERE ExceptionTemplateID = 3016
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Arrival Date is required' WHERE ExceptionTemplateID = 3017
UPDATE ExceptionTemplate SET ExceptionTemplate = 'No. of Passengers <PassengerCount> exceeds aircraft seating capacity' WHERE ExceptionTemplateID = 3018
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Aircraft Type Code is required' WHERE ExceptionTemplateID = 4000
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Arrival ICAO ID is required' WHERE ExceptionTemplateID = 4001
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Departure ICAO ID is required' WHERE ExceptionTemplateID = 4002
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>  Distance beyond aircraft capability' WHERE ExceptionTemplateID = 4003
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Date Range between Current Leg <legnum1> and Previous Leg <legnum2> exceeds system defaults' WHERE ExceptionTemplateID = 4004
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Arrival Date/Time (Local) overlaps next Leg <legnum2> Departure Date/Time' WHERE ExceptionTemplateID = 4005
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Request must have at least one Leg' WHERE ExceptionTemplateID = 4006
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Arrival Airport Minimum Runway Length below aircraft requirements' WHERE ExceptionTemplateID = 4007
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Arrival Airport is Inactive' WHERE ExceptionTemplateID = 4008
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Departure Airport is Inactive' WHERE ExceptionTemplateID = 4009
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Depart Date is required' WHERE ExceptionTemplateID = 4010
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Local Departure Date is required' WHERE ExceptionTemplateID = 4011
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Local Arrival Date is required' WHERE ExceptionTemplateID = 4012
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum1> Departure Date/Time  (Local) overlaps Previous Leg <legnum2> Arrival Date/Time' WHERE ExceptionTemplateID = 4013
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Time aloft beyond aircraft capability' WHERE ExceptionTemplateID = 4014
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum>: Tail: <tailnum> conflicts  Trip No.: <tripnum>  Departure: <dicaoid> Arrival: <aicaoid>' WHERE ExceptionTemplateID = 4016
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Department is Inactive' WHERE ExceptionTemplateID = 4018
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Authorization is Inactive' WHERE ExceptionTemplateID = 4019
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Department is Inactive' WHERE ExceptionTemplateID = 4020
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Leg <legnum> Authorization is Inactive' WHERE ExceptionTemplateID = 4021
UPDATE ExceptionTemplate SET ExceptionTemplate = 'No. of Passengers <PassengerCount> for Leg <legnum>  exceeds aircraft seating capacity' WHERE ExceptionTemplateID = 4022
UPDATE ExceptionTemplate SET ExceptionTemplate = 'Quote has no Leg' WHERE ExceptionTemplateID = 3019
GO