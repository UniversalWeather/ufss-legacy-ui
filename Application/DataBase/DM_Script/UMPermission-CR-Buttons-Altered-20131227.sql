delete from UMPermission where UMPermissionID in (10375,10376,10377)


-- Added this Script to add Permission to CRTransfer,Deny,Mark Accepted
INSERT INTO [dbo].[UMPermission]
           ([UMPermissionID]
           ,[UMPermissionName]
           ,[ModuleID]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[UMPermissionRole]
           ,[FormName]
           ,[IsInActive])
     VALUES
           (10365
           ,'Corporate Request - Transfer'
           ,10003
           ,'UWA Admin'
           ,GETUTCDATE()
           ,0
           ,'CRTransfer'
           ,'CRTRANSFER'
           ,NULL)
GO

INSERT INTO [dbo].[UMPermission]
           ([UMPermissionID]
           ,[UMPermissionName]
           ,[ModuleID]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[UMPermissionRole]
           ,[FormName]
           ,[IsInActive])
     VALUES
           (10366
           ,'Corporate Request - Deny'
           ,10003
           ,'UWA Admin'
           ,GETUTCDATE()
           ,0
           ,'CRDeny'
           ,'CRDENY'
           ,NULL)
GO


INSERT INTO [dbo].[UMPermission]
           ([UMPermissionID]
           ,[UMPermissionName]
           ,[ModuleID]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[UMPermissionRole]
           ,[FormName]
           ,[IsInActive])
     VALUES
           (10367
           ,'Corporate Request - MarkAccepted'
           ,10003
           ,'UWA Admin'
           ,GETUTCDATE()
           ,0
           ,'CRMarkAccepted'
           ,'CRMARKACCEPTED'
           ,NULL)
GO



