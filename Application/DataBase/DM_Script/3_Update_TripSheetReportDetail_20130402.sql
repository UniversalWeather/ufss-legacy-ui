-- UPDATE TripSheetReportDetail for Tripsheet Report

UPDATE TripSheetReportDetail SET ParameterDescription='CREW  -  PILOT 1'
                             WHERE ParameterVariable='CREWPILOT1'
                              AND REPORTNUMBER=2 
UPDATE TripSheetReportDetail SET ParameterDescription='CREW  -  PILOT 2'
                             WHERE  REPORTNUMBER=2 
                              AND ParameterVariable='CREWPILOT2'
UPDATE TripSheetReportDetail SET ParameterDescription='CREW  -  ADDL CREW'
                             WHERE  REPORTNUMBER=2   
                               AND ParameterVariable='CREWADDL' 
UPDATE TripSheetReportDetail SET ParameterDescription='CREW - DATE OF BIRTH'
                             WHERE REPORTNUMBER=2 
                               AND ParameterVariable='CRWDOB' 
UPDATE TripSheetReportDetail SET ParameterDescription='CREW - NATIONALITY'
                             WHERE REPORTNUMBER=2  
                               AND ParameterVariable='CRWNATION' 
UPDATE TripSheetReportDetail SET ParameterDescription='CREW - PASSPORT'
                             WHERE  REPORTNUMBER=2  
                               AND ParameterVariable='CRWPNUM' 
UPDATE TripSheetReportDetail SET ParameterDescription='CREW - PASSPORT EXPIRY DATE'
                             WHERE REPORTNUMBER=2 
                               AND ParameterVariable='CRWPDATE' 
--UPDATE TripSheetReportDetail SET ParameterNValue=1
--                             WHERE ParameterVariable='DISPATCHIN'
--                              AND REPORTNUMBER=2  
                            
----------Dispatch Information Update for 10 Reports------------------

UPDATE TripSheetReportDetail SET ParameterNValue=1,ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=2  
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=1      
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=3                                  
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=4  
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=5     
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=6      
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=8   
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=11     
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=12    
UPDATE TripSheetReportDetail SET ParameterDescription='DISPATCHER - INFORMATION - 1=TOP, 2=BOTTOM'
                             WHERE ParameterVariable='DISPATCHIN'
                              AND REPORTNUMBER=13                                
--select distinct reportnumber from TripSheetReportDetail where ParameterVariable='DISPATCHIN' and reportnumber=4    


