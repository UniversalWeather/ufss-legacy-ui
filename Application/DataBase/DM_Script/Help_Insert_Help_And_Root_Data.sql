Insert into HelpContent Values('AccountsHelp','Company','Accounts','',null,getDate(),0,0)
Insert into HelpContent Values('BudgetHelp','Company','Budget','',null,getDate(),0,0)
Insert into HelpContent Values('ConfiguringCompanyProfile','Company','Configuring Company Profile','',null,getDate(),0,0)
Insert into HelpContent Values('CustomAddressHelp','Company','Custom Address','',null,getDate(),0,0)
Insert into HelpContent Values('DeptGroupHelp','Company','Department Group','',null,getDate(),0,0)
Insert into HelpContent Values('DeptAuthorizationHelp','Company','Department/Authorization','',null,getDate(),0,0)
Insert into HelpContent Values('PayableVendorHelp','Company','Payables Vendor','',null,getDate(),0,0)
Insert into HelpContent Values('PaymentTypesHelp','Company','Payment Types','',null,getDate(),0,0)
Insert into HelpContent Values('SIFLRateHelp','Company','SIFL Rate','',null,getDate(),0,0)
Insert into HelpContent Values('CustomAddressHelp','Company','Custom Address','',null,getDate(),0,0)
Insert into HelpContent Values('VendorHelp','Company','Vendor','',null,getDate(),0,0)

Insert into HelpContent Values('AircraftDutyType','Fleet','Aircraft Duty Type','',null,getDate(),0,0)
Insert into HelpContent Values('AircraftTypeHelp','Fleet','Aircraft Type','',null,getDate(),0,0)
Insert into HelpContent Values('DelayTypeHelp','Fleet','Delay Types','',null,getDate(),0,0)
Insert into HelpContent Values('FleetGroupHelp','Fleet','Fleet Group','',null,getDate(),0,0)
Insert into HelpContent Values('FleetProfileHelp','Fleet','Fleet Profile','',null,getDate(),0,0)
Insert into HelpContent Values('FleetProfileAdditionalInfoHelp','Fleet','Fleet Profile Additional Info','',null,getDate(),0,0)
Insert into HelpContent Values('FlightCategoriesHelp','Fleet','Flight Categories','',null,getDate(),0,0)


Insert into HelpContent Values('ClientHelp','People','Client Code','',null,getDate(),0,0)
Insert into HelpContent Values('CrewAddInfoHelp','People','Crew Additonal Info Codes','',null,getDate(),0,0)
Insert into HelpContent Values('CrewChecklistHelp','People','Crew Checklist Codes','',null,getDate(),0,0)
Insert into HelpContent Values('CrewDutyRulesHelp','People','Crew Duty Rules','',null,getDate(),0,0)
Insert into HelpContent Values('CrewDutyTypesHelp','People','Crew Duty Types','',null,getDate(),0,0)
Insert into HelpContent Values('CrewGroupHelp','People','Crew Group','',null,getDate(),0,0)
Insert into HelpContent Values('CrewRosterHelp','People','Crew Roster','',null,getDate(),0,0)
Insert into HelpContent Values('EmergencyContactHelp','People','Emergency Contact','',null,getDate(),0,0)
Insert into HelpContent Values('FlightPurposeHelp','People','Flight Purposes','',null,getDate(),0,0)
Insert into HelpContent Values('PassengerAddInfoHelp','People','Aircraftype','',null,getDate(),0,0)
Insert into HelpContent Values('PassengerGroupHelp','People','Passenger Group','',null,getDate(),0,0)
Insert into HelpContent Values('PassengerRequestorHelp','People','Passengers/Requestors','',null,getDate(),0,0)
Insert into HelpContent Values('TravelCoordinatorHelp','People','Travel Coordinator','',null,getDate(),0,0)


Insert into HelpContent Values('AirportHelp','Logistics','Airport','',null,getDate(),0,0)
Insert into HelpContent Values('CountryHelp','Logistics','Country','',null,getDate(),0,0)
Insert into HelpContent Values('DSTRegionHelp','Logistics','DST Region','',null,getDate(),0,0)
Insert into HelpContent Values('ExchangeRateHelp','Logistics','Exchange Rate','',null,getDate(),0,0)
Insert into HelpContent Values('FuelLocatorHelp','Logistics','Fuel Locators','',null,getDate(),0,0)
Insert into HelpContent Values('FuelVendorHelp','Logistics','Fuel Vendor','',null,getDate(),0,0)
Insert into HelpContent Values('MetroHelp','Logistics','Metro City','',null,getDate(),0,0)
Insert into HelpContent Values('TripMgrChkLstHelp','Logistics','Trip Checklist','',null,getDate(),0,0)
Insert into HelpContent Values('TripMgrChkLstGrpHelp','Logistics','Trip Checklist Group','',null,getDate(),0,0)


Insert into HelpContent Values('LoggingintoSystem','Administration','Logging into System','',null,getDate(),0,0)
Insert into HelpContent Values('UserManagement','Administration','User Management','',null,getDate(),0,0)


Insert into HelpContent Values('Preflight','Preflight','Preflight','',null,getDate(),0,0)

Insert into HelpContent Values('SchedulingCalendar','Scheduling Calendar','Scheduling Calendar','',null,getDate(),0,0)

Insert into HelpContent Values('Postflight','Postflight','Postflight','',null,getDate(),0,0)

Insert into HelpContent Values('Request','CorporateRequest','CorporateRequest','',null,getDate(),0,0)

Insert into HelpContent Values('Glossary','Glossary','Glossary','',null,getDate(),0,0)

Go
INSERT INTO [dbo].[HelpItems]
           ([Name]
           ,[ParentID]
           ,[MimeType]
           ,[IsDirectory]
           ,[Size]
           ,[Content])
     VALUES
            ('ROOT'
            ,NULL
           ,NULL
           ,1
           ,NULL
           ,NULL)
GO


