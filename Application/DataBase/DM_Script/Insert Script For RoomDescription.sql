SET IDENTITY_INSERT [dbo].[RoomDescription] ON 
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 1)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1, N'0', N'Select', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = '0', [RoomDesc] = 'Select', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 1
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 2)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (2, N'Standard', N'Standard', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Standard', [RoomDesc] = 'Standard', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 2
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 3)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (3, N'Deluxe', N'Deluxe', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Deluxe', [RoomDesc] = 'Deluxe', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 3
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 4)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (4, N'Club', N'Club', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Club', [RoomDesc] = 'Club', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 4
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 5)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (5, N'Crew', N'Crew', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Crew', [RoomDesc] = 'Crew', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 5
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 6)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (6, N'Executive', N'Executive', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Executive', [RoomDesc] = 'Executive', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 6
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 7)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (7, N'Exec Suite', N'Exec Suite', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Exec Suite', [RoomDesc] = 'Exec Suite', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 7
GO
IF NOT EXISTS(SELECT * FROM [dbo].[RoomDescription] WHERE [RoomDescriptionID] = 8)
	INSERT [dbo].[RoomDescription] ([RoomDescriptionID], [RoomDescCode], [RoomDesc], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (8, N'Pre Suite', N'Pre Suite', N'UWA Admin', GETDATE(), 0, 0)
ELSE
	UPDATE [dbo].[RoomDescription] SET [RoomDescCode] = 'Pre Suite', [RoomDesc] = 'Pre Suite', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = 0 WHERE [RoomDescriptionID] = 8
GO
SET IDENTITY_INSERT [dbo].[RoomDescription] OFF
GO