IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 4)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 4
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 5)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 5
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 50)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 50
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 42)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 42
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 43)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 43
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 44)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 44
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 45)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 45
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 46)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 46
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 47)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 47
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 48)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 48
END
GO
IF EXISTS(SELECT * FROM [dbo].[FPSearchParameters] WHERE SearchId = 49)
BEGIN
	DELETE FROM [dbo].[FPSearchParameters] WHERE SearchId = 49
END
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (4, N'TRIPS', N'PreflightMain', N'PreFlightMain', N'views/Transactions/PreFlight/PreflightMain.aspx?seltab=PreFlight', 0, N'Trips', N'
SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
PM.TripId AS PrimaryKeyValue,
''<b>Trip No.: </b>'' + ISNULL(REPLACE(PM.TripNUM,'''''''',''"''),'''') + ''<br/>'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(A.ICAOID,'''''''',''"''),'''') + ''<br/>'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(F.TailNum,'''''''',''"''),'''') + ''<br/>'' +
''<b>Flight No.: </b>'' + ISNULL(REPLACE(PM.FlightNUM,'''''''',''"''),'''') + ''<br/>'' +
''<b>Trip purpose: </b>'' + ISNULL(REPLACE(PM.TripDescription,'''''''',''"''),'''') + ''<br/>'' +
''<b>Dispatch #: </b>'' + ISNULL(REPLACE(PM.DispatchNUM,'''''''',''"''),'''') + ''<br/>'' +
''<b>Requestor: </b>'' + ISNULL(REPLACE(P.PassengerRequestorCD,'''''''',''"''),'''') + ''<br/>'' +
''<b>Dispatcher: </b>'' + ISNULL(REPLACE(PM.DispatcherUserName,'''''''',''"''),'''') + ''<br/>'' +
''<b>Account #: </b>'' + ISNULL(REPLACE(Acc.AccountNum,'''''''',''"''),'''') + ''<br/>'' +
''<b>Depatment: </b>'' + ISNULL(REPLACE(Depart.DepartmentCD,'''''''',''"''),'''') + ''<br/>'' +
''<b>Authorization: </b>'' + ISNULL(REPLACE(Auth.AuthorizationCD,'''''''',''"''),'''') + ''<br/>'' +
''<b>Client Code: </b>'' + ISNULL(REPLACE(Cli.ClientCD,'''''''',''"''),'''') + ''<br/>'' 
AS ColumnValue    
FROM PreflightMain PM  
INNER JOIN Company C on PM.HomebaseID = C.HomebaseID AND PM.CustomerID = C.CustomerID 
INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID AND A.CustomerID = C.CustomerID
LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID  AND Isnull(POM.IsDeleted,0) = 0 AND PM.CustomerID = POM.CustomerID
LEFT JOIN Fleet F on  PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID AND PM.CustomerID = P.CustomerID 
LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID AND PM.CustomerID = UM.CustomerID
LEFT JOIN Account Acc ON Acc.AccountId = PM.AccountID AND Acc.CustomerID = PM.CustomerID
LEFT JOIN Department Depart ON Depart.DepartmentID = PM.DepartmentId AND Depart.CustomerID = PM.CustomerID
LEFT JOIN DepartmentAuthorization Auth ON Auth.AuthorizationID = PM.AuthorizationID AND Auth.CustomerID = PM.CustomerID
LEFT JOIN Client Cli ON Cli.ClientID = PM.ClientID AND Cli.CustomerID = PM.CustomerID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(PM.TripNUM like ''%'' + ''@SearchStr'' +''%'' 
OR A.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR F.TailNum like ''%'' + ''@SearchStr'' +''%''
OR PM.FlightNUM like ''%'' + ''@SearchStr'' +''%''
OR PM.TripDescription like ''%'' + ''@SearchStr'' +''%''
OR PM.DispatchNUM like ''%'' + ''@SearchStr'' +''%''
OR P.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%''
OR PM.DispatcherUserName like ''%'' + ''@SearchStr'' +''%''
OR Acc.AccountNum like ''%'' + ''@SearchStr'' +''%''
OR Depart.DepartmentCD like ''%'' + ''@SearchStr'' +''%''
OR Auth.AuthorizationCD like ''%'' + ''@SearchStr'' +''%''
OR Cli.ClientCD like ''%'' + ''@SearchStr'' +''%''

-- LEG
OR 1 = (SELECT TOP 1 1 FROM PreflightLeg 
LEFT JOIN Airport A ON A.AirportID = PreflightLeg.DepartICAOID AND A.CustomerID = PreflightLeg.CustomerID
LEFT JOIN Airport B ON B.AirportID = PreflightLeg.ArriveICAOID AND B.CustomerID = PreflightLeg.CustomerID
WHERE 
(A.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 B.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightLeg.FlightNUM like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightLeg.TripID = PM.TripID AND PreflightLeg.IsDeleted = 0)

-- Crew

OR 1 = (SELECT TOP 1 1 FROM PreflightCrewList 
INNER JOIN Crew A ON A.CrewID = PreflightCrewList.CrewID AND A.CustomerID = PreflightCrewList.CustomerID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightCrewList.LegID AND PreflightLeg.TripID = PM.TripID AND PreflightLeg.CustomerID = PM.CustomerID
WHERE 
(A.CrewCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightCrewList.IsDeleted = 0)

-- PAX

OR 1 = (SELECT TOP 1 1 FROM PreflightPassengerList 
INNER JOIN Passenger A ON A.PassengerRequestorID = PreflightPassengerList.PassengerID AND A.CustomerID = PreflightPassengerList.CustomerID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightPassengerList.LegID AND PreflightLeg.TripID = PM.TripID
WHERE 
(A.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightPassengerList.IsDeleted = 0)

-- Logistics

OR 1 = (SELECT TOP 1 1 FROM PreflightFBOList 
INNER JOIN FBO A ON A.FBOID = PreflightFBOList.FBOID AND A.CustomerID = PreflightFBOList.CustomerID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightFBOList.LegID AND PreflightLeg.CustomerID = PreflightFBOList.CustomerID
WHERE PreflightFBOList.IsDeleted = 0 AND PreflightLeg.CustomerID = PM.CustomerID AND PreflightLeg.TripID = PM.TripID
AND (A.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.FBOVendor like ''%'' + ''@SearchStr'' +''%'' 
))

-- Catering

OR 1 = (SELECT TOP 1 1 FROM PreflightCateringDetail 
INNER JOIN catering A ON A.CateringID = PreflightCateringDetail.CateringID AND A.CustomerID = PreflightCateringDetail.CustomerID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightCateringDetail.LegID AND PreflightLeg.CustomerID = PreflightCateringDetail.CustomerID
WHERE PreflightCateringDetail.IsDeleted = 0 AND PreflightLeg.TripID = PM.TripID
AND PreflightLeg.CustomerID = PM.CustomerID
AND (A.CateringCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCateringDetail.ContactName like ''%'' + ''@SearchStr'' +''%'' 
))
)  
AND
PM.RecordType= ''T'' and PM.CustomerID=@CustomerId and PM.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PreFlightMain'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (5, N'TRIPLOGS', N'PostflightMain', N'PostflightMain', N'views/Transactions/PostFlight/PostFlightMain.aspx', 0, N'Trip Logs', N'
SELECT   
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
POMain.POLogID AS PrimaryKeyValue,
''<b>Log No: </b>'' + ISNULL(REPLACE(POMain.LogNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(airport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(fleet.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Client Code: </b>'' + ISNULL(REPLACE(client.ClientCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Flight No.: </b>'' + ISNULL(REPLACE(POMain.FlightNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Dispatch No.: </b>'' + ISNULL(REPLACE(POMain.DispatchNum,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue      
FROM PostflightMain POMain  
LEFT OUTER JOIN PreflightMain preflightMain ON preflightMain.TripID = POMain.TripID AND preflightMain.CustomerID = POMain.CustomerID
LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = POMain.FleetID AND fleet.CustomerID = POMain.CustomerID
LEFT OUTER JOIN Company company on POMain.HomeBaseID = company.HomeBaseID AND POMain.CustomerID = company.CustomerID
LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID  
LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = POMain.AuthorizationID AND auth.CustomerID = POMain.CustomerID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
LEFT JOIN Client client ON Client.ClientID = POMain.ClientID
WHERE  
(
POMain.LogNum like ''%'' + ''@SearchStr'' +''%'' OR
airport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
fleet.TailNum like ''%'' + ''@SearchStr'' +''%'' OR
client.ClientCD like ''%'' + ''@SearchStr'' +''%'' OR
POMain.FlightNum like ''%'' + ''@SearchStr'' +''%'' OR
POMain.DispatchNum like ''%'' + ''@SearchStr'' +''%'' 

-- LEG
OR 1 = (SELECT TOP 1 1 FROM PostflightLeg 
LEFT JOIN Airport A ON A.AirportID = PostflightLeg.DepartICAOID AND A.CustomerID = PostflightLeg.CustomerID
LEFT JOIN Airport B ON B.AirportID = PostflightLeg.ArriveICAOID AND B.CustomerID = PostflightLeg.CustomerID
WHERE PostflightLeg.POLogID = POMain.POLogID AND PostflightLeg.IsDeleted = 0 AND
(A.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
B.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
PostflightLeg.FlightNum like ''%'' + ''@SearchStr'' +''%'' OR
PostflightLeg.FlightPurpose like ''%'' + ''@SearchStr'' +''%'' 
))

-- Crew

OR 1 = (SELECT TOP 1 1 FROM PostflightCrew 
INNER JOIN Crew A ON A.CrewID = PostflightCrew.CrewID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightCrew.POLegID AND PostflightLeg.CustomerID = PostflightCrew.CustomerID
WHERE PostflightCrew.IsDeleted = 0 AND PostflightLeg.POLogID = POMain.POLogID
AND PostflightLeg.CustomerID = POMain.CustomerID AND
(A.CrewCD like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewLastName like ''%'' + ''@SearchStr'' +''%'' 
))

-- PAX

OR 1 = (SELECT TOP 1 1 FROM PostflightPassenger 
LEFT JOIN Passenger A ON A.PassengerRequestorID = PostflightPassenger.PassengerID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightPassenger.POLegID AND PostflightLeg.CustomerID = PostflightPassenger.CustomerID
WHERE PostflightPassenger.IsDeleted = 0 AND PostflightLeg.POLogID = POMain.POLogID
AND PostflightLeg.CustomerID = POMain.CustomerID AND 
(A.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerLastName like ''%'' + ''@SearchStr'' +''%'' 
))

-- EXPENSE

OR 1 = (SELECT TOP 1 1 FROM PostflightExpense 
LEFT JOIN FBO A ON A.FBOID = PostflightExpense.FBOID AND A.CustomerID = PostflightExpense.CustomerID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightExpense.POLegID AND PostflightLeg.CustomerID = PostflightExpense.CustomerID
WHERE PostflightExpense.IsDeleted = 0 AND PostflightLeg.POLogID = POMain.POLogID AND
PostflightLeg.CustomerID = POMain.CustomerID AND
(A.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.FBOVendor like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightExpense.SlipNUM like ''%'' + ''@SearchStr'' +''%'' 
))

) AND
POMain.CustomerID = @CustomerID  
AND isnull(POMain.IsDeleted,0) = 0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''POLogManager'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (50, N'TRIPLOGS', N'PostflightMain', N'PostflightMain', N'views/Transactions/PostFlight/ExpenseCatalog.aspx', 0, N'Trip Logs', N'
SELECT   
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
PostflightExpense.PostflightExpenseID AS PrimaryKeyValue,
''<b>Log No: </b>'' + ISNULL(REPLACE(POMain.LogNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(fleet.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Slip No.: </b>'' + ISNULL(REPLACE(PostflightExpense.SlipNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(HomebaseAirport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>ICAO: </b>'' + ISNULL(REPLACE(Airport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>FBO Code: </b>'' + ISNULL(REPLACE(FBO.FBOCD,'''''''',''"''),'''') + ''<br />'' +
''<b>FBO Name: </b>'' + ISNULL(REPLACE(FBO.FBOVendor,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue      
FROM PostflightMain POMain  
INNER JOIN PostflightExpense ON PostflightExpense.POLogID = POMain.POLogID AND PostflightExpense.CustomerID = POMain.CustomerID
LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = PostflightExpense.FleetID AND fleet.CustomerID = PostflightExpense.CustomerID
LEFT OUTER JOIN Company company on PostflightExpense.HomeBaseID = company.HomeBaseID AND PostflightExpense.CustomerID = company.CustomerID
LEFT OUTER JOIN Airport HomebaseAirport ON HomebaseAirport.AirportID = company.HomebaseAirportID  
LEFT OUTER JOIN Airport ON Airport.AirportID = PostflightExpense.AirportID  
LEFT JOIN FBO ON FBO.FBOID = PostflightExpense.FBOID AND FBO.CustomerID = PostflightExpense.CustomerID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId

WHERE  
(
POMain.LogNum like ''%'' + ''@SearchStr'' +''%'' OR
fleet.TailNum like ''%'' + ''@SearchStr'' +''%'' OR
PostflightExpense.SlipNUM like ''%'' + ''@SearchStr'' +''%'' OR
HomebaseAirport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
Airport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
FBO.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
FBO.FBOVendor like ''%'' + ''@SearchStr'' +''%''

) AND
PostflightExpense.CustomerID = @CustomerID  
AND isnull(PostflightExpense.IsDeleted,0) = 0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''POLogManager'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (42, N'CHARTER', N'CQCustomer', N'CQCustomer', N'Views/Settings/People/CharterQuoteCustomerCatalog.aspx?Screen=CharterQuote', 0, N'Charter Quote Customer', N'
SELECT FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQCustomer.CQCustomerID AS PrimaryKeyValue,
''<b>Customer Code: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerCD,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Customer Name: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerName,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Closest ICAO: </b>'' + ISNULL(REPLACE(A.IcaoID,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Billing Infor - Name: </b>'' + ISNULL(REPLACE(CQCustomer.BillingName,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Main Contact - Name: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerName,'''''''',''"''),'''') + ''<br />'' 

AS ColumnValue

  FROM CQCustomer       
  LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
  LEFT OUTER JOIN Company ON Company.HomebaseID = CQCustomer.HomebaseID AND Company.CustomerID = CQCustomer.CustomerID        
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID AND Airport.CustomerID = Company.CustomerID
  LEFT OUTER JOIN Airport A ON A.AirportID =  CQCustomer.AirportID AND A.CustomerID = CQCustomer.CustomerID
  WHERE 
  (
  CQCustomerCD like ''%'' + ''@SearchStr'' +''%'' 
   OR CQCustomerName like ''%'' + ''@SearchStr'' +''%''
   OR Airport.IcaoID like ''%'' + ''@SearchStr'' +''%''
   OR A.IcaoID like ''%'' + ''@SearchStr'' +''%''
   OR CQCustomer.BillingName like ''%'' + ''@SearchStr'' +''%''
   OR CQCustomer.CQCustomerName like ''%'' + ''@SearchStr'' +''%''
  )  AND CQCustomer.CustomerID = @CustomerID AND CQCustomer.IsDeleted = 0
  AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQCustomer'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (43, N'CHARTER', N'FeeSchedule', N'FeeSchedule', N'Views/Settings/Fleet/FeeScheduleCatalog.aspx?Screen=FeeSchedule', 0, N'Fee Schedule', N'
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FeeScheduleID AS PrimaryKeyValue,
''<b>Account No.: </b>'' + ISNULL(REPLACE(Account.AccountNum,'''''''',''"''),'''') + ''<br />''  
+  ''<b>Group: </b>'' + ISNULL(REPLACE(FeeGroup.FeeGroupCD,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM FeeSchedule    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId    
    LEFT JOIN Account ON Account.AccountID = FeeSchedule.AccountID AND Account.CustomerID = FeeSchedule.CustomerID
    LEFT JOIN FeeGroup ON FeeGroup.FeeGroupID = FeeSchedule.FeeGroupID AND FeeGroup.CustomerID = FeeSchedule.CustomerID
WHERE     (    
Account.AccountNum like ''%'' + ''@SearchStr'' +''%''     
OR FeeGroup.FeeGroupCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND FeeSchedule.CustomerID = @CustomerID
AND FeeSchedule.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FeeSchedule'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (44, N'CHARTER', N'FeeGroup', N'FeeGroup', N'Views/Settings/Fleet/FeeScheduleGroupCatalog.aspx?Screen=FeeScheduleGroup', 0, N'Fee Schedule Group', N'
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FeeGroupID AS PrimaryKeyValue,
''<b>Fee Schedule Group Code: </b>'' + ISNULL(REPLACE(FeeGroup.FeeGroupCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM FeeGroup    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 FeeGroup.FeeGroupCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND FeeGroup.CustomerID = @CustomerID
AND FeeGroup.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FeeGroup'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (45, N'CHARTER', N'LeadSource', N'LeadSource', N'Views/Settings/Fleet/LeadSourceCatalog.aspx?Screen=LeadSource', 0, N'Lead Source', N'
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

LeadSourceID AS PrimaryKeyValue,
''<b>Lead Source Code: </b>'' + ISNULL(REPLACE(LeadSource.LeadSourceCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM LeadSource    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 LeadSource.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND LeadSource.CustomerID = @CustomerID
AND LeadSource.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''LeadSource'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (46, N'CHARTER', N'Salesperson', N'SalesPerson', N'Views/Settings/People/SalesPersonCatalog.aspx?Screen=Salesperson', 0, N'Salesperson', N'
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

SalesPersonID AS PrimaryKeyValue,
''<b>Code: </b>'' + ISNULL(REPLACE(SalesPerson.SalesPersonCD,'''''''',''"''),'''') + ''<br />''  
+  ''<b>First Name: </b>'' + ISNULL(REPLACE(SalesPerson.FirstName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Middle Name: </b>'' + ISNULL(REPLACE(SalesPerson.MiddleName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Last Name: </b>'' + ISNULL(REPLACE(SalesPerson.LastName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM SalesPerson    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId            
    LEFT JOIN Company ON Company.HomebaseID = SalesPerson.HomebaseID AND Company.CustomerID = SalesPerson.CustomerID
    LEFT JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID AND Airport.CustomerID = Company.CustomerID
WHERE     (    
SalesPerson.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''     
OR SalesPerson.FirstName like ''%'' + ''@SearchStr'' +''%''    
OR SalesPerson.MiddleName like ''%'' + ''@SearchStr'' +''%'' 
OR SalesPerson.LastName like ''%'' + ''@SearchStr'' +''%'' 
OR Airport.IcaoID like ''%'' + ''@SearchStr'' +''%'' 
) 

AND SalesPerson.CustomerID = @CustomerID
AND SalesPerson.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''SalesPerson'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (47, N'CHARTER', N'CQLostBusiness', N'LostBusiness', N'Views/Settings/Fleet/LostBusinessCatalog.aspx?Screen=LostBusiness', 0, N'Lost Business', N'
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CQLostBusinessID AS PrimaryKeyValue,
''<b>Lost Business Code: </b>'' + ISNULL(REPLACE(CQLostBusiness.CQLostBusinessCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM CQLostBusiness    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 CQLostBusiness.CQLostBusinessCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND CQLostBusiness.CustomerID = @CustomerID
AND CQLostBusiness.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''LostBusiness'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (48, N'Charter', N'CQFile', N'CQFile', N'views/Transactions/CharterQuote/CharterQuoteRequest.aspx?seltab=File', 0, N'Trips', N'
SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQ.CQFileID AS PrimaryKeyValue,
''<b>File No.: </b>'' + ISNULL(REPLACE(CQ.FileNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(A.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Customer: </b>'' + ISNULL(REPLACE(CQCust.CQCustomerCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Contact: </b>'' + ISNULL(REPLACE(CQCustCon.CQCustomerContactCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Salesperson: </b>'' + ISNULL(REPLACE(SP.SalesPersonCD,'''''''',''"''),'''') + ''<br />'' +''<b>Lead Source: </b>'' + ISNULL(REPLACE(LS.LeadSourceCD,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM CQFile CQ 
INNER JOIN Company C on CQ.HomebaseID = C.HomebaseID AND CQ.CustomerID = C.CustomerID
INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID AND A.CustomerID = C.CustomerID
LEFT JOIN CQCustomer CQCust on CQ.CQCustomerID = CQCust.CQCustomerID AND CQ.CustomerID = CQCust.CustomerID
LEFT JOIN CQCustomerContact CQCustCon on CQ.CQCustomerContactID = CQCustCon.CQCustomerContactID AND CQ.CustomerID = CQCustCon.CustomerID
LEFT JOIN SalesPerson SP on CQ.SalesPersonID = SP.SalesPersonID AND CQ.CustomerID = SP.CustomerID
LEFT JOIN LeadSource LS on CQ.LeadSourceID = LS.LeadSourceID AND CQ.CustomerID = LS.CustomerID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(CQ.FileNUM like ''%'' + ''@SearchStr'' +''%'' 
OR A.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR CQCust.CQCustomerCD like ''%'' + ''@SearchStr'' +''%''
OR CQCustCon.CQCustomerContactCD like ''%'' + ''@SearchStr'' +''%''
OR SP.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''
OR LS.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''

--Quote CQMain
OR 1 = (SELECT TOP 1 1 FROM CQMain CQM
LEFT JOIN Vendor V on CQM.VendorID = V.VendorID AND CQM.CustomerID = V.CustomerID
LEFT JOIN Fleet F on CQM.FleetID = F.FleetID AND CQM.CustomerID = F.CustomerID
LEFT JOIN Aircraft A on CQM.AircraftID = A.AircraftID AND CQM.CustomerID = A.CustomerID
LEFT JOIN Passenger P on CQM.PassengerRequestorID = P.PassengerRequestorID AND CQM.CustomerID = P.CustomerID
LEFT JOIN CQLostBusiness LB on CQM.CQLostBusinessID= LB.CQLostBusinessID AND CQM.CustomerID = LB.CustomerID

--LEG
LEFT JOIN CQLEG CQL on CQM.CQMainID = CQL.CQMainID AND CQM.CustomerID = CQL.CustomerID
LEFT JOIN AIRPORT DA on CQL.DAirportID = DA.AirportID AND CQL.CustomerID = DA.CustomerID
LEFT JOIN AIRPORT AA on CQL.AAirportID = AA.AirportID AND CQL.CustomerID = AA.CustomerID

--PAX
LEFT JOIN CQPassenger CQP on CQL.CQLegID = CQP.CQLegID AND CQL.CustomerID = CQP.CustomerID
LEFT JOIN Passenger DBP on CQP.PassengerRequestorID = DBP.PassengerRequestorID AND CQP.CustomerID = DBP.CustomerID

-- Logistics
LEFT JOIN CQCateringList CQC on CQL.CQLegID = CQC.CQLegID AND CQL.CustomerID = CQC.CustomerID
LEFT JOIN Catering DBC on CQC.CateringID = DBC.CateringID AND CQC.CustomerID = DBC.CustomerID
LEFT JOIN CQFBOList CQF on CQL.CQLegID = CQF.CQLegID AND CQL.CustomerID = CQF.CustomerID
LEFT JOIN FBO DBF on CQF.FBOID = DBF.FBOID AND CQF.CustomerID = DBF.CustomerID
LEFT JOIN CQHotelList CQH on CQL.CQLegID = CQH.CQLegID AND CQL.CustomerID = CQH.CustomerID
LEFT JOIN Hotel DBH on CQH.HotelID = DBH.HotelID AND CQH.CustomerID = DBH.CustomerID
LEFT JOIN CQTransportList CQT on CQL.CQLegID= CQT.CQLegID AND CQL.CustomerID = CQT.CustomerID
LEFT JOIN Transport DBT on CQT.TransportID = DBT.TransportID AND CQT.CustomerID = DBT.CustomerID

WHERE 
(
-- Quote
CQM.QuoteNUM like ''%'' + ''@SearchStr'' +''%''  
OR V.VendorCD like ''%'' + ''@SearchStr'' +''%'' 
OR F.TailNum  like ''%'' + ''@SearchStr'' +''%'' 
OR A.AircraftCD like ''%'' + ''@SearchStr'' +''%'' 
OR P.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' 
OR LB.CQLostBusinessCD like ''%'' + ''@SearchStr'' +''%'' 

--LEG
OR DA.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR AA.ICAOID like ''%'' + ''@SearchStr'' +''%''

--PAX
OR DBP.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQP.PassengerName like ''%'' + ''@SearchStr'' +''%'' 
--Logistics
OR DBF.FBOCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQF.CQFBOListDescription like ''%'' + ''@SearchStr'' +''%'' 
OR DBC.CateringCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQC.CQCateringListDescription like ''%'' + ''@SearchStr'' +''%'' 
OR DBH.HotelCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQH.CQHotelListDescription like ''%'' + ''@SearchStr'' +''%''
OR DBT.TransportCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQT.CQTransportListDescription like ''%'' + ''@SearchStr'' +''%''
)
 AND CQM.CQFileID = CQ.CQFileID AND CQM.IsDeleted = 0)
 ) AND isnull(CQ.ExpressQuote,0) =0   and CQ.CustomerID=@CustomerId and CQ.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQFile'') = 1', 1)
GO
INSERT [dbo].[FPSearchParameters] ([SearchId], [ModuleName], [TableName], [PermissionName], [PageVirtualPath], [SysAdminAccessOnly], [UserReadableTableName], [StaticQuery], [IsCustomerIdRequired]) VALUES (49, N'Charter', N'CQFile', N'CQFile', N'views/Transactions/CharterQuote/ExpressQuote.aspx', 0, N'Trips', N'
SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQ.CQFileID AS PrimaryKeyValue,
''<b>Vendor: </b>'' + ISNULL(REPLACE(V.VendorCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(F.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Type Code: </b>'' + ISNULL(REPLACE(A.AircraftCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Salesperson: </b>'' + ISNULL(REPLACE(SP.SalesPersonCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Lead Source: </b>'' + ISNULL(REPLACE(LS.LeadSourceCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Depart ICAO: </b>'' + ISNULL(REPLACE(DA.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Arrival ICAO: </b>'' + ISNULL(REPLACE(AA.ICAOID,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM CQFile CQ 
INNER JOIN CQMain CQM on CQ.CQFileID = CQM.CQFileID AND CQ.CustomerID = CQM.CustomerID
INNER JOIN CQLEG CQL on CQM.CQMainID = CQL.CQMainID AND CQM.CustomerID = CQL.CustomerID
LEFT JOIN SalesPerson SP on CQ.SalesPersonID = SP.SalesPersonID AND CQ.CustomerID = SP.CustomerID
LEFT JOIN LeadSource LS on CQ.LeadSourceID = LS.LeadSourceID AND CQ.CustomerID = LS.CustomerID
LEFT JOIN Vendor V on CQM.VendorID = V.VendorID AND CQM.CustomerID = V.CustomerID
LEFT JOIN Fleet F on CQM.FleetID = F.FleetID AND CQM.CustomerID = F.CustomerID
LEFT JOIN Aircraft A on CQM.AircraftID= A.AircraftID AND CQM.CustomerID = A.CustomerID
LEFT JOIN AIRPORT DA on CQL.DAirportID = DA.AirportID AND CQL.CustomerID = DA.CustomerID
LEFT JOIN AIRPORT AA on CQL.AAirportID = AA.AirportID AND CQL.CustomerID = AA.CustomerID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(CQ.FileNUM like ''%'' + ''@SearchStr'' +''%'' 
OR SP.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''
OR LS.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''
OR V.VendorCD like ''%'' + ''@SearchStr'' +''%'' 
OR F.TailNum  like ''%'' + ''@SearchStr'' +''%'' 
OR A.AircraftCD like ''%'' + ''@SearchStr'' +''%'' 

--LEG
OR DA.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR AA.ICAOID like ''%'' + ''@SearchStr'' +''%''
)
AND CQM.CQFileID = CQ.CQFileID AND CQM.IsDeleted = 0
AND isnull(CQ.ExpressQuote,0) =1   and CQ.CustomerID=@CustomerId and CQ.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQFile'') = 1', 1)
GO