/* UPDATE SCRIPT FOR ROLE PERMISSION FOR UTILITIES */
UPDATE UMPermission SET UMPermissionRole = 'AirportPairs' WHERE UMPermissionID = 10343
UPDATE UMPermission SET UMPermissionRole = 'AirportPairLegs' WHERE UMPermissionID = 10344
UPDATE UMPermission SET UMPermissionRole = 'ItineraryPlan' WHERE UMPermissionID = 10345
UPDATE UMPermission SET UMPermissionRole = 'ItineraryPlanLegs' WHERE UMPermissionID = 10346
UPDATE UMPermission SET UMPermissionRole = 'WolrdWinds' WHERE UMPermissionID = 10348
UPDATE UMPermission SET UMPermissionRole = 'WolrdClock' WHERE UMPermissionID = 10349
UPDATE UMPermission SET UMPermissionRole = 'UVTripPlanner' WHERE UMPermissionID = 10347
