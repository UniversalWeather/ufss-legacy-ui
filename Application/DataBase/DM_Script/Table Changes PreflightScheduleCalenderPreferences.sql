Go
Alter table PreflightScheduleCalenderPreferences add CalenderType varchar(25) null
Go
Alter table PreflightScheduleCalenderPreferences add CrewGroupIDS VARCHAR(MAX) null
Go
Alter table PreflightScheduleCalenderPreferences add IsCrewGroup bit not null default 0
Go
update PreflightScheduleCalenderPreferences set CalenderType = 'View_Monthly' 
Go
Alter table PreflightScheduleCalenderPreferences alter column CalenderType varchar(25) not null
Go