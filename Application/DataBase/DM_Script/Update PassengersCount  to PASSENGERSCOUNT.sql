update [TSFlightLog] set OriginalDescription = 'PASSENGER COUNT' 
where OriginalDescription = 'Passengers' OR OriginalDescription = 'Passenger count' 
OR OriginalDescription = 'PASSENGERS'

update [TSFlightLog] set CustomDescription = 'PASSENGER COUNT' 
where CustomDescription = 'Passengers' OR CustomDescription = 'Passenger count' 
OR CustomDescription = 'PASSENGERS'


update [TSDefinitionLog] set OriginalDescription = 'PASSENGER COUNT' 
where OriginalDescription = 'Passengers' OR OriginalDescription = 'Passenger count' 
OR OriginalDescription = 'PASSENGERS'

update [TSDefinitionLog] set CustomDescription = 'PASSENGER COUNT' 
where CustomDescription = 'Passengers' OR CustomDescription = 'Passenger count' 
OR CustomDescription = 'PASSENGERS'