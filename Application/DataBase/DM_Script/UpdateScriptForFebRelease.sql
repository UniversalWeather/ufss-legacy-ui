

update ExceptionTemplate set ExceptionTemplate = 'Aircraft Type Code or Tail No. is required' where 
ExceptionTemplateID = 1000	 and ModuleID= 10011

GO

UPDATE PostflightCrew SET 
	CrewFirstName = C.FirstName,
	CrewMiddleName = C.MiddleInitial,
	CrewLastName = C.LastName
FROM CREW C
WHERE C.CrewID = PostflightCrew.CrewID
 AND PostflightCrew.CrewFirstName IS NULL
GO