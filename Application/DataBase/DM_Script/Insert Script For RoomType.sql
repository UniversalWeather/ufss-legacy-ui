IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000012)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000012, N'Select', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Select', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000012
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000013)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000013, N'Single', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Single', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000013
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000014)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000014, N'Double', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Double', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000014
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000015)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000015, N'Triple', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Triple', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000015
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000016)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000016, N'Family', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Family', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000016
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000017)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000017, N'Twin', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Twin', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000017
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000018)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000018, N'Queen', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'Queen', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000018
GO
IF NOT EXISTS (SELECT * FROM [dbo].[RoomType] WHERE [RoomTypeID] = 1000019)
	INSERT [dbo].[RoomType] ([RoomTypeID], [RoomDescription], [LastUpdUID], [LastUpdTS], [IsDeleted], [IsInActive]) VALUES (1000019, N'King', N'UWA Admin', GETDATE(), 0, NULL)
ELSE
	UPDATE [dbo].[RoomType] SET [RoomDescription] = 'King', [LastUpdUID] = 'UWA Admin', [LastUpdTS] = GETDATE(), [IsDeleted] = 0, [IsInActive] = NULL WHERE [RoomTypeID] = 1000019
GO