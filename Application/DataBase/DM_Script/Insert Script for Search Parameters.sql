DELETE FROM [FPSearchParameters]
-- Dummy
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired]) 
VALUES (0,'','','','',0,'','SELECT CAST(0 AS bigint) AS SearchId,
'''' AS ModuleName,
'''' AS TableName, 
'''' AS PageVirtualPath, 
0 AS PrimaryKeyValue,
'''' AS ColumnValue
FROM FPSearchParameters WHERE 1=2',1)
-- Airport
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired])
VALUES (1, 'LOGISTICS', 'Airport', 'Airport', 'Views/Settings/Airports/AirportCatalog.aspx?Screen=Airport&IsWidget=0',
0, 'Airport Catalog', 'DECLARE @UWACustomerID BIGINT    
SELECT @UWACustomerID  = dbo.GetUWACustomerID()     
    
; WITH TempAirport AS (Select A.AirportID,
A.IcaoID, 
A.AirportName,
A.AirportInfo,
A.Alerts,
A.GeneralNotes,
A.AirportManagerPhoneNum,
A.HoursOfOperation,
A.AsosPhoneNum,
A.CustomPhoneNum,
A.ImmigrationPhoneNum,
A.AgPhoneNum,
A.AwosPhoneNum,
A.AtisPhoneNum,
A.FssPhone,
A.CityName,
A.StateName,
A.FssName,
A.AirportManager,
A.CountryName,
A.CustomLocation,
A.CustomNotes,
A.CustomerID,
A.CountryID,
A.DSTRegionID,
A.MetroID,
A.ExchangeRateID
  from Airport A     
where A.CustomerID =@CustomerID and A.IsDeleted =0 and A.IsInActive <= 1
union all    
Select 
B.AirportID,
B.IcaoID, 
B.AirportName,
B.AirportInfo,
B.Alerts,
B.GeneralNotes,
B.AirportManagerPhoneNum,
B.HoursOfOperation,
B.AsosPhoneNum,
B.CustomPhoneNum,
B.ImmigrationPhoneNum,
B.AgPhoneNum,
B.AwosPhoneNum,
B.AtisPhoneNum,
B.FssPhone,
B.CityName,
B.StateName,
B.FssName,
B.AirportManager,
B.CountryName,
B.CustomLocation,
B.CustomNotes,
B.CustomerID,
B.CountryID,
B.DSTRegionID,
B.MetroID,
B.ExchangeRateID
  from Airport B     
where B.CustomerID <> @CustomerID and B.CustomerID = @UWACustomerID  and B.IsDeleted =0  and B.IsInActive <= 1 and IcaoID Not in (    
Select distinct IcaoID  from Airport where CustomerID = @CustomerID and IsDeleted = 0 ))   
  SELECT FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
TempAirport.AirportId AS PrimaryKeyValue,
''<b>ICOID : </b>'' + ISNULL(TempAirport.IcaoID,'''') + ''<br />'' +
''<b>Airport Name : </b>'' + ISNULL(TempAirport.AirportName,'''') + ''<br />'' +
''<b>City Name : </b>'' + ISNULL(TempAirport.CityName,'''') + ''<br />'' +
''<b>Country Name: </b>'' + ISNULL(Country.CountryName,'''') + ''<br />'' +
''<b>DST Region: </b>'' + ISNULL(DSTRegion.DSTRegionName,'''') + ''<br />'' +
''<b>Metro: </b>'' + ISNULL(Metro.MetroName,'''') + ''<br />'' +
''<b>Exchange Rate Code: </b>'' + ISNULL(ExchangeRate.ExchRateCD,'''') + ''<br />'' AS ColumnValue
FROM TempAirport
LEFT JOIN Country ON Country.CountryId = TempAirport.CountryID
LEFT JOIN DSTRegion ON DSTRegion.DSTRegionID = TempAirport.DSTRegionID
LEFT JOIN Metro ON Metro.MetroID = TempAirport.MetroID
LEFT JOIN ExchangeRate ON ExchangeRate.ExchangeRateID = TempAirport.ExchangeRateID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
WHERE (
TempAirport.AirportInfo like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.IcaoID like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.Alerts like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.GeneralNotes like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AirportManagerPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.HoursOfOperation like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AsosPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.CustomPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.ImmigrationPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AgPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AwosPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AtisPhoneNum like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.FssPhone like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AirportName like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.CityName like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.StateName like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.FssName like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.AirportManager like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.CountryName like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.CustomLocation like ''%'' + ''@SearchStr'' +''%''
OR TempAirport.CustomNotes like ''%'' + ''@SearchStr'' +''%''
OR Country.CountryCD like ''%'' + ''@SearchStr'' +''%''
OR Country.CountryName like ''%'' + ''@SearchStr'' +''%''
OR DSTRegion.DSTRegionCD like ''%'' + ''@SearchStr'' +''%''
OR DSTRegion.DSTRegionName like ''%'' + ''@SearchStr'' +''%''
OR Metro.MetroCD like ''%'' + ''@SearchStr'' +''%''
OR Metro.MetroName like ''%'' + ''@SearchStr'' +''%''
OR ExchangeRate.ExchRateCD like ''%'' + ''@SearchStr'' +''%''
OR ExchangeRate.ExchRateDescription like ''%'' + ''@SearchStr'' +''%''
)
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Airport'') = 1
',1)
-- Country
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (2, 'LOGISTICS', 'Country', 'Country', 'views/Settings/Company/CountryCatalog.aspx?Screen=Country',
0, 'Country Catalog', 'SELECT FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
Country.CountryId AS PrimaryKeyValue,
''<b>Country Code: </b>'' + ISNULL(REPLACE(Country.CountryCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Description: </b>'' + ISNULL(REPLACE(Country.CountryName,'''''''',''"''),'''') + ''<br />'' +
''<b>ISO Code: </b>'' + ISNULL(REPLACE(Country.ISOCD,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue

FROM [Country]  
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
WHERE  
(
CountryCD like ''%'' + ''@SearchStr'' +''%'' 
OR CountryName like ''%'' + ''@SearchStr'' +''%''
OR ISOCD like ''%'' + ''@SearchStr'' +''%''
) 
AND IsDeleted = 0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Country'') = 1
',0)
-- Accounts
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (3, 'COMPANY', 'Account', 'Accounts', 'views/Settings/Company/AccountsCatalog.aspx?Screen=Accounts',
0, 'Accounts Catalog','
SELECT FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
Account.AccountID AS PrimaryKeyValue,
''<b>Account No.: </b>'' + ISNULL(REPLACE(Account.AccountNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Description: </b>'' + ISNULL(REPLACE(Account.AccountDescription,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue

  FROM Account       
  LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
  LEFT OUTER JOIN Company ON Company.HomebaseID = Account.HomebaseID        
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID        
  WHERE 
  (
  AccountNum like ''%'' + ''@SearchStr'' +''%'' 
  OR AccountDescription like ''%'' + ''@SearchStr'' +''%''
  ) AND Account.CustomerID = @CustomerID   AND Account.IsDeleted = 0
  AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Accounts'') = 1
  ',1)
-- Preflight
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (4, 'TRIPS', 'PreflightMain', 'PreFlightMain', 'views/Transactions/PreFlight/PreflightMain.aspx?seltab=PreFlight',
0, 'Trips','SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
PM.TripId AS PrimaryKeyValue,
''<b>Trip No.: </b>'' + ISNULL(REPLACE(PM.TripNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(A.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(F.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Flight No.: </b>'' + ISNULL(REPLACE(PM.FlightNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Trip purpose: </b>'' + ISNULL(REPLACE(PM.TripDescription,'''''''',''"''),'''') + ''<br />'' +
''<b>Dispatch #: </b>'' + ISNULL(REPLACE(PM.DispatchNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Requestor: </b>'' + ISNULL(REPLACE(P.PassengerRequestorCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Dispatcher: </b>'' + ISNULL(REPLACE(PM.DispatcherUserName,'''''''',''"''),'''') + ''<br />'' +
''<b>Account #: </b>'' + ISNULL(REPLACE(Acc.AccountNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Depatment: </b>'' + ISNULL(REPLACE(Depart.DepartmentCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Authorization: </b>'' + ISNULL(REPLACE(Auth.AuthorizationCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Client Code: </b>'' + ISNULL(REPLACE(Cli.ClientCD,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM PreflightMain PM  
INNER JOIN Company C on PM.HomebaseID = C.HomebaseID  
INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID   
LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID  AND Isnull(POM.IsDeleted,0) = 0 
LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  
LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID 
LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID
LEFT JOIN Account Acc ON Acc.AccountId = PM.AccountID
LEFT JOIN Department Depart ON Depart.DepartmentID = PM.DepartmentId
LEFT JOIN DepartmentAuthorization Auth ON Auth.AuthorizationID = PM.AuthorizationID
LEFT JOIN Client Cli ON Cli.ClientID = PM.ClientID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(PM.TripNUM like ''%'' + ''@SearchStr'' +''%'' 
OR A.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR F.TailNum like ''%'' + ''@SearchStr'' +''%''
OR PM.FlightNUM like ''%'' + ''@SearchStr'' +''%''
OR PM.TripDescription like ''%'' + ''@SearchStr'' +''%''
OR PM.DispatchNUM like ''%'' + ''@SearchStr'' +''%''
OR P.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%''
OR PM.DispatcherUserName like ''%'' + ''@SearchStr'' +''%''
OR Acc.AccountNum like ''%'' + ''@SearchStr'' +''%''
OR Depart.DepartmentCD like ''%'' + ''@SearchStr'' +''%''
OR Auth.AuthorizationCD like ''%'' + ''@SearchStr'' +''%''
OR Cli.ClientCD like ''%'' + ''@SearchStr'' +''%''

-- LEG
OR 1 = (SELECT TOP 1 1 FROM PreflightLeg 
LEFT JOIN Airport A ON A.AirportID = PreflightLeg.DepartICAOID
LEFT JOIN Airport B ON B.AirportID = PreflightLeg.ArriveICAOID
WHERE 
(A.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 B.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightLeg.FlightNUM like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightLeg.TripID = PM.TripID AND PreflightLeg.IsDeleted = 0)

-- Crew

OR 1 = (SELECT TOP 1 1 FROM PreflightCrewList 
INNER JOIN Crew A ON A.CrewID = PreflightCrewList.CrewID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightCrewList.LegID AND PreflightLeg.TripID = PM.TripID
WHERE 
(A.CrewCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCrewList.CrewLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightCrewList.IsDeleted = 0)

-- PAX

OR 1 = (SELECT TOP 1 1 FROM PreflightPassengerList 
LEFT JOIN Passenger A ON A.PassengerRequestorID = PreflightPassengerList.PassengerID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightPassengerList.LegID AND PreflightLeg.TripID = PM.TripID
WHERE 
(A.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightPassengerList.PassengerLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightPassengerList.IsDeleted = 0)

-- Logistics

OR 1 = (SELECT TOP 1 1 FROM PreflightFBOList 
LEFT JOIN FBO A ON A.FBOID = PreflightFBOList.FBOID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightFBOList.LegID AND PreflightLeg.TripID = PM.TripID
WHERE 
(A.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.FBOVendor like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightFBOList.IsDeleted = 0)

-- Catering

OR 1 = (SELECT TOP 1 1 FROM PreflightCateringDetail 
LEFT JOIN catering A ON A.CateringID = PreflightCateringDetail.CateringID
INNER JOIN PreflightLeg ON PreflightLeg.LegID = PreflightCateringDetail.LegID AND PreflightLeg.TripID = PM.TripID
WHERE 
(A.CateringCD like ''%'' + ''@SearchStr'' +''%'' OR
 PreflightCateringDetail.ContactName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PreflightCateringDetail.IsDeleted = 0)
)  
AND
PM.RecordType= ''T'' and PM.CustomerID=@CustomerId and PM.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PreFlightMain'') = 1',1)
-- Postflight
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (5, 'TRIPLOGS', 'PostflightMain', 'PostflightMain', 'views/Transactions/PostFlight/PostFlightMain.aspx',
0, 'Trip Logs','SELECT   
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
POMain.POLogID AS PrimaryKeyValue,
''<b>Log No: </b>'' + ISNULL(REPLACE(POMain.LogNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(airport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(fleet.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Client Code: </b>'' + ISNULL(REPLACE(client.ClientCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Flight No.: </b>'' + ISNULL(REPLACE(POMain.FlightNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Dispatch No.: </b>'' + ISNULL(REPLACE(POMain.DispatchNum,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue      
FROM PostflightMain POMain  
LEFT OUTER JOIN PreflightMain preflightMain ON preflightMain.TripID = POMain.TripID  
LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = POMain.FleetID  
LEFT OUTER JOIN Company company on POMain.HomeBaseID = company.HomeBaseID  
LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID  
LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = POMain.AuthorizationID  
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
LEFT JOIN Client client ON Client.ClientID = POMain.ClientID
WHERE  
(
POMain.LogNum like ''%'' + ''@SearchStr'' +''%'' OR
airport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
fleet.TailNum like ''%'' + ''@SearchStr'' +''%'' OR
client.ClientCD like ''%'' + ''@SearchStr'' +''%'' OR
POMain.FlightNum like ''%'' + ''@SearchStr'' +''%'' OR
POMain.DispatchNum like ''%'' + ''@SearchStr'' +''%'' 

-- LEG
OR 1 = (SELECT TOP 1 1 FROM PostflightLeg 
LEFT JOIN Airport A ON A.AirportID = PostflightLeg.DepartICAOID
LEFT JOIN Airport B ON B.AirportID = PostflightLeg.ArriveICAOID
WHERE 
(A.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
B.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
PostflightLeg.FlightNum like ''%'' + ''@SearchStr'' +''%'' OR
PostflightLeg.FlightPurpose like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PostflightLeg.POLogID = POMain.POLogID AND PostflightLeg.IsDeleted = 0)

-- Crew

OR 1 = (SELECT TOP 1 1 FROM PostflightCrew 
INNER JOIN Crew A ON A.CrewID = PostflightCrew.CrewID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightCrew.POLegID AND PostflightLeg.POLogID = POMain.POLogID
WHERE 
(A.CrewCD like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightCrew.CrewLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PostflightCrew.IsDeleted = 0)

-- PAX

OR 1 = (SELECT TOP 1 1 FROM PostflightPassenger 
LEFT JOIN Passenger A ON A.PassengerRequestorID = PostflightPassenger.PassengerID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightPassenger.POLegID AND PostflightLeg.POLogID = POMain.POLogID
WHERE 
(A.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerFirstName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerMiddleName like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightPassenger.PassengerLastName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PostflightPassenger.IsDeleted = 0)

-- EXPENSE

OR 1 = (SELECT TOP 1 1 FROM PostflightExpense 
LEFT JOIN FBO A ON A.FBOID = PostflightExpense.FBOID
INNER JOIN PostflightLeg ON PostflightLeg.POLegID = PostflightExpense.POLegID AND PostflightLeg.POLogID = POMain.POLogID
WHERE 
(A.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.FBOVendor like ''%'' + ''@SearchStr'' +''%'' OR
 PostflightExpense.SlipNUM like ''%'' + ''@SearchStr'' +''%'' 
) AND 
PostflightExpense.IsDeleted = 0)

) AND
POMain.CustomerID = @CustomerID  
AND isnull(POMain.IsDeleted,0) = 0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''POLogManager'') = 1',1)
-- Budget
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (6, 'COMPANY', 'Budget', 'Budget', 'Views/Settings/Company/BudgetCatalog.aspx?Screen=Budget',
0, 'Budget','SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

Budget.BudgetID AS PrimaryKeyValue,
''<b>Account No.: </b>'' + ISNULL(REPLACE(Budget.AccountNum,'''''''',''"''),'''') + ''<br />''  
+  ''<b>Year: </b>'' + ISNULL(REPLACE(Budget.FiscalYear,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Tail No.: </b>'' + ISNULL(REPLACE(Fleet.TailNum,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Budget    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Fleet ON Fleet.FleetID = Budget.FleetID

WHERE     (    
AccountNum like ''%'' + ''@SearchStr'' +''%'' 
OR FiscalYear like ''%'' + ''@SearchStr'' +''%'' 
OR TailNum like ''%'' + ''@SearchStr'' +''%''
)
AND Budget.CustomerID = @CustomerID   
AND Budget.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Budget'') = 1    ',1)
--Company
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (7, 'COMPANY', 'Company', 'CompanyProfile', 'Views/Settings/Company/CompanyProfileCatalog.aspx?Screen=CompanyProfile',
0, 'CompanyProfile','SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

Company.HomebaseID AS PrimaryKeyValue,
''<b>Home Base: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''
+  ''<b>Base Description: </b>'' + ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Company Name: </b>'' + ISNULL(REPLACE(Company.CompanyName,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Company    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID

WHERE     (    
IcaoID like ''%'' + ''@SearchStr'' +''%''     
OR BaseDescription like ''%'' + ''@SearchStr'' +''%''    
OR CompanyName like ''%'' + ''@SearchStr'' +''%''    
) 

AND Company.CustomerID = @CustomerID   
AND Company.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CompanyProfile'') = 1 ',1)
-- Custom Address
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (8, 'COMPANY', 'Company', 'CustomAddress', 'Views/Settings/People/CustomerAddressCatalog.aspx?Screen=CustomAddress',
0, 'CustomAddress','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CustomAddress.CustomAddressID AS PrimaryKeyValue,
''<b>Custom Address Code: </b>'' + ISNULL(REPLACE(CustomAddress.CustomAddressCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Name: </b>'' + ISNULL(REPLACE(CustomAddress.Name,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Closest ICAO: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Metro: </b>'' + ISNULL(REPLACE(CustomAddress.MetropolitanArea,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Country: </b>'' + ISNULL(REPLACE(Country.CountryName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Contact: </b>'' + ISNULL(REPLACE(CustomAddress.Contact,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM CustomAddress    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Company ON Company.HomebaseID = CustomAddress.HomebaseID
LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
LEFT OUTER JOIN Country ON Country.CountryID = CustomAddress.CountryID

WHERE     (    
CustomAddressCD like ''%'' + ''@SearchStr'' +''%''     
OR Name like ''%'' + ''@SearchStr'' +''%''    
OR BaseDescription like ''%'' + ''@SearchStr'' +''%''    
OR IcaoID like ''%'' + ''@SearchStr'' +''%''    
OR MetropolitanArea like ''%'' + ''@SearchStr'' +''%''    
OR Country.CountryName like ''%'' + ''@SearchStr'' +''%''    
OR Contact like ''%'' + ''@SearchStr'' +''%''    
) 

AND CustomAddress.CustomerID = @CustomerID   
AND CustomAddress.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CustomerAddress'') = 1  ',1)
--Department Group
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (9, 'COMPANY', 'DepartmentGroup', 'DepartmentGroup', 'Views/Settings/Company/DepartmentGroup.aspx?Screen=DeptGroup',
0, 'DepartmentGroup','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

DepartmentGroup.DepartmentGroupID AS PrimaryKeyValue,
''<b>Group Code: </b>'' + ISNULL(REPLACE(DepartmentGroup.DepartmentGroupCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Description: </b>'' + ISNULL(REPLACE(DepartmentGroup.DepartmentGroupDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM DepartmentGroup    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Company ON Company.HomebaseID = DepartmentGroup.HomebaseID

WHERE     (    
DepartmentGroupCD like ''%'' + ''@SearchStr'' +''%''     
OR BaseDescription like ''%'' + ''@SearchStr'' +''%''    
OR DepartmentGroupDescription like ''%'' + ''@SearchStr'' +''%''    
) 

AND DepartmentGroup.CustomerID = @CustomerID   
AND DepartmentGroup.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''DepartmentGroup'') = 1   ',1)
--Department Authorization
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (10, 'COMPANY', 'Department', 'DepartmentAuthorization', 'Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Screen=DeptAuthorization',
0, 'DepartmentAuthorization','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

Department.DepartmentID AS PrimaryKeyValue,
''<b>Department Code: </b>'' + ISNULL(REPLACE(Department.DepartmentCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(Department.DepartmentName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Client Code: </b>'' + ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + ''<br />''   
AS ColumnValue

FROM Department    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Client ON Client.ClientID = Department.ClientID

WHERE     (    
DepartmentCD like ''%'' + ''@SearchStr'' +''%''     
OR DepartmentName like ''%'' + ''@SearchStr'' +''%''    
OR ClientCD like ''%'' + ''@SearchStr'' +''%''    
) 
AND Department.CustomerID = @CustomerID   
AND Department.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''DepartmentAuthorization'') = 1',1)
--Payable Vendor
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (11, 'COMPANY', 'Vendor', 'PayableVendor', 'Views/Settings/Logistics/PayableVendor.aspx?Screen=PayableVendor',
0, 'PayableVendor','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

Vendor.VendorID AS PrimaryKeyValue,
''<b>Payable Vendor Code: </b>'' + ISNULL(REPLACE(Vendor.VendorCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Name: </b>'' + ISNULL(REPLACE(Vendor.Name,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Closest ICAO: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Code: </b>'' + ISNULL(REPLACE(VendorContact.VendorContactCD,'''''''',''"''),'''') + ''<br />''   
+  ''<b>First Name: </b>'' + ISNULL(REPLACE(VendorContact.FirstName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Middle Name: </b>'' + ISNULL(REPLACE(VendorContact.MiddleName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Last Name: </b>'' + ISNULL(REPLACE(VendorContact.LastName,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Vendor    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Company ON Company.HomebaseID = Vendor.HomebaseID
LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
LEFT OUTER JOIN VendorContact ON VendorContact.VendorID = Vendor.VendorID

WHERE     (    
Vendor.VendorCD like ''%'' + ''@SearchStr'' +''%''     
OR Name like ''%'' + ''@SearchStr'' +''%''    
OR BaseDescription like ''%'' + ''@SearchStr'' +''%''  
OR IcaoID like ''%'' + ''@SearchStr'' +''%''  
OR VendorContactCD like ''%'' + ''@SearchStr'' +''%''  
OR FirstName like ''%'' + ''@SearchStr'' +''%''  
OR MiddleName like ''%'' + ''@SearchStr'' +''%''  
OR LastName like ''%'' + ''@SearchStr'' +''%''  
) 
AND Vendor.CustomerID = @CustomerID   
AND Vendor.IsDeleted = 0    
AND Vendor.VendorType = ''P''
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PayableVendor'') = 1 ',1)
-- Payment Type
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (12, 'COMPANY', 'PaymentType', 'PaymentType', 'Views/Settings/Logistics/PayableVendor.aspx?Screen=PayableVendor',
0, 'PaymentType','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

PaymentTypeID AS PrimaryKeyValue,
''<b>Payment Code: </b>'' + ISNULL(REPLACE(PaymentType.PaymentTypeCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(PaymentType.PaymentTypeDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM PaymentType    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
PaymentTypeCD like ''%'' + ''@SearchStr'' +''%''     
OR PaymentTypeDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND PaymentType.CustomerID = @CustomerID   
AND PaymentType.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PaymentType'') = 1  ',1)
-- Payment Type
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (13, 'COMPANY', 'PaymentType', 'PaymentType', 'Views/Settings/Company/PaymentType.aspx?Screen=PaymentTypes',
0, 'PaymentType','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

PaymentTypeID AS PrimaryKeyValue,
''<b>Payment Code: </b>'' + ISNULL(REPLACE(PaymentType.PaymentTypeCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(PaymentType.PaymentTypeDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM PaymentType    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
PaymentTypeCD like ''%'' + ''@SearchStr'' +''%''     
OR PaymentTypeDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND PaymentType.CustomerID = @CustomerID   
AND PaymentType.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PaymentType'') = 1  ',1)
--Vendor
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (14, 'COMPANY', 'Vendor', 'Vendor', 'Views/Settings/Logistics/VendorCatalog.aspx?Screen=Vendor',
0, 'Vendor','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

Vendor.VendorID AS PrimaryKeyValue,
''<b>Vendor Code: </b>'' + ISNULL(REPLACE(Vendor.VendorCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Vendor Name: </b>'' + ISNULL(REPLACE(Vendor.Name,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Closest ICAO: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Code: </b>'' + ISNULL(REPLACE(VendorContact.VendorContactCD,'''''''',''"''),'''') + ''<br />''   
+  ''<b>First Name: </b>'' + ISNULL(REPLACE(VendorContact.FirstName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Middle Name: </b>'' + ISNULL(REPLACE(VendorContact.MiddleName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Last Name: </b>'' + ISNULL(REPLACE(VendorContact.LastName,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Vendor    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Company ON Company.HomebaseID = Vendor.HomebaseID
LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
LEFT OUTER JOIN VendorContact ON VendorContact.VendorID = Vendor.VendorID

WHERE     (    
Vendor.VendorCD like ''%'' + ''@SearchStr'' +''%''     
OR Name like ''%'' + ''@SearchStr'' +''%''    
OR BaseDescription like ''%'' + ''@SearchStr'' +''%''  
OR IcaoID like ''%'' + ''@SearchStr'' +''%''  
OR VendorContactCD like ''%'' + ''@SearchStr'' +''%''  
OR FirstName like ''%'' + ''@SearchStr'' +''%''  
OR MiddleName like ''%'' + ''@SearchStr'' +''%''  
OR LastName like ''%'' + ''@SearchStr'' +''%''  
)
AND Vendor.CustomerID = @CustomerID   
AND Vendor.IsDeleted = 0    
AND Vendor.VendorType = ''V''
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Vendor'') = 1  ',1)
-- Aircraft Duty Type
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (15, 'FLEET', 'AircraftDuty', 'AircraftDutyType', 'Views/Settings/Fleet/AircraftDutyType.aspx?Screen=AircraftDutyType',
0, 'AircraftDutyType','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

AircraftDutyID AS PrimaryKeyValue,
''<b>Aircraft Duty Code: </b>'' + ISNULL(REPLACE(AircraftDuty.AircraftDutyCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(AircraftDuty.AircraftDutyDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM AircraftDuty    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
AircraftDutyCD like ''%'' + ''@SearchStr'' +''%''     
OR AircraftDutyDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND AircraftDuty.CustomerID = @CustomerID   
AND AircraftDuty.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''AircraftDutyType'') = 1   ',1)
--Aircraft Type
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (16, 'FLEET', 'Aircraft', 'AircraftType', 'Views/Settings/Fleet/AircraftType.aspx?Screen=AircraftType',
0, 'AircraftType','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

AircraftID AS PrimaryKeyValue,
''<b>Aircraft Type Code: </b>'' + ISNULL(REPLACE(Aircraft.AircraftCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(Aircraft.AircraftDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Aircraft    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
AircraftCD like ''%'' + ''@SearchStr'' +''%''     
OR AircraftDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND Aircraft.CustomerID = @CustomerID   
AND Aircraft.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''AircraftType'') = 1 
',1)
--Delay Type
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (17, 'FLEET', 'DelayType', 'DelayType', 'Views/Settings/Fleet/DelayTypeCatalog.aspx?Screen=DelayType',
0, 'DelayType','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

DelayTypeID AS PrimaryKeyValue,
''<b>Delay Type Code: </b>'' + ISNULL(REPLACE(DelayType.DelayTypeCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(DelayType.DelayTypeDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM DelayType    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
DelayTypeCD like ''%'' + ''@SearchStr'' +''%''     
OR DelayTypeDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND DelayType.CustomerID = @CustomerID   
AND DelayType.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''DelayType'') = 1
',1)
--Fleet Group
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (18, 'FLEET', 'FleetGroup', 'FleetGroup', 'Views/Settings/Fleet/FleetGroupCatalog.aspx?Screen=FleetGroup',
0, 'FleetGroup','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FleetGroupID AS PrimaryKeyValue,
''<b>Fleet Group Code: </b>'' + ISNULL(REPLACE(FleetGroup.FleetGroupCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(FleetGroup.FleetGroupDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM FleetGroup    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
FleetGroupCD like ''%'' + ''@SearchStr'' +''%''     
OR FleetGroupDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND FleetGroup.CustomerID = @CustomerID   
AND FleetGroup.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FleetGroup'') = 1
',1)
--Flight Categories
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (19, 'FLEET', 'FlightCatagory', 'FlightCategory', 'Views/Settings/Fleet/FlightCategory.aspx?Screen=FlightCategories',
0, 'FlightCategory','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FlightCategoryID AS PrimaryKeyValue,
''<b>Department Code: </b>'' + ISNULL(REPLACE(FlightCatagory.FlightCatagoryCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(FlightCatagory.FlightCatagoryDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Client Code: </b>'' + ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + ''<br />''   
AS ColumnValue

FROM FlightCatagory    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Client ON Client.ClientID = FlightCatagory.ClientID

WHERE     (    
FlightCatagoryCD like ''%'' + ''@SearchStr'' +''%''     
OR FlightCatagoryDescription like ''%'' + ''@SearchStr'' +''%''    
OR ClientCD like ''%'' + ''@SearchStr'' +''%''    
) 
AND FlightCatagory.CustomerID = @CustomerID   
AND FlightCatagory.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FlightCategory'') = 1 
',1)
--DST Region
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (20, 'LOGISTICS', 'DSTRegion', 'DaylightSavingTimeRegion', 'Views/Settings/Airports/DaylightSavingTimeRegion.aspx?Screen=DSTRegion',
0, 'DaylightSavingTimeRegion','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

DSTRegionID AS PrimaryKeyValue,
''<b>DST Region Code: </b>'' + ISNULL(REPLACE(DSTRegion.DSTRegionCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(DSTRegion.DSTRegionName,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM DSTRegion    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
DSTRegionCD like ''%'' + ''@SearchStr'' +''%''     
OR DSTRegionName like ''%'' + ''@SearchStr'' +''%''    ) 

AND DSTRegion.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''DaylightSavingTimeRegion'') = 1
',1)
--Exchange Rate
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (21, 'LOGISTICS', 'ExchangeRate', 'ExchangeRates', 'Views/Settings/Company/ExchangeRatesCatalog.aspx?Screen=ExchangeRate',
0, 'ExchangeRates','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

ExchangeRateID AS PrimaryKeyValue,
''<b>Rate Code: </b>'' + ISNULL(REPLACE(ExchangeRate.ExchRateCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(ExchangeRate.ExchRateDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM ExchangeRate    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
ExchRateCD like ''%'' + ''@SearchStr'' +''%''     
OR ExchRateDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND ExchangeRate.CustomerID = @CustomerID
AND ExchangeRate.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''ExchangeRates'') = 1
',1)
--Fuel Locator
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (22, 'LOGISTICS', 'FuelLocator', 'FuelLocator', 'Views/Settings/Logistics/FuelLocator.aspx?Screen=FuelLocator',
0, 'FuelLocator','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FuelLocatorID AS PrimaryKeyValue,
''<b>Locator Code: </b>'' + ISNULL(REPLACE(FuelLocator.FuelLocatorCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(FuelLocator.FuelLocatorDescription,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Client Code: </b>'' + ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + ''<br />''   
AS ColumnValue

FROM FuelLocator    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
LEFT OUTER JOIN Client ON Client.ClientID = FuelLocator.ClientID

WHERE     (    
FuelLocatorCD like ''%'' + ''@SearchStr'' +''%''     
OR FuelLocatorDescription like ''%'' + ''@SearchStr'' +''%''    
OR ClientCD like ''%'' + ''@SearchStr'' +''%''    
) 
AND FuelLocator.CustomerID = @CustomerID   
AND FuelLocator.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FuelLocator'') = 1 
',1)
--Fuel Vendor
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (23, 'LOGISTICS', 'FuelVendor', 'FuelVendor', 'Views/Settings/Logistics/FuelVendor.aspx?Screen=FuelVendor',
0, 'FuelVendor','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FuelVendorID AS PrimaryKeyValue,
''<b>Fuel Vendor Code: </b>'' + ISNULL(REPLACE(FuelVendor.VendorCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Name: </b>'' + ISNULL(REPLACE(FuelVendor.VendorName,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Description: </b>'' + ISNULL(REPLACE(FuelVendor.VendorDescription,'''''''',''"''),'''') + ''<br />''   
AS ColumnValue

FROM FuelVendor    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
VendorCD like ''%'' + ''@SearchStr'' +''%''     
OR VendorName like ''%'' + ''@SearchStr'' +''%''    
OR VendorDescription like ''%'' + ''@SearchStr'' +''%''    
) 
AND FuelVendor.CustomerID = @CustomerID   
AND FuelVendor.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FuelVendor'') = 1 
',1)
--Metro City
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (24, 'LOGISTICS', 'Metro', 'Metro', 'Views/Settings/Airports/MetroCity.aspx?Screen=Metro',
0, 'Metro','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

MetroID AS PrimaryKeyValue,
''<b>Metro Code: </b>'' + ISNULL(REPLACE(Metro.MetroCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(Metro.MetroName,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Metro    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
MetroCD like ''%'' + ''@SearchStr'' +''%''     
OR MetroName like ''%'' + ''@SearchStr'' +''%''    ) 

AND Metro.CustomerID = @CustomerID
AND Metro.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''Metro'') = 1 
',1)
--Trip Checklist
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (25, 'LOGISTICS', 'TripManagerCheckList', 'TripManagerCheckList', 'Views/Settings/Company/TripManagerCheckListCatalog.aspx?Screen=TripMgrChkLst',
0, 'TripManagerCheckList','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CheckListID AS PrimaryKeyValue,
''<b>Checklist Code: </b>'' + ISNULL(REPLACE(TripManagerCheckList.CheckListCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(TripManagerCheckList.CheckListDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM TripManagerCheckList    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
CheckListCD like ''%'' + ''@SearchStr'' +''%''     
OR CheckListDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND TripManagerCheckList.CustomerID = @CustomerID
AND TripManagerCheckList.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''TripManagerCheckList'') = 1
',1)
--Trip Checklist Group
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (26, 'LOGISTICS', 'TripManagerCheckListGroup', 'TripManagerCheckListGroup', 'Views/Settings/Company/TripManagerCheckListGroupCatalog.aspx?Screen=TripMgrChkLstGrp',
0, 'TripManagerCheckListGroup','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CheckGroupID AS PrimaryKeyValue,
''<b>Checklist Group Code: </b>'' + ISNULL(REPLACE(TripManagerCheckListGroup.CheckGroupCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(TripManagerCheckListGroup.CheckGroupDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM TripManagerCheckListGroup    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
CheckGroupCD like ''%'' + ''@SearchStr'' +''%''     
OR CheckGroupDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND TripManagerCheckListGroup.CustomerID = @CustomerID
AND TripManagerCheckListGroup.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''TripManagerCheckListGroup'') = 1
',1)
--CREW
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (27, 'PEOPLE', 'Crew', 'CrewRoster', 'Views/Settings/People/CrewRoster.aspx?Screen=CrewRoster',
0, 'CrewRoster','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 Crew.CrewID AS PrimaryKeyValue,  
 ''<b>Crew Code: </b>'' + 
 ISNULL(REPLACE(Crew.CrewCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Home Base: </b>'' + 
  ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Client Code: </b>'' +
  ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Department: </b>'' +
  ISNULL(REPLACE(Department.DepartmentCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>First Name: </b>'' +
  ISNULL(REPLACE(Crew.FirstName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Middle Name: </b>'' +
  ISNULL(REPLACE(Crew.MiddleInitial,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Last Name: </b>'' +
  ISNULL(REPLACE(Crew.LastName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Citizenship: </b>'' +
 ISNULL(REPLACE(Country.CountryCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Country of Birth: </b>'' + 
 ISNULL(REPLACE(Country.CountryCD,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM Crew    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
	LEFT JOIN Company ON Company.HomebaseID = Crew.HomebaseID 
	LEFT JOIN Client ON Client.ClientID = Crew.ClientID  
	LEFT JOIN Department ON Department.DepartmentID = Crew.DepartmentID 
	LEFT JOIN Country ON Country.CountryID = Crew.Citizenship
	and Country.CountryID = Crew.CountryOfBirth
	--LEFT JOIN Country c2 ON c2.CountryID = Crew.CountryOfBirth
   
  WHERE   
   (  Crew.CrewCD like ''%'' + ''@SearchStr'' +''%''   
      OR Company.BaseDescription like ''%'' + ''@SearchStr'' +''%'' 
      OR Client.ClientCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Department.DepartmentCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Crew.FirstName like ''%'' + ''@SearchStr'' +''%'' 
      OR Crew.MiddleInitial like ''%'' + ''@SearchStr'' +''%'' 
      OR Crew.LastName like ''%'' + ''@SearchStr'' +''%''  
      OR Country.CountryCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Country.CountryCD like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND Crew.CustomerID = @CustomerID	
   AND Crew.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewRoster'') = 1  
',1)
--PASSENGER
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (28, 'PEOPLE', 'Passenger', 'PassengerRequestor', 'Views/Settings/People/PassengerCatalog.aspx?Screen=PassengerRequestor',
0, 'PassengerRequestor','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 Passenger.PassengerRequestorID AS PrimaryKeyValue,  
 ''<b>PAX Code: </b>'' + 
 ISNULL(REPLACE(Passenger.PassengerRequestorCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>First Name: </b>'' + 
  ISNULL(REPLACE(Passenger.FirstName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Middle Name: </b>'' +
  ISNULL(REPLACE(Passenger.MiddleInitial,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Last Name: </b>'' +
  ISNULL(REPLACE(Passenger.LastName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Country Of Residence: </b>'' +
  ISNULL(REPLACE(Country.CountryCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Nationality: </b>'' +
  ISNULL(REPLACE(Country.CountryCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Client Code: </b>'' +
  ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Home Base: </b>'' +
 ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Department: </b>'' + 
 ISNULL(REPLACE(Department.DepartmentCD,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM Passenger    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
	LEFT JOIN Country ON Country.CountryID = Passenger.CountryOfResidenceID 
	AND Country.CountryID = Passenger.GreenCardCountryID
	--LEFT JOIN Country c2 ON c2.CountryID = Passenger.GreenCardCountryID
	LEFT JOIN Client ON Client.ClientID = Passenger.ClientID  
	LEFT JOIN Company ON Company.HomebaseID = Passenger.HomebaseID
	LEFT JOIN Department ON Department.DepartmentID = Passenger.DepartmentID 
	   
  WHERE   
   (  Passenger.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%''   
      OR Passenger.FirstName like ''%'' + ''@SearchStr'' +''%'' 
      OR Passenger.MiddleInitial like ''%'' + ''@SearchStr'' +''%'' 
      OR Passenger.LastName like ''%'' + ''@SearchStr'' +''%'' 
      OR Country.CountryCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Country.CountryCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Client.ClientCD like ''%'' + ''@SearchStr'' +''%'' 
      OR Company.BaseDescription like ''%'' + ''@SearchStr'' +''%'' 
      OR Department.DepartmentCD like ''%'' + ''@SearchStr'' +''%'' 
   )  
   AND Passenger.CustomerID = @CustomerID
   AND Passenger.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PassengerRequestor'') = 1  
',1)
--TRAVEL COORDINATOR  
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (29, 'PEOPLE', 'TravelCoordinator', 'TravelCoordinator', 'Views/Settings/People/TravelCoordinator.aspx?Screen=TravelCoordinator',
0, 'TravelCoordinator','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 TravelCoordinator.TravelCoordinatorID AS PrimaryKeyValue,  
 ''<b>Travel Coordinator Code: </b>'' + 
 ISNULL(REPLACE(TravelCoordinator.TravelCoordCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>First Name: </b>'' + 
  ISNULL(REPLACE(TravelCoordinator.FirstName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Middle Name: </b>'' +
  ISNULL(REPLACE(TravelCoordinator.MiddleName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Last Name: </b>'' + 
 ISNULL(REPLACE(TravelCoordinator.LastName,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM TravelCoordinator    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
	   
  WHERE   
   (  TravelCoordinator.TravelCoordCD like ''%'' + ''@SearchStr'' +''%''   
      OR TravelCoordinator.FirstName like ''%'' + ''@SearchStr'' +''%'' 
      OR TravelCoordinator.MiddleName like ''%'' + ''@SearchStr'' +''%'' 
      OR TravelCoordinator.LastName like ''%'' + ''@SearchStr'' +''%'' 
   )  
   AND TravelCoordinator.CustomerID = @CustomerID  
   AND TravelCoordinator.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''TravelCoordinator'') = 1   
',1)
--Emergency Contact
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (30, 'PEOPLE', 'EmergencyContact', 'EmergencyContact', 'Views/Settings/People/EmergencyContactCatalog.aspx?Screen=EmergencyContact',
0, 'EmergencyContact','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 EmergencyContact.EmergencyContactID AS PrimaryKeyValue,  
 ''<b>Emergency Contact Code: </b>'' + 
 ISNULL(REPLACE(EmergencyContact.EmergencyContactCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>First Name: </b>'' + 
  ISNULL(REPLACE(EmergencyContact.FirstName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Middle Name: </b>'' +
  ISNULL(REPLACE(EmergencyContact.MiddleName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Last Name: </b>'' + 
 ISNULL(REPLACE(EmergencyContact.LastName,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM EmergencyContact    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  EmergencyContact.EmergencyContactCD like ''%'' + ''@SearchStr'' +''%''   
      OR EmergencyContact.FirstName like ''%'' + ''@SearchStr'' +''%'' 
      OR EmergencyContact.MiddleName like ''%'' + ''@SearchStr'' +''%'' 
      OR EmergencyContact.LastName like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND EmergencyContact.CustomerID = @CustomerID
   AND EmergencyContact.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''EmergencyContact'') = 1    
',1)
-- CLIENT
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (31, 'PEOPLE', 'Client', 'ClientCode', 'Views/Settings/Company/ClientCodeCatalog.aspx?Screen=Client',
0, 'ClientCode','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 Client.ClientID AS PrimaryKeyValue,  
 ''<b>Client Code: </b>'' + 
 ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
 ISNULL(REPLACE(Client.ClientDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM Client    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  Client.ClientCD like ''%'' + ''@SearchStr'' +''%''   
      OR Client.ClientDescription like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND Client.CustomerID = @CustomerID
   AND Client.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''ClientCode'') = 1   
',1)
-- CREW CHECKLIST
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (32, 'PEOPLE', 'CrewCheckList', 'CrewCheckList', 'Views/Settings/People/CrewCheckListCatalog.aspx?Screen=CrewChecklist',
0, 'CrewCheckList','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 CrewCheckList.CrewCheckID AS PrimaryKeyValue,  
 ''<b>Checklist Code: </b>'' + 
 ISNULL(REPLACE(CrewCheckList.CrewCheckCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
 ISNULL(REPLACE(CrewCheckList.CrewChecklistDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM CrewCheckList    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  CrewCheckList.CrewCheckCD like ''%'' + ''@SearchStr'' +''%''   
      OR CrewCheckList.CrewChecklistDescription like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND CrewCheckList.CustomerID = @CustomerID
   AND CrewCheckList.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewCheckList'') = 1   
',1)
-- CREW DUTY RULES
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (33, 'PEOPLE', 'CrewDutyRules', 'CrewDutyRules', 'Views/Settings/People/CrewDutyRulesCatalog.aspx?Screen=CrewDutyRules',
0, 'CrewDutyRules','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 CrewDutyRules.CrewDutyRulesID AS PrimaryKeyValue,  
 ''<b>Duty Rule Code: </b>'' + 
 ISNULL(REPLACE(CrewDutyRules.CrewDutyRuleCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
 ISNULL(REPLACE(CrewDutyRules.CrewDutyRulesDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM CrewDutyRules    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  CrewDutyRules.CrewDutyRuleCD like ''%'' + ''@SearchStr'' +''%''   
      OR CrewDutyRules.CrewDutyRulesDescription like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND CrewDutyRules.CustomerID = @CustomerID
   AND CrewDutyRules.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewDutyRules'') = 1  
',1)
-- CREW DUTY TYPES
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (34, 'PEOPLE', 'CrewDutyType', 'CrewDutyTypes', 'Views/Settings/People/CrewDutyTypes.aspx?Screen=CrewDutyTypes',
0, 'CrewDutyTypes','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 CrewDutyType.DutyTypeID AS PrimaryKeyValue,  
 ''<b>Crew Duty Type Code: </b>'' + 
 ISNULL(REPLACE(CrewDutyType.DutyTypeCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
 ISNULL(REPLACE(CrewDutyType.DutyTypesDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM CrewDutyType    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  CrewDutyType.DutyTypeCD like ''%'' + ''@SearchStr'' +''%''   
      OR CrewDutyType.DutyTypesDescription like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND CrewDutyType.CustomerID = @CustomerID
   AND CrewDutyType.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewDutyTypes'') = 1  
',1)
-- PASSENGER ADDITIONAL INFO

INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (35, 'PEOPLE', 'PassengerAdditionalInfo', 'PassengerAdditionalInfo', 'Views/Settings/People/PassengerAdditionalInfo.aspx?Screen=PassengerAddInfo',
0, 'PassengerAdditionalInfo','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 PassengerInformation.PassengerInformationID AS PrimaryKeyValue,  
 ''<b>Additional Info Code: </b>'' + 
 ISNULL(REPLACE(PassengerInformation.PassengerInfoCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
 ISNULL(REPLACE(PassengerInformation.PassengerDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM PassengerInformation    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId 
   
  WHERE   
   (  PassengerInformation.PassengerInfoCD like ''%'' + ''@SearchStr'' +''%''   
      OR PassengerInformation.PassengerDescription like ''%'' + ''@SearchStr'' +''%'' 
   )   
   AND PassengerInformation.CustomerID = @CustomerID
   AND PassengerInformation.IsDeleted = 0    

',1)
--PASSENGER GROUP
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (36, 'PEOPLE', 'PassengerGroup', 'PassengerGroup', 'Views/Settings/People/PassengerGroupCatalog.aspx?Screen=PassengerGroup',
0, 'PassengerGroup','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 PassengerGroup.PassengerGroupID AS PrimaryKeyValue,  
 ''<b>Passenger Group Code: </b>'' + 
 ISNULL(REPLACE(PassengerGroup.PassengerGroupCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
  ISNULL(REPLACE(PassengerGroup.PassengerGroupName,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Home Base: </b>'' + 
 ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM PassengerGroup    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
	LEFT JOIN Company ON Company.HomebaseID = PassengerGroup.HomebaseID
	   
  WHERE   
   (  PassengerGroup.PassengerGroupCD like ''%'' + ''@SearchStr'' +''%''   
      OR PassengerGroup.PassengerGroupName like ''%'' + ''@SearchStr'' +''%'' 
      OR Company.BaseDescription like ''%'' + ''@SearchStr'' +''%'' 
   )  
   AND PassengerGroup.CustomerID = @CustomerID
   AND PassengerGroup.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''PassengerGroup'') = 1  
',1)
--FLIGHT PURPOSE
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (37, 'PEOPLE', 'FlightPurpose', 'FlightPurpose', 'Views/Settings/Fleet/FlightPurpose.aspx?Screen=FlightPurpose',
0, 'FlightPurpose','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 FlightPurpose.FlightPurposeID AS PrimaryKeyValue,  
 ''<b>Flight Purpose Code: </b>'' + 
 ISNULL(REPLACE(FlightPurpose.FlightPurposeCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
  ISNULL(REPLACE(FlightPurpose.FlightPurposeDescription,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Client Code: </b>'' + 
 ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM FlightPurpose    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
	LEFT JOIN Client ON Client.ClientID = FlightPurpose.ClientID
	   
  WHERE   
   (  FlightPurpose.FlightPurposeCD like ''%'' + ''@SearchStr'' +''%''   
      OR FlightPurpose.FlightPurposeDescription like ''%'' + ''@SearchStr'' +''%'' 
      OR Client.ClientCD like ''%'' + ''@SearchStr'' +''%'' 
   )  
   AND FlightPurpose.CustomerID = @CustomerID
   AND FlightPurpose.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FlightPurpose'') = 1  
',1)
--CREW GROUP
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (38, 'PEOPLE', 'CrewGroup', 'CrewGroup', 'Views/Settings/People/CrewGroupCatalog.aspx?Screen=CrewGroup',
0, 'CrewGroup','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,  
 CrewGroup.CrewGroupID AS PrimaryKeyValue,  
 ''<b>Flight Purpose Code: </b>'' + 
 ISNULL(REPLACE(CrewGroup.CrewGroupCD,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Description: </b>'' + 
  ISNULL(REPLACE(CrewGroup.CrewGroupDescription,'''''''',''"''),'''') + 
 ''<br />'' +  ''<b>Client Code: </b>'' + 
 ISNULL(REPLACE(Company.BaseDescription,'''''''',''"''),'''') + 
 ''<br />''   AS ColumnValue 
   
  FROM CrewGroup    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
	LEFT JOIN Company ON Company.HomebaseID = CrewGroup.HomebaseID
	   
  WHERE   
   (  CrewGroup.CrewGroupCD like ''%'' + ''@SearchStr'' +''%''   
      OR CrewGroup.CrewGroupDescription like ''%'' + ''@SearchStr'' +''%'' 
      OR Company.BaseDescription like ''%'' + ''@SearchStr'' +''%'' 
   )  
   AND CrewGroup.CustomerID = @CustomerID
   AND CrewGroup.IsDeleted = 0    
   AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewGroup'') = 1  
',1)
----User Administration
--SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

--UserName AS PrimaryKeyValue,
--'<b>Display Name: </b>' + ISNULL(REPLACE(UserMaster.UserName,'''','"'),'') + '<br />'
--+  '<b>First Name: </b>' + ISNULL(REPLACE(UserMaster.FirstName,'''','"'),'') + '<br />'   
--+  '<b>Middle Name: </b>' + ISNULL(REPLACE(UserMaster.MiddleName,'''','"'),'') + '<br />'   
--+  '<b>Last Name: </b>' + ISNULL(REPLACE(UserMaster.LastName,'''','"'),'') + '<br />'   
--+  '<b>Home Base: </b>' + ISNULL(REPLACE(Company.BaseDescription,'''','"'),'') + '<br />'   
--+  '<b>Client Code: </b>' + ISNULL(REPLACE(Client.ClientCD,'''','"'),'') + '<br />'   
--+  '<b>Travel Coordinator: </b>' + ISNULL(REPLACE(TravelCoordinator.TravelCoordCD,'''','"'),'') + '<br />'   

--AS ColumnValue

--FROM UserMaster    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
--LEFT OUTER JOIN Company ON Company.HomebaseID = UserMaster.HomebaseID
--LEFT OUTER JOIN Client ON Client.ClientID = UserMaster.ClientID
--LEFT OUTER JOIN TravelCoordinator ON TravelCoordinator.TravelCoordinatorID = UserMaster.TravelCoordID

--WHERE     (    
--UserName like '%' + '@SearchStr' +'%'     
--OR UserMaster.FirstName like '%' + '@SearchStr' +'%'    
--OR UserMaster.MiddleName like '%' + '@SearchStr' +'%'   
--OR UserMaster.LastName like '%' + '@SearchStr' +'%'   
--OR BaseDescription like '%' + '@SearchStr' +'%'   
--OR ClientCD like '%' + '@SearchStr' +'%'   
--OR TravelCoordCD like '%' + '@SearchStr' +'%'   
--) 

--AND UserMaster.CustomerID = @CustomerID
--AND UserMaster.IsDeleted = 0    
--AND [dbo].[fCheckPermission] ('@SessionId','@IsSysAdmin','UserMaster') = 1

----Group Administration
--SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

--UserGroupID AS PrimaryKeyValue,
--'<b>User Group: </b>' + ISNULL(REPLACE(UserGroup.UserGroupCD,'''','"'),'') + '<br />'
--+  '<b>Description: </b>' + ISNULL(REPLACE(UserGroup.UserGroupDescription,'''','"'),'') + '<br />'   

--AS ColumnValue

--FROM UserGroup    LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

--WHERE     (    
--UserGroupCD like '%' + '@SearchStr' +'%'     
--OR UserGroupDescription like '%' + '@SearchStr' +'%'    ) 

--AND UserGroup.CustomerID = @CustomerID
--AND UserGroup.IsDeleted = 0    
--AND [dbo].[fCheckPermission] ('@SessionId','@IsSysAdmin','UserGroup') = 1

-- FLEET ADDITIONAL INFO
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (39, 'FLEET', 'FleetProfileInformation', 'FleetProfileAdditionalInfo', 'Views/Settings/Fleet/FleetProfileAdditionalInfoCatalog.aspx?Screen=FleetProfileAdditionalInfoCatalog',
0, 'FleetProfileAdditionalInfo','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FleetProfileInformationID AS PrimaryKeyValue,
''<b>Fleet Additional Info Code: </b>'' + ISNULL(REPLACE(FleetProfileInformation.FleetInfoCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(FleetProfileInformation.FleetProfileAddInfDescription,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM FleetProfileInformation    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
	

WHERE     (    
FleetInfoCD like ''%'' + ''@SearchStr'' + ''%''     
OR FleetProfileAddInfDescription like ''%'' + ''@SearchStr'' + ''%''    ) 

AND FleetProfileInformation.CustomerID = @CustomerID
AND FleetProfileInformation.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FleetProfileAdditionalInfo'') = 1',1)

-- CREW ADDITIONAL INFO
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (40, 'PEOPLE', 'CrewInformation', 'CrewRosterAddInfoCatalog', 'Views/Settings/People/CrewRosterAddInfoCatalog.aspx?Screen=CrewAddInfo',
0, 'CrewInformation','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CrewInfoID AS PrimaryKeyValue,
''<b>Crew Additional Info Code: </b>'' + ISNULL(REPLACE(CrewInformation.CrewInfoCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Description: </b>'' + ISNULL(REPLACE(CrewInformation.CrewInformationDescription,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM CrewInformation    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    

WHERE     (    
CrewInfoCD like ''%'' + ''@SearchStr'' +''%''     
OR CrewInformationDescription like ''%'' + ''@SearchStr'' +''%''    ) 

AND CrewInformation.CustomerID = @CustomerID
AND CrewInformation.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CrewRosterAddInfo'') = 1',1)

--FLEET PROFILE
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (41, 'FLEET', 'Fleet', 'FleetProfile', 'Views/Settings/Fleet/FleetProfileCatalog.aspx?Screen=FleetProfile',
0, 'Fleet','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FleetID AS PrimaryKeyValue,
''<b>Aircraft Code: </b>'' + ISNULL(REPLACE(Fleet.AircraftCD,'''''''',''"''),'''') + ''<br />''
+  ''<b>Serial N0.: </b>'' + ISNULL(REPLACE(Fleet.SerialNum,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Tail No.: </b>'' + ISNULL(REPLACE(Fleet.TailNum,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Aircraft Type Description: </b>'' + ISNULL(REPLACE(Fleet.TypeDescription,'''',''"''),'''') + ''<br />''   
+  ''<b>Home Base:  </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Client: </b>'' + ISNULL(REPLACE(Client.ClientCD,'''''''',''"''),'''') + ''<br />''   
+  ''<b>Vendor: </b>'' + ISNULL(REPLACE(Vendor.VendorCD,'''''''',''"'' ),'''') + ''<br />''   
+  ''<b>Emergency Contact: </b>'' + ISNULL(REPLACE(EmergencyContact.EmergencyContactCD,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM Fleet    
	LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId    
	LEFT OUTER JOIN Airport ON Airport.AirportID = Fleet.HomebaseID
	LEFT OUTER JOIN Client ON Client.ClientID = Fleet.ClientID
	LEFT OUTER JOIN Vendor ON Vendor.VendorID = Fleet.VendorID
	LEFT OUTER JOIN EmergencyContact ON EmergencyContact.EmergencyContactID = Fleet.EmergencyContactID
WHERE     (    
AircraftCD like ''%'' + ''@SearchStr'' +''%''     
OR SerialNum like ''%'' + ''@SearchStr'' +''%''    
OR TailNum like ''%'' + ''@SearchStr'' +''%''    
OR TypeDescription like ''%'' + ''@SearchStr'' +''%''    
OR IcaoID like ''%'' + ''@SearchStr'' +''%''    
OR ClientCD like ''%'' + ''@SearchStr'' +''%''    
OR VendorCD like ''%'' + ''@SearchStr''+''%''    
OR EmergencyContactCD like ''%'' + ''@SearchStr'' + ''%'' )

AND Fleet.CustomerID = @CustomerID
AND Fleet.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FleetProfile'') = 1',1)

--CharterQuote
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (48, 'Charter', 'CQFile', 'CQFile', 'views/Transactions/CharterQuote/CharterQuoteRequest.aspx?seltab=File',
0, 'Trips','SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQ.CQFileID AS PrimaryKeyValue,
''<b>File No.: </b>'' + ISNULL(REPLACE(CQ.FileNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(A.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Customer: </b>'' + ISNULL(REPLACE(CQCust.CQCustomerCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Contact: </b>'' + ISNULL(REPLACE(CQCustCon.CQCustomerContactCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Salesperson: </b>'' + ISNULL(REPLACE(SP.SalesPersonCD,'''''''',''"''),'''') + ''<br />'' +\
''<b>Lead Source: </b>'' + ISNULL(REPLACE(LS.LeadSourceCD,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM CQFile CQ 
INNER JOIN Company C on CQ.HomebaseID = C.HomebaseID  
INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID   
LEFT JOIN CQCustomer CQCust on  CQ.CustomerID = CQCust.CQCustomerID
LEFT JOIN CQCustomerContact CQCustCon on CQ.CQCustomerContactID = CQCustCon.CQCustomerContactID
LEFT JOIN SalesPerson SP on CQ.SalesPersonID = SP.SalesPersonID
LEFT JOIN LeadSource LS on CQ.LeadSourceID =   LS.LeadSourceID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(CQ.FileNUM like ''%'' + ''@SearchStr'' +''%'' 
OR A.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR CQCust.CQCustomerCD like ''%'' + ''@SearchStr'' +''%''
OR CQCustCon.CQCustomerContactCD like ''%'' + ''@SearchStr'' +''%''
OR SP.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''
OR LS.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''

--Quote CQMain
OR 1 = (SELECT TOP 1 1 FROM CQMain CQM
LEFT JOIN Vendor V on CQM.VendorID = V.VendorID
LEFT JOIN Fleet F on CQM.FleetID= F.FleetID
LEFT JOIN Aircraft A on CQM.AircraftID= A.AircraftID
LEFT JOIN Passenger P on CQM.PassengerRequestorID= P.PassengerRequestorID
LEFT JOIN CQLostBusiness LB on CQM.CQLostBusinessID= LB.CQLostBusinessID

--LEG
LEFT JOIN CQLEG CQL on CQM.CQMainID = CQL.CQMainID
LEFT JOIN AIRPORT DA on CQL.DAirportID = DA.AirportID
LEFT JOIN AIRPORT AA on CQL.AAirportID = AA.AirportID

--PAX
LEFT JOIN CQPassenger CQP on  CQL.CQLegID = CQP.CQLegID
LEFT JOIN Passenger DBP on CQP.PassengerRequestorID= DBP.PassengerRequestorID

-- Logistics
LEFT JOIN CQCateringList CQC on  CQL.CQLegID = CQC.CQLegID
LEFT JOIN Catering DBC on CQC.CateringID = DBC.CateringID
LEFT JOIN CQFBOList CQF on  CQL.CQLegID = CQF.CQLegID
LEFT JOIN FBO DBF on CQF.FBOID = DBF.FBOID
LEFT JOIN CQHotelList CQH on  CQL.CQLegID= CQH.CQLegID
LEFT JOIN Hotel DBH on CQH.HotelID = DBH.HotelID
LEFT JOIN CQTransportList CQT on  CQL.CQLegID= CQT.CQLegID
LEFT JOIN Transport DBT on CQT.TransportID = DBT.TransportID

WHERE 
(
-- Quote
CQM.QuoteNUM like ''%'' + ''@SearchStr'' +''%''  
OR V.VendorCD like ''%'' + ''@SearchStr'' +''%'' 
OR F.TailNum  like ''%'' + ''@SearchStr'' +''%'' 
OR A.AircraftCD like ''%'' + ''@SearchStr'' +''%'' 
OR P.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' 
OR LB.CQLostBusinessCD like ''%'' + ''@SearchStr'' +''%'' 

--LEG
OR DA.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR AA.ICAOID like ''%'' + ''@SearchStr'' +''%''

--PAX
OR DBP.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQP.PassengerName like ''%'' + ''@SearchStr'' +''%'' \

--Logistics
OR DBF.FBOCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQF.CQFBOListDescription like ''%'' + ''@SearchStr'' +''%'' 
OR DBC.CateringCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQC.CQCateringListDescription like ''%'' + ''@SearchStr'' +''%'' 
OR DBH.HotelCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQH.CQHotelListDescription like ''%'' + ''@SearchStr'' +''%''
OR DBT.TransportCD like ''%'' + ''@SearchStr'' +''%'' 
OR CQT.CQTransportListDescription like ''%'' + ''@SearchStr'' +''%''
)
 AND CQM.CQFileID = CQ.CQFileID AND CQM.IsDeleted = 0)
 ) AND isnull(CQ.ExpressQuote,0) =0   and CQ.CustomerID=@CustomerId and CQ.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQFile'') = 1' ,1)

-- ExpressQuote
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (49, 'Charter', 'CQFile', 'CQFile', 'views/Transactions/CharterQuote/ExpressQuote.aspx',
0, 'Trips','SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQ.CQFileID AS PrimaryKeyValue,
''<b>Vendor: </b>'' + ISNULL(REPLACE(V.VendorCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(F.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Type Code: </b>'' + ISNULL(REPLACE(A.AircraftCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Salesperson: </b>'' + ISNULL(REPLACE(SP.SalesPersonCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Lead Source: </b>'' + ISNULL(REPLACE(LS.LeadSourceCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Depart ICAO: </b>'' + ISNULL(REPLACE(DA.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>Arrival ICAO: </b>'' + ISNULL(REPLACE(AA.ICAOID,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM CQFile CQ 
INNER JOIN CQMain CQM on CQ.CQFileID = CQM.CQFileID
INNER JOIN CQLEG CQL on CQM.CQMainID = CQL.CQMainID
LEFT JOIN SalesPerson SP on CQ.SalesPersonID = SP.SalesPersonID
LEFT JOIN LeadSource LS on CQ.LeadSourceID =   LS.LeadSourceID
LEFT JOIN Vendor V on CQM.VendorID = V.VendorID
LEFT JOIN Fleet F on CQM.FleetID= F.FleetID
LEFT JOIN Aircraft A on CQM.AircraftID= A.AircraftID
LEFT JOIN AIRPORT DA on CQL.DAirportID = DA.AirportID
LEFT JOIN AIRPORT AA on CQL.AAirportID = AA.AirportID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(CQ.FileNUM like ''%'' + ''@SearchStr'' +''%'' 
OR SP.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''
OR LS.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''
OR V.VendorCD like ''%'' + ''@SearchStr'' +''%'' 
OR F.TailNum  like ''%'' + ''@SearchStr'' +''%'' 
OR A.AircraftCD like ''%'' + ''@SearchStr'' +''%'' 

--LEG
OR DA.ICAOID like ''%'' + ''@SearchStr'' +''%''
OR AA.ICAOID like ''%'' + ''@SearchStr'' +''%''
)
AND CQM.CQFileID = CQ.CQFileID AND CQM.IsDeleted = 0
AND isnull(CQ.ExpressQuote,0) =1   and CQ.CustomerID=@CustomerId and CQ.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQFile'') = 1'
,1)

-- Charter Quote Customer
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (42, 'CHARTER', 'CQCustomer', 'CQCustomer', 'Views/Settings/People/CharterQuoteCustomerCatalog.aspx?Screen=CharterQuote',
0, 'Charter Quote Customer','
SELECT FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CQCustomer.CQCustomerID AS PrimaryKeyValue,
''<b>Customer Code: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerCD,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Customer Name: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerName,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Closest ICAO: </b>'' + ISNULL(REPLACE(A.IcaoID,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Billing Infor - Name: </b>'' + ISNULL(REPLACE(CQCustomer.BillingName,'''''''',''"''),'''') + ''<br />'' 
+  ''<b>Main Contact - Name: </b>'' + ISNULL(REPLACE(CQCustomer.CQCustomerName,'''''''',''"''),'''') + ''<br />'' 

AS ColumnValue

  FROM CQCustomer       
  LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId
  LEFT OUTER JOIN Company ON Company.HomebaseID = CQCustomer.HomebaseID        
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
  LEFT OUTER JOIN Airport A ON A.AirportID =  CQCustomer.AirportID       
  WHERE 
  (
  CQCustomerCD like ''%'' + ''@SearchStr'' +''%'' 
   OR CQCustomerName like ''%'' + ''@SearchStr'' +''%''
   OR Airport.IcaoID like ''%'' + ''@SearchStr'' +''%''
   OR A.IcaoID like ''%'' + ''@SearchStr'' +''%''
   OR CQCustomer.BillingName like ''%'' + ''@SearchStr'' +''%''
   OR CQCustomer.CQCustomerName like ''%'' + ''@SearchStr'' +''%''
  )  AND CQCustomer.CustomerID = @CustomerID AND CQCustomer.IsDeleted = 0
  AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CQCustomer'') = 1
  ',1)
  
--Fee Schedule
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (43, 'CHARTER', 'FeeSchedule', 'FeeSchedule', 'Views/Settings/Fleet/FeeScheduleCatalog.aspx?Screen=FeeSchedule',
0, 'Fee Schedule','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FeeScheduleID AS PrimaryKeyValue,
''<b>Account No.: </b>'' + ISNULL(REPLACE(Account.AccountNum,'''''''',''"''),'''') + ''<br />''  
+  ''<b>Group: </b>'' + ISNULL(REPLACE(FeeGroup.FeeGroupCD,'''''''',''"''),'''') + ''<br />''   

AS ColumnValue

FROM FeeSchedule    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId    
    LEFT JOIN Account ON Account.AccountID = FeeSchedule.AccountID
    LEFT JOIN FeeGroup ON FeeGroup.FeeGroupID = FeeSchedule.FeeGroupID
WHERE     (    
Account.AccountNum like ''%'' + ''@SearchStr'' +''%''     
OR FeeGroup.FeeGroupCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND FeeSchedule.CustomerID = @CustomerID
AND FeeSchedule.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FeeSchedule'') = 1',1)
 
--Fee Group
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (44, 'CHARTER', 'FeeGroup', 'FeeGroup', 'Views/Settings/Fleet/FeeScheduleGroupCatalog.aspx?Screen=FeeScheduleGroup',
0, 'Fee Schedule Group','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

FeeGroupID AS PrimaryKeyValue,
''<b>Fee Schedule Group Code: </b>'' + ISNULL(REPLACE(FeeGroup.FeeGroupCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM FeeGroup    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 FeeGroup.FeeGroupCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND FeeGroup.CustomerID = @CustomerID
AND FeeGroup.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''FeeGroup'') = 1',1)

--Lead Source
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (45, 'CHARTER', 'LeadSource', 'LeadSource', 'Views/Settings/Fleet/LeadSourceCatalog.aspx?Screen=LeadSource',
0, 'Lead Source','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

LeadSourceID AS PrimaryKeyValue,
''<b>Lead Source Code: </b>'' + ISNULL(REPLACE(LeadSource.LeadSourceCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM LeadSource    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 LeadSource.LeadSourceCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND LeadSource.CustomerID = @CustomerID
AND LeadSource.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''LeadSource'') = 1',1)

--Sales Person
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (46, 'CHARTER', 'Salesperson', 'SalesPerson', 'Views/Settings/People/SalesPersonCatalog.aspx?Screen=Salesperson',
0, 'Salesperson','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

SalesPersonID AS PrimaryKeyValue,
''<b>Code: </b>'' + ISNULL(REPLACE(SalesPerson.SalesPersonCD,'''''''',''"''),'''') + ''<br />''  
+  ''<b>First Name: </b>'' + ISNULL(REPLACE(SalesPerson.FirstName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Middle Name: </b>'' + ISNULL(REPLACE(SalesPerson.MiddleName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Last Name: </b>'' + ISNULL(REPLACE(SalesPerson.LastName,'''''''',''"''),'''') + ''<br />''
+  ''<b>Home Base: </b>'' + ISNULL(REPLACE(Airport.IcaoID,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM SalesPerson    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId            
    LEFT JOIN Company ON Company.HomebaseID = SalesPerson.HomebaseID
    LEFT JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
WHERE     (    
SalesPerson.SalesPersonCD like ''%'' + ''@SearchStr'' +''%''     
OR SalesPerson.FirstName like ''%'' + ''@SearchStr'' +''%''    
OR SalesPerson.MiddleName like ''%'' + ''@SearchStr'' +''%'' 
OR SalesPerson.LastName like ''%'' + ''@SearchStr'' +''%'' 
OR Airport.IcaoID like ''%'' + ''@SearchStr'' +''%'' 
) 

AND SalesPerson.CustomerID = @CustomerID
AND SalesPerson.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''SalesPerson'') = 1',1)

--Lost Business
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (47, 'CHARTER', 'CQLostBusiness', 'LostBusiness', 'Views/Settings/Fleet/LostBusinessCatalog.aspx?Screen=LostBusiness',
0, 'Lost Business','
SELECT FPSearchParameters.SearchId,  FPSearchParameters.ModuleName,  FPSearchParameters.TableName,   FPSearchParameters.PageVirtualPath,   

CQLostBusinessID AS PrimaryKeyValue,
''<b>Lost Business Code: </b>'' + ISNULL(REPLACE(CQLostBusiness.CQLostBusinessCD,'''''''',''"''),'''') + ''<br />''

AS ColumnValue

FROM CQLostBusiness    
	LEFT JOIN FPSearchParameters ON FPSearchParameters.SearchId = @EntityId        
WHERE     (    
 CQLostBusiness.CQLostBusinessCD like ''%'' + ''@SearchStr'' +''%''    ) 

AND CQLostBusiness.CustomerID = @CustomerID
AND CQLostBusiness.IsDeleted = 0    
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''LostBusiness'') = 1',1)


--Postflight Expense
INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (50, 'TRIPLOGS', 'PostflightMain', 'PostflightMain', 'views/Transactions/PostFlight/ExpenseCatalog.aspx',
0, 'Trip Logs','SELECT   
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
PostflightExpense.PostflightExpenseID AS PrimaryKeyValue,
''<b>Log No: </b>'' + ISNULL(REPLACE(POMain.LogNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(fleet.TailNum,'''''''',''"''),'''') + ''<br />'' +
''<b>Slip No.: </b>'' + ISNULL(REPLACE(PostflightExpense.SlipNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(HomebaseAirport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>ICAO: </b>'' + ISNULL(REPLACE(Airport.ICAOID,'''''''',''"''),'''') + ''<br />'' +
''<b>FBO Code: </b>'' + ISNULL(REPLACE(FBO.FBOCD,'''''''',''"''),'''') + ''<br />'' +
''<b>FBO Name: </b>'' + ISNULL(REPLACE(FBO.FBOVendor,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue      
FROM PostflightMain POMain  
INNER JOIN PostflightExpense ON PostflightExpense.POLogID = POMain.POLogID  
LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = PostflightExpense.FleetID  
LEFT OUTER JOIN Company company on PostflightExpense.HomeBaseID = company.HomeBaseID  
LEFT OUTER JOIN Airport HomebaseAirport ON HomebaseAirport.AirportID = company.HomebaseAirportID  
LEFT OUTER JOIN Airport ON Airport.AirportID = PostflightExpense.AirportID  
LEFT JOIN FBO  ON FBO.FBOID = PostflightExpense.FBOID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId

WHERE  
(
POMain.LogNum like ''%'' + ''@SearchStr'' +''%'' OR
fleet.TailNum like ''%'' + ''@SearchStr'' +''%'' OR
PostflightExpense.SlipNUM like ''%'' + ''@SearchStr'' +''%'' OR
HomebaseAirport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
Airport.ICAOID like ''%'' + ''@SearchStr'' +''%'' OR
FBO.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
FBO.FBOVendor like ''%'' + ''@SearchStr'' +''%''

) AND
PostflightExpense.CustomerID = @CustomerID  
AND isnull(PostflightExpense.IsDeleted,0) = 0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''POLogManager'') = 1',1)

--Corporate Request 

INSERT INTO [FPSearchParameters] ([SearchId],[ModuleName],[TableName],[PermissionName],[PageVirtualPath],[SysAdminAccessOnly],[UserReadableTableName],[StaticQuery],[IsCustomerIdRequired] ) 
VALUES (51, 'CORPORATE', 'CRMain', 'CRManager', 'Views/Transactions/CorporateRequest/CorporateRequestMain.aspx?seltab=CorporateRequest',
0, 'Request','
SELECT 
FPSearchParameters.SearchId,
FPSearchParameters.ModuleName,
FPSearchParameters.TableName, 
FPSearchParameters.PageVirtualPath, 
CR.CRMainID AS PrimaryKeyValue,
''<b>Request No.: </b>'' + ISNULL(REPLACE(CR.CRTripNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Approver: </b>'' + ISNULL(REPLACE(CR.Approver,'''''''',''"''),'''') + ''<br />'' +
''<b>Trip No.: </b>'' + ISNULL(REPLACE(PM.TripNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Travel Coordinator: </b>'' + ISNULL(REPLACE(CR.TravelCoordinatorName,'''''''',''"''),'''') + ''<br />'' +
''<b>Tail No.: </b>'' + ISNULL(REPLACE(F.TailNUM,'''''''',''"''),'''') + ''<br />'' +
''<b>Type Code: </b>'' + ISNULL(REPLACE(AF.AircraftCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Requestor: </b>'' + ISNULL(REPLACE(P.PassengerName,'''''''',''"''),'''') + ''<br />'' +
''<b>Department: </b>'' + ISNULL(REPLACE(D.DepartmentCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Authorization: </b>'' + ISNULL(REPLACE(DA.AuthorizationCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Client Code: </b>'' + ISNULL(REPLACE(CL.ClientCD,'''''''',''"''),'''') + ''<br />'' +
''<b>Home Base: </b>'' + ISNULL(REPLACE(A.ICAOID,'''''''',''"''),'''') + ''<br />'' 
AS ColumnValue    
FROM CRMain CR 
INNER JOIN Company C on CR.HomebaseID = C.HomebaseID  
INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID   
LEFT JOIN PreflightMain PM on  PM.TripID = CR.TripID
LEFT JOIN Aircraft AF on AF.AircraftID = CR.AircraftID
LEFT JOIN Fleet F on F.FleetID = CR.FleetID
LEFT JOIN Passenger P on P.PassengerRequestorID = CR.PassengerRequestorID
LEFT JOIN Department D on D.DepartmentID = CR.DepartmentID
LEFT JOIN DepartmentAuthorization DA on DA.AuthorizationID = CR.AuthorizationID
LEFT JOIN Client CL on CL.ClientID = CR.ClientID
LEFT JOIN FPSearchParameters On FPSearchParameters.SearchId = @EntityId  
WHERE   
(CR.CRTripNUM like ''%'' + ''@SearchStr'' +''%'' 
OR CR.Approver like ''%'' + ''@SearchStr'' +''%''
OR PM.TripNUM like ''%'' + ''@SearchStr'' +''%''
OR CR.TravelCoordinatorName like ''%'' + ''@SearchStr'' +''%''
OR F.TailNUM like ''%'' + ''@SearchStr'' +''%''
OR AF.AircraftCD like ''%'' + ''@SearchStr'' +''%''
OR P.PassengerName like ''%'' + ''@SearchStr'' +''%''
OR D.DepartmentCD like ''%'' + ''@SearchStr'' +''%''
OR CL.ClientCD like ''%'' + ''@SearchStr'' +''%''
OR A.ICAOID like ''%'' + ''@SearchStr'' +''%''

--LEG
OR 1 = (SELECT TOP 1 1 FROM CRLeg CRL
LEFT JOIN AIRPORT DA on CRL.DAirportID = DA.AirportID
LEFT JOIN AIRPORT AA on CRL.AAirportID = AA.AirportID
WHERE 
(DA.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 AA.IcaoID like ''%'' + ''@SearchStr'' +''%'' OR
 CRL.TripNUM like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRL.ClientID = CR.ClientID AND CRL.IsDeleted = 0)

--PAX
OR 1 = (SELECT TOP 1 1 FROM CRPassenger 
LEFT JOIN Passenger PAX ON PAX.PassengerRequestorID = CRPassenger.PassengerRequestorID
INNER JOIN CRLeg ON CRLeg.LegID = CRPassenger.LegID 
WHERE 
(PAX.PassengerRequestorCD like ''%'' + ''@SearchStr'' +''%'' OR
 CRPassenger.PassengerName like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRPassenger.IsDeleted = 0)


-- FBO
OR 1 = (SELECT TOP 1 1 FROM CRFBOList 
LEFT JOIN FBO A ON A.FBOID = CRFBOList.FBOID
INNER JOIN CRLeg ON CRLeg.LegID = CRFBOList.LegID 
WHERE 
(A.FBOCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.FBOVendor like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRFBOList.IsDeleted = 0)

-- Catering

OR 1 = (SELECT TOP 1 1 FROM CRCateringList 
LEFT JOIN catering A ON A.CateringID = CRCateringList.CateringID
INNER JOIN CRLeg ON CRLeg.LegID = CRCateringList.LegID 
WHERE 
(A.CateringCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.CateringVendor like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRCateringList.IsDeleted = 0)

-- Transport

OR 1 = (SELECT TOP 1 1 FROM CRTransportList 
LEFT JOIN Transport A ON A.TransportID = CRTransportList.TransportID
INNER JOIN CRLeg ON CRLeg.LegID = CRTransportList.LegID 
WHERE 
(A.TransportCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.TransportationVendor like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRTransportList.IsDeleted = 0)

-- Hotel

OR 1 = (SELECT TOP 1 1 FROM CRHotelList 
LEFT JOIN Hotel A ON A.HotelID = CRHotelList.HotelID
INNER JOIN CRLeg ON CRLeg.LegID = CRHotelList.LegID 
WHERE 
(A.HotelCD like ''%'' + ''@SearchStr'' +''%'' OR
 A.Name like ''%'' + ''@SearchStr'' +''%'' 
) AND 
CRHotelList.IsDeleted = 0)

 ) AND CR.CustomerID=@CustomerId and CR.IsDeleted =0  
AND [dbo].[fCheckPermission] (''@SessionId'',''@IsSysAdmin'',''CRManager'') = 1' ,1)
