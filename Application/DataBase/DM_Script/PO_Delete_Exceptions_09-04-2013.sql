
/*

-- Deleted Exceptions for Postflight (BackUP)

2017	10009	2	Requestor is missing	2	NULL
2018	10009	2	Department is missing	2	NULL
2019	10009	2	Authorization is missing	2	NULL
2020	10009	2	Flight Category is missing	2	NULL
2021	10009	2	Fuel Out is missing	2	NULL
2022	10009	2	Fuel In is missing	2	NULL
2023	10009	2	Fuel Used is missing	2	NULL
2043	10009	5	Year No. is required	1	NULL

*/

DELETE FROM PostflightTripException 
	WHERE ExceptionTemplateID IN (2017, 2018, 2019, 2020, 2021, 2022, 2023, 2043)
GO	

DELETE FROM ExceptionTemplate 
	WHERE ModuleID = 10009
		AND ExceptionTemplateID IN (2017, 2018, 2019, 2020, 2021, 2022, 2023, 2043)
GO

	
	