--Corporate request reports
UPDATE UMPermission SET  UMPermissionRole='CRPassengerItinerary' WHERE ModuleID=10004 AND UMPermissionID = 10035

--Charter quote reports
UPDATE UMPermission SET  UMPermissionRole='QuoteLog' WHERE ModuleID=10002 AND UMPermissionID = 10024
UPDATE UMPermission SET  UMPermissionRole='CQPaxProfile' WHERE ModuleID=10002 AND UMPermissionID = 10022
