
UPDATE TFL1 SET TFL1.SequenceOrder = TFL2.NewSequanceNo
from TSFlightLog TFL1
inner join
(
	select ROW_NUMBER() over(PARTITION BY CustomerID,HomebaseID ORDER BY CustomerID,HomebaseID,SequenceOrder) NewSequanceNo
	,* from TSFlightLog 
	where isnull(IsDeleted,0) = 0 
		  AND Category= 'FLIGHT LOG' 
		  AND SequenceOrder > 0
)TFL2 ON TFL1.TSFlightLogID = TFL2.TSFlightLogID 