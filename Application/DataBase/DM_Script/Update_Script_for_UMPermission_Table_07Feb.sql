--Defect# 2004, The Edit option for Setting has different names listed.
--Script to fix the inconsistencies in naming 
UPDATE UMPermission SET UMPermissionName = 'Locator' WHERE UMPermissionName= 'Universal Locator'
--Defect# 2011
UPDATE UMPermission SET UMPermissionRole = 'UtilAirport' WHERE UMPermissionName = 'Airport Table' AND ModuleID=10019