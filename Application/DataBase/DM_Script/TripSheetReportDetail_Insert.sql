DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 


  WHILE @@FETCH_STATUS = 0
	 BEGIN
	 
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','ATISFREQ','ATIS FREQUENCY','L',0,1,0,2,0) 


EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','DEPATIS','DEPARTURE ATIS FREQUENCY','L',0,1,0,2,0)
                      
 EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
 
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','ARRATIS','ARRIVAL ATIS FREQUENCY','L',0,1,0,2,0)

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
                      
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CLRNCE','CLEARANCE','L',0,1,0,2,0)
                      
 EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
 
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWADLINFO','CREW ADDITONAL INFORMATION','L',0,1,0,2,0)
                      
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWPHONE','CREW ADDITONAL PHONE','L',0,1,0,2,0)
                      

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWDOB','CREW DOB','L',0,1,0,2,0)

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWPNUM','CREW PASSPORT NUM','L',0,1,0,2,0)
                      

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWNATION','CREW NATIONALITY','L',0,1,0,2,0)
                      
 
 EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
 
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWPDATE','CREW PASSPORT EXPIRATION DATE','L',0,1,0,2,0)
                      
 EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
 
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CRWVISA','CREW VISA','L',0,1,0,2,0)
                      
 
 EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
 
 INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ParameterValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','PAXREQDOC','PAX REQUESTORS DOCUMENT','N',0,1,0,'1,2,3',2,0)
                      
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','LEGSEP','LEG NOTES SEPARATE','L',0,1,0,2,0)
                      

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CQNAME','CHARTER QUOTE CUSTOMER NAME','L',0,1,0,2,0)
                      
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','CQCONTACT','CHARTER QUOTE MAIN CONTACT NUMBER','L',0,1,0,2,0)
                       
	 

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
	
 END
	CLOSE C1;
	DEALLOCATE C1;
	
--UPDATE flightpaksequence SET MasterModuleCurrentNo = 166277  where customerid = 10011
--select max(TripSheetReportDetailID) from TripSheetReportDetail where customerid = 10011
--SELECT * FROM flightpaksequence   where customerid = 10011