
DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 


  WHILE @@FETCH_STATUS = 0
	 BEGIN

------------------------------TRIPSHEET IN OUTLOOK-----------------------------------------------------------
		EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
		
		INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
			   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
			   ParameterLValue,ParameterNValue,ReportOrder,
			   ParameterValue, ParameterSize, WarningMessage)
		  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,17,'TRIPSHEET IN OUTLOOK',1,1,'null','null',
			'','N',1,0,1,2,
			'1,2,3,4,5', '1-5', 'Entering $d$ is not valid entry.Please enter value between 1 and 5')   

			
FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
	
 END
CLOSE C1;
DEALLOCATE C1;
                               