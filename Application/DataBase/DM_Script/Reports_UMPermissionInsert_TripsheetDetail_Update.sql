
-----Charter Quote Description change for Name and Phone in Tripsheet and Itinerary---
UPDATE TripSheetReportDetail SET ParameterDescription='HEADER ITEMS - CHARTER QUOTE CUSTOMER NAME'
                             WHERE ReportNumber=2
                               AND ParameterVariable='CQNAME'
                               
UPDATE TripSheetReportDetail SET ParameterDescription='HEADER ITEMS - CHARTER QUOTE MAIN CONTACT NUMBER'
                             WHERE ReportNumber=2
                               AND ParameterVariable='CQCONTACT'
                               
UPDATE TripSheetReportDetail SET ParameterDescription='HEADER ITEMS - CHARTER QUOTE CUSTOMER NAME'
                             WHERE ReportNumber=3
                               AND ParameterVariable='CQNAME'
                               
UPDATE TripSheetReportDetail SET ParameterDescription='HEADER ITEMS - CHARTER QUOTE MAIN CONTACT NUMBER'
                             WHERE ReportNumber=3
                               AND ParameterVariable='CQCONTACT' 