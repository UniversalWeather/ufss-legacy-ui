
----FSS-2414 --
DELETE FROM TripSheetReportDetail WHERE ReportNumber=13 AND ParameterVariable='PRIMARYMOB'
DELETE FROM TripSheetReportDetail WHERE ReportNumber=13 AND ParameterVariable='BUSPHONE'

DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 

WHILE @@FETCH_STATUS = 0
BEGIN

	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT
		 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
			   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
	VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,
			'RPTTS13','PRIMARYMOB','REQUESTOR PRIMARY MOBILE/CELL','L',0,1,0,3,0) 

	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT

	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
				ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
	VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,
				'RPTTS13','BUSPHONE','REQUESTOR BUSINESS PHONE','L',0,0,0,3,0) 
		  
FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
 
END
	
CLOSE C1;
DEALLOCATE C1;


