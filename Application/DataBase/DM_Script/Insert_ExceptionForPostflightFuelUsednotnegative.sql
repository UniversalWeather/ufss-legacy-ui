USE [FP_DEV_Dev2]
GO

IF (Select top 1 ExceptionTemplateID from ExceptionTemplate Where ModuleID=10009 And SubmoduleID=2 And ExceptionTemplateID=2052) Is null
BEGIN

	INSERT INTO [dbo].[ExceptionTemplate]([ExceptionTemplateID],[ModuleID],[SubModuleID],[ExceptionTemplate],[Severity],[IsInActive])
	VALUES (2052,10009,2,'Fuel Used cannot be a negative value',1,null)

END
GO
