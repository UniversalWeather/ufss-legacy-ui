
---Update Statements for Custom Pilot Log to fix 2809
-- Created by : Karthikeyan.S
-- Created Date : 24/03/2014
update TSFlightLog set OriginalDescription = 'Appr  Npr',CustomDescription = 'Appr  Npr' where OriginalDescription = 'Approach Non-Precision' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Appr  Pr',CustomDescription = 'Appr  Pr' where OriginalDescription = 'Approach Precision' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Arr. ICAO',CustomDescription = 'Arr. ICAO' where OriginalDescription = 'Arrival ICAO' and Category = 'PILOT LOG'


update TSFlightLog set OriginalDescription = 'Blk In',CustomDescription = 'Blk In' where OriginalDescription = 'Block In' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Blk Out',CustomDescription = 'Blk Out' where OriginalDescription = 'Block Out' and Category = 'PILOT LOG'


update TSFlightLog set OriginalDescription = 'Dep. ICAO',CustomDescription = 'Dep. ICAO' where OriginalDescription = 'Departure ICAO' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Flt Cat',CustomDescription = 'Flt Cat' where OriginalDescription = 'Flight Category' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Flt Typ',CustomDescription = 'Flt Typ' where OriginalDescription = 'Flight Type' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Inst Time',CustomDescription = 'Inst Time' where OriginalDescription = 'Instrument Time' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Lnding Dt',CustomDescription = 'Lnding Dt' where OriginalDescription = 'Landing Day' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'Lnding Nt',CustomDescription = 'Lnding Nt' where OriginalDescription = 'Landing Night' and Category = 'PILOT LOG'



update TSFlightLog set OriginalDescription = 'Nite Time',CustomDescription = 'Nite Time' where OriginalDescription = 'Night Time' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'P S',CustomDescription = 'P S' where OriginalDescription = 'PIC/SIC' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'T/OFF Dt',CustomDescription = 'T/OFF Dt' where OriginalDescription = 'Takeoff Day' and Category = 'PILOT LOG'

update TSFlightLog set OriginalDescription = 'T/OFF Nt',CustomDescription = 'T/OFF Nt' where OriginalDescription = 'Takeoff Night' and Category = 'PILOT LOG'

