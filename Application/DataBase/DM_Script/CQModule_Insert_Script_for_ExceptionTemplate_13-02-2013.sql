
--// Script in Exception Template for Charter Quote
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3000,10001,1,'Total Quote <MainTotalQuote> Is Over the Customer''s Credit Limit of <FileTotalQuote>.', 2) 
GO

insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3001,10001,2,'Departure Airport Inactive.', 2) 
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3002,10001,2,'Arrival Airport Inactive.', 2) 
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3003,10001,2,'Leg <legnum1> Arrival Date/Time (Local) Overlaps Next Leg <legnum2> Departure Date/Time.', 2)
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3004,10001,2,'Leg <legnum1> Departure Date/Time (Local) Overlaps PreviousLeg <legnum2> Arrival Date/Time.', 2) 
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3005,10001,2,'Distance Beyond Aircraft Capability.', 2) 
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3006,10001,2,'Arrival airport minimum runway length below aircraft requirements.', 2) 
GO
insert into ExceptionTemplate 
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity) 
values (3007,10001,2,'Time Aloft Beyond Aircraft Capability.', 2)
GO

insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3008,10001,0,'Homebase is Required.',	1)
GO
insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3009,10001,1,'Vendor Code is Required.',	1)
GO
insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3010,10001,1,'Tail Number is Required.',	1)
GO
insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3011,10001,1,'Type Code is Required.',1)
GO
insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3012,10001,2,'Depart Icao Id is Required.',1)
GO
insert into ExceptionTemplate
(ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity)
values (3013,10001,2,'Arrival Icao Id is Required.',1)
GO