-- Default Exception Template Records --


INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (1034,10011,1,'Department is InActive.',2)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (1035,10011,1,'Authorization is InActive',2)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (1036,10011,2,'Leg <legnum> Department is InActive.',2)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (1037,10011,2,'Leg <legnum> Authorization is InActive.',2)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (3014,10001,0,'Depart Date is Required.',1)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (3015,10001,0,'Quote Date is Required.',1)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (3016,10001,2,'Depart Date is Required.',1)

INSERT INTO ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) VALUES (3017,10001,2,'Arrival Date is Required.',1)