UPDATE AircraftDuty SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE Budget SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE Aircraft SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE Client SET IsInActive = 0 WHERE IsInActive IS NULL

UPDATE CrewDutyRules SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE CrewDutyType SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE CustomAddress SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE DelayType SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE CrewCheckList SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE EmergencyContact SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE ExchangeRate SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE FlightPurpose SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE FuelVendor SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE PaymentType SET IsInActive = 0 WHERE IsInActive IS NULL

UPDATE FeeGroup SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE LeadSource SET IsInActive = 0 WHERE IsInActive IS NULL

UPDATE DepartmentGroup SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE FleetGroup SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE PassengerGroup SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE TravelCoordinator SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE TripManagerCheckListGroup SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE TripManagerCheckList SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE FareLevel SET IsInActive = 0 WHERE IsInActive IS NULL
UPDATE SalesPerson SET IsInActive = 0 WHERE IsInActive IS NULL
Update FuelLocator SET IsInActive = 0 WHERE IsInActive IS NULL