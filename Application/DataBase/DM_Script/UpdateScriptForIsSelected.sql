UPDATE TripSheetReportDetail
      SET    ISSELECTED = 0
      FROM   TripSheetReportDetail TMP
             INNER JOIN (SELECT DISTINCT TripSheetReportHeaderID,CustomerID,ReportNumber FROM dbo.TripSheetReportDetail WHERE IsSelected=0) T1 ON T1.TripSheetReportHeaderID = TMP.TripSheetReportHeaderID AND T1.CustomerID=TMP.CustomerID AND T1.ReportNumber=TMP.ReportNumber
             INNER JOIN (SELECT DISTINCT TripSheetReportHeaderID,CustomerID,ReportNumber FROM dbo.TripSheetReportDetail WHERE IsSelected=1) T2 ON T1.TripSheetReportHeaderID = T2.TripSheetReportHeaderID AND T1.CustomerID=T2.CustomerID AND T1.ReportNumber=T2.ReportNumber


