use fss_dev
go

begin

/*
    The script was written to add missing rows in the TSFlightLog based on the homebaseId
*/
declare @destinationHomeBase bigint

declare @maxTSRow int
declare @TSRowNum int
declare @sourceCustID bigint
declare @destinationCustID bigint
declare @desc varchar(50)
declare @category varchar(10) 
declare @isInActive bit
declare @custDesc varchar(50)
declare @sequence varchar(50)
declare @isPrint bit
declare @date datetime
declare @maxTSId bigint
declare @maxTSPrime int
declare @LogID bigint
declare @newsql varchar(2000)
declare @lastUpID varchar(30)
declare @sourceHomeBasId bigint

set nocount on

--Set the source customer from where we need to pull the template data for the queries
set @sourceCustID = -1
--Set the source customer's homebase id which contains records we would like to use for the new customer or homebase
set @sourceHomeBasId = -1
--set customerID for the customer to be processed
set @destinationCustID = -1
-- set @destinationHomeBase
set @destinationHomeBase= -1

--  get a list of homebase's that need to be added to TSFlightLog Table
SELECT Company.HomebaseID as CompanyHomeBase, Company.LastUpdUID  
INTO #list1         
FROM Company  
WHERE Company.CustomerID = @destinationCustID and Company.LastUpdUID  is not null and Company.LastUpdUID != ''

/*
 Get a list that will be used to add the messing homebases to the TSFlightLog Table
 this will be base on customer @sourceCustID homebaseID @sourceHomeBasId so that this script can 
 be used for other customers if needed.   This will be a standard static list.
*/
SELECT *  INTO #tempTSFlightLog  FROM TSFlightLog 
WHERE CustomerID = @sourceCustID and HomebaseID = @sourceHomeBasId and 
OriginalDescription NOT IN (Select OriginalDescription from TSFlightLog 
WHERE CustomerID = @destinationCustID and HomebaseID = @destinationHomeBase)


set @maxTSRow = (select COUNT(#tempTSFlightLog.primeKey) from #tempTSFlightLog)
set @maxTSId = (select MAX(TSFlightLogID) from TSFlightLog where CustomerID = @destinationCustID)
set @maxTSPrime = (select MAX(PrimeKey) from TSFlightLog where CustomerID = @destinationCustID)

select top 1  @lastUpID = LastUpdUID from #list1 

BEGIN
  -- get the home base

  set @LogID = 0
  set @TSRowNum = 0
      
  -- Start adding missing rows to TSFlightLog table
  WHILE @TSRowNum < @maxTSRow 
  BEGIN
      	  SET @TSRowNum = @TSRowNum + 1
      	  -- get row from temp table that needs to be added to TSFlightLog table
		  SELECT TOP 1 @LogID=TSFlightLogID, @desc = OriginalDescription,
				       @custDesc = CustomDescription, @sequence = SequenceOrder, 
				       @isPrint = IsPrint,@category=Category, @isInActive=IsInActive, 
				       @date=LastUpdTS
     	  FROM #tempTSFlightLog
          WHERE TSFlightLogID > @LogID
      
		  SET @maxTSId = @maxTSId + 1
          SET @maxTSPrime = @maxTSPrime + 1
    
          --Insert record into TSFlightLot table
         set @newsql ='INSERT INTO TSFlightLog (TSFlightLogID, CustomerID, PrimeKey, HomebaseID,OriginalDescription, CustomDescription, isDeleted,SequenceOrder, IsPrint, LastUpdUID, LastUpdTS, Category,IsInActive)
          VALUES (' + cast(@maxTSId as varchar(40)) + ',' + cast(@destinationCustID as varchar(40)) + ',' + 
		          cast(@maxTSPrime  as varchar(40)) + ',' + cast(@destinationHomeBase  as varchar(40))+
		          ',' + char(39) + @desc + char(39) + ',' + char(39) +   @custDesc + char(39) + ',0' + 
		          ',' + cast(@sequence  as varchar(1)) + ',' +  cast(@isPrint as varchar(1)) + ',' + 
                  char(39)+@lastUpID+char(39)+',' + char(39)+cast(@date as varchar(40))+char(39) + ',' + char(39) + 
				   @category + char(39) + ',0)' 

		 PRINT @newsql
		 PRINT 'GO'
      END
END

drop table #tempTSFlightLog
drop table #list1

end
GO