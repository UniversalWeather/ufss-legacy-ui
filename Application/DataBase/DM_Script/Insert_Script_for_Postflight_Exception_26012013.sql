
-- Script for Showing Warning message, when selecting Inactive Department and Authorization Records.

insert into ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) values 
(2046,10009,1,'Department is InActive.',2) 
insert into ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) values 
(2047,10009,1,'Authorization is InActive',2) 
insert into ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) values 
(2048,10009,2,'Department is InActive.',2) 
insert into ExceptionTemplate (ExceptionTemplateID,ModuleID,SubModuleID,ExceptionTemplate,Severity) values 
(2049,10009,2,'Authorization is InActive.',2) 