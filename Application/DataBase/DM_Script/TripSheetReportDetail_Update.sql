ALTER TABLE TripSheetReportDetail ALTER COLUMN ParameterDescription VARCHAR(150) NULL
GO

-----------------------------------TripSheet Start-----------------------------------------------------------
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='ADDITIONAL CREW HOTEL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='MAINTCOMM'
                               AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING ARRIVAL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CACOMMENT'
                             AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING DEPARTURE - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CDCOMMENT'
                             AND ReportNumber=2

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW HOTEL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CREWHCOMM'
                             AND ReportNumber=2
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT ARRIVAL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTACOMMENT' 
                             AND ReportNumber=2                               


UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT DEPARTURE - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTDCOMMENT'
                             AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='FBO - ARRIVAL COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='FBOACOMM'
                             AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='FBO - DEPARTURE COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='FBODCOMM'
                             AND ReportNumber=2
                               
                               

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX HOTEL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='HOTELCOMM'
                             AND ReportNumber=2
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX TRANSPORT ARRIVAL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PTACOMMENT'
                             AND ReportNumber=2  
                                                           

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX TRANSPORT DEPARTURE - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PTDCOMMENT'
                             AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='AIRCRAFT NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='ACNOTES'
                             AND ReportNumber=2
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='GENERAL NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='GENNOTES'
                             AND ReportNumber=2
                               
                               

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='LEG ITEMS - AIRPORT NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='NOTES'
                             AND ReportNumber=2
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3',ParameterSize='1-3',
         ParameterDescription ='PAX NOTES - 1=SepPG,2=Aft.Ea.Leg,3=NONE',
         WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 3'
     WHERE ParameterVariable='PAXNOTES'
     AND ReportNumber=2   
-----------------------------------TripSheet End-----------------------------------------------------------

-----------------------------------Itinerary Start-----------------------------------------------------------

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING ARRIVAL - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CACOMMENT'
                             AND ReportNumber=3  
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING DEPARTURE - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CDCOMMENT'
                             AND ReportNumber=3  
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW HOTEL - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CREWHCOMM'
                             AND ReportNumber=3  
                               
                               

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT ARRIVAL - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTACOMMENT'
                             AND ReportNumber=3  
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT DEPARTURE - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTDCOMMENT'   
                             AND ReportNumber=3  
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING ARRIVAL - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CACOMMENT'
                             AND ReportNumber=3  
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='FBO - ARRIVAL COMMENTS- 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='FBOACOMM'
                             AND ReportNumber=3  
                             

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='FBO - DEPARTURE COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='FBODCOMM'
                             AND ReportNumber=3  
                               
                               

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='MAINT HOTEL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='MAINTCOMM'
                             AND ReportNumber=3  
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX HOTEL - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='HOTELCOMM'   
                             AND ReportNumber=3   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX TRANSPORT DEPARTURE - COMMENT - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PTDCOMMENT'
                             AND ReportNumber=3  
/*                            
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
         ParameterDescription ='LEG NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
         WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
     WHERE ParameterVariable='LEGNOTES'   
     AND ReportNumber=3  
*/      
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4',ParameterSize='1-4',
         ParameterDescription ='LEG NOTES - 1=PAX,2=CRW,3=BOTH,4=NONE',
         WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 4'
     WHERE ParameterVariable='LEGNOTES'   
     AND ReportNumber=3                               
                             
 
-----------------------------------Itinereay End-----------------------------------------------------------  


-----------------------------------Checklist Start-----------------------------------------------------------  

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='ADDITIONAL CREW HOTEL - COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='MAINTCOMM'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING ARRIVAL-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CACOMMENT'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CATERING DEPARTURE-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CDCOMMENT'   
                             AND ReportNumber=12
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW HOTEL-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CREWHCOMM'   
                             AND ReportNumber=12   

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT ARRIVAL-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTACOMMENT'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='CREW TRANSPORT DEPARTURE-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='CTDCOMMENT'   
                             AND ReportNumber=12  
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX HOTEL-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='HOTELCOMM'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX TRANSPORT ARRIVAL-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PTACOMMENT'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAX TRANSPORT DEPARTURE-COMMENTS - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PTDCOMMENT'   
                             AND ReportNumber=12   

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='AIRPORT NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='NOTES'   
                             AND ReportNumber=12   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='LEG NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='LEG'   
                             AND ReportNumber=12          
-----------------------------------Checklist End----------------------------------------------------------- 

-----------------------------------Summary Start-----------------------------------------------------------
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='AIRPORT NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='ANOTES'   
                             AND ReportNumber=13   
                             
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='LEGNOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='LEGNOTES'   
                             AND ReportNumber=13  

-----------------------------------Summary End----------------------------------------------------------- 


-----------------------------------Overview Start-----------------------------------------------------------

UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='GENERAL NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='GENNOTES'   
                             AND ReportNumber=1

-----------------------------------Overview Start-----------------------------------------------------------


-----------------------------------PaxProfile Start-----------------------------------------------------------
UPDATE TripSheetReportDetail SET ParameterType='N',ParameterNValue=2,ParameterValue='1,2,3,4,5,6',ParameterSize='1-6',
                                 ParameterDescription ='PAXNOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL',
                                 WarningMessage='Entering $d$ is not valid entry.Please enter value between 1 and 6'
                             WHERE ParameterVariable='PAXNOTES'   
                             AND ReportNumber=5

-----------------------------------PaxProfile End-----------------------------------------------------------

UPDATE TripSheetReportDetail SET ParameterDescription='LEG NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL'
                             WHERE ParameterVariable='BOLDNOTES'
                               AND ReportNumber=2
                               AND ReportDescription='TRIPSHEET'
                             
UPDATE TripSheetReportDetail SET ParameterDescription='LEG NOTES ON SEPARATE PAGE'
                             WHERE ParameterVariable='LEGSEP'
                               AND ReportNumber=2	

UPDATE TripSheetReportDetail SET IsDeleted = 1
                             WHERE ParameterVariable='CRWPHONE' AND ReportNumber = 2
UPDATE TripSheetReportDetail SET IsDeleted = 1
                             WHERE ParameterVariable='CRWVISA' AND ReportNumber = 2	
                             
/*                                                              
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CRWPHONE'
                                    AND ParameterDescription='CREW - ADDITONAL PHONE'
                                    
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CRWVISA'
                                    AND ParameterDescription='CREW - VISA'

*/

DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 


  WHILE @@FETCH_STATUS = 0
	 BEGIN
-------------------------------ITINERARY-----------------------------------------------------------


  
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                           ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                           ParameterLValue,ParameterNValue,ReportOrder
                         )
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,3,'ITINERARY',1,1,'RPTTS03','LEGSEP','LEG NOTES ON SEPARATE PAGE','N',1,0,2,2)   

EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,3,'ITINERARY',1,1,'RPTTS03','LEGNOTESF',
						'LEG NOTES - 1=NO,2=NORMAL,3=BOLD,4=ITALIC,5=UNDERLINE,6=ALL','N',1,0,2,2
						,'1,2,3,4,5,6', '1-6', 'Entering $d$ is not valid entry.Please enter value between 1 and 6')   

------------------------------TRIPSHEET-----------------------------------------------------------
		EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
		
		INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
			   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
			   ParameterLValue,ParameterNValue,ReportOrder,
			   ParameterValue, ParameterSize, WarningMessage)
		  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','PAXNOTESF',
			'PAX NOTES - 1=NORMAL,2=BOLD,3=ITALIC,4=UNDERLINE,5=ALL','N',1,0,1,2,
			'1,2,3,4,5', '1-5', 'Entering $d$ is not valid entry.Please enter value between 1 and 5')   

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
	
 END
CLOSE C1;
DEALLOCATE C1;
                               

/*

DELETE FROM TripSheetReportDetail WHERE ParameterVariable='MAINTCOMMF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CACOMMENTF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CDCOMMENTF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CREWHCOMMF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTACOMMENF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTDCOMMENF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='FBOACOMMF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='FBODCOMMF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='HOTELCOMMF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PTACOMMENF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PTDCOMMENF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='ACNOTESF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='GENNOTESF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='NOTESF' AND ReportNumber=2
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PAXNOTESF' AND ReportNumber=2

DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CACOMMENTF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CDCOMMENTF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CREWHCOMMF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTACOMMENF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTDCOMMENF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='FBOACOMMF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='FBODCOMMF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='MAINTCOMMF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='HOTELCOMMF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PTDCOMMENF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='LEGNOTESF' AND ReportNumber=3
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='LEGSEP' AND ReportNumber=3

DELETE FROM TripSheetReportDetail WHERE ParameterVariable='MAINTCOMMF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CACOMMENTF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CDCOMMENTF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CREWHCOMMF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTACOMMENF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='CTDCOMMENF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='HOTELCOMMF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PTACOMMENF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PTDCOMMENF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='NOTESF' AND ReportNumber=12
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='LEGF' AND ReportNumber=12

DELETE FROM TripSheetReportDetail WHERE ParameterVariable='ANOTESF' AND ReportNumber=13
DELETE FROM TripSheetReportDetail WHERE ParameterVariable='LEGNOTESF' AND ReportNumber=13

DELETE FROM TripSheetReportDetail WHERE ParameterVariable='GENNOTESF' AND ReportNumber=1


DELETE FROM TripSheetReportDetail WHERE ParameterVariable='PAXNOTESF' AND ReportNumber=5

*/