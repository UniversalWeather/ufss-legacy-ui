Update dbo.PreflightAdvancedFilters Set Settings='<AdvancedFilterSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Filters>
    <TimeBase>Local</TimeBase>
    <Client />
    <Requestor />
    <Department />
    <FlightCategory />
    <CrewDuty />
    <HomeBase />
    <HomeBaseID />
    <Trip>true</Trip>
    <WorkSheet>false</WorkSheet>
    <SchedServ>false</SchedServ>
    <Canceled>false</Canceled>
    <UnFulfilled>false</UnFulfilled>
    <Hold>true</Hold>
    <AllCrew>true</AllCrew>
    <HomeBaseOnly>false</HomeBaseOnly>
    <FixedWingCrewOnly>false</FixedWingCrewOnly>
    <RotaryWingCrewCrewOnly>false</RotaryWingCrewCrewOnly>
    <VendorsFilter>IncludeActiveVendors</VendorsFilter>
	<ShowMultipleLegs>false</ShowMultipleLegs>
  </Filters>
  <Display>
    <Planner>
      <PlannerDisplayOption>Both</PlannerDisplayOption>
      <AircraftColors>false</AircraftColors>
      <NonReversedColors>false</NonReversedColors>
      <ActiveOnly>false</ActiveOnly>
      <BlackWhiteColor>false</BlackWhiteColor>
    </Planner>
    <WeeklyFleet>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>true</LegPurpose>
      <SeatsAvailable>true</SeatsAvailable>
      <PaxCount>true</PaxCount>
      <Requestor>true</Requestor>
      <Department>true</Department>
      <Authorization>true</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>false</CummulativeETE>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <ShowTrip>true</ShowTrip>
      <DisplayRedEyeFlightsInContinuation>false</DisplayRedEyeFlightsInContinuation>
      <ActivityOnly>false</ActivityOnly>
      <SaveColumnWidths>false</SaveColumnWidths>
      <NoLineSeparator>true</NoLineSeparator>
    </WeeklyFleet>
    <WeeklyCrew>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>true</LegPurpose>
      <SeatsAvailable>true</SeatsAvailable>
      <PaxCount>true</PaxCount>
      <Requestor>true</Requestor>
      <Department>true</Department>
      <Authorization>true</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>false</CummulativeETE>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <ShowTrip>true</ShowTrip>
      <DisplayRedEyeFlightsInContinuation>false</DisplayRedEyeFlightsInContinuation>
      <ActivityOnly>false</ActivityOnly>
      <SaveColumnWidths>false</SaveColumnWidths>
      <NoLineSeparator>false</NoLineSeparator>
    </WeeklyCrew>
    <Monthly>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>true</LegPurpose>
      <SeatsAvailable>true</SeatsAvailable>
      <PaxCount>true</PaxCount>
      <Requestor>true</Requestor>
      <Department>true</Department>
      <Authorization>true</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>false</CummulativeETE>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <ShowTrip>true</ShowTrip>
      <DisplayRedEyeFlightsInContinuation>false</DisplayRedEyeFlightsInContinuation>
      <CrewCalActivity>false</CrewCalActivity>
    </Monthly>
    <Weekly>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>true</LegPurpose>
      <SeatsAvailable>true</SeatsAvailable>
      <PaxCount>true</PaxCount>
      <Requestor>true</Requestor>
      <Department>true</Department>
      <Authorization>true</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>false</CummulativeETE>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <ShowTrip>true</ShowTrip>
      <DisplayRedEyeFlightsInContinuation>false</DisplayRedEyeFlightsInContinuation>
      <CrewCalActivity>false</CrewCalActivity>
    </Weekly>
    <BusinessWeek>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>true</LegPurpose>
      <SeatsAvailable>true</SeatsAvailable>
      <PaxCount>true</PaxCount>
      <Requestor>true</Requestor>
      <Department>true</Department>
      <Authorization>true</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>false</CummulativeETE>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <ShowTrip>true</ShowTrip>
      <DisplayRedEyeFlightsInContinuation>false</DisplayRedEyeFlightsInContinuation>
      <CrewCalActivity>false</CrewCalActivity>
      <SaveColumnWidhts>false</SaveColumnWidhts>
    </BusinessWeek>
    <Day>
      <AircraftColors>false</AircraftColors>
      <ActivityOnly>true</ActivityOnly>
      <BlackWhiteColor>false</BlackWhiteColor>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <ShowTripStatus>false</ShowTripStatus>
      <SaveColumnWidhts>false</SaveColumnWidhts>
      <CummulativeETE>false</CummulativeETE>
      <CrewCalActivity>false</CrewCalActivity>
      <DayWeeklyDetailSortOption>TailNumber</DayWeeklyDetailSortOption>
    </Day>
    <Corporate>
      <AircraftColors>false</AircraftColors>
      <BlackWhiteColor>false</BlackWhiteColor>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <ShowTripStatus>false</ShowTripStatus>
      <SaveColumnWidhts>false</SaveColumnWidhts>
      <NoLineSeparator>false</NoLineSeparator>
    </Corporate>
    <WeeklyDetail>
      <DepartArriveInfo>ICAO</DepartArriveInfo>
      <FirstLastLeg>false</FirstLastLeg>
      <Crew>true</Crew>
      <ShowTripStatus>false</ShowTripStatus>
      <FlightNumber>false</FlightNumber>
      <LegPurpose>false</LegPurpose>
      <SeatsAvailable>false</SeatsAvailable>
      <PaxCount>false</PaxCount>
      <Requestor>false</Requestor>
      <Department>false</Department>
      <Authorization>false</Authorization>
      <FlightCategory>true</FlightCategory>
      <CummulativeETE>true</CummulativeETE>
      <ActivityOnly>false</ActivityOnly>
      <SaveColumnWidhts>false</SaveColumnWidhts>
      <NoLineSeparator>false</NoLineSeparator>
      <DayWeeklyDetailSortOption>TailNumber</DayWeeklyDetailSortOption>
      <PaxFullName>false</PaxFullName>
      <ShowTripNumber>false</ShowTripNumber>
      <CrewFullName>false</CrewFullName>
      <Pax>true</Pax>
    </WeeklyDetail>
  </Display>
</AdvancedFilterSettings>'
Where UserName is null and CustomerID is null
