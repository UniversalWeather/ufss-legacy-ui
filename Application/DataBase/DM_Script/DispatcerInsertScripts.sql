
 -------------------------------Dispatcher ----------------------    
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=1 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=12 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=13 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=8 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=5 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=6 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=4 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=11 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=2 AND ParameterVariable='DISPATCHIN'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=3 AND ParameterVariable='DISPATCHIN'

-------------------------------DISPATCHER EMAIL ------------
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=1 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=12 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=13 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=8 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=5 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=6 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=4 AND ParameterVariable='DSPTEMAIL'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=11 AND ParameterVariable='DSPTEMAIL'

-------------------------------DISPATCHER NAME------------ 
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=1 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=12 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=13 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=8 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=5 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=6 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=4 AND ParameterVariable='DISPATCHER'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=11 AND ParameterVariable='DISPATCHER'

-------------------------------DISPATCHER PHONE------------ 
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=1 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=12 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=13 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=8 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=5 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=6 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=4 AND ParameterVariable='DSPTPHONE'
DELETE FROM  TripSheetReportDetail WHERE  ReportNumber=11 AND ParameterVariable='DSPTPHONE'

DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 


  WHILE @@FETCH_STATUS = 0
	 BEGIN



                      
      -------------------------------Dispatcher Start----------------------   
      	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT              
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,1,'OVERVIEW',1,1,'RPTTS01','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,1
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2')  

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 						
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,12,'TRIP CHECKLIST',1,1,'RPTTS12','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,13
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 						
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,'RPTTS13','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,3
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2')  

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 						
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,8,'TRIPSHEET ALERTS',1,1,'RPTTS08','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,9
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 

 
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,5,'PAX PROFILE',1,1,'RPTTS05','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,6
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2')  
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,6,'INTERNATIONAL DATA',1,1,'RPTTS06','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,7
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,4,'FLIGHT LOG',1,1,'RPTTS04','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,5
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2')  
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,11,'CREW DUTY OVERVIEW',1,1,'RPTTS11','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,12
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 

	
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,2
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder,
                                   ParameterValue, ParameterSize, WarningMessage)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,3,'ITINERARY',1,1,'RPTTS03','DISPATCHIN',
						'DISPATCHER-INFORMATION - 1=TOP, 2=BOTTOM','N',1,0,0,4
						,'1,2', '1-2', 'Entering $d$ is not valid entry.Please enter value between 1 and 2') 	
		-------------------------------Dispatcher End---------------------- 	
		
		      -------------------------------DISPATCHER EMAIL Start----------------------                

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,1,'OVERVIEW',1,1,'RPTTS01','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,1) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,12,'TRIP CHECKLIST',1,1,'RPTTS12','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,13) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,'RPTTS13','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,3) 

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,8,'TRIPSHEET ALERTS',1,1,'RPTTS08','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,9) 

 
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,5,'PAX PROFILE',1,1,'RPTTS05','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,6) 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,6,'INTERNATIONAL DATA',1,1,'RPTTS06','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,7) 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,4,'FLIGHT LOG',1,1,'RPTTS04','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,5) 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,11,'CREW DUTY OVERVIEW',1,1,'RPTTS11','DSPTEMAIL','DISPATCHER EMAIL','L',0,1,0,12)  
		
		-------------------------------DISPATCHER EMAIL End---------------------- 
					                    
		      ------------------------------- DISPATCHER NAME  Start----------------------                
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,1,'OVERVIEW',1,1,'RPTTS01','DISPATCHER','DISPATCHER NAME','L',0,1,0,1) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,12,'TRIP CHECKLIST',1,1,'RPTTS12','DISPATCHER','DISPATCHER NAME','L',0,1,0,13) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,'RPTTS13','DISPATCHER','DISPATCHER NAME','L',0,1,0,3) 

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,8,'TRIPSHEET ALERTS',1,1,'RPTTS08','DISPATCHER','DISPATCHER NAME','L',0,1,0,9) 

 
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,5,'PAX PROFILE',1,1,'RPTTS05','DISPATCHER','DISPATCHER NAME','L',0,1,0,6) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,6,'INTERNATIONAL DATA',1,1,'RPTTS06','DISPATCHER','DISPATCHER NAME','L',0,1,0,7) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,4,'FLIGHT LOG',1,1,'RPTTS04','DISPATCHER','DISPATCHER NAME','L',0,1,0,5) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,11,'CREW DUTY OVERVIEW',1,1,'RPTTS11','DISPATCHER','DISPATCHER NAME','L',0,1,0,12)  
		
		------------------------------- DISPATCHER NAME  End---------------------- 


		      ------------------------------- DISPATCHER PHONE  Start----------------------                

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,1,'OVERVIEW',1,1,'RPTTS01','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,1) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,12,'TRIP CHECKLIST',1,1,'RPTTS12','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,13) 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,13,'TRIPSHEET SUMMARY',1,1,'RPTTS13','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,3) 

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,8,'TRIPSHEET ALERTS',1,1,'RPTTS08','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,9) 

 
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,5,'PAX PROFILE',1,1,'RPTTS05','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,6) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,6,'INTERNATIONAL DATA',1,1,'RPTTS06','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,7) 
						
	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,4,'FLIGHT LOG',1,1,'RPTTS04','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,5) 
						

	 
EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
                                   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
                                   ParameterLValue,ParameterNValue,ReportOrder)
                      VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,11,'CREW DUTY OVERVIEW',1,1,'RPTTS11','DSPTPHONE','DISPATCHER PHONE','L',0,1,0,12)  
		
		------------------------------- DISPATCHER PHONE  End---------------------- 

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
	
 END
	CLOSE C1;
	DEALLOCATE C1;
	
--------------------------------UPDATE START----------------------------------------------------------

--UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCH NUMBER' WHERE ReportNumber=3 AND ParameterVariable='DISPATCHNO'
--UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCH NUMBER' WHERE ReportNumber=2 AND ParameterVariable='DISPATCHNO'


UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER EMAIL' WHERE ReportNumber=3 AND ParameterVariable='DSPTEMAIL'
UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER EMAIL' WHERE ReportNumber=2 AND ParameterVariable='DSPTEMAIL'

UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER NAME' WHERE ReportNumber=3 AND ParameterVariable='DISPATCHER'
UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER NAME' WHERE ReportNumber=2 AND ParameterVariable='DISPATCHER'

UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER PHONE' WHERE ReportNumber=3 AND ParameterVariable='DSPTPHONE'
UPDATE  TripSheetReportDetail SET ParameterDescription='DISPATCHER PHONE' WHERE ReportNumber=2 AND ParameterVariable='DSPTPHONE'

--------------------------------UPDATE END----------------------------------------------------------

	

	
	
--UPDATE flightpaksequence SET MasterModuleCurrentNo = 166277  where customerid = 10011
--select max(TripSheetReportDetailID) from TripSheetReportDetail where customerid = 10011
--SELECT * FROM flightpaksequence   where customerid = 10011

