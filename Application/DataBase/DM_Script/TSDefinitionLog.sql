update TSDefinitionLog set CustomDescription = 'Approach Non-Precision' where CustomDescription = 'Appr  Npr' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Approach Precision' where CustomDescription = 'Appr  Pr' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Arrival ICAO' where CustomDescription = 'Arr. ICAO' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Block In' where CustomDescription = 'Blk In' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Block Out' where CustomDescription = 'Blk Out' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Departure ICAO' where CustomDescription = 'Dep. ICAO' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Flight Category' where CustomDescription = 'Flt Cat' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Flight Type' where CustomDescription = 'Flt Typ' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Instrument Time' where CustomDescription = 'Inst Time' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Landing Day' where CustomDescription = 'Lnding Dt' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Landing Night' where CustomDescription = 'Lnding Nt' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Night Time' where CustomDescription = 'Nite Time' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'PIC/SIC' where CustomDescription = 'P   S' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'RON' where CustomDescription = 'RONs' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Takeoff Day' where CustomDescription = 'T/OFF Dt' AND Category='PILOT LOG'
update TSDefinitionLog set CustomDescription = 'Takeoff Night' where CustomDescription = 'T/Off Nt' AND Category='PILOT LOG'



update TSDefinitionLog set OriginalDescription = 'Approach Non-Precision' where OriginalDescription = 'Appr  Npr' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Approach Precision' where OriginalDescription = 'Appr  Pr' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Arrival ICAO' where OriginalDescription = 'Arr. ICAO' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Block In' where OriginalDescription = 'Blk In' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Block Out' where OriginalDescription = 'Blk Out' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Departure ICAO' where OriginalDescription = 'Dep. ICAO' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Flight Category' where OriginalDescription = 'Flt Cat' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Flight Type' where OriginalDescription = 'Flt Typ' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Instrument Time' where OriginalDescription = 'Inst Time' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Landing Day' where OriginalDescription = 'Lnding Dt' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Landing Night' where OriginalDescription = 'Lnding Nt' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Night Time' where OriginalDescription = 'Nite Time' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'PIC/SIC' where OriginalDescription = 'P   S' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'RON' where OriginalDescription = 'RONs' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Takeoff Day' where OriginalDescription = 'T/OFF Dt' AND Category='PILOT LOG'
update TSDefinitionLog set OriginalDescription = 'Takeoff Night' where OriginalDescription = 'T/Off Nt' AND Category='PILOT LOG'







Update TSFlightLog set OriginalDescription = REPLACE(OriginalDescription,'CrewLog','Crew Log')  
where OriginalDescription like '%CrewLog%'

Update TSFlightLog set OriginalDescription = REPLACE(OriginalDescription,'label','Label')  
where OriginalDescription like '%Crew Log%'

Update TSFlightLog set OriginalDescription = REPLACE(OriginalDescription,'CREW LOG CUSTOM LABEL','Crew Log Custom Label')  


Update TSFlightLog set CustomDescription = REPLACE(CustomDescription,'CREW LOG CUSTOM LABEL','Crew Log Custom Label')  


Update TSDefinitionLog set OriginalDescription = REPLACE(OriginalDescription,'CREW LOG CUSTOM LABEL','Crew Log Custom Label')  

Update TSDefinitionLog set CustomDescription = REPLACE(CustomDescription,'CREW LOG CUSTOM LABEL','Crew Log Custom Label')  

