use fp_test_dev2
go
declare @cnt int
DECLARE @ICAOCD VARCHAR(50)                                        
DECLARE @UWAAirportId bigint                                    
DECLARE @CustomAirportID bigint
DECLARE @AirportID varchar(50)
DECLARE @ischoicefbo bigint
DECLARE @ischoiceinlist bigint
DECLARE @ischoicepresent bit
DECLARE @fbovendorname varchar(500)
DECLARE @customeridbyemailid as bigint
DECLARE @customername as varchar(500)
DECLARE @cusidofUWA  as bigint = dbo.GetUWACustomerID();                                
print  @cusidofUWA


set @cnt = 1

IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#ISCHOICELIST%') BEGIN
   DROP TABLE #ISCHOICELIST;
END;
IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#fboidtemp%') BEGIN
   DROP TABLE #fboidtemp;
END;
CREATE TABLE #ISCHOICELIST(CustomerId BIGINT, CUSTOMERNAME VARCHAR(500), CustomAirportID BIGINT, UWAAirport BIGINT, ICAO varchar(100),fboid bigint,FBOVendor varchar(150),ischoice varchar(100))

DECLARE customer_cursor CURSOR FOR
SELECT CustomerID, CustomerName from customer where customerid > 10000 and isdeleted=0 --AND CustomerID in ( 10005, 10006, 10007)
ORDER BY CUSTOMERID
OPEN customer_cursor

FETCH NEXT FROM customer_cursor 
INTO @customeridbyemailid, @customername
WHILE @@FETCH_STATUS = 0
BEGIN
	print 'working on customer ' + cast(@customeridbyemailid as varchar(20)) + ',' + @customername
	DECLARE airport_cursor CURSOR FOR 
	SELECT distinct airportid
	FROM fbo
	WHERE customerid=@customeridbyemailid
	ORDER BY airportid;

	OPEN airport_cursor

	FETCH NEXT FROM airport_cursor 
	INTO @AirportID
	WHILE @@FETCH_STATUS = 0
	BEGIN
	SELECT @ICAOCD = icaoid FROM Airport WHERE AirportID = @AirportID       
	print 'working on ' + @ICAOCD + ', ID: ' + cast(@AirportID as varchar(20))
	SELECT @UWAAirportId =  AirportID FROM Airport WHERE CustomerID = @cusidofUWA AND IcaoID = @ICAOCD                                            
	--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.                                    
	SELECT @CustomAirportID =  AirportID FROM Airport WHERE CustomerID = @customeridbyemailid AND IcaoID = @ICAOCD 
	if @CustomAirportID is null
	  set @CustomAirportID = 0
	select MIN(fboInner.FBOID) FBOID  into #fboidtemp from
	FBO fboInner                                                            
	WHERE ISNULL(fboInner.IsDeleted, 0) = 0                                                                     
	AND (((fboInner.AirportID  = @CustomAirportID AND fboInner.CustomerID = @customeridbyemailid) or (fboInner.AirportID = @UWAAirportId AND  fboInner.CustomerID = @customeridbyemailid ))
	OR ((fboInner.AirportID = @UWAAirportId  AND fboInner.CustomerID = @cusidofUWA) AND fboInner.FBOCD NOT IN                                                                                 
		(SELECT FBOCD FROM FBO WHERE ((AirportID = @CustomAirportID AND CustomerID = @customeridbyemailid) or 
		(AirportID = @UWAAirportId AND CustomerID = @customeridbyemailid))                                                      
		AND ISNULL(IsDeleted, 0) = 0) 
	))
	AND ISNULL(fboInner.IsInActive, 0) = 0 
	group by fboInner.fbovendor 

	select @ischoicefbo= fboid from fbo where (customerid=@customeridbyemailid 
	and (airportid=@UWAAirportId or airportid=@CustomAirportID))-- or (AirportID = @UWAAirportId  AND CustomerID = @cusidofUWA)
	and ischoice=1

	select @ischoiceinlist=FBOID from #fboidtemp where FBOID=@ischoicefbo

	select @fbovendorname=fbovendor from fbo where fboid=@ischoicefbo

	IF @ischoicefbo IS NOT NULL
	BEGIN
	IF @ischoiceinlist =  @ischoicefbo
	 --INSERT INTO #ISCHOICELIST VALUES(@customeridbyemailid,@customername, @CustomAirportID,@UWAAirportId, @ICAOCD, @ischoicefbo, @fbovendorname, 'Pass')
	 print 'pass'
	ELSE
	 INSERT INTO #ISCHOICELIST VALUES(@customeridbyemailid,@customername, @CustomAirportID,@UWAAirportId, @ICAOCD, @ischoicefbo, @fbovendorname, 'Error')
	END
	set @ICAOCD = null
	SET @ischoicefbo = NULL
	SET @fbovendorname = NULL
	SET @ischoiceinlist = NULL
	set @CustomAirportID = null

	drop table #fboidtemp
	 FETCH NEXT FROM airport_cursor 
		INTO @AirportID
	END 
	CLOSE airport_cursor;
	DEALLOCATE airport_cursor;

	SET @customername = NULL
	SET @customeridbyemailid = NULL
	set @cnt = @cnt + 1
--	if(@cnt > 4)
--	  break

FETCH NEXT FROM customer_cursor 
INTO @customeridbyemailid, @customername
END
CLOSE customer_cursor;
DEALLOCATE customer_cursor;

select * from #ISCHOICELIST
drop table #ISCHOICELIST
