-----------------------------------
-- Specify the CustomerID here
DECLARE @CustomerID BIGINT;
SET @CustomerID = 10022;
-----------------------------------


DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;
DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader WHERE CustomerID = @CustomerID;
OPEN C1;
FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 

WHILE @@FETCH_STATUS = 0
BEGIN	
	
  DELETE FROM TripSheetReportDetail WHERE ReportNumber = 2 AND ParameterVariable = 'FBOARRUNIC' AND TripSheetReportHeaderID = @CrTripSheetReportHeaderID
  DELETE FROM TripSheetReportDetail WHERE ReportNumber = 2 AND ParameterVariable = 'FBOARRARIN' AND TripSheetReportHeaderID = @CrTripSheetReportHeaderID
  DELETE FROM TripSheetReportDetail WHERE ReportNumber = 2 AND ParameterVariable = 'FBODEPUNIC' AND TripSheetReportHeaderID = @CrTripSheetReportHeaderID
  DELETE FROM TripSheetReportDetail WHERE ReportNumber = 2 AND ParameterVariable = 'FBODEPARIN' AND TripSheetReportHeaderID = @CrTripSheetReportHeaderID
		
	
	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','FBOARRUNIC','FBO - ARRIVAL UNICOM','L',0,0,0,2,0) 
						  
   	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','FBOARRARIN','FBO - ARRIVAL ARINC','L',0,0,0,2,0) 
						  
						  
		EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','FBODEPUNIC','FBO - DEPARTURE UNICOM','L',0,0,0,2,0) 
						  
		EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','FBODEPARIN','FBO - DEPARTURE ARINC','L',0,0,0,2,0) 					  
  
   
	FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
		
END
CLOSE C1;
DEALLOCATE C1;

--SELECT PA FROM TripSheetReportDetail WHERE TRIPSHEETREPORTHEADERID=10099165088 AND CUSTOMERID=10099 AND REPORTNUMBER=2