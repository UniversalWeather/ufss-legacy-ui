
-- Updated Script for the Bug UW-708
-- Due to the impact of Save issue in UI, the calculated value is not saved in DB. So the value differ in UI and Report for the log no.s (71 & 1453)

--select BeginningDuty,DutyEnd, * from PostflightCrew WHERE PostflightCrewListID IN (10005121582, 10005121583) AND CustomerID = 10005
--select BeginningDuty,DutyEnd, * from PostflightCrew WHERE PostflightCrewListID IN (100241367, 100241368, 100241369) AND CustomerID = 10024
--select BeginningDuty,DutyEnd, * from PostflightCrew WHERE PostflightCrewListID IN (100241371, 100241372, 100241373) AND CustomerID = 10024	

-- For Log Number : 1453
UPDATE PostflightCrew SET BeginningDuty = '14:00', DutyEnd = '22:40' 
	WHERE PostflightCrewListID IN (10005121582, 10005121583) AND CustomerID = 10005

-- For Log Number : 71
UPDATE PostflightCrew SET BeginningDuty = '04:00', DutyEnd = '08:05' 
	WHERE PostflightCrewListID IN (100241367, 100241368, 100241369) AND CustomerID = 10024	

UPDATE PostflightCrew SET BeginningDuty = '07:18', DutyEnd = '11:20' 
	WHERE PostflightCrewListID IN (100241371, 100241372, 100241373) AND CustomerID = 10024	
