IF NOT EXISTS (select 1 from  dbo.ExceptionTemplate  WHERE ExceptionTemplateID = 1046)
BEGIN
  INSERT INTO dbo.ExceptionTemplate 
  (ExceptionTemplateID, ModuleID, SubModuleID, ExceptionTemplate, Severity, IsInActive)
  VALUES(1046,10011,3,'PIC not assigned to Leg <legnum>',2,NULL)
END