

--// Updated Role for Charter Quote

UPDATE UMPermission SET UMPermissionRole = 'CQInvoiceReport' WHERE UMPermissionID = 10001
GO
UPDATE UMPermission SET UMPermissionRole = 'CQLeg' WHERE UMPermissionID = 10003
GO
UPDATE UMPermission SET UMPermissionRole = 'CQLiveTrip' WHERE UMPermissionID = 10005
GO
UPDATE UMPermission SET UMPermissionRole = 'CQLogistics' WHERE UMPermissionID = 10006
GO
UPDATE UMPermission SET UMPermissionRole = 'CQPAXManifest' WHERE UMPermissionID = 10007
GO
UPDATE UMPermission SET UMPermissionRole = 'CQPAXManifestOther' WHERE UMPermissionID = 10008
GO
UPDATE UMPermission SET UMPermissionRole = 'CQQuoteDetail' WHERE UMPermissionID = 10009
GO
UPDATE UMPermission SET UMPermissionRole = 'CQQuoteReport' WHERE UMPermissionID = 10010
GO
UPDATE UMPermission SET UMPermissionRole = 'CQQuotesOnFile' WHERE UMPermissionID = 10011
GO
UPDATE UMPermission SET UMPermissionRole = 'CQFile' WHERE UMPermissionID = 10012
GO
UPDATE UMPermission SET UMPermissionRole = 'CQExpressQuote' WHERE UMPermissionID = 10014
GO

