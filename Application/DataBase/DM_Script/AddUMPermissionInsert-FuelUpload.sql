DECLARE @maxUMPermissionID BIGINT;
SET @maxUMPermissionID=(SELECT MAX(UMPermissionID) FROM UMPermission)

IF NOT EXISTS(SELECT * FROM UMPermission WHERE UMPermissionRole like 'HistoricalFuelData')
BEGIN
INSERT INTO UMPermission(UMPermissionID,UMPermissionName,ModuleID,LastUpdUID,LastUpdTS,UMPermissionRole,FormName,IsInActive,CustomerID)
VALUES(@maxUMPermissionID+1,'Historical Fuel Data',10005,'Ajeet',GETUTCDATE(),'HistoricalFuelData','DBVEND4',null,null)
END


