IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewDefinitionInformation_Test]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewDefinitionInformation_Test]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerAdditionalInformation_test]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerAdditionalInformation_test]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSExport_Main]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSExport_Main]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub2]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub3]
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSheetMhtmlExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSheetMhtmlExportInformation]
GO
