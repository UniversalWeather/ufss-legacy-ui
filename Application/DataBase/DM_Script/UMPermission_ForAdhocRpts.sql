IF NOT EXISTS (SELECT * FROM UMPermission WHERE ModuleID=10010 AND UMPermissionRole='adhocexport')
BEGIN
DECLARE @UMPermissionID BIGINT;

SELECT @UMPermissionID=MAX(UMPermissionID) FROM UMPermission 

--Post Flight
INSERT INTO UMPermission(UMPermissionID,UMPermissionName,ModuleID,LastUpdUID,LastUpdTS,IsDeleted,UMPermissionRole,FormName)
            VALUES(@UMPermissionID+1,'AdHoc Data Export',10010,'UWA Admin',GETDATE(),0,'adhocexport','RPTPO078')
END 