--Defect# 2950 - Fix for  - Ramesh

--Update IsUWAData to 1 when the record is of UWA Maintained
IF EXISTS(SELECT * FROM FleetNewCharterRate WHERE FleetNewCharterRateDescription = 'Standard Charge' AND (IsUWAData = 0 OR IsUWAData = null))
BEGIN
	UPDATE FleetNewCharterRate SET IsUwaData = 1 WHERE FleetNewCharterRateDescription = 'Standard Charge' --AND OrderNum IS NOT NULL AND IsUwaData IS NULL
END
IF EXISTS(SELECT * FROM FleetNewCharterRate WHERE FleetNewCharterRateDescription = 'Positioning Charge' AND (IsUWAData = 0 OR IsUWAData = null))
BEGIN
	UPDATE FleetNewCharterRate SET IsUwaData = 1 WHERE FleetNewCharterRateDescription = 'Positioning Charge' --AND OrderNum IS NOT NULL AND IsUwaData IS NULL
END
IF EXISTS(SELECT * FROM FleetNewCharterRate WHERE FleetNewCharterRateDescription = 'Std Crew RON Chrg' AND (IsUWAData = 0 OR IsUWAData = null))
BEGIN
	UPDATE FleetNewCharterRate SET IsUwaData = 1 WHERE FleetNewCharterRateDescription = 'Std Crew RON Chrg' --AND OrderNum IS NOT NULL AND IsUwaData IS NULL
END

--Update IsUWAData to 0 when the record is of Custom data
IF EXISTS(SELECT * FROM FleetNewCharterRate WHERE FleetNewCharterRateDescription not in ('Standard Charge','Positioning Charge','Std Crew RON Chrg')  AND (IsUWAData = 1 OR IsUWAData = null))
BEGIN
	UPDATE FleetNewCharterRate SET IsUwaData = 0 WHERE FleetNewCharterRateDescription not in ('Standard Charge','Positioning Charge','Std Crew RON Chrg') --AND OrderNum IS NOT NULL AND IsUwaData IS NULL
END

--Update IsDeleted to 0 for UWA maintained records
UPDATE FleetNewCharterRate SET IsDeleted = 0 WHERE FleetNewCharterRateDescription = 'Standard Charge' --AND OrderNum IS NOT NULL AND IsDeleted = 1
UPDATE FleetNewCharterRate SET IsDeleted = 0 WHERE FleetNewCharterRateDescription = 'Positioning Charge' --AND OrderNum IS NOT NULL AND IsDeleted = 1
UPDATE FleetNewCharterRate SET IsDeleted = 0 WHERE FleetNewCharterRateDescription = 'Std Crew RON Chrg' --AND OrderNum IS NOT NULL AND IsDeleted = 1



