--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--1.  Please remove Table from all names - it is sometime appended and sometime not.  For consistency, it should be removed from all.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Accounts' WHERE UMPERMISSIONID = 10037 AND UMPERMISSIONNAME = 'Accounts Table                                  '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Airport' WHERE UMPERMISSIONID = 10041 AND UMPERMISSIONNAME = 'Airport Table                                   '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Authorization' WHERE UMPERMISSIONID = 10042 AND UMPERMISSIONNAME = 'Authorization Table                               '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Budget' WHERE UMPERMISSIONID = 10043 AND UMPERMISSIONNAME = 'Budget Table                                    '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Catering' WHERE UMPERMISSIONID = 10044 AND UMPERMISSIONNAME = 'Catering Table                                  '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Charter Quote' WHERE UMPERMISSIONID = 10046 AND UMPERMISSIONNAME = 'Charter Quote Customer Table                    '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Client' WHERE UMPERMISSIONID = 10048 AND UMPERMISSIONNAME = 'Client Table                                    '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Country' WHERE UMPERMISSIONID = 10052 AND UMPERMISSIONNAME = 'Country Table                                   '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Crew Addlitional Information Codes' WHERE UMPERMISSIONID = 10054 AND UMPERMISSIONNAME = 'Crew Addlitional Information Codes Table '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Crew Checklist Codes' WHERE UMPERMISSIONID = 10057 AND UMPERMISSIONNAME = 'Crew Checklist Codes Table                      '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Crew Group' WHERE UMPERMISSIONID = 10061 AND UMPERMISSIONNAME = 'Crew Group Table                                '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Crew Passport' WHERE UMPERMISSIONID = 10062 AND UMPERMISSIONNAME = 'Crew Passport Table                             '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Custom Addresses' WHERE UMPERMISSIONID = 10067 AND UMPERMISSIONNAME = 'Custom Addresses Table                          '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Department Group' WHERE UMPERMISSIONID = 10069 AND UMPERMISSIONNAME = 'Department Group Table                          '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'DST Region' WHERE UMPERMISSIONID = 10071 AND UMPERMISSIONNAME = 'DST Region Table                                '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Emergency' WHERE UMPERMISSIONID = 10072 AND UMPERMISSIONNAME = 'Emergency Contact Table                         '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Exchange' WHERE UMPERMISSIONID = 10073 AND UMPERMISSIONNAME = 'Exchange Rate Table                             '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'FBO' WHERE UMPERMISSIONID = 10074 AND UMPERMISSIONNAME = 'FBO Table                                       '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Fee Schedule' WHERE UMPERMISSIONID = 10076 AND UMPERMISSIONNAME = 'Fee Schedule Table                              '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Fee Schedule Group' WHERE UMPERMISSIONID = 10077 AND UMPERMISSIONNAME = 'Fee Schedule Group Table                        '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Fleet Group' WHERE UMPERMISSIONID = 10084 AND UMPERMISSIONNAME = 'Fleet Group Table                               '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Fuel Vendor' WHERE UMPERMISSIONID = 10092 AND UMPERMISSIONNAME = 'Fuel Vendor Table                               '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Hotel' WHERE UMPERMISSIONID = 10094 AND UMPERMISSIONNAME = 'Hotel Table                                     '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Lead Source' WHERE UMPERMISSIONID = 10095 AND UMPERMISSIONNAME = 'Lead Source Table                               '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Metro City' WHERE UMPERMISSIONID = 10096 AND UMPERMISSIONNAME = 'Metro City Table                                '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Passenger Group' WHERE UMPERMISSIONID = 10098 AND UMPERMISSIONNAME = 'Passenger Group Table                           '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Payables Vendor' WHERE UMPERMISSIONID = 10100 AND UMPERMISSIONNAME = 'Payables Vendor Table                           '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Payment Types' WHERE UMPERMISSIONID = 10101 AND UMPERMISSIONNAME = 'Payment Types Table                             '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Salesperson' WHERE UMPERMISSIONID = 10103 AND UMPERMISSIONNAME = 'Salesperson Table                               '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'SIFL Rate' WHERE UMPERMISSIONID = 10104 AND UMPERMISSIONNAME = 'SIFL Rate Table'
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Transportation' WHERE UMPERMISSIONID = 10105 AND UMPERMISSIONNAME = 'Transportation Table                            '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Trip Manager Checklist' WHERE UMPERMISSIONID = 10109 AND UMPERMISSIONNAME = 'Trip Manager Checklist Table'
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Trip Manager Checklist Group' WHERE UMPERMISSIONID = 10110 AND UMPERMISSIONNAME = 'Trip Manager Checklist Group Table'
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Vendor' WHERE UMPERMISSIONID = 10112 AND UMPERMISSIONNAME = 'Vendor Table                                    '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Lost Business' WHERE UMPERMISSIONID = 10359 AND UMPERMISSIONNAME = 'Lost Business Table'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--2.  For Company Profile - Invoice Report Default, it should be changed to plural:  Company Profile - Invoice Report Defaults
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Company Profile - Invoice Report Defaults' WHERE UMPermissionID = 10050 AND UMPermissionName = 'Company Profile - Invoice Report Default          '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--3.  Crew Additional Information should be Crew Additional Info
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Crew Additional Info' WHERE UMPermissionID = 10053 AND UMPermissionName = 'Crew Additional Information                       '
UPDATE UMPermission SET UMPermissionName = 'Crew Additional Info' WHERE UMPermissionID = 10119 AND UMPermissionName = 'Crew Additional Information                       '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--4.  Crew Additional Information Codes Table should be Crew Additional Info Codes
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Crew Addlitional Info Codes' WHERE UMPermissionID = 10054 AND UMPermissionName = 'Crew Addlitional Information Codes'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--5.  Custom Addresses Table should be Custom Address
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Custom Address' WHERE UMPermissionID = 10067 AND UMPermissionName = 'Custom Addresses'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--6.  FBO Choice Selection should be FBO Choice
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'FBO Choice' WHERE UMPermissionID = 10075 AND UMPermissionName = 'FBO Choice Selection                              '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--7.  Fleet Additional Information should be Fleet Additional Info
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Fleet Additional Info' WHERE UMPermissionID = 10078 AND UMPermissionName = 'Fleet Additional Information'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--8.  Fleet New Charter Rates should be Fleet Charter Rates
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Fleet Charter Rates' WHERE UMPermissionID = 10086 AND UMPermissionName = 'Fleet New Charter Rates                           '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--9.  Fleet Profile Additional Information should be Fleet Profile Additional Info
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Fleet Profile Additional Info' WHERE UMPermissionID = 10088 AND UMPermissionName = 'Fleet Profile Additional Information              '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--10.  Passenger Additional Information should be Passenger Additional Info
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Passenger Additional Info' WHERE UMPermissionID = 10097 AND UMPermissionName = 'Passenger Additional Information'
UPDATE UMPermission SET UMPermissionName = 'Passenger Additional Info' WHERE UMPermissionID = 10131 AND UMPermissionName = 'Passenger Additional Information'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--11.  Trip Manager Checklist Group Table should be Checklist Group
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Checklist Group' WHERE UMPermissionID = 10110 AND UMPermissionName = 'Trip Manager Checklist Group'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--12.  Trip Manager Checklist Table should be Checklist
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Checklist' WHERE UMPermissionID = 10109 AND UMPermissionName = 'Trip Manager Checklist'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--13.  The list should be alphabetic - Checklist before Checklist Group, Fee Schedule before Fee Schedule Group
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--14.  Don't know why Crew Roster Reports is listed here AND under module name Database Reports?!  Should be removed from here.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Update existing to Database reports
DELETE FROM UserGroupPermission WHERE UMPermissionID = 10122

UPDATE UserGroupPermission SET UMPermissionID = 10122 WHERE UMPermissionID = 10065

DELETE FROM UMPermission WHERE UMPermissionName = 'CREW ROSTER REPORTS' AND UMPermissionID = 10065
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--1.  There are two options for SIFL - what is the difference?  Please clarify.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Already Fixed.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--2.  The Trip Manager permission name should be changed to Preflight
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Preflight' WHERE UMPermissionID = 10238 AND UMPermissionName = 'Trip Manager                                      '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--3.  Trip Manager should be removed from all remaining Permission Names
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Airport                              ' WHERE UMPERMISSIONID = 10239 AND UMPERMISSIONNAME = 'Trip Manager Airport                              '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight APIS Interface                       ' WHERE UMPERMISSIONID = 10240 AND UMPERMISSIONNAME = 'Trip Manager APIS Interface                       '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Catering                             ' WHERE UMPERMISSIONID = 10241 AND UMPERMISSIONNAME = 'Trip Manager Catering                             '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Checklist                            ' WHERE UMPERMISSIONID = 10242 AND UMPERMISSIONNAME = 'Trip Manager Checklist                            '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Corporate Request                    ' WHERE UMPERMISSIONID = 10243 AND UMPERMISSIONNAME = 'Trip Manager Corporate Request                    '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Crew                                 ' WHERE UMPERMISSIONID = 10244 AND UMPERMISSIONNAME = 'Trip Manager Crew                                 '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Crew Available                       ' WHERE UMPERMISSIONID = 10245 AND UMPERMISSIONNAME = 'Trip Manager Crew Available                       '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Crew Hotels                          ' WHERE UMPERMISSIONID = 10246 AND UMPERMISSIONNAME = 'Trip Manager Crew Hotels                          '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Crew Transportation                  ' WHERE UMPERMISSIONID = 10247 AND UMPERMISSIONNAME = 'Trip Manager Crew Transportation                  '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Exceptions                           ' WHERE UMPERMISSIONID = 10248 AND UMPERMISSIONNAME = 'Trip Manager Exceptions                           '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight FBO/Handler                          ' WHERE UMPERMISSIONID = 10249 AND UMPERMISSIONNAME = 'Trip Manager FBO/Handler                          '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Fuel                                 ' WHERE UMPERMISSIONID = 10250 AND UMPERMISSIONNAME = 'Trip Manager Fuel                                 '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight History                              ' WHERE UMPERMISSIONID = 10251 AND UMPERMISSIONNAME = 'Trip Manager History                              '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Leg Notes                            ' WHERE UMPERMISSIONID = 10252 AND UMPERMISSIONNAME = 'Trip Manager Leg Notes                            '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Legs                                 ' WHERE UMPERMISSIONID = 10253 AND UMPERMISSIONNAME = 'Trip Manager Legs                                 '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Log Button                           ' WHERE UMPERMISSIONID = 10254 AND UMPERMISSIONNAME = 'Trip Manager Log Button                           '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Main Information                     ' WHERE UMPERMISSIONID = 10255 AND UMPERMISSIONNAME = 'Trip Manager Main Information                     '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Outbound Instructions                ' WHERE UMPERMISSIONID = 10256 AND UMPERMISSIONNAME = 'Trip Manager Outbound Instructions                '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Passenger                            ' WHERE UMPERMISSIONID = 10257 AND UMPERMISSIONNAME = 'Trip Manager Passenger                            '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Passenger Hotels' WHERE UMPERMISSIONID = 10258 AND UMPERMISSIONNAME = 'Trip Manager Passenger Hotels'
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Passenger Transportation' WHERE UMPERMISSIONID = 10259 AND UMPERMISSIONNAME = 'Trip Manager Passenger Transportation'
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Reports                              ' WHERE UMPERMISSIONID = 10260 AND UMPERMISSIONNAME = 'Trip Manager Reports                              '
UPDATE UMPERMISSION SET UMPERMISSIONNAME = 'Preflight Trip Request                         ' WHERE UMPERMISSIONID = 10262 AND UMPERMISSIONNAME = 'Trip Manager Trip Request                         '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--1.  Crew Additional Information should be Crew Additional Info - Done
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--2.  Passenger Additional Information should be Passenger Additional Info - Done
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--3.  Payable Vendors should be Payables Vendor
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Payables Vendor' WHERE UMPermissionID = 10134 AND UMPermissionName = 'Payable Vendors                                   '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--4.  Salespersons should be singular, Salesperson
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Salesperson' WHERE UMPermissionID = 10137 AND UMPermissionName = 'Salespersons                                      '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--5.  Vendors should be singular, Vendor
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Vendor' WHERE UMPermissionID = 10142 AND UMPermissionName = 'Vendors                                           '
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--1.  FlightPak Database should be Database
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Database' WHERE UMPermissionID = 10145 AND UMPermissionName = 'FlightPak Database'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--2.  FlightPak Postflight should be Postflight
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Postflight' WHERE UMPermissionID = 10146 AND UMPermissionName = 'FlightPak Postflight'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--3.  FlightPak Preflight should be Preflight
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Preflight' WHERE UMPermissionID = 10147 AND UMPermissionName = 'FlightPak Preflight'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--4.  FlightPak Utilities should be Utilties
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Utilities' WHERE UMPermissionID = 10148 AND UMPermissionName = 'FlightPak Utilities'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--The module name should be Security Administration
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE Module SET ModuleName = 'Security Administration' WHERE ModuleID = 10015 AND ModuleName = 'Security'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--UWATrip Planner should be UVTripPlanner
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'UVTripPlanner' WHERE UMPermissionID = 10347 AND UMPermissionName = 'UWATrip Planner'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--1.  The module name should be UVtriplink
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--There is no module called UWATripLink
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--For Module Name:  Postflight:
--1.  Expense Table should match the screen and be identified as Expense Catalog
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE UMPermission SET UMPermissionName = 'Expense Catalog' WHERE UMPermissionID = 10152
AND UMPermissionName = 'Expense Table                                   '