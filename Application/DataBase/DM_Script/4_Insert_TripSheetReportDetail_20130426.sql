DELETE FROM TripSheetReportDetail WHERE ReportNumber=2 AND ParameterVariable='MINFUEL'
DELETE FROM TripSheetReportDetail WHERE ReportNumber=2 AND ParameterVariable='POWERSET'
DELETE FROM TripSheetReportDetail WHERE ReportNumber=2 AND ParameterVariable='AIRSPEED'

DECLARE @CrCustomerID BIGINT,@CrTripSheetReportHeaderID BIGINT, @TripSheetReportDetailID BIGINT;;

DECLARE C1 CURSOR FOR
SELECT DISTINCT CustomerID,TripSheetReportHeaderID FROM TripSheetReportHeader;

OPEN C1;

FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 

WHILE @@FETCH_STATUS = 0
BEGIN
		 
	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 

	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','LGMINFUEL','LEG ITEMS - MINIMUM FUEL','L',0,1,0,2,0) 

	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','LGPOWERSET','LEG ITEMS - AIRCRAFT POWER SETTING','L',0,1,0,2,0) 

	EXECUTE dbo.usp_GetSequenceNumber @CrCustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT 
	INSERT INTO TripSheetReportDetail(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,
									   ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,
									   ParameterLValue,ParameterNValue,ReportOrder,TripSheetOrder)
						  VALUES(@TripSheetReportDetailID,@CrCustomerID,@CrTripSheetReportHeaderID,2,'TRIPSHEET',1,1,'RPTTS02','LGAIRSPEED','LEG ITEMS - AIRCRAFT TRUE AIR SPEED','L',0,1,0,2,0) 
						  
FETCH NEXT FROM C1 INTO @CrCustomerID,@CrTripSheetReportHeaderID; 
 
END
	
CLOSE C1;
DEALLOCATE C1;