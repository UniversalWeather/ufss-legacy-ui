
DECLARE @PaxInfoID bigint 
DECLARE @PaxInfoCD char(3) 
DECLARE @CustomerID bigint 
DECLARE @i INT =1
DECLARE @count INT 
DECLARE @counti INT 

--Outer Tanle
DECLARE  @TCustomer TABLE  ( SNo INT IDENTITY(1,1),PassengerInformationID BIGINT ) 
INSERT INTO @TCustomer
SELECT DISTINCT PassengerInformationID FROM PassengerInformation 

SELECT @COUNT=COUNT(PassengerInformationID) FROM @TCustomer  

WHILE (@i <= @count) 
BEGIN 
	SELECT @CustomerID = CustomerID,@PaxInfoCD=PassengerInfoCD,@PaxInfoID = PassengerInformationID 
	                     FROM PassengerInformation 
	                     WHERE PassengerInformationID=(SELECT PassengerInformationID FROM @TCustomer WHERE SNo=@I)
			
       UPDATE PassengerAdditionalInfo SET PassengerInformationID = @PaxInfoID 
                                      WHERE AdditionalINFOCD = @PaxInfoCD 
                                       AND CustomerID = @CustomerID
	   SET @i = @i + 1
 END
		
