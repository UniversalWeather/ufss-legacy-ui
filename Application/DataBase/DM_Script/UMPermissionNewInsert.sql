
DELETE FROM UMPermission WHERE ModuleID=10010 AND UMPermissionRole='StateMileage'
DELETE FROM UMPermission WHERE ModuleID=10012 AND UMPermissionRole='TripSheetCancellaion'
DELETE FROM UMPermission WHERE ModuleID=10002 AND UMPermissionRole='TripStatus'


DECLARE @UMPermissionID BIGINT;

SELECT @UMPermissionID=MAX(UMPermissionID) FROM UMPermission 


INSERT INTO UMPermission(UMPermissionID,UMPermissionName,ModuleID,LastUpdUID,LastUpdTS,IsDeleted,UMPermissionRole,FormName)
            VALUES(@UMPermissionID+1,'State Mileage',10010,'UWA Admin',GETDATE(),0,'StateMileage','RPTPO077')

--Pre Flight
INSERT INTO UMPermission(UMPermissionID,UMPermissionName,ModuleID,LastUpdUID,LastUpdTS,IsDeleted,UMPermissionRole,FormName)
            VALUES(@UMPermissionID+2,'TripSheet Cancellaion',10012,'UWA Admin',GETDATE(),0,'TripSheetCancellaion','RPTPR049')
--Charter Quote
INSERT INTO UMPermission(UMPermissionID,UMPermissionName,ModuleID,LastUpdUID,LastUpdTS,IsDeleted,UMPermissionRole,FormName)
            VALUES(@UMPermissionID+3,'Trip Status',10002,'UWA Admin',GETDATE(),0,'TripStatus','RPTCQ09')

--select  * from UMPermission  order by UMPermissionID

