-- New User Creation
DELETE FROM FPEmailTranLog
DELETE FROM FPEmailTmpl

Insert Into FPEmailTmpl (EmailTmplID,TmplName,EmailBody,EmailSubject,CustomerID,LastUpdUID,LastUpdTS,IsDeleted,IsInActive) 
values (2,'New User Creation', '<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<base href="http://dev.univ-wea.com:8177/_email/public/SP/GR/GR016/">

<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Welcome to Universal Flight Scheduling Software � Your Account Access
Information</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Template>NormalEmail</o:Template>
  <o:Revision>1</o:Revision>
  <o:TotalTime>5</o:TotalTime>
  <o:Created>2013-02-08T09:48:00Z</o:Created>
  <o:Pages>1</o:Pages>
  <o:Words>496</o:Words>
  <o:Characters>2831</o:Characters>
  <o:Company>Hewlett-Packard Company</o:Company>
  <o:Lines>23</o:Lines>
  <o:Paragraphs>6</o:Paragraphs>
  <o:CharactersWithSpaces>3321</o:CharactersWithSpaces>
  <o:Version>12.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->

<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-IN</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:DoNotExpandShiftReturn/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
span.EmailStyle20
	{mso-style-type:personal-compose;
	mso-style-noshow:yes;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-family:"Arial","sans-serif";
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	color:#333333;
	mso-no-proof:yes;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-ascii-font-family:Tahoma;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-IN link=blue vlink=purple style=''tab-interval:.5in''>

<div class=Section1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:112.5pt''>
  <td width=700 colspan=5 valign=bottom style=''width:525.0pt;padding:0in 0in 0in 0in;
  height:112.5pt''>
  <p class=MsoNormal style=''line-height:13.5pt''><span style=''font-size:9.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman";
  color:#333333;mso-no-proof:yes''><img width=700 height=150 id="_x0000_i1034"
  src="http://k.universalweather.com/SP/GR/GR016/UWA-email-header-operational-UFSS.png"
  alt="An important message from Universal Weather and Aviation, Inc. - Flight Scheduling Software"></span><span
  style=''font-size:9.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:
  "Times New Roman";color:#333333''><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;mso-yfti-lastrow:yes''>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=600 style=''width:6.25in;padding:0in 0in 0in 0in''>
  <div align=center>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style=''width:100.0%;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
   0in 0in 0in 0in''>
   <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:.25in''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:.25in''></td>
   </tr>
   <tr style=''mso-yfti-irow:1''>
    <td style=''padding:0in 0in 0in 0in''>
    <p align=center style=''margin:0in;margin-bottom:.0001pt;text-align:center;
    line-height:22.5pt''><strong><span style=''font-size:10.5pt;font-family:"Arial","sans-serif";
    color:#DB063B''>**Please keep this e-mail for your records.**</span></strong><span
    style=''font-size:10.5pt;font-family:"Arial","sans-serif";color:#DB063B''><o:p></o:p></span></p>
    </td>
   </tr>
   <tr style=''mso-yfti-irow:2;height:.25in''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:.25in''></td>
   </tr>
   <tr style=''mso-yfti-irow:3''>
    <td style=''padding:0in 0in 0in 0in''>
    <p style=''margin:0in;margin-bottom:.0001pt;line-height:22.5pt''><span
    style=''font-size:16.0pt;font-family:"Arial","sans-serif";color:#666666''>Your
    Account Access Information<o:p></o:p></span></p>
    </td>
   </tr>
   <tr style=''mso-yfti-irow:4;height:.25in''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:.25in''></td>
   </tr>
   <tr style=''mso-yfti-irow:5''>
    <td style=''padding:0in 0in 0in 0in''>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Hi [[Client Name]],<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Welcome to Universal� Flight Scheduling Software!<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>You can access this application from anywhere by going to <a
    href="https://scheduling.universalweather.com" target="_blank">https://scheduling.universalweather.com</a>
    and logging in with the following credentials:<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Username: [[User Name]]<br>
    Password: [[Password]]<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>You will be required to change your password after logging back into the application. When you have successfully changed your password, you will be granted full access to the application.<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Thank you for choosing Universal for your aviation needs. If
    there is anything we can do to help, please let us know!<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Best regards,<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>[[Name]]<br>
    Universal Flight Scheduling Software<br>
    Universal Weather and Aviation, Inc.<br>
    Worldwide: (713) 944-1440 x 92050<br>
    N. America: (800) 231-8648 x 92050<br>
    E-mail: <a href="mailto:softwaresupport@univ-wea.com">softwaresupport@univ-wea.com</a><br>
    <a href="http://www.universalweather.com" target="_blank">universalweather.com</a><o:p></o:p></span></p>
    </td>
   </tr>
   <tr style=''mso-yfti-irow:6;mso-yfti-lastrow:yes;height:22.5pt''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:22.5pt''></td>
   </tr>
  </table>
  </div>
  </td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:11.25pt''>
  <td width=700 colspan=10 valign=top style=''width:525.0pt;background:#414141;
  padding:0in 0in 0in 0in;height:11.25pt''>
  <p style=''margin:0in;margin-bottom:.0001pt;mso-line-height-alt:11.25pt''><span
  style=''mso-no-proof:yes''><img border=0 width=700 height=15 id="_x0000_i1033"
  src="http://i.universalweather.com/_email/social/section1-top.png"
  alt="http://i.universalweather.com/_email/social/section1-top.png"></span><o:p></o:p></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;height:28.5pt''>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=124 style=''width:93.0pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><span style=''mso-no-proof:yes''><img
  border=0 width=124 height=38 id="_x0000_i1032"
  src="http://i.universalweather.com/_email/social/section1-connect.png"
  alt="http://i.universalweather.com/_email/social/section1-connect.png"></span><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://plus.google.com/100024822646419142787/posts" target="_blank"
  title="Google+"><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1031"
  src="http://i.universalweather.com/_email/social/section1-googleplus.png"
  alt="Google+"></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://twitter.com/#!/universalweathr" target="_blank" title=Twitter><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=30 height=38
  id="_x0000_i1030"
  src="http://i.universalweather.com/_email/social/section1-twitter.png"
  alt=Twitter></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.facebook.com/universalweathr" target="_blank"
  title=Facebook><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1029"
  src="http://i.universalweather.com/_email/social/section1-facebook.png"
  alt=Facebook></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.linkedin.com/company/universal-weather-and-aviation"
  target="_blank" title=LinkedIn><span style=''font-family:"Arial","sans-serif";
  color:#555555;mso-no-proof:yes;text-decoration:none;text-underline:none''><img
  border=0 width=30 height=38 id="_x0000_i1028"
  src="http://i.universalweather.com/_email/social/section1-linkedin.png"
  alt=LinkedIn></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.youtube.com/user/universalweather" target="_blank"
  title=YouTube><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1027"
  src="http://i.universalweather.com/_email/social/section1-youtube.png"
  alt=YouTube></span></a><o:p></o:p></p>
  </td>
  <td width=185 style=''width:138.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=141 style=''width:105.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.universalweather.com/blog/" target="_blank" title=Blog><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=141 height=38
  id="_x0000_i1026"
  src="http://i.universalweather.com/_email/social/section1-blog.png"
  alt="Read our blog"></span></a><o:p></o:p></p>
  </td>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:2;mso-yfti-lastrow:yes;height:70.5pt''>
  <td width=700 colspan=10 valign=bottom style=''width:525.0pt;background:white;
  padding:0in 0in 0in 0in;height:70.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://itunes.apple.com/us/app/universal-ops-brief-aviation/id467458235?mt=8"
  title="&quot;Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more.&quot; t "><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=700 height=92
  id="_x0000_i1025"
  src="http://i.universalweather.com/_email/social/section2-app-oft-mobile.png"
  alt="Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more."></span></a><o:p></o:p></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes''>
  <td style=''padding:15.75pt 37.5pt 0in 37.5pt''>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>Universal is a registered trademark of Universal Weather and
  Aviation, Inc.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>� 2013 Universal Weather and Aviation Inc. 8787 Tallyho Road,
  Houston, Texas 77061-3420 USA.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>App Store and iPad are trademarks of Apple, Inc., registered
  in the U.S. and other countries.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>SP-GR016<o:p></o:p></span></p>
  </td>
 </tr>
</table>

</div>

<div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman"''>&nbsp;<o:p></o:p></span></p>

</div>

</div>

</body>

</html>
', 'Welcome to Universal Flight Scheduling Software - Your Account Access Information',
10000,'UWA Admin','2013-02-02 00:00:00.000',0,0)
-- Change Password
Insert Into FPEmailTmpl (EmailTmplID,TmplName,EmailBody,EmailSubject,CustomerID,LastUpdUID,LastUpdTS,IsDeleted,IsInActive) 
values (3,'Change Password','<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<base href="http://dev.univ-wea.com:8177/_email/public/SP/GR/GR015/">

<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Password Change Confirmation � Universal Flight Scheduling Software</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Template>NormalEmail</o:Template>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2013-02-08T09:53:00Z</o:Created>
  <o:Pages>1</o:Pages>
  <o:Words>411</o:Words>
  <o:Characters>2347</o:Characters>
  <o:Company>Hewlett-Packard Company</o:Company>
  <o:Lines>19</o:Lines>
  <o:Paragraphs>5</o:Paragraphs>
  <o:CharactersWithSpaces>2753</o:CharactersWithSpaces>
  <o:Version>12.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->

<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-IN</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:DoNotExpandShiftReturn/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
span.EmailStyle18
	{mso-style-type:personal-compose;
	mso-style-noshow:yes;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-family:"Arial","sans-serif";
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	color:#333333;
	mso-no-proof:yes;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-ascii-font-family:Tahoma;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-IN link=blue vlink=purple style=''tab-interval:.5in''>

<div class=Section1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:112.5pt''>
  <td width=700 colspan=5 valign=bottom style=''width:525.0pt;padding:0in 0in 0in 0in;
  height:112.5pt''>
  <p class=MsoNormal style=''line-height:13.5pt''><span style=''font-size:9.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman";
  color:#333333;mso-no-proof:yes''><img width=700 height=150 id="_x0000_i1034"
  src="http://k.universalweather.com/SP/GR/GR015/UWA-email-header-operational-UFSS.png"
  alt="An important message from Universal Weather and Aviation, Inc. - Flight Scheduling Software"></span><span
  style=''font-size:9.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:
  "Times New Roman";color:#333333''><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;mso-yfti-lastrow:yes''>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=600 style=''width:6.25in;padding:0in 0in 0in 0in''>
  <div align=center>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style=''width:100.0%;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
   0in 0in 0in 0in''>
   <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:.25in''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:.25in''></td>
   </tr>
   <tr style=''mso-yfti-irow:1''>
    <td style=''padding:0in 0in 0in 0in''>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Hi [[Client Name]],<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Your Universal� Flight Scheduling Software password has been
    changed. If you did not perform this password change, please call Universal
    immediately at (713) 944-1440 ext. 92050.<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Best regards,<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>[[Name]]<br>
    Universal Flight Scheduling Software<br>
    Universal Weather and Aviation, Inc.<br>
    Worldwide: (713) 944-1440 x 92050<br>
    N. America: (800) 231-8648 x 92050<br>
    E-mail: <a href="mailto:softwaresupport@univ-wea.com">softwaresupport@univ-wea.com</a><br>
    <a href="http://www.universalweather.com" target="_blank">universalweather.com</a><o:p></o:p></span></p>
    </td>
   </tr>
   <tr style=''mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.5pt''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:22.5pt''></td>
   </tr>
  </table>
  </div>
  </td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:11.25pt''>
  <td width=700 colspan=10 valign=top style=''width:525.0pt;background:#414141;
  padding:0in 0in 0in 0in;height:11.25pt''>
  <p style=''margin:0in;margin-bottom:.0001pt;mso-line-height-alt:11.25pt''><span
  style=''mso-no-proof:yes''><img border=0 width=700 height=15 id="_x0000_i1033"
  src="http://i.universalweather.com/_email/social/section1-top.png"
  alt="http://i.universalweather.com/_email/social/section1-top.png"></span><o:p></o:p></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;height:28.5pt''>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=124 style=''width:93.0pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><span style=''mso-no-proof:yes''><img
  border=0 width=124 height=38 id="_x0000_i1032"
  src="http://i.universalweather.com/_email/social/section1-connect.png"
  alt="http://i.universalweather.com/_email/social/section1-connect.png"></span><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://plus.google.com/100024822646419142787/posts" target="_blank"
  title="Google+"><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1031"
  src="http://i.universalweather.com/_email/social/section1-googleplus.png"
  alt="Google+"></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://twitter.com/#!/universalweathr" target="_blank" title=Twitter><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=30 height=38
  id="_x0000_i1030"
  src="http://i.universalweather.com/_email/social/section1-twitter.png"
  alt=Twitter></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.facebook.com/universalweathr" target="_blank"
  title=Facebook><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1029"
  src="http://i.universalweather.com/_email/social/section1-facebook.png"
  alt=Facebook></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.linkedin.com/company/universal-weather-and-aviation"
  target="_blank" title=LinkedIn><span style=''font-family:"Arial","sans-serif";
  color:#555555;mso-no-proof:yes;text-decoration:none;text-underline:none''><img
  border=0 width=30 height=38 id="_x0000_i1028"
  src="http://i.universalweather.com/_email/social/section1-linkedin.png"
  alt=LinkedIn></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.youtube.com/user/universalweather" target="_blank"
  title=YouTube><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1027"
  src="http://i.universalweather.com/_email/social/section1-youtube.png"
  alt=YouTube></span></a><o:p></o:p></p>
  </td>
  <td width=185 style=''width:138.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=141 style=''width:105.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.universalweather.com/blog/" target="_blank" title=Blog><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=141 height=38
  id="_x0000_i1026"
  src="http://i.universalweather.com/_email/social/section1-blog.png"
  alt="Read our blog"></span></a><o:p></o:p></p>
  </td>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:2;mso-yfti-lastrow:yes;height:70.5pt''>
  <td width=700 colspan=10 valign=bottom style=''width:525.0pt;background:white;
  padding:0in 0in 0in 0in;height:70.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://itunes.apple.com/us/app/universal-ops-brief-aviation/id467458235?mt=8"
  title="&quot;Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more.&quot; t "><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=700 height=92
  id="_x0000_i1025"
  src="http://i.universalweather.com/_email/social/section2-app-oft-mobile.png"
  alt="Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more."></span></a><o:p></o:p></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes''>
  <td style=''padding:15.75pt 37.5pt 0in 37.5pt''>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>Universal is a registered trademark of Universal Weather and
  Aviation, Inc.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>� 2013 Universal Weather and Aviation Inc. 8787 Tallyho Road,
  Houston, Texas 77061-3420 USA.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>App Store and iPad are trademarks of Apple, Inc., registered
  in the U.S. and other countries.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>SP-GR015<o:p></o:p></span></p>
  </td>
 </tr>
</table>

</div>

<div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman"''>&nbsp;<o:p></o:p></span></p>

</div>

</div>

</body>

</html>
','Password Change Confirmation - Universal Flight Scheduling Software',
10000,'UWA Admin','2013-02-02 00:00:00.000',0,0)
-- Reset Password
Insert Into FPEmailTmpl (EmailTmplID,TmplName,EmailBody,EmailSubject,CustomerID,LastUpdUID,LastUpdTS,IsDeleted,IsInActive) 
values (4, 'Reset Password','<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<base href="http://dev.univ-wea.com:8177/_email/public/SP/GR/GR014/">

<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Password Reset Confirmation � Universal Flight Scheduling Software</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Template>NormalEmail</o:Template>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2013-02-08T09:55:00Z</o:Created>
  <o:Pages>1</o:Pages>
  <o:Words>481</o:Words>
  <o:Characters>2746</o:Characters>
  <o:Company>Hewlett-Packard Company</o:Company>
  <o:Lines>22</o:Lines>
  <o:Paragraphs>6</o:Paragraphs>
  <o:CharactersWithSpaces>3221</o:CharactersWithSpaces>
  <o:Version>12.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->

<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-IN</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:DoNotExpandShiftReturn/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
span.EmailStyle19
	{mso-style-type:personal-compose;
	mso-style-noshow:yes;
	mso-style-unhide:no;
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-family:"Arial","sans-serif";
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	color:#333333;
	mso-no-proof:yes;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-ascii-font-family:Tahoma;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-IN link=blue vlink=purple style=''tab-interval:.5in''>

<div class=Section1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:112.5pt''>
  <td width=700 colspan=5 valign=bottom style=''width:525.0pt;padding:0in 0in 0in 0in;
  height:112.5pt''>
  <p class=MsoNormal style=''line-height:13.5pt''><span style=''font-size:9.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman";
  color:#333333;mso-no-proof:yes''><img width=700 height=150 id="_x0000_i1034"
  src="http://k.universalweather.com/SP/GR/GR014/UWA-email-header-operational-UFSS.png"
  alt="An important message from Universal Weather and Aviation, Inc. - Flight Scheduling Software"></span><span
  style=''font-size:9.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:
  "Times New Roman";color:#333333''><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;mso-yfti-lastrow:yes''>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=600 style=''width:6.25in;padding:0in 0in 0in 0in''>
  <div align=center>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style=''width:100.0%;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
   0in 0in 0in 0in''>
   <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:.25in''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:.25in''></td>
   </tr>
   <tr style=''mso-yfti-irow:1''>
    <td style=''padding:0in 0in 0in 0in''>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Hi [[Client Name]],<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Your Universal� Flight Scheduling Software password has been
    reset. If you did not perform this password reset, please call Universal
    immediately at (713) 944-1440 ext. 92050.<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><strong><span style=''font-size:9.0pt;font-family:
    "Arial","sans-serif";color:#333333''>Logging in</span></strong><span
    style=''font-size:9.0pt;font-family:"Arial","sans-serif";color:#333333''><o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>You can access this application from anywhere by going to <a
    href="https://scheduling.universalweather.com" target="_blank">https://scheduling.universalweather.com</a>
    and logging in with the following credentials:<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Username: [[User Name]]<br>
    Password: [[Password]]<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>You will be required to change your password after logging back into the application. When you have successfully changed your password, you will be granted full access to the application.<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>Best regards,<o:p></o:p></span></p>
    <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
    0in;line-height:13.5pt''><span style=''font-size:9.0pt;font-family:"Arial","sans-serif";
    color:#333333''>[[Name]]<br>
    Universal Flight Scheduling Software<br>
    Universal Weather and Aviation, Inc.<br>
    Worldwide: (713) 944-1440 x 92050<br>
    N. America: (800) 231-8648 x 92050<br>
    E-mail: <a href="mailto:softwaresupport@univ-wea.com">softwaresupport@univ-wea.com</a><br>
    <a href="http://www.universalweather.com" target="_blank">universalweather.com</a><o:p></o:p></span></p>
    </td>
   </tr>
   <tr style=''mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.5pt''>
    <td style=''background:white;padding:0in 0in 0in 0in;height:22.5pt''></td>
   </tr>
  </table>
  </div>
  </td>
  <td width=49 valign=top style=''width:36.75pt;padding:0in 0in 0in 0in''></td>
  <td width=1 valign=top style=''width:.75pt;background:gainsboro;padding:0in 0in 0in 0in''></td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;height:11.25pt''>
  <td width=700 colspan=10 valign=top style=''width:525.0pt;background:#414141;
  padding:0in 0in 0in 0in;height:11.25pt''>
  <p style=''margin:0in;margin-bottom:.0001pt;mso-line-height-alt:11.25pt''><span
  style=''mso-no-proof:yes''><img border=0 width=700 height=15 id="_x0000_i1033"
  src="http://i.universalweather.com/_email/social/section1-top.png"
  alt="http://i.universalweather.com/_email/social/section1-top.png"></span><o:p></o:p></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:1;height:28.5pt''>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=124 style=''width:93.0pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><span style=''mso-no-proof:yes''><img
  border=0 width=124 height=38 id="_x0000_i1032"
  src="http://i.universalweather.com/_email/social/section1-connect.png"
  alt="http://i.universalweather.com/_email/social/section1-connect.png"></span><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://plus.google.com/100024822646419142787/posts" target="_blank"
  title="Google+"><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1031"
  src="http://i.universalweather.com/_email/social/section1-googleplus.png"
  alt="Google+"></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://twitter.com/#!/universalweathr" target="_blank" title=Twitter><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=30 height=38
  id="_x0000_i1030"
  src="http://i.universalweather.com/_email/social/section1-twitter.png"
  alt=Twitter></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.facebook.com/universalweathr" target="_blank"
  title=Facebook><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1029"
  src="http://i.universalweather.com/_email/social/section1-facebook.png"
  alt=Facebook></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.linkedin.com/company/universal-weather-and-aviation"
  target="_blank" title=LinkedIn><span style=''font-family:"Arial","sans-serif";
  color:#555555;mso-no-proof:yes;text-decoration:none;text-underline:none''><img
  border=0 width=30 height=38 id="_x0000_i1028"
  src="http://i.universalweather.com/_email/social/section1-linkedin.png"
  alt=LinkedIn></span></a><o:p></o:p></p>
  </td>
  <td width=30 style=''width:22.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.youtube.com/user/universalweather" target="_blank"
  title=YouTube><span style=''font-family:"Arial","sans-serif";color:#555555;
  mso-no-proof:yes;text-decoration:none;text-underline:none''><img border=0
  width=30 height=38 id="_x0000_i1027"
  src="http://i.universalweather.com/_email/social/section1-youtube.png"
  alt=YouTube></span></a><o:p></o:p></p>
  </td>
  <td width=185 style=''width:138.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=141 style=''width:105.75pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="http://www.universalweather.com/blog/" target="_blank" title=Blog><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=141 height=38
  id="_x0000_i1026"
  src="http://i.universalweather.com/_email/social/section1-blog.png"
  alt="Read our blog"></span></a><o:p></o:p></p>
  </td>
  <td width=50 style=''width:37.5pt;background:#414141;padding:0in 0in 0in 0in;
  height:28.5pt''>
  <p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=''mso-yfti-irow:2;mso-yfti-lastrow:yes;height:70.5pt''>
  <td width=700 colspan=10 valign=bottom style=''width:525.0pt;background:white;
  padding:0in 0in 0in 0in;height:70.5pt''>
  <p style=''margin:0in;margin-bottom:.0001pt''><a
  href="https://itunes.apple.com/us/app/universal-ops-brief-aviation/id467458235?mt=8"
  title="&quot;Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more.&quot; t "><span
  style=''font-family:"Arial","sans-serif";color:#555555;mso-no-proof:yes;
  text-decoration:none;text-underline:none''><img border=0 width=700 height=92
  id="_x0000_i1025"
  src="http://i.universalweather.com/_email/social/section2-app-oft-mobile.png"
  alt="Download our iPad� app | Access your flight plans, fuel pricing, ops briefs, and more."></span></a><o:p></o:p></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''font-family:"Arial","sans-serif";mso-fareast-font-family:
"Times New Roman";display:none;mso-hide:all''><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=700
 style=''width:525.0pt;mso-cellspacing:0in;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 0in 0in 0in''>
 <tr style=''mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes''>
  <td style=''padding:15.75pt 37.5pt 0in 37.5pt''>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>Universal is a registered trademark of Universal Weather and
  Aviation, Inc.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>� 2013 Universal Weather and Aviation Inc. 8787 Tallyho Road,
  Houston, Texas 77061-3420 USA.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>App Store and iPad are trademarks of Apple, Inc., registered
  in the U.S. and other countries.<o:p></o:p></span></p>
  <p style=''margin-top:0in;margin-right:0in;margin-bottom:16.2pt;margin-left:
  0in;line-height:13.5pt''><span style=''font-size:8.5pt;font-family:"Arial","sans-serif";
  color:#777777''>SP-GR014<o:p></o:p></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style=''mso-fareast-font-family:"Times New Roman"''><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
','Password Reset Confirmation - Universal Flight Scheduling Software',
10000,'UWA Admin','2013-02-02 00:00:00.000',0,0)



