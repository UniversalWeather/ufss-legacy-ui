
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLocatorCustom]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLocatorCustom]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetLocatorCustom]  
(  
 @CustomerID BIGINT,   
 @AirportID BIGINT,  
 @IcaoID char(4),   
 @Iata char(3),  
 @StateName varchar(25),  
 @CityName varchar(25),  
 @CountryCD char(3),  
 @CountryID BIGINT,   
 @MetroID BIGINT,   
 @MetroCD char(3),  
 @Name varchar(40),  
 @MilesFrom int  
  
 )    
AS    
-- =============================================    
-- Author:   
-- Create date:     
-- Description: Get the Locator information of Custom   
-- Modification History: Updated by Ramesh on 5th Nov 2013, for MapQuest plotting
--						 Added 2 new columns OrigLatitude and OrigLongitude
-- =============================================    
SET FMTONLY OFF   
   if  @IcaoID ='' 
		set @IcaoID = null
		 if @Iata ='' 
		set @Iata = null
		 if @CountryCD ='' 
		set @CountryCD = null
		 if @CityName ='' 
		set @CityName = null
		 if @MetroCD ='' 
		set @MetroCD = null
		 if @StateName ='' 
		set @StateName = null
		 if @Name ='' 
		set @Name = null
		  
  
SELECT   A.[AirportID]  
        ,A.[IcaoID]  
        ,A.[Iata]  
        ,CA.[CustomerID]  
       ,CA.CustomCity  
       , CA.CustomState     
  , CA.CountryID  
       ,CA.Name     
  ,CA.MetropolitanArea 
  ,C.CountryCD  
  ,0 as 'Miles' 
  ,CA.PhoneNum,
    --start added for miles calc
	case 
	when UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180 
	END LatitudeRad,	
	case
	when UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180 
	END LongitudeRad,	
 --end added for miles calc	     
--start added for miles calc
	case 
	when UPPER(CA.LatitudeNorthSouth) = 'S' Then (0-(CA.LatitudeDegree + (CA.LatitudeMinutes / 60))) * PI() / 180  else (CA.LatitudeDegree + (CA.LatitudeMinutes / 60)) * PI() / 180 
	END LatitudeRadCityCenter,
	case
	when UPPER(CA.LatitudeNorthSouth) = 'S' Then (0-(CA.LatitudeDegree + (CA.LatitudeMinutes / 60))) else (CA.LatitudeDegree + (CA.LatitudeMinutes / 60))
	END OrigLatitude,
	case
	when UPPER(CA.LongitudeEastWest) = 'W' Then (0-(CA.LongitudeDegrees + (CA.LongitudeMinutes / 60))) * PI() / 180  else (CA.LongitudeDegrees + (CA.LongitudeMinutes / 60)) * PI() / 180 
	END LongitudeRadCityCenter,
	case
	when UPPER(CA.LongitudeEastWest) = 'W' Then (0-(CA.LongitudeDegrees + (CA.LongitudeMinutes / 60))) else (CA.LongitudeDegrees + (CA.LongitudeMinutes / 60))
	END OrigLongitude 
--end added for miles calc			
	into  #TempResultTable	 
   
  FROM  [CustomAddress] CA 
     LEFT OUTER JOIN Airport A on CA.AirportID = A.AirportID   
      
  LEFT OUTER JOIN [Country] C on CA .CountryID = C.CountryID    
   --LEFT OUTER JOIN [Airport] A on CA .AirportID = A.AirportID  
     
   where 
   (((CA .CustomerID = @CustomerID)) 
   OR (ISNULL(@AirportID,0))=0 OR A.AirportID = @AirportID) and 
   
	ISNULL(A.IcaoID, '') = case when @IcaoID is not null then @IcaoID else ISNULL(A.IcaoID, '') end and
   ISNULL(A.Iata, '') = case when @Iata is not null then @Iata else ISNULL(A.Iata, '') end and
  
   
   ISNULL(C.CountryCD, '') = case when @CountryCD  is not null then @CountryCD  else ISNULL(C.CountryCD , '') end  and
   ISNULL(CA.MetropolitanArea, '') = case when @MetroCD is not null then @MetroCD else ISNULL(CA.MetropolitanArea, '') end and
  
	     ISNULL(CA.CustomCity , '') = case when @CityName  is not null then @CityName  else ISNULL(CA.CustomCity  , '') end  and
   ISNULL(CA.CustomState, '') = case when @StateName is not null then @StateName else ISNULL(CA.CustomState, '') end and
   ISNULL(CA.Name, '') = case when @Name  is not null then @Name  else ISNULL(CA.Name , '') end  and
      
    	     CA.IsDeleted = 0
    	     and CA .CustomerID = @CustomerID
  
 SELECT Custom.* FROM(   	     
SELECT   [AirportID]  
        ,[IcaoID]  
        ,[Iata]  
        ,[CustomerID]  
       ,CustomCity  
       ,CustomState     
  , CountryID  
       ,Name     
  ,MetropolitanArea 
  ,CountryCD  
  ,ISNull((cast( acos(sin(LatitudeRadCityCenter) * sin(LatitudeRad) + cos(LatitudeRadCityCenter) * cos(LatitudeRad) * cos(LongitudeRad - (LongitudeRadCityCenter))) * 3959   as Int)),0)  as 'Miles'
  ,PhoneNum, OrigLatitude, OrigLongitude
  from #TempResultTable
   )Custom  
   WHERE Miles <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END
  
   
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 

GO


