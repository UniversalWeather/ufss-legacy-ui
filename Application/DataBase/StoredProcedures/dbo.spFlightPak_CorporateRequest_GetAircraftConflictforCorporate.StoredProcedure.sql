/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAircraftConflictforCorporate]    Script Date: 02/19/2013 19:54:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetAircraftConflictforCorporate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAircraftConflictforCorporate]
GO


CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAircraftConflictforCorporate]       
(        
  @CRMainID BIGINT,  
  @FleetID BIGINT,  
  @CustomerID BIGINT,
  @DepartureGreenwichDTTM DATETIME,  
  @ArrivalGreenwichDTTM DATETIME  
 )        
AS        
SET NOCOUNT ON       
BEGIN        
 SELECT   
  PM.CRTripNUM,F.TailNum, PL.LegNUM, DA.IcaoID as 'DepartICaoID', AA.IcaoID as 'ArrivalICaoID', PM.RecordType, PL.DutyTYPE,  
  PL.DepartureGreenwichDTTM, PL.ArrivalGreenwichDTTM    
 FROM   
 CRMain PM   
  inner join  CRLeg PL on PM.CRMainID = PL.CRMainID  
  inner join Airport DA on DA.AirportID = PL.DAirportID  
  inner join Airport AA on AA.AirportID = PL.AAirportID  
  inner join Fleet F on PM.FleetID = F.FleetID  
 where   
  PM.CRMainID <> @CRMainID  
  and PM.FleetID = @FleetID  
  and PM.CustomerID=@CustomerID
  and (PM.RecordType= 'W') 
  --and PM.TripStatus in ('T', 'H')  
  --or PM.RecordType = 'M') 
  and PM.IsDeleted =0 
  and PL.DepartureGreenwichDTTM <= @ArrivalGreenwichDTTM AND PL.ArrivalGreenwichDTTM >= @DepartureGreenwichDTTM  
END  
  
  


GO


