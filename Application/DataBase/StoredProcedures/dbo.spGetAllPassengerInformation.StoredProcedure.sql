
/****** Object:  StoredProcedure [dbo].[spGetAllPassengerInformation]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllPassengerInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllPassengerInformation]
GO

CREATE PROCEDURE [dbo].[spGetAllPassengerInformation] (@CustomerID BIGINT,@ClientID BIGINT)    
AS    
-- =============================================    
-- Author: Akhila.A    
-- Create date: 3/5/2012    
-- Description: Get All Passenger Information    
-- =============================================    
SET NOCOUNT ON    
  if(@ClientID>0)  
  begin  
SELECT PassengerInformationID    
 ,CustomerID    
 ,PassengerInfoCD    
 ,PassengerDescription    
 ,ClientID    
 ,LastUpdUID    
 ,LastUpdTS    
 ,IsShowOnTrip    
 ,IsDeleted
 ,IsInactive  
 ,IsCheckList  
FROM [PassengerInformation]    
WHERE     
 CustomerID = @CustomerID AND    ISNULL(IsDeleted, 0) = 0  and ClientID=@ClientID  
 order by PassengerInfoCD
 end  
 else  
 begin  
 SELECT PassengerInformationID    
 ,CustomerID    
 ,PassengerInfoCD    
 ,PassengerDescription    
 ,ClientID    
 ,LastUpdUID    
 ,LastUpdTS    
 ,IsShowOnTrip    
 ,IsDeleted
 ,IsInactive 
 ,IsCheckList   
FROM [PassengerInformation]    
WHERE     
 CustomerID = @CustomerID AND    ISNULL(IsDeleted, 0) = 0 
 order by PassengerInfoCD  
 end