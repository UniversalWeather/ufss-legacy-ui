

/****** Object:  StoredProcedure [dbo].[spAddVendor]    Script Date: 03/15/2013 18:52:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddVendor]
GO



/****** Object:  StoredProcedure [dbo].[spAddVendor]    Script Date: 03/15/2013 18:52:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddVendor]        
 (      
 @CustomerID BIGINT,        
 @VendorCD char(5),        
 @Name varchar(40),        
 @VendorType char(1),        
 @IsApplicationFiled bit,        
 @IsApproved bit,        
 @IsDrugTest bit,        
 @IsInsuranceCERT bit,        
 @IsFAR135Approved bit,        
 @IsFAR135CERT bit,        
 @AdditionalInsurance varchar(60),        
 @Contact varchar(60),        
 @VendorContactName varchar(40),        
 @BillingName varchar(40),        
 @BillingAddr1 varchar(40),        
 @BillingAddr2 varchar(40),        
 @BillingCity varchar(40),        
 @BillingState varchar(10),        
 @BillingZip varchar(15),        
 @CountryID bigint,        
 @MetroID bigint,        
 @BillingPhoneNum varchar(25),        
 @BillingFaxNum varchar(25),        
 @Notes varchar(max),        
 @Credit NUMERIC(17,2),        
 @DiscountPercentage NUMERIC(5,2),        
 @TaxID varchar(15),        
 @Terms varchar(25),        
 @IsInActive bit,        
 @HomebaseID bigint,        
 @LatitudeDegree NUMERIC(3,0),        
 @LatitudeMinutes NUMERIC(5,1),        
 @LatitudeNorthSouth char(1),        
 @LongitudeDegree NUMERIC(3,0),        
 @LongitudeMinutes NUMERIC(5,1),        
 @LongitudeEastWest char(1),        
 @AirportID bigint,        
 @LastUpdUID varchar(30),        
 @LastUpdTS datetime,        
 @IsTaxExempt bit,        
 @DateAddedDT datetime,        
 @EmailID varchar(100),        
 @WebAddress varchar(100),        
 @IsDeleted bit         
 ,@TollFreePhone VARCHAR(25)  
,@BusinessEmail VARCHAR(250)  
,@Website VARCHAR(200)  
,@BillingAddr3 VARCHAR(40)  
,@BusinessPhone VARCHAR(25)  
,@CellPhoneNum VARCHAR(25)  
,@HomeFax VARCHAR(25)  
,@CellPhoneNum2 VARCHAR(25)  
,@OtherPhone VARCHAR(25)  
,@MCBusinessEmail VARCHAR(250)  
,@PersonalEmail VARCHAR(250)  
,@OtherEmail VARCHAR(250)  
,@CertificateNumber VARCHAR(50)
,@SITANumber VARCHAR(100)   
)        
-- =============================================        
-- Author: Karthikeyan.S        
-- Create date: 19/4/2012        
-- Modified by Mathes 01-07-12      
-- Description: Insert the Vendor  information        
-- =============================================        
AS        
 BEGIN     
 SET NoCOUNT ON    
 DECLARE @VendorID BIGINT    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @VendorID OUTPUT  
 select @VendorID as VendorID
   
 set @LastUpdTS = GETUTCDATE()    
        
   INSERT INTO [Vendor]                    
      ([VendorID]       
      ,[CustomerID]        
      ,[VendorCD]        
      ,[Name]        
      ,[VendorType]        
      ,[IsApplicationFiled]        
      ,[IsApproved]        
      ,[IsDrugTest]        
      ,[IsInsuranceCERT]        
      ,[IsFAR135Approved]        
      ,[IsFAR135CERT]        
      ,[AdditionalInsurance]        
      ,[Contact]        
      ,[VendorContactName]        
      ,[BillingName]        
      ,[BillingAddr1]        
      ,[BillingAddr2]        
      ,[BillingCity]        
      ,[BillingState]        
      ,[BillingZip]        
      ,[CountryID]         
      ,[MetroID]         
      ,[BillingPhoneNum]        
      ,[BillingFaxNum]        
      ,[Notes]        
      ,[Credit]        
      ,[DiscountPercentage]        
      ,[TaxID]        
      ,[Terms]        
      ,[IsInActive]        
      ,[HomebaseID]         
      ,[LatitudeDegree]        
      ,[LatitudeMinutes]        
      ,[LatitudeNorthSouth]        
      ,[LongitudeDegree]        
      ,[LongitudeMinutes]        
      ,[LongitudeEastWest]        
      ,[AirportID]         
      ,[LastUpdUID]        
      ,[LastUpdTS]        
      ,[IsTaxExempt]        
      ,[DateAddedDT]        
      ,[EmailID]        
      ,[WebAddress]        
      ,[IsDeleted]  
  ,TollFreePhone  
  ,BusinessEmail  
  ,Website  
  ,BillingAddr3  
  ,BusinessPhone  
  ,CellPhoneNum  
  ,HomeFax  
  ,CellPhoneNum2  
  ,OtherPhone  
  ,MCBusinessEmail  
  ,PersonalEmail  
  ,OtherEmail
  ,CertificateNumber
  ,SITA)        
     VALUES        
           (       
             @VendorID,      
             @CustomerID,        
    @VendorCD,        
    @Name,        
    @VendorType,        
    @IsApplicationFiled,        
    @IsApproved,        
    @IsDrugTest,        
    @IsInsuranceCERT,        
    @IsFAR135Approved,        
    @IsFAR135CERT,        
    @AdditionalInsurance,        
    @Contact,        
    @VendorContactName,        
    @BillingName,        
    @BillingAddr1,        
    @BillingAddr2,        
    @BillingCity,        
    @BillingState,        
    @BillingZip,        
    @CountryID,        
    @MetroID,        
    @BillingPhoneNum,        
    @BillingFaxNum,        
    @Notes,        
    @Credit,        
    @DiscountPercentage,        
    @TaxID,        
    @Terms,        
    @IsInActive,        
    @HomebaseID,        
    @LatitudeDegree,        
    @LatitudeMinutes,        
    @LatitudeNorthSouth,        
    @LongitudeDegree,        
    @LongitudeMinutes,        
    @LongitudeEastWest,        
    @AirportID,        
    @LastUpdUID,        
    @LastUpdTS,        
    @IsTaxExempt,        
    @DateAddedDT,        
    @EmailID,        
    @WebAddress,        
    @IsDeleted        
    ,@TollFreePhone  
 ,@BusinessEmail  
 ,@Website  
 ,@BillingAddr3  
 ,@BusinessPhone  
 ,@CellPhoneNum  
 ,@HomeFax  
 ,@CellPhoneNum2  
 ,@OtherPhone  
 ,@MCBusinessEmail  
 ,@PersonalEmail  
 ,@OtherEmail
 ,@CertificateNumber
 ,@SITANumber)  
END

GO


