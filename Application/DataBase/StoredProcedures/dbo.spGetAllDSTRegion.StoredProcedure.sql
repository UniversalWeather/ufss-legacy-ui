IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllDSTRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllDSTRegion]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllDSTRegion]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllDSTRegion]  
as  
set nocount on  
begin  

SELECT [DSTRegionID]
      ,[DSTRegionCD]  
      ,[DSTRegionName]  
      ,[StartDT] = CASE Year(StartDT) WHEN  1900 THEN null
					ELSE [StartDT]
					END
      ,[EndDT] = CASE Year(EndDT) WHEN 1900 THEN null
				ELSE [EndDT]
				END
      ,[OffSet]  
      ,[CityList]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]
      ,[UpdateDT]
      ,[IsInactive]
      ,[UWAUpdates]  
  FROM [DSTRegion] where IsDeleted='false'  
  order by DSTRegionCD
END
GO
