
/****** Object:  StoredProcedure [dbo].[sp_fss_GetDepartmentAuthorization]    Script Date: 02/28/2014 13:27:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetDepartmentAuthorization]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetDepartmentAuthorization]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetDepartmentAuthorization]    Script Date: 02/28/2014 13:27:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetDepartmentAuthorization]  
(  
 @CustomerID  BIGINT  
 ,@ClientID BIGINT  
 ,@DepartmentID BIGINT   
 ,@AuthorizationID BIGINT  
 ,@AuthorizationCD varchar(8)    
 ,@FetchActiveOnly BIT      
)    
AS   
-- =============================================              
-- Author: Sridhar             
-- Create date: 26/02/2014              
-- Description: Get All GetDepartmentAuthorization details based on Department for Popup      
-- Exec sp_fss_GetDepartmentAuthorization 10099,NULL,10099170,0,'',0  
-- Exec sp_fss_GetDepartmentAuthorization 10099,NULL,10099170,0,'',1    
-- Exec sp_fss_GetDepartmentAuthorization 10099,NULL,10099168,1009948959,'',0  
-- Exec sp_fss_GetDepartmentAuthorization 10099,NULL,10099170,0,'MAINT',0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
 SELECT   
   AuthorizationID  
   ,CASE WHEN AuthorizationCD IS NULL THEN '' ELSE LTRIM(RTRIM(AuthorizationCD)) END AS AuthorizationCD   
   ,CustomerID  
   ,DepartmentID  
   ,DeptAuthDescription  
   ,ClientID   
   ,LastUpdTS   
   ,LastUpdUID   
   ,AuthorizerPhoneNum   
   ,ISNULL(IsDeleted,0) IsDeleted  
   ,ISNULL(IsInactive,0) IsInactive              
  FROM   
  DepartmentAuthorization  
  WHERE   
  CustomerID = @CustomerID and DepartmentID = @DepartmentID    
  AND ISNULL(ClientID,0) = Case when @ClientID > 0 then @ClientID else  ISNULL(ClientID,0) end  
  AND AuthorizationID = case when @AuthorizationID <> 0 then @AuthorizationID else AuthorizationID end   
  AND AuthorizationCD = case when Ltrim(Rtrim(@AuthorizationCD)) <> '' then @AuthorizationCD else AuthorizationCD end                                    
  AND ISNULL(IsInActive,0) = case when @FetchActiveOnly =1 then ISNULL(IsInActive,0) else 0 end  
  AND ISNULL(IsDeleted,0) = 0                                  
 ORDER BY AuthorizationCD ASC          
END 
GO

