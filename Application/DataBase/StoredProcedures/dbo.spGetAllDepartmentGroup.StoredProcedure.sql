
GO
/****** Object:  StoredProcedure [dbo].[spGetAllDepartmentGroup]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllDepartmentGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllDepartmentGroup]
GO

CREATE Procedure [dbo].[spGetAllDepartmentGroup](@CustomerID bigint)
AS
-- =============================================
-- Author: Sujitha.V
-- Create date: 10/7/2012
-- Description: Get All Department Group Informations
-- =============================================
SET NOCOUNT ON

SELECT DepartmentGroup.[CustomerID]    
      ,DepartmentGroup.[HomebaseID]    
      ,DepartmentGroup.DepartmentGroupID    
      ,CASE WHEN DepartmentGroup.[DepartmentGroupCD] IS NULL THEN '' ELSE LTRIM(RTRIM(DepartmentGroup.[DepartmentGroupCD])) END AS DepartmentGroupCD
      ,DepartmentGroup.[DepartmentGroupDescription]    
      ,DepartmentGroup.[LastUpdUID]    
      ,DepartmentGroup.[LastUpdTS]    
      ,DepartmentGroup.[IsDeleted]
      ,DepartmentGroup.[IsInActive]
      --,Company.HomebaseCD as HomebaseCD    
      ,Airport.ICAOID AS HomebaseCD  
      FROM  [DepartmentGroup] as DepartmentGroup    
 LEFT OUTER JOIN Company as Company on Company.HomebaseID = DepartmentGroup.HomebaseID    
 LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID        
     
 WHERE DepartmentGroup.CustomerID=@CustomerID AND ISNULL(DepartmentGroup.IsDeleted,0) = 0
 order by DepartmentGroup.[DepartmentGroupCD]  
GO
