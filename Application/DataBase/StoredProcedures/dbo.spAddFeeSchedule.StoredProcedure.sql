
GO
/****** Object:  StoredProcedure [dbo].[spAddFeeSchedule]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFeeSchedule]
GO

CREATE PROCEDURE spAddFeeSchedule
(@CustomerID BIGINT,
@FeeGroupID BIGINT,
@AccountID BIGINT,
@Amount NUMERIC(17,2),
@IsTaxable BIT,
@IsDiscount BIT,
@IsInActive BIT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	DECLARE @FeeScheduleID BIGINT
	SET @LastUpdTS = GETUTCDATE()
	
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FeeScheduleID OUTPUT
	
	INSERT INTO [FeeSchedule]
           ([FeeScheduleID]
           ,[CustomerID]
           ,[FeeGroupID]
           ,[AccountID]
           ,[Amount]
           ,[IsTaxable]
           ,[IsDiscount]
           ,[IsInActive]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted])
     VALUES
           (@FeeScheduleID
           ,@CustomerID
           ,@FeeGroupID
           ,@AccountID
           ,@Amount
           ,@IsTaxable
           ,@IsDiscount
           ,@IsInActive
           ,@LastUpdUID
           ,@LastUpdTS
           ,@IsDeleted)
           
	SELECT @FeeScheduleID AS FeeScheduleID

END