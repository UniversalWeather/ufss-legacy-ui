

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightFBO]    Script Date: 09/24/2013 00:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightFBO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightFBO]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightFBO]    Script Date: 09/24/2013 00:01:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightFBO]    
 (             
           @PreflightFBOName varchar(60),    
           @LegID bigint,    
           @FBOID bigint,    
           @IsArrivalFBO bit,    
           @IsDepartureFBO bit,    
           @AirportID bigint,    
           @Street varchar(50),    
           @CityName varchar(20),    
           @StateName varchar(20),    
           @PostalZipCD char(20),    
           @PhoneNum1 varchar(25),    
           @PhoneNum2 varchar(25),    
           @PhoneNum3 varchar(25),    
           @PhoneNum4 varchar(25),    
           @FaxNUM varchar(25),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @NoUpdated bit,    
           @FBOArrangedBy bit,    
           @IsCompleted bit,    
           @FBOInformation varchar(100),    
           @ConfirmationStatus varchar(max),    
           @Comments varchar(max),    
           @CustomerID BIGINT,    
           @Status varchar(20)     
    )AS    
BEGIN     
SET NOCOUNT ON    
    
    
DECLARE @PreflightFBOID BIGINT       
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @PreflightFBOID OUTPUT    
    
       
INSERT INTO [dbo].[PreflightFBOList]    
           ([PreflightFBOID]    
           ,[PreflightFBOName]    
           ,[LegID]    
           ,[FBOID]    
           ,[IsArrivalFBO]    
           ,[IsDepartureFBO]    
           ,[AirportID]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[NoUpdated]    
           ,[FBOArrangedBy]    
           ,[IsCompleted]    
           ,[FBOInformation]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[Status]    
           )    
     VALUES    
           (    
           @PreflightFBOID ,    
           @PreflightFBOName ,    
           @LegID ,    
           @FBOID ,    
           @IsArrivalFBO ,    
           @IsDepartureFBO ,    
           @AirportID ,    
           @Street ,    
           @CityName ,    
           @StateName ,    
           @PostalZipCD ,    
           @PhoneNum1 ,    
           @PhoneNum2 ,    
           @PhoneNum3 ,    
           @PhoneNum4 ,    
           @FaxNUM ,    
           @LastUpdUID ,    
           @LastUpdTS ,    
           @IsDeleted ,    
           @NoUpdated,    
           @FBOArrangedBy,    
           @IsCompleted,    
           @FBOInformation,    
           @ConfirmationStatus,    
           @Comments,    
           @CustomerID,    
     @Status    
           )    
               
               
           select @PreflightFBOID as 'PreflightFBOID'    
               
END    
    
    



GO


