

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAirportForGrid]    Script Date: 02/28/2014 13:34:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAirportForGrid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAirportForGrid]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetAirportForGrid]    Script Date: 02/28/2014 13:34:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

          
CREATE PROCEDURE [dbo].[sp_fss_GetAirportForGrid]  
(  
 @CustomerID BIGINT   
 ,@AirportID BIGINT  
    ,@ICAOID char(4)   
 ,@FetchRunwayOnly BIGINT=0  
 ,@FetchActiveOnly BIT  
)   
AS  
-- =============================================              
-- Author: Sridhar             
-- Create date: 27/02/2014              
-- Description: Get All AirportForGrid details for Popup        
-- Exec sp_fss_GetAirportForGrid 10099,0,'',0,0  
-- Exec sp_fss_GetAirportForGrid 10099,0,'',0,1  
-- Exec sp_fss_GetAirportForGrid 10099,1009943868,'',0,1  
-- Exec sp_fss_GetAirportForGrid 10099,0,'KDAL',0,1  
-- Exec sp_fss_GetAirportForGrid 10099,0,'',10000,0  
-- Exec sp_fss_GetAirportForGrid 10099,0,'',10000,1  
  
  
-- =============================================            
  
SET NOCOUNT ON   
IF LEN(@CustomerID) > 0                
BEGIN             
 DECLARE @UWACustomerID BIGINT            
 SELECT @UWACustomerID  = dbo.GetUWACustomerID()             
            
 Select A.AirportID,  A.CustomerID,  A.IcaoID,  A.CityName,   A.StateName,  A.CountryID, C.CountryCD,   
   A.AirportName, A.OffsetToGMT,  A.IsEntryPort,    
    ISNULL(A.IsInactive,0)  IsInactive      
    ,A.LastUpdUID,  A.LastUpdTS, A.IsWorldClock,  
   (SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,  
   A.UWAID,  A.Iata,          
   isnull(LongestRunway,0) as [MaxRunway], C.CountryName    
   --(SELECT Max(v)             
   --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length) ,(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]          
 from Airport A             
   left join Country C on C.CountryID = A.CountryID    
 where A.CustomerID = @CustomerID   
 AND A.AirportID = case when @AirportID <> 0 then @AirportID else A.AirportID end   
 AND A.ICAOID    = case when Ltrim(Rtrim(@ICAOID)) <> '' then @ICAOID else A.ICAOID end   
 and ISNULL(LongestRunway,0) >= @FetchRunwayOnly   
 AND ISNULL(A.IsInActive,0) = case when @FetchActiveOnly = 1 then ISNULL(A.IsInActive,0) else 0 end  
 AND ISNULL(A.IsDeleted,0) = 0   
           
   
 union all            
   
 Select A.AirportID,  A.CustomerID,  A.IcaoID,  A.CityName,   A.StateName,  A.CountryID, C.CountryCD,   
   A.AirportName, A.OffsetToGMT, A.IsEntryPort,        
     ISNULL(A.IsInactive,0) IsInactive  
    ,A.LastUpdUID,  A.LastUpdTS, A.IsWorldClock,  
   (SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,  
   A.UWAID,A.Iata,           
   isnull(LongestRunway,0) as [MaxRunway], C.CountryName         
   --(SELECT Max(v)             
   --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length),(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]          
 from Airport A             
   left join Country C on C.CountryID = A.CountryID    
 where A.CustomerID <> @CustomerID and A.CustomerID = @UWACustomerID    
   AND A.AirportID = case when @AirportID <> 0 then @AirportID else A.AirportID end   
   AND A.ICAOID    = case when Ltrim(Rtrim(@ICAOID)) <> '' then @ICAOID else A.ICAOID end  
   and ISNULL(LongestRunway,0) >= @FetchRunwayOnly  
   AND ISNULL(A.IsDeleted,0) = 0   
   AND ISNULL(A.IsInActive,0) = case when @FetchActiveOnly = 1 then ISNULL(A.IsInActive,0) else 0 end  
   and IcaoID Not in (Select distinct IcaoID  from Airport where CustomerID = @CustomerID AND ISNULL(IsDeleted,0) = 0)   
 order by CityName,IcaoID                              
END 
GO

