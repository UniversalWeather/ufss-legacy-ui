

/****** Object:  StoredProcedure [dbo].[spGetAllFleetforCR]    Script Date: 04/30/2013 18:45:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetforCR]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetforCR]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllFleetforCR]    Script Date: 04/30/2013 18:45:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE  Procedure [dbo].[spGetAllFleetforCR](@TravelID bigint)          
AS          
-- =============================================          
-- Author: Karthikeyan.S          
-- Create date: 4/30/2012          
-- Description: Get All Fleet Profile Informations          
-- =============================================          
SET NOCOUNT ON          
SELECT Fleet.[FleetID]        
      ,[TailNum]        
      ,Fleet.[CustomerID]         
      ,Fleet.[AircraftCD]  
      ,Aircraft.AircraftCD as AirCraft_AircraftCD       
      ,[SerialNum]        
      ,[TypeDescription]        
      ,[MaximumReservation]        
      ,[LastInspectionDT]        
      ,[InspectionHrs]        
      ,[WarningHrs]        
      ,[MaximumPassenger]        
      ,Fleet.[HomebaseID]        
      ,[MaximumFuel]        
      ,[MinimumFuel]        
      ,[BasicOperatingWeight]        
      ,[MimimumRunway]        
      ,Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive       
      ,[IsDisplay31]        
      ,[SIFLMultiControlled]        
      ,[SIFLMultiNonControled]        
      ,Fleet.[Notes]        
      ,[ComponentCD]        
      ,Fleet.[ClientID]        
      ,[FleetType]        
      ,[FlightPhoneNum]        
      ,[Class]        
      ,Fleet.[VendorID]        
      ,Fleet.[VendorType]        
      ,[YearMade]        
      ,[ExteriorColor]        
      ,[InteriorColor]        
      ,[IsAFIS]        
      ,[IsUWAData]        
      ,Fleet.[LastUpdUID]        
      ,Fleet.[LastUpdTS]        
      ,[FlightPlanCruiseSpeed]        
      ,[FlightPlanMaxFlightLevel]        
      ,[MaximumTakeOffWeight]        
      ,[MaximumLandingWeight]        
      ,[MaximumWeightZeroFuel]        
      ,[TaxiFuel]        
      ,[MultiSec]        
      ,[ForeGrndCustomColor]        
      ,[BackgroundCustomColor]        
      ,[FlightPhoneIntlNum]        
      ,[FleetSize]        
      ,[FltScanDoc]        
      ,[MinimumDay]        
      ,[StandardCrewIntl]        
      ,[StandardCrewDOM]        
      ,[IsFAR91]        
      ,[IsFAR135]        
      ,FLEET.[EmergencyContactID]        
      ,[IsTaxDailyAdj]        
      ,[IsTaxLandingFee]        
      ,Fleet.[IsDeleted]        
      ,Fleet.[AircraftID]        
      ,Fleet.[CrewID]    
      ,SecondaryDomFlightPhone    
       ,SecondaryIntlFlightPhone  
       ,Client.ClientCD  
       ,Vendor.VendorCD  
       ,Airport.IcaoID  
       ,Aircraft.PowerSettings1TrueAirSpeed  
       ,Aircraft.PowerSettings2TrueAirSpeed  
       ,Aircraft.PowerSettings3TrueAirSpeed 
       ,Aircraft.IsFixedRotary 
       
         FROM [Fleet]   LEFT OUTER JOIN                
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN     
                      Airport ON Company.HomebaseAirportID=Airport.AirportID LEFT OUTER JOIN            
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                
                      Vendor ON Fleet.VendorID = Vendor.VendorID  LEFT OUTER JOIN  
                      TravelCoordinatorFleet as TravelFleet on TravelFleet.FleetID = Fleet.FleetID                  
                WHERE TravelFleet.TravelCoordinatorID = @TravelID AND ISNULL(Fleet.IsDeleted,0) = 0 AND ISNULL(TravelFleet.IsDeleted,0) = 0                           
                      Order by Fleet.AircraftCD                          


GO


