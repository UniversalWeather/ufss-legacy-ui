
GO
/****** Object:  StoredProcedure [dbo].[spAddCrewDutyRules]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCrewDutyRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCrewDutyRules]
go
CREATE Procedure [dbo].[spAddCrewDutyRules](
    
    @CrewDutyRuleCD char(4),
	@CustomerID bigint ,
	@CrewDutyRulesDescription varchar(30), 
	@FedAviatRegNum char(3) ,
	@DutyDayBeingTM NUMERIC(6,3) ,
	@DutyDayEndTM NUMERIC(6,3) ,
	@MaximumDutyHrs NUMERIC(6,3) ,
	@MaximumFlightHrs NUMERIC(6,3) ,
	@MinimumRestHrs NUMERIC(6,3),
	@RestMultiple NUMERIC(4,2),
	@RestMultipleHrs NUMERIC(6,3),
	@LastUpdUID varchar(30) ,
	@LastUptTM datetime,
    @IsDeleted bit,
    @IsInActive bit )
-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 30/4/2012
-- Description: Insert the CrewDutyRules information
-- =============================================
as
begin 
SET NoCOUNT ON

DECLARE @CrewDutyRulesID BIGINT
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @CrewDutyRulesID OUTPUT
--End of Primary Key generation

set @LastUptTM = GETUTCDATE()
   INSERT INTO [dbo].[CrewDutyRules]
    (
    CrewDutyRulesID,
    CrewDutyRuleCD,
    CustomerID ,
	CrewDutyRulesDescription , 
	FedAviatRegNum  ,
	DutyDayBeingTM  ,
	DutyDayEndTM  ,
	MaximumDutyHrs  ,
	MaximumFlightHrs  ,
	MinimumRestHrs ,
	RestMultiple,
	RestMultipleHrs,
	LastUpdUID,
	LastUptTM,
    IsDeleted,
    IsInActive )

     VALUES
           (
    @CrewDutyRulesID,
    @CrewDutyRuleCD,
    @CustomerID ,
	@CrewDutyRulesDescription, 
	@FedAviatRegNum  ,
	@DutyDayBeingTM  ,
	@DutyDayEndTM  ,
	@MaximumDutyHrs  ,
	@MaximumFlightHrs  ,
	@MinimumRestHrs ,
	@RestMultiple,
	@RestMultipleHrs,
	@LastUpdUID,
	@LastUptTM,
    @IsDeleted,
    @IsInActive
        )


END
GO
