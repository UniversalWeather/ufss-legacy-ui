/****** Object:  StoredProcedure [dbo].[spAddPassengerRequestor]    Script Date: 03/18/2013 12:43:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPassengerRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPassengerRequestor]
GO

/****** Object:  StoredProcedure [dbo].[spAddPassengerRequestor]    Script Date: 03/18/2013 12:43:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spAddPassengerRequestor]                
(@PassengerRequestorCD varchar(5)                
,@PassengerName varchar(63)                
,@CustomerID bigint                
,@DepartmentID bigint                
,@PassengerDescription varchar(25)                
,@PhoneNum varchar(25)                
,@IsEmployeeType char(1)                
,@StandardBilling varchar(25)                
,@AuthorizationID bigint                
,@AuthorizationDescription varchar(25)                
,@Notes varchar(max)                
,@LastName varchar(20)                
,@FirstName varchar(20)                
,@MiddleInitial varchar(20)                
,@DateOfBirth date                
,@ClientID bigint                
,@AssociatedWithCD char(5)                
,@HomebaseID bigint                
,@IsActive bit                
,@FlightPurposeID bigint                
,@SSN varchar(9)                
,@Title varchar(30)                
,@SalaryLevel numeric(4,0)                
,@IsPassengerType numeric(1,0)                
,@LastUpdUID varchar(30)                
,@LastUpdTS datetime                
,@IsScheduledServiceCoord bit                
,@CompanyName varchar(60)                
,@EmployeeID varchar(25)                
,@FaxNum varchar(25)                
,@EmailAddress varchar(250)                
,@PersonalIDNum nvarchar(40)                
,@IsSpouseDependant bit                
,@AccountID bigint                
,@CountryID bigint                
,@IsSIFL bit                
,@PassengerAlert varchar(max)                
,@Gender char(1)                
,@AdditionalPhoneNum varchar(25)                
,@IsRequestor bit                
,@CountryOfResidenceID bigint                
,@PaxScanDoc varchar(max)                
,@TSAStatus char(1)                
,@TSADTTM datetime                
,@Addr1 varchar(40)                
,@Addr2 varchar(40)                
,@City varchar(25)                
,@StateName varchar(10)                
,@PostalZipCD varchar(15)                
,@IsDeleted bit      
,@PrimaryMobile varchar(25)      
,@BusinessFax varchar(25)      
,@CellPhoneNum2 varchar(25)      
,@OtherPhone varchar(25)      
,@PersonalEmail varchar(250)      
,@OtherEmail varchar(250)      
,@Addr3 varchar(40)      
,@PassengerWeight NUMERIC(5,2)      
,@PassengerWeightUnit CHAR(3)      
,@Birthday DATE      
,@Anniversaries DATE      
,@EmergencyContacts VARCHAR(100)      
,@CreditcardInfo VARCHAR(100)      
,@CateringPreferences VARCHAR(100)      
,@HotelPreferences VARCHAR(100)      
,@INSANumber VARCHAR(60)      
,@Category VARCHAR(30)      
,@CardExpires DATE      
,@ResidentSinceYear INT      
,@GreenCardGender CHAR(1)      
,@GreenCardCountryID BIGINT    
,@cqcustomerid BIGINT    
,@PaxTitle VARCHAR(MAX)    
,@CityOfBirth VARCHAR(25)    
,@Nickname VARCHAR(50))      
                    
 AS                    
BEGIN                    
-- ====================================================                    
-- Author: Srinivasan. J                    
-- Create date: 05/06/2012                    
-- Description: Add/ Update Passenger Requestor Information                    
-- ====================================================                    
SET NOCOUNT OFF      
      
  IF EXISTS(SELECT TSANoFlyID FROM TSANoFly       
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
      )-- AND DOB=@DateOfBirth)      
   BEGIN      
    SET @TSAStatus = 'R'      
    SELECT @TSADTTM = TSATime FROM TSANoFly       
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
   END      
  IF EXISTS(SELECT TSASelectID FROM TSASelect      
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
      )-- AND DOB=@DateOfBirth)      
   BEGIN      
    SET @TSAStatus = 'S'      
    SELECT @TSADTTM = TSATime FROM TSASelect      
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
   END      
  IF EXISTS(SELECT TSACleartID FROM TSAClear       
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
      )-- AND DOB=@DateOfBirth)      
   BEGIN      
    SET @TSAStatus = 'C'      
    SELECT @TSADTTM = TSATime FROM TSAClear       
     WHERE CustomerID=@CustomerID AND       
      (ISNULL(FirstName,'')=ISNULL(@FirstName,'') AND       
       ISNULL(MiddleName,'')=ISNULL(@MiddleInitial,'') AND       
       ISNULL(LastName,'')=ISNULL(@LastName,''))      
   END      
      
DECLARE @PassengerRequestorID as BIGINT              
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassengerRequestorID OUTPUT             
    SELECT @PassengerRequestorID AS PassengerRequestorID              
DECLARE @PaxVal char(5)          
EXECUTE dbo.Usp_getcrewpaxcodegeneration @CustomerID,@PassengerRequestorCD,'Passenger',@PaxVal OUTPUT                    
  set @LastUpdTS = GETUTCDATE()              
INSERT INTO [Passenger]                
   ([PassengerRequestorID]                
   ,[PassengerRequestorCD]                
   ,[PassengerName]                
   ,[CustomerID]                
   ,[DepartmentID]                
   ,[PassengerDescription]                
   ,[PhoneNum]                
   ,[IsEmployeeType]                
   ,[StandardBilling]                
   ,[AuthorizationID]                
   ,[AuthorizationDescription]                
   ,[Notes]                
   ,[LastName]                
   ,[FirstName]                
   ,[MiddleInitial]                
   ,[DateOfBirth]                
   ,[ClientID]                
   ,[AssociatedWithCD]                
   ,[HomebaseID]                
   ,[IsActive]                
   ,[FlightPurposeID]                
   ,[SSN]                
   ,[Title]                
   ,[SalaryLevel]                
   ,[IsPassengerType]                
   ,[LastUpdUID]                
   ,[LastUpdTS]                
   ,[IsScheduledServiceCoord]                
   ,[CompanyName]                
   ,[EmployeeID]                
   ,[FaxNum]                
   ,[EmailAddress]                
   ,[PersonalIDNum]                
   ,[IsSpouseDependant]                
   ,[AccountID]                
   ,[CountryID]                
   ,[IsSIFL]                
   ,[PassengerAlert]                
   ,[Gender]                
   ,[AdditionalPhoneNum]                
   ,[IsRequestor]                
   ,[CountryOfResidenceID]                
   ,[PaxScanDoc]                
   ,[TSAStatus]                
   ,[TSADTTM]                
   ,[Addr1]                
   ,[Addr2]                
   ,[City]                
   ,[StateName]                
   ,[PostalZipCD]                
   ,[IsDeleted]      
   ,[PrimaryMobile]      
   ,[BusinessFax]      
   ,[CellPhoneNum2]      
   ,[OtherPhone]      
   ,[PersonalEmail]      
   ,[OtherEmail]      
   ,[Addr3]      
   ,PassengerWeight      
   ,PassengerWeightUnit      
   ,Birthday      
   ,Anniversaries      
   ,EmergencyContacts      
   ,CreditcardInfo      
   ,CateringPreferences      
   ,HotelPreferences      
   ,INSANumber      
   ,Category      
   ,CardExpires      
   ,ResidentSinceYear      
   ,GreenCardGender      
   ,GreenCardCountryID    
   ,cqcustomerid     
   ,PaxTitle    
   ,CityOfBirth    
   ,Nickname     
   )                
     VALUES                
   (@PassengerRequestorID             
   ,@PaxVal  --@PassengerRequestorCD                
   ,@PassengerName                
   ,@CustomerID                
   ,@DepartmentID                
   ,@PassengerDescription                
   ,@PhoneNum                
   ,@IsEmployeeType                
   ,@StandardBilling                
   ,@AuthorizationID                
   ,@AuthorizationDescription                
   ,@Notes                
   ,@LastName                
   ,@FirstName                
   ,@MiddleInitial                
   ,@DateOfBirth                
   ,@ClientID                
   ,@AssociatedWithCD                
   ,@HomebaseID          
   ,@IsActive                
   ,@FlightPurposeID                
   ,@SSN                
   ,@Title                
   ,@SalaryLevel                
   ,@IsPassengerType                
   ,@LastUpdUID                
   ,@LastUpdTS                
   ,@IsScheduledServiceCoord                
   ,@CompanyName                
   ,@EmployeeID                
   ,@FaxNum                
   ,@EmailAddress                
   ,@PersonalIDNum                
   ,@IsSpouseDependant                
   ,@AccountID                
   ,@CountryID                
   ,@IsSIFL                
   ,@PassengerAlert                
   ,@Gender                
   ,@AdditionalPhoneNum                
   ,@IsRequestor                
   ,@CountryOfResidenceID                
   ,@PaxScanDoc                
   ,@TSAStatus                
   ,@TSADTTM                
   ,@Addr1                
   ,@Addr2                
   ,@City                
   ,@StateName                
   ,@PostalZipCD                
   ,@IsDeleted      
   ,@PrimaryMobile      
   ,@BusinessFax      
   ,@CellPhoneNum2      
   ,@OtherPhone      
   ,@PersonalEmail      
   ,@OtherEmail      
   ,@Addr3      
   ,@PassengerWeight      
   ,@PassengerWeightUnit      
   ,@Birthday      
   ,@Anniversaries      
   ,@EmergencyContacts      
   ,@CreditcardInfo      
   ,@CateringPreferences      
   ,@HotelPreferences      
   ,@INSANumber      
   ,@Category      
   ,@CardExpires      
   ,@ResidentSinceYear      
   ,@GreenCardGender      
   ,@GreenCardCountryID    
   ,@cqcustomerid    
   ,@PaxTitle    
   ,@CityOfBirth    
   ,@Nickname)                
              
--SELECT @PassengerRequestorID AS PassengerRequestorID              
              
END      
  
GO


