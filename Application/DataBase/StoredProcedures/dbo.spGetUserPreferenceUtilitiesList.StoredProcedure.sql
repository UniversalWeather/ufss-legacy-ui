IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetUserPreferenceUtilitiesList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetUserPreferenceUtilitiesList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetUserPreferenceUtilitiesList]    
(    
@SessionId uniqueidentifier,  
@UserGroupID BIGINT    
)      
            
AS              
SET NOCOUNT ON;              
    
BEGIN    
 DECLARE @UserName varchar(30)  
 DECLARE @CustomerId BIGINT  
   
 SELECT @UserName = LoginSessionUID, @CustomerId = CustomerID FROM FPLoginSession where SessionID = @SessionId  
   
 DECLARE @IsSysAdmin BIT    
 SELECT @IsSysAdmin = IsNull(IsSystemAdmin,0) FROM UserMaster WHERE RTRIM(UserName) = RTRIM(@UserName) AND CustomerID = @CustomerID AND isnull(IsDeleted,0) = 0    
 DECLARE @Final TABLE (KeyName VARCHAR(50), KeyValue VARCHAR(50))    
     
 IF @IsSysAdmin = 1    
 BEGIN    
  IF EXISTS(SELECT   TOP 1  * from  USERPREFERENCE UP where UserName = @UserName and customerid = @CustomerID and RTRIM(CategoryName) = 'Utility'  )                
  BEGIN        
       
       
       
   INSERT INTO @Final    
   SELECT KeyName,KeyValue    
   FROM userpreference     
   WHERE     
    UserName = @UserName     
    AND customerid = @CustomerID     
    AND CategoryName = 'Utility'    
        
   UNION    
    
   SELECT DISTINCT P.UMPermissionName         
    , 'TRUE'    
   FROM Module M     
    inner join  CustomerModuleAccess CMA on M.ModuleID = CMA.ModuleID             
    INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID              
   WHERE     
    CMA.CustomerID = @CustomerID    
      --and UserGroupID=@UserGroupID    
      AND M.ModuleID =10019    
      AND P.FormName NOT IN('UTAPDETL', 'UTITLEGS')---('UTAPDETL', 'UTITLEGS', 'UVTP')
       AND P.UMPermissionName NOT IN(SELECT KeyName     
             FROM userpreference     
             WHERE UserName = @UserName     
              AND customerid = @CustomerID     
              AND CategoryName = 'Utility')    
           
    SELECT KeyName, KeyValue FROM @Final    
        
        
  END    
  ELSE    
  BEGIN    
   SELECT P.UMPermissionName as 'KeyName', 'TRUE' AS 'KeyValue'    
   FROM Module M     
    inner join  CustomerModuleAccess CMA on M.ModuleID = CMA.ModuleID             
    INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID              
   WHERE     
    CMA.CustomerID = @CustomerID    
    and M.ModuleID =10019    
    AND P.FormName NOT IN('UTAPDETL', 'UTITLEGS') --('UTAPDETL', 'UTITLEGS', 'UVTP')    
       
  END      
 END    
 ELSE    
 BEGIN    
    
  IF EXISTS(SELECT   TOP 1  * from  USERPREFERENCE UP where UserName = @UserName and customerid = @CustomerID and RTRIM(CategoryName) = 'Utility'  )                
  BEGIN        
       
       
   INSERT INTO @Final    
   SELECT KeyName,KeyValue    
   FROM userpreference     
   WHERE     
    UserName = @UserName     
    AND customerid = @CustomerID     
    AND CategoryName = 'Utility'    
        
   UNION    
    
   SELECT DISTINCT P.UMPermissionName         
    , 'TRUE'    
   FROM Module M              
      INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID              
      INNER JOIN UserGroupPermission CP ON CP.UMPermissionID =P.UMPermissionID               
   WHERE CP.CustomerID=@CustomerID    
      --and UserGroupID=@UserGroupID    
      AND M.ModuleID =10019    
      AND P.FormName NOT IN('UTAPDETL', 'UTITLEGS')  --('UTAPDETL', 'UTITLEGS', 'UVTP')  
      AND CP.CanView = 1    
       AND P.UMPermissionName NOT IN(SELECT KeyName     
             FROM userpreference     
             WHERE UserName = @UserName     
              AND customerid = @CustomerID     
              AND CategoryName = 'Utility')    
           
    SELECT KeyName, KeyValue FROM @Final    
        
        
  END    
  ELSE    
  BEGIN    
   SELECT DISTINCT P.UMPermissionName as 'KeyName', 'TRUE' AS 'KeyValue'    
   FROM Module M              
    INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID              
    INNER JOIN UserGroupPermission CP ON CP.UMPermissionID =P.UMPermissionID               
   WHERE CP.CustomerID=@CustomerID    
    --AND UserGroupID=@UserGroupID    
    AND M.ModuleID =10019    
    AND P.FormName NOT IN('UTAPDETL', 'UTITLEGS') --('UTAPDETL', 'UTITLEGS', 'UVTP')    
    AND CP.CanView = 1    
  END    
 END           
        
END     


GO


