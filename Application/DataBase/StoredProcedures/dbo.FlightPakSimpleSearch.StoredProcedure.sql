/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FlightPakSimpleSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FlightPakSimpleSearch]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[FlightPakSimpleSearch]  
@SearchStr varchar(100),  
@SessionId UniqueIdentifier,  
@ModuleName varchar(30),  
@PageNum int ,  
@PageSize int    
AS  
BEGIN  
  
 -- Temp Table Creation Start
 CREATE TABLE #Results
 (
	SearchId bigint,
	ModuleName varchar(30), 
	TableName nvarchar(128),
	PageVirtualPath nvarchar(2048),
	PrimaryKeyValue bigint,
	ColumnValue nvarchar(max)
 )
 -- Temp Table Creation End
 -- Variable Declaration Start  
  DECLARE @DynamicSQL VARCHAR(MAX)  
  DECLARE @CustomerId bigint  
  DECLARE @IsCustomerIdRequired bit  
  DECLARE @IsSysAdmin bit  
  DECLARE @EntityId bigint
  SET @EntityId = 0
  ----TEMP  
  --DECLARE @SearchStr varchar(100)  
  --DECLARE @SessionId UniqueIdentifier  
  --DECLARE @EntityId bigint  
  --DECLARE @PageNum int   
  --DECLARE @PageSize int    
 -- Variable Declaration End  
   
 -- Set Value for variable Start  
  ----TEMP   
  --SET @SearchStr = 'a'  
  --SET @SessionId = '3184c72a-81ad-4cf1-8f59-991d3d019b79'  
  --SET @EntityId = 1  
  --SET @PageNum = 0   
  --SET @PageSize = 0  
  --END TEMP  
  SELECT @CustomerId = UserMaster.CustomerID,@IsSysAdmin =  UserMaster.IsSystemAdmin FROM FPLoginSession  
  JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID  
  WHERE FPLoginSession.SessionID = @SessionId  
  
    
 --Set Value for variable End  
   
 -- Replace Parameters in static query Start  
  WHILE(@EntityId IS NOT NULL)
	BEGIN
		SET @EntityId = (SELECT MIN(SearchId) FROM FPSearchParameters WHERE SearchId > @EntityId AND ModuleName = @ModuleName) 
		IF(@EntityId IS NOT NULL)
			BEGIN
				Select @DynamicSQL  = [StaticQuery], @IsCustomerIdRequired = IsCustomerIdRequired  FROM FPSearchParameters WHERE SearchId = @EntityId   
				--PRINT @EntityId
				IF(@IsCustomerIdRequired = 1)  
					BEGIN  
						SELECT @DynamicSQL = REPLACE(@DynamicSQL, '@CustomerId', CAST( @CustomerId  AS VARCHAR(MAX)))  
					END  
    
				SELECT @DynamicSQL = REPLACE(@DynamicSQL, '@SessionId', @SessionId)  
				SELECT @DynamicSQL = REPLACE(@DynamicSQL, '@SearchStr', @SearchStr)  
				SELECT @DynamicSQL = REPLACE(@DynamicSQL, '@EntityId', @EntityId)  
				SELECT @DynamicSQL = REPLACE(@DynamicSQL, '@IsSysAdmin', @IsSysAdmin)  
				-- Replace Parameters in static query End  
				--PRINT @DynamicSQL
				-- Execute SQL 
				INSERT INTO #Results	 
				EXECUTE (@DynamicSQL)
			END
	END  
 -- Execute Sql  
 
 -- Select Temp Result set
	SELECT * FROM #Results ORDER BY TableName
END  
  
GO


