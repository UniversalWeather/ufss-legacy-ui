
/****** Object:  StoredProcedure [dbo].[spGetCrewselected]    Script Date: 04/16/2014 15:18:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCrewselected]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCrewselected]
GO


/****** Object:  StoredProcedure [dbo].[spGetCrewselected]    Script Date: 04/16/2014 15:18:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      

CREATE PROCEDURE [dbo].[spGetCrewselected](@CustomerID BIGINT ,          
             @CrewGroupID BIGINT)            
AS            
SET NOCOUNT ON            
BEGIN            
            
SELECT           
			Crew.[CrewID]           
           ,Crew.[CrewCD]          
           ,Crew.[CustomerID]          
           ,[LastName]          
           ,[FirstName]          
           ,[MiddleInitial]          
           ,[IsFixedWing]          
           ,[IsRotaryWing]          
           ,[Addr1]          
           ,[Addr2]          
           ,[CityName]          
           ,[StateName]          
           ,[PostalZipCD]          
           ,[CountryID]          
           ,[PhoneNum]          
           ,[SSN]          
           ,[IsStatus]          
           ,[HomebaseID]          
           ,[BirthDT]          
           ,[ClientID]          
           ,[CellPhoneNum]          
           ,[PagerNum]          
           ,[CrewTypeCD]          
           ,Crew.[LastUpdUID]          
           ,Crew.[LastUpdTS]          
           ,[PilotLicense1]          
           ,[Notes]          
           ,[CheckList]          
           ,[PilotLicense2]          
           ,[LicenseCountry1]          
           ,[LicenseCountry2]          
           ,[IsNoCalendarDisplay]          
           ,[CrewTypeDescription]          
           ,[Gender]          
           ,[EmailAddress]          
           ,[FaxNum]          
           ,[HireDT]          
           ,[TerminationDT]          
           ,[LicenseType1]          
           ,[LicenseType2]          
           ,[DepartmentID]          
           ,[IsCheckListFrequency]          
           ,[IsNextMonth]          
           ,[CrewScanDocuments]          
           ,[Citizenship]          
           ,[Notes2]          
           ,[IsDepartmentAuthorization]          
           ,[IsRequestPhoneNum]          
           ,[IsCancelDescription]          
           ,[IsTripN]          
           ,[IsTripA]          
           ,[IsAirport]          
           ,[IsCheckList]          
           ,[IsAccountNum]          
           ,[IsStatusT]          
           ,[IsDayLightSavingTimeARR]          
           ,[IsDayLightSavingTimeDEPART]          
           ,[IsHomeArrivalTM]          
           ,[IsHomeDEPARTTM]          
           ,[IsEndDuty]          
           ,[IsOverride]          
           ,[IsCrewRules]          
           ,[IsFAR]          
           ,[IsDuty]          
           ,[IsFlightHours]          
           ,[IsRestHrs]          
           ,[Comments]          
           ,[IsDepartureFBO]          
           ,[IsArrivalFBO]          
           ,[IsCrewHotel]          
           ,[IsCrewDepartTRANS]          
           ,[IsCrewArrivalTRANS]          
           ,[IsCrewHotelN]          
           ,[IsPassHotel]          
           ,[IsPassDepartTRANS]          
           ,[IsPassArrivalHotel]          
           ,[IsPassDetails]          
           ,[IsPassPhoneNum]          
           ,[IsDepartCatering]          
           ,[IsArrivalCatering]          
           ,[IsArrivalDepartTime]          
           ,[IcaoID]          
           ,[IsCrew]          
           ,[IsPassenger]          
           ,[IsLegN]          
           ,[IsOutboundINST]          
           ,[IsCheckList1]          
           ,[IsFBO]          
           ,[IsHotel]          
           ,[IsTransportation]          
           ,[IsCatering]          
           ,[Label1]          
           ,[Label2]          
           ,[Label3]          
           ,[Label4]          
           ,[Label5]          
           ,[Label6]          
           ,[Label7]          
           ,[IsCrewHotelConfirm]          
           ,[IsCrewHotelComments]          
           ,[IsCrewDepartTRANSConfirm]          
           ,[IsCrewDepartTRANSComments]          
           ,[IsCrewArrivalConfirm]          
           ,[IsCrewArrivalComments]          
           ,[IsCrewNotes]          
           ,[IsPassHotelConfirm]          
           ,[IsPassHotelComments]          
           ,[IsPassDepartTRANSConfirm]          
           ,[IsPassDepartTRANSComments]          
           ,[IsPassArrivalConfirm]          
           ,[IsPassArrivalComments]          
           ,[IsPassNotes]          
           ,[IsOutboundComments]          
           ,[IsDepartAirportNotes]          
           ,[IsArrivalAirportNoes]          
           ,[IsDepartAirportAlerts]          
           ,[IsArrivalAirportAlerts]          
		   ,[IsFBODepartConfirm]          
           ,[IsFBOArrivalConfirm]          
           ,[IsFBODepartComment]          
           ,[IsFBOArrivalComment]          
           ,[IsDepartCateringConfirm]          
           ,[IsDepartCateringComment]          
           ,[IsArrivalCateringConfirm]          
           ,[IsArrivalCateringComment]          
           ,[IsTripAlerts]          
           ,[IsTripNotes]          
           ,[IsRunway]          
           ,[IsPassengerPhoneNum]          
           ,[BlackBerryTM]          
           ,[IsAssociatedCrew]          
           ,[CityOfBirth]          
           ,[CountryOfBirth]          
           ,[License1CityCountry]          
           ,[License2CityCountry]          
           ,[License1ExpiryDT]          
           ,[License2ExpiryDT]          
           ,[APISException]          
           ,[APISSubmit]          
           ,[APISUserID]          
           ,[IsAircraft]          
           ,[StateofBirth]          
           ,Crew.[IsDeleted]        
           ,BusinessPhone           
		   ,BusinessFax           
		   ,CellPhoneNum2          
		   ,OtherPhone           
		   ,PersonalEmail          
		   ,OtherEmail           
		   ,Addr3          
		   ,Crew.IsInActive      
		   ,Birthday      
		   ,Anniversaries      
		   ,EmergencyContacts      
		   ,CreditcardInfo      
		   ,CateringPreferences      
		   ,HotelPreferences     
		   ,IsTripEmail      
		   ,IsCrewDisplay  
   FROM  Crew           
   INNER JOIN CrewGroupOrder ON CrewGroupOrder.CrewID = Crew.CrewID      
   WHERE CrewGroupOrder.CrewGroupID = @CrewGroupID AND IsStatus = 1   
   ORDER BY CrewGroupOrder.OrderNum      
   --WHERE Crew.CrewID       
   --IN(select CrewGroupOrder.CrewID FROM CrewGroupOrder           
   --                                                  WHERE CrewGroupOrder.CrewGroupID =@CrewGroupID)           
   --AND Crew.CustomerID=@CustomerID   AND Crew.CrewCD is not null  AND IsDeleted = 0        
   --ORDER BY  Crew.LastName         
END 
GO


