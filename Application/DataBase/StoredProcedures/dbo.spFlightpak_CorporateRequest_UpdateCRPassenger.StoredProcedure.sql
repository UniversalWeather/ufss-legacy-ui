

/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_UpdateCRPassenger]    Script Date: 02/21/2013 18:26:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_UpdateCRPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRPassenger]
GO



/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_UpdateCRPassenger]    Script Date: 02/21/2013 18:26:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRPassenger]
(@CRPassengerID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@OrderNUM INT
,@PassengerRequestorID BIGINT
,@PassengerName VARCHAR(63)
,@FlightPurposeID BIGINT
,@PassportID BIGINT
,@Billing VARCHAR(25)
,@IsNonPassenger BIT
,@IsBlocked BIT
,@ReservationNUM VARCHAR(6)
,@WaitList VARCHAR(1)
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
		
	UPDATE CRPassenger
	SET		CustomerID = @CustomerID
			,CRLegID = @CRLegID
			,TripNUM = @TripNUM
			,LegID = @LegID
			,OrderNUM = @OrderNUM
			,PassengerRequestorID = @PassengerRequestorID
			,PassengerName = @PassengerName
			,FlightPurposeID = @FlightPurposeID
			,PassportID = @PassportID
			,Billing = @Billing
			,IsNonPassenger = @IsNonPassenger
			,IsBlocked = @IsBlocked
			,ReservationNUM = @ReservationNUM
			,WaitList = @WaitList
			,LastUpdUID = @LastUpdUID
			,LastUpdTS = @LastUpdTS
			,IsDeleted = @IsDeleted
	WHERE 
		CRPassengerID = @CRPassengerID

END
GO


