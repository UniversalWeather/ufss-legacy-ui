/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFleetInfoForFleetTree]    Script Date: 02/03/2015 19:58:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetFleetInfoForFleetTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetFleetInfoForFleetTree]
GO
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetFleetInfoForFleetTree]
(
	@CustomerID BIGINT, 
	@ClientID BIGINT, 
	@USERHOMEBASEID BIGINT
)
AS  
BEGIN
	-- NOTE: In R1 - Fleet.homebaseId is filtered with Company.HomebaseId. In R2, it should be done with Company.HomebaseAirportId 
	IF(@ClientID>0)        
	BEGIN

		IF(@USERHOMEBASEID>0) -- HOMEBASE ONLY FILTER SELECTED  
		BEGIN

			-- Select all fleet ids for main tree for given client ID
			SELECT * FROM (
				SELECT Fleet.FleetID,Fleet.TailNum, Fleet.TypeDescription,  Fleet.AircraftCD,
				NULL AS FleetGroupID, NULL AS FleetGroupCD, NULL as FleetGroupDescription, NULL as OrderNum, Fleet.HomebaseID
				FROM Fleet 
				LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
				WHERE Fleet.CustomerID  = @CustomerID and Fleet.IsDeleted = 0 and Fleet.ClientID = @ClientID  AND Fleet.IsInActive != 1
				UNION
				-- select all fleet groups for given client ID
				SELECT Fleet.FleetID, Fleet.TailNum ,Fleet.TypeDescription, Fleet.AircraftCD,
				FleetGroup.FleetGroupID, FleetGroup.FleetGroupCD, FleetGroup.FleetGroupDescription,
				FleetGroupOrder.OrderNum  , Fleet.HomebaseID         
				FROM FleetGroupOrder
				INNER JOIN  Fleet  ON FleetGroupOrder.FleetID = Fleet.FleetID AND Fleet.IsDeleted = 0 AND Fleet.ClientID = @ClientID AND Fleet.IsInActive != 1
				INNER JOIN FleetGroup ON FleetGroup.FleetGroupID = FleetGroupOrder.FleetGroupID AND FleetGroup.IsDeleted = 0
				LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
				WHERE  FleetGroupOrder.FleetGroupID IN (SELECT FleetGroupID FROM FleetGroupOrder WHERE CustomerID = @CustomerID and FleetGroupOrder.IsDeleted = 0 ) 
			) As T
			Where T.HomebaseID=@USERHOMEBASEID
			ORDER BY T.FleetGroupCD,T.OrderNum

		END
		ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
		BEGIN

			-- Select all fleet ids for main tree for given client ID
			SELECT Fleet.FleetID,Fleet.TailNum, Fleet.TypeDescription,  Fleet.AircraftCD,
			NULL AS FleetGroupID, NULL AS FleetGroupCD, NULL as FleetGroupDescription, NULL as OrderNum, Fleet.HomebaseID
			FROM Fleet 
			LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID 
			WHERE Fleet.CustomerID = @CustomerID and Fleet.IsDeleted = 0 and Fleet.ClientID = @ClientID AND Fleet.IsInActive != 1
			UNION
			-- select all fleet groups for given client ID
			SELECT Fleet.FleetID, Fleet.TailNum ,Fleet.TypeDescription, Fleet.AircraftCD,
			FleetGroup.FleetGroupID, FleetGroup.FleetGroupCD, FleetGroup.FleetGroupDescription,
			FleetGroupOrder.OrderNum  , Fleet.HomebaseID         
			FROM FleetGroupOrder
			INNER JOIN  Fleet  ON FleetGroupOrder.FleetID = Fleet.FleetID AND Fleet.IsDeleted = 0 and Fleet.ClientID = @ClientID  AND Fleet.IsInActive != 1
			INNER JOIN FleetGroup ON FleetGroup.FleetGroupID = FleetGroupOrder.FleetGroupID AND FleetGroup.IsDeleted = 0
			LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID 
			WHERE  FleetGroupOrder.FleetGroupID IN (SELECT FleetGroupID FROM FleetGroupOrder WHERE CustomerID = @CustomerID and FleetGroupOrder.IsDeleted = 0 ) 
			ORDER BY FleetGroupCD, OrderNum

		END

	END
	ELSE  -- No client ID filter
	BEGIN    
			
		IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
		BEGIN

			-- Select all fleet ids for main tree for given client ID
			SELECT * FROM (
				SELECT Fleet.FleetID,Fleet.TailNum, Fleet.TypeDescription,  Fleet.AircraftCD,
				NULL AS FleetGroupID, NULL AS FleetGroupCD, NULL as FleetGroupDescription, NULL as OrderNum, Fleet.HomebaseID
				FROM Fleet 
				LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
				WHERE Fleet.CustomerID  = @CustomerID and Fleet.IsDeleted = 0  AND Fleet.IsInActive != 1
				UNION
				-- select all fleet groups for given client ID
				SELECT Fleet.FleetID, Fleet.TailNum ,Fleet.TypeDescription, Fleet.AircraftCD,
				FleetGroup.FleetGroupID, FleetGroup.FleetGroupCD, FleetGroup.FleetGroupDescription,
				FleetGroupOrder.OrderNum  , Fleet.HomebaseID         
				FROM FleetGroupOrder
				INNER JOIN  Fleet  ON FleetGroupOrder.FleetID = Fleet.FleetID AND Fleet.IsDeleted = 0 
				INNER JOIN FleetGroup ON FleetGroup.FleetGroupID = FleetGroupOrder.FleetGroupID AND FleetGroup.IsDeleted = 0  AND Fleet.IsInActive != 1
				LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
				WHERE  FleetGroupOrder.FleetGroupID IN (SELECT FleetGroupID FROM FleetGroupOrder WHERE CustomerID = @CustomerID and FleetGroupOrder.IsDeleted = 0 ) 
			) As T
			Where T.HomebaseID=@USERHOMEBASEID
			ORDER BY T.FleetGroupCD,T.OrderNum			

		END
		ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
		BEGIN

			-- Select all fleet ids for main tree for given client ID
			SELECT Fleet.FleetID,Fleet.TailNum, Fleet.TypeDescription,  Fleet.AircraftCD,
			NULL AS FleetGroupID, NULL AS FleetGroupCD, NULL as FleetGroupDescription, NULL as OrderNum, Fleet.HomebaseID
			FROM Fleet 
			LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID 
			WHERE Fleet.CustomerID = @CustomerID and Fleet.IsDeleted = 0  AND Fleet.IsInActive != 1
			UNION
			-- select all fleet groups for given client ID
			SELECT Fleet.FleetID, Fleet.TailNum ,Fleet.TypeDescription, Fleet.AircraftCD,
			FleetGroup.FleetGroupID, FleetGroup.FleetGroupCD, FleetGroup.FleetGroupDescription,
			FleetGroupOrder.OrderNum  , Fleet.HomebaseID         
			FROM FleetGroupOrder
			INNER JOIN  Fleet  ON FleetGroupOrder.FleetID = Fleet.FleetID AND Fleet.IsDeleted = 0 
			INNER JOIN FleetGroup ON FleetGroup.FleetGroupID = FleetGroupOrder.FleetGroupID AND FleetGroup.IsDeleted = 0 AND Fleet.IsInActive != 1
			LEFT OUTER JOIN Company ON Company.HomebaseID = Fleet.HomebaseID 
			WHERE  FleetGroupOrder.FleetGroupID IN (SELECT FleetGroupID FROM FleetGroupOrder WHERE CustomerID = @CustomerID and FleetGroupOrder.IsDeleted = 0 ) 
			ORDER BY FleetGroupCD, OrderNum

		END

	END	
	
END