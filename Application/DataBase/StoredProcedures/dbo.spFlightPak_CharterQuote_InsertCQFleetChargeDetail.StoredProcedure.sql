
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQFleetChargeDetail]    Script Date: 02/25/2013 15:05:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQFleetChargeDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFleetChargeDetail]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQFleetChargeDetail]    Script Date: 02/25/2013 15:05:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFleetChargeDetail]
(
	
	@CustomerID bigint,
	@CQMainID bigint,
	@FileNUM int,
	@QuoteID int,
	@AccountID bigint,
	@AircraftCharterRateID bigint,
	@FleetCharterRateID bigint,
	@CQCustomerCharterRateID bigint,
	@OrderNUM int,
	@CQFlightChargeDescription varchar(40),
	@ChargeUnit varchar(25),
	@NegotiatedChgUnit numeric(2, 0),
	@Quantity numeric(17, 3),
	@BuyDOM numeric(17, 2),
	@SellDOM numeric(17, 2),
	@IsTaxDOM bit,
	@IsDiscountDOM bit,
	@BuyIntl numeric(17, 2),
	@SellIntl numeric(17, 2),
	@IsTaxIntl bit,
	@IsDiscountIntl bit,
	@StandardCrewDOM numeric(2, 0),
	@StandardCrewINTL numeric(2, 0),
	@MinimumDailyRequirement numeric(17, 2),
	@YearMade char(4),
	@ExteriorColor varchar(25),
	@ColorIntl varchar(25),
	@IsAFIS bit,
	@IsUWAData bit,
	@AircraftFlightChg1 varchar(32),
	@AircraftFlightChg2 varchar(32),
	@BuyAircraftFlightIntl varchar(32),
	@FeeAMT numeric(17, 2),
	--@FeeGroupID bigint,
	@SellAircraftFlight varchar(32),
	@IsToPrint bit,
	--@IsDailyTaxAdj bit,
	--@IsLandingFeeTax bit,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQFleetChargeDetailID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQFleetChargeDetailID OUTPUT 
INSERT INTO [CQFleetChargeDetail] ([CQFleetChargeDetailID], [CustomerID], [CQMainID], [FileNUM], [QuoteID], [AccountID], [AircraftCharterRateID], [FleetCharterRateID], [CQCustomerCharterRateID], [OrderNUM], [CQFlightChargeDescription], [ChargeUnit], [NegotiatedChgUnit], [Quantity], [BuyDOM], [SellDOM], [IsTaxDOM], [IsDiscountDOM], [BuyIntl], [SellIntl], [IsTaxIntl], [IsDiscountIntl], [StandardCrewDOM], [StandardCrewINTL], [MinimumDailyRequirement], [YearMade], [ExteriorColor], [ColorIntl], [IsAFIS], [IsUWAData], [AircraftFlightChg1], [AircraftFlightChg2], [BuyAircraftFlightIntl], [FeeAMT], 
--[FeeGroupID], 
[SellAircraftFlight], [IsToPrint], 
--[IsDailyTaxAdj], [IsLandingFeeTax], 
[LastUpdUID], [LastUpdTS], [IsDeleted]) VALUES (@CQFleetChargeDetailID, @CustomerID, @CQMainID, @FileNUM, @QuoteID, @AccountID, @AircraftCharterRateID, @FleetCharterRateID, @CQCustomerCharterRateID, @OrderNUM, @CQFlightChargeDescription, @ChargeUnit, @NegotiatedChgUnit, @Quantity, @BuyDOM, @SellDOM, @IsTaxDOM, @IsDiscountDOM, @BuyIntl, @SellIntl, @IsTaxIntl, @IsDiscountIntl, @StandardCrewDOM, @StandardCrewINTL, @MinimumDailyRequirement, @YearMade, @ExteriorColor, @ColorIntl, @IsAFIS, @IsUWAData, @AircraftFlightChg1, @AircraftFlightChg2, @BuyAircraftFlightIntl, @FeeAMT, 
--@FeeGroupID, 
@SellAircraftFlight, @IsToPrint, 
--@IsDailyTaxAdj, @IsLandingFeeTax,
 @LastUpdUID, @LastUpdTS, @IsDeleted);
	
SELECT @CQFleetChargeDetailID AS 'CQFleetChargeDetailID'

GO


