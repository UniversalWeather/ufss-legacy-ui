/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRHotelList]    Script Date: 04/25/2013 17:48:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRHotelList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRHotelList]
(@CustomerID BIGINT
,@CRLegID BIGINT
,@CRHotelListID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRHotelList.CRHotelListID
			,CRHotelList.CustomerID
			,CRHotelList.CRLegID
			,CRHotelList.TripNUM
			,CRHotelList.LegID
			,CRHotelList.RecordType
			,CRHotelList.HotelID
			,CRHotelList.CRHotelListDescription
			,CRHotelList.PhoneNUM
			,CRHotelList.IsCompleted
			,CRHotelList.AirportID
			,CRHotelList.LastUpdUID
			,CRHotelList.LastUpdTS
			,CRHotelList.IsDeleted
			,CRHotelList.FaxNum as FaxNumber
			,CRHotelList.Email	as BusinessEmail
			,CRHotelList.Rate				
			,Hotel.HotelCD
			,Hotel.Name
			,Hotel.PhoneNum
			,Hotel.FaxNum
			,Hotel.ContactEmail
			,Hotel.NegociatedRate
	FROM CRHotelList
	LEFT OUTER JOIN Hotel ON Hotel.HotelID = CRHotelList.HotelID
	WHERE CRHotelList.CustomerID = @CustomerID
	AND CRHotelList.CRLegID = @CRLegID
	AND CRHotelList.IsDeleted = 0
	AND CRHotelList.CRHotelListID =	CASE WHEN ((@CRHotelListID IS NULL) OR (@CRHotelListID <> -1))
						THEN (@CRHotelListID)
						ELSE (SELECT CRHotelListID FROM CRHotelList 
								WHERE CustomerID = @CustomerID
										AND CRLegID = @CRLegID
										AND IsDeleted = 0)
						END
END
GO