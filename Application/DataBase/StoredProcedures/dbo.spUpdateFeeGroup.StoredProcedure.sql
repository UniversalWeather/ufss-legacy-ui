
/****** Object:  StoredProcedure [dbo].[spUpdateFeeGroup]    Script Date: 02/15/2013 19:14:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFeeGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFeeGroup]
GO

USE [FP_Rel2]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFeeGroup]    Script Date: 02/15/2013 19:14:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

          
CREATE procedure [dbo].[spUpdateFeeGroup]              
(              
 @FeeGroupID bigint              
,@CustomerID bigint              
,@FeeGroupCD char(4)              
,@FeeGroupDescription varchar(100)        
,@LastUpdUID varchar(30)              
,@LastUpdTS datetime              
,@IsDeleted bit              
,@IsInActive bit          
)              
-- =============================================              
-- Author:Sujitha.V              
-- Create date: 12/4/2012              
-- Description: Update the DelayType  information              
-- =============================================              
as              
begin               
SET NoCOUNT ON              
DECLARE @currentTime datetime                
SET @currentTime = GETUTCDATE()                
SET @LastUpdTS = @currentTime                
UPDATE FeeGroup              
   SET               
            FeeGroupCD=@FeeGroupCD            
           ,[FeeGroupDescription]=@FeeGroupDescription              
           ,[LastUpdUID]=@LastUpdUID              
           ,[LastUpdTS]=@LastUpdTS              
           ,[IsDeleted]=@IsDeleted        
           ,[IsInActive]=@IsInActive                                      
 WHERE CustomerID =@CustomerID and FeeGroupID=@FeeGroupID              
              
end 
GO


