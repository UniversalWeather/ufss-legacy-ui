/****** Object:  StoredProcedure [dbo].[spGetAllCrewDutyType]    Script Date: 01/07/2013 19:56:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewDutyType]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCrewDutyType]    Script Date: 01/07/2013 19:56:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllCrewDutyType](@CustomerID BIGINT)  
AS  
-- =============================================  
-- Author:Prakash Pandian.S  
-- Create date: 9/5/2012  
-- Description: Get the CrewDutyTypes information  
-- =============================================  
  
if len(@CustomerID) >0  
BEGIN   
SET NOCOUNT ON  
 SELECT [DutyTypeID]  
      ,CASE WHEN DutyTypeCD IS NULL THEN '' ELSE LTRIM(RTRIM(DutyTypeCD)) END AS DutyTypeCD
      ,[CustomerID]  
      ,[DutyTypesDescription]  
      ,[ValuePTS]  
      ,[GraphColor]  
      ,[WeekendPTS]  
      ,[IsWorkLoadIndex]  
      ,[IsOfficeDuty]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[ForeGrndCustomColor]  
      ,[BackgroundCustomColor]  
      ,[IsCrewCurrency]  
      ,[HolidayPTS]  
      ,[IsCrewDuty]  
      ,[CalendarEntry]  
      ,[DutyStartTM]  
      ,[DutyEndTM]  
      ,[IsStandby]  
      ,[IsDeleted]  
      ,[IsInActive]
  FROM [CrewDutyType]  
 WHERE   IsDeleted = 'false' and CustomerID = @CustomerID  
 order by DutyTypeCD  
end 
GO


