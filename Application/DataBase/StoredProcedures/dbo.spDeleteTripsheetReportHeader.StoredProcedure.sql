
/****** Object:  StoredProcedure [dbo].[spDeleteTripsheetReportHeader]    Script Date: 7/15/2015 3:41:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spDeleteTripsheetReportHeader]
(@TripSheetReportHeaderID bigint,
@CustomerID bigint,
@ReportID varchar(8),
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit)
AS BEGIN

	DECLARE @RecordUsedInCompany int SET @RecordUsedInCompany = 0

	SELECT @RecordUsedInCompany = COUNT(*) FROM [dbo].[Company] 
	WHERE TripsheetRptWriteGroup in (SELECT DISTINCT ReportId FROM TripSheetReportHeader WHERE TripSheetReportHeaderID = @TripSheetReportHeaderID )

	IF (@RecordUsedInCompany <> 0 )
		 BEGIN
			RAISERROR(N'-500010', 17, 1)
		 END
	ELSE
		 BEGIN
			SET @IsDeleted = 1
			UPDATE	TripSheetReportHeader
			SET		LastUpdUID=@LastUpdUID,
					LastUpdTS=@LastUpdTS,
					IsDeleted=@IsDeleted
			WHERE	TripSheetReportHeaderID=@TripSheetReportHeaderID
		 End
END
GO

