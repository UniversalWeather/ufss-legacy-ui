/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllDeletedPreflightMainDetails]    Script Date: 03/14/2013 20:08:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllDeletedPreflightMainDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllDeletedPreflightMainDetails]
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAllDeletedPreflightMainDetails]   
 (  
  @CustomerID Bigint  
 )  
 AS    
 SET NOCOUNT ON;   
 BEGIN  
   
   
  --set @CustomerID =10000  
  --set @HomebaseID = null  
  --set @ClientID = null  
  --set @TripSheet = 't'  
  --set @Worksheetstr = 'w'  
  --set @Hold = 'h'  
  --set @Cancelled = null  
  --set @SchedServ = null  
  --set @UnFillFilled = null  
  --set @isLog  = 1  
  --set @EstDepartureDT = DATEADD(DAY,-300,GETDATE())  
    
  
  Declare @Sql varchar(2000)  
  Declare @StatusSql varchar(2000)  
  Declare @StatusEntered bit  
  
  set @Sql =  'SELECT   Distinct 
      PM.TripID,  
      PM.TripNUM, 
      PM.DispatchNUM, 
      PM.EstDepartureDT,  
      '''' as ''RequestorFirstName'',  
      PM.TripDescription,  
      PM.TripStatus,  
      PM.RequestDT,  
      F.TailNum,  
      PM.TripNUM as LOGNUM,  
      PM.Acknowledge,  
      PM.WaitList,  
      '''' AS HomebaseCD,
      '''' as Alert,
      PM.APISSubmit,
      PM.APISStatus,
      '''' as APISException,         
	  '''' as CR,
	  '''' as TripReqLastSubmitDT,
	  '''' as  DutyType,
	  '''' as TripExcep,
	  '''' as ForeGrndCustomColor,
	  '''' as BackgroundCustomColor
      FROM PreflightMain PM
      INNER JOIN PreflightLeg PL on PM.TripID = PL.TripID  
      LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  
      WHERE   
      PM.RecordType= ''T'' and PM.CustomerID=' +   CAST( @CustomerID  as varchar(20))  
      set @Sql =  @Sql + ' and PM.IsDeleted =1 '   
    
   print @Sql  
    
      create table #tempPreflightmain  
      (  
      TripID bigint,  
      TripNUM bigint, 
      DispatchNUM varchar(12), 
      EstDepartureDT date,  
      RequestorFirstName varchar(100),  
      TripDescription varchar(40),  
      TripStatus char(1),  
      RequestDT date,  
      TailNum char(10),  
      LogNum bigint,  
      Acknowledge char(1),  
      WaitList char(1),  
      HomebaseCD  char(4),  
      Alert char(1),
      APISSubmit datetime,
      APISStatus varchar(20),
      APISException varchar(MAX),         
	  CR char(1),
	  TripReqLastSubmitDT datetime,
	  DutyType char(1),
	  TripExcep char(1),
	  ForeGrndCustomColor varchar(8),
	  BackgroundCustomColor	 varchar(8)
      )  
         
        
      insert into #tempPreflightmain Exec ( @Sql)  
        
SELECT
      
		TripID ,  
		TripNUM , 
		DispatchNUM,  
		EstDepartureDT ,  
		RequestorFirstName,  
		TripDescription,  
		TripStatus,  
		RequestDT,  
		TailNum,  
		LogNum ,  
		Acknowledge,  
		WaitList,  
		HomebaseCD,
		Alert,
        APISSubmit,
        APISStatus,
        APISException,         
	    CR,
	    TripReqLastSubmitDT,
	    DutyType,
	    TripExcep,
		ForeGrndCustomColor,
		BackgroundCustomColor
	    
FROM #tempPreflightmain

      union  
      SELECT   
      PM.TripID,  
      PM.TripNUM,  
      PM.DispatchNUM, 
      PM.EstDepartureDT,  
      P.PassengerRequestorCD as 'RequestorFirstName',  
      PM.TripDescription,  
      PM.TripStatus,  
      PM.RequestDT,  
      F.TailNum,  
      POM.LogNum,  
      PM.Acknowledge,  
      PM.WaitList,  
      A.IcaoID as HomebaseCD ,
	  PM.Notes as Alert,
      PM.APISSubmit,
      PM.APISStatus,
      PM.APISException,         
	  '' as CR,
	  UM.TripReqLastSubmitDT,
	  '' as DutyType,
	  '' as TripExcep,
	  '' as  ForeGrndCustomColor,
	  '' as BackgroundCustomColor	
	  
	  FROM PreflightMain PM  
      INNER JOIN Company C on PM.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID AND IsNull(POM.IsDeleted,0) = 0
      LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  
      LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID 
      LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID 
      
      WHERE 1=2  
        
      DROP TABLE #tempPreflightmain  
  
	--EXEC [spFlightPak_Preflight_GetAllDeletedPreflightMainDetails]  10002   
 END
GO