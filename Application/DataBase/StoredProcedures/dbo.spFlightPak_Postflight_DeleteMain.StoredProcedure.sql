IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_DeleteMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_DeleteMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Postflight_DeleteMain]
(
	@POLogID bigint,
	@CustomerID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FleetID BIGINT
	DECLARE @LastUpdTS datetime = NULL
	
	SET @LastUpdTS = GETUTCDATE()
	SELECT @FleetID = FleetID FROM PostflightMain WHERE POLogID = @POLogID

	IF EXISTS (SELECT NULL FROM PostflightLeg WHERE POLogID = @POLogID AND IsNull(IsDeleted,0) = 0)
	BEGIN
		DECLARE @FleetInfo Table (AirFrameHours numeric(11,3), Engine1Hours numeric(11,3), Engine2Hours numeric(11,3), Engine3Hours numeric(11,3)
		, Engine4Hours numeric(11,3), AirframeCycle numeric(6,3), Engine1Cycle numeric(6,3), Engine2Cycle numeric(6,3), Engine3Cycle numeric(6,3), Engine4Cycle numeric(6,3)
		, Reverse1Hrs numeric(11,3), Reverse2Hrs numeric(11,3), Reverse3Hrs numeric(11,3), Reverse4Hrs numeric(11,3)
		, Reverser1Cycle numeric(6,3), Reverser2Cycle numeric(6,3), Reverser3Cycle numeric(6,3), Reverser4Cycle numeric(6,3))

		--// SUM the Airframe and Engine Hour Values and save it in Temporary Table
		INSERT INTO @FleetInfo (AirFrameHours, Engine1Hours, Engine2Hours, Engine3Hours,  Engine4Hours
			, AirframeCycle, Engine1Cycle , Engine2Cycle , Engine3Cycle , Engine4Cycle
			, Reverse1Hrs, Reverse2Hrs, Reverse3Hrs, Reverse4Hrs, Reverser1Cycle, Reverser2Cycle, Reverser3Cycle, Reverser4Cycle)
		SELECT SUM(IsNull(AirFrameHours,0)), SUM(IsNull(Engine1Hours,0)), SUM(IsNull(Engine2Hours,0)), SUM(IsNull(Engine3Hours,0)), SUM(IsNull(Engine4Hours,0))
			, SUM(IsNull(AirframeCycle,0)), SUM(IsNull(Engine1Cycle,0)), SUM(IsNull(Engine2Cycle,0)), SUM(IsNull(Engine3Cycle,0)), SUM(IsNull(Engine4Cycle,0))
			, SUM(IsNull(Re1Hours,0)), SUM(IsNull(Re2Hours,0)), SUM(IsNull(Re3Hours,0)), SUM(IsNull(Re4Hours,0))
			, SUM(IsNull(Re1Cycle,0)), SUM(IsNull(Re2Cycle,0)), SUM(IsNull(Re3Cycle,0)), SUM(IsNull(Re4Cycle,0))
		FROM PostflightLeg WHERE IsNull(IsDeleted,0) = 0 AND POLogID = @POLogID AND CustomerID = @CustomerID
			
		--// Deduct from the Fleet Information Table based on the specified Customer and Fleet Id
		UPDATE [FleetInformation] SET 	
			AirframeHrs = CASE WHEN IsNull([FleetInformation].AirframeHrs,0) > 0 THEN IsNull([FleetInformation].AirframeHrs,0) - IsNull(temp.AirFrameHours,0) ELSE 0 END,
			AirframeCycle = CASE WHEN IsNull([FleetInformation].AirframeCycle,0) > 0 THEN IsNull([FleetInformation].AirframeCycle,0) - IsNull(temp.AirframeCycle,0) ELSE 0 END,
			AIrframeAsOfDT = @LastUpdTS,
			Engine1Hrs = CASE WHEN IsNull([FleetInformation].Engine1Hrs,0) > 0 THEN IsNull([FleetInformation].Engine1Hrs,0) - IsNull(temp.Engine1Hours,0) ELSE 0 END,
			Engine1Cycle = CASE WHEN IsNull([FleetInformation].Engine1Cycle,0) > 0 THEN IsNull([FleetInformation].Engine1Cycle,0) - IsNull(temp.Engine1Cycle,0) ELSE 0 END, 
			Engine1AsOfDT = @LastUpdTS,
			Engine2Hrs = CASE WHEN IsNull([FleetInformation].Engine2Hrs,0) > 0 THEN IsNull([FleetInformation].Engine2Hrs,0) - IsNull(temp.Engine2Hours,0) ELSE 0 END,
			Engine2Cycle = CASE WHEN IsNull([FleetInformation].Engine2Cycle,0) > 0 THEN IsNull([FleetInformation].Engine2Cycle,0) - IsNull(temp.Engine2Cycle,0) ELSE 0 END, 
			Engine2AsOfDT = @LastUpdTS,
			Engine3Hrs = CASE WHEN IsNull([FleetInformation].Engine3Hrs,0) > 0 THEN IsNull([FleetInformation].Engine3Hrs,0) - IsNull(temp.Engine3Hours,0) ELSE 0 END,
			Engine3Cycle = CASE WHEN IsNull([FleetInformation].Engine3Cycle,0) > 0 THEN IsNull([FleetInformation].Engine3Cycle,0) - IsNull(temp.Engine3Cycle,0) ELSE 0 END,
			Engine3AsOfDT = @LastUpdTS,
			Engine4Hrs = CASE WHEN IsNull([FleetInformation].Engine4Hrs,0) > 0 THEN IsNull([FleetInformation].Engine4Hrs,0) - IsNull(temp.Engine4Hours,0) ELSE 0 END,
			Engine4Cycle = CASE WHEN IsNull([FleetInformation].Engine4Cycle,0) > 0 THEN IsNull([FleetInformation].Engine4Cycle,0) - IsNull(temp.Engine4Cycle,0) ELSE 0 END,
			Engine4AsOfDT = @LastUpdTS,
			
			Reverse1Hrs = CASE WHEN IsNull([FleetInformation].Reverse1Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse1Hrs,0) - IsNull(temp.Reverse1Hrs,0) ELSE 0 END,
			Reverser1Cycle = CASE WHEN IsNull([FleetInformation].Reverser1Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser1Cycle,0) - IsNull(temp.Reverser1Cycle,0) ELSE 0 END, 
			Reverser1AsOfDT = @LastUpdTS,
			Reverse2Hrs = CASE WHEN IsNull([FleetInformation].Reverse2Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse2Hrs,0) - IsNull(temp.Reverse2Hrs,0) ELSE 0 END,
			Reverser2Cycle = CASE WHEN IsNull([FleetInformation].Reverser2Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser2Cycle,0) - IsNull(temp.Reverser2Cycle,0) ELSE 0 END, 
			Reverser2AsOfDT = @LastUpdTS,
			Reverse3Hrs = CASE WHEN IsNull([FleetInformation].Reverse3Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse3Hrs,0) - IsNull(temp.Reverse3Hrs,0) ELSE 0 END,
			Reverser3Cycle = CASE WHEN IsNull([FleetInformation].Reverser3Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser3Cycle,0) - IsNull(temp.Reverser3Cycle,0) ELSE 0 END,
			Reverser3AsOfDT = @LastUpdTS,
			Reverse4Hrs = CASE WHEN IsNull([FleetInformation].Reverse4Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse4Hrs,0) - IsNull(temp.Reverse4Hrs,0) ELSE 0 END,
			Reverser4Cycle = CASE WHEN IsNull([FleetInformation].Reverser4Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser4Cycle,0) - IsNull(temp.Reverser4Cycle,0) ELSE 0 END,
			Reverser4AsOfDT = @LastUpdTS,
				
			LastUpdTS = @LastUpdTS
		FROM @FleetInfo temp, [FleetInformation]
		WHERE [FleetInformation].CustomerID = @CustomerID AND [FleetInformation].FleetID = @FleetID AND isnull([FleetInformation].IsDeleted,0) = 0					
		
	END

	--// Set the IsLog Status into the PreflightMain Table
	DECLARE @TripID BIGINT
	
	SELECT @TripID = TripID FROM [PostflightMain] WHERE POLogID = @POLogID
	
	UPDATE PreflightMain SET IsLog = 0 WHERE TripID = @TripID AND [CustomerID] = @CustomerID
	
	--// Call Delete after deduct airframe and engine hours from fleet information table
	--DELETE FROM [PostflightMain]
	--	WHERE (
	--	[POLogID] = @POLogID AND
	--	[CustomerID] = @CustomerID
	
	UPDATE PostflightMain  
	SET 
	--[LastUpdUID] = @LastUpdUID  
	[LastUpdTS] = @LastUpdTS  
	,[IsDeleted] = 1  
	WHERE POLogID = @POLogID and CustomerID=@CustomerID 		
	
END

GO


