/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllFlightLogReset]    Script Date: 04/15/2013 16:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllFlightLogReset]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllFlightLogReset]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllFlightLogReset]    Script Date: 04/15/2013 16:50:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spFlightPak_GetAllFlightLogReset]  
(  
 @CustomerID BIGINT   
)  
AS  
BEGIN  
SET NOCOUNT ON;  
SELECT   
 [TSDefinitionLogID] as TSFlightLogID,  
    [CustomerID],  
 [PrimeKey],  
 [HomebaseID],  
 [OriginalDescription],  
 [CustomDescription],  
 [SequenceOrder],  
 [IsPrint],  
 [LastUpdUID],  
 [LastUpdTS],  
 [Category],  
 [IsDeleted],
 [IsInActive]  
FROM TSDefinitionLog  
WHERE CustomerID = @CustomerID  AND ISNULL(IsDeleted,0) = 0 AND Category= 'FLIGHT LOG'  
END  
GO


