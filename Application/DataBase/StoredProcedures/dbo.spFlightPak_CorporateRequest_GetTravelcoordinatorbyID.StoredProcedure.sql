/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetTravelcoordinatorbyID]    Script Date: 04/18/2013 20:42:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetTravelcoordinatorbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetTravelcoordinatorbyID]
GO


Create procedure [dbo].[spFlightPak_CorporateRequest_GetTravelcoordinatorbyID]
(@CustomerID BIGINT,@TravelCoordinatorID BIGINT)  
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 18/4/2013    
-- Description: Get the Travelcoordinator information    
-- =============================================    
SET NOCOUNT ON    
SELECT [TravelCoordinatorID]
      ,[CustomerID]
      ,[TravelCoordCD]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[PhoneNum]
      ,[FaxNum]
      ,[PagerNum]
      ,[CellPhoneNum]
      ,[EmailID]
      ,[Notes]
      ,[HomebaseID]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,[BusinessPhone]
      ,[HomeFax]
      ,[CellPhoneNum2]
      ,[OtherPhone]
      ,[BusinessEmail]
      ,[PersonalEmail]
      ,[OtherEmail]
      ,[Addr3]
      ,[IsInActive]
  FROM TravelCoordinator
  where IsDeleted = 0  AND TravelCoordinatorID=@TravelCoordinatorID
GO


