
GO
/****** Object:  StoredProcedure [dbo].[spAddBookmark]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddBookmark]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddBookmark]
GO

CREATE PROCEDURE spAddBookmark
(@UserPreferenceID bigint
,@CustomerID bigint
,@UserName varchar(30)
,@CategoryName varchar(100)
,@SubCategoryName varchar(100)
,@KeyName varchar(50)
,@KeyValue varchar(300)
,@LastUpdTS datetime)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()
	
	IF (@KeyName = '-1') AND (@KeyValue = '-1')
		BEGIN
			DELETE FROM UserPreference WHERE UserPreferenceID = @UserPreferenceID
		END
	ELSE
		BEGIN
			IF NOT EXISTS (SELECT UserPreferenceID FROM UserPreference WHERE CustomerID = @CustomerID
														AND UserPreferenceID = @UserPreferenceID)
				BEGIN
					--FlightpakSequence
					--EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'UtilityCurrentNo',  @UserPreferenceID OUTPUT
					SELECT @UserPreferenceID = MAX(ISNULL(UserPreferenceID,0)) + 1 FROM UserPreference
					
					INSERT INTO UserPreference
						([UserPreferenceID]
						,[CustomerID]
						,[UserName]
						,[CategoryName]
						,[SubCategoryName]
						,[KeyName]
						,[KeyValue]
						,[LastUpdTS])
					VALUES
						(@UserPreferenceID
						,@CustomerID
						,@UserName
						,@CategoryName
						,@SubCategoryName
						,@KeyName
						,@KeyValue
						,@LastUpdTS)
			
				END
			ELSE
				BEGIN
				
					UPDATE UserPreference
					SET UserName = @UserName
						,CategoryName = @CategoryName
						,SubCategoryName = @SubCategoryName
						,KeyName = @KeyName
						,KeyValue = @KeyValue
						,LastUpdTS = @LastUpdTS
					WHERE CustomerID = @CustomerID AND UserPreferenceID = @UserPreferenceID
				
				END
		END
	
	

	SELECT	[UserPreferenceID]
			,[CustomerID]
			,[UserName]
			,[CategoryName]
			,[SubCategoryName]
			,[KeyName]
			,[KeyValue]
			,[LastUpdTS]
	FROM UserPreference WHERE UserPreferenceID = @UserPreferenceID
	
END