/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllFlightCategory]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllFlightCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllFlightCategory]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllFlightCategory]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetAllFlightCategory] (@CustomerID bigint,@ClientID bigint,@FlightCategoryID bigint,@FlightCatagoryCD char(4),@FetchInactiveOnly BIT=0 )      
AS      
-- ==================================================      
-- Author: Karthikeyan.S    
-- Create date: 06/03/2014      
-- Description: Get Flight Category Informations 
-- EXEC [sp_fss_GetAllFlightCategory] 10003,0,0,'',1
-- EXEC sp_fss_GetAllFlightCategory 10005,100055,0,'',0      
-- ==================================================     
SET NOCOUNT ON      

BEGIN

 SELECT     [FlightCategoryID]      
           ,CASE WHEN [FlightCatagoryCD] IS NULL THEN '' ELSE LTRIM(RTRIM([FlightCatagoryCD])) END AS FlightCatagoryCD      
           ,[FlightCatagoryDescription]      
           ,[CustomerID]      
           ,[GraphColor]      
           ,[ClientID]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[ForeGrndCustomColor]      
           ,[BackgroundCustomColor]      
           ,[CallKey]      
           ,ISNULL(IsInActive,0) as IsInActive      
           ,[IsDeleted]    
           ,[IsDeadorFerryHead]
FROM  [FlightCatagory] WHERE 
CustomerID=@CustomerID
AND FlightCategoryID = CASE when (@FlightCategoryID <> 0) then @FlightCategoryID ELSE FlightCategoryID END
AND isnull(ClientID,0) = CASE when @ClientID <> 0 then @ClientID ELSE isnull(ClientID,0) END  
AND FlightCatagoryCD = CASE when (LTRIM(RTRIM(@FlightCatagoryCD)) <> '') then @FlightCatagoryCD else FlightCatagoryCD END
AND ISNULL(IsInActive,0) = CASE when @FetchInactiveOnly =1 then ISNULL(IsInActive,0) ELSE 0 END       
AND ISNULL(IsDeleted,0) = 0
order by FlightCatagoryCD
END
GO

