/****** Object:  StoredProcedure [dbo].[spGetAircraftActualCount]  ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAircraftActualCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAircraftActualCount]
GO

/****** Object:  StoredProcedure [dbo].[spGetAircraftActualCount]     ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spGetAircraftActualCount  
(@CustomerId bigint)  
AS BEGIN  
SELECT AircraftCount as AircraftCount FROM CustomerLicense where CustomerID = @CustomerId  
END