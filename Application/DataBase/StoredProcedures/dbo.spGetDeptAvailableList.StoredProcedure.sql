

/****** Object:  StoredProcedure [dbo].[spGetDeptAvailableList]    Script Date: 05/07/2013 12:15:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDeptAvailableList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetDeptAvailableList]
GO



/****** Object:  StoredProcedure [dbo].[spGetDeptAvailableList]    Script Date: 05/07/2013 12:15:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDeptAvailableList](@CustomerID BIGINT,    
            @DeptCode BIGINT,@ClientID BIGINT)      
-- =============================================      
-- Author: Sujitha.V      
-- Create date: 02/05/2012      
-- Description: Get the Available Department List      
-- =============================================      
AS      
SET NOCOUNT ON      
BEGIN      
  IF(@ClientID>0)    
  BEGIN    
SELECT [DepartmentID],    
    [DepartmentCD],      
    [DepartmentName],      
    [CustomerID],      
    [ClientID],      
    [LastUpdUID],      
    [LastUpdTS],      
    [IsInActive],      
    [IsDeleted],
    DepartPercentage     
 FROM [Department]     
 WHERE [Department].[DepartmentID] NOT IN(SELECT DepartmentGroupOrder.DepartmentID FROM DepartmentGroupOrder       
                       WHERE  DepartmentGroupOrder.DepartmentGroupID=@DeptCode)       
 AND [Department].CustomerID=@CustomerID and [Department].ClientID = @ClientID  AND [Department].IsDeleted = 0   
end    
ELSE    
BEGIN    
SELECT [DepartmentID],    
    [DepartmentCD],      
    [DepartmentName],      
    [CustomerID],      
    [ClientID],      
    [LastUpdUID],      
    [LastUpdTS],      
    [IsInActive],      
    [IsDeleted],
    DepartPercentage      
 FROM [Department]     
 WHERE [Department].[DepartmentID] NOT IN(SELECT DepartmentGroupOrder.DepartmentID FROM DepartmentGroupOrder       
                       WHERE  DepartmentGroupOrder.DepartmentGroupID=@DeptCode)       
 AND [Department].CustomerID=@CustomerID AND [Department].IsDeleted = 0  
END    
END
GO


