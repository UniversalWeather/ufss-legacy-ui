
GO
/****** Object:  StoredProcedure [dbo].[spGetAirportPair]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportPair]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportPair]
GO

CREATE PROCEDURE spGetAirportPair
(@AirportPairID	BIGINT,
@CustomerID	BIGINT,
@IsDeleted BIT)
AS BEGIN

	IF (@AirportPairID = -1)
		BEGIN
			SELECT	AirportPair.AirportPairID,
					AirportPair.CustomerID,
					AirportPair.AirportPairNumber,
					AirportPair.AircraftID,
					AirportPair.WindQTR,
					AirportPair.AirportPairDescription,
					AirportPair.LastUpdUID,
					AirportPair.LastUpdTS,
					AirportPair.IsDeleted,
					Aircraft.AircraftCD,
					Aircraft.AircraftDescription
			FROM	AirportPair
			LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = AirportPair.AircraftID
			WHERE	AirportPair.CustomerID = @CustomerID
					AND AirportPair.IsDeleted = @IsDeleted
		END
	ELSE
		BEGIN
			SELECT	AirportPair.AirportPairID,
					AirportPair.CustomerID,
					AirportPair.AirportPairNumber,
					AirportPair.AircraftID,
					AirportPair.WindQTR,
					AirportPair.AirportPairDescription,
					AirportPair.LastUpdUID,
					AirportPair.LastUpdTS,
					AirportPair.IsDeleted,
					Aircraft.AircraftCD,
					Aircraft.AircraftDescription
			FROM	AirportPair
			LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = AirportPair.AircraftID
			WHERE	AirportPair.CustomerID = @CustomerID
					AND AirportPair.IsDeleted = @IsDeleted
					AND AirportPair.AirportPairID = @AirportPairID
		END

END