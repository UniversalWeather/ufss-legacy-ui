  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_PostflightExp_Search]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_PostflightExp_Search]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spFlightPak_PostflightExp_Search]
(
 @CustomerID bigint
)  
AS  
BEGIN  
-- =============================================
-- 28-09-2012 - Added Company as left outer for getting Homebase Code and Slip Number
-- 08-11-2012 - Updated the ICAO Id link
-- 22-11-2012 - Updated Fuel Qty and Post Fuel Price - Bug 2837
-- 26-01-2013 - Fix the Data Issue by adding condition with Leg and Expense Table
-- 01-02-2012 - Fix the Filter issue
-- 06-03-2012 - Data not shown if the expenese directly added from the catalog, IsNull condition checked
-- =============================================
 SET NOCOUNT ON;  
	SELECT 
			isnull(PE.PostflightExpenseID,0) as 'PostflightExpenseID',ISNULL(POL.POLogID,0) as 'POLogID', ISNULL(PO.LogNum, 0) AS LogNum, ISNULL(POL.POLegID,0) as 'POLegID', ISNULL(POL.LegNUM, 0) AS LegNUM
			, ISNULL(icao.IcaoID,'') as 'IcaoID', ISNULL(F.TailNum,'') as 'TailNum', V.VendorCD, PT.PaymentTypeCD,
			ISNULL(FL.FuelLocatorCD,'') as 'FuelLocatorCD',AC.AccountNum,AC.AccountDescription, InvoiceNUM,isnull(ExpenseAMT,0) as 'ExpenseAMT',PurchaseDT,
			AccountPeriod,C.HomebaseID, ISNULL(FB.FBOCD,'') as 'FBOCD',ISNULL(FB.FBOVendor,'') as 'FBOVendor', ISNULL(FC.FlightCatagoryCD,'') as 'FlightCatagoryCD', PE.DispatchNUM,V.VendorContactName,
			isnull(UnitPrice,0) as 'UnitPrice',isnull(PE.PostEDPR,0) as 'PostedPrice',isnull(SateTAX,0) as 'SateTAX'
			, isnull(SlipNUM,0) AS 'SlipNUM',  A.ICAOID AS 'HomebaseCD'
			, isnull(PE.FuelQTY,0) as 'FuelQTY', isnull(PE.PostFuelPrice,0) as 'PostFuelPrice'
			, isnull(PE.FuelPurchase,0) as 'FuelPurchase', isnull(PE.FederalTAX,0) as 'FederalTAX'
			, isnull(PE.SaleTAX,0) as 'SaleTAX', PE.IsAutomaticCalculation, PE.IsBilling
			, isnull(PE.IsDeleted,0) as 'IsDeleted', PE.LastUpdUID, PE.LastUpdTS
			, isnull(CW.CrewCD,'') as 'CrewCD'
			, isnull(CW.CrewID,'') as 'CrewID'
		FROM
			PostflightExpense PE
			LEFT JOIN PostflightLeg POL ON PE.PoLegId = POL.POLegID 
			LEFT JOIN PostflightMain PO ON PO.PoLogId = POL.PoLogId
			LEFT JOIN Vendor V ON V.VendorID = PE.PaymentVendorID
			LEFT JOIN PaymentType PT ON PT.PaymentTypeID=PE.PaymentTypeID
			LEFT JOIN Account AC ON AC.AccountID=PE.AccountID
			LEFT JOIN FuelLocator FL ON FL.FuelLocatorID=PE.FuelLocatorID
			LEFT JOIN Fleet F ON F.FleetID=PE.FleetID
			LEFT JOIN Company C ON C.HomebaseID=PE.HomebaseID
			LEFT JOIN Airport A ON A.AirportID= C.HomebaseAirportID
			LEFT JOIN Airport icao ON icao.AirportID= PE.AirportID
			LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID=PE.FlightCategoryID
			LEFT JOIN FBO FB ON FB.FBOID=PE.FBOID
			LEFT JOIN Crew CW ON CW.CrewID=PE.CrewID
		WHERE
			PE.CustomerID= @CustomerID
		ORDER BY PO.LogNum, POL.LegNUM
END
GO

/*
exec [spFlightPak_PostflightExp_Search] 10003
*/
