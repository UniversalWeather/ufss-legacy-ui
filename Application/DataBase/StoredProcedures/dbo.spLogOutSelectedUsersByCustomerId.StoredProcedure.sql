
/****** Object:  StoredProcedure [dbo].[spLogOutSelectedUsersByCustomerId]    Script Date: 01/18/2013 09:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLogOutSelectedUsersByCustomerId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLogOutSelectedUsersByCustomerId]
GO


/****** Object:  StoredProcedure [dbo].[spLogOutSelectedUsersByCustomerId]    Script Date: 01/18/2013 09:05:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--This SP is used to get the list of all logged in users by customer id except the user who are requesting
CREATE PROC [dbo].[spLogOutSelectedUsersByCustomerId]
@CustomerId bigint,
@UserName varchar(30)
AS
BEGIN
 -- Delete login records on logout      
 UPDATE FPLoginSession SET LoginStatus = 0 WHERE  LoginSessionUID = @UserName AND CustomerID = @CustomerID
 --Release all locks made by the user in that session    
 DELETE FROM FPLockLog WHERE LockReqUID = @UserName AND CustomerID = @CustomerId

END






GO


