

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFile]
(
	@CQFileID bigint,
	@CustomerID bigint,
	@FileNUM int,
	@EstDepartureDT date,
	@QuoteDT date,
	@HomebaseID bigint,
	@CQFileDescription varchar(40),
	@CQCustomerID bigint,
	@CQCustomerName varchar(40),
	@CQCustomerContactID bigint,
	@CQCustomerContactName varchar(40),
	@PhoneNUM varchar(25),
	@FaxNUM varchar(25),
	@IsApplicationFiled bit,
	@IsApproved bit,
	@Credit numeric(17, 2),
	@DiscountPercentage numeric(5, 2),
	@SalesPersonID bigint,
	@LeadSourceID bigint,
	@Notes varchar(MAX),
	@QuoteCenter int,
	@Sync char(1),
	@ExpressQuote bit,
	@IsFinancialWarning bit,
	@IsErrorMessage bit,
	@CharterQuoteDTRONExcept char(1),
	@ExchangeRateID bigint,
	@ExchangeRate numeric(10, 5),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@EmailID VARCHAR(100),
	@UseCustomFleetCharge BIT
)
AS
	SET NOCOUNT ON;
	
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQFile] SET  [FileNUM] = @FileNUM, [EstDepartureDT] = @EstDepartureDT, [QuoteDT] = @QuoteDT, [HomebaseID] = @HomebaseID, [CQFileDescription] = @CQFileDescription, [CQCustomerID] = @CQCustomerID, [CQCustomerName] = @CQCustomerName, [CQCustomerContactID] = @CQCustomerContactID, [CQCustomerContactName] = @CQCustomerContactName, [PhoneNUM] = @PhoneNUM, [FaxNUM] = @FaxNUM, [IsApplicationFiled] = @IsApplicationFiled, [IsApproved] = @IsApproved, [Credit] = @Credit, [DiscountPercentage] = @DiscountPercentage, [SalesPersonID] = @SalesPersonID, [LeadSourceID] = @LeadSourceID, [Notes] = @Notes, [QuoteCenter] = @QuoteCenter, [Sync] = @Sync, [ExpressQuote] = @ExpressQuote, [IsFinancialWarning] = @IsFinancialWarning, [IsErrorMessage] = @IsErrorMessage, [CharterQuoteDTRONExcept] = @CharterQuoteDTRONExcept, [ExchangeRateID] = @ExchangeRateID, [ExchangeRate] = @ExchangeRate, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, [EmailID] = @EmailID 
		, [UseCustomFleetCharge] = @UseCustomFleetCharge
	WHERE 
		[CQFileID] = @CQFileID and [CustomerID] = @CustomerID
	
GO
