/****** Object:  StoredProcedure [dbo].[spGetAircraftByAircraftID]    Script Date: 03/27/2013 19:17:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAircraftByAircraftID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAircraftByAircraftID]
GO

CREATE PROCEDURE [dbo].[spGetAircraftByAircraftID](@CustomerID BIGINT, @AircraftID BIGINT)  
AS  
-- ====================================================================  
-- Author:  
-- Create date:   
-- Description: Get the Aircraft information for a specific AircraftID  
-- ====================================================================  
SET NOCOUNT ON  
  
BEGIN   
SELECT    
  
  AircraftID,  
  CustomerID,  
  AircraftCD,  
  AircraftDescription,  
  ChargeRate ,  
  ChargeUnit ,  
  PowerSetting ,  
  PowerDescription ,  
  WindAltitude ,  
  PowerSettings1Description ,  
  PowerSettings1TrueAirSpeed ,  
  PowerSettings1HourRange ,  
  PowerSettings1TakeOffBias ,  
  PowerSettings1LandingBias ,  
  PowerSettings2Description ,  
  PowerSettings2TrueAirSpeed ,  
  PowerSettings2HourRange ,  
  PowerSettings2TakeOffBias ,  
  PowerSettings2LandingBias ,  
  PowerSettings3Description ,  
  PowerSettings3TrueAirSpeed ,  
  PowerSettings3HourRange ,  
  PowerSettings3TakeOffBias ,  
  PowerSettings3LandingBias ,  
  IsFixedRotary ,  
  ClientID ,  
  CQChargeRate ,  
  CQChargeUnit ,  
  PositionRate ,  
  PostionUnit ,  
  StandardCrew ,  
  StandardCrewRON ,  
  AdditionalCrew ,  
  AdditionalCrewRON ,  
  CharterQuoteWaitTM ,  
  LandingFee ,  
  CostBy ,  
  FixedCost ,  
  PercentageCost ,  
  TaxRON ,  
  AdditionalCrewTax ,  
  IsCharterQuoteWaitTax ,  
  LandingTax ,  
  DescriptionRON ,  
  AdditionalCrewDescription ,  
  MinDailyREQ ,  
  LastUpdUID ,  
  LastUpdTS  ,  
  AircraftSize ,  
  AircraftTypeCD ,  
  IntlStdCrewNum ,  
  DomesticStdCrewNum ,  
  MinimumDayUseHrs ,  
  DailyUsageAdjTax ,  
  LandingFeeTax ,  
  IsDeleted,
  IsInActive,
  MarginalPercentage 
  
  FROM  [Aircraft]   
  WHERE CustomerID = @CustomerID   
  AND AircraftID = @AircraftID  
    
END  
GO


