/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRMainCorpStatus]    Script Date: 02/19/2013 19:53:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_UpdateCRMainCorpStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRMainCorpStatus]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRMainCorpStatus]
(
			@CRMainID bigint
		   ,@CustomerID bigint 
           ,@CorporateRequestStatus char(1)
           ,@LastUpdUID varchar(30)
)
AS
BEGIN 
SET NOCOUNT OFF
	DECLARE @LastUpdTS DATETIME
	SET @LastUpdTS = GETUTCDATE()

	UPDATE CRMain
	SET	CorporateRequestStatus = 'S',
		LastUpdUID = @LastUpdUID,
		LastUpdTS = @LastUpdTS
	WHERE CRMainID = @CRMainID

END
GO


