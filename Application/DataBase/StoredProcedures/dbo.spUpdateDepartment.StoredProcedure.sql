
/****** Object:  StoredProcedure [dbo].[spUpdateDepartment]    Script Date: 05/07/2013 12:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateDepartment]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateDepartment]    Script Date: 05/07/2013 12:41:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateDepartment]  
(  
 @DepartmentID BIGINT,  
 @DepartmentCD VARCHAR(8),  
 @DepartmentName VARCHAR(25),  
 @CustomerID BIGINT,
 @ClientID BIGINT,  
 @LastUpdUID VARCHAR(30),  
 @LastUpdTS DATETIME,  
 @IsInActive BIT,  
 @IsDeleted BIT,
 @DepartPercentage Numeric(7,2)    
)  
-- =============================================  
-- Author: Sujitha.V  
-- Create date: 5/5/2012  
-- Description: Update the Department information  
-- =============================================  
AS
BEGIN  
SET NOCOUNT ON  
 SET @LastUpdTS = GETUTCDATE() 
 UPDATE Department  
 SET [DepartmentCD]=@DepartmentCD, 
	 [DepartmentName]=@DepartmentName,
	 [CustomerID]=@CustomerID,   
	 [ClientID]=@ClientID,  
	 [LastUpdUID]=@LastUpdUID,  
	 [LastUpdTS]=@LastUpdTS,  
	 [IsInActive]=@IsInActive,  
	 [IsDeleted]=@IsDeleted,
	 DepartPercentage = @DepartPercentage   
 WHERE [DepartmentID] = @DepartmentID and [DepartmentCD]=@DepartmentCD  
  
END

GO


