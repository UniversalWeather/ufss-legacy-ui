/****** Object:  StoredProcedure [dbo].[spResetFlightLog]    Script Date: 04/15/2013 16:49:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spResetFlightLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spResetFlightLog]
GO


/****** Object:  StoredProcedure [dbo].[spResetFlightLog]    Script Date: 04/15/2013 16:49:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spResetFlightLog]  
(@TSFlightLogID BIGINT    
,@CustomerID BIGINT  
,@IsPrint BIT  
,@HomebaseID BIGINT  
,@Category VARCHAR(10)  
,@IsDeleted BIT)      
-- =============================================      
-- Author:Srinivasan.J  
-- Create date: 3/10/2012      
-- Description: Reset Flight log/ Pilot log      
-- Category : Flight Log/ PILOT LOG  
-- =============================================      
AS BEGIN  
 -- Assumed @Print as Reset all or only CrewLog Custom label1  
 DECLARE @TempFlightLog TABLE            
 (            
  [TSFlightLogID] [bigint] NOT NULL,  
  [CustomerID] [bigint] NULL,  
  [PrimeKey] [int] NULL,  
  [HomebaseID] [bigint] NULL,  
  [OriginalDescription] [varchar](25) NULL,  
  [CustomDescription] [varchar](25) NULL,  
  [SequenceOrder] [int] NULL,  
  [IsPrint] [bit] NULL,  
  [LastUpdUID] [varchar](30) NULL,  
  [LastUpdTS] [datetime] NULL,  
  [Category] [varchar](10) NULL,  
  [IsDeleted] [bit] NOT NULL,
  [IsInActive] [bit] NOT NULL  
 )                    
 IF (@IsPrint = 0)  
 BEGIN  
  IF (@Category = 'FLIGHT LOG')  
  BEGIN  
   --reset and fetch flight log records reseting only CrewLog Custom label1 and CrewLog Custom label2  
   UPDATE TSFlightLog SET CustomDescription = OriginalDescription  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category=@Category  
    AND (OriginalDescription = 'CrewLog Custom label 1'   
    OR OriginalDescription = 'CrewLog Custom label 2')  
  END  
  IF (@Category = 'PILOT LOG')  
  BEGIN  
   --reset and fetch pilot log records reseting only CrewLog Custom label3 and CrewLog Custom label4  
   UPDATE TSFlightLog SET CustomDescription = OriginalDescription  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category=@Category  
    AND (OriginalDescription = 'CrewLog Custom label 3'   
    OR OriginalDescription = 'CrewLog Custom label 4')  
  END  
    
  SELECT TSFlightLogID as TSFlightLogID,CustomerID,PrimeKey,HomebaseID,OriginalDescription,  
    CustomDescription,SequenceOrder,IsPrint,Category,LastUpdUID,LastUpdTS,IsDeleted, IsInActive  
  FROM TSFlightLog WHERE CustomerID=@CustomerID  
       AND HomebaseID=@HomebaseID  
       AND Category=@Category  
       AND ISNULL(IsDeleted,0)=0  
  ORDER BY OriginalDescription ASC  
 END  
 ELSE IF (@IsPrint = 1)  
 BEGIN  
  --delete all the record of flight log based on customerid and homebaseid  
  IF (@Category = 'FLIGHT LOG')  
  BEGIN     
   DELETE FROM TSFlightLog WHERE CustomerID=@CustomerID  
          AND HomebaseID=@HomebaseID  
          AND Category=@Category  
          --AND (OriginalDescription <> 'CrewLog Custom label 1'  
          --AND OriginalDescription <> 'CrewLog Custom label 2')  
     
   EXECUTE spFlightPak_GetAllFlightLog @CustomerID, @HomebaseID        
  END  
  --delete all the record of pilot log based on customerid and homebaseid  
  IF (@Category = 'PILOT LOG')  
  BEGIN  
   DELETE FROM TSFlightLog WHERE CustomerID=@CustomerID  
          AND HomebaseID=@HomebaseID  
          AND Category=@Category  
          --AND (OriginalDescription <> 'CrewLog Custom label 3'  
          --AND OriginalDescription <> 'CrewLog Custom label 4')     
     
   EXECUTE spFlightPak_GetAllPilotLog @CustomerID, @HomebaseID     
  END  
    
  SELECT TSFlightLogID as TSFlightLogID,CustomerID,PrimeKey,HomebaseID,OriginalDescription,  
    CustomDescription,SequenceOrder,IsPrint,Category,LastUpdUID,LastUpdTS,IsDeleted,IsInActive 
  FROM TSFlightLog WHERE CustomerID=@CustomerID  
       AND HomebaseID=@HomebaseID  
       AND Category=@Category  
       AND ISNULL(IsDeleted,0)=0  
  ORDER BY OriginalDescription ASC  
 END    
END  
GO


