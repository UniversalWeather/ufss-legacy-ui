
/****** Object:  StoredProcedure [dbo].[spGetCQReportDetail]    Script Date: 04/25/2013 15:02:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCQReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCQReportDetail]
GO


/****** Object:  StoredProcedure [dbo].[spGetCQReportDetail]    Script Date: 04/25/2013 15:02:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetCQReportDetail]
(@CustomerID bigint)
AS BEGIN

	SELECT CQReportDetailID,
			CustomerID,
			CQReportHeaderID,
			ReportNumber,
			ReportDescription,
			IsSelected,
			Copies,
			ReportProcedure,
			ParameterCD,
			ParamterDescription,
			ParamterType,
			ParameterWidth,
			ParameterCValue,
			ParameterLValue,
			ParameterNValue,
			ParameterValue,
			ReportOrder,
			--TripSheetOrder,
			LastUpdUID,
			LastUpdTS,
			IsDeleted,
			ParameterSize,
			WarningMessage
		FROM CQReportDetail
	WHERE CustomerID = @CustomerID AND ISNULL(IsDeleted,0)=0

END


GO


