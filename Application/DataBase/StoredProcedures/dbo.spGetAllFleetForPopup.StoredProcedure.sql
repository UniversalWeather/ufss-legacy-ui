

/****** Object:  StoredProcedure [dbo].[spGetAllFleetForPopup]    Script Date: 06/07/2013 14:38:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetForPopup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetForPopup]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllFleetForPopup]    Script Date: 06/07/2013 14:38:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[spGetAllFleetForPopup](@CustomerID bigint,@ClientID bigint)            
AS   
  
SET NOCOUNT ON    
-- =============================================          
-- Author: Ramesh. J          
-- Create date: 11/6/2012          
-- Description: Get All Fleet, Vendor and Aircraft details for Popup    
-- =============================================          
        
  IF(@ClientID>0)     
  BEGIN       
 SELECT [FleetID]          
      ,[TailNum]          
      ,Fleet.[CustomerID]           
      ,Fleet.[AircraftCD]    
      ,Aircraft.AircraftCD as AirCraft_AircraftCD         
      ,[SerialNum]          
      ,[TypeDescription]          
      ,[MaximumReservation]          
      ,[LastInspectionDT]          
      ,[InspectionHrs]          
      ,[WarningHrs]          
      ,[MaximumPassenger]          
      ,Fleet.[HomebaseID]          
      ,[MaximumFuel]          
      ,[MinimumFuel]          
      ,[BasicOperatingWeight]          
      ,[MimimumRunway]  
      ,Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive          
      ,[IsDisplay31]          
      ,[SIFLMultiControlled]          
      ,[SIFLMultiNonControled]          
      ,Fleet.[Notes]          
      ,[ComponentCD]          
      ,Fleet.[ClientID]          
      ,[FleetType]          
      ,[FlightPhoneNum]          
      ,[Class]          
      ,Fleet.[VendorID]          
      ,Fleet.[VendorType]          
      ,[YearMade]          
      ,[ExteriorColor]          
      ,[InteriorColor]          
      ,[IsAFIS]          
      ,[IsUWAData]          
      ,Fleet.[LastUpdUID]          
      ,Fleet.[LastUpdTS]          
      ,[FlightPlanCruiseSpeed]          
      ,[FlightPlanMaxFlightLevel]          
      ,[MaximumTakeOffWeight]          
      ,[MaximumLandingWeight]          
      ,[MaximumWeightZeroFuel]          
      ,[TaxiFuel]          
      ,[MultiSec]          
      ,[ForeGrndCustomColor]          
      ,[BackgroundCustomColor]          
      ,[FlightPhoneIntlNum]          
      ,[FleetSize]          
      ,[FltScanDoc]          
      ,[MinimumDay]          
      ,[StandardCrewIntl]          
      ,[StandardCrewDOM]          
      ,[IsFAR91]          
      ,[IsFAR135]          
      ,FLEET.[EmergencyContactID]          
      ,[IsTaxDailyAdj]          
      ,[IsTaxLandingFee]          
      ,Fleet.[IsDeleted]          
      ,Fleet.[AircraftID]          
      ,Fleet.[CrewID]      
      ,SecondaryDomFlightPhone      
       ,SecondaryIntlFlightPhone    
       ,Client.ClientCD    
       ,Vendor.VendorCD    
       ,Airport.IcaoID    
       ,Aircraft.PowerSettings1TrueAirSpeed    
       ,Aircraft.PowerSettings2TrueAirSpeed    
       ,Aircraft.PowerSettings3TrueAirSpeed    
       --FROM [Fleet]     
   --LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
   --LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID                       
       --WHERE Fleet.CustomerID=@CustomerID AND ISNULL(Fleet.IsDeleted,0) = 0 and Fleet.ClientID=@ClientID      
         FROM [Fleet]   LEFT OUTER JOIN                  
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                  
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                  
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                  
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN       
                      Airport ON Company.HomebaseAirportID=Airport.AirportID LEFT OUTER JOIN              
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                  
                      Vendor ON Fleet.VendorID = Vendor.VendorID                       
                WHERE Fleet.CustomerID=@CustomerID AND Fleet.ClientID =@ClientID AND ISNULL(Fleet.IsDeleted,0) = 0                             
                      Order by Fleet.AircraftCD                                   
       END          
 ELSE          
       BEGIN          
       SELECT  [FleetID]          
      ,[TailNum]          
      ,Fleet.[CustomerID]           
      ,Fleet.[AircraftCD]    
      ,Aircraft.AircraftCD as AirCraft_AircraftCD         
      ,[SerialNum]          
      ,[TypeDescription]          
      ,[MaximumReservation]          
      ,[LastInspectionDT]          
      ,[InspectionHrs]          
      ,[WarningHrs]          
      ,[MaximumPassenger]          
      ,Fleet.[HomebaseID]          
      ,[MaximumFuel]          
      ,[MinimumFuel]          
      ,[BasicOperatingWeight]          
      ,[MimimumRunway]          
      ,Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive       
      ,[IsDisplay31]          
      ,[SIFLMultiControlled]          
      ,[SIFLMultiNonControled]          
      ,Fleet.[Notes]          
      ,[ComponentCD]          
      ,Fleet.[ClientID]          
      ,[FleetType]          
      ,[FlightPhoneNum]          
      ,[Class]          
      ,Fleet.[VendorID]          
      ,Fleet.[VendorType]          
      ,[YearMade]          
      ,[ExteriorColor]          
      ,[InteriorColor]          
      ,[IsAFIS]          
      ,[IsUWAData]          
      ,Fleet.[LastUpdUID]          
      ,Fleet.[LastUpdTS]          
      ,[FlightPlanCruiseSpeed]          
      ,[FlightPlanMaxFlightLevel]          
      ,[MaximumTakeOffWeight]          
      ,[MaximumLandingWeight]          
      ,[MaximumWeightZeroFuel]          
      ,[TaxiFuel]          
      ,[MultiSec]          
      ,[ForeGrndCustomColor]          
      ,[BackgroundCustomColor]          
      ,[FlightPhoneIntlNum]          
      ,[FleetSize]          
      ,[FltScanDoc]          
      ,[MinimumDay]          
      ,[StandardCrewIntl]          
      ,[StandardCrewDOM]          
      ,[IsFAR91]          
      ,[IsFAR135]          
      ,Fleet.[EmergencyContactID]          
      ,[IsTaxDailyAdj]          
      ,[IsTaxLandingFee]          
      ,Fleet.[IsDeleted]          
      ,Fleet.[AircraftID]          
      ,Fleet.[CrewID]      
      ,SecondaryDomFlightPhone      
      ,SecondaryIntlFlightPhone    
      ,Client.ClientCD    
      ,Vendor.VendorCD    
      ,Airport.IcaoID    
      ,Aircraft.PowerSettings1TrueAirSpeed    
      ,Aircraft.PowerSettings2TrueAirSpeed    
      ,Aircraft.PowerSettings3TrueAirSpeed    
       FROM [Fleet]     
   --LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
   --LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID                       
       --WHERE Fleet.CustomerID=@CustomerID AND ISNULL(Fleet.IsDeleted,0) = 0      
       LEFT OUTER JOIN                  
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                  
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                  
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                  
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN    
                      Airport ON Company.HomebaseAirportID=Airport.AirportID LEFT OUTER JOIN                  
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                  
                      Vendor ON Fleet.VendorID = Vendor.VendorID                       
                WHERE Fleet.CustomerID=@CustomerID AND ISNULL(Fleet.IsDeleted,0) = 0                  
                      Order by Fleet.AircraftCD     
       END 
GO


