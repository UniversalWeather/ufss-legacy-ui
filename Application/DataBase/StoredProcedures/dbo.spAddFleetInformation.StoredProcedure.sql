
/****** Object:  StoredProcedure [dbo].[spAddFleetInformation]    Script Date: 09/13/2012 12:45:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFleetInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFleetInformation]
GO

/****** Object:  StoredProcedure [dbo].[spAddFleetInformation]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddFleetInformation]  
           (@CustomerID BIGINT  
           ,@FleetID BIGINT  
           ,@AircraftCD CHAR(3)    
           ,@AirframeHrs NUMERIC(11,3)    
           ,@AirframeCycle NUMERIC(6,0)    
           ,@AIrframeAsOfDT DATE    
           ,@Engine1Hrs NUMERIC(11,3)    
           ,@Engine1Cycle NUMERIC(6,0)    
           ,@Engine1AsOfDT DATE    
           ,@Engine2Hrs NUMERIC(11,3)    
           ,@Engine2Cycle NUMERIC(6,0)    
           ,@Engine2AsOfDT DATE    
           ,@Engine3Hrs NUMERIC(11,3)    
           ,@Engine3Cycle NUMERIC(6,0)    
           ,@Engine3AsOfDT DATE    
           ,@Engine4Hrs NUMERIC(11,3)    
           ,@Engine4Cycle NUMERIC(6,0)    
           ,@Engine4AsOfDT DATE    
           ,@Reverse1Hrs NUMERIC(11,3)    
           ,@Reverser1Cycle NUMERIC(6,0)    
           ,@Reverser1AsOfDT DATE    
           ,@Reverse2Hrs NUMERIC(11,3)    
           ,@Reverser2Cycle NUMERIC(6,0)    
           ,@Reverser2AsOfDT DATE    
           ,@Reverse3Hrs NUMERIC(11,3)    
           ,@Reverser3Cycle NUMERIC(6,0)    
           ,@Reverser3AsOfDT DATE    
           ,@Reverse4Hrs NUMERIC(11,3)    
           ,@Reverser4Cycle NUMERIC(6,0)    
           ,@Reverser4AsOfDT DATE    
           ,@LastUpdUID VARCHAR(30)    
           ,@LastUpdTS DATETIME     
           ,@MACHFuel1Hrs NUMERIC(5,0)    
           ,@MACHFuel2Hrs NUMERIC(5,0)    
           ,@MACHFuel3Hrs NUMERIC(5,0)    
           ,@MACHFuel4Hrs NUMERIC(5,0)    
           ,@MACHFuel5Hrs NUMERIC(5,0)    
           ,@MACHFuel6Hrs NUMERIC(5,0)    
           ,@MACHFuel7Hrs NUMERIC(5,0)    
           ,@MACHFuel8Hrs NUMERIC(5,0)    
           ,@MACHFuel9Hrs NUMERIC(5,0)    
           ,@MACHFuel10Hrs NUMERIC(5,0)    
           ,@MACHFuel11Hrs NUMERIC(5,0)    
           ,@MACHFuel12Hrs NUMERIC(5,0)    
           ,@MACHFuel13Hrs NUMERIC(5,0)    
           ,@MACHFuel14Hrs NUMERIC(5,0)    
           ,@MACHFuel15Hrs NUMERIC(5,0)    
           ,@MACHFuel16Hrs NUMERIC(5,0)    
           ,@MACHFuel17Hrs NUMERIC(5,0)    
           ,@MACHFuel18Hrs NUMERIC(5,0)    
           ,@MACHFuel19Hrs NUMERIC(5,0)    
           ,@MACHFuel20Hrs NUMERIC(5,0) 
           ,@LongRangeFuel1Hrs NUMERIC(5,0)    
           ,@LongRangeFuel2Hrs NUMERIC(5,0)    
           ,@LongRangeFuel3Hrs NUMERIC(5,0)    
           ,@LongRangeFuel4Hrs NUMERIC(5,0)    
           ,@LongRangeFuel5Hrs NUMERIC(5,0)    
           ,@LongRangeFuel6Hrs NUMERIC(5,0)    
           ,@LongRangeFuel7Hrs NUMERIC(5,0)    
           ,@LongRangeFuel8Hrs NUMERIC(5,0)    
           ,@LongRangeFuel9Hrs NUMERIC(5,0)    
           ,@LongRangeFuel10Hrs NUMERIC(5,0)    
           ,@LongRangeFuel11Hrs NUMERIC(5,0)    
           ,@LongRangeFuel12Hrs NUMERIC(5,0)    
           ,@LongRangeFuel13Hrs NUMERIC(5,0)    
           ,@LongRangeFuel14Hrs NUMERIC(5,0)    
           ,@LongRangeFuel15Hrs NUMERIC(5,0) 
           ,@LongRangeFuel16Hrs NUMERIC(5,0)    
           ,@LongRangeFuel17Hrs NUMERIC(5,0)    
           ,@LongRangeFuel18Hrs NUMERIC(5,0)    
           ,@LongRangeFuel19Hrs NUMERIC(5,0)    
           ,@LongRangeFuel20Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel1Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel2Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel3Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel4Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel5Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel6Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel7Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel8Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel9Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel10Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel11Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel12Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel13Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel14Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel51Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel16Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel17Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel18Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel19Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel20Hrs NUMERIC(5,0)  
           ,@Fuel1Hrs NUMERIC(5,0)    
           ,@Fuel2Hrs NUMERIC(5,0)    
           ,@Fuel3Hrs NUMERIC(5,0)    
           ,@Fuel4Hrs NUMERIC(5,0)    
           ,@Fuel5Hrs NUMERIC(5,0)   
           ,@IsDeleted BIT)    
-- =============================================    
-- Author:Badrinath    
-- Create DATE: 12/4/2012    
-- Altered by Mathes on 05-07-12  
-- Description: Insert the Fleet  information    
-- =============================================    
AS      
 BEGIN   
 SET NoCOUNT ON  
 DECLARE @FleetInformationID BIGINT  
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetInformationID OUTPUT  
  set @LastUpdTS = GETUTCDATE()  
   INSERT INTO [FleetInformation]    
           (  
            [FleetInformationID]   
           ,[CustomerID]    
           ,[FleetID]    
           ,[AircraftCD]    
           ,[AirframeHrs]    
           ,[AirframeCycle]    
           ,[AIrframeAsOfDT]    
           ,[Engine1Hrs]    
           ,[Engine1Cycle]    
           ,[Engine1AsOfDT]    
           ,[Engine2Hrs]     
           ,[Engine2Cycle]     
           ,[Engine2AsOfDT]    
           ,[Engine3Hrs]    
           ,[Engine3Cycle]    
           ,[Engine3AsOfDT]    
           ,[Engine4Hrs]    
           ,[Engine4Cycle]    
           ,[Engine4AsOfDT]    
           ,[Reverse1Hrs]    
           ,[Reverser1Cycle]    
           ,[Reverser1AsOfDT]    
           ,[Reverse2Hrs]    
           ,[Reverser2Cycle]    
           ,[Reverser2AsOfDT]    
           ,[Reverse3Hrs]    
           ,[Reverser3Cycle]    
           ,[Reverser3AsOfDT]    
           ,[Reverse4Hrs]    
           ,[Reverser4Cycle]    
           ,[Reverser4AsOfDT]    
           ,[LastUpdUID]    
           ,[LastUpdTS]     
           ,[MACHFuel1Hrs]     
           ,[MACHFuel2Hrs]    
           ,[MACHFuel3Hrs]    
           ,[MACHFuel4Hrs]    
           ,[MACHFuel5Hrs]    
           ,[MACHFuel6Hrs]    
           ,[MACHFuel7Hrs]    
           ,[MACHFuel8Hrs]    
           ,[MACHFuel9Hrs]    
           ,[MACHFuel10Hrs]    
           ,[MACHFuel11Hrs]    
           ,[MACHFuel12Hrs]    
           ,[MACHFuel13Hrs]    
           ,[MACHFuel14Hrs]    
           ,[MACHFuel15Hrs]                  
           ,[MACHFuel16Hrs]     
           ,[MACHFuel17Hrs]     
           ,[MACHFuel18Hrs]     
           ,[MACHFuel19Hrs]     
           ,[MACHFuel20Hrs]  
           ,[LongRangeFuel1Hrs]    
           ,[LongRangeFuel2Hrs]    
           ,[LongRangeFuel3Hrs]    
           ,[LongRangeFuel4Hrs]    
           ,[LongRangeFuel5Hrs]    
           ,[LongRangeFuel6Hrs]    
           ,[LongRangeFuel7Hrs]    
           ,[LongRangeFuel8Hrs]    
           ,[LongRangeFuel9Hrs]    
           ,[LongRangeFuel10Hrs]    
           ,[LongRangeFuel11Hrs]    
           ,[LongRangeFuel12Hrs]    
           ,[LongRangeFuel13Hrs]    
           ,[LongRangeFuel14Hrs]    
           ,[LongRangeFuel15Hrs]  
		   ,[LongRangeFuel16Hrs]
	       ,[LongRangeFuel17Hrs]
           ,[LongRangeFuel18Hrs]
           ,[LongRangeFuel19Hrs]
           ,[LongRangeFuel20Hrs]             
           ,[HighSpeedFuel1Hrs]    
           ,[HighSpeedFuel2Hrs]    
           ,[HighSpeedFuel3Hrs]    
           ,[HighSpeedFuel4Hrs]    
           ,[HighSpeedFuel5Hrs]    
           ,[HighSpeedFuel6Hrs]    
           ,[HighSpeedFuel7Hrs]    
           ,[HighSpeedFuel8Hrs]    
           ,[HighSpeedFuel9Hrs]    
           ,[HighSpeedFuel10Hrs]    
           ,[HighSpeedFuel11Hrs]    
           ,[HighSpeedFuel12Hrs]    
           ,[HighSpeedFuel13Hrs]    
           ,[HighSpeedFuel14Hrs]    
           ,[HighSpeedFuel51Hrs]   
		   ,[HighSpeedFuel16Hrs]
		   ,[HighSpeedFuel17Hrs]
		   ,[HighSpeedFuel18Hrs]
		   ,[HighSpeedFuel19Hrs]
		   ,[HighSpeedFuel20Hrs]            
           ,[Fuel1Hrs]    
           ,[Fuel2Hrs]    
           ,[Fuel3Hrs]    
           ,[Fuel4Hrs]    
           ,[Fuel5Hrs]    
           ,[IsDeleted]  )    
     VALUES    
           (  
            @FleetInformationID    
           ,@CustomerID    
           ,@FleetID    
           ,@AircraftCD    
           ,@AirframeHrs    
           ,@AirframeCycle    
           ,@AIrframeAsOfDT    
           ,@Engine1Hrs    
           ,@Engine1Cycle    
           ,@Engine1AsOfDT    
           ,@Engine2Hrs    
           ,@Engine2Cycle    
           ,@Engine2AsOfDT    
           ,@Engine3Hrs    
           ,@Engine3Cycle    
           ,@Engine3AsOfDT    
           ,@Engine4Hrs    
           ,@Engine4Cycle    
           ,@Engine4AsOfDT    
           ,@Reverse1Hrs    
           ,@Reverser1Cycle    
           ,@Reverser1AsOfDT    
           ,@Reverse2Hrs    
           ,@Reverser2Cycle    
           ,@Reverser2AsOfDT    
           ,@Reverse3Hrs    
           ,@Reverser3Cycle    
           ,@Reverser3AsOfDT    
           ,@Reverse4Hrs    
           ,@Reverser4Cycle    
           ,@Reverser4AsOfDT    
           ,@LastUpdUID    
           ,@LastUpdTS     
           ,@MACHFuel1Hrs    
           ,@MACHFuel2Hrs    
           ,@MACHFuel3Hrs    
           ,@MACHFuel4Hrs    
           ,@MACHFuel5Hrs    
           ,@MACHFuel6Hrs    
           ,@MACHFuel7Hrs    
           ,@MACHFuel8Hrs    
           ,@MACHFuel9Hrs    
           ,@MACHFuel10Hrs    
           ,@MACHFuel11Hrs    
           ,@MACHFuel12Hrs    
           ,@MACHFuel13Hrs    
           ,@MACHFuel14Hrs    
           ,@MACHFuel15Hrs 
		   ,@MACHFuel16Hrs 
		   ,@MACHFuel17Hrs 
		   ,@MACHFuel18Hrs 
		   ,@MACHFuel19Hrs 
		   ,@MACHFuel20Hrs               
           ,@LongRangeFuel1Hrs    
           ,@LongRangeFuel2Hrs    
           ,@LongRangeFuel3Hrs    
           ,@LongRangeFuel4Hrs    
           ,@LongRangeFuel5Hrs    
           ,@LongRangeFuel6Hrs    
           ,@LongRangeFuel7Hrs    
           ,@LongRangeFuel8Hrs    
           ,@LongRangeFuel9Hrs    
           ,@LongRangeFuel10Hrs    
           ,@LongRangeFuel11Hrs    
           ,@LongRangeFuel12Hrs    
           ,@LongRangeFuel13Hrs    
           ,@LongRangeFuel14Hrs    
           ,@LongRangeFuel15Hrs  
		   ,@LongRangeFuel16Hrs
		   ,@LongRangeFuel17Hrs
		   ,@LongRangeFuel18Hrs
		   ,@LongRangeFuel19Hrs
		   ,@LongRangeFuel20Hrs             
           ,@HighSpeedFuel1Hrs    
           ,@HighSpeedFuel2Hrs    
           ,@HighSpeedFuel3Hrs    
           ,@HighSpeedFuel4Hrs    
           ,@HighSpeedFuel5Hrs    
           ,@HighSpeedFuel6Hrs    
           ,@HighSpeedFuel7Hrs    
           ,@HighSpeedFuel8Hrs    
           ,@HighSpeedFuel9Hrs    
           ,@HighSpeedFuel10Hrs    
           ,@HighSpeedFuel11Hrs    
           ,@HighSpeedFuel12Hrs    
           ,@HighSpeedFuel13Hrs    
           ,@HighSpeedFuel14Hrs    
           ,@HighSpeedFuel51Hrs 
		   ,@HighSpeedFuel16Hrs
		   ,@HighSpeedFuel17Hrs
		   ,@HighSpeedFuel18Hrs
		   ,@HighSpeedFuel19Hrs
		   ,@HighSpeedFuel20Hrs             
           ,@Fuel1Hrs    
           ,@Fuel2Hrs    
           ,@Fuel3Hrs    
           ,@Fuel4Hrs    
           ,@Fuel5Hrs    
           ,@IsDeleted    
           )    
    
END  