/****** Object:  StoredProcedure [dbo].[spGetAllCrewCheckList]    Script Date: 01/07/2013 19:43:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewCheckList]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllCrewCheckList]    Script Date: 01/07/2013 19:43:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetAllCrewCheckList](@CustomerID BIGINT)    
AS    
-- =============================================    
-- Author: SelvaKumar.S    
-- Create date: 11/5/2012    
-- Altered by Mathes on 05-07-12  
-- Description: Get All CrewCheckList    
-- =============================================    
SET NoCOUNT ON    
    
SELECT      [CrewCheckID]  
           ,[CustomerID]    
           ,CASE WHEN [CrewCheckCD] IS NULL THEN '' ELSE LTRIM(RTRIM([CrewCheckCD])) END AS CrewCheckCD
           ,[CrewChecklistDescription]    
           ,[ClientID]     
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsScheduleCheck]     
           ,[IsCrewCurrencyPlanner]    
           ,[IsDeleted] 
           ,[IsInActive]
FROM  [CrewCheckList]   WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0  
order by CrewCheckCD 
GO


