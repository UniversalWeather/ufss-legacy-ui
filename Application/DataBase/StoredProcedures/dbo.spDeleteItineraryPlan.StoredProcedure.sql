
GO
/****** Object:  StoredProcedure [dbo].[spDeleteItineraryPlan]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteItineraryPlan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteItineraryPlan]
GO

CREATE PROCEDURE spDeleteItineraryPlan
(@ItineraryPlanID	BIGINT
,@CustomerID	BIGINT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()

	UPDATE ItineraryPlan
    SET CustomerID  = @CustomerID       
       ,LastUpdUID  = @LastUpdUID
       ,LastUpdTS  = @LastUpdTS
       ,IsDeleted = @IsDeleted
    WHERE ItineraryPlanID = @ItineraryPlanID

END