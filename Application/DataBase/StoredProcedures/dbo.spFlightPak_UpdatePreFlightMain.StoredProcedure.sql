
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePreFlightMain]    Script Date: 03/01/2013 19:23:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdatePreFlightMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdatePreFlightMain]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePreFlightMain]    Script Date: 03/01/2013 19:23:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_UpdatePreFlightMain]  
(  
		  @TripID bigint
		,@CustomerID bigint
		,@TripNUM bigint
		,@PreviousNUM bigint
		,@TripDescription varchar(40)
		,@FleetID bigint
		,@DispatchNUM char(12)
		,@TripStatus char(1)
		,@EstDepartureDT date
		,@EstArrivalDT date
		,@RecordType char(1)
		,@AircraftID bigint
		,@PassengerRequestorID bigint
		,@RequestorFirstName varchar(63)
		,@RequestorMiddleName varchar(60)
		,@RequestorLastName varchar(60)
		,@RequestorPhoneNUM varchar(25)
		,@DepartmentDescription varchar(25)
		,@AuthorizationDescription varchar(25)
		,@RequestDT date
		,@IsCrew bit
		,@IsPassenger bit
		,@IsQuote bit
		,@IsScheduledServices bit
		,@IsAlert bit
		,@Notes text
		,@LogisticsHistory text
		,@HomebaseID bigint
		,@ClientID bigint
		,@IsAirportAlert bit
		,@IsLog bit
		,@BeginningGMTDT date
		,@EndingGMTDT date
		,@LastUpdUID char(30)
		,@LastUpdTS datetime
		,@IsPrivate bit
		,@IsCorpReq bit
		,@Acknowledge char(1)
		,@DispatchNotes text
		,@TripRequest char(1)
		,@WaitList char(1)
		,@FlightNUM char(12)
		,@FlightCost numeric(12, 2)
		,@AccountID bigint
		,@TripException text
		,@TripSheetNotes text
		,@CancelDescription varchar(60)
		,@IsCompleted bit
		,@IsFBOUpdFlag bit
		,@RevisionNUM numeric(3, 0)
		,@RevisionDescriptioin varchar(200)
		,@IsPersonal bit
		,@DsptnName varchar(60)
		,@ScheduleCalendarHistory text
		,@ScheduleCalendarUpdate datetime
		,@CommentsMessage text
		,@EstFuelQTY numeric(17, 3)
		,@CrewID bigint
		,@ReleasedBy varchar(60)
		,@IsDeleted bit
		,@VerifyNUM varchar(40)
		,@APISSubmit datetime
		,@APISStatus varchar(20)
		,@APISException text
		,@IsAPISValid bit
		,@DepartmentID bigint
		,@AuthorizationID bigint
		,@DispatcherUserName char(30)
		,@EmergencyContactID bigint
		,@FleetCalendarNotes varchar(max)
        ,@CrewCalendarNotes varchar(max)
        ,@FuelUnits numeric(1)
        ,@CQCustomerID bigint
		,@IsTripCopied bit
)    
AS    
BEGIN    
UPDATE PreFlightMain  
 SET    
		[TripNUM] = @TripNUM
		,[PreviousNUM] = @PreviousNUM
		,[TripDescription] = @TripDescription
		,[FleetID] = @FleetID
		,[DispatchNUM] = @DispatchNUM
		,[TripStatus] = @TripStatus
		,[EstDepartureDT] = @EstDepartureDT
		,[EstArrivalDT] = @EstArrivalDT
		,[RecordType] = @RecordType
		,[AircraftID] = @AircraftID
		,[PassengerRequestorID] = @PassengerRequestorID
		,[RequestorFirstName] = @RequestorFirstName
		,[RequestorMiddleName] = @RequestorMiddleName
		,[RequestorLastName] = @RequestorLastName
		,[RequestorPhoneNUM] = @RequestorPhoneNUM
		,[DepartmentDescription] = @DepartmentDescription
		,[AuthorizationDescription] = @AuthorizationDescription
		,[RequestDT] = @RequestDT
		,[IsCrew] = @IsCrew
		,[IsPassenger] = @IsPassenger
		,[IsQuote] = @IsQuote
		,[IsScheduledServices] = @IsScheduledServices
		,[IsAlert] = @IsAlert
		,[Notes] = @Notes
		,[LogisticsHistory] = @LogisticsHistory
		,[HomebaseID] = @HomebaseID
		,[ClientID] = @ClientID
		,[IsAirportAlert] = @IsAirportAlert
		,[IsLog] = @IsLog
		,[BeginningGMTDT] = @BeginningGMTDT
		,[EndingGMTDT] = @EndingGMTDT
		,[LastUpdUID] = @LastUpdUID
		,[LastUpdTS] = @LastUpdTS
		,[IsPrivate] = @IsPrivate
		,[IsCorpReq] = @IsCorpReq
		,[Acknowledge] = @Acknowledge
		,[DispatchNotes] = @DispatchNotes
		,[TripRequest] = @TripRequest
		,[WaitList] = @WaitList
		,[FlightNUM] = @FlightNUM
		,[FlightCost] = @FlightCost
		,[AccountID] = @AccountID
		,[TripException] = @TripException
		,[TripSheetNotes] = @TripSheetNotes
		,[CancelDescription] = @CancelDescription
		,[IsCompleted] = @IsCompleted
		,[IsFBOUpdFlag] = @IsFBOUpdFlag
		,[RevisionNUM] = @RevisionNUM
		,[RevisionDescriptioin] = @RevisionDescriptioin
		,[IsPersonal] = @IsPersonal
		,[DsptnName] = @DsptnName
		,[ScheduleCalendarHistory] = @ScheduleCalendarHistory
		,[ScheduleCalendarUpdate] = @ScheduleCalendarUpdate
		,[CommentsMessage] = @CommentsMessage
		,[EstFuelQTY] = @EstFuelQTY
		,[CrewID] = @CrewID
		,[ReleasedBy] = @ReleasedBy
		,[IsDeleted] = @IsDeleted
		,[VerifyNUM] = @VerifyNUM
		,[APISSubmit] = @APISSubmit
		,[APISStatus] = @APISStatus
		,[APISException] = @APISException
		,[IsAPISValid] = @IsAPISValid
		,[DepartmentID] = @DepartmentID
		,[AuthorizationID] = @AuthorizationID
		,[DispatcherUserName] = @DispatcherUserName
		,[EmergencyContactID] = @EmergencyContactID
		,[FleetCalendarNotes]=@FleetCalendarNotes
        ,[CrewCalendarNotes]=@CrewCalendarNotes
        ,[FuelUnits] = @FuelUnits
        ,CQCustomerID = @CQCustomerID
		,IsTripCopied = @IsTripCopied


 WHERE   
		[TripID] = @TripID and
		[CustomerID] = @CustomerID
END


GO


