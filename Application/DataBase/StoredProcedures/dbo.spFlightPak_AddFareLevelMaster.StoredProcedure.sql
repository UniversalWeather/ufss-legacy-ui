
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddFareLevelMaster]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddFareLevelMaster]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddFareLevelMaster]
GO
CREATE PROCEDURE [dbo].[spFlightPak_AddFareLevelMaster]  
(@FareLevelID bigint  
,@FareLevelCD bigint  
,@CustomerID bigint  
,@StartDT date  
,@EndDT date  
,@Rate1 numeric(8,4)  
,@Rate2 numeric(8,4)  
,@Rate3 numeric(8,4)  
,@TerminalCHG numeric(8,4)  
,@LastUpdUID varchar(30)  
,@LastUpdTS datetime  
,@IsDeleted bit
,@IsInActive bit)  
-- =============================================  
-- Author:Karthikeyan.S  
-- Create date: 14/5/2012  
-- Description: Insert the FareLevel information  
-- =============================================  
as  
begin   
SET NoCOUNT ON  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FareLevelID OUTPUT  
set @LastUpdTS = GETUTCDATE()  
select @FareLevelCD = isnull(max(FareLevelCD),0) + 1 from FareLevel where CustomerID = @CustomerID  
INSERT INTO [FareLevel]  
           ([FareLevelID]  
           ,FareLevelCD  
           ,[CustomerID]  
           ,[StartDT]  
           ,[EndDT]  
           ,[Rate1]  
           ,[Rate2]  
           ,[Rate3]  
           ,[TerminalCHG]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]
           ,[IsInActive])  
     VALUES  
   (@FareLevelID  
   ,@FareLevelCD  
   ,@CustomerID  
   ,@StartDT  
   ,@EndDT  
   ,@Rate1  
   ,@Rate2  
   ,@Rate3  
   ,@TerminalCHG  
   ,@LastUpdUID  
   ,@LastUpdTS  
   ,@IsDeleted
   ,@IsInActive)              
END  
GO
