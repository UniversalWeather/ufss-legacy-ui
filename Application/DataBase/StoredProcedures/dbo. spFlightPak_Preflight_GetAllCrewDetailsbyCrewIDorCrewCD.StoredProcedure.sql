
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]    Script Date: 10/08/2013 10:32:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]    Script Date: 10/08/2013 10:32:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--/*** PARAMETERS ***/  
  
--DECLARE @FleetID BIGINT  
--DECLARE @DepartDate DATETIME   
--DECLARE @ArrivalDate DATETIME   
--DECLARE @CustomerID BIGINT  
--DECLARE @CrewGroupID BIGINT   
--DECLARE @HomeBaseID BIGINT  
  
--SET @FleetID = 10001212621  
--SET @CustomerID = 10001  
  
--SET @DepartDate = GETDATE() - 10   
--SET @ArrivalDate = GETDATE() - 5  
--SET @HomeBaseID = 1000177437  
  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, '18-sep-2012' , '12-sep-2012', 10001262978, 1000177437, 10001212621  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, '8-sep-2012' , '3-sep-2012', null, 1000177437, 10001212621, NULL  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, null, '8-sep-2012' , '3-sep-2012', 0, 1000177437, 10001212621, NULL ,0 

  
--/*** PARAMETERS ***/  
  
  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]   
(  
 @CustomerID BIGINT,  
 @ClientID BIGINT,  -- Select Crew members available for a client (if not set)
 @DepartDate DATETIME,	-- UTC/GMT DATE
 @ArrivalDate DATETIME,	-- UTC/GMT DATE
 @CrewGroupID BIGINT,   -- Pick crew members of a particular group. If NULL or 0 is passed all Crew members will be avilable
 @HomeBaseID BIGINT,   -- For getting the Company Profile settings (Aircrafft Bias)
 @FleetID BIGINT,   -- For finding the Aircraft Type
 @ArrivalAirportID BIGINT = NULL, -- For finding visa
 @CurrentTripID  BIGINT,
 @MinDepartDate DATETIME,	-- Minimum UTC/GMT DATE
 @MaxArrivalDate DATETIME,	-- Maximum UTC/GMT DATE
 @AircraftID BIGINT,
 @CrewID BIGINT,
 @CrewCD varchar(5)
)  
AS

DECLARE @TodayDate            DATE  
DECLARE @180DaysStartDate     DATE  
DECLARE @90DaysStartDate      DATE   
DECLARE @30DaysStartDate      DATE  
DECLARE @7DaysStartDate       DATE  
DECLARE @MonthStartDate       DATE  

  



  
  
  
DECLARE @AircraftTypeCD VARCHAR(10)   
DECLARE @AircraftDescription VARCHAR(15)   
DECLARE @AircraftTypeID BIGINT  
DECLARE @AircraftBasis INT  

DECLARE @Notified BIT 


DECLARE @CountryID	BIGINT 
  

  
SET @TodayDate = GETUTCDATE() --CONVERT(DATE, '2010.8.8', 120)  
SET @180DaysStartDate = DATEADD(DAY, -180, @TodayDate)   
SET @90DaysStartDate = DATEADD(DAY, -90, @TodayDate)   
SET @30DaysStartDate = DATEADD(DAY, -30, @TodayDate)   
SET @7DaysStartDate = DATEADD(DAY, -7, @TodayDate)   
SET @MonthStartDate = CONVERT(DATE, CONVERT(VARCHAR, YEAR(@TodayDate)) + '.' + CONVERT(VARCHAR, MONTH(@TodayDate))+ '.' + '01', 120)  

SET @CrewGroupID = ISNULL(@CrewGroupID, 0)
SET @CurrentTripID = ISNULL(@CurrentTripID, 0)

SET @Notified = 0
  


  
  
  SELECT @CountryID = CountryID FROM Airport WITH (NOLOCK)
  WHERE AirportID = @ArrivalAirportID  
  
  
  
  
  
  

  
  
/* START: CALCULATE THE CREW DATA FROM PostflightCrew TABLE ***/  
  
  
/* END: CALCULATE THE CREW DATA FROM PostflightCrew TABLE ***/  
  
/* START: CALCULATE THE CREW DATA FROM PostFlightSimulatorLog TABLE ***/  
  
/* END: CALCULATE THE CREW DATA FROM PostFlightSimulatorLog TABLE ***/  
  
  
  
  
  
/*** Final Select ***/  
  
 
 if @CrewCD <>''
 BEGIN
 SELECT   
  CRW.CrewID,  
  CRW.CrewCD,  
  (RTRIM(ISNULL(CRW.LastName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.FirstName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.MiddleInitial,'')))   
  AS CrewName,   
 0 AS [Standby],  
  0 AS TailAssigned,  
  @AircraftTypeCD as Actype,
 0 AS Conflicts,
 0 AS ChkList,   
  cast(0 as numeric(8,0)) AS Daytkoff,  
   cast(0 as numeric(8,0))AS DayLnding,   
   cast(0 as numeric(8,0))AS Nighttkoff,  
   cast(0 as numeric(8,0)) AS NightLnding,  
  cast( 0 as numeric(8,0)) AS Appr,  
   cast(0 as numeric(14,3)) AS Instr,  
   cast(0  as numeric(14,3)) AS [SevenDays],  
    
   cast(0  as numeric(14,3)) AS [ThirtyDays],  
   cast(0  as numeric(14,3))  AS CalMon,  
  
   cast(0  as numeric(14,3)) AS 
  [NinetyDays], 
 
  Air.IcaoID AS Home,  
    
  -- CC.IsNoConflictEvent,  
  CC.AlertDT AS Alert,  
  CC.GraceDT AS Grace,  
  @AircraftTypeID  AS AirCraftType,  
  @AircraftDescription AS AirCraftDesc,   
  CC.CrewChecklistDescription AS [Description],  
  CC.DueDT AS Due,   
  '' as Leg,
  '' as Purpose,      
  ((ISNULL(CRW.Addr1,'')) + (ISNULL(CRW.Addr2,'')))  as Street,  
  CRW.PostalZipCD as Postal,  
  CRW.CityName as City,  
  CRW.StateName as [State],  
  CRWPASS.PassportNum as Passport,  
  CRWPASS.PassportID as PassportID,  
  CRWPASSVISA.VisaID as VisaID,  
  CRWPASSVISA.VisaNum as Visa,  
  isnull(CRWPASS.PassportExpiryDT,'') as 'PassportExpiryDT',
  PassportCountry.CountryID as 'PassportCountryID',
  PassportCountry.CountryCD as 'Nation',
  
    
  --CASE WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0 AND ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND ISNULL(CRWRATE.IsSecondInCommand,0) = 0 THEN ''  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,S135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 THEN 'P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'P91,S91'      
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0  THEN 'P135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) =0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  THEN 'S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 0  THEN 'P91'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'S91'  
  --END  AS CrewRating,   
   case when CRWRATE.IsPilotinCommand =1 then 'P91,' else '' end + 
	+ case when CRWRATE.IsSecondInCommand =1 then 'S91,' else '' end 
	+ case when CRWRATE.IsEngineer =1 then 'E,' else '' end 
	+ case when CRWRATE.IsAttendant =1 then 'A,' else '' end 
	+ case when CRWRATE.IsInstructor =1 then 'I,' else '' end 
	+ case when CRWRATE.IsAttendant121 =1 then 'A121,' else '' end 
	+ case when CRWRATE.IsAttendant125 =1 then 'A125,' else '' end 
	+ case when CRWRATE.IsPIC121 =1 then 'P121,' else '' end 
	+ case when CRWRATE.IsPIC125 =1 then 'P125,' else '' end 
	+ case when CRWRATE.IsSIC121 =1 then 'S121,' else '' end 
	+ case when CRWRATE.IsSIC125 =1 then 'S125,' else '' end
	+ case when CRWRATE.IsQualInType135PIC =1 then 'P135,' else '' end
	+ case when CRWRATE.IsQualInType135SIC =1 then 'P135,' else '' end
   AS CrewRating,  
    
  CONVERT(BIGINT, 0) CrewGroupID,  
--  '' CrewGroupCD,  
  --(isnull(CRW.Notes2,''))  as AddlNotes    
  CONVERT(VARCHAR(MAX), CRW.Notes2) as AddlNotes,  
  Convert(varchar(max),CRW.Notes) as Notes,    
  CRW.IsStatus,
  '' as IsDiscontinue, 
  @Notified as IsNotified,   
  0 as OrderNUM    
 FROM Crew CRW  
  LEFT JOIN    
  ( SELECT   
    CRWDTYXREF.CrewID,  
    CRWDTYTP.IsStandby,  
    CRWDTYTP.IsDeleted,  
    CRWDTYTP.CustomerID   
   FROM CrewDutyTypeXRef CRWDTYXREF   
    INNER JOIN CrewDutyType CRWDTYTP  
    ON CRWDTYXREF.DutyTypeID = CRWDTYTP.DutyTypeID   
   WHERE CRWDTYTP.IsDeleted = 0  
  ) CDT ON CRW.CrewCD = @CrewCD AND CRW.CustomerID = @CustomerID
    
  LEFT JOIN CrewAircraftAssigned CAA   
   ON CAA.CrewID = CRW.CrewID AND CAA.FleetID = @FleetID AND CAA.IsDeleted = 0 AND CAA.CustomerID = CRW.CustomerID  
    
  LEFT JOIN     
   ( SELECT  CCD.CrewID,  
       CCD.IsNoConflictEvent,  
       CCD.AlertDT,  
       CCD.GraceDT,  
       CCD.AircraftID,  
       CCL.CrewChecklistDescription,  
       CCD.DueDT,   
       CCL.CustomerID,   
       CCL.ClientID   
  
    FROM CrewChecklistDetail CCD  
       INNER JOIN   CrewChecklist CCL   
       ON CCD.CheckListCD= CCL.CrewCheckCD    AND CCD.IsDeleted=0 AND CCL.IsDeleted=0
       
    WHERE   
    CCD.IsInActive = 0  
    AND (CCD.AlertDT <= @DepartDate   
      OR CCD.GraceDT <= @DepartDate   
      OR CCD.AlertDT <= @ArrivalDate   
      OR CCD.GraceDT <= @ArrivalDate  
      OR (CCD.IsOneTimeEvent = 0 AND CCD.IsCompleted = 0)  
      )  
    AND CCD.CustomerID = @CustomerID
    ) CC ON CC.CrewID = CRW.CrewID AND CC.CustomerID = CRW.CustomerID 
        
  --LEFT OUTER JOIN Company COM  
  --ON CRW.HomebaseID = COM.HomebaseID and CRW.CustomerID = COM.CustomerID AND COM.IsDeleted = 0   
  
  LEFT OUTER JOIN Airport Air  
  ON CRW.HomebaseID = Air.AirportID  AND Air.IsDeleted = 0        
    
  LEFT OUTER JOIN CrewPassengerPassport CRWPASS  
  ON CRW.CrewID = CRWPASS.CrewID and CRW.CustomerID = CRWPASS.CustomerID  AND CRWPASS.IsDeleted = 0  AND CRWPASS.Choice=1 
   AND CRWPASS.PassportID is not null  
    --AND CRWPASS.IsDefaultPassport = 1   
    
  LEFT OUTER JOIN CrewPassengerVisa CRWPASSVISA  
  ON CRW.CrewID = CRWPASSVISA.CrewID and CRW.CustomerID = CRWPASSVISA.CustomerID    
    AND CRWPASSVISA.IsDeleted = 0 AND CRWPASSVISA.VisaID is not null    
    AND CRWPASSVISA.CountryID =   @CountryID
    
  LEFT OUTER JOIN Country as PassportCountry on CRWPASS.CountryID = PassportCountry.CountryID
      
  LEFT OUTER JOIN CrewRating CRWRATE  
  ON CRW.CrewID = CRWRATE.CrewID and CRW.CustomerID = CRWRATE.CustomerID AND CRWRATE.IsDeleted = 0 AND CRWRATE.IsInActive=0 AND CRWRATE.AircraftTypeID = @AircraftTypeID    
    
 
    
  WHERE ISNULL(CRW.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0))  
  AND CRW.CustomerID = @CustomerID AND CRW.IsDeleted=0 and CRW.CrewID Not in (Select CrewID  from CrewRating where CustomerID=@CustomerID and IsDeleted = 0 and IsInActive=1 and AircraftTypeID=@AircraftTypeID)
  AND CRW.CrewCD = @CrewCD
  ORDER BY [Standby],TailAssigned,Actype,Conflicts,CheckList
END
ELSE
BEGIN
SELECT   
  CRW.CrewID,  
  CRW.CrewCD,  
  (RTRIM(ISNULL(CRW.LastName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.FirstName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.MiddleInitial,'')))   
  AS CrewName,   
  --ISNULL(cdt.IsStandby, 0) [Standby],  
  0 AS [Standby],  
  0 AS TailAssigned,  
  @AircraftTypeCD as Actype,
 0 AS Conflicts,
 0 AS ChkList,   
  cast(0 as numeric(8,0)) AS Daytkoff,  
   cast(0 as numeric(8,0))AS DayLnding,   
   cast(0 as numeric(8,0))AS Nighttkoff,  
   cast(0 as numeric(8,0)) AS NightLnding,  
  cast( 0 as numeric(8,0)) AS Appr,  
   cast(0 as numeric(14,3)) AS Instr,  
   cast(0  as numeric(14,3)) AS [SevenDays],  
    
   cast(0  as numeric(14,3)) AS [ThirtyDays],  
   cast(0  as numeric(14,3))  AS CalMon,  
  
   cast(0  as numeric(14,3)) AS 
  [NinetyDays],  
  Air.IcaoID AS Home,  
    
  -- CC.IsNoConflictEvent,  
  CC.AlertDT AS Alert,  
  CC.GraceDT AS Grace,  
  @AircraftTypeID  AS AirCraftType,  
  @AircraftDescription AS AirCraftDesc,   
  CC.CrewChecklistDescription AS [Description],  
  CC.DueDT AS Due,   
  '' as Leg,
  '' as Purpose,      
  ((ISNULL(CRW.Addr1,'')) + (ISNULL(CRW.Addr2,'')))  as Street,  
  CRW.PostalZipCD as Postal,  
  CRW.CityName as City,  
  CRW.StateName as [State],  
  CRWPASS.PassportNum as Passport,  
  CRWPASS.PassportID as PassportID,  
  CRWPASSVISA.VisaID as VisaID,  
  CRWPASSVISA.VisaNum as Visa,  
  isnull(CRWPASS.PassportExpiryDT,'') as 'PassportExpiryDT',
  PassportCountry.CountryID as 'PassportCountryID',
  PassportCountry.CountryCD as 'Nation',
  
    
  --CASE WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0 AND ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND ISNULL(CRWRATE.IsSecondInCommand,0) = 0 THEN ''  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,S135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 THEN 'P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'P91,S91'      
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0  THEN 'P135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) =0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  THEN 'S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 0  THEN 'P91'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'S91'  
  --END  AS CrewRating,   
   case when CRWRATE.IsPilotinCommand =1 then 'P91,' else '' end + 
	+ case when CRWRATE.IsSecondInCommand =1 then 'S91,' else '' end 
	+ case when CRWRATE.IsEngineer =1 then 'E,' else '' end 
	+ case when CRWRATE.IsAttendant =1 then 'A,' else '' end 
	+ case when CRWRATE.IsInstructor =1 then 'I,' else '' end 
	+ case when CRWRATE.IsAttendant121 =1 then 'A121,' else '' end 
	+ case when CRWRATE.IsAttendant125 =1 then 'A125,' else '' end 
	+ case when CRWRATE.IsPIC121 =1 then 'P121,' else '' end 
	+ case when CRWRATE.IsPIC125 =1 then 'P125,' else '' end 
	+ case when CRWRATE.IsSIC121 =1 then 'S121,' else '' end 
	+ case when CRWRATE.IsSIC125 =1 then 'S125,' else '' end
	+ case when CRWRATE.IsQualInType135PIC =1 then 'P135,' else '' end
	+ case when CRWRATE.IsQualInType135SIC =1 then 'P135,' else '' end
   AS CrewRating,  
    
  CONVERT(BIGINT, 0) CrewGroupID,  
--  '' CrewGroupCD,  
  --(isnull(CRW.Notes2,''))  as AddlNotes    
  CONVERT(VARCHAR(MAX), CRW.Notes2) as AddlNotes,  
  Convert(varchar(max),CRW.Notes) as Notes,    
  CRW.IsStatus,
  '' as IsDiscontinue, 
  @Notified as IsNotified,   
  0 as OrderNUM    
 FROM Crew CRW  
  LEFT JOIN    
  ( SELECT   
    CRWDTYXREF.CrewID,  
    CRWDTYTP.IsStandby,  
    CRWDTYTP.IsDeleted,  
    CRWDTYTP.CustomerID   
   FROM CrewDutyTypeXRef CRWDTYXREF   
    INNER JOIN CrewDutyType CRWDTYTP  
    ON CRWDTYXREF.DutyTypeID = CRWDTYTP.DutyTypeID   
   WHERE CRWDTYTP.IsDeleted = 0  
  ) CDT ON CRW.CrewID = @CrewID AND CRW.CustomerID = @CustomerID
    
  LEFT JOIN CrewAircraftAssigned CAA   
   ON CAA.CrewID = CRW.CrewID AND CAA.FleetID = @FleetID AND CAA.IsDeleted = 0 AND CAA.CustomerID = CRW.CustomerID  
    
  LEFT JOIN     
   ( SELECT  CCD.CrewID,  
       CCD.IsNoConflictEvent,  
       CCD.AlertDT,  
       CCD.GraceDT,  
       CCD.AircraftID,  
       CCL.CrewChecklistDescription,  
       CCD.DueDT,   
       CCL.CustomerID,   
       CCL.ClientID   
  
    FROM CrewChecklistDetail CCD  
       INNER JOIN   CrewChecklist CCL   
       ON CCD.CheckListCD= CCL.CrewCheckCD    AND CCD.IsDeleted=0 AND CCL.IsDeleted=0 AND CCD.CrewID = @CrewID
       
    WHERE   
    CCD.IsInActive = 0  
    AND (CCD.AlertDT <= @DepartDate   
      OR CCD.GraceDT <= @DepartDate   
      OR CCD.AlertDT <= @ArrivalDate   
      OR CCD.GraceDT <= @ArrivalDate  
      OR (CCD.IsOneTimeEvent = 0 AND CCD.IsCompleted = 0)  
      )  
    AND CCD.CustomerID = @CustomerID
    ) CC ON CC.CrewID = CRW.CrewID AND CC.CustomerID = CRW.CustomerID 
   --AND 
	--CC.ClientID = CRW.ClientID  
   --ISNULL(CC.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0)) 
      

      
  --LEFT OUTER JOIN Company COM  
  --ON CRW.HomebaseID = COM.HomebaseID and CRW.CustomerID = COM.CustomerID AND COM.IsDeleted = 0   
  
  LEFT OUTER JOIN Airport Air  
  ON CRW.HomebaseID = Air.AirportID  AND Air.IsDeleted = 0        
    
  LEFT OUTER JOIN CrewPassengerPassport CRWPASS  
  ON CRW.CrewID = CRWPASS.CrewID and CRW.CustomerID = CRWPASS.CustomerID  AND CRWPASS.IsDeleted = 0  AND CRWPASS.Choice=1 
   AND CRWPASS.PassportID is not null  
    --AND CRWPASS.IsDefaultPassport = 1   
    
  LEFT OUTER JOIN CrewPassengerVisa CRWPASSVISA  
  ON CRW.CrewID = CRWPASSVISA.CrewID and CRW.CustomerID = CRWPASSVISA.CustomerID    
    AND CRWPASSVISA.IsDeleted = 0 AND CRWPASSVISA.VisaID is not null    
    AND CRWPASSVISA.CountryID =   @CountryID
    
  LEFT OUTER JOIN Country as PassportCountry on CRWPASS.CountryID = PassportCountry.CountryID
      
  LEFT OUTER JOIN CrewRating CRWRATE  
  ON CRW.CrewID = CRWRATE.CrewID and CRW.CustomerID = CRWRATE.CustomerID AND CRWRATE.IsDeleted = 0 AND CRWRATE.IsInActive=0 AND CRWRATE.AircraftTypeID = @AircraftTypeID    
    

    
  WHERE ISNULL(CRW.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0))  
  AND CRW.CustomerID = @CustomerID AND CRW.IsDeleted=0 and CRW.CrewID Not in (Select CrewID  from CrewRating where CustomerID=@CustomerID and IsDeleted = 0 and IsInActive=1 and AircraftTypeID=@AircraftTypeID)
  AND CRW.CrewID =@CrewID
  ORDER BY [Standby],TailAssigned,Actype,Conflicts,CheckList
END  


GO


