

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddHelpItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddHelpItems]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
  
CREATE PROCEDURE [dbo].[spAddHelpItems]  
(@ItemID int  
,@Name varchar(100)  
,@ParentID int  
,@MimeType varchar(25)  
,@IsDirectory bit  
,@Size bigint  
,@Content varbinary(max))  
AS BEGIN  
  
 SET NOCOUNT ON;  
 
  
   INSERT INTO HelpItems (  
    [Name],   
    ParentId,   
    MimeType,   
    IsDirectory,   
    [Size],   
    Content)   
   VALUES (  
    @Name,   
    @ParentId,   
    @MimeType,   
    @IsDirectory,   
    @Size,   
    @Content)  
  
END  
GO

