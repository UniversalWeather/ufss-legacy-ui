/****** Object:  StoredProcedure [dbo].[spAddEmergencyContact]    Script Date: 09/13/2012 12:27:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddEmergencyContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddEmergencyContact]
GO
/****** Object:  StoredProcedure [dbo].[spAddEmergencyContact]    Script Date: 09/13/2012 12:27:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spAddEmergencyContact]  
(@EmergencyContactID bigint  
,@EmergencyContactCD char(5)  
,@CustomerID bigint  
,@LastName varchar(20)  
,@FirstName varchar(20)  
,@MiddleName varchar(20)  
,@Addr1 varchar(25)  
,@Addr2 varchar(25)  
,@CityName varchar(25)  
,@StateName varchar(10)  
,@PostalZipCD varchar(15)  
,@CountryID bigint  
,@PhoneNum varchar(25)  
,@FaxNum varchar(25)  
,@CellPhoneNum varchar(15)  
,@EmailAddress varchar(100)  
,@LastUpdUID varchar(30)  
,@LastUpdTS datetime  
,@IsDeleted bit
,@BusinessPhone	varchar(25)
,@BusinessFax	varchar(25)
,@CellPhoneNum2	varchar(25)
,@OtherPhone	varchar(25)
,@PersonalEmail	varchar(250)
,@OtherEmail	varchar(250)
,@Addr3	varchar(40)
,@IsInActive bit)
 
    
AS    
BEGIN     
SET NOCOUNT ON    

SET @LastUpdTS = GETUTCDATE()

 --DECLARE @EmergencyContactID BIGINT  
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @EmergencyContactID OUTPUT   
    
INSERT INTO EmergencyContact  
           ([EmergencyContactID]  
           ,[EmergencyContactCD]  
           ,[CustomerID]  
           ,[LastName]  
           ,[FirstName]  
           ,[MiddleName]  
           ,[Addr1]  
           ,[Addr2]  
           ,[CityName]  
           ,[StateName]  
           ,[PostalZipCD]  
           ,[CountryID]  
           ,[PhoneNum]  
           ,[FaxNum]  
           ,[CellPhoneNum]  
           ,[EmailAddress]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]
           ,[BusinessPhone]
		   ,[BusinessFax]
		   ,[CellPhoneNum2]
		   ,[OtherPhone]
		   ,[PersonalEmail]
		   ,[OtherEmail]
		   ,[Addr3]
		   ,[IsInActive])  
     VALUES  
           (@EmergencyContactID  
           ,@EmergencyContactCD  
           ,@CustomerID  
           ,@LastName  
           ,@FirstName  
           ,@MiddleName  
           ,@Addr1  
           ,@Addr2  
           ,@CityName  
           ,@StateName  
           ,@PostalZipCD  
           ,@CountryID  
           ,@PhoneNum  
           ,@FaxNum  
           ,@CellPhoneNum  
           ,@EmailAddress  
           ,@LastUpdUID  
           ,@LastUpdTS  
           ,@IsDeleted
		   ,@BusinessPhone	
		   ,@BusinessFax	
		   ,@CellPhoneNum2	
		   ,@OtherPhone	
		   ,@PersonalEmail	
		   ,@OtherEmail	
		   ,@Addr3  
		   ,@IsInActive   
           )    
END

GO


