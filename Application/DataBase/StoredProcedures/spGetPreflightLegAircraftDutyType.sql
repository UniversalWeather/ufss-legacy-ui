/****** Object:  StoredProcedure [dbo].[spGetPreflightLegAircraftDutyType]    Script Date: 10/23/2013 13:25:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPreflightLegAircraftDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPreflightLegAircraftDutyType]
GO



/****** Object:  StoredProcedure [dbo].[spGetPreflightLegAircraftDutyType]    Script Date: 10/09/2015 13:25:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetPreflightLegAircraftDutyType]
	-- Add the parameters for the stored procedure here
	 @CustomerId as BIGINT --Mandatory
	,@AircraftDuty as Char(2) --Mandatory
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Select statement
	SELECT count(LegId) as TotalCount from PreflightLeg where DutyType = @AircraftDuty and customerid=@CustomerId
	
End
GO