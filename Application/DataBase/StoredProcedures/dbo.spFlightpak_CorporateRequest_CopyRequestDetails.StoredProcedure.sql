/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_CopyRequestDetails]    Script Date: 04/25/2013 14:35:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_CopyRequestDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_CopyRequestDetails]
GO

CREATE PROCEDURE  [dbo].[spFlightpak_CorporateRequest_CopyRequestDetails]       
 -- Add the parameters for the stored procedure here      
 @DepartureDate datetime,      
 @CRMainID Bigint,      
 @CustomerID Bigint,      
 @IsLogisticsToBeCopied bit,   
 @IsPAXToBeCopied bit, 
 @IsLegsToBeCopied bit,
 @IsAllToBeCopied bit,     
 @CopyORMove bit,   
 @IsAutoDispatch bit,   
 @IsRevision bit,  
 @username varchar(30)  
   
AS      
BEGIN      
 DECLARE  @NewCRMainID BIGINT         
 DECLARE  @NewCRNUM BIGINT   
 DECLARE  @OldCRNUM BIGINT   
 DECLARE  @DateDiffValue INT    
       
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewCRMainID OUTPUT          
 set @NewCRNUM = 0      
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'RequestCurrentNo',  @NewCRNUM OUTPUT        
        
 select @NewCRNUM = RequestCurrentNo  from [FlightpakSequence] where customerID = @CustomerID      
       
 select @DateDiffValue = datediff (d, isnull(EstDepartureDT,GETDATE()),@DepartureDate), @OldCRNUM = CRTripNUM from CRMain where CRMainID = @CRMainID      
   
      
--Main      
 INSERT INTO [CRMain]  
           ([CRMainID]  
           ,[CustomerID]  
           ,[CRTripNUM]  
           ,[Approver]  
           ,[TripID]  
           ,[TripStatus]  
           ,[TripSheetStatus]  
           ,[AcknowledgementStatus]  
           ,[CharterQuoteStatus]  
           ,[CorporateRequestStatus]  
           ,[TravelCoordinatorID]  
           ,[TravelCoordinatorName]  
           ,[TravelCoordinatorPhone]  
           ,[TravelCoordinatorFax]  
           ,[DispatchNUM]  
           ,[EstDepartureDT]  
           ,[EstArrivalDT]  
           ,[RecordType]  
           ,[FleetID]  
           ,[AircraftID]  
           ,[PassengerRequestorID]  
           ,[RequestorName]  
           ,[RequestorPhone]  
           ,[DepartmentID]  
           ,[DepartmentDescription]  
           ,[AuthorizationID]  
           ,[AuthorizationDescription]  
           ,[HomebaseID]  
           ,[ClientID]  
           ,[IsPrivate]  
           ,[CRMainDescription]  
           ,[RequestDT]  
           ,[BeginningGMTDT]  
           ,[EndingGMTDT]  
           ,[IsCrew]  
           ,[IsPassenger]  
           ,[IsAlert]  
           ,[IsAirportAlert]  
           ,[IsLog]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])  
             
  select    @NewCRMainID   
           ,CustomerID   
           ,@NewCRNUM   
           ,[Approver]  
           ,NULL 
           ,[TripStatus]  
           ,[TripSheetStatus]  
           ,[AcknowledgementStatus]  
           ,[CharterQuoteStatus]  
           ,'W'  
           ,[TravelCoordinatorID]  
           ,[TravelCoordinatorName]  
           ,[TravelCoordinatorPhone]  
           ,[TravelCoordinatorFax]  
           ,@NewCRNUM
           ,@DepartureDate  
           ,[EstArrivalDT]  
           ,[RecordType]  
           ,[FleetID]  
           ,[AircraftID]  
           ,[PassengerRequestorID]  
           ,[RequestorName]  
           ,[RequestorPhone]  
           ,[DepartmentID]  
           ,[DepartmentDescription]  
           ,[AuthorizationID]  
           ,[AuthorizationDescription]  
           ,[HomebaseID]  
           ,[ClientID]  
           ,[IsPrivate]  
           ,[CRMainDescription]  
           ,[RequestDT]  
           ,[BeginningGMTDT]  
           ,[EndingGMTDT]  
           ,[IsCrew]  
           ,[IsPassenger]  
           ,[IsAlert]  
           ,[IsAirportAlert]  
           ,[IsLog]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]  
         
   from CRMain where CRMainID = @CRMainID      
       
 if (@CopyORMove=1)      
 begin      
  CREATE Table #TempLeg (      
  rownum int identity (1,1),      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempLeg (NewLegId,OldLegID ) select null, CRLegID from CRLeg where CRMainID = @CRMainID       
        
  Declare @Rownumber int      
  DECLARE  @NewLegID BIGINT          
  set @Rownumber = 1      
        
  while (select COUNT(1) from #TempLeg where NewLegId is null) >0      
  begin      
   Set @NewLegID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewLegID OUTPUT       
   update #TempLeg set NewLegId = @NewLegID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
       
       
--Legs      
 INSERT INTO [CRLeg]  
           ([CRLegID]  
           ,[CustomerID]  
           ,[CRMainID]  
           ,[TripNUM]  
           ,[LegNUM]  
           ,[DAirportID]  
           ,[AAirportID]  
           ,[DepartureCity]  
           ,[ArrivalCity]  
           ,[DepartureDTTMLocal]  
           ,[ArrivalDTTMLocal]  
           ,[PassengerRequestorID]  
           ,[DepartmentID]  
           ,[AuthorizationID]  
           ,[FlightCategoryID]  
           ,[IsPrivate]  
           ,[Distance]  
           ,[ElapseTM]  
           ,[PassengerTotal]  
           ,[PowerSetting]  
           ,[TakeoffBIAS]  
           ,[LandingBIAS]  
           ,[TrueAirSpeed]  
           ,[WindsBoeingTable]  
           ,[IsDepartureConfirmed]  
           ,[IsArrivalConfirmation]  
           ,[IsApproxTM]  
           ,[IsScheduledServices]  
           ,[DepartureGreenwichDTTM]  
           ,[ArrivalGreenwichDTTM]  
           ,[HomeDepartureDTTM]  
           ,[HomeArrivalDTTM]  
           ,[NegotiateTime]  
           ,[LogBreak]  
           ,[FlightPurpose]  
           ,[SeatTotal]  
           ,[ReservationTotal]  
           ,[ReservationAvailable]  
           ,[WaitNUM]  
           ,[Duty_Type]  
           ,[DutyHours]  
           ,[RestHours]  
           ,[FlightHours]  
           ,[Notes]  
           ,[FuelLoad]  
           ,[OutboundInstruction]  
           ,[DutyType]  
           ,[IsCrewDiscount]  
           ,[CrewCurrency]  
           ,[IsDutyEnd]  
           ,[FedAviationRegNUM]  
           ,[LegID]  
           ,[WindReliability]  
           ,[CROverRide]  
           ,[CheckGroup]  
           ,[AirportAlertCD]  
           ,[AdditionalCrew]  
           ,[PilotInCommand]  
           ,[SecondInCommand]  
           ,[NextLocalDTTM]  
           ,[NextGMTDTTM]  
           ,[NextHomeDTTM]  
           ,[CrewNotes]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]  
           ,[FBOID]  
           ,[ClientID]  
           )   
    select  T.NewLegId      
     ,[CustomerID]        
     ,@NewCRMainID  
     ,[TripNUM]  
           ,[LegNUM]  
           ,[DAirportID]  
           ,[AAirportID]  
           ,[DepartureCity]  
           ,[ArrivalCity]  
           ,case when [DepartureDTTMLocal] IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[DepartureDTTMLocal])END 
           ,case when [ArrivalDTTMLocal]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[ArrivalDTTMLocal])END    
           ,[PassengerRequestorID]  
           ,[DepartmentID]  
           ,[AuthorizationID]  
           ,[FlightCategoryID]  
           ,[IsPrivate]  
           ,[Distance]  
           ,[ElapseTM]  
           ,case when @IsPAXToBeCopied=1 THEN [PassengerTotal] ELSE 0 END  
           ,[PowerSetting]  
           ,[TakeoffBIAS]  
           ,[LandingBIAS]  
           ,[TrueAirSpeed]  
           ,[WindsBoeingTable]  
           ,[IsDepartureConfirmed]  
           ,[IsArrivalConfirmation]  
           ,[IsApproxTM]  
           ,[IsScheduledServices]  
           ,[DepartureGreenwichDTTM]  
           ,[ArrivalGreenwichDTTM]  
           ,[HomeDepartureDTTM]  
           ,[HomeArrivalDTTM]  
           ,[NegotiateTime]  
           ,[LogBreak]  
           ,[FlightPurpose]  
           ,case when @IsPAXToBeCopied=1 THEN [SeatTotal]  ELSE 0 END  
           ,case when @IsPAXToBeCopied=1 THEN [ReservationTotal] ELSE 0 END  
           ,case when @IsPAXToBeCopied=1 THEN [ReservationAvailable] ELSE 0 END   
           ,[WaitNUM]  
           ,[Duty_Type]  
           ,[DutyHours]  
           ,[RestHours]  
           ,[FlightHours]  
           ,[Notes]  
           ,[FuelLoad]  
           ,[OutboundInstruction]  
           ,[DutyType]  
           ,[IsCrewDiscount]  
           ,[CrewCurrency]  
           ,[IsDutyEnd]  
           ,[FedAviationRegNUM]  
           ,[LegID]  
           ,[WindReliability]  
           ,[CROverRide]  
           ,[CheckGroup]  
           ,[AirportAlertCD]  
           ,[AdditionalCrew]  
           ,[PilotInCommand]  
           ,[SecondInCommand]  
           ,[NextLocalDTTM]  
           ,[NextGMTDTTM]  
           ,[NextHomeDTTM]  
           ,[CrewNotes]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]  
           ,[FBOID]  
           ,[ClientID]  
    from crleg PL, #TempLeg T      
    where PL.CRLegID = T.OldLegID      
 End      
  
    
--Pax      
 if (@IsPAXToBeCopied = 1 or @IsAllToBeCopied=1)      
 begin      
       
  CREATE Table #TempPax (      
  rownum int identity (1,1),      
  NewPreflightPassengerListID Bigint,      
  oldPreflightPassengerListID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
  insert into #TempPax (NewPreflightPassengerListID ,      
   oldPreflightPassengerListID ,      
   NewLegId ,      
   OldLegID )     
      
  select null,  [CRPassengerID], NewLegId, OldLegID      
  from CRPassenger PP, #TempLeg TL      
  where PP.CRLegID = TL.OldLegID      
    
  DECLARE  @NewPreflightPassengerListID BIGINT         
  set @Rownumber = 1     
     
  while (select COUNT(1) from #TempPax where NewPreflightPassengerListID is null) >0      
  begin      
   Set @NewPreflightPassengerListID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewPreflightPassengerListID OUTPUT       
   update #TempPax set NewPreflightPassengerListID = @NewPreflightPassengerListID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
    
  INSERT INTO [CRPassenger]  
           ([CRPassengerID]  
           ,[CustomerID]  
           ,[CRLegID]  
           ,[TripNUM]  
           ,[LegID]  
           ,[OrderNUM]  
           ,[PassengerRequestorID]  
           ,[PassengerName]  
           ,[FlightPurposeID]  
           ,[PassportID]  
           ,[Billing]  
           ,[IsNonPassenger]  
           ,[IsBlocked]  
           ,[ReservationNUM]  
           ,[WaitList]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])  
     Select      
           
           TP.NewPreflightPassengerListID    
           ,[CustomerID]    
           ,TP.NewLegId      
           ,[TripNUM]  
           ,[LegID]  
           ,[OrderNUM]  
           ,[PassengerRequestorID]  
           ,[PassengerName]  
           ,[FlightPurposeID]  
           ,[PassportID]  
           ,[Billing]  
           ,[IsNonPassenger]  
           ,[IsBlocked]  
           ,[ReservationNUM]  
           ,[WaitList]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]     
                 
           from [CRPassenger] PP, #TempPax TP      
     where PP.CRPassengerID = TP.oldPreflightPassengerListID   
        
  ---Transport      
    --Transport      
  CREATE Table #TempPAXTransportList (      
  rownum int identity (1,1),      
  NewPreflightTransportID Bigint,      
  oldPreflightTransportID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempPAXTransportList (NewPreflightTransportID ,      
   oldPreflightTransportID ,      
   NewLegId ,      
   OldLegID )      
  select null,  [CRTransportListID], NewLegId, OldLegID      
  from [CRTransportList] PC, #TempLeg TL      
  where PC.CRLegID = TL.OldLegID and PC.RecordType ='P'      
        
  DECLARE  @NewPreflightTransportID BIGINT         
        
  set @Rownumber = 1      
        
  while (select COUNT(1) from #TempPAXTransportList where NewPreflightTransportID is null) >0      
  begin      
   --Set @NewPreflightCrewListID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewPreflightTransportID OUTPUT       
   update #TempPAXTransportList set NewPreflightTransportID = @NewPreflightTransportID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [CRTransportList]  
           ([CRTransportListID]  
           ,[CustomerID]  
           ,[CRLegID]  
           ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[TransportID]  
           ,[CRTransportListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])  
           
  select         
                
   TC.NewPreflightTransportID      
           ,[CustomerID]  
           ,TC.NewLegId      
           ,[TripNUM]  
           ,[LegID]  
           ,'P' 
           ,[TransportID]  
           ,[CRTransportListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]    
              
  from [CRTransportList] PC, #TempPAXTransportList TC      
  where PC.CRLegID = TC.OldLegID and PC.RecordType = 'P'       
  and PC.CRTransportListID = TC.oldPreflightTransportID      
        
  Drop Table #TempPAXTransportList      
  --Transport      
        
  ---Transport      
        
  Drop table #TempPax      
 End      
       
       
--Logistics      
 if (@IsLogisticsToBeCopied = 1 or @IsAllToBeCopied=1)      
 begin      
  --Fbo      
  CREATE Table #TempFBO (      
  rownum int identity (1,1),      
  NewPreflightFBOID Bigint,      
  oldPreflightFBOID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempFBO (NewPreflightFBOID ,      
   oldPreflightFBOID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CRFBOListID, NewLegId, OldLegID      
  from CRFBOList PF, #TempLeg TL      
  where PF.CRLegID = TL.OldLegID      
        
  DECLARE  @NewPreflightFBOID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempFBO where NewPreflightFBOID is null) >0      
  begin      
   Set @NewPreflightFBOID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewPreflightFBOID OUTPUT       
   update #TempFBO set NewPreflightFBOID = @NewPreflightFBOID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [CRFBOList]  
           ([CRFBOListID]  
           ,[CustomerID]  
           ,[CRLegID]  
           ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[FBOID]  
           ,[CRFBOListDescription]  
           ,[PhoneNUM]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])  
     Select      
           TF.NewPreflightFBOID      
           ,[CustomerID]  
           ,TF.NewLegId      
            ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[FBOID]  
           ,[CRFBOListDescription]  
           ,[PhoneNUM]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]     
                 
           from [CRFBOList] PF, #TempFBO TF      
     where PF.CRFBOListID = TF.oldPreflightFBOID      
           
  Drop table #TempFBO      
        
  --Catering        
  CREATE Table #TempCatering (      
  rownum int identity (1,1),      
  NewPreflightCateringID Bigint,      
  oldPreflightCateringID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempCatering (NewPreflightCateringID ,      
   oldPreflightCateringID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CRCateringListID, NewLegId, OldLegID      
  from CRCateringList PC, #TempLeg TL      
  where PC.CRLegID = TL.OldLegID      
        
  DECLARE  @NewPreflightCateringID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempCatering where NewPreflightCateringID is null) >0      
  begin      
   Set @NewPreflightCateringID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewPreflightCateringID OUTPUT       
   update #TempCatering set NewPreflightCateringID = @NewPreflightCateringID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [CRCateringList]  
           ([CRCateringListID]  
           ,[CustomerID]  
           ,[CRLegID]  
           ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[CateringID]  
           ,[CRCateringListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])       
     Select      
                 
    TC.NewPreflightCateringID  
           ,[CustomerID]      
           ,TC.NewLegId      
           ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[CateringID]  
           ,[CRCateringListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]   
                 
           from [CRCateringList] PC, #TempCatering TC      
     where PC.CRCateringListID = TC.oldPreflightCateringID      
        
  Drop table #TempCatering    
  
  --Crewhotel
  CREATE Table #TempCrewHotel (      
  rownum int identity (1,1),      
  NewCrewHotelID Bigint,      
  oldCrewHotelID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  ) 
  
  insert into #TempCrewHotel (NewCrewHotelID ,      
   oldCrewHotelID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CRHotelListID, NewLegId, OldLegID      
  from CRHotelList PH, #TempLeg TL      
  where PH.CRLegID = TL.OldLegID  and    PH.RecordType='C' 
  
  DECLARE  @NewCrewHotelID BIGINT         
  
  set @Rownumber = 1      
  
  while (select COUNT(1) from #TempCrewHotel where NewCrewHotelID is null) >0      
  begin      
   Set @NewCrewHotelID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewCrewHotelID OUTPUT       
   update #TempCrewHotel set NewCrewHotelID = @NewCrewHotelID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
  
  INSERT INTO CRHotelList
		([CRHotelListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[HotelID]
		,[CRHotelListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted])
	  Select      
                 
            TCH.NewCrewHotelID  
           ,[CustomerID]      
           ,TCH.NewLegId      
           ,[TripNUM]
		   ,[LegID]
		   ,'C'
		   ,[HotelID]
		   ,[CRHotelListDescription]
		   ,[PhoneNUM]
		   ,[IsCompleted]
			,[AirportID]
			,[LastUpdUID]
			,[LastUpdTS]
			,[IsDeleted]   
                 
           from [CRHotelList] PCH, #TempCrewHotel TCH      
     where PCH.CRHotelListID = TCH.oldCrewHotelID  and PCH.RecordType = 'C'   
  
  Drop table #TempCrewHotel    
  
  
  ---Pax hotel
   CREATE Table #TempPaxHotel (      
  rownum int identity (1,1),      
  NewPaxHotelID Bigint,      
  oldPaxHotelID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  ) 
  
  insert into #TempPaxHotel (NewPaxHotelID ,      
   oldPaxHotelID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CRHotelListID, NewLegId, OldLegID      
  from CRHotelList PH, #TempLeg TL      
  where PH.CRLegID = TL.OldLegID  and    PH.RecordType='P' 
  
  DECLARE  @NewPaxHotelID BIGINT         
  
  set @Rownumber = 1      
  
  while (select COUNT(1) from #TempPaxHotel where NewPaxHotelID is null) >0      
  begin      
   Set @NewPaxHotelID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewPaxHotelID OUTPUT       
   update #TempPaxHotel set NewPaxHotelID = @NewPaxHotelID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
  
  INSERT INTO CRHotelList
		([CRHotelListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[HotelID]
		,[CRHotelListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted])
	  Select      
                 
            TCH.NewPaxHotelID  
           ,[CustomerID]      
           ,TCH.NewLegId      
           ,[TripNUM]
		   ,[LegID]
		   ,'P'
		   ,[HotelID]
		   ,[CRHotelListDescription]
		   ,[PhoneNUM]
		   ,[IsCompleted]
			,[AirportID]
			,[LastUpdUID]
			,[LastUpdTS]
			,[IsDeleted]   
                 
           from [CRHotelList] PCH, #TempPaxHotel TCH      
     where PCH.CRHotelListID = TCH.oldPaxHotelID and PCH.RecordType = 'P' 
  
  Drop table #TempPaxHotel 
  
  
  ---Maint Hotel
   CREATE Table #TempMaintHotel (      
  rownum int identity (1,1),      
  NewMaintHotelID Bigint,      
  oldMaintHotelID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  ) 
 
 
   insert into #TempMaintHotel (NewMaintHotelID ,      
   oldMaintHotelID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CRHotelListID, NewLegId, OldLegID      
  from CRHotelList PH, #TempLeg TL      
  where PH.CRLegID = TL.OldLegID  and    PH.RecordType='A' 
  
  DECLARE  @NewMaintHotelID BIGINT         
  
  set @Rownumber = 1       
     
   while (select COUNT(1) from #TempMaintHotel where NewMaintHotelID is null) >0      
  begin      
   Set @NewMaintHotelID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewMaintHotelID OUTPUT       
   update #TempMaintHotel set NewMaintHotelID = @NewMaintHotelID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
  
  INSERT INTO CRHotelList
		([CRHotelListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[HotelID]
		,[CRHotelListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted])
	  Select      
                 
            TCH.NewMaintHotelID  
           ,[CustomerID]      
           ,TCH.NewLegId      
           ,[TripNUM]
		   ,[LegID]
		   ,'A'
		   ,[HotelID]
		   ,[CRHotelListDescription]
		   ,[PhoneNUM]
		   ,[IsCompleted]
			,[AirportID]
			,[LastUpdUID]
			,[LastUpdTS]
			,[IsDeleted]   
                 
           from [CRHotelList] PCH, #TempMaintHotel TCH      
     where PCH.CRHotelListID = TCH.oldMaintHotelID  and PCH.RecordType = 'A' 
  
  Drop table #TempMaintHotel    
  
  
  --Crew Transport
  CREATE Table #TempCrewTransportList (      
  rownum int identity (1,1),      
  NewCrewTransportID Bigint,      
  oldCrewTransportID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempCrewTransportList (NewCrewTransportID ,      
   oldCrewTransportID ,      
   NewLegId ,      
   OldLegID )      
  select null,  [CRTransportListID], NewLegId, OldLegID      
  from [CRTransportList] PC, #TempLeg TL      
  where PC.CRLegID = TL.OldLegID and PC.RecordType ='C'      
        
  DECLARE  @NewCrewTransportID BIGINT         
        
  set @Rownumber = 1      
        
  while (select COUNT(1) from #TempCrewTransportList where NewCrewTransportID is null) >0      
  begin 
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewCrewTransportID OUTPUT       
   update #TempCrewTransportList set NewCrewTransportID = @NewCrewTransportID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [CRTransportList]  
           ([CRTransportListID]  
           ,[CustomerID]  
           ,[CRLegID]  
           ,[TripNUM]  
           ,[LegID]  
           ,[RecordType]  
           ,[TransportID]  
           ,[CRTransportListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted])  
           
  select         
                
   TC.NewCrewTransportID      
           ,[CustomerID]  
           ,TC.NewLegId      
           ,[TripNUM]  
           ,[LegID]  
           ,'C'
           ,[TransportID]  
           ,[CRTransportListDescription]  
           ,[PhoneNUM]  
           ,[Rate]  
           ,[IsCompleted]  
           ,[AirportID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]    
              
  from [CRTransportList] PC, #TempCrewTransportList TC      
  where PC.CRLegID = TC.OldLegID and PC.RecordType = 'C'       
  and PC.CRTransportListID = TC.oldCrewTransportID      
        
  Drop Table #TempCrewTransportList      
    
 End      
       
     
--History
 CREATE Table #TempTripHistory (    
  rownum int identity (1,1),    
  NewTripHistoryID Bigint,    
  OldTripHistoryID Bigint    
  )    
      
  insert into #TempTripHistory (NewTripHistoryID,OldTripHistoryID ) select null, CRHistoryID from CRHistory where CRMainID = @CRMainID    
      
  DECLARE  @NewTripHistoryID BIGINT        
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempTripHistory where NewTripHistoryID is null) >0    
  begin    
   Set @NewTripHistoryID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewTripHistoryID OUTPUT     
   update #TempTripHistory set NewTripHistoryID = @NewTripHistoryID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
  
  Set @NewTripHistoryID =0    
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewTripHistoryID OUTPUT    
     
  
  INSERT INTO [CRHistory]    
            (
    [CRHistoryID] ,
    [CustomerID],
	[CRMainID],	
	[LogisticsHistory],
	[LastUpdUID],
	[LastUpdTS],
	[IsDeleted],
	[RevisionNumber]
            )    
       
    values   (@NewTripHistoryID 
			 ,@CustomerID       
             ,@NewCRMainID               
             ,'Trip Request Details Copied From Trip No.: ' + CONVERT (varchar(30),@OldCRNUM)
             ,@username    
             ,GETUTCDATE() 
             ,0   
             ,1
           )  
  
  
  INSERT INTO [CRHistory]    
            (
	    [CRHistoryID] ,
		[CustomerID],
		[CRMainID],	
		[LogisticsHistory],
		[LastUpdUID],
		[LastUpdTS],
		[IsDeleted],
		[RevisionNumber]
            )    
       
    select  T.NewTripHistoryID 
			  ,[CustomerID]      
             ,@NewCRMainID 
             ,[LogisticsHistory]    
             ,[LastUpdUID]    
             ,[LastUpdTS]   
             ,[IsDeleted] 
             ,[RevisionNumber]
             
    from CRHistory PL, #TempTripHistory  T    
    where PL.CRHistoryID = T.OldTripHistoryID    
    
  Drop table #TempTripHistory 



--History     
     
       
 if (@CopyORMove=1)      
 begin      
  Drop table #TempLeg      
 end      
 select @NewCRMainID as CRMainID          
     
END
GO