

/****** Object:  StoredProcedure [dbo].[spUpdateCrewCheckListforYES]    Script Date: 10/26/2012 13:16:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewCheckListforYES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewCheckListforYES]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateCrewCheckListforYES]    Script Date: 10/26/2012 13:16:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--select distinct crewid from crew where customerid = 10001 -- 203

--select distinct crewid from crewchecklistdetail where customerid = 10001 -- 203

--select * from crewchecklistdetail where customerid = 10001 -- 2796 + 203 = 2972

--SELECT DISTINCT CREWID FROM CREW WHERE crewid  not in (select crewid from crewchecklistdetail where CheckListCD = @CrewCheckCD and CustomerID=@CustomerID and isdeleted = 0)

--spUpdateCrewCheckListforYES 10001,'RAMESH','RAMESH',NULL,'Fernando_1',NULL,0,0,0


CREATE procedure [dbo].[spUpdateCrewCheckListforYES]  
(           @CrewCheckID BIGINT
           ,@CustomerID BIGINT  
           ,@CrewCheckCD char(3)  
           ,@CrewChecklistDescription varchar(25)  
           ,@ClientID BIGINT  
           ,@LastUpdUID varchar(30)  
           ,@LastUpdTS datetime  
           ,@IsScheduleCheck bit  
           ,@IsCrewCurrencyPlanner bit  
           ,@IsDeleted bit  
           ,@IsInActive bit
)  
-- =============================================  
-- Author: SelvaKumar  
-- Create date: 11/5/2012  
-- Modified by Mathes on 05-07-12
-- Description: Update the CrewCheckList  
-- =============================================  
  
As  
BEGIN
SET NoCOUNT ON  
  set @LastUpdTS=GETUTCDATE()
 UPDATE  CrewCheckList  
 SET        [CustomerID] =@CustomerID 
           ,[CrewChecklistDescription]=@CrewChecklistDescription  
           ,[ClientID] =@ClientID   
           ,[LastUpdUID]=@LastUpdUID  
           ,[LastUpdTS]=@LastUpdTS  
           ,[IsScheduleCheck]=@IsScheduleCheck   
           ,[IsCrewCurrencyPlanner]=@IsCrewCurrencyPlanner  
           ,[IsDeleted]=@IsDeleted    
           ,[IsInActive]=@IsInActive    
    WHERE   CrewCheckID=@CrewCheckID 
    
    update crewchecklistdetail set lastupdts = @LastUpdTS,LastUpdUID = @LastUpdUID,IsInActive=@IsInActive where CheckListCD = @CrewCheckCD and CustomerID=@CustomerID and isdeleted =0

 -- Cursot to get the values of crew which are not having the specified crewinfoid  
   DECLARE  @CrewID BIGINT  
    DECLARE curTailInfo CURSOR FOR SELECT DISTINCT CREWID FROM CREW WHERE crewid  not in (select crewid from crewchecklistdetail where CheckListCD = @CrewCheckCD and CustomerID=@CustomerID and isdeleted = 0) AND CustomerID = @CustomerID
        
 OPEN curTailInfo; 
            
 -- PRINT @@CURSOR_ROWS            
  FETCH NEXT FROM curTailInfo INTO @CREWID;   
  WHILE @@FETCH_STATUS = 0            
  BEGIN  
  BEGIN          
  --INSERT INTO()
 EXEC spFlightPak_AddCrewCheckListDate NULL,@CrewCheckCD,NULL,@CustomerID,@CrewID,NULL,NULL,NULL,0,0,0,NULL,0,@LastUpdUID,@LastUpdTS,0,0,0,0,0,NULL,0,0,0,0,0,0,0,NULL,NULL,0,0,NULL,0,0,0,0
 
  END  
   FETCH NEXT FROM curTailInfo INTO  @CREWID
              
 END             
 CLOSE curTailInfo;            
 DEALLOCATE curTailInfo; 
  
END



GO


