

/****** Object:  StoredProcedure [dbo].[spGetAllSalesPerson]    Script Date: 04/08/2013 19:40:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllSalesPerson]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllSalesPerson]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllSalesPerson]    Script Date: 04/08/2013 19:40:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author: Ramesh. J    
-- Create date: 25/02/2013      
-- Description: Get all Sales Person details    
-- =============================================  
CREATE Procedure [dbo].[spGetAllSalesPerson]          
(          
 @CustomerID  BIGINT     
)          
AS          
BEGIN          
SET NOCOUNT ON;          
       
SELECT         
       [SalesPersonID]    
      ,SalesPerson.[CustomerID]    
      ,SalesPerson.[SalesPersonCD]    
      ,SalesPerson.[FirstName]    
      ,SalesPerson.[LastName]    
      ,SalesPerson.[MiddleName]   
      ,isnull(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleName,'') as Name  
      ,SalesPerson.[PhoneNUM]    
      ,SalesPerson.[Address1]    
      ,SalesPerson.[Address2]    
      ,SalesPerson.[Address3]    
      ,SalesPerson.[CityName]    
      ,SalesPerson.[StateName]    
      ,SalesPerson.[PostalZipCD]    
      ,SalesPerson.[CountryID]    
      ,SalesPerson.[FaxNUM]    
      ,SalesPerson.[PersonalFaxNum]    
      ,SalesPerson.[CellPhoneNUM]    
      ,SalesPerson.[EmailAddress]    
      ,SalesPerson.[PersonalEmailID]    
      ,SalesPerson.[Logo]    
      ,SalesPerson.[LogoPosition]    
      ,SalesPerson.[Notes]    
      ,SalesPerson.[IsInActive]    
      ,SalesPerson.[LastUpdUID]    
      ,SalesPerson.[LastUpdTS]    
      ,SalesPerson.[IsDeleted]    
      ,Nationality.CountryCD as NationalityCD 
      ,Airport.IcaoID
      ,SalesPerson.HomeBaseID  
 FROM [SalesPerson]        
    LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = SalesPerson.CountryID   
    LEFT OUTER JOIN Company as Company ON SalesPerson.HomeBaseID = Company.HomebaseID
    LEFT OUTER JOIN Airport as Airport ON Airport.AirportID = Company.HomebaseAirportID  
 WHERE SalesPerson.CustomerID  = @CustomerID          
 AND ISNULL(SalesPerson.IsDeleted,0) = 0        
 order by SalesPersonCD     
 END 
GO


