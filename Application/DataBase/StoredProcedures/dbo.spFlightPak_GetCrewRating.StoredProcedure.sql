/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewRating]    Script Date: 08/07/2014 17:53:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetCrewRating]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetCrewRating]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewRating]    Script Date: 08/07/2014 17:53:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_GetCrewRating]      
(      
 @CustomerID BIGINT,    
 @CrewID BIGINT,    
 @ClientID BIGINT    
)      
AS      
BEGIN    
  SET NOCOUNT ON;     
  IF(@ClientID>0)    
  BEGIN     
	SELECT       
       CrewRating.CrewRatingID    
      ,CrewRating.[CrewID]    
	  ,CrewRating.[CustomerID]    
	  ,CrewRating.[AircraftTypeID]    
	  ,CrewRating.[CrewRatingDescription]    
	  ,CrewRating.[IsPilotinCommand]    
	  ,CrewRating.[IsSecondInCommand]    
	  ,CrewRating.[IsEngineer]    
	  ,CrewRating.[IsInstructor]    
	  ,CrewRating.[IsAttendant]    
	  ,CrewRating.[AsOfDT]    
	  ,CrewRating.[TimeInType]    
	  ,CrewRating.[Day1]    
	  ,CrewRating.[Night1]    
	  ,CrewRating.[Instrument]    
	  ,CrewRating.[Simulator]    
	  ,CrewRating.[PilotinCommand]    
	  ,CrewRating.[SecondinCommand]    
	  ,CrewRating.[FlightEngineer]    
	  ,CrewRating.[FlightInstructor]    
	  ,CrewRating.[FlightAttendant]    
	  ,CrewRating.[ClientID]    
	  ,CrewRating.[LastUpdUID]    
	  ,CrewRating.[LastUpdTS]    
	  ,CrewRating.[IsQualInType135PIC]    
	  ,CrewRating.[IsQualInType135SIC]    
	  ,CrewRating.[PilotinCommandDayHrs]    
	  ,CrewRating.[PilotInCommandNightHrs]    
	  ,CrewRating.[PilotInCommandINSTHrs]    
	  ,CrewRating.[SecondInCommandDay]    
	  ,CrewRating.[SecondInCommandNight]    
	  ,CrewRating.[SecondInCommandInstr]    
	  ,CrewRating.[TotalTimeInTypeHrs]    
	  ,CrewRating.[TotalDayHrs]    
	  ,CrewRating.[TotalNightHrs]    
	  ,CrewRating.[TotalINSTHrs]    
	  ,CrewRating.[PilotInCommandTypeHrs]    
	  ,CrewRating.[TPilotinCommandDayHrs]    
	  ,CrewRating.[TPilotInCommandNightHrs]    
	  ,CrewRating.[TPilotInCommandINSTHrs]    
	  ,CrewRating.[SecondInCommandTypeHrs]    
	  ,CrewRating.[SecondInCommandDayHrs]    
	  ,CrewRating.[SecondInCommandNightHrs]    
	  ,CrewRating.[SecondInCommandINSTHrs]    
	  ,CrewRating.[OtherSimHrs]    
	  ,CrewRating.[OtherFlightEngHrs]    
	  ,CrewRating.[OtherFlightInstrutorHrs]    
	  ,CrewRating.[OtherFlightAttdHrs]    
	  ,CrewRating.[TotalUpdateAsOfDT]    
	  ,CrewRating.[IsInActive]    
	  ,CrewRating.[IsCheckAirman]    
	  ,CrewRating.[IsCheckAttendant]    
	  ,CrewRating.[IsAttendantFAR91]    
	  ,CrewRating.[IsAttendantFAR135]    
	  ,CrewRating.[IsDeleted]  
	  ,ISNULL(CrewRating.[IsPIC121],0) as IsPIC121  
	  ,ISNULL(CrewRating.[IsPIC125],0) as IsPIC125  
	  ,ISNULL(CrewRating.[IsSIC121],0) as IsSIC121  
	  ,ISNULL(CrewRating.[IsSIC125],0) as IsSIC125  
	  ,ISNULL(CrewRating.[IsAttendant121],0) as IsAttendant121  
	  ,ISNULL(CrewRating.[IsAttendant125],0) as IsAttendant125    
	  ,ISNULL(CrewRating.[IsPIC91],0) as IsPIC91  
	  ,ISNULL(CrewRating.[IsSIC91],0) as IsSIC91  
	  ,AircraftType.AircraftCD as AircraftTypeCD    
	  ,Client.ClientCD as ClientCD    
	FROM CrewRating as CrewRating      
		LEFT OUTER JOIN Aircraft as AircraftType on AircraftType.AircraftID = CrewRating.AircraftTypeID    
		LEFT OUTER JOIN Client as Client on Client.ClientID = CrewRating.ClientID      
	WHERE CrewRating.CustomerID = @CustomerID AND CrewRating.CrewID = @CrewID AND CrewRating.ClientID=@ClientID  AND CrewRating.isdeleted = 0 
	ORDER BY AircraftTypeCD
  END     
  ELSE    
  BEGIN    
	SELECT       
       CrewRating.CrewRatingID    
      ,CrewRating.[CrewID]    
	  ,CrewRating.[CustomerID]    
	  ,CrewRating.[AircraftTypeID]    
	  ,CrewRating.[CrewRatingDescription]    
	  ,CrewRating.[IsPilotinCommand]    
	  ,CrewRating.[IsSecondInCommand]    
	  ,CrewRating.[IsEngineer]    
	  ,CrewRating.[IsInstructor]    
	  ,CrewRating.[IsAttendant]    
	  ,CrewRating.[AsOfDT]    
	  ,CrewRating.[TimeInType]    
	  ,CrewRating.[Day1]    
	  ,CrewRating.[Night1]    
	  ,CrewRating.[Instrument]    
	  ,CrewRating.[Simulator]    
	  ,CrewRating.[PilotinCommand]    
	  ,CrewRating.[SecondinCommand]    
	  ,CrewRating.[FlightEngineer]    
	  ,CrewRating.[FlightInstructor]    
	  ,CrewRating.[FlightAttendant]    
	  ,CrewRating.[ClientID]    
	  ,CrewRating.[LastUpdUID]    
	  ,CrewRating.[LastUpdTS]    
	  ,CrewRating.[IsQualInType135PIC]    
	  ,CrewRating.[IsQualInType135SIC]    
	  ,CrewRating.[PilotinCommandDayHrs]    
	  ,CrewRating.[PilotInCommandNightHrs]    
	  ,CrewRating.[PilotInCommandINSTHrs]    
	  ,CrewRating.[SecondInCommandDay]    
	  ,CrewRating.[SecondInCommandNight]    
	  ,CrewRating.[SecondInCommandInstr]    
	  ,CrewRating.[TotalTimeInTypeHrs]    
	  ,CrewRating.[TotalDayHrs]    
	  ,CrewRating.[TotalNightHrs]    
	  ,CrewRating.[TotalINSTHrs]    
	  ,CrewRating.[PilotInCommandTypeHrs]    
	  ,CrewRating.[TPilotinCommandDayHrs]    
	  ,CrewRating.[TPilotInCommandNightHrs]    
	  ,CrewRating.[TPilotInCommandINSTHrs]    
	  ,CrewRating.[SecondInCommandTypeHrs]    
	  ,CrewRating.[SecondInCommandDayHrs]    
	  ,CrewRating.[SecondInCommandNightHrs]    
	  ,CrewRating.[SecondInCommandINSTHrs]    
	  ,CrewRating.[OtherSimHrs]    
	  ,CrewRating.[OtherFlightEngHrs]    
	  ,CrewRating.[OtherFlightInstrutorHrs]    
	  ,CrewRating.[OtherFlightAttdHrs]    
	  ,CrewRating.[TotalUpdateAsOfDT]    
	  ,CrewRating.[IsInActive]    
	  ,CrewRating.[IsCheckAirman]    
	  ,CrewRating.[IsCheckAttendant]    
	  ,CrewRating.[IsAttendantFAR91]    
	  ,CrewRating.[IsAttendantFAR135]    
	  ,CrewRating.[IsDeleted]  
	  ,ISNULL(CrewRating.[IsPIC121],0) as IsPIC121  
	  ,ISNULL(CrewRating.[IsPIC125],0) as IsPIC125  
	  ,ISNULL(CrewRating.[IsSIC121],0) as IsSIC121  
	  ,ISNULL(CrewRating.[IsSIC125],0) as IsSIC125  
	  ,ISNULL(CrewRating.[IsPIC91],0) as IsPIC91  
	  ,ISNULL(CrewRating.[IsSIC91],0) as IsSIC91  
	  ,ISNULL(CrewRating.[IsAttendant121],0) as IsAttendant121  
	  ,ISNULL(CrewRating.[IsAttendant125],0) as IsAttendant125  
	  ,AircraftType.AircraftCD as AircraftTypeCD    
	  ,Client.ClientCD as ClientCD    
	FROM CrewRating as CrewRating      
		LEFT OUTER JOIN Aircraft as AircraftType on AircraftType.AircraftID = CrewRating.AircraftTypeID    
		LEFT OUTER JOIN Client as Client on Client.ClientID = CrewRating.ClientID      
	WHERE CrewRating.CustomerID = @CustomerID AND CrewRating.CrewID = @CrewID AND CrewRating.isdeleted = 0  
	ORDER BY AircraftTypeCD  
  END    
END


GO

