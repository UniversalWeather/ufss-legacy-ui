/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_DeleteCRCateringList]    Script Date: 02/11/2013 15:55:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_DeleteCRCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRCateringList]
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRCateringList]
(@CRCateringListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
)
AS BEGIN

	SET NOCOUNT OFF;
	Delete from CRCateringList where 
		CRCateringListID = @CRCateringListID and CustomerID=@CustomerID and CRLegID=@CRLegID

END
GO


