
GO
/****** Object:  StoredProcedure [dbo].[spAddFuelLocator]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFuelLocator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFuelLocator]
go
CREATE PROCEDURE [dbo].[spAddFuelLocator] (
	@CustomerID BIGINT  
 ,@FuelLocatorCD CHAR(4)  
 ,@FuelLocatorDescription VARCHAR(25)  
 ,@ClientID bigint  
 ,@LastUpdUID VARCHAR(30)  
 ,@LastUpdTS DATETIME  
 ,@IsDeleted BIT
 ,@IsInActive BIT  
 )  
 -- =============================================  
 -- Author:Hamsha.S  
 -- Create date: 08/05/2012  
 -- Description: Insert the FuelLocator information  
 -- =============================================  
AS  
BEGIN  
 SET NOCOUNT ON  
  
 DECLARE @FuelLocatorID BIGINT  
  
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID  
  ,'MasterModuleCurrentNo'  
  ,@FuelLocatorID OUTPUT  
SET @LastUpdTS = GETUTCDATE()   
 INSERT INTO [FuelLocator] (  
  FuelLocatorID  
  ,CustomerID  
  ,FuelLocatorCD  
  ,FuelLocatorDescription  
  ,ClientID  
  ,LastUpdUID  
  ,LastUpdTS  
  ,IsDeleted  
  ,IsInActive
  )  
 VALUES (  
  @FuelLocatorID  
  ,@CustomerID  
  ,@FuelLocatorCD  
  ,@FuelLocatorDescription  
  ,@ClientID  
  ,@LastUpdUID  
  ,@LastUpdTS  
  ,@IsDeleted  
  ,@IsInActive
  )  
END  
GO
