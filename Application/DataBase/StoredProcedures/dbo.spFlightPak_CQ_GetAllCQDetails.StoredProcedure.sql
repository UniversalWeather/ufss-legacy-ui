
/****** Object:  StoredProcedure [dbo].[spFlightPak_CQ_GetAllCQDetails]    Script Date: 02/05/2014 11:43:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CQ_GetAllCQDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CQ_GetAllCQDetails]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CQ_GetAllCQDetails]    Script Date: 02/05/2014 11:43:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




CREATE PROCEDURE [dbo].[spFlightPak_CQ_GetAllCQDetails]   
 (  
  @CustomerID Bigint,  
  @HomebaseID Bigint, 
  @EstDepartureDT date = null,
  @ExpressQuote bit,
  @LeadSourceID Bigint, 
  @CustomerType varchar(10),
  @SalesPersonID Bigint
  )  
 AS    
 SET NOCOUNT ON;   
 BEGIN   
     --Exec spFlightPak_CQ_GetAllCQDetails 10002,0,'', 0, 0,'',0
  
  Declare @Sql varchar(4000)    
  
  set @Sql =  'SELECT   
      CQF.CQFileID,  
      CQF.CustomerID,  
      CQF.FileNUM,           
      CQF.EstDepartureDT,
      CQF.QuoteDt,
      CQF.CQCustomerID,
      CQF.CQCustomerName,
      CQF.CQFileDescription,      
      CQF.IsFinancialWarning,
      Case When CQF.Notes is not null and CQF.Notes <> '''' then ''!'' else '''' end as Alert,      
      A.ICAOID AS HomebaseCD ,
       CASE WHEN
	 (
       SELECT count(1)
       FROM CQException PREFEX
       INNER JOIN CQMain CM ON CM.CQFileID = CQF.CQFileID
       WHERE CM.CQMainID = PREFEX.CQMainID
	    ) >0  THEN ''!'' 
	    ELSE ''''
		END as FileExcep ,
		isnull(LS.LeadSourceCD,'''') as LeadSourceCD,
		case when isnull(CQC.CustomerType,'''')=''1'' then ''Broker''
			when  isnull(CQC.CustomerType,'''')=''2'' then ''Block CQ client''
			else ''''
			end as ''CustomerType''
			,				  
		isnull(SP.SalesPersonCD,'''') as SalesPersonCD,
		 ISNULL(CQM.QuoteNUM,'''') as ''QuoteNUM'',
      ISNULL(PM.TripNUM,'''') as ''TripNUM'', 
      case when isnull(PM.TripStatus,'''')=''W'' then ''Worksheet''
			when  isnull(PM.TripStatus,'''')=''T'' then ''Tripsheet''
			when  isnull(PM.TripStatus,'''')=''U'' then ''Unfulfilled''
			when  isnull(PM.TripStatus,'''')=''X'' then ''Canceled''
			when  isnull(PM.TripStatus,'''')=''H'' then ''Hold''	
			else ''''		
			end as ''TripStatus''
			,	 
       
		ISNULL(CQM.QuoteStage,'''') as QuoteStage
      FROM CQFile CQF
      INNER JOIN Company C on CQF.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID 
      LEFT JOIN LeadSource LS on CQF.LeadSourceID = LS.LeadSourceID
      LEFT JOIN CQCustomer CQC on CQF.CQCustomerID =   CQC.CQCustomerID
      LEFT JOIN SalesPerson SP on CQF.SalesPersonID =   SP.SalesPersonID
      LEFT JOIN CQMain CQM on CQM.CQFileID =   CQF.CQFileID
      LEFT JOIN PreflightMain PM on PM.TripID =   CQM.TripID		    
      WHERE         
       CQF.CustomerID= '+   CAST( @CustomerID  as varchar(20))  
      set @Sql =  @Sql + ' and CQF.IsDeleted =0 ' 
  
    If (@HomebaseID<> 0)  
     set @Sql =  @Sql + ' and CQF.HomebaseID  = ' + cast (@HomebaseID as varchar(25))
    If (@LeadSourceID<> 0)  
     set @Sql =  @Sql + ' and CQF.LeadSourceID  = ' + cast (@LeadSourceID as varchar(25)) 
    If (@SalesPersonID<> 0)  
     set @Sql =  @Sql + ' and CQF.SalesPersonID  = ' + cast (@SalesPersonID as varchar(25)) 
    if (@EstDepartureDT IS NOT NULL) 
     set @Sql =  @Sql + ' and  CQF.EstDepartureDT >=''' + cast (@EstDepartureDT as varchar(25)) + ''''
     if (@CustomerType<> '')  
     set @Sql =  @Sql + ' and CQC.CustomerType  = ''' + cast (@CustomerType as varchar(1))   + ''''
      
     If (@ExpressQuote is not null)  
     set @Sql =  @Sql + ' and CQF.ExpressQuote  = ' + cast (@ExpressQuote as varchar(1))  
     set @Sql = @Sql + ' order by CQF.EstDepartureDT, CQF.CQFileID, CQM.QuoteNUM'
   print @Sql  
    
      create table #tempCQmain  
      ( 
      id int identity(1,1), 
      CQFileID bigint,  
      CustomerID bigint,
      FileNUM int,  
      EstDepartureDT date,
      QuoteDt  date,
      CQCustomerID bigint,
      CQCustomerName Varchar(40),
      CQFileDescription Varchar(40),      
      IsFinancialWarning bit,
      Alert char(1),      
      HomebaseCD  char(4),
      FileExcep char(1) ,
      LeadSourceCD char(4),
	  CustomerType varchar(20),	  
	  SalesPersonCD char(5),
	  QuoteNUM int,
      TripNUM bigint,
      TripStatus Varchar(25),
      QuoteStage varchar(20) 
          )  
         
        
      insert into #tempCQmain Exec ( @Sql)  
        
SELECT
      id,
		CQFileID ,  
		CustomerID ,  
		FileNUM ,  
		EstDepartureDT,  
		QuoteDt,
		CQCustomerID,  
		CQCustomerName,  
		CQFileDescription,  
		IsFinancialWarning,  
		Alert ,  		 
		HomebaseCD,
		FileExcep,
		LeadSourceCD,
		CustomerType,
		SalesPersonCD,
		QuoteNUM ,
        TripNUM ,
        TripStatus,
        QuoteStage 
	
FROM #tempCQmain 

      union  
      
      SELECT
      
      0 as 'id',   
      CQFile.CQFileID,  
      CQFile.CustomerID,  
      CQFile.FileNUM,  
      CQFile.EstDepartureDT,
      QuoteDt,
      CQFile.CQCustomerID,
      CQFile.CQCustomerName,
      CQFile.CQFileDescription,      
      CQFile.IsFinancialWarning,
     CQFile.Notes as Alert,    
      A.ICAOID AS HomebaseCD ,
      '' as FileExcep,
      '' as LeadSourceCD,
	  '' as CustomerType,
	  '' as SalesPersonCD,
	  '' as QuoteNUM,
	  '' as TripNUM,
	  '' as TripStatus,
	  '' as QuoteStage
      FROM CQFile
       
      INNER JOIN Company C on CQFile.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID 
      
      WHERE 1=2 
        
      DROP TABLE #tempCQmain  

 
 END




GO


