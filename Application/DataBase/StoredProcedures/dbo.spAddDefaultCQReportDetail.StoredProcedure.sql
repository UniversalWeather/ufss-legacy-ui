
/****** Object:  StoredProcedure [dbo].[spAddDefaultCQReportDetail]    Script Date: 04/27/2013 11:37:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddDefaultCQReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddDefaultCQReportDetail]
GO

/****** Object:  StoredProcedure [dbo].[spAddDefaultCQReportDetail]    Script Date: 04/27/2013 11:37:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddDefaultCQReportDetail]
(@CustomerID bigint,
@CQReportHeaderID bigint)
AS BEGIN
			DECLARE @CQReportDetailID BIGINT
			DECLARE @TMPCQReportDetailID BIGINT
			DECLARE @tblCQReportDetail TABLE
			(
				CQReportDetailID bigint,
				CustomerID bigint,
				CQReportHeaderID bigint,
				ReportNumber int,
				ReportDescription varchar(35),
				IsSelected bit,
				Copies int,
				ReportProcedure varchar(8),
				ParameterCD varchar(10),
				ParamterDescription varchar(100),
				ParamterType char(1),
				ParameterWidth int,
				ParameterCValue varchar(60),
				ParameterLValue bit,
				ParameterNValue int,
				ParameterValue varchar(1500),
				ReportOrder int,
				--TripSheetOrder int,
				LastUpdUID varchar(30),
				LastUpdTS datetime,
				IsDeleted bit,
				ParameterSize varchar(50),
				WarningMessage varchar(500)
			)
			
			INSERT INTO @tblCQReportDetail 
			(CQReportDetailID,CustomerID,CQReportHeaderID,ReportNumber,ReportDescription,IsSelected,
				Copies,ReportProcedure,ParameterCD,ParamterDescription,ParamterType,ParameterWidth,ParameterCValue,
				ParameterLValue,ParameterNValue,ParameterValue,ReportOrder,LastUpdUID,LastUpdTS,IsDeleted,ParameterSize,WarningMessage)
			
			SELECT CQReportDetailID,CustomerID,CQReportHeaderID,ReportNumber,ReportDescription,IsSelected,
					Copies,ReportProcedure,ParameterCD,ParamterDescription,ParamterType,ParameterWidth,ParameterCValue,
					ParameterLValue,ParameterNValue,ParameterValue,ReportOrder,LastUpdUID,LastUpdTS,IsDeleted,ParameterSize,WarningMessage
				FROM CQReportDetail
				WHERE CustomerID = @CustomerID AND CQReportHeaderID = (SELECT TOP 1 CQReportHeaderID FROM CQReportHeader WHERE CustomerID = @CustomerID AND ReportID = 'DEFAULT')

			WHILE (SELECT Count(*) from @tblCQReportDetail)>0
				BEGIN
					
					SELECT TOP 1 @TMPCQReportDetailID = CQReportDetailID FROM @tblCQReportDetail 
							ORDER BY CQReportDetailID ASC
				
				
					EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CQReportDetailID OUTPUT
	
						INSERT INTO CQReportDetail
							   ([CQReportDetailID]
							   ,[CustomerID]
							   ,[CQReportHeaderID]
							   ,[ReportNumber]
							   ,[ReportDescription]
							   ,[IsSelected]
							   ,[Copies]
							   ,[ReportProcedure]
							   ,[ParameterCD]
							   ,[ParamterDescription]
							   ,[ParamterType]
							   ,[ParameterWidth]
							   ,[ParameterCValue]
							   ,[ParameterLValue]
							   ,[ParameterNValue]
							   ,[ParameterValue]
							   ,[ReportOrder]
							   --,[TripSheetOrder]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsDeleted]
							   ,[ParameterSize]
							   ,[WarningMessage])
							
							SELECT @CQReportDetailID
							   ,[CustomerID]
							   ,@CQReportHeaderID
							   ,[ReportNumber]
							   ,[ReportDescription]
							   ,[IsSelected]
							   ,[Copies]
							   ,[ReportProcedure]
							   ,[ParameterCD]
							   ,[ParamterDescription]
							   ,[ParamterType]
							   ,[ParameterWidth]
							   ,[ParameterCValue]
							   ,[ParameterLValue]
							   ,[ParameterNValue]
							   ,[ParameterValue]
							   ,[ReportOrder]
							   --,[TripSheetOrder]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsDeleted]							   
							   ,[ParameterSize]
							   ,[WarningMessage]
							   FROM @tblCQReportDetail
							   WHERE CQReportDetailID=@TMPCQReportDetailID
							   
							   DELETE FROM @tblCQReportDetail WHERE CQReportDetailID=@TMPCQReportDetailID
				   				
				END
					

END

GO


