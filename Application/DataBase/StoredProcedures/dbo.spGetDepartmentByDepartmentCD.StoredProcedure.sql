

/****** Object:  StoredProcedure [dbo].[spGetDepartmentByDepartmentCD]    Script Date: 05/07/2013 12:13:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDepartmentByDepartmentCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetDepartmentByDepartmentCD]
GO



/****** Object:  StoredProcedure [dbo].[spGetDepartmentByDepartmentCD]    Script Date: 05/07/2013 12:13:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDepartmentByDepartmentCD]     
(    
 @CustomerID  BIGINT,    
 @DepartmentCD varchar(8) ,  
 @ClientID BIGINT   
 )    
AS        
BEGIN        
-- =============================================        
-- Author: Hamsha  
-- Create date: 09/11/2012       
-- Description: To get the Department record based on DepartmentCD  
-- =============================================       
  
  IF(@ClientID>0)        
    BEGIN     
 SELECT D.DepartmentID  
      ,D.DepartmentCD     
      ,D.CustomerID        
      ,D.ClientID        
      ,D.IsDeleted
     ,D.DepartPercentage          
                 
   FROM Department D  
  
    WHERE   D.CustomerID = @CustomerID  
    AND D.ClientID = @ClientID  
    and D.IsDeleted != 1   
    and D.DepartmentCD = @DepartmentCD  
      
 END  
ELSE  
  BEGIN  
 SELECT D.DepartmentID  
      ,D.DepartmentCD     
      ,D.CustomerID        
      ,D.ClientID        
      ,D.IsDeleted 
      ,D.DepartPercentage          
                 
   FROM Department D  
  
    WHERE   D.CustomerID = @CustomerID  
    and D.IsDeleted != 1   
    and D.DepartmentCD = @DepartmentCD  
 END  
END  
GO


