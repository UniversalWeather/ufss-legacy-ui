

/****** Object:  StoredProcedure [dbo].[sp_fss_GetFBOByAirportID_OLD]    Script Date: 02/28/2014 13:39:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetFBOByAirportID_OLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetFBOByAirportID_OLD]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetFBOByAirportID_OLD]    Script Date: 02/28/2014 13:39:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetFBOByAirportID_OLD]  
(  
 @CustomerID BIGINT  
 ,@AirportID BIGINT  
 ,@FBOID BIGINT  
    ,@FBOCD varchar(4)   
    ,@FetchChoiceOnly BIT  
    ,@FetchUVAIROnly BIT   
 ,@FetchActiveOnly BIT      
)                
AS  
BEGIN  
SET NOCOUNT ON    
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get ALL FBOByAirportID details for Popup        
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,0,1  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,1,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',1,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,1009958128,'',0,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'0014',0,0,0  
-- =============================================                  
              
              
--DECLARE @AirportICAOID CHAR(4)              
DECLARE @UWACustomerID BIGINT              
              
--SELECT @AirportICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID              
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output              
                   
DECLARE @ICAOCD VARCHAR(50)        
DECLARE @UWAAirportId VARCHAR(50)      
DECLARE @ICAOCD1 VARCHAR(50)        
DECLARE @CustomAirportID VARCHAR(50)           
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID          
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD        
    
--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD       
         
         
         
SELECT   
  F.FBOID         
        ,F.AirportID  
        ,F.FBOCD  
        ,F.CustomerID  
        ,IsChoice  
        ,FBOVendor  
        ,PhoneNUM1  
        ,Frequency  
        ,IsUWAAirPartner  
        ,IsCrewCar  
        ,FuelBrand  
        ,ControlNum  
        ,FaxNum  
        ,LastFuelDT  
        ,LastFuelPrice  
        ,NegotiatedFuelPrice  
        ,Addr1  
        ,Addr2  
  ,CityName  
        ,StateName  
        ,PostalZipCD  
        ,F.CountryID  
        ,Contact  
        ,Remarks  
        ,IsFPK  
        ,FPKUpdateDT              
        ,F.LastUpdUID              
        ,LastUpdTS              
        ,RecordType                      
         ,ISNULL(F.IsInactive,0) IsInactive
        ,PhoneNUM2              
        ,HoursOfOperation              
        ,SourceID              
        ,UpdateDT              
        ,IsPrimaryChoice              
        ,PostedPrice              
        ,PaymentType              
  ,FuelQty              
        ,FixBaseOperatorID              
        ,HHEATED              
        ,HSJET              
        ,HMJET              
        ,HLJET              
        ,HCJET              
        ,FLSJETA              
        ,FLS100LL              
        ,FLS80              
        ,FSSJETA              
        ,FSS100LL              
        ,FSS80              
        ,FVOLDISCT              
        ,ARFEE              
        ,ARFEECOM              
        ,AHFEE              
  ,AHFEECOM              
        ,A24              
        ,AQT              
        ,ASEC              
        ,AddDeIcing              
        ,AOX              
        ,ANIT              
        ,ALAV              
        ,AGPU              
        ,AOWGR              
        ,AMMINOR              
        ,AMMAJOR              
        ,AMAV              
        ,AMAUTH              
        ,AMAUTHCOM              
        ,AICLEAN              
        ,AICS              
        ,AIIR              
        ,AEWAS              
        ,AEBRITE              
        ,AEPAINT              
        ,PWPS              
        ,PFPR         
        ,PCT              
        ,PCC              
        ,PLOUNGE              
        ,PREC              
        ,PEXEC            
        ,PKIT              
        ,PLAUND              
        ,PSR              
        ,PSHOWER              
        ,PSUPP              
        ,PMEOS              
        ,RHERTZ              
        ,RAVIS              
        ,ROTHER              
        ,ROCOMM              
        ,PPLIMO              
        ,PPTAXI              
        ,PPROS              
        ,PPRWD              
        ,PPCONC              
        ,PPCS              
        ,PPCR              
  ,PPLSR              
        ,PPSEC              
        ,PPCWIA              
        ,PPMS              
        ,PPFS              
        ,PPCA              
        ,PPGOLF              
        ,PPCOFFEE              
        ,PPSNACKS              
  ,PPNEWS              
        ,PPICE              
        ,PXMF              
        ,PXCOFFEE              
        ,PXBAR              
        ,PXCCC              
        ,PXVCC              
        ,PXLOUNGE              
        ,PXGIFT              
        ,OHF              
        ,OPCS              
        ,OTCS              
        ,OJCS              
        ,OHCS              
        ,OAPS              
        ,OCH              
        ,CreditCardVISA              
        ,CreditCardMasterCard              
  ,CreditCardAMEX              
        ,CreditCardDinerClub              
        ,CreditCardJCB              
        ,CreditCardMS              
        ,CreditCardAVCard              
        ,CreditCardUVAir              
        ,COMJET              
        ,TollFreePhoneNum              
        ,SITA              
        ,UNICOM              
        ,ARINC              
        ,IsUVAirPNR              
        ,Website              
        ,EmailAddress              
        ,LatitudeDegree              
        ,LatitudeMinutes              
        ,LatitudeNorthSouth              
        ,LongitudeDegrees              
        ,LongitudeMinutes              
        ,LongitudeEastWest              
        ,(SELECT CAST(CASE WHEN F.IsUWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END As varchar )) as Filter          
        ,IsUWAMaintFlag                     
        ,UWAID              
        ,ReceiptNum              
        ,ISNULL(F.IsDeleted,0) IsDeleted              
        ,UWAUpdates              
        ,C.CountryCD              
        ,F.ContactBusinessPhone            
  ,F.ContactCellPhoneNum            
  ,F.ContactEmail            
  ,F.Addr3            
  ,F.CustomField1      
  ,F.CustomField2      
  ,F.ExchangeRateID      
  ,F.NegotiatedTerms      
  ,F.SundayWorkHours      
  ,F.MondayWorkHours      
  ,F.TuesdayWorkHours      
  ,F.WednesdayWorkHours      
  ,F.ThursdayWorkHours      
  ,F.FridayWorkHours      
  ,F.SaturdayWorkHours      
  FROM  FBO F                
        left outer join  Country C on F.CountryID = C.CountryID                
   WHERE ISNULL(F.IsDeleted,0) = 0   
   AND ((F.AirportID  = @CustomAirportID AND F.CustomerID = @CustomerID) or (F.AirportID = @UWAAirportId AND  F.CustomerID = @CustomerID ))  
    AND F.FBOID = case when @FBOID <>0 then @FBOID else F.FBOID end   
    AND F.FBOCD = case when Ltrim(Rtrim(@FBOCD)) <> '' then @FBOCD else F.FBOCD end   
   AND ISNULL(F.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(F.IsChoice,0) end  
   AND ISNULL(F.IsUWAAirPartner,0) = case when @FetchUVAIROnly =1 then 1 else ISNULL(F.IsUWAAirPartner,0) end       
   AND ISNULL(F.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(F.IsInActive,0) end  
 --ORDER BY F.FBOVendor ASC  
                        
 UNION ALL          
                 
    SELECT F.FBOID              
           ,F.AirportID              
           ,F.FBOCD              
           ,F.CustomerID              
           ,IsChoice              
           ,FBOVendor              
           ,PhoneNUM1              
           ,Frequency              
           ,IsUWAAirPartner              
           ,IsCrewCar              
           ,FuelBrand              
           ,ControlNum              
           ,FaxNum              
           ,LastFuelDT              
           ,LastFuelPrice              
           ,NegotiatedFuelPrice              
           ,Addr1              
           ,Addr2              
           ,CityName              
           ,StateName              
           ,PostalZipCD              
           ,F.CountryID              
           ,Contact              
           ,Remarks              
           ,IsFPK              
           ,FPKUpdateDT              
           ,F.LastUpdUID              
           ,LastUpdTS              
           ,RecordType              
            ,ISNULL(F.IsInactive,0)  IsInactive           
           ,PhoneNUM2              
           ,HoursOfOperation              
           ,SourceID              
           ,UpdateDT              
           ,IsPrimaryChoice              
           ,PostedPrice              
           ,PaymentType              
           ,FuelQty              
           ,FixBaseOperatorID              
           ,HHEATED              
           ,HSJET              
           ,HMJET              
           ,HLJET              
           ,HCJET              
           ,FLSJETA              
           ,FLS100LL              
           ,FLS80              
           ,FSSJETA              
           ,FSS100LL              
           ,FSS80              
           ,FVOLDISCT              
           ,ARFEE              
           ,ARFEECOM              
           ,AHFEE              
           ,AHFEECOM              
           ,A24              
           ,AQT              
           ,ASEC              
           ,AddDeIcing              
           ,AOX              
           ,ANIT              
           ,ALAV              
           ,AGPU              
           ,AOWGR              
           ,AMMINOR              
           ,AMMAJOR              
           ,AMAV              
           ,AMAUTH          
           ,AMAUTHCOM              
           ,AICLEAN              
           ,AICS              
           ,AIIR              
           ,AEWAS              
           ,AEBRITE              
           ,AEPAINT              
           ,PWPS              
           ,PFPR              
           ,PCT              
           ,PCC              
           ,PLOUNGE              
           ,PREC              
           ,PEXEC              
           ,PKIT              
           ,PLAUND              
           ,PSR              
           ,PSHOWER              
           ,PSUPP              
           ,PMEOS              
           ,RHERTZ              
           ,RAVIS              
           ,ROTHER              
           ,ROCOMM              
           ,PPLIMO              
           ,PPTAXI              
           ,PPROS              
           ,PPRWD              
           ,PPCONC              
           ,PPCS              
           ,PPCR              
     ,PPLSR              
           ,PPSEC              
           ,PPCWIA              
           ,PPMS              
           ,PPFS              
           ,PPCA              
           ,PPGOLF              
           ,PPCOFFEE              
           ,PPSNACKS              
           ,PPNEWS              
           ,PPICE              
           ,PXMF              
           ,PXCOFFEE              
           ,PXBAR              
           ,PXCCC              
           ,PXVCC              
           ,PXLOUNGE              
           ,PXGIFT              
           ,OHF              
           ,OPCS              
           ,OTCS              
           ,OJCS              
           ,OHCS              
           ,OAPS              
           ,OCH              
           ,CreditCardVISA              
           ,CreditCardMasterCard              
           ,CreditCardAMEX              
           ,CreditCardDinerClub              
           ,CreditCardJCB              
           ,CreditCardMS              
           ,CreditCardAVCard              
           ,CreditCardUVAir              
           ,COMJET              
           ,TollFreePhoneNum              
           ,SITA              
           ,UNICOM              
           ,ARINC              
           ,IsUVAirPNR              
           ,Website              
           ,EmailAddress              
           ,LatitudeDegree        
           ,LatitudeMinutes              
           ,LatitudeNorthSouth              
           ,LongitudeDegrees              
           ,LongitudeMinutes              
           ,LongitudeEastWest              
            ,(SELECT CAST(CASE WHEN F.IsUWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END As varchar )) as Filter          
           ,IsUWAMaintFlag                  
           ,UWAID              
           ,ReceiptNum              
           ,ISNULL(F.IsDeleted,0)  IsDeleted             
           ,UWAUpdates              
           ,C.CountryCD              
           ,F.ContactBusinessPhone            
     ,F.ContactCellPhoneNum            
     ,F.ContactEmail            
     ,F.Addr3      
     ,F.CustomField1      
     ,F.CustomField2      
     ,F.ExchangeRateID      
     ,F.NegotiatedTerms      
     ,F.SundayWorkHours      
     ,F.MondayWorkHours      
     ,F.TuesdayWorkHours      
     ,F.WednesdayWorkHours      
     ,F.ThursdayWorkHours      
     ,F.FridayWorkHours      
     ,F.SaturdayWorkHours      
           
     FROM  FBO F        
            left outer join  Country C on F.CountryID = C.CountryID            
                        
  WHERE F.AirportID = @UWAAirportId  AND F.CustomerID = @UWACustomerID          
    AND ISNULL(F.IsDeleted,0) = 0   
    AND F.FBOID = case when @FBOID <>0 then @FBOID else F.FBOID end   
    AND F.FBOCD = case when Ltrim(Rtrim(@FBOCD)) <> '' then @FBOCD else F.FBOCD end   
    AND ISNULL(F.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(F.IsChoice,0) end  
    AND ISNULL(F.IsUWAAirPartner,0) = case when @FetchUVAIROnly =1 then 1 else ISNULL(F.IsUWAAirPartner,0) end       
    AND ISNULL(F.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(F.IsInActive,0) end     
    AND F.FBOCD NOT IN           
   (SELECT FBOCD FROM FBO WHERE ((AirportID = @CustomAirportID AND CustomerID = @CustomerID) or (AirportID = @UWAAirportId AND CustomerID = @CustomerID))        
    AND ISNULL(IsDeleted,0) = 0)                   
 ORDER BY  FBOVendor --FBOCD     
 END  
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetFBOByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetFBOByAirportID]
GO

CREATE PROCEDURE [dbo].[sp_fss_GetFBOByAirportID]  
(  
 @CustomerID BIGINT  
 ,@AirportID BIGINT  
 ,@FBOID BIGINT  
    ,@FBOCD varchar(4)   
    ,@FetchChoiceOnly BIT  
    ,@FetchUVAIROnly BIT   
 ,@FetchActiveOnly BIT      
)                
AS  
BEGIN  
SET NOCOUNT ON    
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get ALL FBOByAirportID details for Popup        
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,0,1  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',0,1,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'',1,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,1009958128,'',0,0,0  
-- Exec sp_fss_GetFBOByAirportID 10099,1009938248,0,'0014',0,0,0  
-- =============================================                  

-- =============================================
-- Changed By: Rajitha Fernando
-- Changed Date: 12/23/2014	
-- Description: Union All is removed to increase the performance
-- =============================================

              
              
--DECLARE @AirportICAOID CHAR(4)              
DECLARE @UWACustomerID BIGINT              
              
--SELECT @AirportICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID              
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output              
                   
DECLARE @ICAOCD VARCHAR(50)        
DECLARE @UWAAirportId VARCHAR(50)      
DECLARE @ICAOCD1 VARCHAR(50)        
DECLARE @CustomAirportID VARCHAR(50)           
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID          
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD        
    
--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD       
         
         
SELECT F.FBOID
,F.AirportID
,F.FBOCD
,F.CustomerID
,F.IsChoice
,F.FBOVendor
,F.PhoneNUM1
,F.Frequency
,F.IsUWAAirPartner
,F.IsCrewCar
,F.FuelBrand
,F.ControlNum
,F.FaxNum
,F.LastFuelDT
,F.LastFuelPrice
,F.NegotiatedFuelPrice
,F.Addr1
,F.Addr2
,F.CityName
,F.StateName
,F.PostalZipCD
,F.CountryID
,F.Contact
,F.Remarks
,F.IsFPK
,F.FPKUpdateDT
,F.LastUpdUID
,F.LastUpdTS
,F.RecordType
,ISNULL(F.IsInactive,0)  IsInactive
,F.PhoneNUM2
,F.HoursOfOperation
,F.SourceID
,F.UpdateDT
,F.IsPrimaryChoice
,F.PostedPrice
,F.PaymentType
,F.FuelQty
,F.FixBaseOperatorID
,F.HHEATED
,F.HSJET
,F.HMJET
,F.HLJET
,F.HCJET
,F.FLSJETA
,F.FLS100LL
,F.FLS80
,F.FSSJETA
,F.FSS100LL
,F.FSS80
,F.FVOLDISCT
,F.ARFEE
,F.ARFEECOM
,F.AHFEE
,F.AHFEECOM
,F.A24
,F.AQT
,F.ASEC
,F.AddDeIcing
,F.AOX
,F.ANIT
,F.ALAV
,F.AGPU
,F.AOWGR
,F.AMMINOR
,F.AMMAJOR
,F.AMAV
,F.AMAUTH
,F.AMAUTHCOM
,F.AICLEAN
,F.AICS
,F.AIIR
,F.AEWAS
,F.AEBRITE
,F.AEPAINT
,F.PWPS
,F.PFPR
,F.PCT
,F.PCC
,F.PLOUNGE
,F.PREC
,F.PEXEC
,F.PKIT
,F.PLAUND
,F.PSR
,F.PSHOWER
,F.PSUPP
,F.PMEOS
,F.RHERTZ
,F.RAVIS
,F.ROTHER
,F.ROCOMM
,F.PPLIMO
,F.PPTAXI
,F.PPROS
,F.PPRWD
,F.PPCONC
,F.PPCS
,F.PPCR
,F.PPLSR
,F.PPSEC
,F.PPCWIA
,F.PPMS
,F.PPFS
,F.PPCA
,F.PPGOLF
,F.PPCOFFEE
,F.PPSNACKS
,F.PPNEWS
,F.PPICE
,F.PXMF
,F.PXCOFFEE
,F.PXBAR
,F.PXCCC
,F.PXVCC
,F.PXLOUNGE
,F.PXGIFT
,F.OHF
,F.OPCS
,F.OTCS
,F.OJCS
,F.OHCS
,F.OAPS
,F.OCH
,F.CreditCardVISA
,F.CreditCardMasterCard
,F.CreditCardAMEX
,F.CreditCardDinerClub
,F.CreditCardJCB
,F.CreditCardMS
,F.CreditCardAVCard
,F.CreditCardUVAir
,F.COMJET
,F.TollFreePhoneNum
,F.SITA
,F.UNICOM
,F.ARINC
,F.IsUVAirPNR
,F.Website
,F.EmailAddress
,F.LatitudeDegree
,F.LatitudeMinutes
,F.LatitudeNorthSouth
,F.LongitudeDegrees
,F.LongitudeMinutes
,F.LongitudeEastWest
,(SELECT CAST(CASE WHEN F.IsUWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END As varchar )) as Filter
,F.IsUWAMaintFlag  
,F.UWAID
,f.ReceiptNum
,ISNULL(F.IsDeleted,0)  IsDeleted  
,F.UWAUpdates
,C.CountryCD
,F.ContactBusinessPhone 
,F.ContactCellPhoneNum 
,F.ContactEmail 
,F.Addr3 
,F.CustomField1 
,F.CustomField2 
,F.ExchangeRateID 
,F.NegotiatedTerms 
,F.SundayWorkHours 
,F.MondayWorkHours 
,F.TuesdayWorkHours 
,F.WednesdayWorkHours 
,F.ThursdayWorkHours 
,F.FridayWorkHours 
,F.SaturdayWorkHours 

FROM  FBO F
 left outer join  Country C on F.CountryID = C.CountryID 
  
WHERE ((F.AirportID = @UWAAirportId  AND F.CustomerID = @UWACustomerID          
    AND ISNULL(F.IsDeleted,0) = 0   
    AND F.FBOID = case when @FBOID <>0 then @FBOID else F.FBOID end   
    AND F.FBOCD = case when Ltrim(Rtrim(@FBOCD)) <> '' then @FBOCD else F.FBOCD end   
    AND ISNULL(F.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(F.IsChoice,0) end  
    AND ISNULL(F.IsUWAAirPartner,0) = case when @FetchUVAIROnly =1 then 1 else ISNULL(F.IsUWAAirPartner,0) end       
    AND ISNULL(F.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(F.IsInActive,0) end     
    AND F.FBOCD NOT IN           
   (SELECT FBOCD FROM FBO WHERE ((AirportID = @CustomAirportID AND CustomerID = @CustomerID) or (AirportID = @UWAAirportId AND CustomerID = @CustomerID))        
    AND ISNULL(IsDeleted,0) = 0))  OR
	(ISNULL(F.IsDeleted,0) = 0   
   AND ((F.AirportID  = @CustomAirportID AND F.CustomerID = @CustomerID) or (F.AirportID = @UWAAirportId AND  F.CustomerID = @CustomerID ))  
    AND F.FBOID = case when @FBOID <>0 then @FBOID else F.FBOID end   
    AND F.FBOCD = case when Ltrim(Rtrim(@FBOCD)) <> '' then @FBOCD else F.FBOCD end   
   AND ISNULL(F.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(F.IsChoice,0) end  
   AND ISNULL(F.IsUWAAirPartner,0) = case when @FetchUVAIROnly =1 then 1 else ISNULL(F.IsUWAAirPartner,0) end       
   AND ISNULL(F.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(F.IsInActive,0) end ))

 ORDER BY  F.FBOVendor --FBOCD

         
END  
GO