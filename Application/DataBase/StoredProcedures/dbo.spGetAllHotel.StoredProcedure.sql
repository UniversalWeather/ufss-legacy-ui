/****** Object:  StoredProcedure [dbo].[spGetAllHotel]    Script Date: 01/08/2013 16:39:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllHotel]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllHotel]    Script Date: 01/08/2013 16:39:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllHotel](@CustomerID BIGINT)    
AS    
-- =============================================    
-- Author: Akhila.A    
-- Create date: 2/5/2012    
-- Description: Get the Hotel information    
-- =============================================    
SET NOCOUNT ON    
IF len(@CustomerID) >0    
BEGIN     
 SELECT   A.[HotelID]  
   ,A.[AirportID]  
   ,A.[HotelCD]  
   ,A.[CustomerID]  
   ,[IsChoice]  
   ,[Name]  
   ,[PhoneNum]  
   ,[MilesFromICAO]  
   ,[HotelRating]  
   ,[FaxNum]  
   ,[NegociatedRate]  
   ,[Addr1]  
   ,[Addr2]  
   ,[Addr3]  
   ,A.[CityName]  
   ,A.[MetroID]  
   ,A.[StateName]  
   ,[PostalZipCD]  
   ,A.[CountryID]  
   ,[ContactName]  
   ,[Remarks]  
   ,[IsPassenger]  
   ,[IsCrew]  
   ,[IsUWAMaintained]  
   ,A.[LatitudeDegree]  
   ,A.[LatitudeMinutes]  
   ,A.[LatitudeNorthSouth]  
   ,A.[LongitudeDegrees]  
   ,A.[LongitudeMinutes]  
   ,A.[LongitudeEastWest]  
   ,A.[LastUpdUID]  
   ,A.[LastUpdTS]  
   ,A.[UpdateDT]  
   ,A.[RecordType]  
   ,[MinutesFromICAO]  
   ,[SourceID]  
   ,[ControlNum]  
   ,[Website]  
   ,A.[UWAMaintFlag]  
   ,A.[UWAID]  
   ,A.[IsInActive]  
   ,A.[IsDeleted]  
   ,A.[UWAUpdates]  
   ,Ai.IcaoID     
   ,m.MetroCD  
   ,c.CountryCD  
   ,A.[AltBusinessPhone]  
            ,A.[TollFreePhone]  
            ,A.[BusinessEmail]  
            ,A.[ContactBusinessPhone]  
            ,A.[CellPhoneNum]  
            ,A.[ContactEmail] 
             ,A.[ExchangeRateID]
            ,A.[NegotiatedTerms]
            ,A.[SundayWorkHours] 
            ,A.[MondayWorkHours]   
            ,A.[TuesdayWorkHours]
            ,A.[WednesdayWorkHours]
            ,A.[ThursdayWorkHours]
            ,A.[FridayWorkHours]
            ,A.[SaturdayWorkHours]      
           FROM  [Hotel] A inner join [Airport] Ai on A.AirportID = Ai.AirportID LEFT OUTER JOIN metro m on m.MetroID= a.MetroID LEFT OUTER JOIN Country c on c.CountryID = a.CountryID,  
                      (SELECT CustomerID, AirportID, HotelID,HotelCD FROM Hotel    
                                                            WHERE  (AirportID+HotelCD in (SELECT AirportID+HotelCD FROM Hotel   
                                                                                                  WHERE IsDeleted ='false' AND CustomerID = @CustomerID GROUP BY AirportID+HotelCD )  
  and IsDeleted ='false' AND CustomerID = @CustomerID)  
                       UNION    
                       SELECT CustomerID, AirportID, HotelID,HotelCD FROM Hotel    
                                                             WHERE  (UWAMaintFlag = 'true' OR CustomerID =@CustomerID)     
                                                             AND AirportID+HotelCD NOT IN (SELECT AirportID+HotelCD FROM Hotel   
                                                                                                    WHERE IsDeleted ='false' and UWAMaintFlag = 'false'   
                                                                                                    GROUP BY AirportID+HotelCD   
                                                                                                    )    
                                                             AND IsDeleted = 'false'    
 ) B    
 WHERE A.CustomerID = B.CustomerID   
 AND A.HotelID = B.HotelID   
 AND A.AirportID = B.AirportID   
 ORDER BY UWAMaintFlag    
    
END  
GO


