
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateClientID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateClientID]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateClientID]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [spUpdateClientID]            
(@ClientID bigint,            
 @UserMasterID varchar(max),            
 @FleetID varchar(max),            
 @CrewID text,            
 --@PassengerRequestorID nvarchar(max),            
 @PassengerRequestorID text,    
 @DepartmentID  varchar(max),            
 @FlightCategoryID varchar(max),            
 @FlightPurposeID varchar(max),            
 @FuelLocatorID varchar(max))            
AS BEGIN            

--EXEC SplitText @PassengerRequestorID
            
IF (@ClientID = -1)            
BEGIN            
set @ClientID = null            
END    

DECLARE @ParameterTable TABLE
(
	PrimaryID BIGINT
)        
       
if (@UserMasterID is not null)          
begin          
   
 DECLARE @UserName Varchar(30)    
 DECLARE @CustomerID BIGINT    
 DECLARE @LastUpdUID  Varchar(30)    
 SELECT i.items INTO #TempUserMaster FROM dbo.Split(@UserMasterID,',') AS i    
 While (SELECT Count(*) FROM #TempUserMaster) > 0    
 Begin    
  Select Top 1 @UserName = items From #TempUserMaster    
    
  If Exists(Select UserName From ClientCodeMapping Where UserName = @UserName)    
  Begin    
   Update ClientCodeMapping Set ClientID = @ClientID WHERE Username = @UserName    
  End    
  Else    
  Begin    
   SELECT @CustomerID = CustomerID, @LastUpdUID=LastUpdUID FROM UserMaster Where UserName = @UserName    
      
   Execute spAddClientCodeMapping null, @CustomerID, @ClientID, @UserName, @LastUpdUID , null, 0    
  End     
 DELETE FROM #TempUserMaster WHERE items = @UserName    
 End    
 DROP TABLE #TempUserMaster    
end  
       
if (@FleetID is not null)            
begin            
Update Fleet set ClientID = @ClientID WHERE FleetID in (SELECT i.items FROM dbo.Split(@FleetID,',') AS i)          
end            
          
if (@CrewID is not null)            
begin            
delete from @ParameterTable
INSERT INTO @ParameterTable (PrimaryID) EXEC spSplitText @CrewID
Update Crew set ClientID = @ClientID WHERE CrewID in (SELECT PrimaryID FROM @ParameterTable)          
end            
          
if (@PassengerRequestorID is not null)            
begin            
delete from @ParameterTable
INSERT INTO @ParameterTable (PrimaryID) EXEC spSplitText @PassengerRequestorID
Update Passenger set ClientID = @ClientID WHERE PassengerRequestorID in (SELECT PrimaryID FROM @ParameterTable)          
end            
          
if (@DepartmentID is not null)            
begin            
Update Department set ClientID = @ClientID WHERE DepartmentID in (SELECT i.items FROM dbo.Split(@DepartmentID,',') AS i)          
end             
        
if (@FlightCategoryID is not null)            
begin            
Update FlightCatagory set ClientID = @ClientID WHERE FlightCategoryID in (SELECT i.items FROM dbo.Split(@FlightCategoryID,',') AS i)          
end         
        
if (@FlightPurposeID is not null)            
begin            
Update FlightPurpose set ClientID = @ClientID WHERE FlightPurposeID in (SELECT i.items FROM dbo.Split(@FlightPurposeID,',') AS i)          
end         
        
if (@FuelLocatorID is not null)            
begin            
Update FuelLocator set ClientID = @ClientID WHERE FuelLocatorID in (SELECT i.items FROM dbo.Split(@FuelLocatorID,',') AS i)          
end          
            
END 