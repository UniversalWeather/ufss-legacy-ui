
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQMain]
(
	@CQMainID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;

	--// Delete Dependencies from Quote Table
	DECLARE @RowQuoteNumber int    
	DECLARE @QuoteID BIGINT        

	CREATE Table #TempQuote ( rownum int identity (1,1), QuoteId Bigint,  IsUpdated bit)  
	INSERT INTO #TempQuote (QuoteId, IsUpdated) 
	SELECT CQQuoteMainID,0 FROM CQQuoteMain 
		WHERE CQMainID = @CQMainID    
      
	SET @RowQuoteNumber = 1    
	WHILE (SELECT COUNT(1) FROM #TempQuote WHERE IsUpdated =0) >0    
	BEGIN    
		SELECT @QuoteID = QuoteId FROM #TempQuote WHERE rownum = @RowQuoteNumber
		EXECUTE dbo.spFlightPak_CharterQuote_DeleteCQQuoteMain @QuoteID, @CustomerID
		
		UPDATE #TempQuote SET IsUpdated = 1 WHERE rownum = @RowQuoteNumber    
		SET @RowQuoteNumber = @RowQuoteNumber +1     
	END

	--// Delete Dependencies from Invoice Table
	DECLARE @RowInvNumber int    
	DECLARE @InvoiceID BIGINT        

	CREATE Table #TempInvoice ( rownum int identity (1,1), InvoiceId Bigint,  IsUpdated bit)  
	INSERT INTO #TempInvoice (InvoiceId, IsUpdated) 
	SELECT CQInvoiceMainID,0 FROM CQInvoiceMain 
		WHERE CQMainID = @CQMainID    
      
	SET @RowInvNumber = 1    
	WHILE (SELECT COUNT(1) FROM #TempInvoice WHERE IsUpdated =0) >0    
	BEGIN    
		SELECT @InvoiceID = InvoiceId FROM #TempInvoice WHERE rownum = @RowInvNumber
		EXECUTE dbo.spFlightPak_CharterQuote_DeleteCQInvoiceMain @InvoiceID, @CustomerID
		
		UPDATE #TempInvoice SET IsUpdated = 1 WHERE rownum = @RowInvNumber    
		SET @RowInvNumber = @RowInvNumber +1     
	END

	--// Delete the Depeendencies Tables	
	DELETE FROM CQException WHERE [CQMainID] = @CQMainID AND CustomerID = @CustomerID
	DELETE FROM CQFleetChargeDetail WHERE [CQMainID] = @CQMainID AND CustomerID = @CustomerID
	DELETE FROM CQFleetCharterRate WHERE [CQMainID] = @CQMainID AND CustomerID = @CustomerID
	DELETE FROM CQHistory WHERE [CQMainID] = @CQMainID AND CustomerID = @CustomerID
	DELETE FROM FileWarehouse WHERE RecordID = @CQMainID AND CustomerID = @CustomerID

	--// Delete Dependencies from CQLeg Table
	DECLARE @Rownumber int    
	DECLARE  @LegID BIGINT        

	CREATE Table #TempLeg ( rownum int identity (1,1), LegId Bigint,  IsUpdated bit)  
	INSERT INTO #TempLeg (LegId, IsUpdated) 
	SELECT CQLegID,0 FROM CQLeg 
		WHERE CQMainID = @CQMainID    
      
	SET @Rownumber = 1    
	WHILE (SELECT COUNT(1) FROM #TempLeg WHERE IsUpdated =0) >0    
	BEGIN    
		SELECT @LegID = LegID FROM #TempLeg WHERE rownum = @Rownumber
		EXECUTE dbo.spFlightPak_CharterQuote_DeleteCQLeg @LegID, @CustomerID
		
		UPDATE #TempLeg SET IsUpdated = 1 WHERE rownum = @Rownumber    
		SET @Rownumber = @Rownumber +1     
	END
	
	DELETE FROM [CQMain] WHERE [CQMainID] = @CQMainID AND CustomerID = @CustomerID

GO
