
/****** Object:  StoredProcedure [dbo].[spFlightpak_UpdateReportDetailasDeleted]    Script Date: 08/13/2013 17:35:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_UpdateReportDetailasDeleted]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_UpdateReportDetailasDeleted]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_UpdateReportDetailasDeleted]    Script Date: 08/13/2013 17:35:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightpak_UpdateReportDetailasDeleted]
(
@CustomerID BIGINT,
@TripSheetReportHeaderID BIGINT
)
AS 

-- =============================================          
-- Author:Karthikeyan          
-- Create date: 13/08/2013          
-- Description: Update the Report Sheet Detail as Deleted         
-- ============================================= 

BEGIN

UPDATE TripSheetReportDetail SET IsSelected = 0 WHERE CustomerID = @CustomerID AND TripSheetReportHeaderID = @TripSheetReportHeaderID AND IsDeleted = 0
 
 END
GO

