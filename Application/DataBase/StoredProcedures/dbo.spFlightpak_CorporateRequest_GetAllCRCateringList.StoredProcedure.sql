/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRCateringList]    Script Date: 04/25/2013 17:49:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRCateringList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRCateringList]
(@CustomerID BIGINT
,@CRLegID BIGINT
,@CRCateringListID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRCateringList.CRCateringListID
			,CRCateringList.CustomerID
			,CRCateringList.CRLegID
			,CRCateringList.TripNUM
			,CRCateringList.LegID
			,CRCateringList.RecordType
			,CRCateringList.CateringID
			,CRCateringList.CRCateringListDescription
			,CRCateringList.PhoneNUM
			,CRCateringList.IsCompleted
			,CRCateringList.AirportID
			,CRCateringList.LastUpdUID
			,CRCateringList.LastUpdTS
			,CRCateringList.IsDeleted
			,CRCateringList.FaxNum as FaxNumber
			,CRCateringList.Email  as BusinessEmail	
			,CRCateringList.Rate
			,Catering.CateringCD
			,Catering.ContactName
			,Catering.PhoneNum
			,Catering.FaxNum
			,Catering.ContactEmail
			,Catering.NegotiatedRate
	FROM CRCateringList
	LEFT OUTER JOIN Catering ON Catering.CateringID = CRCateringList.CateringID
	WHERE CRCateringList.CustomerID = @CustomerID
	AND CRCateringList.CRLegID = @CRLegID
	AND CRCateringList.IsDeleted = 0
	AND CRCateringList.CRCateringListID =	CASE WHEN (@CRCateringListID <> -1)
						THEN (@CRCateringListID)
						ELSE (SELECT CRCateringListID FROM CRCateringList 
								WHERE CustomerID = @CustomerID
										AND CRLegID = @CRLegID
										AND IsDeleted = 0)
						END
END
GO