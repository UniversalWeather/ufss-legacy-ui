

/****** Object:  StoredProcedure [dbo].[spGetDepartmentForClient]    Script Date: 05/07/2013 12:09:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDepartmentForClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetDepartmentForClient]
GO



/****** Object:  StoredProcedure [dbo].[spGetDepartmentForClient]    Script Date: 05/07/2013 12:09:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDepartmentForClient]  
(@CustomerID BIGINT  
,@ClientID BIGINT)  
AS BEGIN  
 SET NOCOUNT ON  
 IF (@ClientID > 0)  
 BEGIN  
  SELECT DepartmentID,  
    DepartmentCD,  
    DepartmentName,  
    CustomerID,  
    ClientID,  
    IsDeleted,
    DepartPercentage  
  FROM Department  
  WHERE CustomerID = @CustomerID AND  
    ISNULL(IsDeleted,0) = 0 AND   
    ClientID=@ClientID  
  ORDER BY DepartmentCD ASC  
 END    
 ELSE    
 BEGIN    
  SELECT DepartmentID,  
    DepartmentCD,  
    DepartmentName,  
    CustomerID,  
    ClientID,  
    IsDeleted,
    DepartPercentage  
  FROM Department  
  WHERE CustomerID = @CustomerID AND  
    ISNULL(IsDeleted,0) = 0   
  ORDER BY DepartmentCD ASC  
 END  
END
GO


