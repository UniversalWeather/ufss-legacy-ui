
/****** Object:  StoredProcedure [dbo].[spAddPassengerInformation]     ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPassengerInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPassengerInformation]
GO



/****** Object:  StoredProcedure [dbo].[spAddPassengerInformation]     ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spAddPassengerInformation] (    
 @CustomerID BIGINT    
 ,@PassengerInfoCD CHAR(3)    
 ,@PassengerDescription VARCHAR(25)    
 ,@ClientID BIGINT    
 ,@LastUpdUID VARCHAR(30)    
 ,@LastUpdTS DATETIME    
 ,@IsShowOnTrip BIT    
 ,@IsDeleted BIT 
 ,@IsCheckList BIT
 )    
AS    
BEGIN    
 SET NOCOUNT ON    
    
 DECLARE @PassengerInformationID BIGINT    
    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID    
  ,'MasterModuleCurrentNo'    
  ,@PassengerInformationID OUTPUT    
    
  SET @LastUpdTS = GETUTCDATE()  
    
 INSERT INTO [PassengerInformation] (    
  PassengerInformationID    
  ,CustomerID    
  ,ClientID    
  ,PassengerInfoCD    
  ,PassengerDescription    
  ,LastUpdUID    
  ,LastUpdTS    
  ,IsShowOnTrip    
  ,IsDeleted
  ,IsCheckList    
  )    
 VALUES (    
  @PassengerInformationID    
  ,@CustomerID    
  ,@ClientID    
  ,@PassengerInfoCD    
  ,@PassengerDescription    
  ,@LastUpdUID    
  ,@LastUpdTS    
  ,@IsShowOnTrip    
  ,@IsDeleted 
  ,@IsCheckList   
  )    
END  