

/****** Object:  StoredProcedure [dbo].[spGetAllCrewAircraftAssign]    Script Date: 28/01/2015 17:30:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewAircraftAssign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewAircraftAssign]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllCrewAircraftAssign]    Script Date: 28/01/2015 17:30:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllCrewAircraftAssign]      
(@CustomerID bigint,      
 @CrewID bigint)      
 as begin       
       
 SELECT Fleet.FleetID      
  ,Fleet.TailNum      
  ,Aircraft.AircraftCD  as AircraftCD    
  ,Fleet.TypeDescription      
  ,CREWASSIGN.CrewID      
  ,isnull(CREWASSIGN.CrewAircraftAssignedID,null) as CrewAircraftAssignedID      
FROM Fleet  
LEFT OUTER JOIN Aircraft as Aircraft on Aircraft.AircraftID = Fleet.AircraftID     
LEFT OUTER JOIN CrewAircraftAssigned as CREWASSIGN ON(CREWASSIGN.IsDeleted = 0 AND CREWASSIGN.FleetID = Fleet.FleetID AND CREWASSIGN.CrewID =@CrewID and CREWASSIGN.CustomerID=@CustomerID)      
where Fleet.customerid = @CustomerID and Fleet.IsDeleted = 0 and Fleet.IsInActive = 0    
order by Fleet.TailNum    
      
end 

GO


