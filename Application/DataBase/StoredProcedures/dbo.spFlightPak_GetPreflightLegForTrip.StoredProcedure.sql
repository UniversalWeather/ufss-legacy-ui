/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPreflightLegForTrip]    Script Date: 01/22/2013 19:42:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetPreflightLegForTrip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetPreflightLegForTrip]
GO


CREATE PROCEDURE [dbo].[spFlightPak_GetPreflightLegForTrip]       
(     
       
 @TripID  BIGINT,      
 @CustomerID BIGINT      
 )        
AS        
SET NOCOUNT ON       
BEGIN        
SELECT [LegID]    
           ,[TripID]    
           ,[CustomerID]    
           ,[TripNUM]    
           ,[LegNUM]    
           ,[DepartICAOID]    
           ,[ArriveICAOID]    
           ,[Distance]    
           ,[PowerSetting]    
           ,[TakeoffBIAS]    
           ,[LandingBias]    
           ,[TrueAirSpeed]    
           ,[WindsBoeingTable]    
           ,[ElapseTM]    
           ,[IsDepartureConfirmed]    
           ,[IsArrivalConfirmation]    
           ,[IsApproxTM]    
           ,[IsScheduledServices]    
           ,[DepartureDTTMLocal]    
           ,[ArrivalDTTMLocal]    
           ,[DepartureGreenwichDTTM]    
           ,[ArrivalGreenwichDTTM]    
           ,[HomeDepartureDTTM]    
           ,[HomeArrivalDTTM]    
           ,[GoTime]    
           ,[FBOID]    
           ,[LogBreak]    
           ,[FlightPurpose]    
           ,[PassengerRequestorID]    
           ,[RequestorName]    
           ,[DepartmentID]    
           ,[DepartmentName]    
           ,[AuthorizationID]    
           ,[AuthorizationName]    
           ,[PassengerTotal]    
           ,[SeatTotal]    
           ,[ReservationTotal]    
           ,[ReservationAvailable]    
           ,[WaitNUM]    
           ,[DutyTYPE]    
           ,[DutyHours]    
           ,[RestHours]    
           ,[FlightHours]    
           ,[Notes]    
           ,[FuelLoad]    
           ,[OutbountInstruction]    
           ,[DutyTYPE1]    
           ,[IsCrewDiscount]    
           ,[CrewDutyRulesID]    
           ,[IsDutyEnd]    
           ,[FedAviationRegNUM]    
           ,[ClientID]    
           ,[LegID1]    
           ,[WindReliability]    
           ,[OverrideValue]    
           ,[CheckGroup]    
           ,[AdditionalCrew]    
           ,[PilotInCommand]    
           ,[SecondInCommand]    
           ,[NextLocalDTTM]    
           ,[NextGMTDTTM]    
           ,[NextHomeDTTM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[CrewNotes]    
           ,[IsPrivate]    
           ,[WaitList]    
           ,[FlightNUM]    
           ,[FlightCost]    
           ,[CrewFuelLoad]    
           ,[IsDeleted]    
           ,[UWAID]    
           ,[USCrossing]    
           ,[Response]    
           ,[ConfirmID]    
           ,[CrewDutyAlert]    
           ,[AccountID]  
           ,[FlightCategoryID]
           ,[LastUpdTSCrewNotes]
           ,[LastUpdTSPaxNotes]        
       FROM PreflightLeg      
       WHERE CustomerID=@CustomerID     
       AND TripID=@TripID        
       END  
GO


