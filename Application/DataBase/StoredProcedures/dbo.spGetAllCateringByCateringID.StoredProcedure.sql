

/****** Object:  StoredProcedure [dbo].[spGetAllCateringByCateringID]    Script Date: 01/24/2013 14:35:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCateringByCateringID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCateringByCateringID]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllCateringByCateringID]    Script Date: 01/24/2013 14:35:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE Procedure [dbo].[spGetAllCateringByCateringID]( @CateringID BIGINT)      
  as  
-- =============================================      
-- Author:Sujitha V    
-- Create date: 11/08/2012      
-- Description: Get the Catering information for an Catering      
-- =============================================      
   
 SELECT          
 a.[CateringID]      
 ,a.[CustomerID]       
          ,a.[AirportID]       
          ,a.[CateringCD]       
          ,a.[IsChoice]       
          ,a.[CateringVendor]        
          ,a.[PhoneNum]       
          ,a.[FaxNum]       
          ,a.[ContactName]       
          ,a.[Remarks]       
          ,a.[NegotiatedRate]       
          ,a.[LastUpdUID]       
          ,a.[LastUpdTS]       
          ,a.[UpdateDT]       
          ,a.[RecordType]       
          ,a.[SourceID]       
          ,a.[ControlNum]       
          ,a.[IsInActive]       
          ,a.[TollFreePhoneNum]       
          ,a.[WebSite]       
          ,(SELECT CAST(CASE WHEN a.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter    
          ,a.[UWAMaintFlag]    
          ,a.[UWAID]       
          ,a.[IsDeleted]      
          ,a.[UWAUpdates]      
          ,a.[BusinessEmail]      
          ,a.[ContactBusinessPhone]      
          ,a.[CellPhoneNum]      
          ,a.[ContactEmail]      
          ,a.[Addr1]      
          ,a.[Addr2]      
          ,a.[Addr3]      
          ,a.[City]      
          ,a.[StateProvince]       
          ,a.[MetroID]      
          ,a.[CountryID]      
           ,M.MetroCD      
              ,C.CountryCD   
                   ,a.ExchangeRateID    
     ,a.NegotiatedTerms    
     ,a.SundayWorkHours    
     ,a.MondayWorkHours    
     ,a.TuesdayWorkHours    
     ,a.WednesdayWorkHours    
     ,a.ThursdayWorkHours    
     ,a.FridayWorkHours    
     ,a.SaturdayWorkHours           
 FROM  [Catering] a LEFT OUTER JOIN [metro] M on a.MetroID = M.MetroID          
 LEFT OUTER JOIN [Country] C on a.CountryID = C.CountryID      
 --inner join [Airport] Ai on A.AirportID = Ai.AirportID       
       
 WHERE   
    CateringID = @CateringID     
   
  
GO


