
GO
/****** Object:  StoredProcedure [dbo].[spAddItineraryPlan]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddItineraryPlan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddItineraryPlan]
GO

CREATE PROCEDURE spAddItineraryPlan
(@CustomerID	BIGINT
,@IntinearyNUM	INT
,@ItineraryPlanQuarter	INT
,@ItineraryPlanQuarterYear	VARCHAR(4)
,@AircraftID	BIGINT
,@FleetID	BIGINT
,@ItineraryPlanDescription	VARCHAR(40)
,@ClientID	BIGINT
,@HomebaseID	BIGINT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	DECLARE @ItineraryPlanID	bigint
	SET @LastUpdTS = GETUTCDATE()
	
	SELECT @IntinearyNUM = (ISNULL(MAX(IntinearyNUM),0) + 1) FROM ItineraryPlan WHERE CustomerID=@CustomerID
	
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'UtilityCurrentNo',  @ItineraryPlanID OUTPUT
	
	INSERT INTO ItineraryPlan
           (ItineraryPlanID 
           ,CustomerID 
           ,IntinearyNUM 
           ,ItineraryPlanQuarter 
           ,ItineraryPlanQuarterYear 
           ,AircraftID 
           ,FleetID 
           ,ItineraryPlanDescription 
           ,ClientID 
           ,HomebaseID 
           ,LastUpdUID 
           ,LastUpdTS 
           ,IsDeleted)
     VALUES
           (@ItineraryPlanID
           ,@CustomerID
           ,@IntinearyNUM
           ,@ItineraryPlanQuarter
           ,@ItineraryPlanQuarterYear
           ,@AircraftID
           ,@FleetID
           ,@ItineraryPlanDescription
           ,@ClientID
           ,@HomebaseID
           ,@LastUpdUID
           ,@LastUpdTS
           ,@IsDeleted)
	
	SELECT @ItineraryPlanID AS ItineraryPlanID

END