/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRCateringList]    Script Date: 04/25/2013 17:44:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_AddCRCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRCateringList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRCateringList]
(--@CRCateringListID BIGINT,
@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@CateringID BIGINT
,@CRCateringListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@FaxNum VARCHAR(25)
,@Email VARCHAR(250)
,@Rate numeric(6, 2)
)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
	DECLARE @CRCateringListID BIGINT
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRCateringListID OUTPUT
		
	INSERT INTO CRCateringList
		([CRCateringListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[CateringID]
		,[CRCateringListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted]
		,[FaxNum]
		,[Email]
		,[Rate]
		)
	VALUES
		(@CRCateringListID
		,@CustomerID
		,@CRLegID
		,@TripNUM
		,@LegID
		,@RecordType
		,@CateringID
		,@CRCateringListDescription
		,@PhoneNUM
		,@IsCompleted
		,@AirportID
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted
		,@FaxNum
		,@Email
		,@Rate
		)
		
	SELECT @CRCateringListID AS CRCateringListID
	
END
GO