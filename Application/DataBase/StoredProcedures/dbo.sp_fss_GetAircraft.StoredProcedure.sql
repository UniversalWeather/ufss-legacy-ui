/****** Object:  StoredProcedure [dbo].[sp_fss_GetAircraft]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAircraft]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAircraft]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetAircraft]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE Procedure [dbo].[sp_fss_GetAircraft](@CustomerID  BIGINT,      
  @AircraftCD varchar(10),      
  @AircraftID BIGINT,      
  @FetchInactiveOnly BIT=0      
  )       
as        
  -- =============================================                
  -- Author: Karthikeyan.S      
  -- Create date: 10/03/2014                
  -- Description: Get Aircraft Informations          
  -- EXEC [sp_fss_GetAircraft] 10003,'',0,1  
  -- EXEC [sp_fss_GetAircraft] 10003,'',0,0  
  -- EXEC [sp_fss_GetAircraft] 10003,'',10003181320,1  
  -- EXEC [sp_fss_GetAircraft] 10003,'003',0,0  
  -- =============================================        
set nocount on        
        
begin         
SELECT          
        
AircraftID,        
CustomerID,        
CASE WHEN AircraftCD IS NULL THEN '' ELSE LTRIM(RTRIM(AircraftCD)) END AS AircraftCD,        
  AircraftDescription,       
   isnull(AircraftDescription,'') + ', ( ' + ISNULL(AircraftCD,'') + ' )' as 'BindingDesc',      
 -- isnull(AircraftCD,'') + ', ' + ISNULL(AircraftDescription,'') as 'BindingDesc',         
  ChargeRate ,        
  ChargeUnit ,        
  PowerSetting ,        
  PowerDescription ,        
  WindAltitude ,        
  PowerSettings1Description ,        
  PowerSettings1TrueAirSpeed ,        
  PowerSettings1HourRange ,        
  PowerSettings1TakeOffBias ,        
  PowerSettings1LandingBias ,        
  PowerSettings2Description ,        
  PowerSettings2TrueAirSpeed ,        
  PowerSettings2HourRange ,        
  PowerSettings2TakeOffBias ,        
  PowerSettings2LandingBias ,        
  PowerSettings3Description ,        
  PowerSettings3TrueAirSpeed ,        
  PowerSettings3HourRange ,        
  PowerSettings3TakeOffBias ,        
  PowerSettings3LandingBias ,        
  IsFixedRotary ,        
  ClientID ,        
  CQChargeRate ,        
  CQChargeUnit ,        
  PositionRate ,        
  PostionUnit ,        
  StandardCrew ,        
  StandardCrewRON ,        
  AdditionalCrew ,        
  AdditionalCrewRON ,        
  CharterQuoteWaitTM ,        
  LandingFee ,        
  CostBy ,        
  FixedCost ,        
  PercentageCost ,        
  TaxRON ,        
  AdditionalCrewTax ,        
  IsCharterQuoteWaitTax ,        
  LandingTax ,        
  DescriptionRON ,        
  AdditionalCrewDescription ,        
  MinDailyREQ ,        
  LastUpdUID ,        
  LastUpdTS  ,        
  AircraftSize ,        
  AircraftTypeCD ,        
  IntlStdCrewNum ,        
  DomesticStdCrewNum ,        
  MinimumDayUseHrs ,        
  DailyUsageAdjTax ,        
  LandingFeeTax ,        
  ISNULL(IsDeleted,0) IsDeleted,      
  ISNULL(IsInActive,0) IsInActive,    
  MarginalPercentage        
  FROM  [Aircraft] WHERE CustomerID=@CustomerID   
  AND ISNULL(IsDeleted,0) = 0        
  AND AircraftID = case when @AircraftID <>0 then @AircraftID else AircraftID end       
  AND AircraftCD = case when Ltrim(Rtrim(@AircraftCD)) <> '' then @AircraftCD else AircraftCD end      
  AND ISNULL(IsInActive,0) = case when @FetchInactiveOnly =1 then @FetchInactiveOnly else  ISNULL(IsInActive,0) end       
order by AircraftCD        
end        
GO

