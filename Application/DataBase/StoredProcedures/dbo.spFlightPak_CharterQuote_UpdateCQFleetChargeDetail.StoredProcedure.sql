/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQFleetChargeDetail]    Script Date: 02/25/2013 15:06:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQFleetChargeDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFleetChargeDetail]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQFleetChargeDetail]    Script Date: 02/25/2013 15:06:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFleetChargeDetail]
(
	@CQFleetChargeDetailID bigint,
	@CustomerID bigint,
	@CQMainID bigint,
	@FileNUM int,
	@QuoteID int,
	@AccountID bigint,
	@AircraftCharterRateID bigint,
	@FleetCharterRateID bigint,
	@CQCustomerCharterRateID bigint,
	@OrderNUM int,
	@CQFlightChargeDescription varchar(40),
	@ChargeUnit varchar(25),
	@NegotiatedChgUnit numeric(2, 0),
	@Quantity numeric(17, 3),
	@BuyDOM numeric(17, 2),
	@SellDOM numeric(17, 2),
	@IsTaxDOM bit,
	@IsDiscountDOM bit,
	@BuyIntl numeric(17, 2),
	@SellIntl numeric(17, 2),
	@IsTaxIntl bit,
	@IsDiscountIntl bit,
	@StandardCrewDOM numeric(2, 0),
	@StandardCrewINTL numeric(2, 0),
	@MinimumDailyRequirement numeric(17, 2),
	@YearMade char(4),
	@ExteriorColor varchar(25),
	@ColorIntl varchar(25),
	@IsAFIS bit,
	@IsUWAData bit,
	@AircraftFlightChg1 varchar(32),
	@AircraftFlightChg2 varchar(32),
	@BuyAircraftFlightIntl varchar(32),
	@FeeAMT numeric(17, 2),
	--@FeeGroupID bigint,
	@SellAircraftFlight varchar(32),
	@IsToPrint bit,
	--@IsDailyTaxAdj bit,
	--@IsLandingFeeTax bit,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
	
)
AS
	SET NOCOUNT ON;
UPDATE [CQFleetChargeDetail] SET  [FileNUM] = @FileNUM, [QuoteID] = @QuoteID, [AccountID] = @AccountID, [AircraftCharterRateID] = @AircraftCharterRateID, [FleetCharterRateID] = @FleetCharterRateID, [CQCustomerCharterRateID] = @CQCustomerCharterRateID, [OrderNUM] = @OrderNUM, [CQFlightChargeDescription] = @CQFlightChargeDescription, [ChargeUnit] = @ChargeUnit, [NegotiatedChgUnit] = @NegotiatedChgUnit, [Quantity] = @Quantity, [BuyDOM] = @BuyDOM, [SellDOM] = @SellDOM, [IsTaxDOM] = @IsTaxDOM, [IsDiscountDOM] = @IsDiscountDOM, [BuyIntl] = @BuyIntl, [SellIntl] = @SellIntl, [IsTaxIntl] = @IsTaxIntl, [IsDiscountIntl] = @IsDiscountIntl, [StandardCrewDOM] = @StandardCrewDOM, [StandardCrewINTL] = @StandardCrewINTL, [MinimumDailyRequirement] = @MinimumDailyRequirement, [YearMade] = @YearMade, [ExteriorColor] = @ExteriorColor, [ColorIntl] = @ColorIntl, [IsAFIS] = @IsAFIS, [IsUWAData] = @IsUWAData, [AircraftFlightChg1] = @AircraftFlightChg1, [AircraftFlightChg2] = @AircraftFlightChg2, [BuyAircraftFlightIntl] = @BuyAircraftFlightIntl, [FeeAMT] = @FeeAMT, 
--[FeeGroupID] = @FeeGroupID, 
[SellAircraftFlight] = @SellAircraftFlight, [IsToPrint] = @IsToPrint,
-- [IsDailyTaxAdj] = @IsDailyTaxAdj, [IsLandingFeeTax] = @IsLandingFeeTax,
 [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted
WHERE [CQFleetChargeDetailID] = @CQFleetChargeDetailID AND [CustomerID] = @CustomerID AND [CQMainID] = @CQMainID

GO


