
/****** Object:  StoredProcedure [dbo].[sp_fss_GetMetroCity]    Script Date: 02/28/2014 13:23:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetMetroCity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetMetroCity]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetMetroCity]    Script Date: 02/28/2014 13:23:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE Procedure [dbo].[sp_fss_GetMetroCity]    
(    
    @CustomerID BIGINT  
    ,@MetroID BIGINT  
    ,@MetroCD varchar(3)    
 --,@FetchActiveOnly       
)    
AS   
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get All MetroCity details for Popup        
-- Exec sp_fss_GetMetroCity 10099,0,''  
-- Exec sp_fss_GetMetroCity 10099, 10099206,NULL  
-- Exec sp_fss_GetMetroCity 10099, 0, 'AAM'  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
 SELECT   
     M.CustomerID  
    ,M.MetroID    
    ,M.MetroCD   
    ,M.MetroName    
    ,M.CountryID    
    ,M.StateName    
    ,M.LastUpdUID    
    ,M.LastUpdTS    
    ,ISNULL(M.IsDeleted,0) IsDeleted       
    ,ISNULL(M.IsInactive,0) IsInactive   
  FROM Metro M   
  WHERE M.CustomerID = @CustomerID   
  AND M.MetroID = case when @MetroID <>0 then @MetroID else M.MetroID end   
  AND M.MetroCD = case when Ltrim(Rtrim(@MetroCD)) <> '' then @MetroCD else M.MetroCD end                                    
  --AND ISNULL(M.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(M.IsInActive,0) else 0 end  
  AND ISNULL(M.IsDeleted,0) = 0                                  
 ORDER BY M.MetroCD ASC          
END  
  
  
  
  
GO

