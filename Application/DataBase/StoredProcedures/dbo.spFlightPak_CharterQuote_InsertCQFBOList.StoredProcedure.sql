

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQFBOList]    Script Date: 03/07/2013 17:40:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFBOList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQFBOList]    Script Date: 03/07/2013 17:40:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFBOList]
(
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@FBOID bigint,
	@CQFBOListDescription varchar(60),
	@PhoneNUM varchar(25),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	DECLARE @CQFBOListID BIGINT
	SET @LastUpdTS = GETUTCDATE()	
	
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQFBOListID OUTPUT 
	
	INSERT INTO 
	[CQFBOList] ([CQFBOListID], [CustomerID], [CQLegID], [QuoteID], [LegID], [RecordType], [FBOID], [CQFBOListDescription], [PhoneNUM], [AirportID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted],[FaxNUM],[Email]) 
	VALUES 
	(@CQFBOListID, @CustomerID, @CQLegID, @QuoteID, @LegID, @RecordType, @FBOID, @CQFBOListDescription, @PhoneNUM, @AirportID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@FaxNUM, @Email);
	
	SELECT @CQFBOListID AS CQFBOListID


GO


