
/****** Object:  StoredProcedure [dbo].[spAddFeeGroup]    Script Date: 02/15/2013 19:11:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateSequenceForTrialCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateSequenceForTrialCustomer]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateSequenceForTrialCustomer]    Script Date: 02/15/2013 19:11:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE PROC [dbo].[spUpdateSequenceForTrialCustomer]
@CustomerId	bigint,
@RowsToBeUpdated bigint
AS
BEGIN
	UPDATE FlightpakSequence 
		Set 
			MasterModuleCurrentNo = @RowsToBeUpdated 
	WHERE CustomerID = @CustomerId
END      

GO


