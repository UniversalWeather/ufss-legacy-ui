IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_UpdatePassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_UpdatePassenger]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Postflight_UpdatePassenger]
(
	@PostflightPassengerListID bigint,
	@POLogID bigint,
	@POLegID bigint,
	@CustomerID bigint,
	@PassengerID bigint,
	@PassengerFirstName varchar(60),
	@PassengerMiddleName varchar(60),
	@PassengerLastName varchar(60),
	@FlightPurposeID bigint,
	@IsNonPassenger bit,
	@PassportNUM varchar(25),
	@Billing varchar(25),
	@OrderNUM int,
	@LastUpdUID char(30),
	@LastUpdTS datetime,
	@WaitList char(1),
	@IsDeleted bit,
	@DepartPercentage NUMERIC(7,2)
)
AS
BEGIN

-- =============================================
-- 06-05-2013 - Added DepartPercentage column for BRM Item No. 18
-- =============================================

	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [PostflightPassenger] SET [POLogID] = @POLogID, [POLegID] = @POLegID, [PassengerID] = @PassengerID, [PassengerFirstName] = @PassengerFirstName, [PassengerMiddleName] = @PassengerMiddleName, [PassengerLastName] = @PassengerLastName, [FlightPurposeID] = @FlightPurposeID, [IsNonPassenger] = @IsNonPassenger, [PassportNUM] = @PassportNUM, [Billing] = @Billing, [OrderNUM] = @OrderNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [WaitList] = @WaitList, [IsDeleted] = @IsDeleted 
			, DepartPercentage = @DepartPercentage 
	WHERE [PostflightPassengerListID] = @PostflightPassengerListID AND [CustomerID] = @CustomerID
END

GO


