

/****** Object:  StoredProcedure [dbo].[spGetFleetByTailNumber]    Script Date: 10/09/2013 15:06:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetByTailNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetByTailNumber]
GO



/****** Object:  StoredProcedure [dbo].[spGetFleetByTailNumber]    Script Date: 10/09/2013 15:06:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[spGetFleetByTailNumber]     
(    
 @CustomerID  BIGINT,    
 @TailNum varchar(9),  
 @ClientID BIGINT    
 )    
AS        
BEGIN        
-- =============================================        
-- Author: Hamsha  
-- Create date: 09/11/2012       
-- Description: To get the Department record based on DepartmentCD  
-- =============================================       
  
 IF(@ClientID>0)        
   BEGIN     
 SELECT  FleetID  
 , TailNum  
 , A.CustomerID  
 , A.ClientID  
 , A.IsDeleted  
 , A.AircraftID  
 , A.AircraftCD  
 , A.AircraftDescription  
           
   FROM Fleet FC  
   JOIN Aircraft A ON  A.AircraftID = FC.AircraftID  
    WHERE   FC.CustomerID = @CustomerID  
    AND FC.ClientID  = @ClientID  
    AND FC.IsDeleted != 1 AND  A.IsDeleted != 1  
    AND TailNum = @TailNum  
      
 END  
ELSE  
 BEGIN  
 SELECT  FleetID  
 , TailNum  
 , A.CustomerID  
 , A.ClientID  
 , A.IsDeleted  
 , A.AircraftID  
 , A.AircraftCD  
 , A.AircraftDescription  
         
                 
   FROM Fleet FC  
   JOIN Aircraft A ON  A.AircraftID = FC.AircraftID  
    WHERE   FC.CustomerID = @CustomerID  
    and FC.IsDeleted != 1 and  A.IsDeleted != 1  
    and TailNum = @TailNum  
      
 END  
END  
GO


