
/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRPassenger]    Script Date: 02/21/2013 18:21:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_AddCRPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRPassenger]
GO



/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRPassenger]    Script Date: 02/21/2013 18:21:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRPassenger]
(--@CRPassengerID BIGINT,
@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@OrderNUM INT
,@PassengerRequestorID BIGINT
,@PassengerName VARCHAR(63)
,@FlightPurposeID BIGINT
,@PassportID BIGINT
,@Billing VARCHAR(25)
,@IsNonPassenger BIT
,@IsBlocked BIT
,@ReservationNUM VARCHAR(6)
,@WaitList VARCHAR(1)
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
	DECLARE @CRPassengerID BIGINT
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPassengerID OUTPUT
		
	INSERT INTO CRPassenger
		(CRPassengerID
		,CustomerID
		,CRLegID
		,TripNUM
		,LegID
		,OrderNUM
		,PassengerRequestorID
		,PassengerName
		,FlightPurposeID
		,PassportID
		,Billing
		,IsNonPassenger
		,IsBlocked
		,ReservationNUM
		,WaitList
		,LastUpdUID
		,LastUpdTS
		,IsDeleted)
	VALUES
		(@CRPassengerID
		,@CustomerID
		,@CRLegID
		,@TripNUM
		,@LegID
		,@OrderNUM
		,@PassengerRequestorID
		,@PassengerName
		,@FlightPurposeID
		,@PassportID
		,@Billing
		,@IsNonPassenger
		,@IsBlocked
		,@ReservationNUM
		,@WaitList
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted)
		
	SELECT @CRPassengerID AS CRPassengerID
	
END
GO


