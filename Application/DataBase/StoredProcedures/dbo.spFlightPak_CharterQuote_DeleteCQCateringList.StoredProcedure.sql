IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQCateringList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQCateringList]
(
	@CQCateringListID bigint,
	@CustomerID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime
)
AS
	SET NOCOUNT ON;
DELETE FROM [CQCateringList] WHERE [CQCateringListID] = @CQCateringListID and CustomerID = @CustomerID
GO
