IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllDelayType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllDelayType]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllDelayType]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllDelayType](@CustomerID bigint)
as
-- =============================================
-- Author:Sujitha.V
-- Create date: 12/4/2012
-- Description: Get the Delay Type information
-- =============================================
set nocount on
if len(@CustomerID) >0
begin 
	SELECT  DelayTypeID 
			,CustomerID 
			,DelayTypeCD
			,DelayTypeDescription 
			,ClientID 
			,LastUpdUID 
			,LastUpdTS 
			,IsSiflExcluded 
			,IsDeleted
			,IsInActive     
	FROM  [DelayType] 
	WHERE CustomerID=@CustomerID  and IsDeleted = 'false' 
	ORDER BY DelayTypeCD asc

END
GO
