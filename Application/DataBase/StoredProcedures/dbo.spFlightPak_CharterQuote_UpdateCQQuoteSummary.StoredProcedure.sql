IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQQuoteSummary]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQQuoteSummary
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQQuoteSummary
(
	@CQQuoteSummaryID bigint,
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@IsPrintUsageAdj bit,
	@UsageAdjDescription varchar(60),
	@CustomUsageAdj numeric(17, 2),
	@IsPrintAdditonalFees bit,
	@AdditionalFeeDescription varchar(60),
	@AdditionalFeesTax numeric(17, 2),
	@PositioningDescriptionINTL numeric(17, 2),
	@IsAdditionalFees numeric(17, 2),
	@CustomAdditionalFees numeric(17, 2),
	@IsPrintLandingFees bit,
	@LandingFeeDescription varchar(60),
	@CustomLandingFee numeric(17, 2),
	@IsPrintSegmentFee bit,
	@SegmentFeeDescription varchar(60),
	@CustomSegmentFee numeric(17, 2),
	@IsPrintStdCrew bit,
	@StdCrewROMDescription varchar(60),
	@CustomStdCrewRON numeric(17, 2),
	@IsPrintAdditionalCrew bit,
	@AdditionalCrewDescription varchar(60),
	@CustomAdditionalCrew numeric(17, 2),
	@IsAdditionalCrewRON bit,
	@AdditionalDescriptionCrewRON varchar(60),
	@CustomAdditionalCrewRON numeric(17, 2),
	@IsPrintWaitingChg bit,
	@WaitChgDescription varchar(60),
	@CustomWaitingChg numeric(17, 2),
	@IsPrintFlightChg bit,
	@FlightChgDescription varchar(60),
	@CustomFlightChg numeric(17, 2),
	@IsPrintSubtotal bit,
	@SubtotalDescription varchar(60),
	@IsPrintDiscountAmt bit,
	@DescriptionDiscountAmt varchar(60),
	@IsPrintTaxes bit,
	@TaxesDescription varchar(60),
	@IsPrintInvoice bit,
	@InvoiceDescription varchar(60),
	@ReportQuote numeric(17, 2),
	@IsPrintPay bit,
	@PrepayDescription varchar(60),
	@IsPrintRemaingAMT bit,
	@RemainingAmtDescription varchar(60),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
UPDATE [CQQuoteSummary] SET [IsPrintUsageAdj] = @IsPrintUsageAdj, [UsageAdjDescription] = @UsageAdjDescription, [CustomUsageAdj] = @CustomUsageAdj, [IsPrintAdditonalFees] = @IsPrintAdditonalFees, [AdditionalFeeDescription] = @AdditionalFeeDescription, [AdditionalFeesTax] = @AdditionalFeesTax, [PositioningDescriptionINTL] = @PositioningDescriptionINTL, [IsAdditionalFees] = @IsAdditionalFees, [CustomAdditionalFees] = @CustomAdditionalFees, [IsPrintLandingFees] = @IsPrintLandingFees, [LandingFeeDescription] = @LandingFeeDescription, [CustomLandingFee] = @CustomLandingFee, [IsPrintSegmentFee] = @IsPrintSegmentFee, [SegmentFeeDescription] = @SegmentFeeDescription, [CustomSegmentFee] = @CustomSegmentFee, [IsPrintStdCrew] = @IsPrintStdCrew, [StdCrewROMDescription] = @StdCrewROMDescription, [CustomStdCrewRON] = @CustomStdCrewRON, [IsPrintAdditionalCrew] = @IsPrintAdditionalCrew, [AdditionalCrewDescription] = @AdditionalCrewDescription, [CustomAdditionalCrew] = @CustomAdditionalCrew, [IsAdditionalCrewRON] = @IsAdditionalCrewRON, [AdditionalDescriptionCrewRON] = @AdditionalDescriptionCrewRON, [CustomAdditionalCrewRON] = @CustomAdditionalCrewRON, [IsPrintWaitingChg] = @IsPrintWaitingChg, [WaitChgDescription] = @WaitChgDescription, [CustomWaitingChg] = @CustomWaitingChg, [IsPrintFlightChg] = @IsPrintFlightChg, [FlightChgDescription] = @FlightChgDescription, [CustomFlightChg] = @CustomFlightChg, [IsPrintSubtotal] = @IsPrintSubtotal, [SubtotalDescription] = @SubtotalDescription, [IsPrintDiscountAmt] = @IsPrintDiscountAmt, [DescriptionDiscountAmt] = @DescriptionDiscountAmt, [IsPrintTaxes] = @IsPrintTaxes, [TaxesDescription] = @TaxesDescription, [IsPrintInvoice] = @IsPrintInvoice, [InvoiceDescription] = @InvoiceDescription, [ReportQuote] = @ReportQuote, [IsPrintPay] = @IsPrintPay, [PrepayDescription] = @PrepayDescription, [IsPrintRemaingAMT] = @IsPrintRemaingAMT, [RemainingAmtDescription] = @RemainingAmtDescription, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted 
	WHERE [CQQuoteSummaryID] = @CQQuoteSummaryID AND [CustomerID] = @CustomerID
	
GO
