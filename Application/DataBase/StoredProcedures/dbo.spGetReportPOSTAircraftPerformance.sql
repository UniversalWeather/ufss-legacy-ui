IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftPerformance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftPerformance]
GO

/****** Object:  StoredProcedure [dbo].[spGetReportPOSTAircraftPerformance]    Script Date: 04/11/2015 16:40:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetReportPOSTAircraftPerformance]   
(   
  @UserCD AS VARCHAR(30),
  @DATEFROM AS DATETIME,
  @DATETO AS DATETIME,
  @TailNUM AS VARCHAR(5000) = '',  
  @FleetGroupCD AS VARCHAR(5000)='', 
  @sortby As NVARCHAR(1000) = '',
  @SeperatePage Bit=0 
  )       
AS   
-- ===============================================================================    
-- SPC Name: spGetReportAircraftPerformanceAnalysis    
-- Author:  P.srinivas    
-- Create date: 
-- Altered by Mathes 30-10-2012    
-- Description: Get Postflight Aircraft Performance Analysis for REPORTS    
-- Revision History    
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================    
SET NOCOUNT ON   
  Declare @DateRange Varchar(30)
  Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)                                   

 DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';   
  DECLARE @ParameterDefinition AS NVARCHAR(500)  
  
  IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
      DROP table #TmpFltTail    
  IF OBJECT_ID('tempdb..#TmpTails') is not null                        
      DROP table #TmpTails      
      
  Create Table #TmpTails (TailNum Varchar(9))      
  Create Table #TmpFltTail (TailNum Varchar(9))  
  Declare @TenToMin Int
  Declare @Units Varchar(20)=''
  Declare @DayMonth int
  Select @TenToMin=Company.TimeDisplayTenMin, 
         @DayMonth=Company.DayMonthFormat,
         @Units=Case Company.FuelPurchase When 1 Then 'Gallons' When 2 Then 'Liters' WHEN 3 THEN 'Imp Gals' WHEN 4 THEN 'Pounds' ELSE 'Kilos' End 
         From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD) And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)          
  
  
  Declare @Filter Char(1)='F'
  IF @TailNum <>''
  Begin
    INSERT INTO #TmpTails SELECT DISTINCT TailNum FROM Fleet  
    WHERE TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  AND
    CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
    AND IsDeleted=0
    AND IsInActive=0
    Set @Filter='T'
  End 
   
  IF @FleetGroupCD <>''
  Begin
   INSERT INTO #TmpTails SELECT DISTINCT TailNum 
   FROM  FleetGroupOrder INNER JOIN Fleet ON FleetGroupOrder.FleetID = Fleet.FleetID    
   INNER JOIN FleetGroup ON FleetGroup.FleetGroupID=FleetGroupOrder.FleetGroupID 
   WHERE FleetGroupOrder.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
   FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) AND
   Fleet.TailNum Not IN(SELECT DISTINCT TailNum From #TmpTails)
   AND Fleet.IsDeleted=0
   AND Fleet.IsInActive=0
   Set @Filter='T'
  End     
 
 
 IF OBJECT_ID('tempdb..#TmpRpt') is not null                        
      DROP table #TmpRpt  
      
 Create Table #TmpRpt(POLogID BigInt,
                      POLegID BigInt,
                      TailNum Varchar(10), 
                      ScheduledTM DateTime,
                      EstDepartureDT DateTime,
                      DispatchNum Varchar(12),
                      LogNum BigInt,
                      LegNUM BigInt,
                      DepartICAOID varchar(30),
                      ArrivalICAOID varchar(30),
                      FlightCatagoryCD Varchar(6),
                      SeatLeft Varchar(50) ,
                      SeatRight  Varchar(50), 
                      Distance  Numeric(5,0),
                      StatuteMiles Numeric(5,0),
                      DelayTypeCD Varchar(10),
                      DelayTM Numeric(6,3),
                      BlockHours Numeric(10,3),
					  FlightHours Numeric(10,3), 
                      FuelUsed Numeric(7,0), 
                      PassengerTotal Numeric(5,0),
                      FuelLocatorCD  Varchar(10), 
                      FuelQTY Numeric(15,2),
                      ExpenseAMT Numeric(17,2),
                      AircraftCD Varchar(6),
                      InboundDTTM  DateTime,
                      OutboundDTTM DateTime,
                      SLIPNUM BIGINT,
                      HoursAway Numeric(10,2),
                      Rnk INT,
                      Rnk1 INT
                       )   
 
SET @SQLSCRIPT ='Insert Into #TmpRpt SELECT
DISTINCT 
PFL.POLogID,
PFL.POLegID,
FT.TailNum, 
PFL.ScheduledTM,
POM.EstDepartureDT,
POM.DispatchNum,
POM.LogNum,
PFL.LegNUM,
CONVERT (VARCHAR, ADeprt.IcaoID) DepartICAOID,
CONVERT (VARCHAR, AArr.IcaoID) ArrivalICAOID,        
FC.FlightCatagoryCD,
SeatLeft=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''L'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),
SeatRight=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''R'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),  
PFL.Distance,
PFL.StatuteMiles,
DT.DelayTypeCD,
PFL.DelayTM,
ROUND(PFL.BlockHours,1) BlockHours,
ROUND(PFL.FlightHours,1) FlightHours, 
PFL.FuelUsed, 
PFL.PassengerTotal,
					--FuelLocatorCD=(SELECT Top 1   FL.FuelLocatorCD FROM  PostflightExpense PE 
					--LEFT JOIN FuelLocator FL ON PE.FuelLocatorID = FL.FuelLocatorID 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc),
FuelLocatorCD, 
					--(SELECT Top 1 DBO.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD) FROM  PostflightExpense PE 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc), 
DBO.GetFuelConversion(PFE.FuelPurchase,PFE.FuelQTY,@UserCD), 
					--( SELECT Top 1 (Case WHEN CPY.FuelPurchase <> PE.FuelPurchase  THEN  (PFE.FuelQTY*PE.UnitPrice)/NULLIF(DBO.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD),0) 
					--                                             ELSE PE.UnitPrice End) FROM  PostflightExpense PE 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc), 
PFE.ExpenseAMT,
FT.AircraftCD,
PFL.InboundDTTM,
PFL.OutboundDTTM,
SLIPNUM = SlipNUM,
0,
ROW_NUMBER()OVER(PARTITION BY PFL.POLEGID ORDER BY FT.TailNum,PFL.POLEGID) Rnk ,
ROW_NUMBER()OVER(PARTITION BY PFL.POLEGID,ISNULL(SLIPNUM,0) ORDER BY FT.TailNum,PFL.POLEGID,SLIPNUM) Rnk1

FROM  PostflightMain POM
INNER JOIN PostflightLeg PFL on PFL.POLogID = POM.POLogID AND PFL.ISDELETED=0
LEFT JOIN FlightCatagory FC On PFL.FlightCategoryID = FC.FlightCategoryID 
LEFT JOIN PostflightExpense PFE on  PFE.POLegID = PFL.POLegID AND PFE.FuelQty > 0
LEFT JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY POLogID,POLEGID ORDER BY SlipNum DESC) Rnk ,FuelLocatorCD,POLogID,POLegID FROM PostflightExpense PFE INNER JOIN FuelLocator  ON PFE.FuelLocatorID = FuelLocator.FuelLocatorID ) PE ON PE.POLogID=POM.POLogID AND RNK=1 AND PE.PoLegid=PFL.PoLegid 
LEFT JOIN Fleet FT ON FT.FleetID = POM.FleetID   
LEFT JOIN Company CPY on CPY.HomebaseID = FT.HomebaseID 
LEFT JOIN  Airport ADeprt ON ADeprt.AirportID = PFL.DepartICAOID
LEFT JOIN  Airport AArr ON AArr.AirportID = PFL.ArriveICAOID  
LEFT JOIN DelayType DT ON DT.DelayTypeID = PFL.DelayTypeID
LEFT JOIN PostflightCrew PFC on PFC.POLegID = PFL.POLegID 
WHERE PFL.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))     
+ ' AND CONVERT(DATE,PFL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND POM.ISDELETED = 0'  
  
IF @Filter='T' 
BEGIN      
 SET @SQLSCRIPT = @SQLSCRIPT + ' AND FT.TailNUM IN ( Select Distinct TailNum From #TmpTails) '    
END   
 
IF(@sortby='ScheduledTM')  
 BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY ScheduledTM'  
  END  
IF(@sortby='LogNum')  
 BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY LogNum'  
  END  
  
 SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME,@DATETO AS DATETIME,@TailNUM AS VARCHAR(5000),@FleetGroupCD AS VARCHAR(5000),@sortby As NVARCHAR(1000)'
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD,@DATEFROM,@DATETO,@TailNUM,@FleetGroupCD,@sortby   
 

 Declare @TailNm Varchar(10)    
 Declare @AircraftCD Varchar(10) 
 IF @TailNum ='' AND @FleetGroupCD =''
 Begin 
	 IF OBJECT_ID('tempdb..#Tmp') is not null                        
		  DROP table #Tmp  
	 Create Table #Tmp(TailNum Varchar(10),AircraftCD Varchar(10))  
	 INSERT INTO #Tmp SELECT DISTINCT TailNum,AircraftCD From Fleet Where CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND IsDeleted=0 AND IsInActive=0 And TailNum Not In(Select Distinct TailNum from #TmpRpt)
	 WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
	 BEGIN 
	   Set @TailNm=''
	   Set @AircraftCD=''
	   Select Top 1 @TailNm=TailNum,@AircraftCD=AircraftCD From #Tmp  
	   Insert Into #TmpRpt(TailNum,AircraftCD, DelayTM,BlockHours,FlightHours) Values(@TailNm,@AircraftCD,0,0,0)
	   Delete From #Tmp Where TailNum=@TailNm  
	 END           
	 IF OBJECT_ID('tempdb..#Tmp') is not null                        
		  DROP table #Tmp  	
 End   
 IF OBJECT_ID('tempdb..#TmpTails') is not null                        
      DROP table #TmpTails      
      	   

IF @SuppressActivityAircraft=1
 
 BEGIN
 IF @sortby='ScheduledTM'  
 BEGIN  
  Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduledTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END Distance,
        CASE WHEN Rnk=1 THEN  IsNull(StatuteMiles,0)  ELSE 0 END StatuteMiles,
        DelayTypeCD,
        CASE WHEN Rnk=1 THEN DelayTM ELSE 0 END  DelayTM,
        CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0)  ELSE 0 END BlockHours,
        CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0)  ELSE 0 END FlightHours,   
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END FuelUsed,
        CASE WHEN Rnk=1 THEN  IsNull(PassengerTotal,0)  ELSE 0 END PassengerTotal,
        FuelLocatorCD,
        CASE WHEN Rnk1=1 THEN IsNull(FuelQTY,0) ELSE 0 END FuelQTY,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END ExpenseAMT,  
        InboundDTTM,
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FltSpeed,
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END  FuelBurn,
        CASE WHEN Rnk=1 AND IsNull(BlockHours,0)<>0 THEN(CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0) ELSE 0 END) ELSE 0 END FuelBHR,         
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FuelFHR, 
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END FuelPrice, 
        HoursAway=CASE WHEN RNK=1 THEN (dbo.GetHoursAway(@UserCD,POLegID,POLogID)) ELSE 0 END,
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt 
        WHERE ScheduledTM IS NOT NULL
        order by AircraftCD,ScheduleDTTMLocal,LogNum,LegNUM
  END  
 ELSE
  BEGIN  
   Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduledTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END Distance,
        CASE WHEN Rnk=1 THEN  IsNull(StatuteMiles,0)  ELSE 0 END StatuteMiles,
        DelayTypeCD,
        CASE WHEN Rnk=1 THEN DelayTM ELSE 0 END  DelayTM,
        CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0)  ELSE 0 END BlockHours,
        CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0)  ELSE 0 END FlightHours,   
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END FuelUsed,
        CASE WHEN Rnk=1 THEN  IsNull(PassengerTotal,0)  ELSE 0 END PassengerTotal,
        FuelLocatorCD,
        CASE WHEN Rnk1=1 THEN IsNull(FuelQTY,0) ELSE 0 END FuelQTY,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END ExpenseAMT, 
        InboundDTTM,
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FltSpeed,
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END  FuelBurn,
        CASE WHEN Rnk=1 AND IsNull(BlockHours,0)<>0 THEN(CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0) ELSE 0 END) ELSE 0 END FuelBHR,         
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FuelFHR,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END FuelPrice, 
        HoursAway=CASE WHEN RNK=1 THEN (dbo.GetHoursAway(@UserCD,POLegID,POLogID)) ELSE 0 END,
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange
        From #TmpRpt 
        WHERE ScheduledTM IS NOT NULL
        order by AircraftCD, LogNum, ScheduleDTTMLocal, LegNUM
        
  END  
      	
 
 END
 
 ELSE
 
 
 BEGIN
 IF @sortby='ScheduledTM'  
 BEGIN  
 Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduledTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END Distance,
        CASE WHEN Rnk=1 THEN  IsNull(StatuteMiles,0)  ELSE 0 END StatuteMiles,
        DelayTypeCD,
        CASE WHEN Rnk=1 THEN DelayTM ELSE 0 END  DelayTM,
        CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0)  ELSE 0 END BlockHours,
        CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0)  ELSE 0 END FlightHours,   
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END FuelUsed,
        CASE WHEN Rnk=1 THEN  IsNull(PassengerTotal,0)  ELSE 0 END PassengerTotal,
        FuelLocatorCD,
        CASE WHEN Rnk1=1 THEN IsNull(FuelQTY,0) ELSE 0 END FuelQTY,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END ExpenseAMT, 
        InboundDTTM,
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FltSpeed,
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END  FuelBurn,
        CASE WHEN Rnk=1 AND IsNull(BlockHours,0)<>0 THEN(CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0) ELSE 0 END) ELSE 0 END FuelBHR,         
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FuelFHR, 
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END FuelPrice, 
        HoursAway=CASE WHEN RNK=1 THEN (dbo.GetHoursAway(@UserCD,POLegID,POLogID)) ELSE 0 END,
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt order by AircraftCD,ScheduleDTTMLocal,LogNum,LegNUM
  END  
 ELSE
  BEGIN 

    Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduledTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END Distance,
        CASE WHEN Rnk=1 THEN  IsNull(StatuteMiles,0)  ELSE 0 END StatuteMiles,
        DelayTypeCD,
        CASE WHEN Rnk=1 THEN DelayTM ELSE 0 END  DelayTM,
        CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0)  ELSE 0 END BlockHours,
        CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0)  ELSE 0 END FlightHours,   
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END FuelUsed,
        CASE WHEN Rnk=1 THEN  IsNull(PassengerTotal,0)  ELSE 0 END PassengerTotal,
        FuelLocatorCD,
        CASE WHEN Rnk1=1 THEN IsNull(FuelQTY,0) ELSE 0 END FuelQTY,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END ExpenseAMT,  
        InboundDTTM,
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(Distance,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FltSpeed,
        CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END  FuelBurn,
        CASE WHEN Rnk=1 AND IsNull(BlockHours,0)<>0 THEN(CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(BlockHours,0) ELSE 0 END) ELSE 0 END FuelBHR,         
        CASE WHEN Rnk=1 AND IsNull(FlightHours,0)<>0 THEN (CASE WHEN Rnk=1 THEN  IsNull(FuelUsed,0)  ELSE 0 END)/(CASE WHEN Rnk=1 THEN  IsNull(FlightHours,0) ELSE 0 END) ELSE 0 END FuelFHR,
        CASE WHEN Rnk1=1 THEN IsNull(ExpenseAMT,0) ELSE 0 END FuelPrice, 
        HoursAway=CASE WHEN RNK=1 THEN (dbo.GetHoursAway(@UserCD,POLegID,POLogID)) ELSE 0 END,
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt order by AircraftCD, LogNum, ScheduleDTTMLocal, LegNUM
  END  
      	
 
 END 
 GO
 --EXEC spGetReportPOSTAircraftPerformance 'supervisor_99','31-oct-2000','02-nov-2015','N400W' 


