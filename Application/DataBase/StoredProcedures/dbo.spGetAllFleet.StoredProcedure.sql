

/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 03/18/2013 12:39:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleet]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 03/18/2013 12:39:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[spGetAllFleet](@CustomerID bigint,@ClientID bigint)        
AS        
-- =============================================        
-- Author: Badrinath        
-- Create date: 7/5/2012        
-- Description: Get All Fleet Profile Informations        
-- =============================================        
SET NOCOUNT ON        
  IF(@ClientID>0)      
  BEGIN      
SELECT [FleetID]      
      ,CASE WHEN [TailNum] IS NULL THEN '' ELSE LTRIM(RTRIM(TailNum)) END AS TailNum     
      ,[CustomerID]       
      ,[AircraftCD]      
      ,[SerialNum]      
      ,[TypeDescription]      
      ,[MaximumReservation]      
      ,[LastInspectionDT]      
      ,[InspectionHrs]      
      ,[WarningHrs]      
      ,[MaximumPassenger]      
      ,[HomebaseID]      
      ,[MaximumFuel]      
      ,[MinimumFuel]      
      ,[BasicOperatingWeight]      
      ,[MimimumRunway]      
      ,[IsInActive]      
      ,[IsDisplay31]      
      ,[SIFLMultiControlled]      
      ,[SIFLMultiNonControled]      
      ,[Notes]      
      ,[ComponentCD]      
      ,[ClientID]      
      ,[FleetType]      
      ,[FlightPhoneNum]      
      ,[Class]      
      ,[VendorID]      
      ,[VendorType]      
      ,[YearMade]      
      ,[ExteriorColor]      
      ,[InteriorColor]      
      ,[IsAFIS]      
      ,[IsUWAData]      
      ,[LastUpdUID]      
      ,[LastUpdTS]      
      ,[FlightPlanCruiseSpeed]      
      ,[FlightPlanMaxFlightLevel]      
      ,[MaximumTakeOffWeight]      
      ,[MaximumLandingWeight]      
      ,[MaximumWeightZeroFuel]      
      ,[TaxiFuel]      
      ,[MultiSec]      
      ,[ForeGrndCustomColor]      
      ,[BackgroundCustomColor]      
      ,[FlightPhoneIntlNum]      
      ,[FleetSize]      
      ,[FltScanDoc]      
      ,[MinimumDay]      
      ,[StandardCrewIntl]      
      ,[StandardCrewDOM]      
      ,[IsFAR91]      
      ,[IsFAR135]      
      ,[EmergencyContactID]      
      ,[IsTaxDailyAdj]      
      ,[IsTaxLandingFee]      
      ,[IsDeleted]      
      ,[AircraftID]      
      ,[CrewID]  
      ,SecondaryDomFlightPhone  
       ,SecondaryIntlFlightPhone
       ,marginalpercentage  ,NationalityCountryID          
       FROM [Fleet] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0 and ClientID=@ClientID      
       END      
       ELSE      
       BEGIN      
       SELECT [FleetID]      
      ,CASE WHEN [TailNum] IS NULL THEN '' ELSE LTRIM(RTRIM(TailNum)) END AS TailNum        
      ,[CustomerID]       
      ,[AircraftCD]      
      ,[SerialNum]      
      ,[TypeDescription]      
      ,[MaximumReservation]      
      ,[LastInspectionDT]      
      ,[InspectionHrs]      
      ,[WarningHrs]      
      ,[MaximumPassenger]      
      ,[HomebaseID]      
      ,[MaximumFuel]      
      ,[MinimumFuel]      
      ,[BasicOperatingWeight]      
      ,[MimimumRunway]      
      ,[IsInActive]      
      ,[IsDisplay31]      
      ,[SIFLMultiControlled]      
      ,[SIFLMultiNonControled]      
      ,[Notes]      
      ,[ComponentCD]      
      ,[ClientID]      
      ,[FleetType]      
      ,[FlightPhoneNum]      
      ,[Class]      
      ,[VendorID]      
      ,[VendorType]      
      ,[YearMade]      
      ,[ExteriorColor]      
      ,[InteriorColor]      
      ,[IsAFIS]      
      ,[IsUWAData]      
      ,[LastUpdUID]      
      ,[LastUpdTS]      
      ,[FlightPlanCruiseSpeed]      
      ,[FlightPlanMaxFlightLevel]      
      ,[MaximumTakeOffWeight]      
      ,[MaximumLandingWeight]      
      ,[MaximumWeightZeroFuel]      
      ,[TaxiFuel]      
      ,[MultiSec]      
      ,[ForeGrndCustomColor]      
      ,[BackgroundCustomColor]      
      ,[FlightPhoneIntlNum]      
      ,[FleetSize]      
      ,[FltScanDoc]      
      ,[MinimumDay]      
      ,[StandardCrewIntl]      
      ,[StandardCrewDOM]      
      ,[IsFAR91]      
      ,[IsFAR135]      
      ,[EmergencyContactID]      
      ,[IsTaxDailyAdj]      
      ,[IsTaxLandingFee]      
      ,[IsDeleted]      
      ,[AircraftID]      
      ,[CrewID]   
       ,SecondaryDomFlightPhone  
       ,SecondaryIntlFlightPhone 
       ,marginalpercentage ,NationalityCountryID           
       FROM [Fleet] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0       
       END