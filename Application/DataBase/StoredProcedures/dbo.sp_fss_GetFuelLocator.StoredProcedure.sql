/****** Object:  StoredProcedure [dbo].[sp_fss_GetMetroCity]    Script Date: 02/28/2014 13:23:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetFuelLocator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetFuelLocator]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetFuelLocator]    Script Date: 02/28/2014 13:23:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[sp_fss_GetFuelLocator]  
(  
  @CustomerID BIGINT  
    ,@FuelLocatorID BIGINT  
    ,@FuelLocatorCD varchar(4)   
 ,@ClientID BIGINT  
 ,@FetchActiveOnly BIT    
)  
AS   
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get All FuelLocator details for Popup        
-- Exec sp_fss_GetFuelLocator 10099,0,'',0, 1  
-- Exec sp_fss_GetFuelLocator 10099,0,'',0, 0  
-- Exec sp_fss_GetFuelLocator 10099, 10099205,'',NULL, 0  
-- Exec sp_fss_GetFuelLocator 10099, 0, 'INTL',NULL,0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
 SELECT FL.FuelLocatorID  
   ,FL.CustomerID  
   ,FL.FuelLocatorCD  
   ,FL.FuelLocatorDescription  
   ,FL.ClientID  
   ,C.ClientCD   
   ,FL.LastUpdUID      
            ,FL.LastUpdTS     
   ,ISNULL(FL.IsDeleted,0) IsDeleted     
   , ISNULL(FL.IsInactive,0) IsInactive    
 FROM FuelLocator FL LEFT OUTER JOIN Client C on FL.ClientID= C.ClientID    
 WHERE FL.CustomerID = @CustomerID   
         AND ISNULL(FL.ClientID,0) = Case when @ClientID >0 then @ClientID else  ISNULL(FL.ClientID,0) end  
         AND FL.FuelLocatorID = case when @FuelLocatorID <>0 then @FuelLocatorID else FL.FuelLocatorID end   
         AND FL.FuelLocatorCD = case when Ltrim(Rtrim(@FuelLocatorCD)) <> '' then @FuelLocatorCD else FL.FuelLocatorCD end                                    
         AND ISNULL(FL.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(FL.IsInActive,0) else 0 end  
         AND ISNULL(FL.IsDeleted,0) = 0                                  
 ORDER BY FuelLocatorCD ASC  
END   
  
--select * from FuelLocator FL  
  
GO

