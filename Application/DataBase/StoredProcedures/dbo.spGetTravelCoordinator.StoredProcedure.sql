
GO
/****** Object:  StoredProcedure [dbo].[spGetTravelCoordinator]    Script Date: 08/24/2012 10:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTravelCoordinator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTravelCoordinator]
GO

CREATE Procedure [spGetTravelCoordinator]  
 (@CustomerID BIGINT)          
as begin  
-- =============================================          
-- Author:RUKMINI M          
-- Create date: 26/04/2012         
-- Modofied by Mathes on 03-07-12         
-- Description: Get the Travel information          
-- =============================================          
 set nocount on          
 if len(@CustomerID) >0          
 begin           
  SELECT TravelCoordinator.TravelCoordinatorID        
    ,TravelCoordinator.CustomerID        
    ,TravelCoordinator.TravelCoordCD        
    ,TravelCoordinator.FirstName        
    ,TravelCoordinator.MiddleName        
    ,TravelCoordinator.LastName        
    ,TravelCoordinator.PhoneNum        
    ,TravelCoordinator.FaxNum        
    ,TravelCoordinator.PagerNum        
    ,TravelCoordinator.CellPhoneNum        
    ,TravelCoordinator.EmailID        
    ,TravelCoordinator.Notes        
    ,TravelCoordinator.HomebaseID        
    ,TravelCoordinator.LastUpdUID        
    ,TravelCoordinator.LastUpdTS        
    ,TravelCoordinator.IsDeleted        
    ,TravelCoordinator.BusinessPhone  
    ,TravelCoordinator.HomeFax  
    ,TravelCoordinator.CellPhoneNum2  
    ,TravelCoordinator.OtherPhone  
    ,TravelCoordinator.BusinessEmail  
    ,TravelCoordinator.PersonalEmail  
    ,TravelCoordinator.OtherEmail
    ,TravelCoordinator.IsInactive  
    ,TravelCoordinator.Addr3  
    --,Company.HomebaseCD         
    ,Company.BaseDescription         
    ,Airport.ICAOID AS HomebaseCD            
    ,TravelCoordinator.LastName + ', '+ TravelCoordinator.FirstName AS DisplayName 
    ,xx.PassengerRequestorCD
    ,xx.PassengerName                
   
  FROM TravelCoordinator 
	LEFT OUTER JOIN (
		SELECT TravelCoordinatorRequestor.TravelCoordinatorID ,Passenger.PassengerRequestorCD ,Passenger.PassengerName  
		FROM TravelCoordinatorRequestor 
		INNER JOIN Passenger ON Passenger.PassengerRequestorID = TravelCoordinatorRequestor.PassengerRequestorID							      
		WHERE TravelCoordinatorRequestor.IsChoice = 1  and TravelCoordinatorRequestor.IsDeleted = 0	
	) xx on TravelCoordinator.TravelCoordinatorID = xx.TravelCoordinatorID
    LEFT OUTER JOIN Company ON Company.HomebaseID = TravelCoordinator.HomebaseID          
    LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID      
  WHERE TravelCoordinator.CustomerID=@CustomerID      
    AND TravelCoordinator.IsDeleted = 0
 ORDER BY TravelCoordinator.TravelCoordCD 
 END   
end
GO