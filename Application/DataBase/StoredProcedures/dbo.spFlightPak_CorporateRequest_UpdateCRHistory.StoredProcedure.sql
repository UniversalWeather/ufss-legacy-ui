/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRHistory]    Script Date: 04/24/2013 19:48:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_UpdateCRHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRHistory]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRHistory](  
  @CRMainID bigint
 ,@CustomerID bigint
 ,@LogisticsHistory varchar(MAX)
 ,@LastUpdUID varchar(30)
 ,@LastUpdTS datetime
 ,@IsDeleted bit
 ,@CRHistoryID bigint
 ,@RevisionNumber int
)      
AS      
BEGIN
SET NOCOUNT OFF
 UPDATE [dbo].[CRHistory] SET
  LogisticsHistory = @LogisticsHistory,
  [LastUpdUID]=@LastUpdUID,
  [LastUpdTS]=@LastUpdTS 
  
  WHERE CRMainID=@CRMainID and CustomerID=@CustomerID and  CRHistoryID = @CRHistoryID and RevisionNumber=@RevisionNumber
END
GO