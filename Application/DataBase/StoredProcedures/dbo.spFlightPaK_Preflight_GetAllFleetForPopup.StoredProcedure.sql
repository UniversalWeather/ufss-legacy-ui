
/****** Object:  StoredProcedure [dbo].[spFlightPaK_Preflight_GetAllFleetForPopup]    Script Date: 01/30/2014 15:53:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPaK_Preflight_GetAllFleetForPopup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPaK_Preflight_GetAllFleetForPopup]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPaK_Preflight_GetAllFleetForPopup]    Script Date: 01/30/2014 15:53:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spFlightPaK_Preflight_GetAllFleetForPopup](@CustomerID bigint,
  @ClientID bigint,
  @FetchActiveOnly BIT=0,
  @IsFixedRotary CHAR(1)='B',
  @ICAOID varchar(4) ,
  @VendorID BIGINT
)              
AS     
    
SET NOCOUNT ON      
-- =============================================            
-- Author: Prabhu           
-- Create date: 28/01/2014            
-- Description: Get All Fleet, Vendor and Aircraft details for Popup      
-- Exec spFlightPaK_Preflight_GetAllFleetForPopup 10003, null, 0,'B','',0
-- =============================================            
          
  
 SELECT [FleetID]            
      ,[TailNum]            
      ,Fleet.[CustomerID]             
      ,Fleet.[AircraftCD]      
      ,Aircraft.AircraftCD as AirCraft_AircraftCD           
      ,[SerialNum]            
      ,[TypeDescription]            
      ,[MaximumReservation]            
      ,[LastInspectionDT]            
      ,[InspectionHrs]            
      ,[WarningHrs]            
      ,[MaximumPassenger]            
      ,Fleet.[HomebaseID]            
      ,[MaximumFuel]            
      ,[MinimumFuel]            
      ,[BasicOperatingWeight]            
      ,[MimimumRunway]    
      ,Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive            
      ,[IsDisplay31]            
      ,[SIFLMultiControlled]            
      ,[SIFLMultiNonControled]            
      ,Fleet.[Notes]            
      ,[ComponentCD]            
      ,Fleet.[ClientID]            
      ,[FleetType]            
      ,[FlightPhoneNum]            
      ,[Class]            
      ,Fleet.[VendorID]            
      ,Fleet.[VendorType]            
      ,[YearMade]            
      ,[ExteriorColor]            
      ,[InteriorColor]            
      ,[IsAFIS]            
      ,[IsUWAData]            
      ,Fleet.[LastUpdUID]            
      ,Fleet.[LastUpdTS]            
      ,[FlightPlanCruiseSpeed]            
      ,[FlightPlanMaxFlightLevel]            
      ,[MaximumTakeOffWeight]            
      ,[MaximumLandingWeight]            
      ,[MaximumWeightZeroFuel]            
      ,[TaxiFuel]            
      ,[MultiSec]            
      ,[ForeGrndCustomColor]            
      ,[BackgroundCustomColor]            
      ,[FlightPhoneIntlNum]            
      ,[FleetSize]            
      ,[FltScanDoc]            
      ,[MinimumDay]            
      ,[StandardCrewIntl]            
      ,[StandardCrewDOM]            
      ,[IsFAR91]            
      ,[IsFAR135]            
      ,FLEET.[EmergencyContactID]            
      ,[IsTaxDailyAdj]            
      ,[IsTaxLandingFee]            
      ,Fleet.[IsDeleted]            
      ,Fleet.[AircraftID]            
      ,Fleet.[CrewID]        
      ,SecondaryDomFlightPhone        
       ,SecondaryIntlFlightPhone      
       ,Client.ClientCD      
       ,Vendor.VendorCD      
       ,Airport.IcaoID      
       ,Aircraft.PowerSettings1TrueAirSpeed      
       ,Aircraft.PowerSettings2TrueAirSpeed      
       ,Aircraft.PowerSettings3TrueAirSpeed      
       --FROM [Fleet]       
   --LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID       
   --LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID                         
       --WHERE Fleet.CustomerID=@CustomerID AND ISNULL(Fleet.IsDeleted,0) = 0 and Fleet.ClientID=@ClientID        
         FROM [Fleet]   LEFT OUTER JOIN                    
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                    
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                    
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                    
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN         
                      Airport ON Company.HomebaseAirportID=Airport.AirportID LEFT OUTER JOIN                
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                    
                      Vendor ON Fleet.VendorID = Vendor.VendorID                         
               WHERE 
               
               Fleet.CustomerID=@CustomerID 
               AND ISNULL(Fleet.ClientID,0) = Case when @ClientID >0 then @ClientID else  ISNULL(Fleet.ClientID,0) end
               and ISNULL(Fleet.IsInActive,0) = case when @FetchActiveOnly =1 then 0 else ISNULL(Fleet.IsInActive,0) end
               AND Aircraft.IsFixedRotary =CASE WHEN @IsFixedRotary = 'B' then Aircraft.IsFixedRotary else @IsFixedRotary end
               and ISNULL(Airport.IcaoID,'') = CASE WHEN ltrim(rtrim(@ICAOID)) <>'' then @ICAOID else ISNULL(Airport.IcaoID,'') end
               and ISNULL(Fleet.VendorID,0) = case when @VendorID <>0 then @VendorID else ISNULL(Fleet.VendorID,0) end
               AND ISNULL(Fleet.IsDeleted,0) = 0                               
               --and fleet.FleetID =10003181803
               
               Order by  Fleet.AircraftCD
           
 
GO


