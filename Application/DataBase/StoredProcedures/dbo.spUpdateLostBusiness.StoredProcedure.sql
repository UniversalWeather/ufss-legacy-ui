
/****** Object:  StoredProcedure [dbo].[spUpdateLostBusiness]    Script Date: 20/02/2013 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateLostBusiness]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateLostBusiness]
go

CREATE procedure [dbo].[spUpdateLostBusiness]
(
	 @CQLostBusinessID bigint
	,@CustomerID bigint	
	,@CQLostBusinessDescription varchar(100)	
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime	
	,@IsDeleted bit
	
)
-- =============================================
-- Author: Ramesh.J
-- Create date: 20/02/2013
-- Description: Update the CQLostBusiness information
-- =============================================
as
begin 
SET NoCOUNT ON
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
UPDATE [CQLostBusiness]
   SET             
           [CQLostBusinessDescription]=@CQLostBusinessDescription
           ,[LastUpdUID]=@LastUpdUID
           ,[LastUpdTS]=@LastUpdTS          
           ,[IsDeleted]=@IsDeleted           
 WHERE CustomerID=@CustomerID and CQLostBusinessID=@CQLostBusinessID

end
GO
