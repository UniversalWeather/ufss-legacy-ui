
/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_AddLeg]    Script Date: 02/18/2014 11:41:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_AddLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_AddLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_AddLeg]    Script Date: 02/18/2014 11:41:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Postflight_AddLeg]
(
	@POLogID bigint,
	@LegNUM bigint,
	@TripLegID bigint,
	@CustomerID bigint,
	@DepartICAOID bigint,
	@ArriveICAOID bigint,
	@Distance numeric(5),
	@ScheduledTM datetime = null,
	@BlockOut char(5),
	@BlockIN char(5),
	@TimeOff char(5),
	@TimeOn char(5),
	@BlockHours numeric(12, 3),
	@FlightHours numeric(12, 3),
	@FedAviationRegNum char(3),
	@ClientID bigint,
	@FlightPurpose varchar(40),
	@DutyTYPE numeric(1, 0),
	@CrewCurrency char(4),
	@IsDutyEnd bit,
	@FuelOut numeric(6, 0),
	@FuelIn numeric(6, 0),
	@FuelUsed numeric(7, 0),
	@FuelBurn numeric(1, 0),
	@DelayTM numeric(6, 3),
	@DutyHrs numeric(12, 3),
	@RestHrs numeric(12, 3),
	@NextDTTM datetime = null,
	@DepatureFBO bigint,
	@ArrivalFBO bigint,
	@CrewID bigint,
	@IsException bit,
	@IsAugmentCrew bit,
	@BeginningDutyHours char(5),
	@EndDutyHours char(5),
	@LastUpdUID char(30),
	@LastUpdTS datetime = null,
	@PassengerTotal numeric(4, 0),
	@ScheduleDTTMLocal datetime = null,
	@BlockOutLocal char(5),
	@BlockInLocal char(5),
	@TimeOffLocal char(5),
	@TimeOnLocal char(5),
	@OutboundDTTM datetime = null,
	@InboundDTTM datetime = null,
	@AdditionalDist numeric(5, 0),
	@AVTrak char(1),
	@FlightNum varchar(12),
	@CargoOut numeric(7, 0),
	@CargoIn numeric(7, 0),
	@CargoThru numeric(7, 0),
	@StatuteMiles numeric(5),
	@FlightCost numeric(12, 2),
	@TechLog varchar(12),
	@AirFrameHours numeric(11, 3),
	@AirframeCycle numeric(6, 0),
	@Engine1Hours numeric(11, 3),
	@Engine2Hours numeric(11, 3),
	@Engine3Hours numeric(11, 3),
	@Engine4Hours numeric(11, 3),
	@Engine1Cycle numeric(6, 0),
	@Engine2Cycle numeric(6, 0),
	@Engine3Cycle numeric(6, 0),
	@Engine4Cycle numeric(6, 0),
	@Re1Hours numeric(11, 3),
	@Re2Hours numeric(11, 3),
	@Re3Hours numeric(11, 3),
	@Re4Hours numeric(11, 3),
	@Re1Cycle numeric(6, 0),
	@Re2Cycle numeric(6, 0),
	@Re3Cycle numeric(6, 0),
	@Re4Cycle numeric(6, 0),
	@FuelOn numeric(17, 6),
	@IsDeleted bit,
	@AccountID bigint,
	@DelayTypeID bigint,
	@PassengerRequestorID bigint,
	@DepartmentID bigint,
	@AuthorizationID bigint,
	@FlightCategoryID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @POLegID BIGINT

	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PostflightCurrentNo',  @POLegID OUTPUT
	
	SET @LastUpdTS = GETUTCDATE()
	
	--// Update Fleet Information based on Completed Status field from POSTFLIGHT.
	DECLARE @IsCompleted bit
	DECLARE @FleetID BIGINT
	DECLARE @AircraftCD VARCHAR(100)
	SET @IsCompleted = 0
	
	--// Check If IsCompleted status is Unchecked from UI, but it's already in Completed Status before. 
	--// Then Deduct the Airframe and Engine Hours from Fleet Information Table.
	DECLARE @FleetInformationID BIGINT
	
	SELECT @IsCompleted = IsCompleted, @FleetID = FleetID FROM PostflightMain WHERE POLogID = @POLogID AND Isnull(IsDeleted,0) = 0
	SELECT @AircraftCD = AircraftCD FROM Fleet with(nolock) WHERE FleetID = @FleetID AND Isnull(IsDeleted,0) = 0

	IF ISNULL(@IsCompleted,0) = 1
	BEGIN
	
		--// Check If Leg(s) details Exists
		 IF EXISTS (SELECT NULL FROM [FleetInformation] WHERE FleetID = @FleetID AND CustomerID = @CustomerID AND isnull(IsDeleted,0) = 0)
		 BEGIN
		
			--// Add into Fleet Information Table based on the specified Customer and Fleet Id
			UPDATE [FleetInformation] SET 	
				AirframeHrs = IsNull([FleetInformation].AirframeHrs,0) + IsNull(@AirFrameHours,0),
				AirframeCycle = IsNull([FleetInformation].AirframeCycle,0) + IsNull(@AirframeCycle,0),
				AIrframeAsOfDT = @LastUpdTS,
				
				Engine1Hrs = IsNull([FleetInformation].Engine1Hrs,0) + IsNull(@Engine1Hours,0),
				Engine1Cycle = IsNull([FleetInformation].Engine1Cycle,0) + IsNull(@Engine1Cycle,0), 
				Engine1AsOfDT = @LastUpdTS,
				Engine2Hrs = IsNull([FleetInformation].Engine2Hrs,0) + IsNull(@Engine2Hours,0),
				Engine2Cycle = IsNull([FleetInformation].Engine2Cycle,0) + IsNull(@Engine2Cycle,0), 
				Engine2AsOfDT = @LastUpdTS,
				Engine3Hrs = IsNull([FleetInformation].Engine3Hrs,0) + IsNull(@Engine3Hours,0),
				Engine3Cycle = IsNull([FleetInformation].Engine3Cycle,0) + IsNull(@Engine3Cycle,0),
				Engine3AsOfDT = @LastUpdTS,
				Engine4Hrs = IsNull([FleetInformation].Engine4Hrs,0) + IsNull(@Engine4Hours,0),
				Engine4Cycle = IsNull([FleetInformation].Engine4Cycle,0) + IsNull(@Engine4Cycle,0),
				Engine4AsOfDT = @LastUpdTS,

				Reverse1Hrs = IsNull([FleetInformation].Reverse1Hrs,0) + IsNull(@Re1Hours,0),
				Reverser1Cycle = IsNull([FleetInformation].Reverser1Cycle,0) + IsNull(@Re1Cycle,0), 
				Reverser1AsOfDT = @LastUpdTS,
				Reverse2Hrs = IsNull([FleetInformation].Reverse2Hrs,0) + IsNull(@Re2Hours,0),
				Reverser2Cycle = IsNull([FleetInformation].Reverser2Cycle,0) + IsNull(@Re2Cycle,0), 
				Reverser2AsOfDT = @LastUpdTS,
				Reverse3Hrs = IsNull([FleetInformation].Reverse3Hrs,0) + IsNull(@Re3Hours,0),
				Reverser3Cycle = IsNull([FleetInformation].Reverser3Cycle,0) + IsNull(@Re3Cycle,0),
				Reverser3AsOfDT = @LastUpdTS,
				Reverse4Hrs = IsNull([FleetInformation].Reverse4Hrs,0) + IsNull(@Re4Hours,0),
				Reverser4Cycle = IsNull([FleetInformation].Reverser4Cycle,0) + IsNull(@Re4Cycle,0),
				Reverser4AsOfDT = @LastUpdTS,
								
				LastUpdUID = @LastUpdUID,
				LastUpdTS = @LastUpdTS
			FROM [FleetInformation]
			WHERE [FleetInformation].CustomerID = @CustomerID AND [FleetInformation].FleetID = @FleetID AND isnull([FleetInformation].IsDeleted,0) = 0			 

		END
		ELSE
		BEGIN

			--// Check If there is No Fleet Information in the Table, then Insert the New Airframe Values 
			 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetInformationID OUTPUT

			INSERT INTO [FleetInformation] (FleetInformationID, CustomerID, FleetID, AircraftCD, AirframeHrs,AirframeCycle,AIrframeAsOfDT,Engine1Hrs,Engine1Cycle,Engine1AsOfDT,Engine2Hrs,Engine2Cycle,Engine2AsOfDT
				, Engine3Hrs,Engine3Cycle,Engine3AsOfDT,Engine4Hrs,Engine4Cycle,Engine4AsOfDT
				, Reverse1Hrs, Reverser1Cycle, Reverser1AsOfDT, Reverse2Hrs, Reverser2Cycle, Reverser2AsOfDT, Reverse3Hrs, Reverser3Cycle
				, Reverser3AsOfDT, Reverse4Hrs, Reverser4Cycle, Reverser4AsOfDT	
				, LastUpdUID,LastUpdTS)
			SELECT @FleetInformationID, @CustomerID, @FleetID, @AircraftCD, @AirFrameHours, @AirframeCycle, @LastUpdTS
				, @Engine1Hours, @Engine1Cycle, @LastUpdTS, @Engine2Hours, @Engine2Cycle, @LastUpdTS
				, @Engine3Hours, @Engine3Cycle, @LastUpdTS, @Engine4Hours, @Engine4Cycle, @LastUpdTS
				, @Re1Hours, @Re1Cycle, @LastUpdTS, @Re2Hours, @Re2Cycle, @LastUpdTS
				, @Re3Hours, @Re3Cycle, @LastUpdTS, @Re4Hours, @Re4Cycle, @LastUpdTS
				, @LastUpdUID,@LastUpdTS
			
		END		
	END	
	
	INSERT INTO [PostflightLeg] ([POLegID], [POLogID], [LegNUM], [TripLegID], [CustomerID], [DepartICAOID], [ArriveICAOID], [Distance], [ScheduledTM], [BlockOut], [BlockIN], [TimeOff], [TimeOn], [BlockHours], [FlightHours], [FedAviationRegNum], [ClientID], [FlightPurpose], [DutyTYPE], [CrewCurrency], [IsDutyEnd], [FuelOut], [FuelIn], [FuelUsed], [FuelBurn], [DelayTM], [DutyHrs], [RestHrs], [NextDTTM], [DepatureFBO], [ArrivalFBO], [CrewID], [IsException], [IsAugmentCrew], [BeginningDutyHours], [EndDutyHours], [LastUpdUID], [LastUpdTS], [PassengerTotal], [ScheduleDTTMLocal], [BlockOutLocal], [BlockInLocal], [TimeOffLocal], [TimeOnLocal], [OutboundDTTM], [InboundDTTM], [AdditionalDist], [AVTrak], [FlightNum], [CargoOut], [CargoIn], [CargoThru], [StatuteMiles], [FlightCost], [TechLog], [AirFrameHours], [AirframeCycle], [Engine1Hours], [Engine2Hours], [Engine3Hours], [Engine4Hours], [Engine1Cycle], [Engine2Cycle], [Engine3Cycle], [Engine4Cycle], [Re1Hours], [Re2Hours], [Re3Hours], [Re4Hours], [Re1Cycle], [Re2Cycle], [Re3Cycle], [Re4Cycle], [FuelOn], [IsDeleted], [AccountID], [DelayTypeID], [PassengerRequestorID], [DepartmentID], [AuthorizationID], [FlightCategoryID]) 
		VALUES (@POLegID, @POLogID, @LegNUM, @TripLegID, @CustomerID, @DepartICAOID, @ArriveICAOID, @Distance, @ScheduledTM, @BlockOut, @BlockIN, @TimeOff, @TimeOn, 
		CASE WHEN @BlockHours <0 THEN 0 ELSE @BlockHours END
	  ,CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END
		, @FedAviationRegNum, @ClientID, @FlightPurpose, @DutyTYPE, @CrewCurrency, @IsDutyEnd, @FuelOut, @FuelIn, @FuelUsed, @FuelBurn, @DelayTM, 
		CASE WHEN @DutyHrs <0 THEN 0 ELSE @DutyHrs END
	  ,CASE WHEN @RestHrs <0 THEN 0 ELSE @RestHrs END
		, @NextDTTM, @DepatureFBO, @ArrivalFBO, @CrewID, @IsException, @IsAugmentCrew, @BeginningDutyHours, @EndDutyHours, @LastUpdUID, @LastUpdTS, @PassengerTotal, @ScheduleDTTMLocal, @BlockOutLocal, @BlockInLocal, @TimeOffLocal, @TimeOnLocal, @OutboundDTTM, @InboundDTTM, @AdditionalDist, @AVTrak, @FlightNum, @CargoOut, @CargoIn, @CargoThru, @StatuteMiles, @FlightCost, @TechLog, @AirFrameHours, @AirframeCycle, @Engine1Hours, @Engine2Hours, @Engine3Hours, @Engine4Hours, @Engine1Cycle, @Engine2Cycle, @Engine3Cycle, @Engine4Cycle, @Re1Hours, @Re2Hours, @Re3Hours, @Re4Hours, @Re1Cycle, @Re2Cycle, @Re3Cycle, @Re4Cycle, @FuelOn, @IsDeleted, @AccountID, @DelayTypeID, @PassengerRequestorID, @DepartmentID, @AuthorizationID, @FlightCategoryID);
	
	SELECT isnull(@POLegID,0) as 'POLegID'

END


GO


