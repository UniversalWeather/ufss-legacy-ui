
/****** Object:  StoredProcedure [dbo].[spCrewRosterCurrency]    Script Date: 06/14/2013 19:34:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCrewRosterCurrency]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCrewRosterCurrency]
GO


/****** Object:  StoredProcedure [dbo].[spCrewRosterCurrency]    Script Date: 06/14/2013 19:34:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spCrewRosterCurrency](@crew_code BIGINT, @LoggedInUserName VARCHAR(30))        
AS        
BEGIN          
  
DECLARE @ldQtrBeg DATE          
DECLARE @ldPQtrBeg DATE          
DECLARE @lnQtrSec BIGINT  --changed from int to big int       
DECLARE @lnQtrHrs BIGINT  --changed from int to big int         
          
DECLARE @FIRST_DAY_MONTH DATE          
DECLARE @FIRST_DAY_YEAR DATE          
--SET GETDATE()=(SELECT CONVERT(DATE,GETDATE()))          
SET @FIRST_DAY_MONTH=(SELECT CONVERT(DATE,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)))          
SET @FIRST_DAY_YEAR=( SELECT CONVERT(DATE,DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0)))          

--Get LoggedIn User HomebaseId and CustomerId
DECLARE @UserHomeBaseId BIGINT
DECLARE @UserCustomerId BIGINT
SELECT @UserHomeBaseId = HomebaseID, @UserCustomerId=CustomerID FROM UserMaster WHERE UserName = @LoggedInUserName

--Get LoggedIn User HomebaseAirportId
DECLARE @UserHomeBaseAirportId BIGINT
SELECT @UserHomeBaseAirportId = HomeBaseAirportId FROM Company WHERE HomebaseID = @UserHomeBaseId

----Commented since not used in SP 
--DECLARE @TimeDisplayTenMin NUMERIC(1,0)           
----SELECT @TimeDisplayTenMin=TimeDisplayTenMin FROM COMPANY WHERE homebaseairportid IN(           
----SELECT AirportID FROM Airport WHERE AirportID IN(SELECT HomebaseID FROM Crew WHERE CrewID=@crew_code))          
--SELECT @TimeDisplayTenMin=TimeDisplayTenMin FROM COMPANY WHERE homebaseairportid IN(SELECT HomebaseID FROM Crew WHERE CrewID=@crew_code)    

--Get Crew HomebaseId and CustomerId
DECLARE @Homebaseid BIGINT  
DECLARE @CrewCustomerId BIGINT
SELECT @Homebaseid = HomebaseID, @CrewCustomerId = CustomerID FROM Crew WHERE CrewID =  @crew_code 
--Print @Homebaseid  
--Print @CrewCustomerId

--Check whether the Crew has a Company mapped, to get Company Profile settings. If not take LoggedIn user's Company Profile settings
IF(NOT EXISTS(SELECT HomebaseID FROM Company WHERE HomebaseAirportID = @Homebaseid AND CustomerID = @CrewCustomerId))
BEGIN
	SET @Homebaseid = @UserHomeBaseAirportId
	SET @CrewCustomerId = @UserCustomerId
END

DECLARE @ICAOID CHAR(4)  
SELECT @ICAOID = IcaoID FROM Airport WHERE AirportID = @Homebaseid  
--Print @ICAOID  

-- Commented out not required  
--DECLARE @UWACustomerID BIGINT  
--SELECT @UWACustomerID  = dbo.GetUWACustomerID()    
  
--DECLARE @AirportID BIGINT  
--SELECT @AirportID = AirportID FROM Airport WHERE ICAOID  = @ICAOID AND CustomerID = @UWACustomerID  
  
--Get AircraftBasis info from Company Profile for calculation purpose
DECLARE @AircraftBasis NUMERIC(3,0)          
SELECT @AircraftBasis = AircraftBasis FROM Company WHERE Company.homebaseairportid = @Homebaseid 
					--,Crew           
                    -- WHERE (Company.homebaseairportid=Crew.HomebaseID  OR Company.homebaseairportid = @AirportID)  
                    -- AND  Crew.CrewID=@crew_code         
--print @AircraftBasis   

--@TempPO is combination of      
-- PostflightCrew, PostflightLeg, PostflightMain, Fleet, Aircraft and Crew      
-- UNION      
-- PostflightSimulatorLog, Crew and Aircraft      
DECLARE @TempPO TABLE(RowID INT IDENTITY,          
    AircraftCD CHAR(10),          
    FleetID BIGINT,          
    POLogID BIGINT,          
    POLegID BIGINT,          
    ScheduledTM DATE,          
    InboundDTTM DATETIME,          
    LandingDay NUMERIC(3,0),          
    LandingNight NUMERIC(3,0),          
    ApproachPrecision NUMERIC(2,0),          
    ApproachNonPrecision NUMERIC(2,0),          
    Instrument NUMERIC(7,3),          
    BlockHours NUMERIC(7,3),          
    FlightHours  NUMERIC(7,3),          
    DutyHours NUMERIC(7,3),          
    DutyEnd CHAR(5),          
    CrewID BIGINT,          
    TakeOffDay NUMERIC(2,0),          
    TakeOffNight NUMERIC(2,0),          
    RType CHAR(5),          
    Actualduty NUMERIC(7,3),          
    CrewName VARCHAR(70))          
              
    
--@TEMP is combination fo Crew and Company Tables               
DECLARE @TEMP table(CrewID BIGINT,          
     DayLanding NUMERIC(3,0),          
     NightLanding NUMERIC(3,0),          
     Approach NUMERIC(3,0),          
     Instrument NUMERIC(3,0),          
     Day7 NUMERIC(3,0),           
     RestDays NUMERIC(3,0),          
     Day90 NUMERIC(3,0),          
     Month6 NUMERIC(3,0),          
     Month12 NUMERIC(3,0),          
     Day365 NUMERIC(3,0),          
     TakeoffDay NUMERIC(3,0),          
     TakeoffNight NUMERIC(3,0),          
     Day30 NUMERIC(3,0))         
INSERT INTO @TEMP(CrewID,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30)        
	SELECT --crew.CrewID,
	@crew_code,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30       
    FROM Company       
    WHERE Company.HomebaseAirportID = @Homebaseid AND Company.CustomerID = @CrewCustomerId 
    --Company,Crew           
    --WHERE (Company.HomebaseAirportID = Crew.HomebaseID OR Company.homebaseairportid = @AirportID) AND Crew.CrewID = @crew_code  AND Crew.IsStatus <> 0  AND Crew.IsDeleted = 0 AND Company.CustomerID = Crew.CustomerID     
       
--Identify Current Quarter Begin Date and Previous Quarter begin date                                                  
SET @ldQtrBeg=(SELECT CONVERT(DATE,Dateadd(qq, Datediff(qq,0,GetDate()), 0)))          
SET @ldPQtrBeg=(SELECT CONVERT(DATE,DATEADD(qq,DATEDIFF(qq,0,GETDATE())-1,0)))          
  
--select * from @TEMP          
          
INSERT @TempPO(AircraftCD,          
    FleetID,          
    POLogID,          
    POLegID,          
    ScheduledTM,          
    InboundDTTM,          
    LandingDay,          
    LandingNight,          
    ApproachPrecision,          
    ApproachNonPrecision,          
    Instrument,          
    BlockHours,          
    FlightHours,          
    DutyHours,          
    DutyEnd,          
    CrewID,          
    TakeOffDay,          
    TakeOffNight,          
    RType,     
    Actualduty,          
    CrewName)           
(SELECT A.AircraftCD,PM.FleetID,PL.POLogID,PL.POLegID,CONVERT(DATE,PL.ScheduledTM) ScheduledTM,PL.InboundDTTM,PC.LandingDay,          
       PC.LandingNight,PC.ApproachPrecision,PC.ApproachNonPrecision,PC.Instrument,PC.BlockHours,          
       PC.FlightHours,PC.DutyHours,PC.DutyEnd,PC.CrewID,PC.TakeOffDay,PC.TakeOffNight,'FL' AS RType,          
       CONVERT(INT,'')  AS Actualduty,C.FirstName+''+C.MiddleInitial+''+C.LastName AS CrewName          
      FROM  PostflightMain PM     
      INNER JOIN PostflightCrew PC ON PC.POLogID=PM.POLogID    
      INNER JOIN PostflightLeg PL ON PC.POLegID=PL.POLegID     
      INNER JOIN Fleet F ON F.FleetID=PM.FleetID    
      LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID    
      INNER JOIN Crew C ON PC.CrewID=C.CrewID           
  WHERE    CONVERT(DATE,PL.ScheduledTM) BETWEEN GETDATE()-400 AND GETDATE()      
    AND C.CrewID=@crew_code AND PM.IsDeleted = 0    
UNION            
          
 SELECT  A.AircraftCD,0 FleetID,0 LogID,0 POLegID,PFSL.SessionDT AS SCHEDTTM,''InboundDTTM,          
          PFSL.LandingDay,PFSL.LandingNight,PFSL.ApproachPrecision,PFSL.ApproachNonPrecision,          
          PFSL.Instrument,PFSL.FlightHours AS BLK_HRS,PFSL.FlightHours,NULL DutyHours,''DutyEnd,          
          PFSL.CrewID,PFSL.TakeOffDay,PFSL.TakeOffNight,'SL' AS RType,CONVERT(INT,'')  AS Actualduty,          
          C.FirstName+''+C.MiddleInitial+''+C.LastName AS CrewName           
    FROM PostflightSimulatorLog PFSL INNER JOIN Crew C ON PFSL.CrewID=C.CrewID    
         LEFT OUTER JOIN Aircraft A ON PFSL.AircraftID=A.AircraftID         
   WHERE  PFSL.SessionDT BETWEEN GETDATE()-400 AND GETDATE()    
     AND C.CrewID=@crew_code AND PFSL.IsDeleted = 0)       
     
-- select * from @TempPO   
SET @lnQtrSec=(SELECT DATEDIFF(hh, @ldQtrBeg,GETDATE()) as Hours_Difference)    
    
--SET @lnQtrHrs=(SELECT DATEDIFF(hh, @ldQtrBeg, GETDATE())*60*60 as Seconds_Difference)          
SET @lnQtrHrs= (SELECT DATEDIFF(day, @ldQtrBeg, getdate()) * 24)    
     
--UPDATE @TempPO SET Actualduty = CASE WHEN DutyEnd <> CONVERT(INT,'') THEN DutyHours END           
--UPDATE @TempPO SET Actualduty = CASE WHEN DutyEnd <> '' THEN DutyHours END           
--UPDATE @TempPO SET Actualduty = (SELECT DutyHours FROM @TempPO T1 WHERE T1.RowID IN(SELECT T2.RowID FROM  @TempPO T2 WHERE T2.RowID =T2.RowID +1 )           
                                                                    --AND T1.POLogID NOT IN (SELECT T3.POLogID FROM  @TempPO T3 WHERE T3.RowID =T3.RowID +1 ))          
    
IF @AircraftBasis=1           
 BEGIN        
  --UPDATE @TempPO SET Actualduty = (SELECT SUM(DutyHours) FROM @TempPO T1 WHERE T1.CrewID = @crew_code AND T1.ScheduledTM BETWEEN @ldQtrBeg and GETDATE())          
    
 SELECT A.AircraftCD,SUM(A.L_D90) LandingDay,SUM(A.L_N90)L_N90,SUM(A.Appr90)Appr90,SUM(A.Instr90)Instr90,SUM(A.days7)days7,MAX(A.dayrest7) dayrest7,SUM(A.CalMon)CalMon,SUM(A.days90) days90,MAX(A.CalQtrRest) CalQtrRest,SUM(A.CalQtr)CalQtr,SUM(A.Mon6)Mon6,
  
SUM(A.Prev2Qtrs)Prev2Qtrs,SUM(A.Mon12)Mon12,SUM(A.Calyr)Calyr,SUM(A.Days365)Days365,SUM(A.T_D90)T_D90,SUM(A.T_N90)T_N90,SUM(A.days30) days30 FROM    
 (SELECT T1.AircraftCD, T1.RType,         
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.DayLanding,0)) AND GETDATE() THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.NightLanding,0)) AND GETDATE() THEN ISNULL(SUM(T1.LandingNight),0)ELSE '0.0' END) L_N90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Approach,0)) AND GETDATE() THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Instrument,0)) AND GETDATE() THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Day7,0)) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) days7,          
    
  CASE WHEN (T1.RType = 'FL') THEN    
   --ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.RestDays,0)) AND GETDATE() THEN ISNULL(SUM(T1.Actualduty),0) ELSE '0.0' END),0)    
   ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.RestDays,0)) AND GETDATE() THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)          
  ELSE    
   ISNULL((T.RestDays*24),0)    
  END dayrest7,          
      
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalMon,          
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(Day90,0) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days90,          
     
  CASE WHEN (T1.RType = 'FL') THEN     
   --ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.Actualduty),0) ELSE '0.0' END),0)     
   ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)      
  ELSE    
   ISNULL(@lnQtrHrs,0)    
  END CalQtrRest,      
    
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalQtr,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL((T.Month6*30),0) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon6,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Prev2Qtrs,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL((T.Month12*30),0) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon12,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Calyr,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.Day365,0) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) Days365,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.TakeoffDay,0) AND GETDATE() THEN ISNULL(SUM(T1.TakeOffDay),0) ELSE '0.0' END) T_D90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.TakeoffNight,0) AND GETDATE() THEN ISNULL(SUM(T1.TakeoffNight),0) ELSE '0.0' END) T_N90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.Day30,0) AND GETDATE() THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days30,          
       T1.CrewID,T1.CrewName        
   FROM  Crew C    
       INNER JOIN @TEMP T ON C.CrewID = T.CrewID    
       INNER JOIN  @TempPO T1 ON C.CrewID = T1.CrewID     
       LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE  TP.CrewID = @crew_code AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID    
   WHERE     
  C.CrewID = @crew_code        
   GROUP BY T1.AircraftCD, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM)A       
GROUP BY A.AircraftCD--,A.CrewID,A.CrewName      
  END           
ELSE          
  BEGIN        
--UPDATE @TempPO SET Actualduty = CASE WHEN DutyEnd <> CONVERT(INT,'') THEN DutyHours END           
--UPDATE @TempPO SET Actualduty = CASE WHEN DutyEnd <> '' THEN DutyHours END          
--UPDATE @TempPO SET Actualduty = (SELECT DutyHours FROM @TempPO T1 WHERE T1.RowID IN(SELECT T2.RowID FROM  @TempPO T2 WHERE T2.RowID =T2.RowID +1 )           
--                                                                    AND T1.POLogID NOT IN (SELECT T3.POLogID FROM  @TempPO T3 WHERE T3.RowID =T3.RowID +1 ))          
-- UPDATE @TempPO SET Actualduty = (SELECT SUM(DutyHours) FROM @TempPO T1 WHERE T1.CrewID = @crew_code AND T1.ScheduledTM BETWEEN @ldQtrBeg and GETDATE())     
      
 SELECT A.AircraftCD,SUM(A.L_D90) LandingDay,SUM(A.L_N90)L_N90,SUM(A.Appr90)Appr90,SUM(A.Instr90)Instr90,SUM(A.days7)days7,MAX(A.dayrest7) dayrest7,SUM(A.CalMon)CalMon,SUM(A.days90) days90,MAX(A.CalQtrRest) CalQtrRest,SUM(A.CalQtr)CalQtr,SUM(A.Mon6)Mon6,
  
 SUM(A.Prev2Qtrs)Prev2Qtrs,SUM(A.Mon12)Mon12,SUM(A.Calyr)Calyr,SUM(A.Days365)Days365,SUM(A.T_D90)T_D90,SUM(A.T_N90)T_N90,SUM(A.days30) days30 FROM       
 (SELECT T1.AircraftCD, T1.RType,    
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.DayLanding,0)) AND GETDATE() THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.NightLanding,0)) AND GETDATE() THEN ISNULL(SUM(T1.LandingNight),0) ELSE '0.0' END) L_N90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Approach,0)) AND GETDATE() THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Instrument,0)) AND GETDATE() THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,    
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.Day7,0)) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0) ELSE '0.0' END) days7,          
    
  CASE WHEN (T1.RType = 'FL') THEN     
   --ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.RestDays,0)) AND GETDATE() THEN ISNULL(SUM(T1.Actualduty),0)ELSE 0.0 END),0)     
   ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (GETDATE()-ISNULL(T.RestDays,0)) AND GETDATE() THEN ISNULL(SUM(TM.DutyHours),0)ELSE 0.0 END),0)    
       ELSE    
   ISNULL((T.RestDays*24),0)    
       END dayrest7,          
    
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalMon,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(Day90,0) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days90,        
    
  CASE WHEN (T1.RType = 'FL') THEN        
   --ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.Actualduty),0)ELSE '0.0' END),0)     
    ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(TM.DutyHours),0)ELSE '0.0' END),0)             
  ELSE    
   ISNULL((@lnQtrHrs),0)    
  END CalQtrRest,          
    
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalQtr,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL((T.Month6*30),0) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon6,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Prev2Qtrs,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL((T.Month12*30),0) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon12,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Calyr,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.Day365,0) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Days365,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.TakeoffDay,0) AND GETDATE() THEN ISNULL(SUM(T1.TakeOffDay),0)ELSE '0.0' END) T_D90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.TakeoffNight,0) AND GETDATE() THEN ISNULL(SUM(T1.TakeoffNight),0)ELSE '0.0' END) T_N90,          
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  GETDATE()-ISNULL(T.Day30,0) AND GETDATE() THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days30,          
       T1.CrewID,T1.CrewName      
   FROM   Crew C         
  INNER JOIN @TEMP T ON C.CrewID = T.CrewID    
  INNER JOIN @TempPO T1 ON C.CrewID = T1.CrewID    
  LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE  TP.CrewID = @crew_code AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID       
   WHERE     
  C.CrewID = @crew_code         
   GROUP BY T1.AircraftCD, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM) A    
   GROUP BY A.AircraftCD--,A.CrewID,A.CrewName     
   END          
END      
  
GO

