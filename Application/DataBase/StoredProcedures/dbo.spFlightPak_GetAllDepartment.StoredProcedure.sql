
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllDepartment]    Script Date: 05/07/2013 14:18:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllDepartment]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllDepartment]    Script Date: 05/07/2013 14:18:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spFlightPak_GetAllDepartment]    
(    
 @CustomerID  BIGINT,  
 @ClientID BIGINT  
)    
AS    
BEGIN    
SET NOCOUNT ON;    
IF(@ClientID>0)  
BEGIN  
SELECT   
            [DepartmentID]  
           ,[DepartmentCD]  
           ,[DepartmentName] 
           ,[DepartPercentage] 
           ,[CustomerID]  
           ,[ClientID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsInActive]  
           ,[IsDeleted]  
 FROM [Department]    
 WHERE CustomerID  = @CustomerID    
 AND ISNULL(IsDeleted,0) = 0  and ClientID=@ClientID  
  order by DepartmentCD
 END    
 ELSE  
 BEGIN  
 SELECT   
            [DepartmentID]  
           ,[DepartmentCD]  
           ,[DepartmentName]  
           ,[DepartPercentage]
           ,[CustomerID]  
           ,[ClientID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsInActive]  
           ,[IsDeleted]  
 FROM [Department]    
 WHERE CustomerID  = @CustomerID    
 AND ISNULL(IsDeleted,0) = 0  
  order by DepartmentCD
 END  
 END


GO


