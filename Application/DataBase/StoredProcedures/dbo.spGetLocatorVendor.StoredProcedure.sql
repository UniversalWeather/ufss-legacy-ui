
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLocatorVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLocatorVendor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetLocatorVendor]  
(  
 @CustomerID BIGINT,   
 @AirportID BIGINT,  
 @IcaoID char(4),   
 @Iata char(3),  
 @StateName varchar(25),  
 @CityName varchar(25),  
 @CountryCD char(3),  
 @CountryID BIGINT,   
 @MetroID BIGINT,   
 @MetroCD char(3),  
 @Name varchar(40),  
 @MilesFrom int,  
 @AircraftID bigint,  
 @AircraftCD varchar(10)  
   
  
 )    
AS    
-- =============================================    
-- Author:   
-- Create date:     
-- Description: Get the Locator information of Vendor   
-- Modification History: Updated by Ramesh on 5th Nov 2013, for MapQuest plotting
--						 Added 2 new columns OrigLatitude and OrigLongitude
-- =============================================    
BEGIN  
  
SET FMTONLY OFF    
   if  @IcaoID =''   
  set @IcaoID = null  
   if @Iata =''   
  set @Iata = null  
   if @CountryCD =''   
  set @CountryCD = null  
   if @CityName =''   
  set @CityName = null  
   if @MetroCD =''   
  set @MetroCD = null  
   if @StateName =''   
  set @StateName = null  
   if @Name =''   
  set @Name = null  
  if @AircraftCD =''   
  set @AircraftCD = null   
	 

SELECT distinct  A.[AirportID]  
        ,A.[IcaoID]  
        ,A.[Iata]  
        , v.[CustomerID]  
  ,v.BillingCity  
  ,v.BillingState     
  , v.[CountryID]  
  ,v.Name     
  , v.[MetroID]  
  ,M.MetroCD  
  ,C.CountryCD  
  ,0 as 'Miles'  
  ,V.BusinessPhone,  
  -- Aircraft  
  --start added for miles calc  
 case   
 when UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180   
 END LatitudeRad,     
 case  
 when UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180   
 END LongitudeRad,   
 --end added for miles calc        
--start added for miles calc  
 case   
 when UPPER(V.LatitudeNorthSouth) = 'S' Then (0-(V.LatitudeDegree + (V.LatitudeMinutes / 60))) * PI() / 180  else (V.LatitudeDegree + (V.LatitudeMinutes / 60)) * PI() / 180   
 END LatitudeRadCityCenter,  
  case
  when UPPER(V.LatitudeNorthSouth) = 'S' Then (0-(V.LatitudeDegree + (V.LatitudeMinutes / 60))) else (V.LatitudeDegree + (V.LatitudeMinutes / 60))
  END OrigLatitude, 
 case  
 when UPPER(V.LongitudeEastWest) = 'W' Then (0-(V.LongitudeDegree + (V.LongitudeMinutes / 60))) * PI() / 180  else (V.LongitudeDegree + (V.LongitudeMinutes / 60)) * PI() / 180   
 END LongitudeRadCityCenter,
 case
  when UPPER(V.LongitudeEastWest) = 'W' Then (0-(V.LongitudeDegree + (V.LongitudeMinutes / 60))) else (V.LongitudeDegree + (V.LongitudeMinutes / 60))
  END OrigLongitude
--end added for miles calc     
 ,AC.AircraftCD  
 into  #TempResultTable     
    
     
  FROM  [Vendor] V  
   LEFT OUTER JOIN Airport A on V.AirportID = A.AirportID  
    LEFT OUTER JOIN [metro] M on V.MetroID = M.MetroID      
    LEFT OUTER JOIN [Country] C on V.CountryID = C.CountryID    
    --LEFT OUTER JOIN [Airport] A on V.AirportID = A.AirportID   
    LEFT OUTER JOIN [Fleet] F on V.VendorID = F.VendorID   
    LEFT OUTER JOIN [Aircraft] AC on F.AircraftID = AC.AircraftID   
    where  
    --LEFT OUTER JOIN [Aircraft] Ar on V.airc   
    ((V.CustomerID = @CustomerID)   
  -- OR (ISNULL(@AirportID,0))=0 OR A.AirportID = @AirportID) and  
  OR (A.AirportID = @AirportID and V.CustomerID = @CustomerID)) and  
 ISNULL(A.IcaoID, '') = case when @IcaoID is not null then @IcaoID else ISNULL(A.IcaoID, '') end and  
   ISNULL(A.Iata, '') = case when @Iata is not null then @Iata else ISNULL(A.Iata, '') end and  
   ISNULL(C.CountryCD, '') = case when @CountryCD  is not null then @CountryCD  else ISNULL(C.CountryCD , '') end  and  
   ISNULL(M.MetroCD, '') = case when @MetroCD is not null then @MetroCD else ISNULL(M.MetroCD, '') end and  
   ISNULL(V.BillingCity, '') = case when @CityName is not null then @CityName else ISNULL(V.BillingCity, '') end and  
   ISNULL(v.BillingState, '') = case when @StateName is not null then @StateName else ISNULL(v.BillingState, '') end and  
   ISNULL(V.Name, '') = case when @Name is not null then @Name else ISNULL(V.Name, '') end and  
   ISNULL(AC.AircraftID, '') = case when (@AircraftID is not null AND @AircraftID != 0) then @AircraftID else ISNULL(AC.AircraftID, '') end and  
       --  (V.BillingCity = @CityName   OR @CityName is NULL Or   @CityName = '')  and  
       --(v.BillingState = @StateName   OR @StateName is NULL Or   @StateName = '')  and  
       --  (V.Name = @Name   OR @Name is NULL Or   @Name = '')  and       
    V.IsDeleted = 0 and  V.VendorType= 'V'  
   
  
SELECT   [AirportID]  
        ,[IcaoID]  
        ,[Iata]  
        ,[CustomerID]  
  ,BillingCity  
  ,BillingState     
  ,[CountryID]  
  ,Name     
  ,[MetroID]  
  ,MetroCD  
  ,CountryCD  
  ,ISNull((cast( acos(sin(LatitudeRadCityCenter) * sin(LatitudeRad) + cos(LatitudeRadCityCenter) * cos(LatitudeRad) * cos(LongitudeRad - (LongitudeRadCityCenter))) * 3959   as Int)),0)  as Miles  
  ,BusinessPhone  
  ,AircraftCD,   
  OrigLatitude, OrigLongitude  
  from #TempResultTable  
 WHERE Miles <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END  
ORDER BY Name  
  
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 
  
END  