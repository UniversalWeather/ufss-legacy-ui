/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRDispatchNotes]    Script Date: 02/08/2013 18:25:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_AddCRDispatchNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRDispatchNotes]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRDispatchNotes]
(
            
            @CustomerID bigint,
            @CRMainID bigint,
            @CRDispatchNotes varchar(max),
            @LastUpdUID varchar(30),
            @LastUpdTS datetime,
            @IsDeleted bit
)

AS
BEGIN 
		DECLARE @CRDispatchNotesID Bigint
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRDispatchNotesID OUTPUT 
		
INSERT INTO [dbo].[CRDispatchNotes]
           (
            [CRDispatchNotesID]
           ,[CustomerID]
           ,[CRMainID]
           ,[CRDispatchNotes]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           )
     VALUES
           (
            @CRDispatchNotesID
           ,@CustomerID
           ,@CRMainID
           ,@CRDispatchNotes
           ,@LastUpdUID
           ,@LastUpdTS
           ,@IsDeleted
           )
           
           Select @CRDispatchNotesID as CRDispatchNotesID  
		
				  
END




GO


