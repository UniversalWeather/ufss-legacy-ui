

/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_QueueTransfer]    Script Date: 09/10/2013 18:25:56 ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_QueueTransfer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_QueueTransfer]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_QueueTransfer]    Script Date: 09/10/2013 18:25:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_QueueTransfer]       
(@CRMainID BIGINT        
,@CustomerID BIGINT        
,@LastUpdUID VARCHAR(30))      

-- =============================================        
-- Author:Karthikeyan.S        
-- Create date: 09/10/2013        
-- Description: Modified the Queue Transfer Proc.      
-- =============================================  
  AS
   BEGIN             
      DECLARE @LastUpdTS DATETIME = Getutcdate()             
      DECLARE @CurrentDate DATETIME = Getdate()             
      DECLARE @NewTripID BIGINT             
      DECLARE @NewTripNUM BIGINT             
      DECLARE @IsAutoRevisionNum BIT             
      DECLARE @IsAutoDispatch BIT             
      DECLARE @TripStatus VARCHAR(2)             
      DECLARE @EstDeptDate DATE             
      DECLARE @TripCost NUMERIC(12, 2)             
      DECLARE @CRTRIPNUM INT             
      DECLARE @CRLegnum INT             
            
      --Get Auto dispatch number based on the userid                                  
      SELECT @IsAutoDispatch = isautomaticdispatchnum,             
             @IsAutoRevisionNum = isautorevisionnum,             
             @TripStatus = tripmgrdefaultstatus             
      FROM   company             
      WHERE  homebaseid = (SELECT homebaseid             
                           FROM   usermaster             
                           WHERE  username = @LastUpdUID)             
            
      -- Get trip id value to check whether it new transfer or updation                                 
      SELECT @NewTripID = tripid             
      FROM   crmain             
      WHERE  crmainid = @CRMainID             
            
      --check whether it is new transfer or updation                                 
      IF ( @NewTripID = 0 )             
          OR ( @NewTripID IS NULL )             
        BEGIN             
            -- Generate new trip id                                  
            EXECUTE dbo.Usp_getsequencenumber             
              @CustomerID,             
              'PreflightCurrentNo',             
              @NewTripID output             
            
            -- Generate new Trip num                                 
            SET @NewTripNUM = 0             
            
            EXECUTE dbo.Usp_getsequencenumber             
              @CustomerID,             
              'TripCurrentNo',             
              @NewTripNUM output             
            
            -- Get Current trip number from sequence table -- this query to checked                                 
            SELECT @NewTripNUM = tripcurrentno             
            FROM   [flightpaksequence]             
            WHERE  customerid = @CustomerID             
            
            -- Get Current trip number from crmain table -- this query to checked                                 
            SELECT @CRTRIPNUM = crtripnum             
            FROM   crmain             
            WHERE  crmainid = @CRMainID             
            
            -- Copy the CRMain record to temp table                                 
            SELECT crmainid,             
                   customerid,             
                   crtripnum,             
                   approver,             
                   tripid,             
                   tripstatus,             
                   tripsheetstatus,             
                   acknowledgementstatus,             
                   charterquotestatus,             
                   corporaterequeststatus,             
                   travelcoordinatorid,             
                   travelcoordinatorname,             
                   travelcoordinatorphone,             
                   travelcoordinatorfax,           
                   dispatchnum,             
                   estdeparturedt,             
                   estarrivaldt,             
   recordtype,             
                   fleetid,             
                   aircraftid,             
                   passengerrequestorid,             
                   requestorname,             
                   requestorphone,             
                   departmentid,             
                   departmentdescription,            
                   authorizationid,             
                   authorizationdescription,             
                   homebaseid,             
                   clientid,             
                   isprivate,             
                   crmaindescription,             
                   requestdt,             
                   beginninggmtdt,             
                   endinggmtdt,             
                   iscrew,             
                   ispassenger,             
                   isalert,             
                   isairportalert,             
                   islog,             
                   lastupduid,             
                   lastupdts,             
                   isdeleted             
            INTO   #tmpcrmain             
            FROM   crmain             
            WHERE  crmainid = @CRMainID             
            
            -- Copy the CRLeg record to temp table                                  
            SELECT crlegid,             
                   customerid,             
                   crmainid,             
                   tripnum,             
                   legnum,             
                   dairportid,             
                   aairportid,             
                   departurecity,             
                   arrivalcity,             
                   departuredttmlocal,             
                   arrivaldttmlocal,             
                   passengerrequestorid,             
                   departmentid,             
                   authorizationid,             
                   flightcategoryid,             
                   isprivate,             
                   distance,             
                   elapsetm,             
                   passengertotal,             
                   powersetting,             
                   takeoffbias,             
                   landingbias,             
                   trueairspeed,             
                   windsboeingtable,             
                   isdepartureconfirmed,             
                   isarrivalconfirmation,             
                   isapproxtm,             
                   isscheduledservices,             
                   departuregreenwichdttm,             
                   arrivalgreenwichdttm,             
                   homedeparturedttm,             
                   homearrivaldttm,             
                   negotiatetime,             
                   logbreak,             
                   flightpurpose,             
                   seattotal,             
                   reservationtotal,             
                   reservationavailable,             
                   waitnum,             
                   duty_type,             
                   dutyhours,             
                   resthours,             
                   flighthours,             
                   notes,             
                   fuelload,             
                   outboundinstruction,             
                   dutytype,             
                   iscrewdiscount,             
                   crewcurrency,             
                   isdutyend,             
                   fedaviationregnum,             
                   legid,             
                   windreliability,             
                   croverride,             
                   checkgroup,             
                   airportalertcd,             
                   additionalcrew,             
                   pilotincommand,             
                   secondincommand,             
                   nextlocaldttm,             
                   nextgmtdttm,             
                   nexthomedttm,             
                   crewnotes,        
                   lastupduid,             
                   lastupdts,             
                   isdeleted,             
                   fboid,             
                   clientid,             
                   estfuelqty,             
                   fuelunits             
            INTO   #tmpcrleg             
            FROM   crleg             
            WHERE  crmainid = @CRMainID             
            
            -- Need to modify                                 
            --SELECT @EstDeptDate = CONVERT(DATE, MIN(DepartureDTTMLocal)),@TripCost = SUM(Cost)  FROM #tmpCRLeg                                
            SELECT @EstDeptDate = CONVERT(DATE, Min(departuredttmlocal))             
            FROM   #tmpcrleg             
            
            -- Insert the Crmain record into preflightmain table                                 
            INSERT INTO preflightmain             
                        (tripid,             
                         customerid,             
                         tripnum,             
                         previousnum,             
                         tripdescription,             
                         fleetid,             
                      dispatchnum,             
                         tripstatus,             
                         estdeparturedt,             
                         estarrivaldt,             
                         recordtype,             
                         aircraftid,             
                         passengerrequestorid,             
                         requestorfirstname,             
                         requestormiddlename,             
                         requestorlastname,             
                         requestorphonenum,             
                   departmentdescription,             
                         authorizationdescription,             
                         requestdt,             
                         iscrew,             
                         ispassenger,             
                         isquote,             
                         isscheduledservices,             
                         isalert,             
                         notes,             
                         logisticshistory,             
                         homebaseid,             
                         clientid,             
                         isairportalert,             
                         islog,             
                         beginninggmtdt,             
                         endinggmtdt,             
                         lastupduid,             
                         lastupdts,             
                         isprivate,             
                         iscorpreq,             
                         acknowledge,             
                         dispatchnotes,             
                         triprequest,             
                         waitlist,             
                         flightnum,             
                         flightcost,             
                         accountid,             
                         tripexception,             
                         tripsheetnotes,             
                         canceldescription,             
                         iscompleted,             
                         isfboupdflag,             
                         revisionnum,             
                         revisiondescriptioin,             
                         ispersonal,             
                         dsptnname,             
                         schedulecalendarhistory,                                      schedulecalendarupdate,             
                         commentsmessage             
                         --,EstFuelQTY    -------karthik                                 
                         ,             
                         crewid,             
                         releasedby,             
                         isdeleted,             
                         verifynum,             
                         apissubmit,             
                  apisstatus,             
                         apisexception,             
                         isapisvalid,             
                         departmentid,             
                         authorizationid,             
                         dispatcherusername,             
                         emergencycontactid)             
            SELECT @NewTripID,             
                   @CustomerID,             
                   @NewTripNUM,             
                   0,         
                   crmaindescription,             
                   [fleetid],             
                   CASE             
                     WHEN @IsAutoDispatch = 0 THEN NULL             
                     ELSE Cast(@CRTRIPNUM AS VARCHAR(12))             
                   END             
                   -- ,case when rtrim(ltrim(isnull([DispatchNUM],'')))='' THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END                                 
                   ,             
                   @TripStatus --'T'                                     
                   ,            
                   @EstDeptDate,             
                   NULL --[EstArrivalDT]                                     
                   ,             
                   'T' --RecordType                                 
                   ,             
                   [aircraftid],             
                   passengerrequestorid,             
                   NULL --[RequestorFirstName]                                     
                   ,             
                   NULL --[RequestorMiddleName]                                     
                   ,             
                   NULL --[RequestorLastName]                              
                   ,             
                   NULL --[RequestorPhoneNUM]                                     
                   ,             
                   NULL --[DepartmentDescription]                                     
                   ,             
                   NULL --[AuthorizationDescription]                                     
                   ,             
                   @CurrentDate-- [RequestDT]                                     
                   ,             
                   NULL -- [IsCrew]                                     
                   ,             
                   NULL --[IsPassenger]                                     
                   ,             
                   NULL --[IsQuote]                            
                   ,             
                   NULL --[IsScheduledServices]                                     
                   ,             
                   NULL --[IsAlert]                                     
                   ,             
                   NULL --[Notes]                                     
                   ,             
                   NULL --[LogisticsHistory]                                     
                   ,             
                   [homebaseid],             
                   [clientid],             
                   NULL --[IsAirportAlert]                                     
                   ,             
                   0,             
                   beginninggmtdt,             
                   endinggmtdt,             
                   [lastupduid],             
                   [lastupdts] --@LastUpdUID                                  
                   ,                                0,             
                   1--[IsCorpReq]                                     
                   ,             
                   NULL--[Acknowledge]                                     
                   ,             
                   NULL--[DispatchNotes]                                     
                   ,             
                   NULL--[TripRequest]                           
                   ,             
                   NULL--[WaitList]                                     
                   ,             
                   NULL--[FlightNUM]                                     
             ,             
                   @TripCost,             
                   NULL--[AccountID]                                     
                   ,             
                   NULL--[TripException]                                     
                   ,             
                   NULL--[TripSheetNotes]                                     
                   ,             
                   NULL--[CancelDescription]                                     
                   ,             
                   NULL--[IsCompleted]                                     
                   ,             
                   NULL--[IsFBOUpdFlag]               
                   --,--case when @IsRevision=0  then null  else 1 end                                      
                   ,             
                   CASE             
                     WHEN Ltrim(Rtrim(Isnull(@IsAutoRevisionNum, 0))) = 0 THEN             
                     NULL             
                     ELSE 1             
                   END,             
                   NULL --[RevisionDescriptioin]                                     
                   ,             
                   NULL --[IsPersonal]                                     
                   ,             
                   NULL --[DsptnName]                                     
                   ,             
                   NULL --[ScheduleCalendarHistory]                                     
                   ,             
                   NULL --[ScheduleCalendarUpdate]                                     
                   ,             
                   NULL --[CommentsMessage]                                     
                   --- ,null --[EstFuelQTY]                                     
                   ,             
                   NULL --[CrewID]                                     
                   ,             
                   NULL --[ReleasedBy]                                     
                   ,             
                   0,             
                   NULL --[VerifyNUM]                                     
                   ,             
                   NULL--[APISSubmit]                                     
                   ,             
                  NULL --[APISStatus]                                     
                   ,             
                   NULL --[APISException]                                     
                   ,             
                   NULL --[IsAPISValid]                                     
                   ,             
                   departmentid,             
                   authorizationid,             
                   NULL --[DispatcherUserName]                                     
                   ,             
                   NULL --[EmergencyContactID]                           
            FROM   #tmpcrmain             
            
            -- Create temp table for copy leg details                                 
            CREATE TABLE #templeg             
              (             
                 rownum   INT IDENTITY (1, 1),             
                 newlegid BIGINT,             
                 crlegid  BIGINT             
              )             
            
            -- copy leg records into another temp table for further process                                 
            INSERT INTO #templeg             
                        (newlegid,             
                         crlegid)             
            SELECT NULL,             
                   crlegid             
            FROM   #tmpcrleg             
            
            DECLARE @Rownumber INT = 1             
            DECLARE @NewLegID BIGINT             
            
            -- Loop through for generating the New leg id                                 
            WHILE (SELECT Count(1)             
                   FROM   #templeg             
                   WHERE  newlegid IS NULL) > 0             
              BEGIN             
                  SET @NewLegID = 0             
            
                  EXECUTE dbo.Usp_getsequencenumber             
                    @CustomerID,             
                    'PreflightCurrentNo',             
                    @NewLegID output             
            
                  UPDATE #templeg             
                  SET    newlegid = @NewLegID             
                  WHERE  rownum = @Rownumber             
            
                  SET @Rownumber = @Rownumber + 1             
              END             
            
            -- Insert the temp leg record into preflightleg table                               
            INSERT INTO preflightleg             
                        (legid,             
                         tripid,             
                         customerid,             
                         tripnum,             
                         legnum,             
       departicaoid,             
                         arriveicaoid,             
                         distance,             
                         powersetting,             
                         takeoffbias,             
                         landingbias,             
                         trueairspeed,             
                         windsboeingtable,             
                         elapsetm,             
                         isdepartureconfirmed,             
                         isarrivalconfirmation,             
                         isapproxtm,             
                         isscheduledservices,             
                         departuredttmlocal,             
                         arrivaldttmlocal,             
                         departuregreenwichdttm,             
                         arrivalgreenwichdttm,             
                         homedeparturedttm,             
                         homearrivaldttm,             
                         gotime,             
                         fboid,             
                         logbreak,             
                         flightpurpose,             
                         passengerrequestorid,             
                         requestorname,             
                         departmentid,             
                         departmentname,             
                         authorizationid,             
                         authorizationname,             
                         passengertotal,             
                         seattotal,             
                         reservationtotal,             
                         reservationavailable,             
                         waitnum,             
                         dutytype,             
                         dutyhours,             
                         resthours,             
                         flighthours,             
                         notes,             
                         fuelload,             
                         outbountinstruction,             
                         dutytype1,             
                         iscrewdiscount,             
                         crewdutyrulesid,             
                         isdutyend,             
                         fedaviationregnum,   
                         clientid,             
                         legid1,             
                         windreliability,             
                         overridevalue,             
                         checkgroup,             
                         additionalcrew,             
                         pilotincommand,             
                         secondincommand,             
                         nextlocaldttm,             
                         nextgmtdttm,             
                         nexthomedttm,             
                         lastupduid,             
                         lastupdts,             
                         crewnotes,             
                         isprivate,             
                         waitlist,             
                         flightnum,             
                         flightcost,             
                         crewfuelload,             
             isdeleted,             
                         uwaid,             
                         uscrossing,             
                         response,             
                         confirmid,             
                         crewdutyalert,             
                         accountid,             
                         flightcategoryid,             
                         estfuelqty,             
                         fuelunits)             
            SELECT newlegid,             
                   @NewTripID,             
                   @CustomerID,             
                   NULL --[TripNUM]                                     
                   ,             
                   [legnum],             
                   dairportid,             
                   aairportid,             
                   [distance],             
                   [powersetting],             
                   [takeoffbias],             
                   [landingbias],             
                   [trueairspeed],             
                   [windsboeingtable],             
                   [elapsetm],             
                   [isdepartureconfirmed],             
                   [isarrivalconfirmation],             
                   0 --[IsApproxTM]                                     
                   ,             
                   NULL --[IsScheduledServices]                                      
                   ,             
                   departuredttmlocal,             
                   arrivaldttmlocal,             
                   departuregreenwichdttm,             
                   arrivalgreenwichdttm,             
                   homedeparturedttm,             
                   homearrivaldttm,             
                   NULL --[GoTime]                                     
                   ,             
                   NULL --[FBOID]                                     
                   ,             
                   NULL --[LogBreak]                                     
                   ,             
                   NULL --[FlightPurpose]         
                   ,             
                   passengerrequestorid,             
                   NULL --[RequestorName]                                     
                   ,             
                   departmentid,             
                   NULL --[DepartmentName]                                     
                   ,             
                   authorizationid,             
                   NULL --[AuthorizationName]                                     
                   ,             
                   IP.passengertotal --PassengerTotal                                    
                   ,             
                   IP.seattotal--[@SeatTotal]                                 
                   ,             
                   IP.reservationtotal,             
                   IP.reservationavailable,             
                   NULL --[WaitNUM]                                     
                   ,             
                   NULL --[DutyTYPE]                                     
                   ,             
                   NULL --[DutyHours]                                     
                   ,             
                   NULL --[RestHours]                                     
      ,             
                   NULL --[FlightHours]                                     
                   ,             
                   NULL --[Notes]                                     
                   ,             
                   NULL --[FuelLoad]                                     
                   ,             
                   NULL --[OutbountInstruction]                             
                   ,             
                   NULL --[DutyTYPE1]                                     
                   ,             
                   NULL --[IsCrewDiscount]                                     
                   ,             
                   NULL --[CrewDutyRulesID]                                     
               ,             
                   0 --[IsDutyEnd]                                     
                   ,             
                   NULL --[FedAviationRegNUM]                                     
                   ,             
                   clientid,             
                   NULL --[LegID1]                                     
                   ,             
                   [windreliability],             
                   NULL --[OverrideValue]                                     
                   ,             
                   NULL --[CheckGroup]                                     
                   ,             
                   NULL --[AdditionalCrew]                       
                   ,             
                   NULL --[PilotInCommand]                                     
                   ,             
                   NULL --[SecondInCommand]                                     
                   ,             
                   NULL --NextLocalDTTM    --need to modify                                 
                   ,             
                   NULL             
                   --[NextGMTDTTM]        --need to modify                                 
                   ,             
                   NULL             
                   --[NextHomeDTTM]        --need to modify                                 
                   ,             
                   [lastupduid],             
                   [lastupdts] --@LastUpdUID                                 
                   ,             
                   NULL --[CrewNotes]                                     
                   ,             
                   0,             
                   NULL --[WaitList]                                     
                   ,             
                   NULL --[FlightNUM]                                     
                   ,             
                   0 --cost                        
                   ,             
                   NULL --[CrewFuelLoad]                                     
                   ,             
                   0,             
                   NULL --[UWAID]                                     
                   ,             
                   NULL --[USCrossing]                                     
                   ,             
                   NULL --[Response]                                     
                   ,             
                   NULL --[ConfirmID]                                     
                   ,             
                   NULL --[CrewDutyAlert]                                     
                   ,             
                   NULL --[AccountID]                                     
                   ,             
                   flightcategoryid,             
                   estfuelqty,           
                   fuelunits             
            FROM   #tmpcrleg IP,             
                   #templeg T             
            WHERE  IP.crlegid = T.crlegid                         
                     
            
        /* Moving CRPassenger To PreflightPassengerList starts here */             
            -- Move the Leg details to another temp table                                 
            SELECT newlegid,             
                   crlegid             
            INTO   #tmpleg             
            FROM   #templeg             
            
            DECLARE @CurrentCRLegID BIGINT             
            
            -- Looping Leg record one by one                                 
            -- Start of While loop for #tmpLeg                                 
            WHILE (SELECT Count(*)             
                   FROM   #tmpleg) > 0             
              BEGIN             
                  -- Get the Cr Leg id and  preflight new leg id                                  
                  SELECT TOP 1 @CurrentCRLegID = crlegid,             
                               @NewLegID = newlegid             
                  FROM   #tmpleg             
            
                  EXECUTE Spflightpak_corporaterequest_queuetransferinfo             
                    @CRMainID,             
                    @CustomerID,             
                    @LastUpdUID,             
                    @NewLegID,             
                    @CurrentCRLegID             
            
                  DELETE FROM #tmpleg             
                  WHERE  crlegid = @CurrentCRLegID             
              END             
            
            -- End of While loop for #tmpLeg                                 
            -- drop all temp table here                                 
            --DROP TABLE #tmpCRLeg                                 
            --DROP TABLE #tmpCRMain                                 
            /* Moving CRPassenger To PreflightPassengerList Ends here */             
            UPDATE crmain             
            SET    tripid = @NewTripID,             
                   tripstatus = @TripStatus,             
                   corporaterequeststatus = CASE             
                                              WHEN @TripStatus != 'C' THEN 'T'             
                                              ELSE 'C'             
                                            END,             
                   lastupduid = @LastUpdUID,             
                   lastupdts = @LastUpdTS             
  WHERE  crmainid = @CRMainID             
            
            DROP TABLE #tmpcrleg             
            
            DROP TABLE #tmpcrmain             
            
            -- Create temp table for copy leg details                                 
            CREATE TABLE #temphistory             
              (             
                 revisionno INT,             
                 lastdate   DATETIME             
              )             
            
            -- copy leg records into another temp table for further process                                 
            INSERT INTO #temphistory             
                        (revisionno,             
                         lastdate)             
            SELECT revisionnumber,             
                   lastupdts             
            FROM   crhistory             
            WHERE  crmainid = @CRMainID             
                   AND customerid = @CustomerID             
            ORDER  BY revisionnumber DESC             
            
            DECLARE @FirHistoryDescription VARCHAR(max)             
            DECLARE @FirHistoryDescr VARCHAR(max)             
            DECLARE @FirHisDesc VARCHAR(max)             
            DECLARE @NewFirHisDesc VARCHAR(max)             
            DECLARE @LstUpdTS DATETIME             
            
            --SET @FirHisDesc  = 'Acknowledged Changes Made By Dispatch.'                                  
            SET @NewFirHisDesc = 'Creating New Tripsheet From Corp. Request: '             
                            + Cast(@CRTRIPNUM AS VARCHAR(50))             
            
            DECLARE @CurrentCRRevisionNum BIGINT             
            
            WHILE (SELECT Count(*)             
                   FROM   #temphistory) > 0             
              BEGIN             
                  -- Get the Cr revision number                                  
                  SELECT TOP 1 @CurrentCRRevisionNum = revisionno,             
                               @LstUpdTS = lastdate             
                  FROM   #temphistory             
            
                  SET @FirHistoryDescr = (SELECT logisticshistory             
                                          FROM   crhistory             
                                          WHERE  crmainid = @CRMainID             
                                                 AND customerid = @CustomerID             
                                                 AND revisionnumber =             
                                                     @CurrentCRRevisionNum)             
                  SET @FirHistoryDescription = Isnull(@NewFirHisDesc, '') +             
                                               '<br />'             
                                               + Isnull(@FirHistoryDescr, '')             
                  SET @FirHisDesc = NULL             
                  SET @NewFirHisDesc =NULL             
            
                  EXECUTE Spflightpak_preflight_addpreflighttriphistory             
                    @NewTripID,             
                    @CustomerID,             
                    @FirHistoryDescription,             
                    @LastUpdUID,             
                    @LstUpdTS             
            
                  DELETE FROM #temphistory             
                  WHERE  revisionno = @CurrentCRRevisionNum             
              END             
            
            DROP TABLE #temphistory            
                    
                  
                -- First time click issues fix    
            exec [Spflightpak_corporaterequest_queuetransfer_AddUpdateAdditionalinfo] @CRMainID,@CustomerID ,@LastUpdUID        
                    
                    
                    
                     
        END             
      ELSE             
      BEGIN  
       exec [Spflightpak_corporaterequest_queuetransfer_AddUpdateAdditionalinfo] @CRMainID,@CustomerID ,@LastUpdUID      
      END  
        
--        BEGIN             
--            -- LEELA CODE TO BE INSERTED HERE                                 
--            DECLARE @LastCRLegNum INT             
            
--            SET @LastCRLegNum = 1             
            
--            DECLARE @PrefLegnum INT             
--            DECLARE @MaxCRLegnum INT             
--            DECLARE @MaxPrefLegnum INT             
--            DECLARE @Totlegs INT             
            
--            IF EXISTS (SELECT tripid             
--                       FROM   crmain             
--          WHERE  tripid = @NewTripID)             
--              BEGIN             
--                  UPDATE preflightmain             
--                  SET    [tripstatus] = crmain.tripstatus             
--                         --,[TripSheetStatus] = @TripSheetStatus                                 
--                         --,[AcknowledgementStatus] = ''    -- need to modify                                 
--                         --,[CorporateRequestStatus] = 'A'  -- need to modify                                 
--                         ,             
--                         [dispatchnum] = crmain.dispatchnum,             
--                         [estdeparturedt] = crmain.estdeparturedt,             
--                         tripdescription = crmain.crmaindescription,             
--        [estarrivaldt] = crmain.estarrivaldt,             
--                         [recordtype] = 'T',             
--                         [fleetid] = crmain.fleetid,             
--   [aircraftid] = crmain.aircraftid,             
--                         [passengerrequestorid] = crmain.passengerrequestorid,             
--                         [departmentid] = crmain.departmentid,             
--                         [authorizationid] = crmain.authorizationid,             
--                         [homebaseid] = crmain.homebaseid,             
--                         [clientid] = crmain.clientid,             
--                         [isprivate] = crmain.isprivate,             
--                         [requestdt] = crmain.requestdt,             
--                         [beginninggmtdt] = crmain.beginninggmtdt,             
--  [endinggmtdt] = crmain.endinggmtdt,             
--                         [iscrew] = crmain.iscrew,             
--                         [ispassenger] = crmain.ispassenger,             
--                         [isalert] = crmain.isalert,             
--                         [isairportalert] = crmain.isairportalert,             
--                         [islog] = crmain.islog,             
--                         [lastupduid] = @LastUpdUID,             
--                         [lastupdts] = @LastUpdTS             
--                  FROM   preflightmain             
--                         JOIN crmain             
--                           ON crmain.tripid = preflightmain.tripid             
--                  WHERE  crmain.tripid = @NewTripID             
            
--                  --- ends here                                 
--                  ---Matching Records                                 
--                  WHILE (SELECT Count(1)             
--  FROM   crleg             
--                                JOIN preflightleg             
--                                  ON crleg.legnum = preflightleg.legnum             
--                         WHERE  crleg.crmainid = @CRMainID             
--                                AND crleg.legnum = @LastCRLegNum             
--                                AND preflightleg.tripid = @NewTripID             
--                                AND crleg.isdeleted = 0) > 0             
--                    BEGIN             
--                        ---Delete legs if not exists                                 
--                        SET @MaxCRLegnum = (SELECT Max(legnum)             
--                                            FROM   crleg             
--                                            WHERE  crleg.crmainid = @CRMainID)             
--                        SET @MaxPrefLegnum = (SELECT Max(legnum)             
--                                              FROM   preflightleg             
--                                              WHERE             
--                        preflightleg.tripid = @NewTripID)             
--                        SET @Totlegs = @MaxPrefLegnum - @MaxCRLegnum             
            
--                        IF( @MaxCRLegnum < @MaxPrefLegnum )             
--                          BEGIN             
--                              DECLARE @temp INT = @Totlegs             
            
--                              WHILE ( @temp != 0 )             
--                                BEGIN             
--                                    DECLARE @LegID BIGINT             
            
--        SET @MaxPrefLegnum = (SELECT Max(legnum)             
--                                                          FROM   preflightleg             
--                                                          WHERE             
--                                    preflightleg.tripid = @NewTripID)             
--                                    SET @LegID = (SELECT Max(legid)             
--                                                  FROM   preflightleg             
--                                 WHERE             
--                                    preflightleg.tripid = @NewTripID             
--                                    AND legnum = @MaxPrefLegnum)             
            
--                                    EXEC Spflightpak_deletepreflightleg             
--                                      @LegID,             
--                                      @LastUpdUID,             
--                                      @LastUpdTS,             
--                                      1             
            
--                                    SET @temp = @temp - 1             
--                                END             
--                          END             
            
--                        ---ends here                                   
--                        ---Update Leg                                 
--                        UPDATE leg             
--                        SET    [DepartICAOID] = PL.dairportid,             
--                               [ArriveICAOID] = PL.aairportid,             
--                               [DepartureDTTMLocal] = PL.departuredttmlocal,             
--                               [ArrivalDTTMLocal] = PL.arrivaldttmlocal,             
--                               [PassengerRequestorID] = PL.passengerrequestorid,             
--                               [DepartmentID] = PL.departmentid,             
--                               [AuthorizationID] = PL.authorizationid,             
--                               [FlightCategoryID] = PL.flightcategoryid,             
--                               [IsPrivate] = PL.isprivate,             
--                               [Distance] = PL.distance,             
--                               [ElapseTM] = PL.elapsetm,             
--                               [PassengerTotal] = PL.passengertotal,             
--                               [PowerSetting] = PL.powersetting,             
--                               [TakeoffBIAS] = PL.takeoffbias,             
--                  [LandingBIAS] = PL.landingbias,             
--                               [TrueAirSpeed] = PL.trueairspeed,             
--                               [WindsBoeingTable] = PL.windsboeingtable,             
--                               [IsDepartureConfirmed] = PL.isdepartureconfirmed,             
--                               [IsArrivalConfirmation] =             
--                               PL.isarrivalconfirmation,             
--                               [IsApproxTM] = PL.isapproxtm,             
--                               [IsScheduledServices] = PL.isscheduledservices,             
--                               [DepartureGreenwichDTTM] =             
--                               PL.departuregreenwichdttm,             
--                               [ArrivalGreenwichDTTM] = PL.arrivalgreenwichdttm,             
--                               [HomeDepartureDTTM] = PL.homedeparturedttm,             
--                               [HomeArrivalDTTM] = PL.homearrivaldttm,             
--                               [LogBreak] = PL.logbreak,             
--                               [FlightPurpose] = PL.flightpurpose,             
--                               [SeatTotal] = PL.seattotal,             
--                               [ReservationTotal] = PL.reservationtotal,             
--                               [ReservationAvailable] = PL.reservationavailable,           
--                               [WaitNUM] = PL.waitnum,             
--                               [DutyType] = PL.duty_type,             
--                               [DutyHours] = PL.dutyhours,             
--                               [RestHours] = PL.resthours,             
--        [FlightHours] = PL.flighthours,             
--                               [Notes] = PL.notes,             
--                               [FuelLoad] = PL.fuelload,             
--                               [OutbountInstruction] = PL.outboundinstruction,             
--                               [DutyTYPE1] = PL.dutytype,             
--                               [IsCrewDiscount] = PL.iscrewdiscount,             
--                               [IsDutyEnd] = PL.isdutyend,             
--                               [FedAviationRegNUM] = PL.fedaviationregnum,             
--                               [WindReliability] = PL.windreliability,             
--                               [CheckGroup] = PL.checkgroup,             
--                               [AdditionalCrew] = PL.additionalcrew,             
--                               [PilotInCommand] = PL.pilotincommand,             
--                               [SecondInCommand] = PL.secondincommand,             
--                               [NextLocalDTTM] = PL.nextlocaldttm,             
--                               [NextGMTDTTM] = PL.nextgmtdttm,             
--                               [NextHomeDTTM] = PL.nexthomedttm,             
--                               [CrewNotes] = PL.crewnotes,             
--                               [LastUpdUID] = @LastUpdUID,             
--                               [LastUpdTS] = @LastUpdTS,             
--                               [FBOID] = PL.fboid,             
--                               [ClientID] = PL.clientid             
--                        FROM   preflightleg leg             
--                               JOIN crleg PL             
--                                 ON leg.legnum = PL.legnum             
--                                    AND PL.crmainid = @CRMainID             
--                               JOIN crmain PM             
--       ON PL.crmainid = PM.crmainid             
--                        WHERE  PM.tripid = @NewTripID             
--                               AND leg.tripid = @NewTripID             
            
--                        ---ends here                          
--                        ---Update Passenger                                 
--                        DECLARE @CurrentUpdatePassengerID BIGINT             
            
--                        CREATE TABLE #tempupdateprefpassenger             
--                          (             
--                           [crpassengerid]        [BIGINT] NOT NULL,             
--                             [customerid]           [BIGINT] NULL,             
--                             [crlegid]              [BIGINT] NULL,             
--                             [tripnum]              [BIGINT] NULL,             
--                             [legid]                [BIGINT] NULL,             
--                             [ordernum]             [INT] NULL,             
--                             [passengerrequestorid] [BIGINT] NULL,             
--                             [passengername]        [VARCHAR](63) NULL,             
--                             [flightpurposeid]      [BIGINT] NULL,             
--                             [passportid]           [BIGINT] NULL,             
--                             [billing]              [VARCHAR](25) NULL,             
--                             [isnonpassenger]       [BIT] NULL,             
--                             [isblocked]            [BIT] NULL,             
--                             [reservationnum]       [VARCHAR](6) NULL,             
--                             [waitlist]             [CHAR](1) NULL,             
--                             [lastupduid]           [VARCHAR](30) NULL,             
--                             [lastupdts]            [DATETIME] NULL,             
--                             [isdeleted]            [BIT] NOT NULL             
--                          )             
            
--                        INSERT INTO #tempupdateprefpassenger             
--                        SELECT PPL.*             
--                        FROM   crpassenger PPL,           
--                               crleg IP,             
--                               crmain T,             
--                               preflightmain C             
--                        WHERE  PPL.crlegid = IP.crlegid             
--                               AND IP.crmainid = T.crmainid             
--                               AND T.tripid = C.tripid             
--                               AND T.tripid = @NewTripID             
--                               AND IP.legnum = @LastCRLegNum             
--                               AND C.tripid = @NewTripID             
--                        ORDER  BY PPL.crpassengerid ASC             
            
--                        ---Delete Passenger                                 
--                        DECLARE @MaxCRPassenger INT             
--                        DECLARE @MaxPrefPassenger INT             
            
--                        SET @MaxCRPassenger = (SELECT Count(*)             
--                                               FROM   crpassenger             
--                                                      JOIN crleg             
--                                                        ON crpassenger.crlegid             
--                                                           = crleg.crlegid             
--                                               WHERE  crleg.crmainid = @CRMainID             
--                                                      AND crleg.legnum =             
--                                                          @LastCRLegNum             
--                                              )             
--                        SET @MaxPrefPassenger = (SELECT Count(*)             
--                                                 FROM   preflightpassengerlist             
--                                                        JOIN preflightleg             
--                                                          ON             
--        preflightpassengerlist.legid =             
--        preflightleg.legid             
--                                                 WHERE             
--                        preflightleg.tripid = @NewTripID             
--         AND preflightleg.legnum =             
--                            @LastCRLegNum)             
            
--                        IF( @MaxPrefPassenger > @MaxCRPassenger )             
--                          --or @MaxCRPassenger > @MaxPrefPassenger)          
--                          BEGIN             
--                              DECLARE @PassID BIGINT             
            
--                              CREATE TABLE #tempdeleteprefpassenger             
--                                (             
--                                   [passengerrequestorid]     [BIGINT],             
--                                   [preflightpassengerlistid] [BIGINT]             
--                                )             
            
--                              -- commented and added new for transfer PAX                                
--                              ----INSERT INTO #TempDeletePrefPassenger                                 
--                              ---- SELECT PPL.PassengerRequestorID,P.PreflightPassengerListID FROM CRPassenger PPL,  CRLeg IP, CRMain T ,PreflightMain C ,PreflightLeg L                                
--                              ---- ,PreflightPassengerList P                                 
--                              ----   WHERE PPL.CRLegID=IP.CRLegID and  IP.CRMainID = T.CRMainID and T.TripID = C.TripID                                 
--                              ----   and T.TripID=@NewTripID and IP.Legnum=@LastCRLegNum and L.TripID=C.TripID and P.LegID=L.LegID and                                 
--                              ----   L.LegNum= IP.Legnum and T.CRMainID=@CRMainID                                       
--                              ----   ORDER BY PPL.CRPassengerID ASC                                   
--                              INSERT INTO #tempdeleteprefpassenger             
--                              SELECT PPL.passengerid,             
--                                     PPL.preflightpassengerlistid             
--                              FROM   crmain CM             
--                                     INNER JOIN crleg CL             
--                                             ON CM.crmainid = CL.crmainid             
--                                     INNER JOIN crpassenger CP             
--                                             ON CL.crlegid = CP.crlegid           
--                                     INNER JOIN preflightmain PM             
--                                             ON CM.tripid = PM.tripid             
--                                     INNER JOIN preflightleg PL             
--                                             ON PM.tripid = @NewTripID             
--                                                AND CL.legnum = PL.legnum             
--                                     INNER JOIN preflightpassengerlist PPL             
--                                             ON PL.legid = PPL.legid             
--                                                AND CP.passengerrequestorid =             
--                                                    PPL.passengerid             
--                                                AND CP.flightpurposeid =             
--                                                    PPL.flightpurposeid             
--                                     INNER JOIN passenger P             
--                                             ON PPL.passengerid =             
--                                                P.passengerrequestorid             
--                              WHERE  CM.crmainid = @CRMainID             
--                                     AND CL.legnum = @LastCRLegNum             
            
--                              DELETE FROM preflightpassengerhotellist             
--                              WHERE             
--                          preflightpassengerhotellist.preflightpassengerlistid             
--                          NOT             
--                          IN (             
--                          SELECT             
--                          preflightpassengerlistid             
--                          FROM             
--                                 #tempdeleteprefpassenger)             
            
--                              DELETE FROM preflightpassengerlist             
--       WHERE             
--                          preflightpassengerlist.preflightpassengerlistid             
--                          NOT             
--                          IN             
--                          (             
--                          SELECT             
--                          preflightpassengerlistid             
--                          FROM             
--                                 #tempdeleteprefpassenger)             
            
--                              DROP TABLE #tempdeleteprefpassenger             
--                          END             
            
--                        ---Ends here                                                
--                        ---Update or insert                                 
--                        WHILE (SELECT Count(*)             
--                               FROM   #tempupdateprefpassenger) > 0             
--                          BEGIN             
--                              DECLARE @NewPrefLegID BIGINT             
            
--                              SET @NewPrefLegID = (SELECT legid             
--                                                   FROM   preflightleg             
--                                                   WHERE             
--                              tripid = @NewTripID             
--                              AND legnum = @LastCRLegNum)             
            
--                              -- Get current crpassenger id                                 
--                              SELECT TOP 1 @CurrentUpdatePassengerID =             
--                                           crpassengerid             
--                              FROM   #tempupdateprefpassenger             
            
--                              DECLARE @PASSENGERID BIGINT             
            
--                              SELECT TOP 1 @PASSENGERID = passengerrequestorid             
--                              FROM   #tempupdateprefpassenger             
            
--                              IF EXISTS (SELECT             
--                                 passenger.preflightpassengerlistid             
--FROM   preflightpassengerlist passenger             
--                                                JOIN preflightleg leg             
--                                                  ON passenger.legid = leg.legid             
--                                         WHERE             
--                                 passenger.passengerid = @PASSENGERID             
--                                 AND leg.legnum = @LastCRLegNum             
--                   AND leg.tripid = @NewTripID)             
--                                BEGIN             
--                                    UPDATE passenger             
--                                    SET             
--                                    -- this point is impo                               
--                                    CustomerID = @CustomerID,             
--                                    OrderNUM = PP.ordernum,             
--                                    PassengerID = PP.passengerrequestorid,             
--                                    PassengerFirstName = PP.passengername,             
--                                    FlightPurposeID = PP.flightpurposeid,             
--                                    PassportID = PP.passportid,             
--                                    IsNonPassenger = PP.isnonpassenger,             
--                                    IsBlocked = PP.isblocked,             
--                                    ReservationNUM = PP.reservationnum,             
--                                    WaitList = PP.waitlist,             
--                                    LastUpdUID = @LastUpdUID,             
--                                    LastUpdTS = @LastUpdTS             
--                                    FROM   preflightpassengerlist passenger             
--                                           JOIN preflightleg leg             
--                                             ON passenger.legid = leg.legid             
--                                                AND leg.tripid = @NewTripID             
--               JOIN crleg PL             
--                                             ON leg.legnum = PL.legnum             
--       AND PL.crmainid = @CRMainID             
--                                           JOIN crmain PM             
--                                             ON PL.crmainid = PM.crmainid             
--                                           JOIN crpassenger PP             
--                                             ON PP.crlegid = PL.crlegid             
--                                                AND PP.passengerrequestorid =             
--                                                    passenger.passengerid             
--                                    WHERE  PM.crmainid = @CRMainID             
--                                           AND leg.tripid = @NewTripID             
--                                           AND             
--                             PP.crpassengerid = @CurrentUpdatePassengerID             
--                                END             
--    ELSE             
--                                BEGIN             
--                                    DECLARE @PrefPassengerListID BIGINT             
            
--                                    EXECUTE dbo.Usp_getsequencenumber             
--                                      @CustomerID,             
--                                      'PreflightCurrentNo',             
--                                      @PrefPassengerListID output             
            
--                                    INSERT INTO preflightpassengerlist             
--                                                (preflightpassengerlistid,             
--                                                 legid,             
--                                                 customerid,             
--                                                 passengerid,             
--                                                 passportid,             
--                                                 visaid,             
--                                                 hotelname,             
--  passengerfirstname,             
--                                                 passengermiddlename,             
--                                                 passengerlastname,             
--                                                 flightpurposeid,             
--                                                 passportnum,             
--                                                 billing,             
--                                                 isnonpassenger,             
--                                                 isblocked,             
--                                                 ordernum,             
--                            reservationnum,             
--                                                 lastupduid,             
--                                                 lastupdts,             
--                                                 waitlist,             
--                                                 associatedpassenger,             
--                                                 tripstatus,             
--                                                 tsadttm,             
--                                                 isdeleted,             
--                                                 noofrooms,             
--                                                 clubcard,             
--                                                 isratecap,             
--                                                 maxcapamount,             
--                                                 street,             
--                                                 cityname,             
--                                                 statename,             
--                                                 postalzipcd)             
--                                    SELECT @PrefPassengerListID,             
--                                           @NewPrefLegID,             
--                                           PPL.customerid,             
--                                          PPL.passengerrequestorid,             
--                                           PPL.passportid,             
--                                           NULL -- VisaID                                 
--                                           ,             
--                NULL -- HotelName                                 
--                                           ,             
--                                           PPL.passengername             
--                                           -- to be modified                                  
--                                           ,             
--                                           NULL,             
--                                           NULL,             
--                                           PPL.flightpurposeid,             
--                                           NULL             
--                                           -- PassportNum                                 
--                                           ,             
--                                           PPL.billing,             
--                                           PPL.isnonpassenger,             
--                                           PPL.isblocked,             
--                                           PPL.ordernum,             
--                                           PPL.reservationnum,             
--                                           @LastUpdUID,             
--                                           @LastUpdTS,             
--                                           PPL.waitlist,             
--                                           NULL             
--                                           --AssociatedPassenger                                 
--                                           ,             
--                                           NULL --TripStatus                                 
--                                           ,             
--                                           NULL --TSADTTM                                 
--                                           ,             
--                                           PPL.isdeleted,             
-- NULL --NoofRooms                                 
--                                           ,             
--                                           NULL --ClubCard                                 
--                                           ,             
--                                           NULL --IsRateCap                                 
--                                           ,             
--                                           NULL             
--                                           --MaxCapAmount                                 
--                                           ,             
--                                           NULL --Street                                 
--                                           ,             
--                                           NULL --CityName                                 
--                    ,             
--                                           NULL --StateName                                 
--                                           ,             
--                                           NULL             
--                                    --PostalZipCD                                            
--                                    FROM   #tempupdateprefpassenger PPL             
--                                    WHERE  crpassengerid =             
--                                           @CurrentUpdatePassengerID             
--                                END             
            
--                              -- delete the cr hotel record from temp table                                 
--                              DELETE FROM #tempupdateprefpassenger             
--                              WHERE  crpassengerid = @CurrentUpdatePassengerID             
--                          END             
            
--                        DROP TABLE #tempupdateprefpassenger             
            
--                        ---ends here                                  
--                        -- Update Leg for Passenger Total                                 
--                        DECLARE @Passengertot INT             
            
--                        SET @Passengertot = (SELECT Count(*)             
--                                             FROM   preflightpassengerlist             
--                                                    JOIN preflightleg             
--                                                      ON             
--  preflightpassengerlist.legid             
--  =             
--  preflightleg.legid             
--                                             WHERE             
--                        preflightleg.tripid = @NewTripID             
--                        AND preflightleg.legnum =             
--                            @LastCRLegNum)             
            
--                        UPDATE preflightleg             
--                        SET    passengertotal = @Passengertot             
--                        WHERE  preflightleg.tripid = @NewTripID             
--                               AND preflightleg.legnum = @LastCRLegNum             
            
--                        ---Ends here                                  
--                        ---Update DepartFbo                                 
--                        DECLARE @CurrentUpdateFboID BIGINT             
            
--                        CREATE TABLE #tempupdatepreffbolist             
--                          (             
--                             [crfbolistid]          [BIGINT] NOT NULL,             
--                             [customerid]           [BIGINT] NULL,             
--                             [crlegid]              [BIGINT] NULL,             
--                             [tripnum]              [BIGINT] NULL,             
--                             [legid]                [BIGINT] NULL,             
--                             [recordtype]           [CHAR](2) NULL,             
--                             [fboid]                [BIGINT] NULL,             
--                             [crfbolistdescription] [VARCHAR](60) NULL,             
--                             [phonenum]             [VARCHAR](25) NULL,             
--                             [iscompleted]          [BIT] NULL,             
--   [airportid]            [BIGINT] NULL,             
--                             [lastupduid]           [VARCHAR](30) NULL,             
--                             [lastupdts]            [DATETIME] NULL,             
--                             [isdeleted]            [BIT] NOT NULL             
--                          )             
            
--                        INSERT INTO #tempupdatepreffbolist             
--                        SELECT PFO.*             
--                        FROM   crfbolist PFO,             
--                               crleg IP,             
--                               crmain T,             
--                               preflightmain C             
--                        WHERE  PFO.crlegid = IP.crlegid             
--                               AND IP.crmainid = T.crmainid             
--                               AND T.tripid = C.tripid             
--                               AND T.crmainid = @CRMainID             
--                               AND IP.legnum = @LastCRLegNum             
--                               AND PFO.recordtype = 'D'             
--                               AND C.tripid = @NewTripID             
--                        ORDER  BY PFO.crfbolistid ASC             
            
--                        WHILE (SELECT Count(*)             
--                               FROM   #tempupdatepreffbolist) > 0             
--                          BEGIN             
--                              DECLARE @NewCRLegIDFBO BIGINT             
            
--                              SET @NewCRLegIDFBO = (SELECT legid             
--                                                    FROM   preflightleg             
--                                                    WHERE             
--                              tripid = @NewTripID             
--                     AND legnum = @LastCRLegNum)             
            
--                              -- Get current crpassenger id                                 
--                              SELECT TOP 1 @CurrentUpdateFboID = crfbolistid             
--                              FROM   #tempupdatepreffbolist             
            
--               DECLARE @FBOID BIGINT             
            
--                              SELECT TOP 1 @FBOID = fboid             
--                              FROM   #tempupdatepreffbolist             
            
--                              IF EXISTS (SELECT fbo.fboid             
--                                         FROM   preflightfbolist fbo             
--                                                JOIN preflightleg leg          
--                                                  ON fbo.legid = leg.legid             
--                                         WHERE  fbo.fboid = @FBOID             
--                                                AND leg.legnum = @LastCRLegNum             
--                                                AND leg.tripid = @NewTripID)             
--                                BEGIN             
--                                    UPDATE departfbo             
--                                    SET    [LastUpdUID] = @LastUpdUID,             
--                                           [LastUpdTS] = @LastUpdTS,             
--                                           [IsDeleted] = PFO.isdeleted,             
--                                           [IsCompleted] = PFO.iscompleted,             
--                                           [AirportID] = PFO.airportid,             
--                                           [FBOID] = PFO.fboid             
--                                    FROM   preflightfbolist departfbo             
--                                           JOIN preflightleg leg             
--                                             ON departfbo.legid = leg.legid             
--                                                AND leg.tripid = @NewTripID             
--                                           JOIN crleg PL             
--                                             ON leg.legnum = PL.legnum             
--                                                AND PL.crmainid = @CRMainID             
--                                           JOIN crmain PM             
--         ON PL.crmainid = PM.crmainid             
--                                           JOIN crfbolist PFO             
--                                             ON PFO.legid = PL.legid             
--                                                AND PFO.fboid = departfbo.fboid             
--                                                AND PFO.recordtype = 'D'             
--                                    WHERE  PM.crmainid = @CRMainID             
--                                           AND leg.tripid = @NewTripID             
--                                           AND departfbo.isdeparturefbo = 1             
--                                           AND             
--                                    PFO.crfbolistid = @CurrentUpdateFboID             
--                                END             
--                              ELSE             
--                                BEGIN             
--                                    DECLARE @PrefFBOListsID BIGINT             
            
--                                    EXECUTE dbo.Usp_getsequencenumber             
--                                      @CustomerID,             
--                                      'PreflightCurrentNo',             
--                                      @PrefFBOListsID output             
            
--                                    INSERT INTO [dbo].[preflightfbolist]             
--           ([preflightfboid],             
--                                                 [preflightfboname],             
--                                                 [legid],             
--                                                 [fboid],             
--                                                 [isarrivalfbo],             
--                                                 [isdeparturefbo],             
--                [airportid],             
--                                                 [street],             
--                                                 [cityname],             
--                                                 [statename],             
--                                                 [postalzipcd],             
--                                                 [phonenum1],             
--                                                 [phonenum2],             
--                                                 [phonenum3],             
--                                                 [phonenum4],             
--                                                 [faxnum],             
--                                                 [lastupduid],             
--                                                 [lastupdts],             
--                                   [isdeleted],             
--                                                 [noupdated],             
--                                                 [fboarrangedby],             
--                                                 [iscompleted],             
--                                                 [fboinformation],             
--                                                 [confirmationstatus],             
--                                                 [comments],             
--                                                 [customerid]             
--                                    --,[IsRequired]                                 
--                                    --,[IsNotRequired]                                 
--                                    --,[IsInProgress]                                 
--                                    --,[IsChange]                                 
--                                    --,[IsCancelled]                                 
--                                    )             
--                                    SELECT @PrefFBOListsID,             
--                                           NULL,             
--                                           @NewCRLegIDFBO,             
--                                           PFO.fboid,             
--                                           0,             
--                                           1,             
--                                           PFO.airportid,             
--                                 NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--            NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           @LastUpdUID,             
--                                           @LastUpdTS,             
--                                           PFO.[isdeleted],             
--                                           0,             
--                                           0,             
--                                           PFO.iscompleted,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           PFO.customerid             
--                                    --,0                                 
--                                    --,0                                 
--                                    --,0                                 
--                                    --,0                                 
--                                    --,0           
--                                    FROM   #tempupdatepreffbolist PFO             
--                              WHERE  crfbolistid = @CurrentUpdateFboID             
--                                END             
            
--                              DELETE FROM #tempupdatepreffbolist             
--                              WHERE  crfbolistid = @CurrentUpdateFboID             
--                          END             
            
--                        DROP TABLE #tempupdatepreffbolist             
            
--                        --ends here                                  
--                        ---Update arriveFbo                                 
--                        DECLARE @CurrentUpdateArrFboID BIGINT             
            
--                        CREATE TABLE #tempupdateprefarrfbolist             
--                          (             
--                             [crfbolistid]          [BIGINT] NOT NULL,             
--                             [customerid]           [BIGINT] NULL,             
--                             [crlegid]              [BIGINT] NULL,             
--                             [tripnum]              [BIGINT] NULL,             
--                             [legid]                [BIGINT] NULL,             
--                             [recordtype]           [CHAR](2) NULL,             
--                             [fboid]                [BIGINT] NULL,             
--                             [crfbolistdescription] [VARCHAR](60) NULL,             
--                             [phonenum]             [VARCHAR](25) NULL,             
--                             [iscompleted]          [BIT] NULL,             
--                             [airportid]            [BIGINT] NULL,             
--                             [lastupduid]           [VARCHAR](30) NULL,             
--                             [lastupdts]            [DATETIME] NULL,             
--                             [isdeleted]            [BIT] NOT NULL             
--                          )             
            
--                        INSERT INTO #tempupdateprefarrfbolist             
--                        SELECT PFO.*             
--                        FROM   crfbolist PFO,             
--                               crleg IP,             
--                               crmain T,             
--                               preflightmain C             
--                        WHERE  PFO.crlegid = IP.crlegid             
--                               AND IP.crmainid = T.crmainid             
--                               AND T.tripid = C.tripid             
--                               AND T.crmainid = @CRMainID             
--                               AND IP.legnum = @LastCRLegNum             
--                              AND PFO.recordtype = 'A'             
--                               AND C.tripid = @NewTripID             
--                        ORDER  BY PFO.crfbolistid ASC             
            
--                        WHILE (SELECT Count(*)             
--                               FROM   #tempupdateprefarrfbolist) > 0             
--                          BEGIN             
--                              DECLARE @NewCRLegIDArrFBO BIGINT             
            
--                              SET @NewCRLegIDArrFBO = (SELECT legid             
--                                                       FROM   preflightleg             
--                                                       WHERE             
--                              tripid = @NewTripID             
--                              AND legnum = @LastCRLegNum             
--                                                      )             
            
--                              -- Get current crpassenger id                                 
--                              SELECT TOP 1 @CurrentUpdateArrFboID = crfbolistid             
--         FROM   #tempupdateprefarrfbolist             
            
--                              DECLARE @ArrFBOID BIGINT             
            
--                              SELECT TOP 1 @ArrFBOID = fboid             
--                              FROM   #tempupdateprefarrfbolist             
            
--IF EXISTS (SELECT fbo.fboid             
--                                         FROM   preflightfbolist fbo             
--                                                JOIN preflightleg leg             
--                                                  ON fbo.legid = leg.legid             
--                                         WHERE  fbo.fboid = @ArrFBOID             
--                                                AND leg.legnum = @LastCRLegNum             
--             AND leg.tripid = @NewTripID)             
--                                BEGIN             
--                                    UPDATE departfbo             
--                                    SET    [LastUpdUID] = @LastUpdUID,             
--                                           [LastUpdTS] = @LastUpdTS,             
--                                           [IsDeleted] = PFO.isdeleted,             
--                                           [IsCompleted] = PFO.iscompleted,             
--                                           [AirportID] = PFO.airportid,             
--                                           [FBOID] = PFO.fboid             
--                                    FROM   preflightfbolist departfbo             
--                                           JOIN preflightleg leg             
--                                             ON departfbo.legid = leg.legid             
--                                                AND leg.tripid = @NewTripID             
--                                           JOIN crleg PL             
--                                             ON leg.legnum = PL.legnum             
--                                                AND PL.crmainid = @CRMainID             
--                                           JOIN crmain PM             
--                                             ON PL.crmainid = PM.crmainid             
--                                           JOIN crfbolist PFO             
--                                             ON PFO.legid = PL.legid             
--                                                AND PFO.fboid = departfbo.fboid             
--                                                AND PFO.recordtype = 'A'             
--                                    WHERE  PM.crmainid = @CRMainID             
--                                           AND leg.tripid = @NewTripID             
--                                           AND departfbo.isdeparturefbo = 0             
--                                           AND departfbo.isarrivalfbo = 1             
--                                           AND             
--                                    PFO.crfbolistid = @CurrentUpdateArrFboID             
--                                END             
--                              ELSE                                         BEGIN             
--                                    DECLARE @PrefArrFBOListsID BIGINT             
            
--                                    EXECUTE dbo.Usp_getsequencenumber             
--                                      @CustomerID,             
--                                      'PreflightCurrentNo',             
--                                      @PrefArrFBOListsID output             
            
--                                    INSERT INTO [dbo].[preflightfbolist]             
--                                                ([preflightfboid],             
--                                                 [preflightfboname],             
--                                                 [legid],             
--                                                 [fboid],             --                                                 [isarrivalfbo],             
--                                                 [isdeparturefbo],             
--                                                 [airportid],             
--                                                 [street],             
--                                     [cityname],             
--                                                 [statename],             
--                                                 [postalzipcd],                                                            [phonenum1],             
--                                                 [phonenum2],             
--                                                 [phonenum3],             
--                                                 [phonenum4],             
--                                                 [faxnum],             
--                                                 [lastupduid],             
--                                                 [lastupdts],             
--                                                 [isdeleted],             
--                                                 [noupdated],             
--                                                 [fboarrangedby],             
--                                                 [iscompleted],             
--                               [fboinformation],             
--                                                 [confirmationstatus],             
--                                                 [comments],             
--                                                 [customerid]             
--                                    --,[IsRequired]                                 
--                                    --,[IsNotRequired]                                 
--                                    --,[IsInProgress]                                 
--                                    --,[IsChange]                                 
--                                    --,[IsCancelled]                                 
--                                    )             
--                                    SELECT @PrefArrFBOListsID,             
--                                           NULL,             
--      @NewCRLegIDArrFBO,             
--                                           PFO.fboid,             
--                                           1,             
--                                           0,             
--                                           PFO.airportid,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           @LastUpdUID,             
--                                           @LastUpdTS,             
--                                           PFO.[isdeleted],             
--                                           0,             
--                                           0,             
--                      PFO.iscompleted,             
--                                           NULL,             
--                                           NULL,             
--                                           NULL,             
--                                           PFO.customerid             
--                                    --,0                                 
--                                    --,0                                 
--                                    --,0                           
--                                    --,0                                 
--                                    --,0                                 
--                                    FROM   #tempupdateprefarrfbolist PFO             
--                                    WHERE  crfbolistid = @CurrentUpdateArrFboID             
--                                END             
            
--                              DELETE FROM #tempupdateprefarrfbolist             
--                              WHERE  crfbolistid = @CurrentUpdateArrFboID             
--                          END             
            
--                        DROP TABLE #tempupdateprefarrfbolist             
            
--                        --ends here                                  
--                        ---Update DepartCatering                                 
--                        DECLARE @CurrentUpdateDEPCaterID BIGINT             
            
--                        CREATE TABLE #tempprefdepcaterlist             
--             (             
--                             [crcateringlistid]          [BIGINT] NOT NULL,             
--                             [customerid]                [BIGINT] NULL,             
--                             [crlegid]                   [BIGINT] NULL,             
--                             [tripnum]                   [BIGINT] NULL,             
--                             [legid]                     [BIGINT] NULL,             
--                             [recordtype]                [CHAR](2) NULL,             
--                             [cateringid]                [BIGINT] NULL,             
--                             [crcateringlistdescription] [VARCHAR](60) NULL,             
--                             [phonenum]                  [VARCHAR](25) NULL,             
--                             [rate]                      [NUMERIC](6, 2) NULL,             
--                      [iscompleted]               [BIT] NULL,             
--                             [airportid]                 [BIGINT] NULL,             
--                             [lastupduid]                [VARCHAR](30) NULL,             
--                             [lastupdts]                 [DATETIME] NULL,             
--                             [isdeleted]                 [BIT] NOT NULL,             
--                             [faxnum]                    [VARCHAR](25) NULL,             
--                             [email]                     [VARCHAR](250) NULL             
--                          )             
            
--                        INSERT INTO #tempprefdepcaterlist             
--                        SELECT PCD.*             
--                        FROM   crcateringlist PCD,             
--                               crleg IP,             
--                               crmain T,             
--                               preflightmain C             
--                        WHERE  PCD.crlegid = IP.crlegid             
--                               AND IP.crmainid = T.crmainid             
--                               AND T.tripid = C.tripid             
--                               AND T.crmainid = @CRMainID             
--                               AND IP.legnum = @LastCRLegNum             
--                               AND C.tripid = @NewTripID             
--                               AND PCD.recordtype = 'D'             
--                        ORDER  BY PCD.crcateringlistid ASC             
            
--                        WHILE (SELECT Count(*)             
--                               FROM   #tempprefdepcaterlist) > 0             
--                          BEGIN             
--                              DECLARE @NewCRLegIDDEPCATER BIGINT             
            
--             SET @NewCRLegIDDEPCATER = (SELECT legid             
--                                                         FROM   preflightleg     
--                                                         WHERE             
--                              tripid = @NewTripID             
--                              AND legnum = @LastCRLegNum)             
            
--                              -- Get current crpassenger id                                 
--                              SELECT TOP 1 @CurrentUpdateDEPCaterID =             
--                                           crcateringlistid             
--                              FROM   #tempprefdepcaterlist             
            
--                              DECLARE @DEPCATERID BIGINT             
            
--                              SELECT TOP 1 @DEPCATERID = cateringid             
--                              FROM   #tempprefdepcaterlist             
            
--                              IF EXISTS (SELECT cater.cateringid             
--                                         FROM   preflightcateringdetail cater             
--                                   JOIN preflightleg leg             
--                                                  ON cater.legid = leg.legid             
--                                         WHERE  cater.cateringid = @DEPCATERID             
--                                                AND leg.legnum = @LastCRLegNum             
--                                                AND leg.tripid = @NewTripID)             
--                                BEGIN             
--                                    UPDATE departcatering             
--                                    SET    [ArriveDepart] = 'D',             
--                                           [IsCompleted] = PCA.iscompleted,             
--                                           [LastUpdUID] = @LastUpdUID,             
--                                 [LastUpdTS] = @LastUpdTS,             
--                                           [IsDeleted] = PCA.isdeleted,             
--                                           AirportID = PCA.airportid,             
--                                           CateringID = PCA.cateringid             
--                                    FROM   preflightcateringdetail             
--                                           departcatering             
--                                           JOIN preflightleg leg             
--                                             ON departcatering.legid = leg.legid             
--                                                AND leg.tripid = @NewTripID             
--                JOIN crleg PL             
--                                             ON leg.legnum = PL.legnum             
--                                                AND PL.crmainid = @CRMainID             
--                                           JOIN crmain PM             
--                                             ON PL.crmainid = PM.crmainid             
--                                           JOIN crcateringlist PCA             
--                                             ON PCA.crlegid = PL.crlegid             
--                                                AND PCA.cateringid =             
--                                                    departcatering.cateringid             
--                                                AND PCA.recordtype = 'D'             
--                                    WHERE  PM.crmainid = @CRMainID             
--                                           AND leg.tripid = @NewTripID             
--                                           AND departcatering.arrivedepart = 'D'             
--                                           AND             
--  PCA.crcateringlistid = @CurrentUpdateDEPCaterID             
--  END             
--  ELSE             
--  BEGIN             
--  DECLARE @PreflightCateringID BIGINT             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--    @CustomerID,             
--    'PreflightCurrentNo',             
--    @PreflightCateringID output             
            
--  INSERT INTO [dbo].[preflightcateringdetail]             
--              ([preflightcateringid],             
--               [legid],             
--    [customerid],             
--               [airportid],             
--               [arrivedepart],             
--               [cateringid],             
--               [iscompleted],             
--               [cost],             
--               [contactname],             
--               [contactphone],             
--               [contactfax],             
--               [cateringconfirmation],             
--               [cateringcomments],             
--               [lastupduid],             
--               [lastupdts],             
--               [isdeleted],             
--               [isuwaarranger],             
--               [cateringcontactname])             
--  SELECT @PreflightCateringID,             
--         @NewCRLegIDDEPCATER,             
--         PCD.customerid,             
--         PCD.airportid,             
--         'D',             
--         PCD.cateringid,             
--         PCD.iscompleted,             
--         0,             
--         NULL,             
--         NULL,             
--         NULL,            
--         NULL,             
--         NULL,             
--         @LastUpdUID,             
--         @LastUpdTS,             
--         PCD.isdeleted,             
--         0,             
--         NULL             
--  FROM   #tempprefdepcaterlist PCD             
--  WHERE  crcateringlistid = @CurrentUpdateDEPCaterID             
--  END             
            
--  DELETE FROM #tempprefdepcaterlist             
--  WHERE  crcateringlistid = @CurrentUpdateDEPCaterID             
--  END             
            
--  DROP TABLE #tempprefdepcaterlist             
            
--  --ends here                                   
--  ---Update ArriveCatering                                 
--  DECLARE @CurrentUpdateARRCaterID BIGINT             
            
--  CREATE TABLE #tempprefarrcaterlist             
--  (             
--  [crcateringlistid]          [BIGINT] NOT NULL,             
--  [customerid]                [BIGINT] NULL,             
--  [crlegid]                   [BIGINT] NULL,             
--  [tripnum]                   [BIGINT] NULL,             
--  [legid]                     [BIGINT] NULL,             
--  [recordtype]                [CHAR](2) NULL,             
--  [cateringid]                [BIGINT] NULL,             
--  [crcateringlistdescription] [VARCHAR](60) NULL,             
--  [phonenum]                  [VARCHAR](25) NULL,             
--  [rate]                      [NUMERIC](6, 2) NULL,             
--  [iscompleted]               [BIT] NULL,             
--  [airportid]                 [BIGINT] NULL,             
--  [lastupduid]                [VARCHAR](30) NULL,             
--  [lastupdts]                 [DATETIME] NULL,     [isdeleted]                 [BIT] NOT NULL,             
--  [faxnum]                    [VARCHAR](25) NULL,             
--  [email]                     [VARCHAR](250) NULL             
--  )             
            
--  INSERT INTO #tempprefarrcaterlist             
--  SELECT PCD.*             
--  FROM   crcateringlist PCD,             
--  crleg IP,             
--  crmain T,             
--  preflightmain C             
--  WHERE  PCD.crlegid = IP.crlegid             
--  AND IP.crmainid = T.crmainid             
--  AND T.tripid = C.tripid             
--  AND T.crmainid = @CRMainID             
--  AND IP.legnum = @LastCRLegNum             
--  AND PCD.recordtype = 'A'             
--  AND C.tripid = @NewTripID             
--  ORDER  BY PCD.crcateringlistid ASC             
            
--  WHILE (SELECT Count(*)             
--  FROM   #tempprefarrcaterlist) > 0             
--  BEGIN             
--  DECLARE @NewCRLegIDARRCATER BIGINT             
            
--  SET @NewCRLegIDARRCATER = (SELECT legid             
--  FROM   preflightleg             
--                       WHERE             
--  tripid = @NewTripID             
-- AND legnum = @LastCRLegNum)             
            
--  -- Get current crpassenger id                                 
--  SELECT TOP 1 @CurrentUpdateARRCaterID = crcateringlistid             
--  FROM   #tempprefarrcaterlist             
            
--  DECLARE @ARRCATERID BIGINT             
            
--  SELECT TOP 1 @ARRCATERID = cateringid             
--  FROM   #tempprefarrcaterlist             
            
--  IF EXISTS (SELECT cater.cateringid             
--       FROM   preflightcateringdetail cater             
--              JOIN preflightleg leg             
--                ON cater.legid = leg.legid             
--       WHERE  cater.cateringid = @ARRCATERID             
--              AND leg.legnum = @LastCRLegNum             
--              AND leg.tripid = @NewTripID)             
--  BEGIN             
--  UPDATE departcatering             
--  SET    [ArriveDepart] = 'A',             
--         [IsCompleted] = PCA.iscompleted,             
--         [LastUpdUID] = @LastUpdUID,             
--         [LastUpdTS] = @LastUpdTS,             
--         [IsDeleted] = PCA.isdeleted,             
--         AirportID = PCA.airportid,             
--         CateringID = PCA.cateringid             
--  FROM   preflightcateringdetail departcatering             
--         JOIN preflightleg leg             
--           ON departcatering.legid = leg.legid             
--              AND leg.tripid = @NewTripID             
--         JOIN crleg PL             
--           ON leg.legnum = PL.legnum             
--            AND PL.crmainid = @CRMainID             
--         JOIN crmain PM             
--           ON PL.crmainid = PM.crmainid             
--         JOIN crcateringlist PCA             
--           ON PCA.crlegid = PL.crlegid             
--              AND PCA.cateringid =             
--                  departcatering.cateringid             
--              AND PCA.recordtype = 'A'             
--  WHERE  PM.crmainid = @CRMainID             
--         AND leg.tripid = @NewTripID             
--         AND departcatering.arrivedepart = 'A'             
--         AND             
--  PCA.crcateringlistid = @CurrentUpdateARRCaterID             
--  END             
--  ELSE             
--  BEGIN             
--  DECLARE @PreflightArrCateringID BIGINT             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--    @CustomerID,             
--    'PreflightCurrentNo',             
--    @PreflightArrCateringID output             
            
--  INSERT INTO [dbo].[preflightcateringdetail]             
--              ([preflightcateringid],             
--               [legid],             
--               [customerid],             
--               [airportid],             
--               [arrivedepart],             
--               [cateringid],             
--               [iscompleted],             
--               [cost],             
--               [contactname],             
--               [contactphone],             
--               [contactfax],             
--               [cateringconfirmation],             
--               [cateringcomments],             
--               [lastupduid],             
--               [lastupdts],             
--               [isdeleted],             
--               [isuwaarranger],             
--               [cateringcontactname])             
--  SELECT @PreflightArrCateringID,             
--         @NewCRLegIDARRCATER,             
--         PCD.customerid,             
--         PCD.airportid,             
--         'A',             
--         PCD.cateringid,             
--         PCD.iscompleted,             
--         0,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         @LastUpdUID,             
--         @LastUpdTS,             
--       PCD.isdeleted,             
--         0,             
--         NULL             
--  FROM   #tempprefarrcaterlist PCD             
--  WHERE  crcateringlistid = @CurrentUpdateARRCaterID             
--  END             
            
--  DELETE FROM #tempprefarrcaterlist             
--  WHERE  crcateringlistid = @CurrentUpdateARRCaterID             
--  END             
            
--  DROP TABLE #tempprefarrcaterlist             
            
--  --ends here                                   
--  ---Update crew hotel                             
--  --DECLARE @CurrentUpdatePrefCrewHotelID BIGINT                                 
--  DECLARE @CurrentUpdatePrefHotelID BIGINT             
            
--  -- CREATE Table #TempUpdatePrefCrewHotellist                                 
--  CREATE TABLE #tempupdateprefhotellist             
--  (             
--  [crhotellistid]          [BIGINT] NOT NULL,             
--  [customerid]             [BIGINT] NULL,             
--  [crlegid]   [BIGINT] NULL,             
--  [tripnum]                [BIGINT] NULL,             
--  [legid]                  [BIGINT] NULL,             
--  [recordtype]             [CHAR](2) NULL,             
--  [hotelid]                [BIGINT] NULL,             
--  [crhotellistdescription] [VARCHAR](60) NULL,             
--  [phonenum]               [VARCHAR](25) NULL,             
--  [rate]                   [NUMERIC](6, 2) NULL,             
--  [iscompleted]         [BIT] NULL,             
--  [airportid]              [BIGINT] NULL,             
--  [lastupduid]             [VARCHAR](30) NULL,             
--  [lastupdts]              [DATETIME] NULL,             
--  [isdeleted]              [BIT] NOT NULL,             
--  [faxnum]                 [VARCHAR](25) NULL,             
--  [email]                  [VARCHAR](250) NULL             
--  )             
            
--  INSERT INTO #tempupdateprefhotellist             
--  SELECT PCH.*             
--  FROM   crhotellist PCH,             
--  crleg IP,             
--  crmain T,             
--  preflightmain C             
--  WHERE  PCH.crlegid = IP.crlegid             
--  AND IP.crmainid = T.crmainid             
--  AND T.tripid = C.tripid             
--  AND T.crmainid = @CRMainID             
--  AND IP.legnum = @LastCRLegNum             
--  AND C.tripid = @NewTripID             
--  AND PCH.recordtype = 'C'             
--  ORDER  BY PCH.crhotellistid ASC             
            
--  --    WHILE (SELECT COUNT(*) from #TempUpdatePrefCrewHotellist)>0                                 
--  WHILE  (SELECT Count(*)             
--  FROM   #tempupdateprefhotellist) > 0             
--  BEGIN             
--      DECLARE @NewCRLegIDCREW BIGINT             
                
--      SET @NewCRLegIDCREW = (SELECT legid             
--           FROM   preflightleg             
--           WHERE  tripid = @NewTripID             
--            AND legnum = @LastCRLegNum             
--          )             
                
          
                
--      -- Get current crpassenger id                                 
--      --  SELECT TOP 1 @CurrentUpdatePrefCrewHotelID = CRHotelListID FROM #TempUpdatePrefCrewHotellist                                
--      SELECT TOP 1 @CurrentUpdatePrefHotelID = crhotellistid             
--      FROM   #tempupdateprefhotellist             
                
--      DECLARE @ARRCREWHOTELID BIGINT             
                
--      --SELECT TOP 1 @ARRCREWHOTELID = HotelID FROM #TempUpdatePrefCrewHotellist                                  
--      SELECT TOP 1 @ARRCREWHOTELID = hotelid             
--      FROM   #tempupdateprefhotellist             
                
--      --  IF EXISTS (SELECT  hotel.HotelID FROM PreflightCrewHotelList  hotel JOIN PreflightCrewList PCL on     
--      declare @countexistLeg int    
--      SELECT @countexistLeg=Count(*)             
--        FROM   preflighthotellist hotel             
--         --JOIN PreflightCrewList PCL on hotel.PreflightcrewListID =PCL.PreflightcrewListID                  
--         -- JOIN PreflightCrewList PCL on hotel.PreflightcrewListID =PCL.PreflightcrewListID                       
--         -- JOIN  PreflightLeg leg on PCL.LegID = leg.LegID                                  
--         JOIN preflightleg leg             
--        ON hotel.legid = leg.legid             
--        WHERE -- hotel.hotelid = @ARRCREWHOTELID     AND     
--        leg.legnum = @LastCRLegNum             
--         AND leg.tripid = @NewTripID    
          
--      print @ARRCREWHOTELID        
--      print @LastCRLegNum    
--      print @NewTripID    
                                      
--      IF  (@countexistLeg >=1)             
--      BEGIN            
          
          
           
             
--           UPDATE hotel             
--           SET    customerid = @CustomerID,             
--         hotelid = CRHL.hotelid,             
--         iscompleted = CRHL.iscompleted,             
--         airportid = CRHL.airportid,             
--         lastupduid = @LastUpdUID,             
--         lastupdts = @LastUpdTS,             
--         isdeleted = CRHL.isdeleted             
--           --    FROM  PreflightCrewHotelList crewhotel                                 
--           FROM   preflighthotellist hotel             
--         --   JOIN PreflightCrewList PCH on  crewhotel.PreflightCrewListID=PCH.PreflightCrewListID                                
--         -- JOIN PreflightCrewList PCH on  hotel.PreflightCrewListID=PCH.PreflightCrewListID                                
--         --    JOIN  PreflightLeg leg on PCH.LegID = leg.LegID and leg.TripID=@NewTripID                                 
--         JOIN preflightleg leg             
--           ON hotel.legid = leg.legid             
--           AND leg.tripid = @NewTripID             
--         JOIN preflightmain PM             
--           ON leg.tripid = PM.tripid             
--         JOIN crmain CRM             
--           ON PM.tripid = CRM.tripid             
--         JOIN crleg PL             
--           ON leg.legnum = PL.legnum             
--           AND CRM.crmainid = @CRMainID             
--         JOIN crhotellist CRHL             
--           ON CRHL.crlegid = PL.crlegid             
--           WHERE  PM.tripid = @NewTripID             
--         AND leg.tripid = @NewTripID             
--         AND CRM.crmainid = @CRMainID             
--         -- and CRHL.RecordType='C' and CRHL.CRHotelListID= @CurrentUpdatePrefCrewHotelID                                
--         AND CRHL.recordtype = 'C'             
--         AND CRHL.crhotellistid =             
--          @CurrentUpdatePrefHotelID       
        
           
--       END             
--      ELSE             
--      BEGIN             
--        --DECLARE @PreflightCrewHotelListID BIGINT                                    
--        DECLARE @PreflightHotelListID BIGINT             
                           
--        --    EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo', @PreflightCrewHotelListID  OUTPUT                                
--        EXECUTE dbo.Usp_getsequencenumber             
--       @CustomerID,             
--       'PreflightCurrentNo',             
--       @PreflightHotelListID output             
                           
--        --#tempupdateprefhotellist          
--        -- INSERT INTO [dbo].[PreflightCrewHotelList]                                 
--        INSERT INTO [dbo].[preflighthotellist]             
--        -- ([PreflightCrewHotelListID]                                 
--        ([preflighthotellistid],             
--         [preflighthotelname]             
--         -- ,[PreflightCrewListID]                                 
--         ,             
--         [legid],             
--         [hotelid],             
--         [airportid],             
--         [roomtype],             
--         [roomrent],             
--         [roomconfirm],             
--         [roomdescription],             
--   [street],             
--         [cityname],             
--         [statename],             
--         [postalzipcd],             
--         [phonenum1],             
--         [phonenum2],             
--         [phonenum3],             
--         [phonenum4],             
--         [faxnum],             
--         [lastupduid],             
--         [lastupdts],             
--         [isdeleted],             
--         [hotelarrangedby],             
--         [rate],             
--         [datein],             
--         [dateout],             
--         [issmoking],             
--         [roomtypeid],             
--         [noofbeds],             
--         [isallcreworpax],             
--         [iscompleted],             
--         [isearlycheckin],             
--         [earlycheckinoption],             
--         [specialinstructions],             
--         [confirmationstatus],             
--         [comments],             
--         [customerid],             
--         [address1],             
--         [address2],             
--         [address3],             
--         [metroid],             
--         [city],             
--         [stateprovince],             
--         [countryid],             
--         [postalcode],             
--         status,             
--         isarrivalhotel,             
--         isdeparturehotel,             
--         crewpassengertype)             
--        SELECT             
--        --   @PreflightCrewHotelListID                                 
--        @PreflightHotelListID,             
--        NULL,             
--        @NewCRLegIDCREW,             
--        PCH.[hotelid],             
--        PCH.[airportid],             
--        NULL,             
--        0,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        @LastUpdUID,             
--        @LastUpdTS,             
--        PCH.[isdeleted],             
--        NULL,             
--        0,             
--        NULL,             
--        NULL,             
--        0,             
--        NULL,             
--        NULL,             
--        NULL,             
--        PCH.[iscompleted],             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        PCH.[customerid],             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,             
--        NULL,           
--        NULL,             
--        NULL,             
--        1,             
--        0,             
--        'C'             
--        --    FROM #TempUpdatePrefCrewHotellist PCH                        
--        --WHERE CRHotelListID = @CurrentUpdatePrefCrewHotelID                                    
--        FROM   #tempupdateprefhotellist PCH             
--        WHERE  crhotellistid = @CurrentUpdatePrefHotelID             
--      END             
                
--      -- DELETE FROM #TempUpdatePrefCrewHotellist where CRHotelListID = @CurrentUpdatePrefCrewHotelID                                
--      DELETE FROM #tempupdateprefhotellist             
--      WHERE  crhotellistid = @CurrentUpdatePrefHotelID             
--      END             
          
--  DROP TABLE #tempupdateprefhotellist             
            
--  --    DROP TABLE #TempUpdatePrefCrewHotellist                                  
--  ---ends here                                   
--  ---Update passenger hotel                                 
--  DECLARE @CurrentUpdatePrefPaxHotelID BIGINT             
            
--  CREATE TABLE #tempupdateprefpaxhotellist             
--  (   
--  [crhotellistid]          [BIGINT] NOT NULL,             
--  [customerid]             [BIGINT] NULL,             
--  [crlegid]                [BIGINT] NULL,             
--  [tripnum]                [BIGINT] NULL,             
--  [legid]                  [BIGINT] NULL,             
--  [recordtype]             [CHAR](2) NULL,             
--  [hotelid]                [BIGINT] NULL,             
--  [crhotellistdescription] [VARCHAR](60) NULL,             
--  [phonenum]               [VARCHAR](25) NULL,             
--  [rate]                   [NUMERIC](6, 2) NULL,             
--  [iscompleted]            [BIT] NULL,             
--  [airportid]              [BIGINT] NULL,             
--  [lastupduid]             [VARCHAR](30) NULL,             
--  [lastupdts]              [DATETIME] NULL,             
--  [isdeleted]              [BIT] NOT NULL,             
--  [faxnum]                 [VARCHAR](25) NULL,             
--  [email]                  [VARCHAR](250) NULL             
--  )             
            
--  INSERT INTO #tempupdateprefpaxhotellist             
--  SELECT PPH.*             
--  FROM   crhotellist PPH,             
--  crleg IP,             
--  crmain T,             
--  preflightmain C             
--  WHERE  PPH.crlegid = IP.crlegid             
--  AND IP.crmainid = T.crmainid             
--  AND T.tripid = C.tripid             
--  AND T.crmainid = @CRMainID             
--  AND IP.legnum = @LastCRLegNum             
--  AND C.tripid = @NewTripID             
--  AND PPH.recordtype = 'P'             
--  ORDER  BY PPH.crhotellistid ASC             
            
--  WHILE (SELECT Count(*)             
--  FROM   #tempupdateprefpaxhotellist) > 0             
--  BEGIN             
--  DECLARE @NewCRLegIDPAX BIGINT             
            
--  SET @NewCRLegIDPAX = (SELECT legid             
--                  FROM   preflightleg             
--                  WHERE  tripid = @NewTripID             
--                         AND legnum = @LastCRLegNum)             
            
--  -- Get current crpassenger id                                 
--  SELECT TOP 1 @CurrentUpdatePrefPaxHotelID =             
--         crhotellistid             
--  FROM   #tempupdateprefpaxhotellist             
            
--  DECLARE @ARRPAXHOTELID BIGINT             
            
--  SELECT TOP 1 @ARRPAXHOTELID = hotelid             
--  FROM   #tempupdateprefpaxhotellist             
            
--  IF EXISTS (SELECT hotel.hotelid             
--       FROM   preflightpassengerhotellist hotel             
--              JOIN preflightpassengerlist PP             
--                ON             
--  hotel.preflightpassengerlistid = PP.preflightpassengerlistid             
--  JOIN preflightleg leg             
--  ON PP.legid = leg.legid             
--  WHERE  hotel.hotelid = @ARRPAXHOTELID             
--  AND leg.legnum = @LastCRLegNum             
--  AND leg.tripid = @NewTripID)             
--  BEGIN             
--  UPDATE passengerhotel             
--  SET    CustomerID = @CustomerID,             
--  HotelID = CRH.hotelid,             
--  IsCompleted = CRH.iscompleted,             
--  AirportID = CRH.airportid,             
--  LastUpdUID = @LastUpdUID,             
--  LastUpdTS = @LastUpdTS             
--  FROM   preflightpassengerhotellist passengerhotel             
--  JOIN preflightpassengerlist PCH             
--  ON             
--  passengerhotel.preflightpassengerlistid = PCH.preflightpassengerlistid             
--  JOIN preflightleg PL             
--  ON PCH.legid = PL.legid             
--  AND PL.tripid = @NewTripID             
--  JOIN preflightmain PM             
--  ON PL.tripid = PM.tripid             
--  JOIN crmain CRM             
--  ON CRM.tripid = PM.tripid             
--  AND CRM.crmainid = @CRMainID             
--  JOIN crleg leg             
--  ON leg.crmainid = CRM.crmainid             
--  JOIN crhotellist CRH             
--  ON CRH.crlegid = leg.crlegid             
--  WHERE  PM.tripid = @NewTripID             
--  AND leg.crmainid = @CRMainID             
--  AND CRH.recordtype = 'P'             
--  AND CRH.crhotellistid = @CurrentUpdatePrefPaxHotelID             
--  END             
--  ELSE             
--  BEGIN             
--  DECLARE @PreflightPaxHotelListID BIGINT             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--  @CustomerID,             
--  'PreflightCurrentNo',             
--  @PreflightPaxHotelListID output             
            
--  INSERT INTO [dbo].[preflightpassengerhotellist]             
--  ([preflightpassengerhotellistid],             
--   [preflighthotelname],             
--   [preflightpassengerlistid],             
--   [hotelid],             
--   [airportid],             
--   [roomtype],             
--   [roomrent],             
--   [roomconfirm],             
--   [roomdescription],             
--   [street],             
--   [cityname],             
--   [statename],             
--   [postalzipcd],             
--   [phonenum1],             
--   [phonenum2],             
--   [phonenum3],             
--   [phonenum4],             
--   [faxnum],             
--   [lastupduid],             
--   [lastupdts],             
--   [isdeleted],             
--   [hotelarrangedby],             
--   [rate],             
--   [datein],             
--   [dateout],             
--   [issmoking],             
--   [roomtypeid],             
--   [noofbeds],             
--   [isallcrew],             
--   [iscompleted],             
--   [isearlycheckin],             
--   [earlycheckinoption],             
--   [specialinstructions],             
--   [confirmationstatus],             
--   [comments],             
--   [customerid],             
--   [address1],             
--   [address2],             
--   [address3],             
--   [metroid],             
--   [city],             
--   [stateprovince],             
--   [countryid],             
--   [postalcode])             
--  SELECT @PreflightPaxHotelListID,             
--  NULL,             
--  NULL,             
--  PCH.[hotelid],             
--  PCH.[airportid],             
--  NULL,             
--  0,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  @LastUpdUID,             
--  @LastUpdTS,             
--  PCH.[isdeleted],             
--  NULL,             
--  0,             
--  NULL,             
--  NULL,             
--  0,             
--  NULL,             
--  NULL,             
--  NULL,             
--  PCH.[iscompleted],             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  PCH.[customerid],             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL,             
--  NULL             
--  FROM   #tempupdateprefpaxhotellist PCH             
--  WHERE  crhotellistid = @CurrentUpdatePrefPaxHotelID             
--  END             
            
--  DELETE FROM #tempupdateprefpaxhotellist             
--  WHERE  crhotellistid = @CurrentUpdatePrefPaxHotelID             
--  END             
            
--  DROP TABLE #tempupdateprefpaxhotellist             
            
--  ---ends here                                  
--  ---Update Crew Transport                                 
--  DECLARE @CurrentPrefTransportID BIGINT             
            
--  CREATE TABLE #temppreftransportlist             
--  (             
--  [crtransportlistid]          [BIGINT] NOT NULL,             
--  [customerid]                 [BIGINT] NULL,             
--  [crlegid]              [BIGINT] NULL,             
--  [tripnum]                    [BIGINT] NULL,             
--  [legid]                      [BIGINT] NULL,             
--  [recordtype]                 [CHAR](2) NULL,             
--  [transportid]                [BIGINT] NULL,             
--  [crtransportlistdescription] [VARCHAR](60) NULL,             
--  [phonenum]                   [VARCHAR](25) NULL,             
--  [rate]                       [NUMERIC](6, 2) NULL,             
--  [iscompleted]                [BIT] NULL,             
--  [airportid]                  [BIGINT] NULL,             
--  [lastupduid]                 [VARCHAR](30) NULL,             
--  [lastupdts]                  [DATETIME] NULL,             
--  [isdeleted]                  [BIT] NOT NULL,             
--  [faxnum]                     [VARCHAR](25) NULL,             
--  [email]                      [VARCHAR](250) NULL             
--  )             
            
--  INSERT INTO #temppreftransportlist             
--  SELECT PCT.*             
--  FROM   crtransportlist PCT,             
--  crleg IP,             
--  crmain T,             
--  preflightmain C             
--  WHERE  PCT.crlegid = IP.crlegid             
--  AND IP.crmainid = T.crmainid             
--  AND T.tripid = C.tripid             
--  AND C.tripid = @NewTripID             
--  AND IP.legnum = @LastCRLegNum            
--  AND T.crmainid = @CRMainID             
--  AND PCT.recordtype = 'C'             
--  ORDER  BY PCT.crtransportlistid ASC             
            
--  WHILE (SELECT Count(*)             
--  FROM   #temppreftransportlist) > 0             
--  BEGIN             
--  DECLARE @NewCRLegIDTrans BIGINT             
            
--  SET @NewCRLegIDTrans = (SELECT legid             
--                    FROM   preflightleg             
--                    WHERE             
--  tripid = @NewTripID             
--  AND legnum = @LastCRLegNum)             
            
--  -- Get current crpassenger id                                 
--  SELECT TOP 1 @CurrentPrefTransportID = crtransportlistid             
--  FROM   #temppreftransportlist             
            
--  DECLARE @ARRTRansportID BIGINT             
            
--  SELECT TOP 1 @ARRTRansportID = transportid             
--  FROM   #temppreftransportlist             
            
--  IF EXISTS (SELECT Transport.transportid             
--       FROM   preflighttransportlist Transport             
--              JOIN preflightleg leg             
--                ON Transport.legid = leg.legid             
--       WHERE             
--  Transport.transportid = @ARRTRansportID             
--  AND leg.legnum = @LastCRLegNum             
--  AND leg.tripid = @NewTripID)             
--  BEGIN             
--  UPDATE transport             
--  SET    CustomerID = @CustomerID,             
--         TransportID = CRH.transportid,             
--         IsCompleted = CRH.iscompleted,             
--         AirportID = CRH.airportid,             
--         LastUpdUID = @LastUpdUID,             
--         LastUpdTS = @LastUpdTS,             
--         CrewPassengerType = 'C'             
--  FROM   preflighttransportlist transport             
--         JOIN preflightleg PL             
--           ON transport.legid = PL.legid             
--              AND PL.tripid = @NewTripID             
--         JOIN preflightmain PM             
--           ON PL.tripid = PM.tripid             
--         JOIN crmain CRM             
--           ON CRM.tripid = PM.tripid             
--              AND CRM.crmainid = @CRMainID             
--         JOIN crleg leg             
--           ON leg.crmainid = CRM.crmainid             
--         JOIN crtransportlist CRH             
--           ON CRH.crlegid = leg.crlegid             
--  WHERE  PM.tripid = @NewTripID             
--         AND leg.crmainid = @CRMainID             
--         AND CRH.crtransportlistid =             
--             @CurrentPrefTransportID             
--         AND transport.isarrivaltransport = 1             
--         AND transport.crewpassengertype = 'C'             
--  END             
--  ELSE             
--  BEGIN             
--  DECLARE @PrefTransportListID BIGINT             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--    @CustomerID,             --    'PreflightCurrentNo',             
--    @PrefTransportListID output       
            
--  INSERT INTO [dbo].[preflighttransportlist]             
--              ([preflighttransportid],             
--               [preflighttransportname],             
--               [legid],             
--               [transportid],             
--               [isarrivaltransport],             
--               [isdeparturetransport],             
--               [airportid],             
--               [crewpassengertype],             
--               [street],             
--               [cityname],             
--               [statename],             
--               [postalzipcd],             
--               [phonenum1],             
--               [phonenum2],             
--               [phonenum3],             
--               [phonenum4],             
--               [faxnum],             
--               [lastupduid],             
--               [lastupdts],             
--               [isdeleted],             
--               [confirmationstatus],             
--               [comments],             
--               [iscompleted],             
--               [customerid])             
--  SELECT @PrefTransportListID,             
--         NULL,             
--         @NewCRLegIDTrans,             
--         PCT.[transportid],             
--         1,             
--         0,             
--         PCT.[airportid],             
--         'C',             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         @LastUpdUID,             
--         @LastUpdTS,             
--         PCT.[isdeleted],             
--         NULL,             
--         NULL,             
--   PCT.[iscompleted],             
--         PCT.[customerid]             
--  FROM   #temppreftransportlist PCT             
--  WHERE  crtransportlistid = @CurrentPrefTransportID             
--  END             
            
--  DELETE FROM #temppreftransportlist             
--  WHERE  crtransportlistid = @CurrentPrefTransportID             
--  END             
            
--  DROP TABLE #temppreftransportlist             
            
--  ---ends here                                  
--  ---Update Pax Transport                                 
--  DECLARE @CurrentPrefPaxTransportID BIGINT             
            
--  CREATE TABLE #tempprefpaxtransportlist             
--  (             
--  [crtransportlistid]          [BIGINT] NOT NULL,             
--  [customerid]                 [BIGINT] NULL,             
--  [crlegid]                    [BIGINT] NULL,             
--  [tripnum]                    [BIGINT] NULL,             
--  [legid]                      [BIGINT] NULL,             
--  [recordtype]                 [CHAR](2) NULL,             
--  [transportid]                [BIGINT] NULL,             
--  [crtransportlistdescription] [VARCHAR](60) NULL,             
--  [phonenum]                   [VARCHAR](25) NULL,             
--  [rate]                       [NUMERIC](6, 2) NULL,             
--  [iscompleted]                [BIT] NULL,             
--  [airportid]                  [BIGINT] NULL,             
--  [lastupduid]                 [VARCHAR](30) NULL,             
--  [lastupdts]                  [DATETIME] NULL,             
--  [isdeleted]                  [BIT] NOT NULL,             
--  [faxnum]                     [VARCHAR](25) NULL,             
--  [email]                      [VARCHAR](250) NULL             
--  )             
            
--  INSERT INTO #tempprefpaxtransportlist             
--  SELECT PCT.*             
--  FROM   crtransportlist PCT,             
--  crleg IP,             
--  crmain T,             
--  preflightmain C             
--  WHERE  PCT.crlegid = IP.crlegid             
--  AND IP.crmainid = T.crmainid      
--  AND T.tripid = C.tripid             
--  AND C.tripid = @NewTripID             
--  AND IP.legnum = @LastCRLegNum             
--  AND T.crmainid = @CRMainID             
--  AND PCT.recordtype = 'P'             
--  ORDER  BY PCT.crtransportlistid ASC             
            
--  WHILE (SELECT Count(*)             
--  FROM   #tempprefpaxtransportlist) > 0             
--  BEGIN             
--  DECLARE @NewCRLegIDPaxTrans BIGINT             
            
--  SET @NewCRLegIDPaxTrans = (SELECT legid             
--                       FROM   preflightleg             
--                       WHERE             
--  tripid = @NewTripID             
--  AND legnum = @LastCRLegNum)             
            
--  -- Get current crpassenger id                                 
--  SELECT TOP 1 @CurrentPrefPaxTransportID =             
--         crtransportlistid             
--  FROM   #tempprefpaxtransportlist             
            
--  DECLARE @ARRPaxTRansportID BIGINT             
            
--  SELECT TOP 1 @ARRPaxTRansportID = transportid             
--  FROM   #tempprefpaxtransportlist             
            
--  IF EXISTS (SELECT Transport.transportid             
--       FROM   preflighttransportlist Transport             
--              JOIN preflightleg leg             
--                ON Transport.legid = leg.legid             
--       WHERE             
--  Transport.transportid = @ARRPaxTRansportID             
--  AND leg.legnum = @LastCRLegNum             
--  AND leg.tripid = @NewTripID)             
--  BEGIN             
--  UPDATE transport             
--  SET    CustomerID = @CustomerID,             
--         TransportID = CRH.transportid,             
--         IsCompleted = CRH.iscompleted,             
--         AirportID = CRH.airportid,             
--         LastUpdUID = @LastUpdUID,             
--         LastUpdTS = @LastUpdTS,             
--         CrewPassengerType = 'P'             
--  FROM   preflighttransportlist transport             
--         JOIN preflightleg PL             
--           ON transport.legid = PL.legid             
--              AND PL.tripid = @NewTripID             
--         JOIN preflightmain PM             
--           ON PL.tripid = PM.tripid             
--         JOIN crmain CRM             
--           ON CRM.tripid = PM.tripid             
--              AND CRM.crmainid = @CRMainID             
--         JOIN crleg leg             
--           ON leg.crmainid = CRM.crmainid             
--         JOIN crtransportlist CRH             
--           ON CRH.crlegid = leg.crlegid             
--  WHERE  PM.tripid = @NewTripID             
--         AND leg.crmainid = @CRMainID             
--         AND CRH.crtransportlistid =             
--             @CurrentPrefPaxTransportID             
--         AND transport.isarrivaltransport = 1             
--         AND transport.crewpassengertype = 'P'             
--  END             
--  ELSE             
--  BEGIN             
--  DECLARE @PrefPaxTransportListID BIGINT             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--    @CustomerID,             
--    'PreflightCurrentNo',             
--    @PrefPaxTransportListID output             
            
--  INSERT INTO [dbo].[preflighttransportlist]             
--              ([preflighttransportid],             
--               [preflighttransportname],             
--               [legid],             
--      [transportid],             
--               [isarrivaltransport],             
--               [isdeparturetransport],             
--               [airportid],             
--               [crewpassengertype],             
--               [street],             
--               [cityname],             
--               [statename],             
--               [postalzipcd],             
--               [phonenum1],             
--               [phonenum2],             
--               [phonenum3],             
--               [phonenum4],             
-- [faxnum],             
--               [lastupduid],             
--               [lastupdts],             
--               [isdeleted],             
--               [confirmationstatus],             
--               [comments],             
--               [iscompleted],             
--               [customerid])             
--  SELECT @PrefPaxTransportListID,             
--         NULL,             
--         @NewCRLegIDPaxTrans,             
--         PCT.[transportid],             
--         1,             
--         0,             
--         PCT.[airportid],             
--         'P',             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         NULL,             
--         @LastUpdUID,             
--         @LastUpdTS,             
--         PCT.[isdeleted],             
--         NULL,             
--         NULL,             
--         PCT.[iscompleted],             
--         PCT.[customerid]             
--  FROM   #tempprefpaxtransportlist PCT             
--  WHERE  crtransportlistid =             
--         @CurrentPrefPaxTransportID             
--  END             
            
--  DELETE FROM #tempprefpaxtransportlist             
--  WHERE  crtransportlistid = @CurrentPrefPaxTransportID             
--  END             
            
--  DROP TABLE #tempprefpaxtransportlist             
            
--  ---ends here                                  
--  SET @LastCRLegNum = @LastCRLegNum + 1             
            
--  IF ( @LastCRLegNum > (SELECT Max(legnum)             
--            FROM   preflightleg             
--            WHERE             
--  preflightleg.tripid = @NewTripID) )             
--  BEGIN             
--  BREAK             
--  END             
--  END             
            
--  ---Unmatched Records                                 
--  WHILE (SELECT Count(1)             
--  FROM   crleg             
--  JOIN preflightleg             
--  ON crleg.legnum != preflightleg.legnum             
--  WHERE  crleg.crmainid = @CRMainID             
--  AND crleg.legnum = @LastCRLegNum             
--  AND preflightleg.tripid = @NewTripID             
--  AND crleg.isdeleted = 0) > 0             
--  BEGIN             
--  ---Unmatched Records- Insert into crLeg             
--  SET @CRLegnum = (SELECT Max(legnum + 1)             
--       FROM   preflightleg             
--       WHERE  preflightleg.tripid = @NewTripID)             
            
--  IF ( @CRLegnum = @LastCRLegNum )             
--  BEGIN             
--  DECLARE @InsLeg BIGINT             
            
--  SET @InsLeg = 0             
            
--  EXECUTE dbo.Usp_getsequencenumber             
--  @CustomerID,             
--  'PreflightCurrentNo',             
--  @InsLeg output             
            
--  -- Insert the temp leg record into preflightleg table                                 
--  INSERT INTO preflightleg             
--        (legid,             
--         tripid,             
--         customerid,             
--         tripnum,             
--         legnum,             
--         departicaoid,             
--         arriveicaoid,             
--         distance,             
--         powersetting,             
--         takeoffbias,             
--         landingbias,             
--         trueairspeed,             
--         windsboeingtable,             
--         elapsetm,             
--         isdepartureconfirmed,             
--         isarrivalconfirmation,             
--         isapproxtm,             
--         isscheduledservices,             
--         departuredttmlocal,             
--         arrivaldttmlocal,             
--         departuregreenwichdttm,             
--         arrivalgreenwichdttm,             
--         homedeparturedttm,             
--         homearrivaldttm,             
--         gotime,             
--         fboid,             
--         logbreak,             
--         flightpurpose,             
--         passengerrequestorid,             
--         requestorname,             
--         departmentid,             
--         departmentname,             
--         authorizationid,             
--         authorizationname,             
--         passengertotal,             
--         seattotal,             
--         reservationtotal,             
--         reservationavailable,             
--         waitnum,             
--         dutytype,             
--         dutyhours,             
--         resthours,             
--         flighthours,             
--         notes,             
--         fuelload,             
--         outbountinstruction,             
--         dutytype1,             
--         iscrewdiscount,             
--  crewdutyrulesid,             
--         isdutyend,             
--         fedaviationregnum,             
--         clientid,             
--         legid1,             
--         windreliability,             
--         overridevalue,             
--         checkgroup,             
--         additionalcrew,             
--         pilotincommand,             
--         secondincommand,             
--         nextlocaldttm,             
--         nextgmtdttm,             
--         nexthomedttm,                    lastupduid,             
--         lastupdts,             
--         crewnotes,             
--         isprivate,             
--         waitlist,             
--         flightnum,             
--         flightcost,             
--         crewfuelload,             
--         isdeleted,             
--         uwaid,             
--         uscrossing,             
--         response,             
--         confirmid,             
--         crewdutyalert,             
--         accountid,             
--         flightcategoryid)             
--  SELECT @InsLeg,             
--   @NewTripID,             
--   @CustomerID,             
--   NULL --[TripNUM]                                     
--   ,             
--   IP.[legnum],             
--   IP.dairportid,             
--   IP.aairportid,             
--   IP.[distance],             
--   IP.[powersetting],             
--   IP.[takeoffbias],             
--   IP.[landingbias],             
--   IP.[trueairspeed],             
--   IP.[windsboeingtable],             
--   IP.[elapsetm],             
--   IP.[isdepartureconfirmed],             
--   IP.[isarrivalconfirmation],             
--   0 --[IsApproxTM]                                     
--   ,             
--   NULL             
--   --[IsScheduledServices]                                      
--   ,             
--   IP.departuredttmlocal,             
--   IP.arrivaldttmlocal,             
--   IP.departuregreenwichdttm,             
--   IP.arrivalgreenwichdttm,             
--   IP.homedeparturedttm,             
--   IP.homearrivaldttm,             
--   NULL --[GoTime]                                     
--   ,             
--   NULL --[FBOID]                                     
--   ,             
--   NULL --[LogBreak]                                     
--   ,             
--   NULL --[FlightPurpose]                                     
--   ,             
--   NULL             
--   --[PassengerRequestorID]                                     
--   ,             
--   NULL --[RequestorName]                                     
--   ,             
--   NULL --[DepartmentID]                                     
--   ,             
--   NULL --[DepartmentName]                                     
--   ,             
--   NULL --[AuthorizationID]                                     
--   ,             
--   NULL             
--   --[AuthorizationName]                                     
--   ,             
--   IP.passengertotal,             
--   IP.seattotal,             
--   IP.reservationtotal,             
--   IP.reservationavailable,             
--   NULL --[WaitNUM]                                     
--   ,             --   NULL --[DutyTYPE]                                     
--   ,             
--   NULL --[DutyHours]                                     
--   ,             
--   NULL --[RestHours]                                     
--   ,             
--   NULL --[FlightHours]                         
--   ,             
--   NULL --[Notes]                                     
--   ,             
--   NULL --[FuelLoad]                                     
--   ,             
--   NULL             
--   --[OutbountInstruction]                                     
--   ,             
--   NULL --[DutyTYPE1]                                     
--   ,             
--   NULL --[IsCrewDiscount]                                     
--   ,             
--   NULL --[CrewDutyRulesID]                                     
--   ,             
--   0 --[IsDutyEnd]                                     
--   ,            
--   NULL --[FedAviationRegNUM]                            
--   ,             
--   NULL --[ClientID]                                     
--   ,             
--   NULL --[LegID1]                                     
--   ,             
--   IP.[windreliability],             
--   NULL --[OverrideValue]                                     
--   ,             
--   NULL --[CheckGroup]                                     
--   ,             
--   NULL --[AdditionalCrew]                                     
--   ,             
--   NULL --[PilotInCommand]                                     
--   ,             
--   NULL --[SecondInCommand]                                     
--   ,             
--   NULL             
--   --[NextLocalDTTM]    --need to modify                                 
--   ,             
--   NULL             
--   --[NextGMTDTTM]        --need to modify                                 
--   ,             
--   NULL             
--   --[NextHomeDTTM]        --need to modify                                 
--   ,             
--   IP.[lastupduid],             
--   IP.[lastupdts] --@LastUpdUID                                 
--   ,             
--   NULL --[CrewNotes]                                     
--   ,             
--   0,             
--   NULL --[WaitList]                                     
--   ,             
--   NULL --[FlightNUM]                         
--   ,             
--   0 --cost                                 
--   ,             
--   NULL --[CrewFuelLoad]                        
--   ,             
--   0,             
--   NULL --[UWAID]                                     
--   ,             
--   NULL --[USCrossing]                                     
--   ,             
--   NULL --[Response]                                     
--   ,             
--   NULL --[ConfirmID]                                     
--   ,             
--   NULL --[CrewDutyAlert]                                     
--   ,             
--   NULL --[AccountID]                                     
--   ,             
--   NULL --[FlightCategoryID]                                     
--  FROM   crleg IP,             
--   crmain T,             
--   preflightmain C             
--  WHERE  IP.crmainid = T.crmainid             
--   AND T.tripid = C.tripid             
--   AND T.tripid = @NewTripID             
--   AND C.tripid = @NewTripID             
--   AND T.crmainid = @CRMainID             
--   AND IP.legnum = @LastCRLegNum             
            
--  DECLARE @CurrCRlegId BIGINT             
            
--  -- Get the Cr Leg id and  preflight new leg id                                  
--  SELECT TOP 1 @CurrCRlegId = crlegid             
--  FROM   crleg IP,             
--   crmain T             
--  WHERE  IP.crmainid = T.crmainid             
--   AND T.crmainid = @CRMainID             
--   AND IP.legnum = @LastCRLegNum             
            
--  EXECUTE Spflightpak_corporaterequest_queuetransferinfo             
--  @CRMainID,             
--  @CustomerID,             
--  @LastUpdUID,             
--  @InsLeg,             
--  @CurrCRlegId             
--  END             
            --  SET @LastCRLegNum = @LastCRLegNum + 1             
--  END             
--  END             
            
--  UPDATE crmain             
--  SET    tripid = @NewTripID,             
--  tripstatus = @TripStatus,             
--  corporaterequeststatus = CASE             
--            WHEN @TripStatus != 'C' THEN 'T'             
--            ELSE 'C'             
--          END,             
--  --AcknowledgementStatus ='A',                                 
--  lastupduid = @LastUpdUID,             
--  lastupdts = @LastUpdTS             
--  WHERE  crmainid = @CRMainID             
            
--  DECLARE @HistoryDescription VARCHAR(max)             
--  DECLARE @HistoryDescr VARCHAR(max)             
--  DECLARE @RevisionNum INT             
            
--  SET @RevisionNum =(SELECT Isnull(Max(revisionnumber), 1)             
--  FROM   crhistory             
--  WHERE  crmainid = @CRMainID             
--    AND customerid = @CustomerID)             
--  --SET @HistoryDescription = 'Acknowledged Changes Made By Dispatch.' +  '                      '  + 'Updating Existing Tripsheet From Corp. Request: ' + CAST(@CRTRIPNUM AS VARCHAR(50))                                
--  SET @HistoryDescr = (SELECT TOP 1 logisticshistory             
--  FROM   crhistory             
--  WHERE  crmainid = @CRMainID             
--      AND customerid = @CustomerID             
--      AND revisionnumber = @RevisionNum)             
--  SET @HistoryDescription = @HistoryDescr             
--    +             
--  'Updating Existing Tripsheet From Corp. Request: '             
--    + Cast(@CRTRIPNUM AS VARCHAR(50))             
            
--  EXECUTE Spflightpak_preflight_addpreflighttriphistory             
--  @NewTripID,             
--  @CustomerID,             
--  @HistoryDescription,             
--  @LastUpdUID,             
--  @LastUpdTS             
--END             
            
      SELECT @NewTripID AS RESULT             
  END 
  GO