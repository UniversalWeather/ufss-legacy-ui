
/****** Object:  StoredProcedure [dbo].[spUpdateFleetInformation]    Script Date: 09/13/2012 12:45:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFleetInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFleetInformation]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFleetInformation]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[spUpdateFleetInformation]      
(           @FleetInformationID BIGINT  
           ,@CustomerID BIGINT  
           ,@FleetID BIGINT  
           ,@AircraftCD char(3)    
           ,@AirframeHrs numeric(11,3)    
           ,@AirframeCycle numeric(6,0)    
           ,@AIrframeAsOfDT date    
           ,@Engine1Hrs numeric(11,3)    
           ,@Engine1Cycle numeric(6,0)    
           ,@Engine1AsOfDT date    
           ,@Engine2Hrs numeric(11,3)    
           ,@Engine2Cycle numeric(6,0)    
           ,@Engine2AsOfDT date    
           ,@Engine3Hrs numeric(11,3)    
           ,@Engine3Cycle numeric(6,0)    
           ,@Engine3AsOfDT date    
           ,@Engine4Hrs numeric(11,3)    
           ,@Engine4Cycle numeric(6,0)    
           ,@Engine4AsOfDT date    
           ,@Reverse1Hrs numeric(11,3)    
           ,@Reverser1Cycle numeric(6,0)    
           ,@Reverser1AsOfDT date    
           ,@Reverse2Hrs numeric(11,3)    
           ,@Reverser2Cycle numeric(6,0)    
           ,@Reverser2AsOfDT date    
           ,@Reverse3Hrs numeric(11,3)    
           ,@Reverser3Cycle numeric(6,0)    
           ,@Reverser3AsOfDT date    
           ,@Reverse4Hrs numeric(11,3)    
           ,@Reverser4Cycle numeric(6,0)    
           ,@Reverser4AsOfDT date    
           ,@LastUpdUID varchar(30)    
           ,@LastUpdTS datetime     
           ,@MACHFuel1Hrs numeric(5,0)    
           ,@MACHFuel2Hrs numeric(5,0)    
           ,@MACHFuel3Hrs numeric(5,0)    
           ,@MACHFuel4Hrs numeric(5,0)    
           ,@MACHFuel5Hrs numeric(5,0)    
           ,@MACHFuel6Hrs numeric(5,0)    
           ,@MACHFuel7Hrs numeric(5,0)    
           ,@MACHFuel8Hrs numeric(5,0)    
           ,@MACHFuel9Hrs numeric(5,0)    
           ,@MACHFuel10Hrs numeric(5,0)    
           ,@MACHFuel11Hrs numeric(5,0)    
           ,@MACHFuel12Hrs numeric(5,0)    
           ,@MACHFuel13Hrs numeric(5,0)    
           ,@MACHFuel14Hrs numeric(5,0)    
           ,@MACHFuel15Hrs numeric(5,0)   
           ,@MACHFuel16Hrs NUMERIC(5,0)    
           ,@MACHFuel17Hrs NUMERIC(5,0)    
           ,@MACHFuel18Hrs NUMERIC(5,0)    
           ,@MACHFuel19Hrs NUMERIC(5,0)    
           ,@MACHFuel20Hrs NUMERIC(5,0)  
           ,@LongRangeFuel1Hrs numeric(5,0)    
           ,@LongRangeFuel2Hrs numeric(5,0)    
           ,@LongRangeFuel3Hrs numeric(5,0)    
           ,@LongRangeFuel4Hrs numeric(5,0)    
           ,@LongRangeFuel5Hrs numeric(5,0)    
           ,@LongRangeFuel6Hrs numeric(5,0)    
           ,@LongRangeFuel7Hrs numeric(5,0)    
           ,@LongRangeFuel8Hrs numeric(5,0)    
           ,@LongRangeFuel9Hrs numeric(5,0)    
           ,@LongRangeFuel10Hrs numeric(5,0)    
           ,@LongRangeFuel11Hrs numeric(5,0)    
           ,@LongRangeFuel12Hrs numeric(5,0)    
           ,@LongRangeFuel13Hrs numeric(5,0)    
           ,@LongRangeFuel14Hrs numeric(5,0)    
           ,@LongRangeFuel15Hrs numeric(5,0)
           ,@LongRangeFuel16Hrs NUMERIC(5,0)    
           ,@LongRangeFuel17Hrs NUMERIC(5,0)    
           ,@LongRangeFuel18Hrs NUMERIC(5,0)    
           ,@LongRangeFuel19Hrs NUMERIC(5,0)    
           ,@LongRangeFuel20Hrs NUMERIC(5,0)     
           ,@HighSpeedFuel1Hrs numeric(5,0)    
           ,@HighSpeedFuel2Hrs numeric(5,0)    
           ,@HighSpeedFuel3Hrs numeric(5,0)    
           ,@HighSpeedFuel4Hrs numeric(5,0)    
           ,@HighSpeedFuel5Hrs numeric(5,0)    
           ,@HighSpeedFuel6Hrs numeric(5,0)    
           ,@HighSpeedFuel7Hrs numeric(5,0)    
           ,@HighSpeedFuel8Hrs numeric(5,0)    
           ,@HighSpeedFuel9Hrs numeric(5,0)    
           ,@HighSpeedFuel10Hrs numeric(5,0)    
           ,@HighSpeedFuel11Hrs numeric(5,0)    
           ,@HighSpeedFuel12Hrs numeric(5,0)    
           ,@HighSpeedFuel13Hrs numeric(5,0)    
           ,@HighSpeedFuel14Hrs numeric(5,0)    
           ,@HighSpeedFuel51Hrs numeric(5,0) 
           ,@HighSpeedFuel16Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel17Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel18Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel19Hrs NUMERIC(5,0)    
           ,@HighSpeedFuel20Hrs NUMERIC(5,0)     
           ,@Fuel1Hrs numeric(5,0)    
           ,@Fuel2Hrs numeric(5,0)    
           ,@Fuel3Hrs numeric(5,0)    
           ,@Fuel4Hrs numeric(5,0)    
           ,@Fuel5Hrs numeric(5,0)    
           ,@IsDeleted bit)    
-- =============================================    
-- Author:Mathes    
-- Create date: 28/6/2012    
-- Altered by Mathes on 05-07-12  
-- Description: IUpdate the FleetInformation    
-- =============================================    
as    
begin     
SET NoCOUNT ON    
  set @LastUpdTS = GETUTCDATE()  
    
    UPDATE [FleetInformation]    
    SET     
           [CustomerID] = @CustomerID,  
           [FleetID] = @FleetID,  
           [AircraftCD] = @AircraftCD,    
           [AirframeHrs] = @AirframeHrs,    
           [AirframeCycle] = @AirframeCycle,    
           [AIrframeAsOfDT] = @AIrframeAsOfDT,    
           [Engine1Hrs] = @Engine1Hrs,    
           [Engine1Cycle] = @Engine1Cycle,    
           [Engine1AsOfDT] = @Engine1AsOfDT,    
           [Engine2Hrs] = @Engine2Hrs,    
           [Engine2Cycle] = @Engine2Cycle,    
           [Engine2AsOfDT] = @Engine2AsOfDT,    
           [Engine3Hrs] = @Engine3Hrs,    
           [Engine3Cycle] = @Engine3Cycle,    
           [Engine3AsOfDT] =@Engine3AsOfDT,    
           [Engine4Hrs] = @Engine4Hrs,    
           [Engine4Cycle] = @Engine4Cycle,    
           [Engine4AsOfDT] = @Engine4AsOfDT,    
           [Reverse1Hrs] = @Reverse1Hrs,    
           [Reverser1Cycle] = @Reverser1Cycle,    
           [Reverser1AsOfDT] = @Reverser1AsOfDT,    
           [Reverse2Hrs] = @Reverse2Hrs,    
           [Reverser2Cycle] = @Reverser2Cycle,    
           [Reverser2AsOfDT] = @Reverser2AsOfDT,    
           [Reverse3Hrs] = @Reverse3Hrs,    
           [Reverser3Cycle] = @Reverser3Cycle,    
           [Reverser3AsOfDT] = @Reverser3AsOfDT,    
           [Reverse4Hrs] = @Reverse4Hrs,    
           [Reverser4Cycle] = @Reverser4Cycle,    
           [Reverser4AsOfDT] = @Reverser4AsOfDT,    
           [LastUpdUID] = @LastUpdUID,    
           [LastUpdTS] = @LastUpdTS,    
           [MACHFuel1Hrs] = @MACHFuel1Hrs,    
           [MACHFuel2Hrs] = @MACHFuel2Hrs,    
           [MACHFuel3Hrs] = @MACHFuel3Hrs,    
           [MACHFuel4Hrs] = @MACHFuel4Hrs,    
           [MACHFuel5Hrs] = @MACHFuel5Hrs,    
           [MACHFuel6Hrs] = @MACHFuel6Hrs,    
           [MACHFuel7Hrs] = @MACHFuel7Hrs,    
           [MACHFuel8Hrs] = @MACHFuel8Hrs,    
           [MACHFuel9Hrs] = @MACHFuel9Hrs,    
           [MACHFuel10Hrs] = @MACHFuel10Hrs,    
           [MACHFuel11Hrs] = @MACHFuel11Hrs,    
           [MACHFuel12Hrs] = @MACHFuel12Hrs,    
           [MACHFuel13Hrs] = @MACHFuel13Hrs,    
           [MACHFuel14Hrs] = @MACHFuel14Hrs,    
           [MACHFuel15Hrs] = @MACHFuel15Hrs,  
		   [MACHFuel16Hrs] = @MACHFuel16Hrs,
		   [MACHFuel17Hrs] = @MACHFuel17Hrs,
		   [MACHFuel18Hrs] = @MACHFuel18Hrs,
		   [MACHFuel19Hrs] = @MACHFuel19Hrs,
		   [MACHFuel20Hrs] = @MACHFuel20Hrs,             
           [LongRangeFuel1Hrs] = @LongRangeFuel1Hrs,    
           [LongRangeFuel2Hrs] = @LongRangeFuel2Hrs,    
           [LongRangeFuel3Hrs] = @LongRangeFuel3Hrs,    
           [LongRangeFuel4Hrs] = @LongRangeFuel4Hrs,    
           [LongRangeFuel5Hrs] = @LongRangeFuel5Hrs,    
           [LongRangeFuel6Hrs] = @LongRangeFuel6Hrs,    
           [LongRangeFuel7Hrs] = @LongRangeFuel7Hrs,    
           [LongRangeFuel8Hrs] = @LongRangeFuel8Hrs,    
           [LongRangeFuel9Hrs] = @LongRangeFuel9Hrs,    
           [LongRangeFuel10Hrs] = @LongRangeFuel10Hrs,    
           [LongRangeFuel11Hrs] = @LongRangeFuel11Hrs,    
           [LongRangeFuel12Hrs] = @LongRangeFuel12Hrs,    
           [LongRangeFuel13Hrs] = @LongRangeFuel13Hrs,    
           [LongRangeFuel14Hrs] = @LongRangeFuel14Hrs,    
           [LongRangeFuel15Hrs] = @LongRangeFuel15Hrs,  
		   [LongRangeFuel16Hrs] = @LongRangeFuel16Hrs,
		   [LongRangeFuel17Hrs] = @LongRangeFuel17Hrs,
		   [LongRangeFuel18Hrs] = @LongRangeFuel18Hrs,
		   [LongRangeFuel19Hrs] = @LongRangeFuel19Hrs,
		   [LongRangeFuel20Hrs] = @LongRangeFuel20Hrs,             
           [HighSpeedFuel1Hrs] = @HighSpeedFuel1Hrs,    
           [HighSpeedFuel2Hrs] = @HighSpeedFuel2Hrs,    
           [HighSpeedFuel3Hrs] = @HighSpeedFuel3Hrs,    
           [HighSpeedFuel4Hrs] = @HighSpeedFuel4Hrs,    
           [HighSpeedFuel5Hrs] = @HighSpeedFuel5Hrs,    
           [HighSpeedFuel6Hrs] = @HighSpeedFuel6Hrs,    
           [HighSpeedFuel7Hrs] = @HighSpeedFuel7Hrs,    
           [HighSpeedFuel8Hrs] = @HighSpeedFuel8Hrs,    
           [HighSpeedFuel9Hrs] = @HighSpeedFuel9Hrs,    
           [HighSpeedFuel10Hrs] = @HighSpeedFuel10Hrs,    
           [HighSpeedFuel11Hrs] = @HighSpeedFuel11Hrs,    
           [HighSpeedFuel12Hrs] = @HighSpeedFuel12Hrs,    
           [HighSpeedFuel13Hrs] = @HighSpeedFuel13Hrs,    
           [HighSpeedFuel14Hrs] = @HighSpeedFuel14Hrs,    
           [HighSpeedFuel51Hrs] = @HighSpeedFuel51Hrs,
		   [HighSpeedFuel16Hrs] = @HighSpeedFuel16Hrs,
		   [HighSpeedFuel17Hrs] = @HighSpeedFuel17Hrs,
		   [HighSpeedFuel18Hrs] = @HighSpeedFuel18Hrs,
		   [HighSpeedFuel19Hrs] = @HighSpeedFuel19Hrs,
		   [HighSpeedFuel20Hrs] = @HighSpeedFuel20Hrs,                
           [Fuel1Hrs] = @Fuel1Hrs,    
           [Fuel2Hrs] = @Fuel2Hrs,    
           [Fuel3Hrs] = @Fuel3Hrs,    
           [Fuel4Hrs] = @Fuel4Hrs,    
           [Fuel5Hrs] = @Fuel5Hrs,    
           [IsDeleted] = @IsDeleted    
     WHERE FleetInformationID=@FleetInformationID  
END  