

/****** Object:  StoredProcedure [dbo].[spGetPassengerAdditionalInfo]    Script Date: 05/24/2013 15:06:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassengerAdditionalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassengerAdditionalInfo]
GO



/****** Object:  StoredProcedure [dbo].[spGetPassengerAdditionalInfo]    Script Date: 05/24/2013 15:06:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetPassengerAdditionalInfo]        
(@CustomerID BIGINT,@ClientID BIGINT)    
AS        
BEGIN        
-- =============================================        
-- Author:  <Srinivasan. J>        
-- Create date: <11/06/2012>        
-- Description: <Procedure for fetching all record from Passenger Additional Info table>      
if(@ClientID>0)    
begin      
-- =============================================        
SELECT PassengerAdditionalInfo.PassengerAdditionalInfoID    
      ,PassengerAdditionalInfo.CustomerID    
      ,PassengerAdditionalInfo.PassengerRequestorID    
      ,PassengerAdditionalInfo.AdditionalINFOCD    
      ,PassengerAdditionalInfo.AdditionalINFODescription    
      ,PassengerAdditionalInfo.AdditionalINFOValue    
      ,PassengerAdditionalInfo.ClientID    
      ,PassengerAdditionalInfo.LastUpdUID    
      ,PassengerAdditionalInfo.LastUptTS    
      ,PassengerAdditionalInfo.IsDeleted    
      ,Passenger.PassengerRequestorCD    
      ,Passenger.PassengerName    
      ,Client.ClientCD    
      ,Client.ClientDescription
      ,PassengerAdditionalInfo.PassengerInformationID
      ,PassengerAdditionalInfo.IsPrint
  FROM PassengerAdditionalInfo     
  LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = PassengerAdditionalInfo.PassengerRequestorID    
  LEFT OUTER JOIN Client ON Client.ClientID = PassengerAdditionalInfo.ClientID        
  WHERE PassengerAdditionalInfo.CustomerID = @CustomerID  and PassengerAdditionalInfo.ClientID=@ClientID    
END    
else    
begin    
SELECT PassengerAdditionalInfo.PassengerAdditionalInfoID    
      ,PassengerAdditionalInfo.CustomerID    
      ,PassengerAdditionalInfo.PassengerRequestorID    
      ,PassengerAdditionalInfo.AdditionalINFOCD    
      ,PassengerAdditionalInfo.AdditionalINFODescription    
      ,PassengerAdditionalInfo.AdditionalINFOValue    
      ,PassengerAdditionalInfo.ClientID    
      ,PassengerAdditionalInfo.LastUpdUID    
      ,PassengerAdditionalInfo.LastUptTS    
      ,PassengerAdditionalInfo.IsDeleted    
      ,Passenger.PassengerRequestorCD    
      ,Passenger.PassengerName    
      ,Client.ClientCD    
      ,Client.ClientDescription
      ,PassengerAdditionalInfo.PassengerInformationID
      ,PassengerAdditionalInfo.IsPrint          
  FROM PassengerAdditionalInfo     
  LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = PassengerAdditionalInfo.PassengerRequestorID    
  LEFT OUTER JOIN Client ON Client.ClientID = PassengerAdditionalInfo.ClientID        
  WHERE PassengerAdditionalInfo.CustomerID = @CustomerID    
end    
end  
GO


