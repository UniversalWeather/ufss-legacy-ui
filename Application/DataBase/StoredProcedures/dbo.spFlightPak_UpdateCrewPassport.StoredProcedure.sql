
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateCrewPassport]    Script Date: 10/11/2013 15:15:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateCrewPassport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateCrewPassport]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateCrewPassport]    Script Date: 10/11/2013 15:15:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_UpdateCrewPassport]                        
(@PassportID bigint                      
,@CrewID bigint                      
,@PassengerRequestorID bigint                      
,@CustomerID bigint                      
,@Choice bit                      
,@IssueCity varchar(40)                      
,@PassportNum nvarchar(120)                      
,@PassportExpiryDT date                      
,@CountryID bigint                      
,@IsDefaultPassport bit                      
,@PilotLicenseNum varchar(25)                      
,@LastUpdUID varchar(30)                      
,@LastUpdTS datetime                     
,@IssueDT date                      
,@IsDeleted bit)                
            
-- =============================================                    
-- Author:Karthikeyan.S                    
-- Create date: 09/23/2013                   
-- Description: Update the PassportInfo       
-- Modified date: 10/04/2013              
-- Modified Description: If the entered Reocrd is Expired Date,allowed to save it directly    
-- Modified by: J. Ramesh
-- Modified date: 10/10/2013
-- Modified Description: Added condition to update isdeleted record
-- =============================================              
                    
AS                       
BEGIN                       
 SET NOCOUNT OFF;                
 DECLARE @ErrorMessage nVarchar(2000); 
 SET @LastUpdTS = GETUTCDATE()                  
 --For validating duplicate Passport Number for the customer and Passenger                
 IF (@PassengerRequestorID IS NOT NULL)                
 BEGIN                
	IF (@IsDeleted <> 1)                
    BEGIN       
		IF(GETDATE() > @PassportExpiryDT)      
		BEGIN        
			UPDATE [CrewPassengerPassport]                       
			   SET                         
			   Choice = @Choice                      
			   ,IssueCity = @IssueCity                      
			   ,PassportNum = @PassportNum                      
			   ,PassportExpiryDT = @PassportExpiryDT                      
			   ,CountryID = @CountryID                      
			   ,IsDefaultPassport = @IsDefaultPassport                      
			   ,PilotLicenseNum = @PilotLicenseNum                      
			   ,LastUpdUID = @LastUpdUID                      
			   ,LastUpdTS = @LastUpdTS                      
			   ,IssueDT = @IssueDT                      
			   ,IsDeleted = @IsDeleted                      
			   WHERE            
			   (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID     
		END         
		ELSE    
		BEGIN    
			IF(SELECT Count(PassportNum) FROM [CrewPassengerPassport] WHERE CustomerID = @CustomerID                
                    AND PassportID <> @PassportID                
                    AND CrewID IS NULL                
                    AND PassportNum = @PassportNum      
                    --AND PassportExpiryDT > GETDATE()               
                    AND GETDATE() < PassportExpiryDT               
                    AND CountryID = @CountryID                 
                    AND IsDeleted = 0)  > 0              
			BEGIN                
				SET @ErrorMessage = '-500012~' + @PassportNum;                
				RAISERROR(@ErrorMessage, 17, 1)                
			END         
			ELSE       
			BEGIN      
				UPDATE [CrewPassengerPassport]                       
				SET                         
				 Choice = @Choice                      
				   ,IssueCity = @IssueCity                      
				   ,PassportNum = @PassportNum                      
				   ,PassportExpiryDT = @PassportExpiryDT                      
				   ,CountryID = @CountryID                      
				   ,IsDefaultPassport = @IsDefaultPassport                      
				   ,PilotLicenseNum = @PilotLicenseNum                      
				   ,LastUpdUID = @LastUpdUID                      
				   ,LastUpdTS = @LastUpdTS                      
				   ,IssueDT = @IssueDT                      
				   ,IsDeleted = @IsDeleted                      
				   WHERE            
				   (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID       
		   END       
		END    
	END   
	ELSE --If IsDeleted = 1
	BEGIN
		UPDATE [CrewPassengerPassport]                       
			   SET                         
			   Choice = @Choice                      
			   ,IssueCity = @IssueCity                      
			   ,PassportNum = @PassportNum                      
			   ,PassportExpiryDT = @PassportExpiryDT                      
			   ,CountryID = @CountryID                      
			   ,IsDefaultPassport = @IsDefaultPassport                      
			   ,PilotLicenseNum = @PilotLicenseNum                      
			   ,LastUpdUID = @LastUpdUID                      
			   ,LastUpdTS = @LastUpdTS                      
			   ,IssueDT = @IssueDT                      
			   ,IsDeleted = @IsDeleted                      
			   WHERE            
			   (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID     
	END	             
 END                
 --For validating duplicate Passport Number for the customer and Crew                
 IF (@CrewID IS NOT NULL)                
 BEGIN 
	IF (@IsDeleted <> 1)                
	BEGIN       	
		IF(GETDATE() > @PassportExpiryDT)      
		BEGIN   		
		   UPDATE [CrewPassengerPassport]                       
		   SET                         
		   Choice = @Choice                      
		   ,IssueCity = @IssueCity                      
		   ,PassportNum = @PassportNum                      
		   ,PassportExpiryDT = @PassportExpiryDT                      
		   ,CountryID = @CountryID                      
		   ,IsDefaultPassport = @IsDefaultPassport                      
		   ,PilotLicenseNum = @PilotLicenseNum                      
		   ,LastUpdUID = @LastUpdUID                      
		   ,LastUpdTS = @LastUpdTS                      
		   ,IssueDT = @IssueDT                      
		   ,IsDeleted = @IsDeleted                      
		   WHERE            
		   (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID     
		END      
		ELSE    
		BEGIN  		  
			IF(SELECT Count(PassportNum) FROM [CrewPassengerPassport] WHERE CustomerID = @CustomerID                
                    AND PassportID <> @PassportID                
                    AND PassengerRequestorID IS NULL                
                    AND PassportNum = @PassportNum         
                    --AND PassportExpiryDT > GETDATE()            
                    AND GETDATE() < PassportExpiryDT                
                    AND CountryID = @CountryID                
                    AND IsDeleted = 0)  > 0              
			BEGIN 			
				SET @ErrorMessage = '-500012~' + @PassportNum;                
				RAISERROR(@ErrorMessage, 17, 1)                
			END              
			ELSE       
			BEGIN      			
                UPDATE [CrewPassengerPassport]                       
				SET                         
				 Choice = @Choice                      
				 ,IssueCity = @IssueCity                      
				 ,PassportNum = @PassportNum                      
				 ,PassportExpiryDT = @PassportExpiryDT                      
				 ,CountryID = @CountryID                      
				 ,IsDefaultPassport = @IsDefaultPassport                      
				 ,PilotLicenseNum = @PilotLicenseNum                      
				 ,LastUpdUID = @LastUpdUID        
				 ,LastUpdTS = @LastUpdTS                      
				 ,IssueDT = @IssueDT                      
				 ,IsDeleted = @IsDeleted                      
				 WHERE            
				 (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID       
			END                
		END    
    END                
    ELSE --If IsDeleted = 1
    BEGIN
		UPDATE [CrewPassengerPassport]                       
		SET                         
		 Choice = @Choice                      
		 ,IssueCity = @IssueCity                      
		 ,PassportNum = @PassportNum                      
		 ,PassportExpiryDT = @PassportExpiryDT                      
		 ,CountryID = @CountryID                      
		 ,IsDefaultPassport = @IsDefaultPassport                      
		 ,PilotLicenseNum = @PilotLicenseNum                      
		 ,LastUpdUID = @LastUpdUID        
		 ,LastUpdTS = @LastUpdTS                      
		 ,IssueDT = @IssueDT                      
		 ,IsDeleted = @IsDeleted                      
		 WHERE            
		 (PassengerRequestorID = isnull(@PassengerRequestorID,0) or CrewID=isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND PassportID=@PassportID           
	END                    
 END      
    
END 
GO

