

/****** Object:  StoredProcedure [dbo].[GetCrewResourceType]    Script Date: 09/13/2013 00:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCrewResourceType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCrewResourceType]
GO

/****** Object:  StoredProcedure [dbo].[GetCrewResourceType]    Script Date: 09/13/2013 00:13:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCrewResourceType]
(
	@CustomerID bigint, 
	@ClientID BIGINT, 
	@IN_CREW_IDS nvarchar(max), 
	@IN_HOMEBASE_IDS nvarchar(max), 
	@USERHOMEBASEID BIGINT, 
	@IsFixedCrewOnly bit, 
	@IsRotaryWingCrewOnly bit,
	@UserName CHAR(30),
	@CalenderType varchar(25)	
)
                              
AS

Declare @IsCrewGroup bit
set @IsCrewGroup = 0
IF EXISTS( SELECT 1 FROM dbo.PreflightScheduleCalenderPreferences WHERE CustomerID = @CustomerID AND UserName = @UserName and CalenderType = @CalenderType)
	Select @IsCrewGroup = IsCrewGroup from dbo.PreflightScheduleCalenderPreferences WHERE CustomerID = @CustomerID AND UserName = @UserName and CalenderType = @CalenderType

IF(@ClientID>0)   -- CLIENT ID FILTER     
BEGIN
  
		IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
		BEGIN				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				     IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
				     Begin
				            IF (@IsCrewGroup = 0) 
				            BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
							
					 End
					 Else
					 Begin
							IF (@IsCrewGroup = 0) 
				            BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
					 End
					 
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
					 IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
				     Begin
							IF (@IsCrewGroup = 0) 
				            BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
							
					 End
					 Else
					 Begin
							IF (@IsCrewGroup = 0) 
				            BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
					 End
				END
			   
		END
				
		ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
		BEGIN
					
				IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER    					    
				BEGIN
					IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
					Begin
						IF (@IsCrewGroup = 0) 
				        BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 
							AND CR.ClientID = @ClientID
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							ORDER BY CR.CrewCD
					    END
					    ELSE
					    BEGIN					        							
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 
							AND CR.ClientID = @ClientID
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD
						END						
					End
					Else
					Begin
						IF (@IsCrewGroup = 0) 
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND CR.ClientID = @ClientID
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							ORDER BY CR.CrewCD
						END
						ELSE
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND CR.ClientID = @ClientID
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD
						END
					End
				 
				END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
						IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
						Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
						End
						Else
						Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = CR.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND CR.ClientID = @ClientID
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
						End
				    
					END
		END
			
END    

ELSE -- NO CLIENT ID

BEGIN
  
		IF(@USERHOMEBASEID>0)    -- HOMEBASE ONLY FILTER SELECTED        
		BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER          
				BEGIN
					IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
				    Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
								ORDER BY CGO.OrderNum
							END
							
				     End
				     Else
				     Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
								ORDER BY CGO.OrderNum
							END
				     End				 
				END
				
				ELSE		  -- HOMEBASE NOT SELECTED IN FILTER    
				   		   
				BEGIN
					IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
				    Begin
						IF (@IsCrewGroup = 0) 
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD 
						END
						ELSE
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD 
							ORDER BY CGO.OrderNum
							
						END
				    End
				    Else
				    Begin
						IF (@IsCrewGroup = 0) 
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							ORDER BY CR.CrewCD 
						END
						ELSE
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD 							
							ORDER BY CGO.OrderNum
						END
				    End				    				    
				END
			   
		END
				
		ELSE  -- HOMEBASE ONLY FILTER NOT SELECTED    
		BEGIN
					
				IF(@IN_HOMEBASE_IDS != '0')       -- HOMEBASE IS SELECTED IN FILTER    
				BEGIN
						IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
						Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
								ORDER BY CGO.OrderNum
							END
						End
						Else
						Begin
							IF (@IsCrewGroup = 0) 
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
							END
							ELSE
							BEGIN
								SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
								FROM Crew CR 
								LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID AND Company.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND Company.HomebaseID = @USERHOMEBASEID
								LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
								WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
								AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
								AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
								AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
								--ORDER BY CR.CrewCD
								ORDER BY CGO.OrderNum 
							END
						End
				 
				END
				
				ELSE  -- HOMEBASE NOT SELECTED IN FILTER       	
				   
				BEGIN
					IF (@IsFixedCrewOnly = 0 and @IsRotaryWingCrewOnly = 0)
					Begin
						IF (@IsCrewGroup = 0) 
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD							
							--ORDER BY CGO.OrderNum
						END
						ELSE
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))							
							ORDER BY CGO.OrderNum 
						END
					End
					Else
					Begin
						IF (@IsCrewGroup = 0) 
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							--ORDER BY CR.CrewCD
						END
						ELSE
						BEGIN
							SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(CR.LastName,''))+ ', ' + RTRIM(isnull(CR.FirstName,'')) + ' ' + RTRIM(isnull(CR.MiddleInitial,'')) [Description], CR.HomebaseID
							FROM Crew CR 
							LEFT OUTER JOIN Company ON HomebaseAirportID = Company.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
							LEFT OUTER JOIN CrewGroupOrder CGO ON CR.CrewID = CGO.CrewID
							WHERE CR.CrewCD IS Not NULL AND  CR.CustomerID = @CustomerID
							AND (CR.IsNoCalendarDisplay is null or CR.IsNoCalendarDisplay = 0)
							AND CR.IsDeleted = 0 AND CR.IsFixedWing = @IsFixedCrewOnly  AND CR.IsRotaryWing = @IsRotaryWingCrewOnly
							AND  CR.CrewID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_CREW_IDS, ','))
							ORDER BY CGO.OrderNum 
						END
						
					End				    
				END
		END
	
END  
--Exec [GetCrewResourceType] 10001 , NULL,'1000182571,1000182563'

GO


