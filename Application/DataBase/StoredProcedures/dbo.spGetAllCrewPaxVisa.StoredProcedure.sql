/****** Object:  StoredProcedure [dbo].[spGetAllCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewPaxVisa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewPaxVisa]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllCrewPaxVisa]        
(@CrewID bigint,      
@PassengerRequestorID bigint  ,    
@CustomerID bigint    
)      
AS        
begin    
 SET NOCOUNT ON;        
SELECT CrewPassengerVisa.VisaID        
      ,CrewPassengerVisa.CrewID        
      ,CrewPassengerVisa.PassengerRequestorID        
      ,CrewPassengerVisa.CustomerID        
      ,CrewPassengerVisa.VisaTYPE        
      ,CrewPassengerVisa.VisaNum        
      ,CrewPassengerVisa.LastUpdUID        
      ,CrewPassengerVisa.LastUpdTS        
      ,CrewPassengerVisa.CountryID        
      ,CrewPassengerVisa.ExpiryDT        
      ,CrewPassengerVisa.Notes        
      ,CrewPassengerVisa.IssuePlace        
      ,CrewPassengerVisa.IssueDate        
      ,CrewPassengerVisa.IsDeleted    
      ,CrewPassengerVisa.IsInActive  
      ,CrewPassengerVisa.EntriesAllowedInPassport    
      ,CrewPassengerVisa.TypeOfVisa
      ,CrewPassengerVisa.VisaExpireInDays
      ,Country.CountryCD      
      ,Country.CountryName  
  FROM [CrewPassengerVisa]      
  LEFT OUTER JOIN Country ON Country.CountryID = CrewPassengerVisa.CountryID    
  WHERE ([CrewID]=@CrewID Or [PassengerRequestorID]=@PassengerRequestorID  )  
and CrewPassengerVisa.CustomerID=@CustomerID and CrewPassengerVisa.IsDeleted = 0    
end  
GO


