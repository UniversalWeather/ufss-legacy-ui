/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRFBOList]    Script Date: 02/11/2013 15:57:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRFBOList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRFBOList]
(@CustomerID BIGINT
,@CRLegID BIGINT
,@CRFBOListID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRFBOList.CRFBOListID
			,CRFBOList.CustomerID
			,CRFBOList.CRLegID
			,CRFBOList.TripNUM
			,CRFBOList.LegID
			,CRFBOList.RecordType
			,CRFBOList.FBOID
			,CRFBOList.CRFBOListDescription
			,CRFBOList.PhoneNUM
			,CRFBOList.IsCompleted
			,CRFBOList.AirportID
			,CRFBOList.LastUpdUID
			,CRFBOList.LastUpdTS
			,CRFBOList.IsDeleted
			,FBO.FBOCD
			,FBO.FBOVendor
			,FBO.PhoneNUM1
			,FBO.FaxNum
			,FBO.ContactEmail
	FROM CRFBOList
	LEFT OUTER JOIN FBO ON FBO.FBOID = CRFBOList.FBOID
	WHERE CRFBOList.CustomerID = @CustomerID
	AND CRFBOList.CRLegID = @CRLegID
	AND CRFBOList.IsDeleted = 0
	AND CRFBOList.CRFBOListID =	CASE WHEN ((@CRFBOListID IS NULL) OR (@CRFBOListID <> -1))
						THEN (@CRFBOListID)
						ELSE (SELECT CRFBOListID FROM CRFBOList 
								WHERE CustomerID = @CustomerID
										AND CRLegID = @CRLegID
										AND IsDeleted = 0)
						END
END
GO


