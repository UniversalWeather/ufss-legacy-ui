/****** Object:  StoredProcedure [dbo].[spAddFlightCategory]    Script Date: 01/11/2013 14:23:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFlightCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFlightCategory]
GO

/****** Object:  StoredProcedure [dbo].[spAddFlightCategory]    Script Date: 01/11/2013 14:23:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddFlightCategory]
 (       
       @FlightCatagoryCD char(4),
       @FlightCatagoryDescription varchar(25),
       @CustomerID bigint,
       @GraphColor varchar(11),
       @ClientID bigint,
       @LastUpdUID varchar(30),
       @LastUpdTS datetime,
       @ForeGrndCustomColor varchar(8),
       @BackgroundCustomColor varchar(8),
       @CallKey int,
       @IsInActive bit,
       @IsDeleted bit,
       @IsDeadorFerryHead bit
)
-- =============================================
-- Author:Sujitha.v
-- Create date: 16/4/2012
-- Description: Insert the Flight Category information
-- =============================================
AS
BEGIN 
SET NoCOUNT ON
DECLARE @FlightCategoryID BIGINT

EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @FlightCategoryID OUTPUT
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
   INSERT INTO FlightCatagory
	  (
	  FlightCategoryID,
	  FlightCatagoryCD ,
       FlightCatagoryDescription,
       CustomerID,
       GraphColor ,
       ClientID ,
       LastUpdUID ,
       LastUpdTS,
       ForeGrndCustomColor,
       BackgroundCustomColor,
       CallKey ,
       IsInActive ,
       IsDeleted,
       IsDeadorFerryHead )
      
      
     VALUES
           (
           @FlightCategoryID,
       @FlightCatagoryCD ,
       @FlightCatagoryDescription,
       @CustomerID,
       @GraphColor ,
       @ClientID ,
       @LastUpdUID ,
       @LastUpdTS,
       @ForeGrndCustomColor,
       @BackgroundCustomColor,
       @CallKey ,
       @IsInActive ,      
       isnull(@IsDeleted,0),
       @IsDeadorFerryHead   )

END

GO


