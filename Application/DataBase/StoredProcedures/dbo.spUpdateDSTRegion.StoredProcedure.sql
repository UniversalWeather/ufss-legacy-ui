SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateDSTRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateDSTRegion]
go

CREATE procedure [dbo].[spUpdateDSTRegion](@DSTRegionID BIGINT,   
                                           @DSTRegionCD VARCHAR(10),    
                                           @DSTRegionName varchar(40),    
                                           @StartDT datetime,    
                                           @EndDT datetime,    
                                           @OffSet NUMERIC(6,2),    
                                           @CityList Varchar(60),    
                                           @LastUpdUID VARCHAR(10),    
                                           @LastUpdTS datetime,    
                                           @IsDeleted bit)    
as    
begin     
SET NoCOUNT ON    
SET @LastUpdTS = GETUTCDATE()   
UPDATE [DSTRegion]      
   SET		DSTRegionCD=@DSTRegionCD    
           ,DSTRegionName=@DSTRegionName    
           ,StartDT=@StartDT               
           ,EndDT=@EndDT    
           ,OffSet=@OffSet    
           ,CityList=@CityList    
           ,LastUpdUID=@LastUpdUID              
           ,LastUpdTS=@LastUpdTS              
           ,IsDeleted=@IsDeleted    
 WHERE DSTRegionID=@DSTRegionID    

 UPDATE [Airport]
	SET
		DayLightSavingStartDT = CONVERT(Date,@StartDT),
		DayLightSavingEndDT = CONVERT(Date,@EndDT),
		DaylLightSavingStartTM = SUBSTRING (CONVERT(VARCHAR(30),@StartDT,113),13,5),
		DayLightSavingEndTM = SUBSTRING (CONVERT(VARCHAR(30),@EndDT,113),13,5),
		OffsetToGMT = @OffSet,
		LastUpdUID=@LastUpdUID,
        LastUpdTS=@LastUpdTS 
	WHERE DSTRegionID = @DSTRegionID		  
    
end  
