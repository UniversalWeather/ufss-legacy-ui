
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllVendor]    Script Date: 03/14/2014 22:00:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllVendor]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllVendor]    Script Date: 03/14/2014 22:00:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetAllVendor]
(
	@CustomerID BIGINT
	,@VendorType char(1)
	,@VendorID BIGINT    
	,@VendorCD varchar(5) 
	,@FetchHomebaseOnly CHAR(4)
	,@FetchActiveOnly BIT 
)        
AS        
-- =============================================                
-- Author: Sridhar               
-- Create date: 14/03/2014                
-- Description: Get All Vendor details for Popup          
-- Exec sp_fss_GetAllVendor 10099,'P',0,'','',0
-- Exec sp_fss_GetAllVendor 10099,'P',0,'','',1   
-- Exec sp_fss_GetAllVendor 10099,'P',0,'','kdal',0
-- Exec sp_fss_GetAllVendor 10099, 'P', 10099152826,'','',0
-- Exec sp_fss_GetAllVendor 10099, 'P', 0, 'P1000','',0    
-- =============================================         
SET NOCOUNT ON        
BEGIN        
	SELECT        
			V.VendorID      
			,V.CustomerID        
			,CASE WHEN V.VendorCD IS NULL THEN '' ELSE LTRIM(RTRIM(V.VendorCD)) END AS VendorCD
			,V.Name        
			,V.VendorType        
			,V.IsApplicationFiled        
			,V.IsApproved        
			,V.IsDrugTest        
			,V.IsInsuranceCERT        
			,V.IsFAR135Approved        
			,V.IsFAR135CERT        
			,V.AdditionalInsurance        
			,V.Contact        
			,V.VendorContactName        
			,V.BillingName        
			,V.BillingAddr1        
			,V.BillingAddr2        
			,V.BillingCity        
			,V.BillingState        
			,V.BillingZip        
			,V.CountryID         
			,V.MetroID         
			,V.BillingPhoneNum        
			,V.BillingFaxNum        
			,V.Notes        
			,V.Credit        
			,V.DiscountPercentage        
			,V.TaxID        
			,V.Terms        			      
			,V.HomebaseID         
			,V.LatitudeDegree        
			,V.LatitudeMinutes        
			,V.LatitudeNorthSouth        
			,V.LongitudeDegree        
			,V.LongitudeMinutes        
			,V.LongitudeEastWest        
			,V.AirportID         
			,V.LastUpdUID        
			,V.LastUpdTS        
			,V.IsTaxExempt        
			,V.DateAddedDT        
			,V.EmailID        
			,V.WebAddress        
			,ISNULL(V.IsDeleted,0) IsDeleted       
			,ISNULL(V.IsInactive,0) IsInactive                
			,V.TollFreePhone    
			,V.BusinessEmail    
			,V.Website    
			,V.BillingAddr3    
			,V.BusinessPhone    
			,V.CellPhoneNum    
			,V.HomeFax    
			,V.CellPhoneNum2    
			,V.OtherPhone    
			,V.MCBusinessEmail    
			,V.PersonalEmail    
			,V.OtherEmail   
			,V.WebsiteAddress
			,V.CertificateNumber
			,V.AuditingCompany 
			,V.SITA
			,N.CountryCD NationalityCD      
			,M.MetroCD MetroCD      
			,HB.IcaoID HomeBaseCD      
			,A.IcaoID ClosestICAO  
			,VC.FirstName FirstName  
			,VC.LastName LastName  
      
	FROM 
		Vendor V     
		LEFT OUTER JOIN Country N on N.CountryID = V.CountryID      
		LEFT OUTER JOIN Metro M on M.MetroID = V.MetroID      
		LEFT OUTER JOIN Airport HB on HB.AirportID = V.HomebaseID      
		LEFT OUTER JOIN Airport A on A.AirportID = V.AirportID       
		LEFT OUTER JOIN VendorContact VC on (VC.VendorID = V.VendorID and VC.IsChoice = 1)  
        
	WHERE 
		V.CustomerID=@CustomerID AND V.VendorType = @VendorType 
		 AND V.VendorID = case when @VendorID <>0 then @VendorID else V.VendorID end     
         AND V.VendorCD = case when Ltrim(Rtrim(@VendorCD)) <> '' then @VendorCD else V.VendorCD end  
         AND ISNULL(HB.ICAOID,'')    = CASE WHEN LTRIM(RTRIM(@FetchHomebaseOnly)) <> '' THEN @FetchHomebaseOnly ELSE ISNULL(HB.ICAOID,'') END                                     
         AND ISNULL(V.IsInActive,1) = case when @FetchActiveOnly =1 then ISNULL(V.IsInActive,1) else 1 end    
         AND ISNULL(V.IsDeleted,0) = 0                                     
	order by VendorCD      
END 


GO


