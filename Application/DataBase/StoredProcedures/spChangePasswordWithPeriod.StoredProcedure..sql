
/****** Object:  StoredProcedure [dbo].[spChangePasswordWithPeriod]    Script Date: 8/13/2015 8:31:31 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spChangePasswordWithPeriod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spChangePasswordWithPeriod];
GO

/****** Object:  StoredProcedure [dbo].[spChangePasswordWithPeriod]    Script Date: 8/13/2015 8:31:31 AM ******/

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


CREATE PROC [dbo].[spChangePasswordWithPeriod]  
 @UserName varchar(30),  
 @OldPassword nvarchar(500),  
 @NewPassword nvarchar(500),  
 @CustomerId bigint,  
 @ResetPeriod bigint,  
 @ReturnValue int output  
AS  
BEGIN  
  
SET @ReturnValue = (SELECT Count(UserMaster.UserName) FROM UserMaster   
     JOIN UserPassword ON UserMaster.UserName = UserPassword.UserName    
     where UPPER(UserMaster.UserName) = UPPER(@UserName) AND UserPassword.FPPWD = @OldPassword              
     AND UserPassword.ActiveStartDT <= GETUTCDATE() AND (UserPassword.ActiveEndDT IS NULL OR UserPassword.ActiveEndDT >= GETUTCDATE())  AND PWDStatus = 1)  
  
IF(@ReturnValue > 0)  
BEGIN  
 DECLARE  @UserPasswordID BIGINT    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @UserPasswordID OUTPUT    
 --Insert New password    
 INSERT INTO UserPassword 
 (UserPasswordID,UserName,FPPWD,ActiveStartDT,ActiveEndDT,PWDStatus,LastUpdUID,LastUpdTS,IsInActive)
 VALUES(@UserPasswordID, @UserName, @NewPassword, GETUTCDATE(), DATEADD(month, @ResetPeriod, GETUTCDATE()), 1, @UserName, GETUTCDATE(),0)    
 -- Set other passwords inactive for the user except current    
 UPDATE UserPassword SET ActiveEndDT = GETUTCDATE(), PWDStatus = 0, LastUpdUID = @UserName, LastUpdTS = GETUTCDATE() WHERE UserName = @UserName AND     
  UserPasswordID <> @UserPasswordID    
 -- Set ForceChangePassword flag to false
 UPDATE UserMaster SET ForceChangePassword = 0 Where UserName = @UserName
END

END;

GO


