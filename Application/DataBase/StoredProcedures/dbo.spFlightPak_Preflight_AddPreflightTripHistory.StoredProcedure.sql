IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightTripHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightTripHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightTripHistory](
	@TripID bigint
	,@CustomerID bigint
	,@HistoryDescription varchar(MAX)
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
	,@RevisionNumber int
)    
AS    
BEGIN   
	DECLARE @TripHistoryID  BIGINT   

	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @TripHistoryID OUTPUT     
	
	SET @RevisionNumber = ISNULL(@RevisionNumber,1)
	
	INSERT INTO PreflightTripHistory(
		[TripHistoryID]
		,[TripID]
		,[CustomerID]
		,[HistoryDescription]
		,[LastUpdUID]
		,[LastUpdTS]
		,[RevisionNumber]
		)    
	VALUES(@TripHistoryID
		,@TripID
		,@CustomerID
		,@HistoryDescription
		,@LastUpdUID
		,@LastUpdTS
		,@RevisionNumber
	)    

	SELECT @TripHistoryID as [TripHistoryID]
END
GO