

/****** Object:  StoredProcedure [dbo].[spflightpak_DeleteFleetNewCharterRate]    Script Date: 01/30/2013 11:53:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spflightpak_DeleteFleetNewCharterRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spflightpak_DeleteFleetNewCharterRate]
GO



/****** Object:  StoredProcedure [dbo].[spflightpak_DeleteFleetNewCharterRate]    Script Date: 01/30/2013 11:53:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spflightpak_DeleteFleetNewCharterRate]
(  
    @FleetNewCharterRateID bigint,  
    @LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)  
AS  
-- =============================================  
-- Author: Karthikeyan.S  
-- Create date: 30/01/2013  
-- Description: Delete Fleet New Charter Informations  
-- =============================================  
 SET NOCOUNT OFF; 
 DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)        
SET @ENTITYNAME = N'FleetNewCharterRate'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @FleetNewCharterRateID, @RecordUsed OUTPUT
--End of Delete Check

 if (@RecordUsed <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 else
   BEGIN    
 set @LastUpdTS = GETUTCDATE()
 UPDATE [FleetNewCharterRate]  
   SET   
      LastUpdUID = @LastUpdUID ,  
      LastUpdTS = @LastUpdTS ,      
      IsDeleted = @IsDeleted        
 WHERE [FleetNewCharterRateID]=@FleetNewCharterRateID
 end

GO


