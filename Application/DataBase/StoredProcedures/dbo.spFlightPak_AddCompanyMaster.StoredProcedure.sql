
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCompanyMaster]    Script Date: 02/18/2014 14:09:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddCompanyMaster]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddCompanyMaster]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCompanyMaster]    Script Date: 02/18/2014 14:09:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

      
CREATE procedure [dbo].[spFlightPak_AddCompanyMaster]                
 (                 
  --@HomebaseCD char(4)  ,              
  @CustomerID bigint                
 ,@BaseDescription varchar(30)                
 ,@ActiveDT date                
 ,@CompanyName varchar(40)                
 ,@IsDisplayNoteFlag bit                
 ,@RefreshTM numeric(4,0)                
 ,@IsReportCrewDuty bit                
 ,@LogFixed int                
 ,@LogRotary int                
 ,@IsZeroSuppressActivityAircftRpt bit                
 ,@IsZeroSuppressActivityCrewRpt bit                
 ,@IsZeroSuppressActivityDeptRpt bit                
 ,@IsZeroSuppressActivityPassengerRpt bit                
 ,@IsAutomaticDispatchNum bit                
 ,@IsAutomaticCommentTrips bit                
 ,@IsConflictCheck bit                
 ,@WindReliability int                
 ,@GroundTM numeric(5,2)                
 ,@IsTimeStampRpt bit                
 ,@ApplicationMessage varchar(max)                
 ,@CustomRptMessage varchar(80)                
 ,@RptTabOutbdInstLab1 varchar(10)                
 ,@RptTabOutbdInstLab2 varchar(10)                
 ,@RptTabOutbdInstLab3 varchar(10)                
 ,@RptTabOutbdInstLab4 varchar(10)                
 ,@RptTabOutbdInstLab5 varchar(10)                
 ,@RptTabOutbdInstLab6 varchar(10)                
 ,@RptTabOutbdInstLab7 varchar(10)                
 ,@ClientID bigint                
 ,@FedAviationRegNum char(3)                
 ,@DutyBasis numeric(1,0)                
 ,@AircraftBasis numeric(1,0)                
 ,@IsTripsheetView bit                
 ,@CrewDutyID bigint                
 ,@TripsheetDTWarning numeric(3,0)                
 ,@DefaultFlightCatID bigint                
 ,@DefaultCheckListGroupID bigint                
 ,@FuelPurchase numeric(1,0)                
 ,@FuelBurnKiloLBSGallon numeric(1,0)                
 ,@ApplicationDateFormat varchar(25)                
 ,@AccountFormat varchar(32)                
 ,@CorpAccountFormat varchar(64)                
 ,@FederalTax varchar(32)                
 ,@StateTax varchar(32)                
 ,@SaleTax varchar(32)                
 ,@IsTextAutoTab bit                
 ,@DepartmentID bigint                
 ,@FlightCatagoryPassenger char(4)                
 ,@FlightCatagoryPassengerNum char(4)                
 ,@CQFederalTax numeric(5,2)                
 ,@FederalACCT varchar(32)                
 ,@RuralTax numeric(5,2)                
 ,@RuralAccountNum varchar(32)                
 ,@IsAppliedTax numeric(1,0)                
 ,@MainFee char(4)                
 ,@ChtQouteDOMSegCHG numeric(17,2)                
 ,@ChtQouteDOMSegCHGACCT varchar(32)                
 ,@ChtQouteIntlSegCHG numeric(17,2)                
 ,@ChtQouteIntlSegCHGACCT varchar(32)                
 ,@ChtQouteHeaderDetailRpt varchar(max)                 
 ,@ChtQouteFooterDetailRpt varchar(max)                 
 ,@ChtQuoteCompany varchar(max)                 
 ,@ChtQuoteCompanyNameINV varchar(max)                 
 ,@ChtQuoteHeaderINV varchar(max)                 
 ,@ChtQuoteFooterINV varchar(max)                 
 ,@IsChecklistWarning bit                
 ,@CrewLogCustomLBLShort1 varchar(10)                
 ,@CrewLogCustomLBLShort2 varchar(10)                
 ,@CrewLogCustomLBLLong1 varchar(25)                
 ,@CrewLogCustomLBLLong2 varchar(25)                
 ,@IsLogsheetWarning bit                
 ,@CurrencySymbol char(3)                
 ,@RptSize numeric(1,0)                
 ,@AugmentCrewPercentage numeric(5,1)                
 ,@IsDeactivateHomeBaseFilter bit                
 ,@TripMGRDefaultStatus char(1)                
 ,@SearchGridTM numeric(4,2)                
 ,@LastUpdUID varchar(30)                
 ,@LastUpdTS datetime                
 ,@QuotePageOffsettingInvoice numeric(3,0)                
 ,@IsInvoiceDTsystemDT bit                
 ,@IsQuoteDetailPrint bit                
 ,@isQuotePrintFees bit                
 ,@IsQuotePrintSum bit                
 ,@IsQuotePrintSubtotal bit                
 ,@IsCityDescription bit           
 ,@IsAirportDescription bit                
 ,@IsQuoteICAODescription bit                
 ,@IsQuotePrepaidMinimUsageFeeAmt bit                
 ,@QuoteMinimumUseFeeDepositAmt varchar(60)                
 ,@IsQuoteFlightChargePrepaid bit                
 ,@DepostFlightCharge varchar(60)                
 ,@IsQuotePrepaidSegementFee bit                
 ,@QuoteSegmentFeeDeposit varchar(60)                
 ,@IsQuoteAdditionalCrewPrepaid bit                
 ,@QuoteAdditonCrewDeposit varchar(60)                
 ,@IsQuoteStdCrewRonPrepaid bit                
 ,@QuoteStdCrewRONDeposit varchar(60)         
 ,@IsQuoteAdditonalCrewRON bit                
 ,@QuoteAdditionCrewRON varchar(60)                
 ,@IsQuoteFlightLandingFeePrint bit                
 ,@QuoteLandingFeeDefault varchar(60)                
 ,@IsQuoteWaitingChargePrepaid bit                
 ,@QuoteWaitingCharge varchar(60)                
 ,@IsQuotePrintAdditionalFee bit                
 ,@QuoteAdditionalFeeDefault varchar(60)                
 ,@IsQuoteDispcountAmtPrint bit                
 ,@DiscountAmountDefault varchar(60)                
 ,@IsQuoteSubtotalPrint bit                
 ,@QuoteSubtotal varchar(60)                
 ,@IsQuoteTaxesPrint bit                
 ,@QuoteTaxesDefault varchar(60)                
 ,@IsQuoteInvoicePrint bit                
 ,@DefaultInvoice varchar(60)                
 ,@IsInvoiceRptLegNum bit                
 ,@IsInvoiceRptDetailDT bit                
 ,@IsInvoiceRptFromDescription bit                
 ,@IsQuoteDetailRptToDescription bit                
 ,@IsQuoteDetailInvoiceAmt bit                
 ,@IsQuoteDetailFlightHours bit                
 ,@IsQuoteDetailMiles bit                
 ,@IsQuoteDetailPassengerCnt bit                
 ,@IsQuoteFeelColumnDescription bit                
 ,@IsQuoteFeelColumnInvoiceAmt bit                
 ,@IsQuoteDispatch bit                
 ,@IsQuoteInformation bit                
 ,@IsCompanyName bit                
 ,@QuotePageOff numeric(3,0)                
 ,@IsChangeQueueDetail bit                
 ,@IsQuoteChangeQueueFees bit                
 ,@IsQuoteChangeQueueSum bit                
 ,@IsQuoteChangeQueueSubtotal bit                
 ,@IsChangeQueueCity bit                
 ,@IsChangeQueueAirport bit                
 ,@IsQuoteChangeQueueICAO bit                
 ,@IsQuotePrepaidMinimumUsage2FeeAdj bit                
 ,@QuoteMinimumUse2FeeDepositAdj varchar(60)                
 ,@IsQuotePrepaidMin2UsageFeeAdj bit                
 ,@IsQuoteFlight2ChargePrepaid bit                
 ,@Deposit2FlightCharge varchar(60)                
 ,@IsQuotePrepaidSegement2Fee bit                
 ,@QuoteSegment2FeeDeposit varchar(60)                
 ,@IsQuoteAdditional2CrewPrepaid bit                
 ,@QuoteAddition2CrewDeposit varchar(60)                
 ,@IsQuoteUnpaidAdditional2Crew bit                
 ,@IsQuoteStdCrew2RONPrepaid bit                
 ,@QouteStdCrew2RONDeposit varchar(60)                
 ,@IsQuoteUnpaidStd2CrewRON bit                
 ,@IsQuoteAdditional2CrewRON bit                
 ,@QuoteAddition2CrewRON varchar(60)                
 ,@IsQuoteUpaidAdditionl2CrewRON bit                
 ,@IsQuoteFlight2LandingFeePrint bit                
 ,@uote2LandingFeeDefault varchar(60)                
 ,@IsQuoteWaiting2ChargePrepaid bit                
 ,@QuoteWaiting2Charge varchar(60)                
 ,@IsQuoteUpaid2WaitingChg bit                
 ,@IsQuoteAdditional2FeePrint bit                
 ,@Additional2FeeDefault varchar(60)                
 ,@IsQuoteDiscount2AmountPrepaid bit                
 ,@Discount2AmountDeposit varchar(60)                
 ,@IsQuoteUnpaid2DiscountAmt bit                
 ,@IsQuoteSubtotal2Print bit                
 ,@Quote2Subtotal varchar(60)                
 ,@IsQuoteTaxes2Print bit                
 ,@Quote2TaxesDefault varchar(60)                
 ,@IsQuoteInvoice2Print bit                
 ,@QuoteInvoice2Default varchar(60)                
 ,@IsRptQuoteDetailLegNum bit                
 ,@IsRptQuoteDetailDepartureDT bit               
 ,@IsRptQuoteDetailFromDescription bit                
 ,@IsRptQuoteDetailToDescription bit                
 ,@IsRptQuoteDetailQuotedAmt bit                
 ,@IsRptQuoteDetailFlightHours bit                
 ,@IsRptQuoteDetailMiles bit                
 ,@IsRptQuoteDetailPassengerCnt bit                
 ,@IsRptQuoteFeeDescription bit                
 ,@IsRptQuoteFeeQuoteAmt bit                
 ,@IsQuoteInformation2 bit                
 ,@IsCompanyName2 bit                
 ,@QuoteDescription bit                
 ,@IsQuoteSales bit                
 ,@IsQuoteTailNum bit                
 ,@IsQuoteTypeCD bit                
 ,@QuoteTitle varchar(60)                
 ,@IsCorpReqActivateAlert bit                
 ,@CorpReqTMEntryFormat numeric(1,0)             
 ,@IsQuoteUnpaidFlight2Chg bit                
 ,@IsQuoteUnpaidFlightChg bit                
 ,@IsQuote2PrepayPrint bit                
 ,@QuotePrepay2Default varchar(60)                
 ,@IsQuotePrepayPrint bit                
 ,@QuotePrepayDefault varchar(60)                
 ,@IsQuoteRemaining2AmtPrint bit                
 ,@QuoteRemaining2AmountDefault varchar(60)                
 ,@IsQuoteRemainingAmtPrint bit               
 ,@QuoteRemainingAmountDefault varchar(60)                
 ,@IsQuoteDeactivateAutoUpdateRates bit                
 ,@CorpReqWeeklyCalTimer numeric(6,0)                
 ,@CorpReqExcChgWithinHours numeric(6,2)                
 ,@CorpReqExcChgWithinWeekend bit                
 ,@FleetCalendar5DayTimer numeric(6,0)                
 ,@Crew5DayTimer numeric(6,0)                
 ,@IsAutomaticCalcLegTimes bit                
 ,@IsCorpReqAllowLogUpdCateringInfo bit                
 ,@IsCorpReqAllowLogUpdCrewInfo bit                
 ,@IsCorpReqAllowLogUpdFBOInfo bit                
 ,@IsCorpReqAllowLogUpdPaxInfo bit                
 ,@CorpReqMinimumRunwayLength numeric(5,0)                
 ,@CorpReqStartTM char(5)                
 ,@CorpReqChgQueueTimer numeric(5,0)                
 ,@IsCorpReqActivateApproval bit                
 ,@IsCorpReqOverrideSubmittoChgQueue bit                
 ,@IsQuoteTaxDiscount bit                
 ,@ScheduleServiceTimeFormat numeric(1,0)                
 ,@SSHRFormat numeric(1,0)                
 ,@ScheduleServiceListCTDetail numeric(3,0)                
 ,@DayMonthFormat numeric(1,0)                
 ,@AcceptDTTM datetime                
 ,@HTMLHeader varchar(max)                 
 ,@HTMLFooter varchar(max)                 
 ,@HTMLBackgroun varchar(max)                 
 ,@ScheduledServicesSecurity numeric(1,0)                
 ,@ScheduleServiceWaitListID bigint                
 ,@IsTransWaitListPassengerfrmPreflightPostflight bit                
 ,@WebSecurityType numeric(1,0)                
 ,@RowsOnPageCnt numeric(3,0)                
 ,@WebPageBackgroundColor varchar(max)                 
 ,@HeardTextRpt varchar(max)                 
 ,@FooterTextRpt varchar(max)                 
 ,@HomebasePhoneNUM varchar(25)                
 ,@MenuBackground varchar(max)                 
 ,@MenuHeader varchar(max)                 
 ,@MenuFooter varchar(max)                 
 ,@SSBACKGRND varchar(max)                 
 ,@SSHEADER varchar(max)                 
 ,@SSFOOTER varchar(max)                 
 ,@WebBackground varchar(max)                 
 ,@WebHeader varchar(max)                 
 ,@WebFooter varchar(max)                 
 ,@QuoteIDTYPE numeric(1,0)                
 ,@IsQuoteEdit bit                
 ,@IsAutoRON bit                
 ,@IsAutoRollover bit                
 ,@IsQuoteWarningMessage bit                
 ,@SegmentFeeAlaska numeric(17,2)                
 ,@SegementFeeHawaii numeric(17,2)                
 ,@SeqmentAircraftAlaska varchar(32)                
 ,@SegementAircraftHawaii varchar(32)                
 ,@IsInActive bit                
 ,@TimeDisplayTenMin numeric(1,0)                
 ,@MinuteHoursRON numeric(2,0)                
 ,@IsChecklistAssign bit                
 ,@SpecificationShort3 varchar(10)                
 ,@SpecificationShort4 varchar(10)                
 ,@SpecificationLong3 varchar(25)                
 ,@SpecificationLong4 varchar(25)                
 ,@Specdec3 int                
 ,@Specdec4 int                
 ,@IsLogoUpload bit                
 ,@DayLanding numeric(3,0)                
 ,@DayLandingMinimum numeric(3,0)                
 ,@NightLanding numeric(3,0)                
 ,@NightllandingMinimum numeric(3,0)                
 ,@Approach numeric(3,0)                
 ,@ApproachMinimum numeric(3,0)                
 ,@Instrument numeric(3,0)                
 ,@InstrumentMinimum numeric(3,0)                
 ,@TakeoffDay numeric(3,0)                
 ,@TakeoffDayMinimum numeric(3,0)                
 ,@TakeoffNight numeric(3,0)                
 ,@TakeofNightMinimum numeric(3,0)                
 ,@Day7 numeric(3,0)                
 ,@DayMaximum7 numeric(8,3)                
 ,@Day30 numeric(3,0)                
 ,@DayMaximum30 numeric(8,3)                
 ,@CalendarMonthMaximum numeric(8,3)                
 ,@Day90 numeric(3,0)                
 ,@DayMaximum90 numeric(8,3)                
 ,@CalendarQTRMaximum numeric(8,3)                
 ,@Month6 numeric(3,0)                
 ,@MonthMaximum6 numeric(8,3)                
 ,@Previous2QTRMaximum numeric(8,3)                
 ,@Month12 numeric(3,0)                
 ,@MonthMaximum12 numeric(8,3)                
 ,@CalendarYearMaximum numeric(8,3)                
 ,@Day365 numeric(3,0)                
 ,@DayMaximum365 numeric(8,3)                
 ,@RestDays numeric(3,0)                
 ,@RestDaysMinimum numeric(2,0)                
 ,@RestCalendarQTRMinimum numeric(2,0)                
 ,@IsSLeg bit                
 ,@IsVLeg bit                
 ,@IsSConflict bit                
 ,@IsVConflict bit                
 ,@IsSChecklist bit                
 ,@IsVChecklist bit                
 ,@IsSCrewCurrency bit                
 ,@IsVCrewCurrency bit                
 ,@IsSPassenger bit                
 ,@IsVPassenger bit                
 ,@Tsense varchar(250)                
 ,@QuoteLogo varchar(max)                 
 ,@QuoteLogoPosition int                
 ,@Ilogo varchar(max)                 
 ,@IlogoPOS int                
 ,@IsBlockSeats bit                
 ,@FlightPurpose char(2)                
 ,@GroundTMIntl numeric(5,2)                
 ,@IsDepartAuthDuringReqSelection bit                
 ,@IsNonLogTripSheetIncludeCrewHIST bit                
 ,@TripsheetTabColor varchar(max)                 
 ,@TripsheetSearchBack int                
 ,@GeneralAviationDesk varchar(25)                
 ,@IsAutoValidateTripManager bit                
 ,@HoursMinutesCONV numeric(1,0)                
 ,@TripSheetLogoPosition varchar(max)                 
 ,@TripS int                
 ,@IsQuoteFirstPage bit                
 ,@IFirstPG bit                
 ,@FiscalYRStart numeric(2,0)                
 ,@FiscalYREnd numeric(2,0)                
 ,@LayoverHrsSIFL int                
 ,@IsAutoCreateCrewAvailInfo bit                
 ,@IsAutoPassenger bit                
 ,@PassengerOFullName int                
 ,@PassengerOMiddle int                
 ,@PassengerOLast int                
 ,@PassengerOLastFirst int                
 ,@PassengerOLastMiddle int                
 ,@PassengerOLastLast int                
 ,@IsAutoCrew bit                
 ,@CrewOFullName int                
 ,@CrewOMiddle int                
 ,@CrewOLast int                
 ,@CrewOLastFirst int                
 ,@CrewOLastMiddle int                
 ,@CrewOLastLast int                
 ,@TripsheetRptWriteGroup varchar(8)                
 ,@IsReqOnly bit                
 ,@BaseFax varchar(25)                
 ,@IsCanPass bit                
 ,@CanPassNum varchar(25)                
 ,@ElapseTMRounding numeric(1,0)                
 ,@IsAutoDispat bit                
 ,@PrivateTrip varchar(max)                 
 ,@PasswordValidDays numeric(2,0)                
 ,@PasswordHistoryNum numeric(2,0)                
 ,@IsAutoRevisionNum bit                
 ,@IsAutoPopulated bit                
 ,@TaxiTime char(5)                
 ,@AutoClimbTime char(5)                
,@CQMessageCD numeric(3,0)                
 ,@IVMessageCD numeric(3,0)                
 ,@StdCrewRON varchar(20)                
 ,@AdditionalCrewCD varchar(20)                
 ,@AdditionalCrewRON varchar(20)                
 ,@WaitTM varchar(20)                
 ,@LandingFee varchar(20)                
 ,@BackupDT date                
 ,@IsOutlook bit                
 ,@IsCharterQuoteFee3 bit                
 ,@IsCharterQuoteFee4 bit                
 ,@IsCharterQuote5 bit                
 ,@IsCOLFee3 bit                
 ,@IsCOLFee4 bit                
 ,@IsCOLFee5 bit                
 ,@IsANotes bit                
 ,@IsCNotes bit                
 ,@IsPNotes bit                
 ,@CQCrewDuty char(4)                
 ,@IsOpenHistory bit                
 ,@ReportWriteLogo varchar(max)                 
 ,@ReportWriteLogoPosition int                
 ,@IsEndDutyClient bit                
 ,@IsBlackberrySupport bit                
 ,@IsSSL bit                
 ,@ServerName varchar(100)                
 ,@ServerReport numeric(10,0)                
 ,@UserID char(30)                
 ,@UserPassword varchar(10)                
 ,@UserEmail varchar(100)                
 ,@ServerRPF numeric(1,0)                
 ,@IsAllBase bit                
 ,@IsSIFLCalMessage bit                
 ,@Searchack int                
 ,@SearchBack int                
 ,@IsCrewOlap bit                
 ,@ImageCnt numeric(1,0)                
 ,@ImagePosition numeric(1,0)                
 ,@IsMaxCrew bit                
 ,@IsTailInsuranceDue bit                
 ,@IsCrewQaulifiedFAR bit                
 ,@IsTailBase bit              
 ,@IsEnableTSA bit                
 ,@IsScheduleDTTM bit                
 ,@IsAPISSupport bit                
 ,@APISUrl varchar(250)                
 ,@LoginUrl varchar(250)                
 ,@IsPassengerOlap bit                
 ,@XMLPath varchar(100)                
 ,@IsAutoFillAF bit                
 ,@AircraftBlockFlight numeric(1,0)                
 ,@UserNameCBP nvarchar(55)                
 ,@UserPasswordCBP nvarchar(55)                
 ,@Domain varchar(50)                
 ,@IsAPISAlert bit                
 ,@IsSTMPBlock bit                
 ,@IsEnableTSAPX bit                
 ,@HomebaseAirportID bigint        
 ,@IsKilometer bit        
 ,@IsMiles bit        
 ,@CrewChecklistAlertDays NUMERIC(3)        
 ,@BusinessWeekStart NUMERIC(1)        
 ,@ExchangeRateID BIGINT        
 ,@InvoiceTitle varchar(60)      
 ,@IsRptQuoteDetailArrivalDate bit      
 ,@IsInvoiceRptArrivalDate bit      
 ,@IsRptQuoteDetailQuoteDate bit      
 ,@IsInvoiceRptQuoteDate bit       
 ,@CRSearchBack int      
 ,@IsTailNumber bit      
 ,@IsAircraftTypeCode bit      
 ,@IsPODepartPercentage bit  
 ,@IsPrintUWAReports bit
 ,@QuoteSalesLogoPosition int  
 ,@InvoiceSalesLogoPosition int
 ,@IsDefaultTripDepartureDate bit      
 )                
-- =============================================                
-- Author:Mullai.D                
-- Create date: 10/5/2012                
-- Description: Insert the Company information                
-- =============================================                
as                
BEGIN        
 SET NoCOUNT ON                
 DECLARE @AirportID BIGINT         
 DECLARE @HomebaseID BIGINT         
 IF NOT EXISTS(SELECT * FROM Company WHERE HomebaseAirportID = @HomebaseAirportID AND CustomerID = @CustomerID AND IsDeleted = 1)        
 BEGIN         
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @HomebaseID OUTPUT                
  SET @LastUpdTS = GETUTCDATE()                
  INSERT INTO [Company]                
      ([HomebaseID]                
      --,[HomebaseCD]                
      ,[CustomerID]                
      ,[BaseDescription]                
      ,[ActiveDT]                
      ,[CompanyName]                
      ,[IsDisplayNoteFlag]                
      ,[RefreshTM]                
      ,[IsReportCrewDuty]                
      ,[LogFixed]                
      ,[LogRotary]                
      ,[IsZeroSuppressActivityAircftRpt]                
      ,[IsZeroSuppressActivityCrewRpt]                
      ,[IsZeroSuppressActivityDeptRpt]                
      ,[IsZeroSuppressActivityPassengerRpt]                
      ,[IsAutomaticDispatchNum]                
      ,[IsAutomaticCommentTrips]                
      ,[IsConflictCheck]                
      ,[WindReliability]                
      ,[GroundTM]                
      ,[IsTimeStampRpt]                
      ,[ApplicationMessage]                
      ,[CustomRptMessage]                
      ,[RptTabOutbdInstLab1]               
      ,[RptTabOutbdInstLab2]                
      ,[RptTabOutbdInstLab3]                
      ,[RptTabOutbdInstLab4]                
      ,[RptTabOutbdInstLab5]                
      ,[RptTabOutbdInstLab6]                
      ,[RptTabOutbdInstLab7]                
      ,[ClientID]                
      ,[FedAviationRegNum]                
      ,[DutyBasis]                
      ,[AircraftBasis]                
      ,[IsTripsheetView]                
      ,[CrewDutyID]                
      ,[TripsheetDTWarning]                
  ,[DefaultFlightCatID]                
      ,[DefaultCheckListGroupID]                
      ,[FuelPurchase]                
      ,[FuelBurnKiloLBSGallon]                
      ,[ApplicationDateFormat]                
      ,[AccountFormat]                
      ,[CorpAccountFormat]                
      ,[FederalTax]                
      ,[StateTax]                
      ,[SaleTax]                
      ,[IsTextAutoTab]                
      ,[DepartmentID]                
      ,[FlightCatagoryPassenger]                
      ,[FlightCatagoryPassengerNum]                
      ,[CQFederalTax]                
      ,[FederalACCT]                
      ,[RuralTax]                
      ,[RuralAccountNum]                
      ,[IsAppliedTax]                
      ,[MainFee]                
      ,[ChtQouteDOMSegCHG]                
      ,[ChtQouteDOMSegCHGACCT]                
      ,[ChtQouteIntlSegCHG]                
      ,[ChtQouteIntlSegCHGACCT]                
      ,[ChtQouteHeaderDetailRpt]                
      ,[ChtQouteFooterDetailRpt]                
      ,[ChtQuoteCompany]                
      ,[ChtQuoteCompanyNameINV]                
      ,[ChtQuoteHeaderINV]                
      ,[ChtQuoteFooterINV]                
      ,[IsChecklistWarning]                
      ,[CrewLogCustomLBLShort1]                
      ,[CrewLogCustomLBLShort2]                
      ,[CrewLogCustomLBLLong1]                
      ,[CrewLogCustomLBLLong2]                
      ,[IsLogsheetWarning]                
      ,[CurrencySymbol]                
      ,[RptSize]                
      ,[AugmentCrewPercentage]                
      ,[IsDeactivateHomeBaseFilter]                
      ,[TripMGRDefaultStatus]                
      ,[SearchGridTM]                
      ,[LastUpdUID]                
      ,[LastUpdTS]                
      ,[QuotePageOffsettingInvoice]                
      ,[IsInvoiceDTsystemDT]                
      ,[IsQuoteDetailPrint]                
      ,[isQuotePrintFees]                
      ,[IsQuotePrintSum]                
      ,[IsQuotePrintSubtotal]                
      ,[IsCityDescription]                
      ,[IsAirportDescription]                
      ,[IsQuoteICAODescription]                
      ,[IsQuotePrepaidMinimUsageFeeAmt]                
      ,[QuoteMinimumUseFeeDepositAmt]                
      ,[IsQuoteFlightChargePrepaid]                
      ,[DepostFlightCharge]                
      ,[IsQuotePrepaidSegementFee]                
      ,[QuoteSegmentFeeDeposit]                
      ,[IsQuoteAdditionalCrewPrepaid]                
      ,[QuoteAdditonCrewDeposit]                
      ,[IsQuoteStdCrewRonPrepaid]                
      ,[QuoteStdCrewRONDeposit]                
      ,[IsQuoteAdditonalCrewRON]                
      ,[QuoteAdditionCrewRON]                
      ,[IsQuoteFlightLandingFeePrint]                
      ,[QuoteLandingFeeDefault]                
      ,[IsQuoteWaitingChargePrepaid]                
      ,[QuoteWaitingCharge]                
      ,[IsQuotePrintAdditionalFee]                
      ,[QuoteAdditionalFeeDefault]                
      ,[IsQuoteDispcountAmtPrint]                
      ,[DiscountAmountDefault]                
      ,[IsQuoteSubtotalPrint]                
      ,[QuoteSubtotal]                
      ,[IsQuoteTaxesPrint]                
      ,[QuoteTaxesDefault]                
      ,[IsQuoteInvoicePrint]                
      ,[DefaultInvoice]                
      ,[IsInvoiceRptLegNum]                
      ,[IsInvoiceRptDetailDT]                
      ,[IsInvoiceRptFromDescription]                
      ,[IsQuoteDetailRptToDescription]                
      ,[IsQuoteDetailInvoiceAmt]                
      ,[IsQuoteDetailFlightHours]                
      ,[IsQuoteDetailMiles]                
      ,[IsQuoteDetailPassengerCnt]                
      ,[IsQuoteFeelColumnDescription]                
      ,[IsQuoteFeelColumnInvoiceAmt]                
      ,[IsQuoteDispatch]                
      ,[IsQuoteInformation]                
      ,[IsCompanyName]                
      ,[QuotePageOff]                
      ,[IsChangeQueueDetail]                
      ,[IsQuoteChangeQueueFees]                
 ,[IsQuoteChangeQueueSum]                
      ,[IsQuoteChangeQueueSubtotal]                
      ,[IsChangeQueueCity]                
      ,[IsChangeQueueAirport]                
      ,[IsQuoteChangeQueueICAO]                
      ,[IsQuotePrepaidMinimumUsage2FeeAdj]                
      ,[QuoteMinimumUse2FeeDepositAdj]                
      ,[IsQuotePrepaidMin2UsageFeeAdj]                
      ,[IsQuoteFlight2ChargePrepaid]                
      ,[Deposit2FlightCharge]                
      ,[IsQuotePrepaidSegement2Fee]                
      ,[QuoteSegment2FeeDeposit]                
      ,[IsQuoteAdditional2CrewPrepaid]                
      ,[QuoteAddition2CrewDeposit]                
      ,[IsQuoteUnpaidAdditional2Crew]                
      ,[IsQuoteStdCrew2RONPrepaid]                
      ,[QouteStdCrew2RONDeposit]                
      ,[IsQuoteUnpaidStd2CrewRON]                
      ,[IsQuoteAdditional2CrewRON]                
      ,[QuoteAddition2CrewRON]                
      ,[IsQuoteUpaidAdditionl2CrewRON]                
      ,[IsQuoteFlight2LandingFeePrint]                
      ,[uote2LandingFeeDefault]                
      ,[IsQuoteWaiting2ChargePrepaid]                
      ,[QuoteWaiting2Charge]                
 ,[IsQuoteUpaid2WaitingChg]                
      ,[IsQuoteAdditional2FeePrint]                
      ,[Additional2FeeDefault]                
      ,[IsQuoteDiscount2AmountPrepaid]                
      ,[Discount2AmountDeposit]                
      ,[IsQuoteUnpaid2DiscountAmt]                
      ,[IsQuoteSubtotal2Print]                
      ,[Quote2Subtotal]                
      ,[IsQuoteTaxes2Print]                
      ,[Quote2TaxesDefault]                
      ,[IsQuoteInvoice2Print]                
      ,[QuoteInvoice2Default]                
      ,[IsRptQuoteDetailLegNum]                
      ,[IsRptQuoteDetailDepartureDT]                
      ,[IsRptQuoteDetailFromDescription]                
      ,[IsRptQuoteDetailToDescription]                
      ,[IsRptQuoteDetailQuotedAmt]                
      ,[IsRptQuoteDetailFlightHours]                
      ,[IsRptQuoteDetailMiles]                
      ,[IsRptQuoteDetailPassengerCnt]                
      ,[IsRptQuoteFeeDescription]                
      ,[IsRptQuoteFeeQuoteAmt]                
      ,[IsQuoteInformation2]                
      ,[IsCompanyName2]                
      ,[QuoteDescription]                
      ,[IsQuoteSales]                
      ,[IsQuoteTailNum]                
      ,[IsQuoteTypeCD]                
      ,[QuoteTitle]                
      ,[IsCorpReqActivateAlert]                
      ,[CorpReqTMEntryFormat]                
      ,[IsQuoteUnpaidFlight2Chg]                
      ,[IsQuoteUnpaidFlightChg]                
      ,[IsQuote2PrepayPrint]                
      ,[QuotePrepay2Default]              
      ,[IsQuotePrepayPrint]                
      ,[QuotePrepayDefault]                
      ,[IsQuoteRemaining2AmtPrint]                
      ,[QuoteRemaining2AmountDefault]                
      ,[IsQuoteRemainingAmtPrint]                
      ,[QuoteRemainingAmountDefault]                
      ,[IsQuoteDeactivateAutoUpdateRates]                
      ,[CorpReqWeeklyCalTimer]                
      ,[CorpReqExcChgWithinHours]                
      ,[CorpReqExcChgWithinWeekend]                
      ,[FleetCalendar5DayTimer]                
      ,[Crew5DayTimer]                
      ,[IsAutomaticCalcLegTimes]                
      ,[IsCorpReqAllowLogUpdCateringInfo]                
      ,[IsCorpReqAllowLogUpdCrewInfo]                
      ,[IsCorpReqAllowLogUpdFBOInfo]                
      ,[IsCorpReqAllowLogUpdPaxInfo]                
      ,[CorpReqMinimumRunwayLength]                
      ,[CorpReqStartTM]                
      ,[CorpReqChgQueueTimer]                
      ,[IsCorpReqActivateApproval]                
      ,[IsCorpReqOverrideSubmittoChgQueue]                
      ,[IsQuoteTaxDiscount]                
      ,[ScheduleServiceTimeFormat]                
      ,[SSHRFormat]                
      ,[ScheduleServiceListCTDetail]                
      ,[DayMonthFormat]                
      ,[AcceptDTTM]         
      ,[HTMLHeader]                
      ,[HTMLFooter]                
      ,[HTMLBackgroun]                
      ,[ScheduledServicesSecurity]                
      ,[ScheduleServiceWaitListID]                
      ,[IsTransWaitListPassengerfrmPreflightPostflight]                
      ,[WebSecurityType]                
      ,[RowsOnPageCnt]                
      ,[WebPageBackgroundColor]                
      ,[HeardTextRpt]                
      ,[FooterTextRpt]                
      ,[HomebasePhoneNUM]                
      ,[MenuBackground]                
      ,[MenuHeader]                
      ,[MenuFooter]                
      ,[SSBACKGRND]                
      ,[SSHEADER]                
      ,[SSFOOTER]                
      ,[WebBackground]                
      ,[WebHeader]                
      ,[WebFooter]                
      ,[QuoteIDTYPE]                
      ,[IsQuoteEdit]                
      ,[IsAutoRON]                
      ,[IsAutoRollover]                
      ,[IsQuoteWarningMessage]                
      ,[SegmentFeeAlaska]                
      ,[SegementFeeHawaii]                
      ,[SeqmentAircraftAlaska]                
      ,[SegementAircraftHawaii]                
      ,[IsInActive]                
      ,[TimeDisplayTenMin]                
      ,[MinuteHoursRON]                
      ,[IsChecklistAssign]                
      ,[SpecificationShort3]                
      ,[SpecificationShort4]                
 ,[SpecificationLong3]                
      ,[SpecificationLong4]                
      ,[Specdec3]                
      ,[Specdec4]                
      ,[IsLogoUpload]                
      ,[DayLanding]                
      ,[DayLandingMinimum]                
      ,[NightLanding]                
      ,[NightllandingMinimum]                
      ,[Approach]                
      ,[ApproachMinimum]                
      ,[Instrument]                
      ,[InstrumentMinimum]                
      ,[TakeoffDay]                
      ,[TakeoffDayMinimum]                
      ,[TakeoffNight]                
      ,[TakeofNightMinimum]                
      ,[Day7]                
      ,[DayMaximum7]                
      ,[Day30]                
      ,[DayMaximum30]                
      ,[CalendarMonthMaximum]                
      ,[Day90]                
      ,[DayMaximum90]                
      ,[CalendarQTRMaximum]                
      ,[Month6]                
      ,[MonthMaximum6]                
      ,[Previous2QTRMaximum]                
      ,[Month12]                
      ,[MonthMaximum12]                
      ,[CalendarYearMaximum]                
      ,[Day365]                
      ,[DayMaximum365]                
      ,[RestDays]                
      ,[RestDaysMinimum]                
      ,[RestCalendarQTRMinimum]                
      ,[IsSLeg]                
      ,[IsVLeg]                
      ,[IsSConflict]                
      ,[IsVConflict]                
      ,[IsSChecklist]                
      ,[IsVChecklist]                
      ,[IsSCrewCurrency]                
      ,[IsVCrewCurrency]                
      ,[IsSPassenger]                
      ,[IsVPassenger]                
      ,[Tsense]                
      ,[QuoteLogo]                
      ,[QuoteLogoPosition]                
      ,[Ilogo]                
      ,[IlogoPOS]                
      ,[IsBlockSeats]                
      ,[FlightPurpose]                
      ,[GroundTMIntl]                
      ,[IsDepartAuthDuringReqSelection]                
      ,[IsNonLogTripSheetIncludeCrewHIST]                
      ,[TripsheetTabColor]                
      ,[TripsheetSearchBack]                
      ,[GeneralAviationDesk]                
      ,[IsAutoValidateTripManager]                
      ,[HoursMinutesCONV]                
      ,[TripSheetLogoPosition]                
      ,[TripS]                
      ,[IsQuoteFirstPage]                
      ,[IFirstPG]                
      ,[FiscalYRStart]                
      ,[FiscalYREnd]                
      ,[LayoverHrsSIFL]                
      ,[IsAutoCreateCrewAvailInfo]                
      ,[IsAutoPassenger]                
      ,[PassengerOFullName]                
      ,[PassengerOMiddle]          
      ,[PassengerOLast]                
      ,[PassengerOLastFirst]                
      ,[PassengerOLastMiddle]                
      ,[PassengerOLastLast]                
      ,[IsAutoCrew]                
      ,[CrewOFullName]                
      ,[CrewOMiddle]                
      ,[CrewOLast]                
      ,[CrewOLastFirst]                
      ,[CrewOLastMiddle]                
      ,[CrewOLastLast]                
      ,[TripsheetRptWriteGroup]                
      ,[IsReqOnly]                
      ,[BaseFax]                
      ,[IsCanPass]                
      ,[CanPassNum]                
      ,[ElapseTMRounding]                
      ,[IsAutoDispat]                
      ,[PrivateTrip]                
      ,[PasswordValidDays]                
      ,[PasswordHistoryNum]                
      ,[IsAutoRevisionNum]                
      ,[IsAutoPopulated]                
      ,[TaxiTime]                
      ,[AutoClimbTime]                
      ,[CQMessageCD]                
      ,[IVMessageCD]                
      ,[StdCrewRON]                
      ,[AdditionalCrewCD]                
      ,[AdditionalCrewRON]                
      ,[WaitTM]                
      ,[LandingFee]                
      ,[BackupDT]                
      ,[IsOutlook]                
      ,[IsCharterQuoteFee3]                
      ,[IsCharterQuoteFee4]                
      ,[IsCharterQuote5]                
      ,[IsCOLFee3]                
      ,[IsCOLFee4]                
      ,[IsCOLFee5]                
      ,[IsANotes]                
      ,[IsCNotes]                
      ,[IsPNotes]                
      ,[CQCrewDuty]                
      ,[IsOpenHistory]                
      ,[ReportWriteLogo]                
      ,[ReportWriteLogoPosition]                
      ,[IsEndDutyClient]                
      ,[IsBlackberrySupport]                
      ,[IsSSL]                
      ,[ServerName]                
      ,[ServerReport]                
      ,[UserID]                
      ,[UserPassword]                
      ,[UserEmail]                
      ,[ServerRPF]                
      ,[IsAllBase]                
      ,[IsSIFLCalMessage]                
      ,[Searchack]                
      ,[SearchBack]                
      ,[IsCrewOlap]                
      ,[ImageCnt]                
      ,[ImagePosition]                
      ,[IsMaxCrew]                
      ,[IsTailInsuranceDue]                
      ,[IsCrewQaulifiedFAR]                
      ,[IsTailBase]                
      ,[IsEnableTSA]                
      ,[IsScheduleDTTM]                
      ,[IsAPISSupport]                
      ,[APISUrl]                
      ,[LoginUrl]                
      ,[IsPassengerOlap]                
      ,[XMLPath]                
      ,[IsAutoFillAF]                
      ,[AircraftBlockFlight]                
      ,[UserNameCBP]                
      ,[UserPasswordCBP]                
      ,[Domain]                
      ,[IsAPISAlert]                
      ,[IsSTMPBlock]                
      ,[IsEnableTSAPX]            
      ,[HomebaseAirportID]        
      ,[IsKilometer]        
      ,[IsMiles]         
      ,CrewChecklistAlertDays        
      ,BusinessWeekStart        
      ,ExchangeRateID       
      ,InvoiceTitle      
   ,IsRptQuoteDetailArrivalDate      
   ,IsInvoiceRptArrivalDate      
   ,IsRptQuoteDetailQuoteDate      
   ,IsInvoiceRptQuoteDate      
   ,CRSearchBack      
   ,IsTailNumber      
   ,IsAircraftTypeCode       
   ,IsPODepartPercentage  
   ,IsPrintUWAReports
   ,QuoteSalesLogoPosition
   ,InvoiceSalesLogoPosition
   ,[IsDefaultTripDepartureDate]
      )                
   VALUES                
    (@HomebaseID                
    --,@HomebaseCD                
    ,@CustomerID                
    ,@BaseDescription                
    ,@ActiveDT                
    ,@CompanyName                
    ,@IsDisplayNoteFlag                
    ,@RefreshTM                
    ,@IsReportCrewDuty                
    ,@LogFixed                
    ,@LogRotary                
    ,@IsZeroSuppressActivityAircftRpt                
    ,@IsZeroSuppressActivityCrewRpt                
    ,@IsZeroSuppressActivityDeptRpt                
    ,@IsZeroSuppressActivityPassengerRpt                
    ,@IsAutomaticDispatchNum                
    ,@IsAutomaticCommentTrips                
    ,@IsConflictCheck                
    ,@WindReliability                
    ,@GroundTM                
    ,@IsTimeStampRpt                
    ,@ApplicationMessage                
    ,@CustomRptMessage                
    ,@RptTabOutbdInstLab1                
    ,@RptTabOutbdInstLab2                
    ,@RptTabOutbdInstLab3                
    ,@RptTabOutbdInstLab4                
    ,@RptTabOutbdInstLab5                
    ,@RptTabOutbdInstLab6                
    ,@RptTabOutbdInstLab7                
    ,@ClientID                
    ,@FedAviationRegNum                
    ,@DutyBasis                
    ,@AircraftBasis                
    ,@IsTripsheetView                
    ,@CrewDutyID                
    ,@TripsheetDTWarning                
    ,@DefaultFlightCatID                
    ,@DefaultCheckListGroupID                
    ,@FuelPurchase                
    ,@FuelBurnKiloLBSGallon                
    ,@ApplicationDateFormat                
    ,@AccountFormat                
    ,@CorpAccountFormat                
    ,@FederalTax                
    ,@StateTax                
    ,@SaleTax                
    ,@IsTextAutoTab                
    ,@DepartmentID                
    ,@FlightCatagoryPassenger                
    ,@FlightCatagoryPassengerNum                
    ,@CQFederalTax                
    ,@FederalACCT                
    ,@RuralTax                
    ,@RuralAccountNum                
    ,@IsAppliedTax                
    ,@MainFee                
    ,@ChtQouteDOMSegCHG                
    ,@ChtQouteDOMSegCHGACCT                
    ,@ChtQouteIntlSegCHG                
    ,@ChtQouteIntlSegCHGACCT                
    ,@ChtQouteHeaderDetailRpt                
    ,@ChtQouteFooterDetailRpt                
    ,@ChtQuoteCompany                
    ,@ChtQuoteCompanyNameINV                
    ,@ChtQuoteHeaderINV                
    ,@ChtQuoteFooterINV                
    ,@IsChecklistWarning                
    ,@CrewLogCustomLBLShort1                
    ,@CrewLogCustomLBLShort2                
    ,@CrewLogCustomLBLLong1                
    ,@CrewLogCustomLBLLong2                
    ,@IsLogsheetWarning                
    ,@CurrencySymbol                
    ,@RptSize                
  ,@AugmentCrewPercentage                
    ,@IsDeactivateHomeBaseFilter                
    ,@TripMGRDefaultStatus                
    ,@SearchGridTM                
    ,@LastUpdUID                
    ,@LastUpdTS                
    ,@QuotePageOffsettingInvoice                
    ,@IsInvoiceDTsystemDT                
    ,@IsQuoteDetailPrint                
    ,@isQuotePrintFees                
    ,@IsQuotePrintSum                
    ,@IsQuotePrintSubtotal                
    ,@IsCityDescription                
    ,@IsAirportDescription                
    ,@IsQuoteICAODescription                
    ,@IsQuotePrepaidMinimUsageFeeAmt                
    ,@QuoteMinimumUseFeeDepositAmt                
    ,@IsQuoteFlightChargePrepaid                
    ,@DepostFlightCharge                
    ,@IsQuotePrepaidSegementFee                
    ,@QuoteSegmentFeeDeposit                
    ,@IsQuoteAdditionalCrewPrepaid                
    ,@QuoteAdditonCrewDeposit                
    ,@IsQuoteStdCrewRonPrepaid                
    ,@QuoteStdCrewRONDeposit                
    ,@IsQuoteAdditonalCrewRON                
    ,@QuoteAdditionCrewRON                
    ,@IsQuoteFlightLandingFeePrint                
    ,@QuoteLandingFeeDefault                
    ,@IsQuoteWaitingChargePrepaid                
    ,@QuoteWaitingCharge                
    ,@IsQuotePrintAdditionalFee                
    ,@QuoteAdditionalFeeDefault                
    ,@IsQuoteDispcountAmtPrint                
    ,@DiscountAmountDefault                
    ,@IsQuoteSubtotalPrint                
    ,@QuoteSubtotal                
    ,@IsQuoteTaxesPrint                
    ,@QuoteTaxesDefault                
    ,@IsQuoteInvoicePrint                
    ,@DefaultInvoice                
    ,@IsInvoiceRptLegNum                
    ,@IsInvoiceRptDetailDT                
    ,@IsInvoiceRptFromDescription                
    ,@IsQuoteDetailRptToDescription                
    ,@IsQuoteDetailInvoiceAmt                
    ,@IsQuoteDetailFlightHours              
    ,@IsQuoteDetailMiles                
    ,@IsQuoteDetailPassengerCnt                
    ,@IsQuoteFeelColumnDescription                
    ,@IsQuoteFeelColumnInvoiceAmt                
    ,@IsQuoteDispatch                
    ,@IsQuoteInformation                
    ,@IsCompanyName                
    ,@QuotePageOff                
    ,@IsChangeQueueDetail                
    ,@IsQuoteChangeQueueFees                
    ,@IsQuoteChangeQueueSum                
    ,@IsQuoteChangeQueueSubtotal                
    ,@IsChangeQueueCity                
    ,@IsChangeQueueAirport                
    ,@IsQuoteChangeQueueICAO                
    ,@IsQuotePrepaidMinimumUsage2FeeAdj                
    ,@QuoteMinimumUse2FeeDepositAdj                
    ,@IsQuotePrepaidMin2UsageFeeAdj                
    ,@IsQuoteFlight2ChargePrepaid                
    ,@Deposit2FlightCharge                
    ,@IsQuotePrepaidSegement2Fee                
    ,@QuoteSegment2FeeDeposit                
    ,@IsQuoteAdditional2CrewPrepaid                
    ,@QuoteAddition2CrewDeposit                
    ,@IsQuoteUnpaidAdditional2Crew                
    ,@IsQuoteStdCrew2RONPrepaid                
    ,@QouteStdCrew2RONDeposit                
    ,@IsQuoteUnpaidStd2CrewRON                
    ,@IsQuoteAdditional2CrewRON                
    ,@QuoteAddition2CrewRON                
    ,@IsQuoteUpaidAdditionl2CrewRON                
    ,@IsQuoteFlight2LandingFeePrint                
    ,@uote2LandingFeeDefault                
    ,@IsQuoteWaiting2ChargePrepaid                
    ,@QuoteWaiting2Charge                
    ,@IsQuoteUpaid2WaitingChg                
    ,@IsQuoteAdditional2FeePrint                
    ,@Additional2FeeDefault                
    ,@IsQuoteDiscount2AmountPrepaid                
    ,@Discount2AmountDeposit                
    ,@IsQuoteUnpaid2DiscountAmt                
    ,@IsQuoteSubtotal2Print                
    ,@Quote2Subtotal                
    ,@IsQuoteTaxes2Print      
    ,@Quote2TaxesDefault                
    ,@IsQuoteInvoice2Print                
    ,@QuoteInvoice2Default                
    ,@IsRptQuoteDetailLegNum       
    ,@IsRptQuoteDetailDepartureDT                
    ,@IsRptQuoteDetailFromDescription                
    ,@IsRptQuoteDetailToDescription                
    ,@IsRptQuoteDetailQuotedAmt                
    ,@IsRptQuoteDetailFlightHours                
    ,@IsRptQuoteDetailMiles                
    ,@IsRptQuoteDetailPassengerCnt                
    ,@IsRptQuoteFeeDescription                
    ,@IsRptQuoteFeeQuoteAmt                
    ,@IsQuoteInformation2                
    ,@IsCompanyName2                
    ,@QuoteDescription                
    ,@IsQuoteSales                
    ,@IsQuoteTailNum                
    ,@IsQuoteTypeCD                
    ,@QuoteTitle                
    ,@IsCorpReqActivateAlert                
    ,@CorpReqTMEntryFormat                
    ,@IsQuoteUnpaidFlight2Chg                
    ,@IsQuoteUnpaidFlightChg                
    ,@IsQuote2PrepayPrint                
    ,@QuotePrepay2Default                
    ,@IsQuotePrepayPrint                
    ,@QuotePrepayDefault                
    ,@IsQuoteRemaining2AmtPrint                
    ,@QuoteRemaining2AmountDefault                
    ,@IsQuoteRemainingAmtPrint                
    ,@QuoteRemainingAmountDefault                
    ,@IsQuoteDeactivateAutoUpdateRates                
    ,@CorpReqWeeklyCalTimer                
    ,@CorpReqExcChgWithinHours                
    ,@CorpReqExcChgWithinWeekend                
    ,@FleetCalendar5DayTimer                
    ,@Crew5DayTimer                
    ,@IsAutomaticCalcLegTimes                
    ,@IsCorpReqAllowLogUpdCateringInfo                
    ,@IsCorpReqAllowLogUpdCrewInfo                
    ,@IsCorpReqAllowLogUpdFBOInfo                
    ,@IsCorpReqAllowLogUpdPaxInfo                
    ,@CorpReqMinimumRunwayLength                
    ,@CorpReqStartTM                
    ,@CorpReqChgQueueTimer                
    ,@IsCorpReqActivateApproval                
    ,@IsCorpReqOverrideSubmittoChgQueue                
    ,@IsQuoteTaxDiscount                
    ,@ScheduleServiceTimeFormat             
    ,@SSHRFormat                
    ,@ScheduleServiceListCTDetail                
    ,@DayMonthFormat                
    ,@AcceptDTTM                
    ,@HTMLHeader                
    ,@HTMLFooter                
    ,@HTMLBackgroun                
    ,@ScheduledServicesSecurity                
    ,@ScheduleServiceWaitListID                
    ,@IsTransWaitListPassengerfrmPreflightPostflight                
    ,@WebSecurityType                
    ,@RowsOnPageCnt                
    ,@WebPageBackgroundColor                
    ,@HeardTextRpt                
    ,@FooterTextRpt                
    ,@HomebasePhoneNUM                
    ,@MenuBackground                
    ,@MenuHeader                
    ,@MenuFooter                
    ,@SSBACKGRND                
    ,@SSHEADER                
    ,@SSFOOTER                
    ,@WebBackground                
    ,@WebHeader                
    ,@WebFooter                
    ,@QuoteIDTYPE                
    ,@IsQuoteEdit                
    ,@IsAutoRON                
    ,@IsAutoRollover                
    ,@IsQuoteWarningMessage                
    ,@SegmentFeeAlaska                
    ,@SegementFeeHawaii                
    ,@SeqmentAircraftAlaska                
    ,@SegementAircraftHawaii                
    ,@IsInActive                
    ,@TimeDisplayTenMin                
    ,@MinuteHoursRON                
    ,@IsChecklistAssign                
    ,@SpecificationShort3                
    ,@SpecificationShort4                
    ,@SpecificationLong3                
    ,@SpecificationLong4                
    ,@Specdec3                
    ,@Specdec4                
    ,@IsLogoUpload                
    ,@DayLanding                
    ,@DayLandingMinimum                
    ,@NightLanding                
    ,@NightllandingMinimum                
    ,@Approach                
    ,@ApproachMinimum                
    ,@Instrument                
    ,@InstrumentMinimum                
    ,@TakeoffDay                
    ,@TakeoffDayMinimum                
    ,@TakeoffNight                
    ,@TakeofNightMinimum                
    ,@Day7                
    ,@DayMaximum7                
    ,@Day30                
    ,@DayMaximum30                
    ,@CalendarMonthMaximum                
    ,@Day90                
    ,@DayMaximum90                
    ,@CalendarQTRMaximum                
    ,@Month6                
    ,@MonthMaximum6                
    ,@Previous2QTRMaximum                
    ,@Month12                
    ,@MonthMaximum12                
    ,@CalendarYearMaximum                
    ,@Day365                
    ,@DayMaximum365                
    ,@RestDays                
    ,@RestDaysMinimum                
    ,@RestCalendarQTRMinimum                
    ,@IsSLeg                
    ,@IsVLeg                
    ,@IsSConflict                
    ,@IsVConflict                
    ,@IsSChecklist                
    ,@IsVChecklist                
    ,@IsSCrewCurrency                
    ,@IsVCrewCurrency                
    ,@IsSPassenger                
    ,@IsVPassenger                
    ,@Tsense                
    ,@QuoteLogo                
    ,@QuoteLogoPosition                
    ,@Ilogo                
    ,@IlogoPOS                
    ,@IsBlockSeats                
    ,@FlightPurpose                
    ,@GroundTMIntl                
    ,@IsDepartAuthDuringReqSelection                
    ,@IsNonLogTripSheetIncludeCrewHIST                
    ,@TripsheetTabColor                
    ,@TripsheetSearchBack                
    ,@GeneralAviationDesk                
    ,@IsAutoValidateTripManager                
    ,@HoursMinutesCONV                
    ,@TripSheetLogoPosition                
    ,@TripS                
    ,@IsQuoteFirstPage                
    ,@IFirstPG                
    ,@FiscalYRStart                
    ,@FiscalYREnd                
    ,@LayoverHrsSIFL                
    ,@IsAutoCreateCrewAvailInfo                
    ,@IsAutoPassenger                
    ,@PassengerOFullName                
    ,@PassengerOMiddle                
    ,@PassengerOLast                
    ,@PassengerOLastFirst                
    ,@PassengerOLastMiddle                
    ,@PassengerOLastLast                
    ,@IsAutoCrew                
    ,@CrewOFullName                
    ,@CrewOMiddle                
    ,@CrewOLast                
    ,@CrewOLastFirst                
    ,@CrewOLastMiddle                
    ,@CrewOLastLast                
    ,@TripsheetRptWriteGroup                
    ,@IsReqOnly                
    ,@BaseFax                
    ,@IsCanPass                
    ,@CanPassNum                
    ,@ElapseTMRounding                
    ,@IsAutoDispat                
    ,@PrivateTrip                
    ,@PasswordValidDays                
    ,@PasswordHistoryNum                
    ,@IsAutoRevisionNum                
    ,@IsAutoPopulated                
    ,@TaxiTime                
    ,@AutoClimbTime                
    ,@CQMessageCD                
    ,@IVMessageCD                
    ,@StdCrewRON                
    ,@AdditionalCrewCD                
    ,@AdditionalCrewRON                
    ,@WaitTM                
    ,@LandingFee                
    ,@BackupDT                
    ,@IsOutlook                
    ,@IsCharterQuoteFee3                
    ,@IsCharterQuoteFee4                
    ,@IsCharterQuote5                
    ,@IsCOLFee3                
    ,@IsCOLFee4                
    ,@IsCOLFee5                
    ,@IsANotes                
    ,@IsCNotes                
    ,@IsPNotes                
    ,@CQCrewDuty                
    ,@IsOpenHistory                
    ,@ReportWriteLogo                
    ,@ReportWriteLogoPosition                
    ,@IsEndDutyClient               
    ,@IsBlackberrySupport                
    ,@IsSSL                
    ,@ServerName                
    ,@ServerReport                
    ,@UserID                
    ,@UserPassword                
    ,@UserEmail                
    ,@ServerRPF                
    ,@IsAllBase                
    ,@IsSIFLCalMessage                
    ,@Searchack                
    ,@SearchBack                
    ,@IsCrewOlap                
    ,@ImageCnt                
    ,@ImagePosition                
    ,@IsMaxCrew                
    ,@IsTailInsuranceDue                
    ,@IsCrewQaulifiedFAR                
    ,@IsTailBase                
    ,@IsEnableTSA                
    ,@IsScheduleDTTM                
    ,@IsAPISSupport                
    ,@APISUrl                
    ,@LoginUrl                
    ,@IsPassengerOlap                
    ,@XMLPath                
    ,@IsAutoFillAF                
    ,@AircraftBlockFlight                
    ,@UserNameCBP                
    ,@UserPasswordCBP                
    ,@Domain                
    ,@IsAPISAlert                
    ,@IsSTMPBlock                
    ,@IsEnableTSAPX            
    ,@HomebaseAirportID        
    ,@IsKilometer        
    ,@IsMiles        
  ,@CrewChecklistAlertDays        
    ,@BusinessWeekStart        
    ,@ExchangeRateID       
    ,@InvoiceTitle      
 ,@IsRptQuoteDetailArrivalDate      
 ,@IsInvoiceRptArrivalDate      
 ,@IsRptQuoteDetailQuoteDate      
 ,@IsInvoiceRptQuoteDate        
 ,@CRSearchBack      
 ,@IsTailNumber      
 ,@IsAircraftTypeCode      
 ,@IsPODepartPercentage     
 ,@IsPrintUWAReports
 ,@QuoteSalesLogoPosition
 ,@InvoiceSalesLogoPosition
 ,@IsDefaultTripDepartureDate   
    )                
                 
  update Company set UserNameCBP=@UserNameCBP, UserPasswordCBP=@UserPasswordCBP where CustomerID=@CustomerID                
 END        
 ELSE        
 BEGIN          
  SELECT @HomebaseID = HomebaseID FROM Company WHERE HomebaseAirportID = @HomebaseAirportID AND CustomerID = @CustomerID AND IsDeleted = 1        
          
  EXEC spFlightPak_UpdateCompanyMaster         
  @HomebaseID        
  --,@HomebaseCD        
  ,@CustomerID        
  ,@BaseDescription        
  ,@ActiveDT        
  ,@CompanyName        
  ,@IsDisplayNoteFlag        
  ,@RefreshTM        
  ,@IsReportCrewDuty        
  ,@LogFixed        
  ,@LogRotary        
  ,@IsZeroSuppressActivityAircftRpt        
  ,@IsZeroSuppressActivityCrewRpt        
  ,@IsZeroSuppressActivityDeptRpt        
  ,@IsZeroSuppressActivityPassengerRpt        
  ,@IsAutomaticDispatchNum        
  ,@IsAutomaticCommentTrips        
  ,@IsConflictCheck        
  ,@WindReliability        
  ,@GroundTM        
  ,@IsTimeStampRpt        
  ,@ApplicationMessage        
  ,@CustomRptMessage        
  ,@RptTabOutbdInstLab1        
  ,@RptTabOutbdInstLab2        
  ,@RptTabOutbdInstLab3        
  ,@RptTabOutbdInstLab4        
  ,@RptTabOutbdInstLab5        
  ,@RptTabOutbdInstLab6        
  ,@RptTabOutbdInstLab7        
  ,@ClientID        
  ,@FedAviationRegNum        
  ,@DutyBasis        
  ,@AircraftBasis        
  ,@IsTripsheetView        
  ,@CrewDutyID        
  ,@TripsheetDTWarning        
  ,@DefaultFlightCatID        
  ,@DefaultCheckListGroupID        
  ,@FuelPurchase        
  ,@FuelBurnKiloLBSGallon        
  ,@ApplicationDateFormat        
  ,@AccountFormat        
  ,@CorpAccountFormat        
  ,@FederalTax        
  ,@StateTax        
  ,@SaleTax        
  ,@IsTextAutoTab        
  ,@DepartmentID        
  ,@FlightCatagoryPassenger        
  ,@FlightCatagoryPassengerNum        
  ,@CQFederalTax        
  ,@FederalACCT        
  ,@RuralTax        
  ,@RuralAccountNum        
  ,@IsAppliedTax        
  ,@MainFee        
  ,@ChtQouteDOMSegCHG        
  ,@ChtQouteDOMSegCHGACCT        
  ,@ChtQouteIntlSegCHG        
  ,@ChtQouteIntlSegCHGACCT        
  ,@ChtQouteHeaderDetailRpt        
  ,@ChtQouteFooterDetailRpt        
  ,@ChtQuoteCompany        
  ,@ChtQuoteCompanyNameINV        
  ,@ChtQuoteHeaderINV        
  ,@ChtQuoteFooterINV        
  ,@IsChecklistWarning        
  ,@CrewLogCustomLBLShort1        
  ,@CrewLogCustomLBLShort2        
  ,@CrewLogCustomLBLLong1        
  ,@CrewLogCustomLBLLong2        
  ,@IsLogsheetWarning        
  ,@CurrencySymbol        
  ,@RptSize        
  ,@AugmentCrewPercentage        
  ,@IsDeactivateHomeBaseFilter        
  ,@TripMGRDefaultStatus        
  ,@SearchGridTM        
  ,@LastUpdUID        
  ,@LastUpdTS        
  ,@QuotePageOffsettingInvoice        
  ,@IsInvoiceDTsystemDT        
  ,@IsQuoteDetailPrint        
  ,@isQuotePrintFees        
  ,@IsQuotePrintSum        
  ,@IsQuotePrintSubtotal        
  ,@IsCityDescription        
  ,@IsAirportDescription        
  ,@IsQuoteICAODescription        
  ,@IsQuotePrepaidMinimUsageFeeAmt        
  ,@QuoteMinimumUseFeeDepositAmt        
  ,@IsQuoteFlightChargePrepaid        
  ,@DepostFlightCharge        
  ,@IsQuotePrepaidSegementFee        
  ,@QuoteSegmentFeeDeposit        
  ,@IsQuoteAdditionalCrewPrepaid        
  ,@QuoteAdditonCrewDeposit        
  ,@IsQuoteStdCrewRonPrepaid        
  ,@QuoteStdCrewRONDeposit        
  ,@IsQuoteAdditonalCrewRON        
  ,@QuoteAdditionCrewRON        
  ,@IsQuoteFlightLandingFeePrint        
  ,@QuoteLandingFeeDefault        
  ,@IsQuoteWaitingChargePrepaid        
  ,@QuoteWaitingCharge        
  ,@IsQuotePrintAdditionalFee        
  ,@QuoteAdditionalFeeDefault          
  ,@IsQuoteDispcountAmtPrint        
  ,@DiscountAmountDefault        
  ,@IsQuoteSubtotalPrint        
  ,@QuoteSubtotal        
  ,@IsQuoteTaxesPrint        
  ,@QuoteTaxesDefault        
  ,@IsQuoteInvoicePrint        
  ,@DefaultInvoice        
  ,@IsInvoiceRptLegNum        
  ,@IsInvoiceRptDetailDT        
  ,@IsInvoiceRptFromDescription        
  ,@IsQuoteDetailRptToDescription        
  ,@IsQuoteDetailInvoiceAmt        
  ,@IsQuoteDetailFlightHours        
  ,@IsQuoteDetailMiles        
  ,@IsQuoteDetailPassengerCnt        
  ,@IsQuoteFeelColumnDescription        
  ,@IsQuoteFeelColumnInvoiceAmt        
  ,@IsQuoteDispatch        
  ,@IsQuoteInformation        
  ,@IsCompanyName        
  ,@QuotePageOff        
  ,@IsChangeQueueDetail        
  ,@IsQuoteChangeQueueFees        
  ,@IsQuoteChangeQueueSum        
  ,@IsQuoteChangeQueueSubtotal        
  ,@IsChangeQueueCity        
  ,@IsChangeQueueAirport        
  ,@IsQuoteChangeQueueICAO        
  ,@IsQuotePrepaidMinimumUsage2FeeAdj        
  ,@QuoteMinimumUse2FeeDepositAdj        
  ,@IsQuotePrepaidMin2UsageFeeAdj        
  ,@IsQuoteFlight2ChargePrepaid        
  ,@Deposit2FlightCharge        
  ,@IsQuotePrepaidSegement2Fee        
  ,@QuoteSegment2FeeDeposit        
  ,@IsQuoteAdditional2CrewPrepaid        
  ,@QuoteAddition2CrewDeposit        
  ,@IsQuoteUnpaidAdditional2Crew        
  ,@IsQuoteStdCrew2RONPrepaid        
  ,@QouteStdCrew2RONDeposit        
  ,@IsQuoteUnpaidStd2CrewRON        
  ,@IsQuoteAdditional2CrewRON        
  ,@QuoteAddition2CrewRON        
  ,@IsQuoteUpaidAdditionl2CrewRON        
  ,@IsQuoteFlight2LandingFeePrint        
  ,@uote2LandingFeeDefault        
  ,@IsQuoteWaiting2ChargePrepaid        
  ,@QuoteWaiting2Charge        
  ,@IsQuoteUpaid2WaitingChg        
  ,@IsQuoteAdditional2FeePrint        
  ,@Additional2FeeDefault        
  ,@IsQuoteDiscount2AmountPrepaid        
,@Discount2AmountDeposit        
  ,@IsQuoteUnpaid2DiscountAmt        
  ,@IsQuoteSubtotal2Print        
  ,@Quote2Subtotal        
  ,@IsQuoteTaxes2Print        
  ,@Quote2TaxesDefault        
  ,@IsQuoteInvoice2Print        
  ,@QuoteInvoice2Default        
  ,@IsRptQuoteDetailLegNum        
  ,@IsRptQuoteDetailDepartureDT        
  ,@IsRptQuoteDetailFromDescription        
  ,@IsRptQuoteDetailToDescription        
  ,@IsRptQuoteDetailQuotedAmt        
  ,@IsRptQuoteDetailFlightHours        
  ,@IsRptQuoteDetailMiles        
  ,@IsRptQuoteDetailPassengerCnt        
  ,@IsRptQuoteFeeDescription        
  ,@IsRptQuoteFeeQuoteAmt        
  ,@IsQuoteInformation2        
  ,@IsCompanyName2        
  ,@QuoteDescription        
  ,@IsQuoteSales        
  ,@IsQuoteTailNum        
  ,@IsQuoteTypeCD        
  ,@QuoteTitle        
  ,@IsCorpReqActivateAlert        
  ,@CorpReqTMEntryFormat        
  ,@IsQuoteUnpaidFlight2Chg        
  ,@IsQuoteUnpaidFlightChg        
  ,@IsQuote2PrepayPrint        
  ,@QuotePrepay2Default        
  ,@IsQuotePrepayPrint        
  ,@QuotePrepayDefault        
  ,@IsQuoteRemaining2AmtPrint        
  ,@QuoteRemaining2AmountDefault        
  ,@IsQuoteRemainingAmtPrint        
  ,@QuoteRemainingAmountDefault        
  ,@IsQuoteDeactivateAutoUpdateRates        
  ,@CorpReqWeeklyCalTimer        
  ,@CorpReqExcChgWithinHours        
  ,@CorpReqExcChgWithinWeekend        
  ,@FleetCalendar5DayTimer        
  ,@Crew5DayTimer        
  ,@IsAutomaticCalcLegTimes        
  ,@IsCorpReqAllowLogUpdCateringInfo        
  ,@IsCorpReqAllowLogUpdCrewInfo        
  ,@IsCorpReqAllowLogUpdFBOInfo        
  ,@IsCorpReqAllowLogUpdPaxInfo        
  ,@CorpReqMinimumRunwayLength        
  ,@CorpReqStartTM        
  ,@CorpReqChgQueueTimer        
  ,@IsCorpReqActivateApproval        
  ,@IsCorpReqOverrideSubmittoChgQueue        
  ,@IsQuoteTaxDiscount        
  ,@ScheduleServiceTimeFormat        
  ,@SSHRFormat        
  ,@ScheduleServiceListCTDetail        
  ,@DayMonthFormat        
  ,@AcceptDTTM        
  ,@HTMLHeader        
  ,@HTMLFooter        
  ,@HTMLBackgroun        
  ,@ScheduledServicesSecurity        
  ,@ScheduleServiceWaitListID        
  ,@IsTransWaitListPassengerfrmPreflightPostflight        
  ,@WebSecurityType        
  ,@RowsOnPageCnt        
  ,@WebPageBackgroundColor        
  ,@HeardTextRpt        
  ,@FooterTextRpt        
  ,@HomebasePhoneNUM        
  ,@MenuBackground        
  ,@MenuHeader        
  ,@MenuFooter        
  ,@SSBACKGRND        
  ,@SSHEADER        
  ,@SSFOOTER        
  ,@WebBackground        
  ,@WebHeader        
  ,@WebFooter        
  ,@QuoteIDTYPE        
  ,@IsQuoteEdit        
  ,@IsAutoRON        
  ,@IsAutoRollover        
  ,@IsQuoteWarningMessage        
  ,@SegmentFeeAlaska        
  ,@SegementFeeHawaii        
  ,@SeqmentAircraftAlaska        
  ,@SegementAircraftHawaii        
  ,@IsInActive        
  ,@TimeDisplayTenMin        
  ,@MinuteHoursRON        
  ,@IsChecklistAssign        
  ,@SpecificationShort3        
  ,@SpecificationShort4        
  ,@SpecificationLong3        
  ,@SpecificationLong4        
  ,@Specdec3        
  ,@Specdec4        
  ,@IsLogoUpload        
  ,@DayLanding        
  ,@DayLandingMinimum        
  ,@NightLanding        
  ,@NightllandingMinimum        
  ,@Approach        
  ,@ApproachMinimum        
  ,@Instrument        
  ,@InstrumentMinimum        
  ,@TakeoffDay        
  ,@TakeoffDayMinimum        
  ,@TakeoffNight        
  ,@TakeofNightMinimum        
  ,@Day7        
  ,@DayMaximum7        
  ,@Day30        
  ,@DayMaximum30        
  ,@CalendarMonthMaximum        
  ,@Day90        
  ,@DayMaximum90        
  ,@CalendarQTRMaximum        
  ,@Month6        
  ,@MonthMaximum6        
  ,@Previous2QTRMaximum        
  ,@Month12        
  ,@MonthMaximum12        
  ,@CalendarYearMaximum        
  ,@Day365        
  ,@DayMaximum365        
  ,@RestDays        
  ,@RestDaysMinimum        
  ,@RestCalendarQTRMinimum        
  ,@IsSLeg        
  ,@IsVLeg        
  ,@IsSConflict        
  ,@IsVConflict        
  ,@IsSChecklist        
  ,@IsVChecklist        
  ,@IsSCrewCurrency        
  ,@IsVCrewCurrency        
  ,@IsSPassenger        
  ,@IsVPassenger        
  ,@Tsense        
  ,@QuoteLogo        
  ,@QuoteLogoPosition        
  ,@Ilogo        
  ,@IlogoPOS        
  ,@IsBlockSeats        
  ,@FlightPurpose        
  ,@GroundTMIntl        
  ,@IsDepartAuthDuringReqSelection      
  ,@IsNonLogTripSheetIncludeCrewHIST        
  ,@TripsheetTabColor        
  ,@TripsheetSearchBack        
  ,@GeneralAviationDesk        
  ,@IsAutoValidateTripManager        
  ,@HoursMinutesCONV        
  ,@TripSheetLogoPosition        
  ,@TripS        
  ,@IsQuoteFirstPage        
  ,@IFirstPG        
  ,@FiscalYRStart        
  ,@FiscalYREnd        
  ,@LayoverHrsSIFL        
  ,@IsAutoCreateCrewAvailInfo        
  ,@IsAutoPassenger        
  ,@PassengerOFullName        
  ,@PassengerOMiddle        
  ,@PassengerOLast        
  ,@PassengerOLastFirst        
  ,@PassengerOLastMiddle        
  ,@PassengerOLastLast        
  ,@IsAutoCrew        
  ,@CrewOFullName        
  ,@CrewOMiddle        
  ,@CrewOLast        
  ,@CrewOLastFirst     
  ,@CrewOLastMiddle        
  ,@CrewOLastLast        
  ,@TripsheetRptWriteGroup        
  ,@IsReqOnly        
  ,@BaseFax        
  ,@IsCanPass        
  ,@CanPassNum        
  ,@ElapseTMRounding        
  ,@IsAutoDispat        
  ,@PrivateTrip        
  ,@PasswordValidDays        
  ,@PasswordHistoryNum        
  ,@IsAutoRevisionNum        
  ,@IsAutoPopulated        
  ,@TaxiTime        
  ,@AutoClimbTime        
  ,@CQMessageCD        
  ,@IVMessageCD        
  ,@StdCrewRON        
  ,@AdditionalCrewCD        
  ,@AdditionalCrewRON        
  ,@WaitTM        
  ,@LandingFee        
  ,@BackupDT        
  ,@IsOutlook        
  ,@IsCharterQuoteFee3        
  ,@IsCharterQuoteFee4        
  ,@IsCharterQuote5        
  ,@IsCOLFee3        
  ,@IsCOLFee4        
  ,@IsCOLFee5        
  ,@IsANotes        
  ,@IsCNotes        
  ,@IsPNotes        
  ,@CQCrewDuty        
  ,@IsOpenHistory        
  ,@ReportWriteLogo        
  ,@ReportWriteLogoPosition        
  ,@IsEndDutyClient        
  ,@IsBlackberrySupport        
  ,@IsSSL        
  ,@ServerName        
  ,@ServerReport        
  ,@UserID        
  ,@UserPassword        
  ,@UserEmail        
  ,@ServerRPF        
  ,@IsAllBase        
  ,@IsSIFLCalMessage        
  ,@Searchack        
  ,@SearchBack        
  ,@IsCrewOlap        
  ,@ImageCnt        
  ,@ImagePosition        
  ,@IsMaxCrew        
  ,@IsTailInsuranceDue        
  ,@IsCrewQaulifiedFAR        
  ,@IsTailBase        
  ,@IsEnableTSA        
  ,@IsScheduleDTTM        
  ,@IsAPISSupport        
  ,@APISUrl        
  ,@LoginUrl        
  ,@IsPassengerOlap        
  ,@XMLPath        
  ,@IsAutoFillAF        
  ,@AircraftBlockFlight        
  ,@UserNameCBP        
  ,@UserPasswordCBP        
  ,@Domain        
  ,@IsAPISAlert        
  ,@IsSTMPBlock        
  ,@IsEnableTSAPX        
  ,@HomebaseAirportID        
  ,@IsKilometer        
  ,@IsMiles        
  ,@CrewChecklistAlertDays        
  ,@BusinessWeekStart        
  ,@ExchangeRateID       
  ,@InvoiceTitle      
  ,@IsRptQuoteDetailArrivalDate      
  ,@IsInvoiceRptArrivalDate      
  ,@IsRptQuoteDetailQuoteDate      
  ,@IsInvoiceRptQuoteDate         
  ,@CRSearchBack         
  ,@IsTailNumber      
  ,@IsAircraftTypeCode      
  ,@IsPODepartPercentage  
  ,@IsPrintUWAReports
  ,@QuoteSalesLogoPosition
  ,@InvoiceSalesLogoPosition        
  ,@IsDefaultTripDepartureDate
        
  UPDATE  Company SET IsDeleted = 0 WHERE HomebaseAirportID = @HomebaseAirportID AND CustomerID = @CustomerID AND IsDeleted = 1        
 END            
      
  UPDATE  Company SET IsBlackberrySupport = @IsBlackberrySupport       
 WHERE CustomerID = @CustomerID AND isnull(IsDeleted,0) = 0   

--Ram Mahajan- 30Jan2015 Update Crew Log Custom Label custom description for CustomPilotLog
IF LEN(ISNULL(@CrewLogCustomLBLLong1,''))!=0
BEGIN
UPDATE TSFlightLog SET CustomDescription = @CrewLogCustomLBLLong1  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('PILOT LOG','PILOTLOG','FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 1','Crew Log Custom Label1','CrewLog Custom label 1')
END
IF LEN(ISNULL(@CrewLogCustomLBLLong2,''))!=0
BEGIN  	
UPDATE TSFlightLog SET CustomDescription = @CrewLogCustomLBLLong2  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('PILOT LOG','PILOTLOG','FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 2','Crew Log Custom Label2','CrewLog Custom label 2')
	END
IF LEN(ISNULL(@SpecificationLong3,''))!=0
BEGIN  
UPDATE TSFlightLog SET CustomDescription = @SpecificationLong3  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('PILOT LOG','PILOTLOG','FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 3','Crew Log Custom Label3','CrewLog Custom label 3')
END
IF LEN(ISNULL(@SpecificationLong4,''))!=0
BEGIN  
UPDATE TSFlightLog SET CustomDescription = @SpecificationLong4  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('PILOT LOG','PILOTLOG','FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 4','Crew Log Custom Label4','CrewLog Custom label 4')  
END
--Ram Mahajan- 30Jan2015 Update Crew Log Custom Label custom description for CustomPilotLog
        
 select @HomebaseID AS HomebaseID                
END 
GO


