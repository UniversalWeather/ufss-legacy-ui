

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightFBO]    Script Date: 09/24/2013 00:02:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightFBO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightFBO]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightFBO]    Script Date: 09/24/2013 00:02:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightFBO]    
 (      
         @PreflightFBOID bigint    
           ,@PreflightFBOName varchar(60)    
           ,@LegID bigint    
           ,@FBOID bigint    
           ,@IsArrivalFBO bit    
           ,@IsDepartureFBO bit    
           ,@AirportID bigint    
           ,@Street varchar(50)    
           ,@CityName varchar(20)    
           ,@StateName varchar(20)    
           ,@PostalZipCD char(20)    
           ,@PhoneNum1 varchar(25)    
           ,@PhoneNum2 varchar(25)    
           ,@PhoneNum3 varchar(25)    
           ,@PhoneNum4 varchar(25)    
           ,@FaxNUM varchar(25)    
           ,@LastUpdUID char(30)    
           ,@LastUpdTS datetime    
           ,@IsDeleted bit     
           ,@NoUpdated bit    
           ,@FBOArrangedBy bit    
           ,@IsCompleted bit    
           ,@FBOInformation varchar(100)    
           ,@ConfirmationStatus varchar(max)    
           ,@Comments varchar(max)    
           ,@CustomerID bigint    
           ,@Status varchar(20)     
    )    
AS    
BEGIN     
SET NOCOUNT OFF    
    
UPDATE [dbo].[PreflightFBOList]    
   SET [PreflightFBOName] = @PreflightFBOName     
      ,[Street] = @Street    
      ,[CityName] = @CityName    
      ,[StateName] = @StateName    
      ,[PostalZipCD] = @PostalZipCD    
      ,[PhoneNum1] = @PhoneNum1    
      ,[PhoneNum2] = @PhoneNum2    
      ,[PhoneNum3] = @PhoneNum3    
      ,[PhoneNum4] = @PhoneNum4    
      ,[FaxNUM] = @FaxNUM    
      ,[LastUpdUID] = @LastUpdUID    
      ,[LastUpdTS] = @LastUpdTS    
      ,[IsDeleted] = @IsDeleted    
      ,[NoUpdated]=@NoUpdated    
   ,[FBOArrangedBy]=@FBOArrangedBy    
      ,[IsCompleted]=@IsCompleted    
      ,[FBOInformation]=@FBOInformation    
   ,[ConfirmationStatus] = @ConfirmationStatus    
   ,[Comments] = @Comments     
   ,[AirportID] = @AirportID    
   ,[FBOID] = @FBOID    
   ,[Status]= @Status    
       
 WHERE  PreflightFBOID=@PreflightFBOID AND LegID=@LegID     
      
END    
    
    



GO


