
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetClientByIDOrCD]    Script Date: 01/24/2014 11:29:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetClientByIDOrCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetClientByIDOrCD]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetClientByIDOrCD]    Script Date: 01/24/2014 11:29:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[spFlightPak_Preflight_GetClientByIDOrCD](@CustomerID BIGINT,
@ClientCD varchar(8),
@ClientID BIGINT
)        
as        
-- =============================================        
-- Author: Mohan Raja .C        
-- Create date: 18/4/2012        
-- Description: Get Client  Informations        
-- =============================================        
SET NOCOUNT ON        
BEGIN       
SELECT [ClientID],        
       CASE WHEN [ClientCD] IS NULL THEN '' ELSE LTRIM(RTRIM([ClientCD])) END AS [ClientCD],        
       [ClientDescription],        
       [CustomerID],        
       [LastUpdUID],        
       [LastUpdTS],      
       [IsDeleted] ,  
       [IsInActive]  
FROM  [Client] WHERE CustomerID=@CustomerID and [IsDeleted] = 0    
AND ClientID = case when @ClientID <>0 then @ClientID else ClientID end 
AND ClientCD = case when Ltrim(Rtrim(@ClientCD)) <> '' then @ClientCD else ClientCD end 

order by ClientCD    
      
END 
GO


