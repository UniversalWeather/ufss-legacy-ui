
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteHelpItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteHelpItems]
GO


/****** Object:  StoredProcedure [dbo].[spDeleteHelpItems]    Script Date: 11/01/2013 17:56:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteHelpItems]  
(    
 @ItemId INT 
 )  
AS  
BEGIN   
SET NOCOUNT ON  

DELETE FROM [HelpItems] WHERE ItemID = @ItemId
  
END

GO

