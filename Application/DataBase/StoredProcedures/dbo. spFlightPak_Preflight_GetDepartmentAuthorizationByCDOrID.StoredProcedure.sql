
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID]    Script Date: 01/24/2014 11:10:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID]    Script Date: 01/24/2014 11:10:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID](@CustomerID  BIGINT,
@AuthorizationCD varchar(8),
@AuthorizationID BIGINT
)      
AS     
Begin 
-- =============================================      
-- Author: Sujitha.V      
-- Create date: 5/5/2012      
-- Description: Get All DepartmentAuthorization Informations      
-- EXEC [spFlightPak_Preflight_GetDepartmentAuthorizationByCDOrID] 10003, '34534', 0
-- =============================================      
SET NOCOUNT ON      
   
SELECT      [AuthorizationID]    
           ,da .[DepartmentID]    
           ,CASE WHEN [AuthorizationCD] IS NULL THEN '' ELSE LTRIM(RTRIM([AuthorizationCD])) END AS AuthorizationCD   
           ,da .[CustomerID]    
           ,da .[DeptAuthDescription]    
           ,da .[ClientID]    
           ,da .[LastUpdUID]    
           ,da .[LastUpdTS]    
           ,da .[IsInActive]    
           ,[AuthorizerPhoneNum]    
           ,da .[IsDeleted]    
             
               
FROM  DepartmentAuthorization da    WHERE da.CustomerID =@CustomerID  
AND ISNULL(da.IsDeleted,0) = 0 
AND da.AuthorizationID = case when @AuthorizationID <>0 then @AuthorizationID else da.AuthorizationID end 
AND da.AuthorizationCD = case when Ltrim(Rtrim(@AuthorizationCD)) <> '' then @AuthorizationCD else da.AuthorizationCD end 

order by AuthorizationCD  
END    


GO


