
GO
/****** Object:  StoredProcedure [dbo].[spAddHelpContent]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddHelpContent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddHelpContent]
GO

CREATE PROCEDURE spAddHelpContent
(@Identifier varchar(60)
,@Category varchar(100)
,@Topic varchar(100)
,@Content nvarchar(max)
,@LastUpdUID varchar(30)
,@LastUpdTS datetime
,@IsDeleted bit
,@IsInActive bit)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()

	IF NOT EXISTS (SELECT Identifier FROM HelpContent
					WHERE	Identifier = @Identifier
						AND	Category = @Category
						AND	Topic = @Topic)
		BEGIN

			INSERT INTO HelpContent
			(
				Identifier,
				Topic,
				Category,
				Content,
				LastUpdUID,
				LastUpdTS,
				IsDeleted,
				IsInActive
			)
			VALUES
			(
				@Identifier,
				@Topic,
				@Category,
				@Content,
				@LastUpdUID,
				@LastUpdTS,
				@IsDeleted,
				@IsInActive
			)

		END
	ELSE
		BEGIN

			UPDATE	HelpContent
			SET		Identifier = @Identifier,
					Category = @Category,
					Topic = @Topic,
					Content = @Content,
					LastUpdUID = @LastUpdUID,
					LastUpdTS = @LastUpdTS,
					IsDeleted = @IsDeleted,
					IsInActive = @IsInActive
			WHERE 	Identifier = @Identifier
					AND	Category = @Category
					AND	Topic = @Topic

		END

END