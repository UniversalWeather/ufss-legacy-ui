
GO
/****** Object:  StoredProcedure [dbo].[spUpdateSalesPerson]    Script Date: 02/25/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CustomUpdateUserGroupPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CustomUpdateUserGroupPermission]
go
CREATE PROC [dbo].[spFlightPak_CustomUpdateUserGroupPermission]  
@UpdateTable dbo.UserGroupPermissionType ReadOnly  
AS  
BEGIN  
	MERGE INTO dbo.UserGroupPermission AS T  
    USING @UpdateTable AS S  
    --CustomerID=@CustomerID and UMPermissionID=@UMPermissionID and UserGroupID=@UserGroupID  
    ON --T.CustomerID = S.CustomerID AND T.UMPermissionID  = S.UMPermissionID AND  T.UserGroupID = S.UserGroupID
    T.UserGroupPermissionId = S.UserGroupPermissionId
    WHEN MATCHED THEN UPDATE SET CanView = S.CanView, CanAdd=S.CanAdd    
          ,CanEdit=S.CanEdit ,CanDelete=S.CanDelete, LastUpdUID=GETUTCDATE(),LastUpdTS=S.LastUpdTS, IsDeleted=0, IsInActive = NULL  ;  
    --WHEN NOT MATCHED THEN   
      
    --INSERT VALUES((SELECT MAX(USERGROUPPERMISSIONID + 1) FROM USerGroupPermission)  
    --      ,S.CustomerId        
    --       ,S.UserGroupID        
    --       ,S.UMPermissionID        
    --       ,S.CanView        
    --       ,S.CanAdd        
    --       ,S.CanEdit        
    --       ,S.CanDelete        
    --       ,S.LastUpdUID        
    --       ,GETUTCDATE()        
    --       ,0  
    --       ,NULL);  
    DECLARE @LoopingVariable BIGINT 
    SET  @LoopingVariable = -1000000
    DECLARE @CustomerId Bigint  
    WHILE (@LoopingVariable Is NOT Null)  
		BEGIN  
		IF(@LoopingVariable IS NOT NULL)
		BEGIN
			SET @LoopingVariable = (SELECT MIN(UsergroupPermissionId) FROM  @UpdateTable WHERE UsergroupPermissionId > @LoopingVariable)  
			DECLARE @UserGroupPermissionID BIGINT  
			SELECT @CustomerId = CustomerId from @UpdateTable WHERE UsergroupPermissionId = @LoopingVariable  
			IF(@LoopingVariable <0)
			BEGIN
				EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo',  @UserGroupPermissionID OUTPUT       
			    INSERT INTO UserGroupPermission (UserGroupPermissionID,CustomerID,UserGroupID,UMPermissionID,  
				CanView,CanAdd,CanEdit,CanDelete,LastUpdUID,LastUpdTS,IsDeleted)  
				SELECT @UserGroupPermissionID,CustomerID,UserGroupID,UMPermissionID,  
				CanView,CanAdd,CanEdit,CanDelete,LastUpdUID,LastUpdTS,IsDeleted FROM @UpdateTable WHERE UsergroupPermissionId = @LoopingVariable  
			END
			--ELSE
			--BEGIN
			--	UPDATE UserGroupPermission SET CanView = S.CanView, CanAdd=S.CanAdd    
			--	,CanEdit=S.CanEdit ,CanDelete=S.CanDelete, LastUpdUID = GETUTCDATE(),LastUpdTS=S.LastUpdTS, IsDeleted=0, IsInActive = NULL 
			--	FROM UserGroupPermission T
			--	JOIN @UpdateTable AS S ON T.CustomerID = S.CustomerID AND T.UMPermissionID  = S.UMPermissionID AND  T.UserGroupID = S.UserGroupID
			--END
			
		END
 END  
END  
  
  
  



GO
