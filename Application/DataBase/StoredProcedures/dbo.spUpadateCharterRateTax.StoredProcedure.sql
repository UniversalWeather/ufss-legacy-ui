
/****** Object:  StoredProcedure [dbo].[UpadateCharterRateTax]    Script Date: 02/01/2013 11:59:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpadateCharterRateTax]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpadateCharterRateTax]
GO



/****** Object:  StoredProcedure [dbo].[UpadateCharterRateTax]    Script Date: 02/01/2013 11:59:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[UpadateCharterRateTax]
(
@FleetID bigint,
@AircraftID bigint,
@DailyUsageAdjTax bit,
@LandingFeeTax bit,
@LastUpdUID char(30),      
@LastUpdTS datetime 
)
as 
Begin       
Set NoCount On
-- =============================================      
-- Author:  karthikeyan.s      
-- Create date: 01/31/2013
-- Description: Updating the Trip     
-- =============================================   
SET @LastUpdTS = GETUTCDATE() 
IF(@FleetID < 1)      
BEGIN
	UPDATE Aircraft Set DailyUsageAdjTax = @DailyUsageAdjTax,LandingFeeTax = @LandingFeeTax,[LastUpdUID] = @LastUpdUID,[LastUpdTS] = @LastUpdTS   where AircraftID = @AircraftID
END
IF(@AircraftID < 1)      
BEGIN
	UPDATE Fleet Set IsTaxDailyAdj = @DailyUsageAdjTax,IsTaxLandingFee = @LandingFeeTax,[LastUpdUID] = @LastUpdUID,[LastUpdTS] = @LastUpdTS   where FleetID = @FleetID
END
End

GO


