
/****** Object:  StoredProcedure [dbo].[spUpdateExchangeRate]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateExchangeRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateExchangeRate]
go
CREATE procedure [dbo].[spUpdateExchangeRate](
	@ExchangeRateID BIGINT,
	@CustomerID BIGINT ,
	@ExchRateCD VARCHAR(6) ,
	@ExchRateDescription VARCHAR(30) ,
	@FromDT DATE,
	@ToDT DATE,
	@ExchRate NUMERIC(10, 5) ,
	@CurrencyID NUMERIC(3, 0) ,
	@LastUpdUID VARCHAR(10) ,
	@LastUpdTS DATETIME ,
	@IsDeleted BIT ,
	@IsInActive BIT)

-- =============================================
-- Author:Hamsha.S
-- Create date: 01/05/2012
-- Description: Update the ExchangeRateMaster information
-- =============================================
as
begin 
SET NoCOUNT ON
set @LastUpdTS = GETUTCDATE()
UPDATE [ExchangeRate]
   SET 
    CustomerID=@CustomerID
    ,ExchRateCD=@ExchRateCD 
	,ExchRateDescription=@ExchRateDescription 
	,FromDT=Convert(DATE,@FromDT)
	,ToDT=Convert(DATE,@ToDT)
	,ExchRate=@ExchRate	
	,LastUpdUID=@LastUpdUID 
	,LastUpdTS=@LastUpdTS
	,IsDeleted=@IsDeleted
	,IsInActive=@IsInActive 
		  
 WHERE ExchangeRateID = @ExchangeRateID

end
GO
