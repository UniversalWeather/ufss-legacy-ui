/****** Object:  StoredProcedure [dbo].[spGetAllAircraftDutyType]    Script Date: 01/07/2013 19:32:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAircraftDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAircraftDutyType]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllAircraftDutyType]    Script Date: 01/07/2013 19:32:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllAircraftDutyType](@CustomerID  BIGINT)    
AS    
  
SET NOCOUNT ON    
    
SELECT [AircraftDutyID]  
      ,[CustomerID]    
      ,CASE WHEN [AircraftDutyCD] IS NULL THEN '' ELSE LTRIM(RTRIM(AircraftDutyCD)) END AS AircraftDutyCD   
      ,[AircraftDutyDescription]    
      ,[ClientID]    
      ,[GraphColor]    
      ,[LastUpdUID]    
      ,[LastUpdTS]    
      ,[ForeGrndCustomColor]    
      ,[BackgroundCustomColor]    
      ,[IsCalendarEntry]    
      ,[DefaultStartTM]    
      ,[DefualtEndTM]    
      ,[IsAircraftStandby]    
      ,[IsDeleted]    
      ,[IsInActive]
FROM  [AircraftDuty] WHERE   ISNULL(IsDeleted,0) = 0 AND CustomerID = @CustomerID   
order by AircraftDutyCD  
GO


