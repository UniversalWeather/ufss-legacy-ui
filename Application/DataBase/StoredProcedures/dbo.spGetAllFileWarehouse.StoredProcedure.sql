IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFileWarehouse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFileWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFileWarehouse]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetAllFileWarehouse]
 (
   @CustomerID BIGINT,
   @RecordType VARCHAR(20),
   @RecordID BIGINT
  )  
-- =============================================  
-- Author:Mathes
-- Create date: 17/7/2012   
-- Description: Get the Filewarehouse information based on type 
-- =============================================  
as  
begin   
SET NoCOUNT ON  
     SELECT  
			FileWarehouseID,
			UWAFileName,
			UWAFilePath,
			UWAWebpageName,
			CustomerID,
			RecordID,
			SequenceNumber, 
			RecordType
			LastUpdUID,
			LastUpdTS,
			IsDeleted,
			RecordType,
			IsInactive
	 FROM   FileWarehouse  
     WHERE  CustomerID=@CustomerID AND RecordType=@RecordType AND RecordID=@RecordID AND ISNULL(IsDeleted,0) = 0
     ORDER BY FileWarehouseID DESC
  
end
GO
