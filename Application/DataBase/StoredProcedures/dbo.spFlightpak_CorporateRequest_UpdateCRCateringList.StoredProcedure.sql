/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_UpdateCRCateringList]    Script Date: 04/25/2013 17:50:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_UpdateCRCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRCateringList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRCateringList]
(@CRCateringListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@CateringID BIGINT
,@CRCateringListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@FaxNum VARCHAR(25)
,@Email VARCHAR(250)
,@Rate numeric(6, 2)
)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
		
	UPDATE CRCateringList
	SET CustomerID = @CustomerID
		,CRLegID = @CRLegID
		,TripNUM = @TripNUM
		,LegID = @LegID
		,RecordType = @RecordType
		,CateringID = @CateringID
		,CRCateringListDescription = @CRCateringListDescription
		,PhoneNUM = @PhoneNUM
		,IsCompleted = @IsCompleted
		,AirportID = @AirportID
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
		,FaxNum=@FaxNum
		,Email=@Email
		,Rate=@Rate
	WHERE 
		CRCateringListID = @CRCateringListID

END
GO