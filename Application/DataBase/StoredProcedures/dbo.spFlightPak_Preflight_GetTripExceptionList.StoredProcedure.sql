

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripExceptionList]    Script Date: 09/19/2013 03:48:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetTripExceptionList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetTripExceptionList]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripExceptionList]    Script Date: 09/19/2013 03:48:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create Procedure [dbo].[spFlightPak_Preflight_GetTripExceptionList]
(
@CustomerID BIGINT,
@TripID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get Preflightmain by Trip ID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PEx.*
FROM PreflightMain PM , PreflightTripException PEx
 where 
 PM.TripID = @TripID 
 and PM.TripID = PEx.TripID
 and PM.CustomerID=@CustomerID and PM.IsDeleted=0


GO


