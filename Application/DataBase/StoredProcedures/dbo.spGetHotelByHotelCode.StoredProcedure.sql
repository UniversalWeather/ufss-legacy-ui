/****** Object:  StoredProcedure [dbo].[spGetHotelByHotelCode]    Script Date: 01/08/2013 16:37:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHotelByHotelCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHotelByHotelCode]
GO


/****** Object:  StoredProcedure [dbo].[spGetHotelByHotelCode]    Script Date: 01/08/2013 16:37:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetHotelByHotelCode](@CustomerID bigint, @AirportID BIGINT, @HotelCode char(4))      
AS      
-- =============================================      
-- Author: Leela M     
-- Create date: 09/11/2012      
-- Description: Get Hotel By HotelCode    
-- =============================================      
  -- declare @CustomerID bigint  
  -- declare @AirportID BIGINT  
  -- declare @HotelCode char(4)  
  -- set @CustomerID = 10001  
  -- set @AirportID = 1000164674  
  -- set @HotelCode = '0001'  
-- =============================================      
  
--[dbo].[spGetHotelByHotelCode] '10001' , 1000167401, '0001'  
SET NOCOUNT ON      
    
DECLARE @HotelICAOID CHAR(4)    
--DECLARE @UWACustomerID BIGINT    
    
SELECT @HotelICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID    
--EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
DECLARE @UWACustomerID BIGINT       
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
      
DECLARE @ICAOCD VARCHAR(50)  
DECLARE @UWAAirportId VARCHAR(50)      
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD  
    
 SELECT   H.[HotelID]    
   ,H.[AirportID]    
   ,H.[HotelCD]    
   ,H.[CustomerID]    
   ,H.[IsChoice]    
   ,H.[Name]    
   ,H.[PhoneNum]    
   ,H.[MilesFromICAO]    
   ,H.[HotelRating]    
   ,H.[FaxNum]    
   ,H.[NegociatedRate]    
   ,H.[Addr1]    
   ,H.[Addr2]    
   ,H.[Addr3]    
   ,H.[CityName]    
   ,H.[MetroID]    
   ,H.[StateName]    
   ,H.[PostalZipCD]    
   ,H.[CountryID]    
   ,H.[ContactName]    
   ,H.[Remarks]    
   ,H.[IsPassenger]    
   ,H.[IsCrew]    
   ,H.[IsUWAMaintained]    
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter    
   ,H.[UWAMaintFlag]     
   ,H.[LatitudeDegree]    
   ,H.[LatitudeMinutes]    
   ,H.[LatitudeNorthSouth]    
   ,H.[LongitudeDegrees]    
   ,H.[LongitudeMinutes]    
   ,H.[LongitudeEastWest]    
   ,H.[LastUpdUID]    
   ,H.[LastUpdTS]    
   ,H.[UpdateDT]    
   ,H.[RecordType]    
   ,H.[MinutesFromICAO]    
   ,H.[SourceID]    
   ,H.[ControlNum]    
   ,H.[Website]       
   ,H.[UWAID]    
   ,H.[IsInActive]    
   ,H.[IsDeleted]    
   ,H.[UWAUpdates]    
   ,@HotelICAOID IcaoID       
   ,m.MetroCD    
   ,c.CountryCD     
   ,H.[AltBusinessPhone]    
            ,H.[TollFreePhone]    
            ,H.[BusinessEmail]    
            ,H.[ContactBusinessPhone]    
            ,H.[CellPhoneNum]    
            ,H.[ContactEmail]
            ,H.[ExchangeRateID]
            ,H.[NegotiatedTerms]
            ,H.[SundayWorkHours] 
            ,H.[MondayWorkHours]   
            ,H.[TuesdayWorkHours]
            ,H.[WednesdayWorkHours]
            ,H.[ThursdayWorkHours]
            ,H.[FridayWorkHours]
            ,H.[SaturdayWorkHours]           
 FROM  [Hotel] H    
 --inner join [Airport] Ai on H.AirportID = Ai.AirportID     
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID     
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID    
 WHERE     
   
 ((H.AirportID = @AirportID       
  AND H.CustomerID = @CustomerID ) or (H.AirportID  = @UWAAirportId       
  AND  H.CustomerID = @CustomerID )) AND H.IsDeleted = 0 AND H.HotelCD = @HotelCode  
 --H.AirportID = @AirportID    
 ----Ai.IcaoID = @HotelICAOID     
 ----AND (AI.CustomerID = @CustomerID OR Ai.CustomerID = @UWACustomerID)    
 --AND (H.CustomerID = @CustomerID)     
 --AND H.IsDeleted = 0    
     
 UNION ALL    
     
 SELECT   H.[HotelID]    
   ,H.[AirportID]    
   ,H.[HotelCD]    
   ,H.[CustomerID]    
   ,H.[IsChoice]    
   ,H.[Name]    
   ,H.[PhoneNum]    
   ,H.[MilesFromICAO]    
   ,H.[HotelRating]    
   ,H.[FaxNum]    
   ,H.[NegociatedRate]    
   ,H.[Addr1]    
   ,H.[Addr2]    
   ,H.[Addr3]    
   ,H.[CityName]    
   ,H.[MetroID]    
   ,H.[StateName]    
   ,H.[PostalZipCD]    
   ,H.[CountryID]    
   ,H.[ContactName]    
   ,H.[Remarks]    
   ,H.[IsPassenger]    
   ,H.[IsCrew]    
   ,H.[IsUWAMaintained]    
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter    
   ,H.[UWAMaintFlag]      
   ,H.[LatitudeDegree]    
   ,H.[LatitudeMinutes]    
   ,H.[LatitudeNorthSouth]    
   ,H.[LongitudeDegrees]    
   ,H.[LongitudeMinutes]    
   ,H.[LongitudeEastWest]    
   ,H.[LastUpdUID]    
   ,H.[LastUpdTS]    
   ,H.[UpdateDT]    
   ,H.[RecordType]    
   ,H.[MinutesFromICAO]    
   ,H.[SourceID]    
   ,H.[ControlNum]    
   ,H.[Website]       
   ,H.[UWAID]    
   ,H.[IsInActive]    
   ,H.[IsDeleted]    
   ,H.[UWAUpdates]    
   ,@HotelICAOID IcaoID       
   ,m.MetroCD    
   ,c.CountryCD     
   ,H.[AltBusinessPhone]    
            ,H.[TollFreePhone]    
            ,H.[BusinessEmail]    
            ,H.[ContactBusinessPhone]    
            ,H.[CellPhoneNum]    
            ,H.[ContactEmail]  
             ,H.[ExchangeRateID]
            ,H.[NegotiatedTerms]
            ,H.[SundayWorkHours] 
            ,H.[MondayWorkHours]   
            ,H.[TuesdayWorkHours]
            ,H.[WednesdayWorkHours]
            ,H.[ThursdayWorkHours]
            ,H.[FridayWorkHours]
            ,H.[SaturdayWorkHours]          
 FROM  [Hotel] H    
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID     
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID    
    
 WHERE     
 H.AirportID = @UWAAirportId    
 AND H.CustomerID = @UWACustomerID    
 AND H.IsDeleted = 0    
 AND H.HotelCD NOT IN     
  (SELECT HotelCD FROM Hotel     
  WHERE   
   
  ((AirportID  = @AirportID       
  AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId       
  AND  CustomerID = @CustomerID ))  
  AND IsDeleted = 0   AND H.HotelCD = @HotelCode  
  -- AirportID = @AirportID    
  ----Ai.IcaoID = @HotelICAOID     
  ----AND (AI.CustomerID = @CustomerID OR Ai.CustomerID = @UWACustomerID)    
  --AND CustomerID = @CustomerID    
  --AND IsDeleted = 0    
 )    
  AND H.HotelCD = @HotelCode  
    
 -- ORDER BY HotelCD  
  
  
GO


