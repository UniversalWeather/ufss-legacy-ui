

/****** Object:  StoredProcedure [dbo].[CopyAllFleetCharterRate]    Script Date: 04/17/2013 15:56:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CopyAllFleetCharterRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CopyAllFleetCharterRate]
GO



/****** Object:  StoredProcedure [dbo].[CopyAllFleetCharterRate]    Script Date: 04/17/2013 15:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[CopyAllFleetCharterRate]
(
@CustomerID bigint,
@AircraftID bigint,
@NewAircraftID bigint
)
as 
begin


DECLARE @FleetNewCharterRateID BIGINT          
         
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetNewCharterRateID OUTPUT    
print @FleetNewCharterRateID
INSERT FleetNewCharterRate (    
    FleetNewCharterRateID,            
    CustomerID,    
 FleetID,    
 AircraftCD,    
 OrderNum,    
 FleetNewCharterRateDescription,    
 ChargeUnit,    
 NegotiatedChgUnit,    
 BuyDOM,    
 SellDOM,    
 IsTaxDOM,    
 IsDiscountDOM,    
 BuyIntl,    
 SellIntl,    
 IsTaxIntl,    
 IsDiscountIntl,    
 StandardCrewDOM,    
 StandardCrewIntl,    
 MinimumDailyRequirement,    
 YearMade,    
 ExteriorColor,    
 ColorIntl,    
 IsAFIS,    
 IsUWAData,    
 DBAircraftFlightChgID,    
 DSAircraftFlightChgID,    
 BuyAircraftFlightIntlID,    
 SellAircraftFlightIntlID,    
 LastUpdUID,    
 LastUpdTS,    
 AircraftTypeID,
 CQcustomerId,    
 IsDeleted    
 )  
 select 
@FleetNewCharterRateID as 'FleetNewCharterRateID',            
    CustomerID,    
 FleetID,    
 AircraftCD,    
 OrderNum,    
 FleetNewCharterRateDescription,    
 ChargeUnit,    
 NegotiatedChgUnit,    
 BuyDOM,    
 SellDOM,    
 IsTaxDOM,    
 IsDiscountDOM,    
 BuyIntl,    
 SellIntl,    
 IsTaxIntl,    
 IsDiscountIntl,    
 StandardCrewDOM,    
 StandardCrewIntl,    
 MinimumDailyRequirement,    
 YearMade,    
 ExteriorColor,    
 ColorIntl,    
 IsAFIS,    
 IsUWAData,    
 DBAircraftFlightChgID,    
 DSAircraftFlightChgID,    
 BuyAircraftFlightIntlID,    
 SellAircraftFlightIntlID,    
 LastUpdUID,    
 LastUpdTS,    
 @NewAircraftID as 'AircraftTypeID',
 CQcustomerId,    
 IsDeleted
 FROM FleetNewCharterRate where AircraftTypeID = @NewAircraftID
 
 end
GO


