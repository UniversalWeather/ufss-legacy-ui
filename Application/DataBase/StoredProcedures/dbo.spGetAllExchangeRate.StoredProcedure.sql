IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllExchangeRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllExchangeRate]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllExchangeRate]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllExchangeRate](@CustomerID BIGINT)
AS
-- =============================================
-- Author:Hamsha
-- Create date: 12/4/2012
-- Description: Get the ExchangeRate information
-- =============================================
SET NoCOUNT ON
IF LEN(@CustomerID) > 0
BEGIN 
	
	SELECT 
	   ExchangeRateID	
	  ,[CustomerID]
      ,[ExchRateCD]
      ,[ExchRateDescription]
      ,[FromDT]
      ,[ToDT]
      ,[ExchRate]
      ,[CurrencyID]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive  
  FROM ExchangeRate
  WHERE CustomerID = @CustomerID  and 
		IsDeleted = 'false'  
		order by ExchRateCD
END
GO
