
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateClient]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateClient]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpdateClient]( @ClientID BIGINT,  
           @ClientCD CHAR(5),   
           @ClientDescription VARCHAR(60),   
           @CustomerID BIGINT,    
           @LastUpdUID VARCHAR(30),    
           @LastUpdTS DATETIME,  
           @IsDeleted BIT,
           @IsInActive BIT)    
-- =============================================    
-- Author: Sujitha.V    
-- Create date: 5/5/2012    
-- Description: Update the Department information    
-- =============================================    
AS    
BEGIN     
SET NOCOUNT ON    
  SET @LastUpdTS = GETUTCDATE()  
 UPDATE Client    
 SET ClientCD=@ClientCD,  
     ClientDescription=@ClientDescription,  
     CustomerID=@CustomerID,  
     LastUpdUID=@LastUpdUID,  
     LastUpdTS=@LastUpdTS,  
     IsDeleted=@IsDeleted,
     IsInActive=@IsInActive 
 WHERE ClientID = @ClientID  
    
END  
GO
