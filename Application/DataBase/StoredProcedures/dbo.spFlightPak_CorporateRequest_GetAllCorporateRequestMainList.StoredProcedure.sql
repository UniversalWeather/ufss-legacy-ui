
/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAllCorporateRequestMainList]    Script Date: 02/05/2014 11:47:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetAllCorporateRequestMainList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAllCorporateRequestMainList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAllCorporateRequestMainList]    Script Date: 02/05/2014 11:47:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAllCorporateRequestMainList]   
 (  
  @CustomerID Bigint,  
  @HomebaseID Bigint,  
  @ClientID Bigint,  
  @TripSheet varchar(1),  
  @Worksheetstr varchar(1),  
  @Hold varchar(1),  
  @Cancelled varchar(1),  
  @SchedServ varchar(1),  
  @UnFillFilled varchar(1),  
  @isLog  bit,  
  @EstDepartureDT date = null,
  @isPrivateUser  bit,
  @HomebaseIDstr varchar(1000),
  @TravelID Bigint
 )  
 AS    
 SET NOCOUNT ON;   
 BEGIN  
      
  
  Declare @Sql varchar(2000)  
  Declare @StatusSql varchar(2000)  
  Declare @StatusEntered bit  
  
  set @Sql =  'SELECT   
      CR.CRMainID,  
      CR.CRTripNUM,  
      CR.DispatchNUM,
      CR.EstDepartureDT,  
      P.PassengerRequestorCD as ''Rqstr'',  
      CR.CRMainDescription,  
      CR.CorporateRequestStatus,  
      CR.RequestDT,  
      F.TailNum,
      AR.AircraftCD, 
      CR.AcknowledgementStatus, 
      A.ICAOID AS HomebaseCD,
      PM.TripNUM,
      CR.TripStatus 
      FROM CRMain CR
      INNER JOIN Company C on CR.HomebaseID = C.HomebaseID  
      INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID   
      LEFT JOIN PreflightMain PM on CR.TripID = PM.TripID   AND Isnull(PM.IsDeleted,0) = 0 
      LEFT JOIN Fleet F on  CR.FleetID = F.FleetID  
      LEFT JOIN Aircraft AR on F.AircraftID = AR.AircraftID
      LEFT JOIN Passenger P on CR.PassengerRequestorID = P.PassengerRequestorID       
      WHERE           
       CR.CustomerID=' +   CAST( @CustomerID  as varchar(20))  
      set @Sql =  @Sql + ' and CR.IsDeleted =0 '   
        
      set @StatusSql = ''  
        
      if (@TripSheet <> '')  
      begin  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @TripSheet + ''' ) '  
      end  
        
      if (@Worksheetstr <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (CR.TripStatus = ''' + @Worksheetstr + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @Worksheetstr + ''' )'  
      end  
        
      if (@Hold <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (CR.TripStatus = ''' + @Hold + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @Hold + ''' )'  
      end  
        
      if (@Cancelled  <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (CR.TripStatus = ''' + @Cancelled  + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @Cancelled  + ''' )'  
      end  
      
    if (@UnFillFilled <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (CR.TripStatus = ''' + @UnFillFilled + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @UnFillFilled  + ''' )'  
      end  
      
    if (@SchedServ <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (CR.TripStatus = ''' + @SchedServ + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(CR.TripStatus = ''' + @SchedServ  + ''' )'  
      end 
    
       
    if (@StatusSql<>'')     
     set @Sql =  @Sql + ' and ( ' + @StatusSql + ' ) ' 
     
    if (@EstDepartureDT is not null)  
     set @Sql =  @Sql + ' and CR.EstDepartureDT > = ''' + cast (@EstDepartureDT as varchar(25)) + ''''  
    
    if (@isPrivateUser <>0)
		set @Sql =  @Sql + ' and (CR.isPrivate = 0) '  
    
    
     
    
    
    If (@HomebaseIDstr<> '')  
     set @Sql =  @Sql + ' and CR.HomebaseID  in ('  + @HomebaseIDstr     + ')'
    else
		begin
			If (@HomebaseID<> 0)  
				set @Sql =  @Sql + ' and CR.HomebaseID  = ' + cast (@HomebaseID as varchar(25))    
		end
    If (@ClientID<> 0)  
     set @Sql =  @Sql + ' and CR.ClientID  = ' + cast (@ClientID as varchar(25))    
     
   If (@TravelID<> 0)  
     set @Sql =  @Sql + ' and CR.TravelCoordinatorID  = ' + cast (@TravelID as varchar(25))        
    
    set @Sql =  @Sql + ' Order by CR.EstDepartureDT'        
    
   --print @Sql  
    
      create table #tempCRmain  
      (  
      CRMainID bigint,  
      CRTripNUM int,
      DispatchNUM varchar(12),
      EstDepartureDT date,  
      Rqstr varchar(100),  
      TripDescription varchar(40),  
      CorporateRequestStatus char(1),  
      RequestDT date,  
      TailNum char(10),
      AircraftCD varchar(10), 
      AcknowledgementStatus char(1), 
      HomebaseCD  char(4),  
      TripNUM bigint,
      TripSheetStatus char(1)
      )  
      insert into #tempCRmain Exec ( @Sql)  
        
SELECT
      
		  CRMainID,  
		  CRTripNUM,
		  DispatchNUM,
		  EstDepartureDT,  
		  Rqstr,  
		  TripDescription,  
		  CorporateRequestStatus,  
		  RequestDT,  
		  TailNum,
		  AircraftCD, 
		  AcknowledgementStatus, 
		  HomebaseCD,  
		  TripNUM,
		  TripSheetStatus
	    
FROM #tempCRmain

      union  
      SELECT   
      CR.CRMainID,  
      CR.CRTripNUM,  
      CR.DispatchNUM,   
      CR.EstDepartureDT,      
      P.PassengerRequestorCD as 'Rqstr',  
      CR.CRMainDescription,  
      CR.CorporateRequestStatus,  
      CR.RequestDT,  
      F.TailNum, 
	  AR.AircraftCD,    
      CR.AcknowledgementStatus, 
      A.IcaoID as HomebaseCD ,
	  PM.TripNUM,
      CR.TripStatus 
      FROM CRMain CR
      INNER JOIN Company C on CR.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      LEFT JOIN PreflightMain PM on CR.TripID = PM.TripID  
      LEFT JOIN Fleet F on  CR.FleetID = F.FleetID  
      LEFT JOIN Aircraft AR on F.AircraftID = AR.AircraftID
      LEFT JOIN Passenger P on CR.PassengerRequestorID = P.PassengerRequestorID     
      WHERE 1=2  
        
      DROP TABLE #tempCRmain  
 END


GO


