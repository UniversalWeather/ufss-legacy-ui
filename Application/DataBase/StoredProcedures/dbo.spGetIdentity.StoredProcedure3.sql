/****** Object:  StoredProcedure [dbo].[spGetIdentity]    Script Date: 08/29/2012 16:50:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetIdentity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetIdentity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--DROP PROC    [spGetIdentity] 
-- =========================================================                                                
-- Author:  Viswanathan Perumal                                                
-- Create date: 19-07-2012                                                
-- Description: Get Identity of an user based on SessionId                                                
-- =========================================================                                                
CREATE PROC [dbo].[spGetIdentity]                                                
@SessionId [uniqueidentifier]                                                
AS                                                
BEGIN                                                
Declare @listStr VARCHAR(MAX)     
DECLARE @listModule varchar(MAX)                                               
Declare @listClientInfo VARCHAR(MAX)                        
Declare @IsAdmin bit    
DECLARE @CustomerId BIGINT                  
-- Get the IsAdmin State                    
Select @IsAdmin = UserMaster.IsSystemAdmin, @CustomerId = UserMaster.CustomerID from FPLoginSession JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID WHERE                                                 
  FPLoginSession.SessionID = @SessionId                    
-- Build the actual roles                       
-- If the user is admin give the full access rights to that user which the modules he have access                                       
IF(@IsAdmin = 'true')                    
BEGIN                    
SELECT @listStr = COALESCE(@listStr+',' , '') + LOWER(Value) + LTRIM(RTRIM(LOWER([PermissionName])))   FROM (                                                  
 SELECT                                                 
  'true' AS [Add],                                                
  'true' AS [Delete],                                                
  'true' AS [Edit],                                                
  'true' AS [View],                                                
  UMPermission.UMPermissionRole AS [PermissionName]                                                
 FROM                                               
  FPLoginSession                                                
 JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID                                                
 JOIN CustomerModuleAccess ON CustomerModuleAccess.CustomerID = UserMaster.CustomerID AND CustomerModuleAccess.CanAccess = 1                    
 LEFT JOIN UMPermission ON UMPermission.ModuleID = CustomerModuleAccess.ModuleID                                           
                      
 WHERE                                                 
  FPLoginSession.SessionID = @SessionId                                               
   AND                                                 
  UserMaster.IsDeleted = 'false'                                                
   AND                                                 
  UserMaster.IsActive = 'true'                                            
  AND                                                       
  UMPermission.IsDeleted = 'false'                           
 AND                         
  UserMaster.IsUserLock = 'false'    
  AND
  (UMPermission.CustomerId IS NULL OR UMPermission.CustomerId = @CustomerId)                                       
 )P                                                 
 UNPIVOT                                                     
  (col FOR value IN                                                      
  ([Add],                                                     
  [Delete],                                                     
  [Edit],                                                     
  [View]                                                  
  )                                                     
) AS RolesManager WHERE col = 'true'                        
END                    
ELSE                    
BEGIN                         
SELECT @listStr = COALESCE(@listStr+',' , '') + LOWER(Value) + LTRIM(RTRIM(LOWER([PermissionName])))   FROM (                                                  
 SELECT                                                 
  UserGroupPermission.CanAdd AS [Add],             
  UserGroupPermission.CanDelete AS [Delete],                                                
  UserGroupPermission.CanEdit AS [Edit],                                                
  UserGroupPermission.CanView AS [View],                                       
  UMPermission.UMPermissionRole AS [PermissionName]            
 FROM                                               
  FPLoginSession                                                
 JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID                                                
 LEFT JOIN UserGroupMapping ON UserGroupMapping.UserName = UserMaster.UserName                                                
 LEFT JOIN UserGroup ON UserGroup.UserGroupID = UserGroupMapping.UserGroupID                                                
 LEFT JOIN UserGroupPermission ON UserGroupPermission.UserGroupID = UserGroupMapping.UserGroupID                                                
 JOIN CustomerModuleAccess ON CustomerModuleAccess.CustomerID = UserMaster.CustomerID AND CustomerModuleAccess.CanAccess = 1                       
 LEFT JOIN UMPermission ON  UMPermission.UMPermissionID = UserGroupPermission.UMPermissionID  AND   UMPermission.ModuleID = CustomerModuleAccess.ModuleID                                            
                      
 WHERE                                                 
  FPLoginSession.SessionID = @SessionId 
 AND UserMaster.IsDeleted = 'false'                                             
 AND UserMaster.IsActive = 'true'                                            
 AND UserGroupMapping.IsDeleted = 'false'                                          
 AND UserGroup.IsDeleted = 'false'                                          
 AND UserGroupPermission.IsDeleted = 'false'                                          
 AND UMPermission.IsDeleted = 'false'                           
 AND UserMaster.IsUserLock = 'false'
 AND (UMPermission.CustomerId IS NULL OR UMPermission.CustomerId = @CustomerId)                                                
 )P                                                 
 UNPIVOT                                                     
  (col FOR value IN                                                      
  ([Add],                                                     
  [Delete],                                                     
  [Edit],                                                     
  [View]                                                  
  )                                                     
) AS RolesManager WHERE col = 'true'                                            
END   
select @listModule = COALESCE(@listModule+',' , '') + LTRIM(RTRIM(LOWER([ModuleID]))) from CustomerModuleAccess   
--JOIN Module on Module.ModuleID = CustomerModuleAccess.ModuleID  
where CustomerID = @CustomerId and CanAccess = 1 AND ( CONVERT(DATE,expirydate) >= CONVERT(DATE,GetDate()))                    
SELECT                                                 
 UserMaster.UserName,                                              
 UserMaster.FirstName,                                            
 UserMaster.MiddleName,                                            
 UserMaster.LastName,                                            
 UserMaster.HomebaseID,                                            
 --UserMaster.ClientID,                                              
 UserMaster.EmailID,                                                
 UserMaster.CustomerID,                         
 UserMaster.HomeBase,                                                
 UserMaster.IsSystemAdmin,                                     
 UserMaster.IsDispatcher,                           
 UserMaster.IsTripPrivacy,           
 UserMaster.IsCorpRequestApproval,          
 UserMaster.TravelCoordID,          
 TravelCoordinator.TravelCoordCD,                                 
 Convert(VARCHAR(max),@listStr) AS Roles,    
 Convert(VARCHAR(MAX), @listModule) AS Modules,                                    
 Airport.AirportName,                                      
 Airport.AirportID,                                      
 Airport.IcaoID,                                      
 dbo.GetClientInfobyUserName (UserMaster.UserName)  AS Info,                                         
 --ClientInfo.ClientId,                                    
 --ClientInfo.ClientCD                                        
   CAST(0 AS bigint) as ClientId,                                  
   '' as ClientCD,       
  Customer.IsAll,              
  Customer.IsAPIS,              
  Customer.IsATS,              
  Customer.IsMapping,              
  Customer.IsUVFuel,              
  Customer.IsUVTripLink,          
  UserPreference.KeyValue As LandingPage                               
FROM FPLoginSession                                                
 JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID                                         
 JOIN Company ON Company.HomebaseID = UserMaster.HomebaseID                                      
 JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                 
 JOIN Customer ON Customer.CustomerID = UserMaster.CustomerID                                          
 Left Join Client ON Client.ClientID = UserMaster.ClientID            
 LEFT JOIN TravelCoordinator ON TravelCoordinator.TravelCoordinatorID = UserMaster.TravelCoordID    
 LEFT JOIN UserPreference ON UserPreference.UserName = UserMaster.UserName AND UserPreference.CategoryName = 'landingpage'                              
 --CROSS APPLY ClientInfo               
WHERE                                                 
 FPLoginSession.SessionID = @SessionId                                           
  AND                                                 
 UserMaster.IsDeleted = 'false'                                               
  AND                
 UserMaster.IsActive = 'true' AND UserMaster.IsUserLock = 'false'  ORDER BY FPLoginSession.LoginStartTime desc                                      
                                       
                                       
END 