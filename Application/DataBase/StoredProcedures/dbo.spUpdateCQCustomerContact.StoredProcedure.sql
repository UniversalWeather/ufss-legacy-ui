
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCQCustomerContact]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCQCustomerContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCQCustomerContact]
GO

CREATE PROCEDURE spUpdateCQCustomerContact
(@CQCustomerContactID bigint
,@CustomerID bigint
,@CQCustomerID bigint
,@CQCustomerContactCD int
,@IsChoice bit
,@FirstName varchar(40)
,@Addr1 varchar(40)
,@Addr2 varchar(40)
,@Addr3 varchar(40)
,@CityName varchar(40)
,@StateName varchar(10)
,@PostalZipCD varchar(15)
,@CountryID bigint
,@PhoneNum varchar(25)
,@OtherPhoneNum varchar(25)
,@PrimaryMobileNum varchar(25)
,@SecondaryMobileNum varchar(25)
,@FaxNum varchar(25)
,@HomeFaxNum varchar(25)
,@CreditName1 nvarchar(240)
,@CreditNum1 nvarchar(240)
,@CardType1 nvarchar(240)
,@SecurityCode1 nvarchar(240)
,@ExpirationDate1 nvarchar(240)
,@CreditName2 nvarchar(240)
,@CreditNum2 nvarchar(240)
,@CardType2 nvarchar(240)
,@SecurityCode2 nvarchar(240)
,@ExpirationDate2 nvarchar(240)
,@MoreInfo varchar(MAX)
,@Notes varchar(MAX)
,@Title varchar(40)
,@LastName varchar(40)
,@MiddleName varchar(40)
,@EmailID varchar(100)
,@PersonalEmailID varchar(100)
,@OtherEmailID varchar(100)
,@ContactName varchar(40)
,@BusinessPhoneNum varchar(25)
,@BusinessFaxNum varchar(25)
,@BusinessEmailID varchar(100)
,@IsInActive bit
,@LastUpdUID varchar(30)
,@LastUpdTS datetime
,@IsDeleted bit)
AS BEGIN

	SET NOCOUNT OFF;	
	SET @LastUpdTS = GETUTCDATE()	
	
	IF (@IsChoice = 1)
		BEGIN
			UPDATE CQCustomerContact SET IsChoice = 0 WHERE CustomerID = @CustomerID
														AND CQCustomerID = @CQCustomerID
		END
		
	UPDATE CQCustomerContact
	SET	CustomerID = @CustomerID
		,CQCustomerID = @CQCustomerID
		,CQCustomerContactCD = @CQCustomerContactCD
		,IsChoice = @IsChoice
		,FirstName = @FirstName
		,Addr1 = @Addr1
		,Addr2 = @Addr2
		,Addr3 = @Addr3
		,CityName = @CityName
		,StateName = @StateName
		,PostalZipCD = @PostalZipCD
		,CountryID = @CountryID
		,PhoneNum = @PhoneNum
		,OtherPhoneNum = @OtherPhoneNum
		,PrimaryMobileNum = @PrimaryMobileNum
		,SecondaryMobileNum = @SecondaryMobileNum
		,FaxNum = @FaxNum
		,HomeFaxNum = @HomeFaxNum
		,CreditName1 = @CreditName1
		,CreditNum1 = @CreditNum1
		,CardType1 = @CardType1
		,SecurityCode1 = @SecurityCode1
		,ExpirationDate1 = @ExpirationDate1
		,CreditName2 = @CreditName2
		,CreditNum2 = @CreditNum2
		,CardType2 = @CardType2
		,SecurityCode2 = @SecurityCode2
		,ExpirationDate2 = @ExpirationDate2
		,MoreInfo = @MoreInfo
		,Notes = @Notes
		,Title = @Title
		,LastName = @LastName
		,MiddleName = @MiddleName
		,EmailID = @EmailID
		,PersonalEmailID = @PersonalEmailID
		,OtherEmailID = @OtherEmailID
		,ContactName = @ContactName
		,BusinessPhoneNum = @BusinessPhoneNum
		,BusinessFaxNum = @BusinessFaxNum
		,BusinessEmailID = @BusinessEmailID
		,IsInActive = @IsInActive
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = IsDeleted
	WHERE CQCustomerContactID = @CQCustomerContactID
	
END