/****** Object:  StoredProcedure [dbo].[spGetFlightPurposebyFlightPurposeID]    Script Date: 11/28/2012 18:43:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFlightPurposebyFlightPurposeID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFlightPurposebyFlightPurposeID]
GO

/****** Object:  StoredProcedure [dbo].[spGetFlightPurposebyFlightPurposeID]    Script Date: 11/28/2012 18:43:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetFlightPurposebyFlightPurposeID](@CustomerID BIGINT,@FlightPurposeID BIGINT)        
AS        
-- =============================================        
-- Author: Leela M        
-- Create date: 28 Nov 2012
-- Description: Get All Flight Purpose Informations        
-- =============================================  
BEGIN      
	SET NOCOUNT ON        
	  
	SELECT       
		[FlightPurposeID]      
	    ,[FlightPurposeCD]      
	   ,[FlightPurposeDescription]      
	   ,[CustomerID]      
	   ,[IsDefaultPurpose]      
	   ,[IsPersonalTravel]      
	   ,[ClientID]      
	   ,[LastUpdUID]      
	   ,[LastUpdTS]      
	   ,[IsWaitList]      
	   ,[IsDeleted]  
	   ,IsInactive
	FROM  [FlightPurpose] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0  AND FlightPurposeID=@FlightPurposeID
	
END 



GO


