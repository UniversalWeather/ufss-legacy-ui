
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetEmergencyContactByIDOrCD]    Script Date: 01/24/2014 11:10:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetEmergencyContactByIDOrCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetEmergencyContactByIDOrCD]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetEmergencyContactByIDOrCD]    Script Date: 01/24/2014 11:10:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREate PROCEDURE [dbo].[spFlightPak_Preflight_GetEmergencyContactByIDOrCD](@CustomerID  BIGINT,
@EmergencyContactCD varchar(8),
@EmergencyContactID Bigint
)        
AS        
SET NOCOUNT ON        
        
BEGIN        
--EXEC [spFlightPak_GetAllEmergencyContactByIDOrCD] 10003, '', 10003181145
SELECT  [EmergencyContactID]      
           ,[EmergencyContactCD]      
           ,[CustomerID]      
           ,[LastName]      
           ,[FirstName]      
           ,[MiddleName]      
           ,[Addr1]      
           ,[Addr2]      
           ,[CityName]      
           ,[StateName]      
           ,[PostalZipCD]      
           ,[CountryID]      
           ,[PhoneNum]      
           ,[FaxNum]      
           ,[CellPhoneNum]      
           ,[EmailAddress]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[IsDeleted]  
           ,[BusinessPhone]  
     ,[BusinessFax]  
     ,[CellPhoneNum2]  
     ,[OtherPhone]  
     ,[PersonalEmail]  
     ,[OtherEmail]  
     ,[Addr3]      
     ,IsInactive  
  FROM  EmergencyContact       
  WHERE CustomerID =@CustomerID        
 AND IsDeleted='false'    
 AND EmergencyContactID = case when @EmergencyContactID <>0 then @EmergencyContactID else EmergencyContactID end 
 AND EmergencyContactCD = case when Ltrim(Rtrim(@EmergencyContactCD)) <> '' then @EmergencyContactCD else EmergencyContactCD end 
 order by  EmergencyContactCD    
END  
  
GO


