/****** Object:  StoredProcedure [dbo].[spGetLocatorVendor]    Script Date: 01/29/2013 18:05:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[spGetLocatorVendor]
(
 @CustomerID BIGINT, 
 @AirportID BIGINT,
 @IcaoID char(4), 
 @Iata char(3),
 @StateName varchar(25),
 @CityName varchar(25),
 @CountryCD char(3),
 @CountryID BIGINT, 
 @MetroID BIGINT, 
 @MetroCD char(3),
 @Name varchar(40),
 @MilesFrom int,
 @AircraftID bigint,
 @AircraftCD varchar(10)
 

 )  
AS  
-- =============================================  
-- Author: 
-- Create date:   
-- Description: Get the Locator information of Airport 
-- =============================================  
SET NOCOUNT ON  
        if  @IcaoID ='' 
		set @IcaoID = null
		 if @Iata ='' 
		set @Iata = null
		 if @CountryCD ='' 
		set @CountryCD = null
		 if @CityName ='' 
		set @CityName = null
		 if @MetroCD ='' 
		set @MetroCD = null
		 if @StateName ='' 
		set @StateName = null
		 if @Name ='' 
		set @Name = null
		if @AircraftCD ='' 
		set @AircraftCD = null	


SELECT   A.[AirportID]
        ,A.[IcaoID]
        ,A.[Iata]
        , v.[CustomerID]
		,v.BillingCity
		,v.BillingState			
		, v.[CountryID]
		,v.Name			
		, v.[MetroID]
		,M.MetroCD
		,C.CountryCD
		,0 as 'Miles'
		,V.BusinessPhone
		-- Aircraft
		
			
	 FROM  [Vendor] V
	  LEFT OUTER JOIN Airport A on V.AirportID = A.AirportID
    LEFT OUTER JOIN [metro] M on V.MetroID = M.MetroID    
    LEFT OUTER JOIN [Country] C on V.CountryID = C.CountryID  
    --LEFT OUTER JOIN [Airport] A on V.AirportID = A.AirportID  
    where
    --LEFT OUTER JOIN [Aircraft] Ar on V.airc 
    ((V.CustomerID = @CustomerID) 
  -- OR (ISNULL(@AirportID,0))=0 OR A.AirportID = @AirportID) and
  OR (A.AirportID = @AirportID and V.CustomerID = @CustomerID)) and
	ISNULL(A.IcaoID, '') = case when @IcaoID is not null then @IcaoID else ISNULL(A.IcaoID, '') end and
   ISNULL(A.Iata, '') = case when @Iata is not null then @Iata else ISNULL(A.Iata, '') end and
   ISNULL(C.CountryCD, '') = case when @CountryCD  is not null then @CountryCD  else ISNULL(C.CountryCD , '') end  and
   ISNULL(M.MetroCD, '') = case when @MetroCD is not null then @MetroCD else ISNULL(M.MetroCD, '') end and
   ISNULL(V.BillingCity, '') = case when @CityName is not null then @CityName else ISNULL(V.BillingCity, '') end and
   ISNULL(v.BillingState, '') = case when @StateName is not null then @StateName else ISNULL(v.BillingState, '') end and
   ISNULL(V.Name, '') = case when @Name is not null then @Name else ISNULL(V.Name, '') end and
	      --  (V.BillingCity = @CityName   OR @CityName is NULL Or   @CityName = '')  and
    	  --(v.BillingState = @StateName   OR @StateName is NULL Or   @StateName = '')  and
    	  --  (V.Name = @Name   OR @Name is NULL Or   @Name = '')  and
     
	   V.IsDeleted = 0 and  V.VendorType= 'V'
 


