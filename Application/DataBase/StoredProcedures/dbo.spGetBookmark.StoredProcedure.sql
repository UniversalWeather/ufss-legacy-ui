
GO
/****** Object:  StoredProcedure [dbo].[spGetBookmark]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetBookmark]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetBookmark]
GO

CREATE PROCEDURE spGetBookmark
(@CustomerID bigint
,@UserName varchar(30)
,@CategoryName varchar(100))
AS BEGIN

	SET NOCOUNT OFF;
	
	SELECT	UserPreferenceID,
			CustomerID,
			UserName,
			CategoryName,
			SubCategoryName,
			KeyName,
			KeyValue,
			LastUpdTS
	FROM	UserPreference
	WHERE	CustomerID = @CustomerID
		AND	UserName = @UserName
		AND CategoryName = @CategoryName

END