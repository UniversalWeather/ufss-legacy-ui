IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTripManagerCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTripManagerCheckList]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllTripManagerCheckList]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetAllTripManagerCheckList](@CustomerID bigint)
as
-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 13/5/2012
-- Description: Get the TripManagerCheckList information
-- =============================================
set nocount on

begin 
	SELECT   TripManagerCheckList.[CheckListID]
      ,TripManagerCheckList.[CustomerID]
      ,TripManagerCheckList.[CheckListCD]
      ,TripManagerCheckList.[CheckListDescription]
      ,TripManagerCheckList.[CheckGroupID]
      ,TripManagerCheckList.[LastUpdUID]
      ,TripManagerCheckList.[LastUpdTS]
      ,TripManagerCheckList.[IsInactive]
      ,TripManagerCheckList.[IsDeleted]
      ,ChecklistGroup.CheckGroupCD as CheckGroupCD
      
	FROM  [TripManagerCheckList] as TripManagerCheckList
	LEFT OUTER JOIN TripManagerCheckListGroup as ChecklistGroup on ChecklistGroup.CheckGroupID = TripManagerCheckList.CheckGroupID
	
	
	 WHERE TripManagerCheckList.CustomerID=@CustomerID  and TripManagerCheckList.IsDeleted = 'false'
	 order by CheckListCD
end
GO
