/****** Object:  StoredProcedure [dbo].[spGetSalesPersonID]    Script Date: 02/25/2013 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetSalesPersonID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetSalesPersonID]
GO
-- =============================================    
-- Author: Ramesh. J  
-- Create date: 25/02/2013    
-- Description: Get Lastly SAVED SalesPersonID  
-- =============================================    
Create Procedure [dbo].[spGetSalesPersonID]  
                ( @CustomerID BIGINT,  
                  @UserName char(30)  
                 )    
AS    
SET NOCOUNT ON    
    
SELECT Top 1 SalesPersonID FROM SalesPerson WHERE CustomerID=@CustomerID AND  
                                                      LastUpdUID=@UserName   
                                                      ORDER BY SalesPersonID DESC  