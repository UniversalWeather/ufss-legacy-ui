/****** Object:  StoredProcedure [dbo].[spGetAccounts]    Script Date: 12/05/2013 14:51:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAccounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAccounts]
GO

/****** Object:  StoredProcedure [dbo].[spGetAccounts]    Script Date: 12/05/2013 14:51:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAccounts](@CustomerID  BIGINT)          
AS        
-- =============================================          
-- Author:Prabhu          
-- Create date: 12/4/2012          
-- Description: Get the Account information          
-- =============================================          
SET NOCOUNT ON           
IF LEN(@CustomerID) >0          
BEGIN           
 SELECT Account.AccountID      
      ,CASE WHEN [AccountNum] IS NULL THEN '' ELSE LTRIM(RTRIM(Account.AccountNum)) END AS AccountNum     
      ,Account.CustomerID      
      ,Account.AccountDescription      
      ,Account.CorpAccountNum      
      ,Account.HomebaseID      
      ,Account.IsBilling      
      ,Account.LastUpdUID      
      ,Account.LastUpdTS      
      ,Account.Cost      
      ,Account.IsInActive      
      ,Account.IsDeleted      
      ,Airport.ICAOID AS HomebaseCD      
      ,Company.BaseDescription      
  FROM Account      
  LEFT OUTER JOIN Company ON Company.HomebaseID = Account.HomebaseID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID      
  WHERE Account.CustomerID =@CustomerID   AND Account.IsDeleted = 'false' 
  order by Account.AccountNum         
END

GO

