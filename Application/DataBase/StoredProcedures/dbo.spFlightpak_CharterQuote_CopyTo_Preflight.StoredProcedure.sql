
/****** Object:  StoredProcedure [dbo].[spFlightpak_CharterQuote_CopyTo_Preflight]    Script Date: 01/21/2014 14:42:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CharterQuote_CopyTo_Preflight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CharterQuote_CopyTo_Preflight]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_CharterQuote_CopyTo_Preflight]    Script Date: 01/21/2014 14:42:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Modified Date : 23-04-2013 (SIGN : MR23042013)  
-- Modified Details : Estimated Trip Date should be the Departure Date of the first Leg.  
-- Modified Date: 15-01-2014
-- Defect: 2240: Auto Dispatch Number missing.Auto Dispatch Number missing.See attached & File 49, Quote 1. Check CP for Auto Dispatch No. setting, created quote, transferred as Live Trip to Preflight, yet no dispatch number is assigned per CP setting. See FPK functionality.
-- Defect: 2535 : When the quote is made a Live Trip, the Trip Description from CQ should auto populate the Preflight Trip Purpose and Leg Purposes
-- Defect: 2664: When the quote is moved to preflight fbo details are not populating correctly.
-- =============================================  
    
CREATE PROCEDURE  [dbo].[spFlightpak_CharterQuote_CopyTo_Preflight]         
 -- Add the parameters for the stored procedure here        
 @CQFileID Bigint,             
 @CQMainID Bigint,  
 @username varchar(30),    
 @CustomerID Bigint,  
 @IsAutoDispatch bit,  
 @TripStatus varchar(1),  
 @IsRevision bit        
AS        
BEGIN        
 DECLARE  @NewTripID BIGINT         
 DECLARE  @OldTripID BIGINT   
 DECLARE  @NewTripNUM BIGINT   
 DECLARE  @HomebaseID BIGINT   
 DECLARE  @CQCustomerID BIGINT  
 DECLARE  @EstDeptDate DATETIME  
 DECLARE  @IsAutoDispatchNum BIT
 
 --Defect :2535     
 DECLARE  @TripDescription varchar(40)
 
 set @IsAutoDispatchNum =0
 set @TripDescription = ''
   
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripID OUTPUT          
 set @NewTripNUM = 0      
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @NewTripNUM OUTPUT        
      
 select @NewTripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID      
 
 --Defect :2535           
 select @EstDeptDate = EstDepartureDT, @HomebaseID= HomebaseID,@CQCustomerID = CQCustomerID, @TripDescription = CQFileDescription   from CQFile where CQFileID = @CQFileID   
 select @OldTripID = TripID from CQMain where CQMainID = @CQMainID  
 set @OldTripID = ISNULL(@OldTripID,0)  
   
 Declare @UserHomebase bigint  
 select @UserHomebase = HomebaseID from UserMaster where UserName = @username  
 Declare @flightCategoryID bigint  
   
Declare @PaxFlightCategoryID bigint  
Declare @NoPaxFlightCategoryID bigint  
  
Declare @CQDepartmentID bigint  
  
  
select  @PaxFlightCategoryID = pf.FlightCategoryID, @NoPaxFlightCategoryID =  NPF.FlightCategoryID, @CQDepartmentID = c.DepartmentID,
		@IsAutoDispatchNum =   c.IsAutomaticDispatchNum
 from Company c  
left join  FlightCatagory PF on c.FlightCatagoryPassenger = pf.FlightCatagoryCD  
left join FlightCatagory NPF on c.FlightCatagoryPassenger = npf.FlightCatagoryCD  
where HomebaseID = @UserHomebase  
  
if (@CQDepartmentID =0)  
 begin  
 set @CQDepartmentID = null  
 end  
   
  if exists (select * from CQLeg, CQPassenger where CQMainID = @CQMainID and CQLeg.CQLegID = CQPassenger.CQLegID)  
 BEGIN  
 set @flightCategoryID = @PaxFlightCategoryID  
 END  
 ELSE  
 BEGIN  
 set @flightCategoryID = @NoPaxFlightCategoryID  
 END  
   
 --//  MR23042013 - Added for updating Estimated Depart Date from First Leg at CQLeg table  
 IF EXISTS (select * from CQLeg WITH(NOLOCK) where CQMainID = @CQMainID AND ISNULL(IsDeleted,0) = 0)  
 BEGIN  
 SELECT @EstDeptDate = DepartureDTTMLocal   
  FROM CQLeg WITH(NOLOCK)  
 WHERE CQMainID = @CQMainID AND ISNULL(IsDeleted,0) = 0 AND LegNUM = 1  
 END  
 -- // MR23042013 - END  
    
 --PreflightMain       
  insert into PreflightMain      
  ([TripID]      
   ,[CustomerID]      
   ,[TripNUM]      
   ,[PreviousNUM]      
   ,[TripDescription]      
   ,[FleetID]      
   ,[DispatchNUM]      
   ,[TripStatus]      
   ,[EstDepartureDT]      
   ,[EstArrivalDT]      
   ,[RecordType]      
   ,[AircraftID]      
   ,[PassengerRequestorID]      
   ,[RequestorFirstName]      
   ,[RequestorMiddleName]      
   ,[RequestorLastName]      
   ,[RequestorPhoneNUM]      
   ,[DepartmentDescription]      
   ,[AuthorizationDescription]      
   ,[RequestDT]      
   ,[IsCrew]      
   ,[IsPassenger]      
   ,[IsQuote]      
   ,[IsScheduledServices]      
   ,[IsAlert]      
   ,[Notes]      
   ,[LogisticsHistory]      
   ,[HomebaseID]      
   ,[ClientID]      
   ,[IsAirportAlert]      
   ,[IsLog]      
   ,[BeginningGMTDT]      
   ,[EndingGMTDT]      
   ,[LastUpdUID]      
   ,[LastUpdTS]      
   ,[IsPrivate]      
   ,[IsCorpReq]      
   ,[Acknowledge]      
   ,[DispatchNotes]      
   ,[TripRequest]      
   ,[WaitList]      
   ,[FlightNUM]      
   ,[FlightCost]      
   ,[AccountID]      
   ,[TripException]      
   ,[TripSheetNotes]      
   ,[CancelDescription]      
   ,[IsCompleted]      
   ,[IsFBOUpdFlag]      
   ,[RevisionNUM]      
   ,[RevisionDescriptioin]      
   ,[IsPersonal]      
   ,[DsptnName]      
   ,[ScheduleCalendarHistory]      
   ,[ScheduleCalendarUpdate]      
   ,[CommentsMessage]      
   ,[EstFuelQTY]      
   ,[CrewID]      
   ,[ReleasedBy]      
   ,[IsDeleted]      
   ,[VerifyNUM]      
   ,[APISSubmit]      
   ,[APISStatus]      
   ,[APISException]      
   ,[IsAPISValid]      
   ,[DepartmentID]      
   ,[AuthorizationID]      
   ,[DispatcherUserName]      
   ,[EmergencyContactID]  
   ,[CQCustomerID]  
   )             
           
    select  
       @NewTripID    
   ,@CustomerID      
   ,@NewTripNUM      
   ,0 --[PreviousNUM]    
   ,@TripDescription --[Comments] Defect :2535     
   ,[FleetID]      
   ,case when @IsAutoDispatchNum=0 THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END      
   ,@TripStatus      
   ,@EstDeptDate      
   ,null   --EstArrivalDT   
   ,'T'  
   ,[AircraftID]      
   ,[PassengerRequestorID]      
   ,null --[RequestorFirstName]      
   ,null --[RequestorMiddleName]      
   ,null --[RequestorLastName]      
   ,null --[RequestorPhoneNUM]      
   ,null --[DepartmentDescription]      
   ,null --[AuthorizationDescription]      
   ,null --[RequestDT]      
   ,null --[IsCrew]      
   ,null --[IsPassenger]      
   ,1  
   ,null --[IsScheduledServices]      
   ,null --[IsAlert]      
   ,[Comments]  
   ,null--[LogisticsHistory]      
   ,@HomebaseID      
   ,null--[ClientID]      
   ,null--[IsAirportAlert]      
   ,null--[IsLog]      
   ,null--[BeginningGMTDT]      
   ,null--[EndingGMTDT]      
   ,@username  
   ,GETUTCDATE()  
   ,0 --[IsPrivate]      
   ,0 --[IsCorpReq]      
   ,0 --[Acknowledge]      
   ,null --[DispatchNotes]      
   ,null --[TripRequest]      
   ,null-- [WaitList]      
   ,null--[FlightNUM]      
   ,[QuoteTotal] --[FlightCost]      
   ,null--[AccountID]      
   ,null-- [TripException]      
   ,null-- [TripSheetNotes]      
   ,null-- [CancelDescription]      
   ,null--[IsCompleted]      
   ,null--[IsFBOUpdFlag]      
   ,case when @IsRevision=0  then null  else 1 end           
   ,null--[RevisionDescriptioin]      
   ,null--[IsPersonal]      
   ,null--[DsptnName]      
   ,null--[ScheduleCalendarHistory]      
   ,null--[ScheduleCalendarUpdate]      
   ,null--[CommentsMessage]      
   ,0 --[EstFuelQTY]      
   ,null--[CrewID]      
   ,null--[ReleasedBy]      
   ,0 --[IsDeleted]      
   ,null--[VerifyNUM]      
   ,null--[APISSubmit]      
   ,null--[APISStatus]      
   ,null--[APISException]      
   ,null--[IsAPISValid]      
   ,@CQDepartmentID--[DepartmentID]      
   ,null--[AuthorizationID]      
   ,case when @IsAutoDispatch=0 THEN  NULL ELSE @username END  --[DispatcherUserName] Defect :2240
   ,null--[EmergencyContactID]  
   ,@CQCustomerID  
  from CQMain CM  
    where   
      CM.CQMainID =@CQMainID   
      
      
  --PreflightLeg           
  CREATE Table #TempLeg (        
  rownum int identity (1,1),        
  NewLegId Bigint,        
  OldLegID Bigint,  
  
  )        
          
  insert into #TempLeg (NewLegId,OldLegID) select null, CQLegID from   
  CQLeg CQL  
  where   
 CQL.CQMainID = @CQMainID  
    
  Declare @Rownumber int       
  DECLARE  @NewLegID BIGINT            
  set @Rownumber = 1        
          
  while (select COUNT(1) from #TempLeg where NewLegId is null) >0        
  begin        
   Set @NewLegID =0        
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewLegID OUTPUT         
   update #TempLeg set NewLegID = @NewLegID where rownum = @Rownumber        
   set @Rownumber = @Rownumber +1         
  End        
         
         
--Legs        
  INSERT INTO [PreflightLeg]  
         ([LegID]      
    ,[TripID]      
    ,[CustomerID]      
    ,[TripNUM]      
    ,[LegNUM]      
    ,[DepartICAOID]      
    ,[ArriveICAOID]      
    ,[Distance]      
    ,[PowerSetting]      
    ,[TakeoffBIAS]      
    ,[LandingBias]      
    ,[TrueAirSpeed]      
    ,[WindsBoeingTable]      
    ,[ElapseTM]      
    ,[IsDepartureConfirmed]      
    ,[IsArrivalConfirmation]      
    ,[IsApproxTM]      
    ,[IsScheduledServices]      
    ,[DepartureDTTMLocal]      
    ,[ArrivalDTTMLocal]      
    ,[DepartureGreenwichDTTM]      
    ,[ArrivalGreenwichDTTM]      
    ,[HomeDepartureDTTM]      
    ,[HomeArrivalDTTM]      
    ,[GoTime]      
    ,[FBOID]      
    ,[LogBreak]      
    ,[FlightPurpose]      
    ,[PassengerRequestorID]      
    ,[RequestorName]      
    ,[DepartmentID]      
    ,[DepartmentName]      
    ,[AuthorizationID]      
    ,[AuthorizationName]      
    ,[PassengerTotal]     
    ,[SeatTotal]      
    ,[ReservationTotal]     
    ,[ReservationAvailable]     
    ,[WaitNUM]      
    ,[DutyTYPE]      
    ,[DutyHours]      
    ,[RestHours]      
    ,[FlightHours]      
    ,[Notes]      
    ,[FuelLoad]      
    ,[OutbountInstruction]      
    ,[DutyTYPE1]      
    ,[IsCrewDiscount]      
    ,[CrewDutyRulesID]      
    ,[IsDutyEnd]      
    ,[FedAviationRegNUM]      
    ,[ClientID]      
    ,[LegID1]      
    ,[WindReliability]      
    ,[OverrideValue]      
    ,[CheckGroup]      
    ,[AdditionalCrew]      
    ,[PilotInCommand]      
    ,[SecondInCommand]      
    ,[NextLocalDTTM]      
    ,[NextGMTDTTM]      
    ,[NextHomeDTTM]      
    ,[LastUpdUID]      
    ,[LastUpdTS]      
    ,[CrewNotes]      
    ,[IsPrivate]      
    ,[WaitList]      
    ,[FlightNUM]      
    ,[FlightCost]      
    ,[CrewFuelLoad]      
    ,[IsDeleted]      
    ,[UWAID]      
    ,[USCrossing]      
    ,[Response]      
    ,[ConfirmID]      
    ,[CrewDutyAlert]      
    ,[AccountID]      
    ,[FlightCategoryID]         
    ,[CQCustomerID]  
    )            
           
    select  
       NewLegId      
    ,@NewTripID      
    ,@CustomerID      
    ,null--[TripNUM]      
    ,[LegNUM]      
    , [DAirportID]  
    ,[AAirportID]      
    ,[Distance]      
    ,[PowerSetting]      
    ,[TakeoffBIAS]      
    ,[LandingBias]      
    ,[TrueAirSpeed]      
    ,[WindsBoeingTable]      
    ,[ElapseTM]      
    ,[IsDepartureConfirmed]      
    ,[IsArrivalConfirmation]      
    ,0 --[IsApproxTM]      
    ,0--[IsScheduledServices]      
    ,[DepartureDTTMLocal]      
    ,[ArrivalDTTMLocal]      
    ,[DepartureGreenwichDTTM]      
    ,[ArrivalGreenwichDTTM]      
    ,[HomeDepartureDTTM]      
    ,[HomeArrivalDTTM]      
    ,null --[GoTime]      
    ,null --[FBOID]      
    ,null --[LogBreak]      
    ,@TripDescription --[FlightPurposeDescription]      Defect :2535
    ,null--[PassengerRequestorID]      
    ,null--[RequestorName]      
    ,null--[DepartmentID]      
    ,null--[DepartmentName]      
    ,null--[AuthorizationID]      
,null--[AuthorizationName]      
    ,[PassengerTotal]     
    ,0--[SeatTotal]      
    ,0--[ReservationTotal]     
    ,[ReservationAvailable]     
    ,null--[WaitNUM]      
    ,null --[DutyTYPE]      
    ,[DutyHours]      
    ,[RestHours]      
    ,[FlightHours]      
    ,[Notes]      
    ,0--[FuelLoad]      
    ,null--[OutbountInstruction]      
    ,[DutyTYPE] --   [DutyTYPE1]  
    ,0--[IsCrewDiscount]      
    ,[CrewDutyRulesID]      
    ,[IsDutyEnd]      
    ,[FedAviationRegNUM]      
    ,[ClientID]      
    ,null --[LegID1]      
    ,[WindReliability]      
    ,CQOverRide --[override]      
    , case when isnull(DutyTYPE,1)=1 then 'DOM' else 'INT'  end --[CheckGroup]      
    ,null--[AdditionalCrew]      
    ,null--[PilotInCommand]      
    ,null--[SecondInCommand]      
    ,[NextLocalDTTM]      
    ,[NextGMTDTTM]      
    ,[NextHomeDTTM]      
    ,[LastUpdUID]      
    ,[LastUpdTS]      
    ,[CrewNotes]      
    ,0 --[IsPrivate]      
    ,null--[WaitList]      
    ,null--[FlightNUM]      
    ,ChargeTotal   -- Flightcost  
    ,null--[CrewFuelLoad]      
    ,[IsDeleted]      
    ,null--[UWAID]      
    ,null--[USCrossing]      
    ,null--[Response]      
    ,null--[ConfirmID]      
    ,[CrewDutyAlert]      
    ,null--[AccountID]      
    ,@flightCategoryID  
    ,@CQCustomerID      
    from CQLeg CL, #TempLeg T        
    where CL.CQLegID = T.OldLegID    
    
  CREATE Table #TempPax (      
  rownum int identity (1,1),      
  NewCQPassengerID Bigint,      
  oldCQPassengerID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )  
        
  insert into #TempPax (NewCQPassengerID ,      
   oldCQPassengerID ,      
   NewLegId ,      
   OldLegID )      
  select null,  [CQPassengerID], NewLegId, OldLegID      
  from CQPassenger PP, #TempLeg TL      
  where PP.CQLegID = TL.OldLegID      
  DECLARE  @NewCQPassengerID BIGINT         
  set @Rownumber = 1      
  while (select COUNT(1) from #TempPax where NewCQPassengerID is null) >0      
  begin      
   Set @NewCQPassengerID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewCQPassengerID OUTPUT       
   update #TempPax set NewCQPassengerID = @NewCQPassengerID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
  INSERT INTO [PreflightPassengerList]      
           (      
            [PreflightPassengerListID]      
           ,[LegID]      
           ,[CustomerID]      
           ,[PassengerID]      
           ,[PassportID]      
           ,[VisaID]      
           ,[HotelName]      
           ,[PassengerFirstName]      
           ,[PassengerMiddleName]      
           ,[PassengerLastName]      
           ,[FlightPurposeID]      
           ,[PassportNUM]      
           ,[Billing]      
           ,[IsNonPassenger]      
           ,[IsBlocked]      
           ,[OrderNUM]      
           ,[ReservationNUM]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[WaitList]      
           ,[AssociatedPassenger]      
           ,[TripStatus]      
           ,[TSADTTM]      
           ,[IsDeleted]      
           ,[NoofRooms]      
           ,[ClubCard]      
           ,[IsRateCap]      
           ,[MaxCapAmount]      
         ,[Street]      
           ,[CityName]      
           ,[StateName]      
           ,[PostalZipCD]      
           )      
     Select      
           
           TP.NewCQPassengerID      
           ,TP.NewLegId      
           ,[CustomerID]      
           ,PassengerRequestorID      
           ,[PassportID]      
           ,null --[VisaID]      
           ,null --[HotelName]      
           ,LEFT(PassengerName,60) -- [PassengerFirstName]      
           ,NULL --[PassengerMiddleName]      
           ,NULL --[PassengerLastName]      
           ,[FlightPurposeID]      
           ,NULL-- [PassportNUM]      
           ,[Billing]      
           ,0 --[IsNonPassenger]      
           ,0--[IsBlocked]      
           ,1 --[OrderNUM]      
           ,NULL--[ReservationNUM]      
           ,@username  
           ,GETUTCDATE()  
           ,NULL --[WaitList]      
           ,NULL --[AssociatedPassenger]      
           ,NULL --[TripStatus]      
           ,NULL-- [TSADTTM]      
           ,[IsDeleted]      
           ,0 --[NoofRooms]      
           ,NULL --[ClubCard]      
           ,NULL--[IsRateCap]      
           ,NULL --[MaxCapAmount]      
           ,NULL --[Street]      
           ,NULL --[CityName]      
           ,NULL --[StateName]      
           ,NULL --[PostalZipCD]      
                 
           from CQPassenger  PP, #TempPax TP      
     where PP.CQPassengerID = TP.oldCQPassengerID     
        
    
  --Fbo      
  CREATE Table #TempFBO (      
  rownum int identity (1,1),      
  NewPreflightFBOID Bigint,      
  oldCQFBOListID Bigint,      
  NewLegId Bigint,      
  )      
        
  insert into #TempFBO (NewPreflightFBOID ,      
   oldCQFBOListID ,      
   NewLegId       
 )      
  select null,  CQFBOListID, NewLegId  
  from CQFBOList PF, #TempLeg TL      
  where PF.cqlegID = TL.OldLegID      
        
  DECLARE  @NewPreflightFBOID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempFBO where NewPreflightFBOID is null) >0      
  begin      
   Set @NewPreflightFBOID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightFBOID OUTPUT       
   update #TempFBO set NewPreflightFBOID = @NewPreflightFBOID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [dbo].[PreflightFBOList]      
           ([PreflightFBOID]      
           ,[PreflightFBOName]      
           ,[LegID]      
           ,[FBOID]      
           ,[IsArrivalFBO]      
           ,[IsDepartureFBO]      
           ,[AirportID]      
           ,[Street]      
           ,[CityName]      
           ,[StateName]      
           ,[PostalZipCD]      
           ,[PhoneNum1]      
           ,[PhoneNum2]      
           ,[PhoneNum3]      
           ,[PhoneNum4]      
           ,[FaxNUM]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[IsDeleted]      
           ,[NoUpdated]      
           ,[FBOArrangedBy]      
           ,[IsCompleted]      
           ,[FBOInformation]      
           ,[ConfirmationStatus]      
           ,[Comments]      
           ,[CustomerID]      
           )      
     Select      
           TF.NewPreflightFBOID      
           ,F.FBOVendor--null --[PreflightFBOName]      
           ,TF.NewLegId      
           ,PF.[FBOID]      
           ,case when PF.RecordType='FA' then  1 else 0 end --[IsArriveFBO]  
           ,case when PF.RecordType='FD' then  1 else 0 end --[IsDepartureFBO]      
           ,PF.[AirportID]      
           ,null --[Street]      
           ,F.CityName -- null --[CityName]      
           ,F.StateName -- null --[StateName]      
           ,F.PostalZipCD --null --[PostalZipCD]      
           ,F.PhoneNUM1--PF.PhoneNUM      
           ,F.PhoneNUM2 --  null --[PhoneNum2]      
           ,null --[PhoneNum3]      
           ,null --[PhoneNum4]      
           ,F.FaxNum --null --[FaxNUM]      
           ,@username  
           ,GETUTCDATE()     
           ,PF.[IsDeleted]      
           ,null --[NoUpdated]      
           ,null --[FBOArrangedBy]      
           ,null --[IsCompleted]      
           ,left(isnull(F.fbovendor,'') + ','  + isnull(F.cityname,'')+ ','  + isnull(F.statename,'')+ ','  + isnull(F.postalzipcd,'')+ ','  +
 isnull(F.phonenum1,'')+ ','  + isnull(F.phonenum2,'')+ ','  + isnull(F.faxnum,'')+ ','  +isnull(F.unicom,'')+ ','  +
 isnull(F.arinc,'')+ ','  + isnull(F.addr1,'')+ ','  + isnull(F.addr2,'')+ ','  +isnull(F.addr3,'')+ ','  +isnull(F.emailaddress,''), 1000)--null --[FBOInformation]      
           ,null --[ConfirmationStatus]      
           ,null --[Comments]      
           ,@CustomerID  
                 
           from CQFBOList PF, #TempFBO TF, Fbo F
     where PF.CQFBOListID = TF.oldCQFBOListID   
     and PF.FBOID = F.FBOID   
           
  Drop table #TempFBO      
        
  --Catering        
  CREATE Table #TempCatering (      
  rownum int identity (1,1),      
  NewPreflightCateringID Bigint,      
  oldCQCateringListID Bigint,      
  NewLegId Bigint     
  )      
        
  insert into #TempCatering (NewPreflightCateringID ,      
   oldCQCateringListID ,      
   NewLegId )      
  select null,  CQCateringListID, NewLegId      
  from CQCateringList PC, #TempLeg TL      
  where PC.CQLegID = TL.OldLegID      
        
  DECLARE  @NewPreflightCateringID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempCatering where NewPreflightCateringID is null) >0      
  begin      
   Set @NewPreflightCateringID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCateringID OUTPUT       
   update #TempCatering set NewPreflightCateringID = @NewPreflightCateringID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [dbo].[PreflightCateringDetail]      
           ([PreflightCateringID]      
           ,[LegID]      
           ,[CustomerID]      
           ,[AirportID]      
           ,[ArriveDepart]      
           ,[CateringID]      
           ,[IsCompleted]      
           ,[Cost]      
           ,[ContactName]      
           ,[ContactPhone]      
           ,[ContactFax]      
           ,[CateringConfirmation]      
           ,[CateringComments]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[IsDeleted]      
           ,[IsUWAArranger]      
           )      
     Select      
                 
    TC.NewPreflightCateringID      
           ,TC.NewLegId      
           ,@CustomerID      
           ,[AirportID]      
           ,case when RecordType='CM' then  'D' else 'A' end --[ArriveDepart]      
           ,[CateringID]      
           ,null--[IsCompleted]      
           ,PC.Rate --null--[Cost]      
           ,PC.CQCateringListDescription-- null--[ContactName]      
           ,PC.PhoneNUM  
           ,PC.FaxNUM--null--[ContactFax]      
           ,null--[CateringConfirmation]      
           ,null--[CateringComments]      
           ,@username  
           ,GETUTCDATE()   
           ,[IsDeleted]      
           ,null--[IsUWAArranger]   
                 
           from [CQCateringList] PC, #TempCatering TC      
     where PC.CQCateringListID = TC.oldCQCateringListID      
        
  Drop table #TempCatering  
    
  Drop table #TempLeg  
     
 if @OldTripID <>0  
 begin  
  update PreflightMain set IsDeleted =1 where TripID = @OldTripID  
 end  
      
    update CQMain set TripID =@NewTripID, LiveTrip = @TripStatus  where CQMainID = @CQMainID  
 select @NewTripID as TripID        
        
    
END      
  
      
    
  



GO


