


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceQuoteDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceQuoteDetail]
(
	@CQInvoiceQuoteDetailID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQInvoiceQuoteDetail] WHERE [CQInvoiceQuoteDetailID] = @CQInvoiceQuoteDetailID AND [CustomerID] = @CustomerID
GO


