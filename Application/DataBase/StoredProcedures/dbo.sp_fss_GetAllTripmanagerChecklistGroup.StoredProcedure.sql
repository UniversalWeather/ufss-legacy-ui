/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllTripmanagerChecklistGroup]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllTripmanagerChecklistGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllTripmanagerChecklistGroup]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllTripmanagerChecklistGroup]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create  procedure [dbo].[sp_fss_GetAllTripmanagerChecklistGroup](@CustomerID bigint,  
@CheckGroupID bigint,  
@CheckGroupCD char(3),  
@FetchActiveOnly BIT=0)    
as    
-- =============================================    
-- Author:Karthikeyan.S  
-- Create date: 07/03/2014  
-- Description: Get the TripManagerCheckListGroup information   
-- EXEC sp_fss_getallTripmanagerChecklistGroup 10003,0,'',1  
-- =============================================    
set nocount on    
    
begin     
 SELECT  CheckGroupID       
         ,CustomerID     
         ,CheckGroupCD     
         ,CheckGroupDescription     
         ,LastUpdUID     
         ,LastUpdTS     
         ,IsDeleted    
         ,IsInactive    
 FROM  [TripManagerCheckListGroup] WHERE CustomerID=@CustomerID  
AND ISNULL(CheckGroupID,0) = CASE when (@CheckGroupID <> 0) then @CheckGroupID ELSE CheckGroupID END  
AND ISNULL(CheckGroupCD,'') = CASE when (LTRIM(RTRIM(@CheckGroupCD)) <> '') then @CheckGroupCD ELSE ISNULL(CheckGroupCD,'') END  
AND ISNULL(IsInActive,0) = case when @FetchActiveOnly = 0 then ISNULL(IsInActive,0) else 0 end 
AND ISNULL(IsDeleted,0) = 0     
 order by CheckGroupCD    
end 
GO

