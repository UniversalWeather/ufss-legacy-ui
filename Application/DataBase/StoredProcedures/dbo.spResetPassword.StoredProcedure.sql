/****** Object:  StoredProcedure [dbo].[spResetPassword]    Script Date: 08/29/2012 16:50:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spResetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spResetPassword]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROC [dbo].[spResetPassword]            
@CustomerId bigint,            
@EmailId   varchar(100),            
@Password nvarchar(500),            
@LastUpdUID char(30)            
AS             
BEGIN            
 DECLARE @UserName varchar(30)        
 SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId        
 DECLARE  @UserPasswordID BIGINT            
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @UserPasswordID OUTPUT            
             
 --Insert New password            
 INSERT INTO UserPassword     
  (UserPasswordID,UserName,FPPWD,ActiveStartDT,ActiveEndDT,PWDStatus,LastUpdUID,LastUpdTS)    
  VALUES(@UserPasswordID, @EmailId, @Password, GETUTCDATE(), DATEADD(day,1,GETUTCDATE()), 1, @LastUpdUID, GETUTCDATE())            
            
 -- Set other passwords inactive for the user except current            
 UPDATE UserPassword SET ActiveEndDT = GETUTCDATE(), PWDStatus = 0, LastUpdUID = @LastUpdUID, LastUpdTS = GETUTCDATE() WHERE UserName = @EmailId AND             
 UserPasswordID <> @UserPasswordID            
           
 -- Set ForceChangePassword flag to true          
 UPDATE UserMaster SET ForceChangePassword = 1, ForceChangeSecurityHint = 1, InvalidLoginCount = 0 Where UserName = @EmailId          
             
END       
      
GO
