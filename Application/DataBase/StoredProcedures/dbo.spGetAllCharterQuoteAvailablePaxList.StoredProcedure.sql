
/****** Object:  StoredProcedure [dbo].[spGetAllCharterQuoteAvailablePaxList]    Script Date: 06/03/2013 17:33:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCharterQuoteAvailablePaxList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCharterQuoteAvailablePaxList]
GO


/****** Object:  StoredProcedure [dbo].[spGetAllCharterQuoteAvailablePaxList]    Script Date: 06/03/2013 17:33:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		PraKash Pandian
-- Create date: 20/07/2012
-- Description:	Get All the Passengers for Charter Quote Pax Avaialable Grid
-- =============================================
CREATE PROCEDURE [dbo].[spGetAllCharterQuoteAvailablePaxList]
	-- Add the parameters for the stored procedure here
	(
	 @CustomerID BIGINT,	
	 @ClientID bigint
	 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
		 p.PassengerRequestorID AS PassengerRequestorID
		,p.PassengerRequestorCD AS Code
		,p.PassengerName AS Name
		,p.DateOfBirth AS DateOfBirth	
		,f.FlightPurposeDescription AS FlightPurpose
		,(select top(1) PassportNum from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS Passport
		,p.StandardBilling AS BillingCode
		,((isnull(p.Addr1,'''')) + (isnull(p.Addr2,'''')))  as Street
		,p.PostalZipCD as Postal
		,p.City as City
		,p.StateName as [State]
		,(select top(1) PassportID from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportID
		,'''' as Leg
		,Convert(varchar(max),p.Notes) as Notes
		,Convert(varchar(max),p.PassengerAlert) as PassengerAlert
		,(select top(1) VisaID from CrewPassengerVisa where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0) AS VisaID
		,(select top(1) PassportExpiryDT from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportExpiryDT
		,(select top(1) Country.CountryCD from CrewPassengerPassport LEFT OUTER JOIN Country  on CrewPassengerPassport.CountryID = Country.CountryID where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(CrewPassengerPassport.IsDeleted,0) =0 and CrewPassengerPassport.Choice=1) AS Nation
		,1 as OrderNUM
	FROM Passenger p 
					--LEFT OUTER JOIN CQPassenger cqp on p.PassengerRequestorID=cqp.PassengerRequestorID AND ISNULL(cqp.IsDeleted,0) =0 
					--and cqp.Billing is not null and p.PassengerName is not null
					 LEFT OUTER JOIN FlightPurpose f on p.FlightPurposeID=f.FlightPurposeID   AND ISNULL(f.IsDeleted,0) =0 and f.FlightPurposeID is not null
					 
	WHERE p.CustomerID = @CustomerID
	AND ISNULL(p.ClientID, 0) =  ISNULL(@ClientID, ISNULL(p.ClientID,0)) 
	 AND ISNULL(p.IsDeleted,0) =0 	 
END

GO


