
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripByTripNUM]    Script Date: 09/19/2013 14:43:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetTripByTripNUM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetTripByTripNUM]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripByTripNUM]    Script Date: 09/19/2013 14:43:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spFlightPak_Preflight_GetTripByTripNUM]
(
@CustomerID BIGINT,
@TripNUM BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get Preflightmain by Trip NUM
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PM.*
FROM PreflightMain PM 
 where 
 PM.TripNUM = @TripNUM 
 
 and PM.TripID in (
	Select PM.TripID from PreflightMain PM , Preflightleg PL where 
	PL.TripID = PM.TripID 
	and PM.TripNUM = @TripNUM
	and  PL.CustomerID = @CustomerID
	)
 and PM.CustomerID=@CustomerID and PM.IsDeleted=0


 

GO


