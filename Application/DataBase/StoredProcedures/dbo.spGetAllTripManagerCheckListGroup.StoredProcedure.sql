IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTripManagerCheckListGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTripManagerCheckListGroup]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllTripManagerCheckListGroup]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetAllTripManagerCheckListGroup](@CustomerID bigint)
as
-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 24/4/2012
-- Description: Get the TripManagerCheckListGroup information
-- =============================================
set nocount on

begin 
	SELECT
	         CheckGroupID   
	        ,CustomerID 
	        ,CheckGroupCD 
			,CheckGroupDescription 
	        ,LastUpdUID 
	        ,LastUpdTS 
            ,IsDeleted
            ,IsInactive
	FROM  [TripManagerCheckListGroup] WHERE CustomerID=@CustomerID  and IsDeleted = 'false'
	order by CheckGroupCD
end
GO
