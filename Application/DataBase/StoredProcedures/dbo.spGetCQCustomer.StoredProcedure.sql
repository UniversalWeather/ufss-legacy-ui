/****** Object:  StoredProcedure [dbo].[spGetCQCustomer]    Script Date: 03/27/2013 14:46:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCQCustomer]
GO

CREATE PROCEDURE [dbo].[spGetCQCustomer]
(@CustomerID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
	
	SELECT	CQCustomer.CQCustomerID
			,CQCustomer.CustomerID
			,CQCustomer.CQCustomerCD
			,CQCustomer.CQCustomerName
			,CQCustomer.IsApplicationFiled
			,CQCustomer.IsApproved
			,CQCustomer.BillingName
			,CQCustomer.BillingAddr1
			,CQCustomer.BillingAddr2
			,CQCustomer.BillingAddr3
			,CQCustomer.BillingCity
			,CQCustomer.BillingState
			,CQCustomer.BillingZip
			,CQCustomer.CountryID
			,CQCustomer.BillingPhoneNum
			,CQCustomer.BillingFaxNum
			,CQCustomer.Notes
			,CQCustomer.Credit
			,CQCustomer.DiscountPercentage
			,CQCustomer.IsInActive
			,CQCustomer.HomebaseID
			,CQCustomer.AirportID
			,CQCustomer.DateAddedDT
			,CQCustomer.WebAddress
			,CQCustomer.EmailID
			,CQCustomer.TollFreePhone
			,CQCustomer.LastUpdUID
			,CQCustomer.LastUpdTS
			,CQCustomer.IsDeleted
			,Country.CountryCD AS CountryCD
			,Country.CountryName AS CountryName
			,Company.BaseDescription AS BaseDescription
			,HBAirport.IcaoID AS HomeBaseIcaoID
			,Airport.IcaoID AS AirportIcaoID
			,CQCustomer.CustomerType
			,CQCustomer.IntlStdCrewNum
			,CQCustomer.DomesticStdCrewNum
			,CQCustomer.MinimumDayUseHrs
			,CQCustomer.DailyUsageAdjTax
			,CQCustomer.LandingFeeTax	
			,CQCustomer.MarginalPercentage		
	FROM	CQCustomer
	LEFT OUTER JOIN Country ON Country.CountryID = CQCustomer.CountryID
	LEFT OUTER JOIN Company ON Company.HomebaseID = CQCustomer.HomebaseID
	LEFT OUTER JOIN Airport AS HBAirport ON HBAirport.AirportID = Company.HomebaseAirportID
	LEFT OUTER JOIN Airport ON Airport.AirportID = CQCustomer.AirportID
	WHERE	CQCustomer.CustomerID = @CustomerID
		AND CQCustomer.IsDeleted = 0
	ORDER BY CQCustomer.CQCustomerCD ASC
				
END

GO


