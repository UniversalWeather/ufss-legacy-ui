

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQQuoteMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteMain]
(
	@CQQuoteMainID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;

	--// Delete the Depeendencies Tables	
	DELETE FROM CQQuoteDetail WHERE [CQQuoteMainID] = @CQQuoteMainID AND CustomerID = @CustomerID
	DELETE FROM CQQuoteAdditionalFees WHERE [CQQuoteMainID] = @CQQuoteMainID AND CustomerID = @CustomerID

	DELETE FROM [CQQuoteMain] WHERE [CQQuoteMainID] = @CQQuoteMainID AND CustomerID = @CustomerID

GO


