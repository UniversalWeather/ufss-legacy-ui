


/****** Object:  StoredProcedure [dbo].[spGetEmergencyContact]    Script Date: 09/14/2012 18:40:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetEmergencyContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetEmergencyContact]
GO


/****** Object:  StoredProcedure [dbo].[spGetEmergencyContact]   Script Date: 09/14/2012 18:40:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetEmergencyContact]      
(@CustomerID  BIGINT)        
AS        
SET NOCOUNT ON        
        
BEGIN        
SELECT  EmergencyContact.EmergencyContactID      
           ,EmergencyContact.EmergencyContactCD      
           ,EmergencyContact.CustomerID      
           ,EmergencyContact.LastName      
           ,EmergencyContact.FirstName      
           ,EmergencyContact.MiddleName      
           ,EmergencyContact.Addr1      
           ,EmergencyContact.Addr2      
           ,EmergencyContact.CityName      
           ,EmergencyContact.StateName      
           ,EmergencyContact.PostalZipCD      
           ,EmergencyContact.CountryID      
           ,EmergencyContact.PhoneNum      
           ,EmergencyContact.FaxNum      
           ,EmergencyContact.CellPhoneNum      
           ,EmergencyContact.EmailAddress      
           ,EmergencyContact.LastUpdUID      
           ,EmergencyContact.LastUpdTS      
           ,EmergencyContact.IsDeleted 
           ,EmergencyContact.BusinessPhone  
		   ,EmergencyContact.BusinessFax  
		   ,EmergencyContact.CellPhoneNum2  
		   ,EmergencyContact.OtherPhone  
		   ,EmergencyContact.PersonalEmail 
		   ,EmergencyContact.OtherEmail  
		   ,EmergencyContact.Addr3           
           ,Country.CountryCD      
           ,Country.CountryName  
           ,EmergencyContact.IsInActive                
  FROM  EmergencyContact       
  LEFT OUTER JOIN Country on Country.CountryID = EmergencyContact.CountryID      
  WHERE EmergencyContact.CustomerID =@CustomerID        
AND EmergencyContact.IsDeleted='false'
order by  EmergencyContactCD        
END  
GO
