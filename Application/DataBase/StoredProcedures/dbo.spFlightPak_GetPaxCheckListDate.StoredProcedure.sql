

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPaxCheckListDate]    Script Date: 03/28/2013 18:44:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetPaxCheckListDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetPaxCheckListDate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPaxCheckListDate]    Script Date: 03/28/2013 18:44:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
    
CREATE PROCEDURE [dbo].[spFlightPak_GetPaxCheckListDate]          
(          
 @CustomerID BIGINT,      
 @PassengerID BIGINT          
)          
AS          
BEGIN        
SET NOCOUNT ON;        
  


  
    
SELECT   

           PaxCheckListDetail.[PassengerCheckListDetailID]  
           ,PaxCheckListDetail.[PassengerAdditionalInfoID] 
           
           ,isnull((Paxaddinfo.PassengerInfoCD),'0') as 'PassengerChecklistCD'
           ,isnull((Paxaddinfo.PassengerDescription),'0') as 'PassengerChecklistDescription' 
           ,PaxCheckListDetail.[PreviousCheckDT]  
           ,PaxCheckListDetail.[CustomerID]  
           ,PaxCheckListDetail.[PassengerID]  
           ,PaxCheckListDetail.[DueDT]  
           ,PaxCheckListDetail.[AlertDT]  
           ,PaxCheckListDetail.[FrequencyMonth]  
           ,PaxCheckListDetail.[AlertDays]  
           ,PaxCheckListDetail.[GraceDays]  
           ,PaxCheckListDetail.[GraceDT]  
           ,PaxCheckListDetail.[LastUpdUID]  
           ,PaxCheckListDetail.[LastUpdTS]  
           ,PaxCheckListDetail.[IsMonthEnd]  
           ,PaxCheckListDetail.[IsStopCALC]  
           ,PaxCheckListDetail.[IsOneTimeEvent]  
           ,PaxCheckListDetail.[IsNoConflictEvent]  
           ,PaxCheckListDetail.[IsNoChecklistREPT]  
           ,PaxCheckListDetail.[IsInActive]  
           ,PaxCheckListDetail.[Specific]  
           ,PaxCheckListDetail.[IsCompleted]  
           ,PaxCheckListDetail.[OriginalDT]  
           ,PaxCheckListDetail.[IsPrintStatus]  
           ,PaxCheckListDetail.[IsPassedDueAlert]  
           ,PaxCheckListDetail.[Frequency]  
           ,PaxCheckListDetail.[IsNextMonth]  
           ,PaxCheckListDetail.[IsEndCalendarYear]  
           ,PaxCheckListDetail.[IsScheduleCheck]  
           ,PaxCheckListDetail.[IsDeleted]

     
FROM PassengerCheckListDetail as PaxCheckListDetail      
LEFT OUTER JOIN PassengerInformation as Paxaddinfo  ON Paxaddinfo.PassengerInformationID = PaxCheckListDetail.PassengerAdditionalInfoID    
WHERE PaxCheckListDetail.[CustomerID] = @CustomerID AND  PaxCheckListDetail.PassengerID = @PassengerID AND PaxCheckListDetail.isdeleted = 0         
order by Paxaddinfo.PassengerInfoCD    
END      
      
GO


