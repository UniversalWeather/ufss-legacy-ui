
/****** Object:  StoredProcedure [dbo].[spGetAllLocks]    Script Date: 04/03/2013 17:43:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllLocks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllLocks]
GO


/****** Object:  StoredProcedure [dbo].[spGetAllLocks]    Script Date: 04/03/2013 17:43:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Manish Varma
-- Create date: 14/09/2012
-- Description:	To get all locks Placed based on Customer ID
-- =============================================
CREATE PROCEDURE [dbo].[spGetAllLocks](@CustomerID BIGINT)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements. 
	Select FPLockID, FPLockLog.customerID, EntityType, LockReqUID ,  LockReqStartTime, LockReqEndTime, EntityPrimaryKey,   
	  FirstName + ' '+ MiddleName + ' ' + LastName as FullName, UserMaster.EmailID, UserMaster.PhoneNum  
from FPLockLog, UserMaster


where 
	FPLockLog.LockReqUID= UserMaster.UserName
	and FPLockLog.CustomerID =@CustomerID
END
