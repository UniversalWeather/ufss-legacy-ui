IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllCQCustomerAndContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllCQCustomerAndContact]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spFlightPak_GetAllCQCustomerAndContact]      
(      
 @CustomerID  BIGINT  
  
)      
AS      
BEGIN      
SET NOCOUNT ON;      
   
SELECT     
       cqc.[CustomerID] 
       ,cqc.[CQCustomerID]      
      ,[CQCustomerCD]
      ,[CQCustomerName]
      ,[IsApplicationFiled]
      ,[IsApproved]
      ,[BillingName]
      ,[BillingAddr1]
      ,[BillingAddr2]
      ,[BillingAddr3]
      ,[BillingCity]
      ,[BillingState]
      ,[BillingZip]
      ,cqc.[CountryID]
      ,[BillingPhoneNum]
      ,[BillingFaxNum]
      ,cqc.[Notes]
      ,[Credit]
      ,[DiscountPercentage]
      ,cqc.[IsInActive]
      ,cqc.[HomebaseID]
      ,cqc.[AirportID]
      ,[DateAddedDT]
      ,[WebAddress]
      ,cqc.[EmailID]
      ,[TollFreePhone]
      ,cqc.[LastUpdUID]
      ,cqc.[LastUpdTS]
      ,cqc.[IsDeleted]
      ,cqcc.CQCustomerContactCD
      ,cqcc.CQCustomerContactID
      ,cqcc.PhoneNum
      ,cqcc.FaxNum
      ,cqcc.FirstName as Name
      ,Airport.ICAOID AS HomebaseCD    
 FROM [CQCustomer] cqc
 left outer join   CQCustomerContact cqcc on cqc.CustomerID = cqcc.CustomerID       
  LEFT OUTER JOIN Airport ON Airport.AirportID = cqc.AirportID 
 WHERE cqc.[CQCustomerID]  = @CustomerID      
 AND ISNULL(cqc.IsDeleted,0) = 0    
 order by CQCustomerName 
 END 
GO
