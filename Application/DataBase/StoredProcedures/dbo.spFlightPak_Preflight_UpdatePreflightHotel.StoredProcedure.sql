
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightHotel]    Script Date: 04/01/2013 16:18:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightHotel]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightHotel]    Script Date: 04/01/2013 16:18:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightHotel]    
 (      
         @PreflightCrewHotelListID bigint    
           ,@PreflightHotelName varchar(60)    
           ,@PreflightCrewListID bigint    
           ,@HotelID bigint    
           ,@AirportID bigint             
           ,@RoomType varchar(60)    
           ,@RoomRent numeric(6,2)    
           ,@RoomConfirm text    
           ,@RoomDescription text    
           ,@Street varchar(50)    
           ,@CityName varchar(20)    
           ,@StateName varchar(20)    
           ,@PostalZipCD char(20)    
           ,@PhoneNum1 varchar(25)    
           ,@PhoneNum2 varchar(25)    
           ,@PhoneNum3 varchar(25)    
           ,@PhoneNum4 varchar(25)    
           ,@FaxNUM varchar(25)    
           ,@LastUpdUID char(30)    
           ,@LastUpdTS datetime    
           ,@IsDeleted bit     
           ,@HotelArrangedBy varchar(10)    
           ,@Rate numeric(17,2)    
           ,@DateIn date    
           ,@DateOut date    
           ,@IsSmoking bit    
           ,@RoomTypeID bigint    
           ,@NoofBeds numeric(3,0)    
           ,@IsAllCrew bit    
           ,@IsCompleted bit    
           ,@IsEarlyCheckIn bit    
           ,@EarlyCheckInOption char(1)    
           ,@SpecialInstructions varchar(100)    
           ,@ConfirmationStatus  varchar(max)    
           ,@Comments varchar(max)    
           ,@CustomerID Bigint             
     ,@Address1 varchar(40)    
     ,@Address2 varchar(40)    
     ,@Address3 varchar(40)    
   ,@MetroID bigint    
   ,@City varchar(30)    
   ,@StateProvince varchar(25)    
   ,@CountryID bigint    
   ,@PostalCode varchar(15)    
   ,@Status varchar(20)     
    )    
AS    
BEGIN     
SET NOCOUNT OFF    
    
UPDATE [dbo].[PreflightCrewHotelList]    
   SET [PreflightHotelName] = @PreflightHotelName      
      ,[HotelID] =@HotelID    
      ,[AirportID] = @AirportID    
      ,[RoomType] = @RoomType    
      ,[RoomRent] = @RoomRent    
      ,[RoomConfirm] = @RoomConfirm    
      ,[RoomDescription] = @RoomDescription    
      ,[Street] = @Street    
      ,[CityName] = @CityName    
      ,[StateName] = @StateName    
      ,[PostalZipCD] = @PostalZipCD    
      ,[PhoneNum1] = @PhoneNum1    
      ,[PhoneNum2] = @PhoneNum2    
      ,[PhoneNum3] = @PhoneNum3    
      ,[PhoneNum4] = @PhoneNum4    
      ,[FaxNUM] = @FaxNUM    
      ,[LastUpdUID] = @LastUpdUID    
      ,[LastUpdTS] = @LastUpdTS    
      ,[IsDeleted] = @IsDeleted    
      ,[HotelArrangedBy] = @HotelArrangedBy    
   ,[Rate] = @Rate    
   ,[DateIn] = @DateIn    
   ,[DateOut] = @DateOut    
   ,[IsSmoking] = @IsSmoking    
   ,[RoomTypeID] = @RoomTypeID    
   ,[NoofBeds] = @NoofBeds    
   ,[IsAllCrew] = @IsAllCrew    
   ,[IsCompleted] = @IsCompleted    
   ,[IsEarlyCheckIn] = @IsEarlyCheckIn    
   ,[EarlyCheckInOption] = @EarlyCheckInOption    
   ,[SpecialInstructions] = @SpecialInstructions    
   ,[ConfirmationStatus] = @ConfirmationStatus    
   ,[Comments] = @Comments     
     ,[Address1] = @Address1    
     ,[Address2] =@Address2    
     ,[Address3] =@Address3    
   ,[MetroID]= @MetroID    
   ,[City] =@City    
   ,[StateProvince]=@StateProvince    
   ,[CountryID]=@CountryID    
   ,[PostalCode]= @PostalCode    
   ,[Status] = @Status    
       
 WHERE  PreflightCrewHotelListID=@PreflightCrewHotelListID AND PreflightCrewListID=@PreflightCrewListID     
     
END    
    
    
    




GO


