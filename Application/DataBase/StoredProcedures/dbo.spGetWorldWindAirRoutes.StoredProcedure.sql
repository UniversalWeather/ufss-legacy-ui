GO
/****** Object:  StoredProcedure [dbo].[spGetWorldWindAirRoutes]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetWorldWindAirRoutes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetWorldWindAirRoutes]
GO

CREATE PROCEDURE [dbo].[spGetWorldWindAirRoutes]
(@CustomerID BIGINT
,@DepartureAirportID BIGINT
,@ArrivalAirportID BIGINT)
AS BEGIN


	DECLARE @DepZone BIGINT 
	DECLARE @ArrZone BIGINT 
	DECLARE @TempZone BIGINT
	
	
	--SELECT @DepZone = ISNULL(WindZone,'') FROM Airport WHERE CustomerID = @CustomerID AND AirportID = @DepartureAirportID
	SELECT @DepZone = ISNULL(WindZone,0) FROM Airport WHERE AirportID = @DepartureAirportID
	--SELECT @ArrZone = ISNULL(WindZone,'') FROM Airport WHERE CustomerID = @CustomerID AND AirportID = @ArrivalAirportID	
        SELECT @ArrZone = ISNULL(WindZone,0) FROM Airport WHERE  AirportID = @ArrivalAirportID	

	DECLARE @IsExist INT
	SET @IsExist = 0
	
	
	DECLARE @tmpWorldWind TABLE 
	(
		WorldWindID bigint,
		CustomerID bigint,
		DepartureZoneAirport bigint,
		ArrivalZoneAirport bigint,
		DepartureCity varchar(25),
		ArrivalCity varchar(25),
		WorldWindQuarter char(1),
		Altitude char(1),
		Direction int,
		ReturnDirection int,
		WorldWindStandard int,
		LastUpdUID varchar(30),
		LastUpdTS datetime,
		IsDeleted bit
	)
	
	IF EXISTS(SELECT WorldWindID FROM WorldWind WHERE CustomerID = @CustomerID AND DepartureZoneAirport = @DepZone AND ArrivalZoneAirport = @ArrZone)
		BEGIN
			SET @IsExist = 1
			
			INSERT INTO @tmpWorldWind
				(WorldWindID,CustomerID,DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,LastUpdUID,LastUpdTS,IsDeleted)
			SELECT WorldWindID,CustomerID,DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,LastUpdUID,LastUpdTS,IsDeleted
			FROM WorldWind WHERE CustomerID = @CustomerID AND DepartureZoneAirport = @DepZone AND ArrivalZoneAirport = @ArrZone
			ORDER BY WorldWindQuarter ASC
			
		END
	ELSE IF EXISTS(SELECT WorldWindID FROM WorldWind WHERE CustomerID = @CustomerID AND DepartureZoneAirport = @ArrZone AND ArrivalZoneAirport = @DepZone)
		BEGIN
			SET @IsExist = 1
			
			INSERT INTO @tmpWorldWind
				(WorldWindID,CustomerID,DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,LastUpdUID,LastUpdTS,IsDeleted)
			SELECT WorldWindID,CustomerID,DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,LastUpdUID,LastUpdTS,IsDeleted
			FROM WorldWind WHERE CustomerID = @CustomerID AND DepartureZoneAirport = @ArrZone AND ArrivalZoneAirport = @DepZone
			ORDER BY WorldWindQuarter ASC
			
		END
		
	SELECT WorldWindID,CustomerID,DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
			,Altitude,Direction,ReturnDirection,WorldWindStandard,LastUpdUID,LastUpdTS,IsDeleted FROM @tmpWorldWind
END
