
GO
/****** Object:  StoredProcedure [dbo].[spAddFlightPurpose]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFlightPurpose]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFlightPurpose]
GO
CREATE PROCEDURE [dbo].[spAddFlightPurpose]  
 (  @FlightPurposeCD CHAR(2) 
   ,@FlightPurposeDescription VARCHAR(25)  
   ,@CustomerId BIGINT  
   ,@IsDefaultPurpose BIT  
   ,@IsPersonalTravel BIT  
   ,@ClientID BIGINT 
   ,@LastUpdUID VARCHAR(30)  
   ,@LastUpdTS DATETIME  
   ,@IsWaitList BIT  
   ,@IsDeleted BIT  
   ,@IsInActive BIT
)  
-- =============================================  
-- Author:Sujitha.v  
-- Create date: 17/4/2012  
-- Description: Insert the Flight Purpose information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON
DECLARE  @FlightPurposeID  BIGINT  
  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FlightPurposeID OUTPUT
if (@IsDefaultPurpose='True')        
BEGIN        
UPDATE [FlightPurpose]        
SET        
IsDefaultPurpose='False'        
WHERE CustomerID =@CustomerID
SET @LastUpdTS = GETUTCDATE()  
   INSERT INTO FlightPurpose  ([FlightPurposeID]
							   ,[FlightPurposeCD]
							   ,[FlightPurposeDescription]
							   ,[CustomerID]
							   ,[IsDefaultPurpose]
							   ,[IsPersonalTravel]
							   ,[ClientID]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsWaitList]
							   ,[IsDeleted]
							   ,[IsInActive])  
                        VALUES (@FlightPurposeID
							   ,@FlightPurposeCD
							   ,@FlightPurposeDescription
							   ,@CustomerID
							   ,@IsDefaultPurpose
							   ,@IsPersonalTravel
							   ,@ClientID
							   ,@LastUpdUID
							   ,@LastUpdTS
							   ,@IsWaitList
							   , isnull(@IsDeleted,0)
							   , isnull(@IsInActive,0))  
  
End         
ELSE IF(@IsDefaultPurpose='False')        
BEGIN	
INSERT INTO FlightPurpose  ([FlightPurposeID]
							   ,[FlightPurposeCD]
							   ,[FlightPurposeDescription]
							   ,[CustomerID]
							   ,[IsDefaultPurpose]
							   ,[IsPersonalTravel]
							   ,[ClientID]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsWaitList]
							   ,[IsDeleted]
							   ,[IsInActive])  
                        VALUES (@FlightPurposeID
							   ,@FlightPurposeCD
							   ,@FlightPurposeDescription
							   ,@CustomerID
							   ,@IsDefaultPurpose
							   ,@IsPersonalTravel
							   ,@ClientID
							   ,@LastUpdUID
							   ,@LastUpdTS
							   ,@IsWaitList
							   , isnull(@IsDeleted,0)
							   , isnull(@IsInActive,0))  
							   END        
END         
GO
