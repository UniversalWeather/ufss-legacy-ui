/****** Object:  StoredProcedure [dbo].[spCQAddMessage]    Script Date: 02/22/2013 17:12:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCQAddMessage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCQAddMessage]
GO
/****** Object:  StoredProcedure [dbo].[spCQAddMessage]    Script Date: 02/22/2013 17:12:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
CREATE procedure [dbo].[spCQAddMessage]  
(  
    
 @CustomerID bigint ,  
 @MessageTYPE char(1) ,  
 @HomebaseID bigint ,  
 @MessageCD numeric(3,0) ,  
 @MessageDescription varchar(max) ,  
 @MessageLine varchar(50) ,  
 @LastUpdUID varchar(30) ,  
 @LastUpdTS datetime ,  
 @IsDeleted bit    
 )  
 AS    
BEGIN     
SET NOCOUNT ON    
    Declare @CQMessageID BIGINT
   
    
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CQMessageID OUTPUT  
  set @LastUpdTS = GETUTCDATE()  
    
  
   INSERT INTO CQMessage    
     (  
     CQMessageID,  
     [CustomerID] ,  
 [MessageTYPE],   
 [HomebaseID],  
 [MessageCD],   
 [MessageDescription],   
 [MessageLine] ,  
 [LastUpdUID],  
 [LastUpdTS] ,  
 [IsDeleted]   
 )  
 values  
 (  
 @CQMessageID,  
    @CustomerID ,  
 @MessageTYPE,   
 @HomebaseID,  
 @MessageCD,   
 @MessageDescription,   
 @MessageLine ,  
 @LastUpdUID,  
 @LastUpdTS ,  
 @IsDeleted   
 )  
END  
GO


