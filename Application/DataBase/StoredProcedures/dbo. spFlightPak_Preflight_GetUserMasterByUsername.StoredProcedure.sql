
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetUserMasterByUsername]    Script Date: 01/24/2014 11:11:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetUserMasterByUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetUserMasterByUsername]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetUserMasterByUsername]    Script Date: 01/24/2014 11:11:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spFlightPak_Preflight_GetUserMasterByUsername]    
(  
 @CustomerID Bigint ,
 @Username varchar(30)
)  
AS    
SET NOCOUNT ON;   
BEGIN  
SELECT [UserName]  
,[CustomerID]  
,[HomebaseID]  
,[ClientID]  
,[FirstName]  
,[MiddleName]  
,[LastName]  
,[HomeBase]  
,[TravelCoordID]  
,[IsActive]  
,[IsUserLock]  
,[IsTripPrivacy]  
,[EmailID]  
,[PhoneNum]  
,[PassengerRequestorID]  
,[UVTripLinkID]  
,[UVTripLinkPassword]  
,[IsPrimaryContact]  
,[IsSecondaryContact]  
,[IsSystemAdmin]  
,[IsDispatcher]  
,[IsOverrideCorp]  
,[IsCorpRequestApproval]  
,[LastUpdUID]  
,[LastUpdTS]  
,[IsDeleted]  
FROM [dbo].[UserMaster]  
where IsDispatcher =1  
and CustomerID = @CustomerID  
and Username = @Username
and IsDeleted =0
END  
GO


