/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddFleetCalendarEntries]    Script Date: 01/10/2013 17:11:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddFleetCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddFleetCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddFleetCalendarEntries]    Script Date: 01/10/2013 17:11:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author: Veerendra  
-- Create date: 17/7/2012  
-- Description: Insert the Fleet Calendar Entries 

--Update Date: 31/7/2012
-- Description: added the Trip Number in Leg & main table.
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddFleetCalendarEntries]  
	-- Add the parameters for the stored procedure here
(
@CustomerID BIGINT,
@TailNum CHAR(6),
@HomebaseID BIGINT,
@LastUpdUID CHAR(30),
@DepartureDTTMLocal DATETIME,
@ArrivalDTTMLocal DATETIME,
@DepartureGreenwichDTTM DATETIME,
@ArrivalGreenwichDTTM DATETIME,
@HomeDepartureDTTM DATETIME,
@HomeArrivalDTTM DATETIME,
@DepartICAOID BIGINT,
@DutyTYPE CHAR(2),
@Notes TEXT,
@ClientID BIGINT,
@FleetID BIGINT,
@NoteSec varchar(max)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@ClientID=0)
	begin
	set @ClientID=null
	end
    -- Insert statements for procedure here
 --   DECLARE  @FleetID BIGINT 
 --   DECLARE @CheckFleet INT    
 --   SELECT  @CheckFleet=COUNT(*) FROM Fleet WHERE TailNum=@TailNum and CustomerID=@CustomerID
 --   IF @CheckFleet>0
 --   BEGIN
	--	Select @FleetID = FleetID FROM Fleet WHERE TailNum=@TailNum and CustomerID=@CustomerID
 --   END
 --   ELSE
 --   BEGIN
    
	--EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @FleetID OUTPUT 

	--INSERT INTO dbo.Fleet
	--		(	FleetID,
	--			CustomerID,
	--			TailNum,
	--			HomebaseID,
	--			IsDeleted) 
	--	VALUES( @FleetID,
	--			@CustomerID,
	--			@TailNum,
	--			@HomebaseID,
	--			0)
	--End
DECLARE  @TripID BIGINT 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @TripID OUTPUT 

DECLARE  @TripNUM BIGINT 
SET @TripNUM = 0
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @TripNUM  OUTPUT 
SELECT @TripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID
			
	INSERT INTO dbo.PreflightMain
				(	TripID,
					TripNUM,
					FleetID,
					CustomerID,
					Notes,
					HomebaseID,
					RecordType,
					IsDeleted,
					ClientID,
					FleetCalendarNotes
					)
			VALUES( @TripID,
					@TripNUM,
					@FleetID,
					@CustomerID,
					@Notes,
					@HomebaseID,
					'M',
					0,
					@ClientID,
					@NoteSec
					)
					
DECLARE  @LegID BIGINT 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @LegID OUTPUT 
					
	INSERT INTO dbo.PreflightLeg
				(	LegID,
					TripID,					
					LegNUM,
					TripNUM,
					CustomerID,
					DepartICAOID,
					ArriveICAOID,
					DepartureDTTMLocal,
					ArrivalDTTMLocal,
					DepartureGreenwichDTTM,
					ArrivalGreenwichDTTM,
					HomeDepartureDTTM,
					HomeArrivalDTTM,
					DutyTYPE,
					IsDeleted,
					ClientID)
			VALUES( @LegID,
					@TripID,
					1,
					@TripNUM,
					@CustomerID,
					@DepartICAOID,
					@DepartICAOID,
					@DepartureDTTMLocal,
					@ArrivalDTTMLocal,
					@DepartureGreenwichDTTM,
					@ArrivalGreenwichDTTM,
					@HomeDepartureDTTM,
					@HomeArrivalDTTM,
					@DutyTYPE,
					0,
					@ClientID)
	
END

GO


