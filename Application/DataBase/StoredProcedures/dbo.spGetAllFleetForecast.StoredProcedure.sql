


GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetForecast]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetForecast]
GO 
CREATE Procedure [dbo].[spGetAllFleetForecast](@CustomerID  BIGINT, @FleetID BIGINT, @Year INT)    
AS    
-- =============================================    
-- Author: Badinath    
-- Create date: 17/5/2012    
-- Description: Get All FleetForecast Informations    
-- =============================================    
BEGIN  
SET NOCOUNT ON    
    IF @Year >0
     BEGIN
       SELECT DISTINCT STR(YEAR(YearMonthDate),4,0) + ' FORECAST' as YearMon,  
            [FleetForeCastID]  
           ,[CustomerID]  
           ,[FleetID]  
           ,[AircraftCD]  
           ,[YearMonthDate]  
           ,'' AS [Year]    
           ,[ForecastHrs]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted] 
           ,IsInactive  
        FROM  [FleetForecast]   
        WHERE CustomerID =@CustomerID AND FleetID=@FleetID          
        AND ISNULL(IsDeleted,0) = 0   AND STR(YEAR(YearMonthDate),4,0) =@Year  
      END
    ELSE
      BEGIN    
         SELECT DISTINCT STR(YEAR(YearMonthDate),4,0) + ' FORECAST' as YearMon,  
            [FleetForeCastID]  
           ,[CustomerID]  
           ,[FleetID]  
           ,[AircraftCD]  
           ,[YearMonthDate]  
           ,'' AS [Year]    
           ,[ForecastHrs]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted] 
           ,IsInactive  
         FROM  [FleetForecast]   
         WHERE CustomerID =@CustomerID AND FleetID=@FleetID          
         AND ISNULL(IsDeleted,0) = 0   
         Order By YearMon
      END
 END
GO
