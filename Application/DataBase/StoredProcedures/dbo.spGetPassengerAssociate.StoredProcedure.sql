/****** Object:  StoredProcedure [dbo].[spGetPassengerAssociate]    Script Date: 12/12/2012 12:44:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassengerAssociate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassengerAssociate]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllPassenger]    Script Date: 12/12/2012 12:44:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetPassengerAssociate]
(@CustomerID bigint
,@ClientID bigint)
AS BEGIN
-- ====================================================
-- Author: Srinivasan. J
-- Create date: 12/12/2012
-- Description: Get All Passenger Requestor Information
-- ====================================================
	IF(@ClientID>0)
		BEGIN
			SELECT	Passenger.PassengerRequestorID
				   ,Passenger.PassengerRequestorCD
				   ,Passenger.PassengerName
				   ,Passenger.CustomerID
				   ,Passenger.DepartmentID
				   ,Passenger.PassengerDescription
				   ,Passenger.PhoneNum
				   ,Passenger.IsEmployeeType
				   ,Passenger.StandardBilling
				   ,Passenger.AuthorizationID
				   ,Passenger.AuthorizationDescription
				   ,Passenger.Notes
				   ,Passenger.LastName
				   ,Passenger.FirstName
				   ,Passenger.MiddleInitial
				   ,Passenger.DateOfBirth
				   ,Passenger.ClientID
				   ,Passenger.AssociatedWithCD
				   ,Passenger.HomebaseID
				   ,Passenger.IsActive
				   ,Passenger.FlightPurposeID
				   ,Passenger.SSN
				   ,Passenger.Title
				   ,Passenger.SalaryLevel
				   ,Passenger.IsPassengerType
				   ,Passenger.LastUpdUID
				   ,Passenger.LastUpdTS
				   ,Passenger.IsScheduledServiceCoord
				   ,Passenger.CompanyName
				   ,Passenger.EmployeeID
				   ,Passenger.FaxNum
				   ,Passenger.EmailAddress
				   ,Passenger.PersonalIDNum
				   ,Passenger.IsSpouseDependant
				   ,Passenger.AccountID
				   ,Passenger.CountryID
				   ,Passenger.IsSIFL
				   ,Passenger.PassengerAlert
				   ,Passenger.Gender
				   ,Passenger.AdditionalPhoneNum
				   ,Passenger.IsRequestor
				   ,Passenger.CountryOfResidenceID
				   ,Passenger.PaxScanDoc
				   ,Passenger.TSAStatus
				   ,Passenger.TSADTTM
				   ,Passenger.Addr1
				   ,Passenger.Addr2
				   ,Passenger.City
				   ,Passenger.StateName
				   ,Passenger.PostalZipCD
				   ,Passenger.IsDeleted
				   ,Nationality.CountryName AS NationalityName
				   ,Nationality.CountryCD AS NationalityCD
				   ,Client.ClientCD
				   ,Client.ClientDescription
				   ,Account.AccountNum
				   ,Account.AccountDescription
				   ,Department.DepartmentCD
				   ,Department.DepartmentName
				   ,DepartmentAuthorization.AuthorizationCD
				   ,DepartmentAuthorization.DeptAuthDescription
				   ,FlightPurpose.FlightPurposeCD
				   ,FlightPurpose.FlightPurposeDescription
				   ,CountryOfResidence.CountryName AS ResidenceCountryName
				   ,CountryOfResidence.CountryCD AS ResidenceCountryCD
				   ,HomeBase.CityName AS HomeBaseName
				   ,HomeBase.IcaoID AS HomeBaseCD
				   ,Passenger.PrimaryMobile
				   ,Passenger.BusinessFax
				   ,Passenger.CellPhoneNum2
				   ,Passenger.OtherPhone
				   ,Passenger.PersonalEmail
				   ,Passenger.OtherEmail
				   ,Passenger.Addr3
				   ,Passenger.Birthday
				   ,Passenger.Anniversaries
				   ,Passenger.EmergencyContacts
				   ,Passenger.CreditcardInfo
				   ,Passenger.CateringPreferences
				   ,Passenger.HotelPreferences  
				   ,Passenger.PaxTitle
				   ,Passenger.CityOfBirth
				   ,Passenger.Nickname
			FROM Passenger AS Passenger
				LEFT OUTER JOIN Country AS Nationality ON Nationality.CountryID = Passenger.CountryID
				LEFT OUTER JOIN Client AS Client ON Client.ClientID = Passenger.ClientID
				LEFT OUTER JOIN Account AS Account ON Account.AccountID = Passenger.AccountID
				LEFT OUTER JOIN Department AS Department ON Department.DepartmentID = Passenger.DepartmentID
				LEFT OUTER JOIN DepartmentAuthorization AS DepartmentAuthorization ON DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID
				LEFT OUTER JOIN Airport AS HomeBase ON HomeBase.AirportID = Passenger.HomebaseID
				LEFT OUTER JOIN FlightPurpose AS FlightPurpose ON FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID
				LEFT OUTER JOIN Country AS CountryOfResidence ON CountryOfResidence.CountryID = Passenger.CountryOfResidenceID
			WHERE Passenger.CustomerID = @CustomerID AND 
				Passenger.isdeleted=0 AND
				Passenger.ClientID=@ClientID AND
				Passenger.IsEmployeeType <> 'G'
			ORDER BY Passenger.PassengerName
		END
	ELSE
		BEGIN  
			SELECT	Passenger.PassengerRequestorID
					,Passenger.PassengerRequestorCD
					,Passenger.PassengerName
					,Passenger.CustomerID
					,Passenger.DepartmentID
					,Passenger.PassengerDescription
					,Passenger.PhoneNum
					,Passenger.IsEmployeeType
					,Passenger.StandardBilling
					,Passenger.AuthorizationID
					,Passenger.AuthorizationDescription
					,Passenger.Notes
					,Passenger.LastName
					,Passenger.FirstName
					,Passenger.MiddleInitial
					,Passenger.DateOfBirth
					,Passenger.ClientID
					,Passenger.AssociatedWithCD
					,Passenger.HomebaseID
					,Passenger.IsActive
					,Passenger.FlightPurposeID
					,Passenger.SSN
					,Passenger.Title
					,Passenger.SalaryLevel
					,Passenger.IsPassengerType
					,Passenger.LastUpdUID
					,Passenger.LastUpdTS
					,Passenger.IsScheduledServiceCoord
					,Passenger.CompanyName
					,Passenger.EmployeeID
					,Passenger.FaxNum
					,Passenger.EmailAddress
					,Passenger.PersonalIDNum
					,Passenger.IsSpouseDependant
					,Passenger.AccountID
					,Passenger.CountryID
					,Passenger.IsSIFL
					,Passenger.PassengerAlert
					,Passenger.Gender
					,Passenger.AdditionalPhoneNum
					,Passenger.IsRequestor
					,Passenger.CountryOfResidenceID
					,Passenger.PaxScanDoc
					,Passenger.TSAStatus
					,Passenger.TSADTTM
					,Passenger.Addr1
					,Passenger.Addr2
					,Passenger.City
					,Passenger.StateName
					,Passenger.PostalZipCD
					,Passenger.IsDeleted
					,Nationality.CountryName AS NationalityName
					,Nationality.CountryCD AS NationalityCD
					,Client.ClientCD
					,Client.ClientDescription
					,Account.AccountNum
					,Account.AccountDescription
					,Department.DepartmentCD
					,Department.DepartmentName
					,DepartmentAuthorization.AuthorizationCD
					,DepartmentAuthorization.DeptAuthDescription
					,FlightPurpose.FlightPurposeCD
					,FlightPurpose.FlightPurposeDescription
					,CountryOfResidence.CountryName AS ResidenceCountryName
					,CountryOfResidence.CountryCD AS ResidenceCountryCD
					,HomeBase.CityName AS HomeBaseName
					,HomeBase.IcaoID AS HomeBaseCD
					,Passenger.PrimaryMobile
					,Passenger.BusinessFax
					,Passenger.CellPhoneNum2
					,Passenger.OtherPhone
					,Passenger.PersonalEmail
					,Passenger.OtherEmail
					,Passenger.Addr3
					,Passenger.Birthday
				    ,Passenger.Anniversaries
				    ,Passenger.EmergencyContacts
				    ,Passenger.CreditcardInfo
				    ,Passenger.CateringPreferences
				    ,Passenger.HotelPreferences  
				    ,Passenger.PaxTitle
				    ,Passenger.CityOfBirth
				    ,Passenger.Nickname
			FROM Passenger AS Passenger
				LEFT OUTER JOIN Country AS Nationality ON Nationality.CountryID = Passenger.CountryID
				LEFT OUTER JOIN Client AS Client ON Client.ClientID = Passenger.ClientID
				LEFT OUTER JOIN Account AS Account ON Account.AccountID = Passenger.AccountID
				LEFT OUTER JOIN Department AS Department ON Department.DepartmentID = Passenger.DepartmentID
				LEFT OUTER JOIN DepartmentAuthorization AS DepartmentAuthorization ON DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID
				LEFT OUTER JOIN Airport AS HomeBase ON HomeBase.AirportID = Passenger.HomebaseID
				LEFT OUTER JOIN FlightPurpose AS FlightPurpose ON FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID
				LEFT OUTER JOIN Country AS CountryOfResidence ON CountryOfResidence.CountryID = Passenger.CountryOfResidenceID
			WHERE Passenger.CustomerID = @CustomerID AND
				Passenger.isdeleted=0 AND 
				Passenger.IsEmployeeType <> 'G'
			ORDER BY Passenger.PassengerName
		END
END 
GO