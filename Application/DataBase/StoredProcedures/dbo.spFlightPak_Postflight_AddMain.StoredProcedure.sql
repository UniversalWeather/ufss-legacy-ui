IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_AddMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_AddMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Postflight_AddMain]  
(  
 @LogNum bigint,  
 @CustomerID bigint,  
 @TripID bigint,  
 @POMainDescription varchar(40),  
 @DispatchNum varchar(12),  
 @EstDepartureDT datetime,  
 @RecordType char(1),  
 @FleetID bigint,  
 @PassengerRequestorID bigint,  
 @RequestorName varchar(25),  
 @DepartmentDescription varchar(25),  
 @AuthorizationDescription varchar(25),  
 @ClientID bigint,  
 @HomebaseID bigint,  
 @IsPersonal bit,  
 @IsException bit,  
 @Notes text,  
 @LastUpdUID char(30),  
 @LastUpdTS datetime,  
 @FlightNum varchar(12),  
 @IsCompleted bit,  
 @History text,  
 @TechLog varchar(12),  
 @AircraftID bigint,  
 @IsLastAdj bit,  
 @IsDeleted bit,  
 @AccountID bigint,  
 @DepartmentID bigint,  
 @AuthorizationID bigint  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @OldPOLogID BIGINT
 DECLARE @POLogID BIGINT
 SET @LastUpdTS = GETUTCDATE()
  
 SELECT @OldPOLogID = POlogID FROM PostflightMain WHERE TripID = @TripID
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PostflightCurrentNo',  @POLogID OUTPUT  
 SET @LogNum = 0
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripLogCurrentNo',  @LogNum  OUTPUT  
 select @LogNum = TripLogCurrentNo  from [FlightpakSequence] where customerID = @CustomerID

 INSERT INTO [PostflightMain] ([POLogID], [LogNum], [CustomerID], [TripID], [POMainDescription], [DispatchNum], [EstDepartureDT], [RecordType], [FleetID], [PassengerRequestorID], [RequestorName], [DepartmentDescription], [AuthorizationDescription], [ClientID], [HomebaseID], [IsPersonal], [IsException], [Notes], [LastUpdUID], [LastUpdTS], [FlightNum], [IsCompleted], [History], [TechLog], [AircraftID], [IsLastAdj], [IsDeleted], [AccountID], [DepartmentID], [AuthorizationID])   
 VALUES (@POLogID, @LogNum, @CustomerID, @TripID, @POMainDescription, @DispatchNum, @EstDepartureDT, @RecordType, @FleetID, @PassengerRequestorID, @RequestorName, @DepartmentDescription, @AuthorizationDescription, @ClientID, @HomebaseID, @IsPersonal, @IsException, @Notes, @LastUpdUID, @LastUpdTS, @FlightNum, @IsCompleted, @History, @TechLog, @AircraftID, @IsLastAdj, @IsDeleted, @AccountID, @DepartmentID, @AuthorizationID);  
 
 if(@TripID>0)
 BEGIN	
	UPDATE PreflightMain SET IsLog = 1 WHERE TripID = @TripID	
	if(@OldPOLogID>0)
	BEGIN
		DELETE FROM PostflightLogHistory WHERE POLogID = @OldPOLogID -- BUG ID : 6926
		DELETE FROM PostflightTripException WHERE POLogID = @OldPOLogID
		DELETE FROM [PostflightNote]
		WHERE 
			[PostflightExpenseID] IN 
			(SELECT PostflightExpenseID FROM PostflightExpense WHERE 
			[POLogID] = @OldPOLogID
			)
	
		DELETE FROM [PostflightPassenger]
		WHERE (
		[POLogID] = @OldPOLogID
		)
	
		DELETE FROM PostflightCrew WHERE POLogID = @OldPOLogID
		DELETE FROM PostflightExpense WHERE POLogID = @OldPOLogID
		DELETE FROM PostflightSIFL WHERE POLogID = @OldPOLogID
		DELETE FROM PostflightLeg WHERE POLogID = @OldPOLogID
		DELETE FROM PostflightMain WHERE POLogID = @OldPOLogID
	END
 END
 
 SELECT @POLogID as POLogID  
END

GO


