

/****** Object:  StoredProcedure [dbo].[spCQDeleteMessage]    Script Date: 02/22/2013 17:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCQDeleteMessage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCQDeleteMessage]
GO


/****** Object:  StoredProcedure [dbo].[spCQDeleteMessage]    Script Date: 02/22/2013 17:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create procedure [dbo].[spCQDeleteMessage]  
( 
 @CustomerID bigint ,  
 @MessageTYPE char(1) ,  
 @HomebaseID bigint 


)
as
begin
delete  from CQMessage where CustomerID = @CustomerID and MessageTYPE = @MessageTYPE and HomebaseID = @HomebaseID  
end

GO


