
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeIcaoID]    Script Date: 01/17/2013 19:00:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_MergeIcaoID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_MergeIcaoID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeIcaoID]    Script Date: 01/17/2013 19:00:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_MergeIcaoID]
(
@OldIcaoId varchar(30),
@NewIcaoId varchar(30),
@CustomerID BIGINT,
@LastUpdUID CHAR(30)

)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	
	begin Try
			
			begin Transaction

		  -- Airport 		 
			UPDATE Airport  
				SET IcaoID =@NewIcaoId,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE IcaoID =@OldIcaoId and CustomerID=@CustomerID
			
		  
          		   
		  --Airport
		   UPDATE Airport SET IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
		   WHERE IcaoID = @OldIcaoId
			
			commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	
	select   @MergeSuccess as 'MergeSuccess'
END



GO


