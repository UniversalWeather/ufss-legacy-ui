/****** Object:  StoredProcedure [dbo].[spFlightPak_GetUserGroupSelectedList]    Script Date: 08/29/2012 16:50:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetUserGroupSelectedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetUserGroupSelectedList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetUserGroupSelectedList]    
 (@UserName char(30)    
 ,@CustomerID bigint)    
AS    
--SET NOCOUNT ON;    
BEGIN    
SELECT [UserGroupID]    
      ,[UserGroupCD]    
      ,[UserGroupDescription]    
      ,[CustomerID]    
      ,[LastUpdUID]    
      ,[LastUpdTS]    
      ,[IsDeleted]    
      ,[IsInActive]  
  FROM [UserGroup]     
     WHERE  [UserGroupID]    
  IN(SELECT [UserGroupID] FROM UserGroupMapping    
   WHERE UserName=@UserName AND IsDeleted='FALSE')     
 AND [CustomerID]=@CustomerID AND UserGroup.IsDeleted = 'False'     
End 
GO
