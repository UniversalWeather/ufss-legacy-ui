
GO
/****** Object:  StoredProcedure [dbo].[spGetAirportPairDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportPairDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportPairDetail]
GO

CREATE PROCEDURE spGetAirportPairDetail
(@AirportPairDetailID BIGINT,
@CustomerID	BIGINT,
@IsDeleted BIT)
AS BEGIN

	IF (@AirportPairDetailID = -1)
		BEGIN
			SELECT	AirportPairDetail.AirportPairDetailID,
					AirportPairDetail.CustomerID,
					AirportPairDetail.AirportPairID,
					AirportPairDetail.LegID,
					AirportPairDetail.AircraftID,
					AirportPairDetail.QTR,
					AirportPairDetail.DAirportID,
					AirportPairDetail.AAirportID,
					AirportPairDetail.Distance,
					AirportPairDetail.PowerSetting,
					AirportPairDetail.TakeoffBIAS,
					AirportPairDetail.LandingBIAS,
					AirportPairDetail.TrueAirSpeed,
					AirportPairDetail.Winds,
					AirportPairDetail.ElapseTM,
					AirportPairDetail.Cost,
					AirportPairDetail.WindReliability,
					AirportPairDetail.LastUpdUID,
					AirportPairDetail.LastUpdTS,
					AirportPairDetail.IsDeleted,
					AirportPair.AirportPairNumber,
					AirportPair.AirportPairDescription,
					AirportPair.AircraftID As AirportPairAircraftID,
					Aircraft.AircraftCD,
					Aircraft.AircraftDescription,
					DAirport.IcaoID As DAirportIcaoID,
					DAirport.CityName As DAirportCityName,
					DAirport.CountryName As DAirportCountryName,
					AAirport.IcaoID As AAirportIcaoID,
					AAirport.CityName As AAirportCityName,
					AAirport.CountryName As AAirportCountryName
					,DAirport.Alerts As DAirportAlerts
					,AAirport.Alerts As AAirportAlerts
			FROM	AirportPairDetail AirportPairDetail
			LEFT OUTER JOIN AirportPair AirportPair ON AirportPair.AirportPairID = AirportPairDetail.AirportPairID
			LEFT OUTER JOIN Aircraft Aircraft ON Aircraft.AircraftID = AirportPairDetail.AircraftID
			LEFT OUTER JOIN Airport DAirport ON DAirport.AirportID = AirportPairDetail.DAirportID
			LEFT OUTER JOIN Airport AAirport ON AAirport.AirportID = AirportPairDetail.AAirportID
			WHERE	AirportPairDetail.CustomerID = @CustomerID
					AND AirportPairDetail.IsDeleted = @IsDeleted
		END
	ELSE
		BEGIN
			SELECT	AirportPairDetail.AirportPairDetailID,
					AirportPairDetail.CustomerID,
					AirportPairDetail.AirportPairID,
					AirportPairDetail.LegID,
					AirportPairDetail.AircraftID,
					AirportPairDetail.QTR,
					AirportPairDetail.DAirportID,
					AirportPairDetail.AAirportID,
					AirportPairDetail.Distance,
					AirportPairDetail.PowerSetting,
					AirportPairDetail.TakeoffBIAS,
					AirportPairDetail.LandingBIAS,
					AirportPairDetail.TrueAirSpeed,
					AirportPairDetail.Winds,
					AirportPairDetail.ElapseTM,
					AirportPairDetail.Cost,
					AirportPairDetail.WindReliability,
					AirportPairDetail.LastUpdUID,
					AirportPairDetail.LastUpdTS,
					AirportPairDetail.IsDeleted,
					AirportPair.AirportPairNumber,
					AirportPair.AirportPairDescription,
					AirportPair.AircraftID As AirportPairAircraftID,
					Aircraft.AircraftCD,
					Aircraft.AircraftDescription,
					DAirport.IcaoID As DAirportIcaoID,
					DAirport.CityName As DAirportCityName,
					DAirport.CountryName As DAirportCountryName,
					AAirport.IcaoID As AAirportIcaoID,
					AAirport.CityName As AAirportCityName,
					AAirport.CountryName As AAirportCountryName
					,DAirport.Alerts As DAirportAlerts
					,AAirport.Alerts As AAirportAlerts
			FROM	AirportPairDetail AirportPairDetail
			LEFT OUTER JOIN AirportPair AirportPair ON AirportPair.AirportPairID = AirportPairDetail.AirportPairID
			LEFT OUTER JOIN Aircraft Aircraft ON Aircraft.AircraftID = AirportPairDetail.AircraftID
			LEFT OUTER JOIN Airport DAirport ON DAirport.AirportID = AirportPairDetail.DAirportID
			LEFT OUTER JOIN Airport AAirport ON AAirport.AirportID = AirportPairDetail.AAirportID
			WHERE	AirportPairDetail.CustomerID = @CustomerID
					AND AirportPairDetail.IsDeleted = @IsDeleted
					AND AirportPairDetail.AirportPairDetailID = @AirportPairDetailID
		END

END