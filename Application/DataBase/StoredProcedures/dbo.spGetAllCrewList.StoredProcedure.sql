

/****** Object:  StoredProcedure [dbo].[spGetAllCrewList]    Script Date: 12/05/2013 12:57:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewList]
GO


/****** Object:  StoredProcedure [dbo].[spGetAllCrewList]    Script Date: 12/05/2013 12:57:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetAllCrewList](@CustomerID bigint)  
-- =============================================  
-- Author: MohanRaja.C  
-- Create date: 08/05/2012  
-- Description: Get all the Crew List  
-- =============================================  
as  
SET NoCOUNT ON  
BEGIN  
   
   
SELECT [CrewID]  
      ,[CrewCD]  
      ,[CustomerID]  
      ,[LastName]  
      ,[FirstName]  
      ,[MiddleInitial]  
      ,[IsFixedWing]  
      ,[IsRotaryWing]  
      ,[Addr1]  
      ,[Addr2]  
      ,[CityName]  
      ,[StateName]  
      ,[PostalZipCD]  
      ,[CountryID]  
      ,[PhoneNum]  
      ,[SSN]  
      ,[IsStatus]  
      ,[HomebaseID]  
      ,[BirthDT]  
      ,[ClientID]  
      ,[CellPhoneNum]  
      ,[PagerNum]  
      ,[CrewTypeCD]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[PilotLicense1]  
      ,[Notes]  
      ,[CheckList]  
      ,[PilotLicense2]  
      ,[LicenseCountry1]  
      ,[LicenseCountry2]  
      ,[IsNoCalendarDisplay]  
      ,[CrewTypeDescription]  
      ,[Gender]  
      ,[EmailAddress]  
      ,[FaxNum]  
      ,[HireDT]  
      ,[TerminationDT]  
      ,[LicenseType1]  
      ,[LicenseType2]  
      ,[DepartmentID]  
      ,[IsCheckListFrequency]  
      ,[IsNextMonth]  
      ,[CrewScanDocuments]  
      ,[Citizenship]  
      ,[Notes2]  
      ,[IsDepartmentAuthorization]  
      ,[IsRequestPhoneNum]  
      ,[IsCancelDescription]  
      ,[IsTripN]  
      ,[IsTripA]  
      ,[IsAirport]  
      ,[IsCheckList]  
      ,[IsAccountNum]  
      ,[IsStatusT]  
      ,[IsDayLightSavingTimeARR]  
      ,[IsDayLightSavingTimeDEPART]  
      ,[IsHomeArrivalTM]  
      ,[IsHomeDEPARTTM]  
      ,[IsEndDuty]  
      ,[IsOverride]  
      ,[IsCrewRules]  
      ,[IsFAR]  
      ,[IsDuty]  
      ,[IsFlightHours]  
      ,[IsRestHrs]  
      ,[Comments]  
      ,[IsDepartureFBO]  
      ,[IsArrivalFBO]  
      ,[IsCrewHotel]  
      ,[IsCrewDepartTRANS]  
      ,[IsCrewArrivalTRANS]  
      ,[IsCrewHotelN]  
      ,[IsPassHotel]  
      ,[IsPassDepartTRANS]  
      ,[IsPassArrivalHotel]  
      ,[IsPassDetails]  
      ,[IsPassPhoneNum]  
      ,[IsDepartCatering]  
      ,[IsArrivalCatering]  
      ,[IsArrivalDepartTime]  
      ,[IcaoID]  
      ,[IsCrew]  
      ,[IsPassenger]  
      ,[IsLegN]  
      ,[IsOutboundINST]  
      ,[IsCheckList1]  
      ,[IsFBO]  
      ,[IsHotel]  
      ,[IsTransportation]  
      ,[IsCatering]  
      ,[Label1]  
      ,[Label2]  
      ,[Label3]  
      ,[Label4]  
      ,[Label5]  
      ,[Label6]  
      ,[Label7]  
      ,[IsCrewHotelConfirm]  
      ,[IsCrewHotelComments]  
      ,[IsCrewDepartTRANSConfirm]  
      ,[IsCrewDepartTRANSComments]  
      ,[IsCrewArrivalConfirm]  
      ,[IsCrewArrivalComments]  
      ,[IsCrewNotes]  
      ,[IsPassHotelConfirm]  
      ,[IsPassHotelComments]  
      ,[IsPassDepartTRANSConfirm]  
      ,[IsPassDepartTRANSComments]  
      ,[IsPassArrivalConfirm]  
      ,[IsPassArrivalComments]  
      ,[IsPassNotes]  
      ,[IsOutboundComments]  
      ,[IsDepartAirportNotes]  
      ,[IsArrivalAirportNoes]  
      ,[IsDepartAirportAlerts]  
      ,[IsArrivalAirportAlerts]  
      ,[IsFBODepartConfirm]  
      ,[IsFBOArrivalConfirm]  
      ,[IsFBODepartComment]  
      ,[IsFBOArrivalComment]  
      ,[IsDepartCateringConfirm]  
      ,[IsDepartCateringComment]  
      ,[IsArrivalCateringConfirm]  
      ,[IsArrivalCateringComment]  
      ,[IsTripAlerts]  
      ,[IsTripNotes]  
      ,[IsRunway]  
      ,[IsPassengerPhoneNum]  
      ,[BlackBerryTM]  
      ,[IsAssociatedCrew]  
      ,[CityOfBirth]  
      ,[CountryOfBirth]  
      ,[License1CityCountry]  
      ,[License2CityCountry]  
      ,[License1ExpiryDT]  
      ,[License2ExpiryDT]  
      ,[APISException]  
      ,[APISSubmit]  
      ,[APISUserID]  
      ,[IsAircraft]  
      ,[StateofBirth]  
      ,[IsDeleted]  
      ,Birthday  
        ,Anniversaries  
        ,EmergencyContacts  
        ,CreditcardInfo  
        ,CateringPreferences  
        ,HotelPreferences 
        ,IsTripEmail 
  FROM [Crew] with(nolock)   
WHERE CustomerID = @CustomerID AND ISNULL(IsDeleted,0) =0 AND Crew.CrewCD is not null  
   
end  
GO


