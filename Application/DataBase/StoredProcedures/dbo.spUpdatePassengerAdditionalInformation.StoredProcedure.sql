

/****** Object:  StoredProcedure [dbo].[spUpdatePassengerAdditionalInformation]    Script Date: 05/24/2013 15:08:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePassengerAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePassengerAdditionalInformation]
GO



/****** Object:  StoredProcedure [dbo].[spUpdatePassengerAdditionalInformation]    Script Date: 05/24/2013 15:08:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdatePassengerAdditionalInformation]      
(@PassengerAdditionalInfoID bigint  
,@CustomerID bigint  
,@PassengerRequestorID bigint  
,@AdditionalINFOCD char(3)  
,@AdditionalINFODescription varchar(25)  
,@AdditionalINFOValue nvarchar(120)  
,@ClientID bigint  
,@LastUpdUID varchar(30)  
,@LastUptTS datetime  
,@IsDeleted bit
,@PassengerInformationID bigint
,@IsPrint bit)  
AS  
BEGIN  
set @LastUptTS=GETUTCDATE()  
--IF EXISTS(SELECT * FROM [PassengerAdditionalInfo]    
--                   WHERE PassengerAdditionalInfoID = @PassengerAdditionalInfoID     
--       AND PassengerRequestorID = @PassengerRequestorID)  
--BEGIN  
 UPDATE [PassengerAdditionalInfo]      
   SET   --PassengerAdditionalInfoID = @PassengerAdditionalInfoID  
    --CustomerID = @CustomerID  
    --PassengerRequestorID = @PassengerRequestorID  
    --AdditionalINFOCD = @AdditionalINFOCD  
    AdditionalINFODescription = @AdditionalINFODescription  
    ,AdditionalINFOValue = @AdditionalINFOValue  
    ,ClientID = @ClientID  
    ,LastUpdUID = @LastUpdUID  
    ,LastUptTS = @LastUptTS  
    ,IsDeleted = @IsDeleted  
    ,PassengerInformationID = @PassengerInformationID
    ,IsPrint = @IsPrint
  WHERE    --PassengerAdditionalInfoID = @PassengerAdditionalInfoID     
     CustomerID = @CustomerID AND PassengerRequestorID = @PassengerRequestorID AND   
     AdditionalINFOCD = @AdditionalINFOCD  AND PassengerAdditionalInfoID=@PassengerAdditionalInfoID     
--END  
--ELSE  
--BEGIN  
-- EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @PassengerAdditionalInfoID OUTPUT  
-- INSERT INTO [PassengerAdditionalInfo]  
--      ([PassengerAdditionalInfoID]  
--      ,[CustomerID]  
--      ,[PassengerRequestorID]  
--      ,[AdditionalINFOCD]  
--      ,[AdditionalINFODescription]  
--      ,[AdditionalINFOValue]  
--      ,[ClientID]  
--      ,[LastUpdUID]  
--      ,[LastUptTS]  
--      ,[IsDeleted])  
--   VALUES  
--      (@PassengerAdditionalInfoID  
--    ,@CustomerID  
--    ,@PassengerRequestorID  
--    ,@AdditionalINFOCD  
--    ,@AdditionalINFODescription  
--    ,@AdditionalINFOValue  
--    ,@ClientID  
--    ,@LastUpdUID  
--    ,@LastUptTS  
--    ,@IsDeleted)   
--END  
END  
GO


