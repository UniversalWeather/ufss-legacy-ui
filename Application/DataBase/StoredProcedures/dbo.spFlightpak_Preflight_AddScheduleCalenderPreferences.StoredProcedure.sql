
GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferences]    Script Date: 03/27/2014 23:33:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferences]
GO


GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferences]    Script Date: 03/27/2014 23:33:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Sridhar Manoharan>  
-- Create date: <01 Oct 2013>  
-- Description: <Add preflight schedule calender preferences based on username and customerid>  
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferences]
(
	@CustomerID bigint,
	@UserName CHAR(30), 
	@FleetIDS NVARCHAR(4000), 
	@CrewIDS NVARCHAR(4000),
	@CalenderType VARCHAR(25)	
)     
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;    
 DECLARE  @SCPreferenceID BIGINT 
	 IF EXISTS( SELECT 1 FROM dbo.PreflightScheduleCalenderPreferences WHERE CustomerID = @CustomerID AND UserName = @UserName and CalenderType = @CalenderType)
	 BEGIN
		UPDATE dbo.PreflightScheduleCalenderPreferences SET FleetIDS = @FleetIDS,CrewIDS = @CrewIDS  WHERE CustomerID = @CustomerID AND UserName = @UserName and CalenderType = @CalenderType
	 END
	 ELSE
	 BEGIN
	 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @SCPreferenceID OUTPUT
		-- Insert statements for procedure here  
		INSERT INTO DBO.PreflightScheduleCalenderPreferences (SCPreferenceID,UserName,CustomerID,FleetIDS,CrewIDS, CalenderType)
		VALUES (@SCPreferenceID, @UserName, @CustomerID, @FleetIDS, @CrewIDS, @CalenderType) 	
	 END
  END

GO


