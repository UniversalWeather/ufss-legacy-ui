
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeCrewID]    Script Date: 01/22/2013 14:37:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_MergeCrewID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_MergeCrewID]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeCrewID]    Script Date: 01/22/2013 14:37:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_MergeCrewID]
(
@OldCrewID BIGINT,
@NewCrewID BIGINT,
@CustomerID BIGINT,
@LastUpdUID CHAR(30),
@CrewCD Varchar(5)

)

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	if @NewCrewID  = 0 
	begin
		begin Try
		begin Transaction
			update Crew set 
			CrewCD =@CrewCD 
			,LastUpdTS = GETUTCDATE()
			,LastUpdUID = @LastUpdUID
			where CustomerID = @CustomerID and CrewID = @OldCrewID 
		
		commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	end
	else
	begin		
	
	 
	DECLARE @LastName varchar(30)	
	DECLARE @FirstName varchar(20)	
	DECLARE @MiddleInitial varchar(20)

	
	begin Try
			
			begin Transaction
			
			select @LastName = LastName,
				@FirstName = FirstName,
				 @MiddleInitial = @MiddleInitial
			from Crew where crewID = @NewCrewID
			and CustomerID = @CustomerID

		  -- CrewAircraftAssigned 		 
			UPDATE CrewAircraftAssigned  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
		  --  CrewCheckListDetail		  
		    UPDATE CrewCheckListDetail  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
		  -- CrewDefinition			
		    UPDATE CrewDefinition  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
		  -- CrewRating
             UPDATE CrewRating  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
		  -- PreflightCrewList
		  UPDATE PreflightCrewList  
				SET CrewID =@NewCrewID,
				CrewFirstName = @FirstName,
				CrewMiddleName = @MiddleInitial,
				CrewLastName = @LastName,
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- PostflightCrew
          UPDATE PostflightCrew  
				SET CrewID =@NewCrewID,
				CrewFirstName = @FirstName,	
				CrewMiddleName	= @MiddleInitial,
				CrewLastName= @LastName,
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- CrewPassengerPassport 
          UPDATE CrewPassengerPassport  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			     
          -- CrewPassengerVisa
          UPDATE CrewPassengerVisa  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- caldflts   ( No table available in DB)       
          -- CrewGroupOrder
          UPDATE CrewGroupOrder  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- uvhotels ( No table available in DB)
          -- PostflightSimulatorLog
          UPDATE PostflightSimulatorLog  
				SET CrewID =@NewCrewID,		
				LastUserID=@LastUpdUID,
				LastUptTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- PostflightExpense
          UPDATE PostflightExpense  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- PostflightLeg
          UPDATE PostflightLeg  
				SET CrewID =@NewCrewID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		  WHERE CrewID =@OldCrewID and CustomerID=@CustomerID
			
          -- Users ( No CrewID available in this table)
          -- uvpass ( No table available in DB)
          		   
		  --Crew
		   UPDATE Crew SET IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
		   WHERE CrewID = @OldCrewID
			
			commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	END
	select   @MergeSuccess as 'MergeSuccess'


END
GO


