/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRTransportList]    Script Date: 04/25/2013 17:48:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRTransportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRTransportList]
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRTransportList]
(@CustomerID BIGINT
,@CRLegID BIGINT
,@CRTransportListID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRTransportList.CRTransportListID
			,CRTransportList.CustomerID
			,CRTransportList.CRLegID
			,CRTransportList.TripNUM
			,CRTransportList.LegID
			,CRTransportList.RecordType
			,CRTransportList.TransportID
			,CRTransportList.CRTransportListDescription
			,CRTransportList.PhoneNUM
			,CRTransportList.IsCompleted
			,CRTransportList.AirportID
			,CRTransportList.LastUpdUID
			,CRTransportList.LastUpdTS
			,CRTransportList.IsDeleted
			,CRTransportList.FaxNum as FaxNumber
			,CRTransportList.Email as BusinessEmail	
			,CRTransportList.Rate
			,Transport.TransportCD
			,Transport.ContactName
			,Transport.PhoneNum
			,Transport.FaxNum
			,Transport.ContactEmail
			,Transport.NegotiatedRate
	FROM CRTransportList
	LEFT OUTER JOIN Transport ON Transport.TransportID = CRTransportList.TransportID
	WHERE CRTransportList.CustomerID = @CustomerID
	AND CRTransportList.CRLegID = @CRLegID
	AND CRTransportList.IsDeleted = 0
	AND CRTransportList.CRTransportListID =	CASE WHEN (@CRTransportListID <> -1)
						THEN (@CRTransportListID)
						ELSE (SELECT CRTransportListID FROM CRTransportList 
								WHERE CustomerID = @CustomerID
										AND CRLegID = @CRLegID
										AND IsDeleted = 0)
						END
END
GO