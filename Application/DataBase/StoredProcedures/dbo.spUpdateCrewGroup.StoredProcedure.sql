
/****** Object:  StoredProcedure [dbo].[spUpdateCrewGroup]    Script Date: 01/24/2013 13:00:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewGroup]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCrewGroup]    Script Date: 01/24/2013 13:00:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateCrewGroup](@CrewGroupID BIGINT,
											@CrewGroupCD CHAR(4),
											@CustomerID BIGINT,
											@CrewGroupDescription  VARCHAR(30),
											@LastUpdUID VARCHAR(30),
											@LastUpdTS DATETIME,
											@HomeBaseID BIGINT,
											@IsDeleted BIT,
											@IsInActive BIT )  
AS  
BEGIN   
SET NOCOUNT ON  
IF @IsDeleted=0  
BEGIN  
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
UPDATE [CrewGroup] SET  [CrewGroupID]=@CrewGroupID
					   ,[CrewGroupCD]=@CrewGroupCD
					   ,[CustomerID]=@CustomerID
					   ,[CrewGroupDescription]=@CrewGroupDescription
					   ,[LastUpdUID]=@LastUpdUID
					   ,[LastUpdTS]=@LastUpdTS
					   ,[HomebaseID]=@HomeBaseID
					   ,[IsInActive] = @IsInActive
                  WHERE CrewGroupID=@CrewGroupID 
  
END  
ELSE  
BEGIN  
UPDATE  [CrewGroup]  SET [CrewGroupID]=@CrewGroupID
					   ,[CrewGroupCD]=@CrewGroupCD
					   ,[CustomerID]=@CustomerID
					   ,[CrewGroupDescription]=@CrewGroupDescription
					   ,[LastUpdUID]=@LastUpdUID
					   ,[LastUpdTS]=@LastUpdTS
					   ,[HomebaseID]=@HomeBaseID
					   ,[IsInActive] = @IsInActive
					   ,[IsDeleted] =1  
     
 
             
            WHERE CustomerID =@CustomerID and  CrewGroupCD=@CrewGroupCD  
  
END  
END

GO


