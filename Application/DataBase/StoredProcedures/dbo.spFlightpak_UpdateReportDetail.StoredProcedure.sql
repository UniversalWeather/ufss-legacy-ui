/****** Object:  StoredProcedure [dbo].[spFlightpak_UpdateReportDetail]    Script Date: 08/13/2013 17:35:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_UpdateReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_UpdateReportDetail]
GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_UpdateReportDetail]    Script Date: 08/13/2013 17:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_UpdateReportDetail]
(
@CustomerID BIGINT,
@TripSheetReportHeaderID BIGINT,
@ReportNumber INT
)
AS 
-- =============================================          
-- Author:Karthikeyan          
-- Create date: 13/08/2013          
-- Description: Update the Report Sheet Detail       
-- ============================================= 
BEGIN

UPDATE TripSheetReportDetail SET IsSelected = 1 WHERE CustomerID = @CustomerID AND TripSheetReportHeaderID = @TripSheetReportHeaderID AND IsDeleted = 0 AND ReportNumber = @ReportNumber
 
 END

GO

