
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeleteScheduleCalenderPreferences]    Script Date: 10/03/2013 23:10:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_DeleteScheduleCalenderPreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_DeleteScheduleCalenderPreferences]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeleteScheduleCalenderPreferences]    Script Date: 10/03/2013 23:10:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_DeleteScheduleCalenderPreferences]
(
	@UserName VARCHAR(30),
	@CustomerID BIGINT
)  
AS          
SET NOCOUNT ON;          

BEGIN
	
	DELETE FROM dbo.PreflightScheduleCalenderPreferences WHERE RTRIM(UserName) = RTRIM(@UserName) AND CustomerID = @CustomerID
	   
END 
  

GO


