GO
/****** Object:  StoredProcedure [dbo].[spGetFlightPurposeForClient]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFlightPurposeForClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFlightPurposeForClient]
GO

CREATE PROCEDURE [dbo].[spGetFlightPurposeForClient]
(@CustomerID BIGINT
,@ClientID BIGINT)
AS BEGIN
	SET NOCOUNT ON      
    IF (@ClientID > 0)
    BEGIN
		SELECT	FlightPurposeID
				,FlightPurposeCD
				,FlightPurposeDescription
				,CustomerID
				,ClientID
				,IsDeleted
				,IsInactive
		FROM	FlightPurpose
		WHERE	CustomerID = @CustomerID AND
				ISNULL(IsDeleted,0) = 0 AND
				ClientID=@ClientID 
		ORDER BY FlightPurposeCD ASC
	END
	ELSE
	BEGIN    
		SELECT	FlightPurposeID
				,FlightPurposeCD
				,FlightPurposeDescription
				,CustomerID
				,ClientID
				,IsDeleted
				,IsInactive
		FROM	FlightPurpose
		WHERE	CustomerID = @CustomerID AND
				ISNULL(IsDeleted,0) = 0
		ORDER BY FlightPurposeCD ASC
	END
END