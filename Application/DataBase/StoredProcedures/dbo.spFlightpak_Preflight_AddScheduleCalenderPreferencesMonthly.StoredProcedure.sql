
GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferencesMonthly]    Script Date: 10/15/2013 18:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferencesMonthly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferencesMonthly]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferencesMonthly]    Script Date: 10/15/2013 18:56:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Sridhar Manoharan>  
-- Create date: <01 Oct 2013>  
-- Description: <Add preflight schedule calender preferences based on username and customerid>  
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightpak_Preflight_AddScheduleCalenderPreferencesMonthly]
(
	@CustomerID bigint,
	@UserName CHAR(30), 
	@SaturdaySundayWeek bit, 
	@NoPastWeek bit,
	@CalenderType VARCHAR(25)
)     
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;    
 DECLARE  @SCPreferenceID BIGINT 
	 IF EXISTS( SELECT 1 FROM dbo.PreflightScheduleCalenderPreferences WHERE CustomerID = @CustomerID AND UserName = @UserName and CalenderType = @CalenderType)
	 BEGIN
		UPDATE dbo.PreflightScheduleCalenderPreferences SET SaturdaySundayWeek = @SaturdaySundayWeek,NoPastWeek = @NoPastWeek  WHERE CustomerID = @CustomerID AND UserName = @UserName  and CalenderType = @CalenderType
	 END
	 ELSE
	 BEGIN
	 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @SCPreferenceID OUTPUT
		-- Insert statements for procedure here  
		INSERT INTO DBO.PreflightScheduleCalenderPreferences (SCPreferenceID,UserName,CustomerID, SaturdaySundayWeek, NoPastWeek, CalenderType, CrewGroupIDS,IsCrewGroup)
		VALUES (@SCPreferenceID, @UserName, @CustomerID, @SaturdaySundayWeek, @NoPastWeek, @CalenderType, null, 0) 	
	 END
  END

GO


