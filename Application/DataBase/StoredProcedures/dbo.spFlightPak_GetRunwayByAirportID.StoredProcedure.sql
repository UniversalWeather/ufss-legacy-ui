/****** Object:  StoredProcedure [dbo].[spFlightPak_GetRunwayByAirportID]    Script Date: 01/07/2013 20:35:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetRunwayByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetRunwayByAirportID]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetRunwayByAirportID]    Script Date: 01/07/2013 20:35:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spFlightPak_GetRunwayByAirportID]   
(  
 @AirportID bigint  
)  
-- =============================================  
-- Author: Sujitha.V  
-- Create date: 16/05/2012  
-- Description: Get Runway Info   
-- =============================================  
AS  
SET NoCOUNT ON  
BEGIN  
      
DECLARE @UWACustomerID BIGINT        
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output      
  
SET NOCOUNT ON          
 DECLARE @ICAOCD VARCHAR(50)  
 DECLARE @UWAAirportId VARCHAR(50)   
      
 SELECT @ICAOCD =  icaoid FROM Airport WHERE AirportID = @AirportID    
 SELECT @UWAAirportId =  AirportID FROM Airport WHERE CustomerID = @UWACustomerID and IcaoID = @ICAOCD  
SELECT  
           
            [RunwayID]  
           ,[CustomerID]  
           ,[AirportID]  
           ,[UWAID]  
           ,[UWARunwayID]  
           ,[RunwayLength]  
           ,[RunwayWidth]  
           ,[Surface]  
           ,[Lights]  
           ,[InstLandSysType]  
           ,[NavigationAids]  
           ,[UWAUpdates]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]  
           ,[IsInActive]
          
FROM Runway WHERE AirportID=@UWAAirportId  and IsDeleted = 0  
ORDER BY RunwayLength DESC  
   
END  
GO


