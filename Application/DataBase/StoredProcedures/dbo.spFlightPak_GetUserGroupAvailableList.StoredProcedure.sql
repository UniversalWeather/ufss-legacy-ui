/****** Object:  StoredProcedure [dbo].[spFlightPak_GetUserGroupAvailableList]    Script Date: 01/18/2013 21:17:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetUserGroupAvailableList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetUserGroupAvailableList]
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetUserGroupAvailableList]         
 (@UserName char(30)        
 ,@CustomerID bigint)        
AS        
--SET NOCOUNT ON;        
BEGIN        
          
--SELECT [UserGroupID]        
--      ,[UserGroupCD]        
--      ,[UserGroupDescription]        
--      ,[CustomerID]        
--      ,[LastUpdUID]        
--      ,[LastUpdTS]        
--      ,[IsDeleted]        
--FROM [UserGroup]         
--WHERE  [UserGroupID]        
-- IN(SELECT UserGroupID FROM UserGroupMapping        
--   WHERE UserName=@UserName AND  IsDeleted=''true'')         
-- AND [CustomerID]=@CustomerID  AND UserGroup.IsDeleted = ''false''           
         
--UNION        
         
SELECT [UserGroupID]        
      ,[UserGroupCD]        
      ,[UserGroupDescription]        
      ,[CustomerID]        
      ,[LastUpdUID]        
      ,[LastUpdTS]        
      ,[IsDeleted]   
      ,[IsInActive]    
FROM [UserGroup]         
WHERE  [UserGroupID]        
 NOT IN(SELECT UserGroupID FROM UserGroupMapping        
    WHERE UserName=@UserName AND  IsDeleted='false')         
 AND [CustomerID]=@CustomerID  AND UserGroup.IsDeleted = 'false'       
End  
GO


