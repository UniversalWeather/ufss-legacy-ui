IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQPassenger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQPassenger]
(
	@CQPassengerID bigint,
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@PassengerRequestorID bigint,
	@PassengerName varchar(63),
	@FlightPurposeID bigint,
	@Billing varchar(25),
	@IsNonPassenger bit,
	@IsBlocked bit,
	@OrderNUM int,
	@WaitList char(1),
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@PassportID bigint
	
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
UPDATE [CQPassenger] SET  [QuoteID] = @QuoteID, [LegID] = @LegID, [PassengerRequestorID] = @PassengerRequestorID, [PassengerName] = @PassengerName, [FlightPurposeID] = @FlightPurposeID, [Billing] = @Billing, [IsNonPassenger] = @IsNonPassenger, [IsBlocked] = @IsBlocked, [OrderNUM] = @OrderNUM, [WaitList] = @WaitList, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, [PassportID] = @PassportID
 WHERE [CQPassengerID] = @CQPassengerID AND [CustomerID] = @CustomerID AND [CQLegID] = @CQLegID
	
GO
