
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQCateringList]    Script Date: 03/07/2013 17:40:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQCateringList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQCateringList]    Script Date: 03/07/2013 17:40:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQCateringList]
(
	
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@CateringID bigint,
	@CQCateringListDescription varchar(60),
	@PhoneNUM varchar(25),
	@Rate numeric(17, 2),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQCateringListID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQCateringListID OUTPUT 
INSERT INTO [CQCateringList] ([CQCateringListID], [CustomerID], [CQLegID], [QuoteID], [LegID], [RecordType], [CateringID], [CQCateringListDescription], [PhoneNUM], [Rate], [AirportID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted],[FaxNUM],[Email]) VALUES (@CQCateringListID, @CustomerID, @CQLegID, @QuoteID, @LegID, @RecordType, @CateringID, @CQCateringListDescription, @PhoneNUM, @Rate, @AirportID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@FaxNUM, @Email);
select @CQCateringListID as 'CQCateringListID'


GO


