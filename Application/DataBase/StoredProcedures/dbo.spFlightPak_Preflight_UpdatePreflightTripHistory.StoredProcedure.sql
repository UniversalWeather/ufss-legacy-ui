/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightTripHistory]    Script Date: 01/23/2013 17:25:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightTripHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightTripHistory]
GO
  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightTripHistory](  
  @TripID bigint  
 ,@CustomerID bigint  
 ,@HistoryDescription varchar(MAX)  
 ,@LastUpdUID varchar(30)  
 ,@LastUpdTS datetime 
 ,@RevisionNumber int 
)      
AS      
BEGIN
SET NOCOUNT OFF
 UPDATE [dbo].[PreflightTripHistory] SET
  [HistoryDescription] = @HistoryDescription,
  [LastUpdUID]=@LastUpdUID,
  [LastUpdTS]=@LastUpdTS 
  
  WHERE TripID=@TripID and CustomerID=@CustomerID and RevisionNumber=@RevisionNumber
END  
  
  

GO