
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQFile]
(
	@CustomerID bigint,
	@FileNUM int,
	@EstDepartureDT date,
	@QuoteDT date,
	@HomebaseID bigint,
	@CQFileDescription varchar(40),
	@CQCustomerID bigint,
	@CQCustomerName varchar(40),
	@CQCustomerContactID bigint,
	@CQCustomerContactName varchar(40),
	@PhoneNUM varchar(25),
	@FaxNUM varchar(25),
	@IsApplicationFiled bit,
	@IsApproved bit,
	@Credit numeric(17, 2),
	@DiscountPercentage numeric(5, 2),
	@SalesPersonID bigint,
	@LeadSourceID bigint,
	@Notes varchar(MAX),
	@QuoteCenter int,
	@Sync char(1),
	@ExpressQuote bit,
	@IsFinancialWarning bit,
	@IsErrorMessage bit,
	@CharterQuoteDTRONExcept char(1),
	@ExchangeRateID bigint,
	@ExchangeRate numeric(10, 5),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@EmailID VARCHAR(100),
	@UseCustomFleetCharge BIT
)
AS
	SET NOCOUNT OFF;
	
	SET @LastUpdTS = GETUTCDATE()
	DECLARE  @CQFileID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQFileID OUTPUT 
	SET @FileNUM = 0
	
	--if @ExpressQuote=0
	--BEGIN
		
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CQFileNumCurrentNo',  @FileNUM  OUTPUT  
		select @FileNUM = CQFileNumCurrentNo  from [FlightpakSequence] where customerID = @CustomerID
	--END
	--else
	--BEGIN
	--	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'EQFileNumCurrentNo',  @FileNUM  OUTPUT  
	--	select @FileNUM = EQFileNumCurrentNo  from [FlightpakSequence] where customerID = @CustomerID
	--END
	
	INSERT INTO [CQFile] ([CQFileID], [CustomerID], [FileNUM], [EstDepartureDT], [QuoteDT], [HomebaseID], [CQFileDescription], [CQCustomerID], [CQCustomerName], [CQCustomerContactID], [CQCustomerContactName], [PhoneNUM], [FaxNUM], [IsApplicationFiled], [IsApproved], [Credit], [DiscountPercentage], [SalesPersonID], [LeadSourceID], [Notes], [QuoteCenter], [Sync], [ExpressQuote], [IsFinancialWarning], [IsErrorMessage], [CharterQuoteDTRONExcept], [ExchangeRateID], [ExchangeRate], [LastUpdUID], [LastUpdTS], [IsDeleted], [EmailID],[UseCustomFleetCharge]) 
		VALUES 
	(@CQFileID, @CustomerID, @FileNUM, @EstDepartureDT, @QuoteDT, @HomebaseID, @CQFileDescription, @CQCustomerID, @CQCustomerName, @CQCustomerContactID, @CQCustomerContactName, @PhoneNUM, @FaxNUM, @IsApplicationFiled, @IsApproved, @Credit, @DiscountPercentage, @SalesPersonID, @LeadSourceID, @Notes, @QuoteCenter, @Sync, @ExpressQuote, @IsFinancialWarning, @IsErrorMessage, @CharterQuoteDTRONExcept, @ExchangeRateID, @ExchangeRate, @LastUpdUID, @LastUpdTS, @IsDeleted, @EmailID, @UseCustomFleetCharge);
		
	SELECT @CQFileID as CQFileID
GO
