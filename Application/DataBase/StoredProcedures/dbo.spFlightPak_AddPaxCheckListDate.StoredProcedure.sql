

/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPaxCheckListDate]    Script Date: 03/28/2013 18:43:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddPaxCheckListDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddPaxCheckListDate]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPaxCheckListDate]    Script Date: 03/28/2013 18:43:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_AddPaxCheckListDate]    
(   @PassengerCheckListDetailID bigint,
	@PassengerAdditionalInfoID bigint,
	@PreviousCheckDT datetime,
	@CustomerID bigint,
	@PassengerID bigint,
	@DueDT datetime,
	@AlertDT datetime,
	@FrequencyMonth int,
	@AlertDays int,
	@GraceDays int,
	@GraceDT datetime,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsMonthEnd bit,
	@IsStopCALC bit,
	@IsOneTimeEvent bit,
	@IsNoConflictEvent bit,
	@IsNoChecklistREPT bit,
	@IsInActive bit,
	@Specific int,
	@IsCompleted bit,
	@OriginalDT datetime,
	@IsPrintStatus bit,
	@IsPassedDueAlert bit,
	@Frequency numeric(1, 0),
	@IsNextMonth bit,
	@IsEndCalendarYear bit,
	@IsScheduleCheck bit,
	@IsDeleted bit
	)

AS    
BEGIN  
SET NOCOUNT ON;  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @PassengerCheckListDetailID  OUTPUT     
 set @LastUpdTS = GETUTCDATE() 
 
INSERT INTO [PassengerCheckListDetail]
           ([PassengerCheckListDetailID]
           ,[PassengerAdditionalInfoID]
           ,[PreviousCheckDT]
           ,[CustomerID]
           ,[PassengerID]
           ,[DueDT]
           ,[AlertDT]
           ,[FrequencyMonth]
           ,[AlertDays]
           ,[GraceDays]
           ,[GraceDT]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsMonthEnd]
           ,[IsStopCALC]
           ,[IsOneTimeEvent]
           ,[IsNoConflictEvent]
           ,[IsNoChecklistREPT]
           ,[IsInActive]
           ,[Specific]
           ,[IsCompleted]
           ,[OriginalDT]
           ,[IsPrintStatus]
           ,[IsPassedDueAlert]
           ,[Frequency]
           ,[IsNextMonth]
           ,[IsEndCalendarYear]
           ,[IsScheduleCheck]
           ,[IsDeleted])
     VALUES
           (
           @PassengerCheckListDetailID,
	@PassengerAdditionalInfoID,
	@PreviousCheckDT,
	@CustomerID,
	@PassengerID,
	@DueDT,
	@AlertDT,
	@FrequencyMonth,
	@AlertDays,
	@GraceDays,
	@GraceDT,
	@LastUpdUID,
	@LastUpdTS,
	@IsMonthEnd,
	@IsStopCALC,
	@IsOneTimeEvent,
	@IsNoConflictEvent,
	@IsNoChecklistREPT,
	@IsInActive,
	@Specific,
	@IsCompleted,
	@OriginalDT,
	@IsPrintStatus,
	@IsPassedDueAlert,
	@Frequency,
	@IsNextMonth,
	@IsEndCalendarYear,
	@IsScheduleCheck,
	@IsDeleted
	)
END



GO


