
/****** Object:  StoredProcedure [dbo].[spAddFlightpakFuel]    Script Date: 09/27/2013 20:08:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFlightpakFuel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFlightpakFuel]
GO

/****** Object:  StoredProcedure [dbo].[spAddFlightpakFuel]    Script Date: 09/27/2013 20:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddFlightpakFuel]              
 (  --@FlightpakFuelID BIGINT            
   --,          
   @CustomerID BIGINT              
   ,@DepartureICAOID BIGINT              
   ,@FBOName VARCHAR(25)              
   ,@GallonFrom NUMERIC(17,3)              
   ,@GallonTO NUMERIC(17,3)            
   ,@EffectiveDT DATE            
   ,@UnitPrice NUMERIC(15,4)            
   ,@FuelVendorID BIGINT            
   ,@UpdateDTTM DATETIME                
   ,@LastUpdUID VARCHAR(30)              
   ,@LastUpdTS DATETIME                
   ,@IsActive BIT      
   ,@FuelVendorText VARCHAR(max)          
)              
-- =============================================              
-- Author:Srinivas.p              
-- Create date: 6/7/2012              
-- Description: Insert InTo Flight Pak Fuel            
-- Modified details : 11/09/2012: Changed from MasterModuleCurrentNo to FuelVendorCurrentNo   
-- Modified details : 27/09/2013: Added new column FuelVendorText as parameter and in insert query --Karthi
-- =============================================              
AS              
BEGIN            
--if exists(select * from  FlightpakFuel where FuelVendorID=@FuelVendorID)              
--BEGIN            
--delete from FlightpakFuel where FuelVendorID=@FuelVendorID            
--END             
--To generate PrimaryKey when CustomerId is not available set the value as 0      
IF (@CustomerID is null)        
BEGIN        
SET @CustomerID=10000       
END        
--DELETE FROM FlightpakFuel WHERE [CustomerID]=@CustomerID and [FuelVendorID]=@FuelVendorID      
DECLARE @FlightpakFuelID BIGINT            
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'FuelVendorCurrentNo',  @FlightpakFuelID OUTPUT          
--End of Primary Key generation      
      
--To store the lastupdateddate as UTC date      
DECLARE @currentTime datetime      
SET @currentTime = GETUTCDATE()      
SET @LastUpdTS = @currentTime      
--End of UTC date      
      
   INSERT INTO FlightpakFuel  (FlightpakFuelID            
          ,[CustomerID]            
          ,[DepartureICAOID]            
          ,[FBOName]            
          ,[GallonFrom]            
          ,[GallonTO]            
          ,[EffectiveDT]            
          ,[UnitPrice]            
          ,[FuelVendorID]            
          ,[UpdateDTTM]            
          ,[LastUpdUID]            
          ,[LastUpdTS]            
          ,[IsActive]  
          ,FuelVendorText            
          )              
    VALUES (@FlightpakFuelID            
          ,@CustomerID            
          ,@DepartureICAOID                      
          ,@FBOName            
          ,@GallonFrom            
          ,@GallonTO            
          ,@EffectiveDT            
          ,@UnitPrice            
          ,@FuelVendorID            
          ,@UpdateDTTM            
          ,@LastUpdUID            
          ,@LastUpdTS            
          ,@IsActive   
          ,@FuelVendorText           
          )          
              
END 
GO

