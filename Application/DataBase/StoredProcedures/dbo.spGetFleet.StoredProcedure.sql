/****** Object:  StoredProcedure [dbo].[spGetFleet]    Script Date: 03/27/2013 19:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleet]
GO

CREATE Procedure [dbo].[spGetFleet]    
(    
 @CustomerID BIGINT,     
 @ClientID BIGINT     
)                  
AS                  
-- =============================================                  
-- Author: Badrinath                  
-- Create date: 7/5/2012      
-- Modified on 20-09-12 by Mathes                
-- Description: Get All Fleet Profile Informations                  
-- =============================================                  
SET NOCOUNT ON          
BEGIN                
 IF(@ClientID>0)          
   BEGIN                
      SELECT [FleetID]                
   ,[TailNum]                
   ,Fleet.CustomerID                 
   ,Fleet.AircraftCD                
   ,Aircraft.AircraftCD as AirCraft_AircraftCD                
   ,[SerialNum]                
   ,[TypeDescription]                
   ,[MaximumReservation]                
   ,[LastInspectionDT]                
   ,[InspectionHrs]                
   ,[WarningHrs]                
   ,[MaximumPassenger]                
   ,Fleet.HomebaseID                
   ,[MaximumFuel]                
   ,[MinimumFuel]                
   ,[BasicOperatingWeight]                
   ,[MimimumRunway]                
   ,IsNull(Fleet.IsInActive,0) As IsInActive       
   ,[IsDisplay31]                
   ,[SIFLMultiControlled]                
   ,[SIFLMultiNonControled]                
   ,Fleet.Notes                
   ,[ComponentCD]                
   ,Fleet.ClientID                
   ,[FleetType]                
   ,[FlightPhoneNum]                
   ,[Class]                
   ,Fleet.VendorID                
   ,Fleet.VendorType                
   ,[YearMade]                
   ,[ExteriorColor]                
   ,[InteriorColor]                
   ,[IsAFIS]                
   ,[IsUWAData]                
   ,Fleet.LastUpdUID                
   ,Fleet.LastUpdTS                
   ,[FlightPlanCruiseSpeed]                
   ,[FlightPlanMaxFlightLevel]                
   ,[MaximumTakeOffWeight]                
   ,[MaximumLandingWeight]                
   ,[MaximumWeightZeroFuel]                
   ,[TaxiFuel]                
   ,[MultiSec]                
   ,[ForeGrndCustomColor]                
   ,[BackgroundCustomColor]                
   ,[FlightPhoneIntlNum]                
   ,[FleetSize]                
   ,[FltScanDoc]                
   ,[MinimumDay]                
   ,[StandardCrewIntl]                
   ,[StandardCrewDOM]                
   ,[IsFAR91]                
   ,[IsFAR135]                
   ,Fleet.EmergencyContactID                
   ,[IsTaxDailyAdj]                
   ,[IsTaxLandingFee]                
   ,Fleet.IsDeleted                
   ,Fleet.AircraftID                
   ,Fleet.CrewID          
   ,Fleet.SecondaryDomFlightPhone          
   ,Fleet.SecondaryIntlFlightPhone                
   ,EmergencyContact.EmergencyContactCD, Client.ClientCD, Vendor.VendorCD,   
   Fleet.marginalpercentage         
   ,Aircraft.AircraftDescription, Crew.CrewCD,Aircraft.IsFixedRotary,Fleet.NationalityCountryID,C.CountryCD                          
            FROM [Fleet]   LEFT OUTER JOIN                
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN                
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                
                      Vendor ON Fleet.VendorID = Vendor.VendorID
            LEFT OUTER JOIN Country C on C.countryid=Fleet.NationalityCountryID                      
                WHERE Fleet.CustomerID=@CustomerID AND Fleet.ClientID =@ClientID AND ISNULL(Fleet.IsDeleted,0) = 0                           
                      Order by Fleet.AircraftCD               
                        
   END        
  ELSE        
       BEGIN                
      SELECT [FleetID]                
   ,[TailNum]                
   ,Fleet.CustomerID                 
   ,Fleet.AircraftCD                
   ,Aircraft.AircraftCD as AirCraft_AircraftCD                
   ,[SerialNum]                
   ,[TypeDescription]                
   ,[MaximumReservation]           
   ,[LastInspectionDT]                
   ,[InspectionHrs]                
   ,[WarningHrs]                
   ,[MaximumPassenger]                
   ,Fleet.HomebaseID                
   ,[MaximumFuel]                
   ,[MinimumFuel]                
   ,[BasicOperatingWeight]                
   ,[MimimumRunway]               
   ,IsNull(Fleet.IsInActive,0) As IsInActive         
   ,[IsDisplay31]                
   ,[SIFLMultiControlled]                
   ,[SIFLMultiNonControled]                
   ,Fleet.Notes                
   ,[ComponentCD]                
   ,Fleet.ClientID                
   ,[FleetType]                
   ,[FlightPhoneNum]                
   ,[Class]                
   ,Fleet.VendorID                
   ,Fleet.VendorType                
   ,[YearMade]                
   ,[ExteriorColor]                
   ,[InteriorColor]                
   ,[IsAFIS]                
   ,[IsUWAData]                
   ,Fleet.LastUpdUID                
   ,Fleet.LastUpdTS                
   ,[FlightPlanCruiseSpeed]                
   ,[FlightPlanMaxFlightLevel]                
   ,[MaximumTakeOffWeight]                
   ,[MaximumLandingWeight]                
   ,[MaximumWeightZeroFuel]                
   ,[TaxiFuel]                
   ,[MultiSec]                
   ,[ForeGrndCustomColor]                
   ,[BackgroundCustomColor]                
   ,[FlightPhoneIntlNum]                
   ,[FleetSize]                
   ,[FltScanDoc]                
   ,[MinimumDay]                
   ,[StandardCrewIntl]                
   ,[StandardCrewDOM]                
   ,[IsFAR91]                
   ,[IsFAR135]                
   ,Fleet.EmergencyContactID                
   ,[IsTaxDailyAdj]                
   ,[IsTaxLandingFee]                
   ,Fleet.IsDeleted                
   ,Fleet.AircraftID                
   ,Fleet.CrewID          
   ,Fleet.SecondaryDomFlightPhone          
   ,Fleet.SecondaryIntlFlightPhone                
   ,EmergencyContact.EmergencyContactCD, Client.ClientCD, Vendor.VendorCD,Fleet.marginalpercentage,          
   Aircraft.AircraftDescription, Crew.CrewCD,Aircraft.IsFixedRotary,Fleet.NationalityCountryID,C.CountryCD               
            FROM [Fleet]   LEFT OUTER JOIN                
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN                
                      Company ON Fleet.HomebaseID = Company.HomebaseID LEFT OUTER JOIN                
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN                
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN                
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN                
                      Vendor ON Fleet.VendorID = Vendor.VendorID       
            LEFT OUTER JOIN Country C on C.countryid=Fleet.NationalityCountryID                
                WHERE Fleet.CustomerID=@CustomerID AND ISNULL(Fleet.IsDeleted,0) = 0                
                      Order by Fleet.AircraftCD               
                        
   END        
   END