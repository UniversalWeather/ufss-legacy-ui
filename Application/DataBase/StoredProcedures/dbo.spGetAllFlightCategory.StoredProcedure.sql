/****** Object:  StoredProcedure [dbo].[spGetAllFlightCategory]    Script Date: 01/07/2013 19:00:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFlightCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFlightCategory]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllFlightCategory]    Script Date: 01/07/2013 19:00:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllFlightCategory](@CustomerID bigint,@ClientID bigint)    
AS    
-- =============================================    
-- Author: Sujitha.V    
-- Create date: 16/4/2012    
-- Description: Get All Flight Category Informations    
-- =============================================    
SET NOCOUNT ON    
IF(@ClientID>0)    
BEGIN    
SELECT      [FlightCategoryID]    
           ,CASE WHEN [FlightCatagoryCD] IS NULL THEN '' ELSE LTRIM(RTRIM([FlightCatagoryCD])) END AS FlightCatagoryCD    
           ,[FlightCatagoryDescription]    
           ,[CustomerID]    
           ,[GraphColor]    
           ,[ClientID]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[ForeGrndCustomColor]    
           ,[BackgroundCustomColor]    
           ,[CallKey]    
           ,[IsInActive]    
           ,[IsDeleted]  
           ,[IsDeadorFerryHead]  
               
FROM  [FlightCatagory] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0 and ClientID=@ClientID    
order by FlightCatagoryCD  
END    
ELSE    
BEGIN    
SELECT      [FlightCategoryID]    
           ,CASE WHEN [FlightCatagoryCD] IS NULL THEN '' ELSE LTRIM(RTRIM([FlightCatagoryCD])) END AS FlightCatagoryCD       
           ,[FlightCatagoryDescription]    
           ,[CustomerID]    
           ,[GraphColor]    
           ,[ClientID]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[ForeGrndCustomColor]    
           ,[BackgroundCustomColor]    
           ,[CallKey]    
           ,[IsInActive]    
           ,[IsDeleted] 
           ,[IsDeadorFerryHead]     
               
FROM  [FlightCatagory] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0    
order by FlightCatagoryCD  
END  
GO


