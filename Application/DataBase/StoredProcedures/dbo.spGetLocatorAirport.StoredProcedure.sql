

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLocatorAirport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLocatorAirport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spGetLocatorAirport]      
(      
 @CustomerID BIGINT,       
 @AirportID BIGINT,      
 @IcaoID char(4),       
 @Iata char(3),      
 @CountryCD char(3),      
 @CountryID BIGINT,      
 @LongestRunway numeric(6,0),      
 @CityName varchar(25),      
 @MetroID BIGINT,       
 @MetroCD char(3),      
 @MilesFrom int,      
 @StateName varchar(25),      
 @AirportName varchar(25),      
 @IsHeliport bit,      
 @IsEntryPort bit,      
 @IsInActive bit       
       
 )        
AS        
-- =============================================        
-- Author:       
-- Create date:         
-- Description: Get the Locator information of Airport  
-- Modification History: Updated by Ramesh on 5th Nov 2013, for MapQuest plotting
--						 Added 2 new columns OrigLatitude and OrigLongitude				
-- =============================================        
SET FMTONLY OFF        
      
   if @IcaoID =''   
  set @IcaoID = null  
   if @Iata =''   
  set @Iata = null  
   if @CountryCD =''   
  set @CountryCD = null  
   if @CityName =''   
  set @CityName = null  
   if @MetroCD =''   
  set @MetroCD = null  
   if @StateName =''   
  set @StateName = null  
   if @AirportName =''   
  set @AirportName = null    
    
DECLARE @PointLatitude decimal(18,10)  
DECLARE @PointLongitutde decimal(18,10)   
DECLARE @CustomerID1 Bigint  
DECLARE @FromCityCenter bit = 0  
DECLARE @FromOneAirportCenter bit = 0  
DECLARE @FromOneCityCenter bit = 0  
--Added by Ramesh on 5th Nov 2013, for MapQuest plotting
DECLARE @OrigLatitude decimal(18,10)  
DECLARE @OrigLongitude decimal(18,10)   

If (@AirportID > 0)  
BEGIN  
select @CustomerID1  = customerid from airport where airportid = @AirportID   
set @CustomerID = @CustomerID1     
END  
  
-- if airport and miles are entered  
If( (@AirportID > 0 )  AND (@MilesFrom > 0 ))  
BEGIN  
 set @FromOneAirportCenter = 1  
 select @PointLatitude = LatitudeRad, @PointLongitutde = LongitudeRad, @OrigLatitude = OrigLatitude, @OrigLongitude = OrigLongitude from  
  (Select  
  case   
  when UPPER(LatitudeNorthSouth) = 'S' Then (0-(LatitudeDegree + (LatitudeMinutes / 60))) * PI() / 180  else (LatitudeDegree + (LatitudeMinutes / 60)) * PI() / 180   
  END LatitudeRad,
  case
  when UPPER(LatitudeNorthSouth) = 'S' Then (0-(LatitudeDegree + (LatitudeMinutes / 60))) else (LatitudeDegree + (LatitudeMinutes / 60))
  END OrigLatitude,  
  case  
  when UPPER(LongitudeEastWest) = 'W' Then (0-(LongitudeDegrees + (LongitudeMinutes / 60))) * PI() / 180  else (LongitudeDegrees + (LongitudeMinutes / 60)) * PI() / 180   
  END LongitudeRad, 
  case
  when UPPER(LongitudeEastWest) = 'W' Then (0-(LongitudeDegrees + (LongitudeMinutes / 60))) else (LongitudeDegrees + (LongitudeMinutes / 60))
  END OrigLongitude, 
  IcaoID,  
  CityName  
  from Airport where AirportID = @AirportID and CustomerID = @CustomerID  )   
 Temp1  
END  
  
-- if only airport enter and miles not entered  
If( ((@AirportID > 0 )  AND (@MilesFrom = 0 )) OR ((@CityName is not NULL) AND (@MilesFrom = 0 )) OR ((@CityName is NULL) AND (@AirportID =0 )))  
BEGIN  
    set @FromCityCenter = 1  
END  
  
if ( (@CityName is not NULL) AND (@MilesFrom > 0 ))  
BEGIN  
set @FromOneCityCenter = 1  
 select @PointLatitude = LatitudeRad, @PointLongitutde = LongitudeRad, @OrigLatitude = OrigLatitude, @OrigLongitude = OrigLongitude from  
  (Select  
  case   
  when UPPER(LatitudeNorthSouth) = 'S' Then (0-(LatitudeDegree + (LatitudeMinutes / 60))) * PI() / 180  else (LatitudeDegree + (LatitudeMinutes / 60)) * PI() / 180   
  END LatitudeRad, 
  case
  when UPPER(LatitudeNorthSouth) = 'S' Then (0-(LatitudeDegree + (LatitudeMinutes / 60))) else (LatitudeDegree + (LatitudeMinutes / 60))
  END OrigLatitude,  
  case  
  when UPPER(LongitudeEastWest) = 'W' Then (0-(LongitudeDegrees + (LongitudeMinutes / 60))) * PI() / 180  else (LongitudeDegrees + (LongitudeMinutes / 60)) * PI() / 180   
  END LongitudeRad,
  case
  when UPPER(LongitudeEastWest) = 'W' Then (0-(LongitudeDegrees + (LongitudeMinutes / 60))) else (LongitudeDegrees + (LongitudeMinutes / 60))
  END OrigLongitude  
       from WorldCity where   
       UPPER(CityName) = UPPER(RTRIM(LTRIM(@CityName)))  
       AND UPPER(StateName) = UPPER(RTRIM(LTRIM(@StateName)))  
       AND UPPER(CountryID) = UPPER(RTRIM(LTRIM(@CountryID)))       
       )   
 Temp1  
END  

    
--Debug  
--select @PointLatitude, @PointLongitutde, @FromCityCenter,@FromOneAirportCenter, @FromOneCityCenter, @OrigLatitude, @OrigLongitude  
  
SELECT   
 [AirportID]      
    ,[IcaoID]      
    ,[Iata]      
    , A.[CustomerID]      
 ,A.[CityName]      
 ,A.[StateName]        
 ,A.[CountryName]      
 ,A.[CountryID]      
 ,[AirportName]        
 ,[LongestRunway]      
 ,[IsEntryPort]        
 ,[IsHeliport]               
 ,A.[IsInActive]      
 ,A.[MetroID]      
 ,M.MetroCD      
 ,C.CountryCD      
 ,0 as 'Miles',  
  --start added for miles calc  
 case   
 when UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180   
 END LatitudeRad,  
 case   
 when UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) 
 END OrigLatitude,
 case  
 when UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180   
 END LongitudeRad,
 case
 when UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) 
 END OrigLongitude,
 --end added for miles calc        
--start added for miles calc  
 case   
 when UPPER(W.LatitudeNorthSouth) = 'S' Then (0-(W.LatitudeDegree + (W.LatitudeMinutes / 60))) * PI() / 180  else (W.LatitudeDegree + (W.LatitudeMinutes / 60)) * PI() / 180   
 END LatitudeRadCityCenter,  
 case  
 when UPPER(W.LongitudeEastWest) = 'W' Then (0-(W.LongitudeDegrees + (W.LongitudeMinutes / 60))) * PI() / 180  else (W.LongitudeDegrees + (W.LongitudeMinutes / 60)) * PI() / 180   
 END LongitudeRadCityCenter   
  
--end added for miles calc     
 into  #TempResultTable   
  FROM  [Airport] A --, Metro M, Country C, WorldCity W      
 LEFT OUTER JOIN [metro] M on A.MetroID = M.MetroID        
 LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID  
 LEFT OUTER JOIN  WorldCity W  on UPPER(A.CityName) = UPPER(RTRIM(LTRIM(W.CityName))) and UPPER(A.StateName) = UPPER(RTRIM(LTRIM(W.StateName))) and A.CountryID = W.CountryID AND  @FromOneAirportCenter = 0 AND  @FromOneCityCenter = 0     
 where A.CustomerID = @CustomerID -- OR A.CustomerID = 10000  
 AND ( @FromOneAirportCenter = 1 OR ISNULL(@AirportID,0)=0 OR A.AirportID = @AirportID)  
 --or ( ISNULL(A.AirportID, 0) = case when @AirportID is not null then @AirportID else ISNULL(A.AirportID, 0) end))   
 and      
 -- ISNULL(A.IcaoID, '') = case when @IcaoID is not null then @IcaoID else ISNULL(A.IcaoID, '') end and  
 ISNULL(A.Iata, '') = case when @Iata is not null then @Iata else ISNULL(A.Iata, '') end and  
 ISNULL(C.CountryCD, '') = case when @CountryCD  is not null then @CountryCD  else ISNULL(C.CountryCD , '') end   
 and ISNULL(A.LongestRunway, 0) >= case when isnull(@LongestRunway,0)=0 then ISNULL(A.LongestRunway, 0)ELSE @LongestRunway end   
 and  ((@FromOneCityCenter = 1) OR (ISNULL(A.CityName, '') = case when @CityName is not null then @CityName else ISNULL(A.CityName, '') end))  
    and ISNULL(M.MetroCD, '') = case when @MetroCD is not null then @MetroCD else ISNULL(M.MetroCD, '') end and  
 ISNULL(A.StateName, '') = case when @StateName is not null then @StateName else ISNULL(A.StateName, '') end and  
 ISNULL(A.AirportName, '') = case when @AirportName is not null then @AirportName else ISNULL(A.AirportName, '') end and  
 ISNULL(A.IsHeliport, 0) = case when isnull(@IsHeliport,0)=0 then ISNULL(A.IsHeliport, 0)ELSE @IsHeliport end  and  
 ISNULL(A.IsEntryPort, 0) = case when isnull(@IsEntryPort,0)=0 then ISNULL(A.IsEntryPort, 0)ELSE @IsEntryPort end and  
 ((A.IsInActive =@IsInActive) OR @IsInActive = 1)   
 --ISNULL(A.IsInActive, 0) = case when isnull(@IsInActive,0)=0 then ISNULL(A.IsInActive, 0)ELSE @IsInActive end   
  and (@FromOneAirportCenter = 1 OR ISNULL(@AirportID,0)=0 OR A.AirportID = @AirportID) and  
  
    A.IsDeleted = 0      
     
     
   select  
    Temp2.[AirportID]     
    ,Temp2.[IcaoID]      
    ,[Iata]      
    ,[CustomerID]      
 ,[CityName]      
 ,[StateName]        
 ,[CountryName]      
 ,[CountryID]      
 ,[AirportName]        
 ,[LongestRunway]      
 ,[IsEntryPort]        
 ,[IsHeliport]               
 ,[IsInActive]      
 ,[MetroID]      
 ,MetroCD      
 ,CountryCD ,  
 OrigLatitude,OrigLongitude,
 case   
 when @FromCityCenter = 1   
 Then  
 ISNull((cast( acos(sin(LatitudeRadCityCenter) * sin(LatitudeRad) + cos(LatitudeRadCityCenter) * cos(LatitudeRad) * cos(LongitudeRad - (LongitudeRadCityCenter))) * 3959   as Int)),0)   
 Else  
 ISNull((cast( acos(sin(@PointLatitude) * sin(LatitudeRad) + cos(@PointLatitude) * cos(LatitudeRad) * cos(LongitudeRad - (@PointLongitutde))) * 3959   as Int)),0)   
 end Miles  
 from  
   (select distinct(IcaoID), MAX(AirportID) AirportID from #TempResultTable group by IcaoID) as Temp1  
   inner join  #TempResultTable Temp2 on Temp1.IcaoID = Temp2.IcaoID and Temp1.AirportID = Temp2.AirportID  
 WHERE    
 --(@PointLatitude is NULL)   
  -- OR (@PointLongitutde is NULL)  
   @FromCityCenter = 1 OR (@MilesFrom=0 OR acos(sin(@PointLatitude) * sin(LatitudeRad) + cos(@PointLatitude) * cos(LatitudeRad) * cos(LongitudeRad - (@PointLongitutde))) * 3959  <= @MilesFrom)  
 order by AirportName;  
     
  IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 