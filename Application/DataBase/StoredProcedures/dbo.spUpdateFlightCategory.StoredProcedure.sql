/****** Object:  StoredProcedure [dbo].[spUpdateFlightCategory]    Script Date: 01/11/2013 14:36:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFlightCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFlightCategory]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFlightCategory]    Script Date: 01/11/2013 14:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spUpdateFlightCategory]
(      @FlightCategoryID bigint,
	   @FlightCatagoryCD char(4),
       @FlightCatagoryDescription varchar(25),
       @CustomerID bigint,
       @GraphColor varchar(11),
       @ClientID bigint,
       @LastUpdUID varchar(30),
       @LastUpdTS datetime,
       @ForeGrndCustomColor varchar(8),
       @BackgroundCustomColor varchar(8),
       @CallKey int,
       @IsInActive bit,
       @IsDeleted bit,
       @IsDeadorFerryHead bit
)
-- =============================================
-- Author: Sujitha.V
-- Create date: 16/4/2012
-- Description: Update the Flight Category information
-- =============================================
as
begin 
SET NoCOUNT ON
SET @LastUpdTS = GETUTCDATE() 
	UPDATE FlightCatagory
	SET 
      [FlightCatagoryDescription] = @FlightCatagoryDescription,
      [GraphColor] = @GraphColor,
      [ClientID]=@ClientID,      
      [LastUpdUID] = @LastUpdUID,
      [LastUpdTS] = @LastUpdTS,
      [ForeGrndCustomColor]=@ForeGrndCustomColor,
      [BackgroundCustomColor]=@BackgroundCustomColor,
      [IsInActive]=@IsInActive, 
      [CallKey]=@CallKey,     
      [IsDeleted] = @IsDeleted,
      [IsDeadorFerryHead]= @IsDeadorFerryHead
      
	WHERE [CustomerID] = @CustomerID and [FlightCategoryID]=@FlightCategoryID

END

GO


