GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllCateringByAirportID]    Script Date: 03/10/2014 20:44:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllCateringByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllCateringByAirportID]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllCateringByAirportID]    Script Date: 03/10/2014 20:44:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[sp_fss_GetAllCateringByAirportID]
(
   @CustomerID BIGINT, 
   @AirportID BIGINT
   ,@CateringID BIGINT  
   ,@CateringCD char(4)   
   ,@FetchChoiceOnly BIT      
   ,@FetchActiveOnly BIT  
)    
AS  
BEGIN  
SET NOCOUNT ON     
-- =============================================              
-- Author: Sridhar             
-- Create date: 10/03/2014              
-- Description: Get ALL CateringByAirportID details for Popup        
-- Exec sp_fss_GetAllCateringByAirportID 10099,1009938248,0,'',0,0  
-- Exec sp_fss_GetAllCateringByAirportID 10099,1009938248,0,'',0,1  
-- Exec sp_fss_GetAllCateringByAirportID 10099,1009938248,0,'',1,0  
-- Exec sp_fss_GetAllCateringByAirportID 10099,1009938248,1009949997,'',0,0  
-- Exec sp_fss_GetAllCateringByAirportID 10099,1009938248,0,'0002',0,0  
-- =============================================    
    
    
DECLARE @UWACustomerID BIGINT  
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
DECLARE @ICAOCD CHAR(4)  
DECLARE @UWAAirportId BIGINT   
DECLARE @ICAOCD1 CHAR(4)  
DECLARE @CustomAirportID BIGINT
   
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD     

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD 
      
SET NOCOUNT ON       
      
 SELECT          
		C.[CateringID]      
		,C.[CustomerID]       
        ,C.[AirportID]       
        ,C.[CateringCD]       
        ,C.[IsChoice]       
        ,C.[CateringVendor]        
        ,C.[PhoneNum]       
        ,C.[FaxNum]       
        ,C.[ContactName]       
        ,C.[Remarks]       
        ,C.[NegotiatedRate]       
        ,C.[LastUpdUID]       
        ,C.[LastUpdTS]       
        ,C.[UpdateDT]       
        ,C.[RecordType]       
        ,C.[SourceID]       
        ,C.[ControlNum]       
        ,ISNULL(C.IsInactive,0) IsInactive      
        ,C.[TollFreePhoneNum]       
        ,C.[WebSite]                 
        ,(SELECT CAST(CASE WHEN C.UWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END As varchar )) as Filter          
        ,C.[UWAMaintFlag]    
        ,C.[UWAID]       
        ,ISNULL(C.IsDeleted,0) IsDeleted       
        ,C.[UWAUpdates]      
        ,C.[BusinessEmail]      
        ,C.[ContactBusinessPhone]      
        ,C.[CellPhoneNum]      
        ,C.[ContactEmail]      
        ,C.[Addr1]      
        ,C.[Addr2]      
        ,C.[Addr3]      
        ,C.[City]      
        ,C.[StateProvince]       
        ,C.[MetroID]   
        ,C.[CountryID]          
		,C.ExchangeRateID    
		,C.NegotiatedTerms    
		 ,C.SundayWorkHours    
		 ,C.MondayWorkHours    
		 ,C.TuesdayWorkHours    
		 ,C.WednesdayWorkHours    
		 ,C.ThursdayWorkHours    
		 ,C.FridayWorkHours    
		 ,C.SaturdayWorkHours     
	     ,M.MetroCD      
         ,Con.CountryCD         
	FROM 
		Catering C 
			LEFT OUTER JOIN Metro M on C.MetroID = M.MetroID          
			LEFT OUTER JOIN [Country] Con on C.CountryID = Con.CountryID      
			--inner join [Airport] Ai on A.AirportID = Ai.AirportID       
       
	WHERE   
		ISNULL(C.IsDeleted,0) = 0   
		AND ((C.AirportID = @CustomAirportID AND C.CustomerID = @CustomerID) or (C.AirportID  = @UWAAirportId  AND  C.CustomerID = @CustomerID)) 
		AND C.CateringID = case when @CateringID <>0 then @CateringID else C.CateringID end   
        AND C.CateringCD   = case when Ltrim(Rtrim(@CateringCD)) <> '' then @CateringCD else C.CateringCD end   
		AND ISNULL(C.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(C.IsChoice,0) end  		
		AND ISNULL(C.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(C.IsInActive,0) end    
 ----A.AirportID = @AirportID      
-- --AND Ai.IcaoID = @AirportICAOID      
-- --AND (a.CustomerID = @CustomerID OR a.CustomerID = @UWACustomerID)      
-- --AND (Ai.CustomerID = @CustomerID OR Ai.CustomerID = @UWACustomerID)      
-- AND a.CustomerID = @CustomerID    
--AND A.IsDeleted = 0    
      
 UNION ALL    
       
SELECT          
		  C.[CateringID]      
		  ,C.[CustomerID]       
          ,C.[AirportID]       
          ,C.[CateringCD]       
          ,C.[IsChoice]       
          ,C.[CateringVendor]        
          ,C.[PhoneNum]       
          ,C.[FaxNum]       
          ,C.[ContactName]       
          ,C.[Remarks]       
          ,C.[NegotiatedRate]       
          ,C.[LastUpdUID]       
          ,C.[LastUpdTS]       
          ,C.[UpdateDT]       
          ,C.[RecordType]       
          ,C.[SourceID]       
          ,C.[ControlNum]       
          ,ISNULL(C.IsInactive,0)  IsInactive      
          ,C.[TollFreePhoneNum]       
          ,C.[WebSite]       
          ,(SELECT CAST(CASE WHEN C.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter    
          ,C.[UWAMaintFlag]       
          ,C.[UWAID]       
          ,ISNULL(C.IsDeleted,0)  IsDeleted     
          ,C.[UWAUpdates]      
          ,C.[BusinessEmail]      
          ,C.[ContactBusinessPhone]      
          ,C.[CellPhoneNum]      
          ,C.[ContactEmail]      
          ,C.[Addr1]                
          ,C.[Addr2]      
          ,C.[Addr3]      
          ,C.[City]      
          ,C.[StateProvince]       
          ,C.[MetroID]      
          ,C.[CountryID]     
          ,C.ExchangeRateID    
          ,C.NegotiatedTerms    
          ,C.SundayWorkHours    
          ,C.MondayWorkHours    
          ,C.TuesdayWorkHours    
          ,C.WednesdayWorkHours    
          ,C.ThursdayWorkHours    
          ,C.FridayWorkHours    
          ,C.SaturdayWorkHours      
          ,M.MetroCD      
          ,Con.CountryCD         
	FROM  
		Catering C 
		LEFT OUTER JOIN Metro M on C.MetroID = M.MetroID          
        LEFT OUTER JOIN Country Con on C.CountryID = Con.CountryID      
WHERE     
		C.AirportID = @UWAAirportId AND C.CustomerID = @UWACustomerID    
		AND ISNULL(C.IsDeleted,0) = 0   
		AND C.CateringID = case when @CateringID <>0 then @CateringID else C.CateringID end   
		AND C.CateringCD = case when Ltrim(Rtrim(@CateringCD)) <> '' then @CateringCD else C.CateringCD end   
		AND ISNULL(C.IsChoice,0) = case when @FetchChoiceOnly =1 then 1 else ISNULL(C.IsChoice,0) end  		
		AND ISNULL(C.IsInActive,0) = case when @FetchActiveOnly =0 then 0 else ISNULL(C.IsInActive,0) end     
		AND C.CateringCD NOT IN     
		(SELECT CateringCD FROM Catering WHERE ((AirportID  = @CustomAirportID AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId  AND  CustomerID = @CustomerID))      
		AND ISNULL(IsDeleted,0) = 0) 
  --AirportID = @AirportID      
  --AND CustomerID = @CustomerID    
  --AND IsDeleted = 0    
     
 ORDER BY C.CateringVendor
END  

GO


