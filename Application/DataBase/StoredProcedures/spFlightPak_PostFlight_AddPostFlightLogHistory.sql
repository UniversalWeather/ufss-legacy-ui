IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_PostFlight_AddPostFlightLogHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_PostFlight_AddPostFlightLogHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_PostFlight_AddPostFlightLogHistory](
	 @POLogID bigint
	,@CUSTOMERID bigint
	,@HistoryDescription varchar(MAX)
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
)    
AS    
BEGIN   
	DECLARE @LogHistoryID  BIGINT   
	DECLARE @RevisionNumber  int   
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PostflightCurrentNo',  @LogHistoryID OUTPUT     
	
	SELECT TOP 1 @RevisionNumber = isnull([RevisionNumber],0) +1 from POSTFLIGHTLOGHISTORY where POLogID = @POLogID order by LastUpdTS desc
	
	SET @RevisionNumber = ISNULL(@RevisionNumber,1)
	
	SET @LastUpdTS = GETUTCDATE()
	
	INSERT INTO POSTFLIGHTLOGHISTORY(
		LogHistoryID
		,POLogID
		,CustomerID
		,HistoryDescription
		,LastUpdUID
		,LastUpdTS
		,RevisionNumber
		)    
	VALUES(@LogHistoryID
		,@POLogID
		,@CustomerID
		,@HistoryDescription
		,@LastUpdUID
		,@LastUpdTS
		,@RevisionNumber
	)    

	SELECT @LogHistoryID as [LogHistoryID]
END







