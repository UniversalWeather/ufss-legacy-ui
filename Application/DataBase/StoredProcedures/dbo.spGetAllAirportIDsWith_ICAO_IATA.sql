USE [FSS_DEV]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllAirportIDsWith_ICAO_IATA]    Script Date: 01/01/2015 14:32:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAirportIDsWith_ICAO_IATA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAirportIDsWith_ICAO_IATA]
GO

USE [FSS_DEV]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllAirportIDsWith_ICAO_IATA]    Script Date: 01/01/2015 14:32:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Snehal Shah
-- Create date: 31-12-2014
-- Description:	Fuel File upload needs Airport ID to for relationship. Needs only Airport ID by ICAO and IATA.
--				This returns required data only.
-- =============================================
CREATE PROCEDURE [dbo].[spGetAllAirportIDsWith_ICAO_IATA]
	-- Add the parameters for the stored procedure here
	@CustomerId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF LEN(@CustomerID) >0    
     
SELECT  A.[AirportID]  
        ,A.[IcaoID]            
		,[AirportName]    
		,[Iata]
  FROM  [Airport] A     
 where A.CustomerID = @CustomerID and A.IsDeleted = 0 
union all  
SELECT  A.[AirportID]  
        ,A.[IcaoID]            
	  ,[AirportName]  
	  ,[Iata]    
      
  FROM  [Airport] A 
 where A.UwaID is not null and A.IsDeleted = 0 and A.CustomerID <> @CustomerID and A.IcaoID not in (  
Select distinct IcaoID  from Airport where CustomerID = @CustomerID and IsDeleted = 0)  

END

GO

