/****** Object:  StoredProcedure [dbo].[spAddLeadSource]    Script Date: 02/11/2013 14:57:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddLeadSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddLeadSource]
GO

/****** Object:  StoredProcedure [dbo].[spAddLeadSource]    Script Date: 02/11/2013 14:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO  
    
CREATE Procedure [dbo].[spAddLeadSource]        
(         
@CustomerID bigint        
,@LeadSourceCD char(4)        
,@LeadSourceDescription varchar(100)        
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime        
,@IsDeleted bit  
,@IsInActive bit          
)        
-- =============================================        
-- Author:Sujitha.V        
-- Create date: 12/4/2012        
-- Description: Insert the DelayType  information        
-- =============================================        
AS        
BEGIN         
SET NoCOUNT ON        
DECLARE @LeadSourceID BIGINT        
        
        
        
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @LeadSourceID OUTPUT        
DECLARE @currentTime datetime          
SET @currentTime = GETUTCDATE()          
SET @LastUpdTS = @currentTime          
        
   INSERT INTO [LeadSource]        
           (        
            LeadSourceID        
           ,CustomerID        
           ,LeadSourceCD        
           ,LeadSourceDescription        
           ,LastUpdUID         
           ,LastUpdTS                  
           ,IsDeleted  
           ,IsInActive          
             )        
     VALUES        
           (        
        @LeadSourceID              
        ,@CustomerID         
  ,@LeadSourceCD         
  ,@LeadSourceDescription        
        ,@LastUpdUID        
        ,@LastUpdTS         
        ,@IsDeleted  
        ,@IsInActive                   
   )        
 END     