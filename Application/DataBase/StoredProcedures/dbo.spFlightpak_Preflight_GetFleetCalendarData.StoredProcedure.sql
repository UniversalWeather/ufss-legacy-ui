/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_GetFleetCalendarData]    Script Date: 02/15/2013 19:11:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Preflight_GetFleetCalendarData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Preflight_GetFleetCalendarData]
GO
/*
Exec [spFlightpak_Preflight_GetFleetCalendarData] 10001,'Local','2013-01-10 00:00:00','2013-01-14 23:59:59',null, null, null, null, null, null
*/
CREATE PROCEDURE [dbo].[spFlightpak_Preflight_GetFleetCalendarData](@CustomerID bigint,  
                                @IN_STR VARCHAR(10),  
                                @START_DATE DATETIME,  
                                @END_DATE DATETIME,  
                                @Client_ID bigint, @Requestor_ID bigint, @Department_ID bigint,  
                                @Flight_Category_ID bigint, @Crew_Duty_ID bigint,  
                                @HomeBaseOnlyID bigint)  
								WITH RECOMPILE
AS  
BEGIN  
   
  SET FMTONLY OFF
IF UPPER(@IN_STR)='NONE'  
   SET @IN_STR='Local'

Declare @RONDiffDay int =365

DECLARE @CumTemp TABLE     
(       
 TripId BIGINT,     
 LegNUM BIGINT,
 CumulativeETE numeric(14,3)    
)    

DECLARE @TotalEteTemp TABLE     
(       
 TripId BIGINT,     
 TotalETE numeric(14,3)    
) 
  
-- Insert  Rest Over Night(RON) Records.  
CREATE TABLE #FleetTemp      
(    
	 ID int IDENTITY(1,1) NOT NULL,
	 TripNUM BIGINT,  
	 TripID BIGINT not null,    
	 TripStatus  CHAR(1),  
	 StartDate DATE,  
	 EndDate DATE,  
	 [Description] VARCHAR(40),  
	 Notes VARCHAR(MAX),  
	 LastUpdTS DATETIME,  
	 LastUpdUID VARCHAR(30),  
	 RecordType CHAR(1),  
	 IsLog BIT,  
	 IsPrivateTrip BIT,  
	 ArrivalAirportId BIGINT,  
	 ArrivalICAOID VARCHAR(8),  
	 ArrivalCity VARCHAR(25),  
	 ArrivalAirportName VARCHAR(25),  
	 ArrivalCountry VARCHAR(60),  
	 DepartureAirportId BIGINT,  
	 DepartureICAOID CHAR(4),  
	 DepartureCity VARCHAR(25),  
	 DepartureAirportName VARCHAR(25),  
	 DepartureCountry VARCHAR(60),  
	 FlightPurpose VARCHAR(40),  
	 ReservationAvailable INT,  
	 PassengerTotal INT,  
	 PassengerRequestorID BIGINT,  
	 FlightNUM VARCHAR(12),  
	 LegID BIGINT NOT NULL,  
	 LegNUM BIGINT,  
	 FuelLoad numeric(6,0),  
	 OutbountInstruction VARCHAR(MAX),  
	 DepartureDTTMLocal DATETIME,  
	 ArrivalDTTMLocal DATETIME,  
	 DepartureDisplayTime DATETIME,  
	 ArrivalDisplayTime DATETIME,  
	 DepartureGreenwichDTTM DATETIME,  
	 ArrivalGreenwichDTTM DATETIME,  
	 HomeDepartureDTTM DATETIME,  
	 HomeArrivalDTTM DATETIME,  
	 ElapseTM NUMERIC(7,3),  
	 IsPrivateLeg BIT,  
	 FleetID BIGINT,  
	 TailNum VARCHAR(20),  
	 FleetNotes VARCHAR(MAX),  
	 AircraftForeColor VARCHAR(8),  
	 AircraftBackColor VARCHAR(8),  
	 IsInActive BIT,  
	 PassengerRequestorCD VARCHAR(5),  
	 DepartmentID BIGINT,  
	 DepartmentCD VARCHAR(8),  
	 DepartmentName VARCHAR(25),  
	 AuthorizationID BIGINT,  
	 AuthorizationCD VARCHAR(8),  
	 ClientID BIGINT,  
	 ClientCD VARCHAR(5),  
	 FlightCategoryID BIGINT,  
	 FlightCatagoryCD CHAR(4),  
	 FlightCatagoryDescription VARCHAR(25),  
	 FlightCategoryBackColor VARCHAR(8),  
	 FlightCategoryForeColor VARCHAR(8),  
	 AircraftDutyID BIGINT,  
	 AircraftDutyCD CHAR(2),  
	 AircraftDutyDescription VARCHAR(25),  
	 AircaftDutyBackColor VARCHAR(8),  
	 AircraftDutyForeColor VARCHAR(8),  
	 IsAircraftStandby BIT,  
	 CrewDutyID BIGINT,  
	 CrewDutyTypeCD CHAR(2),  
	 CrewDutyTypeDescription VARCHAR(25),  
	 CrewDutyTypeBackColor VARCHAR(11),  
	 CrewDutyTypeForeColor VARCHAR(11),  
	 IsCrewStandBy BIT,  
	 HomebaseID BIGINT,  
	 HomebaseCD CHAR(4),  
	 [CrewCodes] VARCHAR(10) not null,  
	 [CrewFullNames] VARCHAR(10) not null,  
	 [PassengerCodes] VARCHAR(10) not null,  
	 [PassengerNames] VARCHAR(10) not null,  
	 CumulativeETE NUMERIC(14,3),
	 TotalETE NUMERIC(14,3),
	 CrewFuelLoad VARCHAR(500)
)    

INSERT INTO @TotalEteTemp(TripId, TotalETE)  
SELECT TripId, ISNULL(SUM(ElapseTM),0) FROM PreflightLeg   
WHERE (CustomerID = @CustomerID OR @CustomerID IS NULL)  
AND (DepartmentID = @Department_ID OR @Department_ID IS NULL)   
AND (PassengerRequestorID = @Requestor_ID OR @Requestor_ID IS NULL)   
AND (FlightCategoryID = @Flight_Category_ID OR @Flight_Category_ID IS NULL)   
group by  TripID   
 
-- Vishwa: To increase performance query has been reframed to check 3 major conditions in if loop and 6 minor conditions in inner queries.
-- Vishwa: To include the legs details who's start date or enddate not belongs to given date range, +- 10 logic implemented  
IF @IN_STR='Local'  
BEGIN  
       INSERT INTO #FleetTemp 
       SELECT PM.TripNUM, PM.TripID, PM.TripStatus, PM.EstDepartureDT as StartDate, PM.EstArrivalDT as EndDate, PM.TripDescription as [Description],PM.Notes,  
           PM.LastUpdTS, PM.LastUpdUID, PM.RecordType, PM.IsLog,PM.IsPrivate as IsPrivateTrip,  
     PL.ArriveICAOID AS ArrivalAirportId,AT.IcaoID AS ArrivalICAOID,  AT.CityName as ArrivalCity,AT.AirportName as ArrivalAirportName, AT.CountryName as ArrivalCountry,  
     PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
     PL.FlightPurpose, FT.MaximumPassenger - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PL.PassengerRequestorID,PL.FlightNUM, PL.LegID,PL.LegNUM, PL.FuelLoad, PL.OutbountInstruction,  
     PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,  
	 PL.DepartureDTTMLocal as DepartureDisplayTime,  -- This is added to display original time   
     PL.ArrivalDTTMLocal as ArrivalDisplayTime,  -- This is added to display original time   
     --CASE(@IN_STR) WHEN 'Local' THEN PL.DepartureDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeDepartureDTTM  
     --              WHEN 'Utc' THEN PL.DepartureGreenwichDTTM END as DepartureDisplayTime,  -- This is added to display original time  
     --CASE(@IN_STR) WHEN 'Local' THEN PL.ArrivalDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeArrivalDTTM  
     --              WHEN 'Utc' THEN PL.ArrivalGreenwichDTTM END as ArrivalDisplayTime,  -- This is added to display original time  
     PL.DepartureGreenwichDTTM,  PL.ArrivalGreenwichDTTM, PL.HomeDepartureDTTM, PL.HomeArrivalDTTM,PL.ElapseTM,PL.IsPrivate as IsPrivateLeg,  
     FT.FleetID ,FT.TailNum, FT.Notes as FleetNotes,FT.ForeGrndCustomColor as AircraftForeColor, FT.BackgroundCustomColor as AircraftBackColor,  
     VR.IsInActive,  
     PA.PassengerRequestorCD,  
     DP.DepartmentID, DP.DepartmentCD, DP.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, CL.ClientID, CL.ClientCD,  
     FC.FlightCategoryID,FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
     AD.AircraftDutyID,AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,  
     CD.DutyTypeID AS CrewDutyID,CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor,   
     CD.ForeGrndCustomColor as CrewDutyTypeForeColor, CD.IsStandby as IsCrewStandBy,  
     HM.HomebaseID, HB.IcaoID HomebaseCD, ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames],0 CumulativeETE, TE.TotalETE,PL.CrewFuelLoad
      FROM PreflightMain PM   
           INNER JOIN PreflightLeg PL ON PL.TripId = PM.TripId  
           LEFT OUTER JOIN Airport AT ON AT.AirportId = PL.ArriveICAOID   
           LEFT OUTER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID                
           LEFT OUTER JOIN   Fleet FT ON FT.FleetId = PM.FleetId AND FT.IsInActive != 1 -- inactive aircraft trips should not be shown  
           LEFT OUTER JOIN Vendor VR ON VR.VendorID = FT.VendorID  
           LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID  
           LEFT OUTER JOIN Department DP ON DP.DepartmentID = PL.DepartmentID   
           LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID  
           LEFT OUTER JOIN Client CL ON CL.ClientID = PM.ClientID    
           LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = PL.FlightCategoryID  
           LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD = PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0  
         LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0   
           LEFT OUTER JOIN Company HM ON HM.HomebaseID = PM.HomebaseID  
           LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change  
           --INNER JOIN @CumTemp T ON PL.TRIPID=T.TripID AND PL.LegNUM = T.LegNUM  
		   INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID
          WHERE  PM.CustomerID = @CustomerID   
          AND PL.DepartureDTTMLocal is not null AND PL.ArrivalDTTMLocal is not null  
      --   AND ((PL.DepartureDTTMLocal >= @START_DATE AND PL.DepartureDTTMLocal <=@END_DATE)  OR (PL.ArrivalDTTMLocal >= @START_DATE AND PL.ArrivalDTTMLocal <=@END_DATE))  
           -- AND PM.TripNUM IS NOT NULL  
          AND PL.LegNUM IS NOT NULL  
           AND PM.IsDeleted !=1 AND PL.IsDeleted != 1  
     AND (PL.PassengerRequestorID = @Requestor_ID OR @Requestor_ID IS NULL)  
     AND (PL.DepartmentID = @Department_ID OR @Department_ID IS NULL)  
     AND (PL.FlightCategoryID = @Flight_Category_ID OR @Flight_Category_ID IS NULL)  
           AND (PM.ClientID = @Client_ID OR @Client_ID IS NULL )  
           AND (PM.HomebaseID = @HomeBaseOnlyID OR @HomeBaseOnlyID IS NULL )  
           AND (CD.DutyTypeID = @Crew_Duty_ID OR @Crew_Duty_ID IS NULL)   
           --ORDER BY TripNUM ASC  
           AND ((DepartureDTTMLocal >= (@START_DATE - @RONDiffDay) AND DepartureDTTMLocal <=(@END_DATE + @RONDiffDay))  OR (ArrivalDTTMLocal >= (@START_DATE - @RONDiffDay) AND ArrivalDTTMLocal <=(@END_DATE + @RONDiffDay))  
     OR  (DepartureDTTMLocal <=(@END_DATE + @RONDiffDay)  AND ArrivalDTTMLocal >= (@START_DATE - @RONDiffDay)))  
               
END  
IF @IN_STR='Home'  
BEGIN  
       INSERT INTO #FleetTemp  
       SELECT PM.TripNUM, PM.TripID, PM.TripStatus, PM.EstDepartureDT as StartDate, PM.EstArrivalDT as EndDate, PM.TripDescription as [Description],PM.Notes,  
           PM.LastUpdTS, PM.LastUpdUID, PM.RecordType, PM.IsLog,PM.IsPrivate as IsPrivateTrip,  
     PL.ArriveICAOID AS ArrivalAirportId,AT.IcaoID AS ArrivalICAOID,  AT.CityName as ArrivalCity,AT.AirportName as ArrivalAirportName, AT.CountryName as ArrivalCountry,  
     PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
     PL.FlightPurpose, FT.MaximumPassenger - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PL.PassengerRequestorID,PL.FlightNUM, PL.LegID,PL.LegNUM, PL.FuelLoad, PL.OutbountInstruction,  
     PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,  
	 PL.HomeDepartureDTTM as DepartureDisplayTime,  -- This is added to display original time  
     PL.HomeArrivalDTTM as ArrivalDisplayTime,  -- This is added to display original time
     --CASE(@IN_STR) WHEN 'Local' THEN PL.DepartureDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeDepartureDTTM  
   --              WHEN 'Utc' THEN PL.DepartureGreenwichDTTM END as DepartureDisplayTime,  -- This is added to display original time  
     --CASE(@IN_STR) WHEN 'Local' THEN PL.ArrivalDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeArrivalDTTM  
     --              WHEN 'Utc' THEN PL.ArrivalGreenwichDTTM END as ArrivalDisplayTime,  -- This is added to display original time  
     PL.DepartureGreenwichDTTM,  PL.ArrivalGreenwichDTTM, PL.HomeDepartureDTTM, PL.HomeArrivalDTTM,PL.ElapseTM,PL.IsPrivate as IsPrivateLeg,  
     FT.FleetID ,FT.TailNum, FT.Notes as FleetNotes,FT.ForeGrndCustomColor as AircraftForeColor, FT.BackgroundCustomColor as AircraftBackColor,  
     VR.IsInActive,  
     PA.PassengerRequestorCD,  
     DP.DepartmentID, DP.DepartmentCD, DP.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, CL.ClientID, CL.ClientCD,  
     FC.FlightCategoryID,FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
     AD.AircraftDutyID,AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,  
     CD.DutyTypeID AS CrewDutyID,CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor,   
     CD.ForeGrndCustomColor as CrewDutyTypeForeColor, CD.IsStandby as IsCrewStandBy,  
     HM.HomebaseID, HB.IcaoID HomebaseCD, ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames],0 CumulativeETE, TE.TotalETE,PL.CrewFuelLoad
      FROM PreflightMain PM   
           INNER JOIN PreflightLeg PL ON PL.TripId = PM.TripId  
           LEFT OUTER JOIN Airport AT ON AT.AirportId = PL.ArriveICAOID   
           LEFT OUTER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID                
           LEFT OUTER JOIN   Fleet FT ON FT.FleetId = PM.FleetId AND FT.IsInActive != 1 -- inactive aircraft trips should not be shown  
           LEFT OUTER JOIN Vendor VR ON VR.VendorID = FT.VendorID  
           LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID  
           LEFT OUTER JOIN Department DP ON DP.DepartmentID = PL.DepartmentID   
           LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID  
           LEFT OUTER JOIN Client CL ON CL.ClientID = PM.ClientID    
           LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = PL.FlightCategoryID  
           LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD = PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0  
           LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0   
           LEFT OUTER JOIN Company HM ON HM.HomebaseID = PM.HomebaseID  
           LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change  
           --INNER JOIN @CumTemp T ON PL.TRIPID=T.TripID AND PL.LegNUM = T.LegNUM
		   INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID
          WHERE  PM.CustomerID = @CustomerID   
          AND PL.DepartureDTTMLocal is not null AND PL.ArrivalDTTMLocal is not null  
      --   AND ((PL.DepartureDTTMLocal >= @START_DATE AND PL.DepartureDTTMLocal <=@END_DATE)  OR (PL.ArrivalDTTMLocal >= @START_DATE AND PL.ArrivalDTTMLocal <=@END_DATE))  
           -- AND PM.TripNUM IS NOT NULL  
          AND PL.LegNUM IS NOT NULL  
           AND PM.IsDeleted !=1 AND PL.IsDeleted != 1  
     AND (PL.PassengerRequestorID = @Requestor_ID OR @Requestor_ID IS NULL)  
     AND (PL.DepartmentID = @Department_ID OR @Department_ID IS NULL)  
     AND (PL.FlightCategoryID = @Flight_Category_ID OR @Flight_Category_ID IS NULL)  
           AND (PM.ClientID = @Client_ID OR @Client_ID IS NULL )  
           AND (PM.HomebaseID = @HomeBaseOnlyID OR @HomeBaseOnlyID IS NULL )  
        AND (CD.DutyTypeID = @Crew_Duty_ID OR @Crew_Duty_ID IS NULL)   
           --ORDER BY TripNUM ASC  
           AND ((HomeDepartureDTTM >= (@START_DATE - @RONDiffDay) AND HomeDepartureDTTM <=(@END_DATE + @RONDiffDay))  OR (HomeArrivalDTTM >= (@START_DATE - @RONDiffDay) AND HomeArrivalDTTM <=(@END_DATE + @RONDiffDay))  
     OR  (HomeDepartureDTTM <=(@END_DATE + @RONDiffDay)  AND HomeArrivalDTTM >= (@START_DATE - @RONDiffDay)))  
               
END  
IF @IN_STR='Utc'  
BEGIN  
       INSERT INTO #FleetTemp  
       SELECT PM.TripNUM, PM.TripID, PM.TripStatus, PM.EstDepartureDT as StartDate, PM.EstArrivalDT as EndDate, PM.TripDescription as [Description],PM.Notes,  
           PM.LastUpdTS, PM.LastUpdUID, PM.RecordType, PM.IsLog,PM.IsPrivate as IsPrivateTrip,  
     PL.ArriveICAOID AS ArrivalAirportId,AT.IcaoID AS ArrivalICAOID,  AT.CityName as ArrivalCity,AT.AirportName as ArrivalAirportName, AT.CountryName as ArrivalCountry,  
     PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
     PL.FlightPurpose, FT.MaximumPassenger - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PL.PassengerRequestorID,PL.FlightNUM, PL.LegID,PL.LegNUM, PL.FuelLoad, PL.OutbountInstruction,  
     PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,  
	 PL.DepartureGreenwichDTTM as DepartureDisplayTime,  -- This is added to display original time  
     PL.ArrivalGreenwichDTTM as ArrivalDisplayTime,      -- This is added to display original time
     --CASE(@IN_STR) WHEN 'Local' THEN PL.DepartureDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeDepartureDTTM  
     --              WHEN 'Utc' THEN PL.DepartureGreenwichDTTM END as DepartureDisplayTime,  -- This is added to display original time  
     --CASE(@IN_STR) WHEN 'Local' THEN PL.ArrivalDTTMLocal   
     --              WHEN  'Home' THEN PL.HomeArrivalDTTM  
     --              WHEN 'Utc' THEN PL.ArrivalGreenwichDTTM END as ArrivalDisplayTime,  -- This is added to display original time  
     PL.DepartureGreenwichDTTM,  PL.ArrivalGreenwichDTTM, PL.HomeDepartureDTTM, PL.HomeArrivalDTTM,PL.ElapseTM,PL.IsPrivate as IsPrivateLeg,  
     FT.FleetID ,FT.TailNum, FT.Notes as FleetNotes,FT.ForeGrndCustomColor as AircraftForeColor, FT.BackgroundCustomColor as AircraftBackColor,  
     VR.IsInActive,  
     PA.PassengerRequestorCD,  
     DP.DepartmentID, DP.DepartmentCD, DP.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, CL.ClientID, CL.ClientCD,  
     FC.FlightCategoryID,FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
     AD.AircraftDutyID,AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,  
     CD.DutyTypeID AS CrewDutyID,CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor,   
     CD.ForeGrndCustomColor as CrewDutyTypeForeColor, CD.IsStandby as IsCrewStandBy,  
     HM.HomebaseID, HB.IcaoID HomebaseCD, ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames],0 CumulativeETE, TE.TotalETE,PL.CrewFuelLoad
      FROM PreflightMain PM   
           INNER JOIN PreflightLeg PL ON PL.TripId = PM.TripId  
           LEFT OUTER JOIN Airport AT ON AT.AirportId = PL.ArriveICAOID   
           LEFT OUTER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID                
           LEFT OUTER JOIN   Fleet FT ON FT.FleetId = PM.FleetId AND FT.IsInActive != 1 -- inactive aircraft trips should not be shown  
           LEFT OUTER JOIN Vendor VR ON VR.VendorID = FT.VendorID  
           LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID  
           LEFT OUTER JOIN Department DP ON DP.DepartmentID = PL.DepartmentID   
           LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID  
           LEFT OUTER JOIN Client CL ON CL.ClientID = PM.ClientID    
           LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = PL.FlightCategoryID  
           LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD = PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0  
           LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0   
           LEFT OUTER JOIN Company HM ON HM.HomebaseID = PM.HomebaseID  
           LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change  
           --INNER JOIN @CumTemp T ON PL.TRIPID=T.TripID AND PL.LegNUM = T.LegNUM
		   INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID
          WHERE  PM.CustomerID = @CustomerID   
          AND PL.DepartureDTTMLocal is not null AND PL.ArrivalDTTMLocal is not null  
      --   AND ((PL.DepartureDTTMLocal >= @START_DATE AND PL.DepartureDTTMLocal <=@END_DATE)  OR (PL.ArrivalDTTMLocal >= @START_DATE AND PL.ArrivalDTTMLocal <=@END_DATE))  
           -- AND PM.TripNUM IS NOT NULL  
          AND PL.LegNUM IS NOT NULL  
           AND PM.IsDeleted !=1 AND PL.IsDeleted != 1  
     AND (PL.PassengerRequestorID = @Requestor_ID OR @Requestor_ID IS NULL)  
     AND (PL.DepartmentID = @Department_ID OR @Department_ID IS NULL)  
    AND (PL.FlightCategoryID = @Flight_Category_ID OR @Flight_Category_ID IS NULL)  
           AND (PM.ClientID = @Client_ID OR @Client_ID IS NULL )  
AND (PM.HomebaseID = @HomeBaseOnlyID OR @HomeBaseOnlyID IS NULL )  
           AND (CD.DutyTypeID = @Crew_Duty_ID OR @Crew_Duty_ID IS NULL)   
           --ORDER BY TripNUM ASC  
           AND ((DepartureGreenwichDTTM >= (@START_DATE - @RONDiffDay) AND DepartureGreenwichDTTM <=(@END_DATE + @RONDiffDay))  OR (ArrivalGreenwichDTTM >= (@START_DATE - @RONDiffDay) AND ArrivalGreenwichDTTM <=(@END_DATE + @RONDiffDay))  
     OR  (DepartureGreenwichDTTM <=(@END_DATE + @RONDiffDay)  AND ArrivalGreenwichDTTM >= (@START_DATE - @RONDiffDay)))  
               
END  
               
  --INSERT RON RECORDS  
  
insert into #FleetTemp  
  
Select   
  A.TripNUM ,  
  A.TripId ,    
  A.TripStatus,  
  A.StartDate ,  
  A.EndDate ,  
  A.[Description] ,  
  A.NOTES ,  
  A.LastUpdTS ,  
  A.LastUpdUID ,  
  'T' RecordType ,  
  A.IsLog ,  
  A.IsPrivateTrip ,  
  A.ArrivalAirportId ,  
  A.ArrivalICAOID  as ArrivalICAOID,  
  A.ArrivalCity ,  
  A.ArrivalAirportName ,  
  A.ArrivalCountry ,  
  A.DepartureAirportId,  
  A.DepartureICAOID ,  
  A.DepartureCity ,  
  A.DepartureAirportName ,  
  A.DepartureCountry ,  
  A.FlightPurpose ,  
  A.ReservationAvailable ,  
  A.PassengerTotal ,  
  A.PassengerRequestorID ,  
  A.FlightNUM ,  
  A.LegID ,  
 0000 AS LegNUM,-- A.LegNUM ,  
  A.FuelLoad ,  
  A.OutbountInstruction ,  
 DateADD(minute,1, A.ArrivalDTTMLocal) as DepartureDTTMLocal,  
 CONVERT(VARCHAR(10),A.ArrivalDTTMLocal, 101) + ' 23:59' ArrivalDTTMLocal, --23HRS : 59 MIN  
    DateADD(minute,1,A.ArrivalDisplayTime) as DepartureDisplayTime ,  
    CONVERT(VARCHAR(10),A.ArrivalDisplayTime, 101) + ' 23:59' ArrivalDisplayTime,  
    DateADD(minute,1, A.ArrivalGreenwichDTTM) as DepartureGreenwichDTTM ,  
    CONVERT(VARCHAR(10),A.ArrivalGreenwichDTTM, 101) + ' 23:59'  ArrivalGreenwichDTTM,  
    DateADD(minute,1, A.HomeArrivalDTTM) as HomeDepartureDTTM ,  
    CONVERT(VARCHAR(10),A.HomeArrivalDTTM, 101) + ' 23:59' HomeArrivalDTTM,  
  0.0 AS ElapseTM ,  
  A.IsPrivateLeg ,  
  A.FleetID ,  
  A.TailNum ,  
  A.FleetNotes ,  
  A.AircraftForeColor ,  
  A.AircraftBackColor ,  
  A.IsInActive ,  
  A.PassengerRequestorCD ,  
  A.DepartmentID ,  
  A.DepartmentCD ,  
  A.DepartmentName ,  
  A.AuthorizationID ,  
  A.AuthorizationCD ,  
  A.ClientID ,  
  A.ClientCD ,  
  A.FlightCategoryID ,  
  A.FlightCatagoryCD ,  
  A.FlightCatagoryDescription ,  
  A.FlightCategoryBackColor ,  
  A.FlightCategoryForeColor ,  
  A.AircraftDutyID ,  
  'R ' AS AircraftDutyCD,--A.AircraftDutyCD ,  
  A.AircraftDutyDescription ,  
  A.AircaftDutyBackColor ,  
  A.AircraftDutyForeColor ,  
  1 AS IsAircraftStandby,--A.IsAircraftStandby ,-- this field is used no where, so this flag can be used to find whether it is dummy appointment ie. true => dummyappointment  
  A.CrewDutyID ,  
  A.CrewDutyTypeCD ,  
  A.CrewDutyTypeDescription ,  
  A.CrewDutyTypeBackColor ,  
  A.CrewDutyTypeForeColor ,  
  A.IsCrewStandBy ,  
  A.HomebaseID ,  
  A.HomebaseCD ,  
  A.[CrewCodes] ,  
  A.[CrewFullNames] ,  
  A.[PassengerCodes] ,  
  A.[PassengerNames] ,  
  A.CumulativeETE,
  A.TotalETE,
  A.CrewFuelLoad   
 from  #FleetTemp A, #FleetTemp B  
 where A.TripId = B.TripId  
 and A.LegNUM+1 = B.LegNUM  
 and  
 (  
 DATEDIFF(DAY,A.ArrivalDTTMLocal,B.DepartureDTTMLocal ) > =  case when @IN_STR = 'Local' then  1 else 999999  end   
 or  
 DATEDIFF(DAY,A.HomeArrivalDTTM,B.HomeDepartureDTTM ) > =   case when @IN_STR = 'Home' then  1 else 999999  end   
 or  
 DATEDIFF(DAY,A.ArrivalGreenwichDTTM,B.DepartureGreenwichDTTM ) > =    case when @IN_STR = 'Utc' then  1 else 999999 end   
 )   
 and    
 isnull(A.ElapseTM,0)>0  
   
 --and DATEDIFF(DAY,  A.ArrivalGreenwichDTTM,A.DepartureGreenwichDTTM) <>0  
   
     
   
--select * from #FleetTemp --where TripId = 10009317280  
    
    
  INSERT INTO #FleetTemp(TripNUM, TripId, TripStatus, StartDate, EndDate, [Description], NOTES, LastUpdTS, LastUpdUID, RecordType, IsLog, IsPrivateTrip,   
       ArrivalAirportId, ArrivalICAOID, ArrivalCity, ArrivalAirportName, ArrivalCountry, DepartureAirportId, DepartureICAOID, DepartureCity,   
                       DepartureAirportName, DepartureCountry, FlightPurpose, ReservationAvailable, PassengerTotal, PassengerRequestorID, FlightNUM, LegID,  
                       LegNUM, FuelLoad, OutbountInstruction, ArrivalDTTMLocal, DepartureDTTMLocal, ArrivalDisplayTime, DepartureDisplayTime,   
                       ArrivalGreenwichDTTM, DepartureGreenwichDTTM, HomeArrivalDTTM, HomeDepartureDTTM, ElapseTM, IsPrivateLeg,  A.FleetID ,  
      TailNum,FleetNotes ,.AircraftForeColor ,AircraftBackColor ,IsInActive ,PassengerRequestorCD ,DepartmentID ,DepartmentCD ,  
      DepartmentName ,AuthorizationID ,AuthorizationCD ,ClientID ,ClientCD ,FlightCategoryID ,FlightCatagoryCD ,FlightCatagoryDescription ,  
      FlightCategoryBackColor ,FlightCategoryForeColor ,AircraftDutyID ,AircraftDutyCD ,AircraftDutyDescription ,AircaftDutyBackColor ,  
      AircraftDutyForeColor ,IsAircraftStandby ,CrewDutyID ,CrewDutyTypeCD ,CrewDutyTypeDescription ,CrewDutyTypeBackColor ,CrewDutyTypeForeColor ,  
      IsCrewStandBy ,HomebaseID ,HomebaseCD ,[CrewCodes] ,[CrewFullNames] ,[PassengerCodes] ,[PassengerNames] ,CumulativeETE,TotalETE,CrewFuelLoad )     
  
SELECT   
 A.TripNUM ,  
 A.TripId ,    
 A.TripStatus,  
 A.StartDate ,  
 A.EndDate ,  
 A.[Description] ,  
 A.NOTES ,  
 A.LastUpdTS ,  
 A.LastUpdUID ,  
 'T' RecordType ,  
 A.IsLog ,  
 A.IsPrivateTrip ,  
 A.ArrivalAirportId ,  
 A.ArrivalICAOID  as ArrivalICAOID,  
 A.ArrivalCity ,  
 A.ArrivalAirportName ,  
 A.ArrivalCountry ,  
 A.DepartureAirportId,  
 A.DepartureICAOID ,  
 A.DepartureCity ,  
 A.DepartureAirportName ,  
 A.DepartureCountry ,  
 A.FlightPurpose ,  
 A.ReservationAvailable ,  
 A.PassengerTotal ,  
 A.PassengerRequestorID ,  
 A.FlightNUM ,  
 A.LegID ,  
0000 AS LegNUM,-- A.LegNUM ,  
 A.FuelLoad ,  
 A.OutbountInstruction ,  
 DATEADD(MINUTE,-1,DATEADD(dd, DATEDIFF(dd, 0, B.DepartureDTTMLocal),0)) ArrivalDTTMLocal, --23HRS : 59 MIN  
 DATEADD(day,1, cast(A.ArrivalDTTMLocal  as date)) DepartureDTTMLocal,  
 DATEADD(MINUTE,-1,DATEADD(dd, DATEDIFF(dd, 0, B.DepartureDisplayTime),0)) ArrivalDisplayTime,  
 DATEADD(day,1, cast(A.ArrivalDisplayTime   as date)) DepartureDisplayTime ,  
 DATEADD(MINUTE,-1,DATEADD(dd, DATEDIFF(dd, 0, B.DepartureGreenwichDTTM),0)) ArrivalGreenwichDTTM,  
 DATEADD(day,1, cast(A.ArrivalGreenwichDTTM   as date)) DepartureGreenwichDTTM,  
 DATEADD(MINUTE,-1,DATEADD(dd, DATEDIFF(dd, 0, B.HomeDepartureDTTM),0)) HomeArrivalDTTM,  
 DATEADD(day,1, cast(A.HomeArrivalDTTM   as date)) HomeDepartureDTTM,  
 0.0 AS ElapseTM ,  
 A.IsPrivateLeg ,  
 A.FleetID ,  
 A.TailNum ,  
 A.FleetNotes ,  
 A.AircraftForeColor ,  
 A.AircraftBackColor ,  
 A.IsInActive ,  
 A.PassengerRequestorCD ,  
 A.DepartmentID ,  
 A.DepartmentCD ,  
 A.DepartmentName ,  
 A.AuthorizationID ,  
 A.AuthorizationCD ,  
 A.ClientID ,  
 A.ClientCD ,  
 A.FlightCategoryID ,  
 A.FlightCatagoryCD ,  
 A.FlightCatagoryDescription ,  
 A.FlightCategoryBackColor ,  
 A.FlightCategoryForeColor ,  
 A.AircraftDutyID ,  
 'R ' AS AircraftDutyCD,--A.AircraftDutyCD ,  
 A.AircraftDutyDescription ,  
 A.AircaftDutyBackColor ,  
 A.AircraftDutyForeColor ,  
 1 AS IsAircraftStandby,--A.IsAircraftStandby ,-- this field is used no where, so this flag can be used to find whether it is dummy appointment ie. true => dummyappointment  
 A.CrewDutyID ,  
 A.CrewDutyTypeCD ,  
 A.CrewDutyTypeDescription ,  
 A.CrewDutyTypeBackColor ,  
 A.CrewDutyTypeForeColor ,  
 A.IsCrewStandBy ,  
 A.HomebaseID ,  
 A.HomebaseCD ,  
 A.[CrewCodes] ,  
 A.[CrewFullNames] ,  
 A.[PassengerCodes] ,  
 A.[PassengerNames] ,  
 A.CumulativeETE,
 A.TotalETE,
 A.CrewFuelLoad   
  FROM #FleetTemp A INNER JOIN #FleetTemp B  
            ON A.TripId=B.TripId  
            AND A.LegNUM=B.LegNUM-1  
             WHERE A.RecordType = 'T'   
               
  
   DELETE FROM #FleetTemp WHERE ArrivalDisplayTime < DepartureDisplayTime AND LegNUM=0000
     
 INSERT INTO @CumTemp(TripId,LegNUM, CumulativeETE)  
SELECT 
	a.TripId, 	
	a.LegNUM,
	ISNULL(SUM(b.ElapseTM),0) ElapseTM 
FROM 
	#FleetTemp a,
	#FleetTemp b
WHERE a.TripId = b.TripId AND
	b.LegNum <= a.LegNum
Group By a.TripId,a.LegNum,a.ElapseTM
order by a.LegNUM 

UPDATE t 
SET t.CumulativeETE = c.CumulativeETE
from  
 #FleetTemp as t inner join @CumTemp as c on c.TripId = t.TripID and c.LegNum = t.LegNum

IF @IN_STR='Local'  
BEGIN  
	SELECT DISTINCT * FROM #FleetTemp   
	WHERE  ((DepartureDTTMLocal >= @START_DATE AND DepartureDTTMLocal <=@END_DATE)  OR (ArrivalDTTMLocal >= @START_DATE AND ArrivalDTTMLocal <=@END_DATE) OR  (DepartureDTTMLocal <=@END_DATE  AND ArrivalDTTMLocal >= @START_DATE))
	And ID Not in (
		Select A.ID from #FleetTemp As A 
		Inner Join #FleetTemp As B On 
		CONVERT(VARCHAR(10),A.DepartureDisplayTime,101)=CONVERT(VARCHAR(10),B.DepartureDisplayTime,101) 
		And A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		Where A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		And ((A.DepartureDTTMLocal >= @START_DATE AND A.DepartureDTTMLocal <=@END_DATE) OR (A.ArrivalDTTMLocal >= @START_DATE AND A.ArrivalDTTMLocal <=@END_DATE) OR (A.DepartureDTTMLocal <=@END_DATE  AND A.ArrivalDTTMLocal >= @START_DATE))
	)   
END  
ELSE IF @IN_STR='Home'  
BEGIN   
	SELECT DISTINCT * FROM #FleetTemp   
	WHERE ((HomeDepartureDTTM >= @START_DATE AND HomeDepartureDTTM <=@END_DATE)  OR (HomeArrivalDTTM >= @START_DATE AND HomeArrivalDTTM <=@END_DATE) OR (HomeDepartureDTTM <=@END_DATE  AND HomeArrivalDTTM >= @START_DATE))
	And ID Not in (
		Select A.ID from #FleetTemp As A 
		Inner Join #FleetTemp As B On CONVERT(VARCHAR(10),A.HomeDepartureDTTM,101)=CONVERT(VARCHAR(10),B.HomeDepartureDTTM,101) 
		And A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		Where A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		And ((A.HomeDepartureDTTM >= @START_DATE AND A.HomeDepartureDTTM <=@END_DATE)  OR (A.HomeArrivalDTTM >= @START_DATE AND A.HomeArrivalDTTM <=@END_DATE) OR (A.HomeDepartureDTTM <=@END_DATE  AND A.HomeArrivalDTTM >= @START_DATE))
	 )
	    
END      
ELSE IF @IN_STR='Utc'  
BEGIN     
    SELECT DISTINCT * FROM #FleetTemp   
    WHERE  ((DepartureGreenwichDTTM >= @START_DATE AND DepartureGreenwichDTTM <=@END_DATE) OR (ArrivalGreenwichDTTM >= @START_DATE AND ArrivalGreenwichDTTM <=@END_DATE) OR (DepartureGreenwichDTTM <=@END_DATE  AND ArrivalGreenwichDTTM >= @START_DATE))   
	And ID Not in (
		Select A.ID from #FleetTemp As A 
		Inner Join #FleetTemp As B On 
		CONVERT(VARCHAR(10),A.DepartureGreenwichDTTM,101)=CONVERT(VARCHAR(10),B.DepartureGreenwichDTTM,101) 
		And A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		Where A.LegNum=0 And B.LegNum>0 And A.TripID=B.TripID
		And ((A.DepartureGreenwichDTTM >= @START_DATE AND A.DepartureGreenwichDTTM <=@END_DATE)  OR (A.ArrivalGreenwichDTTM >= @START_DATE AND A.ArrivalGreenwichDTTM <=@END_DATE) OR (A.DepartureGreenwichDTTM <=@END_DATE  AND A.ArrivalGreenwichDTTM >= @START_DATE))
	) 

END    
--SELECT DISTINCT * FROM #FleetTemp   

IF OBJECT_ID('tempdb..#FleetTemp') IS NOT NULL
	DROP TABLE #FleetTemp

END  
GO