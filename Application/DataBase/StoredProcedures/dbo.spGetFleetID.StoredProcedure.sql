

/****** Object:  StoredProcedure [dbo].[spGetFleetID]    Script Date: 10/09/2013 15:05:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetID]
GO



/****** Object:  StoredProcedure [dbo].[spGetFleetID]    Script Date: 10/09/2013 15:05:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author: Mathes  
-- Create date: 23/7/2012   
-- Modified on 20-09-12 by Mathes   
-- Description: Get Lastly SAVED FleetID  
-- =============================================    
CREATE Procedure [dbo].[spGetFleetID]  
                ( @CustomerID BIGINT,  
                  @TailNum VARCHAR(9),  
                  @SerialNum VARCHAR(20)   
                )    
AS    
SET NOCOUNT ON    
    
SELECT Top 1 FleetID FROM Fleet WHERE CustomerID=@CustomerID AND  
                                      TailNum=@TailNum       AND    
                                      SerialNum=@SerialNum   AND   
                                      ISNULL(IsDeleted,0) = 0  
                                      ORDER BY FleetID DESC  
GO


