IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_GetAllPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_GetAllPassenger]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Postflight_GetAllPassenger]    
(@CustomerID bigint,
@ClientID bigint = NULL
)  
AS    
BEGIN    
-- ====================================================    
-- Author: Mohanraja C
-- Create date: 02/08/2012    
-- Description: Get All Passenger Requestor Information and 
-- Linked Passport Table for Postflight PAX Tab
-- 10-09-2012 : Updated Client ID 
-- 21-09-2021 : IsDeleted Condition added
-- 18-10-2012 : Added Homebase CD - Narasim
-- 20-12-2012 : Added additional fields from Passport
-- 06-05-2013 - Added DepartPercentage column for BRM Item No. 18
-- ====================================================    

 SET NOCOUNT ON; 
 
SELECT     Passenger.PassengerRequestorID  
           ,Passenger.PassengerRequestorCD  
           ,Passenger.PassengerName  
           ,Passenger.CustomerID  
           ,Passenger.DepartmentID  
           ,Passenger.PassengerDescription  
           ,Passenger.PhoneNum  
           ,Passenger.IsEmployeeType  
           ,Passenger.StandardBilling  
           ,Passenger.AuthorizationID  
           ,Passenger.AuthorizationDescription  
           ,Passenger.Notes  
           ,Passenger.LastName  
           ,Passenger.FirstName  
           ,Passenger.MiddleInitial  
           ,Passenger.DateOfBirth  
           ,Passenger.ClientID  
           ,Passenger.AssociatedWithCD  
           ,Passenger.HomebaseID  
           ,Passenger.IsActive  
           ,Passenger.FlightPurposeID  
           ,Passenger.SSN  
           ,Passenger.Title  
           ,Passenger.SalaryLevel  
           ,Passenger.IsPassengerType  
           ,Passenger.LastUpdUID  
           ,Passenger.LastUpdTS  
           ,Passenger.IsScheduledServiceCoord  
           ,Passenger.CompanyName  
           ,Passenger.EmployeeID  
           ,Passenger.FaxNum  
           ,Passenger.EmailAddress  
           ,Passenger.PersonalIDNum  
           ,Passenger.IsSpouseDependant  
           ,Passenger.AccountID  
           ,Passenger.CountryID  
           ,Passenger.IsSIFL  
           ,Passenger.PassengerAlert  
           ,Passenger.Gender  
           ,Passenger.AdditionalPhoneNum  
           ,Passenger.IsRequestor  
           ,Passenger.CountryOfResidenceID  
           ,Passenger.PaxScanDoc  
           ,Passenger.TSAStatus  
           ,Passenger.TSADTTM  
           ,Passenger.Addr1  
           ,Passenger.Addr2  
           ,Passenger.City  
           ,Passenger.StateName  
           ,Passenger.PostalZipCD  
           ,Passenger.IsDeleted    
           ,Nationality.CountryName as NationalityName  
           ,Nationality.CountryCD as NationalityCD  
           ,Client.ClientCD  
           ,Client.ClientDescription  
           ,Account.AccountNum  
           ,Account.AccountDescription  
           ,Department.DepartmentCD  
           ,Department.DepartmentName  
           ,Department.DepartPercentage
           ,DepartmentAuthorization.AuthorizationCD  
           ,DepartmentAuthorization.DeptAuthDescription  
           ,FlightPurpose.FlightPurposeCD  
           ,FlightPurpose.FlightPurposeDescription  
           ,CountryOfResidence.CountryName as ResidenceCountryName  
           ,CountryOfResidence.CountryCD as ResidenceCountryCD  
           ,HomeBase.CountryName as HomeBaseName  
           ,HomeBase.IcaoID as HomeBaseCD  
           , Passport.PassportID
		   , Passport.CrewID
		   , Passport.Choice
           , isnull(Passport.IssueCity,'') as 'IssueCity' 
           , isnull(Passport.PassportNum,'') as 'PassportNum'
           , isnull(Passport.PassportExpiryDT,'') as 'PassportExpiryDT'
           , isnull(Passport.IsDefaultPassport,0) as 'IsDefaultPassport'
           , Passport.PilotLicenseNum
           , isnull(Passport.IssueDT,'') as 'IssueDT'
           , Passport.CountryID as 'PassportCountryID'
           , PassportCountry.CountryCD as 'Nation'
  FROM Passenger as Passenger  
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Passenger.CountryID  
  LEFT OUTER JOIN Client as Client on Client.ClientID = Passenger.ClientID  
  LEFT OUTER JOIN Account as Account on Account.AccountID = Passenger.AccountID  
  LEFT OUTER JOIN Department as Department on Department.DepartmentID = Passenger.DepartmentID  
  LEFT OUTER JOIN DepartmentAuthorization as DepartmentAuthorization on DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID  
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Passenger.HomebaseID  
  LEFT OUTER JOIN FlightPurpose as FlightPurpose on FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID  
  LEFT OUTER JOIN Country as CountryOfResidence on CountryOfResidence.CountryID = Passenger.CountryOfResidenceID  
  LEFT OUTER JOIN CrewPassengerPassport as Passport on Passport.PassengerRequestorID = Passenger.PassengerRequestorID 
					AND isnull(Passport.IsDeleted,0) = 0 AND isnull(Passport.Choice,0) = 1
  LEFT OUTER JOIN Country as PassportCountry on Passport.CountryID = PassportCountry.CountryID
 WHERE Passenger.CustomerID = @CustomerID 
 AND (isnull(@ClientID,0) = '' OR Passenger.ClientID = @ClientID) 
 AND isnull(Passenger.IsDeleted,0) = 0
 ORDER BY Passenger.PassengerRequestorCD
END

GO


