
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeletePreflightHotelList]    Script Date: 05/15/2014 11:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_DeletePreflightHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_DeletePreflightHotelList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeletePreflightHotelList]    Script Date: 05/15/2014 11:37:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Preflight_DeletePreflightHotelList]
	(	
	    @PreflightHotelListID bigint,
	    @CustomerID bigint
    )
AS
BEGIN 
SET NOCOUNT ON

Delete from PreflightHotelCrewPassengerList where PreflightHotelListID = @PreflightHotelListID 
and CustomerID= @CustomerID

Delete from PreflightHotelList where PreflightHotelListID = @PreflightHotelListID 
and CustomerID= @CustomerID

END


GO


