
GO
/****** Object:  StoredProcedure [dbo].[spDeleteAirportPairDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteAirportPairDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteAirportPairDetail]
GO

CREATE PROCEDURE spDeleteAirportPairDetail
(@AirportPairDetailID BIGINT,
@CustomerID BIGINT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE AirportPairDetail
	SET CustomerID=@CustomerID,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=@LastUpdTS,
		IsDeleted=@IsDeleted
	WHERE AirportPairDetailID=@AirportPairDetailID

END