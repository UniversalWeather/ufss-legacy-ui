
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQHotelList]    Script Date: 03/07/2013 17:41:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQHotelList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQHotelList]    Script Date: 03/07/2013 17:41:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQHotelList]
(
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@HotelID bigint,
	@CQHotelListDescription varchar(60),
	@PhoneNUM varchar(25),
	@Rate numeric(17, 2),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQHotelListID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQHotelListID OUTPUT 	
INSERT INTO [CQHotelList] ([CQHotelListID], [CustomerID], [CQLegID], [QuoteID], [LegID], [RecordType], [HotelID], [CQHotelListDescription], [PhoneNUM], [Rate], [AirportID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted],[FaxNUM],[Email]) 
	VALUES 
(@CQHotelListID, @CustomerID, @CQLegID, @QuoteID, @LegID, @RecordType, @HotelID, @CQHotelListDescription, @PhoneNUM, @Rate, @AirportID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@FaxNUM, @Email);
	
SELECT CQHotelListID, CustomerID, CQLegID, QuoteID, LegID, RecordType, HotelID, CQHotelListDescription, PhoneNUM, Rate, AirportID, FileNUM, LastUpdUID, LastUpdTS, IsDeleted,[FaxNUM],[Email] FROM CQHotelList WHERE (CQHotelListID = @CQHotelListID)

GO


