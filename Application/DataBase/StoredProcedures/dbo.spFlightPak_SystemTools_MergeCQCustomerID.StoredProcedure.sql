USE [FP_Rel2]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeCQCustomerID]    Script Date: 04/12/2013 12:11:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spFlightPak_SystemTools_MergeCQCustomerID]
(
@OldCQCustomerID BIGINT,
@NewCQCustomerCD VARCHAR(6),
@CustomerID BIGINT,
@LastUpdUID CHAR(30),
@EffectiveDate DateTime
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	if @EffectiveDate is null 
	begin
		begin Try
		begin Transaction
			update CQCustomer set 
			CQCustomerCD =@NewCQCustomerCD 
			,LastUpdTS = GETUTCDATE()
			,LastUpdUID = @LastUpdUID
			where CustomerID = @CustomerID and CQCustomerID = @OldCQCustomerID 
		
		commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	end
	else
	begin
	
		set @MergeSuccess = 0
		DECLARE @NewCQCustomerID BIGINT
	
		begin Try
		begin Transaction
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @NewCQCustomerID OUTPUT     
			INSERT INTO [dbo].[CQCustomer]    
			   ([CQCustomerID]
				,[CustomerID]
				,[CQCustomerCD]
				,[CQCustomerName]
				,[IsApplicationFiled]
				,[IsApproved]
				,[BillingName]
				,[BillingAddr1]
				,[BillingAddr2]
				,[BillingAddr3]
				,[BillingCity]
				,[BillingState]
				,[BillingZip]
				,[CountryID]
				,[BillingPhoneNum]
				,[BillingFaxNum]
				,[Notes]
				,[Credit]
				,[DiscountPercentage]
				,[IsInActive]
				,[HomebaseID]
				,[AirportID]
				,[DateAddedDT]
				,[WebAddress]
				,[EmailID]
				,[TollFreePhone]
				,[LastUpdUID]
				,[LastUpdTS]
				,[IsDeleted]
				,[CustomerType]				
				,MarginalPercentage
				)
		 select @NewCQCustomerID    			   			   
				,@CustomerID
				,@NewCQCustomerCD   
				,[CQCustomerName]
				,[IsApplicationFiled]
				,[IsApproved]
				,[BillingName]
				,[BillingAddr1]
				,[BillingAddr2]
				,[BillingAddr3]
				,[BillingCity]
				,[BillingState]
				,[BillingZip]
				,[CountryID]
				,[BillingPhoneNum]
				,[BillingFaxNum]
				,[Notes]
				,[Credit]
				,[DiscountPercentage]
				,[IsInActive]
				,[HomebaseID]
				,[AirportID]
				,[DateAddedDT]
				,[WebAddress]
				,[EmailID]
				,[TollFreePhone]
				,@LastUpdUID
				,GETUTCDATE()
				,[IsDeleted]
				,[CustomerType]
				,MarginalPercentage		
			from CQCustomer where CQCustomerID =@OldCQCustomerID  and CustomerID = @CustomerID
			
						 					 
			 --  CQCustomerContact
				UPDATE CQCustomerContact
					SET cqcustomerid=@NewCQCustomerID,
					LastUpdUID= @LastUpdUID,
					LastUpdTS= GETUTCDATE()
				WHERE cqcustomerid =@OldCQCustomerID and CustomerID=@CustomerID
			 
			 --  CQCustomerAdditionalInfo
				UPDATE CQCustomerAdditionalInfo
					SET cqcustomerid=@NewCQCustomerID,
					LastUpdUID= @LastUpdUID,
					LastUpdTS= GETUTCDATE()
				WHERE cqcustomerid =@OldCQCustomerID and CustomerID=@CustomerID
			 
			 --  CQCustomerCharterRate
				UPDATE CQCustomerCharterRate
					SET cqcustomerid=@NewCQCustomerID,
					LastUpdUID= @LastUpdUID,
					LastUpdTS= GETUTCDATE()
				WHERE cqcustomerid =@OldCQCustomerID and CustomerID=@CustomerID
			
			 --  CQFile				
				UPDATE CQFile
					SET cqcustomerid=@NewCQCustomerID,
					LastUpdUID= @LastUpdUID,
					LastUpdTS= GETUTCDATE()
				WHERE cqcustomerid =@OldCQCustomerID and CustomerID=@CustomerID
				and EstDepartureDT >= @EffectiveDate
			
			 --  PreflightMain				
				Update Preflightmain 
					set cqcustomerid = @NewCQCustomerID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()  
					from Preflightleg  PL 
				where Preflightmain.TripID = PL.TripID
					and Preflightmain.IsDeleted = 0
					and Preflightmain.EstDepartureDT >= @EffectiveDate and Preflightmain.cqcustomerid =@OldCQCustomerID and Preflightmain.CustomerID=@CustomerID
				
			 --  PreflightLeg				
				UPDATE PreflightLeg
					SET cqcustomerid=@NewCQCustomerID,
					LastUpdUID= @LastUpdUID,
					LastUpdTS= GETUTCDATE()
				WHERE cqcustomerid =@OldCQCustomerID and CustomerID=@CustomerID
				and DepartureDTTMLocal >= @EffectiveDate	
			 			 			
		     --  CQCustomer
				UPDATE CQCustomer set IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
				where CQCustomerID = @OldCQCustomerID 
				 
					
				commit transaction
				set @MergeSuccess = 1
			
			END TRY
			
			BEGIN CATCH
				set @MergeSuccess = 0
				Rollback transaction
			END CATCH
	end
	select   @MergeSuccess as 'MergeSuccess'
	
END

