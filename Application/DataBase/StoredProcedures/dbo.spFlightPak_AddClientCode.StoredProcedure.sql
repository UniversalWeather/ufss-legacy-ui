
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddClientCode]    Script Date: 08/24/2012 10:20:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddClientCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddClientCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_AddClientCode]           
 (          
--@ClientID bigint      ,  
@ClientCD char(5)        
,@ClientDescription varchar(60)        
,@CustomerID bigint        
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime          
,@IsDeleted bit 
,@IsInActive bit     
 )          
           
AS          
          
BEGIN          
           
 SET NOCOUNT ON          
  
if @CustomerID is null  
begin  
set @CustomerID = 0  
end  
         
 DECLARE @ClientID bigint        
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @ClientID OUTPUT        
   SET @LastUpdTS = GETUTCDATE()        
INSERT INTO [Client]        
           ([ClientID]        
           ,[ClientCD]        
           ,[ClientDescription]        
           ,[CustomerID]        
           ,[LastUpdUID]        
           ,[LastUpdTS]      
           ,[IsDeleted]
           ,[IsInActive])                 
     VALUES        
           (@ClientID         
     ,@ClientCD        
     ,@ClientDescription         
     ,@CustomerID         
     ,@LastUpdUID         
     ,@LastUpdTS      
     ,@IsDeleted
     ,@IsInActive)           
         
SELECT @ClientID AS ClientID      
END  
GO
