
/****** Object:  StoredProcedure [dbo].[spDeleteFlightPurpose]    Script Date: 7/15/2015 3:41:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spDeleteFlightPurpose]  
(  
 @FlightPurposeID BIGINT,  
 @LastUpdUID VARCHAR(30),  
 @LastUpdTS DATETIME,  
 @IsDeleted BIT  
)  
-- =============================================  
-- Author: Sujitha.V  
-- Create date: 17/4/2012  
-- Description: Delete the Flight Purpose information  
-- =============================================  
AS  
BEGIN   
--Check if the record is not being used anywhere in the application before Delete  
 DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)
DECLARE @RecordUsedInCompany int SET @RecordUsedInCompany = 0        
SET @ENTITYNAME = N'FlightPurpose'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @FlightPurposeID, @RecordUsed OUTPUT

SELECT @RecordUsedInCompany = COUNT(*) FROM [dbo].[Company] 
WHERE FlightPurpose IN ( SELECT DISTINCT FlightPurposeCD FROM dbo.FlightPurpose WHERE FlightPurposeID =  @FlightPurposeID)
--End of Delete Check
  
  if (@RecordUsed <> 0 OR @RecordUsedInCompany <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 ELSE  
  BEGIN  
   --To store the lastupdateddate as UTC date 
  
   SET @LastUpdTS = GETUTCDATE()  
   
   --End of UTC date
SET NOCOUNT ON  
  
 UPDATE FlightPurpose  
 SET   
      [LastUpdUID] = @LastUpdUID,  
      [LastUpdTS] = @LastUpdTS,  
      [IsDeleted] = @IsDeleted  
 WHERE [FlightPurposeID] = @FlightPurposeID    
END  
END

GO

