/****** Object:  StoredProcedure [dbo].[spGetAllClient]    Script Date: 01/07/2013 19:36:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllClient]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllClient]    Script Date: 01/07/2013 19:36:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllClient](@CustomerID BIGINT)      
as      
-- =============================================      
-- Author: Mohan Raja .C      
-- Create date: 18/4/2012      
-- Description: Get Client  Informations      
-- =============================================      
SET NOCOUNT ON      
BEGIN     
SELECT [ClientID],      
       CASE WHEN [ClientCD] IS NULL THEN '' ELSE LTRIM(RTRIM([ClientCD])) END AS [ClientCD],      
       [ClientDescription],      
       [CustomerID],      
       [LastUpdUID],      
       [LastUpdTS],    
       [IsDeleted] ,
       [IsInActive]
FROM  [Client] WHERE CustomerID=@CustomerID    and [IsDeleted] = 0  
order by ClientCD  
    
END 
GO


