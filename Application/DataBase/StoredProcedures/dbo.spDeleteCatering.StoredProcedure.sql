
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCatering]
GO
/****** Object:  StoredProcedure [dbo].[spDeleteCatering]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteCatering] (
	 @CateringID BIGINT
	,@AirportID BIGINT
	,@CateringCD CHAR(4)
	,@CustomerID BIGINT
	,@LastUpdUID VARCHAR(30)
	,@LastUpdTS DATETIME
	,@IsDeleted BIT
	
	)
	-- =============================================
	-- Author:Mullai.D
	-- Create date: 24/4/2012
	-- Altered by Karthikeyan.S 04/07/2012
	-- Description: Delete the Catering information. Set IsDeleted to false
	-- =============================================
AS
BEGIN
	SET NoCOUNT ON  
 --Check if the record is not being used anywhere in the application before Delete
DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)        
SET @ENTITYNAME = N'Catering'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @CateringID, @RecordUsed OUTPUT
--End of Delete Check

 if (@RecordUsed <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 else
   BEGIN   
 set @LastUpdTS = GETUTCDATE()
	UPDATE [Catering]
	SET LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
	WHERE CateringID = @CateringID
		
		
END
END
GO
