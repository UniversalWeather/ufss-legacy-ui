IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTravelCoordinatorRequestorSelectedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTravelCoordinatorRequestorSelectedList]
GO
/****** Object:  StoredProcedure [dbo].[spGetTravelCoordinatorRequestorSelectedList]    Script Date: 08/24/2012 10:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetTravelCoordinatorRequestorSelectedList](@CustomerID BIGINT ,@TravelCoordinatorID BIGINT)
-- =============================================
-- Author: srinivas.p
-- Create date: 08/06/2012
-- Get The Selected TravelCoordinatorRequestors
-- =============================================
as
SET NoCOUNT ON
BEGIN
SELECT DISTINCT  
		[TravelCoordinatorRequestor].[TravelCoordinatorRequestorID],
		[TravelCoordinatorRequestor].[CustomerID],
		[TravelCoordinatorRequestor].[TravelCoordinatorID],
		[TravelCoordinatorRequestor].[PassengerRequestorID],
        [TravelCoordinatorRequestor].[IsChoice],
        [TravelCoordinatorRequestor].[LastUpdUID],
        [TravelCoordinatorRequestor].[LastUpdTS],
        [TravelCoordinatorRequestor].[IsDeleted],
        [TravelCoordinatorRequestor].[IsInactive]          	
FROM  [TravelCoordinatorRequestor]
WHERE 

	  [TravelCoordinatorRequestor].[TravelCoordinatorID] in (SELECT TravelCoordinator.TravelCoordinatorID FROM TravelCoordinator 
																WHERE TravelCoordinator.TravelCoordinatorID = @TravelCoordinatorID) 
	  AND TravelCoordinatorRequestor.CustomerID = @CustomerID  
END
GO
