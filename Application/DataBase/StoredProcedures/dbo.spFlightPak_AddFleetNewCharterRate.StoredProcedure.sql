

/****** Object:  StoredProcedure [dbo].[spFlightPak_AddFleetNewCharterRate]    Script Date: 12/05/2013 15:18:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddFleetNewCharterRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddFleetNewCharterRate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_AddFleetNewCharterRate]    Script Date: 12/05/2013 15:18:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_AddFleetNewCharterRate]               
(           
 @CustomerID bigint,        
 @FleetID bigint,        
 @AircraftCD char(4),        
 @OrderNum int,        
 @FleetNewCharterRateDescription varchar(40),        
 @ChargeUnit varchar(25),        
 @NegotiatedChgUnit numeric(2, 0),        
 @BuyDOM numeric(17, 2),        
 @SellDOM numeric(17, 2),        
 @IsTaxDOM bit,        
 @IsDiscountDOM bit,        
 @BuyIntl numeric(17, 2),        
 @SellIntl numeric(17, 2),        
 @IsTaxIntl bit,        
 @IsDiscountIntl bit,        
 @StandardCrewDOM numeric(2, 0),        
 @StandardCrewIntl numeric(2, 0),        
 @MinimumDailyRequirement numeric(17, 2),        
 @YearMade char(4),        
 @ExteriorColor varchar(25),        
 @ColorIntl varchar(25),        
 @IsAFIS bit,        
 @IsUWAData bit,        
 @DBAircraftFlightChgID bigint,        
 @DSAircraftFlightChgID bigint,        
 @BuyAircraftFlightIntlID bigint,        
 @SellAircraftFlightIntlID bigint,        
 @LastUpdUID varchar(30),        
 @LastUpdTS datetime,        
 @AircraftTypeID BIGINT,    
 @CQCustomerID  BIGINT,        
 @IsDeleted bit     
)                
AS                
BEGIN              
-- =============================================                
-- Author: Karthikeyan.S                
-- Create date: 29/01/2013                
-- Description: Insert fleet Charter Details                
-- =============================================                
SET NOCOUNT OFF;                
DECLARE @FleetNewCharterRateID BIGINT    
Declare @TempOrderNum int          
             
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetNewCharterRateID OUTPUT              
            
SET @LastUpdTS = GETUTCDATE()         
    
     
IF (@OrderNum IS NULL AND @FleetID IS NOT NULL)    
BEGIN    
 SELECT @TempOrderNum = COUNT(OrderNum) + 1 FROM FleetNewCharterRate WHERE CustomerID = @CustomerID AND FleetID = @FleetID     
END    
ELSE IF (@OrderNum IS NULL AND @AircraftTypeID IS NOT NULL)    
BEGIN    
 SELECT @TempOrderNum = COUNT(OrderNum) + 1 FROM FleetNewCharterRate WHERE CustomerID = @CustomerID AND AircraftTypeID = @AircraftTypeID     
END    
ELSE IF (@OrderNum IS NULL AND @CQcustomerId IS NOT NULL)    
BEGIN    
 SELECT @TempOrderNum = COUNT(OrderNum) + 1 FROM FleetNewCharterRate WHERE CustomerID = @CustomerID AND CQCustomerID = @CQCustomerID     
 END    

if(@OrderNum is null)
BEGIN
	if @TempOrderNum <4
	begin
		set @TempOrderNum = 4
	end	
	SET @OrderNum = @TempOrderNum
END    
    
INSERT INTO [FleetNewCharterRate] (        
 FleetNewCharterRateID,                
 CustomerID,        
 FleetID,        
 AircraftCD,        
 OrderNum,        
 FleetNewCharterRateDescription,        
 ChargeUnit,        
 NegotiatedChgUnit,        
 BuyDOM,        
 SellDOM,        
 IsTaxDOM,        
 IsDiscountDOM,        
 BuyIntl,        
 SellIntl,        
 IsTaxIntl,        
 IsDiscountIntl,        
 StandardCrewDOM,        
 StandardCrewIntl,        
 MinimumDailyRequirement,        
 YearMade,        
 ExteriorColor,        
 ColorIntl,        
 IsAFIS,        
 IsUWAData,        
 DBAircraftFlightChgID,        
 DSAircraftFlightChgID,        
 BuyAircraftFlightIntlID,        
 SellAircraftFlightIntlID,        
 LastUpdUID,        
 LastUpdTS,        
 AircraftTypeID,    
 CQcustomerId,        
 IsDeleted        
 )                 
  VALUES (              
 @FleetNewCharterRateID,                
 @CustomerID,        
 @FleetID,        
 @AircraftCD,        
 @OrderNum,        
 @FleetNewCharterRateDescription,        
 @ChargeUnit,        
 @NegotiatedChgUnit,        
 @BuyDOM,        
 @SellDOM,        
 @IsTaxDOM,        
 @IsDiscountDOM,        
 @BuyIntl,        
 @SellIntl,        
 @IsTaxIntl,        
 @IsDiscountIntl,        
 @StandardCrewDOM,        
 @StandardCrewIntl,        
 @MinimumDailyRequirement,        
 @YearMade,        
 @ExteriorColor,        
 @ColorIntl,        
 @IsAFIS,        
 @IsUWAData,        
 @DBAircraftFlightChgID,        
 @DSAircraftFlightChgID,        
 @BuyAircraftFlightIntlID,        
 @SellAircraftFlightIntlID,        
 @LastUpdUID,        
 @LastUpdTS,        
 @AircraftTypeID,     
 @CQcustomerId,       
 @IsDeleted        
 );              
            
               
 END 
GO


