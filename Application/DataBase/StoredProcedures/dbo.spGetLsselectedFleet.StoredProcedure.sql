/****** Object:  StoredProcedure [dbo].[spGetLsselectedFleet]    Script Date: 04/29/2013 16:06:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLsselectedFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLsselectedFleet]
GO

/****** Object:  StoredProcedure [dbo].[spGetLsselectedFleet]    Script Date: 04/29/2013 16:06:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetLsselectedFleet](@CustomerID BIGINT,@FleetGroupID BIGINT)      
AS      
SET NOCOUNT ON      
BEGIN          
    SELECT FleetGroupOrder.FleetID, Fleet.AircraftID,  FleetGroupOrder.FleetGroupID,   
           Fleet.TailNum ,Fleet.AircraftCD ,Aircraft.AircraftDescription                   
    FROM   FleetGroupOrder INNER JOIN  
           Fleet ON FleetGroupOrder.FleetID = Fleet.FleetID INNER JOIN  
           Aircraft ON Fleet.AircraftID = Aircraft.AircraftID  
    WHERE  FleetGroupOrder.FleetGroupID = @FleetGroupID AND  
		   FleetGroupOrder.CustomerID = @CustomerID AND
		   Fleet.IsDeleted = 0   
    ORDER BY FleetGroupOrder.OrderNum
END  
GO

