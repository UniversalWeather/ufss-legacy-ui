

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetUserGroupPermission]    Script Date: 08/29/2012 16:50:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetUserGroupPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetUserGroupPermission]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--drop proc   [spFlightPak_GetUserGroupPermission]

CREATE PROCEDURE [dbo].[spFlightPak_GetUserGroupPermission](@UserGroupID bigint, @CustomerID bigint)                  
AS                  
SET NOCOUNT ON;                  
                  
                   
 BEGIN                  
 IF EXISTS(Select   Top 1 CMA.CustomerID from  UserGroupPermission  CMA where UserGroupID=@UserGroupID)                    
 BEGIN            
    SELECT                    
                    
           CP.CustomerID                    
    ,M.ModuleID                          
    ,M.ModuleName           
    , CASE M.ModuleName          
 WHEN 'Database' THEN 1          
 WHEN 'Preflight' THEN 2          
 WHEN 'PostFlight' THEN 3     
 WHEN 'Charter Quote' THEN 4     
 WHEN 'Corporate Request' THEN 5           
 WHEN 'Database Reports' THEN 6           
 WHEN 'Preflight Reports' THEN 7           
 WHEN 'PostFlight Reports' THEN 8   
 WHEN 'Postflight Custom Reports' THEN 9
 WHEN 'Charter Quote Reports' THEN 10     
 WHEN 'Corporate Request Reports' THEN 11
 WHEN 'Tripsheet Reports' THEN 12
 WHEN 'Tripsheet RW Reports' THEN 13
 WHEN 'Report Writer' THEN 14
 WHEN 'Menu' THEN 15
 WHEN 'Favorites' THEN 16
 WHEN 'Scheduled Service' THEN 17
 WHEN 'Security Administration' THEN 18
 WHEN 'System Administration' THEN 19      
 WHEN 'Utility' THEN 20
 WHEN 'Utility Reports' THEN 21
 WHEN 'Universal' THEN 22 END AS SortOrder              
    ,P.UMPermissionName                   
    ,P.UMPermissionID                                    
     ,CP.CanView                  
     ,CP.CanAdd                  
     ,CP.CanEdit                  
     ,CP.CanDelete                   
     ,CP.UserGroupPermissionID                  
                       
   FROM Module M                  
   INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID AND (P.CustomerId IS NULL OR P.CustomerId = @CustomerID)                 
   INNER JOIN UserGroupPermission CP ON CP.UMPermissionID =P.UMPermissionID          
   LEFT Join CustomerModuleAccess MA ON MA.ModuleID = P.ModuleID           
   WHERE CP.CustomerID=@CustomerID and UserGroupID=@UserGroupID AND MA.CanAccess = 1  AND MA.CustomerID = @CustomerID --  
   AND M.ModuleID not in (10013)         
             
   UNION                  
                     
   SELECT  distinct CAST(0 as bigint)  as CustomerID                    
    ,M.ModuleID                          
    ,M.ModuleName           
    , CASE M.ModuleName          
 WHEN 'Database' THEN 1          
 WHEN 'Preflight' THEN 2          
 WHEN 'PostFlight' THEN 3     
 WHEN 'Charter Quote' THEN 4     
 WHEN 'Corporate Request' THEN 5           
 WHEN 'Database Reports' THEN 6           
 WHEN 'Preflight Reports' THEN 7           
 WHEN 'PostFlight Reports' THEN 8   
 WHEN 'Postflight Custom Reports' THEN 9
 WHEN 'Charter Quote Reports' THEN 10     
 WHEN 'Corporate Request Reports' THEN 11
 WHEN 'Tripsheet Reports' THEN 12
 WHEN 'Tripsheet RW Reports' THEN 13
 WHEN 'Report Writer' THEN 14
 WHEN 'Menu' THEN 15
 WHEN 'Favorites' THEN 16
 WHEN 'Scheduled Service' THEN 17
 WHEN 'Security Administration' THEN 18
 WHEN 'System Administration' THEN 19      
 WHEN 'Utility' THEN 20
 WHEN 'Utility Reports' THEN 21
 WHEN 'Universal' THEN 22 END AS SortOrder                               
    ,P.UMPermissionName                   
    ,P.UMPermissionID                                    
     ,Cast('false' as bit) as CanView                  
     ,Cast('false' as bit) as CanAdd                  
     ,Cast('false' as bit) as CanEdit                  
     ,Cast('false' as bit) as CanDelete                  
    ,CAST(0 as bigint) as UserGroupPermissionID                  
                          
    FROM  Module M                   
    INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID    AND (P.CustomerId IS NULL OR P.CustomerId = @CustomerID)                 
    --INNER JOIN UserGroupPermission CP2 ON CP2.UMPermissionID =P.UMPermissionID        
    LEFT  Join CustomerModuleAccess MA ON MA.ModuleID = P.ModuleID              
    where P.IsDeleted= 'false' AND P.UMPermissionID NOT IN (                  
     SELECT  P.UMPermissionID                   
   FROM Module M               INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID              
   INNER JOIN UserGroupPermission CP ON CP.UMPermissionID =P.UMPermissionID                   
   WHERE CP.CustomerID=@CustomerID and UserGroupID=@UserGroupID ) AND         M.ModuleID not in (10013) AND  
   MA.CanAccess = 1 AND MA.CustomerID = @CustomerID                 
   order by  P.UMPermissionName asc                 
   END                  
 ELSE                  
   BEGIN                  
   SELECT   CAST(0 as bigint)  as CustomerID                    
   ,M.ModuleID                          
   ,M.ModuleName            
   , CASE M.ModuleName          
 WHEN 'Database' THEN 1          
 WHEN 'Preflight' THEN 2          
 WHEN 'PostFlight' THEN 3     
 WHEN 'Charter Quote' THEN 4     
 WHEN 'Corporate Request' THEN 5           
 WHEN 'Database Reports' THEN 6           
 WHEN 'Preflight Reports' THEN 7           
 WHEN 'PostFlight Reports' THEN 8   
 WHEN 'Postflight Custom Reports' THEN 9
 WHEN 'Charter Quote Reports' THEN 10     
 WHEN 'Corporate Request Reports' THEN 11
 WHEN 'Tripsheet Reports' THEN 12
 WHEN 'Tripsheet RW Reports' THEN 13
 WHEN 'Report Writer' THEN 14
 WHEN 'Menu' THEN 15
 WHEN 'Favorites' THEN 16
 WHEN 'Scheduled Service' THEN 17
 WHEN 'Security Administration' THEN 18
 WHEN 'System Administration' THEN 19      
 WHEN 'Utility' THEN 20
 WHEN 'Utility Reports' THEN 21
 WHEN 'Universal' THEN 22 END AS SortOrder                         
   ,P.UMPermissionName                   
   ,P.UMPermissionID                                    
    ,Cast('false' as bit) as CanView                  
    ,Cast('false' as bit) as CanAdd                  
    ,Cast('false' as bit) as CanEdit                  
    ,Cast('false' as bit) as CanDelete        
    ,CAST(0 as bigint) as UserGroupPermissionID                                
    FROM  Module M                   
    INNER JOIN UMPermission P ON M.ModuleID =P.ModuleID   AND (P.CustomerId IS NULL OR P.CustomerId = @CustomerID)         
    LEFT Join CustomerModuleAccess MA ON MA.ModuleID = P.ModuleID              
   where P.IsDeleted= 'false'  AND MA.CanAccess = 1 AND MA.CustomerID = @CustomerID     AND  M.ModuleID not in (10013)             
   order by            
   P.UMPermissionName asc                
  END                  
                    
                    
END              
            
            
          