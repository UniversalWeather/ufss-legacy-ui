


/****** Object:  StoredProcedure [dbo].[spGetAllCountry]    Script Date: 01/31/2015 13:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCountry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCountry]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetAllCountry]  
as  

set nocount on  
  
begin   
SELECT      [CountryID]
           ,[CountryCD]  
           ,[CountryName]  
           ,[UVCD]  
           ,[IATOCD]  
           ,[LastUpdUID]  
           ,[LastUpdDT]  
           ,[IsUVFlag]  
           ,[IsInActive]  
           ,[UPDTDATE]  
           ,[ISONum]  
           ,[ISOCD]  
           ,[IsDeleted]        
  FROM  [Country]  WHERE  IsDeleted =0  AND [IsInActive] = 0
  order by CountryCD
END

GO
