IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCQMessage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCQMessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetAllCQMessage](@CustomerID bigint,@MessageTYPE Char(1),@HomebaseID bigint)    
AS    

SET NOCOUNT ON    
   
SELECT    
     CQMessageID,
     [CustomerID] ,
	[MessageTYPE], 
	[HomebaseID],
	[MessageCD], 
	[MessageDescription], 
	[MessageLine] ,
	[LastUpdUID],
	[LastUpdTS] ,
	[IsDeleted] 
               
FROM  CQMessage WHERE CustomerID=@CustomerID  and MessageTYPE = @MessageTYPE and HomebaseID = @HomebaseID
GO
