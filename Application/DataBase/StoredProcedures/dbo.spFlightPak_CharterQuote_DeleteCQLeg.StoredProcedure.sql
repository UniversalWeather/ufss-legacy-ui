
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQLeg]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQLeg]
(
	@CQLegID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	
	--// Delete the Depeendencies Tables
	DELETE FROM CQCateringList WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQFBOList WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQHotelList WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQQuoteDetail WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQInvoiceQuoteDetail WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQPassenger WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID
	DELETE FROM CQTransportList WHERE CQLegID = @CQLegID AND CustomerID = @CustomerID

	--// Delete from actual Table
	DELETE FROM CQLeg WHERE [CQLegID] = @CQLegID AND CustomerID = @CustomerID
GO


