
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQTransportList]    Script Date: 03/07/2013 17:41:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQTransportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQTransportList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQTransportList]    Script Date: 03/07/2013 17:41:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQTransportList]
(
	@CQTransportListID bigint,
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@TransportID bigint,
	@CQTransportListDescription varchar(60),
	@PhoneNUM varchar(25),
	@Rate numeric(17, 2),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
UPDATE [CQTransportList] SET  [QuoteID] = @QuoteID, [LegID] = @LegID, [RecordType] = @RecordType, [TransportID] = @TransportID, [CQTransportListDescription] = @CQTransportListDescription, [PhoneNUM] = @PhoneNUM, [Rate] = @Rate, [AirportID] = @AirportID, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted,
	[FaxNUM] = @FaxNUM,
	[Email] = @Email 
	WHERE [CQTransportListID] = @CQTransportListID AND [CustomerID] = @CustomerID AND [CQLegID] = @CQLegID
	

GO


