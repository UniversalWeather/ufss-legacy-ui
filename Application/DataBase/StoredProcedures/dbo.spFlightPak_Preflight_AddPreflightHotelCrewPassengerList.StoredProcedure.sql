
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightHotelCrewPassengerList]    Script Date: 04/01/2013 16:10:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightHotelCrewPassengerList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotelCrewPassengerList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightHotelCrewPassengerList]    Script Date: 04/01/2013 16:10:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotelCrewPassengerList]    
 (
	@PreflightHotelListID bigint ,
	@CrewID bigint ,
	@PassengerRequestorID bigint ,
	@CustomerID bigint ,
	@LastUpdUID varchar(30) ,
	@LastUpdTS datetime      
  )    
AS    
BEGIN     
  DECLARE @PreflightHotelCrewPassengerListID  BIGINT       
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo', @PreflightHotelCrewPassengerListID  OUTPUT    
        
  INSERT INTO [dbo].[PreflightHotelCrewPassengerList]    
       (	
    [PreflightHotelCrewPassengerListID] ,
	[PreflightHotelListID] ,
	[CrewID] ,
	[PassengerRequestorID] ,
	[CustomerID] ,
	[LastUpdUID] ,
	[LastUpdTS]
       )    
    VALUES    
       (    
     @PreflightHotelCrewPassengerListID ,
	@PreflightHotelListID ,
	@CrewID ,
	@PassengerRequestorID ,
	@CustomerID ,
	@LastUpdUID ,
	@LastUpdTS
    )    
            
  Select @PreflightHotelCrewPassengerListID as PreflightHotelCrewPassengerListID      
END    
    
    



GO


