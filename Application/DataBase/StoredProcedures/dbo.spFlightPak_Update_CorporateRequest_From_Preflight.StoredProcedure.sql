USE [FP_Rel2]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Update_CorporateRequest_From_Preflight]    Script Date: 02/22/2013 14:22:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Update_CorporateRequest_From_Preflight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Update_CorporateRequest_From_Preflight]
GO

USE [FP_Rel2]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Update_CorporateRequest_From_Preflight]    Script Date: 02/22/2013 14:22:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Update_CorporateRequest_From_Preflight]  
(@TripID BIGINT  
,@CustomerID BIGINT  
,@LastUpdUID VARCHAR(30))  
AS BEGIN  
  
If Exists( select * from CRMain where TripID = @TripID and CustomerID = @CustomerID)  
 Begin  
  update CRMain set AcknowledgementStatus = 'A' where TripID = @TripID and CustomerID = @CustomerID  
 End  
END
GO

