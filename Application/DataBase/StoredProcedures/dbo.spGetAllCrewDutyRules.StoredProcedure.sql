/****** Object:  StoredProcedure [dbo].[spGetAllCrewDutyRules]    Script Date: 01/07/2013 19:55:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewDutyRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewDutyRules]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCrewDutyRules]    Script Date: 01/07/2013 19:55:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllCrewDutyRules](@CustomerID bigint)  
as  
-- =============================================  
-- Author:Prakash Pandian.S  
-- Create date: 30/4/2012  
-- Description: Get the CrewDutyRules information  
-- =============================================  
set nocount on  
begin   
 SELECT     
 CrewDutyRulesID,  
 CrewDutyRuleCD,  
 CustomerID,  
 CrewDutyRulesDescription ,   
 FedAviatRegNum  ,  
 DutyDayBeingTM  ,  
 DutyDayEndTM  ,  
 MaximumDutyHrs  ,  
 MaximumFlightHrs  ,  
 MinimumRestHrs ,  
 RestMultiple,  
 RestMultipleHrs,  
 LastUpdUID,  
 LastUptTM,  
    IsDeleted,
    IsInActive   
 FROM  [CrewDutyRules] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0  
 order by CrewDutyRuleCD  
end  
GO


