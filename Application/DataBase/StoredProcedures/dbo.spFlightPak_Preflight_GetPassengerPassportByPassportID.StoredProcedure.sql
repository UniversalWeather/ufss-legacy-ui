/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPassengerPassportByPassportID]    Script Date: 01/16/2013 19:06:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPassengerPassportByPassportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPassengerPassportByPassportID]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPassengerPassportByPassportID]    Script Date: 01/16/2013 19:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      
      
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetPassengerPassportByPassportID]            
(@CustomerID BIGINT,@PassportID BIGINT)              
AS                
BEGIN                
-- ====================================================                
-- Author: Leela M              
-- Create date: 20/12/2012                
-- Description: Get Passenger Passport by Passenger Requestor ID        
-- ====================================================          
      
SELECT [CrewPassengerPassport].[PassportID]      
      ,[CrewPassengerPassport].[CrewID]      
      ,[CrewPassengerPassport].[PassengerRequestorID]      
      ,[CrewPassengerPassport].[CustomerID]      
      ,[CrewPassengerPassport].[Choice]      
      ,[CrewPassengerPassport].[IssueCity]      
      ,[CrewPassengerPassport].[PassportNum]      
      ,[CrewPassengerPassport].[PassportExpiryDT]      
      ,[CrewPassengerPassport].[CountryID]      
      ,[CrewPassengerPassport].[IsDefaultPassport]      
      ,[CrewPassengerPassport].[PilotLicenseNum]      
      ,[CrewPassengerPassport].[LastUpdUID]      
      ,[CrewPassengerPassport].[LastUpdTS]      
      ,[CrewPassengerPassport].[IssueDT]      
      ,[CrewPassengerPassport].[IsDeleted] 
       ,[CrewPassengerPassport].[IsInActive]  
  FROM [CrewPassengerPassport]       
WHERE [CrewPassengerPassport].PassportID = @PassportID and       
[CrewPassengerPassport].CustomerID=@CustomerID       
END      
GO


