
/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRLeg]    Script Date: 02/18/2014 11:21:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_UpdateCRLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRLeg]    Script Date: 02/18/2014 11:21:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

      
      
CREATE Procedure [dbo].[spFlightPak_CorporateRequest_UpdateCRLeg](      
      
            @CRLegID bigint      
           ,@CustomerID bigint      
           ,@CRMainID bigint      
           ,@TripNUM bigint      
           ,@LegNUM bigint      
           ,@DAirportID bigint      
           ,@AAirportID bigint      
           ,@DepartureCity varchar(25)      
           ,@ArrivalCity varchar(25)      
           ,@DepartureDTTMLocal datetime      
           ,@ArrivalDTTMLocal datetime      
           ,@PassengerRequestorID bigint      
           ,@DepartmentID bigint      
           ,@AuthorizationID bigint      
           ,@FlightCategoryID bigint      
           ,@IsPrivate bit      
           ,@Distance numeric(5,0)      
           ,@ElapseTM numeric(7,3)      
           ,@PassengerTotal int      
           ,@PowerSetting varchar(1)      
           ,@TakeoffBIAS numeric(6,3)     
           ,@LandingBIAS numeric(6,3)      
           ,@TrueAirSpeed numeric(3,0)      
           ,@WindsBoeingTable numeric(5,0)      
           ,@IsDepartureConfirmed bit      
           ,@IsArrivalConfirmation bit      
           ,@IsApproxTM bit      
           ,@IsScheduledServices bit      
           ,@DepartureGreenwichDTTM datetime      
           ,@ArrivalGreenwichDTTM datetime      
           ,@HomeDepartureDTTM datetime      
           ,@HomeArrivalDTTM datetime      
           ,@NegotiateTime varchar(5)      
           ,@LogBreak varchar(3)      
           ,@FlightPurpose varchar(40)      
           ,@SeatTotal int      
           ,@ReservationTotal int      
           ,@ReservationAvailable int      
           ,@WaitNUM int      
           ,@Duty_Type varchar(2)      
           ,@DutyHours numeric(12,3)      
           ,@RestHours numeric(12,3)      
           ,@FlightHours numeric(12,3)      
           ,@Notes varchar(Max)      
           ,@FuelLoad numeric(6,0)      
           ,@OutboundInstruction varchar(Max)      
           ,@DutyType char(2)      
           ,@IsCrewDiscount bit      
           ,@CrewCurrency varchar(4)      
           ,@IsDutyEnd bit      
           ,@FedAviationRegNUM varchar(3)      
           ,@LegID bigint      
           ,@WindReliability int      
           ,@CROverRide numeric(4,1)      
           ,@CheckGroup varchar(3)      
           ,@AirportAlertCD varchar(3)      
           ,@AdditionalCrew varchar(24)      
           ,@PilotInCommand varchar(3)      
           ,@SecondInCommand varchar(3)      
           ,@NextLocalDTTM datetime      
           ,@NextGMTDTTM datetime      
           ,@NextHomeDTTM datetime      
           ,@CrewNotes varchar(Max)      
           ,@LastUpdUID varchar(30)      
           ,@LastUpdTS datetime      
           ,@IsDeleted bit      
           ,@FBOID bigint      
           ,@ClientID bigint  
)      
           AS      
BEGIN       
SET NOCOUNT OFF      
        
  ------------------------------------      
  ----INSERT BEGINS HERE      
  ------------------------------       
      
UPDATE [CRLeg]      
   SET       
            
      [TripNUM] = @TripNUM      
      ,[LegNUM] = @LegNUM      
      ,[DAirportID] = @DAirportID      
      ,[AAirportID] = @AAirportID      
      ,[DepartureCity] = @DepartureCity      
      ,[ArrivalCity] = @ArrivalCity      
      ,[DepartureDTTMLocal] = @DepartureDTTMLocal      
      ,[ArrivalDTTMLocal] = @ArrivalDTTMLocal      
      ,[PassengerRequestorID] = @PassengerRequestorID      
      ,[DepartmentID] = @DepartmentID      
      ,[AuthorizationID] = @AuthorizationID      
      ,[FlightCategoryID] = @FlightCategoryID      
      ,[IsPrivate] = @IsPrivate      
      ,[Distance] = @Distance      
      ,[ElapseTM] = @ElapseTM      
      ,[PassengerTotal] = @PassengerTotal      
      ,[PowerSetting] = @PowerSetting      
      ,[TakeoffBIAS] = @TakeoffBIAS      
      ,[LandingBIAS] = @LandingBIAS      
      ,[TrueAirSpeed] = @TrueAirSpeed      
      ,[WindsBoeingTable] = @WindsBoeingTable      
      ,[IsDepartureConfirmed] = @IsDepartureConfirmed      
      ,[IsArrivalConfirmation] = @IsArrivalConfirmation      
      ,[IsApproxTM] = @IsApproxTM      
      ,[IsScheduledServices] = @IsScheduledServices      
      ,[DepartureGreenwichDTTM] = @DepartureGreenwichDTTM      
      ,[ArrivalGreenwichDTTM] = @ArrivalGreenwichDTTM      
      ,[HomeDepartureDTTM] = @HomeDepartureDTTM      
      ,[HomeArrivalDTTM] = @HomeArrivalDTTM      
      ,[NegotiateTime] = @NegotiateTime      
      ,[LogBreak] = @LogBreak      
      ,[FlightPurpose] = @FlightPurpose      
      ,[SeatTotal] = @SeatTotal      
      ,[ReservationTotal] = @ReservationTotal      
      ,[ReservationAvailable] = @ReservationAvailable      
      ,[WaitNUM] = @WaitNUM      
      ,[Duty_Type] = @Duty_Type      
      ,[DutyHours] = CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
	  ,RestHours = CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
	  ,FlightHours = CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END    
      ,[Notes] = @Notes      
      ,[FuelLoad] = @FuelLoad      
      ,[OutboundInstruction] = @OutboundInstruction      
      ,[DutyType] = @DutyType      
      ,[IsCrewDiscount] = @IsCrewDiscount      
      ,[CrewCurrency] = @CrewCurrency      
      ,[IsDutyEnd] = @IsDutyEnd      
      ,[FedAviationRegNUM] = @FedAviationRegNUM      
      ,[LegID] = @LegID      
      ,[WindReliability] = @WindReliability      
      ,[CROverRide] = @CROverRide      
      ,[CheckGroup] = @CheckGroup      
      ,[AirportAlertCD] = @AirportAlertCD      
      ,[AdditionalCrew] = @AdditionalCrew      
      ,[PilotInCommand] = @PilotInCommand      
      ,[SecondInCommand] = @SecondInCommand      
      ,[NextLocalDTTM] = @NextLocalDTTM      
      ,[NextGMTDTTM] = @NextGMTDTTM      
      ,[NextHomeDTTM] = @NextHomeDTTM      
      ,[CrewNotes] = @CrewNotes      
      ,[LastUpdUID] = @LastUpdUID      
      ,[LastUpdTS] = @LastUpdTS      
      ,[IsDeleted] = @IsDeleted      
      ,[FBOID] = @FBOID      
      ,[ClientID] = @ClientID  
 WHERE [CRLegID] = @CRLegID      
      ------------------      
      ---ENDS HERE      
      ------------------       
         
END      
      
GO


