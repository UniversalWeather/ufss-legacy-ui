
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteDetail]    Script Date: 03/13/2013 14:54:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteDetail]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteDetail]    Script Date: 03/13/2013 14:54:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteDetail]
(
	@CQQuoteDetailID bigint,
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@CQLegID bigint,
	@IsPrintable bit,
	@DepartureDT date,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@FlightHours numeric(5, 1),
	@Distance numeric(5, 0),
	@PassengerTotal numeric(3, 0),
	@FlightCharge numeric(17, 2),
	@IsTaxable bit,
	@TaxRate numeric(5, 2),
	@DAirportID bigint,
	@AAirportID bigint,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@LegNum bigint
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQQuoteDetail] SET [IsPrintable] = @IsPrintable, [DepartureDT] = @DepartureDT, [FromDescription] = @FromDescription, [ToDescription] = @ToDescription, [FlightHours] = @FlightHours, [Distance] = @Distance, [PassengerTotal] = @PassengerTotal, [FlightCharge] = @FlightCharge, [IsTaxable] = @IsTaxable, [TaxRate] = @TaxRate, [DAirportID] = @DAirportID, [AAirportID] = @AAirportID, [DepartureDTTMLocal] = @DepartureDTTMLocal, [ArrivalDTTMLocal] = @ArrivalDTTMLocal, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted ,LegNum = @LegNum
		WHERE [CQQuoteDetailID] = @CQQuoteDetailID AND [CustomerID] = @CustomerID
	


GO


