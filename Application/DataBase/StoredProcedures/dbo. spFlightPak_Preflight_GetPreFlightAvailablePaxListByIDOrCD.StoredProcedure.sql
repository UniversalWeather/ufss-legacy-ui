

/****** Object:  StoredProcedure [dbo].[spGetAllPreFlightAvailablePaxList]    Script Date: 10/11/2013 20:09:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreFlightAvailablePaxListByIDOrCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreFlightAvailablePaxListByIDOrCD]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllPreFlightAvailablePaxListByIDOrCD]    Script Date: 10/11/2013 20:09:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Pratyush Agarwal
-- Create date: 20/07/2012
-- Description:	Get All the Passengers for Preflight Pax Avaialable Grid By ID OR CD
-- =============================================
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetPreFlightAvailablePaxListByIDOrCD]
	-- Add the parameters for the stored procedure here
	(
	 @CustomerID BIGINT,	
	 @ClientID bigint,
	 @PassengerRequestorID bigint,
	 @PassengerRequestorCD varchar(5)
	 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @PassengerRequestorCD <>''
	begin
		SELECT 
		 p.PassengerRequestorID AS PassengerRequestorID
		,p.PassengerRequestorCD AS Code
		,p.PassengerName AS Name
		,ppl.TripStatus AS TripStatus
		,f.FlightPurposeDescription AS FlightPurpose
		--,cpp.PassportNUM AS Passport
		,(select top(1) PassportNum from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS Passport
		,ppl.Billing AS BillingCode
		,((isnull(p.Addr1,'')) + (isnull(p.Addr2,'')))  as Street
		,p.PostalZipCD as Postal
		,p.City as City
		,p.StateName as [State]
		--,cpp.PassportID as PassportID
		,(select top(1) PassportID from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportID
		,'' as Leg
		,Convert(varchar(max),p.Notes) as Notes
		,Convert(varchar(max),p.PassengerAlert) as PassengerAlert
		,(select top(1) VisaID from CrewPassengerVisa where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0) AS VisaID
		,(select top(1) PassportExpiryDT from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportExpiryDT
		,(select top(1) Country.CountryCD from CrewPassengerPassport LEFT OUTER JOIN Country  on CrewPassengerPassport.CountryID = Country.CountryID where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(CrewPassengerPassport.IsDeleted,0) =0 and CrewPassengerPassport.Choice=1) AS Nation
		,isnull(ppl.OrderNUM,1) as OrderNUM
	FROM Passenger p 
					LEFT OUTER JOIN PreflightPassengerList ppl on p.PassengerRequestorID=ppl.PassengerID AND ISNULL(ppl.IsDeleted,0) =0 
					and ppl.TripStatus is not null and ppl.Billing is not null and p.PassengerName is not null
					 LEFT OUTER JOIN FlightPurpose f on p.FlightPurposeID=f.FlightPurposeID   AND ISNULL(f.IsDeleted,0) =0 and f.FlightPurposeID is not null
					 
					
	WHERE p.CustomerID = @CustomerID
	AND ISNULL(p.ClientID, 0) =  ISNULL(@ClientID, ISNULL(p.ClientID,0)) 
	 AND ISNULL(p.IsDeleted,0) =0 and P.PassengerRequestorCD =  @PassengerRequestorCD	 
	end
	else
	begin
		SELECT 
		 p.PassengerRequestorID AS PassengerRequestorID
		,p.PassengerRequestorCD AS Code
		,p.PassengerName AS Name
		,ppl.TripStatus AS TripStatus
		,f.FlightPurposeDescription AS FlightPurpose
		--,cpp.PassportNUM AS Passport
		,(select top(1) PassportNum from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS Passport
		,ppl.Billing AS BillingCode
		,((isnull(p.Addr1,'')) + (isnull(p.Addr2,'')))  as Street
		,p.PostalZipCD as Postal
		,p.City as City
		,p.StateName as [State]
		--,cpp.PassportID as PassportID
		,(select top(1) PassportID from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportID
		,'' as Leg
		,Convert(varchar(max),p.Notes) as Notes
		,Convert(varchar(max),p.PassengerAlert) as PassengerAlert
		,(select top(1) VisaID from CrewPassengerVisa where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0) AS VisaID
		,(select top(1) PassportExpiryDT from CrewPassengerPassport where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(IsDeleted,0) =0 and Choice=1) AS PassportExpiryDT
		,(select top(1) Country.CountryCD from CrewPassengerPassport LEFT OUTER JOIN Country  on CrewPassengerPassport.CountryID = Country.CountryID where p.PassengerRequestorID = PassengerRequestorID and  ISNULL(CrewPassengerPassport.IsDeleted,0) =0 and CrewPassengerPassport.Choice=1) AS Nation
		,isnull(ppl.OrderNUM,1) as OrderNUM
	FROM Passenger p 
					LEFT OUTER JOIN PreflightPassengerList ppl on p.PassengerRequestorID=ppl.PassengerID AND ISNULL(ppl.IsDeleted,0) =0 
					and ppl.TripStatus is not null and ppl.Billing is not null and p.PassengerName is not null
					 LEFT OUTER JOIN FlightPurpose f on p.FlightPurposeID=f.FlightPurposeID   AND ISNULL(f.IsDeleted,0) =0 and f.FlightPurposeID is not null
					 
					
	WHERE p.CustomerID = @CustomerID
	AND ISNULL(p.ClientID, 0) =  ISNULL(@ClientID, ISNULL(p.ClientID,0)) 
	 AND ISNULL(p.IsDeleted,0) =0 and  P.PassengerRequestorID =  @PassengerRequestorID	 
	end 
END

GO


