/****** Object:  StoredProcedure [dbo].[GetTripSheetDetailByUMPermission]    Script Date: 10/24/2013 20:17:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTripSheetDetailByUMPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTripSheetDetailByUMPermission]
GO


/****** Object:  StoredProcedure [dbo].[GetTripSheetDetailByUMPermission]    Script Date: 10/24/2013 20:17:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTripSheetDetailByUMPermission]( @CustomerID BIGINT,-- = 10003,  
										           @TripSheetReportHeaderID BIGINT,-- = 10006181825,
												   @Sessionid UNIQUEIDENTIFIER, --= '13773D2F-6124-42F3-BB3F-8030C7C80C3B',
												   @IsSystemAdmin BIT --= 0
												 )

 

-- =============================================    
-- Author:Askar	            
-- Create date: 24/10/2013            
-- Description: Get the Report Sheet Detail using UMPermission    
-- =============================================   
AS
BEGIN  

DECLARE @TripInfo TABLE(TripSheetReportHeaderID BIGINT,
                        ReportNumber INT,
                        ReportDescription VARCHAR(35),
                        IsSelected BIT,
                        CustomerID BIGINT,
                        UMNumber INT
                        )
 
INSERT INTO @TripInfo(TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,CustomerID)                       
SELECT DISTINCT TripSheetReportHeaderID, ReportNumber, ReportDescription, IsSelected,CustomerID 
			FROM dbo.TripSheetReportDetail   
			WHERE CustomerID = @CustomerID 
			  AND TripSheetReportHeaderID = @TripSheetReportHeaderID 
			  AND IsDeleted = 0; 
			  
WITH cte AS (
  SELECT ReportNumber, 
     row_number() OVER(PARTITION BY ReportNumber ORDER BY ReportNumber) AS [rn]
  FROM @TripInfo
)
DELETE cte WHERE [rn] > 1
			   
UPDATE @TripInfo SET UMNumber=8 WHERE ReportNumber=1
UPDATE @TripInfo SET UMNumber=14 WHERE ReportNumber=2
UPDATE @TripInfo SET UMNumber=7 WHERE ReportNumber=3
UPDATE @TripInfo SET UMNumber=3 WHERE ReportNumber=4
UPDATE @TripInfo SET UMNumber=10 WHERE ReportNumber=5
--UPDATE @TripInfo SET UMNumber=6 WHERE ReportNumber=6
--UPDATE @TripInfo SET UMNumber=11 WHERE ReportNumber=7
--UPDATE @TripInfo SET UMNumber=13 WHERE ReportNumber=8
--UPDATE @TripInfo SET UMNumber=9 WHERE ReportNumber=9
UPDATE @TripInfo SET UMNumber=4 WHERE ReportNumber=10
UPDATE @TripInfo SET UMNumber=2 WHERE ReportNumber=11
UPDATE @TripInfo SET UMNumber=12 WHERE ReportNumber=12
UPDATE @TripInfo SET UMNumber=15 WHERE ReportNumber=13
UPDATE @TripInfo SET UMNumber=1 WHERE ReportNumber=14
--UPDATE @TripInfo SET UMNumber=5 WHERE ReportNumber=16

--UPDATE @TripInfo SET UMNumber=5 WHERE ReportNumber=17

DECLARE @UmPermission TABLE(UMPermissionRole VARCHAR(50),
							UMPermissionID BIT,
							UMNumber INT IDENTITY)

INSERT INTO @UmPermission(UMPermissionRole)
SELECT UMPermissionRole FROM UMPermission WHERE ModuleID = 10017 ORDER BY UMPermissionName


DECLARE @Count INT,@Count1 INT=1,@UmID INT=1
SELECT @Count=COUNT(1) FROM @UmPermission

WHILE @Count1 <= @Count
BEGIN
--SELECT UMPermissionRole FROM @UmPermission WHERE UMNumber=@UmID
--SELECT [dbo].[fCheckPermission](@Sessionid,@IsSystemAdmin,(SELECT UMPermissionRole FROM @UmPermission WHERE UMNumber=@UmID))

UPDATE @UmPermission SET UMPermissionID=[dbo].[fCheckPermission](@Sessionid,@IsSystemAdmin,(SELECT UMPermissionRole FROM @UmPermission WHERE UMNumber=@UmID)) 
                     WHERE UMPermissionRole IN(SELECT UMPermissionRole FROM @UmPermission WHERE UMNumber=@UmID)

SET @UmID=@UmID+1
SET @Count1=@Count1+1
END


SELECT * FROM @TripInfo WHERE UMNumber IN (SELECT UMNumber FROM @UmPermission WHERE UMPermissionID=1)

 END  
GO

