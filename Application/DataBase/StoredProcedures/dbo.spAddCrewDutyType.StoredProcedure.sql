
GO
/****** Object:  StoredProcedure [dbo].[spAddCrewDutyType]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCrewDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCrewDutyType]
go
CREATE PROCEDURE [dbo].[spAddCrewDutyType](
	@DutyTypeCD CHAR(2) ,
	@CustomerID BIGINT ,
	@DutyTypesDescription VARCHAR(25), 
	@ValuePTS NUMERIC(2,0),
	@GraphColor varchar(11),
	@WeekendPTS NUMERIC(2,0),
	@IsWorkLoadIndex BIT,
	@IsOfficeDuty BIT,
	@LastUpdUID VARCHAR(30),
	@LastUpdTS DATETIME,
	@ForeGrndCustomColor VARCHAR(11),
	@BackgroundCustomColor VARCHAR(11),
	@IsCrewCurrency BIT,
	@HolidayPTS NUMERIC(2,0),
	@IsCrewDuty BIT,
	@CalendarEntry BIT,
	@DutyStartTM CHAR(5),
	@DutyEndTM CHAR(5),
	@IsStandby BIT,
    @IsDeleted BIT,
    @IsInActive BIT )
-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 9/5/2012
-- Description: Insert the CrewDutyTypes information
-- =============================================
AS
BEGIN 
SET NOCOUNT ON

DECLARE @DutyTypeID BIGINT 

  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @DutyTypeID OUTPUT 
 
SET @LastUpdTS = GETUTCDATE() 
   INSERT INTO [CrewDutyType]
			(DutyTypeID,
			CustomerID  ,
			DutyTypeCD ,
			DutyTypesDescription , 
			ValuePTS ,
			GraphColor ,
			WeekendPTS ,
			IsWorkLoadIndex ,
			IsOfficeDuty ,
			LastUpdUID ,
			LastUpdTS ,
			ForeGrndCustomColor ,
			BackgroundCustomColor ,
			IsCrewCurrency ,
			HolidayPTS ,
			IsCrewDuty ,
			CalendarEntry ,
			DutyStartTM ,
			DutyEndTM ,
			IsStandby ,
			IsDeleted ,
			IsInActive)

     VALUES
           (
			@DutyTypeID,
			@CustomerID ,
			@DutyTypeCD ,
			@DutyTypesDescription , 
			@ValuePTS ,
			@GraphColor ,
			@WeekendPTS ,
			@IsWorkLoadIndex ,
			@IsOfficeDuty ,
			@LastUpdUID ,
			@LastUpdTS ,
			@ForeGrndCustomColor ,
			@BackgroundCustomColor ,
			@IsCrewCurrency ,
			@HolidayPTS ,
			@IsCrewDuty ,
			@CalendarEntry ,
			@DutyStartTM ,
			@DutyEndTM ,
			@IsStandby ,
			@IsDeleted ,
			@IsInActive
        )


END
GO
