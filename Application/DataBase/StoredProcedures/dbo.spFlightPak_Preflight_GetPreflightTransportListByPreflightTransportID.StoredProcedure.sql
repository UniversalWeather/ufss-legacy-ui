

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightTransportListByPreflightTransportID]    Script Date: 09/19/2013 15:21:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightTransportListByPreflightTransportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightTransportListByPreflightTransportID]
GO



/****** Object:  StoredProcedure [dbo].[GetPreflightTransportListByPreflightTransportID]    Script Date: 09/19/2013 15:21:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create Procedure [dbo].[spFlightPak_Preflight_GetPreflightTransportListByPreflightTransportID]
(
@CustomerID BIGINT,
@PreflightTransportID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get GetPreflightTransportListBy PreflightTransportID 
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightTransportList PC 
 where 
 PC.PreflightTransportID = @PreflightTransportID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 



GO


