
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddCrewPaxVisa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddCrewPaxVisa]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_AddCrewPaxVisa]          
(@VisaID bigint        
,@CrewID bigint        
,@PassengerRequestorID bigint        
,@CustomerID bigint        
,@VisaTYPE char(1)        
,@VisaNum nvarchar(120)        
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime        
,@CountryID bigint        
,@ExpiryDT date        
,@Notes varchar(120)        
,@IssuePlace varchar(40)        
,@IssueDate date        
,@IsDeleted bit
,@EntriesAllowedInPassport int
,@TypeOfVisa varchar(10)
,@VisaExpireInDays int)          
AS          
BEGIN  
 SET NOCOUNT OFF;          
       
If ( @CustomerID is null)       
begin      
set @CustomerID = 0      
end    
 DECLARE @ErrorMessage nVarchar(2000);  
 --For validating duplicate Visa number for the customer and Passenger, across all country  
 IF (@PassengerRequestorID IS NOT NULL)  
  BEGIN  
   IF (@IsDeleted <> 1)  
    BEGIN  
     IF EXISTS (SELECT VisaNum FROM [CrewPassengerVisa] WHERE CustomerID = @CustomerID  
                  AND VisaID <> @VisaID  
                  AND CrewID IS NULL  
                  AND VisaNum = @VisaNum  
                  AND ISNULL(CountryID,0) = ISNULL(@CountryID,0)  
                  AND IsDeleted = 0)  
      BEGIN  
       SET @ErrorMessage = '-500011~' + @VisaNum;  
       RAISERROR(@ErrorMessage, 17, 1)  
      END  
    END  
  END  
 --For validating duplicate Visa number for the customer and Crew  
 IF (@CrewID IS NOT NULL)  
  BEGIN  
   IF (@IsDeleted <> 1)  
    BEGIN  
     IF EXISTS (SELECT VisaNum FROM [CrewPassengerVisa] WHERE CustomerID = @CustomerID  
                  AND VisaID <> @VisaID  
                  AND PassengerRequestorID IS NULL  
                  AND VisaNum = @VisaNum  
                  AND IsDeleted = 0)  
      BEGIN  
       SET @ErrorMessage = '-500011~' + @VisaNum;  
       RAISERROR(@ErrorMessage, 17, 1)  
      END  
    END  
  END  
  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @VisaID OUTPUT        
  set @LastUpdTS = GETUTCDATE()      
 INSERT INTO [CrewPassengerVisa]        
           ([VisaID]        
           ,[CrewID]        
           ,[PassengerRequestorID]        
           ,[CustomerID]        
           ,[VisaTYPE]        
           ,[VisaNum]        
           ,[LastUpdUID]        
           ,[LastUpdTS]        
           ,[CountryID]        
           ,[ExpiryDT] 
           ,[EntriesAllowedInPassport]
           ,[TypeOfVisa]       
           ,[VisaExpireInDays]
           ,[Notes]        
           ,[IssuePlace]        
           ,[IssueDate]        
           ,[IsDeleted])        
     VALUES        
           (@VisaID        
   ,@CrewID        
   ,@PassengerRequestorID        
   ,@CustomerID        
   ,@VisaTYPE        
   ,@VisaNum        
   ,@LastUpdUID        
   ,@LastUpdTS        
   ,@CountryID        
   ,@ExpiryDT  
   ,@EntriesAllowedInPassport
   ,@TypeOfVisa  
   ,@VisaExpireInDays
   ,@Notes        
   ,@IssuePlace        
   ,@IssueDate        
   ,@IsDeleted)        
END  