/****** Object:  StoredProcedure [dbo].[spDeleteCustomerFuelVendorAccess]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCustomerFuelVendorAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCustomerFuelVendorAccess]
GO
CREATE PROCEDURE [dbo].[spDeleteCustomerFuelVendorAccess](
							@CustomerFuelVendorAccessID bigint,
							@CustomerID bigint,
							@FuelVendorID bigint,
							@LastUpdTS DATETIME,
							@LastUpdUID varchar(30),
							@IsDeleted bit
						)
-- =============================================      
-- Author:Ajeet      
-- Description: Delete Fuel Vedor Access for Customer      
-- Create date: 10/16/2015      
-- =============================================      
AS      
BEGIN       
SET NOCOUNT ON
 UPDATE CustomerFuelVendorAccess  
 SET   
      [LastUpdUID] = @LastUpdUID,  
      [LastUpdTS] = @LastUpdTS,  
      [IsDeleted] = @IsDeleted  
 WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID AND CustomerFuelVendorAccessID=@CustomerFuelVendorAccessID
END