/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRPassenger]    Script Date: 02/11/2013 15:58:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRPassenger]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRPassenger]
(@CustomerID BIGINT
,@CRLegID BIGINT
,@CRPassengerID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRPassenger.CRPassengerID
			,CRPassenger.CustomerID
			,CRPassenger.CRLegID
			,CRPassenger.TripNUM
			,CRPassenger.LegID
			,CRPassenger.OrderNUM
			,CRPassenger.PassengerRequestorID
			,CRPassenger.PassengerName
			,CRPassenger.FlightPurposeID
			,CRPassenger.PassportID
			,CRPassenger.Billing
			,CRPassenger.IsNonPassenger
			,CRPassenger.IsBlocked
			,CRPassenger.ReservationNUM
			,CRPassenger.WaitList
			,CRPassenger.LastUpdUID
			,CRPassenger.LastUpdTS
			,CRPassenger.IsDeleted
	FROM CRPassenger
	LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = CRPassenger.PassengerRequestorID
	WHERE CRPassenger.CustomerID = @CustomerID
	AND CRPassenger.CRLegID = @CRLegID
	AND CRPassenger.IsDeleted = 0
	AND CRPassenger.CRPassengerID =	CASE WHEN ((@CRPassengerID IS NULL) AND (@CRPassengerID <> -1))
						THEN (@CRPassengerID)
						ELSE (SELECT CRPassengerID FROM CRPassenger 
								WHERE CustomerID = @CustomerID
										AND CRLegID = @CRLegID
										AND IsDeleted = 0)
						END
END
GO


