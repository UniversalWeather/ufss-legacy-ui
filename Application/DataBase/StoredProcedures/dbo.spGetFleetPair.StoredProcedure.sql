
/****** Object:  StoredProcedure [dbo].[spGetFleetPair]    Script Date: 09/13/2012 12:53:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetPair]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetPair]
GO



/****** Object:  StoredProcedure [dbo].[spGetFleetPair]    Script Date: 09/13/2012 12:53:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetFleetPair](@CustomerID BIGINT, @FleetID BIGINT)  
AS  
-- =============================================  
-- Author: Mathes  
-- Create date: 25/6/2012  
-- Description: Get FleetPair based on the Customer and FleetID  
-- =============================================  
SET NOCOUNT ON  
  
           SELECT     FleetPair.FleetProfileInternationalDataID, FleetPair.CustomerID, FleetPair.FleetID, FleetPair.AircraftCD, FleetPair.BuyAircraftAdditionalFeeDOM, FleetPair.AircraftMake, 
                      FleetPair.AircraftModel, FleetPair.AircraftColor1, FleetPair.AircraftColor2, FleetPair.AircraftTrimColor, FleetPair.OwnerLesse, FleetPair.Addr1, FleetPair.Addr2, 
                      FleetPair.CityName, FleetPair.StateName, FleetPair.CountryID, FleetPair.PostalZipCD, FleetPair.LastUpdUID, FleetPair.LastUpdTS, FleetPair.ManufactureYear, 
                      FleetPair.PLastName, FleetPair.PFirstName, FleetPair.PMiddlename, FleetPair.PhoneNum, FleetPair.FaxNum, FleetPair.EmailAddress, FleetPair.Company, 
                      FleetPair.OLastName, FleetPair.OFirstName, FleetPair.OMiddleName, FleetPair.Address1, FleetPair.Address2, FleetPair.OCityName, FleetPair.OStateName, 
                      FleetPair.ZipCD, FleetPair.OPhoneNum, FleetPair.OFaxNum, FleetPair.OEmailAddress, FleetPair.IsDeleted, OCountry.CountryCD AS OCountryCD, Country.CountryCD, 
                      FleetPair.OCountryID,FleetPair.BusinessPhone,FleetPair.OPCellPhoneNum,FleetPair.OPHomeFax,
                      FleetPair.OPCellPhoneNum2
           ,FleetPair.OPOtherPhone
           ,FleetPair.OPPersonalEmail
           ,FleetPair.OPOtherEmail
           ,FleetPair.Address3 
           ,FleetPair.OPBusinessPhone2 
           ,FleetPair.OLCellPhoneNum 
           ,FleetPair.OLHomeFax 
           ,FleetPair.OLCellPhoneNum2 
           ,FleetPair.OLOtherPhone 
           ,FleetPair.OLPersonalEmail 
           ,FleetPair.OLOtherEmail 
           ,FleetPair.Addr3   
           ,FleetPair.IsInactive
		   ,FleetPair.BuyAircraftAdditionalFeeDOM2
		   ,FleetPair.Year1
		   ,FleetPair.Year2
           FROM       FleetPair LEFT OUTER JOIN
                      Country ON FleetPair.CountryID = Country.CountryID LEFT OUTER JOIN
                      Country AS OCountry ON FleetPair.OCountryID = OCountry.CountryID
           WHERE      CustomerID=@CustomerID AND FleetID=@FleetID

GO


