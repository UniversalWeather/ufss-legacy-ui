
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCrewDutyType]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewDutyType]
go
CREATE procedure [dbo].[spUpdateCrewDutyType](
	@DutyTypeID BIGINT,
	@CustomerID BIGINT ,
	@DutyTypeCD CHAR(2) ,
	@DutyTypesDescription VARCHAR(25), 
	@ValuePTS NUMERIC(2,0),
	@GraphColor VARCHAR(11),
	@WeekendPTS NUMERIC(2,0),
	@IsWorkLoadIndex BIT,
	@IsOfficeDuty BIT,
	@LastUpdUID VARCHAR(30) ,
	@LastUpdTS DATETIME,
	@ForeGrndCustomColor VARCHAR(11),
	@BackgroundCustomColor VARCHAR(11),
	@IsCrewCurrency BIT,
	@HolidayPTS NUMERIC(2,0),
	@IsCrewDuty BIT,
	@CalendarEntry BIT,
	@DutyStartTM CHAR(5),
	@DutyEndTM CHAR(5),
	@IsStandby BIT,
    @IsDeleted BIT,
    @IsInActive BIT)

-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 9/5/2012
-- Description: Update the CrewDutyTypes information
-- =============================================
AS
BEGIN 
SET NoCOUNT ON
set @LastUpdTS=GETUTCDATE()
UPDATE [CrewDutyType]
   SET 
	DutyTypeCD=@DutyTypeCD ,
    DutyTypesDescription=@DutyTypesDescription , 
	ValuePTS=@ValuePTS ,
	GraphColor=@GraphColor ,
	WeekendPTS=@WeekendPTS ,
	IsWorkLoadIndex=@IsWorkLoadIndex ,
	IsOfficeDuty=@IsOfficeDuty ,
	LastUpdUID=@LastUpdUID ,
	LastUpdTS=@LastUpdTS ,
	ForeGrndCustomColor=@ForeGrndCustomColor ,
	BackgroundCustomColor=@BackgroundCustomColor ,
	IsCrewCurrency=@IsCrewCurrency ,
	HolidayPTS=@HolidayPTS ,
	IsCrewDuty=@IsCrewDuty ,
	CalendarEntry=@CalendarEntry ,
	DutyStartTM=@DutyStartTM ,
	DutyEndTM=@DutyEndTM ,
	IsStandby=@IsStandby ,
    IsDeleted=@IsDeleted ,
    IsInActive=@IsInActive
            
 WHERE DutyTypeID=@DutyTypeID AND CustomerID =@CustomerID 

END
GO
