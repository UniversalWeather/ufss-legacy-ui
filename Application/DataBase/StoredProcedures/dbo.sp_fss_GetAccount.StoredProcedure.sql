/****** Object:  StoredProcedure [dbo].[sp_fss_GetAccount]    Script Date: 02/28/2014 13:53:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAccount]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetAccount]    Script Date: 02/28/2014 13:53:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[sp_fss_GetAccount](@CustomerID  BIGINT,    
@AccountNum VARCHAR(50),    
@AccountID BIGINT,  
@FetchHomebaseOnly CHAR(4),  
@FetchInactiveOnly BIT  
)                
AS              
-- =============================================                
-- Author: Ramesh                
-- Create date: 26/02/2014                
-- Description: Get the Account information               
-- EXEC [sp_fss_GetAccount] 10002, '', 0, '',1  
-- =============================================                
SET NOCOUNT ON                 
 BEGIN                 
  SELECT Account.AccountID            
    ,CASE WHEN [AccountNum] IS NULL THEN '' ELSE LTRIM(RTRIM(Account.AccountNum)) END AS AccountNum           
    ,Account.CustomerID            
    ,Account.AccountDescription            
    ,Account.CorpAccountNum            
    ,Account.HomebaseID            
    ,Account.IsBilling            
    ,Account.LastUpdUID            
    ,Account.LastUpdTS            
    ,Account.Cost            
    ,ISNULL(Account.IsInActive,0) IsInactive            
    ,Account.IsDeleted            
    ,Airport.ICAOID AS HomebaseCD            
    ,Company.BaseDescription            
  FROM Account            
   LEFT OUTER JOIN Company ON Company.HomebaseID = Account.HomebaseID            
   LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID            
  WHERE     
  Account.CustomerID     = @CustomerID       
  AND ISNULL(Account.IsDeleted,0)  = 0       
  AND LTRIM(RTRIM(Account.AccountNum)) = CASE WHEN LTRIM(RTRIM(@AccountNum))<> '' THEN LTRIM(RTRIM(@AccountNum)) ELSE LTRIM(RTRIM(Account.AccountNum)) END    
  AND Account.AccountID  = CASE WHEN @AccountID <> 0 THEN @AccountID ELSE Account.AccountID END    
  AND ISNULL(Account.IsInActive,0) = CASE WHEN @FetchInactiveOnly = 0 THEN ISNULL(Account.IsInActive,0) ELSE 0 END  
  AND ISNULL(Airport.ICAOID,'')    = CASE WHEN LTRIM(RTRIM(@FetchHomebaseOnly)) <> '' THEN @FetchHomebaseOnly ELSE ISNULL(Airport.ICAOID,'') END     
  ORDER BY Account.AccountNum               
 END      
GO

