
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQCateringList]    Script Date: 03/07/2013 17:40:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQCateringList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQCateringList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQCateringList]    Script Date: 03/07/2013 17:40:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQCateringList]
(
	@CQCateringListID bigint,
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@CateringID bigint,
	@CQCateringListDescription varchar(60),
	@PhoneNUM varchar(25),
	@Rate numeric(17, 2),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)	
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
UPDATE [CQCateringList] SET  [QuoteID] = @QuoteID, [LegID] = @LegID, [RecordType] = @RecordType, [CateringID] = @CateringID, [CQCateringListDescription] = @CQCateringListDescription, [PhoneNUM] = @PhoneNUM, [Rate] = @Rate, [AirportID] = @AirportID, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted ,[FaxNUM] = @FaxNUM,
	[Email] = @Email
where [CQCateringListID] = @CQCateringListID AND [CustomerID] = @CustomerID AND [CQLegID] = @CQLegID

GO


