
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceQuoteDetail]    Script Date: 03/13/2013 14:54:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceQuoteDetail]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceQuoteDetail]    Script Date: 03/13/2013 14:54:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceQuoteDetail]
(
	@CQInvoiceQuoteDetailID bigint,
	@CustomerID bigint,
	@CQInvoiceMainID bigint,
	@InvoiceNUM int,
	@CQLegID bigint,
	@InvoiceDetailID int,
	@QuoteID int,
	@IsPrintable bit,
	@DepartureDT date,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@FlightHours numeric(5, 1),
	@Distance numeric(5, 0),
	@PassengerTotal numeric(3, 0),
	@FlightCharge numeric(17, 2),
	@InvoiceFlightChg numeric(17, 2),
	@IsTaxable bit,
	@TaxRate numeric(5, 2),
	@DAirportID bigint,
	@AAirportID bigint,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@LegID int,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@LegNum Bigint
)
AS
	SET NOCOUNT ON;

UPDATE [CQInvoiceQuoteDetail] SET [InvoiceNUM] = @InvoiceNUM, [CQLegID] = @CQLegID, [InvoiceDetailID] = @InvoiceDetailID, [QuoteID] = @QuoteID, [IsPrintable] = @IsPrintable, [DepartureDT] = @DepartureDT, [FromDescription] = @FromDescription, [ToDescription] = @ToDescription, [FlightHours] = @FlightHours, [Distance] = @Distance, [PassengerTotal] = @PassengerTotal, [FlightCharge] = @FlightCharge, [InvoiceFlightChg] = @InvoiceFlightChg, [IsTaxable] = @IsTaxable, [TaxRate] = @TaxRate, [DAirportID] = @DAirportID, [AAirportID] = @AAirportID, [DepartureDTTMLocal] = @DepartureDTTMLocal, [ArrivalDTTMLocal] = @ArrivalDTTMLocal, [LegID] = @LegID, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted ,LegNum = @LegNum
	WHERE [CQInvoiceQuoteDetailID] = @CQInvoiceQuoteDetailID AND [CustomerID] = @CustomerID 
	


GO


