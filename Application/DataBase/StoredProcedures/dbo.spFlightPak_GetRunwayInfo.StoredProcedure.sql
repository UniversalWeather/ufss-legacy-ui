IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetRunwayInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetRunwayInfo]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetRunwayInfo]    Script Date: 08/24/2012 10:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spFlightPak_GetRunwayInfo]	
-- =============================================
-- Author: Sujitha.V
-- Create date: 16/05/2012
-- Description: Get Runway Info 
-- =============================================
as
SET NoCOUNT ON
BEGIN
	
Select
         
            [RunwayID]
           ,[CustomerID]
           ,[AirportID]
           ,[UWAID]
           ,[UWARunwayID]
           ,[RunwayLength]
           ,[RunwayWidth]
           ,[Surface]
           ,[Lights]
           ,[InstLandSysType]
           ,[NavigationAids]
           ,[UWAUpdates]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,IsInactive
           
        
FROM Runway

	
end
GO
