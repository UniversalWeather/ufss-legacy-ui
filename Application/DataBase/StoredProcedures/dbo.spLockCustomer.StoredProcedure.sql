/****** Object:  StoredProcedure [dbo].[spLockCustomer]    Script Date: 09/13/2012 12:38:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLockCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLockCustomer]
GO
/****** Object:  StoredProcedure [dbo].[spLockCustomer]    Script Date: 09/13/2012 12:38:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spLockCustomer]
@CustomerId bigint
AS
BEGIN
UPDATE Customer set IsCustomerLock = 1 where CustomerID = @CustomerId
END


GO


