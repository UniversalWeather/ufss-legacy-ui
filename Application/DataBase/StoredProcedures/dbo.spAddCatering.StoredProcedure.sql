IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCatering]
GO
/****** Object:  StoredProcedure [dbo].[spAddCatering]    Script Date: 09/13/2012 12:26:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spAddCatering](
    @CateringID BIGINT,
	@AirportID BIGINT,
	@CateringCD CHAR(4),
	@CustomerID BIGINT,
	@IsChoice BIT ,
	@CateringVendor VARCHAR(40), 
	@PhoneNum VARCHAR(25) ,
	@FaxNum VARCHAR(25) ,
	@ContactName VARCHAR(25) ,
	@Remarks varchar(max) ,
	@NegotiatedRate NUMERIC(6, 2) ,
	@LastUpdUID VARCHAR(30) ,
	@LastUpdTS DATE,
	@UpdateDT DATE,
	@RecordType CHAR(1),
	@SourceID VARCHAR(12),
	@ControlNum CHAR(5) ,
	@IsInActive BIT ,
	@TollFreePhoneNum VARCHAR(25) ,
	@WebSite VARCHAR(200) ,
	@UWAMaintFlag BIT ,
	@UWAID VARCHAR(20),
    @IsDeleted BIT,
    @UWAUpdates BIT,
    @BusinessEmail varchar(250),
	@ContactBusinessPhone varchar(25),
	@CellPhoneNum varchar(25),
	@ContactEmail varchar(250),
	@Addr1 varchar(40),
	@Addr2 varchar(40),
	@Addr3 varchar(40),
	@City varchar(25),
	@StateProvince varchar(10),
	@MetroID BIGINT,	
	@CountryID BIGINT,
	@ExchangeRateID	BIGINT,
@NegotiatedTerms	VARCHAR(800),
@SundayWorkHours	VARCHAR(15),
@MondayWorkHours	VARCHAR(15),
@TuesdayWorkHours	VARCHAR(15),
@WednesdayWorkHours	VARCHAR(15),
@ThursdayWorkHours	VARCHAR(15),
@FridayWorkHours	VARCHAR(15),
@SaturdayWorkHours	VARCHAR(15),
@PostalZipCD VARCHAR(15)
		
     )
-- =============================================
-- Author:Mullai.D
-- Create date: 24/4/2012
-- Altered by Karthikeyan.S :04/07/2012
-- Description: Insert the Catering information
-- Altered by Rajesh : 05-May-2015 (Added PostalZipCD)
-- =============================================
as
begin 
DECLARE @UWACustomerID BIGINT   
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output 
DECLARE @ICAOCD VARCHAR(50)
DECLARE @UWAAirportId VARCHAR(50)    
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID  
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD

IF not exists (select CustomerID from Catering where  CateringID = @CateringID) 
begin
IF not exists (select CustomerID from Catering where (AirportID=@AirportID or AirportID = @UWAAirportId)   and (CustomerID = @CustomerID or CustomerID = @UWACustomerID))  
begin
select @CateringCD='0001'
end
else 
begin
select @CateringCD=right('0000'+  cast( MAX(Convert(int,CateringCD))+1 as varchar(5)),4) from Catering where (AirportID=@AirportID or AirportID = @UWAAirportId)   and (CustomerID = @CustomerID or CustomerID = @UWACustomerID) 
end
end
SET NoCOUNT ON
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @CateringID  OUTPUT  
SET @LastUpdTS = GETUTCDATE() 
IF(@IsChoice='true')
   begin
   update Catering set IsChoice='false' where CustomerID=@CustomerID  and (AirportID=@AirportID or AirportID = @UWAAirportId)
   end 
   INSERT INTO [Catering]
    (
    CateringID
	,AirportID
	,CateringCD
	,CustomerID
	,IsChoice 
	,CateringVendor  
	,PhoneNum 
	,FaxNum 
	,ContactName 
	,Remarks 
	,NegotiatedRate
	,LastUpdUID 
	,LastUpdTS 
	,UpdateDT 
	,RecordType 
	,SourceID 
	,ControlNum 
	,IsInActive 
	,TollFreePhoneNum 
	,WebSite 
	,UWAMaintFlag
	,UWAID 
    ,IsDeleted
    ,UWAUpdates
    ,BusinessEmail
	,ContactBusinessPhone
	,CellPhoneNum
	,ContactEmail
	,Addr1
	,Addr2
	,Addr3
	,City
	,StateProvince
	,MetroID
	,CountryID 	
     ,ExchangeRateID  
     ,NegotiatedTerms  
     ,SundayWorkHours  
     ,MondayWorkHours  
     ,TuesdayWorkHours  
     ,WednesdayWorkHours  
     ,ThursdayWorkHours  
     ,FridayWorkHours  
     ,SaturdayWorkHours
     ,PostalZipCD
    )

     VALUES
           (
     @CateringID
	,@AirportID
	,@CateringCD
	,@CustomerID
	,@IsChoice 
	,@CateringVendor  
	,@PhoneNum 
	,@FaxNum 
	,@ContactName 
	,@Remarks 
	,@NegotiatedRate 
	,@LastUpdUID 
	,@LastUpdTS 
	,@UpdateDT 
	,@RecordType 
	,@SourceID 
	,@ControlNum 
	,@IsInActive 
	,@TollFreePhoneNum 
	,@WebSite 
	,@UWAMaintFlag
	,@UWAID 
    ,@IsDeleted
    ,@UWAUpdates
    ,@BusinessEmail
	,@ContactBusinessPhone
	,@CellPhoneNum
	,@ContactEmail
	,@Addr1
	,@Addr2
	,@Addr3
	,@City
	,@StateProvince	
	,@MetroID
	,@CountryID
     ,@ExchangeRateID  
     ,@NegotiatedTerms  
     ,@SundayWorkHours  
     ,@MondayWorkHours  
     ,@TuesdayWorkHours  
     ,@WednesdayWorkHours  
     ,@ThursdayWorkHours  
     ,@FridayWorkHours  
     ,@SaturdayWorkHours
     ,@PostalZipCD
     )


END
GO