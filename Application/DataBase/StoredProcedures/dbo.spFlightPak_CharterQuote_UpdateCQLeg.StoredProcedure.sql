
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePreFlightLeg]    Script Date: 02/18/2014 09:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdatePreFlightLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdatePreFlightLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePreFlightLeg]    Script Date: 02/18/2014 09:29:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_UpdatePreFlightLeg]  
(       
 @LegID bigint
,@TripID bigint
,@CustomerID bigint
,@TripNUM bigint
,@LegNUM bigint
,@DepartICAOID bigint
,@ArriveICAOID bigint
,@Distance numeric(5)
,@PowerSetting char(1)
,@TakeoffBIAS numeric(6, 3)
,@LandingBias numeric(6, 3)
,@TrueAirSpeed numeric(3, 0)
,@WindsBoeingTable numeric(5, 0)
,@ElapseTM numeric(7, 3)
,@IsDepartureConfirmed bit
,@IsArrivalConfirmation bit
,@IsApproxTM bit
,@IsScheduledServices bit
,@DepartureDTTMLocal datetime
,@ArrivalDTTMLocal datetime
,@DepartureGreenwichDTTM datetime
,@ArrivalGreenwichDTTM datetime
,@HomeDepartureDTTM datetime
,@HomeArrivalDTTM datetime
,@GoTime char(5)
,@FBOID bigint
,@LogBreak char(3)
,@FlightPurpose varchar(40)
,@PassengerRequestorID bigint
,@RequestorName varchar(60)
,@DepartmentID bigint
,@DepartmentName varchar(60)
,@AuthorizationID bigint
,@AuthorizationName varchar(60)
,@PassengerTotal int
,@SeatTotal int
,@ReservationTotal int
,@ReservationAvailable int
,@WaitNUM int
,@DutyTYPE char(2)
,@DutyHours numeric(12, 3)
,@RestHours numeric(12, 3)
,@FlightHours numeric(12, 3)
,@Notes text
,@FuelLoad numeric(6, 0)
,@OutbountInstruction text
,@DutyTYPE1 numeric(1, 0)
,@IsCrewDiscount bit
,@CrewDutyRulesID bigint
,@IsDutyEnd bit
,@FedAviationRegNUM char(3)
,@ClientID bigint
,@LegID1 bigint
,@WindReliability int
,@OverrideValue numeric(4, 1)
,@CheckGroup char(3)
,@AdditionalCrew char(24)
,@PilotInCommand char(3)
,@SecondInCommand char(3)
,@NextLocalDTTM datetime
,@NextGMTDTTM datetime
,@NextHomeDTTM datetime
,@LastUpdUID char(30)
,@LastUpdTS datetime
,@CrewNotes text
,@IsPrivate bit
,@WaitList char(1)
,@FlightNUM char(12)
,@FlightCost numeric(12, 2)
,@CrewFuelLoad char(40)
,@IsDeleted bit
,@UWAID char(40)
,@USCrossing varchar(75)
,@Response text
,@ConfirmID char(25)
,@CrewDutyAlert varchar(5)
,@AccountID bigint
,@FlightCategoryID bigint
,@LastUpdTSCrewNotes datetime
,@LastUpdTSPaxNotes datetime
,@CQCustomerID bigint
)    
AS    
BEGIN    
UPDATE PreflightLeg   
 SET   
  

[TripID] = @TripID
,[CustomerID] = @CustomerID
,[TripNUM] = @TripNUM
,[LegNUM] = @LegNUM
,[DepartICAOID] = @DepartICAOID
,[ArriveICAOID] = @ArriveICAOID
,[Distance] = @Distance
,[PowerSetting] = @PowerSetting
,[TakeoffBIAS] = @TakeoffBIAS
,[LandingBias] = @LandingBias
,[TrueAirSpeed] = @TrueAirSpeed
,[WindsBoeingTable] = @WindsBoeingTable
,[ElapseTM] = @ElapseTM
,[IsDepartureConfirmed] = @IsDepartureConfirmed
,[IsArrivalConfirmation] = @IsArrivalConfirmation
,[IsApproxTM] = @IsApproxTM
,[IsScheduledServices] = @IsScheduledServices
,[DepartureDTTMLocal] = @DepartureDTTMLocal
,[ArrivalDTTMLocal] = @ArrivalDTTMLocal
,[DepartureGreenwichDTTM] = @DepartureGreenwichDTTM
,[ArrivalGreenwichDTTM] = @ArrivalGreenwichDTTM
,[HomeDepartureDTTM] = @HomeDepartureDTTM
,[HomeArrivalDTTM] = @HomeArrivalDTTM
,[GoTime] = @GoTime
,[FBOID] = @FBOID
,[LogBreak] = @LogBreak
,[FlightPurpose] = @FlightPurpose
,[PassengerRequestorID] = @PassengerRequestorID
,[RequestorName] = @RequestorName
,[DepartmentID] = @DepartmentID
,[DepartmentName] = @DepartmentName
,[AuthorizationID] = @AuthorizationID
,[AuthorizationName] = @AuthorizationName
,[PassengerTotal] = @PassengerTotal
,[SeatTotal] = @SeatTotal
,[ReservationTotal] = @ReservationTotal
,[ReservationAvailable] = @ReservationAvailable
,[WaitNUM] = @WaitNUM
,[DutyTYPE] = @DutyTYPE
,[DutyHours] = CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
,[RestHours] = 	CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
,[FlightHours] = CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END
--,[DutyHours] = @DutyHours
--,[RestHours] = @RestHours
--,[FlightHours] = @FlightHours
,[Notes] = @Notes
,[FuelLoad] = @FuelLoad
,[OutbountInstruction] = @OutbountInstruction
,[DutyTYPE1] = @DutyTYPE1
,[IsCrewDiscount] = @IsCrewDiscount
,[CrewDutyRulesID] = @CrewDutyRulesID
,[IsDutyEnd] = @IsDutyEnd
,[FedAviationRegNUM] = @FedAviationRegNUM
,[ClientID] = @ClientID
,[LegID1] = @LegID1
,[WindReliability] = @WindReliability
,[OverrideValue] = @OverrideValue
,[CheckGroup] = @CheckGroup
,[AdditionalCrew] = @AdditionalCrew
,[PilotInCommand] = @PilotInCommand
,[SecondInCommand] = @SecondInCommand
,[NextLocalDTTM] = @NextLocalDTTM
,[NextGMTDTTM] = @NextGMTDTTM
,[NextHomeDTTM] = @NextHomeDTTM
,[LastUpdUID] = @LastUpdUID
,[LastUpdTS] = @LastUpdTS
,[CrewNotes] = @CrewNotes
,[IsPrivate] = @IsPrivate
,[WaitList] = @WaitList
,[FlightNUM] = @FlightNUM
,[FlightCost] = @FlightCost
,[CrewFuelLoad] = @CrewFuelLoad
,[IsDeleted] = @IsDeleted
,[UWAID] = @UWAID
,[USCrossing] = @USCrossing
,[Response] = @Response
,[ConfirmID] = @ConfirmID
,[CrewDutyAlert] = @CrewDutyAlert
,[AccountID] = @AccountID
,[FlightCategoryID] = @FlightCategoryID
,[LastUpdTSCrewNotes]=@LastUpdTSCrewNotes
,[LastUpdTSPaxNotes]=@LastUpdTSPaxNotes
,[CQCustomerID] = @CQCustomerID
 WHERE   
  LegID = @LegID AND   
  CustomerID = @CustomerID  
END



GO


