

/****** Object:  StoredProcedure [dbo].[UpadateCharterRateTax]    Script Date: 04/17/2013 15:53:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpadateCharterRateTax]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpadateCharterRateTax]
GO


/****** Object:  StoredProcedure [dbo].[UpadateCharterRateTax]    Script Date: 04/17/2013 15:53:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE procedure [dbo].[UpadateCharterRateTax]    
(    
@FleetID bigint,    
@AircraftID bigint,    
@DailyUsageAdjTax bit,    
@LandingFeeTax bit,    
@LastUpdUID char(30),          
@LastUpdTS datetime,  
@MinimumDailyRequirement numeric(17,2),  
@StandardCrewIntl numeric(2,0),  
@StandardCrewDOM numeric(2,0),  
@CQCustomerID bigint,
@MarginalPercentage numeric(7,2)  
)    
as     
Begin           
Set NoCount On    
-- =============================================          
-- Author:  karthikeyan.s          
-- Create date: 01/31/2013    
-- Description: Updating the Trip         
-- =============================================       
SET @LastUpdTS = GETUTCDATE()     
--IF(@FleetID < 1)          
IF(@AircraftID > 1)          
BEGIN    
 UPDATE Aircraft SET DailyUsageAdjTax = @DailyUsageAdjTax, LandingFeeTax = @LandingFeeTax, MinimumDayUseHrs = @MinimumDailyRequirement, IntlStdCrewNum = @StandardCrewIntl, DomesticStdCrewNum = @StandardCrewDOM, [LastUpdUID] = @LastUpdUID,MarginalPercentage = @MarginalPercentage,[LastUpdTS] = @LastUpdTS WHERE AircraftID = @AircraftID    
END    
--IF(@AircraftID < 1)          
IF(@FleetID > 1)          
BEGIN    
 UPDATE Fleet SET IsTaxDailyAdj = @DailyUsageAdjTax,MarginalPercentage = @MarginalPercentage, IsTaxLandingFee = @LandingFeeTax, MinimumDay = @MinimumDailyRequirement, StandardCrewIntl = @StandardCrewIntl, StandardCrewDOM = @StandardCrewDOM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS WHERE FleetID = @FleetID    
END    
IF(@CQCustomerID > 1)          
BEGIN    
 UPDATE CQCustomer SET DailyUsageAdjTax = @DailyUsageAdjTax,MarginalPercentage = @MarginalPercentage, LandingFeeTax = @LandingFeeTax, MinimumDayUseHrs = @MinimumDailyRequirement, IntlStdCrewNum = @StandardCrewIntl, DomesticStdCrewNum = @StandardCrewDOM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS WHERE CQCustomerID = @CQCustomerID  
END    
  
End 
GO


