/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCustomerModuleAccesses]    Script Date: 01/24/2013 16:12:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetCustomerModuleAccesses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetCustomerModuleAccesses]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCustomerModuleAccesses]    Script Date: 01/24/2013 16:12:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetCustomerModuleAccesses](@CustomerID BIGINT)        
AS        
SET NOCOUNT ON;        
-- =============================================        
-- Author:  PremPrakash.S        
-- Create date: 17-May2012        
-- Description: To get all the Modules access for the Customer        
-- EXEC spFlightPak_GetCustomerModuleAccesses 18      
-- =============================================        
 BEGIN        
 DECLARE  @EmptySetFlag bit       
 IF EXISTS(Select Top 1 CMA.CustomerID from  CustomerModuleAccess  CMA where CMA.CustomerID=@CustomerID)          
   BEGIN        
   SELECT        
    CA.CustomerID,    
    CA.CustomerModuleAccessID            
    ,M.ModuleID             
    ,M.ModuleName         
   ,CA.CanAccess                    
   ,CA.ExpiryDate                    
   ,CA.[LastUpdUID]        
   ,CA.[LastUpdTS]        
   ,M.[IsDeleted]
     ,M.[IsInActive]              
     FROM Module M        
     INNER JOIN CustomerModuleAccess CA ON M.ModuleID=CA.ModuleID  WHERE CA.CustomerID=@CustomerID               
   UNION        
           
   SELECT CAST(0 as BIGINT)  as CustomerID       
   ,    CAST(0 AS BIGINT) AS CustomerModuleAccessID          
    ,m.ModuleID                
    ,M.ModuleName            
      ,Cast('true'as bit) as CanAccess                    
     ,Cast('' as datetime)  as ExpiryDate                    
     ,M.[LastUpdUID]        
     ,'' as [LastUpdTS]        
     ,m.[IsDeleted] 
      ,M.[IsInActive]       
    FROM  Module M where ModuleID NOT IN (SELECT M.ModuleID FROM Module M        
    left JOIN CustomerModuleAccess CA ON M.ModuleID=CA.ModuleID  WHERE CA.CustomerID=@CustomerID )       
          
    Set @EmptySetFlag='false'      
             
    END        
         
  ELSE        
    BEGIN        
    SELECT CAST(0 as BIGINT)  as CustomerID,    
    CAST(0 AS BIGINT) AS CustomerModuleAccessID          
    ,M.ModuleID                
    ,M.ModuleName            
      ,Cast('false'as bit) as CanAccess                    
     ,Cast('' as datetime)  as ExpiryDate                    
     ,M.[LastUpdUID]        
     ,M.[LastUpdTS]        
     ,M.[IsDeleted]   
     ,M.[IsInActive]       
    FROM  Module M where IsDeleted= 'false'       
    Set @EmptySetFlag='false'       
 END      
       
      
       
--IF @EmptySetFlag='true'      
--BEGIN         
--  SELECT CAST(0 as INT)  as CustomerID          
--    ,m.ModuleID                
--    ,M.ModuleName            
--      ,Cast('true'as bit) as CanAccess                    
--     ,Cast('' as datetime)  as ExpiryDate                    
--     ,M.[LastUpdUID]        
--     ,M.[LastUpdTS]        
--     ,m.[IsDeleted]        
--    FROM  Module M where 1=2       
--End          
          
      
--SELECT [CustomerID]        
--      ,cm.[ModuleID]        
--      ,m.ModuleName        
--      ,[CanAccess]        
--      ,[ExpiryDate]        
      
--      ,cm.[LastUpdUID]        
--      ,cm.[LastUpdTS]        
--      ,cm.[IsDeleted]        
--  FROM Modules m         
--  inner join CustomerModuleAccesses cm on m.ModuleID=cm.ModuleID          
          
--   WHERE CustomerID=@CustomerID and cm.[IsDeleted]='false'        
      
        
End    
GO