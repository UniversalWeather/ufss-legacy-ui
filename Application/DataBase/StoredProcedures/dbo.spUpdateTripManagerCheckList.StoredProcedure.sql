
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTripManagerCheckList]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateTripManagerCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateTripManagerCheckList]
go
CREATE procedure [dbo].[spUpdateTripManagerCheckList]
(@CheckListID	bigint
,@CustomerID	bigint
,@CheckListCD	char(4)
,@CheckListDescription	varchar(20)
,@CheckGroupID	bigint
,@LastUpdUID	varchar(30)
,@LastUpdTS	datetime
,@IsDeleted	bit
,@IsInActive bit)

-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 13/5/2012
-- Description: Update the TripManagerCheckList information
-- =============================================
as
begin 
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
UPDATE [TripManagerCheckList]
   SET --CheckListID=@CheckListID		
		CheckListDescription=@CheckListDescription
		,CheckGroupID=@CheckGroupID
		,LastUpdUID=@LastUpdUID
		,LastUpdTS=@LastUpdTS
		,IsDeleted=@IsDeleted
		,IsInActive=@IsInActive
 WHERE CustomerID=@CustomerID and CheckListID=@CheckListID

end
GO
