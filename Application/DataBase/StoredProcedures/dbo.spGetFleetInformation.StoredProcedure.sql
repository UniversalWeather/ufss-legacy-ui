/****** Object:  StoredProcedure [dbo].[spGetFleetInformation]    Script Date: 01/07/2013 18:58:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetInformation]
GO

/****** Object:  StoredProcedure [dbo].[spGetFleetInformation]    Script Date: 01/07/2013 18:58:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetFleetInformation](@CustomerID BIGINT, @FleetID BIGINT)    
AS    
-- =============================================    
-- Author: Mathes    
-- Create date: 25/6/2012    
-- Description: Get FleetPair based on the CustomerID    
-- =============================================    
SET NOCOUNT ON    
SELECT [FleetInformationID],  
       [CustomerID],      
       [FleetID],    
       [AircraftCD],    
       [AirframeHrs],    
       [AirframeCycle],    
       [AIrframeAsOfDT],    
       [Engine1Hrs],    
       [Engine1Cycle],    
       [Engine1AsOfDT],    
       [Engine2Hrs],    
       [Engine2Cycle],    
       [Engine2AsOfDT],    
       [Engine3Hrs],    
       [Engine3Cycle],    
       [Engine3AsOfDT],    
       [Engine4Hrs],    
       [Engine4Cycle],    
       [Engine4AsOfDT],    
       [Reverse1Hrs],    
       [Reverser1Cycle],    
       [Reverser1AsOfDT],    
       [Reverse2Hrs],    
       [Reverser2Cycle],    
       [Reverser2AsOfDT],    
       [Reverse3Hrs],    
       [Reverser3Cycle],    
       [Reverser3AsOfDT],    
       [Reverse4Hrs],    
       [Reverser4Cycle],    
       [Reverser4AsOfDT],    
       [LastUpdUID],    
       [LastUpdTS],    
       [MACHFuel1Hrs],    
       [MACHFuel2Hrs],    
       [MACHFuel3Hrs],    
       [MACHFuel4Hrs],    
       [MACHFuel5Hrs],    
       [MACHFuel6Hrs],    
       [MACHFuel7Hrs],    
       [MACHFuel8Hrs],    
       [MACHFuel9Hrs],    
       [MACHFuel10Hrs],    
       [MACHFuel11Hrs],    
       [MACHFuel12Hrs],    
       [MACHFuel13Hrs],    
       [MACHFuel14Hrs],    
       [MACHFuel15Hrs], 
       [MACHFuel16Hrs],
       [MACHFuel17Hrs],
       [MACHFuel18Hrs],
       [MACHFuel19Hrs],
       [MACHFuel20Hrs],
       [LongRangeFuel1Hrs],    
       [LongRangeFuel2Hrs],    
       [LongRangeFuel3Hrs],    
       [LongRangeFuel4Hrs],    
       [LongRangeFuel5Hrs],    
       [LongRangeFuel6Hrs],    
       [LongRangeFuel7Hrs],    
       [LongRangeFuel8Hrs],    
       [LongRangeFuel9Hrs],    
       [LongRangeFuel10Hrs],    
       [LongRangeFuel11Hrs],    
       [LongRangeFuel12Hrs],    
       [LongRangeFuel13Hrs],    
       [LongRangeFuel14Hrs],    
       [LongRangeFuel15Hrs], 
       [LongRangeFuel16Hrs],
       [LongRangeFuel17Hrs],
       [LongRangeFuel18Hrs],
       [LongRangeFuel19Hrs],
       [LongRangeFuel20Hrs],          
       [HighSpeedFuel1Hrs],    
       [HighSpeedFuel2Hrs],    
       [HighSpeedFuel3Hrs],    
       [HighSpeedFuel4Hrs],    
       [HighSpeedFuel5Hrs],    
       [HighSpeedFuel6Hrs],    
       [HighSpeedFuel7Hrs],    
       [HighSpeedFuel8Hrs],    
       [HighSpeedFuel9Hrs],    
       [HighSpeedFuel10Hrs],    
       [HighSpeedFuel11Hrs],    
       [HighSpeedFuel12Hrs],    
       [HighSpeedFuel13Hrs],    
       [HighSpeedFuel14Hrs],    
       [HighSpeedFuel51Hrs],
       [HighSpeedFuel16Hrs],
       [HighSpeedFuel17Hrs],
       [HighSpeedFuel18Hrs],
       [HighSpeedFuel19Hrs],
       [HighSpeedFuel20Hrs],                  
       [Fuel1Hrs],    
       [Fuel2Hrs],    
       [Fuel3Hrs],    
       [Fuel4Hrs],    
       [Fuel5Hrs],    
       [IsDeleted],
	   [IsInActive]   
FROM  [FleetInformation] WHERE CustomerID = @CustomerID AND FleetID=@FleetID  
GO


