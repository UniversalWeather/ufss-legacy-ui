IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQInvoiceAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceAdditionalFees]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceAdditionalFees]
(
	@CustomerID bigint,
	@CQInvoiceMainID bigint,
	@QuoteID int,
	@InvoiceNUM int,
	@FeeIdentification int,
	@IsPrintable bit,
	@IsTaxable bit,
	@CQInvoiceFeeDescription varchar(60),
	@QuoteAmount numeric(17, 2),
	@InvoiceAmt numeric(17, 2),
	@OrderNUM int,
	@IsDiscount bit,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@Quantity numeric(17,3) ,
	@ChargeUnit varchar(25),
	@Rate numeric(17,2)
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQInvoiceAdditionalFeesID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQInvoiceAdditionalFeesID OUTPUT 
		
	INSERT INTO [CQInvoiceAdditionalFees] ([CQInvoiceAdditionalFeesID], [CustomerID], [CQInvoiceMainID], [QuoteID], [InvoiceNUM], [FeeIdentification], [IsPrintable], [IsTaxable], [CQInvoiceFeeDescription], [QuoteAmount], [InvoiceAmt], [OrderNUM], [IsDiscount], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted] ,[Quantity], [ChargeUnit],[Rate])  
		VALUES (@CQInvoiceAdditionalFeesID, @CustomerID, @CQInvoiceMainID, @QuoteID, @InvoiceNUM, @FeeIdentification, @IsPrintable, @IsTaxable, @CQInvoiceFeeDescription, @QuoteAmount, @InvoiceAmt, @OrderNUM, @IsDiscount, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@Quantity, @ChargeUnit,@Rate);
	
	SELECT @CQInvoiceAdditionalFeesID as CQInvoiceAdditionalFeesID


GO


