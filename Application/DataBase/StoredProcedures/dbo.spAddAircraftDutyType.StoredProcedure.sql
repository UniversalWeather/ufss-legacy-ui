
GO
/****** Object:  StoredProcedure [dbo].[spAddAircraftDutyType]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddAircraftDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddAircraftDutyType]
go
CREATE PROCEDURE [dbo].[spAddAircraftDutyType]  (@AircraftDutyCD  CHAR(2),  
                                                 @CustomerID BIGINT,  
                                                 @AircraftDutyDescription VARCHAR(25),  
                                                 @ClientID BIGINT,  
                                                 @GraphColor VARCHAR(11),  
                                                 @LastUpdUID VARCHAR(30),  
                                                 @LastUpdTS DATETIME,  
                                                 @ForeGrndCustomColor VARCHAR(8),  
                                                 @BackgroundCustomColor VARCHAR(8),  
                                                 @IsCalendarEntry BIT,  
                                                 @DefaultStartTM CHAR(5),  
                                                 @DefualtEndTM CHAR(5),  
                                                 @IsAircraftStandby BIT,  
                                                 @IsDeleted BIT,
                                                 @IsInActive BIT)  

AS  
BEGIN   
SET NoCOUNT ON  
  
  DECLARE @AircraftDutyID BIGINT
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @AircraftDutyID OUTPUT
  set @LastUpdTS = GETUTCDATE()
   INSERT INTO AircraftDuty  
      ([AircraftDutyID]
      ,[CustomerID]  
      ,[AircraftDutyCD]  
      ,[AircraftDutyDescription]  
      ,[ClientID]  
      ,[GraphColor]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[ForeGrndCustomColor]  
      ,[BackgroundCustomColor]  
      ,[IsCalendarEntry]  
      ,[DefaultStartTM]  
      ,[DefualtEndTM]  
      ,[IsAircraftStandby]  
      ,[IsDeleted]
      ,[IsInActive])  
     VALUES  
           (@AircraftDutyID
           ,@CustomerID   
           ,@AircraftDutyCD    
           ,@AircraftDutyDescription  
           ,@ClientID   
           ,@GraphColor   
           ,@LastUpdUID   
           ,@LastUpdTS   
           ,@ForeGrndCustomColor  
           ,@BackgroundCustomColor   
           ,@IsCalendarEntry   
           ,@DefaultStartTM   
           ,@DefualtEndTM   
           ,@IsAircraftStandby  
           ,isnull(@IsDeleted,0)  
           ,isnull(@IsInActive,0)
         )  
           
   
             
             
  
END
go