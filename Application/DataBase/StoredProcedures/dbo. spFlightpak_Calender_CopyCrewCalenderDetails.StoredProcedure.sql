
/****** Object:  StoredProcedure [dbo].[spFlightpak_Calender_CopyCrewCalenderDetails]    Script Date: 03/28/2013 21:06:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Calender_CopyCrewCalenderDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Calender_CopyCrewCalenderDetails]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_Calender_CopyCrewCalenderDetails]    Script Date: 03/28/2013 21:06:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE  [dbo].[spFlightpak_Calender_CopyCrewCalenderDetails]
 -- Add the parameters for the stored procedure here    
 @DepartureDate datetime,    
 @TripID Bigint,    
 @CustomerID Bigint, 
 @CopyORMove bit, 
 @username varchar(30)
AS    
BEGIN    
 DECLARE  @NewTripID BIGINT       
 DECLARE  @NewTripNUM BIGINT 
 DECLARE  @OldTripNUM BIGINT 
 DECLARE  @DateDiffValue INT  
 DECLARE  @RecordType varchar(1)
     
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripID OUTPUT        
 set @NewTripNUM = 0    
 --EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @NewTripNUM OUTPUT      
      
 --select @NewTripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID    
     
select @DateDiffValue = datediff (d, isnull(DepartureDTTMLocal,GETDATE()),@DepartureDate), @OldTripNUM = TripNUM from PreflightLeg where TripID = @TripID    
 
 select @RecordType = RecordType from PreflightMain where TripID=@TripID
 
    
--Main    
 insert into PreflightMain    
  ([TripID]    
   ,[CustomerID]    
   ,[TripNUM]    
   ,[PreviousNUM]    
   ,[TripDescription]    
   ,[FleetID]    
   ,[DispatchNUM]    
   ,[TripStatus]    
   ,[EstDepartureDT]    
   ,[EstArrivalDT]    
   ,[RecordType]    
   ,[AircraftID]    
   ,[PassengerRequestorID]    
   ,[RequestorFirstName]    
   ,[RequestorMiddleName]    
   ,[RequestorLastName]    
   ,[RequestorPhoneNUM]    
   ,[DepartmentDescription]    
   ,[AuthorizationDescription]    
   ,[RequestDT]    
   ,[IsCrew]    
   ,[IsPassenger]    
   ,[IsQuote]    
   ,[IsScheduledServices]    
   ,[IsAlert]    
   ,[Notes]    
   ,[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,[IsAirportAlert]    
   ,[IsLog]    
   ,[BeginningGMTDT]    
   ,[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsPrivate]    
   ,[IsCorpReq]    
   ,[Acknowledge]    
   ,[DispatchNotes]    
   ,[TripRequest]    
   ,[WaitList]    
   ,[FlightNUM]    
   ,[FlightCost]    
   ,[AccountID]    
   ,[TripException]    
   ,[TripSheetNotes]    
   ,[CancelDescription]    
   ,[IsCompleted]    
   ,[IsFBOUpdFlag]    
   ,[RevisionNUM]    
   ,[RevisionDescriptioin]    
   ,[IsPersonal]    
   ,[DsptnName]    
   ,[ScheduleCalendarHistory]    
   ,[ScheduleCalendarUpdate]    
   ,[CommentsMessage]    
   ,[EstFuelQTY]    
   ,[CrewID]    
   ,[ReleasedBy]    
   ,[IsDeleted]    
   ,[VerifyNUM]    
   ,[APISSubmit]    
   ,[APISStatus]    
   ,[APISException]    
   ,[IsAPISValid]    
   ,[DepartmentID]    
   ,[AuthorizationID]    
   ,[DispatcherUserName]    
   ,[EmergencyContactID]
   ,[CQCustomerID])    
     
     
  select @NewTripID    
   ,[CustomerID]    
   ,@NewTripNUM    
   ,[PreviousNUM]    
   ,[TripDescription]    
   ,[FleetID]    
   ,[DispatchNUM]
   ,[TripStatus]    
   ,@DepartureDate    
   ,[EstArrivalDT]    
   ,[RecordType]    
   ,[AircraftID]    
   ,[PassengerRequestorID]    
   ,[RequestorFirstName]    
   ,[RequestorMiddleName]    
   ,[RequestorLastName]    
   ,[RequestorPhoneNUM]    
   ,[DepartmentDescription]    
   ,[AuthorizationDescription]    
   ,[RequestDT]    
   ,[IsCrew]    
   ,[IsPassenger]    
   ,[IsQuote]    
   ,[IsScheduledServices]    
   ,[IsAlert]    
   ,[Notes]    
   ,[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,[IsAirportAlert]    
   ,0    
   ,[BeginningGMTDT]    
   ,[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsPrivate]    
   ,[IsCorpReq]    
   ,[Acknowledge]    
   ,[DispatchNotes]    
   ,[TripRequest]    
   ,[WaitList]    
   ,[FlightNUM]    
   ,[FlightCost]    
   ,[AccountID]    
   ,[TripException]    
   ,[TripSheetNotes]    
   ,[CancelDescription]    
   ,[IsCompleted]    
   ,[IsFBOUpdFlag]    
   ,[RevisionNUM] 
   ,null --[RevisionDescriptioin]    
   ,[IsPersonal]    
   ,[DsptnName]    
   ,[ScheduleCalendarHistory]    
   ,[ScheduleCalendarUpdate]    
   ,[CommentsMessage]    
   ,[EstFuelQTY]    
   ,[CrewID]    
   ,[ReleasedBy]    
   ,[IsDeleted]    
   ,[VerifyNUM]    
   ,null--[APISSubmit]    
   ,null --[APISStatus]    
   ,null --[APISException]    
   ,null --[IsAPISValid]    
   ,[DepartmentID]    
   ,[AuthorizationID]    
   ,[DispatcherUserName]    
   ,[EmergencyContactID]    
   ,[CQCustomerID]    
   from PreflightMain where TripID = @TripID    
     
 if (@CopyORMove=1)    
 begin    
  CREATE Table #TempLeg (    
  rownum int identity (1,1),    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempLeg (NewLegId,OldLegID ) select null, LegID from PreflightLeg where TripID = @TripID    
      
  Declare @Rownumber int    
  DECLARE  @NewLegID BIGINT        
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempLeg where NewLegId is null) >0    
  begin    
   Set @NewLegID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewLegID OUTPUT     
   update #TempLeg set NewLegId = @NewLegID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
     
     
--Legs    
  INSERT INTO [PreflightLeg]        
         ([LegID]    
    ,[TripID]    
    ,[CustomerID]    
    ,[TripNUM]    
    ,[LegNUM]    
    ,[DepartICAOID]    
    ,[ArriveICAOID]    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,[IsApproxTM]    
    ,[IsScheduledServices]    
    ,[DepartureDTTMLocal]    
    ,[ArrivalDTTMLocal]    
    ,[DepartureGreenwichDTTM]    
    ,[ArrivalGreenwichDTTM]    
    ,[HomeDepartureDTTM]    
    ,[HomeArrivalDTTM]    
    ,[GoTime]    
    ,[FBOID]    
    ,[LogBreak]    
    ,[FlightPurpose]    
    ,[PassengerRequestorID]    
    ,[RequestorName]    
    ,[DepartmentID]    
    ,[DepartmentName]    
    ,[AuthorizationID]    
    ,[AuthorizationName]    
    ,[PassengerTotal]   
    ,[SeatTotal]    
    ,[ReservationTotal]   
    ,[ReservationAvailable]   
    ,[WaitNUM]    
    ,[DutyTYPE]    
    ,[DutyHours]    
    ,[RestHours]    
    ,[FlightHours]    
    ,[Notes]    
    ,[FuelLoad]    
    ,[OutbountInstruction]    
    ,[DutyTYPE1]    
    ,[IsCrewDiscount]    
    ,[CrewDutyRulesID]    
    ,[IsDutyEnd]    
    ,[FedAviationRegNUM]    
    ,[ClientID]    
    ,[LegID1]    
    ,[WindReliability]    
    ,[OverrideValue]    
    ,[CheckGroup]    
    ,[AdditionalCrew]    
    ,[PilotInCommand]    
    ,[SecondInCommand]    
    ,[NextLocalDTTM]    
    ,[NextGMTDTTM]    
    ,[NextHomeDTTM]    
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,[CrewNotes]    
    ,[IsPrivate]    
    ,[WaitList]    
    ,[FlightNUM]    
    ,[FlightCost]    
    ,[CrewFuelLoad]    
    ,[IsDeleted]    
    ,[UWAID]    
    ,[USCrossing]    
    ,[Response]    
    ,[ConfirmID]    
    ,[CrewDutyAlert]    
    ,[AccountID]    
    ,[FlightCategoryID] 
    ,[CQCustomerID]   
   )        
       
    select  NewLegId    
    ,@NewTripID    
    ,[CustomerID]    
    ,[TripNUM]    
    ,[LegNUM]    
    ,[DepartICAOID]    
    ,[ArriveICAOID]    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,[IsApproxTM]    
    ,[IsScheduledServices]    
    ,case when [DepartureDTTMLocal] IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[DepartureDTTMLocal])END    
    ,case when [ArrivalDTTMLocal]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[ArrivalDTTMLocal])END    
    ,case when [DepartureGreenwichDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[DepartureGreenwichDTTM])END    
    ,case when [ArrivalGreenwichDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[ArrivalGreenwichDTTM])END    
    ,case when [HomeDepartureDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[HomeDepartureDTTM])END    
    ,case when [HomeArrivalDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[HomeArrivalDTTM])END    
    ,[GoTime]    
    ,[FBOID]    
    ,[LogBreak]    
    ,[FlightPurpose]    
    ,[PassengerRequestorID]    
    ,[RequestorName]    
    ,[DepartmentID]   
    ,[DepartmentName]    
    ,[AuthorizationID]    
    ,[AuthorizationName]    
    ,[PassengerTotal]
    ,[SeatTotal]
    ,[ReservationTotal]
    ,[ReservationAvailable]
    ,[WaitNUM]    
    ,[DutyTYPE]    
    ,[DutyHours]    
    ,[RestHours]    
    ,[FlightHours]    
    ,[Notes]    
    ,[FuelLoad]    
    ,[OutbountInstruction]    
    ,[DutyTYPE1]    
    ,[IsCrewDiscount]    
    ,[CrewDutyRulesID]    
    ,[IsDutyEnd]    
    ,[FedAviationRegNUM]    
    ,[ClientID]    
    ,[LegID1]    
    ,[WindReliability]    
    ,[OverrideValue]    
    ,[CheckGroup]    
    ,[AdditionalCrew]    
    ,[PilotInCommand]    
    ,[SecondInCommand]    
    ,[NextLocalDTTM]    
    ,[NextGMTDTTM]    
    ,[NextHomeDTTM]    
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,[CrewNotes]    
    ,[IsPrivate]    
    ,[WaitList]    
    ,[FlightNUM]    
    ,[FlightCost]    
    ,[CrewFuelLoad]    
    ,[IsDeleted]    
    ,null --[UWAID]    
    ,[USCrossing]    
    ,[Response]    
    ,[ConfirmID]    
    ,[CrewDutyAlert]    
    ,[AccountID]    
    ,[FlightCategoryID]  
    ,[CQCustomerID]  
    from preflightleg PL, #TempLeg T    
    where PL.LegID = T.OldLegID    
 End

if (@RecordType = 'C')
begin
	CREATE Table #TempCrew (    
  rownum int identity (1,1),    
  NewPreflightCrewListID Bigint,    
  oldPreflightCrewListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCrew (NewPreflightCrewListID ,    
   oldPreflightCrewListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightCrewListID], NewLegId, OldLegID    
  from PreflightCrewList PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightCrewListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempCrew where NewPreflightCrewListID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCrewListID OUTPUT     
   update #TempCrew set NewPreflightCrewListID = @NewPreflightCrewListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightCrewList]    
        (    
      [PreflightCrewListID]    
        ,[LegID]    
        ,[CustomerID]    
        ,[CrewID]    
        ,[PassportID]    
        ,[VisaID]       
        ,[HotelName]    
        ,[CrewFirstName]    
        ,[CrewMiddleName]    
        ,[CrewLastName]    
        ,[DutyTYPE]    
        ,[OrderNUM]    
        ,[IsDiscount]    
        ,[LastUpdUID]    
        ,[LastUpdTS]    
        ,[IsBlackberrMailSent]    
        ,[IsNotified]    
        ,[IsDeleted]    
        ,[IsAdditionalCrew]      
        ,[Street]    
        ,[CityName]    
        ,[StateName]    
        ,[PostalZipCD]     
        ,[NoofRooms]    
        ,[ClubCard]    
        ,[IsRateCap]    
        ,[MaxCapAmount]            
        )    
     select       
              
        TC.NewPreflightCrewListID    
        ,TC.NewLegId    
        ,[CustomerID]    
        ,[CrewID]    
        ,[PassportID]    
        ,[VisaID]       
        ,[HotelName]    
        ,[CrewFirstName]    
        ,[CrewMiddleName]    
        ,[CrewLastName]    
        ,[DutyTYPE]    
        ,[OrderNUM]    
        ,[IsDiscount]    
        ,[LastUpdUID]    
        ,[LastUpdTS]    
        ,[IsBlackberrMailSent]    
        ,[IsNotified]    
        ,[IsDeleted]    
        ,[IsAdditionalCrew]      
        ,[Street]    
        ,[CityName]    
        ,[StateName]    
        ,[PostalZipCD]     
        ,[NoofRooms]          ,[ClubCard]    
        ,[IsRateCap]    
        ,[MaxCapAmount]     
            
        from [PreflightCrewList] PC, #TempCrew TC    
        where PC.PreflightCrewListID = TC.oldPreflightCrewListID  
end
    
 Drop table #TempLeg    
 
 select @NewTripID as TripID    
    
    
--exec spFlightpak_Preflight_CopyTripDetails '2012-05-05',10001270658,10001,1,1,1,1    
END  
  



GO


