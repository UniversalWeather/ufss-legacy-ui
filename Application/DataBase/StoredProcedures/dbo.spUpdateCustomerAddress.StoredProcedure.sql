
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomerAddress]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCustomerAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCustomerAddress]
go
CREATE PROCEDURE [dbo].[spUpdateCustomerAddress]  
 (@CustomAddressID BIGINT    
 ,@CustomerID BIGINT    
 ,@UserName VARCHAR(30)    
 ,@Name VARCHAR(40)    
 ,@Address1 VARCHAR(40)    
 ,@Address2 VARCHAR(40)    
 ,@Address3 VARCHAR(40)    
 ,@CustomCity VARCHAR(30)    
 ,@CustomState CHAR(10)    
 ,@PostalZipCD VARCHAR(15)    
 ,@CountryID BIGINT    
 ,@MetropolitanArea CHAR(3)    
 ,@Contact VARCHAR(25)    
 ,@PhoneNum VARCHAR(25)    
 ,@FaxNum VARCHAR(25)    
 ,@ContactEmailID VARCHAR(100)    
 ,@Website VARCHAR(200)    
 ,@Symbol VARCHAR(12)    
 ,@LatitudeDegree NUMERIC(3, 0)    
 ,@LatitudeMinutes NUMERIC(5, 1)    
 ,@LatitudeNorthSouth CHAR(1)    
 ,@LongitudeDegrees NUMERIC(3, 0)    
 ,@LongitudeMinutes NUMERIC(5, 1)    
 ,@LongitudeEastWest CHAR(1)    
 ,@AirportID BIGINT    
 ,@HomebaseID BIGINT    
 ,@Remarks VARCHAR(100)    
 ,@LastUpdUID VARCHAR(30)    
 ,@LastUpdTS DATETIME    
 ,@IsDeleted BIT    
 ,@CustomAddressCD varchar(5)  
 ,@BusinessPhone varchar(25)  
 ,@CellPhoneNum varchar(25)  
 ,@HomeFax varchar(25)  
 ,@CellPhoneNum2 varchar(25)  
 ,@OtherPhone varchar(25)  
 ,@BusinessEmail varchar(250)  
 ,@PersonalEmail varchar(250)  
 ,@OtherEmail varchar(250)
 ,@IsInActive BIT)  
 -- =============================================    
 -- Author:Karthikeyan.S    
 -- Create date: 13/05/2012    
 -- Description: Update the Customer Address information    
 -- =============================================    
AS  BEGIN    
 SET NOCOUNT ON    
 set @LastUpdTS = GETUTCDATE()    
 UPDATE [CustomAddress]    
 SET  UserName = @UserName    
   ,[Name] = @Name    
   ,Address1 = @Address1    
   ,Address2 = @Address2    
   ,Address3 = @Address3    
   ,[CustomCity] = @CustomCity    
   ,[CustomState] = @CustomState    
   ,[PostalZipCD] = @PostalZipCD    
   ,[CountryID] = @CountryID    
   ,[MetropolitanArea] = @MetropolitanArea    
   ,[Contact] = @Contact    
   ,[PhoneNum] = @PhoneNum    
   ,[FaxNum] = @FaxNum    
   ,[ContactEmailID] = @ContactEmailID    
   ,[Website] = @Website    
   ,[Symbol] = @Symbol    
   ,[LatitudeDegree] = @LatitudeDegree    
   ,[LatitudeMinutes] = @LatitudeMinutes    
   ,[LatitudeNorthSouth] = @LatitudeNorthSouth    
   ,[LongitudeDegrees] = @LongitudeDegrees    
   ,[LongitudeMinutes] = @LongitudeMinutes    
   ,[LongitudeEastWest] = @LongitudeEastWest    
   ,AirportID = @AirportID    
   ,HomebaseID = @HomebaseID    
   ,[Remarks] = @Remarks    
   ,[LastUpdUID] = @LastUpdUID    
   ,[LastUpdTS] = @LastUpdTS    
   ,[IsDeleted] = @IsDeleted    
   ,[CustomAddressCD] = @CustomAddressCD  
   ,BusinessPhone=@BusinessPhone  
   ,CellPhoneNum=@CellPhoneNum  
   ,HomeFax=@HomeFax  
   ,CellPhoneNum2=@CellPhoneNum2  
   ,OtherPhone=@OtherPhone  
   ,BusinessEmail=@BusinessEmail  
   ,PersonalEmail=@PersonalEmail  
   ,OtherEmail=@OtherEmail  
   ,IsInActive=@IsInActive
 WHERE   [CustomerID] = @CustomerID AND     
   CustomAddressID = @CustomAddressID         
END    
GO
