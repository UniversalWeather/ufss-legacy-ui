GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spChangePassword]') AND type in (N'P', N'PC'))
DROP PROC [dbo].[spChangePassword]
GO
/*        
Author:   Viswanathan Perumal        
Purpose:  Change Password  
Created Date: 30-08-2012        
Modified By:         
Modified Date:         
*/     
CREATE PROC [dbo].[spChangePassword]  
 @UserName varchar(30),  
 @OldPassword nvarchar(500),  
 @NewPassword nvarchar(500),  
 @CustomerId bigint,  
 @ReturnValue int output  
AS  
BEGIN  
  
SET @ReturnValue = (SELECT Count(UserMaster.UserName) FROM UserMaster   
     JOIN UserPassword ON UserMaster.UserName = UserPassword.UserName    
     where UPPER(UserMaster.UserName) = UPPER(@UserName) AND UserPassword.FPPWD = @OldPassword              
     AND UserPassword.ActiveStartDT <= GETUTCDATE() AND UserPassword.ActiveEndDT IS NULL AND PWDStatus = 1)  
  
IF(@ReturnValue > 0)  
BEGIN  
 DECLARE  @UserPasswordID BIGINT    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @UserPasswordID OUTPUT    
 --Insert New password    
 INSERT INTO UserPassword 
 (UserPasswordID,UserName,FPPWD,ActiveStartDT,ActiveEndDT,PWDStatus,LastUpdUID,LastUpdTS,IsInActive)
 VALUES(@UserPasswordID, @UserName, @NewPassword, GETUTCDATE(), NULL, 1, @UserName, GETUTCDATE(),0)    
 -- Set other passwords inactive for the user except current    
 UPDATE UserPassword SET ActiveEndDT = GETUTCDATE(), PWDStatus = 0, LastUpdUID = @UserName, LastUpdTS = GETUTCDATE() WHERE UserName = @UserName AND     
  UserPasswordID <> @UserPasswordID    
 -- Set ForceChangePassword flag to false
 UPDATE UserMaster SET ForceChangePassword = 0 Where UserName = @UserName
END  
  
END  
GO