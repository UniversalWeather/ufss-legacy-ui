IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteFlightpakFuel]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spDeleteFlightpakFuel]
GO
/****** Object:  StoredProcedure [dbo].[spDeleteFlightpakFuel]    Script Date: 10/3/2015 12:30:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteFlightpakFuel]  
(  
 @CustomerID BIGINT,  
 @FlightpakFuelID BIGINT
)  
AS  
BEGIN
-- =============================================  
-- Author: Srinivas.P
-- Create date: 18/07/2012  
-- Description: Delete FlightpakFuel Informations  
-- =============================================  
 SET NOCOUNT OFF;  
   
DELETE FROM FlightpakFuel WHERE CustomerID=@CustomerID AND FlightpakFuelID=@FlightpakFuelID
END
GO