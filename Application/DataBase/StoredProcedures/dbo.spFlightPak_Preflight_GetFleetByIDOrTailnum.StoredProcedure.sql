
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFleetByIDOrTailnum]    Script Date: 01/24/2014 11:29:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetFleetByIDOrTailnum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetFleetByIDOrTailnum]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFleetByIDOrTailnum]    Script Date: 01/24/2014 11:29:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


   
 CREATE Procedure [dbo].[spFlightPak_Preflight_GetFleetByIDOrTailnum](@CustomerID bigint,
 @Tailnum varchar(9),
 @FleetID Bigint
 )            
AS            
-- =============================================            
-- Author: Badrinath            
-- Create date: 7/5/2012            
-- Description: Get All Fleet Profile Informations 
-- EXEC [spFlightPak_Preflight_GetFleetByIDOrTailnum] 10003, '', 10003153997         
-- =============================================            
SET NOCOUNT ON            
BEGIN  
SELECT [FleetID]  
      ,CASE WHEN [TailNum] IS NULL THEN '' ELSE LTRIM(RTRIM(TailNum)) END AS TailNum  
      ,[CustomerID]           
      ,[AircraftCD]          
      ,[SerialNum]          
      ,[TypeDescription]          
      ,[MaximumReservation]          
      ,[LastInspectionDT]          
      ,[InspectionHrs]          
      ,[WarningHrs]          
      ,[MaximumPassenger]          
      ,[HomebaseID]          
      ,[MaximumFuel]          
      ,[MinimumFuel]          
      ,[BasicOperatingWeight]          
      ,[MimimumRunway]          
      ,[IsInActive]          
      ,[IsDisplay31]          
      ,[SIFLMultiControlled]          
      ,[SIFLMultiNonControled]          
      ,[Notes]          
      ,[ComponentCD]          
      ,[ClientID]          
      ,[FleetType]          
      ,[FlightPhoneNum]          
      ,[Class]          
      ,[VendorID]          
      ,[VendorType]          
      ,[YearMade]          
      ,[ExteriorColor]          
      ,[InteriorColor]          
      ,[IsAFIS]          
      ,[IsUWAData]          
      ,[LastUpdUID]          
      ,[LastUpdTS]          
      ,[FlightPlanCruiseSpeed]          
      ,[FlightPlanMaxFlightLevel]          
      ,[MaximumTakeOffWeight]          
      ,[MaximumLandingWeight]          
      ,[MaximumWeightZeroFuel]          
      ,[TaxiFuel]          
      ,[MultiSec]          
      ,[ForeGrndCustomColor]          
      ,[BackgroundCustomColor]          
      ,[FlightPhoneIntlNum]          
      ,[FleetSize]          
      ,[FltScanDoc]          
      ,[MinimumDay]          
      ,[StandardCrewIntl]          
      ,[StandardCrewDOM]          
      ,[IsFAR91]          
      ,[IsFAR135]          
      ,[EmergencyContactID]          
      ,[IsTaxDailyAdj]          
      ,[IsTaxLandingFee]          
      ,[IsDeleted]          
      ,[AircraftID]          
      ,[CrewID]      
      ,SecondaryDomFlightPhone      
       ,SecondaryIntlFlightPhone    
       ,marginalpercentage                
       FROM [Fleet] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0   
       AND FleetID = case when @FleetID <>0 then @FleetID else FleetID end 
	   AND Tailnum = case when Ltrim(Rtrim(@Tailnum)) <> '' then @Tailnum else TailNum end       
     
END     
GO


