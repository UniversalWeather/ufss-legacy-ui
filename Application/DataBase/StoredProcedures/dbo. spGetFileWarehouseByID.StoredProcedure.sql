
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFileWarehouseByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFileWarehouseByID]
GO

CREATE procedure [dbo].[spGetFileWarehouseByID]
 (
   @CustomerID BIGINT,
   @FileWarehouseID BIGINT
  )  
-- =============================================  
-- Author: Mohanraja
-- Create date: 06/03/2014
-- Description: Added for Getting File details based on FileWarehouseID
-- Fix for #2983 (SUP-428)
-- =============================================  
AS
 
BEGIN   
 SET NoCOUNT ON 

     SELECT  
			FileWarehouseID,
			UWAFileName,
			Case ROW_NUMBER() OVER (ORDER BY FileWarehouseID ) When 1 then UWAFilePath Else Null End as UWAFilePath,
			UWAWebpageName,
			CustomerID,
			RecordID,
			isnull(SequenceNumber,'') as 'SequenceNumber', 
			RecordType
			LastUpdUID,
			LastUpdTS,
			IsDeleted,
			RecordType,
			isnull(IsInActive,0) as 'IsInActive'
	 FROM   FileWarehouse  with(nolock)
		WHERE  CustomerID=@CustomerID AND FileWarehouseID=@FileWarehouseID AND ISNULL(IsDeleted,0) = 0
END

GO