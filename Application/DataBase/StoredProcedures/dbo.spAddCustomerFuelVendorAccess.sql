
GO
/****** Object:  StoredProcedure [dbo].[spAddCustomerFuelVendorAccess]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCustomerFuelVendorAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCustomerFuelVendorAccess]
go
CREATE PROCEDURE [dbo].[spAddCustomerFuelVendorAccess](  
							@CustomerID bigint,
							@FuelVendorID bigint,
							@CanAccess bit,
							@LastUpdUID varchar(30),
							@LastUpdTS DATETIME,
							@IsDeleted bit,
							@IsInActive bit
						)    
-- =============================================      
-- Author:Ajeet      
-- Create date: 10/16/2015      
-- Description: Insert Fuel Vedor Access for Customer      
-- =============================================      
AS      
BEGIN       
SET NOCOUNT ON    
DECLARE @CustomerFuelVendorAccessID AS BIGINT
IF  NOT EXISTS(SELECT CustomerFuelVendorAccessID FROM CustomerFuelVendorAccess WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID)
BEGIN
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
	INSERT INTO CustomerFuelVendorAccess (CustomerID,FuelVendorID,CanAccess,LastUpdUID,LastUpdTS,IsDeleted,IsInActive)    
	VALUES (@CustomerID,@FuelVendorID,@CanAccess,@LastUpdUID,@LastUpdTS,@IsDeleted,@IsInActive)    
 SELECT CustomerFuelVendorAccessID FROM CustomerFuelVendorAccess WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID
END
ELSE
BEGIN
	SELECT @CustomerFuelVendorAccessID=CustomerFuelVendorAccessID FROM CustomerFuelVendorAccess WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID
	EXEC spUpdateCustomerFuelVendorAccess @CustomerFuelVendorAccessID, @CustomerID, @FuelVendorID, @CanAccess, @LastUpdUID, @LastUpdTS,	@IsDeleted,	@IsInActive
	SELECT CustomerFuelVendorAccessID FROM CustomerFuelVendorAccess WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID
END

END