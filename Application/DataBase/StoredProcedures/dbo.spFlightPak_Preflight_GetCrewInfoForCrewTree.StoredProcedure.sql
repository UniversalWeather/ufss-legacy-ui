
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewInfoForCrewTree]    Script Date: 06/04/2013 17:48:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetCrewInfoForCrewTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewInfoForCrewTree]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewInfoForCrewTree]    Script Date: 06/04/2013 17:48:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewInfoForCrewTree]
(
	@CustomerID BIGINT, 
	@ClientID BIGINT, 
	@USERHOMEBASEID BIGINT
)
AS  
IF(@ClientID>0)        
BEGIN

--
	IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
	BEGIN

		-- Select all CREW ids for main tree for given client ID and homebaseOnly option is selected in filter
		SELECT Crew.CrewID,Crew.CrewCD, Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		NULL AS CrewGroupID, NULL AS CrewGroupCD, NULL as CrewGroupDescription, NULL as OrderNum, Crew.HomebaseID  
		FROM Crew 
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
		WHERE Crew.CustomerID = @CustomerID 
		AND Crew.IsDeleted = 0 
		AND Crew.IsNoCalendarDisplay != 1 
		AND Crew.ClientID = @ClientID 
		AND Crew.IsStatus =1
		AND Company.HomebaseID = @USERHOMEBASEID
		UNION
		-- select all CREW groups for given client ID and homebaseOnly option is  selected in filter
		SELECT Crew.CrewID, Crew.CrewCD ,Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		CrewGroup.CrewGroupID, CrewGroup.CrewGroupCD, CrewGroup.CrewGroupDescription,
		CrewGroupOrder.OrderNum , Crew.HomebaseID            
		FROM CrewGroupOrder
		INNER JOIN  Crew  ON CrewGroupOrder.CrewID = Crew.CrewID  AND Crew.IsDeleted = 0 AND Crew.IsNoCalendarDisplay != 1 AND Crew.ClientID = @ClientID
		INNER JOIN CrewGroup ON CrewGroup.CrewGroupID = CrewGroupOrder.CrewGroupID AND CrewGroup.IsDeleted = 0
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
		WHERE  CrewGroupOrder.CrewGroupID IN (SELECT CrewGroupID FROM CrewGroupOrder WHERE CustomerID = @CustomerID ) 
		AND Crew.IsStatus =1
		AND Company.HomebaseID = @USERHOMEBASEID
		ORDER BY CrewGroupCD,CrewCD, OrderNum

	END

	ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
	BEGIN

		-- Select all CREW ids for main tree for given client ID and homebaseOnly option is selected in filter
		SELECT Crew.CrewID,Crew.CrewCD, Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		NULL AS CrewGroupID, NULL AS CrewGroupCD, NULL as CrewGroupDescription, NULL as OrderNum, Crew.HomebaseID  
		FROM Crew 
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID 
		WHERE Crew.CustomerID = @CustomerID AND
		Crew.IsDeleted = 0 AND  
		Crew.IsNoCalendarDisplay != 1 AND 
		Crew.ClientID = @ClientID
		AND Crew.IsStatus =1
		UNION
		-- select all CREW groups for given client ID and homebaseOnly option is  selected in filter
		SELECT Crew.CrewID, Crew.CrewCD ,Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		CrewGroup.CrewGroupID, CrewGroup.CrewGroupCD, CrewGroup.CrewGroupDescription,
		CrewGroupOrder.OrderNum , Crew.HomebaseID            
		FROM CrewGroupOrder
		INNER JOIN  Crew  ON CrewGroupOrder.CrewID = Crew.CrewID  AND Crew.IsDeleted = 0 AND Crew.IsNoCalendarDisplay != 1 AND Crew.ClientID = @ClientID
		INNER JOIN CrewGroup ON CrewGroup.CrewGroupID = CrewGroupOrder.CrewGroupID AND CrewGroup.IsDeleted = 0
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID  
		WHERE  CrewGroupOrder.CrewGroupID IN (SELECT CrewGroupID FROM CrewGroupOrder WHERE CustomerID = @CustomerID ) 
		AND Crew.IsStatus =1 
		ORDER BY CrewGroupCD,CrewCD, OrderNum
	END
--
END
ELSE  --if client id is null or not = 0
BEGIN

--
	IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
	BEGIN

		-- Select all CREW ids for main tree for given client ID and homebaseOnly option is selected in filter
		SELECT Crew.CrewID,Crew.CrewCD, Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		NULL AS CrewGroupID, NULL AS CrewGroupCD, NULL as CrewGroupDescription, NULL as OrderNum, Crew.HomebaseID  
		FROM Crew 
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID AND Company.HomebaseID = @USERHOMEBASEID
		WHERE Crew.CustomerID = @CustomerID 
		AND Crew.IsDeleted = 0 
		AND Crew.IsNoCalendarDisplay != 1 
		AND Crew.IsStatus =1
		AND Company.HomebaseID = @USERHOMEBASEID
		UNION
		-- select all CREW groups for given client ID and homebaseOnly option is  selected in filter
		SELECT Crew.CrewID, Crew.CrewCD ,Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		CrewGroup.CrewGroupID, CrewGroup.CrewGroupCD, CrewGroup.CrewGroupDescription,
		CrewGroupOrder.OrderNum , Crew.HomebaseID            
		FROM CrewGroupOrder
		INNER JOIN  Crew  ON CrewGroupOrder.CrewID = Crew.CrewID  AND Crew.IsDeleted = 0 AND Crew.IsNoCalendarDisplay != 1 
		INNER JOIN CrewGroup ON CrewGroup.CrewGroupID = CrewGroupOrder.CrewGroupID AND CrewGroup.IsDeleted = 0
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID  AND Company.HomebaseID = @USERHOMEBASEID
		WHERE  CrewGroupOrder.CrewGroupID IN (SELECT CrewGroupID FROM CrewGroupOrder WHERE CustomerID = @CustomerID ) 
		AND Crew.IsStatus =1
		AND Company.HomebaseID = @USERHOMEBASEID
		ORDER BY CrewGroupCD,CrewCD, OrderNum
	END
	ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
	BEGIN

		-- Select all CREW ids for main tree for given client ID and homebaseOnly option is selected in filter
		SELECT Crew.CrewID,Crew.CrewCD, Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		NULL AS CrewGroupID, NULL AS CrewGroupCD, NULL as CrewGroupDescription, NULL as OrderNum, Crew.HomebaseID  
		FROM Crew 
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID 
		WHERE Crew.CustomerID = @CustomerID AND
		Crew.IsDeleted = 0 AND  
		Crew.IsNoCalendarDisplay != 1
		AND Crew.IsStatus =1		   
		UNION
		-- select all CREW groups for given client ID and homebaseOnly option is  selected in filter
		SELECT Crew.CrewID, Crew.CrewCD ,Crew.CrewTypeDescription,
		RTRIM(ISNULL(Crew.LastName,'')) as 'LastName',
		RTRIM(ISNULL(Crew.FirstName,'')) as 'FirstName',
		RTRIM(ISNULL(Crew.MiddleInitial,'')) as 'MiddleInitial',
		ISNULL(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'  ,
		CrewGroup.CrewGroupID, CrewGroup.CrewGroupCD, CrewGroup.CrewGroupDescription,
		CrewGroupOrder.OrderNum , Crew.HomebaseID            
		FROM CrewGroupOrder
		INNER JOIN  Crew  ON CrewGroupOrder.CrewID = Crew.CrewID  AND Crew.IsDeleted = 0 AND Crew.IsNoCalendarDisplay != 1 
		INNER JOIN CrewGroup ON CrewGroup.CrewGroupID = CrewGroupOrder.CrewGroupID AND CrewGroup.IsDeleted = 0
		LEFT OUTER JOIN Company  ON Company.HomebaseAirportID = Crew.HomebaseID  
		WHERE  CrewGroupOrder.CrewGroupID IN (SELECT CrewGroupID FROM CrewGroupOrder WHERE CustomerID = @CustomerID ) 
		AND Crew.IsStatus =1
		ORDER BY CrewGroupCD,CrewCD, OrderNum
	END

--
END
--	exec  spFlightPak_Preflight_GetCrewInfoForCrewTree 10001, NULL

GO