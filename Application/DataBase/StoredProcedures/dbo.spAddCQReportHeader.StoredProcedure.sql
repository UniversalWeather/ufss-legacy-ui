

/****** Object:  StoredProcedure [dbo].[spAddCQReportHeader]    Script Date: 04/24/2013 18:59:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCQReportHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCQReportHeader]
GO


/****** Object:  StoredProcedure [dbo].[spAddCQReportHeader]    Script Date: 04/24/2013 18:59:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddCQReportHeader]
(@CQReportHeaderID bigint,
@CustomerID bigint,
@ReportID varchar(8),
@ReportDescription varchar(40),
@HomebaseID bigint,
@UserGroupID bigint,
@ClientID bigint,
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	IF EXISTS(SELECT CQReportHeaderID FROM CQReportHeader WHERE CQReportHeaderID=@CQReportHeaderID AND CustomerID=@CustomerID)
		BEGIN		
			UPDATE	CQReportHeader
			SET		CustomerID=@CustomerID,
					ReportID=@ReportID,
					ReportDescription=@ReportDescription,
					HomebaseID=@HomebaseID,
					UserGroupID=@UserGroupID,
					ClientID=@ClientID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=@LastUpdTS,
					IsDeleted=@IsDeleted
			WHERE	CQReportHeaderID=@CQReportHeaderID
		END
	ELSE
		BEGIN
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CQReportHeaderID OUTPUT
	
			INSERT INTO CQReportHeader
				(CQReportHeaderID
				,CustomerID
				,ReportID
				,ReportDescription
				,HomebaseID
				,UserGroupID
				,ClientID
				,LastUpdUID
				,LastUpdTS
				,IsDeleted)
			VALUES
				(@CQReportHeaderID
				,@CustomerID
				,@ReportID
				,@ReportDescription
				,@HomebaseID
				,@UserGroupID
				,@ClientID
				,@LastUpdUID
				,@LastUpdTS
				,@IsDeleted)
				
				EXECUTE dbo.spAddDefaultCQReportDetail @CustomerID, @CQReportHeaderID
		END

	SELECT @CQReportHeaderID as CQReportHeaderID

END
GO


