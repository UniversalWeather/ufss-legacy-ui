


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceSummary]
(
	@CQInvoiceSummaryID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQInvoiceSummary] WHERE [CQInvoiceSummaryID] = @CQInvoiceSummaryID AND [CustomerID] = @CustomerID
GO


