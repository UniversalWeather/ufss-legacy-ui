
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQQuoteDetail]    Script Date: 03/13/2013 14:53:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQQuoteDetail]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQQuoteDetail]    Script Date: 03/13/2013 14:53:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQQuoteDetail]
(
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@CQLegID bigint,
	@IsPrintable bit,
	@DepartureDT date,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@FlightHours numeric(5, 1),
	@Distance numeric(5, 0),
	@PassengerTotal numeric(3, 0),
	@FlightCharge numeric(17, 2),
	@IsTaxable bit,
	@TaxRate numeric(5, 2),
	@DAirportID bigint,
	@AAirportID bigint,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@LegNum bigint
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()

	DECLARE @CQQuoteDetailID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQQuoteDetailID OUTPUT 
	
	INSERT INTO [CQQuoteDetail] ([CQQuoteDetailID], [CustomerID], [CQQuoteMainID], [CQLegID], [IsPrintable], [DepartureDT], [FromDescription], [ToDescription], [FlightHours], [Distance], [PassengerTotal], [FlightCharge], [IsTaxable], [TaxRate], [DAirportID], [AAirportID], [DepartureDTTMLocal], [ArrivalDTTMLocal], [LastUpdUID], [LastUpdTS], [IsDeleted],[LegNum]) 
		VALUES (@CQQuoteDetailID, @CustomerID, @CQQuoteMainID, @CQLegID, @IsPrintable, @DepartureDT, @FromDescription, @ToDescription, @FlightHours, @Distance, @PassengerTotal, @FlightCharge, @IsTaxable, @TaxRate, @DAirportID, @AAirportID, @DepartureDTTMLocal, @ArrivalDTTMLocal, @LastUpdUID, @LastUpdTS, @IsDeleted,@LegNum);
	
	SELECT @CQQuoteDetailID as CQQuoteDetailID


GO


