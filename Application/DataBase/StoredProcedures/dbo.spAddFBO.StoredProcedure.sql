/****** Object:  StoredProcedure [dbo].[spAddFBO]    Script Date: 09/13/2012 12:27:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFBO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFBO]
GO
/****** Object:  StoredProcedure [dbo].[spAddFBO]    Script Date: 09/13/2012 12:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddFBO]( 
 @FBOID BIGINT,
 @AirportID BIGINT,
 @FBOCD CHAR(4) ,
 @CustomerID BIGINT ,  
 @IsChoice BIT , 
 @FBOVendor VARCHAR(60) ,  
 @PhoneNUM1 VARCHAR(25) ,  
 @Frequency VARCHAR(7) ,  
 @IsUWAAirPartner BIT ,  
 @IsCrewCar BIT ,  
 @FuelBrand VARCHAR(100) ,  
 @ControlNum CHAR(5) ,  
 @FaxNum VARCHAR(25) ,  
 @LastFuelDT DATE ,  
 @LastFuelPrice NUMERIC(9, 4) ,  
 @NegotiatedFuelPrice NUMERIC(7, 4) ,  
 @Addr1 VARCHAR(150) ,  
 @Addr2 VARCHAR(150) ,  
 @CityName VARCHAR(25) ,  
 @StateName VARCHAR(25) ,  
 @PostalZipCD VARCHAR(15) ,  
 @CountryID BIGINT,  
 @Contact VARCHAR(25) ,  
 @Remarks varchar(max) ,  
 @IsFPK BIT ,  
 @FPKUpDATEDT DATETIME ,  
 @LastUpdUID VARCHAR(30) ,  
 @LastUpdTS DATETIME ,  
 @RecordType CHAR(1) ,  
 @IsInActive BIT ,  
 @PhoneNUM2 VARCHAR(25) ,  
 @HoursOfOperation VARCHAR(20) ,  
 @SourceID VARCHAR(12) ,  
 @UpDATEDT DATE ,  
 @IsPrimaryChoice BIT ,  
 @PostedPrice NUMERIC(10, 4) ,  
 @PaymentType CHAR(2) ,  
 @FuelQty NUMERIC(15, 2) ,  
 @FixBaseOperatorID VARCHAR(20) ,  
 @HHEATED CHAR(1) ,  
 @HSJET CHAR(1) ,  
 @HMJET CHAR(1) ,  
 @HLJET CHAR(1) ,  
 @HCJET CHAR(1) ,  
 @FLSJETA CHAR(1) ,  
 @FLS100LL CHAR(1) ,  
 @FLS80 CHAR(1) ,  
 @FSSJETA CHAR(1) ,  
 @FSS100LL CHAR(1) ,  
 @FSS80 CHAR(1) ,  
 @FVOLDISCT CHAR(1) ,  
 @ARFEE CHAR(1) ,  
 @ARFEECOM VARCHAR(100) ,  
 @AHFEE CHAR(1) ,  
 @AHFEECOM VARCHAR(100) ,  
 @A24 CHAR(1) ,  
 @AQT CHAR(1) ,  
 @ASEC CHAR(1) ,  
 @AddDeIcing CHAR(1) ,  
 @AOX CHAR(1) ,  
 @ANIT CHAR(1) ,  
 @ALAV CHAR(1) ,  
 @AGPU CHAR(1) ,  
 @AOWGR CHAR(1) ,  
 @AMMINOR CHAR(1) ,  
 @AMMAJOR CHAR(1) ,  
 @AMAV CHAR(1) ,  
 @AMAUTH CHAR(1) ,  
 @AMAUTHCOM VARCHAR(100) ,  
 @AICLEAN CHAR(1) ,  
 @AICS CHAR(1) ,  
 @AIIR CHAR(1) ,  
 @AEWAS CHAR(1) ,  
 @AEBRITE CHAR(1) ,  
 @AEPAINT CHAR(1) ,  
 @PWPS CHAR(1) ,  
 @PFPR CHAR(1) ,  
 @PCT CHAR(1) ,  
 @PCC CHAR(1) ,  
 @PLOUNGE CHAR(1) ,  
 @PREC CHAR(1) ,  
 @PEXEC CHAR(1) ,  
 @PKIT CHAR(1) ,  
 @PLAUND CHAR(1) ,  
 @PSR CHAR(1) ,  
 @PSHOWER CHAR(1) ,  
 @PSUPP CHAR(1) ,  
 @PMEOS CHAR(1) ,  
 @RHERTZ CHAR(1) ,  
 @RAVIS CHAR(1) ,  
 @ROTHER CHAR(1) ,  
 @ROCOMM VARCHAR(100) ,  
 @PPLIMO CHAR(1) ,  
 @PPTAXI CHAR(1) ,  
 @PPROS CHAR(1) ,  
 @PPRWD CHAR(1) ,  
 @PPCONC CHAR(1) ,  
 @PPCS CHAR(1) ,  
 @PPCR CHAR(1) ,  
 @PPLSR CHAR(1) ,  
 @PPSEC CHAR(1) ,  
 @PPCWIA CHAR(1) ,  
 @PPMS CHAR(1) ,  
 @PPFS CHAR(1) ,  
 @PPCA CHAR(1) ,  
 @PPGOLF CHAR(1) ,  
 @PPCOFFEE CHAR(1) ,  
 @PPSNACKS CHAR(1) ,  
 @PPNEWS CHAR(1) ,  
 @PPICE CHAR(1) ,  
 @PXMF CHAR(1) ,  
 @PXCOFFEE CHAR(1) ,  
 @PXBAR CHAR(1) ,  
 @PXCCC CHAR(1) ,  
 @PXVCC CHAR(1) ,  
 @PXLOUNGE CHAR(1) ,  
 @PXGIFT CHAR(1) ,  
 @OHF CHAR(1) ,  
 @OPCS CHAR(1) ,  
 @OTCS CHAR(1) ,  
 @OJCS CHAR(1) ,  
 @OHCS CHAR(1) ,  
 @OAPS CHAR(1) ,  
 @OCH CHAR(1) ,  
 @CreditCardVISA CHAR(1) ,  
 @CreditCardMasterCard CHAR(1) ,  
 @CreditCardAMEX CHAR(1) ,  
 @CreditCardDinerClub CHAR(1) ,  
 @CreditCardJCB CHAR(1) ,  
 @CreditCardMS CHAR(1) ,  
 @CreditCardAVCard CHAR(1) ,  
 @CreditCardUVAir CHAR(1) ,  
 @COMJET CHAR(1) ,  
 @TollFreePhoneNum VARCHAR(25) ,  
 @SITA VARCHAR(15) ,  
 @UNICOM VARCHAR(10) ,  
 @ARINC VARCHAR(10) ,  
 @IsUVAirPNR BIT ,  
 @Website VARCHAR(200) ,  
 @EmailAddress VARCHAR(200) ,  
 @LatitudeDegree NUMERIC(3, 0) ,  
 @LatitudeMinutes NUMERIC(5, 1) ,  
 @LatitudeNorthSouth CHAR(1) ,  
 @LongitudeDegrees NUMERIC(3, 0) ,  
 @LongitudeMinutes NUMERIC(5, 1) ,  
 @LongitudeEastWest CHAR(1) ,  
 @IsUWAMaINTFlag BIT ,  
 @UWAID VARCHAR(20) ,  
 @ReceiptNum INT ,  
 @IsDeleted BIT,   
 @UWAUpdates BIT,
 @ContactBusinessPhone varchar(25),
 @ContactCellPhoneNum varchar(25),
 @ContactEmail varchar(250),
 @Addr3 varchar(150),
 @CustomField1	VARCHAR(250),
@CustomField2	VARCHAR(250),
@ExchangeRateID	BIGINT,
@NegotiatedTerms	VARCHAR(800),
@SundayWorkHours	VARCHAR(15),
@MondayWorkHours	VARCHAR(15),
@TuesdayWorkHours	VARCHAR(15),
@WednesdayWorkHours	VARCHAR(15),
@ThursdayWorkHours	VARCHAR(15),
@FridayWorkHours	VARCHAR(15),
@SaturdayWorkHours	VARCHAR(15)
)  
-- =============================================  
-- Author:Hamsha S  
-- Create date: 09/05/2012  
-- Description: Insert the FBO information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON 
DECLARE @UWACustomerID BIGINT   
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output 
 DECLARE @ICAOCD VARCHAR(50)
 DECLARE @UWAAirportId VARCHAR(50)    
 select @ICAOCD =  icaoid from Airport where AirportID = @AirportID  
 select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD
 
IF not exists (select CustomerID from FBO where  FBOID=@FBOID)  
begin 
IF not exists (select CustomerID from FBO where  (AirportID=@AirportID or AirportID = @UWAAirportId)   and (CustomerID = @CustomerID or CustomerID = @UWACustomerID))  
begin  
select @FBOCD='0001'  
end  
else   
begin  
select @FBOCD=right('0000'+  cast( MAX(Convert(int,FBOCD))+1 as varchar(5)),4) from FBO where (AirportID=@AirportID or AirportID = @UWAAirportId)   and (CustomerID = @CustomerID or CustomerID = @UWACustomerID)
end 
end 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FBOID OUTPUT
 
SET @LastUpdTS = GETUTCDATE() 
SET NoCOUNT ON  
IF(@IsChoice='true')
   begin
   update FBO set IsChoice='false' where CustomerID=@CustomerID  and (AirportID=@AirportID or AirportID = @UWAAirportId)
   end 
   INSERT INTO [FBO](
        [FBOID]
           ,[AirportID]
           ,[FBOCD]
           ,[CustomerID]
           ,[IsChoice]
           ,[FBOVendor]
           ,[PhoneNUM1]
           ,[Frequency]
           ,[IsUWAAirPartner]
           ,[IsCrewCar]
           ,[FuelBrand]
           ,[ControlNum]
           ,[FaxNum]
           ,[LastFuelDT]
           ,[LastFuelPrice]
           ,[NegotiatedFuelPrice]
           ,[Addr1]
           ,[Addr2]
           ,[CityName]
           ,[StateName]
           ,[PostalZipCD]
           ,[CountryID]
           ,[Contact]
           ,[Remarks]
           ,[IsFPK]
           ,[FPKUpdateDT]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[RecordType]
           ,[IsInActive]
           ,[PhoneNUM2]
           ,[HoursOfOperation]
           ,[SourceID]
           ,[UpdateDT]
           ,[IsPrimaryChoice]
           ,[PostedPrice]
           ,[PaymentType]
           ,[FuelQty]
           ,[FixBaseOperatorID]
           ,[HHEATED]
           ,[HSJET]
           ,[HMJET]
           ,[HLJET]
           ,[HCJET]
           ,[FLSJETA]
           ,[FLS100LL]
           ,[FLS80]
           ,[FSSJETA]
           ,[FSS100LL]
           ,[FSS80]
           ,[FVOLDISCT]
           ,[ARFEE]
           ,[ARFEECOM]
           ,[AHFEE]
           ,[AHFEECOM]
           ,[A24]
           ,[AQT]
           ,[ASEC]
           ,[AddDeIcing]
           ,[AOX]
           ,[ANIT]
           ,[ALAV]
           ,[AGPU]
           ,[AOWGR]
           ,[AMMINOR]
           ,[AMMAJOR]
           ,[AMAV]
           ,[AMAUTH]
           ,[AMAUTHCOM]
           ,[AICLEAN]
           ,[AICS]
           ,[AIIR]
           ,[AEWAS]
           ,[AEBRITE]
           ,[AEPAINT]
           ,[PWPS]
           ,[PFPR]
           ,[PCT]
           ,[PCC]
           ,[PLOUNGE]
           ,[PREC]
           ,[PEXEC]
           ,[PKIT]
           ,[PLAUND]
           ,[PSR]
           ,[PSHOWER]
           ,[PSUPP]
           ,[PMEOS]
           ,[RHERTZ]
           ,[RAVIS]
           ,[ROTHER]
           ,[ROCOMM]
           ,[PPLIMO]
           ,[PPTAXI]
           ,[PPROS]
           ,[PPRWD]
           ,[PPCONC]
           ,[PPCS]
           ,[PPCR]
           ,[PPLSR]
           ,[PPSEC]
           ,[PPCWIA]
           ,[PPMS]
           ,[PPFS]
           ,[PPCA]
           ,[PPGOLF]
           ,[PPCOFFEE]
           ,[PPSNACKS]
           ,[PPNEWS]
           ,[PPICE]
           ,[PXMF]
           ,[PXCOFFEE]
           ,[PXBAR]
           ,[PXCCC]
           ,[PXVCC]
           ,[PXLOUNGE]
           ,[PXGIFT]
           ,[OHF]
           ,[OPCS]
           ,[OTCS]
           ,[OJCS]
           ,[OHCS]
           ,[OAPS]
           ,[OCH]
           ,[CreditCardVISA]
           ,[CreditCardMasterCard]
           ,[CreditCardAMEX]
           ,[CreditCardDinerClub]
           ,[CreditCardJCB]
           ,[CreditCardMS]
           ,[CreditCardAVCard]
           ,[CreditCardUVAir]
           ,[COMJET]
           ,[TollFreePhoneNum]
           ,[SITA]
           ,[UNICOM]
           ,[ARINC]
           ,[IsUVAirPNR]
           ,[Website]
           ,[EmailAddress]
           ,[LatitudeDegree]
           ,[LatitudeMinutes]
           ,[LatitudeNorthSouth]
           ,[LongitudeDegrees]
           ,[LongitudeMinutes]
           ,[LongitudeEastWest]
           ,[IsUWAMaintFlag]
           ,[UWAID]
           ,[ReceiptNum]
           ,[IsDeleted]
           ,[UWAUpdates]
           ,[ContactBusinessPhone]
           ,[ContactCellPhoneNum]
           ,[ContactEmail]
           ,[Addr3]
           ,CustomField1  
     ,CustomField2  
     ,ExchangeRateID  
     ,NegotiatedTerms  
     ,SundayWorkHours  
     ,MondayWorkHours  
     ,TuesdayWorkHours  
     ,WednesdayWorkHours  
     ,ThursdayWorkHours  
     ,FridayWorkHours  
     ,SaturdayWorkHours )  
  
      VALUES(@FBOID
           ,@AirportID
           ,@FBOCD
           ,@CustomerID
           ,@IsChoice
           ,@FBOVendor
           ,@PhoneNUM1
           ,@Frequency
           ,@IsUWAAirPartner
           ,@IsCrewCar
           ,@FuelBrand
           ,@ControlNum
           ,@FaxNum
           ,@LastFuelDT
           ,@LastFuelPrice
           ,@NegotiatedFuelPrice
           ,@Addr1
           ,@Addr2
           ,@CityName
           ,@StateName
           ,@PostalZipCD
           ,@CountryID
           ,@Contact
           ,@Remarks
           ,@IsFPK
           ,@FPKUpdateDT
           ,@LastUpdUID
           ,@LastUpdTS
           ,@RecordType
           ,@IsInActive
           ,@PhoneNUM2
           ,@HoursOfOperation
           ,@SourceID
           ,@UpdateDT
           ,@IsPrimaryChoice
           ,@PostedPrice
           ,@PaymentType
           ,@FuelQty
           ,@FixBaseOperatorID
           ,@HHEATED
           ,@HSJET
           ,@HMJET
           ,@HLJET
           ,@HCJET
           ,@FLSJETA
           ,@FLS100LL
           ,@FLS80
           ,@FSSJETA
           ,@FSS100LL
           ,@FSS80
           ,@FVOLDISCT
           ,@ARFEE
           ,@ARFEECOM
           ,@AHFEE
           ,@AHFEECOM
           ,@A24
           ,@AQT
           ,@ASEC
           ,@AddDeIcing
           ,@AOX
           ,@ANIT
           ,@ALAV
           ,@AGPU
           ,@AOWGR
           ,@AMMINOR
           ,@AMMAJOR
           ,@AMAV
           ,@AMAUTH
           ,@AMAUTHCOM
           ,@AICLEAN
           ,@AICS
           ,@AIIR
           ,@AEWAS
           ,@AEBRITE
           ,@AEPAINT
           ,@PWPS
           ,@PFPR
           ,@PCT
           ,@PCC
           ,@PLOUNGE
           ,@PREC
           ,@PEXEC
           ,@PKIT
           ,@PLAUND
           ,@PSR
           ,@PSHOWER
           ,@PSUPP
           ,@PMEOS
           ,@RHERTZ
           ,@RAVIS
           ,@ROTHER
           ,@ROCOMM
           ,@PPLIMO
           ,@PPTAXI
           ,@PPROS
           ,@PPRWD
           ,@PPCONC
           ,@PPCS
           ,@PPCR
           ,@PPLSR
           ,@PPSEC
           ,@PPCWIA
           ,@PPMS
           ,@PPFS
           ,@PPCA
           ,@PPGOLF
           ,@PPCOFFEE
           ,@PPSNACKS
           ,@PPNEWS
           ,@PPICE
           ,@PXMF
           ,@PXCOFFEE
           ,@PXBAR
           ,@PXCCC
           ,@PXVCC
           ,@PXLOUNGE
           ,@PXGIFT
           ,@OHF
           ,@OPCS
           ,@OTCS
           ,@OJCS
           ,@OHCS
           ,@OAPS
           ,@OCH
           ,@CreditCardVISA
           ,@CreditCardMasterCard
           ,@CreditCardAMEX
           ,@CreditCardDinerClub
           ,@CreditCardJCB
           ,@CreditCardMS
           ,@CreditCardAVCard
           ,@CreditCardUVAir
           ,@COMJET
           ,@TollFreePhoneNum
           ,@SITA
           ,@UNICOM
           ,@ARINC
           ,@IsUVAirPNR
           ,@Website
           ,@EmailAddress
           ,@LatitudeDegree
           ,@LatitudeMinutes
           ,@LatitudeNorthSouth
           ,@LongitudeDegrees
           ,@LongitudeMinutes
           ,@LongitudeEastWest
           ,@IsUWAMaintFlag
           ,@UWAID
           ,@ReceiptNum
           ,@IsDeleted
           ,@UWAUpdates
           ,@ContactBusinessPhone
           ,@ContactCellPhoneNum
           ,@ContactEmail
           ,@Addr3
           ,@CustomField1  
     ,@CustomField2  
     ,@ExchangeRateID  
     ,@NegotiatedTerms  
     ,@SundayWorkHours  
     ,@MondayWorkHours  
     ,@TuesdayWorkHours  
     ,@WednesdayWorkHours  
     ,@ThursdayWorkHours  
     ,@FridayWorkHours  
     ,@SaturdayWorkHours
           )  
  
  
END
GO


