

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripByTripID]    Script Date: 09/19/2013 14:40:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetTripByTripID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetTripByTripID]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetTripByTripID]    Script Date: 09/19/2013 14:40:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create Procedure [dbo].[spFlightPak_Preflight_GetTripByTripID]
(
@CustomerID BIGINT,
@TripID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get Preflightmain by Trip ID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PM.*
FROM PreflightMain PM 
 where 
 PM.TripID = @TripID 
 and PM.CustomerID=@CustomerID and PM.IsDeleted=0
 and PM.TripID in (
	Select TripID from Preflightleg PL where PL.TripID = @TripID and  PL.CustomerID = @CustomerID
	)
 


GO


