
GO
/****** Object:  StoredProcedure [dbo].[spAddPaymentType]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPaymentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPaymentType]
go
CREATE Procedure [dbo].[spAddPaymentType](

	
	@PaymentTypeCD CHAR(2),
	@CustomerID BIGINT, 
	@PaymentTypeDescription varchar(30) ,
	@LastUpdUID varchar(30) ,
	@LastUpdTS datetime ,
	@IsDeleted bit,
	@IsInActive bit)
  
-- =============================================
-- Author:Hamsha.S
-- Create date: 04/05/2012
-- Description: Insert the PaymentType information
-- =============================================
as
begin 
SET NoCOUNT ON

--To generate PrimaryKey when CustomerId is not available set the value as 0
DECLARE @PaymentTypeID BIGINT
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @PaymentTypeID OUTPUT
--End of Primary Key generation

DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime

   INSERT INTO  [PaymentType]
    (
     PaymentTypeID 
	,PaymentTypeCD
	,CustomerID
	,PaymentTypeDescription 
	,LastUpdUID  
	,LastUpdTS  
	,IsDeleted
	,IsInActive )

     VALUES
           (
     @PaymentTypeID 
	,@PaymentTypeCD
	,@CustomerID 
	,@PaymentTypeDescription 
	,@LastUpdUID 
	,@LastUpdTS
	,@IsDeleted 
	,@IsInActive
        )


END
GO
