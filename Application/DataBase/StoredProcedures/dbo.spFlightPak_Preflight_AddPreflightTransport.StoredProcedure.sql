
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightTransport]    Script Date: 04/01/2013 16:15:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightTransport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightTransport]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightTransport]    Script Date: 04/01/2013 16:15:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightTransport]    
 (             
           @PreflightTransportName varchar(60),    
           @LegID bigint,    
           @TransportID bigint,    
           @IsArrivalTransport bit,    
           @IsDepartureTransport bit,    
           @AirportID bigint,    
           @CrewPassengerType char(1),    
           @Street varchar(50),    
           @CityName varchar(20),    
           @StateName varchar(20),    
           @PostalZipCD char(20),    
           @PhoneNum1 varchar(25),    
           @PhoneNum2 varchar(25),    
           @PhoneNum3 varchar(25),    
           @PhoneNum4 varchar(25),    
           @FaxNUM varchar(25),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @ConfirmationStatus varchar(max),    
           @Comments varchar(max),    
           @IsCompleted bit,    
           @CustomerID bigint,    
           @Status varchar(20)               
    )    
AS    
BEGIN     
SET NOCOUNT ON    
--DECLARE @CustomerID BIGINT    
--SET @CustomerID = (Select CustomerID from PreflightLeg where LegID in (select MAX(LegID) from PreflightLeg))    
--SET @CustomerID = 10000    
    
DECLARE @PreflightTransportID BIGINT       
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @PreflightTransportID OUTPUT    
    
Print @PreflightTransportID    
       
INSERT INTO [dbo].[PreflightTransportList]    
           ([PreflightTransportID]    
           ,[PreflightTransportName]    
           ,[LegID]    
           ,[TransportID]    
           ,[IsArrivalTransport]    
           ,[IsDepartureTransport]    
           ,[AirportID]    
           ,[CrewPassengerType]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[IsCompleted]               
           ,[CustomerID]    
           ,[Status]    
           )    
     VALUES    
           (    
           @PreflightTransportID ,    
           @PreflightTransportName ,    
           @LegID ,    
           @TransportID ,    
           @IsArrivalTransport ,    
           @IsDepartureTransport ,    
           @AirportID ,    
           @CrewPassengerType ,    
           @Street ,    
           @CityName ,    
           @StateName ,    
           @PostalZipCD ,    
           @PhoneNum1 ,    
           @PhoneNum2 ,    
           @PhoneNum3 ,    
           @PhoneNum4 ,    
           @FaxNUM ,    
           @LastUpdUID ,    
           @LastUpdTS ,    
           @IsDeleted ,    
           @ConfirmationStatus,    
           @Comments,    
           @IsCompleted,    
           @CustomerID,    
           @Status    
           )    
            Select @PreflightTransportID as PreflightTransportID      
END    
    


GO


