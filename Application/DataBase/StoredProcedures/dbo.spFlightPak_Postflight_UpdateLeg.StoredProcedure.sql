
/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_UpdateLeg]    Script Date: 02/18/2014 11:41:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_UpdateLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_UpdateLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_UpdateLeg]    Script Date: 02/18/2014 11:41:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Postflight_UpdateLeg]
(
	@POLegID bigint,
	@POLogID bigint,
	@LegNUM bigint,
	@TripLegID bigint,
	@CustomerID bigint,
	@DepartICAOID bigint,
	@ArriveICAOID bigint,
	@Distance numeric(5),
	@ScheduledTM datetime = null,
	@BlockOut char(5),
	@BlockIN char(5),
	@TimeOff char(5),
	@TimeOn char(5),
	@BlockHours numeric(12, 3),
	@FlightHours numeric(12, 3),
	@FedAviationRegNum char(3),
	@ClientID bigint,
	@FlightPurpose varchar(40),
	@DutyTYPE numeric(1, 0),
	@CrewCurrency char(4),
	@IsDutyEnd bit,
	@FuelOut numeric(6, 0),
	@FuelIn numeric(6, 0),
	@FuelUsed numeric(7, 0),
	@FuelBurn numeric(1, 0),
	@DelayTM numeric(6, 3),
	@DutyHrs numeric(12, 3),
	@RestHrs numeric(12, 3),
	@NextDTTM datetime = null,
	@DepatureFBO bigint,
	@ArrivalFBO bigint,
	@CrewID bigint,
	@IsException bit,
	@IsAugmentCrew bit,
	@BeginningDutyHours char(5),
	@EndDutyHours char(5),
	@LastUpdUID char(30),
	@LastUpdTS datetime = null,
	@PassengerTotal numeric(4, 0),
	@ScheduleDTTMLocal datetime = null,
	@BlockOutLocal char(5),
	@BlockInLocal char(5),
	@TimeOffLocal char(5),
	@TimeOnLocal char(5),
	@OutboundDTTM datetime = null,
	@InboundDTTM datetime = null,
	@AdditionalDist numeric(5, 0),
	@AVTrak char(1),
	@FlightNum varchar(12),
	@CargoOut numeric(7, 0),
	@CargoIn numeric(7, 0),
	@CargoThru numeric(7, 0),
	@StatuteMiles numeric(5),
	@FlightCost numeric(12, 2),
	@TechLog varchar(12),
	@AirFrameHours numeric(11, 3),
	@AirframeCycle numeric(6, 0),
	@Engine1Hours numeric(11, 3),
	@Engine2Hours numeric(11, 3),
	@Engine3Hours numeric(11, 3),
	@Engine4Hours numeric(11, 3),
	@Engine1Cycle numeric(6, 0),
	@Engine2Cycle numeric(6, 0),
	@Engine3Cycle numeric(6, 0),
	@Engine4Cycle numeric(6, 0),
	@Re1Hours numeric(11, 3),
	@Re2Hours numeric(11, 3),
	@Re3Hours numeric(11, 3),
	@Re4Hours numeric(11, 3),
	@Re1Cycle numeric(6, 0),
	@Re2Cycle numeric(6, 0),
	@Re3Cycle numeric(6, 0),
	@Re4Cycle numeric(6, 0),
	@FuelOn numeric(17, 6),
	@IsDeleted bit,
	@AccountID bigint,
	@DelayTypeID bigint,
	@PassengerRequestorID bigint,
	@DepartmentID bigint,
	@AuthorizationID bigint,
	@FlightCategoryID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @LastUpdTS = GETUTCDATE()
	
	--// Update Fleet Information based on Completed Status field from POSTFLIGHT.
	DECLARE @IsCompleted bit
	DECLARE @FleetID BIGINT
	DECLARE @AircraftCD VARCHAR(100)
	SET @IsCompleted = 0
	
	--// Check If IsCompleted status is Unchecked from UI, but it's already in Completed Status before. 
	--// Then Deduct the Airframe and Engine Hours from Fleet Information Table.
	DECLARE @OldStatus BIT
	DECLARE @FleetInformationID BIGINT
	
	SELECT @OldStatus = IsCompleted FROM [PostflightMain] WHERE POLogID = @POLogID AND IsNull(IsDeleted,0) = 0

	SELECT @IsCompleted = IsCompleted, @FleetID = FleetID FROM PostflightMain WHERE POLogID = @POLogID AND Isnull(IsDeleted,0) = 0
	
	SELECT @AircraftCD = AircraftCD FROM Fleet with(nolock) WHERE FleetID = @FleetID AND Isnull(IsDeleted,0) = 0

	DECLARE @OldFleetInfo Table (AirFrameHours numeric(11,3), Engine1Hours numeric(11,3), Engine2Hours numeric(11,3), Engine3Hours numeric(11,3)
		, Engine4Hours numeric(11,3), AirframeCycle numeric(6,0), Engine1Cycle numeric(6,0), Engine2Cycle numeric(6,0), Engine3Cycle numeric(6,0), Engine4Cycle numeric(6,0)
		, Reverse1Hrs numeric(11,3), Reverse2Hrs numeric(11,3), Reverse3Hrs numeric(11,3), Reverse4Hrs numeric(11,3)
		, Reverser1Cycle numeric(6,0), Reverser2Cycle numeric(6,0), Reverser3Cycle numeric(6,0), Reverser4Cycle numeric(6,0))

	--// SUM the Airframe and Engine Hour Values and save it in Temporary Table
	INSERT INTO @OldFleetInfo (AirFrameHours, Engine1Hours, Engine2Hours, Engine3Hours,  Engine4Hours
		, AirframeCycle, Engine1Cycle , Engine2Cycle , Engine3Cycle , Engine4Cycle
		, Reverse1Hrs, Reverse2Hrs, Reverse3Hrs, Reverse4Hrs, Reverser1Cycle, Reverser2Cycle, Reverser3Cycle, Reverser4Cycle)
	SELECT IsNull(AirFrameHours,0), IsNull(Engine1Hours,0), IsNull(Engine2Hours,0), IsNull(Engine3Hours,0), IsNull(Engine4Hours,0)
		, IsNull(AirframeCycle,0), IsNull(Engine1Cycle,0), IsNull(Engine2Cycle,0), IsNull(Engine3Cycle,0), IsNull(Engine4Cycle,0)
		, IsNull(Re1Hours,0), IsNull(Re2Hours,0), IsNull(Re3Hours,0), IsNull(Re4Hours,0)
		, IsNull(Re1Cycle,0), IsNull(Re2Cycle,0), IsNull(Re3Cycle,0), IsNull(Re4Cycle,0)
	FROM PostflightLeg WHERE POLegID = @POLegID
			
	IF IsNull(@OldStatus,0) = 1 AND (ISNULL(@IsCompleted,0) = 1 OR ISNULL(@IsCompleted,0) = 0)
	BEGIN
		--// Check If Leg(s) details Exists
		 IF EXISTS (SELECT NULL FROM [FleetInformation] WHERE FleetID = @FleetID AND CustomerID = @CustomerID AND isnull(IsDeleted,0) = 0)
		 BEGIN
		
			--// Deduct from the Fleet Information Table based on the specified Customer and Fleet Id
			UPDATE [FleetInformation] SET 	
				AirframeHrs = CASE WHEN IsNull([FleetInformation].AirframeHrs,0) > 0 THEN IsNull([FleetInformation].AirframeHrs,0) - IsNull(temp.AirFrameHours,0) ELSE 0 END,
				AirframeCycle = CASE WHEN IsNull([FleetInformation].AirframeCycle,0) > 0 THEN IsNull([FleetInformation].AirframeCycle,0) - IsNull(temp.AirframeCycle,0) ELSE 0 END,
				AIrframeAsOfDT = @LastUpdTS,
				
				Engine1Hrs = CASE WHEN IsNull([FleetInformation].Engine1Hrs,0) > 0 THEN IsNull([FleetInformation].Engine1Hrs,0) - IsNull(temp.Engine1Hours,0) ELSE 0 END,
				Engine1Cycle = CASE WHEN IsNull([FleetInformation].Engine1Cycle,0) > 0 THEN IsNull([FleetInformation].Engine1Cycle,0) - IsNull(temp.Engine1Cycle,0) ELSE 0 END, 
				Engine1AsOfDT = @LastUpdTS,
				Engine2Hrs = CASE WHEN IsNull([FleetInformation].Engine2Hrs,0) > 0 THEN IsNull([FleetInformation].Engine2Hrs,0) - IsNull(temp.Engine2Hours,0) ELSE 0 END,
				Engine2Cycle = CASE WHEN IsNull([FleetInformation].Engine2Cycle,0) > 0 THEN IsNull([FleetInformation].Engine2Cycle,0) - IsNull(temp.Engine2Cycle,0) ELSE 0 END, 
				Engine2AsOfDT = @LastUpdTS,
				Engine3Hrs = CASE WHEN IsNull([FleetInformation].Engine3Hrs,0) > 0 THEN IsNull([FleetInformation].Engine3Hrs,0) - IsNull(temp.Engine3Hours,0) ELSE 0 END,
				Engine3Cycle = CASE WHEN IsNull([FleetInformation].Engine3Cycle,0) > 0 THEN IsNull([FleetInformation].Engine3Cycle,0) - IsNull(temp.Engine3Cycle,0) ELSE 0 END,
				Engine3AsOfDT = @LastUpdTS,
				Engine4Hrs = CASE WHEN IsNull([FleetInformation].Engine4Hrs,0) > 0 THEN IsNull([FleetInformation].Engine4Hrs,0) - IsNull(temp.Engine4Hours,0) ELSE 0 END,
				Engine4Cycle = CASE WHEN IsNull([FleetInformation].Engine4Cycle,0) > 0 THEN IsNull([FleetInformation].Engine4Cycle,0) - IsNull(temp.Engine4Cycle,0) ELSE 0 END,
				Engine4AsOfDT = @LastUpdTS,

				Reverse1Hrs = CASE WHEN IsNull([FleetInformation].Reverse1Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse1Hrs,0) - IsNull(temp.Reverse1Hrs,0) ELSE 0 END,
				Reverser1Cycle = CASE WHEN IsNull([FleetInformation].Reverser1Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser1Cycle,0) - IsNull(temp.Reverser1Cycle,0) ELSE 0 END, 
				Reverser1AsOfDT = @LastUpdTS,
				Reverse2Hrs = CASE WHEN IsNull([FleetInformation].Reverse2Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse2Hrs,0) - IsNull(temp.Reverse2Hrs,0) ELSE 0 END,
				Reverser2Cycle = CASE WHEN IsNull([FleetInformation].Reverser2Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser2Cycle,0) - IsNull(temp.Reverser2Cycle,0) ELSE 0 END, 
				Reverser2AsOfDT = @LastUpdTS,
				Reverse3Hrs = CASE WHEN IsNull([FleetInformation].Reverse3Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse3Hrs,0) - IsNull(temp.Reverse3Hrs,0) ELSE 0 END,
				Reverser3Cycle = CASE WHEN IsNull([FleetInformation].Reverser3Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser3Cycle,0) - IsNull(temp.Reverser3Cycle,0) ELSE 0 END,
				Reverser3AsOfDT = @LastUpdTS,
				Reverse4Hrs = CASE WHEN IsNull([FleetInformation].Reverse4Hrs,0) > 0 THEN IsNull([FleetInformation].Reverse4Hrs,0) - IsNull(temp.Reverse4Hrs,0) ELSE 0 END,
				Reverser4Cycle = CASE WHEN IsNull([FleetInformation].Reverser4Cycle,0) > 0 THEN IsNull([FleetInformation].Reverser4Cycle,0) - IsNull(temp.Reverser4Cycle,0) ELSE 0 END,
				Reverser4AsOfDT = @LastUpdTS,
				
				LastUpdUID = @LastUpdUID,
				LastUpdTS = @LastUpdTS
			FROM @OldFleetInfo temp, [FleetInformation]
			WHERE [FleetInformation].CustomerID = @CustomerID AND [FleetInformation].FleetID = @FleetID AND isnull([FleetInformation].IsDeleted,0) = 0					
		END
	END	
	ELSE IF IsNull(@OldStatus,0) = 0 AND ISNULL(@IsCompleted,0) = 1
	BEGIN

	 --// Check If Fleet Information Exists for the specified Fleet ID, Append the Hours with existing Values
		 IF EXISTS (SELECT NULL FROM [FleetInformation] WHERE FleetID = @FleetID AND CustomerID = @CustomerID AND isnull(IsDeleted,0) = 0)
		 BEGIN
			UPDATE [FleetInformation] SET 	
				AirframeHrs = Isnull(AirframeHrs,0) + Isnull(@AirFrameHours,0),
				AirframeCycle = Isnull(AirframeCycle,0) + Isnull(@AirframeCycle,0),
				AIrframeAsOfDT = @LastUpdTS,
				Engine1Hrs = Isnull(Engine1Hrs,0) + Isnull(@Engine1Hours,0),
				Engine1Cycle = Isnull(Engine1Cycle,0) + Isnull(@Engine1Cycle,0),
				Engine1AsOfDT = @LastUpdTS,
				Engine2Hrs = Isnull(Engine2Hrs,0) + Isnull(@Engine2Hours,0),
				Engine2Cycle = Isnull(Engine2Cycle,0) + Isnull(@Engine2Cycle,0),
				Engine2AsOfDT = @LastUpdTS,
				Engine3Hrs = Isnull(Engine3Hrs,0) + Isnull(@Engine3Hours,0),
				Engine3Cycle = Isnull(Engine3Cycle,0) + Isnull(@Engine3Cycle,0),
				Engine3AsOfDT = @LastUpdTS,
				Engine4Hrs = Isnull(Engine4Hrs,0) + Isnull(@Engine4Hours,0),
				Engine4Cycle = Isnull(Engine4Cycle,0) + Isnull(@Engine4Cycle,0),
				Engine4AsOfDT = @LastUpdTS,
				
				Reverse1Hrs = Isnull(Reverse1Hrs,0) + Isnull(@Re1Hours,0),
				Reverser1Cycle = Isnull(Reverser1Cycle,0) + Isnull(@Re1Cycle,0),
				Reverser1AsOfDT = @LastUpdTS,
				Reverse2Hrs = Isnull(Reverse2Hrs,0) + Isnull(@Re2Hours,0),
				Reverser2Cycle = Isnull(Reverser2Cycle,0) + Isnull(@Re2Cycle,0),
				Reverser2AsOfDT = @LastUpdTS,
				Reverse3Hrs = Isnull(Reverse3Hrs,0) + Isnull(@Re3Hours,0),
				Reverser3Cycle = Isnull(Reverser3Cycle,0) + Isnull(@Re3Cycle,0),
				Reverser3AsOfDT = @LastUpdTS,
				Reverse4Hrs = Isnull(Reverse4Hrs,0) + Isnull(@Re4Hours,0),
				Reverser4Cycle = Isnull(Reverser4Cycle,0) + Isnull(@Re4Cycle,0),
				Reverser4AsOfDT = @LastUpdTS,
				
				LastUpdUID = @LastUpdUID,
				LastUpdTS = @LastUpdTS
			WHERE CustomerID = @CustomerID AND FleetID = @FleetID AND isnull(IsDeleted,0) = 0
		 END
		 ELSE
		 BEGIN
			--// Check If there is No Fleet Information in the Table, then Insert the New Airframe Values 
			 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetInformationID OUTPUT
 			
			INSERT INTO [FleetInformation] (FleetInformationID, CustomerID, FleetID, AircraftCD, AirframeHrs,AirframeCycle,AIrframeAsOfDT,Engine1Hrs,Engine1Cycle,Engine1AsOfDT,Engine2Hrs,Engine2Cycle,Engine2AsOfDT
				, Engine3Hrs,Engine3Cycle,Engine3AsOfDT,Engine4Hrs,Engine4Cycle,Engine4AsOfDT
				, Reverse1Hrs, Reverser1Cycle, Reverser1AsOfDT, Reverse2Hrs, Reverser2Cycle, Reverser2AsOfDT, Reverse3Hrs, Reverser3Cycle
				, Reverser3AsOfDT, Reverse4Hrs, Reverser4Cycle, Reverser4AsOfDT					
				,LastUpdUID,LastUpdTS)
			VALUES (@FleetInformationID, @CustomerID, @FleetID, @AircraftCD, Isnull(@AirFrameHours,0),Isnull(@AirframeCycle,0)
				 , @LastUpdTS,Isnull(@Engine1Hours,0),Isnull(@Engine1Cycle,0),@LastUpdTS,Isnull(@Engine2Hours,0),Isnull(@Engine2Cycle,0)
				 , @LastUpdTS,Isnull(@Engine3Hours,0),Isnull(@Engine3Cycle,0),@LastUpdTS,Isnull(@Engine4Hours,0),Isnull(@Engine4Cycle,0)
				 , @LastUpdTS,Isnull(@Re1Hours,0), Isnull(@Re1Cycle,0), @LastUpdTS, Isnull(@Re2Hours,0), Isnull(@Re2Cycle,0), @LastUpdTS
				 , Isnull(@Re3Hours,0), Isnull(@Re3Cycle,0), @LastUpdTS, Isnull(@Re4Hours,0), Isnull(@Re4Cycle,0), @LastUpdTS
				 , @LastUpdUID,@LastUpdTS)
		 END
	END
	
		
	UPDATE [PostflightLeg] SET 
		[POLegID] = @POLegID, 
		[POLogID] = @POLogID, 
		[LegNUM] = @LegNUM, 
		[TripLegID] = @TripLegID, 
		[DepartICAOID] = @DepartICAOID, 
		[ArriveICAOID] = @ArriveICAOID, 
		[Distance] = @Distance, 
		[ScheduledTM] = @ScheduledTM, 
		[BlockOut] = @BlockOut, 
		[BlockIN] = @BlockIN, 
		[TimeOff] = @TimeOff, 
		[TimeOn] = @TimeOn, 
		[BlockHours]  = CASE WHEN @BlockHours <0 THEN 0 ELSE @BlockHours END,
		[FlightHours] = CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END,
		[FedAviationRegNum] = @FedAviationRegNum, 
		[ClientID] = @ClientID, 
		[FlightPurpose] = @FlightPurpose, 
		[DutyTYPE] = @DutyTYPE, 
		[CrewCurrency] = @CrewCurrency, 
		[IsDutyEnd] = @IsDutyEnd, 
		[FuelOut] = @FuelOut, 
		[FuelIn] = @FuelIn, 
		[FuelUsed] = @FuelUsed, 
		[FuelBurn] = @FuelBurn, 
		[DelayTM] = @DelayTM, 
		[DutyHrs] = CASE WHEN @DutyHrs <0 THEN 0 ELSE @DutyHrs END,
	    RestHrs = CASE WHEN @RestHrs <0 THEN 0 ELSE @RestHrs  END,
		[NextDTTM] = @NextDTTM, 
		[DepatureFBO] = @DepatureFBO, 
		[ArrivalFBO] = @ArrivalFBO, 
		[CrewID] = @CrewID, 
		[IsException] = @IsException, 
		[IsAugmentCrew] = @IsAugmentCrew, 
		[BeginningDutyHours] = @BeginningDutyHours, 
		[EndDutyHours] = @EndDutyHours, 
		[LastUpdUID] = @LastUpdUID, 
		[LastUpdTS] = @LastUpdTS, 
		[PassengerTotal] = @PassengerTotal, 
		[ScheduleDTTMLocal] = @ScheduleDTTMLocal, 
		[BlockOutLocal] = @BlockOutLocal, 
		[BlockInLocal] = @BlockInLocal, 
		[TimeOffLocal] = @TimeOffLocal, 
		[TimeOnLocal] = @TimeOnLocal, 
		[OutboundDTTM] = @OutboundDTTM, 
		[InboundDTTM] = @InboundDTTM, 
		[AdditionalDist] = @AdditionalDist, 
		[AVTrak] = @AVTrak, 
		[FlightNum] = @FlightNum, 
		[CargoOut] = @CargoOut, 
		[CargoIn] = @CargoIn, 
		[CargoThru] = @CargoThru, 
		[StatuteMiles] = @StatuteMiles, 
		[FlightCost] = @FlightCost, 
		[TechLog] = @TechLog, 
		[AirFrameHours] = @AirFrameHours, 
		[AirframeCycle] = @AirframeCycle, 
		[Engine1Hours] = @Engine1Hours, 
		[Engine2Hours] = @Engine2Hours, 
		[Engine3Hours] = @Engine3Hours, 
		[Engine4Hours] = @Engine4Hours, 
		[Engine1Cycle] = @Engine1Cycle, 
		[Engine2Cycle] = @Engine2Cycle, 
		[Engine3Cycle] = @Engine3Cycle, 
		[Engine4Cycle] = @Engine4Cycle, 
		[Re1Hours] = @Re1Hours, 
		[Re2Hours] = @Re2Hours, 
		[Re3Hours] = @Re3Hours, 
		[Re4Hours] = @Re4Hours, 
		[Re1Cycle] = @Re1Cycle, 
		[Re2Cycle] = @Re2Cycle, 
		[Re3Cycle] = @Re3Cycle, 
		[Re4Cycle] = @Re4Cycle, 
		[FuelOn] = @FuelOn, 
		[IsDeleted] = @IsDeleted, 
		[AccountID] = @AccountID, 
		[DelayTypeID] = @DelayTypeID, 
		[PassengerRequestorID] = @PassengerRequestorID, 
		[DepartmentID] = @DepartmentID, 
		[AuthorizationID] = @AuthorizationID, 
		[FlightCategoryID] = @FlightCategoryID 
		WHERE (
		([POLegID] = @POLegID) AND [CustomerID] = @CustomerID
		)
END		


GO


