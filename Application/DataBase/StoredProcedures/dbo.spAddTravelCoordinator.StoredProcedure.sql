
GO
/****** Object:  StoredProcedure [dbo].[spAddTravelCoordinator]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddTravelCoordinator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddTravelCoordinator]
go
CREATE Procedure [dbo].[spAddTravelCoordinator]      
 (--@TravelCoordinatorID bigint,      
 @CustomerID bigint      
 ,@TravelCoordCD char(5)      
 ,@FirstName varchar(30)      
 ,@MiddleName varchar(30)      
 ,@LastName varchar(30)      
 ,@PhoneNum varchar(25)      
 ,@FaxNum varchar(25)      
 ,@PagerNum varchar(25)      
 ,@CellPhoneNum varchar(25)      
 ,@EmailID varchar(100)      
 ,@Notes varchar(max)      
 ,@HomebaseID bigint      
 ,@LastUpdUID varchar(30)      
 ,@LastUpdTS datetime      
 ,@IsDeleted bit   
 ,@BusinessPhone Varchar(25)  
 ,@HomeFax Varchar(25)  
 ,@CellPhoneNum2 Varchar (25)  
 ,@OtherPhone Varchar(25)  
 ,@BusinessEmail Varchar(250)  
 ,@PersonalEmail Varchar(250)  
 ,@OtherEmail Varchar(250)  
 ,@Addr3 Varchar(40)
 ,@IsInActive bit)      
-- =============================================        
-- Author:RUKMINI M        
-- Create date: 26/04/2012        
-- Modified by Mathes 03-07-12      
-- Description: Insert the TravelCoordinator information        
-- =============================================        
AS      
BEGIN       
 SET NoCOUNT ON      
 IF @CustomerID is null      
 BEGIN     
  SET @CustomerID=0      
 END      
 DECLARE @TravelCoordinatorID BIGINT      
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @TravelCoordinatorID OUTPUT           
 SET @LastUpdTS = GETUTCDATE()    
 INSERT INTO [TravelCoordinator]      
  ([TravelCoordinatorID]      
  ,[CustomerID]      
  ,[TravelCoordCD]      
  ,[FirstName]      
  ,[MiddleName]      
  ,[LastName]      
  ,[PhoneNum]      
  ,[FaxNum]      
  ,[PagerNum]      
  ,[CellPhoneNum]      
  ,[EmailID]      
  ,[Notes]      
  ,[HomebaseID]      
  ,[LastUpdUID]      
  ,[LastUpdTS]      
  ,[IsDeleted]  
  ,[BusinessPhone]  
  ,[HomeFax]  
  ,[CellPhoneNum2]  
  ,[OtherPhone]  
  ,[BusinessEmail]  
  ,[PersonalEmail]  
  ,[OtherEmail]  
  ,[Addr3]
  ,[IsInActive])  
 VALUES      
  (@TravelCoordinatorID      
  ,@CustomerID      
  ,@TravelCoordCD      
  ,@FirstName      
  ,@MiddleName      
  ,@LastName      
  ,@PhoneNum      
  ,@FaxNum      
  ,@PagerNum      
  ,@CellPhoneNum      
  ,@EmailID      
  ,@Notes      
  ,@HomebaseID      
  ,@LastUpdUID      
  ,@LastUpdTS      
  ,@IsDeleted  
  ,@BusinessPhone  
  ,@HomeFax  
  ,@CellPhoneNum2  
  ,@OtherPhone  
  ,@BusinessEmail  
  ,@PersonalEmail  
  ,@OtherEmail  
  ,@Addr3
  ,@IsInActive)       
 SELECT @TravelCoordinatorID as TravelCoordinatorID      
END
GO
