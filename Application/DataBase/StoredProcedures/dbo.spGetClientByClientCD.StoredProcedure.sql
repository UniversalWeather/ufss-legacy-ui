
/****** Object:  StoredProcedure [dbo].[spGetClientByClientCD]    Script Date: 09/14/2012 18:34:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetClientByClientCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetClientByClientCD]
GO

/****** Object:  StoredProcedure [dbo].[spGetClientByClientCD]    Script Date: 09/14/2012 18:34:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetClientByClientCD]   
(  
 @CustomerID  BIGINT,  
 @ClientCD varchar(5)
 )  
AS      
BEGIN      
-- =======================================================      
-- Author: Hamsha
-- Create date: 09/13/2012     
-- Description: To get the Client record based on ClientCD
-- =======================================================    

   
SELECT ClientID  
           ,ClientCD      
           ,CustomerID      
           ,IsDeleted        
              
  FROM [Client]
  WHERE CustomerID = @CustomerID 
   and IsDeleted != 1 
  and ClientCD = @ClientCD
   
END






GO


