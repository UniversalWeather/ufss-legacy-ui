
GO
/****** Object:  StoredProcedure [dbo].[spAddAirportPair]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddAirportPair]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddAirportPair]
GO

CREATE PROCEDURE spAddAirportPair
(@CustomerID	BIGINT,
@AirportPairNumber	INT,
@AircraftID	BIGINT,
@WindQTR NUMERIC(1),
@AirportPairDescription VARCHAR(40),
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	DECLARE @AirportPairID	BIGINT
	SET @LastUpdTS = GETUTCDATE()
	
	SELECT @AirportPairNumber = (ISNULL(MAX(AirportPairNumber),0) + 1) FROM AirportPair WHERE CustomerID=@CustomerID
	
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'UtilityCurrentNo',  @AirportPairID OUTPUT
		
	INSERT INTO AirportPair
		(AirportPairID,
		CustomerID,
		AirportPairNumber,
		AircraftID,
		WindQTR,
		AirportPairDescription,
		LastUpdUID,
		LastUpdTS,
		IsDeleted)
	VALUES
		(@AirportPairID,
		@CustomerID,
		@AirportPairNumber,
		@AircraftID,
		@WindQTR,
		@AirportPairDescription,
		@LastUpdUID,
		@LastUpdTS,
		@IsDeleted)
		
	SELECT @AirportPairID AS AirportPairID
	
END