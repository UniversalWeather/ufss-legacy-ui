
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightCatering]    Script Date: 09/24/2013 00:00:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightCatering]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightCatering]    Script Date: 09/24/2013 00:00:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightCatering]    
 (       
     @LegID bigint,          
           @CustomerID bigint,    
           @AirportID bigint,    
           @ArriveDepart char,    
           @CateringID bigint,    
           @IsCompleted bit,    
           @Cost numeric(17,2),    
           @ContactName varchar(50),    
           @ContactPhone varchar(20),    
           @ContactFax varchar(20),    
           @CateringConfirmation varchar(max),    
           @CateringComments varchar(max),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @IsUWAArranger bit,    
           @CateringContactName varchar(40),    
           @Status varchar(20)     
    )    
AS    
BEGIN     
SET NOCOUNT ON    
    
    
DECLARE @PreflightCateringID BIGINT       
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @PreflightCateringID OUTPUT    
    
       
INSERT INTO [dbo].[PreflightCateringDetail]    
           ([PreflightCateringID]    
           ,[LegID]    
           ,[CustomerID]    
           ,[AirportID]    
           ,[ArriveDepart]    
           ,[CateringID]    
           ,[IsCompleted]    
           ,[Cost]    
           ,[ContactName]    
           ,[ContactPhone]    
           ,[ContactFax]    
           ,[CateringConfirmation]    
           ,[CateringComments]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[IsUWAArranger]    
           ,[CateringContactName]    
           ,[Status]    
           )    
     VALUES    
           (    
           @PreflightCateringID,    
           @LegID,    
           @CustomerID,    
           @AirportID,    
           @ArriveDepart,    
           @CateringID,    
           @IsCompleted,    
           @Cost,    
           @ContactName,    
           @ContactPhone,    
           @ContactFax,    
           @CateringConfirmation,    
           @CateringComments,    
           @LastUpdUID,    
           @LastUpdTS,    
           @IsDeleted,    
           @IsUWAArranger,    
           @CateringContactName,    
           @Status    
           )    
               
               
           select @PreflightCateringID as 'PreflightCateringID'    
               
END    
    
    


GO


