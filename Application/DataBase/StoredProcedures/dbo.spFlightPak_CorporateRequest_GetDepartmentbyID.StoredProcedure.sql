

/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetDepartmentbyID]    Script Date: 05/07/2013 12:08:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetDepartmentbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetDepartmentbyID]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetDepartmentbyID]    Script Date: 05/07/2013 12:08:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spFlightPak_CorporateRequest_GetDepartmentbyID]  
(@CustomerID BIGINT,@DepartmentID BIGINT)    
AS      
-- =============================================      
-- Author:Leela   
-- Create date: 18/4/2013      
-- Description: Get the Department information      
-- =============================================      
SET NOCOUNT ON   
SELECT [DepartmentID]  
      ,[DepartmentCD]  
      ,[DepartmentName]  
      ,[CustomerID]  
      ,[ClientID]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsInActive]  
      ,[IsDeleted]
      ,DepartPercentage  
  FROM Department  
where IsDeleted = 0  AND DepartmentID=@DepartmentID
GO


