
/****** Object:  StoredProcedure [dbo].[spAddPaxInformationforYES]    Script Date: 07/02/2013 14:56:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPaxInformationforYES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPaxInformationforYES]
GO



/****** Object:  StoredProcedure [dbo].[spAddPaxInformationforYES]    Script Date: 07/02/2013 14:56:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
    
    
CREATE PROCEDURE [dbo].[spAddPaxInformationforYES]        
  (       
   @CustomerID BIGINT      
 ,@PassengerInfoCD CHAR(3)      
 ,@PassengerDescription VARCHAR(25)      
 ,@ClientID BIGINT      
 ,@LastUpdUID VARCHAR(30)      
 ,@LastUpdTS DATETIME      
 ,@IsShowOnTrip BIT      
 ,@IsDeleted BIT      
 ,@IsCheckList BIT    
  )        
-- =============================================        
-- Author:A.Akhila        
-- Create date: 7/5/2012        
-- Description: Insert the Crew Roster Additional Information        
-- =============================================        
        
AS        
BEGIN         
SET NOCOUNT ON       
DECLARE  @PassengerInformationID BIGINT    
DECLARE  @PassengerID BIGINT      
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @PassengerInformationID OUTPUT      
set @LastUpdTS = GETUTCDATE()      
INSERT INTO [PassengerInformation] (      
  PassengerInformationID      
  ,CustomerID      
  ,ClientID      
  ,PassengerInfoCD      
  ,PassengerDescription      
  ,LastUpdUID      
  ,LastUpdTS      
  ,IsShowOnTrip      
  ,IsDeleted      
  ,IsCheckList    
  )      
 VALUES (      
  @PassengerInformationID      
  ,@CustomerID      
  ,@ClientID      
  ,@PassengerInfoCD      
  ,@PassengerDescription      
  ,@LastUpdUID      
  ,@LastUpdTS      
  ,@IsShowOnTrip      
  ,@IsDeleted      
  ,@IsCheckList    
  )        
    
    
DECLARE curTailInfo CURSOR FOR SELECT DISTINCT PassengerRequestorID FROM Passenger WHERE CUSTOMERID=@CUSTOMERID            
      
            
 OPEN curTailInfo;                
 -- PRINT @@CURSOR_ROWS                
  FETCH NEXT FROM curTailInfo INTO @PassengerID;       
  WHILE @@FETCH_STATUS = 0                
  BEGIN      
  BEGIN              
  --INSERT INTO()    
  if(@IsCheckList = 0)    
  begin 
  	EXEC spAddPassengerAdditionalInfo NULL,@CustomerID,@PassengerID,@PassengerInfoCD,@PassengerDescription,NULL,@ClientID,@LastUpdUID,@LastUpdTS,0,@PassengerInformationID,0
  end    
 else    
 begin    
 EXECUTE [spFlightPak_AddPaxCheckListDate]     
   NULL    
  ,@PassengerInformationID    
  ,NULL    
  ,@CustomerID    
  ,@PassengerID    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,@LastUpdUID    
  ,@LastUpdTS    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,0    
 end    
     
  END      
   FETCH NEXT FROM curTailInfo INTO  @PassengerID    
                  
 END                 
 CLOSE curTailInfo;                
 DEALLOCATE curTailInfo;     
 END    
    
    
GO


