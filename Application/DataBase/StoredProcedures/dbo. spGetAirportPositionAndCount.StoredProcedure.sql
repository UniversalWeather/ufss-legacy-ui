
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportPositionAndCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportPositionAndCount]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:Parth Shah
-- Create date: 06/01/2015
-- Description: Get Airport total record count and selected record position
-- exec spGetAirportPositionAndCount 10002,0,10002208580,0
-- =============================================            

CREATE PROCEDURE [dbo].[spGetAirportPositionAndCount]  
(
	 @CustomerID BIGINT,
	 @IsInActive bit,
	 @AirportID BIGINT,
	 @selectLastModified bit
)
AS
SET NOCOUNT ON;
BEGIN		
	IF LEN(@CustomerID) >0            
	BEGIN         
	DECLARE @UWACustomerID BIGINT        
	SELECT @UWACustomerID  = dbo.GetUWACustomerID()         
	        
	SELECT @UWACustomerID = dbo.GetUWACustomerID()

	SELECT t1.ROWNUMBER As RecordNumber,
		   t1.TotalCount,
		   t1.AirportId
	FROM
	  (SELECT t.*,
			  Count(*) over () AS TotalCount,
					   ROW_NUMBER() OVER(
										 ORDER BY t.CityName,t.ICaoID) AS ROWNUMBER
	   FROM
		 (SELECT A.AirportID,
				 CityName,
				 IcaoID
		  FROM Airport A
		  LEFT JOIN Country C ON C.CountryID = A.CountryID
		  WHERE A.CustomerID =@CustomerID
			AND A.IsDeleted =0
			AND A.IsInActive <= @IsInActive
		  UNION ALL SELECT A.AirportID,
						   CityName,
						   IcaoID
		  FROM Airport A
		  LEFT JOIN Country C ON C.CountryID = A.CountryID
		  WHERE A.CustomerID <> @CustomerID
			AND A.CustomerID = @UWACustomerID
			AND A.IsDeleted =0
			AND A.IsInActive <= @IsInActive
			AND IcaoID NOT IN
			  (SELECT DISTINCT IcaoID
			   FROM Airport
			   WHERE CustomerID = @CustomerID
				 AND IsDeleted = 0)) AS t) AS t1
	WHERE t1.AirportId = 
		(CASE @selectLastModified
		   WHEN 0 
		   THEN @AirportID
		   ELSE    
			   (Select 
					top 1 t.AirportID
				From 
				(
				SELECT A.AirportID,
					   A.LastUpdTS       
				FROM Airport A
					  LEFT JOIN Country C ON C.CountryID = A.CountryID
				WHERE A.CustomerID =@CustomerID
					  AND A.IsDeleted =0
					  AND A.IsInActive <= @IsInActive

				UNION ALL 

				SELECT A.AirportID,
					   LastUpdTS
				FROM  Airport A
					  LEFT JOIN Country C ON C.CountryID = A.CountryID
				WHERE A.CustomerID <> @CustomerID
					  AND A.CustomerID = @UWACustomerID
					  AND A.IsDeleted =0
					  AND A.IsInActive <= @IsInActive
					  AND IcaoID NOT IN
					  (SELECT DISTINCT IcaoID
					   FROM Airport
					   WHERE CustomerID = @CustomerID
							 AND IsDeleted = 0
					  )
				) As t
				Order By LastUpdTS desc
			   )
		   End )		
	END
End
GO


