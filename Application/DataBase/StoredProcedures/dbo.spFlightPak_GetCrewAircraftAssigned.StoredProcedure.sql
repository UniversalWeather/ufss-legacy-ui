/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewAircraftAssigned]    Script Date: 01/08/2013 14:26:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetCrewAircraftAssigned]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetCrewAircraftAssigned]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewAircraftAssigned]    Script Date: 01/08/2013 14:26:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetCrewAircraftAssigned]    
(    
@CustomerID BIGINT,  
@CrewID BIGINT  
)    
AS    
 SET NOCOUNT ON;    
SELECT [CrewAircraftAssignedID]  
      ,[CrewID]  
      ,[CustomerID]  
      ,[FleetID]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted] 
      ,[IsInActive]
FROM  CrewAircraftAssigned    
WHERE CustomerID = @CustomerID AND CrewID = @CrewID   
AND ISNULL(IsDeleted,0)=0  
GO


