
GO
/****** Object:  StoredProcedure [dbo].[spUpdateItineraryPlanDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateItineraryPlanDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateItineraryPlanDetail]
GO

CREATE PROCEDURE spUpdateItineraryPlanDetail
(@ItineraryPlanDetailID	BIGINT
,@CustomerID	BIGINT
,@ItineraryPlanID	BIGINT
,@IntinearyNUM	INT
,@LegNUM	INT
,@DAirportID	BIGINT
,@AAirportID	BIGINT
,@Distance	NUMERIC(5,0)
,@PowerSetting	VARCHAR(1)
,@TakeoffBIAS	NUMERIC(6,3)
,@LandingBias	NUMERIC(6,3)
,@TrueAirSpeed	NUMERIC(3,0)
,@WindsBoeingTable	NUMERIC(5,0)
,@ElapseTM	NUMERIC(7,3)
,@DepartureLocal	DATETIME
,@DepartureGMT	DATETIME
,@DepartureHome	DATETIME
,@ArrivalLocal	DATETIME
,@ArrivalGMT	DATETIME
,@ArrivalHome	DATETIME
,@IsDepartureConfirmed	BIT
,@IsArrivalConfirmation	BIT
,@PassengerNUM	NUMERIC(3,0)
,@Cost	NUMERIC(12,2)
,@WindReliability	INT
,@LegID	INT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE ItineraryPlanDetail
    SET		CustomerID = @CustomerID
           ,ItineraryPlanID = @ItineraryPlanID
           ,IntinearyNUM = @IntinearyNUM
           ,LegNUM = @LegNUM
           ,DAirportID = @DAirportID
           ,AAirportID = @AAirportID
           ,Distance = @Distance
           ,PowerSetting = @PowerSetting
           ,TakeoffBIAS = @TakeoffBIAS
           ,LandingBias = @LandingBias
           ,TrueAirSpeed = @TrueAirSpeed
           ,WindsBoeingTable = @WindsBoeingTable
           ,ElapseTM = @ElapseTM
           ,DepartureLocal = @DepartureLocal
           ,DepartureGMT = @DepartureGMT
           ,DepartureHome = @DepartureHome
           ,ArrivalLocal = @ArrivalLocal
           ,ArrivalGMT = @ArrivalGMT
           ,ArrivalHome = @ArrivalHome
           ,IsDepartureConfirmed = @IsDepartureConfirmed
           ,IsArrivalConfirmation = @IsArrivalConfirmation
           ,PassengerNUM = @PassengerNUM
           ,Cost = @Cost
           ,WindReliability = @WindReliability
           ,LegID = @LegID
           ,LastUpdUID = @LastUpdUID
           ,LastUpdTS = @LastUpdTS
           ,IsDeleted = @IsDeleted
    WHERE ItineraryPlanDetailID = @ItineraryPlanDetailID

END