

/****** Object:  StoredProcedure [dbo].[spAddPassengerAdditionalInfo]    Script Date: 05/24/2013 14:48:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPassengerAdditionalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPassengerAdditionalInfo]
GO



/****** Object:  StoredProcedure [dbo].[spAddPassengerAdditionalInfo]    Script Date: 05/24/2013 14:48:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddPassengerAdditionalInfo]  
(@PassengerAdditionalInfoID bigint  
,@CustomerID bigint  
,@PassengerRequestorID bigint  
,@AdditionalINFOCD char(3)  
,@AdditionalINFODescription varchar(25)  
,@AdditionalINFOValue nvarchar(120)  
,@ClientID bigint  
,@LastUpdUID varchar(30)  
,@LastUptTS datetime  
,@IsDeleted bit
,@PassengerInformationID bigint
,@IsPrint bit)        
AS      
BEGIN       
SET NOCOUNT ON   
  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @PassengerAdditionalInfoID OUTPUT  
set @LastUptTS=  GETUTCDATE()  
INSERT INTO [PassengerAdditionalInfo]  
           ([PassengerAdditionalInfoID]  
           ,[CustomerID]  
           ,[PassengerRequestorID]  
           ,[AdditionalINFOCD]  
           ,[AdditionalINFODescription]  
           ,[AdditionalINFOValue]  
           ,[ClientID]  
           ,[LastUpdUID]  
           ,[LastUptTS]  
           ,[IsDeleted]
           ,[PassengerInformationID]
           ,[IsPrint])  
     VALUES  
           (@PassengerAdditionalInfoID  
   ,@CustomerID  
   ,@PassengerRequestorID  
   ,@AdditionalINFOCD  
   ,@AdditionalINFODescription  
   ,@AdditionalINFOValue  
   ,@ClientID  
   ,@LastUpdUID  
   ,@LastUptTS  
   ,@IsDeleted
   ,@PassengerInformationID
   ,@IsPrint)    
END  
GO


