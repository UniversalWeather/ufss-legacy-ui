
GO
/****** Object:  StoredProcedure [dbo].[spAddItineraryPlanDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddItineraryPlanDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddItineraryPlanDetail]
GO

CREATE PROCEDURE spAddItineraryPlanDetail
(@CustomerID	BIGINT
,@ItineraryPlanID	BIGINT
,@IntinearyNUM	INT
,@LegNUM	INT
,@DAirportID	BIGINT
,@AAirportID	BIGINT
,@Distance	NUMERIC(5,0)
,@PowerSetting	VARCHAR(1)
,@TakeoffBIAS	NUMERIC(6,3)
,@LandingBias	NUMERIC(6,3)
,@TrueAirSpeed	NUMERIC(3,0)
,@WindsBoeingTable	NUMERIC(5,0)
,@ElapseTM	NUMERIC(7,3)
,@DepartureLocal	DATETIME
,@DepartureGMT	DATETIME
,@DepartureHome	DATETIME
,@ArrivalLocal	DATETIME
,@ArrivalGMT	DATETIME
,@ArrivalHome	DATETIME
,@IsDepartureConfirmed	BIT
,@IsArrivalConfirmation	BIT
,@PassengerNUM	NUMERIC(3,0)
,@Cost	NUMERIC(12,2)
,@WindReliability	INT
,@LegID	INT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	DECLARE @ItineraryPlanDetailID	BIGINT
	SET @LastUpdTS = GETUTCDATE()
	
	IF (@LegNUM <> 0)
		BEGIN
			DECLARE @tmpItineraryPlanDetailID BIGINT
			DECLARE @tmpLegNUM INT
			SET @tmpLegNUM = @LegNUM
			SELECT ItineraryPlanDetailID,CustomerID,ItineraryPlanID,IntinearyNUM,LegNUM,DAirportID,AAirportID,
					Distance,PowerSetting,TakeoffBIAS,LandingBias,TrueAirSpeed,WindsBoeingTable,ElapseTM,DepartureLocal,
					DepartureGMT,DepartureHome,ArrivalLocal,ArrivalGMT,ArrivalHome,IsDepartureConfirmed,IsArrivalConfirmation,
					PassengerNUM,Cost,WindReliability,LegID,LastUpdUID,LastUpdTS,IsDeleted
				INTO #TmpItineraryPlanDetail
			FROM ItineraryPlanDetail
			WHERE CustomerID=@CustomerID
				AND ItineraryPlanID=@ItineraryPlanID
				AND LegNUM >= @tmpLegNUM
				AND IsDeleted = 0
			ORDER BY LegNUM ASC
				
			WHILE (select COUNT(*) from #TmpItineraryPlanDetail) >0
				BEGIN
					SELECT TOP 1 @tmpItineraryPlanDetailID = ItineraryPlanDetailID FROM #TmpItineraryPlanDetail
					SET @tmpLegNUM = @tmpLegNUM + 1
					UPDATE ItineraryPlanDetail SET LegNUM = @tmpLegNUM WHERE ItineraryPlanDetailID = @tmpItineraryPlanDetailID					
					DELETE FROM #TmpItineraryPlanDetail WHERE ItineraryPlanDetailID = @tmpItineraryPlanDetailID
				END
			DROP TABLE #TmpItineraryPlanDetail
		END
	ELSE
		BEGIN
			SELECT @LegNUM = (ISNULL(MAX(LegNUM),0) + 1) FROM ItineraryPlanDetail WHERE CustomerID=@CustomerID
					AND ItineraryPlanID=@ItineraryPlanID
		END

	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'UtilityCurrentNo',  @ItineraryPlanDetailID OUTPUT
	
	INSERT INTO ItineraryPlanDetail
           (ItineraryPlanDetailID
           ,CustomerID
           ,ItineraryPlanID
           ,IntinearyNUM
           ,LegNUM
           ,DAirportID
           ,AAirportID
           ,Distance
           ,PowerSetting
           ,TakeoffBIAS
           ,LandingBias
           ,TrueAirSpeed
           ,WindsBoeingTable
           ,ElapseTM
           ,DepartureLocal
           ,DepartureGMT
           ,DepartureHome
           ,ArrivalLocal
           ,ArrivalGMT
           ,ArrivalHome
           ,IsDepartureConfirmed
           ,IsArrivalConfirmation
           ,PassengerNUM
           ,Cost
           ,WindReliability
           ,LegID
           ,LastUpdUID
           ,LastUpdTS
           ,IsDeleted)
     VALUES
           (@ItineraryPlanDetailID
           ,@CustomerID
           ,@ItineraryPlanID
           ,@IntinearyNUM
           ,@LegNUM
           ,@DAirportID
           ,@AAirportID
           ,@Distance
           ,@PowerSetting
           ,@TakeoffBIAS
           ,@LandingBias
           ,@TrueAirSpeed
           ,@WindsBoeingTable
           ,@ElapseTM
           ,@DepartureLocal
           ,@DepartureGMT
           ,@DepartureHome
           ,@ArrivalLocal
           ,@ArrivalGMT
           ,@ArrivalHome
           ,@IsDepartureConfirmed
           ,@IsArrivalConfirmation
           ,@PassengerNUM
           ,@Cost
           ,@WindReliability
           ,@LegID
           ,@LastUpdUID
           ,@LastUpdTS
           ,@IsDeleted)
	
	SELECT @ItineraryPlanDetailID AS ItineraryPlanDetailID

END