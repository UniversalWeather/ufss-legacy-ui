
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateHelpItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateHelpItems]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateHelpItems]
(@Column VARCHAR(100)
,@Value VARCHAR(100)
,@ItemID VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @query NVARCHAR(MAX)
	DECLARE @ParameterDefinition AS NVARCHAR(1000)
	
	SET @query = 'UPDATE [HelpItems] SET [' + @Column + '] = ''' + @Value + ''' WHERE [ItemID] = ' + @ItemID
	
	SET @ParameterDefinition =  '@Column AS VARCHAR(100), @Value AS VARCHAR(100), @ItemID AS VARCHAR(100)'
	EXECUTE sp_executesql @query, @ParameterDefinition, @Column, @Value, @ItemID

END

