

/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]    Script Date: 10/16/2013 15:28:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]    Script Date: 10/16/2013 15:28:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]    Script Date: 04/04/2013 19:14:45 ******/      
CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_RequestAcknowledge]          
(          
@CRMainID BIGINT,          
@TripID BIGINT,          
@CustomerID BIGINT,          
@LastUpdUID VARCHAR(30),          
@LastUpdTS datetime          
)          
AS BEGIN          
-- ===============================================================================    
-- Revision History    
-- Date                 Name               Change    
-- 16-10-2013   Karthi    Updated PassengerTotal Column in CRLEG -- ================================================================================    
    
          
          
DECLARE @LastPrefLegNum int          
SET @LastPrefLegNum = 1           
DECLARE  @NewLegID BIGINT            
DECLARE @CRLegnum int           
DECLARE @MaxPrefLegnum int          
DECLARE @MaxCRLegnum int                
DECLARE @Totlegs int          
          
  -- check Trip exists          
  IF EXISTS (SELECT TripID FROM PreflightMain where TripID=@TripID)          
  BEGIN          
            
   --- Update main          
   UPDATE main          
   SET              
       [TripStatus] = PM.TripStatus          
      --,[TripSheetStatus] = @TripSheetStatus          
      ,[AcknowledgementStatus] = ''              
      ,[CorporateRequestStatus] = 'A'          
      ,[DispatchNUM] = PM.DispatchNUM         
      ,CRMainDescription = PM.TripDescription         
      ,[EstDepartureDT] = PM.EstDepartureDT          
      ,[EstArrivalDT] = PM.EstArrivalDT          
      ,[RecordType] = PM.RecordType          
      ,[FleetID] = PM.FleetID          
      ,[AircraftID] = PM.AircraftID          
      ,[PassengerRequestorID] = PM.PassengerRequestorID           
      ,[DepartmentID] = PM.DepartmentID            
      ,[AuthorizationID] = PM.AuthorizationID             
      ,[HomebaseID] = PM.HomebaseID          
      ,[ClientID] = PM.ClientID          
      ,[IsPrivate] = PM.IsPrivate          
      ,[RequestDT] = PM.RequestDT          
      ,[BeginningGMTDT] = PM.BeginningGMTDT          
      ,[EndingGMTDT] = PM.EndingGMTDT          
      ,[IsCrew] = PM.IsCrew          
      ,[IsPassenger] = PM.IsPassenger          
      ,[IsAlert] = PM.IsAlert          
      ,[IsAirportAlert] = PM.IsAirportAlert          
      ,[IsLog] = PM.IsLog          
      ,[LastUpdUID] = @LastUpdUID          
      ,[LastUpdTS] = @LastUpdTS          
   FROM    CRMain main          
   JOIN PreflightMain PM on  PM.TripID=main.TripID          
   WHERE    PM.TripID=@TripID          
      --- ends here          
                
                
      ---Matching Records          
      WHILE (SELECT COUNT(1) FROM PreflightLeg join CRLeg on  PreflightLeg.LegNUM = CRLeg.LegNUM   WHERE PreflightLeg.TripID=@TripID and PreflightLeg.LegNUM = @LastPrefLegNum and PreflightLeg.isDeleted = 0) >0              
   BEGIN            
                    
            ---Delete legs if not exists          
     SET @MaxPrefLegnum = (Select Max(Legnum) from PreflightLeg where PreflightLeg.TripID=@TripID)          
     SET @MaxCRLegnum = (Select Max(Legnum) from CRLeg where CRLeg.CRMainID=@CRMainID)          
     SET @Totlegs = @MaxCRLegnum - @MaxPrefLegnum                 
     IF(@MaxPrefLegnum < @MaxCRLegnum)          
     BEGIN           
      DECLARE @temp int = @Totlegs           
      WHILE (@temp !=0)          
      BEGIN               
       DECLARE @CRLegId BIGINT            
                        
       SET @MaxCRLegnum = (Select Max(Legnum) from CRLeg where CRLeg.CRMainID=@CRMainID)          
       SET  @CRLegId = (Select Max(CRLegID) from CRLeg where CRLeg.CRMainID=@CRMainID and LegNum=@MaxCRLegnum)                
       DELETE from CRCateringList   where CRLegID = @CRLegId            
       DELETE from CRFBOList   where CRLegID = @CRLegId            
       DELETE from CRHotelList   where CRLegID = @CRLegId            
       DELETE from CRPassenger   where CRLegID = @CRLegId            
       DELETE from CRTransportList   where CRLegID = @CRLegId            
       DELETE from CRLeg where CRLegID = @CRLegId           
       SET @temp =@temp-1          
        END          
     END          
     ---ends here          
               
          ---Update Leg          
             UPDATE leg            
     SET             
       [TripNUM] = PL.TripNUM              
      ,[DAirportID] = PL.DepartICAOID            
      ,[AAirportID] = PL.ArriveICAOID            
      ,[DepartureDTTMLocal] = PL.DepartureDTTMLocal            
      ,[ArrivalDTTMLocal] = PL.ArrivalDTTMLocal            
      ,[PassengerRequestorID] = PL.PassengerRequestorID            
      ,[DepartmentID] = PL.DepartmentID            
      ,[AuthorizationID] = PL.AuthorizationID            
      ,[FlightCategoryID] = PL.FlightCategoryID            
      ,[IsPrivate] = PL.IsPrivate            
      ,[Distance] = PL.Distance            
      ,[ElapseTM] = PL.ElapseTM            
      ,[PassengerTotal] = PL.PassengerTotal            
      ,[PowerSetting] = PL.PowerSetting            
      ,[TakeoffBIAS] = PL.TakeoffBIAS            
      ,[LandingBIAS] = PL.LandingBias            
      ,[TrueAirSpeed] = PL.TrueAirSpeed            
      ,[WindsBoeingTable] = PL.WindsBoeingTable            
      ,[IsDepartureConfirmed] = PL.IsDepartureConfirmed            
      ,[IsArrivalConfirmation] = PL.IsArrivalConfirmation            
      ,[IsApproxTM] = PL.IsApproxTM            
      ,[IsScheduledServices] = PL.IsScheduledServices            
      ,[DepartureGreenwichDTTM] = PL.DepartureGreenwichDTTM            
      ,[ArrivalGreenwichDTTM] = PL.ArrivalGreenwichDTTM            
      ,[HomeDepartureDTTM] = PL.HomeDepartureDTTM            
      ,[HomeArrivalDTTM] = PL.HomeArrivalDTTM           
      ,[LogBreak] = PL.LogBreak            
      ,[FlightPurpose] = PL.FlightPurpose            
      ,[SeatTotal] = PL.SeatTotal            
      ,[ReservationTotal] = PL.ReservationTotal            
      ,[ReservationAvailable] = PL.ReservationAvailable            
      ,[WaitNUM] = PL.WaitNUM            
      ,[Duty_Type] = PL.DutyTYPE            
      ,[DutyHours] = PL.DutyHours            
      ,[RestHours] = PL.RestHours            
      ,[FlightHours] = PL.FlightHours            
      ,[Notes] = PL.Notes            
      ,[FuelLoad] = PL.FuelLoad            
      ,[OutboundInstruction] = PL.OutbountInstruction            
      ,[DutyType] = PL.DutyTYPE1            
      ,[IsCrewDiscount] = PL.IsCrewDiscount          
      ,[IsDutyEnd] = PL.IsDutyEnd          
      ,[FedAviationRegNUM] = PL.FedAviationRegNUM            
      ,[LegID] = PL.LegID            
      ,[WindReliability] = PL.WindReliability           
      ,[CheckGroup] = PL.CheckGroup           
      ,[AdditionalCrew] = PL.AdditionalCrew            
      ,[PilotInCommand] = PL.PilotInCommand            
      ,[SecondInCommand] = PL.SecondInCommand            
      ,[NextLocalDTTM] = PL.NextLocalDTTM            
      ,[NextGMTDTTM] = PL.NextGMTDTTM            
      ,[NextHomeDTTM] = PL.NextHomeDTTM            
      ,[CrewNotes] = PL.CrewNotes            
      ,[LastUpdUID] = @LastUpdUID            
      ,[LastUpdTS] = @LastUpdTS           
      ,[FBOID] = PL.FBOID            
      ,[ClientID] = PL.ClientID      
      ,EStFuelQty = PL.EStFuelQty            
      ,FuelUnits = PL.FuelUnits        
      FROM    CRLeg leg          
     JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
     JOIN PreflightMain PM on  PL.TripID = PM.TripID          
     WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID          
          ---ends here            
                    
          ---Update Passenger          
          DECLARE @CurrentUpdatePassengerID BIGINT          
     CREATE Table #TempUpdatePrefPassenger          
     (          
        [PreflightPassengerListID] [bigint] NOT NULL,          
     [LegID] [bigint] NULL,          
     [CustomerID] [bigint] NULL,          
     [PassengerID] [bigint] NULL,          
     [PassportID] [bigint] NULL,          
     [VisaID] [bigint] NULL,          
     [HotelName] [varchar](60) NULL,          
     [PassengerFirstName] [varchar](63) NULL,          
     [PassengerMiddleName] [varchar](60) NULL,          
     [PassengerLastName] [varchar](60) NULL,          
     [FlightPurposeID] [bigint] NULL,          
     [PassportNUM] [varchar](25) NULL,          
     [Billing] [varchar](25) NULL,          
     [IsNonPassenger] [bit] NULL,          
     [IsBlocked] [bit] NULL,          
     [OrderNUM] [int] NULL,          
     [ReservationNUM] [varchar](6) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [WaitList] [char](1) NULL,          
     [AssociatedPassenger] [varchar](5) NULL,          
     [TripStatus] [char](1) NULL,          
     [TSADTTM] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [NoofRooms] [numeric](2, 0) NULL,          
     [ClubCard] [varchar](100) NULL,          
     [IsRateCap] [bit] NULL,          
     [MaxCapAmount] [numeric](17, 2) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL          
     )          
     INSERT INTO #TempUpdatePrefPassenger           
     SELECT PPL.* FROM PreflightPassengerList PPL,  PreflightLeg IP, PreflightMain T ,CRMain C           
       WHERE PPL.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
       ORDER BY PPL.PreflightPassengerListID ASC          
                 
         ---Delete Passenger          
           DECLARE  @MaxCRPassenger int          
           DECLARE  @MaxPrefPassenger int          
           SET @MaxCRPassenger = (SELECT Count(*) FROM CRPassenger JOIN CRLeg on CRPassenger.CRlegID =CRLeg.CRLegID            
           WHERE CRleg.CRMainID=@CRMainID and CRLeg.LegNUM = @LastPrefLegNum)          
           SET @MaxPrefPassenger =  (SELECT Count(*) FROM PreflightPassengerList JOIN PreflightLeg on PreflightPassengerList.legID = PreflightLeg.LegID           
           WHERE PreflightLeg.TripID=@TripID and PreflightLeg.LegNUM = @LastPrefLegNum)              
           IF(@MaxCRPassenger > @MaxPrefPassenger)          
           -- or @MaxCRPassenger > @MaxPrefPassenger)          
           BEGIN           
                     
              DECLARE @PassID BIGINT           
               CREATE Table #TempDeletePrefPassenger          
             (          
              [PassengerRequestorID] [bigint],          
              [CRPassengerID] [bigint]          
              )          
    ----         INSERT INTO #TempDeletePrefPassenger          
    ----          SELECT PPL.PassengerRequestorID,PPL.CRPassengerID FROM CRPassenger PPL,  CRLeg IP, CRMain T ,PreflightMain C ,          
    ----          PreflightLeg L ,PreflightPassengerList P          
    ----            WHERE PPL.CRLegID=IP.CRLegID and  IP.CRMainID = T.CRMainID and T.TripID = C.TripID           
    ----            and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum and L.TripID=C.TripID and P.LegID=L.LegID and           
    ----            L.LegNum= IP.Legnum and T.CRMainID=@CRMainID                
    ----            ORDER BY PPL.CRPassengerID ASC          
                     
                     
    ----         INSERT INTO #TempDeletePrefPassenger        
    ----            SELECT PPL.PassengerID,PPL.PreflightPassengerListID        
    ----FROM CRMain CM        
    ----INNER JOIN CRLeg CL ON CM.CRMainID = CL.CRMainID        
    ----INNER JOIN CRPassenger CP ON CL.CRLegID = CP.CRLegID        
    ----INNER JOIN PreflightMain PM ON CM.TripID = PM.TripID        
    ----INNER JOIN PreflightLeg PL ON PM.TripID = @NewTripID AND CL.LegNUM = PL.LegNUM        
    ----INNER JOIN PreflightPassengerList PPL         
    ----ON PL.LegID = PPL.LegID AND CP.PassengerRequestorID = PPL.PassengerID AND CP.FlightPurposeID = PPL.FlightPurposeID        
    ----INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID        
    ----WHERE CM.CRMainID = @CRMainID        
    ----AND CL.LegNUM = @LastCRLegNum        
            
            
      INSERT INTO #TempDeletePrefPassenger        
                SELECT PPL.PassengerID,PPL.PreflightPassengerListID        
    FROM PreflightMain PM        
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID        
    INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID        
    INNER JOIN CRMain CM ON PM.TripID = CM.TripID        
    INNER JOIN CRLeg CL ON CM.CRMainID = @CRMainID AND PL.LegNUM = CL.LegNUM        
    INNER JOIN CRPassenger CP         
    ON CL.LegID = CP.LegID AND PPL.PassengerID = CP.PassengerRequestorID AND PPL.FlightPurposeID = CP.FlightPurposeID        
    WHERE PM.TripID = @TripID        
    AND PL.LegNUM = @LastPrefLegNum        
               
                          
                DELETE FROM CRPassenger WHERE CRPassenger.CRPassengerID  not in (SELECT          
                CRPassengerID FROM #TempDeletePrefPassenger)                       
                          
                DROP TABLE #TempDeletePrefPassenger           
                          
            END          
            
                      
                   
          WHILE (SELECT COUNT(*) from #TempUpdatePrefPassenger)>0          
    BEGIN            
               DECLARE @NewCRLegID BIGINT          
               SET @NewCRLegID = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)          
       -- Get current crpassenger id          
       SELECT TOP 1 @CurrentUpdatePassengerID = PreflightPassengerListID FROM #TempUpdatePrefPassenger            
       DECLARE @PASSENGERID BIGINT          
       SELECT TOP 1 @PASSENGERID = PassengerID FROM #TempUpdatePrefPassenger           
                 
       IF EXISTS (SELECT  passenger.PassengerRequestorID FROM CRPassenger  passenger JOIN  CRLeg leg on passenger.CRLegID = leg.CRLegID           
       WHERE  passenger.PassengerRequestorID=@PASSENGERID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID)          
       BEGIN          
           UPDATE passenger            
         SET            
          CustomerID = @CustomerID              
         ,TripNUM = PM.TripNUM          
         ,LegID = PL.LegID          
         ,OrderNUM = PP.OrderNUM          
         ,PassengerRequestorID = PP.PassengerID          
         ,PassengerName = passenger.PassengerName          
         ,FlightPurposeID = PP.FlightPurposeID          
         ,PassportID = PP.PassportID          
         ,IsNonPassenger = PP.IsNonPassenger          
         ,IsBlocked = PP.IsBlocked          
         ,ReservationNUM = PP.ReservationNUM          
         ,WaitList = PP.WaitList          
         ,LastUpdUID = @LastUpdUID          
         ,LastUpdTS = @LastUpdTS              
           FROM     CRPassenger passenger          
         JOIN  CRLeg leg on passenger.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
         JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
         JOIN PreflightMain PM on  PL.TripID = PM.TripID          
         JOIN PreflightPassengerList PP on  PP.LegID=PL.LegID and PP.PassengerID = passenger.PassengerRequestorID                
         WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID and PP.PreflightPassengerListID=@CurrentUpdatePassengerID          
       END          
       ELSE          
       BEGIN          
        DECLARE @CRPassengerListID BIGINT          
           EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPassengerListID OUTPUT                 
                        
        INSERT INTO CRPassenger          
        (CRPassengerID          
        ,CustomerID          
        ,CRLegID          
        ,TripNUM          
        ,LegID          
        ,OrderNUM          
        ,PassengerRequestorID          
        ,PassengerName          
        ,FlightPurposeID          
        ,PassportID          
        ,Billing          
        ,IsNonPassenger          
        ,IsBlocked          
        ,ReservationNUM          
        ,WaitList          
        ,LastUpdUID          
        ,LastUpdTS          
        ,IsDeleted)          
        SELECT          
         @CRPassengerListID          
        ,PPL.CustomerID            
        ,@NewCRLegID          
        ,NULL          
        ,PPL.LegID          
        ,PPL.OrderNUM          
        ,PPL.PassengerID          
        ,PPL.PassengerFirstName + PPL.PassengerLastName + PPL.PassengerMiddleName          
        ,PPL.FlightPurposeID          
        ,PPL.PassportID          
        ,PPL.Billing          
        ,PPL.IsNonPassenger          
        ,PPL.IsBlocked          
        ,PPL.ReservationNUM          
        ,PPL.WaitList          
        ,@LastUpdUID          
        ,@LastUpdTS          
        ,PPL.IsDeleted          
                  
       FROM #TempUpdatePrefPassenger PPL          
       WHERE PreflightPassengerListID = @CurrentUpdatePassengerID           
       END          
                 
             
      -- delete the cr hotel record from temp table          
     DELETE FROM #TempUpdatePrefPassenger WHERE PreflightPassengerListID = @CurrentUpdatePassengerID             
    END           
             
        DROP TABLE #TempUpdatePrefPassenger           
        ---ends here           
                  
    ---- Update Leg for Passenger Total          
    --DECLARE  @Passengertot int          
    --SET @Passengertot = (SELECT Count(*) FROM CRPassenger JOIN CRLeg on CRPassenger.CRlegID =CRLeg.CRLegID            
    --WHERE CRleg.CRMainID=@CRMainID and CRLeg.LegNUM = @LastPrefLegNum)          
                      
                      
    --UPDATE CRLeg set PassengerTotal = @Passengertot where CRleg.CRMainID=@CRMainID and CRLeg.LegNUM = @LastPrefLegNum          
    -----Ends here            
                   
                 
                 
       ---Update DepartFbo          
        DECLARE @CurrentUpdateFboID BIGINT          
        CREATE Table #TempUpdatePrefFBOlist          
     (          
     [PreflightFBOID] [bigint] NOT NULL,          
     [PreflightFBOName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [FBOID] [bigint] NULL,          
     [IsArrivalFBO] [bit] NULL,          
     [IsDepartureFBO] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [NoUpdated] [bit] NULL,          
     [FBOArrangedBy] [bit] NULL,          
     [IsCompleted] [bit] NULL,          
     [FBOInformation] [varchar](1000) NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Status] [varchar](20) NULL          
     --[IsRequired] [bit] NULL,          
     --[IsNotRequired] [bit] NULL,          
     --[IsInProgress] [bit] NULL,          
     --[IsChange] [bit] NULL,          
     --[IsCancelled] [bit] NULL          
      )          
     INSERT INTO #TempUpdatePrefFBOlist           
     SELECT PFO.* FROM PreflightFBOList PFO,  PreflightLeg IP, PreflightMain T ,CRMain C           
           WHERE PFO.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID           
           and IP.Legnum=@LastPrefLegNum and PFO.IsDepartureFBO=1          
           ORDER BY PFO.PreflightFBOID ASC          
     WHILE (SELECT COUNT(*) from #TempUpdatePrefFBOlist)>0          
     BEGIN            
              DECLARE @NewCRLegIDFBO BIGINT          
               SET @NewCRLegIDFBO = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
               -- Get current crpassenger id          
       SELECT TOP 1 @CurrentUpdateFboID = PreflightFBOID FROM #TempUpdatePrefFBOlist           
       DECLARE @FBOID BIGINT          
       SELECT TOP 1 @FBOID = FBOID FROM #TempUpdatePrefFBOlist           
                 
       IF EXISTS (SELECT  fbo.FBOID FROM CRFBOList  fbo JOIN  CRLeg leg on fbo.CRLegID = leg.CRLegID           
       WHERE  fbo.FBOID=@FBOID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID)          
  BEGIN               
        UPDATE departfbo          
          SET            
          CustomerID = @CustomerID              
         ,TripNUM = PL.TripNUM              
         ,LegID = PL.LegID          
         ,RecordType = 'D'          
         ,FBOID = PFO.FBOID          
         ,CRFBOListDescription = PFO.PreflightFBOName          
         ,PhoneNUM = PFO.PhoneNum1          
         ,IsCompleted = PFO.IsCompleted          
         ,AirportID = PFO.AirportID          
         ,LastUpdUID = @LastUpdUID          
         ,LastUpdTS = @LastUpdTS                    
          FROM     CRFBOList departfbo          
         JOIN  CRLeg leg on departfbo.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
         JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
         JOIN PreflightMain PM on  PL.TripID = PM.TripID          
         JOIN PreflightFBOList PFO on PFO.LegID=PL.LegID and PFO.FBOID = departfbo.FBOID and PFO.IsDepartureFBO=1 and PFO.IsArrivalFBO=0               
         WHERE  PM.TripID=@TripID and leg.CRMainID=@CRMainID and departfbo.RecordType='D' and PFO.PreflightFBOID=@CurrentUpdateFboID          
       END           
       ELSE          
       BEGIN          
           DECLARE @CRFBOListsID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRFBOListsID OUTPUT          
                     
          INSERT INTO CRFBOList          
           ([CRFBOListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[FBOID]          
           ,[CRFBOListDescription]          
           ,[PhoneNUM]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
            @CRFBOListsID          
           ,PFO.CustomerID            
           ,@NewLegID          
           ,NULL          
           ,PFO.LegID          
           ,'D'          
           ,PFO.FBOID          
           ,PFO.PreflightFBOName          
           ,PFO.PhoneNum1          
           ,PFO.IsCompleted          
           ,PFO.AirportID          
           ,@LastUpdUID          
           ,@LastUpdTS          
           ,PFO.IsDeleted          
           FROM #TempUpdatePrefFBOlist PFO          
                       WHERE PreflightFBOID = @CurrentUpdateFboID           
                             
       END          
       DELETE FROM  #TempUpdatePrefFBOlist where PreflightFBOID = @CurrentUpdateFboID           
     END           
     DROP TABLE #TempUpdatePrefFBOlist          
       --ends here           
                 
                 
       ---Update ArriveFbo          
         DECLARE @CurrentUpdateArrFboID BIGINT          
         CREATE Table #TempUpdatePrefArrFBOlist          
     (          
     [PreflightFBOID] [bigint] NOT NULL,          
     [PreflightFBOName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [FBOID] [bigint] NULL,          
     [IsArrivalFBO] [bit] NULL,          
     [IsDepartureFBO] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [NoUpdated] [bit] NULL,          
     [FBOArrangedBy] [bit] NULL,          
     [IsCompleted] [bit] NULL,          
     [FBOInformation] [varchar](1000) NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Status] [varchar](20) NULL          
     --[IsRequired] [bit] NULL,          
     --[IsNotRequired] [bit] NULL,          
     --[IsInProgress] [bit] NULL,          
     --[IsChange] [bit] NULL,          
     --[IsCancelled] [bit] NULL          
      )          
      INSERT INTO #TempUpdatePrefArrFBOlist           
     SELECT PFO.* FROM PreflightFBOList PFO,  PreflightLeg IP, PreflightMain T ,CRMain C           
      WHERE PFO.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum           
      and PFO.isArrivalFBO=1          
           ORDER BY PFO.PreflightFBOID ASC            
      WHILE (SELECT COUNT(*) from #TempUpdatePrefArrFBOlist)>0          
      BEGIN            
            DECLARE @NewCRLegIDARRFBO BIGINT          
               SET @NewCRLegIDARRFBO = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
               -- Get current crpassenger id          
       SELECT TOP 1 @CurrentUpdateArrFboID = PreflightFBOID FROM #TempUpdatePrefArrFBOlist           
       DECLARE @ARRFBOID BIGINT          
       SELECT TOP 1 @ARRFBOID = FBOID FROM #TempUpdatePrefArrFBOlist           
                 
       IF EXISTS (SELECT  fbo.FBOID FROM CRFBOList  fbo JOIN  CRLeg leg on fbo.CRLegID = leg.CRLegID           
       WHERE  fbo.FBOID=@ARRFBOID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID)          
       BEGIN          
        UPDATE arrivefbo          
           SET            
           CustomerID = @CustomerID              
          ,TripNUM = PL.TripNUM              
          ,LegID = PL.LegID          
          ,RecordType = 'A'          
          ,FBOID = PFO.FBOID          
          ,CRFBOListDescription = PFO.PreflightFBOName          
          ,PhoneNUM = PFO.PhoneNum1          
          ,IsCompleted = PFO.IsCompleted          
          ,AirportID = PFO.AirportID          
          ,LastUpdUID = @LastUpdUID          
          ,LastUpdTS = @LastUpdTS                    
           FROM     CRFBOList arrivefbo          
          JOIN  CRLeg leg on arrivefbo.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
          JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
          JOIN PreflightMain PM on  PL.TripID = PM.TripID          
          JOIN PreflightFBOList PFO on  PFO.LegID=PL.LegID and PFO.FBOID = arrivefbo.FBOID and PFO.IsArrivalFBO=1 and  PFO.IsDepartureFBO=0               
          WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID and arrivefbo.RecordType='A' and PFO.PreflightFBOID=@CurrentUpdateArrFboID          
       END          
       ELSE          
       BEGIN          
          DECLARE @CRFBOARRListsID BIGINT          
            EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRFBOARRListsID OUTPUT          
                     
          INSERT INTO CRFBOList          
           ([CRFBOListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[FBOID]          
           ,[CRFBOListDescription]          
           ,[PhoneNUM]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
            @CRFBOARRListsID          
           ,PFO.CustomerID            
           ,@NewCRLegIDARRFBO          
           ,NULL          
           ,PFO.LegID          
           ,'A'          
           ,PFO.FBOID          
           ,PFO.PreflightFBOName          
           ,PFO.PhoneNum1          
           ,PFO.IsCompleted          
           ,PFO.AirportID          
           ,@LastUpdUID          
           ,@LastUpdTS          
           ,PFO.IsDeleted          
           FROM #TempUpdatePrefArrFBOlist PFO          
                       WHERE PreflightFBOID = @CurrentUpdateArrFboID                              
       END          
       DELETE FROM  #TempUpdatePrefArrFBOlist where PreflightFBOID = @CurrentUpdateArrFboID           
      END          
      DROP TABLE #TempUpdatePrefArrFBOlist           
       --ends here             
                 
                 
       ---Update DepartCatering          
           DECLARE @CurrentUpdateDEPCaterID BIGINT          
           CREATE Table #TempPrefDEPCaterlist          
     (          
        [PreflightCateringID] [bigint] NOT NULL,          
     [LegID] [bigint] NULL,          
     [CustomerID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [ArriveDepart] [char](1) NULL,          
     [CateringID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Cost] [numeric](17, 2) NULL,          
     [ContactName] [varchar](50) NULL,          
     [ContactPhone] [varchar](20) NULL,          
     [ContactFax] [varchar](20) NULL,          
     [CateringConfirmation] [varchar](1500) NULL,          
     [CateringComments] [varchar](1500) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [IsUWAArranger] [bit] NULL,          
     [CateringContactName] [varchar](40) NULL,          
     [Status] [varchar](20) NULL          
     )          
        INSERT INTO #TempPrefDEPCaterlist           
     SELECT PCD.* FROM PreflightCateringDetail PCD,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE PCD.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
         and ArriveDepart ='D'          
      ORDER BY PCD.PreflightCateringID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefDEPCaterlist)>0          
       BEGIN            
            DECLARE @NewCRLegIDDEPCATER BIGINT          
               SET @NewCRLegIDDEPCATER = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
       -- Get current crpassenger id          
         SELECT TOP 1 @CurrentUpdateDEPCaterID = PreflightCateringID FROM #TempPrefDEPCaterlist           
         DECLARE @ARRDEPCATERID BIGINT          
         SELECT TOP 1 @ARRDEPCATERID = CateringID FROM #TempPrefDEPCaterlist            
         IF EXISTS (SELECT  cater.CateringID FROM CRCateringList  cater JOIN  CRLeg leg on cater.CRLegID = leg.CRLegID           
       WHERE  cater.CateringID=@ARRDEPCATERID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID)          
       BEGIN          
        UPDATE departcatering            
          SET            
          CustomerID = @CustomerID               
         ,TripNUM = PL.TripNUM             
         ,LegID = PL.LegID          
         ,RecordType = 'D'          
         ,CateringID = PCA.CateringID          
         ,CRCateringListDescription = PCA.CateringContactName          
         ,PhoneNUM = PCA.ContactPhone          
         ,IsCompleted = PCA.IsCompleted          
         ,AirportID = PCA.AirportID          
         ,LastUpdUID = @LastUpdUID          
         ,LastUpdTS = @LastUpdTS               
          FROM     CRCateringList departcatering          
         JOIN  CRLeg leg on departcatering.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
         JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
         JOIN PreflightMain PM on  PL.TripID = PM.TripID          
         JOIN PreflightCateringDetail PCA on  PCA.LegID=PL.LegID and PCA.CateringID = departcatering.CateringID and PCA.ArriveDepart='D'                
         WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID  and departcatering.RecordType='D' and PCA.PreflightCateringID=@CurrentUpdateDEPCaterID          
       END          
       ELSE          
       BEGIN          
            DECLARE @CRDEPCateringListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRDEPCateringListID OUTPUT          
                   
                   
         INSERT INTO [CRCateringList]          
           ([CRCateringListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[CateringID]          
     ,[CRCateringListDescription]          
           ,[PhoneNUM]          
           ,[Rate]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
           @CRDEPCateringListID          
          ,PCD.CustomerID            
          ,@NewCRLegIDDEPCATER          
          ,NULL          
          ,PCD.LegID          
          ,'D'          
          ,PCD.CateringID          
          ,PCD.ContactName          
          ,PCD.ContactPhone          
          ,NULL          
          ,PCD.IsCompleted          
          ,PCD.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCD.IsDeleted          
          FROM #TempPrefDEPCaterlist PCD          
                   WHERE PreflightCateringID = @CurrentUpdateDEPCaterID           
       END          
       DELETE FROM #TempPrefDEPCaterlist where PreflightCateringID=@CurrentUpdateDEPCaterID          
       END          
     DROP TABLE #TempPrefDEPCaterlist           
       --ends here              
                 
                 
        ---Update arrivecatering          
          DECLARE @CurrentUpdateARRCaterID BIGINT          
          CREATE Table #TempPrefARRCaterlist          
     (          
        [PreflightCateringID] [bigint] NOT NULL,          
     [LegID] [bigint] NULL,          
     [CustomerID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [ArriveDepart] [char](1) NULL,          
     [CateringID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Cost] [numeric](17, 2) NULL,          
     [ContactName] [varchar](50) NULL,          
     [ContactPhone] [varchar](20) NULL,          
     [ContactFax] [varchar](20) NULL,          
   [CateringConfirmation] [varchar](1500) NULL,          
     [CateringComments] [varchar](1500) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [IsUWAArranger] [bit] NULL,          
     [CateringContactName] [varchar](40) NULL,          
     [Status] [varchar](20) NULL          
     )          
       INSERT INTO #TempPrefARRCaterlist           
     SELECT PCD.* FROM PreflightCateringDetail PCD,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE PCD.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
         and ArriveDepart ='A'          
      ORDER BY PCD.PreflightCateringID ASC          
       WHILE (SELECT COUNT(*) from #TempPrefARRCaterlist)>0          
     BEGIN          
            DECLARE @NewCRLegIDARRCATER BIGINT          
               SET @NewCRLegIDARRCATER = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
            -- Get current crpassenger id          
         SELECT TOP 1 @CurrentUpdateARRCaterID = PreflightCateringID FROM #TempPrefARRCaterlist           
         DECLARE @ARRCATERID BIGINT          
         SELECT TOP 1 @ARRCATERID = CateringID FROM #TempPrefARRCaterlist            
         IF EXISTS (SELECT  cater.CateringID FROM CRCateringList  cater JOIN  CRLeg leg on cater.CRLegID = leg.CRLegID           
       WHERE  cater.CateringID=@ARRCATERID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID )          
       BEGIN          
        UPDATE arrivecatering            
     SET            
          CustomerID = @CustomerID               
         ,TripNUM = PL.TripNUM             
         ,LegID = PL.LegID          
         ,RecordType = 'A'          
         ,CateringID = PCA.CateringID          
         ,CRCateringListDescription = PCA.CateringContactName          
         ,PhoneNUM = PCA.ContactPhone          
         ,IsCompleted = PCA.IsCompleted          
         ,AirportID = PCA.AirportID          
         ,LastUpdUID = @LastUpdUID          
         ,LastUpdTS = @LastUpdTS               
          FROM     CRCateringList arrivecatering          
         JOIN  CRLeg leg on arrivecatering.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
         JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
         JOIN PreflightMain PM on  PL.TripID = PM.TripID          
         JOIN PreflightCateringDetail PCA on  PCA.LegID=PL.LegID and PCA.CateringID = arrivecatering.CateringID and PCA.ArriveDepart='A'               
         WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID  and arrivecatering.RecordType='A'           
         and PCA.PreflightCateringID=@CurrentUpdateARRCaterID          
          END           
          ELSE          
       BEGIN          
            DECLARE @CRARRCateringListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRARRCateringListID OUTPUT          
                   
                   
         INSERT INTO [CRCateringList]          
           ([CRCateringListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[CateringID]          
           ,[CRCateringListDescription]          
           ,[PhoneNUM]          
           ,[Rate]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
           @CRARRCateringListID          
          ,PCD.CustomerID            
          ,@NewCRLegIDARRCATER          
          ,NULL          
          ,PCD.LegID          
          ,'A'          
          ,PCD.CateringID          
          ,PCD.ContactName          
          ,PCD.ContactPhone          
          ,NULL          
          ,PCD.IsCompleted          
          ,PCD.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCD.IsDeleted          
          FROM #TempPrefARRCaterlist PCD          
                   WHERE PreflightCateringID = @CurrentUpdateARRCaterID           
        
       END          
       DELETE FROM #TempPrefARRCaterlist where PreflightCateringID=@CurrentUpdateARRCaterID          
     END          
       DROP TABLE #TempPrefARRCaterlist           
       --ends here              
                 
                 
       ---Update crew hotel          
         
       -- prem  
         
         
       --DECLARE @CurrentUpdatePrefCrewHotelID BIGINT          
         
       DECLARE @CurrentUpdatePrefHotelID BIGINT          
   -- CREATE Table #TempUpdatePrefCrewHotellist          
   -- prem  
  CREATE Table #TempUpdatePrefHotellist          
     (          
        [PreflightHotelListID] [bigint] NOT NULL,          
   [PreflightHotelName] [varchar](60) NULL,   
  -- prem          
   [LegID] [bigint]  NULL,   
    -- [PreflightCrewListID] [bigint] NULL,    
       
     [HotelID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [RoomType] [varchar](60) NULL,          
     [RoomRent] [numeric](6, 2) NULL,          
     [RoomConfirm] [varchar](max) NULL,          
     [RoomDescription] [varchar](max) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [HotelArrangedBy] [varchar](10) NULL,          
     [Rate] [numeric](17, 2) NULL,          
     [DateIn] [date] NULL,          
     [DateOut] [date] NULL,          
     [IsSmoking] [bit] NULL,          
     [RoomTypeID] [bigint] NULL,          
     [NoofBeds] [numeric](3, 0) NULL,          
     [IsAllCrew] [bit] NULL,          
     [IsCompleted] [bit] NULL,          
     [IsEarlyCheckIn] [bit] NULL,          
     [EarlyCheckInOption] [char](1) NULL,          
     [SpecialInstructions] [varchar](100) NULL,          
[ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Address1] [varchar](40) NULL,          
     [Address2] [varchar](40) NULL,          
     [Address3] [varchar](40) NULL,          
     [MetroID] [bigint] NULL,          
     [City] [varchar](30) NULL,          
     [StateProvince] [varchar](25) NULL,          
     [CountryID] [bigint] NULL,          
     [PostalCode] [varchar](15) NULL,               
     [Status] [varchar](20) NULL   ,    
     isArrivalHotel bit NULL,  
  isDepartureHotel bit NULL,  
  crewPassengerType  varchar(1)   
     )     
    -- prem       
   -- INSERT INTO #TempUpdatePrefCrewHotellist   
   INSERT INTO #TempUpdatePrefHotellist  
  --   SELECT PCH.* FROM PreflightCrewHotelList PCH, PreflightCrewList PCL,  PreflightLeg IP, PreflightMain T ,CRMain C           
    SELECT PHL.* FROM PreflightHotelList PHL,   PreflightLeg IP, PreflightMain T ,CRMain C           
      --   WHERE PCH.PreflightCrewListID=PCL.PreflightCrewListID and  PCL.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
           WHERE PHL.LegID=IP.LegID and   IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
   --   ORDER BY PCH.PreflightCrewHotelListID ASC          
      ORDER BY PHL.PreflightHotelListID ASC          
  --  WHILE (SELECT COUNT(*) from #TempUpdatePrefCrewHotellist)>0       
   WHILE (SELECT COUNT(*) from #TempUpdatePrefHotellist)>0     
    BEGIN          
       DECLARE @NewCRLegIDCREW BIGINT          
                SET @NewCRLegIDCREW = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
                          
                -- Get current crpassenger id          
      -- SELECT TOP 1 @CurrentUpdatePrefCrewHotelID = PreflightCrewHotelListID FROM #TempUpdatePrefCrewHotellist          
       SELECT TOP 1 @CurrentUpdatePrefHotelID = PreflightHotelListID FROM #TempUpdatePrefHotellist          
       --DECLARE @ARRCREWHOTELID BIGINT          
       DECLARE @ARRHOTELID BIGINT          
       --   SELECT TOP 1 @ARRCREWHOTELID = HotelID FROM #TempUpdatePrefCrewHotellist            
       SELECT TOP 1 @ARRHOTELID = HotelID FROM #TempUpdatePrefHotellist            
          IF EXISTS (SELECT  hotel.HotelID FROM CRHotelList  hotel JOIN  CRLeg leg on hotel.CRLegID = leg.CRLegID           
     --  WHERE  hotel.HotelID=@ARRCREWHOTELID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID )          
       WHERE  hotel.HotelID=@ARRHOTELID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID )    
               
       BEGIN          
       UPDATE crewhotel            
         SET            
         CustomerID = @CustomerID              
        ,TripNUM = PL.TripNUM          
        ,LegID = PL.LegID          
        ,RecordType = 'C'          
        --,HotelID = PCH.HotelID          
        --,CRHotelListDescription = PCH.PreflightHotelName          
        --,PhoneNUM = PCH.PhoneNum1          
        --,IsCompleted = PCH.IsCompleted          
        --,AirportID = PCH.AirportID        
        ,HotelID = PHL.HotelID          
        ,CRHotelListDescription = PHL.PreflightHotelName          
        ,PhoneNUM =PHL.PhoneNum1          
        ,IsCompleted = PHL.IsCompleted          
        ,AirportID = PHL.AirportID          
        ,FaxNum =  PHL.FaxNum  
        ,LastUpdUID = @LastUpdUID          
        ,LastUpdTS = @LastUpdTS     
        ,Rate=PHL.Rate          
        ,IsDeleted = PHL.IsDeleted    
       FROM  CRHotelList crewhotel          
        JOIN  CRLeg leg on crewhotel.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
        JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
        JOIN PreflightMain PM on  PL.TripID = PM.TripID          
      --  JOIN PreflightCrewList PC on PC.LegID=PL.LegID          
      --  JOIN PreflightCrewHotelList PCH on  PCH.PreflightCrewListID=PC.PreflightCrewListID and PCH.HotelID = crewhotel.HotelID        
      --   WHERE PM.TripID=@TripID and leg.CRMainID=@CRMainID and crewhotel.RecordType='C' and PCH.PreflightCrewHotelListID= @CurrentUpdatePrefCrewHotelID          
        JOIN PreflightHotelList PHL on  PHL.LegID=PL.LegID and PHL.HotelID = crewhotel.HotelID  
        WHERE PM.TripID=@TripID and leg.CRMainID=@CRMainID and crewhotel.RecordType='C' and PHL.PreflightHotelListID= @CurrentUpdatePrefHotelID   
       
       END          
       ELSE          
       BEGIN          
           -- DECLARE @CRARRCrewHotelListID BIGINT       
              --EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRARRCrewHotelListID OUTPUT     
           DECLARE @CRARRHotelListID BIGINT             
           EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRARRHotelListID OUTPUT     
                   
                   
         INSERT INTO CRHotelList          
         ([CRHotelListID]          
         ,[CustomerID]          
         ,[CRLegID]          
         ,[TripNUM]          
         ,[LegID]          
         ,[RecordType]          
         ,[HotelID]          
         ,[CRHotelListDescription]          
         ,[PhoneNUM]        
         ,[Rate]    
         ,[IsCompleted]          
         ,[AirportID]          
         ,[LastUpdUID]          
         ,[LastUpdTS]          
         ,[IsDeleted])                          
           SELECT          
--           @CRARRCrewHotelListID          
  @CRARRHotelListID  
          ,PHL.CustomerID            
          ,@NewCRLegIDCREW          
          ,NULL          
          ,PHL.LegID          
          ,'C'          
          ,PHL.HotelID          
          ,PHL.PreflightHotelName          
          ,PHL.PhoneNum1      
          ,PHL.Rate               
          ,PHL.IsCompleted          
          ,PHL.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PHL.IsDeleted          
         -- FROM #TempUpdatePrefCrewHotellist PCH                               WHERE PreflightCrewHotelListID = @CurrentUpdatePrefCrewHotelID           
          FROM #TempUpdatePrefHotellist PHL                               WHERE PreflightHotelListID = @CurrentUpdatePrefHotelID           
                   
END          
       DELETE FROM #TempUpdatePrefHotellist where PreflightHotelListID = @CurrentUpdatePrefHotelID          
    END  
      
      
      
               
       
       ---ends here           
                 
                 
         -- INSERT INTO #TempUpdatePrefCrewHotellist   
   INSERT INTO #TempUpdatePrefHotellist  
  --   SELECT PCH.* FROM PreflightCrewHotelList PCH, PreflightCrewList PCL,  PreflightLeg IP, PreflightMain T ,CRMain C           
    SELECT PHL.* FROM PreflightHotelList PHL,   PreflightLeg IP, PreflightMain T ,CRMain C           
      --   WHERE PCH.PreflightCrewListID=PCL.PreflightCrewListID and  PCL.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
           WHERE PHL.LegID=IP.LegID and   IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
   --   ORDER BY PCH.PreflightCrewHotelListID ASC          
      ORDER BY PHL.PreflightHotelListID ASC          
  --  WHILE (SELECT COUNT(*) from #TempUpdatePrefCrewHotellist)>0       
   WHILE (SELECT COUNT(*) from #TempUpdatePrefHotellist)>0     
    BEGIN          
     
                SET @NewCRLegIDCREW = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
                          
                -- Get current crpassenger id          
      -- SELECT TOP 1 @CurrentUpdatePrefCrewHotelID = PreflightCrewHotelListID FROM #TempUpdatePrefCrewHotellist          
       SELECT TOP 1 @CurrentUpdatePrefHotelID = PreflightHotelListID FROM #TempUpdatePrefHotellist          
       --DECLARE @ARRCREWHOTELID BIGINT          
        
       --   SELECT TOP 1 @ARRCREWHOTELID = HotelID FROM #TempUpdatePrefCrewHotellist            
       SELECT TOP 1 @ARRHOTELID = HotelID FROM #TempUpdatePrefHotellist            
          IF EXISTS (SELECT  hotel.HotelID FROM CRHotelList  hotel JOIN  CRLeg leg on hotel.CRLegID = leg.CRLegID           
     --  WHERE  hotel.HotelID=@ARRCREWHOTELID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID )          
       WHERE  hotel.HotelID=@ARRHOTELID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID )    
               
       BEGIN          
       UPDATE crewhotel            
         SET            
         CustomerID = @CustomerID              
        ,TripNUM = PL.TripNUM          
        ,LegID = PL.LegID          
        ,RecordType = 'P'          
        --,HotelID = PCH.HotelID          
        --,CRHotelListDescription = PCH.PreflightHotelName          
        --,PhoneNUM = PCH.PhoneNum1          
        --,IsCompleted = PCH.IsCompleted          
        --,AirportID = PCH.AirportID        
        ,HotelID = PHL.HotelID          
        ,CRHotelListDescription = PHL.PreflightHotelName          
        ,PhoneNUM =PHL.PhoneNum1          
        ,IsCompleted = PHL.IsCompleted          
        ,AirportID = PHL.AirportID          
        ,FaxNum =  PHL.FaxNum  
        ,LastUpdUID = @LastUpdUID          
        ,LastUpdTS = @LastUpdTS     
        ,Rate=PHL.Rate          
        ,IsDeleted = PHL.IsDeleted    
       FROM  CRHotelList crewhotel          
        JOIN  CRLeg leg on crewhotel.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
        JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
        JOIN PreflightMain PM on  PL.TripID = PM.TripID          
      --  JOIN PreflightCrewList PC on PC.LegID=PL.LegID          
      --  JOIN PreflightCrewHotelList PCH on  PCH.PreflightCrewListID=PC.PreflightCrewListID and PCH.HotelID = crewhotel.HotelID        
      --   WHERE PM.TripID=@TripID and leg.CRMainID=@CRMainID and crewhotel.RecordType='C' and PCH.PreflightCrewHotelListID= @CurrentUpdatePrefCrewHotelID          
        JOIN PreflightHotelList PHL on  PHL.LegID=PL.LegID and PHL.HotelID = crewhotel.HotelID  
        WHERE PM.TripID=@TripID and leg.CRMainID=@CRMainID and crewhotel.RecordType='P' and PHL.PreflightHotelListID= @CurrentUpdatePrefHotelID   
       
       END          
       ELSE          
       BEGIN          
           -- DECLARE @CRARRCrewHotelListID BIGINT       
              --EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRARRCrewHotelListID OUTPUT     
           
           EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRARRHotelListID OUTPUT     
                   
                   
         INSERT INTO CRHotelList          
         ([CRHotelListID]          
         ,[CustomerID]          
         ,[CRLegID]          
         ,[TripNUM]          
         ,[LegID]          
         ,[RecordType]          
         ,[HotelID]          
         ,[CRHotelListDescription]          
         ,[PhoneNUM]        
         ,[Rate]    
         ,[IsCompleted]          
         ,[AirportID]          
         ,[LastUpdUID]          
         ,[LastUpdTS]          
         ,[IsDeleted])                          
           SELECT          
--           @CRARRCrewHotelListID          
  @CRARRHotelListID  
          ,PHL.CustomerID            
          ,@NewCRLegIDCREW          
          ,NULL          
          ,PHL.LegID          
          ,'P'          
          ,PHL.HotelID          
          ,PHL.PreflightHotelName          
          ,PHL.PhoneNum1      
          ,PHL.Rate               
          ,PHL.IsCompleted          
          ,PHL.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PHL.IsDeleted          
         -- FROM #TempUpdatePrefCrewHotellist PCH                               WHERE PreflightCrewHotelListID = @CurrentUpdatePrefCrewHotelID           
          FROM #TempUpdatePrefHotellist PHL                               WHERE PreflightHotelListID = @CurrentUpdatePrefHotelID           
                   
END          
       DELETE FROM #TempUpdatePrefHotellist where PreflightHotelListID = @CurrentUpdatePrefHotelID          
    END        
        
          DROP TABLE #TempUpdatePrefHotellist               
                 
       ---Update passengertransport          
       DECLARE @CurrentUpdatePrefPAXTransportID BIGINT          
    CREATE Table #TempUpdatePrefPAXTransportlist          
     (          
     [PreflightTransportID] [bigint] NOT NULL,          
     [PreflightTransportName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [TransportID] [bigint] NULL,          
     [IsArrivalTransport] [bit] NULL,          
     [IsDepartureTransport] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [CrewPassengerType] [char](1) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Status] [varchar](20) NULL          
      )          
    INSERT INTO #TempUpdatePrefPAXTransportlist           
     SELECT PCT.* FROM PreflightTransportList PCT,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE  PCT.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
         and PCT.IsArrivalTransport=1 and CrewPassengerType='P'          
      ORDER BY PCT.PreflightTransportID ASC          
    WHILE (SELECT COUNT(*) from #TempUpdatePrefPAXTransportlist)>0          
    BEGIN          
                DECLARE @NewCRLegIDPaxTrans BIGINT          
                SET @NewCRLegIDPaxTrans = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
                          
                -- Get current crpassenger id          
       SELECT TOP 1 @CurrentUpdatePrefPAXTransportID = PreflightTransportID FROM #TempUpdatePrefPAXTransportlist          
       DECLARE @ARRPAXTransportID BIGINT          
          SELECT TOP 1 @ARRPAXTransportID = TransportID FROM #TempUpdatePrefPAXTransportlist          
          IF EXISTS (SELECT  transport.TransportID FROM CRTransportList  transport JOIN  CRLeg leg on transport.CRLegID = leg.CRLegID           
       WHERE  transport.TransportID=@ARRPAXTransportID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID and transport.RecordType='P')          
       BEGIN              
       UPDATE passengertransport          
         SET            
         CustomerID = @CustomerID              
        ,TripNUM = PL.TripNUM          
        ,LegID = PL.LegID          
        ,RecordType = 'P'          
        ,TransportID = PT.TransportID          
        ,CRTransportListDescription = PT.PreflightTransportName          
        ,PhoneNUM = PT.PhoneNum1          
        ,IsCompleted = PT.IsCompleted          
        ,AirportID = PT.AirportID          
        ,LastUpdUID = @LastUpdUID          
        ,LastUpdTS = @LastUpdTS               
         FROM  CRTransportList passengertransport          
        JOIN  CRLeg leg on passengertransport.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
        JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
        JOIN PreflightMain PM on  PL.TripID = PM.TripID          
        --JOIN PreflightCrewList PC on PC.LegID=PL.LegID          
        JOIN PreflightTransportList PT on  PT.LegID=PL.LegID and PT.TransportID = passengertransport.TransportID          
        WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID and passengertransport.RecordType='P' and          
        PT.PreflightTransportID=@CurrentUpdatePrefPAXTransportID          
       END           
       ELSE         
       BEGIN           
         DECLARE @CRPaxlistTransportListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPaxlistTransportListID OUTPUT          
                   
                   
        INSERT INTO [CRTransportList]            
           ([CRTransportListID]            
           ,[CustomerID]            
           ,[CRLegID]            
           ,[TripNUM]            
           ,[LegID]            
           ,[RecordType]            
           ,[TransportID]            
           ,[CRTransportListDescription]            
           ,[PhoneNUM]            
           ,[Rate]            
           ,[IsCompleted]            
           ,[AirportID]            
           ,[LastUpdUID]            
           ,[LastUpdTS]            
           ,[IsDeleted])                            
           SELECT          
           @CRPaxlistTransportListID          
          ,PCT.CustomerID            
          ,@NewCRLegIDPaxTrans          
          ,NULL          
          ,PCT.LegID          
          ,'P'          
          ,PCT.TransportID          
          ,PCT.PreflightTransportName          
          ,PCT.PhoneNum1           
          ,NULL                  
          ,PCT.IsCompleted          
          ,PCT.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCT.IsDeleted          
          FROM #TempUpdatePrefPAXTransportlist PCT          
     WHERE PreflightTransportID = @CurrentUpdatePrefPAXTransportID           
       END          
       DELETE FROM #TempUpdatePrefPAXTransportlist where PreflightTransportID = @CurrentUpdatePrefPAXTransportID          
    END          
    DROP TABLE #TempUpdatePrefPAXTransportlist                  
       ---ends here   
                 
                 
        ---Update crewtransport          
       DECLARE @CurrentUpdatePrefCREWTransportID BIGINT          
    CREATE Table #TempUpdatePrefCREWTransportlist          
     (          
     [PreflightTransportID] [bigint] NOT NULL,          
     [PreflightTransportName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [TransportID] [bigint] NULL,          
     [IsArrivalTransport] [bit] NULL,          
     [IsDepartureTransport] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [CrewPassengerType] [char](1) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Status] [varchar](20) NULL          
      )          
    INSERT INTO #TempUpdatePrefCREWTransportlist           
     SELECT PCT.* FROM PreflightTransportList PCT,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE  PCT.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
         and PCT.IsArrivalTransport=1 and CrewPassengerType='C'          
      ORDER BY PCT.PreflightTransportID ASC          
    WHILE (SELECT COUNT(*) from #TempUpdatePrefCREWTransportlist)>0          
       BEGIN          
                DECLARE @NewCRLegIDCREWTrans BIGINT          
                SET @NewCRLegIDCREWTrans = (SELECT CRLegID FROM CRLeg where CRMainID=@CRMainID and Legnum=@LastPrefLegNum)           
                          
                -- Get current crpassenger id          
       SELECT TOP 1 @CurrentUpdatePrefCREWTransportID = PreflightTransportID FROM #TempUpdatePrefCREWTransportlist          
       DECLARE @ARRCREWTransportID BIGINT          
          SELECT TOP 1 @ARRCREWTransportID = TransportID FROM #TempUpdatePrefCREWTransportlist          
          IF EXISTS (SELECT  transport.TransportID FROM CRTransportList  transport JOIN  CRLeg leg on transport.CRLegID = leg.CRLegID           
       WHERE  transport.TransportID=@ARRCREWTransportID and leg.Legnum=@LastPrefLegNum and leg.CRMainID=@CRMainID and transport.RecordType='C')          
       BEGIN              
        UPDATE crewtransport          
          SET            
         CustomerID = @CustomerID              
         ,TripNUM = PL.TripNUM          
         ,LegID = PL.LegID          
         ,RecordType = 'C'          
         ,TransportID = PT.TransportID          
         ,CRTransportListDescription = PT.PreflightTransportName          
         ,PhoneNUM = PT.PhoneNum1          
         ,IsCompleted = PT.IsCompleted          
         ,AirportID = PT.AirportID          
         ,LastUpdUID = @LastUpdUID          
         ,LastUpdTS = @LastUpdTS            
          FROM  CRTransportList crewtransport          
         JOIN  CRLeg leg on crewtransport.CRLegID = leg.CRLegID and leg.CRMainID=@CRMainID                
         JOIN PreflightLeg PL on leg.LegNUM = PL.LegNUM and PL.TripID=@TripID          
         JOIN PreflightMain PM on  PL.TripID = PM.TripID          
         --JOIN PreflightCrewList PC on PC.LegID=PL.LegID          
         JOIN PreflightTransportList PT on  PT.LegID=PL.LegID and PT.TransportID = crewtransport.TransportID          
         WHERE    PM.TripID=@TripID and leg.CRMainID=@CRMainID and crewtransport.RecordType='C' and          
         PT.PreflightTransportID=@CurrentUpdatePrefCREWTransportID          
          END          
          ELSE          
          BEGIN          
           DECLARE @CRCrewlistTransportListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRCrewlistTransportListID OUTPUT          
                   
                   
        INSERT INTO [CRTransportList]            
           ([CRTransportListID]            
           ,[CustomerID]            
           ,[CRLegID]            
           ,[TripNUM]            
           ,[LegID]            
           ,[RecordType]            
           ,[TransportID]            
           ,[CRTransportListDescription]            
           ,[PhoneNUM]            
           ,[Rate]            
           ,[IsCompleted]            
           ,[AirportID]            
           ,[LastUpdUID]            
           ,[LastUpdTS]            
           ,[IsDeleted])                            
           SELECT          
           @CRCrewlistTransportListID          
          ,PCT.CustomerID            
          ,@NewCRLegIDCREWTrans          
          ,NULL          
          ,PCT.LegID          
          ,'C'          
          ,PCT.TransportID          
          ,PCT.PreflightTransportName          
          ,PCT.PhoneNum1           
          ,NULL                  
          ,PCT.IsCompleted          
          ,PCT.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
         ,PCT.IsDeleted          
          FROM #TempUpdatePrefCREWTransportlist PCT          
                       WHERE PreflightTransportID = @CurrentUpdatePrefCREWTransportID           
          END          
          DELETE FROM #TempUpdatePrefCREWTransportlist where PreflightTransportID = @CurrentUpdatePrefCREWTransportID          
    END           
    DROP TABLE #TempUpdatePrefCREWTransportlist                    
       ---ends here          
                 
    SET @LastPrefLegNum = @LastPrefLegNum + 1          
              
    IF (@LastPrefLegNum > (Select MAX(Legnum) from CRLeg where CRLeg.CRMainID=@CRMainID))        --IF (@LastPrefLegNum > (Select MAX(Legnum) from CRLeg where CRLeg.CRMainID=@CRMainID))        
    BEGIN          
     BREAK          
    END          
              
   END           
      ---ends here          
               
     ---Unmatched Records          
     WHILE (SELECT COUNT(1) FROM PreflightLeg join CRLeg on  PreflightLeg.LegNUM != CRLeg.LegNUM   WHERE PreflightLeg.TripID=@TripID and PreflightLeg.LegNUM = @LastPrefLegNum and PreflightLeg.isDeleted = 0) >0              
   BEGIN            
             
    ---Unmatched Records- Insert into crLeg          
       set @CRLegnum = (Select Max(Legnum + 1) from CRLeg where CRLeg.CRMainID=@CRMainID)          
       IF (@CRLegnum = @LastPrefLegNum)          
       BEGIN          
                 
       SET @NewLegID = 0          
    EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @NewLegID OUTPUT          
                     
     ----Insert new leg          
        INSERT INTO [CRLeg]            
       ([CRLegID]            
       ,[CustomerID]            
       ,[CRMainID]            
       ,[TripNUM]            
       ,[LegNUM]            
       ,[DAirportID]            
       ,[AAirportID]            
       ,[DepartureCity]            
       ,[ArrivalCity]            
       ,[DepartureDTTMLocal]            
       ,[ArrivalDTTMLocal]            
       ,[PassengerRequestorID]            
       ,[DepartmentID]            
       ,[AuthorizationID]            
       ,[FlightCategoryID]            
       ,[IsPrivate]            
       ,[Distance]            
       ,[ElapseTM]            
       ,[PassengerTotal]            
       ,[PowerSetting]            
       ,[TakeoffBIAS]            
       ,[LandingBIAS]            
       ,[TrueAirSpeed]            
       ,[WindsBoeingTable]            
       ,[IsDepartureConfirmed]            
       ,[IsArrivalConfirmation]            
       ,[IsApproxTM]     
       ,[IsScheduledServices]            
       ,[DepartureGreenwichDTTM]            
       ,[ArrivalGreenwichDTTM]            
       ,[HomeDepartureDTTM]            
       ,[HomeArrivalDTTM]            
       ,[NegotiateTime]            
       ,[LogBreak]            
       ,[FlightPurpose]            
       ,[SeatTotal]            
       ,[ReservationTotal]            
       ,[ReservationAvailable]            
       ,[WaitNUM]            
       ,[Duty_Type]            
       ,[DutyHours]            
       ,[RestHours]            
       ,[FlightHours]            
       ,[Notes]            
       ,[FuelLoad]            
       ,[OutboundInstruction]            
       ,[DutyType]            
       ,[IsCrewDiscount]            
       ,[CrewCurrency]            
       ,[IsDutyEnd]            
       ,[FedAviationRegNUM]            
       ,[LegID]            
       ,[WindReliability]            
       ,[CROverRide]            
       ,[CheckGroup]            
       ,[AirportAlertCD]            
       ,[AdditionalCrew]            
       ,[PilotInCommand]            
       ,[SecondInCommand]            
       ,[NextLocalDTTM]            
       ,[NextGMTDTTM]            
       ,[NextHomeDTTM]            
       ,[CrewNotes]            
       ,[LastUpdUID]            
       ,[LastUpdTS]            
       ,[IsDeleted]            
       ,[FBOID]            
       ,[ClientID])           
     SELECT          
       @NewLegID              
      ,C.CustomerID              
      ,C.CRMainID             
      ,T.TripNUM          
      ,IP.[LegNUM]              
      ,IP.DepartICAOID            
      ,IP.ArriveICAOID              
      ,NULL           
         ,NULL            
         ,IP.DepartureDTTMLocal            
         ,IP.ArrivalDTTMLocal            
         ,IP.PassengerRequestorID            
         ,IP.DepartmentID            
         ,IP.AuthorizationID            
         ,IP.FlightCategoryID            
         ,IP.IsPrivate            
         ,IP.Distance            
         ,IP.ElapseTM            
         ,IP.PassengerTotal            
         ,IP.PowerSetting            
         ,IP.TakeoffBIAS            
         ,IP.LandingBIAS            
         ,IP.TrueAirSpeed            
         ,IP.WindsBoeingTable            
         ,IP.IsDepartureConfirmed            
         ,IP.IsArrivalConfirmation            
         ,IP.IsApproxTM            
         ,IP.IsScheduledServices            
         ,IP.DepartureGreenwichDTTM            
         ,IP.ArrivalGreenwichDTTM            
         ,IP.HomeDepartureDTTM            
         ,IP.HomeArrivalDTTM            
         ,NULL            
         ,IP.LogBreak            
        ,IP.FlightPurpose            
         ,IP.SeatTotal            
         ,IP.ReservationTotal            
         ,IP.ReservationAvailable            
         ,IP.WaitNUM            
         ,IP.DutyTYPE            
         ,IP.DutyHours            
         ,IP.RestHours            
         ,IP.FlightHours            
         ,IP.Notes            
         ,IP.FuelLoad            
         ,IP.OutbountInstruction            
         ,NULL          
         ,IP.IsCrewDiscount            
         ,NULL          
         ,NULL            
         ,IP.FedAviationRegNUM            
         ,IP.LegID            
         ,IP.WindReliability            
         ,NULL           
         ,IP.CheckGroup            
         ,NULL           
         ,IP.AdditionalCrew            
         ,IP.PilotInCommand            
         ,IP.SecondInCommand            
         ,IP.NextLocalDTTM            
         ,IP.NextGMTDTTM            
         ,IP.NextHomeDTTM            
         ,IP.CrewNotes            
         ,@LastUpdUID            
         ,@LastUpdTS            
         ,C.IsDeleted            
         ,IP.FBOID            
         ,IP.ClientID            
     FROM PreflightLeg IP, PreflightMain T ,CRMain C           
     WHERE IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum           
     ----ends here          
               
      ---Insert new Passenger          
     DECLARE @CurrentCRPassengerID BIGINT          
     CREATE Table #TempPrefPassenger          
     (          
        [PreflightPassengerListID] [bigint] NOT NULL,          
     [LegID] [bigint] NULL,          
     [CustomerID] [bigint] NULL,          
     [PassengerID] [bigint] NULL,          
     [PassportID] [bigint] NULL,          
     [VisaID] [bigint] NULL,          
     [HotelName] [varchar](60) NULL,          
     [PassengerFirstName] [varchar](63) NULL,          
     [PassengerMiddleName] [varchar](60) NULL,          
     [PassengerLastName] [varchar](60) NULL,          
     [FlightPurposeID] [bigint] NULL,          
     [PassportNUM] [varchar](25) NULL,          
     [Billing] [varchar](25) NULL,          
     [IsNonPassenger] [bit] NULL,          
     [IsBlocked] [bit] NULL,          
     [OrderNUM] [int] NULL,          
     [ReservationNUM] [varchar](6) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [WaitList] [char](1) NULL,          
     [AssociatedPassenger] [varchar](5) NULL,          
     [TripStatus] [char](1) NULL,          
     [TSADTTM] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [NoofRooms] [numeric](2, 0) NULL,          
     [ClubCard] [varchar](100) NULL,          
     [IsRateCap] [bit] NULL,          
     [MaxCapAmount] [numeric](17, 2) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL          
     )          
     INSERT INTO #TempPrefPassenger           
     SELECT PPL.* FROM PreflightPassengerList PPL,  PreflightLeg IP, PreflightMain T ,CRMain C           
       WHERE PPL.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
       ORDER BY PPL.PreflightPassengerListID ASC               
     WHILE (SELECT COUNT(*) from #TempPrefPassenger) > 0              
     BEGIN          
               
             -- Get current crpassenger id          
       SELECT TOP 1 @CurrentCRPassengerID = PreflightPassengerListID FROM #TempPrefPassenger          
                 
       DECLARE @CRPassengerID BIGINT          
       EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPassengerID OUTPUT          
                 
                        
       INSERT INTO CRPassenger          
        (CRPassengerID          
        ,CustomerID          
        ,CRLegID          
        ,TripNUM          
        ,LegID          
        ,OrderNUM          
        ,PassengerRequestorID          
        ,PassengerName          
        ,FlightPurposeID          
        ,PassportID          
        ,Billing          
        ,IsNonPassenger          
        ,IsBlocked          
        ,ReservationNUM          
        ,WaitList          
,LastUpdUID          
        ,LastUpdTS          
        ,IsDeleted)          
        SELECT          
         @CRPassengerID          
        ,PPL.CustomerID            
        ,@NewLegID          
        ,NULL          
        ,PPL.LegID          
        ,PPL.OrderNUM          
        ,PPL.PassengerID          
        ,PPL.PassengerFirstName + PPL.PassengerLastName + PPL.PassengerMiddleName          
        ,PPL.FlightPurposeID          
        ,PPL.PassportID          
        ,PPL.Billing          
        ,PPL.IsNonPassenger          
        ,PPL.IsBlocked          
        ,PPL.ReservationNUM          
        ,PPL.WaitList          
        ,@LastUpdUID          
        ,@LastUpdTS          
        ,PPL.IsDeleted          
                  
       FROM #TempPrefPassenger PPL          
       WHERE PreflightPassengerListID = @CurrentCRPassengerID           
                 
       DELETE FROM #TempPrefPassenger where PreflightPassengerListID = @CurrentCRPassengerID           
                 
 END               
     DROP TABLE #TempPrefPassenger                  
     --ends here           
               
               
     ---Insert new fbo          
     DECLARE @CurrentCRFBOID BIGINT          
     CREATE Table #TempPrefFBOlist          
     (          
     [PreflightFBOID] [bigint] NOT NULL,          
     [PreflightFBOName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [FBOID] [bigint] NULL,          
     [IsArrivalFBO] [bit] NULL,          
     [IsDepartureFBO] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [NoUpdated] [bit] NULL,          
     [FBOArrangedBy] [bit] NULL,          
     [IsCompleted] [bit] NULL,          
     [FBOInformation] [varchar](1000) NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Status] [varchar](20) NULL          
     --[IsRequired] [bit] NULL,          
     --[IsNotRequired] [bit] NULL,          
     --[IsInProgress] [bit] NULL,          
     --[IsChange] [bit] NULL,          
     --[IsCancelled] [bit] NULL          
      )          
     INSERT INTO #TempPrefFBOlist           
     SELECT PFO.* FROM PreflightFBOList PFO,  PreflightLeg IP, PreflightMain T ,CRMain C           
           WHERE PFO.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
           ORDER BY PFO.PreflightFBOID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefFBOlist) > 0              
     BEGIN          
      -- Get current crpassenger id          
       SELECT TOP 1 @CurrentCRFBOID = PreflightFBOID FROM #TempPrefFBOlist          
      DECLARE @CRFBOListID BIGINT          
      EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRFBOListID OUTPUT          
                     
          INSERT INTO CRFBOList          
           ([CRFBOListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[FBOID]          
           ,[CRFBOListDescription]          
           ,[PhoneNUM]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
            @CRFBOListID          
           ,PFO.CustomerID            
           ,@NewLegID          
           ,NULL          
           ,PFO.LegID          
           ,CASE WHEN PFO.IsDepartureFBO = 1 THEN 'D' ELSE 'A' END          
           ,PFO.FBOID          
           ,PFO.PreflightFBOName          
           ,PFO.PhoneNum1          
           ,PFO.IsCompleted          
           ,PFO.AirportID          
           ,@LastUpdUID          
           ,@LastUpdTS          
           ,PFO.IsDeleted          
           FROM #TempPrefFBOlist PFO          
                       WHERE PreflightFBOID = @CurrentCRFBOID           
                   DELETE FROM  #TempPrefFBOlist where PreflightFBOID = @CurrentCRFBOID             
                               
     END          
     DROP TABLE #TempPrefFBOlist               
     ---ends here          
               
               
     ---Insert new catering          
     DECLARE @CurrentPrefCaterID BIGINT          
     CREATE Table #TempPrefCaterlist          
     (          
        [PreflightCateringID] [bigint] NOT NULL,          
     [LegID] [bigint] NULL,          
     [CustomerID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [ArriveDepart] [char](1) NULL,          
     [CateringID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Cost] [numeric](17, 2) NULL,          
     [ContactName] [varchar](50) NULL,          
     [ContactPhone] [varchar](20) NULL,          
     [ContactFax] [varchar](20) NULL,          
     [CateringConfirmation] [varchar](1500) NULL,          
     [CateringComments] [varchar](1500) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [IsUWAArranger] [bit] NULL,          
     [CateringContactName] [varchar](40) NULL,          
     [Status] [varchar](20) NULL          
     )          
     INSERT INTO #TempPrefCaterlist           
     SELECT PCD.* FROM PreflightCateringDetail PCD,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE PCD.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
      ORDER BY PCD.PreflightCateringID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefCaterlist) > 0              
     BEGIN          
        -- Get current crpassenger id          
            SELECT TOP 1 @CurrentPrefCaterID = PreflightCateringID FROM #TempPrefCaterlist            
                    
         DECLARE @CRCateringListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRCateringListID OUTPUT          
                   
                   
         INSERT INTO [CRCateringList]          
           ([CRCateringListID]          
           ,[CustomerID]          
           ,[CRLegID]          
           ,[TripNUM]          
           ,[LegID]          
           ,[RecordType]          
           ,[CateringID]          
           ,[CRCateringListDescription]          
           ,[PhoneNUM]          
           ,[Rate]          
           ,[IsCompleted]          
           ,[AirportID]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted])          
           SELECT          
           @CRCateringListID          
          ,PCD.CustomerID            
          ,@NewLegID          
          ,NULL          
          ,PCD.LegID          
          ,CASE WHEN PCD.ArriveDepart = 'D' THEN 'D' ELSE 'A' END          
          ,PCD.CateringID          
          ,PCD.ContactName          
          ,PCD.ContactPhone          
          ,NULL          
          ,PCD.IsCompleted          
          ,PCD.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCD.IsDeleted          
          FROM #TempPrefCaterlist PCD          
                       WHERE PreflightCateringID = @CurrentPrefCaterID           
                    
         DELETE FROM #TempPrefCaterlist where PreflightCateringID=@CurrentPrefCaterID          
               
     END          
     DROP TABLE #TempPrefCaterlist            
     ---ends here           
               
               
     ---Insert new Crew Hotel          
     DECLARE @CurrentPrefCrewHotelID BIGINT          
     CREATE Table #TempPrefCrewHotellist          
     (          
        [PreflightHotelListID] [bigint] NOT NULL,          
     [PreflightHotelName] [varchar](60) NULL,          
     [PreflightCrewListID] [bigint] NULL,      
      [LegID] [bigint] NULL,        
     [HotelID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [RoomType] [varchar](60) NULL,          
     [RoomRent] [numeric](6, 2) NULL,          
     [RoomConfirm] [varchar](max) NULL,          
     [RoomDescription] [varchar](max) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,               [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
 [HotelArrangedBy] [varchar](10) NULL,          
     [Rate] [numeric](17, 2) NULL,          
     [DateIn] [date] NULL,          
     [DateOut] [date] NULL,          
     [IsSmoking] [bit] NULL,          
     [RoomTypeID] [bigint] NULL,          
     [NoofBeds] [numeric](3, 0) NULL,          
     [IsAllCrew] [bit] NULL,          
     [IsCompleted] [bit] NULL,          
     [IsEarlyCheckIn] [bit] NULL,          
     [EarlyCheckInOption] [char](1) NULL,          
[SpecialInstructions] [varchar](100) NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Address1] [varchar](40) NULL,          
     [Address2] [varchar](40) NULL,          
     [Address3] [varchar](40) NULL,          
     [MetroID] [bigint] NULL,          
     [City] [varchar](30) NULL,          
     [StateProvince] [varchar](25) NULL,          
     [CountryID] [bigint] NULL,          
     [PostalCode] [varchar](15) NULL,          
     [Status] [varchar](20) NULL          
     )          
     INSERT INTO #TempPrefCrewHotellist           
     SELECT PCH.* FROM PreflightHotelList PCH,   PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE PCH.LegID=IP.LegID and   IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
      ORDER BY PCH.PreflightHotelListID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefCrewHotellist) > 0              
      BEGIN          
        -- Get current crpassenger id          
            SELECT TOP 1 @CurrentPrefCrewHotelID = PreflightCrewHotelListID FROM #TempPrefCrewHotellist            
                    
         DECLARE @CRCrewHotelListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRCrewHotelListID OUTPUT          
                   
                   
         INSERT INTO CRHotelList          
         ([CRHotelListID]          
         ,[CustomerID]          
         ,[CRLegID]          
         ,[TripNUM]          
         ,[LegID]          
         ,[RecordType]          
         ,[HotelID]          
         ,[CRHotelListDescription]          
         ,[PhoneNUM]          
         ,[IsCompleted]          
         ,[AirportID]          
         ,[LastUpdUID]          
         ,[LastUpdTS]          
         ,[IsDeleted])                          
           SELECT          
           @CRCrewHotelListID          
          ,PCH.CustomerID            
          ,@NewLegID          
          ,NULL          
          ,NULL          
          ,'C'          
          ,PCH.HotelID          
          ,PCH.PreflightHotelName          
          ,PCH.PhoneNum1                   
          ,PCH.IsCompleted          
          ,PCH.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCH.IsDeleted          
          FROM #TempPrefCrewHotellist PCH          
                       WHERE PreflightCrewHotelListID = @CurrentPrefCrewHotelID           
                    
         DELETE FROM #TempPrefCrewHotellist where PreflightCrewHotelListID = @CurrentPrefCrewHotelID          
               
     END           
     DROP TABLE #TempPrefCrewHotellist           
     ---ends here           
               
     ---Insert new Passenger hotel          
     DECLARE @CurrentPrefPaxHotelID BIGINT          
     CREATE Table #TempPrefPaxHotellist          
     (          
        [PreflightPassengerHotelListID] [bigint] NOT NULL,          
     [PreflightHotelName] [varchar](60) NULL,          
     [PreflightPassengerListID] [bigint] NULL,          
     [HotelID] [bigint] NULL,          
     [AirportID] [bigint] NULL,          
     [RoomType] [varchar](60) NULL,          
     [RoomRent] [numeric](6, 2) NULL,          
     [RoomConfirm] [varchar](max) NULL,          
     [RoomDescription] [varchar](max) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,          
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [HotelArrangedBy] [varchar](10) NULL,          
     [Rate] [numeric](17, 2) NULL,          
     [DateIn] [date] NULL,          
     [DateOut] [date] NULL,          
     [IsSmoking] [bit] NULL,          
     [RoomTypeID] [bigint] NULL,          
     [NoofBeds] [numeric](3, 0) NULL,          
     [IsAllCrew] [bit] NULL,         
     [IsCompleted] [bit] NULL,          
     [IsEarlyCheckIn] [bit] NULL,          
     [EarlyCheckInOption] [char](1) NULL,          
     [SpecialInstructions] [varchar](100) NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [Address1] [varchar](40) NULL,          
     [Address2] [varchar](40) NULL,          
     [Address3] [varchar](40) NULL,          
     [MetroID] [bigint] NULL,          
     [City] [varchar](30) NULL,          
     [StateProvince] [varchar](25) NULL,          
     [CountryID] [bigint] NULL,          
     [PostalCode] [varchar](15) NULL,          
     [Status] [varchar](20) NULL          
     )          
     INSERT INTO #TempPrefPaxHotellist           
     SELECT PCH.* FROM PreflightPassengerHotelList PCH, PreflightPassengerList PCL,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE PCH.PreflightPassengerListID=PCL.PreflightPassengerListID and  PCL.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
      ORDER BY PCH.PreflightPassengerHotelListID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefPaxHotellist) > 0              
      BEGIN          
        -- Get current crpassenger id          
            SELECT TOP 1 @CurrentPrefPaxHotelID = PreflightPassengerHotelListID FROM #TempPrefPaxHotellist            
                    
         DECLARE @CRPaxHotelListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPaxHotelListID OUTPUT          
                   
                   
         INSERT INTO CRHotelList          
         ([CRHotelListID]          
         ,[CustomerID]          
         ,[CRLegID]          
         ,[TripNUM]          
         ,[LegID]          
         ,[RecordType]          
         ,[HotelID]          
         ,[CRHotelListDescription]          
         ,[PhoneNUM]          
         ,[IsCompleted]          
         ,[AirportID]          
         ,[LastUpdUID]          
         ,[LastUpdTS]          
         ,[IsDeleted])                          
           SELECT          
           @CRPaxHotelListID          
          ,PCH.CustomerID            
          ,@NewLegID          
          ,NULL          
          ,NULL          
          ,'P'          
          ,PCH.HotelID          
          ,PCH.PreflightHotelName          
          ,PCH.PhoneNum1                   
          ,PCH.IsCompleted          
          ,PCH.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCH.IsDeleted          
          FROM #TempPrefPaxHotellist PCH          
                       WHERE PreflightPassengerHotelListID = @CurrentPrefPaxHotelID           
                    
         DELETE FROM  #TempPrefPaxHotellist where  PreflightPassengerHotelListID=@CurrentPrefPaxHotelID          
               
     END          
     DROP TABLE #TempPrefPaxHotellist            
     ---ends here           
               
               
     ---Insert new Transport          
     DECLARE @CurrentPrefTransportID BIGINT          
     CREATE Table #TempPrefTransportlist          
     (          
     [PreflightTransportID] [bigint] NOT NULL,          
     [PreflightTransportName] [varchar](60) NULL,          
     [LegID] [bigint] NULL,          
     [TransportID] [bigint] NULL,          
     [IsArrivalTransport] [bit] NULL,          
     [IsDepartureTransport] [bit] NULL,          
     [AirportID] [bigint] NULL,          
     [CrewPassengerType] [char](1) NULL,          
     [Street] [varchar](50) NULL,          
     [CityName] [varchar](20) NULL,          
     [StateName] [varchar](20) NULL,          
     [PostalZipCD] [varchar](20) NULL,          
     [PhoneNum1] [varchar](25) NULL,          
     [PhoneNum2] [varchar](25) NULL,          
     [PhoneNum3] [varchar](25) NULL,          
     [PhoneNum4] [varchar](25) NULL,          
     [FaxNUM] [varchar](25) NULL,      
     [LastUpdUID] [varchar](30) NULL,          
     [LastUpdTS] [datetime] NULL,          
     [IsDeleted] [bit] NOT NULL,          
     [ConfirmationStatus] [varchar](max) NULL,          
     [Comments] [varchar](max) NULL,          
     [CustomerID] [bigint] NULL,          
     [IsCompleted] [bit] NULL,          
     [Status] [varchar](20) NULL          
      )          
     INSERT INTO #TempPrefTransportlist           
     SELECT PCT.* FROM PreflightTransportList PCT,  PreflightLeg IP, PreflightMain T ,CRMain C           
         WHERE  PCT.LegID=IP.LegID and  IP.TripID = T.TripID and T.TripID = C.TripID and T.TripID=@TripID and IP.Legnum=@LastPrefLegNum          
         and PCT.IsArrivalTransport=1          
      ORDER BY PCT.PreflightTransportID ASC          
     WHILE (SELECT COUNT(*) from #TempPrefTransportlist) > 0              
      BEGIN          
        -- Get current crpassenger id          
            SELECT TOP 1 @CurrentPrefTransportID = PreflightTransportID FROM #TempPrefTransportlist            
                    
         DECLARE @CRPaxTransportListID BIGINT          
         EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRPaxTransportListID OUTPUT          
                   
                   
        INSERT INTO [CRTransportList]            
           ([CRTransportListID]            
           ,[CustomerID]            
           ,[CRLegID]            
           ,[TripNUM]            
           ,[LegID]            
           ,[RecordType]            
           ,[TransportID]            
           ,[CRTransportListDescription]            
           ,[PhoneNUM]            
           ,[Rate]            
           ,[IsCompleted]            
           ,[AirportID]            
           ,[LastUpdUID]            
           ,[LastUpdTS]            
           ,[IsDeleted])                            
           SELECT          
           @CRPaxTransportListID          
          ,PCT.CustomerID            
          ,@NewLegID            ,NULL          
          ,PCT.LegID          
          ,CASE WHEN CrewPassengerType='C' THEN 'C' ELSE 'P' END          
          ,PCT.TransportID          
          ,PCT.PreflightTransportName          
          ,PCT.PhoneNum1           
          ,NULL                  
          ,PCT.IsCompleted          
          ,PCT.AirportID          
          ,@LastUpdUID          
          ,@LastUpdTS          
          ,PCT.IsDeleted          
          FROM #TempPrefTransportlist PCT          
                       WHERE PreflightTransportID = @CurrentPrefTransportID           
                    
        DELETE FROM  #TempPrefTransportlist where  PreflightTransportID = @CurrentPrefTransportID          
               
     END           
     DROP TABLE #TempPrefTransportlist            
     ---ends here           
         
                
       END          
       ---ends here           
                 
             
   SET @LastPrefLegNum = @LastPrefLegNum + 1          
   END          
     ---ends here           
              
  END          
  --Ends Here          
            
  DECLARE @HistoryDescription nvarchar(MAX)          
     DECLARE @HistoryDescr nvarchar(MAX)          
     DECLARE @Historydes nvarchar(MAX)          
     DECLARE @IsDeleted bit          
     DECLARE @CRHistoryID bigint          
     DECLARE @RevisionNum int          
     DECLARE @CRRevisionNum int          
               
     SET @RevisionNum =(SELECT ISNULL(Max(RevisionNumber),1) from PreflightTripHistory where TripID = @TripID and CustomerID=@CustomerID)          
     SET @CRRevisionNum =(SELECT ISNULL(Max(RevisionNumber),1) from CRHistory where CRMainID = @CRMainID and CustomerID=@CustomerID)          
     SET @HistoryDescription = (SELECT top 1 LogisticsHistory from CRHistory where CRMainID = @CRMainID and CustomerID=@CustomerID and RevisionNumber=@CRRevisionNum)                    
     SET @HistoryDescr = (SELECT top 1 HistoryDescription from PreflightTripHistory where TripID = @TripID and CustomerID=@CustomerID and RevisionNumber=@RevisionNum)          
               
               
     SET @Historydes = isnull(CAST(@HistoryDescription as nvarchar(MAX)),'') + isnull(CAST(@HistoryDescr as nvarchar(MAX)),'')          
               
                    
     SET @IsDeleted = (SELECT top 1 IsDeleted from CRHistory where CRMainID = @CRMainID and CustomerID=@CustomerID and RevisionNumber=@CRRevisionNum)          
     SET @CRHistoryID = (SELECT top 1 CRHistoryID from CRHistory where CRMainID = @CRMainID and CustomerID=@CustomerID and RevisionNumber=@CRRevisionNum)          
               
     EXECUTE spFlightPak_CorporateRequest_AddCRHistory @CRMainID,@CustomerID,@Historydes,@LastUpdUID,@LastUpdTS,@IsDeleted          
            
            
  SELECT @CRMainID as CRMainID          
END 