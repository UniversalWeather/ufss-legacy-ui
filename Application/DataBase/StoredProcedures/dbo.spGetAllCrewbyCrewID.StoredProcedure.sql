SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewbyCrewID]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spGetAllCrewbyCrewID]
GO

CREATE procedure [dbo].[spGetAllCrewbyCrewID](@CrewID bigint)          
-- =============================================          
-- Author: Karthikeyan.S          
-- Create date: 08/05/2012          
-- Description: Get all the Crew List by CrewID          
-- =============================================      
AS            
BEGIN    
        
SELECT   Crew.CustomerID,           
Crew.CrewCD,          
Crew.CrewID ,           
rtrim(isnull(Crew.LastName,'')) as 'LastName',           
rtrim(isnull(Crew.FirstName,'')) as 'FirstName',          
 rtrim(isnull(Crew.MiddleInitial,'')) as 'MiddleInitial'          
,isnull(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'          
,Crew.IsFixedWing          
,Crew.IsRotaryWing          
,Crew.Addr1          
,Crew.Addr2          
,Crew.CityName          
,Crew.StateName          
,Crew.PostalZipCD          
,Crew.CountryID          
,Crew.PhoneNum          
,Crew.SSN          
,isnull(Crew.IsStatus,0) as 'IsStatus'           
,Crew.HomebaseID          
,Crew.BirthDT          
,Crew.ClientID          
,Crew.CellPhoneNum          
,Crew.PagerNum          
,Crew.CrewTypeCD          
,Crew.LastUpdUID          
,Crew.LastUpdTS          
,Crew.PilotLicense1          
,Crew.Notes          
,isnull(Crew.CheckList,0) as 'CheckList'          
,Crew.PilotLicense2          
,Crew.LicenseCountry1          
,Crew.LicenseCountry2          
,Crew.IsNoCalendarDisplay          
,Crew.CrewTypeDescription          
,Crew.Gender          
,Crew.EmailAddress          
,Crew.FaxNum          
,Crew.HireDT          
,Crew.TerminationDT          
,Crew.LicenseType1          
,Crew.LicenseType2          
,Crew.DepartmentID          
,Crew.IsCheckListFrequency          
,Crew.IsNextMonth          
,Crew.CrewScanDocuments          
,Crew.Citizenship          
,Crew.Notes2          
,Crew.IsDepartmentAuthorization          
,Crew.IsRequestPhoneNum,           
Crew.IsCancelDescription          
, Crew.IsTripN          
, Crew.IsTripA          
, Crew.IsAirport          
, Crew.IsCheckList          
, Crew.IsAccountNum          
, Crew.IsStatusT          
, Crew.IsDayLightSavingTimeARR          
, Crew.IsDayLightSavingTimeDEPART          
, Crew.IsHomeArrivalTM          
, Crew.IsHomeDEPARTTM          
, Crew.IsEndDuty          
, Crew.IsOverride          
, Crew.IsCrewRules          
, Crew.IsFAR          
, Crew.IsDuty          
, Crew.IsFlightHours          
, Crew.IsRestHrs          
, Crew.Comments          
, Crew.IsDepartureFBO          
, Crew.IsArrivalFBO          
, Crew.IsCrewHotel          
, Crew.IsCrewDepartTRANS          
, Crew.IsCrewArrivalTRANS          
, Crew.IsCrewHotelN          
, Crew.IsPassHotel          
, Crew.IsPassDepartTRANS          
, Crew.IsPassArrivalHotel          
, Crew.IsPassDetails          
, Crew.IsPassPhoneNum          
, Crew.IsDepartCatering          
, Crew.IsArrivalCatering          
, Crew.IsArrivalDepartTime          
, Crew.IcaoID          
, Crew.IsCrew          
, Crew.IsPassenger          
, Crew.IsLegN          
, Crew.IsOutboundINST          
, Crew.IsCheckList1          
, Crew.IsFBO          
, Crew.IsHotel          
, Crew.IsTransportation          
, Crew.IsCatering          
, Crew.Label1          
, Crew.Label2          
, Crew.Label3          
, Crew.Label4          
, Crew.Label5          
, Crew.Label6          
, Crew.Label7          
, Crew.IsCrewHotelConfirm          
, Crew.IsCrewHotelComments          
, Crew.IsCrewDepartTRANSConfirm          
, Crew.IsCrewDepartTRANSComments          
, Crew.IsCrewArrivalConfirm          
, Crew.IsCrewArrivalComments          
, Crew.IsCrewNotes          
, Crew.IsPassHotelConfirm          
, Crew.IsPassHotelComments          
, Crew.IsPassDepartTRANSConfirm          
, Crew.IsPassDepartTRANSComments          
, Crew.IsPassArrivalConfirm          
, Crew.IsPassArrivalComments          
, Crew.IsPassNotes          
, Crew.IsOutboundComments          
, Crew.IsDepartAirportNotes          
, Crew.IsArrivalAirportNoes          
, Crew.IsDepartAirportAlerts          
, Crew.IsArrivalAirportAlerts          
, Crew.IsFBODepartConfirm          
, Crew.IsFBOArrivalConfirm          
, Crew.IsFBODepartComment          
, Crew.IsFBOArrivalComment          
, Crew.IsDepartCateringConfirm          
, Crew.IsDepartCateringComment          
, Crew.IsArrivalCateringConfirm         
, Crew.IsArrivalCateringComment          
, Crew.IsTripAlerts          
, Crew.IsTripNotes          
, Crew.IsRunway          
, Crew.IsPassengerPhoneNum          
, isnull(Crew.BlackBerryTM,'') as 'BlackBerryTM'          
, Crew.IsAssociatedCrew          
, Crew.CityOfBirth          
, Crew.CountryOfBirth          
, Crew.License1CityCountry          
, Crew.License2CityCountry          
, Crew.License1ExpiryDT          
, Crew.License2ExpiryDT          
, Crew.APISException          
, Crew.APISSubmit          
, Crew.APISUserID          
, Crew.IsAircraft          
, Crew.StateofBirth          
, Crew.IsDeleted      
,Crew.BusinessPhone       
,Crew.BusinessFax       
,Crew.CellPhoneNum2      
,Crew.OtherPhone       
,Crew.PersonalEmail      
,Crew.OtherEmail       
,Crew.Addr3  
,Crew.Birthday  
,Crew.Anniversaries  
,Crew.EmergencyContacts
,Crew.CreditcardInfo
,Crew.CateringPreferences
,Crew.HotelPreferences       
,Crew.INSANumber
,Crew.Category
,GreenCard.CountryCD as GreenCardCountryCD
,Crew.CardExpires
,Crew.ResidentSinceYear
,Nationality.CountryName as NationalityName          
,Nationality.CountryCD as NationalityCD          
,Client.ClientCD as ClientCD          
,Client.ClientDescription as ClientDescription          
,Department.DepartmentCD as DepartmentCD          
,Department.DepartmentName as DepartmentName          
,CountryOfResidence.CountryName as ResidenceCountryName          
,CountryOfResidence.CountryCD as ResidenceCountryCD          
,Citizenship.CountryName as CitizenshipName          
,Citizenship.CountryCD as CitizenshipCD   
,Crew.IsTripEmail         
--,LicCountry.CountryName as LicCountryName          
--,LicCountry.CountryCD as LicCountryCD          
--,AddLicCountry.CountryName as AddLicCountryName          
--,AddLicCountry.CountryCD as AddLicCountryCD          
,HomeBase.IcaoID as HomeBaseCD          
FROM Crew as Crew with(nolock)           
          
LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Crew.CountryOfBirth          
LEFT OUTER JOIN Country as Citizenship on Citizenship.CountryID = Crew.Citizenship  
LEFT OUTER JOIN Country as GreenCard on GreenCard.CountryID = Crew.GreenCardCountryID          
--LEFT OUTER JOIN Country as LicCountry on LicCountry.CountryID = Crew.License1CityCountry          
--LEFT OUTER JOIN Country as AddLicCountry on AddLicCountry.CountryID = Crew.License2CityCountry          
LEFT OUTER JOIN Country as CountryOfResidence on CountryOfResidence.CountryID = Crew.CountryID          
LEFT OUTER JOIN Client as Client on Client.ClientID = Crew.ClientID          
LEFT OUTER JOIN Department as Department on Department.DepartmentID = Crew.DepartmentID          
LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Crew.HomebaseID          
WHERE Crew.CrewID = @CrewID      
END
GO