

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetHistory]    Script Date: 02/26/2013 18:12:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_GetHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_GetHistory]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetHistory]    Script Date: 02/26/2013 18:12:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_GetHistory]
@CUSTOMERID BIGINT,
@MAINID BIGINT
AS              
BEGIN            
SET NOCOUNT ON;  
SELECT CQHistoryID,CQMainID,CUSTOMERID,LogisticsHistory,LASTUPDUID,LASTUPDTS,IsDeleted
FROM CQHistory
WHERE CUSTOMERID=@CUSTOMERID AND CQMainID=@MAINID
ORDER BY LASTUPDTS DESC
END       

GO


