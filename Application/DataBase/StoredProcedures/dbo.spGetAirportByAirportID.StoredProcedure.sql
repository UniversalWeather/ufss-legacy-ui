/****** Object:  StoredProcedure [dbo].[spGetAllAirport]    Script Date: 01/17/2013 15:48:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportbyAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportbyAirportID]
GO

CREATE PROCEDURE [dbo].[spGetAirportbyAirportID](@CustomerID BIGINT,@AirportID BIGINT)    
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 17/1/2012    
-- Description: Get the AIRPORT information    
-- =============================================    
SET NOCOUNT ON    
   
SELECT  
   A.[AirportID]  
  ,A.[IcaoID]  
  ,A.[CustomerID]  
  ,[CityName]  
  ,A.[StateName]  
  ,A.[CountryName]  
  ,A.[CountryID]  
  ,[AirportName]  
  ,[LatitudeDegree]  
  ,[LatitudeMinutes]  
  ,[LatitudeNorthSouth]  
  ,[LongitudeDegrees]  
  ,[LongitudeMinutes]  
  ,[LongitudeEastWest]  
  ,[MagneticVariance]  
  ,[VarianceEastWest]  
  ,[Elevation]  
  ,[LongestRunway]  
  ,[IsEntryPort]  
  ,[WindZone]  
  ,[IsUSTax]  
  ,[IsRural]  
  ,[IsAirport]  
  ,[IsHeliport]  
  ,[AirportInfo]  
  ,[OffsetToGMT]  
  ,[IsDayLightSaving]  
  ,[DayLightSavingStartDT]  
  ,[DaylLightSavingStartTM]  
  ,[DayLightSavingEndDT]  
  ,[DayLightSavingEndTM]  
  ,[TowerFreq]  
  ,[ATISFreq]  
  ,[ARINCFreq]  
  ,[GroundFreq]  
  ,[ApproachFreq]  
  ,[DepartFreq]  
  ,[Label1Freq]  
  ,[Freq1]  
  ,[Label2Freq]  
  ,[Freq2]  
  ,[TakeoffBIAS]  
  ,[LandingBIAS]  
  ,[Alerts]  
  ,[GeneralNotes]  
  ,[IsFlightPakFlag]  
  ,[FBOCnt]  
  ,[HotelCnt]  
  ,[TransportationCnt]  
  ,[CateringCnt]  
  ,[FlightPakUPD]  
  ,A.[MetroID]  
  ,A.[DSTRegionID]  
  ,[PPR]  
  ,[Iata]  
  ,[AlternateDestination1]  
  ,[AlternateDestination2]  
  ,[AlternateDestination3]  
  ,A.[LastUpdUID]  
  ,A.[LastUpdTS]  
  ,A.[IsInActive]  
  ,[RecordType]  
  ,[NewICAO]  
  ,[PreviousICAO]  
  ,A.[UpdateDT]  
  ,[IsWorldClock]  
  ,[WidthRunway]  
  ,[FssPhone]  
  ,[LengthWidthRunway]  
  ,[WidthLengthRunway]  
  ,[CustomLocation]  
  ,[CustomPhoneNum]  
  ,[ImmigrationPhoneNum]  
  ,[AgPhoneNum]  
  ,[IsCustomAvailable]  
  ,[PortEntryType]  
  ,[Unicom]  
  ,[Clearence1DEL]  
  ,[Clearence2DEL]  
  ,[AtisPhoneNum]  
  ,[ASOS]  
  ,[AsosPhoneNum]  
  ,[AWOS]  
  ,[AwosPhoneNum]  
  ,[AwosType]  
  ,[IsSlots]  
  ,[FssName]  
  ,[AirportManager]  
  ,[AirportManagerPhoneNum]  
  ,[FAA]  
  ,[UWAID]  
  ,[IsFIX]  
  ,[HoursOfOperation]  
  ,[LandingFeeSmall]  
  ,[LandingFeeMedium]  
  ,[LandingFeeLarge]  
  ,[IsEUETS]  
  ,A.[IsDeleted]  
  ,A.[UWAUpdates]  
  ,A.[RunwayID1]  
  ,A.[RunwayID2]  
  ,A.[RunwayID3]  
  ,A.[RunwayID4]  
  ,A.[Runway2Length]  
  ,A.[Runway2Width]  
  ,A.[Runway3Length]  
  ,A.[Runway3Width]  
  ,A.[Runway4Length]  
  ,A.[Runway4Width]    
  ,M.MetroCD  
  ,C.CountryCD  
  ,D.DSTRegionCD 
  ,A.IsPublic
  ,A.IsPrivate
  ,A.IsMilitary 
  ,A.ExchangeRateID
  ,A.CustomsSundayWorkHours
  ,A.CustomsMondayWorkHours
  ,A.CustomsTuesdayWorkHours
  ,A.CustomsWednesdayWorkHours
  ,A.CustomsThursdayWorkHours
  ,A.CustomsFridayWorkHours
  ,A.CustomsSaturdayWorkHours
  ,A.CustomNotes
      
  FROM  [Airport] A LEFT OUTER JOIN [metro] M on A.MetroID = M.MetroID   
  LEFT OUTER JOIN [DSTRegion] D on A.DSTRegionID = D.DSTRegionID  
  LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID  
   
 where A.IsDeleted = 0  AND A.AirportID=@AirportID
GO