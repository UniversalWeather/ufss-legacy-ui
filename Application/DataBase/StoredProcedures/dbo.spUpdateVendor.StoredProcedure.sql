

/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 03/15/2013 18:53:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateVendor]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 03/15/2013 18:53:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spUpdateVendor]      
(     
  @VendorID BIGINT,    
  @CustomerID BIGINT,      
  @VendorCD char(5),      
  @Name varchar(40),      
  @VendorType char(1),      
  @IsApplicationFiled bit,      
  @IsApproved bit,      
  @IsDrugTest bit,      
  @IsInsuranceCERT bit,      
  @IsFAR135Approved bit,      
  @IsFAR135CERT bit,      
  @AdditionalInsurance varchar(60),      
  @Contact varchar(60),      
  @VendorContactName varchar(40),      
  @BillingName varchar(40),      
  @BillingAddr1 varchar(40),      
  @BillingAddr2 varchar(40),      
  @BillingCity varchar(40),      
  @BillingState varchar(10),      
  @BillingZip varchar(15),      
  @CountryID BIGINT,      
  @MetroID BIGINT,      
  @BillingPhoneNum varchar(25),      
  @BillingFaxNum varchar(25),      
  @Notes varchar(max),      
  @Credit NUMERIC(17,2),      
  @DiscountPercentage NUMERIC(5,2),      
  @TaxID varchar(15),      
  @Terms varchar(25),      
  @IsInActive bit,      
  @HomeBaseID BIGINT,      
  @LatitudeDegree NUMERIC(3,0),      
  @LatitudeMinutes NUMERIC(5,1),      
  @LatitudeNorthSouth char(1),      
  @LongitudeDegree NUMERIC(3,0),      
  @LongitudeMinutes NUMERIC(5,1),      
  @LongitudeEastWest char(1),      
  @AirportID  BIGINT,        
  @LastUpdUID varchar(30),      
  @LastUpdTS datetime,      
  @IsTaxExempt bit,      
  @DateAddedDT datetime,      
  @EmailID varchar(100),      
  @WebAddress varchar(40),      
  @IsDeleted bit       
  ,@TollFreePhone VARCHAR(25)  
,@BusinessEmail VARCHAR(250)  
,@Website VARCHAR(200)  
,@BillingAddr3 VARCHAR(40)  
,@BusinessPhone VARCHAR(25)  
,@CellPhoneNum VARCHAR(25)  
,@HomeFax VARCHAR(25)  
,@CellPhoneNum2 VARCHAR(25)  
,@OtherPhone VARCHAR(25)  
,@MCBusinessEmail VARCHAR(250)  
,@PersonalEmail VARCHAR(250)  
,@OtherEmail VARCHAR(250)
,@CertificateNumber  VARCHAR(50)
,@SITANumber VARCHAR(100)    
)      
-- =============================================      
-- Author: Karthikeyan.S      
-- Create date: 19/4/2012     
-- Altered by Mathes 03-07-12     
-- Description: Update the Vendor  information      
-- =============================================      
AS      
BEGIN       
SET NoCOUNT ON      
 set @LastUpdTS= GETUTCDATE()     
 UPDATE Vendor      
 SET       
      [Name] = @Name,      
      [VendorType] = @VendorType,      
      [IsApplicationFiled]=@IsApplicationFiled,      
      [IsApproved] = @IsApproved,      
      [IsDrugTest] = @IsDrugTest,      
      [IsInsuranceCERT] = @IsInsuranceCERT,      
      [IsFAR135Approved] = @IsFAR135Approved,      
      [IsFAR135CERT] = @IsFAR135CERT,      
      [AdditionalInsurance] = @AdditionalInsurance,      
      [Contact] = @Contact,      
      [VendorContactName] = @VendorContactName,      
      [BillingName] = @BillingName,      
      [BillingAddr1] = @BillingAddr1,      
      [BillingAddr2] = @BillingAddr2,      
      [BillingCity] = @BillingCity,      
      [BillingState] = @BillingState,      
      [BillingZip] = @BillingZip,      
      [CountryID] = @CountryID,      
      [MetroID] = @MetroID,      
      [BillingPhoneNum] = @BillingPhoneNum,      
      [BillingFaxNum] = @BillingFaxNum,      
      [Notes] = @Notes,      
      [Credit] = @Credit,      
      [DiscountPercentage] = @DiscountPercentage,      
      [TaxID] = @TaxID,      
      [Terms] = @Terms,      
      [IsInActive] = @IsInActive,      
      [HomeBaseID] = @HomeBaseID,      
      [LatitudeDegree] = @LatitudeDegree,       
      [LatitudeMinutes] = @LatitudeMinutes,      
      [LatitudeNorthSouth] = @LatitudeNorthSouth,      
      [LongitudeDegree] = @LongitudeDegree,      
      [LongitudeMinutes] = @LongitudeMinutes,      
      [LongitudeEastWest] = @LongitudeEastWest,      
      [AirportID] = @AirportID,      
      [LastUpdUID] = @LastUpdUID,      
      [LastUpdTS] = @LastUpdTS,      
      [IsTaxExempt] = @IsTaxExempt,      
      [DateAddedDT] = @DateAddedDT,      
      [EmailID] = @EmailID,      
      [WebAddress] = @WebAddress,      
      [IsDeleted] = @IsDeleted      
   ,TollFreePhone =@TollFreePhone  
   ,BusinessEmail =@BusinessEmail  
   ,Website =@Website  
   ,BillingAddr3 =@BillingAddr3  
   ,BusinessPhone =@BusinessPhone  
   ,CellPhoneNum =@CellPhoneNum  
   ,HomeFax =@HomeFax  
   ,CellPhoneNum2 =@CellPhoneNum2  
   ,OtherPhone =@OtherPhone  
   ,MCBusinessEmail =@MCBusinessEmail  
   ,PersonalEmail =@PersonalEmail  
   ,OtherEmail =@OtherEmail 
   ,CertificateNumber=@CertificateNumber
   ,SITA = @SITANumber 
  
 WHERE [VendorID]  = @VendorID     
      
END


GO


