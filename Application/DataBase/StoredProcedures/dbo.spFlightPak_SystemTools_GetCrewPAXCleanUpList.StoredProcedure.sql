/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList]    Script Date: 01/22/2013 14:36:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList]    Script Date: 01/22/2013 14:36:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList]
(
@UserCD varchar(30),
@OrderBy CHAR(30),
@ShowInactive bit,
@ReportDate DateTime
)


AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SQLQuery varchar(1000) 
	DECLARE @CustomerID BIGINT
    DECLARE @tblCrewPAXCleanup AS TABLE (  
    CrewOrPax varchar(1)
    ,CrewOrPAXCD varchar(5)
    , CrewOrPaxName varchar(80)
    , IsActive bit 
    , HomeBaseCD char(4)
    )  
	
	Select @CustomerID  = CustomerID  from UserMaster where UserName = @UserCD
	
	insert into @tblCrewPAXCleanup 
	SELECT 'C' as CrewOrPax,c.CrewCD, 
		--C.crewid,ActiveCrewlist.CrewID,
		CASE WHEN ISNULL( c.LastName,'') <>'' THEN C.LastName + ', '  ELSE '' END
		+
		CASE WHEN ISNULL( c.FirstName,'') <>'' THEN C.FirstName+ ' '  ELSE '' END
		+ CASE WHEN ISNULL( c.MiddleInitial,'') <>'' THEN c.MiddleInitial  ELSE '' END
		AS 'CrewOrPaxName',
		C.IsStatus as 'IsActive',
		 HomeBase.IcaoID as HomeBaseCD  
	from crew C
	LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID =  C.HomebaseID 	
	left join
	(
	  

		SELECT POC.CrewID
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
			inner join PostflightCrew POC on POC.POLegID = pol.POLegID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate
	
		union
		
		SELECT POL.CrewID
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
			inner join PostflightCrew POC on POC.POLegID = pol.POLegID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate

		union
	
		SELECT PC.CrewID
		from PreflightMain PM
			inner join PreflightLeg PL on PM.TripID = PL.TripID
			inner join PreflightCrewList PC on PC.LegID= PL.LegID
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PL.DepartureDTTMLocal> = @ReportDate
		union	
		
		SELECT PM.CrewID
		from PreflightMain PM
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PM.EstDepartureDT > @ReportDate
		
			
		--union Corporate Request
		--union Charterquote
	) ActiveCrewlist
		on C.CrewID = ActiveCrewlist.CrewID
		
	where 
		c.CustomerID = @CustomerID
		and ActiveCrewlist.CrewID is null
	
	--ORDER BY CASE WHEN @OrderBy = 'code' then c.CrewCD else c.LastName end ASC
		
	insert into @tblCrewPAXCleanup 
	
	SELECT 
		'P' as CrewOrPax,P.PassengerRequestorCD , 
		--P.PassengerRequestorID ,ActivePassengerlist.PassengerID,
		CASE WHEN ISNULL( P.LastName,'') <>'' THEN P.LastName + ', '  ELSE '' END
		+ 
		CASE WHEN ISNULL( P.FirstName,'') <>'' THEN P.FirstName+ ' '  ELSE '' END
		+ CASE WHEN ISNULL( P.MiddleInitial,'') <>'' THEN P.MiddleInitial  ELSE '' END
		AS 'CrewOrPaxName',
		P.IsActive,
	    HomeBase.IcaoID as HomeBaseCD  
	from Passenger P
	LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = P.HomebaseID 
	left join 
	(
	
		SELECT PO.PassengerRequestorID as 'PassengerID'
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate
			
		union
		
		SELECT POL.PassengerRequestorID as 'PassengerID'
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate
		
		union
		
		SELECT POP.PassengerID
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
			inner join PostflightPassenger POP on POP.POLegID = pol.POLegID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate

		union 

		SELECT PP.PassengerID
		from PreflightMain PM
			inner join PreflightLeg PL on PM.TripID = PL.TripID
			inner join PreflightPassengerList PP on PP.LegID= PL.LegID
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PL.DepartureDTTMLocal> = @ReportDate

		union
		
		SELECT PM.PassengerRequestorID as 'PassengerID'
		from PreflightMain PM
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PM.EstDepartureDT > = @ReportDate

		union
		
		SELECT PL.PassengerRequestorID as 'PassengerID'
		from PreflightMain PM
			inner join PreflightLeg PL on PM.TripID = PL.TripID
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PL.DepartureDTTMLocal> = @ReportDate 
		
		union

		SELECT CP.PassengerRequestorID as 'PassengerID'
		from CQMain CM
			inner join CQLeg CL on CM.CQMainID = CL.CQMainID
			inner join CQPassenger CP on CP.LegID= CL.LegID 
		where CM.IsDeleted = 0
			and CM.CustomerID = @CustomerID
			and CL.DepartureDTTMLocal> = @ReportDate

		--Corporate Request


	) ActivePassengerlist  
		on P.PassengerRequestorID = ActivePassengerlist.PassengerID 
	where
		P.CustomerID = @CustomerID 
		and ActivePassengerlist.PassengerID is null 
	
	
	select CrewOrPax
    ,CrewOrPAXCD 
    , CrewOrPaxName 
    , IsActive,HomeBaseCD from @tblCrewPAXCleanup 
	where IsActive = case when @ShowInactive=1 then IsActive else 1 end
	order by
	(case when @OrderBy='code' then  CrewOrPAX+','+CrewOrPAXCD else  CrewOrPAX+','+CrewOrPaxName end)

	--exec [dbo].[spFlightPak_SystemTools_GetCrewPAXCleanUpList] 	 'LESLIE_3','name',1,'jan 01, 2014'
END

GO
