IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTravelCoordinatorFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTravelCoordinatorFleet]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllTravelCoordinatorFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetAllTravelCoordinatorFleet](@CustomerID BIGINT)  
as  
  
set nocount on  
if len(@CustomerID) >0  
begin   
 SELECT  
            TravelCoordinatorFleetID
           ,CustomerID  
           ,TravelCoordinatorID 
           ,FleetID 
           ,IsChoice  
           ,LastUpdUID  
           ,LastUpdTS  
           ,IsDeleted  
           ,IsInactive
             
 FROM [TravelCoordinatorFleet]   WHERE CustomerID=@CustomerID  and IsDeleted = 'false'  
end
GO
