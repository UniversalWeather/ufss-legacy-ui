

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_ReportSearch]    Script Date: 01/14/2015 13:34:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_ReportSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_ReportSearch]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_ReportSearch]    Script Date: 01/14/2015 13:34:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_ReportSearch]
(
 @CustomerID bigint,    
 @ClientID bigint = NULL,    
 @HomeBaseID bigint = NULL,    
 @FleetID bigint = NULL,      
 @IsPersonal bit = NULL,      
 @IsCompleted bit = NULL,     
 @StartDate date = NULL,      
 @EndDate date = NULL,    
 @FromTripNUM bigint = NULL,    
 @ToTripNUM bigint = NULL    
)        
AS         
BEGIN      
-- =======================================================    
-- 05-Jan-2015- Created SP for Dynamic PreFlight Reports   
-- Created By: Jajati Badu
-- =======================================================      

 SET NOCOUNT ON;        

IF IsNull(@IsCompleted,0) = 1      
BEGIN     
	SELECT DISTINCT
		PreMain.TripID, PreMain.TripNUM, IsNull(PreMain.DispatchNum,'') AS 'DispatchNum'      
		, fleet.TailNum, PreMain.EstDepartureDT
		, isnull(RequestorFirstName,'')+' '+isnull(RequestorMiddleName,'')+' '+isnull(RequestorLastName,'') as 'RequestorName'      
		, isnull(PreMain.DepartmentDescription,'') as 'DepartmentDescription'      
		, PreMain.AuthorizationID, isnull(PreMain.TripDescription,'') as 'TripDescription'      
		--, isnull(IsException,0) as 'IsException'
		, isnull(PreMain.IsPersonal,0) as 'IsPersonal'     
		, PreMain.HomebaseID, isnull(airport.IcaoID,'') AS 'HomebaseCD'       
		, isnull(PreMain.FlightNum,'') as 'FlightNum', isnull(PreMain.IsCompleted,0) as 'IsCompleted'      
		, isnull(auth.AuthorizationCD,'') AS 'AuthorizationCD'      
		,isnull(pax.PassengerRequestorCD,'') AS 'PassengerCD'      
		,isnull(dep.DepartmentCD,'') AS 'DepartmentCD'      

	FROM PreflightMain PreMain   
		INNER JOIN PreflightLeg PreLeg ON PreMain.TripID=PreLeg.TripID   
		LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = PreMain.FleetID      
		LEFT OUTER JOIN Company company on PreMain.HomeBaseID = company.HomeBaseID      
		LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID      
		LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = PreMain.AuthorizationID      
		LEFT OUTER JOIN Passenger pax ON pax.PassengerRequestorID = PreMain.PassengerRequestorID      
		LEFT OUTER JOIN Department dep ON dep.DepartmentID = PreMain.DepartmentID      

	WHERE PreMain.CustomerID = @CustomerID      
		AND (isnull(@ClientID,0) = 0 OR IsNull(PreMain.ClientID,0) = IsNull(@ClientID,0))      
		AND (isnull(@HomebaseID,0) = 0 OR IsNull(PreMain.HomebaseID,0) = IsNull(@HomebaseID,0))      
		AND (isnull(@FleetID,0) = 0 OR IsNull(PreMain.FleetID,0) = IsNull(@FleetID,0))      
		AND (IsNull(@IsPersonal,0) = 0 OR IsNull(PreMain.IsPersonal,0) = IsNull(@IsPersonal,0))      
		AND IsNull(PreMain.IsCompleted,0) = IsNull(@IsCompleted,0)      
		AND PreMain.EstDepartureDT BETWEEN @StartDate AND @EndDate
		AND (ISNULL(@FromTripNUM,0)= 0 OR ISNULL(@ToTripNUM,0)= 0 OR PreMain.TripNUM BETWEEN @FromTripNUM AND @ToTripNUM)    
		AND isnull(PreMain.IsDeleted,0) = 0      
	ORDER BY TripNUM       
END      

ELSE      
BEGIN      
	SELECT DISTINCT
	PreMain.TripID, PreMain.TripNUM, IsNull(PreMain.DispatchNum,'') AS 'DispatchNum'      
	, fleet.TailNum, PreMain.EstDepartureDT
	, isnull(RequestorFirstName,'')+' '+isnull(RequestorMiddleName,'')+' '+isnull(RequestorLastName,'') as 'RequestorName'
	, isnull(PreMain.DepartmentDescription,'') as 'DepartmentDescription'      
	, PreMain.AuthorizationID, isnull(PreMain.TripDescription,'') as 'TripDescription'      
	--, isnull(IsException,0) as 'IsException'
	, isnull(PreMain.IsPersonal,0) as 'IsPersonal'      
	, PreMain.HomebaseID, isnull(airport.IcaoID,'') AS 'HomebaseCD'       
	, isnull(PreMain.FlightNum,'') as 'FlightNum', isnull(PreMain.IsCompleted,0) as 'IsCompleted'      
	, isnull(auth.AuthorizationCD,'') AS 'AuthorizationCD'      
	,isnull(pax.PassengerRequestorCD,'') AS 'PassengerCD'      
	,isnull(dep.DepartmentCD,'') AS 'DepartmentCD'        

	FROM PreflightMain PreMain      
		INNER JOIN PreflightLeg PreLeg ON PreMain.TripID=PreLeg.TripID
		LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = PreMain.FleetID      
		LEFT OUTER JOIN Company company on PreMain.HomeBaseID = company.HomeBaseID      
		LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID      
		LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = PreMain.AuthorizationID      
		LEFT OUTER JOIN Passenger pax ON pax.PassengerRequestorID = PreMain.PassengerRequestorID      
		LEFT OUTER JOIN Department dep ON dep.DepartmentID = PreMain.DepartmentID        

	WHERE PreMain.CustomerID = @CustomerID      
		AND (isnull(@ClientID,0) = 0 OR IsNull(PreMain.ClientID,0) = IsNull(@ClientID,0))      
		AND (isnull(@HomebaseID,0) = 0 OR IsNull(PreMain.HomebaseID,0) = IsNull(@HomebaseID,0))      
		AND (isnull(@FleetID,0) = 0 OR IsNull(PreMain.FleetID,0) = IsNull(@FleetID,0))      
		AND (IsNull(@IsPersonal,0) = 0 OR IsNull(PreMain.IsPersonal,0) = IsNull(@IsPersonal,0))
		AND PreMain.EstDepartureDT BETWEEN @StartDate AND @EndDate
		AND (ISNULL(@FromTripNUM,0)= 0 OR ISNULL(@ToTripNUM,0)= 0 OR PreMain.TripNUM BETWEEN @FromTripNUM AND @ToTripNUM)    
		AND isnull(PreMain.IsDeleted,0) = 0      

	ORDER BY TripNUM      
END        

END