
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHelpItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHelpItems]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON	
GO

CREATE PROCEDURE [dbo].[spGetHelpItems]

AS 

BEGIN

	SET NOCOUNT ON;
	
	SELECT ItemID, [Name], ParentID, MimeType, IsDirectory, [Size], [Content] 
	FROM HelpItems 
	ORDER BY ItemID, [Name]

END
GO




