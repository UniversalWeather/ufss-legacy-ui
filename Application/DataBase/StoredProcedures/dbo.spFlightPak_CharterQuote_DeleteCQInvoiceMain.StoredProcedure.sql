



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceMain]
(
	@CQInvoiceMainID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;

	--// Delete the Depeendencies Tables	
	DELETE FROM CQInvoiceQuoteDetail WHERE [CQInvoiceMainID] = @CQInvoiceMainID AND CustomerID = @CustomerID
	DELETE FROM CQInvoiceAdditionalFees WHERE [CQInvoiceMainID] = @CQInvoiceMainID AND CustomerID = @CustomerID
	DELETE FROM CQInvoiceSummary WHERE [CQInvoiceMainID] = @CQInvoiceMainID AND CustomerID = @CustomerID

	DELETE FROM [CQInvoiceMain] WHERE [CQInvoiceMainID] = @CQInvoiceMainID AND CustomerID = @CustomerID

GO


