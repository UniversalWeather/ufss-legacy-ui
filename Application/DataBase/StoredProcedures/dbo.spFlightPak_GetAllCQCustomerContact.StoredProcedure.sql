/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCQCustomerContact]    Script Date: 03/05/2013 17:59:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllCQCustomerContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllCQCustomerContact]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCQCustomerContact]    Script Date: 03/05/2013 17:59:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spFlightPak_GetAllCQCustomerContact]      
(      
 @CustomerID  BIGINT  
  
)      
AS      
BEGIN      
SET NOCOUNT ON;      
   
SELECT     
      [CQCustomerContactID]
      ,[CustomerID]
      ,[CQCustomerID]
      ,[CQCustomerContactCD]
      ,[IsChoice]
      ,[FirstName]
      ,[Addr1]
      ,[Addr2]
      ,[Addr3]
      ,[CityName]
      ,[StateName]
      ,[PostalZipCD]
      ,[CountryID]
      ,[PhoneNum]
      ,[OtherPhoneNum]
      ,[PrimaryMobileNum]
      ,[SecondaryMobileNum]
      ,[FaxNum]
      ,[HomeFaxNum]
      ,[CreditName1]
      ,[CreditNum1]
      ,[CreditName2]
      ,[CreditNum2]
      ,[MoreInfo]
      ,[Notes]
      ,[Title]
      ,[LastName]
      ,[MiddleName]
      ,[EmailID]
      ,[PersonalEmailID]
      ,[OtherEmailID]
      ,[ContactName]
      ,[IsInActive]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,[BusinessPhoneNum]
      ,[BusinessFaxNum]
      ,[BusinessEmailID]
      ,CardType1
	  ,SecurityCode1
	  ,ExpirationDate1
	  ,CardType2
	  ,SecurityCode2
	  ,ExpirationDate2
 FROM [CQCustomerContact]    
 
 WHERE CQCustomerContact.CustomerID  = @CustomerID      
 AND ISNULL(CQCustomerContact.IsDeleted,0) = 0    
 order by CQCustomerContactCD 
 END    

GO


