/****** Object:  StoredProcedure [dbo].[spGetAllFuelLocator]    Script Date: 01/07/2013 19:09:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFuelLocator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFuelLocator]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllFuelLocator]    Script Date: 01/07/2013 19:09:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllFuelLocator] (@CustomerID BIGINT,@ClientID bigint)    
AS    
-- =============================================    
-- Author:Hamsha.S    
-- Create date: 08/05/2012    
-- Description: Get the FuelLocator information    
-- =============================================    
SET NOCOUNT ON    
    
IF (@ClientID > 0)    
BEGIN    
 SELECT FuelLocatorID    
  ,f.CustomerID    
  ,f.FuelLocatorCD    
  ,f.FuelLocatorDescription    
  ,f.ClientID    
  ,f.LastUpdUID    
  ,f.LastUpdTS    
  ,f.IsDeleted    
  ,c.ClientCD 
  ,f.IsInActive   
 FROM [FuelLocator] f left outer  join Client c on f.ClientID= c.ClientID    
 WHERE f.CustomerID = @CustomerID    
  AND f.IsDeleted = 'false' and f.ClientID =@ClientID    
  order by FuelLocatorCD    
END    
ELSE    
BEGIN    
SELECT FuelLocatorID    
  ,f.CustomerID    
  ,f.FuelLocatorCD    
  ,f.FuelLocatorDescription    
  ,f.ClientID    
  ,f.LastUpdUID    
  ,f.LastUpdTS    
  ,f.IsDeleted    
  ,c.ClientCD  
  ,f.IsInActive     
 FROM [FuelLocator] f left outer  join Client c on f.ClientID= c.ClientID    
 WHERE f.CustomerID = @CustomerID    
  AND f.IsDeleted = 'false'    
  order by FuelLocatorCD    
END  
GO


