
/****** Object:  StoredProcedure [dbo].[sp_fss_GetCQCustomer]    Script Date: 02/28/2014 13:43:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetCQCustomer]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetCQCustomer]    Script Date: 02/28/2014 13:43:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[sp_fss_GetCQCustomer]  
(  
 @CustomerID BIGINT  
 ,@CQCustomerID BIGINT  
 ,@CQCustomerCD varchar(5)   
 ,@FetchHomebaseOnly CHAR(4)  
 ,@FetchActiveOnly BIT  
)  
AS  
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get ALL CQCustomer details for Popup        
-- Exec sp_fss_GetCQCustomer 10099,0,'','',1  
-- Exec sp_fss_GetCQCustomer 10099,0,'','KDAL',1  
-- Exec sp_fss_GetCQCustomer 10099,0,'','KDAL',0  
-- Exec sp_fss_GetCQCustomer 10099, 10099104,NULL, null, 0  
-- Exec sp_fss_GetCQCustomer 10099, 0,'V1000',null,0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
 SELECT   
   CQ.CQCustomerID  
   ,CQ.CustomerID     
   ,CASE WHEN CQ.CQCustomerCD IS NULL THEN '' ELSE LTRIM(RTRIM(CQ.CQCustomerCD)) END AS CQCustomerCD  
   ,CQ.CQCustomerName  
   ,CQ.IsApplicationFiled  
   ,CQ.IsApproved  
   ,CQ.BillingName  
   ,CQ.BillingAddr1  
   ,CQ.BillingAddr2  
   ,CQ.BillingAddr3  
   ,CQ.BillingCity  
   ,CQ.BillingState  
   ,CQ.BillingZip     
   ,CQ.BillingPhoneNum  
   ,CQ.BillingFaxNum  
   ,CQ.Notes  
   ,CQ.Credit  
   ,CQ.DiscountPercentage     
   ,ISNULL(CQ.IsInactive,0) IsInactive   
    ,ISNULL(CQ.IsDeleted,0) IsDeleted  
   ,CQ.HomebaseID  
   ,HBA.IcaoID AS HomeBaseIcaoID  
   ,CQ.AirportID  
   ,A.IcaoID AS AirportIcaoID  
   ,CQ.CountryID  
   ,C.CountryCD AS CountryCD  
   ,C.CountryName AS CountryName  
   ,CQ.DateAddedDT  
   ,CQ.WebAddress  
   ,CQ.EmailID  
   ,CQ.TollFreePhone  
   ,CQ.LastUpdUID  
   ,CQ.LastUpdTS        
   ,Com.BaseDescription AS BaseDescription     
   ,CQ.CustomerType  
   ,CQ.IntlStdCrewNum  
   ,CQ.DomesticStdCrewNum  
   ,CQ.MinimumDayUseHrs  
   ,CQ.DailyUsageAdjTax  
   ,CQ.LandingFeeTax  
   ,CQ.MarginalPercentage                     
 FROM CQCustomer CQ   
  LEFT OUTER JOIN Country C   ON C.CountryID = CQ.CountryID  
  LEFT OUTER JOIN Company Com ON Com.HomebaseID = CQ.HomebaseID  
  LEFT OUTER JOIN Airport HBA ON HBA.AirportID = Com.HomebaseAirportID  
  LEFT OUTER JOIN Airport A   ON A.AirportID = CQ.AirportID   
 WHERE CQ.CustomerID = @CustomerID         
  AND CQ.CQCustomerID = case when @CQCustomerID <>0 then @CQCustomerID else CQ.CQCustomerID end   
     AND CQ.CQCustomerCD = case when Ltrim(Rtrim(@CQCustomerCD)) <> '' then @CQCustomerCD else CQ.CQCustomerCD end         
  AND ISNULL(CQ.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(CQ.IsInActive,0) else 0 end  
  AND ISNULL(HBA.ICAOID,'')    = CASE WHEN LTRIM(RTRIM(@FetchHomebaseOnly)) <> '' THEN @FetchHomebaseOnly ELSE ISNULL(HBA.ICAOID,'') END    
  AND ISNULL(CQ.IsDeleted,0) = 0                                   
 ORDER BY CQ.CQCustomerCD ASC  
      
END  
  
  
GO

