/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_DeleteCRPassenger]    Script Date: 02/11/2013 15:56:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_DeleteCRPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRPassenger]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRPassenger]
(@CRPassengerID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
)
AS BEGIN

	SET NOCOUNT OFF;
	Delete from CRPassenger
	WHERE 
		CRPassengerID = @CRPassengerID and CustomerID=@CustomerID and CRLegID=@CRLegID


END
GO


