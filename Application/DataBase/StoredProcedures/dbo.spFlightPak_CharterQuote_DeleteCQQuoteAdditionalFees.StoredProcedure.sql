
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQQuoteAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteAdditionalFees]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteAdditionalFees]
(
	@CQQuoteAdditionalFeesID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQQuoteAdditionalFees] WHERE [CQQuoteAdditionalFeesID] = @CQQuoteAdditionalFeesID AND [CustomerID] = @CustomerID
GO
