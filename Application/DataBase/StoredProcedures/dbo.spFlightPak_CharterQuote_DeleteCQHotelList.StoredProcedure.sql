IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQHotelList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQHotelList]
(
	@CQHotelListID bigint,
	@CustomerID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime
)
AS
	SET NOCOUNT OFF;
DELETE FROM [CQHotelList] WHERE [CQHotelListID] = @CQHotelListID
GO
