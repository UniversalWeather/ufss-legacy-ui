/****** Object:  StoredProcedure [dbo].[spUnLockCustomer]    Script Date: 09/13/2012 12:38:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUnLockCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUnLockCustomer]
GO
/****** Object:  StoredProcedure [dbo].[spUnLockCustomer]    Script Date: 09/13/2012 12:38:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spUnLockCustomer]
@CustomerId bigint
AS
BEGIN
UPDATE Customer set IsCustomerLock = 0 where CustomerID = @CustomerId
END

GO


