

/****** Object:  StoredProcedure [dbo].[spAddSalesPerson]    Script Date: 04/08/2013 19:22:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddSalesPerson]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddSalesPerson]
GO



/****** Object:  StoredProcedure [dbo].[spAddSalesPerson]    Script Date: 04/08/2013 19:22:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddSalesPerson]    
 (@CustomerID BIGINT,     
 @SalesPersonCD CHAR(5),  
 @FirstName VARCHAR(40),     
 @LastName VARCHAR(40),     
 @MiddleName VARCHAR(40),  
 @PhoneNum VARCHAR(25),        
 @Address1 VARCHAR(60),     
 @Address2 VARCHAR(60),     
 @Address3 VARCHAR(60),     
 @CityName VARCHAR(40),     
 @StateName VARCHAR(15),     
 @PostalZipCD VARCHAR(10),     
 @CountryID BIGINT,        
 @FaxNum VARCHAR(25),     
 @PersonalFaxNum VARCHAR(25),    
 @CellPhoneNum varchar(25),     
 @EmailAddress VARCHAR(200),     
 @PersonalEmailID VARCHAR(100),     
 @Logo VARCHAR(200),     
 @LogoPosition  INT,   
 @Notes VARCHAR(100),     
 @IsInActive BIT,  
 @LastUpdUID VARCHAR(30),     
 @LastUpdTS DATETIME,     
 @IsDeleted BIT,
 @Homebaseid BIGINT     
 )     
-- =============================================      
-- Author:Karthikeyan.S      
-- Create date: 13/05/2012      
-- Description: Insert the Customer Address information      
-- =============================================      
AS  BEGIN       
 SET NOCOUNT ON    
 DECLARE @SalesPersonID BIGINT      
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @SalesPersonID OUTPUT    
 set @LastUpdTS = GETUTCDATE()      
     
 INSERT INTO [SalesPerson]  
  (SalesPersonID,      
  CustomerID,       
  SalesPersonCD,      
  FirstName,      
  LastName,       
  MiddleName,  
  PhoneNum,       
  Address1,       
  Address2,       
  Address3,       
  CityName,       
  StateName,       
  PostalZipCD,       
  CountryID,    
  FaxNum,       
  PersonalFaxNum,  
  CellPhoneNUM,  
  EmailAddress,       
  PersonalEmailID,  
  Logo,  
  LogoPosition,    
  Notes,       
  IsInActive,  
  LastUpdUID,       
  LastUpdTS,       
  IsDeleted,
  HomeBaseId          
  )    
 VALUES    
  (@SalesPersonID,      
  @CustomerID,       
  @SalesPersonCD,      
  @FirstName,      
  @LastName,       
  @MiddleName,  
  @PhoneNum,       
  @Address1,       
  @Address2,       
  @Address3,       
  @CityName,       
  @StateName,       
  @PostalZipCD,       
  @CountryID,    
  @FaxNum,       
  @PersonalFaxNum,  
  @CellPhoneNUM,  
  @EmailAddress,       
  @PersonalEmailID,  
  @Logo,  
  @LogoPosition,      
  @Notes,       
  @IsInActive,  
  @LastUpdUID,       
  @LastUpdTS,       
  @IsDeleted,
  @Homebaseid)    
END      
GO


