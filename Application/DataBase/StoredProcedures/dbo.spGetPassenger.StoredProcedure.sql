

/****** Object:  StoredProcedure [dbo].[spGetPassenger]    Script Date: 03/18/2013 12:48:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassenger]
GO



/****** Object:  StoredProcedure [dbo].[spGetPassenger]    Script Date: 03/18/2013 12:48:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE PROCEDURE [dbo].[spGetPassenger]            
(@CustomerID BIGINT,@ClientID BIGINT)                  
AS                  
SET NOCOUNT ON                  
  IF(@ClientID>0)          
  BEGIN              
SELECT                    
		[PassengerRequestorID]              
		,[PassengerRequestorCD]              
		,[PassengerName]              
		,[CustomerID]              
		,[DepartmentID]              
		,[PassengerDescription]              
		,[PhoneNum]              
		,[IsEmployeeType]              
		,[StandardBilling]              
		,[AuthorizationID]              
		,[AuthorizationDescription]              
		,[Notes]              
		,[LastName]              
		,[FirstName]              
		,[MiddleInitial]              
		,[DateOfBirth]              
		,[ClientID]              
		,[AssociatedWithCD]              
		,[HomebaseID]              
		,[IsActive]              
		,[FlightPurposeID]              
		,[SSN]              
		,[Title]              
		,[SalaryLevel]              
		,[IsPassengerType]              
		,[LastUpdUID]              
		,[LastUpdTS]              
		,[IsScheduledServiceCoord]              
		,[CompanyName]              
		,[EmployeeID]              
		,[FaxNum]              
		,[EmailAddress]              
		,[PersonalIDNum]              
		,[IsSpouseDependant]              
		,[AccountID]              
		,[CountryID]              
		,[IsSIFL]              
		,[PassengerAlert]              
		,[Gender]              
		,[AdditionalPhoneNum]              
		,[IsRequestor]              
		,[CountryOfResidenceID]              
		,[PaxScanDoc]              
		,[TSAStatus]              
		,[TSADTTM]              
		,[Addr1]              
		,[Addr2]              
		,[City]              
		,[StateName]              
		,[PostalZipCD]              
		,[IsDeleted]        
		,PrimaryMobile        
		,BusinessFax        
		,CellPhoneNum2        
		,OtherPhone        
		,PersonalEmail        
		,OtherEmail        
		,Addr3      
		,PassengerWeight    
		,PassengerWeightUnit    
		,Birthday    
		,Anniversaries    
		,EmergencyContacts    
		,CreditcardInfo    
		,CateringPreferences    
		,HotelPreferences     
		,INSANumber  
		,Category  
		,GreenCardCountryID  
		,CardExpires  
		,ResidentSinceYear  
		,GreenCardGender
		,cqcustomerid 
		,PaxTitle
		,CityOfBirth
		,Nickname   
  FROM [Passenger]                
   WHERE CustomerID = @CustomerID and ClientID=@ClientID and IsDeleted = 0         
  END          
  ELSE          
  BEGIN          
  SELECT                    
		[PassengerRequestorID]              
		,[PassengerRequestorCD]              
		,[PassengerName]              
		,[CustomerID]              
		,[DepartmentID]              
		,[PassengerDescription]              
		,[PhoneNum]              
		,[IsEmployeeType]              
		,[StandardBilling]              
		,[AuthorizationID]              
		,[AuthorizationDescription]              
		,[Notes]              
		,[LastName]              
		,[FirstName]              
		,[MiddleInitial]              
		,[DateOfBirth]              
		,[ClientID]              
		,[AssociatedWithCD]              
		,[HomebaseID]              
		,[IsActive]              
		,[FlightPurposeID]              
		,[SSN]              
		,[Title]              
		,[SalaryLevel]              
		,[IsPassengerType]              
		,[LastUpdUID]              
		,[LastUpdTS]              
		,[IsScheduledServiceCoord]              
		,[CompanyName]              
		,[EmployeeID]              
		,[FaxNum]              
		,[EmailAddress]              
		,[PersonalIDNum]              
		,[IsSpouseDependant]              
		,[AccountID]              
		,[CountryID]              
		,[IsSIFL]              
		,[PassengerAlert]              
		,[Gender]              
		,[AdditionalPhoneNum]              
		,[IsRequestor]              
		,[CountryOfResidenceID]              
		,[PaxScanDoc]              
		,[TSAStatus]              
		,[TSADTTM]              
		,[Addr1]              
		,[Addr2]              
		,[City]              
		,[StateName]              
		,[PostalZipCD]              
		,[IsDeleted]          
		,PrimaryMobile        
		,BusinessFax        
		,CellPhoneNum2        
		,OtherPhone        
		,PersonalEmail        
		,OtherEmail        
		,Addr3     
		,PassengerWeight      
		,PassengerWeightUnit    
		,Birthday    
		,Anniversaries    
		,EmergencyContacts    
		,CreditcardInfo    
		,CateringPreferences    
		,HotelPreferences    
		,INSANumber  
		,Category  
		,GreenCardCountryID  
		,CardExpires  
		,ResidentSinceYear  
		,GreenCardGender
		,cqcustomerid  
		,PaxTitle
		,CityOfBirth
		,Nickname         
  FROM [Passenger]                
  WHERE CustomerID = @CustomerID and IsDeleted = 0
  END 
GO


