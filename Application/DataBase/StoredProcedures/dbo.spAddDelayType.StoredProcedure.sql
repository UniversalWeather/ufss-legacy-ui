
/****** Object:  StoredProcedure [dbo].[spAddDelayType]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddDelayType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddDelayType]
go
CREATE Procedure [dbo].[spAddDelayType]
( 
@CustomerID bigint
,@DelayTypeCD char(2)
,@DelayTypeDescription varchar(100)
,@ClientID bigint
,@LastUpdUID varchar(30)
,@LastUpdTS datetime
,@IsSiflExcluded bit
,@IsDeleted bit
,@IsInActive bit
)
-- =============================================
-- Author:Sujitha.V
-- Create date: 12/4/2012
-- Description: Insert the DelayType  information
-- Update: 
-- =============================================
AS
BEGIN 
SET NoCOUNT ON
DECLARE @DelayTypeID BIGINT

EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @DelayTypeID OUTPUT
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  

   INSERT INTO [DelayType]
           (
			DelayTypeID
			,CustomerID
			,DelayTypeCD
			,DelayTypeDescription
			,ClientID
			,LastUpdUID 
			,LastUpdTS          
			,IsSiflExcluded
			,IsDeleted
			,IsInActive
           )
     VALUES
		  (
			@DelayTypeID      
			,@CustomerID 
			,@DelayTypeCD 
			,@DelayTypeDescription
			,@ClientID 
			,@LastUpdUID
			,@LastUpdTS 
			,@IsSiflExcluded 
	        ,@IsDeleted 
	        ,@IsInActive
		 )
 END
GO
