
/****** Object:  StoredProcedure [dbo].[spDeleteDepartmentAuthorization]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteDepartmentAuthorization]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteDepartmentAuthorization]
GO
CREATE PROCEDURE [dbo].[spDeleteDepartmentAuthorization] (
	@AuthorizationID BIGINT
	,@DepartmentID BIGINT
	,@AuthorizationCD VARCHAR(8)
	,@LastUpdUID VARCHAR(30)
	,@LastUpdTS DATETIME
	,@IsDeleted BIT
	)
	-- =============================================  
	-- Author: Sujitha.V  
	-- Create date: 05/5/2012  
	-- Description: Delete the Department information  
	-- =============================================  
AS
BEGIN
			--Check if the record is not being used anywhere in the application before Delete  
 DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)        
SET @ENTITYNAME = N'DepartmentAuthorization'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @AuthorizationID, @RecordUsed OUTPUT
--End of Delete Check
  
  if (@RecordUsed <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 ELSE  
  BEGIN  
   --To store the lastupdateddate as UTC date 
  
   SET @LastUpdTS = GETUTCDATE()  
   
   --End of UTC date
SET NoCOUNT ON


	UPDATE DepartmentAuthorization
	SET [LastUpdUID] = @LastUpdUID
		,[LastUpdTS] = @LastUpdTS
		,[IsDeleted] = @IsDeleted
	WHERE [AuthorizationID] = @AuthorizationID
END
END
GO
