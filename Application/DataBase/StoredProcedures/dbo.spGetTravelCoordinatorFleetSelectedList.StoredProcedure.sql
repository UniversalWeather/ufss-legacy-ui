IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTravelCoordinatorFleetSelectedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTravelCoordinatorFleetSelectedList]
GO
/****** Object:  StoredProcedure [dbo].[spGetTravelCoordinatorFleetSelectedList]    Script Date: 08/24/2012 10:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetTravelCoordinatorFleetSelectedList](@CustomerID BIGINT ,@TravelCoordinatorID BIGINT)
-- =============================================
-- Author: srinivas.p
-- Create date:
-- Get The Selected TravelCoordinator 
-- =============================================
as
SET NoCOUNT ON
BEGIN
SELECT DISTINCT  
		TravelCoordinatorFleetID,
		[CustomerID],
		TravelCoordinatorID,
		[FleetID],
        [IsChoice],
        [LastUpdUID],
        [LastUpdTS],
        [IsDeleted],
        IsInactive
	
FROM  [TravelCoordinatorFleet]
WHERE [TravelCoordinatorFleet].TravelCoordinatorID in (SELECT TravelCoordinator.TravelCoordinatorID 
														FROM TravelCoordinator 
														WHERE TravelCoordinator.TravelCoordinatorID=@TravelCoordinatorID) 
	   AND TravelCoordinatorFleet.CustomerID=@CustomerID  
END
GO
