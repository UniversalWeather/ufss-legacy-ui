

/****** Object:  StoredProcedure [dbo].[spGetUserSecurityHintByEmailId]    Script Date: 09/07/2012 17:33:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetUserSecurityHintByEmailId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetUserSecurityHintByEmailId]
GO



/****** Object:  StoredProcedure [dbo].[spGetUserSecurityHintByEmailId]    Script Date: 09/07/2012 17:33:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetUserSecurityHintByEmailId](      
 @EmailId varchar(100) )      
AS      
BEGIN      
 DECLARE @UserName varchar(30)    
 SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId        
 SELECT [UserSecurityHintID], [UserName],[SecurityQuestion], [ActiveStartDT], [ActiveEndDT],       
 [PWDStatus] , [LastUpdUID], [LastUpdTS] , [SecurityAnswer] , [CustomerID],IsInactive FROM UserSecurityHint      
 WHERE UserName = @UserName      
      
END 
GO


