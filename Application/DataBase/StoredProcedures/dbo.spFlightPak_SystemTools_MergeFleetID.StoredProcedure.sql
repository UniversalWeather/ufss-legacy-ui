
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_MergeFleetID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_MergeFleetID]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_MergeFleetID]
(
@OldFleetID BIGINT,
@NewTailNum VARCHAR(6),
@CustomerID BIGINT,
@LastUpdUID CHAR(30),
@EffectiveDate DateTime
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	if @EffectiveDate is null 
	begin
		begin Try
		begin Transaction
			update Fleet set 
			TailNum =@NewTailNum 
			,LastUpdTS = GETUTCDATE()
			,LastUpdUID = @LastUpdUID
			where CustomerID = @CustomerID and FleetID = @OldFleetID 
		
		commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	end
	else
	begin
	
		set @MergeSuccess = 0
		DECLARE @NewFleetID BIGINT
	
		begin Try
		begin Transaction
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @NewFleetID OUTPUT     
			INSERT INTO [dbo].[Fleet]    
			   (FleetID    
			   ,[TailNum]    
			   ,[CustomerID]    
			   ,[AircraftCD]    
			   ,[SerialNum]    
			   ,[TypeDescription]    
			   ,[MaximumReservation]    
			   ,[LastInspectionDT]    
			   ,[InspectionHrs]    
			   ,[WarningHrs]    
			   ,[MaximumPassenger]    
			   ,[HomebaseID]    
			   ,[MaximumFuel]    
			   ,[MinimumFuel]    
			   ,[BasicOperatingWeight]    
			   ,[MimimumRunway]    
			   ,[IsInActive]    
			   ,[IsDisplay31]    
			   ,[SIFLMultiControlled]    
			   ,[SIFLMultiNonControled]    
			   ,[Notes]    
			   ,[ComponentCD]    
			   ,[ClientID]    
			   ,[FleetType]    
			   ,[FlightPhoneNum]    
			   ,[Class]    
			   ,[VendorID]    
			   ,[VendorType]    
			   ,[YearMade]    
			   ,[ExteriorColor]    
			   ,[INTeriorColor]    
			   ,[IsAFIS]    
			   ,[IsUWAData]    
			   ,[LastUpdUID]    
			   ,[LastUpdTS]    
			   ,[FlightPlanCruiseSpeed]    
			   ,[FlightPlanMaxFlightLevel]    
			   ,[MaximumTakeOffWeight]    
			   ,[MaximumLandingWeight]    
			   ,[MaximumWeightZeroFuel]    
			   ,[TaxiFuel]    
			   ,[MultiSec]    
			   ,[ForeGrndCustomColor]    
			   ,[BackgroundCustomColor]    
			   ,[FlightPhoneINTlNum]    
			   ,[FleetSize]    
			   ,[FltScanDoc]    
			   ,[MinimumDay]    
			   ,[StandardCrewINTl]    
			   ,[StandardCrewDOM]    
			   ,[IsFAR91]    
			   ,[IsFAR135]    
			   ,[EmergencyContactID]    
			   ,[IsTaxDailyAdj]    
			   ,[IsTaxLandingFee]    
			   ,[IsDeleted]    
			   ,[AircraftID]    
			   ,[CrewID]
			   ,SecondaryDomFlightPhone
			   ,SecondaryIntlFlightPhone )
		 select @NewFleetID,    
				@NewTailNum   
			   ,[CustomerID]    
			   ,[AircraftCD]    
			   ,[SerialNum]    
			   ,[TypeDescription]    
			   ,[MaximumReservation]    
			   ,[LastInspectionDT]    
			   ,[InspectionHrs]    
			   ,[WarningHrs]    
			   ,[MaximumPassenger]    
			   ,[HomebaseID]    
			   ,[MaximumFuel]    
			   ,[MinimumFuel]    
			   ,[BasicOperatingWeight]    
			   ,[MimimumRunway]    
			   ,[IsInActive]    
			   ,[IsDisplay31]    
			   ,[SIFLMultiControlled]    
			   ,[SIFLMultiNonControled]    
			   ,[Notes]    
			   ,[ComponentCD]    
			   ,[ClientID]    
			   ,[FleetType]    
			   ,[FlightPhoneNum]    
			   ,[Class]    
			   ,[VendorID]    
			   ,[VendorType]    
			   ,[YearMade]    
			   ,[ExteriorColor]    
			   ,[INTeriorColor]    
			   ,[IsAFIS]    
			   ,[IsUWAData]    
			   ,@LastUpdUID  
			   ,GETUTCDATE()   
			   ,[FlightPlanCruiseSpeed]    
			   ,[FlightPlanMaxFlightLevel]    
			   ,[MaximumTakeOffWeight]    
			   ,[MaximumLandingWeight]    
			   ,[MaximumWeightZeroFuel]    
			   ,[TaxiFuel]    
			   ,[MultiSec]    
			   ,[ForeGrndCustomColor]    
			   ,[BackgroundCustomColor]    
			   ,[FlightPhoneINTlNum]    
			   ,[FleetSize]    
			   ,[FltScanDoc]    
			   ,[MinimumDay]    
			   ,[StandardCrewINTl]    
			   ,[StandardCrewDOM]    
			   ,[IsFAR91]    
			   ,[IsFAR135]    
			   ,[EmergencyContactID]    
			   ,[IsTaxDailyAdj]    
			   ,[IsTaxLandingFee]    
			   ,[IsDeleted]    
			   ,[AircraftID]    
			   ,[CrewID]
			   ,SecondaryDomFlightPhone
			   ,SecondaryIntlFlightPhone
			from fleet where fleetid =@OldFleetID  and CustomerID = @CustomerID
		
		

			 -- //caltdefs -- No Table available in Database	 
			 -- 	CrewAircraftAssigned
			 
				UPDATE CrewAircraftAssigned  
					SET fleetid =@NewFleetID,		
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()
				WHERE fleetid =@OldFleetID and CustomerID=@CustomerID
				
			  --  ItineraryPlan
				Update ItineraryPlan 
					set fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()  
					from ItineraryPlanDetail 
				where ItineraryPlan.ItineraryPlanID =  ItineraryPlanDetail.ItineraryPlanID 
				and DepartureLocal >= @EffectiveDate and ItineraryPlan.FleetID =@OldFleetID and ItineraryPlan.CustomerID=@CustomerID
				
			  -- PostflightExpense
				
				Update PostflightExpense 
					set fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()  
					from PostflightLeg PL ,PostflightMain PM 
				where PM.POLogID = PL.POLogID
					and PL.POLegID = PostflightExpense.POLegID
					and PM.IsDeleted = 0
					and PL.OutboundDTTM >= @EffectiveDate and PostflightExpense.fleetid =@OldFleetID and PostflightExpense.CustomerID=@CustomerID
				
			   -- PostFlightMain	
				
				Update PostFlightMain 
					set fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()  
					from PostflightLeg PL ,PostflightMain PM 
				where PM.POLogID = PL.POLogID
					and PM.IsDeleted = 0
					and PL.OutboundDTTM >= @EffectiveDate and PM.fleetid =@OldFleetID and PM.CustomerID=@CustomerID
					
			   -- Preflightmain 	
			   
			   Update Preflightmain 
					set fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()  
					from Preflightleg  PL 
				where Preflightmain.TripID = PL.TripID
					and Preflightmain.IsDeleted = 0
					and Preflightmain.EstDepartureDT >= @EffectiveDate and Preflightmain.fleetid =@OldFleetID and Preflightmain.CustomerID=@CustomerID
					
				
				-- Budget
			    
				UPDATE Budget  
					SET fleetid =@NewFleetID,		
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()
				WHERE fleetid =@OldFleetID and CustomerID=@CustomerID and budget.fiscalyear > = YEAR(@EffectiveDate)
			    
				-- UWAL
			    -- No Customerid in this table
				UPDATE UWAL  
					SET fleetid =@NewFleetID,		
					LastUpdUID=@LastUpdUID,
					LastUptTS=GETUTCDATE()
					WHERE fleetid =@OldFleetID  and departuredttmlocal > = @EffectiveDate

				--  FleetGroupOrder
				
				UPDATE FleetGroupOrder  
					SET fleetid =@NewFleetID,		
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()
				WHERE fleetid =@OldFleetID and CustomerID=@CustomerID 
				
				-- CQMain
				
				UPDATE cqmain 
					SET fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE() 
					from CQLeg 
				WHERE CQMain.CQMainID = CQLeg.CQMainID
				and cqmain.fleetid = @OldFleetID and cqleg.DepartureDTTMLocal > = @EffectiveDate and cqmain.CustomerID=@CustomerID 
				
				--- CRMain
				
				UPDATE  CRMain 
					SET fleetid = @NewFleetID,
					LastUpdUID=@LastUpdUID,
					LastUpdTS=GETUTCDATE()   
					from CRLeg
				where CRMain.CRMainID = CRLeg.CRMainID
				and CRLeg.DepartureDTTMLocal>=@EffectiveDate and CRMain.CustomerID=@CustomerID and fleetid = @OldFleetID
				
				
			   -- UWATSSMain   
			   
			   UPDATE UWATSSMain  
					SET fleetid =@NewFleetID,		
					LastUpdUID=@LastUpdUID,
					LastUptTS=GETUTCDATE()
				WHERE fleetid =@OldFleetID and CustomerID=@CustomerID
				
				-- TailHistory 	( No Fleetid in this table)

				--Fleet
				UPDATE Fleet set IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
				where FleetID = @OldFleetID 
				 
					
				commit transaction
				set @MergeSuccess = 1
			
			END TRY
			
			BEGIN CATCH
				set @MergeSuccess = 0
				Rollback transaction
			END CATCH
	end
	select   @MergeSuccess as 'MergeSuccess'
	
END

GO
