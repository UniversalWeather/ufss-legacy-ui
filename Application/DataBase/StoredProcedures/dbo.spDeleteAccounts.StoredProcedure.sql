
/****** Object:  StoredProcedure [dbo].[spDeleteAccounts]    Script Date: 7/15/2015 3:39:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER  procedure [dbo].[spDeleteAccounts]  
(@AccountID  BIGINT,   
@AccountNum VARCHAR(32),    
@CustomerID BIGINT,    
@LastUpdUID VARCHAR(30),    
@LastUpdTS DATETIME,  
@IsDeleted BIT)  
-- =============================================  
-- Author:Prabhu.D  
-- Create date: 12/4/2012  
-- Description: Delete the Account information. Set IsDeleted to false  
-- =============================================  
as  
begin   
SET NoCOUNT ON  
 --Check if the record is not being used anywhere in the application before Delete
DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)    
DECLARE @RecordUsedInCompany int SET @RecordUsedInCompany = 0    
SET @ENTITYNAME = N'Account'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETEWITHISDELETE @ENTITYNAME, @AccountID, @RecordUsed OUTPUT
SELECT @RecordUsedInCompany = COUNT(*) FROM [dbo].[Company] Where SaleTax = @AccountNum OR StateTax = @AccountNum OR FederalTax = @AccountNum OR FederalACCT = @AccountNum OR RuralAccountNum = @AccountNum
--End of Delete Check

 if (@RecordUsed <> 0 OR @RecordUsedInCompany <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 else
 Begin
	SET @LastUpdTS = GETUTCDATE()  
UPDATE [Account]  
   SET   
           LastUpdUID=@LastUpdUID  
     ,LastUpdTS=@LastUpdTS  
           ,IsDeleted=@IsDeleted  
 WHERE CustomerID =@CustomerID and AccountNum=@AccountNum  
  End
end
GO

