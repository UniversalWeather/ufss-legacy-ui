/****** Object:  StoredProcedure [dbo].[spAddFPEmailTranLog]    Script Date: 08/29/2012 16:50:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFPEmailTranLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFPEmailTranLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    

CREATE Procedure [dbo].[spAddFPEmailTranLog]      
 (      
	@EmailTmplID [bigint],      
	@EmailReqUID [varchar](30),  
	@EmailType [varchar] (1),
	@CustomerID [bigint],      
	@EmailStatus [varchar](18),      
	@EmailSentTo [varchar](100),      
	@LastUPDUID [varchar](30),      
	@EmailBody [nvarchar](max),    
	@EmailSubject [nvarchar](2000),
	@MeetingStartTime [datetime], 
	@MeetingEndTime [datetime], 
	@MeetingLocation [varchar] (2000),
	@IsBodyHTML [bit]
  )        
-- =============================================        
-- Author:Vishwa        
-- Create date: 11/08/2012        
-- Altered by       
-- Description: Insert Email Log      
-- =============================================        
AS          
 BEGIN       
 SET NoCOUNT ON      
 DECLARE @EmailTranID BIGINT      
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @EmailTranID OUTPUT      
          
   INSERT INTO FPEmailTranLog         
   (    
	[EmailTranID], 
	[EmailTmplID],
	[EmailReqUID], 
	[EmailType], 
	[CustomerID],
	[EmailReqTime],
	[EmailStatus], 
	[EmailSentTo], 
	[LastUPDUID], 
	[LastUpdTS], 
	[IsDeleted], 
	[EmailBody], 
	[EmailSubject], 
	[MeetingStartTime], 
	[MeetingEndTime], 
	[MeetingLocation],
	[IsBodyHTML], 
	[IsInActive] 
   )              
     VALUES        
   (      
   @EmailTranID,      
   @EmailTmplID,      
   @EmailReqUID,  
   @EmailType,
   @CustomerID,      
   GETUTCDATE(),      
   @EmailStatus,      
   @EmailSentTo,      
   @LastUPDUID,      
   GETUTCDATE(),      
   0,    
   @EmailBody,    
   @EmailSubject,
   @MeetingStartTime, 
   @MeetingEndTime, 
   @MeetingLocation,
   @IsBodyHTML, 
   0
   )        
        
END  
GO


