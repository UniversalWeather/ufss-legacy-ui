IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllLostBusiness]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllLostBusiness]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllLostBusiness]    Script Date: 20/02/2013 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllLostBusiness](@CustomerID BIGINT)  
AS  
-- =============================================  
-- Author:Ramesh.J  
-- Create date: 20/02/2013  
-- Description: Get the CQLostBusiness information  
-- =============================================  
SET NOCOUNT ON  
IF len(@CustomerID) >0  
BEGIN   
 SELECT  CQLostBusinessID  
   ,CustomerID      
   ,CQLostBusinessDescription       
   ,LastUpdUID   
   ,LastUpdTS      
   ,IsDeleted   
   ,CQLostBusinessCD         
 FROM  CQLostBusiness   
 WHERE CustomerID=@CustomerID  AND IsDeleted = 'false'   
 ORDER BY CQLostBusinessCD ASC  
  
END  
GO
