/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdateCrewCalendarEntries]    Script Date: 01/10/2013 18:38:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdateCrewCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdateCrewCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdateCrewCalendarEntries]    Script Date: 01/10/2013 18:38:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author: Veerendra  
-- Create date: 24/7/2012 
-- Description: Update the Crew Calendar Entry 
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdateCrewCalendarEntries]
(
@FleetID BIGINT,
@CustomerID BIGINT,
@TailNum CHAR(6),
@HomebaseID BIGINT,
@LastUpdUID CHAR(30),
@DepartureDTTMLocal DATETIME,
@ArrivalDTTMLocal DATETIME,
@DepartureGreenwichDTTM DATETIME,
@ArrivalGreenwichDTTM DATETIME,
@HomeDepartureDTTM DATETIME,
@HomeArrivalDTTM DATETIME,
@DepartICAOID BIGINT,
@DutyTYPE CHAR(2),
@Notes TEXT,
@ClientID BIGINT,
@PreviousNUM BIGINT,
@LegID VARCHAR(400),
@TripID VARCHAR(400),
@CrewIDs VARCHAR(400),
@NoteSec varchar(max)

)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@ClientID=0)
	begin
	set @ClientID=null
	end
	
	if(@FleetID=0)
	begin
	set @FleetID=null
	end
    -- Insert statements for procedure here

	UPDATE PreflightLeg 
		SET DepartureDTTMLocal=@DepartureDTTMLocal,
		ArrivalDTTMLocal=@ArrivalDTTMLocal,
		DepartureGreenwichDTTM=@DepartureGreenwichDTTM,
		ArrivalGreenwichDTTM=@ArrivalGreenwichDTTM,
		HomeDepartureDTTM=@HomeDepartureDTTM,
		HomeArrivalDTTM=@HomeArrivalDTTM,
		DepartICAOID=@DepartICAOID,
		ArriveICAOID=@DepartICAOID,
		DutyTYPE=@DutyTYPE,
		ClientID=@ClientID,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=GETUTCDATE()
	WHERE LegID IN (SELECT items FROM dbo.Split(@LegID,',')) and CustomerID=@CustomerID		

	UPDATE PreflightMain 
		SET Notes=@Notes,
		CrewCalendarNotes=@NoteSec,
		HomebaseID=@HomebaseID,
		ClientID=@ClientID,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=GETUTCDATE(),
		PreviousNUM=@PreviousNUM,
		FleetID=@FleetID		
	WHERE TripID in (SELECT items FROM dbo.Split(@TripID,',')) and CustomerID=@CustomerID and CrewID in(SELECT items FROM dbo.Split(@CrewIDs,','))	
	
	UPDATE PreflightCrewList
		SET LastUpdUID=@LastUpdUID,
		LastUpdTS=GETUTCDATE()
	WHERE CrewID IN(SELECT items FROM dbo.Split(@CrewIDs,',')) and CustomerID=@CustomerID
	AND LegID IN (SELECT items FROM dbo.Split(@LegID,','))

END

GO


