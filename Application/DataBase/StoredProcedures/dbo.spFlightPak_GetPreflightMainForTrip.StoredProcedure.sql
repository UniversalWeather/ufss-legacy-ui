IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetPreflightMainForTrip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetPreflightMainForTrip]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPreflightMainForTrip]    Script Date: 08/24/2012 10:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_GetPreflightMainForTrip]   
(    
 @TripID  BIGINT,  
 @CustomerID BIGINT  
  
 )    
-- =============================================    
-- Author: MohanRaja.C    
-- Create date: 08/05/2012    
-- Description: Get Crew Info based on Crew Code    
-- =============================================    
AS    
SET NOCOUNT ON   
BEGIN    
SELECT [TripID]  
           ,[CustomerID]  
           ,[TripNUM]  
           ,[PreviousNUM]  
           ,[TripDescription]  
           ,[FleetID]  
           ,[DispatchNUM]  
           ,[TripStatus]  
           ,[EstDepartureDT]  
           ,[EstArrivalDT]  
           ,[RecordType]  
           ,[AircraftID]  
           ,[PassengerRequestorID]  
           ,[RequestorFirstName]  
           ,[RequestorMiddleName]  
           ,[RequestorLastName]  
           ,[RequestorPhoneNUM]  
           ,[DepartmentDescription]  
           ,[AuthorizationDescription]  
           ,[RequestDT]  
           ,[IsCrew]  
           ,[IsPassenger]  
           ,[IsQuote]  
           ,[IsScheduledServices]  
           ,[IsAlert]  
           ,[Notes]  
           ,[LogisticsHistory]  
           ,[HomebaseID]  
           ,[ClientID]  
           ,[IsAirportAlert]  
           ,[IsLog]  
           ,[BeginningGMTDT]  
           ,[EndingGMTDT]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsPrivate]  
           ,[IsCorpReq]  
           ,[Acknowledge]  
           ,[DispatchNotes]  
           ,[TripRequest]  
           ,[WaitList]  
           ,[FlightNUM]  
           ,[FlightCost]  
           ,[AccountID]  
           ,[TripException]  
           ,[TripSheetNotes]  
           ,[CancelDescription]  
           ,[IsCompleted]  
           ,[IsFBOUpdFlag]  
           ,[RevisionNUM]  
           ,[RevisionDescriptioin]  
           ,[IsPersonal]  
           ,[DsptnName]  
           ,[ScheduleCalendarHistory]  
           ,[ScheduleCalendarUpdate]  
           ,[CommentsMessage]  
           ,[EstFuelQTY]  
           ,[CrewID]  
           ,[ReleasedBy]  
           ,[IsDeleted]  
           ,[VerifyNUM]  
           ,[APISSubmit]  
           ,[APISStatus]  
           ,[APISException]  
           ,[IsAPISValid]  
           ,[DepartmentID]  
           ,[AuthorizationID]  
           ,[DispatcherUserName]  
           ,[EmergencyContactID]
           ,FleetCalendarNotes
            ,CrewCalendarNotes
  
       FROM PreflightMain  
       WHERE CustomerID=@CustomerID  
       AND TripID=@TripID    
       END
GO
