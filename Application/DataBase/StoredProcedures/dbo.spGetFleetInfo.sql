
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetFleetInfo](@FleetID INT)
AS  
-- =============================================  
-- Author: Badrinath  
-- Create date: 7/5/2012  
-- Description: Get All Fleet Profile Informations  
-- =============================================  
SET NOCOUNT ON  
  
SELECT [FleetID]
           ,[TailNum]
           ,[CustomerID]
           ,[AircraftCD]
           ,[SerialNum]
           ,[TypeDescription]
           ,[MaximumReservation]
           ,[LastInspectionDT]
           ,[InspectionHrs]
           ,[WarningHrs]
           ,[MaximumPassenger]
           ,[HomebaseID]
           ,[MaximumFuel]
           ,[MinimumFuel]
           ,[BasicOperatingWeight]
           ,[MimimumRunway]
           ,[IsInActive]
           ,[IsDisplay31]
           ,[SIFLMultiControlled]
           ,[SIFLMultiNonControled]
           ,[Notes]
           ,[ComponentCD]
           ,[ClientID]
           ,[FleetType]
           ,[FlightPhoneNum]
           ,[Class]
           ,[VendorID]
           ,[VendorType]
           ,[YearMade]
           ,[ExteriorColor]
           ,[InteriorColor]
           ,[IsAFIS]
           ,[IsUWAData]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[FlightPlanCruiseSpeed]
           ,[FlightPlanMaxFlightLevel]
           ,[MaximumTakeOffWeight]
           ,[MaximumLandingWeight]
           ,[MaximumWeightZeroFuel]
           ,[TaxiFuel]
           ,[MultiSec]
           ,[ForeGrndCustomColor]
           ,[BackgroundCustomColor]
           ,[FlightPhoneIntlNum]
           ,[FleetSize]
           ,[FltScanDoc]
           ,[MinimumDay]
           ,[StandardCrewIntl]
           ,[StandardCrewDOM]
           ,[IsFAR91]
           ,[IsFAR135]
           ,[EmergencyContactID]
           ,[IsTaxDailyAdj]
           ,[IsTaxLandingFee]
           ,[IsDeleted]
           ,[AircraftID]
           ,[CrewID] ,[NationalityCountryID]
FROM  [Fleet] WHERE FleetID=@FleetID AND ISNULL(IsDeleted,0) = 0


--drop proc [spGetAllFleet]
