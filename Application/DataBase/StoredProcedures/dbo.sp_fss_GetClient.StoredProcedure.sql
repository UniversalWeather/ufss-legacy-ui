
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetClient]    Script Date: 02/28/2014 13:05:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetClient]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetClient]    Script Date: 02/28/2014 13:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetClient]    
(    
     @CustomerID BIGINT  
    ,@ClientID BIGINT  
    ,@ClientCD varchar(5)    
    ,@FetchActiveOnly BIT      
)    
AS   
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get All Client details for Popup        
-- Exec sp_fss_GetClient 10099,0,'',1  
-- Exec sp_fss_GetClient 10099,0,'',0  
-- Exec sp_fss_GetClient 10099, 100993,NULL, 0  
-- Exec sp_fss_GetClient 10099, 0,'UNIV',0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
 SELECT   
     C.ClientID   
    ,CASE WHEN ClientCD IS NULL THEN '' ELSE LTRIM(RTRIM(ClientCD)) END AS ClientCD  
    ,C.ClientDescription   
    ,C.CustomerID  
    ,C.LastUpdUID    
    ,C.LastUpdTS    
    ,ISNULL(C.IsDeleted,0) IsDeleted   
    ,ISNULL(C.IsInactive,0) IsInactive     
 FROM Client C   
 WHERE C.CustomerID = @CustomerID   
 AND C.ClientID		= case when @ClientID <> 0 then @ClientID else C.ClientID end   
 AND C.ClientCD		= case when Ltrim(Rtrim(@ClientCD)) <> '' then @ClientCD else C.ClientCD end                                    
 AND ISNULL(C.IsInActive,0) = case when @FetchActiveOnly = 0 then ISNULL(C.IsInActive,0) else 0 end  
 AND ISNULL(C.IsDeleted,0)	= 0                                  
 ORDER BY C.ClientCD ASC  
      
END  
  
  
   
   
GO

