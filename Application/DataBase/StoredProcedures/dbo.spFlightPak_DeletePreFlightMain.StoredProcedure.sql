IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_DeletePreFlightMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_DeletePreFlightMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_DeletePreFlightMain] (
 @TripID BIGINT
 ,@LastUpdUID CHAR(30)
 ,@LastUpdTS DATETIME
 ,@IsDeleted BIT
 )
AS
BEGIN
 SET NOCOUNT ON
  
 UPDATE PreFlightMain
 SET [LastUpdUID] = @LastUpdUID
  ,[LastUpdTS] = @LastUpdTS
  ,[IsDeleted] = 1
 WHERE TripID = @TripID
END
GO


