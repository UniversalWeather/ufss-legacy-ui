/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRHotelList]    Script Date: 04/25/2013 17:45:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_AddCRHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRHotelList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRHotelList]
(--@CRHotelListID BIGINT,
@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@HotelID BIGINT
,@CRHotelListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@FaxNum VARCHAR(25)
,@Email VARCHAR(250)
,@Rate numeric(6, 2)
)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
	DECLARE @CRHotelListID BIGINT
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRHotelListID OUTPUT
		
	INSERT INTO CRHotelList
		([CRHotelListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[HotelID]
		,[CRHotelListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted]
		,[FaxNum]
		,[Email]
		,[Rate]
		)
	VALUES
		(@CRHotelListID
		,@CustomerID
		,@CRLegID
		,@TripNUM
		,@LegID
		,@RecordType
		,@HotelID
		,@CRHotelListDescription
		,@PhoneNUM
		,@IsCompleted
		,@AirportID
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted
		,@FaxNum
		,@Email
		,@Rate
		)
		
	SELECT @CRHotelListID AS CRHotelListID
	
END
GO