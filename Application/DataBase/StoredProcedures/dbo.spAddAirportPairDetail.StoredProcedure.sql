
GO
/****** Object:  StoredProcedure [dbo].[spAddAirportPairDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddAirportPairDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddAirportPairDetail]
GO

CREATE PROCEDURE spAddAirportPairDetail
(@CustomerID BIGINT,
@AirportPairID BIGINT,
@LegID INT,
@AircraftID BIGINT,
@QTR NUMERIC(1),
@DAirportID BIGINT,
@AAirportID BIGINT,
@Distance NUMERIC(5),
@PowerSetting VARCHAR(1),
@TakeoffBIAS NUMERIC(6,3),
@LandingBIAS NUMERIC(6,3),
@TrueAirSpeed NUMERIC(3),
@Winds NUMERIC(5),
@ElapseTM NUMERIC(7,3),
@Cost NUMERIC(12,2),
@WindReliability INT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	DECLARE @AirportPairDetailID BIGINT
	SET @LastUpdTS = GETUTCDATE()
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'UtilityCurrentNo',  @AirportPairDetailID OUTPUT
	
	INSERT INTO AirportPairDetail
		(AirportPairDetailID,
		CustomerID,
		AirportPairID,
		LegID,
		AircraftID,
		QTR,
		DAirportID,
		AAirportID,
		Distance,
		PowerSetting,
		TakeoffBIAS,
		LandingBIAS,
		TrueAirSpeed,
		Winds,
		ElapseTM,
		Cost,
		WindReliability,
		LastUpdUID,
		LastUpdTS,
		IsDeleted)
	VALUES
		(@AirportPairDetailID,
		@CustomerID,
		@AirportPairID,
		@LegID,
		@AircraftID,
		@QTR,
		@DAirportID,
		@AAirportID,
		@Distance,
		@PowerSetting,
		@TakeoffBIAS,
		@LandingBIAS,
		@TrueAirSpeed,
		@Winds,
		@ElapseTM,
		@Cost,
		@WindReliability,
		@LastUpdUID,
		@LastUpdTS,
		@IsDeleted)
	
	SELECT @AirportPairDetailID AS AirportPairDetailID

END