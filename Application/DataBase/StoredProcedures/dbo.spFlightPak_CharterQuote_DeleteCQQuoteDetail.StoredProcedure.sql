

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteDetail]
(
	@CQQuoteDetailID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQQuoteDetail] WHERE [CQQuoteDetailID] = @CQQuoteDetailID AND [CustomerID] = @CustomerID
GO
