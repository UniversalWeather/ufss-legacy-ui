/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRHistory]    Script Date: 04/24/2013 19:46:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_AddCRHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRHistory]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRHistory](
	 @CRMainID bigint
	,@CustomerID bigint
	,@LogisticsHistory varchar(MAX)
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
	,@IsDeleted bit	
)    
AS    
BEGIN   
	DECLARE @CRHistoryID  BIGINT  
	DECLARE @RevisionNumber  int   
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRHistoryID OUTPUT
	
	select top 1 @RevisionNumber = isnull([RevisionNumber],0) +1 from CRHistory where CRMainID = @CRMainID order by LastUpdTS desc
	
	set @RevisionNumber = ISNULL(@RevisionNumber,1)     

	
	INSERT INTO CRHistory
	       ([CRHistoryID]
           ,[CustomerID]
           ,[CRMainID]
           ,[LogisticsHistory]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[RevisionNumber]
           )
	VALUES(@CRHistoryID
		,@CustomerID
		,@CRMainID
		,@LogisticsHistory
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted
		,@RevisionNumber
	)    

	SELECT @CRHistoryID as [CRHistoryID]
END
GO