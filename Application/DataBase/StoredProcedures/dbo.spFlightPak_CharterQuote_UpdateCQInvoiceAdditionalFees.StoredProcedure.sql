IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceAdditionalFees]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceAdditionalFees]
(
	@CQInvoiceAdditionalFeesID bigint,
	@CustomerID bigint,
	@CQInvoiceMainID bigint,
	@QuoteID int,
	@InvoiceNUM int,
	@FeeIdentification int,
	@IsPrintable bit,
	@IsTaxable bit,
	@CQInvoiceFeeDescription varchar(60),
	@QuoteAmount numeric(17, 2),
	@InvoiceAmt numeric(17, 2),
	@OrderNUM int,
	@IsDiscount bit,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@Quantity numeric(17,3) ,
	@ChargeUnit varchar(25),
	@Rate numeric(17,2)
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQInvoiceAdditionalFees] SET [InvoiceNUM] = @InvoiceNUM, [FeeIdentification] = @FeeIdentification, [IsPrintable] = @IsPrintable, [IsTaxable] = @IsTaxable, [CQInvoiceFeeDescription] = @CQInvoiceFeeDescription, [QuoteAmount] = @QuoteAmount, [InvoiceAmt] = @InvoiceAmt, [OrderNUM] = @OrderNUM, [IsDiscount] = @IsDiscount, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, [Quantity] = @Quantity, [ChargeUnit] = @ChargeUnit,[Rate] = @Rate 
		WHERE [CQInvoiceAdditionalFeesID] = @CQInvoiceAdditionalFeesID AND [CustomerID] = @CustomerID

GO


