IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQFile]
(
	@CQFileID bigint,
	@CustomerID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime
)
AS
	SET NOCOUNT ON;
	update [CQFile] set IsDeleted = 1,
		LastUpdTS =GETUTCDATE(),
		LastUpdUID = @LastUpdUID,
		CustomerID = @CustomerID
	where
		CQFileID = @CQFileID
		and CustomerID = @CustomerID
	

GO
