

/****** Object:  StoredProcedure [dbo].[SpUpdateCompanyInActive]    Script Date: 01/10/2012 11:34:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateCompanyInActive]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateCompanyInActive]
GO


/****** Object:  StoredProcedure [dbo].[SpUpdateCompanyInActive]    Script Date: 01/10/2012 11:34:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SpUpdateCompanyInActive] (@HomeBaseID BIGINT, @HomebaseAirportID BIGINT, @CustomerID BIGINT, @IsInActive BIT, @LastUpdUID VARCHAR(30), @LastUpdTS DATETIME)    
 AS    
 BEGIN        
 
 DECLARE @ICAOID CHAR(4)
 DECLARE @CustomAirportID BIGINT
 DECLARE @UWAAirportID BIGINT
  
 DECLARE @UWACustomerID BIGINT    
 EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output 
 
 select @ICAOID = ICAOID from Airport where AirportID = @HomebaseAirportID
 --Identify Custom AirportID 
 select @CustomAirportID = AirportID from Airport where ICAOID = @ICAOID and CustomerID = @CustomerID and IsDeleted = 0 
 --Identify UWA AirportID
 select @UWAAirportID = AirportID from Airport where ICAOID = @ICAOID and CustomerID = @UWACustomerID and IsDeleted = 0

 
 UPDATE Account SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID    
      
 UPDATE CQCustomer SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID    
 
 UPDATE Crew SET IsStatus = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomebaseAirportID AND CustomerID = @CustomerID     
 UPDATE Crew SET IsStatus = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @UWAAirportID AND CustomerID = @CustomerID   
 UPDATE Crew SET IsStatus = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @CustomAirportID AND CustomerID = @CustomerID   
     
 UPDATE CrewGroup SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
     
 UPDATE DepartmentGroup SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
      
 UPDATE Fleet SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
     
 UPDATE FleetGroup SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
     
 UPDATE Passenger SET IsActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomebaseAirportID AND CustomerID = @CustomerID     
 UPDATE Passenger SET IsActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @UWAAirportID AND CustomerID = @CustomerID     
 UPDATE Passenger SET IsActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @CustomAirportID AND CustomerID = @CustomerID     
     
 UPDATE PassengerGroup SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
     
 UPDATE TravelCoordinator SET IsInActive = @IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomeBaseID AND CustomerID = @CustomerID     
     
 UPDATE Vendor SET IsInActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @HomebaseAirportID AND CustomerID = @CustomerID     
 UPDATE Vendor SET IsInActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @UWAAirportID AND CustomerID = @CustomerID 
 UPDATE Vendor SET IsInActive = ~@IsInActive, LastUpdUID = @LastUpdUID, LastUpdTS = @LastUpdTS  WHERE HomebaseID = @CustomAirportID AND CustomerID = @CustomerID 
	
END