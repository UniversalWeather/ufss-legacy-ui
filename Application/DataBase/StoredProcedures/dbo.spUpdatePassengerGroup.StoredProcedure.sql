
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePassengerGroup]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePassengerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePassengerGroup]
go
CREATE procedure [dbo].[spUpdatePassengerGroup]
(@PassengerGroupID	bigint
,@CustomerID	bigint
,@HomebaseID	bigint
,@PassengerGroupCD	char(4)
,@PassengerGroupName	varchar(30)
,@LastUpdUID varchar(30)
,@LastUpdTS	datetime
,@IsDeleted	bit
,@IsInActive bit)
-- =============================================
-- Author: MohanRaja.C
-- Create date: 10/4/2012
-- Description: Update the Passenger Group information
-- =============================================
as
begin 
SET NoCOUNT ON
set @LastUpdTS = GETUTCDATE()
	UPDATE PassengerGroup
	SET 
      [PassengerGroupName] = @PassengerGroupName,
      [HomebaseID] = @HomebaseID,
      [LastUpdUID] = @LastUpdUID,
      [LastUpdTS] = @LastUpdTS,
      [IsDeleted] = @IsDeleted,
      [IsInActive] = @IsInActive
	WHERE [CustomerID] = @CustomerID and [PassengerGroupID]=@PassengerGroupID 

END
GO
