/****** Object:  StoredProcedure [dbo].[spGetAllFlightCategories]    Script Date: 01/11/2013 15:06:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFlightCategories]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFlightCategories]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllFlightCategories]    Script Date: 01/11/2013 15:06:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllFlightCategories](@CustomerID bigint,@ClientID bigint)  
AS  
-- =============================================  
-- Author: Sujitha.V  
-- Create date: 16/4/2012  
-- Description: Get All Flight Category Informations  
-- =============================================  
SET NOCOUNT ON  
IF(@ClientID>0)  
BEGIN  
SELECT      fc.[FlightCategoryID]  
           ,fc.[FlightCatagoryCD]  
           ,fc.[FlightCatagoryDescription]  
           ,fc.[CustomerID]  
           ,fc.[GraphColor]  
           ,fc.[ClientID]  
           ,fc.[LastUpdUID]  
           ,fc.[LastUpdTS]  
           ,fc.[ForeGrndCustomColor]  
           ,fc.[BackgroundCustomColor]  
           ,fc.[CallKey]  
           ,fc.[IsInActive]  
           ,fc.[IsDeleted]  
           ,fc.[IsDeadorFerryHead]
           ,c.ClientCD  
FROM  [FlightCatagory] fc left outer join Client c on fc.ClientID =c.clientid  WHERE fc.CustomerID=@CustomerID AND ISNULL(fc.IsDeleted,0) = 0 and fc.ClientID=@ClientID  
order by FlightCatagoryCD
END  
ELSE  
BEGIN  
SELECT      fc.[FlightCategoryID]  
           ,fc.[FlightCatagoryCD]  
           ,fc.[FlightCatagoryDescription]  
           ,fc.[CustomerID]  
           ,fc.[GraphColor]  
           ,fc.[ClientID]  
           ,fc.[LastUpdUID]  
           ,fc.[LastUpdTS]  
           ,fc.[ForeGrndCustomColor]  
           ,fc.[BackgroundCustomColor]  
           ,fc.[CallKey]  
           ,fc.[IsInActive]  
           ,fc.[IsDeleted]  
           ,fc.[IsDeadorFerryHead]
           ,c.ClientCD  
FROM  [FlightCatagory] fc left outer join Client c on fc.ClientID =c.clientid  WHERE fc.CustomerID=@CustomerID AND ISNULL(fc.IsDeleted,0) = 0   
order by FlightCatagoryCD
END

GO


