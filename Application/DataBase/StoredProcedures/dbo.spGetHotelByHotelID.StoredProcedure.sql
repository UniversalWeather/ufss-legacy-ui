
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHotelByHotelID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHotelByHotelID]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[spGetHotelByHotelID]( @HotelID BIGINT)  
AS  
-- =============================================  
-- Author: Leela M 
-- Create date: 09/10/2012  
-- Description: Get Hotel By HotelID
-- =============================================  
SET NOCOUNT ON  
--IF len(@CustomerID) >0  
-- BEGIN   

--DECLARE @HotelICAOID CHAR(4)
--DECLARE @UWACustomerID BIGINT

--SELECT @HotelICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID
--EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output

 SELECT		 H.[HotelID]
			,H.[AirportID]
			,H.[HotelCD]
			,H.[CustomerID]
			,[IsChoice]
			,[Name]
			,[PhoneNum]
			,[MilesFromICAO]
			,[HotelRating]
			,[FaxNum]
			,[NegociatedRate]
			,[Addr1]
			,[Addr2]
			,[Addr3]
			,H.[CityName]
			,H.[MetroID]
			,H.[StateName]
			,[PostalZipCD]
			,H.[CountryID]
			,[ContactName]
			,[Remarks]
			,[IsPassenger]
			,[IsCrew]
			,[IsUWAMaintained]
			,H.[LatitudeDegree]
			,H.[LatitudeMinutes]
			,H.[LatitudeNorthSouth]
			,H.[LongitudeDegrees]
			,H.[LongitudeMinutes]
			,H.[LongitudeEastWest]
			,H.[LastUpdUID]
			,H.[LastUpdTS]
			,H.[UpdateDT]
			,H.[RecordType]
			,[MinutesFromICAO]
			,[SourceID]
			,[ControlNum]
			,[Website]
			,H.[UWAMaintFlag]
			,H.[UWAID]
			,H.[IsInActive]
			,H.[IsDeleted]
			,H.[UWAUpdates]
			,Ai.IcaoID			
			,m.MetroCD
			,c.CountryCD 
			,H.[AltBusinessPhone]
		   ,H.[TollFreePhone]
		   ,H.[BusinessEmail]
		   ,H.[ContactBusinessPhone]
		   ,H.[CellPhoneNum]
		   ,H.[ContactEmail]  
		,H.ExchangeRateID  
     ,H.NegotiatedTerms  
     ,H.SundayWorkHours  
     ,H.MondayWorkHours  
     ,H.TuesdayWorkHours  
     ,H.WednesdayWorkHours  
     ,H.ThursdayWorkHours  
     ,H.FridayWorkHours  
     ,H.SaturdayWorkHours


	FROM  [Hotel] H
	inner join [Airport] Ai on H.AirportID = Ai.AirportID 
	LEFT OUTER JOIN metro m on m.MetroID= H.MetroID 
	LEFT OUTER JOIN Country c on c.CountryID = H.CountryID
	
	
	WHERE 
	H.HotelID = @HotelID
	  

GO


