
/****** Object:  StoredProcedure [dbo].[FlightPakHelpContentSearch]    Script Date: 02/15/2013 19:11:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FlightPakHelpContentSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FlightPakHelpContentSearch]
GO

/****** Object:  StoredProcedure [dbo].[FlightPakHelpContentSearch]    Script Date: 02/15/2013 19:11:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      
CREATE PROC FlightPakHelpContentSearch
@SearchStr varchar(100)
AS
BEGIN
SELECT Identifier, Category, Topic, '...' + SUBSTRING(dbo.RemoveHTML(Content,null), CHARINDEX(@SearchStr, dbo.RemoveHTML(Content,null)) - 100, 200) + ' ...' As Content
from HelpContent where dbo.RemoveHTML(Content,null) like '%' + @SearchStr + '%' and IsDeleted = 0 
END
GO


