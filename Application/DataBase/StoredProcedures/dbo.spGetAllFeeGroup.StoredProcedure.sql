
/****** Object:  StoredProcedure [dbo].[spGetAllFeeGroup]    Script Date: 02/15/2013 19:13:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFeeGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFeeGroup]
GO

USE [FP_Rel2]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllFeeGroup]    Script Date: 02/15/2013 19:13:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllFeeGroup](@CustomerID bigint)    
as    
-- =============================================    
-- Author:Suresh Jinka    
-- Create date: 05/02/2013    
-- Description: Gets the Lead Source catalog information    
-- =============================================    
set nocount on    
if len(@CustomerID) >0    
begin     
 SELECT  FeeGroupID     
   ,CustomerID     
   ,FeeGroupCD    
   ,FeeGroupDescription     
   ,IsInActive         
   ,LastUpdUID     
   ,LastUpdTS     
   ,IsDeleted    
 FROM  FeeGroup     
 WHERE CustomerID=@CustomerID  and IsDeleted = 'false'     
 ORDER BY FeeGroupCD asc    
    
END    
  
  
GO


