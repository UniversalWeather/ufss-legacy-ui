
/****** Object:  StoredProcedure [dbo].[spGetAllFBOByAirportID]    Script Date: 10/03/2013 21:00:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFBOByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFBOByAirportID]
GO


CREATE PROCEDURE [dbo].[spGetAllFBOByAirportID](@CustomerID BIGINT, @AirportID BIGINT)              
AS              
-- ============================================= 
-- Author: Prakash P
-- Create date: 11/09/2012 
-- Description: Get the FBO information 
-- ============================================= 
-- =============================================
-- Changed By: Rajitha Fernando
-- Changed Date: 12/22/2014	
-- Description: Union All is removed to increase the performance
-- =============================================
--DECLARE @AirportICAOID CHAR(4)
DECLARE @UWACustomerID BIGINT

--SELECT @AirportICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output

DECLARE @ICAOCD VARCHAR(50)
DECLARE @UWAAirportId VARCHAR(50)
DECLARE @ICAOCD1 VARCHAR(50)
DECLARE @CustomAirportID VARCHAR(50)
select @ICAOCD =icaoid from Airport where AirportID = @AirportID
select @UWAAirportId =AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.
select @CustomAirportID =AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD
 
SET NOCOUNT ON
 
SELECT F.[FBOID],
F.[AirportID],
F.[FBOCD],
F.[CustomerID],
F.[IsChoice],
F.[FBOVendor],
F.[PhoneNUM1],
F.[Frequency],
F.[IsUWAAirPartner],
F.[IsCrewCar],
F.[FuelBrand],
F.[ControlNum],
F.[FaxNum],
F.[LastFuelDT],
F.[LastFuelPrice],
F.[NegotiatedFuelPrice],
F.[Addr1],
F.[Addr2],
F.[CityName],
F.[StateName],
F.[PostalZipCD],
F.[CountryID],
F.[Contact],
F.[Remarks],
F.[IsFPK],
F.[FPKUpdateDT],
F.[LastUpdUID],
F.[LastUpdTS],
F.[RecordType],
F.[IsInActive],
F.[PhoneNUM2],
F.[HoursOfOperation],
F.[SourceID],
F.[UpdateDT],F.[IsPrimaryChoice],F.[PostedPrice],F.[PaymentType],F.[FuelQty]
,F.[FixBaseOperatorID],F.[HHEATED],F.[HSJET],F.[HMJET],F.[HLJET],F.[HCJET],F.[FLSJETA],F.[FLS100LL],F.[FLS80],F.[FSSJETA],F.[FSS100LL],F.[FSS80]
,F.[FVOLDISCT],F.[ARFEE],F.[ARFEECOM],F.[AHFEE],F.[AHFEECOM],F.[A24],F.[AQT],F.[ASEC],F.[AddDeIcing],F.[AOX],F.[ANIT],F.[ALAV],F.[AGPU]
,F.[AOWGR],F.[AMMINOR],F.[AMMAJOR],F.[AMAV],F.[AMAUTH],F.[AMAUTHCOM],F.[AICLEAN],F.[AICS],F.[AIIR],F.[AEWAS],F.[AEBRITE],F.[AEPAINT],F.[PWPS]
,F.[PFPR],
F.[PCT],
F.[PCC],
F.[PLOUNGE],
F.[PREC],
F.[PEXEC],
F.[PKIT],
F.[PLAUND],
F.[PSR],
F.[PSHOWER],
F.[PSUPP],
F.[PMEOS],
F.[RHERTZ],
F.[RAVIS],
F.[ROTHER],
F.[ROCOMM],
F.[PPLIMO],
F.[PPTAXI],
F.[PPROS],
F.[PPRWD],
F.[PPCONC],
F.[PPCS],
F.[PPCR],
F.[PPLSR],
F.[PPSEC],
F.[PPCWIA],
F.[PPMS],
F.[PPFS],
F.[PPCA],
F.[PPGOLF],
F.[PPCOFFEE],
F.[PPSNACKS],
F.[PPNEWS],
F.[PPICE],
F.[PXMF],
F.[PXCOFFEE],
F.[PXBAR],
F.[PXCCC],
F.[PXVCC],
F.[PXLOUNGE],
F.[PXGIFT],
F.[OHF],
F.[OPCS],
F.[OTCS],
F.[OJCS],
F.[OHCS],
F.[OAPS],
F.[OCH],
F.[CreditCardVISA],
F.[CreditCardMasterCard],
F.[CreditCardAMEX],
F.[CreditCardDinerClub],
F.[CreditCardJCB],
F.[CreditCardMS],
F.[CreditCardAVCard],
F.[CreditCardUVAir],
F.[COMJET],
F.[TollFreePhoneNum],
F.[SITA],
F.[UNICOM],
F.[ARINC],
F.[IsUVAirPNR],
F.[Website],
F.[EmailAddress],
F.[LatitudeDegree],
F.[LatitudeMinutes],
F.[LatitudeNorthSouth],
F.[LongitudeDegrees],
F.[LongitudeMinutes],
F.[LongitudeEastWest],
(SELECT CAST(CASE WHEN F.[IsUWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END As varchar )) as Filter,
F.[IsUWAMaintFlag],
F.[UWAID],
F.[ReceiptNum],
F.[IsDeleted],
F.[UWAUpdates],
C.CountryCD,
F.ContactBusinessPhone,
F.ContactCellPhoneNum,
F.ContactEmail,
F.Addr3,
F.CustomField1,
F.CustomField2,
F.ExchangeRateID,
F.NegotiatedTerms,
F.SundayWorkHours,
F.MondayWorkHours,
F.TuesdayWorkHours,
F.WednesdayWorkHours,
F.ThursdayWorkHours,
F.FridayWorkHours,
F.SaturdayWorkHours

FROM [FBO] F LEFT OUTER JOIN [Country] C ON 
F.CountryID = C.CountryID

where ((F.IsDeleted = 0 AND ((F.AirportID= @CustomAirportID
AND CustomerID = @CustomerID ) OR (F.AirportID= @UWAAirportId
AND CustomerID = @CustomerID ))) OR (AirportID = @UWAAirportId
AND F.CustomerID = @UWACustomerID AND F.IsDeleted = 0
AND F.FBOCD NOT IN  (SELECT FBOCD FROM FBO
WHERE ((AirportID= @CustomAirportID AND CustomerID = @CustomerID ) or (AirportID= @UWAAirportId
AND CustomerID = @CustomerID ) )  AND IsDeleted = 0 )))

ORDER BY F.FBOVendor --FBOCD
      
GO


