

/****** Object:  StoredProcedure [dbo].[spGetDeptSelectedList]    Script Date: 05/07/2013 12:16:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDeptSelectedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetDeptSelectedList]
GO



/****** Object:  StoredProcedure [dbo].[spGetDeptSelectedList]    Script Date: 05/07/2013 12:16:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDeptSelectedList](@CustomerID BIGINT ,  
              @DeptCode BIGINT)    
-- =============================================    
-- Author: Sujitha.V    
-- Create date: 02/05/2012    
-- Description: Get the Selected Department List    
-- =============================================    
as    
SET NoCOUNT ON    
BEGIN    
    
SELECT [DepartmentID],  
    [DepartmentCD],    
    [DepartmentName],    
    [CustomerID],    
    [ClientID],    
    [LastUpdUID],    
    [LastUpdTS],    
    [IsInActive],    
    [IsDeleted],
    DepartPercentage   
 FROM  [Department]   
 WHERE [Department].[DepartmentID] IN (SELECT DepartmentGroupOrder.DepartmentID FROM DepartmentGroupOrder     
                                                                                WHERE DepartmentGroupOrder.DepartmentGroupID=@DeptCode)     
 AND [Department].CustomerID=@CustomerID  AND [Department].IsDeleted = 0    
end 
GO


