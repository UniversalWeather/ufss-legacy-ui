/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRecentActivityCorporateRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRecentActivityCorporateRequest]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spGetRecentActivityCorporateRequest]    
@UserName VARCHAR(30),    
@MaxRecordCount int    
AS    
BEGIN    
 --SELECT TOP (@MaxRecordCount)  CRMain.*,    
 --fleet.TailNum,      
 --P.PassengerRequestorCD AS PassengerRequestorCD    
 --FROM CRMain     
 --LEFT JOIN Passenger P on CRMain.PassengerRequestorID = P.PassengerRequestorID     
 --LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = CRMain.FleetID      
 --WHERE CRMain.LastUpdUID = @UserName ORDER BY CRMain.LastUpdTS DESC    
 SELECT TOP (@MaxRecordCount) CRMain.CRMainID    ,CRMain.CustomerID    ,CRMain.CRTripNUM    ,CRMain.Approver    ,CRMain.TripID 
 --Preflightmain    
 ,CRMain.TripStatus    ,CRMain.TripSheetStatus    ,CRMain.AcknowledgementStatus    ,CRMain.CharterQuoteStatus    ,CRMain.CorporateRequestStatus    ,
 CRMain.TravelCoordinatorID  
 --TravelCoordinator    
 , CRMain.TravelCoordinatorName    ,CRMain.TravelCoordinatorPhone    ,CRMain.TravelCoordinatorFax    ,CRMain.DispatchNUM    ,CRMain.EstDepartureDT    ,CRMain.EstArrivalDT    ,CRMain.RecordType    ,CRMain.FleetID  
 -- Fleet    
 ,CRMain.AircraftID 
 -- Aircraft    
 ,CRMain.PassengerRequestorID 
 --Passenger    
 ,CRMain.RequestorName    
 ,CRMain.RequestorPhone    ,CRMain.DepartmentID 
 --Department    
 ,CRMain.DepartmentDescription    ,CRMain.AuthorizationID  
 --Authorization    
 ,CRMain.AuthorizationDescription    ,CRMain.HomebaseID  
 --Company    
 ,CRMain.ClientID  
 --Client    
 ,CRMain.IsPrivate    
 ,CRMain.CRMainDescription    ,CRMain.RequestDT    ,CRMain.BeginningGMTDT    ,CRMain.EndingGMTDT    ,CRMain.IsCrew    ,CRMain.IsPassenger    ,CRMain.IsAlert    ,CRMain.IsAirportAlert    ,CRMain.IsLog    ,CRMain.LastUpdUID    ,CRMain.LastUpdTS    ,CRMain.IsDeleted     
   ,PreflightMain.TripNUM  
   ,PreflightMain.TripDescription  
   ,TravelCoordinator.TravelCoordCD  
   ,Fleet.TailNum  
   ,Fleet.TypeDescription  
   ,Aircraft.AircraftCD  
   ,Aircraft.AircraftDescription  
   ,Passenger.PassengerRequestorCD  
   ,Passenger.PassengerName  
   ,Department.DepartmentCD  
   ,Department.DepartmentName  
   ,DepartmentAuthorization.AuthorizationCD  
   ,DepartmentAuthorization.DeptAuthDescription  
   ,Company.BaseDescription  
   ,Airport.IcaoID  
   ,Client.ClientCD  
   ,Client.ClientDescription  

 FROM CRMain  
 LEFT OUTER JOIN PreflightMain ON PreflightMain.TripID = CRMain.TripID  
 LEFT OUTER JOIN TravelCoordinator ON TravelCoordinator.TravelCoordinatorID = CRMain.TravelCoordinatorID  
 LEFT OUTER JOIN Fleet ON Fleet.FleetID = CRMain.FleetID  
 LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = CRMain.AircraftID  
 LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = CRMain.PassengerRequestorID  
 LEFT OUTER JOIN Department ON Department.DepartmentID = CRMain.DepartmentID  
 LEFT OUTER JOIN [DepartmentAuthorization] ON [DepartmentAuthorization].AuthorizationID = CRMain.AuthorizationID  
 LEFT OUTER JOIN Company ON Company.HomebaseID = CRMain.HomebaseID  
 LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID  
 LEFT OUTER JOIN Client ON Client.ClientID = CRMain.ClientID   
 WHERE CRMain.LastUpdUID = @UserName
  AND CRMain.IsDeleted = 0  
  --AND CRMain.CorporateRequestStatus = 'S' 
  ORDER BY CRMain.LastUpdTS DESC    
END

GO


