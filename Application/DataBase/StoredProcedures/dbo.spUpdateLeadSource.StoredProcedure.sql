/****** Object:  StoredProcedure [dbo].[spUpdateLeadSource]    Script Date: 02/11/2013 14:57:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateLeadSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateLeadSource]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateLeadSource]    Script Date: 02/11/2013 14:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO  
    
    
CREATE procedure [dbo].[spUpdateLeadSource]        
(        
 @LeadSourceID bigint        
,@CustomerID bigint        
,@LeadSourceCD char(4)        
,@LeadSourceDescription varchar(100)  
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime        
,@IsDeleted bit        
,@IsInActive bit    
)        
-- =============================================        
-- Author:Sujitha.V        
-- Create date: 12/4/2012        
-- Description: Update the DelayType  information        
-- =============================================        
as        
begin         
SET NoCOUNT ON        
DECLARE @currentTime datetime          
SET @currentTime = GETUTCDATE()				
SET @LastUpdTS = @currentTime          
UPDATE [LeadSource]        
   SET         
            LeadSourceCD=@LeadSourceCD      
           ,[LeadSourceDescription]=@LeadSourceDescription        
           ,[LastUpdUID]=@LastUpdUID        
           ,[LastUpdTS]=@LastUpdTS        
           ,[IsDeleted]=@IsDeleted  
           ,[IsInActive]=@IsInActive                                
 WHERE CustomerID =@CustomerID and LeadSourceID=@LeadSourceID        
        
end     
    