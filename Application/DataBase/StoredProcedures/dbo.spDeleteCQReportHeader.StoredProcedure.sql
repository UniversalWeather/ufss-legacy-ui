

/****** Object:  StoredProcedure [dbo].[spDeleteCQReportHeader]    Script Date: 04/24/2013 19:00:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCQReportHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCQReportHeader]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteCQReportHeader]    Script Date: 04/24/2013 19:00:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteCQReportHeader]
(@CQReportHeaderID bigint,
@CustomerID bigint,
@ReportID varchar(8),
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit)
AS BEGIN
	
	SET @IsDeleted = 1
	UPDATE	CQReportHeader
	SET		LastUpdUID=@LastUpdUID,
			LastUpdTS=@LastUpdTS,
			IsDeleted=@IsDeleted
	WHERE	CQReportHeaderID=@CQReportHeaderID

END
GO


