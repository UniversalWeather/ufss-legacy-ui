
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleetGroup]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetGroup]
GO

CREATE procedure [dbo].[spGetAllFleetGroup](@CustomerID bigint)        
as        
SET NoCOUNT ON        
        
    SELECT     FleetGroup.FleetGroupID,       
               FleetGroup.CustomerID,      
               FleetGroup.HomebaseID,       
               CASE WHEN FleetGroup.FleetGroupCD IS NULL THEN '' ELSE LTRIM(RTRIM(FleetGroup.FleetGroupCD)) END AS FleetGroupCD,      
               FleetGroup.FleetGroupDescription,       
               FleetGroup.LastUpdUID,       
               FleetGroup.LastUpdTS,       
               FleetGroup.IsDeleted,
               FleetGroup.IsInactive,     
               Airport.ICAOID AS HomebaseCD          
  FROM         FleetGroup LEFT OUTER JOIN      
               Company ON FleetGroup.HomebaseID = Company.HomebaseID       
               LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID          
               WHERE FleetGroup.CustomerID=@CustomerID AND IsNull(FleetGroup.IsDeleted,0)=0  
               Order by FleetGroup.FleetGroupCD 