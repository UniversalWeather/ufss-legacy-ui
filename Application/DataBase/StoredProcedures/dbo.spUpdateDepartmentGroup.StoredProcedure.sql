
/****** Object:  StoredProcedure [dbo].[spUpdateDepartmentGroup]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateDepartmentGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateDepartmentGroup]
go
CREATE PROCEDURE [dbo].[spUpdateDepartmentGroup]
(
	@DepartmentGroupID BIGINT,
	@CustomerID BIGINT,
	@DepartmentGroupCD  char(4),
	@DepartmentGroupDescription varchar(40),
	@HomeBaseID BIGINT,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@IsInActive bit
)
-- =============================================
-- Author: Sujitha.V
-- Create date: 10/5/2012
-- Description: Update the Department Group information
-- =============================================
as
begin 
SET NoCOUNT ON
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
	UPDATE DepartmentGroup
	SET 
      [DepartmentGroupDescription] = @DepartmentGroupDescription,
      [HomebaseID] = @HomeBaseID,
      [LastUpdUID] = @LastUpdUID,
      [LastUpdTS] = @LastUpdTS,
      [IsDeleted] = @IsDeleted,
      [IsInActive] = @IsInActive
	WHERE [DepartmentGroupID] = @DepartmentGroupID

END
GO
