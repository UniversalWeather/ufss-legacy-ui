

/****** Object:  StoredProcedure [dbo].[spUpdateFleet]    Script Date: 03/18/2013 12:36:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFleet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFleet]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateFleet]    Script Date: 03/18/2013 12:36:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
      
CREATE PROCEDURE [dbo].[spUpdateFleet]          
(          
 @FleetID BIGINT,        
 @TailNum VARCHAR(9),         
 @CustomerID BIGINT,          
 @AircraftCD CHAR(4),          
 @SerialNum VARCHAR(20),          
 @TypeDescription VARCHAR(15),          
 @MaximumReservation NUMERIC(3,0),          
 @LastInspectionDT DATE,          
 @InspectionHrs NUMERIC(4,0),          
 @WarningHrs NUMERIC(4,0),          
 @MaximumPassenger NUMERIC(3,0),          
 @HomebaseID BIGINT,          
 @MaximumFuel NUMERIC(6,0),          
 @MinimumFuel NUMERIC(6,0),          
 @BasicOperatingWeight NUMERIC(6,0),          
 @MimimumRunway NUMERIC(5,0),          
 @IsInActive BIT,          
 @IsDisplay31 BIT,          
 @SIFLMultiControlled NUMERIC(6,1),          
 @SIFLMultiNonControled NUMERIC(6,1),          
 @Notes varchar(max),          
 @ComponentCD INT,          
 @ClientID BIGINT,          
 @FleetType CHAR(1),          
 @FlightPhoneNum VARCHAR(25),          
 @Class NUMERIC(1,0),          
 @VendorID BIGINT,         
 @VendorType CHAR(1),         
 @YearMade CHAR(4),          
 @ExteriorColor VARCHAR(25),          
 @INTeriorColor VARCHAR(25),          
 @IsAFIS BIT,          
 @IsUWAData BIT,          
 @LastUpdUID VARCHAR(30),          
 @LastUpdTS datetime,          
 @FlightPlanCruiseSpeed VARCHAR(10),          
 @FlightPlanMaxFlightLevel NUMERIC(5,0),          
 @MaximumTakeOffWeight NUMERIC(6,0),          
 @MaximumLandingWeight NUMERIC(6,0),          
 @MaximumWeightZeroFuel NUMERIC(6,0),          
 @TaxiFuel NUMERIC(6,0),          
 @MultiSec NUMERIC(6,1),          
 @ForeGrndCustomColor varCHAR(8),          
 @BackgroundCustomColor varCHAR(8),          
 @FlightPhoneINTlNum varCHAR(25),          
 @FleetSize CHAR(1),          
 @FltScanDoc varchar(max),          
 --@MinimumDay NUMERIC(17,2),          
 --@StandardCrewINTl NUMERIC(2,0),          
 --@StandardCrewDOM NUMERIC(2,0),          
 @IsFAR91 BIT,          
 @IsFAR135 BIT,           
 @EmergencyContactID BIGINT,          
 --@IsTaxDailyAdj BIT,          
 --@IsTaxLandingFee BIT,          
 @IsDeleted BIT,        
 @AircraftID BIGINT,        
 @CrewID BIGINT ,      
 @SecondaryDomFlightPhone varchar(25),      
 @SecondaryIntlFlightPhone varchar(25),  
 @marginalpercentage numeric(7,2),
 @NationalityCountryID bigint=null        
)          
-- =============================================          
-- Author: Badrinath          
-- Create date: 7/5/2012          
-- Description: Update the Fleet Profile information      
-- Modification History    
-- Modified BY: Ramesh on 27th Jan 2013    
-- Description of change: Changed the HomebaseId logic        
-- Modified BY: Karthikeyan on 10th Oct 2013      
-- Description of change: Changed the Tail No Datatype      
-- =============================================          
AS          
BEGIN           
SET NoCOUNT ON        
  DECLARE @NewHomeBaseId BIGINT      
  SET @LastUpdTS = GETUTCDATE()     
    
  BEGIN TRAN T1   
  IF (@HomebaseID IS NOT NULL)        
  BEGIN        
  IF EXISTS(SELECT CM.HomebaseId FROM Company CM WHERE CM.HomeBaseAirportID = @HomebaseID AND CM.CustomerID=@CustomerID)                
  BEGIN    
    SELECT @NewHomeBaseId = CM.HomebaseId FROM Company CM WHERE CM.HomeBaseAirportID = @HomebaseID AND CM.CustomerID=@CustomerID    
  END    
  ELSE    
  BEGIN   
    DECLARE @CheckICAOID Char(4)    
    DECLARE @UWACustomerID BIGINT    
    DECLARE @AirportID BIGINT  
      
    SELECT @UWACustomerID  = dbo.GetUWACustomerID()     
    
    --To get ICAOID form Airport    
    SELECT @CheckICAOID = ICAOID FROM Airport WHERE AirportID = @HomebaseID    
     
    --To get AirportID based on ICAOID and UWACustomerID    
    SELECT @AirportID = AirportID FROM Airport WHERE IcaoID = @CheckICAOID AND CustomerID = @UWACustomerID    
     
    --To Check if Company exists for Airport and CustomerID    
    IF EXISTS(SELECT CM.HomebaseId FROM Company CM WHERE CM.HomeBaseAirportID = @AirportID AND CM.CustomerID=@CustomerID)    
    BEGIN    
      --SET @NewHomeBaseId = @AirportID    
      SELECT @NewHomeBaseId = CM.HomebaseId FROM Company CM WHERE CM.HomeBaseAirportID = @AirportID AND CM.CustomerID=@CustomerID  
    END    
    ELSE  --Generate a new company for selected ICAOID  
    BEGIN     
      EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @NewHomeBaseId OUTPUT     
      INSERT INTO Company (HomebaseID, CustomerID, HomebaseAirportID, IsDeleted, LastUpdUID, LastUpdTS) VALUES (@NewHomeBaseId, @CustomerID, @HomebaseID, ~@IsDeleted, @LastUpdUID, @LastUpdTS)     
    END     
  END    
 END  
  ELSE        
  BEGIN        
  SET @NewHomeBaseId = NULL        
  END   
  UPDATE Fleet          
  SET       [FleetID]=@FleetID        
           ,[TailNum]=@TailNum        
           ,[CustomerID]=@CustomerID        
           ,[AircraftCD]=@AircraftCD        
           ,[SerialNum]=@SerialNum        
           ,[TypeDescription]=@TypeDescription        
           ,[MaximumReservation]=@MaximumReservation        
           ,[LastInspectionDT]=@LastInspectionDT        
           ,[InspectionHrs]=@InspectionHrs        
           ,[WarningHrs]=@WarningHrs        
           ,[MaximumPassenger]=@MaximumPassenger        
           --,[HomebaseID]=@HomebaseID        
           ,[HomebaseID]=@NewHomeBaseId    
           ,[MaximumFuel]=@MaximumFuel        
           ,[MinimumFuel]=@MinimumFuel        
           ,[BasicOperatingWeight]=@BasicOperatingWeight        
           ,[MimimumRunway]=@MimimumRunway        
           ,[IsInActive]=@IsInActive        
           ,[IsDisplay31]=@IsDisplay31        
           ,[SIFLMultiControlled]=@SIFLMultiControlled        
           ,[SIFLMultiNonControled]=@SIFLMultiNonControled        
           ,[Notes]=@Notes        
           ,[ComponentCD]=@ComponentCD        
           ,[ClientID]=@ClientID        
           ,[FleetType]=@FleetType        
           ,[FlightPhoneNum]=@FlightPhoneNum        
           ,[Class]=@Class        
           ,[VendorID]=@VendorID        
           ,[VendorType]=@VendorType        
           ,[YearMade]=@YearMade        
           ,[ExteriorColor]=@ExteriorColor        
           ,[InteriorColor]=@InteriorColor        
           ,[IsAFIS]=@IsAFIS        
           ,[IsUWAData]=@IsUWAData        
           ,[LastUpdUID]=@LastUpdUID        
           ,[LastUpdTS]=@LastUpdTS        
           ,[FlightPlanCruiseSpeed]=@FlightPlanCruiseSpeed        
           ,[FlightPlanMaxFlightLevel]=@FlightPlanMaxFlightLevel        
           ,[MaximumTakeOffWeight]=@MaximumTakeOffWeight        
           ,[MaximumLandingWeight]=@MaximumLandingWeight        
       ,[MaximumWeightZeroFuel]=@MaximumWeightZeroFuel        
           ,[TaxiFuel]=@TaxiFuel        
           ,[MultiSec]=@MultiSec        
           ,[ForeGrndCustomColor]=@ForeGrndCustomColor        
           ,[BackgroundCustomColor]=@BackgroundCustomColor        
           ,[FlightPhoneIntlNum]=@FlightPhoneIntlNum        
           ,[FleetSize]=@FleetSize        
           ,[FltScanDoc]=@FltScanDoc        
           --,[MinimumDay]=@MinimumDay        
           --,[StandardCrewIntl]=@StandardCrewIntl        
           --,[StandardCrewDOM]=@StandardCrewDOM        
           ,[IsFAR91]=@IsFAR91        
           ,[IsFAR135]=@IsFAR135        
           ,[EmergencyContactID]=@EmergencyContactID        
           --,[IsTaxDailyAdj]=@IsTaxDailyAdj        
           --,[IsTaxLandingFee]=@IsTaxLandingFee        
           ,[IsDeleted]=@IsDeleted        
           ,[AircraftID]=@AircraftID        
           ,[CrewID]=@CrewID       
           ,SecondaryDomFlightPhone=@SecondaryDomFlightPhone      
           ,SecondaryIntlFlightPhone=@SecondaryIntlFlightPhone  
           ,marginalpercentage = @marginalpercentage  
       ,NationalityCountryID=@NationalityCountryID      
   WHERE [FleetID]=@FleetID          
        
  IF @@ERROR <> 0    
  BEGIN    
  ROLLBACK TRAN T1    
  END    
  ELSE    
  BEGIN    
  COMMIT TRAN T1    
  END        
END