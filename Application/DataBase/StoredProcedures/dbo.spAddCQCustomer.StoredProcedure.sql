/****** Object:  StoredProcedure [dbo].[spAddCQCustomer]    Script Date: 03/27/2013 14:26:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCQCustomer]
GO

CREATE PROCEDURE [dbo].[spAddCQCustomer]
(
@CustomerID BIGINT
,@CQCustomerCD VARCHAR(5)
,@CQCustomerName VARCHAR(40)
,@IsApplicationFiled BIT
,@IsApproved BIT
,@BillingName VARCHAR(40)
,@BillingAddr1 VARCHAR(40)
,@BillingAddr2 VARCHAR(40)
,@BillingAddr3 VARCHAR(40)
,@BillingCity VARCHAR(40)
,@BillingState VARCHAR(10)
,@BillingZip VARCHAR(15)
,@CountryID BIGINT
,@BillingPhoneNum VARCHAR(25)
,@BillingFaxNum VARCHAR(25)
,@Notes VARCHAR(MAX)
,@Credit NUMERIC(17,2)
,@DiscountPercentage NUMERIC(5,2)
,@IsInActive BIT
,@HomebaseID BIGINT
,@AirportID BIGINT
,@DateAddedDT DATETIME
,@WebAddress VARCHAR(100)
,@EmailID VARCHAR(100)
,@TollFreePhone VARCHAR(40)
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@CustomerType varchar(1)
--,@IntlStdCrewNum numeric(2,0)
--,@DomesticStdCrewNum numeric(2,0)
--,@MinimumDayUseHrs numeric(17,2)
--,@DailyUsageAdjTax bit
--,@LandingFeeTax bit
  ,@MarginalPercentage numeric(7,2)
)
AS BEGIN

	SET NOCOUNT OFF;
	DECLARE @CQCustomerID	BIGINT	
	SET @LastUpdTS = GETUTCDATE()
	BEGIN TRAN T1  	
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CQCustomerID OUTPUT
		
	INSERT INTO CQCustomer
		([CQCustomerID]
		,[CustomerID]
		,[CQCustomerCD]
		,[CQCustomerName]
		,[IsApplicationFiled]
		,[IsApproved]
		,[BillingName]
		,[BillingAddr1]
		,[BillingAddr2]
		,[BillingAddr3]
		,[BillingCity]
		,[BillingState]
		,[BillingZip]
		,[CountryID]
		,[BillingPhoneNum]
		,[BillingFaxNum]
		,[Notes]
		,[Credit]
		,[DiscountPercentage]
		,[IsInActive]
		,[HomebaseID]
		,[AirportID]
		,[DateAddedDT]
		,[WebAddress]
		,[EmailID]
		,[TollFreePhone]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted]
		,[CustomerType]
		--,IntlStdCrewNum
		--,DomesticStdCrewNum
		--,MinimumDayUseHrs
		--,DailyUsageAdjTax
		--,LandingFeeTax 
		,MarginalPercentage
		)
	VALUES
		(@CQCustomerID
		,@CustomerID
		,@CQCustomerCD
		,@CQCustomerName
		,@IsApplicationFiled
		,@IsApproved
		,@BillingName
		,@BillingAddr1
		,@BillingAddr2
		,@BillingAddr3
		,@BillingCity
		,@BillingState
		,@BillingZip
		,@CountryID
		,@BillingPhoneNum
		,@BillingFaxNum
		,@Notes
		,@Credit
		,@DiscountPercentage
		,@IsInActive
		,@HomebaseID
		,@AirportID
		,@DateAddedDT
		,@WebAddress
		,@EmailID
		,@TollFreePhone
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted
		,@CustomerType
		--,@IntlStdCrewNum
		--,@DomesticStdCrewNum
		--,@MinimumDayUseHrs
		--,@DailyUsageAdjTax
		--,@LandingFeeTax 
		,@MarginalPercentage
		)
		
		EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,NULL, 1, 'Standard Charge', 'Per Flight Hour', 1,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,NULL,@CQCustomerID,0
		EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,NULL, 2, 'Positioning Charge', 'Per Flight Hour', 1,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,NULL,@CQCustomerID,0
		EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,NULL, 3, 'Std Crew RON Chrg', 'Fixed', 5,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,NULL,@CQCustomerID,0
             
	SELECT @CQCustomerID AS CQCustomerID

IF @@ERROR <> 0
BEGIN
ROLLBACK TRAN T1
END
ELSE
BEGIN
COMMIT TRAN T1
END

END

GO


