

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFlightPakFuelData]    Script Date: 10/23/2013 13:25:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetFlightPakFuelData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetFlightPakFuelData]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFlightPakFuelData]    Script Date: 10/23/2013 13:25:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetFlightPakFuelData]  
(  
@TripID AS BIGINT,  
@DepartureICAOID AS BIGINT,   
@CustomerID AS BIGINT,  
@UserSessionID AS uniqueidentifier  
)  
  
AS  
  
  
-- SET @DepartureICAOID = 1002   
SET NOCOUNT ON;  
SET FMTONLY OFF;  
--Drop table #FlightPakFuel  
  
  
  SELECT   
   --FlightpakFuelID   ,  
   CustomerID  
   ,AirportID as DepartureICAOID  
   ,FBOName  
   ,GallonFrom  
   ,GallonTO  
   ,EffectiveDT  
   ,UnitPrice  
   ,FuelVendorID  
   ,UpdateDTTM  
   ,LastUpdUID  
   ,LastUpdTS  
   --,IsActive  
  
  INTO #FlightPakFuel   
  FROM PreflightFuel where 1= 2  
  
  
if exists(select CustomerID from PreflightFuel where Tripid = @TripID )  
 Begin  
  --if not exists (select CustomerID from PreflightFuel where Tripid = @TripID and AirportID =@DepartureICAOID)  
  --BEGIN  
  -- exec [spFlightPak_Preflight_UpdatePreflightFuelDetails] @TripID, @CustomerID, @UserSessionID  
  --END  
    
  INSERT into #FlightPakFuel   
  SELECT   
   --FlightpakFuelID   ,  
   CustomerID  
   ,AirportID as DepartureICAOID  
   ,FBOName  
   ,GallonFrom  
   ,GallonTO  
   ,EffectiveDT  
   ,UnitPrice  
   ,isnull(FuelVendorID,0)
   ,UpdateDTTM  
   ,LastUpdUID  
   ,LastUpdTS  
   --,IsActive  
  FROM PreflightFuel   
  WHERE AirportID =  @DepartureICAOID and CustomerID = @CustomerID and TripID = @TripID  
 END  
ELSE  
 Begin  
  INSERT into #FlightPakFuel   
  SELECT   
   --FlightpakFuelID,  
   CustomerID  
   ,DepartureICAOID  
   ,FBOName  
   ,GallonFrom  
   ,GallonTO  
   ,EffectiveDT  
   ,UnitPrice  
   ,isnull(FuelVendorID,0)
   ,UpdateDTTM  
   ,LastUpdUID  
   ,LastUpdTS  
   --,IsActive  
  FROM FlightPakFuel   
  WHERE DepartureICAOID =  @DepartureICAOID and CustomerID = @CustomerID  
    
  INSERT into #FlightPakFuel   
  SELECT   
   --FlightpakFuelID   ,  
   CustomerID  
   ,AirportID as DepartureICAOID  
   ,FBOName  
   ,GallonFrom  
   ,GallonTO  
   ,EffectiveDT  
   ,UnitPrice  
   ,isnull(FuelVendorID,0)  
   ,UpdateDTTM  
   ,LastUpdUID  
   ,LastUpdTS  
   --,IsActive  
  FROM PreflightFuel   
  WHERE AirportID =  @DepartureICAOID and CustomerID = @CustomerID and TripID is null  
  and UWAFuelInterfaceRequestID = @UserSessionID  
    
 END  
  
  
  
SELECT   
  
 ROW_NUMBER() OVER (PARTITION BY FuelVendorID, FBOName ORDER BY GallonFrom, GallonTo) AS RangeOrder1  
 --,'SR' + CONVERT(VARCHAR(2), ROW_NUMBER() OVER (PARTITION BY FuelVendorID, FBOName ORDER BY GallonFrom, GallonTo)) AS RangeOrder2  
 --,'ER' + CONVERT(VARCHAR(2), ROW_NUMBER() OVER (PARTITION BY FuelVendorID, FBOName ORDER BY GallonFrom, GallonTo)) AS RangeOrder3  
 -- ,FlightpakFuelID  
 ,CustomerID  
 ,DepartureICAOID  
 ,FBOName  
 ,GallonFrom  
 ,GallonTO  
 ,EffectiveDT  
 ,UnitPrice  
 ,FuelVendorID  
 ,UpdateDTTM  
 ,LastUpdUID  
 ,LastUpdTS  
 --,IsActive  
INTO #FlightPakFuelPivot  
FROM #FlightPakFuel  
  
  
DROP TABLE #FlightPakFuel  
  
  
--SELECT   
-- DepartureICAOID, FBOName, FuelVendorID,  
-- SUM([1]) AS Price1,  SUM([SR1]) AS StartRange1,  SUM([ER1]) AS EndRange1,  
-- SUM([2]) AS Price2,  SUM([SR2]) AS StartRange2,  SUM([ER2]) AS EndRange2,  
-- SUM([3]) AS Price3,  SUM([SR3]) AS StartRange3,  SUM([ER3]) AS EndRange3,  
-- SUM([4]) AS Price4,  SUM([SR4]) AS StartRange4,  SUM([ER4]) AS EndRange4,  
-- SUM([5]) AS Price5,  SUM([SR5]) AS StartRange5,  SUM([ER5]) AS EndRange5,  
-- SUM([6]) AS Price6,  SUM([SR6]) AS StartRange6,  SUM([ER6]) AS EndRange6,  
-- SUM([7]) AS Price7,  SUM([SR7]) AS StartRange7,  SUM([ER7]) AS EndRange7,  
-- SUM([8]) AS Price8,  SUM([SR8]) AS StartRange8,  SUM([ER8]) AS EndRange8,  
-- SUM([9]) AS Price9,  SUM([SR9]) AS StartRange9,  SUM([ER9]) AS EndRange9,  
-- SUM([10]) AS Price10, SUM([SR10]) AS StartRange10, SUM([ER10]) AS EndRange10,  
-- SUM([11]) AS Price11, SUM([SR11]) AS StartRange11, SUM([ER11]) AS EndRange11,  
-- SUM([12]) AS Price12, SUM([SR12]) AS StartRange12, SUM([ER12]) AS EndRange12,  
-- SUM([13]) AS Price13, SUM([SR13]) AS StartRange13, SUM([ER13]) AS EndRange13,  
-- SUM([14]) AS Price14, SUM([SR14]) AS StartRange14, SUM([ER14]) AS EndRange14,  
-- SUM([15]) AS Price15, SUM([SR15]) AS StartRange15, SUM([ER15]) AS EndRange15,  
-- SUM([16]) AS Price16, SUM([SR16]) AS StartRange16, SUM([ER16]) AS EndRange16,  
-- SUM([17]) AS Price17, SUM([SR17]) AS StartRange17, SUM([ER17]) AS EndRange17,  
-- SUM([18]) AS Price18, SUM([SR18]) AS StartRange18, SUM([ER18]) AS EndRange18,  
-- SUM([19]) AS Price19, SUM([SR19]) AS StartRange19, SUM([ER19]) AS EndRange19,  
-- SUM([20]) AS Price20, SUM([SR20]) AS StartRange20, SUM([ER20]) AS EndRange20,  
-- SUM([21]) AS Price21, SUM([SR21]) AS StartRange21, SUM([ER21]) AS EndRange21  
  
--FROM #FlightPakFuelPivot                                   
--PIVOT (  
--   MIN (UnitPrice)          
--   FOR RangeOrder1 in ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21]))                                          
--   AS RangeUnitPrice    
--PIVOT (  
--   MIN (GallonFrom)          
--   FOR RangeOrder2 in ([SR1], [SR2], [SR3], [SR4], [SR5], [SR6], [SR7], [SR8], [SR9], [SR10], [SR11], [SR12], [SR13], [SR14], [SR15], [SR16], [SR17], [SR18], [SR19], [SR20], [SR21]))                                          
--   AS RangeUnitPrice2    
--PIVOT (  
--   MIN(GallonTo)          
--   FOR RangeOrder3 in ([ER1], [ER2], [ER3], [ER4], [ER5], [ER6], [ER7], [ER8], [ER9], [ER10], [ER11], [ER12], [ER13], [ER14], [ER15], [ER16], [ER17], [ER18], [ER19], [ER20], [ER21]))                                          
--   AS RangeUnitPrice3  
     
--   GROUP BY DepartureICAOID, FuelVendorID, FBOName  
--   UNION   
--   SELECT DepartureICAOID, FBOName, FuelVendorID,    
--   UnitPrice AS Price1,  GallonFrom AS StartRange1, GallonTo AS EndRange1,  
-- UnitPrice AS Price2, GallonFrom AS StartRange2, GallonTo AS EndRange2,  
-- UnitPrice AS Price3, GallonFrom AS StartRange3, GallonTo AS EndRange3,  
-- UnitPrice AS Price4, GallonFrom AS StartRange4, GallonTo AS EndRange4,  
-- UnitPrice AS Price5, GallonFrom AS StartRange5, GallonTo AS EndRange5,  
-- UnitPrice AS Price6, GallonFrom AS StartRange6, GallonTo AS EndRange6,  
-- UnitPrice AS Price7, GallonFrom AS StartRange7, GallonTo AS EndRange7,  
-- UnitPrice AS Price8, GallonFrom AS StartRange8, GallonTo AS EndRange8,  
-- UnitPrice AS Price9, GallonFrom AS StartRange9, GallonTo AS EndRange9,  
-- UnitPrice AS Price10, GallonFrom AS StartRange10, GallonTo AS EndRange10,  
-- UnitPrice AS Price11, GallonFrom AS StartRange11, GallonTo AS EndRange11,  
-- UnitPrice AS Price12, GallonFrom AS StartRange12, GallonTo AS EndRange12,  
-- UnitPrice AS Price13, GallonFrom AS StartRange13, GallonTo AS EndRange13,  
-- UnitPrice AS Price14, GallonFrom AS StartRange14, GallonTo AS EndRange14,  
-- UnitPrice AS Price15, GallonFrom AS StartRange15, GallonTo AS EndRange15,  
-- UnitPrice AS Price16, GallonFrom AS StartRange16, GallonTo AS EndRange16,  
-- UnitPrice AS Price17, GallonFrom AS StartRange17, GallonTo AS EndRange17,  
-- UnitPrice AS Price18, GallonFrom AS StartRange18, GallonTo AS EndRange18,  
-- UnitPrice AS Price19, GallonFrom AS StartRange19, GallonTo AS EndRange19,  
-- UnitPrice AS Price20, GallonFrom AS StartRange20, GallonTo AS EndRange20,  
-- UnitPrice AS Price21, GallonFrom AS StartRange21, GallonTo AS EndRange21  
--FROM FlightpakFuel  
--WHERE 1=2  
     
/** Using a case statement instead of PIVOT since case statement is coming up as less costlier in the execution  plan**/  
SELECT   
 P.DepartureICAOID, P.FBOName, P.FuelVendorID,   
 --MIN(P.GallonFrom) GallonFrom,  
 --MIN(P.GallonTO) GallonTO,  
 MIN(isnull(V.VendorCD,'UWA') )VendorCD,   
 MIN(isnull(V.VendorName,'UWA')) VendorName,  
 MIN(P.EffectiveDT) EffectiveDT,  
 SUM(CASE WHEN P.RangeOrder1 = 1 THEN P.UnitPrice ELSE NULL END) AS Price1,  MIN(CASE WHEN P.RangeOrder1 = 1 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange1,     
 SUM(CASE WHEN P.RangeOrder1 = 2 THEN P.UnitPrice ELSE NULL END) AS Price2,  MIN(CASE WHEN P.RangeOrder1 = 2 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange2,     
 SUM(CASE WHEN P.RangeOrder1 = 3 THEN P.UnitPrice ELSE NULL END) AS Price3,  MIN(CASE WHEN P.RangeOrder1 = 3 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange3,     
 SUM(CASE WHEN P.RangeOrder1 = 4 THEN P.UnitPrice ELSE NULL END) AS Price4,  MIN(CASE WHEN P.RangeOrder1 = 4 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange4,     
 SUM(CASE WHEN P.RangeOrder1 = 5 THEN P.UnitPrice ELSE NULL END) AS Price5,  MIN(CASE WHEN P.RangeOrder1 = 5 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange5,     
 SUM(CASE WHEN P.RangeOrder1 = 6 THEN P.UnitPrice ELSE NULL END) AS Price6,  MIN(CASE WHEN P.RangeOrder1 = 6 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange6,     
 SUM(CASE WHEN P.RangeOrder1 = 7 THEN P.UnitPrice ELSE NULL END) AS Price7,  MIN(CASE WHEN P.RangeOrder1 = 7 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange7,     
 SUM(CASE WHEN P.RangeOrder1 = 8 THEN P.UnitPrice ELSE NULL END) AS Price8,  MIN(CASE WHEN P.RangeOrder1 = 8 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange8,     
 SUM(CASE WHEN P.RangeOrder1 = 9 THEN P.UnitPrice ELSE NULL END) AS Price9,  MIN(CASE WHEN P.RangeOrder1 = 9 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange9,     
 SUM(CASE WHEN P.RangeOrder1 = 10 THEN P.UnitPrice ELSE NULL END) AS Price10,  MIN(CASE WHEN P.RangeOrder1 = 10 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange10,  
  
 SUM(CASE WHEN P.RangeOrder1 = 11 THEN P.UnitPrice ELSE NULL END) AS Price11,  MIN(CASE WHEN P.RangeOrder1 = 11 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange11,  
  
 SUM(CASE WHEN P.RangeOrder1 = 12 THEN P.UnitPrice ELSE NULL END) AS Price12,  MIN(CASE WHEN P.RangeOrder1 = 12 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange12,  
  
 SUM(CASE WHEN P.RangeOrder1 = 13 THEN P.UnitPrice ELSE NULL END) AS Price13,  MIN(CASE WHEN P.RangeOrder1 = 13 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange13,  
  
 SUM(CASE WHEN P.RangeOrder1 = 14 THEN P.UnitPrice ELSE NULL END) AS Price14,  MIN(CASE WHEN P.RangeOrder1 = 14 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange14,  
  
 SUM(CASE WHEN P.RangeOrder1 = 15 THEN P.UnitPrice ELSE NULL END) AS Price15,  MIN(CASE WHEN P.RangeOrder1 = 15 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange15,  
  
 SUM(CASE WHEN P.RangeOrder1 = 16 THEN P.UnitPrice ELSE NULL END) AS Price16,  MIN(CASE WHEN P.RangeOrder1 = 16 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange16,  
  
 SUM(CASE WHEN P.RangeOrder1 = 17 THEN P.UnitPrice ELSE NULL END) AS Price17,  MIN(CASE WHEN P.RangeOrder1 = 17 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange17,  
  
 SUM(CASE WHEN P.RangeOrder1 = 18 THEN P.UnitPrice ELSE NULL END) AS Price18,  MIN(CASE WHEN P.RangeOrder1 = 18 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange18,  
  
 SUM(CASE WHEN P.RangeOrder1 = 19 THEN P.UnitPrice ELSE NULL END) AS Price19,  MIN(CASE WHEN P.RangeOrder1 = 19 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange19,  
  
 SUM(CASE WHEN P.RangeOrder1 = 20 THEN P.UnitPrice ELSE NULL END) AS Price20,  MIN(CASE WHEN P.RangeOrder1 = 20 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange20,  
  
 SUM(CASE WHEN P.RangeOrder1 = 21 THEN P.UnitPrice ELSE NULL END) AS Price21,  MIN(CASE WHEN P.RangeOrder1 = 21 THEN CONVERT(VARCHAR(17), CONVERT(INT, P.GallonFrom)) + '-' + CONVERT(VARCHAR(17), CONVERT(INT, P.GallonTo))  ELSE NULL END) AS StartRange21  
  
FROM #FlightPakFuelPivot P   
LEFT JOIN FuelVendor V ON V.FuelVendorID = P.FuelVendorID  
GROUP BY P.DepartureICAOID, P.FuelVendorID, P.FBOName  
UNION   
   SELECT     
 DepartureICAOID, FBOName, FuelVendorID,  
 --null  GallonFrom,  
 --null GallonTo,  
 CONVERT(VARCHAR(2), '') VendorCD,   
 CONVERT(VARCHAR(30), '') VendorName,  
 convert(date, GETDATE()) EffectiveDT,  
 UnitPrice AS Price1, '' AS StartRange1,   
 UnitPrice AS Price2, '' AS StartRange2,   
 UnitPrice AS Price3, '' AS StartRange3,   
 UnitPrice AS Price4, '' AS StartRange4,   
 UnitPrice AS Price5, '' AS StartRange5,   
 UnitPrice AS Price6, '' AS StartRange6,   
 UnitPrice AS Price7, '' AS StartRange7,   
 UnitPrice AS Price8, '' AS StartRange8,   
 UnitPrice AS Price9, '' AS StartRange9,   
 UnitPrice AS Price10, '' AS StartRange10,   
 UnitPrice AS Price11, '' AS StartRange11,   
 UnitPrice AS Price12, '' AS StartRange12,   
 UnitPrice AS Price13, '' AS StartRange13,   
 UnitPrice AS Price14, '' AS StartRange14,   
 UnitPrice AS Price15, '' AS StartRange15,   
 UnitPrice AS Price16, '' AS StartRange16,   
 UnitPrice AS Price17, '' AS StartRange17,   
 UnitPrice AS Price18, '' AS StartRange18,   
 UnitPrice AS Price19, '' AS StartRange19,   
 UnitPrice AS Price20, '' AS StartRange20,   
 UnitPrice AS Price21, '' AS StartRange21  
FROM FlightpakFuel  
WHERE 1=2     
     
DROP TABLE #FlightPakFuelPivot
GO


