
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFuelVendorText]    Script Date: 09/27/2013 20:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetFuelVendorText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetFuelVendorText]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFuelVendorText]    Script Date: 09/27/2013 20:21:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetFuelVendorText]
(  
@FuelVendorID AS BIGINT, 
@CustomerID AS BIGINT, 
@GallonFrom AS NUMERIC(17,3),
@GallonTO AS NUMERIC(17,3),
@DepartureICAOID AS BIGINT,   
@FBOName AS VARCHAR(25)
)  
  
AS  
-- =============================================      
-- Author: KArthikeyan. S
-- Create date: 27-09-2013
-- Description: Enhancement to get vendor comments in preflight logistics
-- =============================================      
 BEGIN  
	SELECT FuelVendorText FROM FlightpakFuel WHERE FuelVendorText <> '' AND FuelVendorID = @FuelVendorID AND CustomerID = @CustomerID AND GallonFrom = @GallonFrom AND GallonTO = @GallonTO AND DepartureICAOID = @DepartureICAOID AND FBOName = @FBOName
  
 END  
GO

