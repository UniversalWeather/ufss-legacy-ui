

/****** Object:  StoredProcedure [dbo].[spGetPaxSelectedList]    Script Date: 04/01/2013 16:24:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPaxSelectedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPaxSelectedList]
GO



/****** Object:  StoredProcedure [dbo].[spGetPaxSelectedList]    Script Date: 04/01/2013 16:24:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
  
CREATE PROCEDURE [dbo].[spGetPaxSelectedList](@CustomerID BIGINT ,    
                                              @PaxCode BIGINT)    
-- =============================================    
-- Author: MohanRaja.C    
-- Create date: 02/05/2012    
-- Description: Get the Selected Passenger List    
-- =============================================    
AS    
SET NOCOUNT ON    
BEGIN    
    
SELECT  PassengerGroupOrder.CustomerID,    
		PassengerGroupOrder.PassengerRequestorID,    
		[PassengerRequestorCD],   
		isnull(FirstName,'') + ' ' + isnull(middleInitial,'') + ' ' + isnull(LastName,'') + ' - (' + [PassengerRequestorCD] + ')' AS PassengerNameforList,     
		[PassengerName] + ' - (' + [PassengerRequestorCD] + ')' AS [PassengerName]    
FROM  PassengerGroupOrder --WHERE [Passenger].[PassengerRequestorID]     
INNER JOIN PassengerGroup ON PassengerGroupOrder.PassengerGroupID = PassengerGroup.PassengerGroupID  
INNER JOIN Passenger ON Passenger.PassengerRequestorID =  PassengerGroupOrder.PassengerRequestorID  
WHERE PassengerGroupOrder.PassengerGroupID=@PaxCode AND IsActive = 1  
ORDER BY PassengerGroupOrder.OrderNum  
  
END  
GO