IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQException]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQException]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQException]
(
	
	@CustomerID bigint,
	@CQMainID bigint,
	@CQExceptionDescription varchar(max),
	@ExceptionTemplateID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@DisplayOrder varchar(15)
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQExceptionID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQExceptionID OUTPUT 
INSERT INTO [CQException] ([CQExceptionID], [CustomerID], [CQMainID], [CQExceptionDescription], [ExceptionTemplateID], [LastUpdUID], [LastUpdTS], [IsDeleted], [DisplayOrder]) VALUES (@CQExceptionID, @CustomerID, @CQMainID, @CQExceptionDescription, @ExceptionTemplateID, @LastUpdUID, @LastUpdTS, @IsDeleted, @DisplayOrder);
	
SELECT @CQExceptionID AS 'CQExceptionID'
GO
