
GO
/****** Object:  StoredProcedure [dbo].[spAddAircraft]    Script Date: 08/24/2012 10:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddAircraft]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddAircraft]
go
  
CREATE Procedure [dbo].[spAddAircraft]        
 (     
 @CustomerID bigint,        
 @AircraftCD Varchar(10) ,        
 @AircraftDescription varchar(15) ,        
 @ChargeRate NUMERIC(17, 2) ,        
 @ChargeUnit char(1) ,        
 @PowerSetting char(1) ,        
 @PowerDescription varchar(17) ,        
 @WindAltitude NUMERIC(5, 0) ,        
 @PowerSettings1Description Varchar(17) ,        
 @PowerSettings1TrueAirSpeed NUMERIC(3, 0) ,        
 @PowerSettings1HourRange NUMERIC(6, 3) ,        
 @PowerSettings1TakeOffBias NUMERIC(6, 3) ,        
 @PowerSettings1LandingBias NUMERIC(6, 3) ,        
 @PowerSettings2Description Varchar(17) ,        
 @PowerSettings2TrueAirSpeed NUMERIC(3, 0) ,        
 @PowerSettings2HourRange NUMERIC(6, 3) ,        
 @PowerSettings2TakeOffBias NUMERIC(6, 3) ,        
 @PowerSettings2LandingBias NUMERIC(6, 3) ,        
 @PowerSettings3Description Varchar(17) ,        
 @PowerSettings3TrueAirSpeed NUMERIC(3, 0) ,        
 @PowerSettings3HourRange NUMERIC(6, 3) ,        
 @PowerSettings3TakeOffBias NUMERIC(6, 3) ,        
 @PowerSettings3LandingBias NUMERIC(6, 3) ,        
 @IsFixedRotary char(1) ,        
 @ClientID bigint,        
 @CQChargeRate NUMERIC(17, 2) ,        
 @CQChargeUnit NUMERIC(1, 0) ,        
 @PositionRate NUMERIC(17, 2) ,        
 @PostionUnit NUMERIC(1, 0) ,        
 @StandardCrew NUMERIC(2, 0) ,        
 @StandardCrewRON NUMERIC(17, 2) ,        
 @AdditionalCrew NUMERIC(17, 2) ,        
 @AdditionalCrewRON NUMERIC(17, 2) ,        
 @CharterQuoteWaitTM NUMERIC(17, 2) ,        
 @LandingFee NUMERIC(17, 2) ,        
 @CostBy NUMERIC(1, 0) ,        
 @FixedCost NUMERIC(17, 2) ,        
 @PercentageCost NUMERIC(17, 2) ,        
 @TaxRON bit ,        
 @AdditionalCrewTax bit ,        
 @IsCharterQuoteWaitTax bit ,        
 @LandingTax bit ,        
 @DescriptionRON bit ,        
 @AdditionalCrewDescription bit ,        
 @MinDailyREQ NUMERIC(17, 2) ,        
 @LastUpdUID Varchar(30) ,        
 @LastUpdTS datetime ,        
 @AircraftSize NUMERIC(1, 0) ,        
 @AircraftTypeCD varchar(max) ,        
 --@IntlStdCrewNum NUMERIC(2, 0) ,        
 --@DomesticStdCrewNum NUMERIC(2, 0) ,        
 --@MinimumDayUseHrs NUMERIC(17, 2) ,        
 --@DailyUsageAdjTax bit ,        
 --@LandingFeeTax bit ,        
 @IsDeleted bit ,    
 @IsInActive bit,    
 @MarginalPercentage numeric(7,2)  
)        
-- =============================================        
-- Author:RUKMINI M        
-- Create date: 10/4/2012        
-- Description: Insert the AircraftType information        
-- =============================================        
as        
begin         
SET NoCOUNT ON        
--To generate PrimaryKey when CustomerId is not available set the value as 0        
DECLARE @AircraftID BIGINT    
BEGIN TRAN T1        
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @AircraftID OUTPUT        
set @LastUpdTS= GETUTCDATE()        
--End of Primary Key generation        
        
   INSERT INTO Aircraft (         
AircraftID,        
CustomerID ,        
AircraftCD,           
AircraftDescription,        
ChargeRate ,        
ChargeUnit ,        
PowerSetting ,        
PowerDescription ,        
WindAltitude ,        
PowerSettings1Description ,        
PowerSettings1TrueAirSpeed ,        
PowerSettings1HourRange ,        
PowerSettings1TakeOffBias ,        
PowerSettings1LandingBias ,        
PowerSettings2Description ,        
PowerSettings2TrueAirSpeed ,        
PowerSettings2HourRange ,        
PowerSettings2TakeOffBias ,        
PowerSettings2LandingBias ,        
PowerSettings3Description ,        
PowerSettings3TrueAirSpeed ,        
PowerSettings3HourRange ,        
PowerSettings3TakeOffBias ,        
PowerSettings3LandingBias ,        
IsFixedRotary ,        
ClientID ,        
CQChargeRate ,        
CQChargeUnit ,        
PositionRate ,        
PostionUnit ,        
StandardCrew ,        
StandardCrewRON ,        
AdditionalCrew ,        
AdditionalCrewRON ,        
CharterQuoteWaitTM ,        
LandingFee ,        
CostBy ,        
FixedCost ,        
PercentageCost ,        
TaxRON ,        
AdditionalCrewTax ,        
IsCharterQuoteWaitTax ,        
LandingTax ,        
DescriptionRON ,        
AdditionalCrewDescription ,        
MinDailyREQ ,        
LastUpdUID ,        
LastUpdTS  ,        
AircraftSize ,        
AircraftTypeCD ,        
--IntlStdCrewNum ,        
--DomesticStdCrewNum ,        
--MinimumDayUseHrs ,        
--DailyUsageAdjTax ,        
--LandingFeeTax ,        
IsDeleted ,    
IsInActive   
,MarginalPercentage    
)        
     VALUES        
(                         
@AircraftID,        
@CustomerID,        
@AircraftCD,        
@AircraftDescription,        
@ChargeRate,        
@ChargeUnit,        
@PowerSetting ,        
@PowerDescription ,        
@WindAltitude ,        
@PowerSettings1Description ,        
@PowerSettings1TrueAirSpeed ,        
@PowerSettings1HourRange ,        
@PowerSettings1TakeOffBias ,        
@PowerSettings1LandingBias ,        
@PowerSettings2Description ,        
@PowerSettings2TrueAirSpeed ,        
@PowerSettings2HourRange ,        
@PowerSettings2TakeOffBias ,        
@PowerSettings2LandingBias ,        
@PowerSettings3Description ,        
@PowerSettings3TrueAirSpeed ,        
@PowerSettings3HourRange ,        
@PowerSettings3TakeOffBias ,        
@PowerSettings3LandingBias ,        
@IsFixedRotary ,        
@ClientID ,        
@CQChargeRate ,        
@CQChargeUnit ,        
@PositionRate ,        
@PostionUnit ,        
@StandardCrew ,        
@StandardCrewRON ,        
@AdditionalCrew ,        
@AdditionalCrewRON ,        
@CharterQuoteWaitTM ,        
@LandingFee ,        
@CostBy ,        
@FixedCost ,        
@PercentageCost ,        
@TaxRON ,        
@AdditionalCrewTax ,        
@IsCharterQuoteWaitTax ,        
@LandingTax ,        
@DescriptionRON ,        
@AdditionalCrewDescription ,        
@MinDailyREQ ,        
@LastUpdUID ,        
@LastUpdTS  ,        
@AircraftSize ,        
@AircraftTypeCD ,        
--@IntlStdCrewNum ,        
--@DomesticStdCrewNum ,        
--@MinimumDayUseHrs ,        
--@DailyUsageAdjTax ,        
--@LandingFeeTax ,        
isnull(@IsDeleted,0) ,    
isnull(@IsInActive,0)    
,@MarginalPercentage     
)        
        
    --EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,@AircraftCD, 1, 'Standard Charge', 'Per Flight Hour', 1,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,@AircraftID,NULL,0  
	--EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,@AircraftCD, 2, 'Positioning Charge', 'Per Flight Hour', 1,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,@AircraftID,NULL,0  
	--EXECUTE dbo.spFlightPak_AddFleetNewCharterRate @CustomerID,NULL,@AircraftCD, 3, 'Std Crew RON Chrg', 'Fixed', 5,0,0,0,0,0,0,0,0,0,0,0,'','','',0,0,NULL,NULL,NULL,NULL,@LastUpdUID,@LastUpdTS,@AircraftID,NULL,0  
  
IF @@ERROR <> 0  
BEGIN  
ROLLBACK TRAN T1  
END  
ELSE  
BEGIN  
COMMIT TRAN T1  
END  
        
END  