
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateCrewPaxVisa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateCrewPaxVisa]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateCrewPaxVisa]    Script Date: 01/07/2013 20:08:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_UpdateCrewPaxVisa]          
(@VisaID bigint        
,@CrewID bigint        
,@PassengerRequestorID bigint        
,@CustomerID bigint        
,@VisaTYPE char(1)        
,@VisaNum nvarchar(120)        
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime        
,@CountryID bigint        
,@ExpiryDT date        
,@Notes varchar(120)        
,@IssuePlace varchar(40)        
,@IssueDate date        
,@IsDeleted bit
,@EntriesAllowedInPassport int
,@TypeOfVisa varchar(10)
,@VisaExpireInDays int)          
AS          
BEGIN        
 SET NOCOUNT OFF;  
 DECLARE @ErrorMessage nVarchar(2000);  
 --For validating duplicate Visa number for the customer and Passenger, across all country  
 IF (@PassengerRequestorID IS NOT NULL)  
  BEGIN  
   IF (@IsDeleted <> 1)  
    BEGIN  
     IF EXISTS (SELECT VisaNum FROM [CrewPassengerVisa] WHERE CustomerID = @CustomerID  
                  AND VisaID <> @VisaID  
                  AND CrewID IS NULL  
                  AND VisaNum = @VisaNum  
                  AND ISNULL(CountryID,0) = ISNULL(@CountryID,0)  
                  AND IsDeleted = 0)  
      BEGIN  
       SET @ErrorMessage = '-500011~' + @VisaNum;  
       RAISERROR(@ErrorMessage, 17, 1)  
      END  
    END  
  END  
 --For validating duplicate Visa number for the customer and Crew  
 IF (@CrewID IS NOT NULL)  
  BEGIN  
   IF (@IsDeleted <> 1)  
    BEGIN  
     IF EXISTS (SELECT VisaNum FROM [CrewPassengerVisa] WHERE CustomerID = @CustomerID  
                  AND VisaID <> @VisaID  
                  AND PassengerRequestorID IS NULL  
                  AND VisaNum = @VisaNum  
                  AND IsDeleted = 0)  
      BEGIN  
       SET @ErrorMessage = '-500011~' + @VisaNum;  
       RAISERROR(@ErrorMessage, 17, 1)  
     END  
    END  
  END  
   
 set @LastUpdTS = GETUTCDATE()        
 --IF EXISTS (SELECT NULL FROM [CrewPassengerVisa] WHERE VisaID = @VisaID)          
--BEGIN          
 UPDATE [CrewPassengerVisa]         
 SET --VisaID = @VisaID        
  --CrewID = @CrewID        
  --,PassengerRequestorID = @PassengerRequestorID        
  --,CustomerID = @CustomerID        
  VisaTYPE = @VisaTYPE        
  ,VisaNum = @VisaNum        
  ,LastUpdUID = @LastUpdUID        
  ,LastUpdTS = @LastUpdTS        
  ,CountryID = @CountryID        
  ,ExpiryDT = @ExpiryDT        
  ,Notes = @Notes        
  ,IssuePlace = @IssuePlace        
  ,IssueDate = @IssueDate        
  ,IsDeleted = @IsDeleted 
  ,EntriesAllowedInPassport = @EntriesAllowedInPassport
  ,TypeOfVisa = @TypeOfVisa
  ,VisaExpireInDays = @VisaExpireInDays


WHERE --VisaID = @VisaID        
(PassengerRequestorID = isnull(@PassengerRequestorID ,0) or CrewID= isnull(@CrewID ,0)) AND CustomerID=@CustomerID AND VisaID=@VisaID--VisaNum=@VisaNum        
--END          
--ELSE          
--BEGIN          
        
--EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @VisaID OUTPUT        
        
--EXECUTE spFlightPak_AddCrewPaxVisa         
--   @VisaID        
--   ,@CrewID        
--   ,@PassengerRequestorID        
--   ,@CustomerID        
--   ,@VisaTYPE        
--   ,@VisaNum        
--   ,@LastUpdUID        
--   ,@LastUpdTS        
--   ,@CountryID        
--   ,@ExpiryDT        
--   ,@Notes        
--   ,@IssuePlace        
--   ,@IssueDate        
--   ,@IsDeleted        
END  