
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFlightPurpose]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFlightPurpose]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFlightPurpose]
GO
CREATE PROCEDURE [dbo].[spUpdateFlightPurpose]  
(   @FlightPurposeID  BIGINT  
   ,@FlightPurposeCD CHAR(2) 
   ,@FlightPurposeDescription VARCHAR(25)  
   ,@CustomerId BIGINT  
   ,@IsDefaultPurpose BIT  
   ,@IsPersonalTravel BIT  
   ,@ClientID bigint 
   ,@LastUpdUID VARCHAR(30)  
   ,@LastUpdTS DATETIME  
   ,@IsWaitList BIT  
   ,@IsDeleted BIT  
   ,@IsInActive BIT
)  
-- =============================================  
-- Author: Sujitha.V  
-- Create date: 17/4/2012  
-- Description: Update the Flight Purpose information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON  
if (@IsDefaultPurpose='True')        
BEGIN        
UPDATE [FlightPurpose]        
SET        
IsDefaultPurpose='False'        
WHERE CustomerID =@CustomerID
  SET @LastUpdTS = GETUTCDATE()
 UPDATE FlightPurpose  
 SET [FlightPurposeID]=@FlightPurposeID
	,[FlightPurposeCD]=@FlightPurposeCD
    ,[FlightPurposeDescription]=@FlightPurposeDescription
    ,[CustomerID]=@CustomerId
    ,[IsDefaultPurpose]=@IsDefaultPurpose
	,[IsPersonalTravel]=@IsPersonalTravel
	,[ClientID]=@ClientID
	,[LastUpdUID]=@LastUpdUID
	,[LastUpdTS]=@LastUpdTS
	,[IsWaitList]=@IsWaitList
	,[IsDeleted]=@IsDeleted
	,[IsInActive]=@IsInActive
 WHERE [FlightPurposeID] = @FlightPurposeID 
  
End         
ELSE IF(@IsDefaultPurpose='False')        
BEGIN	
UPDATE FlightPurpose  
 SET [FlightPurposeID]=@FlightPurposeID
	,[FlightPurposeCD]=@FlightPurposeCD
    ,[FlightPurposeDescription]=@FlightPurposeDescription
    ,[CustomerID]=@CustomerId
    ,[IsDefaultPurpose]=@IsDefaultPurpose
	,[IsPersonalTravel]=@IsPersonalTravel
	,[ClientID]=@ClientID
	,[LastUpdUID]=@LastUpdUID
	,[LastUpdTS]=@LastUpdTS
	,[IsWaitList]=@IsWaitList
	,[IsDeleted]=@IsDeleted
	,[IsInActive]=@IsInActive
 WHERE [FlightPurposeID] = @FlightPurposeID 
 END
 END
GO
