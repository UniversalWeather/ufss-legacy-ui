

/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRFBOList]    Script Date: 02/21/2013 18:18:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_AddCRFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRFBOList]
GO



/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRFBOList]    Script Date: 02/21/2013 18:18:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRFBOList]
(--@CRFBOListID BIGINT,
@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@FBOID BIGINT
,@CRFBOListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
	DECLARE @CRFBOListID BIGINT
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRFBOListID OUTPUT
		
	INSERT INTO CRFBOList
		([CRFBOListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[FBOID]
		,[CRFBOListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted])
	VALUES
		(@CRFBOListID
		,@CustomerID
		,@CRLegID
		,@TripNUM
		,@LegID
		,@RecordType
		,@FBOID
		,@CRFBOListDescription
		,@PhoneNUM
		,@IsCompleted
		,@AirportID
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted)
		
	SELECT @CRFBOListID AS CRFBOListID
	
END
GO


