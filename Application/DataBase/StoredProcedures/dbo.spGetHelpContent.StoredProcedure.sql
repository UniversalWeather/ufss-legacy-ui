
GO
/****** Object:  StoredProcedure [dbo].[spGetHelpContent]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHelpContent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHelpContent]
GO

CREATE PROCEDURE spGetHelpContent
(@Identifier varchar(60)
,@Category varchar(100)
,@Topic varchar(100)
,@IsDeleted bit
,@IsInActive bit)
AS BEGIN

	SET NOCOUNT OFF;
	
	SELECT	Identifier,
			Topic,
			Category,
			Content,
			LastUpdUID,
			LastUpdTS,
			IsDeleted,
			IsInActive
	FROM	HelpContent
	WHERE	Identifier = CASE WHEN @Identifier IS NOT NULL THEN @Identifier ELSE Identifier END
			AND Category = CASE WHEN @Category IS NOT NULL THEN @Category ELSE Category END
			AND Topic = CASE WHEN @Topic IS NOT NULL THEN @Topic ELSE Topic END
			AND IsDeleted = CASE WHEN @IsDeleted IS NOT NULL THEN @IsDeleted ELSE IsDeleted END
			AND IsInActive = CASE WHEN @IsInActive IS NOT NULL THEN @IsInActive ELSE IsInActive END

END