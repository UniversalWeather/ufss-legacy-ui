

/****** Object:  StoredProcedure [dbo].[spflight_DeleteWorldClock]    Script Date: 02/13/2014 15:04:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spflight_DeleteWorldClock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spflight_DeleteWorldClock]
GO



/****** Object:  StoredProcedure [dbo].[spflight_DeleteWorldClock]    Script Date: 02/13/2014 15:04:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spflight_DeleteWorldClock]    
(@AirportID bigint    
)  
AS 
BEGIN    
     UPDATE Airport SET IsWorldClock = 0 WHERE AirportID = @AirportID        
END
GO


