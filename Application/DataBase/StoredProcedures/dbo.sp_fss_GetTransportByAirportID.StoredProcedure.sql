
/****** Object:  StoredProcedure [dbo].[sp_fss_GetTransportByAirportID]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetTransportByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetTransportByAirportID]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetTransportByAirportID]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetTransportByAirportID]
(
	@CustomerID BIGINT
	,@AirportID BIGINT
	,@TransportID BIGINT
    ,@TransportCD char(4)	
    ,@FetchChoiceOnly BIT    
	,@FetchActiveOnly BIT
)        
AS        
-- =============================================            
-- Author: Sridhar           
-- Create date: 27/02/2014            
-- Description: Get All TransportByAirportID details for Popup      
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,0,'',0,0
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,0,'',0,1
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,0,'',0,0
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,0,'',1,0
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,10099150344,'',0,0
-- Exec sp_fss_GetTransportByAirportID 10099,1009938248,0,'0009',0,0
-- =============================================       
BEGIN
SET NOCOUNT ON        
  
DECLARE @UWACustomerID BIGINT       
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
      
DECLARE @ICAOCD VARCHAR(50)  
DECLARE @UWAAirportId VARCHAR(50) 
DECLARE @ICAOCD1 VARCHAR(50)        
DECLARE @CustomAirportID VARCHAR(50)
     
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD      
--DECLARE @TransportICAOID CHAR(4)     
      
--SELECT @TransportICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID      

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD 
  
      
 SELECT T.[TransportID]      
		,T.[AirportID]      
		,T.[TransportCD]      
		,T.[CustomerID]      
		,ISNULL(T.IsChoice,0) IsChoice     
		,T.[TransportationVendor]      
		,T.[PhoneNum]      
		,T.[FaxNum]      
		,T.[NegotiatedRate]      
		,T.[ContactName]      
		,T.[Remarks]      
		,T.[LastUpdUID]      
		,T.[LastUpdTS]      
		,T.[UpdateDT]      
		,T.[RecordType]      
		,T.[SourceID]      
		,T.[ControlNum]      
		,T.[TollFreePhoneNum]      
		,T.[Website]      
		,(SELECT cast(CASE WHEN T.UWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter     
		,T.[UWAMaintFlag]    
		,T.[UWAID]      
		,ISNULL(T.IsInactive,0) IsInactive      
		,ISNULL(T.[IsDeleted],0) IsDeleted
		,T.[UWAUpdates]      
		,T.[AltBusinessPhone]      
		,T.[BusinessEmail]      
		,T.[ContactBusinessPhone]      
		,T.[CellPhoneNum]      
		,T.[ContactEmail]      
		,T.[Addr1]      
		,T.[Addr2]      
		,T.[Addr3]      
		,T.[City]      
		,T.[StateProvince]      
		,T.[MetroID]      
		,T.[CountryID]      
		,M.MetroCD      
		,T.IsPassenger    
		,T.IsCrew    
		,T.ExchangeRateID      
		,T.NegotiatedTerms      
		,T.SundayWorkHours      
		,T.MondayWorkHours      
		,T.TuesdayWorkHours      
		,T.WednesdayWorkHours      
		,T.ThursdayWorkHours      
		,T.FridayWorkHours      
		,T.SaturdayWorkHours                
        ,C.CountryCD              
                
 FROM  Transport T 
		LEFT OUTER JOIN Metro M on T.MetroID = M.MetroID          
		LEFT OUTER JOIN Country C on T.CountryID = C.CountryID               
		--inner join [Airport] Ai on T.AirportID = Ai.AirportID              
 where  ISNULL(T.IsDeleted,0) = 0         
	 AND ((T.AirportID = @CustomAirportID  AND T.CustomerID = @CustomerID) or (T.AirportID  = @UWAAirportId AND T.CustomerID = @CustomerID)) 
     AND T.TransportID = case when @TransportID <>0 then @TransportID else T.TransportID end 
     AND T.TransportCD = case when Ltrim(Rtrim(@TransportCD)) <> '' then @TransportCD else T.TransportCD end 
     AND ISNULL(T.IsChoice,0) = case when @FetchChoiceOnly = 1 then 1 else ISNULL(T.IsChoice,0) end       
     AND ISNULL(T.IsInActive,0) = case when @FetchActiveOnly = 0 then 0 else ISNULL(T.IsInActive,0) end     
 --T.AirportID = @AirportID    
 ----Ai.IcaoID = @TransportICAOID       
 --AND T.CustomerID = @CustomerID     
 --AND T.IsDeleted = 0    
 UNION ALL    
     
 SELECT T.[TransportID]      
	,T.[AirportID]      
	,T.[TransportCD]      
	,T.[CustomerID]      
	,ISNULL(T.IsChoice,0) IsChoice   
	,T.[TransportationVendor]      
	,T.[PhoneNum]      
	,T.[FaxNum]      
	,T.[NegotiatedRate]      
	,T.[ContactName]      
	,T.[Remarks]      
	,T.[LastUpdUID]      
	,T.[LastUpdTS]      
	,T.[UpdateDT]      
	,T.[RecordType]      
	,T.[SourceID]      
	,T.[ControlNum]      
	,T.[TollFreePhoneNum]      
	,T.[Website]      
	,(SELECT cast(CASE WHEN T.UWAMaintFlag = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter     
	,T.[UWAMaintFlag]     
	,T.[UWAID]      
	,ISNULL(T.IsInactive,0)  IsInactive       
	,ISNULL(T.[IsDeleted],0) IsDeleted    
	,T.[UWAUpdates]      
	,T.[AltBusinessPhone]      
	,T.[BusinessEmail]      
	,T.[ContactBusinessPhone]      
	,T.[CellPhoneNum]      
	,T.[ContactEmail]      
	,T.[Addr1]      
	,T.[Addr2]      
	,T.[Addr3]      
	,T.[City]      
	,T.[StateProvince]      
	,T.[MetroID]      
	,T.[CountryID]      
	,M.MetroCD    
    ,T.IsPassenger    
    ,T.IsCrew    
    ,T.ExchangeRateID      
    ,T.NegotiatedTerms      
    ,T.SundayWorkHours      
    ,T.MondayWorkHours      
    ,T.TuesdayWorkHours      
    ,T.WednesdayWorkHours      
    ,T.ThursdayWorkHours      
    ,T.FridayWorkHours      
    ,T.SaturdayWorkHours                  
    ,C.CountryCD              
                
 FROM  Transport T 
		LEFT OUTER JOIN Metro M on T.MetroID = M.MetroID          
		LEFT OUTER JOIN Country C on T.CountryID = C.CountryID        
 WHERE  T.AirportID = @UWAAirportId AND T.CustomerID = @UWACustomerID    
	AND ISNULL(T.IsDeleted,0) = 0    
	AND T.TransportID = case when @TransportID <>0 then @TransportID else T.TransportID end 
    AND T.TransportCD = case when Ltrim(Rtrim(@TransportCD)) <> '' then @TransportCD else T.TransportCD end 
    AND ISNULL(T.IsChoice,0) = case when @FetchChoiceOnly = 1 then 1 else ISNULL(T.IsChoice,0) end       
    AND ISNULL(T.IsInActive,0) = case when @FetchActiveOnly = 0 then 0 else ISNULL(T.IsInActive,0) end	        
	AND T.TransportCD NOT IN     
   (SELECT TransportCD FROM Transport WHERE ((AirportID  = @CustomAirportID AND CustomerID = @CustomerID) or (AirportID  = @UWAAirportId AND CustomerID = @CustomerID))   
	AND ISNULL(IsDeleted,0) = 0)     
  -- AirportID = @AirportID     
  --AND CustomerID = @CustomerID    
  --AND IsDeleted = 0    	
   ORDER BY TransportationVendor --TransportCD  
 
END

GO

