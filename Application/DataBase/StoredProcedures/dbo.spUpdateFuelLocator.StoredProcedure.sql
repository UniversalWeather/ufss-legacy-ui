
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFuelLocator]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFuelLocator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFuelLocator]
GO
CREATE PROCEDURE [dbo].[spUpdateFuelLocator] (
	@FuelLocatorID BIGINT  
 ,@CustomerID BIGINT  
 ,@FuelLocatorCD CHAR(4)  
 ,@FuelLocatorDescription VARCHAR(25)  
 ,@ClientID bigint  
 ,@LastUpdUID VARCHAR(30)  
 ,@LastUpdTS DATETIME  
 ,@IsDeleted BIT  
 ,@IsInActive BIT
 )  
 -- =============================================  
 -- Author:Hamsha.S  
 -- Create date: 08/05/2012  
 -- Description: Update the FuelLocator information  
 -- =============================================  
AS  
BEGIN  
 SET NOCOUNT ON  
set @LastUpdTS = GETUTCDATE()  
 UPDATE [FuelLocator]  
 SET FuelLocatorCD = @FuelLocatorCD  
  ,FuelLocatorDescription = @FuelLocatorDescription  
  ,ClientID = @ClientID  
  ,LastUpdUID = @LastUpdUID  
  ,LastUpdTS = @LastUpdTS  
  ,IsDeleted = @IsDeleted  
  ,IsInActive = @IsInActive
 WHERE CustomerID = @CustomerID  
  AND FuelLocatorID = @FuelLocatorID  
END  