/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_DeleteCRDispatchNotes]    Script Date: 02/08/2013 18:27:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_DeleteCRDispatchNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRDispatchNotes]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRDispatchNotes]
(
            @CRDispatchNotesID bigint,
            @CustomerID bigint,
            @CRMainID bigint
)
AS
BEGIN 
SET NoCOUNT ON

DELETE FROM [dbo].[CRDispatchNotes]
      WHERE  CRMainID = @CRMainID and  CustomerID=@CustomerID and CRDispatchNotesID=@CRDispatchNotesID
END

GO


