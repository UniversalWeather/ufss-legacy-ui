
/****** Object:  StoredProcedure [dbo].[GetFleetResourceType]    Script Date: 09/12/2013 12:36:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFleetResourceType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetFleetResourceType]
GO

/****** Object:  StoredProcedure [dbo].[GetFleetResourceType]    Script Date: 09/12/2013 12:36:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetFleetResourceType](@CustomerID bigint, @ClientID bigint,
                                @IN_VENDOR_FILTER VARCHAR(30),
                                @IN_FLEET_IDS nvarchar(MAX),
                                @IN_HOMEBASE_IDS nvarchar(max),
                                 @USERHOMEBASEID BIGINT)
                              
AS

-- NOTE: In R1 - Fleet.homebaseId is filtered with Company.HomebaseId. In R2, it should be done with Company.HomebaseAirportId 
BEGIN

IF (@IN_VENDOR_FILTER = 'None' OR @IN_VENDOR_FILTER = 'none')
   Set @IN_VENDOR_FILTER='IncludeActiveVendors'

IF @IN_VENDOR_FILTER='IncludeActiveVendors'
BEGIN
   IF(@ClientID>0)        
	BEGIN
	
    
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				 SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL 
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID 
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0 AND FL.IsInActive != 1
		    ORDER BY FL.TailNum
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				
				   SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND  C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				
				     SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND  C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
					END
				END
				
      END
    ELSE --  nO CLIENT ID
      	BEGIN
	
    
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				 SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0 AND FL.IsInActive != 1
		     ORDER BY FL.TailNum 
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				
				   SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND  C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		      AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				
				     SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND  C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS NULL OR VR.IsInActive IS NOT NULL)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
						  
					END
				END
				
      END
             
END
ELSE IF @IN_VENDOR_FILTER='OnlyShowActiveVendors'
BEGIN 
    IF(@ClientID>0)        
	BEGIN
    
          
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				 SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				
				   SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
				 ORDER BY FL.TailNum		  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				
				      SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
					 ORDER BY FL.TailNum	  
					END
				END
          --
       END 
    ELSE -- NO CLIENT ID
      	BEGIN
	
    
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND C.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0 AND FL.IsInActive != 1
		     ORDER BY FL.TailNum 
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				     SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				    SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND FL.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		      AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				     SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		      AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS NOT NULL AND VR.IsInActive = 1)
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
				 ORDER BY FL.TailNum		  
					END
				END
				
      END
      --
END
ELSE IF @IN_VENDOR_FILTER='ExcludeActiveVendors'
BEGIN 
     IF(@ClientID>0)        
	BEGIN  
  
          
          --
          
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				 SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND FL.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				
				   SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND FL.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0 AND FL.IsInActive != 1
		     ORDER BY FL.TailNum 
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				
				      SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		    AND FL.ClientID = @ClientID
		    AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
				 ORDER BY FL.TailNum		  
					END
				END 
          --
     END
   ELSE -- no client id
     
      BEGIN  
  
          
          --
          
		      IF(@USERHOMEBASEID>0)      -- HOMEBASE ONLY FILTER SELECTED  
			BEGIN
				
				IF(@IN_HOMEBASE_IDS != '0')   -- HOMEBASE IS SELECTED IN FILTER     
				BEGIN
				 SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND FL.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		      AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
		     ORDER BY FL.TailNum
				END
				ELSE	-- HOMEBASE NOT SELECTED IN FILTER			   
				BEGIN
				
				   SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
			 ORDER BY FL.TailNum			  
				END
			   
			END
				
			ELSE -- HOMEBASE ONLY FILTER NOT SELECTED  
				BEGIN
					
					 IF(@IN_HOMEBASE_IDS != '0')      -- HOMEBASE IS SELECTED IN FILTER       
					BEGIN
				  SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID AND FL.HomebaseID IN  ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_HOMEBASE_IDS, ',')) AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		   AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0 AND FL.IsInActive != 1
		     ORDER BY FL.TailNum 
					END
					ELSE -- HOMEBASE NOT SELECTED IN FILTER			
				   
					BEGIN
				
				      SELECT  FL.FleetID, FL.TailNum, RTRIM(A.AircraftCD) + ISNULL(' (' + RTRIM(VR.VendorCD) + ')', '') [Description], FL.HomebaseID
			FROM Fleet FL
			INNER JOIN Aircraft A ON FL.AircraftID = A.AircraftID
            LEFT OUTER JOIN Vendor VR ON VR.VendorID = FL.VendorID
            LEFT OUTER JOIN Company C ON C.HomebaseID = FL.HomebaseID  AND C.HomebaseID = @USERHOMEBASEID
		    WHERE  FL.TailNum Is Not NULL AND FL.CustomerID = @CustomerID 
		     AND  FL.FleetID IN ( SELECT  CAST(items AS bigint) FROM dbo.Split(@IN_FLEET_IDS, ','))
		    AND (VR.IsInActive IS  NULL OR (VR.IsInActive IS NOT NULL AND VR.IsInActive = 0))
		    AND FL.IsDeleted = 0  AND FL.IsInActive != 1
				 ORDER BY FL.TailNum		  
					END
				END 
          --
     END
         
END 
END

--Exec [GetFleetResourceType] 10000 , NULL,'IncludeActiveVendors','10000103376,10000102762,10020'


GO


