
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp]    Script Date: 01/17/2013 18:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp]    Script Date: 01/17/2013 18:56:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp]
(
@CustomerID BIGINT,
@OrderBy CHAR(30),
@ShowInactive bit,
@ReportDate DateTime,
@username varchar(30)
)


AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SQLQuery varchar(1000) 
	
    DECLARE @tblCrewPAXCleanup AS TABLE (  
    CrewOrPax varchar(1)
    ,CrewOrPAXID bigint
    ,CrewOrPAXCD varchar(5)
    , CrewOrPaxName varchar(80)
    , IsActive bit 
    )  
	
	insert into @tblCrewPAXCleanup 
	SELECT 'C' as CrewOrPax,C.crewid,c.CrewCD, 
		
		CASE WHEN ISNULL( c.LastName,'') <>'' THEN C.LastName + ', '  ELSE '' END
		+
		CASE WHEN ISNULL( c.FirstName,'') <>'' THEN C.FirstName+ ' '  ELSE '' END
		+ CASE WHEN ISNULL( c.MiddleInitial,'') <>'' THEN c.MiddleInitial  ELSE '' END
		AS 'CrewOrPaxName',
		C.IsStatus as 'IsActive'
	from crew C
	left join
	(
		SELECT POC.CrewID
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
			inner join PostflightCrew POC on POC.POLegID = pol.POLegID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate
	
		union
	
		SELECT PC.CrewID
		from PreflightMain PM
			inner join PreflightLeg PL on PM.TripID = PL.TripID
			inner join PreflightCrewList PC on PC.LegID= PL.LegID
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PL.DepartureDTTMLocal> = @ReportDate
		--union Corporate Request
		--union Charterquote
	) ActiveCrewlist
		on C.CrewID = ActiveCrewlist.CrewID
	where C.IsStatus =1
		and c.CustomerID = @CustomerID
		and ActiveCrewlist.CrewID is null
	
	--ORDER BY CASE WHEN @OrderBy = 'code' then c.CrewCD else c.LastName end ASC
		
	insert into @tblCrewPAXCleanup 
	
	SELECT 
		'P' as CrewOrPax,P.PassengerRequestorID ,P.PassengerRequestorCD , 
		
		--ActivePassengerlist.PassengerID,
		CASE WHEN ISNULL( P.LastName,'') <>'' THEN P.LastName + ', '  ELSE '' END
		+ 
		CASE WHEN ISNULL( P.FirstName,'') <>'' THEN P.FirstName+ ' '  ELSE '' END
		+ CASE WHEN ISNULL( P.MiddleInitial,'') <>'' THEN P.MiddleInitial  ELSE '' END
		AS 'CrewOrPaxName',
		P.IsActive
	from Passenger P
	left join 
	(

		SELECT POP.PassengerID
		from PostflightMain PO
			inner join PostflightLeg POL on po.POLogID = pol.POLogID
			inner join PostflightPassenger POP on POP.POLegID = pol.POLegID
		where po.IsDeleted = 0
			and po.CustomerID = @CustomerID
			and POL.OutboundDTTM > = @ReportDate

		union 

		SELECT PP.PassengerID
		from PreflightMain PM
			inner join PreflightLeg PL on PM.TripID = PL.TripID
			inner join PreflightPassengerList PP on PP.LegID= PL.LegID
		where PM.IsDeleted = 0
			and PM.CustomerID = @CustomerID
			and PL.DepartureDTTMLocal> = @ReportDate

		union 

		SELECT CP.PassengerRequestorID
		from CQMain CM
			inner join CQLeg CL on CM.CQMainID = CL.CQMainID
			inner join CQPassenger CP on CP.LegID= CL.LegID 
		where CM.IsDeleted = 0
			and CM.CustomerID = @CustomerID
			and CL.DepartureDTTMLocal> = @ReportDate

		--Corporate Request


	) ActivePassengerlist  
		on P.PassengerRequestorID = ActivePassengerlist.PassengerID 
	where P.IsActive =1 
		and P.CustomerID = @CustomerID 
		and ActivePassengerlist.PassengerID is null 
	
	
	
	--InactivateCrew
	update Crew Set IsStatus =0 
	,LastUpdUID = @username
	,LastUpdTS = getutcdate()
	 from  @tblCrewPAXCleanup
	where CrewOrPax = 'C' and crew.CrewID = CrewOrPAXID 
	
	--InactivatePAX
	update Passenger Set IsActive=0
	,LastUpdUID = @username
	,LastUpdTS = getutcdate()
	  from  @tblCrewPAXCleanup
	where CrewOrPax = 'P' and Passenger.PassengerRequestorID = CrewOrPAXID
	
	
	select CrewOrPax
    ,CrewOrPAXCD 
    , CrewOrPaxName 
    , IsActive  from @tblCrewPAXCleanup 
	order by
	(case when @OrderBy='code' then  CrewOrPAX+','+CrewOrPAXCD else  CrewOrPAX+','+CrewOrPaxName end)

	--exec [dbo].[spFlightPak_SystemTools_InactivateCrewPAXForCleanUp] 	 10003,'name',1,'jan 01, 2013','sujitha'
END

GO


