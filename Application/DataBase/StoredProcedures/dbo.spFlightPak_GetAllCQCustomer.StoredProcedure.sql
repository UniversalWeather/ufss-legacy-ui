/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCQCustomer]    Script Date: 03/27/2013 14:50:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllCQCustomer]
GO


Create Procedure [dbo].[spFlightPak_GetAllCQCustomer]      
(      
 @CustomerID  BIGINT  
  
)      
AS      
BEGIN      
SET NOCOUNT ON;      
   
SELECT     
       [CQCustomerID]
      ,[CustomerID]
      ,[CQCustomerCD]
      ,[CQCustomerName]
      ,[IsApplicationFiled]
      ,[IsApproved]
      ,[BillingName]
      ,[BillingAddr1]
      ,[BillingAddr2]
      ,[BillingAddr3]
      ,[BillingCity]
      ,[BillingState]
      ,[BillingZip]
      ,[CountryID]
      ,[BillingPhoneNum]
      ,[BillingFaxNum]
      ,[Notes]
      ,[Credit]
      ,[DiscountPercentage]
      ,[IsInActive]
      ,[HomebaseID]
      ,[AirportID]
      ,[DateAddedDT]
      ,[WebAddress]
      ,[EmailID]
      ,[TollFreePhone]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,[CustomerType]
      ,IntlStdCrewNum
	  ,DomesticStdCrewNum
	  ,MinimumDayUseHrs
	  ,DailyUsageAdjTax
	  ,LandingFeeTax
	  ,MarginalPercentage	
      
 FROM [CQCustomer]    
 
 WHERE CQCustomer.CustomerID  = @CustomerID      
 AND ISNULL(CQCustomer.IsDeleted,0) = 0    
 order by CQCustomerCD 
 END    

GO


