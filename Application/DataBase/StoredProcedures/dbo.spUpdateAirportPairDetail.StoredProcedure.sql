
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAirportPairDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAirportPairDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAirportPairDetail]
GO

CREATE PROCEDURE spUpdateAirportPairDetail
(@AirportPairDetailID BIGINT,
@CustomerID BIGINT,
@AirportPairID BIGINT,
@LegID INT,
@AircraftID BIGINT,
@QTR NUMERIC(1),
@DAirportID BIGINT,
@AAirportID BIGINT,
@Distance NUMERIC(5),
@PowerSetting VARCHAR(1),
@TakeoffBIAS NUMERIC(6,3),
@LandingBIAS NUMERIC(6,3),
@TrueAirSpeed NUMERIC(3),
@Winds NUMERIC(5),
@ElapseTM NUMERIC(7,3),
@Cost NUMERIC(12,2),
@WindReliability INT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE AirportPairDetail
	SET CustomerID=@CustomerID,
		AirportPairID=@AirportPairID,
		LegID=@LegID,
		AircraftID=@AircraftID,
		QTR=@QTR,
		DAirportID=@DAirportID,
		AAirportID=@AAirportID,
		Distance=@Distance,
		PowerSetting=@PowerSetting,
		TakeoffBIAS=@TakeoffBIAS,
		LandingBIAS=@LandingBIAS,
		TrueAirSpeed=@TrueAirSpeed,
		Winds=@Winds,
		ElapseTM=@ElapseTM,
		Cost=@Cost,
		WindReliability=@WindReliability,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=@LastUpdTS,
		IsDeleted=@IsDeleted
	WHERE AirportPairDetailID=@AirportPairDetailID

END