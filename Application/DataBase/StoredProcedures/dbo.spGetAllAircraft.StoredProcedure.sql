/****** Object:  StoredProcedure [dbo].[spGetAllAircraft]    Script Date: 03/27/2013 19:17:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAircraft]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAircraft]
GO

  
CREATE Procedure [dbo].[spGetAllAircraft](@CustomerID bigint)    
as    
-- =============================================    
-- Author:RUKMINI M    
-- Create date: 10/4/2012    
-- Description: Get the AircraftType information    
-- =============================================    
set nocount on    
    
begin     
SELECT      
    
AircraftID,    
CustomerID,    
CASE WHEN AircraftCD IS NULL THEN '' ELSE LTRIM(RTRIM(AircraftCD)) END AS AircraftCD,    
  AircraftDescription,   
   isnull(AircraftDescription,'') + ', ( ' + ISNULL(AircraftCD,'') + ' )' as 'BindingDesc',  
 -- isnull(AircraftCD,'') + ', ' + ISNULL(AircraftDescription,'') as 'BindingDesc',     
  ChargeRate ,    
  ChargeUnit ,    
  PowerSetting ,    
  PowerDescription ,    
  WindAltitude ,    
  PowerSettings1Description ,    
  PowerSettings1TrueAirSpeed ,    
  PowerSettings1HourRange ,    
  PowerSettings1TakeOffBias ,    
  PowerSettings1LandingBias ,    
  PowerSettings2Description ,    
  PowerSettings2TrueAirSpeed ,    
  PowerSettings2HourRange ,    
  PowerSettings2TakeOffBias ,    
  PowerSettings2LandingBias ,    
  PowerSettings3Description ,    
  PowerSettings3TrueAirSpeed ,    
  PowerSettings3HourRange ,    
  PowerSettings3TakeOffBias ,    
  PowerSettings3LandingBias ,    
  IsFixedRotary ,    
  ClientID ,    
  CQChargeRate ,    
  CQChargeUnit ,    
  PositionRate ,    
  PostionUnit ,    
  StandardCrew ,    
  StandardCrewRON ,    
  AdditionalCrew ,    
  AdditionalCrewRON ,    
  CharterQuoteWaitTM ,    
  LandingFee ,    
  CostBy ,    
  FixedCost ,    
  PercentageCost ,    
  TaxRON ,    
  AdditionalCrewTax ,    
  IsCharterQuoteWaitTax ,    
  LandingTax ,    
  DescriptionRON ,    
  AdditionalCrewDescription ,    
  MinDailyREQ ,    
  LastUpdUID ,    
  LastUpdTS  ,    
  AircraftSize ,    
  AircraftTypeCD ,    
  IntlStdCrewNum ,    
  DomesticStdCrewNum ,    
  MinimumDayUseHrs ,    
  DailyUsageAdjTax ,    
  LandingFeeTax ,    
  IsDeleted,  
  IsInActive,
  MarginalPercentage    
  FROM  [Aircraft] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0    
order by AircraftCD    
end    
GO


