
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQLeg]    Script Date: 02/18/2014 11:08:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQLeg]    Script Date: 02/18/2014 11:08:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQLeg]
(
	@CQLegID bigint,
	@CustomerID bigint,
	@CQMainID bigint,
	@QuoteID int,
	@LegNUM int,
	@DAirportID bigint,
	@AAirportID bigint,
	@Distance numeric(5, 0),
	@PowerSetting char(1),
	@TakeoffBIAS numeric(6, 3),
	@LandingBIAS numeric(6, 3),
	@TrueAirSpeed numeric(3, 0),
	@WindsBoeingTable numeric(5, 0),
	@ElapseTM numeric(7, 3),
	@IsDepartureConfirmed bit,
	@IsArrivalConfirmation bit,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@DepartureGreenwichDTTM datetime,
	@ArrivalGreenwichDTTM datetime,
	@HomeDepartureDTTM datetime,
	@HomeArrivalDTTM datetime,
	@FBOID bigint,
	@FlightPurposeDescription varchar(100),
	@DutyHours numeric(12, 3),
	@RestHours numeric(12, 3),
	@FlightHours numeric(12, 3),
	@Notes varchar(MAX),
	@DutyTYPE numeric(1, 0),
	@CrewDutyRulesID bigint,
	@IsDutyEnd bit,
	@FedAviationRegNUM char(3),
	@WindReliability int,
	@CQOverRide numeric(4, 1),
	@NextLocalDTTM datetime,
	@NextGMTDTTM datetime,
	@NextHomeDTTM datetime,
	@ChargeRate numeric(17, 2),
	@ChargeTotal numeric(17, 2),
	@IsPositioning bit,
	@IsTaxable bit,
	@FeeGroupDescription varchar(100),
	@LegMargin numeric(17, 2),
	@CrewDutyAlert char(3),
	@RemainOverNightCNT int,
	@TaxRate numeric(5, 2),
	@PassengerTotal numeric(4, 0),
	@ReservationAvailable numeric(4, 0),
	@IsPrintable bit,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@IsRural bit,
	@CrewNotes varchar(MAX),
	@ClientID bigint,
	@DayRONCNT numeric(2, 0),
	@IsSameDay bit,
	@IsErrorTag bit,
	@OQAAirportID bigint,
	@OQDAirportID bigint,
	@OIAAirportID bigint,
	@OIDAirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@PAXBlockedSeat numeric(4, 0)
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQLeg] 
	SET [LegNUM] = @LegNUM, [DAirportID] = @DAirportID, [AAirportID] = @AAirportID, [Distance] = @Distance, [PowerSetting] = @PowerSetting, [TakeoffBIAS] = @TakeoffBIAS, [LandingBIAS] = @LandingBIAS, [TrueAirSpeed] = @TrueAirSpeed, [WindsBoeingTable] = @WindsBoeingTable, [ElapseTM] = @ElapseTM, [IsDepartureConfirmed] = @IsDepartureConfirmed, [IsArrivalConfirmation] = @IsArrivalConfirmation, [DepartureDTTMLocal] = @DepartureDTTMLocal, [ArrivalDTTMLocal] = @ArrivalDTTMLocal, [DepartureGreenwichDTTM] = @DepartureGreenwichDTTM, [ArrivalGreenwichDTTM] = @ArrivalGreenwichDTTM, [HomeDepartureDTTM] = @HomeDepartureDTTM, [HomeArrivalDTTM] = @HomeArrivalDTTM, [FBOID] = @FBOID, [FlightPurposeDescription] = @FlightPurposeDescription, 
	[DutyHours] = CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
		,RestHours = CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
		,FlightHours = CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END
	,[Notes] = @Notes, [DutyTYPE] = @DutyTYPE, [CrewDutyRulesID] = @CrewDutyRulesID, [IsDutyEnd] = @IsDutyEnd, [FedAviationRegNUM] = @FedAviationRegNUM, [WindReliability] = @WindReliability, [CQOverRide] = @CQOverRide, [NextLocalDTTM] = @NextLocalDTTM, [NextGMTDTTM] = @NextGMTDTTM, [NextHomeDTTM] = @NextHomeDTTM, [ChargeRate] = @ChargeRate, [ChargeTotal] = @ChargeTotal, [IsPositioning] = @IsPositioning, [IsTaxable] = @IsTaxable, [FeeGroupDescription] = @FeeGroupDescription, [LegMargin] = @LegMargin, [CrewDutyAlert] = @CrewDutyAlert, [RemainOverNightCNT] = @RemainOverNightCNT, [TaxRate] = @TaxRate, [PassengerTotal] = @PassengerTotal, [ReservationAvailable] = @ReservationAvailable, [IsPrintable] = @IsPrintable, [FromDescription] = @FromDescription, [ToDescription] = @ToDescription, [IsRural] = @IsRural, [CrewNotes] = @CrewNotes, [ClientID] = @ClientID, [DayRONCNT] = @DayRONCNT, [IsSameDay] = @IsSameDay, [IsErrorTag] = @IsErrorTag, [OQAAirportID] = @OQAAirportID, [OQDAirportID] = @OQDAirportID, [OIAAirportID] = @OIAAirportID, [OIDAirportID] = @OIDAirportID, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, [PAXBlockedSeat] = @PAXBlockedSeat 
	WHERE [CQLegID] = @CQLegID AND [CustomerID] = @CustomerID


GO


