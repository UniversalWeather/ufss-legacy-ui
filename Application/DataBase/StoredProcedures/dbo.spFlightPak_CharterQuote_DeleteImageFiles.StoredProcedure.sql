IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteImageFiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteImageFiles]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteImageFiles]
(
	@RecordID BIGINT,
	@CustomerID BIGINT
)
AS
BEGIN
	UPDATE FileWarehouse SET IsDeleted = 1 
		WHERE CustomerID = CustomerID AND RecordID = @RecordID
ENd

GO


