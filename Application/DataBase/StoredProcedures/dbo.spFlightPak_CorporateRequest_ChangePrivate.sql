

/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_ChangePrivate]    Script Date: 02/21/2013 10:02:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_ChangePrivate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_ChangePrivate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_ChangePrivate]    Script Date: 02/21/2013 10:02:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_ChangePrivate]
(@CRLegID	BIGINT
,@IsPrivate bit
,@CustomerID	BIGINT
,@LastUpdUID VARCHAR(30))
AS BEGIN



	DECLARE @LastUpdTS DATETIME
	SET @LastUpdTS = GETUTCDATE()

	UPDATE CRLeg
	SET	IsPrivate = @IsPrivate,
		LastUpdUID = @LastUpdUID,
		LastUpdTS = @LastUpdTS
	WHERE CRLegID = @CRLegID

END

GO


