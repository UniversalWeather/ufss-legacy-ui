
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFlightPurposes]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFlightPurposes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFlightPurposes]
GO


CREATE PROCEDURE [dbo].[spGetAllFlightPurposes](@CustomerID BIGINT,@ClientID bigint)      
AS      
-- =============================================      
-- Author: Sujitha.V      
-- Create date: 17/4/2012      
-- Description: Get All Flight Purpose Informations      
-- =============================================      
SET NOCOUNT ON      
    IF(@ClientID>0)    
    BEGIN    
    SELECT     
       fp.[FlightPurposeID]    
       ,fp.[FlightPurposeCD]    
   ,fp.[FlightPurposeDescription]    
   ,fp.[CustomerID]    
   ,fp.[IsDefaultPurpose]    
   ,fp.[IsPersonalTravel]    
   ,fp.[ClientID]    
   ,fp.[LastUpdUID]    
   ,fp.[LastUpdTS]    
   ,fp.[IsWaitList]    
   ,fp.[IsDeleted] 
   ,fp.IsInactive   
   ,c.ClientCD      
FROM  [FlightPurpose] fp left outer join Client c on fp.ClientID=c.ClientID WHERE fp.CustomerID=@CustomerID AND ISNULL(fp.IsDeleted,0) = 0 and fp.ClientID =@ClientID    
 order by FlightPurposeCD  
END    
ELSE    
BEGIN    
 SELECT     
       fp.[FlightPurposeID]    
       ,fp.[FlightPurposeCD]    
   ,fp.[FlightPurposeDescription]    
   ,fp.[CustomerID]    
   ,fp.[IsDefaultPurpose]    
   ,fp.[IsPersonalTravel]    
   ,fp.[ClientID]    
   ,fp.[LastUpdUID]    
   ,fp.[LastUpdTS]    
   ,fp.[IsWaitList]    
   ,fp.[IsDeleted]    
   ,fp.IsInactive
   ,c.ClientCD      
FROM  [FlightPurpose] fp left outer join Client c on fp.ClientID=c.ClientID WHERE fp.CustomerID=@CustomerID AND ISNULL(fp.IsDeleted,0) = 0    
 order by FlightPurposeCD  
END
