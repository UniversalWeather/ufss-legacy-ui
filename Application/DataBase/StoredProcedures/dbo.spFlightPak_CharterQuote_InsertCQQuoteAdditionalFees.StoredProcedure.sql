

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQQuoteAdditionalFees]    Script Date: 04/10/2013 12:34:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQQuoteAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQQuoteAdditionalFees]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQQuoteAdditionalFees]    Script Date: 04/10/2013 12:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQQuoteAdditionalFees]
(
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@IsPrintable bit,
	@CQQuoteFeeDescription varchar(60),
	@QuoteAmount numeric(17, 2),
	@OrderNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@Quantity numeric(17,3), 
	@ChargeUnit VARCHAR(25) 

)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQQuoteAdditionalFeesID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQQuoteAdditionalFeesID OUTPUT 
		
	INSERT INTO [CQQuoteAdditionalFees] ([CQQuoteAdditionalFeesID], [CustomerID], [CQQuoteMainID], [IsPrintable], [CQQuoteFeeDescription], [QuoteAmount], [OrderNUM], [LastUpdUID], [LastUpdTS], [IsDeleted], [Quantity] ,[ChargeUnit]) 
		VALUES (@CQQuoteAdditionalFeesID, @CustomerID, @CQQuoteMainID, @IsPrintable, @CQQuoteFeeDescription, @QuoteAmount, @OrderNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@Quantity, @ChargeUnit);

	SELECT @CQQuoteAdditionalFeesID as CQQuoteAdditionalFeesID		

GO


