

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Utility_DeleteUserPreference]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Utility_DeleteUserPreference]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*

exec spFlightPak_Utility_DeleteUserPreference 'carrie_3', 10003

*/

CREATE PROCEDURE [dbo].[spFlightPak_Utility_DeleteUserPreference]
(
	@UserName VARCHAR(30),
	@CustomerID BIGINT
)  
AS          
SET NOCOUNT ON;          

BEGIN
	
	DELETE FROM UserPreference WHERE RTRIM(UserName) = RTRIM(@UserName) AND CustomerID = @CustomerID AND CategoryName = 'Utility'
	   
END 
  
GO


