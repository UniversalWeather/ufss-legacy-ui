
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceQuoteDetail]    Script Date: 03/13/2013 14:53:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQInvoiceQuoteDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceQuoteDetail]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceQuoteDetail]    Script Date: 03/13/2013 14:53:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQInvoiceQuoteDetail]
(
	@CustomerID bigint,
	@CQInvoiceMainID bigint,
	@InvoiceNUM int,
	@CQLegID bigint,
	@InvoiceDetailID int,
	@QuoteID int,
	@IsPrintable bit,
	@DepartureDT date,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@FlightHours numeric(5, 1),
	@Distance numeric(5, 0),
	@PassengerTotal numeric(3, 0),
	@FlightCharge numeric(17, 2),
	@InvoiceFlightChg numeric(17, 2),
	@IsTaxable bit,
	@TaxRate numeric(5, 2),
	@DAirportID bigint,
	@AAirportID bigint,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@LegID int,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@LegNum bigint
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQInvoiceQuoteDetailID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQInvoiceQuoteDetailID OUTPUT 
	
	INSERT INTO [CQInvoiceQuoteDetail] ([CQInvoiceQuoteDetailID], [CustomerID], [CQInvoiceMainID], [InvoiceNUM], [CQLegID], [InvoiceDetailID], [QuoteID], [IsPrintable], [DepartureDT], [FromDescription], [ToDescription], [FlightHours], [Distance], [PassengerTotal], [FlightCharge], [InvoiceFlightChg], [IsTaxable], [TaxRate], [DAirportID], [AAirportID], [DepartureDTTMLocal], [ArrivalDTTMLocal], [LegID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted],[LegNum]) 
		VALUES (@CQInvoiceQuoteDetailID, @CustomerID, @CQInvoiceMainID, @InvoiceNUM, @CQLegID, @InvoiceDetailID, @QuoteID, @IsPrintable, @DepartureDT, @FromDescription, @ToDescription, @FlightHours, @Distance, @PassengerTotal, @FlightCharge, @InvoiceFlightChg, @IsTaxable, @TaxRate, @DAirportID, @AAirportID, @DepartureDTTMLocal, @ArrivalDTTMLocal, @LegID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@LegNum);
	
	SELECT @CQInvoiceQuoteDetailID as CQInvoiceQuoteDetailID


GO


