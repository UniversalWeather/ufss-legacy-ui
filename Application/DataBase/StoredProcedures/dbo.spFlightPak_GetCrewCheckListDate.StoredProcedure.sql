

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewCheckListDate]    Script Date: 06/10/2013 13:48:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetCrewCheckListDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetCrewCheckListDate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewCheckListDate]    Script Date: 06/10/2013 13:48:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
    
CREATE PROCEDURE [dbo].[spFlightPak_GetCrewCheckListDate]          
(          
 @CustomerID BIGINT,      
 @CrewID BIGINT          
)          
AS          
BEGIN        
SET NOCOUNT ON;        
  
--Temporary Table to calculate FlightLogHours in CrewRoster CheckList Grid  
DECLARE @FlightLogHours TABLE   
(   
CheckListCD CHAR(3) NULL,   
FlightHours NUMERIC(9,3) NULL,   
BlockHours NUMERIC(9,3) NULL   
)  
  
--Start Insert into Temporary Table to calculate FlightLogHours in CrewRoster CheckList Grid  
  
INSERT INTO @FlightLogHours (CheckListCD,FlightHours,BlockHours)  
SELECT CD.CheckListCD,SUM(TEMP.FlightHours) FlightHours,SUM(TEMP.BlockHours) BlockHours FROM  
(SELECT     Fleet.AircraftCD, Fleet.AircraftID, Fleet.FleetID, PostflightLeg.POLegID, PostflightLeg.POLogID, PostflightLeg.ScheduleDTTMLocal,   
           PostflightCrew.BlockHours, PostflightCrew.FlightHours, PostflightCrew.DutyHours, PostflightCrew.DutyEnd, PostflightCrew.CrewID, 'FL' AS rtype,Fleet.CustomerID  
FROM       PostflightLeg  INNER JOIN  
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN  
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightCrew.POLogID = PostflightMain.POLogID INNER JOIN  
                      Fleet ON PostflightMain.FleetID = Fleet.FleetID  
    
  AND Fleet.CustomerID = PostflightCrew.CustomerID AND PostflightLeg.CustomerID = PostflightMain.CustomerID   
--  AND Fleet.CustomerID = 10002 AND PostflightCrew.CrewID = 10002182943   
    
UNION ALL  
  
 SELECT   '', AircraftID, null,null,null,SessionDT, null, FlightHours,null,null, CrewID,  'SL' AS rtype,CustomerID  
FROM   PostflightSimulatorLog   
--WHERE CrewID = 10002182943 AND CustomerID = 10002  
)TEMP  
  
INNER JOIN CrewChecklistDetail CD ON CD.CrewID=TEMP.CrewID  
WHERE CONVERT(DATE,TEMP.ScheduleDTTMLocal) BETWEEN CONVERT(DATE,CD.PreviousCheckDT) AND CONVERT(DATE,GETDATE()) AND CD.AircraftID = TEMP.AircraftID  
AND CD.IsInActive = 0 AND TEMP.CrewID =@CrewID   
AND TEMP.CustomerID=@CustomerID  
GROUP BY CD.CheckListCD  
  
--End Insert into Temporary Table to calculate FlightLogHours in CrewRoster CheckList Grid  
  
    
SELECT   CrewCheckListDetail.[CheckListID]        
  ,CrewCheckListDetail.[CheckListCD]      
  ,isnull((CrewChecklist.CrewChecklistDescription),'') as 'CrewChecklistDescription'      
  ,CrewCheckListDetail.[PreviousCheckDT]        
  ,CrewCheckListDetail.[CustomerID]        
  ,CrewCheckListDetail.[CrewID]        
  ,CrewCheckListDetail.[DueDT]        
  ,CrewCheckListDetail.[AlertDT]        
  ,CrewCheckListDetail.[BaseMonthDT]        
  ,CrewCheckListDetail.[FrequencyMonth]        
  ,CrewCheckListDetail.[AlertDays]        
  ,CrewCheckListDetail.[GraceDays]        
  ,CrewCheckListDetail.[GraceDT]        
  ,CrewCheckListDetail.[IsMonthEnd]        
  ,CrewCheckListDetail.[LastUpdUID]        
  ,CrewCheckListDetail.[LastUpdTS]        
  ,CrewCheckListDetail.[IsStopCALC]        
  ,CrewCheckListDetail.[IsOneTimeEvent]        
  ,CrewCheckListDetail.[IsNoConflictEvent]        
  ,CrewCheckListDetail.[IsNoCrewCheckListREPTt]        
  ,CrewCheckListDetail.[IsNoChecklistREPT]        
  ,CrewCheckListDetail.[AircraftID]        
  ,CrewCheckListDetail.[IsInActive]      
  ,CrewCheckListDetail.[Specific]      
  ,CrewCheckListDetail.[IsPilotInCommandFAR91]      
  ,CrewCheckListDetail.[IsPilotInCommandFAR135]      
  ,CrewCheckListDetail.[IsSecondInCommandFAR91]      
  ,CrewCheckListDetail.[IsSecondInCommandFAR135]      
  ,CrewCheckListDetail.[IsCompleted]      
  ,CrewCheckListDetail.[OriginalDT]      
  ,CrewCheckListDetail.[TotalREQFlightHrs]      
  ,CrewCheckListDetail.[IsPrintStatus]      
  ,CrewCheckListDetail.[IsPassedDueAlert]      
  ,CrewCheckListDetail.[Frequency]        
  ,CrewCheckListDetail.[IsNextMonth]        
  ,CrewCheckListDetail.[IsEndCalendarYear]      
  ,CrewCheckListDetail.[IsScheduleCheck]        
  ,CrewCheckListDetail.[IsDeleted]      
        
  ,AircraftType.AircraftCD as AircraftTypeCD      
  ,AircraftType.AircraftDescription as AircraftDescription        
  ,CrewChecklist.CrewCheckCD as CrewCheckCD      
  ,FlightLogHours.FlightHours as FlightLogHours      
FROM CrewCheckListDetail as CrewCheckListDetail      
LEFT OUTER JOIN CrewCheckList as CrewChecklist ON (CrewChecklist.crewcheckcd=CrewCheckListDetail.CheckListcd and CrewCheckList.[CustomerID] = @CustomerID and CrewCheckList.isdeleted = 0)
LEFT OUTER JOIN  Aircraft  as  AircraftType on AircraftType.AircraftID=CrewCheckListDetail.AircraftID       
--Using the temp table in join to display the FlightLogHours  
LEFT OUTER JOIN @FlightLogHours as FlightLogHours  ON FlightLogHours.CheckListCD = CrewCheckListDetail.CheckListCD    
WHERE CrewCheckListDetail.[CustomerID] = @CustomerID AND  CrewCheckListDetail.CrewID = @CrewID AND CrewCheckListDetail.isdeleted = 0         
order by CrewCheckListDetail.CheckListCD    
END      
      
GO


