
/****** Object:  StoredProcedure [dbo].[spAddCrewGroup]    Script Date: 01/24/2013 12:59:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCrewGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCrewGroup]
GO

/****** Object:  StoredProcedure [dbo].[spAddCrewGroup]    Script Date: 01/24/2013 12:59:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddCrewGroup](@CrewGroupCD CHAR(4),
                                        @CustomerID BIGINT,
                                        @CrewGroupDescription  VARCHAR(30),
                                        @LastUpdUID VARCHAR(30),
                                        @LastUpdTS DATETIME,
                                        @HomeBaseID BIGINT,
                                        @IsDeleted BIT,
                                        @IsInActive BIT)  
  
  
AS  
BEGIN   
SET NOCOUNT ON  
DECLARE  @CrewGroupID BIGINT
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @CrewGroupID OUTPUT
SET @LastUpdTS = GETUTCDATE() 
INSERT INTO [CrewGroup]([CrewGroupID]
					   ,[CrewGroupCD]
					   ,[CustomerID]
					   ,[CrewGroupDescription]
					   ,[LastUpdUID]
					   ,[LastUpdTS]
					   ,[HomebaseID]
					   ,[IsDeleted]
					   ,[IsInActive]
					   )
                 VALUES(@CrewGroupID
					   ,@CrewGroupCD
					   ,@CustomerID
					   ,@CrewGroupDescription
					   ,@LastUpdUID
					   ,@LastUpdTS
					   ,@HomebaseID
					   ,@IsDeleted
					   ,@IsInActive
					   )
  
END

GO


