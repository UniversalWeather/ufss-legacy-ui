
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllFlightPurposes]    Script Date: 03/14/2014 21:59:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllFlightPurposes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllFlightPurposes]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllFlightPurposes]    Script Date: 03/14/2014 21:59:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_fss_GetAllFlightPurposes]
(
	@CustomerID BIGINT
	,@ClientID BIGINT
    ,@FlightPurposeID BIGINT    
	,@FlightPurposeCD char(2) 
	,@FetchDefaultPurposeOnly BIT     
	,@FetchPersonalTravelOnly BIT         	
	,@FetchActiveOnly BIT     
	
)      
AS      
-- =============================================                
-- Author: Sridhar               
-- Create date: 14/03/2014                
-- Description: Get All FlightPurposes details for Popup          
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 0,0,1    
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 0,0,0    
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 0,1,1    
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 0,1,0    
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 1,0,1    
-- Exec sp_fss_GetAllFlightPurposes 10099,0,0,'', 0,0,1    
-- Exec sp_fss_GetAllFlightPurposes 10099, 0, 10099197,'',0,0,1   
-- Exec sp_fss_GetAllFlightPurposes 10099, 0, 0, 'GP',0,0,0    
-- =============================================     
SET NOCOUNT ON      

IF(@ClientID > 0)    
BEGIN    
    SELECT     
			fp.FlightPurposeID
			,fp.FlightPurposeCD    
			,fp.FlightPurposeDescription
			,fp.CustomerID
			,fp.IsDefaultPurpose    
			,fp.IsPersonalTravel    
			,fp.ClientID    
			,fp.LastUpdUID
			,fp.LastUpdTS    
			,fp.IsWaitList    
			,ISNULL(fp.IsDeleted,0) IsDeleted       
			,ISNULL(fp.IsInactive,0) IsInactive   
			,C.ClientCD      
	FROM 
			FlightPurpose fp left outer join Client C on fp.ClientID=C.ClientID 
	WHERE 
			fp.CustomerID=@CustomerID and fp.ClientID =@ClientID    
			AND fp.FlightPurposeID = case when @FlightPurposeID <>0 then @FlightPurposeID else fp.FlightPurposeID end     
			AND fp.FlightPurposeCD = case when Ltrim(Rtrim(@FlightPurposeCD)) <> '' then @FlightPurposeCD else fp.FlightPurposeCD end                                      
			AND ISNULL(fp.IsDefaultPurpose,0) = case when @FetchDefaultPurposeOnly =0 then ISNULL(fp.IsDefaultPurpose,0) else @FetchDefaultPurposeOnly end    
			AND ISNULL(fp.IsPersonalTravel,0) = case when @FetchPersonalTravelOnly =0 then ISNULL(fp.IsPersonalTravel,0) else @FetchPersonalTravelOnly end    
			AND ISNULL(fp.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(fp.IsInActive,0) else 0 end    
			AND ISNULL(fp.IsDeleted,0) = 0                                    
 
	order by FlightPurposeCD  
END    
ELSE    
BEGIN    
	SELECT     
			fp.FlightPurposeID
			,fp.FlightPurposeCD    
			,fp.FlightPurposeDescription
			,fp.CustomerID
			,fp.IsDefaultPurpose    
			,fp.IsPersonalTravel    
			,fp.ClientID    
			,fp.LastUpdUID
			,fp.LastUpdTS    
			,fp.IsWaitList    
			,ISNULL(fp.IsDeleted,0) IsDeleted       
			,ISNULL(fp.IsInactive,0) IsInactive   
			,C.ClientCD    
	FROM  
		FlightPurpose fp left outer join Client C on fp.ClientID=C.ClientID 
	WHERE 
		fp.CustomerID=@CustomerID 
		AND fp.FlightPurposeID = case when @FlightPurposeID <>0 then @FlightPurposeID else fp.FlightPurposeID end     
		AND fp.FlightPurposeCD = case when Ltrim(Rtrim(@FlightPurposeCD)) <> '' then @FlightPurposeCD else fp.FlightPurposeCD end                                      
		AND ISNULL(fp.IsDefaultPurpose,0) = case when @FetchDefaultPurposeOnly =0 then ISNULL(fp.IsDefaultPurpose,0) else @FetchDefaultPurposeOnly end    
		AND ISNULL(fp.IsPersonalTravel,0) = case when @FetchPersonalTravelOnly =0 then ISNULL(fp.IsPersonalTravel,0) else @FetchPersonalTravelOnly end    			
		AND ISNULL(fp.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(fp.IsInActive,0) else 0 end    
		AND ISNULL(fp.IsDeleted,0) = 0       
 
	order by FlightPurposeCD  
END

GO


