/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFleetCalendarEntries]    Script Date: 01/10/2013 19:25:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetFleetCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetFleetCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetFleetCalendarEntries]    Script Date: 01/10/2013 19:25:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
-- =============================================  
-- Author:  <Author,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetFleetCalendarEntries](  
 @CustomerID bigint,  
 @IN_STR VARCHAR(10),  
 @START_DATE DATETIME,  
 @END_DATE DATETIME)  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  

IF UPPER(@IN_STR)='NONE'  
   SET @IN_STR='Local'
     
IF @IN_STR='Local'  
BEGIN  
 SELECT  F.TailNum AS TailNumber,PFL.DepartureDTTMLocal AS StartDate,  
   PFL.ArrivalDTTMLocal AS EndDate,  
   A.ICAOID,HB.IcaoID HomeBaseCD,AD.AircraftDutyCD,  
   AD.AircraftDutyDescription,PFL.DutyTYPE,  
   CL.ClientCD,CL.ClientID,A.AirportID,C.HomebaseID,F.FleetID,  
   PFM.TripDescription as Notes,PFL.LegID,PFL.TripID,PFM.TripNUM,PFM.FleetCalendarNotes as NoteSec 
 FROM PreflightMain PFM  
   Inner join PreflightLeg PFL ON PFL.TripID=PFM.TripID  
   inner join Fleet F ON F.FleetID=PFM.FleetID  
   Inner join Airport A ON A.AirportID=PFL.DepartICAOID  
   Inner join Company C ON C.HomebaseID=PFM.HomebaseID  
   Inner join AircraftDuty AD ON AD.AircraftDutyCD=PFL.DutyTYPE and AD.CustomerID = @CustomerID AND AD.IsDeleted = 0  
   INNER JOIN Airport HB ON HB.AirportID=C.HomebaseAirportID  
   LEFT OUTER JOIN Client CL on CL.ClientID=PFM.ClientID  
 WHERE F.CustomerID=@CustomerID and F.IsDeleted!=1 and PFM.RecordType='M'  
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and AD.IsDeleted!=1 AND A.IsDeleted!=1 --AND CL.IsDeleted!=1  
   AND (PFL.DepartureDTTMLocal >= @START_DATE AND PFL.DepartureDTTMLocal <=@END_DATE OR (PFL.ArrivalDTTMLocal >= @START_DATE AND PFL.ArrivalDTTMLocal <=@END_DATE))  
   ORDER BY StartDate  
END  
ELSE IF @IN_STR='Home'  
BEGIN  
 SELECT  F.TailNum AS TailNumber,PFL.HomeDepartureDTTM AS StartDate,  
   PFL.HomeArrivalDTTM AS EndDate,A.ICAOID,HB.IcaoID HomeBaseCD,  
   AD.AircraftDutyCD,AD.AircraftDutyDescription,PFL.DutyTYPE,  
   CL.ClientCD,CL.ClientID,A.AirportID,C.HomebaseID,F.FleetID,  
   PFM.TripDescription as Notes,PFL.LegID,PFL.TripID,PFM.TripNUM,PFM.FleetCalendarNotes as NoteSec   
 FROM PreflightMain PFM  
   Inner join PreflightLeg PFL ON PFL.TripID=PFM.TripID  
   inner join Fleet F ON F.FleetID=PFM.FleetID  
   Inner join Airport A ON A.AirportID=PFL.DepartICAOID  
   Inner join Company C ON C.HomebaseID=PFM.HomebaseID  
   Inner join AircraftDuty AD ON AD.AircraftDutyCD=PFL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0  
   INNER JOIN Airport HB ON HB.AirportID=C.HomebaseAirportID  
   LEFT OUTER JOIN Client CL on CL.ClientID=PFM.ClientID  
 WHERE F.CustomerID=@CustomerID and f.IsDeleted!=1 and PFM.RecordType='M'  
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and AD.IsDeleted!=1 AND A.IsDeleted!=1 --AND CL.IsDeleted!=1  
   AND (PFL.HomeDepartureDTTM >= @START_DATE AND PFL.HomeDepartureDTTM <=@END_DATE OR (PFL.HomeArrivalDTTM >= @START_DATE AND PFL.HomeArrivalDTTM <=@END_DATE))  
   ORDER BY StartDate  
END  
ELSE IF @IN_STR='Utc'  
BEGIN  
 SELECT  F.TailNum AS TailNumber,PFL.DepartureGreenwichDTTM AS StartDate,  
   PFL.ArrivalGreenwichDTTM AS EndDate,A.ICAOID,HB.IcaoID HomeBaseCD,  
   AD.AircraftDutyCD,AD.AircraftDutyDescription,PFL.DutyTYPE,  
   CL.ClientCD,CL.ClientID,A.AirportID,C.HomebaseID,F.FleetID,  
   PFM.TripDescription as Notes,PFL.LegID,PFL.TripID,PFM.TripNUM,PFM.FleetCalendarNotes as NoteSec   
 FROM PreflightMain PFM  
   Inner join PreflightLeg PFL ON PFL.TripID=PFM.TripID  
   inner join Fleet F ON F.FleetID=PFM.FleetID  
   Inner join Airport A ON A.AirportID=PFL.DepartICAOID  
   Inner join Company C ON C.HomebaseID=PFM.HomebaseID  
   Inner join AircraftDuty AD ON AD.AircraftDutyCD=PFL.DutyTYPE and AD.CustomerID = @CustomerID AND AD.IsDeleted = 0  
   INNER JOIN Airport HB ON HB.AirportID=C.HomebaseAirportID  
   LEFT OUTER JOIN Client CL on CL.ClientID=PFM.ClientID  
 WHERE F.CustomerID=@CustomerID and f.IsDeleted!=1 and PFM.RecordType='M'  
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and AD.IsDeleted!=1 AND A.IsDeleted!=1 --AND CL.IsDeleted!=1  
   AND (PFL.DepartureGreenwichDTTM >= @START_DATE AND PFL.DepartureGreenwichDTTM <=@END_DATE OR (PFL.ArrivalGreenwichDTTM >= @START_DATE AND PFL.ArrivalGreenwichDTTM <=@END_DATE))  
   ORDER BY StartDate  
END  
END  
  
  
GO


