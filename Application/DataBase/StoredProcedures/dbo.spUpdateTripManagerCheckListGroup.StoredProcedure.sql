
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTripManagerCheckListGroup]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateTripManagerCheckListGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateTripManagerCheckListGroup]
go
CREATE procedure [dbo].[spUpdateTripManagerCheckListGroup]  
 (@CheckGroupID bigint  
 ,@CheckGroupCD char(3)  
 ,@CustomerID bigint  
 ,@CheckGroupDescription varchar(40)  
 ,@LastUpdUID varchar(30)  
 ,@LastUpdTS datetime  
 ,@IsDeleted bit
 ,@IsInActive bit)  
  
-- =============================================  
-- Author:Prakash Pandian.S  
-- Create date: 12/5/2012  
-- Description: Update the TripManagerCheckListGroup information  
-- =============================================  
as  
begin   
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
UPDATE [TripManagerCheckListGroup]  
   SET --@CheckGroupID=@CheckGroupID  
  CheckGroupDescription=@CheckGroupDescription  
  ,LastUpdUID=@LastUpdUID  
  ,LastUpdTS=@LastUpdTS  
  ,IsDeleted=@IsDeleted  
  ,IsInActive=@IsInActive
 WHERE CustomerID=@CustomerID and CheckGroupCD=@CheckGroupCD  
  
end
GO
