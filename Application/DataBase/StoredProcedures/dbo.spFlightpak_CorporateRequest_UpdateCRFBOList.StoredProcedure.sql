
/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_UpdateCRFBOList]    Script Date: 02/21/2013 18:25:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_UpdateCRFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRFBOList]
GO



/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_UpdateCRFBOList]    Script Date: 02/21/2013 18:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_UpdateCRFBOList]
(@CRFBOListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@FBOID BIGINT
,@CRFBOListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
		
	UPDATE CRFBOList
	SET CustomerID = @CustomerID
		,CRLegID = @CRLegID
		,TripNUM = @TripNUM
		,LegID = @LegID
		,RecordType = @RecordType
		,FBOID = @FBOID
		,CRFBOListDescription = @CRFBOListDescription
		,PhoneNUM = @PhoneNUM
		,IsCompleted = @IsCompleted
		,AirportID = @AirportID
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
	WHERE 
		CRFBOListID = @CRFBOListID

END
GO


