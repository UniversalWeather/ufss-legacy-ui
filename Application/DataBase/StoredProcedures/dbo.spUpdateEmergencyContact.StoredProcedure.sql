
/****** Object:  StoredProcedure [dbo].[spUpdateEmergencyContact]    Script Date: 09/13/2012 12:37:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateEmergencyContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateEmergencyContact]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateEmergencyContact]    Script Date: 09/13/2012 12:37:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateEmergencyContact]  
(@EmergencyContactID bigint  
,@EmergencyContactCD char(5)  
,@CustomerID bigint  
,@LastName varchar(20)  
,@FirstName varchar(20)  
,@MiddleName varchar(20)  
,@Addr1 varchar(25)  
,@Addr2 varchar(25)  
,@CityName varchar(25)  
,@StateName varchar(10)  
,@PostalZipCD varchar(15)  
,@CountryID bigint  
,@PhoneNum varchar(25)  
,@FaxNum varchar(25)  
,@CellPhoneNum varchar(15)  
,@EmailAddress varchar(100)  
,@LastUpdUID varchar(30)  
,@LastUpdTS datetime  
,@IsDeleted bit
,@BusinessPhone	varchar(25)
,@BusinessFax	varchar(25)
,@CellPhoneNum2	varchar(25)
,@OtherPhone	varchar(25)
,@PersonalEmail	varchar(250)
,@OtherEmail	varchar(250)
,@Addr3	varchar(40)
,@IsInActive bit)  
AS    
BEGIN     
SET NOCOUNT ON    
SET @LastUpdTS = GETUTCDATE()
UPDATE  [EmergencyContact]    
SET EmergencyContactCD = @EmergencyContactCD  
 ,CustomerID = @CustomerID  
 ,LastName = @LastName  
 ,FirstName = @FirstName  
 ,MiddleName = @MiddleName  
 ,Addr1 = @Addr1  
 ,Addr2 = @Addr2  
 ,CityName = @CityName  
 ,StateName = @StateName  
 ,PostalZipCD = @PostalZipCD  
 ,CountryID = @CountryID  
 ,PhoneNum = @PhoneNum  
 ,FaxNum = @FaxNum  
 ,CellPhoneNum = @CellPhoneNum  
 ,EmailAddress = @EmailAddress  
 ,LastUpdUID = @LastUpdUID  
 ,LastUpdTS = @LastUpdTS  
 ,IsDeleted = @IsDeleted
 ,BusinessPhone	= @BusinessPhone	
 ,BusinessFax = @BusinessFax	
 ,CellPhoneNum2	= @CellPhoneNum2	
 ,OtherPhone = @OtherPhone	
 ,PersonalEmail	= @PersonalEmail	
 ,OtherEmail = @OtherEmail	
 ,Addr3	= @Addr3 
 ,IsInActive = @IsInActive
  
WHERE EmergencyContactID = @EmergencyContactID  
END

GO


