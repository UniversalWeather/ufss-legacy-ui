
/****** Object:  StoredProcedure [dbo].[spDeleteCompany]    Script Date: 08/08/2013 14:08:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCompany]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCompany]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteCompany]    Script Date: 08/08/2013 14:08:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[spDeleteCompany] @HomebaseID=10099180565,@LastUpdUID='SUPERVISOR_99',@LastUpdTS=NULL,@IsDeleted=1
--exec [dbo].[spDeleteCompany] @HomebaseID=10099181348,@LastUpdUID='SUPERVISOR_99',@LastUpdTS=NULL,@IsDeleted=1
CREATE PROCEDURE [dbo].[spDeleteCompany] (    
  @HomebaseID BIGINT   
 ,@LastUpdUID VARCHAR(30)    
 ,@LastUpdTS DATETIME    
 ,@IsDeleted BIT         
 )    
 -- =============================================    
 -- Author:Sujitha.V    
 -- Create date: 13/9/2012     
 -- Description: Delete the Catering information. Set IsDeleted to false    
 -- Updated by: J.Ramesh on 08-Aug-2013 to fix homebase deletion when related records are found 
 -- =============================================    
AS    
BEGIN    
 SET NoCOUNT ON  
 
 --Check if the record is not being used anywhere in the application before Delete    
 DECLARE @RecordUsed INT    
 DECLARE @ENTITYNAME VARCHAR(50)  
 --Newly added 
 DECLARE @LC_TABLENAME VARCHAR(50) 
 DECLARE @HomebaseAirportID BIGINT
 DECLARE @CustomerID BIGINT
 DECLARE @AirportID BIGINT
 DECLARE @IcaoID CHAR(4)
 DECLARE @DYNAMICSQL NVARCHAR(250)  
 DECLARE @PARAMLIST NVARCHAR(100)  
 DECLARE @LC_COUNT INT  
 DECLARE @LC_RESULT INT
 
 SET @LC_COUNT = 0
    
 SET @ENTITYNAME = N'Company'; -- Type Table Name    
 
 --Check if Airport has any custom record
	SELECT @HomebaseAirportID = HomebaseAirportID, @CustomerID = CustomerID FROM Company WHERE HomebaseID = @HomebaseID
	SELECT @IcaoID = IcaoID FROM Airport WHERE AirportID = @HomebaseAirportID
	SELECT @AirportID = AirportID FROM Airport WHERE IcaoID = @IcaoID AND CustomerID = @CustomerID
    
 --Check if the record is not being used in Crew before checking in all other tables 

	--Check for Custom AirportID
	SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Crew WHERE HomebaseID ' + ' = @LC_Key'
	SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  
	
	EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @AirportID, @RecordCount = @LC_COUNT OUTPUT 
	
	--Check for HomebaseAirportID
	IF (@LC_COUNT = 0)
	BEGIN 		
	
		SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Crew WHERE HomebaseID ' + ' = @LC_Key'
		SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  
	
		EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @HomebaseAirportID, @RecordCount = @LC_COUNT OUTPUT  
	END
-----------------------------------------------------
 --Check if the record is not being used in Passenger before checking in all other tables
	--Check for Custom AirportID
	IF (@LC_COUNT = 0)
	BEGIN
		SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Passenger WHERE HomebaseID ' + ' = @LC_Key'
		SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  

		EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @AirportID, @RecordCount = @LC_COUNT OUTPUT  
	END 	
	--Check for HomebaseAirportID
	IF (@LC_COUNT = 0)
	BEGIN
		SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Passenger WHERE HomebaseID ' + ' = @LC_Key'
		SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  

		EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @HomebaseAirportID, @RecordCount = @LC_COUNT OUTPUT  
	END
-----------------------------------------------------
 --Check if the record is not being used in Vendor before checking in all other tables 
	--Check for Custom AirportID
	IF (@LC_COUNT = 0)
	BEGIN
		SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Vendor WHERE HomebaseID ' + ' = @LC_Key'
		SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  

		EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @AirportID, @RecordCount = @LC_COUNT OUTPUT  
	END
	--Check for HomebaseAirportID
	IF (@LC_COUNT = 0)
	BEGIN
		SET @DYNAMICSQL = N'SELECT ' + '@RecordCount' + ' = COUNT(*) FROM Vendor WHERE HomebaseID ' + ' = @LC_Key'
		SET @PARAMLIST = N'@LC_Key BIGINT, @RecordCount INT OUTPUT'  

		EXECUTE sp_executesql @DYNAMICSQL, @PARAMLIST, @LC_Key = @HomebaseAirportID, @RecordCount = @LC_COUNT OUTPUT  
	END
 --End of Check if the record is not being used in Crew, Passenger, Vendor before checking in all other tables 
 
 EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @HomebaseID, @RecordUsed OUTPUT    

--End of Delete Check    
    
 IF (@RecordUsed <> 0 OR @LC_COUNT >= 1)    
 BEGIN     
	RAISERROR(N'-500010', 17, 1)    
 END    
 ELSE    
 BEGIN     
	SET @LastUpdTS = GETUTCDATE()    
	UPDATE [Company]    
		SET LastUpdUID = @LastUpdUID    
		,LastUpdTS = @LastUpdTS    
		,IsDeleted = @IsDeleted    
	WHERE HomebaseID  = @HomebaseID     
 END    
END  
GO

