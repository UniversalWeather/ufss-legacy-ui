/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewCalendarEntries]    Script Date: 01/10/2013 18:38:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetCrewCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewCalendarEntries]    Script Date: 01/10/2013 18:38:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
-- =============================================  
-- Author:  Veerendra  
-- Create date: 24-July-2012  
-- Description: Get Crew Calendar Entries  
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewCalendarEntries](  
 @CustomerID bigint,  
 @IN_STR VARCHAR(10),  
 @START_DATE DATETIME,  
 @END_DATE DATETIME)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  

IF UPPER(@IN_STR)='NONE'  
   SET @IN_STR='LOCAL'

IF UPPER(@IN_STR)='LOCAL'  
BEGIN  
  
 SELECT CW.CrewID,CW.CrewCD,PFCL.PreflightCrewListID,PFL.LegID,  
   A.IcaoID,A.AirportID,PFL.DepartureDTTMLocal AS StartDate,PFL.ArrivalDTTMLocal AS EndDate,     
   PFL.DutyTYPE,CD.DutyTypeCD,CD.DutyTypesDescription,     
   PFM.TripID,PFM.RecordType,PFM.TripDescription as Notes,PFM.HomebaseID,PFM.FleetID,PFM.ClientID,     
   F.TailNum, HB.IcaoID HomebaseCD,CL.ClientCD,PFM.PreviousNUM,
   PFM.TripNUM,isnull(PSL.SimulatorLog,0) AS SimulatorLog,ISNULL(PFM.IsLog,0) AS IsLog,PFM.CrewCalendarNotes  as NoteSec  
 FROM Crew CW   
   INNER JOIN PreflightCrewList PFCL ON PFCL.CrewID=CW.CrewID  
   INNER JOIN PreflightLeg PFL ON PFL.LegID=PFCL.LegID  
   INNER JOIN PreflightMain PFM ON PFM.TripID=PFL.TripID  
   LEFT OUTER JOIN Fleet F ON F.FleetID=PFM.FleetID  
   INNER JOIN Company C ON C.HomebaseID=PFM.HomebaseID  
   LEFT OUTER JOIN Client CL ON CL.ClientID =PFM.ClientID--1  
   INNER JOIN Airport A ON A.AirportID=PFL.DepartICAOID  
   INNER JOIN CrewDutyType CD ON CD.DutyTypeCD=PFL.DutyTYPE and CD.CustomerID = @CustomerID and CD.IsDeleted = 0
   INNER JOIN Airport HB ON HB.AirportId = C.HomebaseAirportID -- Home Base Airport ID change 
   LEFT OUTER JOIN PostflightSimulatorLog PSL ON PSL.TripID=PFM.TripID 
 WHERE  CW.CustomerID=@CustomerID AND PFM.RecordType='C'      
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and CD.IsDeleted!=1 and CW.IsDeleted!=1  
   AND (PFL.DepartureDTTMLocal >= @START_DATE AND PFL.DepartureDTTMLocal <=@END_DATE OR (PFL.ArrivalDTTMLocal >= @START_DATE AND PFL.ArrivalDTTMLocal <=@END_DATE))  
   ORDER BY StartDate
END  
ELSE IF UPPER(@IN_STR)='HOME'  
BEGIN  
  
 SELECT CW.CrewID,CW.CrewCD,PFCL.PreflightCrewListID,PFL.LegID,  
   A.IcaoID,A.AirportID,PFL.HomeDepartureDTTM AS StartDate,PFL.HomeArrivalDTTM AS EndDate,     
   PFL.DutyTYPE,CD.DutyTypeCD,CD.DutyTypesDescription,      
   PFM.TripID,PFM.RecordType,PFM.TripDescription as Notes,PFM.HomebaseID,PFM.FleetID,PFM.ClientID,     
   F.TailNum, HB.IcaoID HomebaseCD,CL.ClientCD,PFM.PreviousNUM,
   PFM.TripNUM,isnull(PSL.SimulatorLog,0) as SimulatorLog,ISNULL(PFM.IsLog,0) AS IsLog,PFM.CrewCalendarNotes  as NoteSec      
 FROM Crew CW   
   INNER JOIN PreflightCrewList PFCL ON PFCL.CrewID=CW.CrewID  
   INNER JOIN PreflightLeg PFL ON PFL.LegID=PFCL.LegID  
   INNER JOIN PreflightMain PFM ON PFM.TripID=PFL.TripID  
   LEFT OUTER JOIN Fleet F ON F.FleetID=PFM.FleetID  
   INNER JOIN Company C ON C.HomebaseID=PFM.HomebaseID  
   LEFT OUTER JOIN Client CL ON CL.ClientID =PFM.ClientID--1  
   INNER JOIN Airport A ON A.AirportID=PFL.DepartICAOID  
   INNER JOIN CrewDutyType CD ON CD.DutyTypeCD=PFL.DutyTYPE   and CD.CustomerID = @CustomerID  and CD.IsDeleted = 0
   INNER JOIN Airport HB ON HB.AirportId = C.HomebaseAirportID -- Home Base Airport ID change 
   LEFT OUTER JOIN PostflightSimulatorLog PSL ON PSL.TripID=PFM.TripID  
 WHERE  CW.CustomerID=@CustomerID AND PFM.RecordType='C'   
   --AND CW.IsDeleted!=1   
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and CD.IsDeleted!=1 and CW.IsDeleted!=1  
   AND (PFL.HomeDepartureDTTM >= @START_DATE AND PFL.HomeDepartureDTTM <=@END_DATE OR (PFL.HomeArrivalDTTM >= @START_DATE AND PFL.HomeArrivalDTTM <=@END_DATE))  
   ORDER BY StartDate
  
END  
ELSE IF UPPER(@IN_STR)='UTC'  
BEGIN  
 SELECT CW.CrewID,CW.CrewCD,PFCL.PreflightCrewListID,PFL.LegID,  
   A.IcaoID,A.AirportID,PFL.DepartureGreenwichDTTM AS StartDate,PFL.ArrivalGreenwichDTTM AS EndDate,     
   PFL.DutyTYPE,CD.DutyTypeCD,CD.DutyTypesDescription,     
   PFM.TripID,PFM.RecordType,PFM.TripDescription as Notes,PFM.HomebaseID,PFM.FleetID,PFM.ClientID,     
   F.TailNum, HB.IcaoID HomebaseCD,CL.ClientCD,PFM.PreviousNUM,
   PFM.TripNUM,isnull(PSL.SimulatorLog,0) as SimulatorLog,ISNULL(PFM.IsLog,0) AS IsLog ,PFM.CrewCalendarNotes  as NoteSec     
 FROM Crew CW   
   INNER JOIN PreflightCrewList PFCL ON PFCL.CrewID=CW.CrewID  
   INNER JOIN PreflightLeg PFL ON PFL.LegID=PFCL.LegID  
   INNER JOIN PreflightMain PFM ON PFM.TripID=PFL.TripID  
   LEFT OUTER JOIN Fleet F ON F.FleetID=PFM.FleetID  
   INNER JOIN Company C ON C.HomebaseID=PFM.HomebaseID  
   LEFT OUTER JOIN Client CL ON CL.ClientID =PFM.ClientID--1  
   INNER JOIN Airport A ON A.AirportID=PFL.DepartICAOID  
   INNER JOIN CrewDutyType CD ON CD.DutyTypeCD=PFL.DutyTYPE   and CD.CustomerID = @CustomerID  and CD.IsDeleted = 0
   INNER JOIN Airport HB ON HB.AirportId = C.HomebaseAirportID -- Home Base Airport ID change 
   LEFT OUTER JOIN PostflightSimulatorLog PSL ON PSL.TripID=PFM.TripID  
 WHERE  CW.CustomerID=@CustomerID AND PFM.RecordType='C'   
   --AND CW.IsDeleted!=1   
   and PFL.IsDeleted!=1 and PFM.IsDeleted!=1 and CD.IsDeleted!=1 and CW.IsDeleted!=1  
   AND (PFL.DepartureGreenwichDTTM >= @START_DATE AND PFL.DepartureGreenwichDTTM <=@END_DATE OR (PFL.ArrivalGreenwichDTTM >= @START_DATE AND PFL.ArrivalGreenwichDTTM <=@END_DATE))  
   ORDER BY StartDate
  
END  
END  
  


GO


