
/****** Object:  StoredProcedure [dbo].[spDeleteCQReportDetail]    Script Date: 04/24/2013 19:00:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCQReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCQReportDetail]
GO


/****** Object:  StoredProcedure [dbo].[spDeleteCQReportDetail]    Script Date: 04/24/2013 19:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteCQReportDetail]
(@CQReportDetailID bigint,
@CustomerID bigint,
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit)
AS BEGIN
	
	SET @IsDeleted = 1
	UPDATE	CQReportDetail
	SET		LastUpdUID=@LastUpdUID,
			LastUpdTS=@LastUpdTS,
			IsDeleted=@IsDeleted
	WHERE	CQReportDetailID=@CQReportDetailID

END
GO


