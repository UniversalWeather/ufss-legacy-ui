/****** Object:  StoredProcedure [dbo].[spLogin]    Script Date: 10/21/2015 17:39:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLogin]
GO


/****** Object:  StoredProcedure [dbo].[spLogin]    Script Date: 10/21/2015 17:39:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


                      
CREATE PROCEDURE [dbo].[spLogin]                          
 @SessionID [uniqueidentifier],                          
 @EmailId varchar(100),                       
 @Password [nvarchar](500),      
 @DefaultInvalidLoginCount int                              
AS                          
BEGIN                          
 -- Variable Declaration                          
 Declare @CustomerId bigint                          
 Declare @UserCount bigint                          
 Declare @UserName varchar(30)      
 SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId                        
 -- Check user existance and get customerid                          
 SELECT @UserCount = Count(UserMaster.UserName), @CustomerId = UserMaster.CustomerID from UserMaster                      
 JOIN UserPassword ON UserPassword.UserName = UserMaster.UserName  
 JOIN Customer ON Customer.CustomerID = UserMaster.CustomerID                    
  where UPPER(UserMaster.UserName) = UPPER(@UserName) AND UserPassword.FPPWD = @Password                          
  AND UserPassword.ActiveStartDT <= GETUTCDATE() AND (UserPassword.ActiveEndDT IS NULL OR UserPassword.ActiveEndDT >= GETUTCDATE()) AND PWDStatus = 1         
  AND UserMaster.IsActive = 1 AND UserMaster.IsDeleted = 0 AND UserMaster.IsUserLock = 0 AND IsCustomerLock = 0                     
  GROUP BY Usermaster.CustomerID                          
 IF(@UserCount > 0)                          
 BEGIN                          
  INSERT INTO FPLoginSession (LoginsessionID,SessionID,LoginStartTime,LoginStatus,LoginEndtime,LoginSessionUID,CustomerID,LastActivityDate) Values                          
   (                          
    (SELECT ISNULL((MAX(LoginsessionID) + 1),1) FROM FPLoginSession),                          
    @SessionID,                          
    GETUTCDATE(),                          
    1,                          
    GETUTCDATE(),                         
    @UserName,                          
    @CustomerId,              
 GETUTCDATE()                          
   )     
   UPDATE UserMaster set InvalidLoginCount = 0 where UserName = @UserName                         
 END          
 ELSE      
 BEGIN      
 -- Check for Invalid Login attempt      
 DECLARE @InvalidLoginCount int      
 SELECT @InvalidLoginCount = ISNULL(InvalidLoginCount,0) FROM UserMaster where UserName = @UserName      
  IF(@InvalidLoginCount > @DefaultInvalidLoginCount)      
  BEGIN      
   DECLARE @LockFlag int  
   SELECT @LockFlag = Count(*) FROM UserMaster where UserName = @UserName and IsUserLock = 1 AND LastUpdTS <= DATEADD(minute, -15, GETUTCDATE())
	   IF(@LockFlag > 0)      
       BEGIN    
	      UPDATE UserMaster set IsUserLock = 0 where UserName = @UserName   
	      set @UserCount = 1;
       END
	   ELSE
	   BEGIN   
	     SELECT @LockFlag = Count(*) FROM UserMaster where UserName = @UserName and IsUserLock = 1 
	      IF(@LockFlag < 1)      
           BEGIN    
	      UPDATE UserMaster set IsUserLock = 1 ,LastUpdTS= GETUTCDATE() where UserName = @UserName 
	      END     
       END
  END      
  ELSE      
  BEGIN      
   UPDATE UserMaster set InvalidLoginCount = (@InvalidLoginCount + 1) where UserName = @UserName      
  END      
 END                      
 SELECT @UserCount AS ReturnValue                          
END   

GO


