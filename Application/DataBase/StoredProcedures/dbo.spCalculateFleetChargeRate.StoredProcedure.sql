/****** Object:  StoredProcedure [dbo].[spCalculateFleetChargeRate]    Script Date: 10/18/2012 14:57:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCalculateFleetChargeRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCalculateFleetChargeRate]
GO

/****** Object:  StoredProcedure [dbo].[spCalculateFleetChargeRate]    Script Date: 10/18/2012 14:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spCalculateFleetChargeRate]
	(@FleetChargeRateID	bigint
	,@CustomerID bigint
	,@FleetID bigint
	,@AircraftID bigint
	,@AircraftCD char(3)
	,@BeginRateDT date
	,@EndRateDT date
	,@ChargeRate numeric(17,2)
	,@ChargeUnit char(1)
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
	,@IsDeleted bit)
AS BEGIN

	-- FETCH AircraftCD from Fleet by FleetID
	SELECT @AircraftCD = AircraftCD FROM Fleet WHERE FleetID=@FleetID

	-- FETCHING Max EndDate from FleetChargeRate by AircraftCD
	DECLARE @LastEndDate DATETIME
	SELECT @LastEndDate = MAX(EndRateDT) FROM FleetChargeRate WHERE FleetID=@FleetID AND IsDeleted=0--AircraftCD = @AircraftCD

	-- CHECK MaxEndDate Less than EndDate
	DECLARE @UpdtRecBet BIT

	IF (CONVERT(datetime,@LastEndDate) > CONVERT(datetime,@EndRateDT))
		BEGIN
			SET @UpdtRecBet = 1
		END
	ELSE
		BEGIN
			SET @UpdtRecBet = 0
		END

	-- Temp table for storing PreflightLeg record
	DECLARE @PreFltTripLegs TABLE
	(
		LegID BIGINT,
		MainTripID BIGINT
	)

	-- FILTERING PreflightLeg Table record that matches the conditions
	IF (@UpdtRecBet = 1)
		BEGIN

			INSERT INTO @PreFltTripLegs (LegID, MainTripID)

			SELECT PreflightLeg.LegID, PreflightLeg.TripID
			FROM PreFlightLeg
			LEFT OUTER JOIN Preflightmain on Preflightmain.TripID = PreFlightLeg.TripID
							AND ISNULL(Preflightmain.IsDeleted,0) = 0
			LEFT OUTER JOIN Fleet on Fleet.FleetID = Preflightmain.FleetID
			LEFT OUTER JOIN FleetChargerate on Fleet.FleetID = FleetChargerate.FleetID
			WHERE FleetChargerate.FleetChargeRateID = @FleetChargeRateID AND
			((PreFlightLeg.DepartureDTTMLocal BETWEEN FleetChargerate.BeginRateDT AND FleetChargerate.EndRateDT)
			OR (PreFlightLeg.ArrivalDTTMLocal BETWEEN FleetChargerate.BeginRateDT AND FleetChargerate.EndRateDT))
			ORDER BY PreflightLeg.LegID

		END
	ELSE
		BEGIN

			INSERT INTO @PreFltTripLegs (LegID, MainTripID)

			SELECT PreflightLeg.LegID, PreflightLeg.TripID
			FROM PreFlightLeg
			LEFT OUTER JOIN Preflightmain on Preflightmain.TripID = PreFlightLeg.TripID
							AND ISNULL(Preflightmain.IsDeleted,0) = 0
			LEFT OUTER JOIN Fleet on Fleet.FleetID = Preflightmain.FleetID
			LEFT OUTER JOIN FleetChargerate on Fleet.FleetID = FleetChargerate.FleetID
			WHERE FleetChargerate.FleetChargeRateID = @FleetChargeRateID AND
			PreFlightLeg.DepartureDTTMLocal >= FleetChargerate.BeginRateDT
			ORDER BY PreflightLeg.LegID

		END

	-- Temp variables for calculating preflight charge rates
	DECLARE @CurrentLegID BIGINT
	DECLARE @CurrentTripNUM BIGINT
	DECLARE @Distance FLOAT
	DECLARE @ElapseTM FLOAT
	DECLARE @FlightCost FLOAT
	DECLARE @MainFlightCost FLOAT
	DECLARE @PreflightMainFlightCost FLOAT
	DECLARE @PreflightLegFlightCost FLOAT

	WHILE (SELECT COUNT(*) FROM @PreFltTripLegs) > 0
	BEGIN
		SELECT  TOP 1 @CurrentLegID = LegID, @CurrentTripNUM = MainTripID FROM @PreFltTripLegs

		IF EXISTS (SELECT * FROM PreflightLeg WHERE LegID=@CurrentLegID AND TripID=@CurrentTripNUM)
			BEGIN
				SELECT @Distance = Distance, @ElapseTM = ElapseTM, @PreflightLegFlightCost=FlightCost FROM PreflightLeg WHERE LegID=@CurrentLegID

				SELECT @PreflightMainFlightCost=FlightCost FROM PreflightMain WHERE TripID=@CurrentTripNUM

				IF (@ChargeUnit = 'N')
					BEGIN
						SET @FlightCost = @Distance * @ChargeRate
					END
				ELSE IF (@ChargeUnit = 'S')
					BEGIN
						--SET @FlightCost = @Distance * @ChargeRate * 1.15077900
						SET @FlightCost = FLOOR(@Distance * 1.15077900) * @ChargeRate
					END
				ELSE IF (@ChargeUnit = 'H')
					BEGIN
						SET @FlightCost = @ElapseTM * @ChargeRate
					END
				ELSE IF (@ChargeUnit = 'K')  
					BEGIN  
						--SET @FlightCost = @Distance * @ChargeRate * 1.60934 
						SET @FlightCost = FLOOR(@Distance * 1.852) * @ChargeRate 
					END 					
				ELSE
					BEGIN
						SET @FlightCost = 0
					END

				SET @MainFlightCost = ISNULL((@PreflightMainFlightCost - @PreflightLegFlightCost + @FlightCost),0)

				UPDATE PreflightMain SET FlightCost = @MainFlightCost WHERE TripID=@CurrentTripNUM

				UPDATE PreflightLeg SET FlightCost = @FlightCost WHERE LegID=@CurrentLegID

			END

		DELETE FROM @PreFltTripLegs WHERE ISNULL(MainTripID,0)=ISNULL(@CurrentTripNUM,0) AND ISNULL(LegID,0)=ISNULL(@CurrentLegID,0)
	END

	-- Temp table for storing PostflightLeg record
	DECLARE @PostFltTripLegs  TABLE
	(
		LegID BIGINT,
		POLogID BIGINT
	)

	-- FILTERING PreflightLeg Table record that matches the conditions
	IF (@UpdtRecBet = 1)
		BEGIN

			INSERT INTO @PostFltTripLegs (LegID, POLogID)

			SELECT DISTINCT PostflightLeg.POLegID, PostflightLeg.POLogID
			FROM PostFlightLeg
			LEFT OUTER JOIN PostflightMain on PostflightMain.POLogID = PostFlightLeg.POLogID
							AND ISNULL(PostflightMain.IsDeleted,0) = 0
			LEFT OUTER JOIN Fleet on Fleet.FleetID = PostflightMain.FleetID
			LEFT OUTER JOIN FleetChargerate on Fleet.FleetID = FleetChargerate.FleetID
			WHERE FleetChargerate.FleetChargeRateID = @FleetChargeRateID AND
					(PostFlightLeg.ScheduledTM BETWEEN @BeginRateDT AND @EndRateDT)
			ORDER BY PostflightLeg.POLegID

		END
	ELSE
		BEGIN

			INSERT INTO @PostFltTripLegs (LegID, POLogID)

			SELECT DISTINCT PostflightLeg.POLegID, PostflightLeg.POLogID
			FROM PostFlightLeg
			LEFT OUTER JOIN PostflightMain on PostflightMain.POLogID = PostFlightLeg.POLogID
							AND ISNULL(PostflightMain.IsDeleted,0) = 0
			LEFT OUTER JOIN Fleet on Fleet.FleetID = PostflightMain.FleetID
			LEFT OUTER JOIN FleetChargerate on Fleet.FleetID = FleetChargerate.FleetID
			WHERE FleetChargerate.FleetChargeRateID = @FleetChargeRateID AND
					PostFlightLeg.ScheduledTM >= @BeginRateDT
			ORDER BY PostflightLeg.POLegID

		END

	-- Temp variables for calculating postflight charge rates
	DECLARE @BlockHours FLOAT
	DECLARE @FlightHours FLOAT
	DECLARE @AircraftBasis BIT
	DECLARE @ExpenseAMT FLOAT
	DECLARE @POLogID BIGINT
	SET @FlightCost = 0

	SELECT @AircraftBasis = AircraftBasis FROM Company
		LEFT OUTER JOIN Fleet ON Fleet.HomebaseID = Company.HomebaseID
	WHERE Fleet.FleetID = @FleetID

	IF (@AircraftBasis = 1)
	BEGIN
		SET @AircraftBasis = 1
	END
	ELSE
	BEGIN
		SET @AircraftBasis = 2
	END

	WHILE (SELECT COUNT(*) FROM @PostFltTripLegs) > 0
	BEGIN
		SELECT TOP 1 @CurrentLegID = LegID FROM @PostFltTripLegs

		IF EXISTS (SELECT * FROM PostflightLeg WHERE POLegID=@CurrentLegID)
			BEGIN
				SELECT @POLogID=POLogID, @Distance = Distance, @BlockHours = BlockHours, @FlightHours = FlightHours FROM PostflightLeg WHERE POLegID=@CurrentLegID

				IF (@POLogID = 0)
					BEGIN
						SET @FlightCost = 0
					END
				ELSE
					IF (@ChargeUnit = 'N')
						BEGIN
							SET @FlightCost = @Distance * @ChargeRate
						END
					ELSE IF (@ChargeUnit = 'S')
						BEGIN
							--SET @FlightCost = @Distance * @ChargeRate * 1.15077900
							SET @FlightCost = FLOOR(@Distance * 1.15077900) * @ChargeRate
						END
					ELSE IF (@ChargeUnit = 'H')
						BEGIN
							IF (@AircraftBasis = 1)
								BEGIN
									SET @FlightCost = @BlockHours * @ChargeRate
								END
							ELSE
								BEGIN
									SET @FlightCost = @FlightHours * @ChargeRate
								END
						END
					 ELSE IF (@ChargeUnit = 'K')  
						BEGIN  
							--SET @FlightCost = @Distance * @ChargeRate * 1.60934 
							SET @FlightCost = FLOOR(@Distance * 1.852) * @ChargeRate
						END 
					ELSE
						BEGIN
							SET @FlightCost = 0
						END

					SELECT @ExpenseAMT = SUM(ExpenseAMT) FROM PostflightExpense WHERE POLegID = @CurrentLegID AND IsBilling = 1

					SET @FlightCost = ISNULL(@FlightCost,0) + ISNULL(@ExpenseAMT,0)

					UPDATE PostflightLeg SET FlightCost = @FlightCost WHERE POLegID = @CurrentLegID

			END

		DELETE FROM @PostFltTripLegs WHERE LegID = @CurrentLegID
	END

END
GO


