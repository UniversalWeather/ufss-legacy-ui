

/****** Object:  StoredProcedure [dbo].[Usp_getcrewpaxcodegeneration]    Script Date: 02/06/2014 15:43:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_getcrewpaxcodegeneration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Usp_getcrewpaxcodegeneration]
GO



/****** Object:  StoredProcedure [dbo].[Usp_getcrewpaxcodegeneration]    Script Date: 02/06/2014 15:43:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_getcrewpaxcodegeneration] (@CustomerID  BIGINT,       
                                                      @CODE        VARCHAR(10),       
                                                      @TYPE        VARCHAR(20),       
                                                      @ReturnValue CHAR(5) output)       
--EXECUTE dbo.[Usp_getcrewpaxcodegeneration] 10001, 'SME', 'Passenger', 'james' --for New logic      
AS BEGIN       
      
DECLARE @SearchCode VARCHAR(6)
DECLARE @GenerationCode VARCHAR(6)      
DECLARE @SEQGenerationCode INT      
DECLARE @xcount INT  
DECLARE @LoopCount INT            
         
SET @SearchCode = @CODE      
SET @SEQGenerationCode = 0      
SET @xcount=0 
SET @LoopCount = 1     
       
 WHILE (@LoopCount <= 99)      
	BEGIN    	        
			IF @TYPE = 'Passenger'  
				 BEGIN
				 
                   Set @xcount = (SELECT COUNT(*) FROM Passenger WHERE CustomerID = @CustomerID AND PassengerRequestorCD = @SearchCode and IsDeleted = 0)

				   IF (@xcount>0)
				   BEGIN 				   				 
						  SET @GenerationCode =  SUBSTRING(@SearchCode, 1, len(@SearchCode)-1) 
						
						  		    IF (@LoopCount > 10)      
									  BEGIN    									 
										 SET @GenerationCode= SUBSTRING(@GenerationCode, 1,len(@GenerationCode) - 1);  
									  END 
										   
							SET @SEQGenerationCode = @SEQGenerationCode + 1  				
				            SET @SearchCode = @GenerationCode + convert(varchar,@SEQGenerationCode)      		
							SET @LoopCount = @LoopCount + 1      
							 CONTINUE     

					  
					  END
				   ELSE  
					   BEGIN  					
							SET @ReturnValue =UPPER(@SearchCode)  						
							BREAK  
					   END  
			     END

			IF @TYPE = 'Crew'      
				BEGIN
				    Set @xcount = (SELECT COUNT(*) FROM Crew WHERE CustomerID = @CustomerID AND CrewCD = @SearchCode and IsDeleted = 0)   
					  IF (@xcount>0)   
					  BEGIN 
					     SET @GenerationCode =  SUBSTRING(@SearchCode, 1, len(@SearchCode)-1) 
						
						  		    IF (@LoopCount > 10)      
									  BEGIN    									 
										 SET @GenerationCode= SUBSTRING(@GenerationCode, 1,len(@GenerationCode) - 1);  
									  END 
										   
							SET @SEQGenerationCode = @SEQGenerationCode + 1  				
				            SET @SearchCode = @GenerationCode + convert(varchar,@SEQGenerationCode)      		
							SET @LoopCount = @LoopCount + 1      
							 CONTINUE    
					  END
				    ELSE  
					   BEGIN  
							SET @ReturnValue =UPPER(@SearchCode)  
							BREAK  
					   END 
			   END
  
	 END  
END 

GO


