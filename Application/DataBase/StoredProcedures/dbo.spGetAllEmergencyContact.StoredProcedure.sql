/****** Object:  StoredProcedure [dbo].[spGetAllEmergencyContact]    Script Date: 09/13/2012 12:32:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllEmergencyContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllEmergencyContact]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllEmergencyContact]    Script Date: 09/13/2012 12:32:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllEmergencyContact](@CustomerID  BIGINT)      
AS      
SET NOCOUNT ON      
      
BEGIN      
SELECT  [EmergencyContactID]    
           ,[EmergencyContactCD]    
           ,[CustomerID]    
           ,[LastName]    
           ,[FirstName]    
           ,[MiddleName]    
           ,[Addr1]    
           ,[Addr2]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[CountryID]    
           ,[PhoneNum]    
           ,[FaxNum]    
           ,[CellPhoneNum]    
           ,[EmailAddress]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]
           ,[BusinessPhone]
		   ,[BusinessFax]
		   ,[CellPhoneNum2]
		   ,[OtherPhone]
		   ,[PersonalEmail]
		   ,[OtherEmail]
		   ,[Addr3]    
		   ,IsInactive
  FROM  EmergencyContact     
  WHERE CustomerID =@CustomerID      
 AND IsDeleted='false'  
 order by  EmergencyContactCD  
END

GO


