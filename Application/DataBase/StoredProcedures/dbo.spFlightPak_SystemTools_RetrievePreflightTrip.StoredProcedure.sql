/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_RetrievePreflightTrip]    Script Date: 01/17/2013 19:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_RetrievePreflightTrip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_RetrievePreflightTrip]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_RetrievePreflightTrip]    Script Date: 01/17/2013 19:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Prabhu Devarajan
-- Create date: 08/01/2013
-- Description:	TO Retrieve Preflight Trip
-- =============================================
CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_RetrievePreflightTrip](@TripID BIGINT, @CustomerID BIGINT, @UserName VARCHAR(30))
	
AS
BEGIN
	SET NOCOUNT ON;
	Update PreflightMain set IsDeleted = 0 , LastUpdUID = @UserName
		, LastUpdTS = GETUTCDATE()
	where TripID = @TripID and CustomerID = @CustomerID
END


GO


