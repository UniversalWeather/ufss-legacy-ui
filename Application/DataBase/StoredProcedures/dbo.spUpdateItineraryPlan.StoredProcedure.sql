
GO
/****** Object:  StoredProcedure [dbo].[spUpdateItineraryPlan]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateItineraryPlan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateItineraryPlan]
GO

CREATE PROCEDURE spUpdateItineraryPlan
(@ItineraryPlanID	BIGINT
,@CustomerID	BIGINT
,@IntinearyNUM	INT
,@ItineraryPlanQuarter	INT
,@ItineraryPlanQuarterYear	VARCHAR(4)
,@AircraftID	BIGINT
,@FleetID	BIGINT
,@ItineraryPlanDescription	VARCHAR(40)
,@ClientID	BIGINT
,@HomebaseID	BIGINT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE ItineraryPlan
    SET CustomerID  = @CustomerID
       ,IntinearyNUM  = @IntinearyNUM
       ,ItineraryPlanQuarter  = @ItineraryPlanQuarter
       ,ItineraryPlanQuarterYear  = @ItineraryPlanQuarterYear
       ,AircraftID  = @AircraftID
       ,FleetID  = @FleetID
       ,ItineraryPlanDescription  = @ItineraryPlanDescription
       ,ClientID  = @ClientID
       ,HomebaseID  = @HomebaseID
       ,LastUpdUID  = @LastUpdUID
       ,LastUpdTS  = @LastUpdTS
       ,IsDeleted = @IsDeleted
    WHERE ItineraryPlanID = @ItineraryPlanID

END