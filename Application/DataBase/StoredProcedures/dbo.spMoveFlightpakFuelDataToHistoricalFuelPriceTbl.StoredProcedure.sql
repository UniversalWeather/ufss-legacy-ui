IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveFlightpakFuelDataToHistoricalFuelPriceTbl]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spMoveFlightpakFuelDataToHistoricalFuelPriceTbl]
GO
/****** Object:  StoredProcedure [dbo].[spMoveFlightpakFuelDataToHistoricalFuelPriceTbl]    Script Date: 10/1/2015 8:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajeet Singh
-- Create date: 2015-10-01
-- Description:	Move all the Flightpak Fuel data to Historical Fuel Price table By VendorId
-- =============================================
CREATE PROCEDURE [dbo].[spMoveFlightpakFuelDataToHistoricalFuelPriceTbl]
	-- Add the parameters for the stored procedure here
	@FuelVendorID BIGINT, 	
	@CustomerID BIGINT,
	@LastUpdUID VARCHAR(30),  
	@LastUpdTS DATETIME
AS
BEGIN
	INSERT INTO [dbo].[HistoricalFuelPrice] ([CustomerID],[DepartureICAOID],[FBOName],[GallonFrom],[GallonTO],[EffectiveDT],[UnitPrice],[FuelVendorID],[LastUpdUID],[LastUpdTS])
	SELECT [CustomerID],[DepartureICAOID],[FBOName],[GallonFrom],[GallonTO],[EffectiveDT],[UnitPrice],[FuelVendorID],@LastUpdUID,@LastUpdTS
	FROM [dbo].[FlightpakFuel]
	WHERE [dbo].[FlightpakFuel].[FuelVendorID]=@FuelVendorID and [dbo].[FlightpakFuel].[CustomerID]=@CustomerID

	DELETE FROM [dbo].[FlightpakFuel]
	WHERE [dbo].[FlightpakFuel].[FuelVendorID]=@FuelVendorID and [dbo].[FlightpakFuel].[CustomerID]=@CustomerID
END
GO