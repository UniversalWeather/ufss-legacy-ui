

/****** Object:  StoredProcedure [dbo].[spGetAllVendor]    Script Date: 03/15/2013 18:54:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllVendor]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllVendor]    Script Date: 03/15/2013 18:54:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE Procedure [dbo].[spGetAllVendor](@CustomerID BIGINT,@VendorType char(1))        
AS        
-- =============================================        
-- Author: Karthikeyan.S        
-- Create date: 10/05/2012        
-- Altered by Mathes 03-07-12      
-- Description: Get All Vendor  Information        
-- =============================================        
SET NOCOUNT ON        
BEGIN        
SELECT        
       Vendor.[VendorID]      
      ,Vendor.[CustomerID]        
      ,CASE WHEN Vendor.[VendorCD] IS NULL THEN '' ELSE LTRIM(RTRIM(Vendor.[VendorCD])) END AS VendorCD
      ,Vendor.[Name]        
      ,Vendor.[VendorType]        
      ,Vendor.[IsApplicationFiled]        
      ,Vendor.[IsApproved]        
      ,Vendor.[IsDrugTest]        
      ,Vendor.[IsInsuranceCERT]        
      ,Vendor.[IsFAR135Approved]        
      ,Vendor.[IsFAR135CERT]        
      ,Vendor.[AdditionalInsurance]        
      ,Vendor.[Contact]        
      ,Vendor.[VendorContactName]        
      ,Vendor.[BillingName]        
      ,Vendor.[BillingAddr1]        
      ,Vendor.[BillingAddr2]        
      ,Vendor.[BillingCity]        
      ,Vendor.[BillingState]        
      ,Vendor.[BillingZip]        
      ,Vendor.[CountryID]         
      ,Vendor.[MetroID]         
      ,Vendor.[BillingPhoneNum]        
      ,Vendor.[BillingFaxNum]        
      ,Vendor.[Notes]        
      ,Vendor.[Credit]        
      ,Vendor.[DiscountPercentage]        
      ,Vendor.[TaxID]        
      ,Vendor.[Terms]        
      ,Vendor.[IsInActive]        
      ,Vendor.[HomebaseID]         
      ,Vendor.[LatitudeDegree]        
      ,Vendor.[LatitudeMinutes]        
      ,Vendor.[LatitudeNorthSouth]        
      ,Vendor.[LongitudeDegree]        
      ,Vendor.[LongitudeMinutes]        
      ,Vendor.[LongitudeEastWest]        
      ,Vendor.[AirportID]         
      ,Vendor.[LastUpdUID]        
      ,Vendor.[LastUpdTS]        
      ,Vendor.[IsTaxExempt]        
      ,Vendor.[DateAddedDT]        
      ,Vendor.[EmailID]        
      ,Vendor.[WebAddress]        
      ,Vendor.[IsDeleted]                
  ,Vendor.TollFreePhone    
  ,Vendor.BusinessEmail    
  ,Vendor.Website    
  ,Vendor.BillingAddr3    
  ,Vendor.BusinessPhone    
  ,Vendor.CellPhoneNum    
  ,Vendor.HomeFax    
  ,Vendor.CellPhoneNum2    
  ,Vendor.OtherPhone    
  ,Vendor.MCBusinessEmail    
  ,Vendor.PersonalEmail    
  ,Vendor.OtherEmail   
  ,Vendor.WebsiteAddress
  ,Vendor.CertificateNumber
  ,Vendor.AuditingCompany 
  ,Vendor.SITA
      ,Nationality.CountryCD as NationalityCD      
      ,Metro.MetroCD as MetroCD      
      ,HomeBase.IcaoID as HomeBaseCD      
      ,Airport.IcaoID as ClosestICAO  
      ,VendorContact.FirstName as FirstName  
      ,VendorContact.LastName as LastName  
      
          
      FROM Vendor as Vendor      
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Vendor.CountryID      
  LEFT OUTER JOIN Metro as Metro on Metro.MetroID = Vendor.MetroID      
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Vendor.HomebaseID      
  LEFT OUTER JOIN Airport as Airport on Airport.AirportID = Vendor.AirportID   
    
  LEFT OUTER JOIN VendorContact as VendorContact on (VendorContact.VendorID = Vendor.VendorID and VendorContact.IsChoice = 1)  
        
WHERE Vendor.CustomerID=@CustomerID AND Vendor.VendorType=@VendorType AND ISNULL(Vendor.IsDeleted,0) = 0   
order by VendorCD      
END 
GO


