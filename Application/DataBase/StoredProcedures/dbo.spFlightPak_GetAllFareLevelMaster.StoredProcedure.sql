IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllFareLevelMaster]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllFareLevelMaster]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllFareLevelMaster]    Script Date: 08/24/2012 10:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_GetAllFareLevelMaster]
(@CustomerID bigint)
AS
-- =============================================
-- Author:Karthikeyan.S
-- Create date: 14/5/2012
-- Description: Get the Fare Level information
-- =============================================
set nocount on
if len(@CustomerID) >0
begin 
SELECT [FareLevelID]
      ,[FareLevelCD]
      ,[CustomerID]
      ,[StartDT]
      ,[EndDT]
      ,[Rate1]
      ,[Rate2]
      ,[Rate3]
      ,[TerminalCHG]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive
  FROM [FareLevel] WHERE CustomerID=@CustomerID  and IsDeleted = 'false'
ORDER BY [StartDT] desc
END
GO
