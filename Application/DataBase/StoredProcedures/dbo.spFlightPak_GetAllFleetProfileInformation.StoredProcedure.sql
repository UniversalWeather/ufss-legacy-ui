


GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllFleetProfileInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllFleetProfileInformation]
GO
 
Create Procedure [dbo].[spFlightPak_GetAllFleetProfileInformation]
 (@CustomerID BIGINT)  
as  
-- =============================================  
-- Author:D.Mullai  
-- Create date: 12/5/2012  
-- Modified by Mathes on 05-07-12
-- Description: Get the FleetProfileInformation  
-- =============================================  
set nocount on  
if len(@CustomerID) >0  
begin   
  SELECT    FleetProfileInformationID  
           ,CustomerID  
           ,FleetInfoCD  
           ,FleetProfileAddInfDescription  
           ,ClientID   
           ,LastUpdUID  
           ,LastUpdTS  
           ,IsDeleted
           ,IsInactive  
  FROM  [FleetProfileInformation] WHERE CustomerID=@CustomerID AND IsNull(FleetProfileInformation.IsDeleted,0)=0      
  Order By  FleetInfoCD  
end
GO
