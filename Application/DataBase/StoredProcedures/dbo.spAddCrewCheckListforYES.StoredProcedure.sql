

/****** Object:  StoredProcedure [dbo].[spAddCrewCheckListforYES]    Script Date: 10/25/2012 19:54:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCrewCheckListforYES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCrewCheckListforYES]
GO



/****** Object:  StoredProcedure [dbo].[spAddCrewCheckListforYES]    Script Date: 10/25/2012 19:54:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--select distinct crewid from crew where customerid = 10001 --199
--select checklistid from crewchecklistdetail where customerid = 10001 --2184 + 197 = 2381
--select distinct crewid from crewchecklistdetail where customerid = 10001 -- 197

--spAddCrewCheckListforYES 10001,'AND','AND',NULL,'Fernando_1',NULL,0,0,0

CREATE procedure [dbo].[spAddCrewCheckListforYES]
    (
     @CustomerID BIGINT,
     @CrewCheckCD char(3),
     @CrewChecklistDescription  varchar(25),
     @ClientID BIGINT,
     @LastUpdUID varchar(30),
     @LastUpdTS datetime,
     @IsScheduleCheck bit,
     @IsCrewCurrencyPlanner bit,
     @IsDeleted bit,
     @IsInActive bit
    )
   
AS    
 BEGIN 
 SET NoCOUNT ON
 DECLARE  @CrewID BIGINT 
 DECLARE @CrewCheckID BIGINT
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CrewCheckID OUTPUT
  set @LastUpdTS = GETUTCDATE()
INSERT INTO [CrewCheckList] 
           ([CrewCheckID]
           ,[CustomerID]  
           ,[CrewCheckCD]  
           ,[CrewChecklistDescription]  
           ,[ClientID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsScheduleCheck]   
           ,[IsCrewCurrencyPlanner]  
           ,[IsDeleted]
           ,[IsInActive])  
         
VALUES  
  
          ( @CrewCheckID
           ,@CustomerID   
           ,@CrewCheckCD  
           ,@CrewChecklistDescription  
           ,@ClientID  
           ,@LastUpdUID  
           ,@LastUpdTS  
           ,@IsScheduleCheck  
           ,@IsCrewCurrencyPlanner   
           ,@IsDeleted
           ,@IsInActive)  
  DECLARE curTailInfo CURSOR FOR SELECT DISTINCT CREWID FROM CREW WHERE CUSTOMERID=@CUSTOMERID        
  
        
 OPEN curTailInfo;            
 -- PRINT @@CURSOR_ROWS            
  FETCH NEXT FROM curTailInfo INTO @CREWID;   
  WHILE @@FETCH_STATUS = 0            
  BEGIN  
  BEGIN          
  --INSERT INTO()
 EXEC spFlightPak_AddCrewCheckListDate NULL,@CrewCheckCD,NULL,@CustomerID,@CrewID,NULL,NULL,NULL,0,0,0,NULL,0,@LastUpdUID,@LastUpdTS,0,0,0,0,0,NULL,@IsInActive,0,0,0,0,0,0,NULL,NULL,0,0,NULL,0,0,0,0
 
  END  
   FETCH NEXT FROM curTailInfo INTO  @CREWID
              
 END             
 CLOSE curTailInfo;            
 DEALLOCATE curTailInfo; 
END


GO


