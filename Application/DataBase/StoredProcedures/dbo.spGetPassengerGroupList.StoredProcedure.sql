
GO
/****** Object:  StoredProcedure [dbo].[spGetPassengerGroupList]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassengerGroupList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassengerGroupList]
GO

CREATE Procedure [dbo].[spGetPassengerGroupList](@CustomerID BIGINT)    
AS    
-- =============================================    
-- Author: MohanRaja.C    
-- Create date: 10/4/2012    
-- Description: Get All Passenger Group Informations    
-- =============================================    
SET NOCOUNT ON    
    
SELECT PassengerGroup.PassengerGroupID    
      ,PassengerGroup.CustomerID    
      ,PassengerGroup.HomebaseID    
      ,PassengerGroup.PassengerGroupCD    
      ,PassengerGroup.PassengerGroupName    
      ,PassengerGroup.LastUpdUID    
      ,PassengerGroup.LastUpdTS    
      ,PassengerGroup.IsDeleted
      ,PassengerGroup.IsInactive    
      --,Company.HomeBaseCD    
      ,Company.BaseDescription    
      ,Airport.ICAOID AS HomebaseCD        
  FROM PassengerGroup    
  LEFT OUTER JOIN Company on Company.HomeBaseID = PassengerGroup.HomeBaseID    
        LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID        
  WHERE     
  PassengerGroup.CustomerID = @CustomerID AND     
  ISNULL(PassengerGroup.IsDeleted,0) = 0 
  Order by PassengerGroup.PassengerGroupCD ASC