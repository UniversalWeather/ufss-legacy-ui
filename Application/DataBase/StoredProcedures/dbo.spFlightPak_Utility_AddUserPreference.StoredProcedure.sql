
/****** Object:  StoredProcedure [dbo].[spFlightPak_Utility_AddUserPreference]    Script Date: 02/18/2013 16:21:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Utility_AddUserPreference]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Utility_AddUserPreference]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Utility_AddUserPreference]    Script Date: 02/18/2013 16:21:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*

exec spFlightPak_Utility_AddUserPreference 'carrie_3', 10003, 100036

*/

CREATE PROCEDURE [dbo].[spFlightPak_Utility_AddUserPreference]
(
	@UserName VARCHAR(30),
	@CustomerID BIGINT,
	@CategoryName VARCHAR(100) = NULL,
	@SubCategoryName VARCHAR(100) = NULL,
	@KeyName VARCHAR(50),
	@KeyValue  VARCHAR(100)
)  
AS          
SET NOCOUNT ON;          
	
	DECLARE @IsSuccess BIT
	SET @IsSuccess = 0
	
BEGIN
	DECLARE @PreferenceID BIGINT
	SELECT @PreferenceID = isnull(MAX(UserPreferenceID),0) + 1 FROM UserPreference
	
	set @PreferenceID = isnull(@PreferenceID,1)

	BEGIN TRY
		BEGIN TRANSACTION		
		INSERT INTO UserPreference (UserPreferenceID
			,CustomerID
			,UserName
			,CategoryName
			,SubCategoryName
			,KeyName
			,KeyValue
			,LastUpdTS)
		SELECT @PreferenceID
			,@CustomerID
			,RTRIM(@UserName)
			,@CategoryName
			,@SubCategoryName
			,@KeyName
			,@KeyValue
			,GETUTCDATE()
		
		COMMIT TRANSACTION
		SET @IsSuccess = 1
		
	END TRY
	BEGIN CATCH
		SET @IsSuccess = 0
		ROLLBACK TRANSACTION
	END CATCH
	
	SELECT  @IsSuccess as 'RESULT'		
END 
  

GO


