

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllFlightLog]    Script Date: 11/12/2013 16:18:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllFlightLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllFlightLog]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllFlightLog]    Script Date: 07/15/2015 14:40:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spFlightPak_GetAllFlightLog]      
(@CustomerID  BIGINT        
,@HomebaseID BIGINT)          
AS BEGIN          
SET NOCOUNT ON;          
 DECLARE @CrewLogCustomLBLLong1	varchar(50)
DECLARE @CrewLogCustomLBLLong2	varchar(50)
DECLARE @SpecificationLong3	varchar(50)
DECLARE @SpecificationLong4	varchar(50)        
 IF  NOT EXISTS (SELECT CustomerID FROM  TSFlightLog         
     WHERE CustomerID =@CustomerID AND Category= 'FLIGHT LOG' AND HomebaseID= @HomebaseID AND ISNULL(IsDeleted,0)=0)        
 BEGIN          
  DECLARE @TSFlightLogID BIGINT        
  DECLARE @TSDefinitionLogID BIGINT        
          
  SELECT TSDefinitionLogID as TSDefinitionLogID INTO #TempTSDefinitionLog        
  FROM TSDefinitionLog         
  WHERE --CustomerID  = @CustomerID AND         
    ISNULL(IsDeleted,0) = 0 AND Category= 'FLIGHT LOG'        
    --AND HomebaseID= @HomebaseID        
        
  DECLARE @CustomeLabel BIGINT        
  SET @CustomeLabel = 1        
  WHILE (@CustomeLabel <= 2)        
  BEGIN        
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @TSFlightLogID OUTPUT        
   INSERT INTO [TSFlightLog]        
      ([TSFlightLogID],[CustomerID],[PrimeKey],[HomebaseID],[OriginalDescription]        
      ,[CustomDescription],[SequenceOrder],[IsPrint],[Category]        
      ,[LastUpdUID],[LastUpdTS],[IsDeleted],[IsInActive])        
     VALUES        
      (@TSFlightLogID,@CustomerID,0,@HomebaseID,'CREW LOG CUSTOM LABEL ' + CAST(@CustomeLabel AS VARCHAR),        
      'CREW LOG CUSTOM LABEL ' + CAST(@CustomeLabel AS VARCHAR),@CustomeLabel,1,'FLIGHT LOG',NULL,NULL,0,0)        
   SET @CustomeLabel = @CustomeLabel + 1        
  END        
  DECLARE @SequenceNo int
  
  SELECT @SequenceNo = Count(*) from TSFlightLog WHERE CustomerID = @CustomerID and HomebaseID = @HomebaseID
  set @SequenceNo = ISNULL(@SequenceNo,0)
  
  WHILE (SELECT count(*) FROM #TempTSDefinitionLog) > 0        
  BEGIN        
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @TSFlightLogID OUTPUT        
           
   SELECT TOP 1 @TSDefinitionLogID = TSDefinitionLogID FROM #TempTSDefinitionLog         
   ORDER BY TSDefinitionLogID ASC        
           
   INSERT INTO [TSFlightLog]        
      ([TSFlightLogID],[CustomerID],[PrimeKey],[HomebaseID],[OriginalDescription]        
      ,[CustomDescription],[SequenceOrder],[IsPrint],[Category]        
      ,[LastUpdUID],[LastUpdTS],[IsDeleted],[IsInActive])        
           
   SELECT  @TSFlightLogID,@CustomerID,[PrimeKey],@HomebaseID,[OriginalDescription]        
      ,[CustomDescription],SequenceOrder + @SequenceNo,[IsPrint],[Category]        
      ,[LastUpdUID],[LastUpdTS],[IsDeleted],[IsInActive]        
   FROM TSDefinitionLog  WHERE --CustomerID  = @CustomerID AND         
         ISNULL(IsDeleted,0) = 0        
         AND Category= 'FLIGHT LOG' --AND HomebaseID= @HomebaseID        
         AND TSDefinitionLogID=@TSDefinitionLogID        
                 
   DELETE FROM #TempTSDefinitionLog WHERE TSDefinitionLogID=@TSDefinitionLogID        
           
  END        
          
  DROP TABLE #TempTSDefinitionLog        
          
SELECT @CrewLogCustomLBLLong1=CrewLogCustomLBLLong1 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@CrewLogCustomLBLLong1,''))!=0
BEGIN
print 1
UPDATE TSFlightLog SET CustomDescription = @CrewLogCustomLBLLong1  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 1','Crew Log Custom Label1','CrewLog Custom label 1')
END

SELECT @CrewLogCustomLBLLong2=CrewLogCustomLBLLong2 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@CrewLogCustomLBLLong2,''))!=0
BEGIN  	
UPDATE TSFlightLog SET CustomDescription = @CrewLogCustomLBLLong2  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 2','Crew Log Custom Label2','CrewLog Custom label 2')
END

SELECT @SpecificationLong3=SpecificationLong3 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@SpecificationLong3,''))!=0
BEGIN  
UPDATE TSFlightLog SET CustomDescription = @SpecificationLong3  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 3','Crew Log Custom Label3','CrewLog Custom label 3')
END

SELECT @SpecificationLong4=SpecificationLong4 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@SpecificationLong4,''))!=0
BEGIN  
UPDATE TSFlightLog SET CustomDescription = @SpecificationLong4  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 4','Crew Log Custom Label4','CrewLog Custom label 4')  
END

    
 END 
 ELSE
 BEGIN
 

SELECT @CrewLogCustomLBLLong1=CrewLogCustomLBLLong1 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@CrewLogCustomLBLLong1,''))!=0
BEGIN
print 2
DECLARE @CrewLogCustomLabel1 varchar(50)
SELECT @CrewLogCustomLabel1=CustomDescription From TSFlightLog  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 1','Crew Log Custom Label1','CrewLog Custom label 1')
	AND LEN(ISNULL(CustomDescription,''))!=0
UPDATE Company SET CrewLogCustomLBLLong1 = @CrewLogCustomLabel1  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
END

SELECT @CrewLogCustomLBLLong2=CrewLogCustomLBLLong2 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@CrewLogCustomLBLLong2,''))!=0
BEGIN  
DECLARE @CrewLogCustomLabel2 varchar(50)
SELECT @CrewLogCustomLabel2=CustomDescription From TSFlightLog  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 2','Crew Log Custom Label2','CrewLog Custom label 2')
	AND LEN(ISNULL(CustomDescription,''))!=0	
UPDATE Company SET CrewLogCustomLBLLong2 = @CrewLogCustomLabel2  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
END

SELECT @SpecificationLong3=SpecificationLong3 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@SpecificationLong3,''))!=0
BEGIN  
DECLARE @CrewLogCustomLabel3 varchar(50)
SELECT @CrewLogCustomLabel3=CustomDescription From TSFlightLog  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 3','Crew Log Custom Label3','CrewLog Custom label 3')
UPDATE Company SET SpecificationLong3 = @CrewLogCustomLabel3  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
END

SELECT @SpecificationLong4=SpecificationLong4 from Company where CustomerID=@CustomerID and HomebaseID=@HomebaseID
IF LEN(ISNULL(@SpecificationLong4,''))!=0
BEGIN  
DECLARE @CrewLogCustomLabel4 varchar(50)
SELECT @CrewLogCustomLabel4=CustomDescription From TSFlightLog  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
    AND Category in('FLIGHT LOG','FLIGHTLOG') 
    AND OriginalDescription IN('Crew Log Custom Label 4','Crew Log Custom Label4','CrewLog Custom label 4')
UPDATE Company SET SpecificationLong4 = @CrewLogCustomLabel4  
   WHERE CustomerID=@CustomerID  
    AND HomebaseID=@HomebaseID  
END


END         
        
 SELECT [TSFlightLogID] AS TSFlightLogID,        
   [CustomerID] AS CustomerID,        
   [PrimeKey] AS PrimeKey,        
   [HomebaseID] AS HomebaseID,        
   [OriginalDescription] AS OriginalDescription,        
   [CustomDescription] AS CustomDescription,        
   [SequenceOrder] AS SequenceOrder,        
   [IsPrint] AS IsPrint,        
   [LastUpdUID] AS LastUpdUID,        
   [LastUpdTS] AS LastUpdTS,        
   [Category] AS Category,        
   [IsDeleted] AS IsDeleted,      
   [IsInActive] AS IsInActive      
 FROM TSFlightLog WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0)=0        
      AND Category='FLIGHT LOG' AND HomebaseID= @HomebaseID        
 ORDER BY OriginalDescription ASC        
END 
GO