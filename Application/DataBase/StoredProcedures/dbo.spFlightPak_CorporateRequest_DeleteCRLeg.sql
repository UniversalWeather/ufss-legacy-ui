/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_DeleteCRLeg]    Script Date: 02/13/2013 15:28:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_DeleteCRLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRLeg]
GO


CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRLeg]
(
	 @CRLegID bigint
	,@CustomerID bigint	 
)
AS
BEGIN 
SET NoCOUNT ON


Delete from CRCateringList   where CRLegID = @CRLegID  
Delete from CRFBOList   where CRLegID = @CRLegID  
Delete from CRHotelList   where CRLegID = @CRLegID  
Delete from CRPassenger   where CRLegID = @CRLegID  
Delete from CRTransportList   where CRLegID = @CRLegID  
Delete from CRLeg where CRLegID = @CRLegID 

END
GO


