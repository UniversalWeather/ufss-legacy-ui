
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCustomer]    Script Date: 01/28/2014 16:10:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddCustomer]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCustomer]    Script Date: 01/28/2014 16:10:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
CREATE PROCEDURE [dbo].[spFlightPak_AddCustomer]      
(      
 @CustomerUID uniqueidentifier ,      
 @CustomerName varchar(50),      
 @PrimaryFirstName varchar(60),       
 @PrimaryMiddleName varchar(60),       
 @PrimaryLastName varchar(60),      
 @PrimaryEmailID varchar(60),        
 @SecondaryFirstName varchar(60),      
 @SecondaryMiddleName varchar(60),      
 @SecondaryLastName varchar(60),      
 @SecondaryEmailID varchar(60),      
 @IsAll bit,      
 @IsAPIS bit,      
 @IsATS bit,      
 @IsMapping bit,      
 @IsUVFuel bit,      
 @IsUVTriplink bit,      
 @UVTripLinkName varchar(60),        
 @UVTripLinkAccountNumber varchar(32),      
 @ApisID varchar(60),      
 @ApisPassword varchar(60),      
 @LastUpdUID varchar(30),      
 @LastUpdTS datetime,      
 @IsDeleted bit      
)          
as          
Begin           
Set NoCount On          
          
-- =============================================          
-- Author:  PremPrakash.s          
-- Create date: 15-may-2012          
-- Description:Add the customer          
-- =============================================          
 Declare @CustomerID BIGINT          
----------------------------------------------------------------------------------------------------          
    -- Select Customer Id      
 Set @CustomerID =(Select isnull(MAX(CustomerID),0) from Customer)          
 --Set Last Updated Time stamp      
 SET @LastUpdTS = GETUTCDATE()         
 if @CustomerID=0        
  begin          
   Set @CustomerID=1          
  end          
 else          
  begin          
   Set  @CustomerID=@CustomerID+1          
  end          
---------------------------------------------------------------------------------------------------          
    -- Inser Customer      
 INSERT INTO [Customer]          
           ([CustomerID]          
            ,[CustomerUID]          
           ,[CustomerName]          
           ,[PrimaryFirstName]          
           ,[PrimaryMiddleName]          
           ,[PrimaryLastName]          
           ,[PrimaryEmailID]          
           ,[SecondaryFirstName]          
           ,[SecondaryMiddleName]          
           ,[SecondaryLastName]          
           ,[SecondaryEmailID]          
           ,[IsAll]          
           ,[IsAPIS]          
           ,[IsATS]          
           ,[IsMapping]          
           ,[IsUVFuel]          
           ,[IsUVTriplink]          
           ,[UVTripLinkName]          
           ,[UVTripLinkAccountNumber]          
           ,[ApisID]          
           ,[ApisPassword]          
           ,[LastUpdUID]           
           ,[LastUpdTS]           
           ,[IsDeleted]          
           )          
                     
     VALUES          
            (@CustomerID          
            ,@CustomerUID          
           ,@CustomerName          
           ,@PrimaryFirstName          
           ,@PrimaryMiddleName          
           ,@PrimaryLastName          
           ,@PrimaryEmailID          
           ,@SecondaryFirstName          
           ,@SecondaryMiddleName          
           ,@SecondaryLastName          
           ,@SecondaryEmailID          
           ,@IsAll          
           ,@IsAPIS          
           ,@IsATS          
           ,@IsMapping          
           ,@IsUVFuel          
           ,@IsUVTriplink          
           ,@UVTripLinkName          
           ,@UVTripLinkAccountNumber          
           ,@ApisID          
           ,@ApisPassword          
           ,@LastUpdUID          
           ,@LastUpdTS           
           ,@IsDeleted                     
           )          
                   
 DECLARE @FlightpakSequenceID BIGINT        
        
 -- Get Sequence Id      
 Set @FlightpakSequenceID =(Select isnull(MAX(FlightpakSequenceID),0) from FlightpakSequence)          
 if @FlightpakSequenceID=0        
begin          
Set @FlightpakSequenceID=1          
end          
else          
begin          
Set  @FlightpakSequenceID=@FlightpakSequenceID+1          
end          
             
 -- Insert Sequence      
 INSERT INTO [FlightpakSequence]        
           ([FlightpakSequenceID]        
           ,[CustomerID]        
           ,[MasterModuleCurrentNo]        
           ,[PreflightCurrentNo]        
           ,[PostflightCurrentNo]        
      ,[PostflightSlipCurrentNo]        
           ,[UtilityCurrentNo]        
           ,[CharterQuoteCurrentNo]        
           ,[CorpReqCurrentNo]        
           ,[FuelVendorCurrentNo]  
           ,[NonFunctionalCurrentNo]  
   ,[TripCurrentNo]  
   ,[TripLogCurrentNo]  
   ,[OtherCrewDutyLogCurrentNO]  
   ,[TSAUploadCurrentNo])        
     VALUES        
           (        
            @FlightpakSequenceID        
         ,@CustomerID        
           ,0        
           ,0        
           ,0        
           ,0        
           ,0        
           ,0        
           ,0        
           ,0  
           ,0  
           ,0  
           ,0  
           ,0  
           ,0)        
       
 DECLARE @AircraftDutyID BIGINT       
 DECLARE @DutyTypeID BIGINT      
 DECLARE @MetroID BIGINT    
 DECLARE @AircraftId BIGINT  
 DECLARE @FuelVendorID BIGINT    
 DECLARE @UWACustomerId BIGINT     
 -- Set Customer Id    
 EXEC sp_GetUWACustomerId @UWACustomerId OUTPUT     
     
 -- Insert Default values for AircraftDuty      
 -- Select records into temp table    
 Select * Into #Tmp_AircraftDuty From AircraftDuty Where CustomerID = @UWACustomerId    
     
 While EXISTS(select * from #Tmp_AircraftDuty)     
  Begin     
   -- Get Sequence Number    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo', @AircraftDutyID OUTPUT    
   -- Insert the UWA records for brand new customer    
   INSERT INTO AircraftDuty (AircraftDutyID,AircraftDutyCD,CustomerID,AircraftDutyDescription,ClientID,GraphColor,LastUpdUID,LastUpdTS,ForeGrndCustomColor,BackgroundCustomColor,IsCalendarEntry,DefaultStartTM,DefualtEndTM,IsAircraftStandby,IsDeleted,IsInActive)      
   (SELECT TOP 1 @AircraftDutyID,AircraftDutyCD,@CustomerId,AircraftDutyDescription,null,GraphColor,NULL,NULL,ForeGrndCustomColor,BackgroundCustomColor,IsCalendarEntry,DefaultStartTM,DefualtEndTM,IsAircraftStandby,IsDeleted,IsInActive FROM #Tmp_AircraftDuty)    
   -- Delete the inserted record from temp table    
   DELETE From #Tmp_AircraftDuty Where #Tmp_AircraftDuty.AircraftDutyID = (SELECT TOP 1 AircraftDutyID FROM #Tmp_AircraftDuty)    
 End     
 -- Drop the temp table    
 DROP TABLE #Tmp_AircraftDuty    
     
 -- Insert default value for Crew Duty    
     
 Select * Into #Tmp_CrewDutyType From CrewDutyType Where CustomerID = @UWACustomerId    
     
 While EXISTS(select * from #Tmp_CrewDutyType)     
  Begin     
   -- Get Sequence Number    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo', @DutyTypeID OUTPUT    
   -- Insert the UWA records for brand new customer    
    INSERT INTO CrewDutyType (DutyTypeID,DutyTypeCD,CustomerID,DutyTypesDescription,ValuePTS,GraphColor,WeekendPTS,IsWorkLoadIndex,IsOfficeDuty,LastUpdUID,LastUpdTS,ForeGrndCustomColor,BackgroundCustomColor,IsCrewCurrency,HolidayPTS,IsCrewDuty,CalendarEntry,DutyStartTM,DutyEndTM,IsStandby,IsDeleted,IsInActive)      
   (SELECT TOP 1 @DutyTypeID,DutyTypeCD,@CustomerID,DutyTypesDescription,ValuePTS,GraphColor,WeekendPTS,IsWorkLoadIndex,IsOfficeDuty,NULL,NULL,ForeGrndCustomColor,BackgroundCustomColor,IsCrewCurrency,HolidayPTS,IsCrewDuty,CalendarEntry,DutyStartTM,DutyEndTM,IsStandby,IsDeleted,IsInActive FROM #Tmp_CrewDutyType)    
   -- Delete the inserted record from temp table    
   DELETE From #Tmp_CrewDutyType Where #Tmp_CrewDutyType.DutyTypeID = (SELECT TOP 1 DutyTypeID FROM #Tmp_CrewDutyType)    
 End     
 -- Drop the temp table    
 DROP TABLE #Tmp_CrewDutyType    
     
 -- Insert default value for Metro    
 Select * Into #Tmp_Metro From Metro Where CustomerID = @UWACustomerId    
     
 While EXISTS(select * from #Tmp_Metro)     
  Begin     
   -- Get Sequence Number    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo', @MetroID OUTPUT    
   -- Insert the UWA records for brand new customer    
    INSERT INTO Metro (MetroID,MetroCD,MetroName,CountryID,StateName,LastUpdUID,LastUpdTS,IsDeleted,CustomerID,IsInActive)      
   (SELECT TOP 1 @MetroID,MetroCD,MetroName,CountryID,StateName,null,null,IsDeleted,@CustomerID,IsInActive FROM #Tmp_Metro)    
   -- Delete the inserted record from temp table    
   DELETE From #Tmp_Metro Where #Tmp_Metro.MetroID = (SELECT TOP 1 MetroID FROM #Tmp_Metro)    
 End     
 -- Drop the temp table    
 DROP TABLE #Tmp_Metro    
 
 --Ramesh: To get the LicenseType selected at the time of CustomerSetup.
 DECLARE @LicenseType VARCHAR(60)
 SELECT @LicenseType = LicenseType FROM CustomerLicense WHERE CustomerID = @CustomerID
 --Ramesh: Condition to avaoid inserting into Aircraft table, when LicenseType is EV.
 IF (@LicenseType != 'EV')
 BEGIN
	 -- Insert default value for Aircraft   
	 Select * Into #Tmp_Aircraft From Aircraft Where CustomerID = @UWACustomerId    
	     
	 While EXISTS(select * from #Tmp_Aircraft)     
	  Begin     
	   -- Get Sequence Number    
	   EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo', @AircraftId OUTPUT    
	   -- Insert the UWA records for brand new customer    
		INSERT INTO Aircraft (AircraftID,CustomerID,AircraftCD,AircraftDescription,ChargeRate,ChargeUnit,  
		   PowerSetting,PowerDescription,WindAltitude,PowerSettings1Description,PowerSettings1TrueAirSpeed,PowerSettings1HourRange,  
		   PowerSettings1TakeOffBias,PowerSettings1LandingBias,PowerSettings2Description,PowerSettings2TrueAirSpeed,PowerSettings2HourRange,  
		   PowerSettings2TakeOffBias,PowerSettings2LandingBias,PowerSettings3Description,PowerSettings3TrueAirSpeed,PowerSettings3HourRange,  
		   PowerSettings3TakeOffBias,PowerSettings3LandingBias,IsFixedRotary,ClientID,CQChargeRate,CQChargeUnit,PositionRate,PostionUnit,StandardCrew,  
		   StandardCrewRON,AdditionalCrew,AdditionalCrewRON,CharterQuoteWaitTM,LandingFee,CostBy,FixedCost,PercentageCost,TaxRON,AdditionalCrewTax,  
		   IsCharterQuoteWaitTax,LandingTax,DescriptionRON,AdditionalCrewDescription,MinDailyREQ,LastUpdUID,LastUpdTS,AircraftSize,AircraftTypeCD,  
		   IntlStdCrewNum,DomesticStdCrewNum,MinimumDayUseHrs,DailyUsageAdjTax,LandingFeeTax,IsDeleted,IsInActive,MarginalPercentage)      
	   (SELECT TOP 1 @AircraftID,@CustomerID,AircraftCD,AircraftDescription,ChargeRate,ChargeUnit,  
		   PowerSetting,PowerDescription,WindAltitude,PowerSettings1Description,PowerSettings1TrueAirSpeed,PowerSettings1HourRange,  
		   PowerSettings1TakeOffBias,PowerSettings1LandingBias,PowerSettings2Description,PowerSettings2TrueAirSpeed,PowerSettings2HourRange,  
		   PowerSettings2TakeOffBias,PowerSettings2LandingBias,PowerSettings3Description,PowerSettings3TrueAirSpeed,PowerSettings3HourRange,  
		   PowerSettings3TakeOffBias,PowerSettings3LandingBias,IsFixedRotary,ClientID,CQChargeRate,CQChargeUnit,PositionRate,PostionUnit,StandardCrew,  
		   StandardCrewRON,AdditionalCrew,AdditionalCrewRON,CharterQuoteWaitTM,LandingFee,CostBy,FixedCost,PercentageCost,TaxRON,AdditionalCrewTax,  
		   IsCharterQuoteWaitTax,LandingTax,DescriptionRON,AdditionalCrewDescription,MinDailyREQ,NULL,NULL,AircraftSize,AircraftTypeCD,  
		   IntlStdCrewNum,DomesticStdCrewNum,MinimumDayUseHrs,DailyUsageAdjTax,LandingFeeTax,IsDeleted,IsInActive,MarginalPercentage FROM #Tmp_Aircraft)    
	   -- Delete the inserted record from temp table    
	   DELETE From #Tmp_Aircraft Where #Tmp_Aircraft.AircraftID = (SELECT TOP 1 AircraftID FROM #Tmp_Aircraft)    
	 End     
	 -- Drop the temp table    
	 DROP TABLE #Tmp_Aircraft    
 END
 
 ---- Insert default value for Fuel Vendor    
 --Select * Into #Tmp_FuelVendor From FuelVendor Where CustomerID = @UWACustomerId    
     
 --While EXISTS(select * from #Tmp_FuelVendor)     
 -- Begin     
 --  -- Get Sequence Number    
 --  EXECUTE dbo.usp_GetSequenceNumber @CustomerId, 'MasterModuleCurrentNo', @FuelVendorID OUTPUT    
 --  -- Insert the UWA records for brand new customer    
 --   INSERT INTO FuelVendor (FuelVendorID,VendorCD,VendorName,AirportID,CustomerID,VendorDescription,ServerInfo,BaseFileName,FileLocation,FileTYPE,FldSep,    
 --        IsHeaderRow,VendorDateFormat,LastUpdUID,LastUpdTS,IsDeleted,UWAUpdates)      
 --  (SELECT TOP 1 @FuelVendorID,VendorCD,VendorName,AirportID,@CustomerID,VendorDescription,ServerInfo,BaseFileName,FileLocation,FileTYPE,FldSep,    
 --       IsHeaderRow,VendorDateFormat,NULL,NULL,IsDeleted,UWAUpdates FROM #Tmp_FuelVendor)    
 --  -- Delete the inserted record from temp table    
 --  DELETE From #Tmp_FuelVendor Where #Tmp_FuelVendor.FuelVendorID = (SELECT TOP 1 FuelVendorID FROM #Tmp_FuelVendor)    
 --End     
 ---- Drop the temp table    
 --DROP TABLE #Tmp_FuelVendor    
     
 Select @CustomerID as CustomerID ,@CustomerUID as CustomerUID          
              
          
End    
GO

