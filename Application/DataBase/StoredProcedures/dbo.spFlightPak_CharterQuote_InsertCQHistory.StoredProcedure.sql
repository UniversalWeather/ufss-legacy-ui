IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQHistory]
(
	
	@CustomerID bigint,
	@CQMainID bigint,
	@LogisticsHistory varchar(MAX),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE @RevisionNumber  int   
	DECLARE  @CQHistoryID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQHistoryID OUTPUT 
	--select top 1 @RevisionNumber = isnull([RevisionNumber],0) +1 from CQHistory where CQMainID = @CQMainID order by LastUpdTS desc
	
	--set @RevisionNumber = ISNULL(@RevisionNumber,1)
INSERT INTO [CQHistory] ([CQHistoryID], [CustomerID], [CQMainID], [LogisticsHistory], [LastUpdUID], [LastUpdTS], [IsDeleted]) VALUES (@CQHistoryID, @CustomerID, @CQMainID, @LogisticsHistory, @LastUpdUID, @LastUpdTS, @IsDeleted);
	
SELECT @CQHistoryID AS CQHistoryID
GO
