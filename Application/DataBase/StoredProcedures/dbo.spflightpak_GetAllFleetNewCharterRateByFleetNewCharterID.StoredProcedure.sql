
/****** Object:  StoredProcedure [dbo].[spflightpak_GetAllFleetNewCharterRateByFleetNewCharterID]    Script Date: 02/01/2013 12:04:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spflightpak_GetAllFleetNewCharterRateByFleetNewCharterID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spflightpak_GetAllFleetNewCharterRateByFleetNewCharterID]
GO



/****** Object:  StoredProcedure [dbo].[spflightpak_GetAllFleetNewCharterRateByFleetNewCharterID]    Script Date: 02/01/2013 12:04:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spflightpak_GetAllFleetNewCharterRateByFleetNewCharterID]    
 (
 @FleetNewCharterRateID BIGINT
 )    
AS    BEGIN      
-- =============================================          
-- Author:Karthikeyan.S          
-- Create date: 29/01/2013          
-- Description: Get the FleetNewCharterRate information          
-- =============================================          
 SET NOCOUNT ON          
    SELECT     
  FleetNewCharterRate.FleetNewCharterRateID,  
  FleetNewCharterRate.CustomerID,  
  FleetNewCharterRate.FleetID,  
  FleetNewCharterRate.AircraftCD,  
  FleetNewCharterRate.OrderNum,  
  FleetNewCharterRate.FleetNewCharterRateDescription,  
  FleetNewCharterRate.ChargeUnit,  
  FleetNewCharterRate.NegotiatedChgUnit,  
  FleetNewCharterRate.BuyDOM,  
  FleetNewCharterRate.SellDOM,  
  FleetNewCharterRate.IsTaxDOM,  
  FleetNewCharterRate.IsDiscountDOM,  
  FleetNewCharterRate.BuyIntl,  
  FleetNewCharterRate.SellIntl,  
  FleetNewCharterRate.IsTaxIntl,  
  FleetNewCharterRate.IsDiscountIntl,   
   --FleetNewCharterRate.StandardCrewDOM,    
  --FleetNewCharterRate.StandardCrewIntl,    
  --FleetNewCharterRate.MinimumDailyRequirement,  
  Fleet.StandardCrewDOM AS FleetStandardCrewDOM,    
  Fleet.StandardCrewIntl AS FleetStandardCrewIntl,    
  Fleet.MinimumDay AS FleetMinimumDay,
  Fleet.IsTaxDailyAdj AS FleetIsTaxDailyAdj,
  Fleet.IsTaxLandingFee AS FleetIsTaxLandingFee,  
  
  Aircraft.DailyUsageAdjTax AS AircraftDailyUsageAdjTax,
  Aircraft.LandingFeeTax AS AircraftLandingFeeTax,
  Aircraft.MinimumDayUseHrs AS AircraftMinimumDayUseHrs,
  Aircraft.IntlStdCrewNum AS AircraftIntlStdCrewNum,
  Aircraft.DomesticStdCrewNum AS AircraftDomesticStdCrewNum,
  
  CQCustomer.DailyUsageAdjTax AS CQCustomerDailyUsageAdjTax,
  CQCustomer.LandingFeeTax AS CQCustomerLandingFeeTax,
  CQCustomer.MinimumDayUseHrs AS CQCustomerMinimumDayUseHrs,
  CQCustomer.IntlStdCrewNum AS CQCustomerIntlStdCrewNum,
  CQCustomer.DomesticStdCrewNum AS CQCustomerDomesticStdCrewNum,
   
  FleetNewCharterRate.YearMade,  
  FleetNewCharterRate.ExteriorColor,  
  FleetNewCharterRate.ColorIntl,  
  FleetNewCharterRate.IsAFIS,  
  FleetNewCharterRate.IsUWAData,  
  FleetNewCharterRate.DBAircraftFlightChgID,  
  FleetNewCharterRate.DSAircraftFlightChgID,  
  FleetNewCharterRate.BuyAircraftFlightIntlID,  
  FleetNewCharterRate.SellAircraftFlightIntlID,  
  FleetNewCharterRate.LastUpdUID,  
  FleetNewCharterRate.LastUpdTS,  
  FleetNewCharterRate.AircraftTypeID,  
  FleetNewCharterRate.IsDeleted,  
  
  DBAccount.AccountDescription as DBAccountDescription,
  DBAccount.AccountNum as DBAccountNum,
  DSAccount.AccountDescription as DSAccountDescription,
  DSAccount.AccountNum as DSAccountNum,
  BuyAccount.AccountDescription as BuyAccountDescription,
  BuyAccount.AccountNum as BuyAccountNum,
  SellAccount.AccountDescription as SellAccountDescription,
  SellAccount.AccountNum as SellAccountNum,
  
  Fleet.AircraftCD as FleetAircraftCD,  
  Fleet.TailNum as FleetTailNum,  
  Aircraft.AircraftCD as AircraftTypeCD  
         FROM FleetNewCharterRate as FleetNewCharterRate  
			LEFT OUTER JOIN Account as DBAccount on DBAccount.AccountID = FleetNewCharterRate.DBAircraftFlightChgID 
			LEFT OUTER JOIN Account as DSAccount on DSAccount.AccountID = FleetNewCharterRate.DSAircraftFlightChgID 
			LEFT OUTER JOIN Account as BuyAccount on BuyAccount.AccountID = FleetNewCharterRate.BuyAircraftFlightIntlID 
			LEFT OUTER JOIN Account as SellAccount on SellAccount.AccountID = FleetNewCharterRate.SellAircraftFlightIntlID 
			LEFT OUTER JOIN Fleet as Fleet  on Fleet.FleetID = FleetNewCharterRate.FleetID  
			LEFT OUTER JOIN Aircraft as Aircraft  on Aircraft.AircraftID = FleetNewCharterRate.AircraftTypeID     
			LEFT OUTER JOIN CQCustomer as CQCustomer on CQCustomer.CQCustomerID = FleetNewCharterRate.CQCustomerID
                       
 WHERE FleetNewCharterRateID = @FleetNewCharterRateID
 order by FleetNewCharterRate.OrderNum ASC 
 --FleetNewCharterRate.FleetNewCharterRateDescription     
END 


GO


