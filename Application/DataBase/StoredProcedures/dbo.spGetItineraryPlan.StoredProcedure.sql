
GO
/****** Object:  StoredProcedure [dbo].[spGetItineraryPlan]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetItineraryPlan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetItineraryPlan]
GO

CREATE PROCEDURE spGetItineraryPlan
(@ItineraryPlanID	BIGINT
,@CustomerID	BIGINT
,@IsDeleted	BIT)
AS BEGIN

	IF (@ItineraryPlanID = -1)
		BEGIN
			SELECT	ItineraryPlan.ItineraryPlanID 
				   ,ItineraryPlan.CustomerID 
				   ,ItineraryPlan.IntinearyNUM 
				   ,ItineraryPlan.ItineraryPlanQuarter 
				   ,ItineraryPlan.ItineraryPlanQuarterYear 
				   ,ItineraryPlan.AircraftID 
				   ,ItineraryPlan.FleetID 
				   ,ItineraryPlan.ItineraryPlanDescription 
				   ,ItineraryPlan.ClientID 
				   ,ItineraryPlan.HomebaseID 
				   ,ItineraryPlan.LastUpdUID 
				   ,ItineraryPlan.LastUpdTS 
				   ,ItineraryPlan.IsDeleted
				   ,Aircraft.AircraftCD AS AircraftCD
				   ,Aircraft.AircraftDescription AS AircraftDescription
				   ,Fleet.TailNum AS TailNum
				   ,Fleet.TypeDescription AS TypeDescription
				   ,Client.ClientCD AS ClientCD
				   ,Client.ClientDescription AS ClientDescription
				   ,Airport.IcaoID AS IcaoID
				   ,Company.BaseDescription AS BaseDescription
			FROM	ItineraryPlan
			LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = ItineraryPlan.AircraftID
			LEFT OUTER JOIN Fleet ON Fleet.FleetID = ItineraryPlan.FleetID
			LEFT OUTER JOIN Client ON Client.ClientID = ItineraryPlan.ClientID
			LEFT OUTER JOIN Company ON Company.HomebaseID = ItineraryPlan.HomebaseID
			LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID			
			WHERE	ItineraryPlan.CustomerID = @CustomerID
					AND ItineraryPlan.IsDeleted = @IsDeleted
		END
	ELSE
		BEGIN
			SELECT	ItineraryPlan.ItineraryPlanID 
				   ,ItineraryPlan.CustomerID 
				   ,ItineraryPlan.IntinearyNUM 
				   ,ItineraryPlan.ItineraryPlanQuarter 
				   ,ItineraryPlan.ItineraryPlanQuarterYear 
				   ,ItineraryPlan.AircraftID 
				   ,ItineraryPlan.FleetID 
				   ,ItineraryPlan.ItineraryPlanDescription 
				   ,ItineraryPlan.ClientID 
				   ,ItineraryPlan.HomebaseID 
				   ,ItineraryPlan.LastUpdUID 
				   ,ItineraryPlan.LastUpdTS 
				   ,ItineraryPlan.IsDeleted
				   ,Aircraft.AircraftCD AS AircraftCD
				   ,Aircraft.AircraftDescription AS AircraftDescription
				   ,Fleet.TailNum AS TailNum
				   ,Fleet.TypeDescription AS TypeDescription
				   ,Client.ClientCD AS ClientCD
				   ,Client.ClientDescription AS ClientDescription
				   ,Airport.IcaoID AS IcaoID
				   ,Company.BaseDescription AS BaseDescription	   
			FROM	ItineraryPlan
			LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = ItineraryPlan.AircraftID
			LEFT OUTER JOIN Fleet ON Fleet.FleetID = ItineraryPlan.FleetID
			LEFT OUTER JOIN Client ON Client.ClientID = ItineraryPlan.ClientID
			LEFT OUTER JOIN Company ON Company.HomebaseID = ItineraryPlan.HomebaseID
			LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID			
			WHERE	ItineraryPlan.CustomerID = @CustomerID
					AND ItineraryPlan.IsDeleted = @IsDeleted
					AND ItineraryPlan.ItineraryPlanID = @ItineraryPlanID
		END

END