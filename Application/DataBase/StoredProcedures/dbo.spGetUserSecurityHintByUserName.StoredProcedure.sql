/****** Object:  StoredProcedure [dbo].[spGetUserSecurityHintByUserName]    Script Date: 09/07/2012 19:21:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetUserSecurityHintByUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetUserSecurityHintByUserName]
GO
CREATE PROCEDURE [dbo].[spGetUserSecurityHintByUserName](      
 @UserName varchar(30) )      
AS      
BEGIN      
       
 SELECT [UserSecurityHintID], [UserName],[SecurityQuestion], [ActiveStartDT], [ActiveEndDT],       
 [PWDStatus] , [LastUpdUID], [LastUpdTS] , [SecurityAnswer] , [CustomerID],IsInactive FROM UserSecurityHint      
 WHERE UserName = @UserName      
      
END 