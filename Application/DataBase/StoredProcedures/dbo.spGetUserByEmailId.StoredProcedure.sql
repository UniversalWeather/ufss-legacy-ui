
GO
/****** Object:  StoredProcedure [dbo].[spGetUserByEmailId]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetUserByEmailId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetUserByEmailId]
GO
 --=====================================================================      
 -- Author:  Viswanathan Perumal
 -- Create date: 20-Nov-2012
 -- Description: Unique Email Id Validation
 --=====================================================================      
CREATE PROCEDURE [dbo].[spGetUserByEmailId](@EmailID varchar(100))      
AS      
BEGIN      

	Select UserMaster.*,Customer.IsCustomerLock FROM UserMaster
 JOIN Customer ON Customer.CustomerID = UserMaster.CustomerID
  Where EmailID = @EmailID
        
END  


GO


