GO
/****** Object:  StoredProcedure [dbo].[spUpdateItineraryPlanTransfer]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateItineraryPlanTransfer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateItineraryPlanTransfer]
GO
CREATE PROCEDURE spUpdateItineraryPlanTransfer
(@ItineraryPlanID	BIGINT
,@CustomerID	BIGINT
,@LastUpdUID VARCHAR(30))
AS BEGIN
/*
spUpdateItineraryPlanTransfer 10003330,10003,'srini3'
spUpdateItineraryPlanTransfer 10003330,10003,'srini3                        '
*/
	DECLARE  @NewTripID BIGINT       
	DECLARE  @NewTripNUM BIGINT 
	DECLARE	 @IsAutoRevisionNum  BIT
	DECLARE  @IsAutoDispatch BIT
	DECLARE  @EstDeptDate date
	DECLARE  @TripCost numeric(12,2)
	
	SELECT @IsAutoDispatch = IsAutomaticDispatchNum , @IsAutoRevisionNum = IsAutoRevisionNum FROM Company 
	WHERE HomebaseId = (SELECT HomebaseID FROM UserMaster WHERE Username = @LastUpdUID)
		
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripID OUTPUT 	
	SET @NewTripNUM = 0
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @NewTripNUM OUTPUT      	
	SELECT @NewTripNUM = TripCurrentNo  FROM [FlightpakSequence] WHERE customerID = @CustomerID
		
	SELECT ItineraryPlanID, CustomerID, IntinearyNUM, ItineraryPlanQuarter, ItineraryPlanQuarterYear, AircraftID, 
			FleetID, ItineraryPlanDescription, ClientID, HomebaseID, LastUpdUID, LastUpdTS, IsDeleted
	INTO #TmpItineraryPlan
	FROM ItineraryPlan
	WHERE ItineraryPlanID = @ItineraryPlanID
		AND IsDeleted = 0

	SELECT ItineraryPlanDetailID, CustomerID, ItineraryPlanID, IntinearyNUM, LegNUM, DAirportID, AAirportID, Distance, 
			PowerSetting, TakeoffBIAS, LandingBias, TrueAirSpeed, WindsBoeingTable, ElapseTM, DepartureLocal, DepartureGMT, 
			DepartureHome, ArrivalLocal, ArrivalGMT, ArrivalHome, IsDepartureConfirmed, IsArrivalConfirmation, PassengerNUM, 
			Cost, WindReliability, LegID, LastUpdUID, LastUpdTS, IsDeleted
	INTO #TmpItineraryPlanDetail
	FROM ItineraryPlanDetail
	WHERE ItineraryPlanID = @ItineraryPlanID
		AND IsDeleted = 0
	
	SELECT @EstDeptDate = convert(date, Min(DepartureLocal)),@TripCost = sum(Cost)  FROM #TmpItineraryPlanDetail
	
	INSERT INTO PreflightMain    
	([TripID]    
   ,[CustomerID]    
   ,[TripNUM]    
   ,[PreviousNUM]    
   ,[TripDescription]    
   ,[FleetID]    
   ,[DispatchNUM]    
   ,[TripStatus]    
   ,[EstDepartureDT]    
   ,[EstArrivalDT]    
   ,[RecordType]    
   ,[AircraftID]    
   ,[PassengerRequestorID]    
   ,[RequestorFirstName]    
   ,[RequestorMiddleName]    
   ,[RequestorLastName]    
   ,[RequestorPhoneNUM]    
   ,[DepartmentDescription]    
   ,[AuthorizationDescription]    
   ,[RequestDT]    
   ,[IsCrew]    
   ,[IsPassenger]    
   ,[IsQuote]    
   ,[IsScheduledServices]    
   ,[IsAlert]    
   ,[Notes]    
   ,[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,[IsAirportAlert]    
   ,[IsLog]    
   ,[BeginningGMTDT]    
   ,[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsPrivate]    
   ,[IsCorpReq]    
   ,[Acknowledge]    
   ,[DispatchNotes]    
   ,[TripRequest]    
   ,[WaitList]    
   ,[FlightNUM]    
   ,[FlightCost]    
   ,[AccountID]    
   ,[TripException]    
   ,[TripSheetNotes]    
   ,[CancelDescription]    
   ,[IsCompleted]    
   ,[IsFBOUpdFlag]    
   ,[RevisionNUM]    
   ,[RevisionDescriptioin]    
   ,[IsPersonal]    
   ,[DsptnName]    
   ,[ScheduleCalendarHistory]    
   ,[ScheduleCalendarUpdate]    
   ,[CommentsMessage]    
   ,[EstFuelQTY]    
   ,[CrewID]    
   ,[ReleasedBy]    
   ,[IsDeleted]    
   ,[VerifyNUM]    
   ,[APISSubmit]    
   ,[APISStatus]    
   ,[APISException]    
   ,[IsAPISValid]    
   ,[DepartmentID]    
   ,[AuthorizationID]    
   ,[DispatcherUserName]    
   ,[EmergencyContactID])     
     
	SELECT @NewTripID    
   ,@CustomerID    
   ,@NewTripNUM    
   ,0    
   ,ItineraryPlanDescription    
   ,[FleetID]    
   ,case when @IsAutoDispatch=0 THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END     
	-- ,case when rtrim(ltrim(isnull([DispatchNUM],'')))='' THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END     
   ,'W'    
   ,@EstDeptDate    
   ,null --[EstArrivalDT]    
   ,'T'    
   ,[AircraftID]    
   ,null --[PassengerRequestorID]    
   ,null --[RequestorFirstName]    
   ,null --[RequestorMiddleName]    
   ,null --[RequestorLastName]    
   ,null --[RequestorPhoneNUM]    
   ,null --[DepartmentDescription]    
   ,null --[AuthorizationDescription]    
   ,getdate()-- [RequestDT]    
   ,null -- [IsCrew]    
   ,null --[IsPassenger]    
   ,null --[IsQuote]    
   ,null --[IsScheduledServices]    
   ,null --[IsAlert]    
   ,null --[Notes]    
   ,null --[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,null --[IsAirportAlert]    
   ,0    
   ,null --[BeginningGMTDT]    
   ,null --[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,0
   ,null--[IsCorpReq]    
   ,null--[Acknowledge]    
   ,null--[DispatchNotes]    
   ,null--[TripRequest]    
   ,null--[WaitList]    
   ,null--[FlightNUM]    
   ,@TripCost 
   ,null--[AccountID]    
   ,null--[TripException]    
   ,null--[TripSheetNotes]    
   ,null--[CancelDescription]    
   ,null--[IsCompleted]    
   ,null--[IsFBOUpdFlag]    
   --,--case when @IsRevision=0  then null  else 1 end     
   ,case when ltrim(rtrim(isnull(@IsAutoRevisionNum,0)))=0  then null  else 1 end     
   ,null --[RevisionDescriptioin]    
   ,null --[IsPersonal]    
   ,null --[DsptnName]    
   ,null --[ScheduleCalendarHistory]    
   ,null --[ScheduleCalendarUpdate]    
   ,null --[CommentsMessage]    
   ,null --[EstFuelQTY]    
   ,null --[CrewID]    
   ,null --[ReleasedBy]    
   ,0    
   ,null --[VerifyNUM]    
   ,null--[APISSubmit]    
   ,null --[APISStatus]    
   ,null --[APISException]    
   ,null --[IsAPISValid]    
   ,null --[DepartmentID]    
   ,null --[AuthorizationID]    
   ,null --[DispatcherUserName]    
   ,null --[EmergencyContactID]           
   from #TmpItineraryPlan
	
	CREATE Table #TempLeg (    
  rownum int identity (1,1),    
  NewLegId Bigint,    
  ItineraryPlanDetailID Bigint    
  )    
      
  insert into #TempLeg (NewLegId,ItineraryPlanDetailID ) select null, ItineraryPlanDetailID from #TmpItineraryPlanDetail
      
  Declare @Rownumber int    
  DECLARE  @NewLegID BIGINT        
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempLeg where NewLegId is null) >0    
  begin    
   Set @NewLegID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewLegID OUTPUT     
   update #TempLeg set NewLegId = @NewLegID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End 
	INSERT INTO [PreflightLeg]        
	([LegID]    
    ,[TripID]    
    ,[CustomerID]    
    ,[TripNUM]    
    ,[LegNUM]    
    ,[DepartICAOID]    
    ,[ArriveICAOID]    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,[IsApproxTM]    
    ,[IsScheduledServices]    
    ,[DepartureDTTMLocal]    
    ,[ArrivalDTTMLocal]    
    ,[DepartureGreenwichDTTM]    
    ,[ArrivalGreenwichDTTM]    
    ,[HomeDepartureDTTM]    
    ,[HomeArrivalDTTM]    
    ,[GoTime]    
    ,[FBOID]    
    ,[LogBreak]    
    ,[FlightPurpose]    
    ,[PassengerRequestorID]    
    ,[RequestorName]    
    ,[DepartmentID]    
    ,[DepartmentName]    
    ,[AuthorizationID]    
    ,[AuthorizationName]    
    ,[PassengerTotal]   
    ,[SeatTotal]    
    ,[ReservationTotal]   
    ,[ReservationAvailable]   
    ,[WaitNUM]    
    ,[DutyTYPE]    
    ,[DutyHours]    
    ,[RestHours]    
    ,[FlightHours]    
    ,[Notes]    
    ,[FuelLoad]    
    ,[OutbountInstruction]    
    ,[DutyTYPE1]    
    ,[IsCrewDiscount]    
    ,[CrewDutyRulesID]    
    ,[IsDutyEnd]    
    ,[FedAviationRegNUM]    
    ,[ClientID]    
    ,[LegID1]    
    ,[WindReliability]    
    ,[OverrideValue]    
    ,[CheckGroup]    
    ,[AdditionalCrew]    
    ,[PilotInCommand]    
    ,[SecondInCommand]    
    ,[NextLocalDTTM]    
    ,[NextGMTDTTM]    
    ,[NextHomeDTTM]    
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,[CrewNotes]    
    ,[IsPrivate]    
    ,[WaitList]    
    ,[FlightNUM]    
    ,[FlightCost]    
    ,[CrewFuelLoad]    
    ,[IsDeleted]    
    ,[UWAID]    
    ,[USCrossing]    
    ,[Response]    
    ,[ConfirmID]    
    ,[CrewDutyAlert]    
    ,[AccountID]    
    ,[FlightCategoryID])        
       
    select  NewLegId    
    ,@NewTripID    
    ,@CustomerID    
    ,null --[TripNUM]    
    ,[LegNUM]    
    ,DAirportID    
    ,AAirportID    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,0 --[IsApproxTM]    
    ,null --[IsScheduledServices]     
	,DepartureLocal    
    ,ArrivalLocal    
    ,DepartureGMT    
    ,ArrivalGMT    
    ,DepartureHome    
    ,ArrivalHome 
    ,null --[GoTime]    
    ,null --[FBOID]    
    ,null --[LogBreak]    
    ,null --[FlightPurpose]    
    ,null --[PassengerRequestorID]    
    ,null --[RequestorName]    
    ,null --[DepartmentID]    
    ,null --[DepartmentName]    
    ,null --[AuthorizationID]    
    ,null --[AuthorizationName]    
    ,0 --PassengerNUM   
    ,0--[@SeatTotal]
    ,0   
    ,0--[ReservationAvailable]
    ,null --[WaitNUM]    
    ,null --[DutyTYPE]    
    ,null --[DutyHours]    
    ,null --[RestHours]    
    ,null --[FlightHours]    
    ,null --[Notes]    
    ,null --[FuelLoad]    
    ,null --[OutbountInstruction]    
    ,null --[DutyTYPE1]    
    ,null --[IsCrewDiscount]    
    ,null --[CrewDutyRulesID]    
    ,0 --[IsDutyEnd]    
    ,null --[FedAviationRegNUM]    
    ,null --[ClientID]    
    ,null --[LegID1]    
    ,[WindReliability]    
    ,null --[OverrideValue]    
    ,null --[CheckGroup]    
    ,null --[AdditionalCrew]    
    ,null --[PilotInCommand]    
    ,null --[SecondInCommand]    
    ,null --[NextLocalDTTM]    --need to modify
    ,null --[NextGMTDTTM]        --need to modify
    ,null --[NextHomeDTTM]        --need to modify
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,null --[CrewNotes]    
    ,0
    ,null --[WaitList]    
    ,null --[FlightNUM]    
    ,cost
    ,null --[CrewFuelLoad]    
    ,0
    ,null --[UWAID]    
    ,null --[USCrossing]    
    ,null --[Response]    
    ,null --[ConfirmID]    
    ,null --[CrewDutyAlert]    
    ,null --[AccountID]    
    ,null --[FlightCategoryID]    
    from #TmpItineraryPlanDetail IP, #TempLeg T    
    where IP.ItineraryPlanDetailID = T.ItineraryPlanDetailID 
	
	DROP TABLE #TmpItineraryPlanDetail
	DROP TABLE #TmpItineraryPlan
	SELECT 1 AS RESULT

END
GO
