
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQQuoteSummary]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQQuoteSummary
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQQuoteSummary
(
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@IsPrintUsageAdj bit,
	@UsageAdjDescription varchar(60),
	@CustomUsageAdj numeric(17, 2),
	@IsPrintAdditonalFees bit,
	@AdditionalFeeDescription varchar(60),
	@AdditionalFeesTax numeric(17, 2),
	@PositioningDescriptionINTL numeric(17, 2),
	@IsAdditionalFees numeric(17, 2),
	@CustomAdditionalFees numeric(17, 2),
	@IsPrintLandingFees bit,
	@LandingFeeDescription varchar(60),
	@CustomLandingFee numeric(17, 2),
	@IsPrintSegmentFee bit,
	@SegmentFeeDescription varchar(60),
	@CustomSegmentFee numeric(17, 2),
	@IsPrintStdCrew bit,
	@StdCrewROMDescription varchar(60),
	@CustomStdCrewRON numeric(17, 2),
	@IsPrintAdditionalCrew bit,
	@AdditionalCrewDescription varchar(60),
	@CustomAdditionalCrew numeric(17, 2),
	@IsAdditionalCrewRON bit,
	@AdditionalDescriptionCrewRON varchar(60),
	@CustomAdditionalCrewRON numeric(17, 2),
	@IsPrintWaitingChg bit,
	@WaitChgDescription varchar(60),
	@CustomWaitingChg numeric(17, 2),
	@IsPrintFlightChg bit,
	@FlightChgDescription varchar(60),
	@CustomFlightChg numeric(17, 2),
	@IsPrintSubtotal bit,
	@SubtotalDescription varchar(60),
	@IsPrintDiscountAmt bit,
	@DescriptionDiscountAmt varchar(60),
	@IsPrintTaxes bit,
	@TaxesDescription varchar(60),
	@IsPrintInvoice bit,
	@InvoiceDescription varchar(60),
	@ReportQuote numeric(17, 2),
	@IsPrintPay bit,
	@PrepayDescription varchar(60),
	@IsPrintRemaingAMT bit,
	@RemainingAmtDescription varchar(60),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQQuoteSummaryID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQQuoteSummaryID OUTPUT 
	
	INSERT INTO [CQQuoteSummary] ([CQQuoteSummaryID], [CustomerID], [CQQuoteMainID], [IsPrintUsageAdj], [UsageAdjDescription], [CustomUsageAdj], [IsPrintAdditonalFees], [AdditionalFeeDescription], [AdditionalFeesTax], [PositioningDescriptionINTL], [IsAdditionalFees], [CustomAdditionalFees], [IsPrintLandingFees], [LandingFeeDescription], [CustomLandingFee], [IsPrintSegmentFee], [SegmentFeeDescription], [CustomSegmentFee], [IsPrintStdCrew], [StdCrewROMDescription], [CustomStdCrewRON], [IsPrintAdditionalCrew], [AdditionalCrewDescription], [CustomAdditionalCrew], [IsAdditionalCrewRON], [AdditionalDescriptionCrewRON], [CustomAdditionalCrewRON], [IsPrintWaitingChg], [WaitChgDescription], [CustomWaitingChg], [IsPrintFlightChg], [FlightChgDescription], [CustomFlightChg], [IsPrintSubtotal], [SubtotalDescription], [IsPrintDiscountAmt], [DescriptionDiscountAmt], [IsPrintTaxes], [TaxesDescription], [IsPrintInvoice], [InvoiceDescription], [ReportQuote], [IsPrintPay], [PrepayDescription], [IsPrintRemaingAMT], [RemainingAmtDescription], [LastUpdUID], [LastUpdTS], [IsDeleted]) 
		VALUES (@CQQuoteSummaryID, @CustomerID, @CQQuoteMainID, @IsPrintUsageAdj, @UsageAdjDescription, @CustomUsageAdj, @IsPrintAdditonalFees, @AdditionalFeeDescription, @AdditionalFeesTax, @PositioningDescriptionINTL, @IsAdditionalFees, @CustomAdditionalFees, @IsPrintLandingFees, @LandingFeeDescription, @CustomLandingFee, @IsPrintSegmentFee, @SegmentFeeDescription, @CustomSegmentFee, @IsPrintStdCrew, @StdCrewROMDescription, @CustomStdCrewRON, @IsPrintAdditionalCrew, @AdditionalCrewDescription, @CustomAdditionalCrew, @IsAdditionalCrewRON, @AdditionalDescriptionCrewRON, @CustomAdditionalCrewRON, @IsPrintWaitingChg, @WaitChgDescription, @CustomWaitingChg, @IsPrintFlightChg, @FlightChgDescription, @CustomFlightChg, @IsPrintSubtotal, @SubtotalDescription, @IsPrintDiscountAmt, @DescriptionDiscountAmt, @IsPrintTaxes, @TaxesDescription, @IsPrintInvoice, @InvoiceDescription, @ReportQuote, @IsPrintPay, @PrepayDescription, @IsPrintRemaingAMT, @RemainingAmtDescription, @LastUpdUID, @LastUpdTS, @IsDeleted);
	
	SELECT @CQQuoteSummaryID  as CQQuoteSummaryID
GO

