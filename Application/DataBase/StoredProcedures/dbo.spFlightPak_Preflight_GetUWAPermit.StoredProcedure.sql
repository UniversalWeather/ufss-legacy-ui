

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetUWAPermit]    Script Date: 09/19/2013 13:30:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetUWAPermit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetUWAPermit]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetUWAPermit]    Script Date: 09/19/2013 13:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create Procedure [dbo].[spFlightPak_Preflight_GetUWAPermit]
(
@UWAPermit BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get UWAPermit by UWAPermit
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
U.*, u.UWAPermit as UWAPermit1
FROM UWAPermit U
 where 
U.UWAPermit = @UWAPermit 


GO