
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTransportByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTransportByAirportID]
GO


CREATE PROCEDURE [dbo].[spGetTransportByAirportID](@CustomerID BIGINT, @AirportID BIGINT)        
AS        
-- =============================================        
-- Modified By: Rajitha
-- Create date: 08/01/2015        
-- Description: Get Transport By Airport ID   
-- =============================================        
-- Author: Leela M       
-- Create date: 09/11/2012        
-- Description: Get Transport By Airport ID      
-- =============================================  
-- Altered by Rajesh: 05-May-2015 (Added PostalZipCD in select)      
SET NOCOUNT ON
     
DECLARE @UWACustomerID BIGINT       
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
      
DECLARE @ICAOCD VARCHAR(50)  
DECLARE @UWAAirportId VARCHAR(50) 
DECLARE @CustomAirportID VARCHAR(50)
     
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD      
--DECLARE @TransportICAOID CHAR(4)     
      
--SELECT @TransportICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID      

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD 
  
 SELECT * FROM
( SELECT T.[TransportID]      
  ,T.[AirportID]      
  ,T.[TransportCD]      
  ,T.[CustomerID]      
  ,[IsChoice]      
  ,T.[TransportationVendor]      
  ,[PhoneNum]      
  ,[FaxNum]      
  ,[NegotiatedRate]      
  ,[ContactName]      
  ,[Remarks]      
  ,T.[LastUpdUID]      
  ,T.[LastUpdTS]      
  ,T.[UpdateDT]      
  ,T.[RecordType]      
  ,[SourceID]      
  ,[ControlNum]      
  ,[TollFreePhoneNum]      
  ,[Website]      
  ,(SELECT cast(CASE WHEN [UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter     
  ,T.[UWAMaintFlag]    
  ,T.[UWAID]      
  ,T.[IsInActive]      
  ,T.[IsDeleted]      
  ,T.[UWAUpdates]      
  ,T.[AltBusinessPhone]      
  ,T.[BusinessEmail]      
  ,T.[ContactBusinessPhone]      
  ,T.[CellPhoneNum]      
  ,T.[ContactEmail]      
  ,T.[Addr1]      
  ,T.[Addr2]      
  ,T.[Addr3]      
  ,T.[City]      
  ,T.[StateProvince]      
  ,T.[MetroID]      
  ,T.[CountryID]      
  ,M.MetroCD      
          ,T.IsPassenger    
        ,T.IsCrew    
         ,T.ExchangeRateID      
     ,T.NegotiatedTerms      
     ,T.SundayWorkHours      
     ,T.MondayWorkHours      
     ,T.TuesdayWorkHours      
     ,T.WednesdayWorkHours      
     ,T.ThursdayWorkHours      
     ,T.FridayWorkHours      
     ,T.SaturdayWorkHours                
     ,T.PostalZipCD             
        ,C.CountryCD,ROW_NUMBER() OVER(PARTITION BY [TransportationVendor] order by [TransportID]) as ROW_CNT              
                
 FROM  [Transport] T LEFT OUTER JOIN [metro] M on T.[MetroID] = M.MetroID          
  LEFT OUTER JOIN [Country] C on T.CountryID = C.CountryID        
       
 --inner join [Airport] Ai on T.AirportID = Ai.AirportID       
       
 where     
   
  (((T.AirportID = @CustomAirportID AND T.CustomerID = @CustomerID AND T.IsDeleted = 0) or (T.AirportID  = @UWAAirportId       
  AND  T.CustomerID = @CustomerID AND T.IsDeleted = 0))  OR      
     

 (T.AirportID = @UWAAirportId    
 AND T.CustomerID = @UWACustomerID    
 AND T.IsDeleted = 0    
 AND T.TransportCD NOT IN     
  (SELECT TransportCD FROM Transport    
  WHERE  
  (AirportID  = @CustomAirportID       
  AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId       
  AND  CustomerID = @CustomerID )   
  AND IsDeleted = 0)     
    
 ))    
     
 ) as A WHERE A.ROW_CNT=1
 
 GO
