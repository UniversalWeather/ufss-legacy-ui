/****** Object:  StoredProcedure [dbo].[spUpdateHotel]    Script Date: 09/13/2012 12:38:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateHotel]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateHotel]    Script Date: 09/13/2012 12:38:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateHotel]  
(@HotelID BIGINT,
 @AirportID BIGINT,
 @HotelCD CHAR(4) ,  
 @CustomerID BIGINT ,  
 @IsChoice BIT ,  
 @Name VARCHAR(40),   
 @PhoneNum VARCHAR(25) ,  
 @MilesFromICAO NUMERIC(6,1) ,  
 @HotelRating CHAR(3) ,  
 @FaxNum VARCHAR(25) ,  
 @NegociatedRate NUMERIC(17,2) ,  
 @Addr1 VARCHAR(40) ,  
 @Addr2 VARCHAR(40) ,  
 @Addr3 VARCHAR(40) ,  
 @CityName VARCHAR(30) ,  
 @MetroID BIGINT ,  
 @StateName VARCHAR(25) ,  
 @PostalZipCD VARCHAR(15) ,  
 @CountryID BIGINT ,  
 @ContactName VARCHAR(30) ,  
 @Remarks varchar(max),  
 @IsPassenger BIT ,  
 @IsCrew BIT ,  
 @IsUWAMaINTained BIT,  
 @LatitudeDegree NUMERIC(3,0) ,  
 @LatitudeMinutes NUMERIC(5,1) ,  
 @LatitudeNorthSouth CHAR(1) ,  
 @LongitudeDegrees NUMERIC(3,0) ,  
 @LongitudeMinutes NUMERIC(5,1) ,  
 @LongitudeEastWest CHAR(1) ,  
 @LastUpdUID VARCHAR(30) ,  
 @LastUpdTS DATETIME ,  
 @UpDATEDT DATE ,  
 @RecordType CHAR(1) ,  
 @MinutesFromICAO NUMERIC(6,1) ,  
 @SourceID VARCHAR(12) ,  
 @ControlNum CHAR(5) ,  
 @WebSite VARCHAR(200) ,  
 @UWAMaINTFlag BIT ,  
 @UWAID VARCHAR(20) ,  
 @IsInActive BIT ,  
 @IsDeleted BIT ,  
 @UWAUpDATEs BIT,
 @AltBusinessPhone varchar(25) ,
 @TollFreePhone varchar(25) ,
 @BusinessEmail varchar(250) ,
 @ContactBusinessPhone varchar(25) ,
 @CellPhoneNum varchar(25) ,
 @ContactEmail varchar(250),
 @ExchangeRateID	BIGINT,
@NegotiatedTerms	VARCHAR(800),
@SundayWorkHours	VARCHAR(15),
@MondayWorkHours	VARCHAR(15),
@TuesdayWorkHours	VARCHAR(15),
@WednesdayWorkHours	VARCHAR(15),
@ThursdayWorkHours	VARCHAR(15),
@FridayWorkHours	VARCHAR(15),
@SaturdayWorkHours	VARCHAR(15)
)  
  
-- =============================================  
-- Author: Akhila.A  
-- Create date: 2/5/2012  
-- Description: Update the Hotel information  
-- =============================================  
AS  
BEGIN  
DECLARE @UWACustomerID BIGINT    
  
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output     
      
      
SET NOCOUNT ON        
 DECLARE @ICAOCD VARCHAR(50)
 DECLARE @UWAAirportId VARCHAR(50)    
 select @ICAOCD =  icaoid from Airport where AirportID = @AirportID  
 select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD
IF @UWAMaintFlag = 'True' AND not exists (SELECT CustomerID FROM Hotel WHERE HotelID=@HotelID and (AirportID = @AirportID or AirportID = @UWAAirportId) and CustomerID=@CustomerID)  
  
BEGIN

SET @LastUpdTS = GETUTCDATE() 
EXEC spAddHotel
 @HotelID,
 @AirportID ,
 @HotelCD, 
 @CustomerID ,    
 @IsChoice ,  
 @Name,   
 @PhoneNum ,  
 @MilesFromICAO ,  
 @HotelRating ,  
 @FaxNum ,  
 @NegociatedRate ,  
 @Addr1 ,  
 @Addr2 ,  
 @Addr3 ,  
 @CityName ,  
 @MetroID ,  
 @StateName ,  
 @PostalZipCD ,  
 @CountryID ,  
 @ContactName ,  
 @Remarks ,  
 @IsPassenger ,  
 @IsCrew ,  
 @IsUWAMaintained,  
 @LatitudeDegree ,  
 @LatitudeMinutes ,  
 @LatitudeNorthSouth ,  
 @LongitudeDegrees ,  
 @LongitudeMinutes ,  
 @LongitudeEastWest ,  
 @LastUpdUID ,  
 @LastUpdTS ,  
 @UpdateDT ,  
 @RecordType ,  
 @MinutesFromICAO ,  
 @SourceID ,  
 @ControlNum ,  
 @WebSite ,  
 @UWAMaintFlag,  
 @UWAID ,  
 @IsInActive ,  
 @IsDeleted,  
 @UWAUpdates,
 @AltBusinessPhone,
 @TollFreePhone,
 @BusinessEmail,
 @ContactBusinessPhone,
 @CellPhoneNum,
 @ContactEmail
  ,@ExchangeRateID  
     ,@NegotiatedTerms  
     ,@SundayWorkHours  
     ,@MondayWorkHours  
     ,@TuesdayWorkHours  
     ,@WednesdayWorkHours  
     ,@ThursdayWorkHours  
     ,@FridayWorkHours  
     ,@SaturdayWorkHours             
END  
  
ELSE  
  
BEGIN  
SET NOCOUNT ON 
IF(@IsChoice='true')
   begin
   update Hotel set IsChoice='false'  where CustomerID=@CustomerID  and (AirportID = @AirportID or AirportID = @UWAAirportId)  
   end  
UPDATE [Hotel]  
   SET   
     IsChoice=@IsChoice  
 ,Name=@Name  
 ,PhoneNum=@PhoneNum  
 ,MilesFromICAO=@MilesFromICAO  
 ,HotelRating=@HotelRating  
 ,FaxNum=@FaxNum  
 ,NegociatedRate=@NegociatedRate  
 ,Addr1=@Addr1  
 ,Addr2=@Addr2  
 ,Addr3=@Addr3  
 ,CityName=@CityName  
 ,MetroID=@MetroID  
 ,StateName=@StateName  
 ,PostalZipCD=@PostalZipCD  
 ,CountryID=@CountryID  
 ,ContactName=@ContactName  
 ,Remarks=@Remarks  
 ,IsPassenger=@IsPassenger  
 ,IsCrew=@IsCrew  
 ,IsUWAMaintained=@IsUWAMaintained  
 ,LatitudeDegree=@LatitudeDegree  
 ,LatitudeMinutes=@LatitudeMinutes  
 ,LatitudeNorthSouth=@LatitudeNorthSouth  
 ,LongitudeDegrees=@LongitudeDegrees  
 ,LongitudeMinutes=@LongitudeMinutes  
 ,LongitudeEastWest=@LongitudeEastWest  
 ,LastUpdUID=@LastUpdUID  
 ,LastUpdTS=@LastUpdTS  
 ,UpdateDT=@UpdateDT  
 ,RecordType=@RecordType  
 ,MinutesFromICAO=@MinutesFromICAO  
 ,SourceID=@SourceID  
 ,ControlNum=@ControlNum  
 ,WebSite=@WebSite  
 ,UWAMaintFlag=@UWAMaintFlag  
 ,UWAID=@UWAID  
 ,IsInActive=@IsInActive  
 ,IsDeleted=@IsDeleted   
 ,UWAUpdates=@UWAUpdates 
 ,AltBusinessPhone=@AltBusinessPhone
 ,TollFreePhone=@TollFreePhone
 ,BusinessEmail=@BusinessEmail
 ,ContactBusinessPhone=@ContactBusinessPhone
 ,CellPhoneNum=@CellPhoneNum
 ,ContactEmail= @ContactEmail 
 ,ExchangeRateID  = @ExchangeRateID
     ,NegotiatedTerms = @NegotiatedTerms
     ,SundayWorkHours  = @SundayWorkHours
     ,MondayWorkHours  = @MondayWorkHours  
     ,TuesdayWorkHours  = @TuesdayWorkHours  
     ,WednesdayWorkHours  = @WednesdayWorkHours
     ,ThursdayWorkHours  = @ThursdayWorkHours  
     ,FridayWorkHours  = @FridayWorkHours  
     ,SaturdayWorkHours =@SaturdayWorkHours 
 WHERE HotelID =@HotelID 
 
END  
END
GO


