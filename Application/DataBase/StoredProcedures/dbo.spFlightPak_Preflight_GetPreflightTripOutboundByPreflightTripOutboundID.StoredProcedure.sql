

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightTripOutboundByPreflightTripOutboundID]    Script Date: 09/19/2013 15:21:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightTripOutboundByPreflightTripOutboundID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightTripOutboundByPreflightTripOutboundID]
GO



/****** Object:  StoredProcedure [dbo].[GetPreflightTripOutboundByPreflightTripOutboundID]    Script Date: 09/19/2013 15:21:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create Procedure [dbo].[spFlightPak_Preflight_GetPreflightTripOutboundByPreflightTripOutboundID]
(
@CustomerID BIGINT,
@PreflightTripOutboundID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get GetPreflightTripOutboundBy PreflightTripOutboundID 
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightTripOutbound PC 
 where 
 PC.PreflightTripOutboundID = @PreflightTripOutboundID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 



GO


