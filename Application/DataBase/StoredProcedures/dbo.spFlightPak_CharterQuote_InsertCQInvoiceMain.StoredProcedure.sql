

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQInvoiceMain]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQInvoiceMain
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQInvoiceMain
(
	@CustomerID bigint,
	@CQMainID bigint,
	@QuoteID int,
	@InvoiceNUM int,
	@InvoiceDT date,
	@DispatchNUM varchar(12),
	@CharterCompanyInformation varchar(MAX),
	@CustomerAddressInformation varchar(MAX),
	@InvoiceHeader varchar(MAX),
	@InvoiceFooter varchar(MAX),
	@PageOff numeric(2, 0),
	@IsPrintDetail bit,
	@IsPrintFees bit,
	@IsPrintSum bit,
	@CQMessageID bigint,
	@IsInvoiceAdditionalFeeTax bit,
	@InvoiceAdditionalFeeTax numeric(5, 2),
	@IsDetailTax bit,
	@IsFlightChg bit,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@IsSumTax bit,
	@IsManualTax bit,
	@IsManualDiscount bit,
	@IsPrintQuoteNUM bit
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQInvoiceMainID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQInvoiceMainID OUTPUT 


	
	INSERT INTO [CQInvoiceMain] ([CQInvoiceMainID], [CustomerID], [CQMainID], [QuoteID], [InvoiceNUM], [InvoiceDT], [DispatchNUM], [CharterCompanyInformation], [CustomerAddressInformation], [InvoiceHeader], [InvoiceFooter], [PageOff], [IsPrintDetail], [IsPrintFees], [IsPrintSum], [CQMessageID], [IsInvoiceAdditionalFeeTax], [InvoiceAdditionalFeeTax], [IsDetailTax], [IsFlightChg], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted], 
		IsSumTax, IsManualTax, IsManualDiscount, IsPrintQuoteNUM) 
	VALUES (@CQInvoiceMainID, @CustomerID, @CQMainID, @QuoteID, @InvoiceNUM, @InvoiceDT, @DispatchNUM, @CharterCompanyInformation, @CustomerAddressInformation, @InvoiceHeader, 
	@InvoiceFooter, 
	@PageOff, @IsPrintDetail, @IsPrintFees, @IsPrintSum, @CQMessageID, @IsInvoiceAdditionalFeeTax, @InvoiceAdditionalFeeTax, @IsDetailTax, @IsFlightChg, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted, 
		@IsSumTax, @IsManualTax, @IsManualDiscount, @IsPrintQuoteNUM)
	
	SELECT @CQInvoiceMainID as CQInvoiceMainID
GO

