IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTravelCoordinatorRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTravelCoordinatorRequestor]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllTravelCoordinatorRequestor]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllTravelCoordinatorRequestor](@CustomerID BIGINT)  
as  
  
set nocount on  
if len(@CustomerID) >0  
begin   
 SELECT  
            TravelCoordinatorRequestorID
           ,CustomerID  
           ,TravelCoordinatorID  
           ,PassengerRequestorID 
           ,IsChoice  
           ,LastUpdUID  
           ,LastUpdTS  
           ,IsDeleted  
           ,IsInactive
             
 FROM [TravelCoordinatorRequestor] WHERE CustomerID=@CustomerID  and IsDeleted = 'false'  
end
GO
