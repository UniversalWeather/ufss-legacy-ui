
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllConversionTable]   Script Date: 10/11/2012 00:10:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllConversionTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllConversionTable]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllConversionTable]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_GetAllConversionTable]  
(@CustomerID bigint)  
as  
-- =============================================  
-- Author:Mullai.D     
-- Create date: 12/6/2012  
-- Description: Get the ConversionTable information  
-- =============================================  
set nocount on  
if len(@CustomerID) >0  
begin   
 SELECT [ConversionTableID]  
           ,[CustomerID]  
           ,[HomebaseID]  
           ,[Tenths]  
           ,[StartMinimum]  
           ,[EndMinutes]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted] 
           ,[IsInActive]
 FROM  [ConversionTable]  WHERE [CustomerID]=@CustomerID  and [IsDeleted] = 'false'  
end  