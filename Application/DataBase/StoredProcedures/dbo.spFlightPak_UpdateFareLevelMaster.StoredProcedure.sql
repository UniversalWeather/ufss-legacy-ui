
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateFareLevelMaster]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateFareLevelMaster]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateFareLevelMaster]
go
CREATE PROCEDURE [dbo].[spFlightPak_UpdateFareLevelMaster]
(@FareLevelID	bigint
,@CustomerID	bigint
,@StartDT	date
,@EndDT	date
,@Rate1	numeric(8,4)
,@Rate2	numeric(8,4)
,@Rate3	numeric(8,4)
,@TerminalCHG	numeric(8,4)
,@LastUpdUID	varchar(30)
,@LastUpdTS	datetime
,@IsDeleted	bit
,@IsInActive bit)
-- =============================================
-- Author:Karthikeyan.S
-- Create date: 14/05/2012
-- Description: Update the FareLevel information
-- =============================================
as
begin 
SET NoCOUNT ON
set @LastUpdTS = GETUTCDATE()
	UPDATE [FareLevel] 
	SET --FareLevelID = @FareLevelID
		--,CustomerID = @CustomerID
		StartDT = @StartDT
		,EndDT = @EndDT
		,Rate1 = @Rate1
		,Rate2 = @Rate2
		,Rate3 = @Rate3
		,TerminalCHG = @TerminalCHG
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted 
		,IsInActive = @IsInActive
	WHERE CustomerID = @CustomerID AND FareLevelID = @FareLevelID
END
GO
