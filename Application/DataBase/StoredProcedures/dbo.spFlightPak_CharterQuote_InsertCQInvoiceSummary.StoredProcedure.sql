

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQInvoiceSummary]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQInvoiceSummary
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQInvoiceSummary
(
	@CustomerID bigint,
	@CQInvoiceMainID bigint,
	@InvoiceSummaryTax numeric(5, 2),
	@IsPrintUsage bit,
	@UsageAdjDescription varchar(60),
	@QuoteUsageAdj numeric(17, 2),
	@UsageAdjTotal numeric(17, 2),
	@IsUsageAdjTax bit,
	@IsPrintAdditonalFees bit,
	@DescriptionAdditionalFee varchar(60),
	@AdditionalFees numeric(17, 2),
	@AdditionalFeeTotal numeric(17, 2),
	@IsAdditionalFeesTax bit,
	@IsPrintLandingFees bit,
	@LandingFeeDescription varchar(60),
	@QuoteLandingFee numeric(17, 2),
	@LandingFeeTotal numeric(17, 2),
	@IsLandingFeeTax bit,
	@IsLandingFeeDiscount bit,
	@IsPrintSegmentFee bit,
	@SegmentFeeDescription varchar(60),
	@QuoteSegementFee numeric(17, 2),
	@SegmentFeeTotal numeric(17, 2),
	@IsSegmentFeeTax bit,
	@IsSegmentFeeDiscount bit,
	@IsPrintStdCrew bit,
	@StdCrewRONDescription varchar(60),
	@QuoteStdCrewRON numeric(17, 2),
	@StdCrewRONTotal numeric(17, 2),
	@IsStdCrewRONTax bit,
	@IsStdCrewRONDiscount bit,
	@IsPrintAdditionalCrew bit,
	@AdditionalCrewDescription varchar(60),
	@CQAdditionalCrew numeric(17, 2),
	@AdditionalCrewTotal numeric(17, 2),
	@IsAdditionalCrewTax bit,
	@AdditionalCrewDiscount bit,
	@IsAdditionalCrewRON bit,
	@AdditionalCrewRON varchar(60),
	@AdditionalRONCrew numeric(17, 2),
	@AdditionalCrewRONTotal numeric(17, 2),
	@IsAdditionalCrewRONTax bit,
	@IsAdditionalCrewRONDiscount bit,
	@IsWaitingChg bit,
	@WaitChgdescription varchar(60),
	@QuoteWaitingChg numeric(17, 2),
	@WaitChgTotal numeric(17, 2),
	@IsWaitingChgTax bit,
	@IsWaitingChgDiscount bit,
	@IsPrintFlightChg bit,
	@FlightChgDescription varchar(60),
	@FlightCharge numeric(17, 2),
	@FlightChargeTotal numeric(17, 2),
	@IsFlightChgTax bit,
	@IsPrintSubtotal bit,
	@SubtotalDescription varchar(60),
	@QuoteSubtotal numeric(17, 2),
	@SubtotalTotal numeric(17, 2),
	@IsSubtotalTax bit,
	@IsPrintDiscountAmt bit,
	@DescriptionDiscountAmt varchar(60),
	@QuoteDiscountAmount numeric(17, 2),
	@DiscountAmtTotal numeric(17, 2),
	@IsPrintTaxes bit,
	@TaxesDescription varchar(60),
	@QuoteTaxes numeric(17, 2),
	@TaxesTotal numeric(17, 2),
	@IsInvoiceTax bit,
	@IsPrintInvoice bit,
	@InvoiceDescription varchar(60),
	@QuoteInvoice numeric(17, 2),
	@InvoiceTotal numeric(17, 2),
	@IsPrintPrepay bit,
	@PrepayDescription varchar(60),
	@QuotePrepay numeric(17, 2),
	@PrepayTotal numeric(17, 2),
	@IsPrintRemaingAMT bit,
	@RemainingAmtDescription varchar(60),
	@QuoteRemainingAmt numeric(17, 2),
	@RemainingAmtTotal numeric(17, 2),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@IsFlightChgDiscount BIT
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQInvoiceSummaryID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQInvoiceSummaryID OUTPUT 
	
	INSERT INTO [CQInvoiceSummary] ([CQInvoiceSummaryID], [CustomerID], [CQInvoiceMainID], [InvoiceSummaryTax], [IsPrintUsage], [UsageAdjDescription], [QuoteUsageAdj], [UsageAdjTotal], [IsUsageAdjTax], [IsPrintAdditonalFees], [DescriptionAdditionalFee], [AdditionalFees], [AdditionalFeeTotal], [IsAdditionalFeesTax], [IsPrintLandingFees], [LandingFeeDescription], [QuoteLandingFee], [LandingFeeTotal], [IsLandingFeeTax], [IsLandingFeeDiscount], [IsPrintSegmentFee], [SegmentFeeDescription], [QuoteSegementFee], [SegmentFeeTotal], [IsSegmentFeeTax], [IsSegmentFeeDiscount], [IsPrintStdCrew], [StdCrewRONDescription], [QuoteStdCrewRON], [StdCrewRONTotal], [IsStdCrewRONTax], [IsStdCrewRONDiscount], [IsPrintAdditionalCrew], [AdditionalCrewDescription], [CQAdditionalCrew], [AdditionalCrewTotal], [IsAdditionalCrewTax], [AdditionalCrewDiscount], [IsAdditionalCrewRON], [AdditionalCrewRON], [AdditionalRONCrew], [AdditionalCrewRONTotal], [IsAdditionalCrewRONTax], [IsAdditionalCrewRONDiscount], [IsWaitingChg], [WaitChgdescription], [QuoteWaitingChg], [WaitChgTotal], [IsWaitingChgTax], [IsWaitingChgDiscount], [IsPrintFlightChg], [FlightChgDescription], [FlightCharge], [FlightChargeTotal], [IsFlightChgTax], [IsPrintSubtotal], [SubtotalDescription], [QuoteSubtotal], [SubtotalTotal], [IsSubtotalTax], [IsPrintDiscountAmt], [DescriptionDiscountAmt], [QuoteDiscountAmount], [DiscountAmtTotal], [IsPrintTaxes], [TaxesDescription], [QuoteTaxes], [TaxesTotal], [IsInvoiceTax], [IsPrintInvoice], [InvoiceDescription], [QuoteInvoice], [InvoiceTotal], [IsPrintPrepay], [PrepayDescription], [QuotePrepay], [PrepayTotal], [IsPrintRemaingAMT], [RemainingAmtDescription], [QuoteRemainingAmt], [RemainingAmtTotal], [LastUpdUID], [LastUpdTS], [IsDeleted], IsFlightChgDiscount) 
		VALUES (@CQInvoiceSummaryID, @CustomerID, @CQInvoiceMainID, @InvoiceSummaryTax, @IsPrintUsage, @UsageAdjDescription, @QuoteUsageAdj, @UsageAdjTotal, @IsUsageAdjTax, @IsPrintAdditonalFees, @DescriptionAdditionalFee, @AdditionalFees, @AdditionalFeeTotal, @IsAdditionalFeesTax, @IsPrintLandingFees, @LandingFeeDescription, @QuoteLandingFee, @LandingFeeTotal, @IsLandingFeeTax, @IsLandingFeeDiscount, @IsPrintSegmentFee, @SegmentFeeDescription, @QuoteSegementFee, @SegmentFeeTotal, @IsSegmentFeeTax, @IsSegmentFeeDiscount, @IsPrintStdCrew, @StdCrewRONDescription, @QuoteStdCrewRON, @StdCrewRONTotal, @IsStdCrewRONTax, @IsStdCrewRONDiscount, @IsPrintAdditionalCrew, @AdditionalCrewDescription, @CQAdditionalCrew, @AdditionalCrewTotal, @IsAdditionalCrewTax, @AdditionalCrewDiscount, @IsAdditionalCrewRON, @AdditionalCrewRON, @AdditionalRONCrew, @AdditionalCrewRONTotal, @IsAdditionalCrewRONTax, @IsAdditionalCrewRONDiscount, @IsWaitingChg, @WaitChgdescription, @QuoteWaitingChg, @WaitChgTotal, @IsWaitingChgTax, @IsWaitingChgDiscount, @IsPrintFlightChg, @FlightChgDescription, @FlightCharge, @FlightChargeTotal, @IsFlightChgTax, @IsPrintSubtotal, @SubtotalDescription, @QuoteSubtotal, @SubtotalTotal, @IsSubtotalTax, @IsPrintDiscountAmt, @DescriptionDiscountAmt, @QuoteDiscountAmount, @DiscountAmtTotal, @IsPrintTaxes, @TaxesDescription, @QuoteTaxes, @TaxesTotal, @IsInvoiceTax, @IsPrintInvoice, @InvoiceDescription, @QuoteInvoice, @InvoiceTotal, @IsPrintPrepay, @PrepayDescription, @QuotePrepay, @PrepayTotal, @IsPrintRemaingAMT, @RemainingAmtDescription, @QuoteRemainingAmt, @RemainingAmtTotal, @LastUpdUID, @LastUpdTS, @IsDeleted, @IsFlightChgDiscount);
	
	SELECT @CQInvoiceSummaryID as CQInvoiceSummaryID
GO

