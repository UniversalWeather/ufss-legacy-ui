
/****** Object:  StoredProcedure [dbo].[spFlightpak_GetReportDetail]    Script Date: 08/13/2013 17:33:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_GetReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_GetReportDetail]
GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_GetReportDetail]    Script Date: 08/13/2013 17:33:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightpak_GetReportDetail]
(
@CustomerID BIGINT,
@TripSheetReportHeaderID BIGINT
)
AS 
-- =============================================          
-- Author:Karthikeyan          
-- Create date: 13/08/2013          
-- Description: Get the Report Sheet Detail         
-- ============================================= 
BEGIN


SELECT DISTINCT TripSheetReportHeaderID, ReportNumber, ReportDescription, IsSelected FROM dbo.TripSheetReportDetail 
WHERE CustomerID = @CustomerID AND TripSheetReportHeaderID = @TripSheetReportHeaderID AND IsDeleted = 0
 
 END
GO

