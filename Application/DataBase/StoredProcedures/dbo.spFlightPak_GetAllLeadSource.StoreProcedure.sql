
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllLeadSource]    Script Date: 01/21/2013 13:59:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllLeadSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllLeadSource]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllLeadSource]    Script Date: 01/21/2013 13:59:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[spFlightPak_GetAllLeadSource]      
(      
 @CustomerID  BIGINT  
  
)      
AS      
BEGIN      
SET NOCOUNT ON;      
   
SELECT     
       [LeadSourceID]
      ,[CustomerID]
      ,[LeadSourceCD]
      ,[LeadSourceDescription]
      ,[IsInActive]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
 FROM [LeadSource]    
 
 WHERE LeadSource.CustomerID  = @CustomerID      
 AND ISNULL(LeadSource.IsDeleted,0) = 0    
 order by LeadSourceCD 
 END    
 
GO


