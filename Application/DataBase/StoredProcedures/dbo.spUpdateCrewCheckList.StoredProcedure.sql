
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCrewCheckList]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewCheckList]
go
CREATE procedure [dbo].[spUpdateCrewCheckList]  
(           @CrewCheckID BIGINT
           ,@CustomerID BIGINT  
           ,@CrewCheckCD char(3)  
           ,@CrewChecklistDescription varchar(25)  
           ,@ClientID BIGINT  
           ,@LastUpdUID varchar(30)  
           ,@LastUpdTS datetime  
           ,@IsScheduleCheck bit  
           ,@IsCrewCurrencyPlanner bit  
           ,@IsDeleted bit  
           ,@IsInActive bit
)  
-- =============================================  
-- Author: SelvaKumar  
-- Create date: 11/5/2012  
-- Modified by Mathes on 05-07-12
-- Description: Update the CrewCheckList  
-- =============================================  
  
As  
BEGIN
SET NoCOUNT ON  
  set @LastUpdTS=GETUTCDATE()
 UPDATE  CrewCheckList  
 SET        [CustomerID] =@CustomerID 
           ,[CrewChecklistDescription]=@CrewChecklistDescription  
           ,[ClientID] =@ClientID   
           ,[LastUpdUID]=@LastUpdUID  
           ,[LastUpdTS]=@LastUpdTS  
           ,[IsScheduleCheck]=@IsScheduleCheck   
           ,[IsCrewCurrencyPlanner]=@IsCrewCurrencyPlanner  
           ,[IsDeleted]=@IsDeleted 
           ,[IsInActive]=@IsInActive       
    WHERE   CrewCheckID=@CrewCheckID 
  
END
GO
