

/****** Object:  StoredProcedure [dbo].[spUpdatePassengerInformation]    Script Date: 05/24/2013 15:10:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePassengerInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePassengerInformation]
GO



/****** Object:  StoredProcedure [dbo].[spUpdatePassengerInformation]    Script Date: 05/24/2013 15:10:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spUpdatePassengerInformation] (    
 @PassengerInformationID BIGINT    
 ,@CustomerID BIGINT    
 ,@PassengerInfoCD CHAR(3)    
 ,@PassengerDescription VARCHAR(25)    
 ,@ClientID BIGINT    
 ,@LastUpdUID VARCHAR(30)    
 ,@LastUpdTS DATETIME    
 ,@IsShowOnTrip BIT    
 ,@IsDeleted BIT  
 ,@IsCheckList BIT  
 )    
 -- =============================================    
 -- Author: A.Akhila    
 -- Create date: 3/5/2012    
 -- Description: Update the Passenger information    
 -- =============================================    
AS    
BEGIN    
 SET NOCOUNT ON    
  SET @LastUpdTS = GETUTCDATE()  
 UPDATE PassengerInformation    
 SET [PassengerDescription] = @PassengerDescription    
  ,[ClientID] = @ClientID    
  ,[LastUpdUID] = @LastUpdUID    
  ,[LastUpdTS] = @LastUpdTS    
  ,[IsShowOnTrip] = @IsShowOnTrip    
  ,[IsDeleted] = @IsDeleted   
  ,[IsCheckList] = @IsCheckList
 WHERE [CustomerID] = @CustomerID    
  AND PassengerInformationID = @PassengerInformationID    
    
 BEGIN    
  DECLARE @PassengerCD CHAR(5)    
   ,@PassengerAdditionalInfoID BIGINT    
    
  DECLARE @Passenger_Cursor CURSOR SET @Passenger_Cursor = CURSOR    
  FOR    
  SELECT PassengerRequestorCD    
  FROM Passenger    
  WHERE PassengerRequestorCD IS NOT NULL    
    
  OPEN @Passenger_Cursor    
    
  FETCH NEXT    
  FROM @Passenger_Cursor    
  INTO @PassengerCD    
    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   UPDATE PassengerAdditionalInfo    
   SET ClientID = @ClientID    
    ,LastUpdUID = @LastUpdUID    
    ,LastUptTS = @LastUpdTS    
    ,IsDeleted = @IsDeleted    
   WHERE [CustomerID] = @CustomerID    
    AND PassengerAdditionalInfoID = @PassengerAdditionalInfoID    
    
   FETCH NEXT    
   FROM @Passenger_Cursor    
   INTO @PassengerCD    
  END    
    
  CLOSE @Passenger_Cursor    
    
  DEALLOCATE @Passenger_Cursor    
 END    
END  
GO


