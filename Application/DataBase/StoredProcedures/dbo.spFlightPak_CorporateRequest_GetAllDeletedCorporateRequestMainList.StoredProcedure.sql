/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAllDeletedCorporateRequestMainList]    Script Date: 03/06/2013 16:12:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetAllDeletedCorporateRequestMainList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAllDeletedCorporateRequestMainList]
GO



CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAllDeletedCorporateRequestMainList]   
 (  
  @CustomerID Bigint  
 )  
 AS    
 SET NOCOUNT ON;   
 BEGIN  
      
  
  Declare @Sql varchar(2000)  
  Declare @StatusSql varchar(2000)  
  Declare @StatusEntered bit  
  
  set @Sql =  'SELECT   Distinct 
      CR.CRMainID,  
      CR.CRTripNUM,  
      CR.DispatchNUM,
      CR.EstDepartureDT,    
      '''' as ''Rqstr'',  
      CR.CRMainDescription,  
      CR.CorporateRequestStatus,  
      CR.RequestDT,  
      F.TailNum,
      AR.AircraftCD, 
      CR.AcknowledgementStatus, 
      A.ICAOID AS HomebaseCD,
      PM.TripNUM,
      CR.TripSheetStatus 
      FROM CRMain CR
      INNER JOIN CRLeg PL on CR.CRMainID = PL.CRMainID  
      LEFT JOIN Fleet F on  CR.FleetID = F.FleetID  
      WHERE   
      CR.CustomerID=' +   CAST( @CustomerID  as varchar(20))  
      set @Sql =  @Sql + ' and CR.IsDeleted =1 '   
    
   print @Sql  
    
      create table #tempCRmain  
      (  
      CRMainID bigint,  
      CRTripNUM int,
      DispatchNUM varchar(12),
      EstDepartureDT date,  
      Rqstr varchar(100),  
      TripDescription varchar(40),  
      CorporateRequestStatus char(1),  
      RequestDT date,  
      TailNum char(10),
      AircraftCD varchar(10), 
      AcknowledgementStatus char(1), 
      HomebaseCD  char(4),  
      TripNUM bigint,
      TripSheetStatus char(1)
      )  
         
        
      insert into #tempCRmain Exec ( @Sql)  
        
SELECT
      
		  CRMainID,  
		  CRTripNUM,
		  DispatchNUM,
		  EstDepartureDT,  
		  Rqstr,  
		  TripDescription,  
		  CorporateRequestStatus,  
		  RequestDT,  
		  TailNum,
		  AircraftCD, 
		  AcknowledgementStatus, 
		  HomebaseCD,  
		  TripNUM,
		  TripSheetStatus
	    
FROM #tempCRmain

      union  
     SELECT   
      CR.CRMainID,  
      CR.CRTripNUM,  
      CR.DispatchNUM,   
      CR.EstDepartureDT,      
      P.PassengerRequestorCD as 'Rqstr',  
      CR.CRMainDescription,  
      CR.CorporateRequestStatus,  
      CR.RequestDT,  
      F.TailNum, 
	  AR.AircraftCD,    
      CR.AcknowledgementStatus, 
      A.IcaoID as HomebaseCD ,
	  PM.TripNUM,
      CR.TripSheetStatus 
      FROM CRMain CR
      INNER JOIN Company C on CR.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      LEFT JOIN PreflightMain PM on CR.TripID = PM.TripID  
      LEFT JOIN Fleet F on  CR.FleetID = F.FleetID  
      LEFT JOIN Aircraft AR on F.AircraftID = AR.AircraftID
      LEFT JOIN Passenger P on CR.PassengerRequestorID = P.PassengerRequestorID     
      WHERE 1=2  
        
      DROP TABLE #tempCRmain  
  
 END

GO