
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeletePreflightHotelCrewPassengerList]    Script Date: 05/14/2014 09:42:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_DeletePreflightHotelCrewPassengerList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_DeletePreflightHotelCrewPassengerList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_DeletePreflightHotelCrewPassengerList]    Script Date: 05/14/2014 09:42:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_DeletePreflightHotelCrewPassengerList]
	(	
	    @PreflightHotelCrewPassengerListID bigint,
	    @CustomerID bigint
    )
AS
BEGIN 
SET NOCOUNT ON

Delete from PreflightHotelCrewPassengerList where PreflightHotelCrewPassengerListID = @PreflightHotelCrewPassengerListID 
and CustomerID= @CustomerID

END

GO


