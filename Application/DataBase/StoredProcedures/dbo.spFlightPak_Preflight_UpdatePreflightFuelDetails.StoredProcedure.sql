
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails]    Script Date: 08/21/2013 15:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails]    Script Date: 08/21/2013 15:19:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails]
	(			
		   @TripID bigint,   	  
           @CustomerID bigint,
           @UserSessionID AS uniqueidentifier
    )
AS
BEGIN 
SET NOCOUNT ON

--set @TripID = 1000013
--set @CustomerID  = 10000

	SELECT	
		Distinct 
		PM.TripID,
		PL.DepartICAOID,
		PM.customerID
		--PL.LegID
	INTO #PrelightFuelForAirport 
	FROM PreflightMain PM, PreflightLeg PL
	where PM.TripID = PL.TripID
	and PM.TripID = @TripID and PM.CustomerID = @CustomerID

	delete from dbo.preflightfuel where TripID = @TripID

	insert into dbo.preflightfuel 
		(
		[LegID],
		[CustomerID],
		[AirportID],
		[OrderNUM],
		[FBOName],
		[EffectiveDT],
		[BestPrice],
		[GallonFrom],
		[GallonTo],
		[UnitPrice],
		[FuelVendorID],
		[LastUpdUID],
		[LastUpdTS],
		[LRange],
		[Lprice],
		[LeastPrice],
		[UpdateDTTM],
		[IsDeleted],
		[TripID]
		)
	
	Select 
		null,
		PFA.[CustomerID],
		PFA.DepartICAOID,
		null,
		FF.[FBOName],
		FF.[EffectiveDT],
		null,
		FF.[GallonFrom],
		FF.[GallonTo],
		FF.[UnitPrice],
		FF.[FuelVendorID],
		null,
		FF.[LastUpdTS],
		null,
		null,
		null,
		null,
		0,
		PFA.[TripID]
	 from 
		 FlightpakFuel FF,#PrelightFuelForAirport  PFA
		where
		FF.DepartureICAOID = PFA.DepartICAOID 
		and FF.CustomerID = PFA.CustomerID
		


	SELECT	
		Distinct 
		
		PL.DepartICAOID,
		PM.customerID,
		@UserSessionID as UWAFuelInterfaceRequestID
		--PL.LegID
	INTO #PrelightFuelForAirportBYUserSessionID
	FROM PreflightMain PM, PreflightLeg PL
	where 
	--PM.TripID is null	and 
	PM.CustomerID = @CustomerID and PM.TripID = @TripID
	

		
	update PreflightFuel set 
	TripID = @TripID,
	UWAFuelInterfaceRequestID =	null
	from #PrelightFuelForAirportBYUserSessionID SessionData
	where  SessionData.UWAFuelInterfaceRequestID = @UserSessionID
	and PreflightFuel.AirportID = SessionData.DepartICAOID 
		and PreflightFuel.CustomerID = SessionData.CustomerID
		and TripID is null
	
	
		
		
	Drop table #PrelightFuelForAirport 
	Drop table #PrelightFuelForAirportBYUserSessionID

--Exec [dbo].[spFlightPak_Preflight_UpdatePreflightFuelDetails] 1000013, 10000
End

GO


