
/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_AddCrew]    Script Date: 02/18/2014 11:50:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_AddCrew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_AddCrew]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_AddCrew]    Script Date: 02/18/2014 11:50:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Postflight_AddCrew]  
(  
 @POLogID bigint,  
 @POLegID bigint,  
 @CustomerID bigint,  
 @CrewID bigint,  
 @CrewFirstName varchar(60),  
 @CrewMiddleName varchar(60),  
 @CrewLastName varchar(60),  
 @DutyTYPE char(1),  
 @TakeOffDay numeric(2, 0),  
 @TakeOffNight numeric(2, 0),  
 @LandingDay numeric(2, 0),  
 @LandingNight numeric(2, 0),  
 @ApproachPrecision numeric(2, 0),  
 @ApproachNonPrecision numeric(2, 0),  
 @Instrument numeric(7, 3),  
 @Night numeric(7, 3),  
 @RemainOverNight numeric(3, 0),  
 @BeginningDuty char(5),  
 @DutyEnd char(5),  
 @DutyHours numeric(12, 3),  
 @Specification1 bit,  
 @Sepcification2 bit,  
 @IsOverride bit,  
 @Discount bit,  
 @BlockHours numeric(12, 3),  
 @FlightHours numeric(12, 3),  
 @IsAugmentCrew bit,  
 @IsRemainOverNightOverride bit,  
 @Seat char(1),  
 @LastUpdUID char(30),  
 @LastUpdTS datetime,  
 @OutboundDTTM datetime,  
 @InboundDTTM datetime,  
 @CrewDutyStartTM datetime,  
 @CrewDutyEndTM datetime,  
 @ItsNew bit,  
 @Specification3 numeric(7, 3),  
 @Specification4 numeric(7, 3),  
 @DaysAway numeric(5, 0),  
 @IsDeleted bit,
 @OrderNUM int
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @LastUpdTS = GETUTCDATE()  
 DECLARE @PostflightCrewListID BIGINT  
  
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PostflightCurrentNo',  @PostflightCrewListID OUTPUT  
   
 INSERT INTO [PostflightCrew] ([PostflightCrewListID], [POLogID], [POLegID], [CustomerID], [CrewID], [CrewFirstName], [CrewMiddleName], [CrewLastName], [DutyTYPE], [TakeOffDay], [TakeOffNight], [LandingDay], [LandingNight], [ApproachPrecision], 
 [ApproachNonPrecision], [Instrument], [Night], [RemainOverNight], [BeginningDuty], [DutyEnd], [DutyHours], [Specification1], [Sepcification2], [IsOverride], [Discount], [BlockHours], [FlightHours], [IsAugmentCrew], [IsRemainOverNightOverride], [Seat], [LastUpdUID],
 [LastUpdTS], [OutboundDTTM], [InboundDTTM], [CrewDutyStartTM], [CrewDutyEndTM], [ItsNew], [Specification3], [Specification4], [DaysAway], [IsDeleted],
 [OrderNUM]
 )   
 VALUES (@PostflightCrewListID, @POLogID, @POLegID, @CustomerID, @CrewID, @CrewFirstName, @CrewMiddleName, @CrewLastName, @DutyTYPE, @TakeOffDay, @TakeOffNight, @LandingDay, @LandingNight, @ApproachPrecision, @ApproachNonPrecision, @Instrument, @Night, 
 @RemainOverNight, @BeginningDuty, @DutyEnd, 
 CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END , 
 @Specification1, @Sepcification2, @IsOverride, @Discount,
 
 CASE WHEN @BlockHours <0 THEN 0 ELSE @BlockHours END, 
 CASE WHEN @FlightHours <0 THEN 0 ELSE @FlightHours END , 
  
  @IsAugmentCrew, @IsRemainOverNightOverride, @Seat, @LastUpdUID, @LastUpdTS, @OutboundDTTM, @InboundDTTM, 
 @CrewDutyStartTM, @CrewDutyEndTM, @ItsNew, @Specification3, @Specification4, @DaysAway, @IsDeleted,@OrderNUM);  
  
 SELECT isnull(@PostflightCrewListID,0) as 'PostflightCrewListID'  
END  


GO


