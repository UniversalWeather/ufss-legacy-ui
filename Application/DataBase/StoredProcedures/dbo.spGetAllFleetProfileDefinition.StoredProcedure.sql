
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetProfileDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetProfileDefinition]
GO 
CREATE PROCEDURE [dbo].[spGetAllFleetProfileDefinition]      
(@CustomerID BIGINT)  
AS      
BEGIN      
-- =============================================      
-- Author:  <Mathes>      
-- Create date: <24-07-2012>      
-- Description: <Procedure for fetching all record from Fleet Additional Info table>      
-- =============================================      
SELECT     FleetProfileInfoXRefID, 
           FleetID, 
           CustomerID,
           FleetProfileInformationID, 
           FleetDescription, 
           InformationValue,
           ClientID, 
           LastUpdUID, 
           LastUpdTS, 
           IsPrintable, 
           IsDeleted,
           IsInactive 
FROM       FleetProfileDefinition  
WHERE      CustomerID = @CustomerID   
END
GO
