/*

exec spGetAllFeeScheduleByGroupID 10002, 10002183493

*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFeeScheduleByGroupID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFeeScheduleByGroupID]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetAllFeeScheduleByGroupID](
@CustomerID BIGINT,
@FeeGroupID BIGINT
)    
as    
-- =============================================    
-- Author: Mohan Raja
-- Create date: 28/02/2013    
-- Description: Gets All Fee Schedule by Fee Group ID
-- =============================================    
SET NOCOUNT ON    

IF LEN(@CustomerID) >0    
BEGIN     

	SELECT  fs.FeeScheduleID
		, fs.CustomerID
		, fs.FeeGroupID
		, fs.AccountID
		, isnull(fs.Amount,0) as 'Amount'
		, isnull(fs.IsTaxable,0) as 'IsTaxable'
		, isnull(fs.IsDiscount,0) as 'IsDiscount'
		, isnull(fs.IsInActive,0) as 'IsInActive'
		, fs.LastUpdUID
		, fs.LastUpdTS
		, isnull(fs.IsDeleted,0) as 'IsDeleted'
		, isnull(acc.AccountDescription,'') as 'AccountDescription'
		, isnull(acc.AccountNum,'') as 'AccountNum'
		, isnull(acc.CorpAccountNum,'') as 'CorpAccountNum'
		, acc.IsBilling
		, isnull(acc.Cost,0) as 'Cost'
	FROM  FeeSchedule fs
		INNER JOIN Account acc  ON fs.AccountID = acc.AccountID  
	WHERE fs.CustomerID = @CustomerID  and isnull(fs.IsDeleted,0) = 0
		AND fs.FeeGroupID = @FeeGroupID
	ORDER BY fs.FeeGroupID ASC
    
END    
  
  

GO


