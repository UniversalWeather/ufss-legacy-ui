/****** Object:  StoredProcedure [dbo].[spGetPasswordHistory]    Script Date: 10/14/2015 18:14:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPasswordHistory]
GO

/****** Object:  StoredProcedure [dbo].[spGetPasswordHistory]    Script Date: 10/14/2015 18:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
CREATE PROC [dbo].[spGetPasswordHistory]  
 @UserName varchar(100)  
AS  
BEGIN   
    Declare @Name varchar(30)
    SELECT Top 1 @Name= UserName from UserMaster where EmailId = @UserName          
 SELECT TOP(10)FPPWD FROM UserPassword UPass  
 WHERE UPPER(UPass.UserName)= UPPER(@Name) ORDER BY UPass.ActiveStartDT DESC
END

GO


