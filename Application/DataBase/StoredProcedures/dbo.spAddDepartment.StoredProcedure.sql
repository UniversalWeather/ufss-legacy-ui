

/****** Object:  StoredProcedure [dbo].[spAddDepartment]    Script Date: 05/07/2013 12:39:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddDepartment]
GO



/****** Object:  StoredProcedure [dbo].[spAddDepartment]    Script Date: 05/07/2013 12:39:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddDepartment]  
 (   
 @DepartmentCD VARCHAR(8),  
 @DepartmentName VARCHAR(25),  
 @CustomerID BIGINT,
 @ClientID BIGINT,  
 @LastUpdUID VARCHAR(30),  
 @LastUpdTS DATETIME,  
 @IsInActive BIT,  
 @IsDeleted BIT,
 @DepartPercentage Numeric(7,2)  
)  
-- =============================================  
-- Author:Sujitha.v  
-- Create date: 05/5/2012  
-- Description: Insert the Department information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON  

DECLARE @DepartmentID BIGINT 
 
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @DepartmentID OUTPUT  
SET @LastUpdTS = GETUTCDATE() 
   INSERT INTO Department  
   (
   [DepartmentID],
   [DepartmentCD],  
   [DepartmentName],  
   [CustomerID],  
   [ClientID],  
   [LastUpdUID],  
   [LastUpdTS],  
   [IsInActive],  
   [IsDeleted],
   DepartPercentage)  
   VALUES  
  (@DepartmentID,
   @DepartmentCD,  
   @DepartmentName,
   @CustomerID,    
   @ClientID,  
   @LastUpdUID,  
   @LastUpdTS,  
   @IsInActive,    
   isnull(@IsDeleted,0),
   @DepartPercentage)  
  
END

GO


