IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllPassengerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllPassengerGroup]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllPassengerGroup]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllPassengerGroup](@CustomerID BIGINT)
AS
-- =============================================
-- Author: MohanRaja.C
-- Create date: 10/4/2012
-- Description: Get All Passenger Group Informations
-- =============================================
SET NOCOUNT ON

SELECT [PassengerGroupID]
      ,[CustomerID]
      ,[HomebaseID]
      ,CASE WHEN [PassengerGroupCD] IS NULL THEN '' ELSE LTRIM(RTRIM([PassengerGroupCD])) END AS PassengerGroupCD
      ,[PassengerGroupName]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive
  FROM [PassengerGroup] 
  WHERE 
  [CustomerID] = @CustomerID AND 
  ISNULL(IsDeleted,0) = 0
   Order by PassengerGroupCD 
GO
