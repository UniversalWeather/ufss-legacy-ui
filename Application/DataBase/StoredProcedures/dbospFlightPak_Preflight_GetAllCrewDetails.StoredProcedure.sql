/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllCrewDetails]    Script Date: 01/17/2013 13:58:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllCrewDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllCrewDetails]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllCrewDetails]    Script Date: 01/17/2013 13:58:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--/*** PARAMETERS ***/  
  
--DECLARE @FleetID BIGINT  
--DECLARE @DepartDate DATETIME   
--DECLARE @ArrivalDate DATETIME   
--DECLARE @CustomerID BIGINT  
--DECLARE @CrewGroupID BIGINT   
--DECLARE @HomeBaseID BIGINT  
  
--SET @FleetID = 10001212621  
--SET @CustomerID = 10001  
  
--SET @DepartDate = GETDATE() - 10   
--SET @ArrivalDate = GETDATE() - 5  
--SET @HomeBaseID = 1000177437  
  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, '18-sep-2012' , '12-sep-2012', 10001262978, 1000177437, 10001212621  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, '8-sep-2012' , '3-sep-2012', null, 1000177437, 10001212621, NULL  
-- [dbo].[spFlightPak_Preflight_GetAllCrewDetails]  10001, null, '8-sep-2012' , '3-sep-2012', 0, 1000177437, 10001212621, NULL ,0 

  
--/*** PARAMETERS ***/  
  
  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAllCrewDetails]   
(  
 @CustomerID BIGINT,  
 @ClientID BIGINT,  -- Select Crew members available for a client (if not set)
 @DepartDate DATETIME,	-- UTC/GMT DATE
 @ArrivalDate DATETIME,	-- UTC/GMT DATE
 @CrewGroupID BIGINT,   -- Pick crew members of a particular group. If NULL or 0 is passed all Crew members will be avilable
 @HomeBaseID BIGINT,   -- For getting the Company Profile settings (Aircrafft Bias)
 @FleetID BIGINT,   -- For finding the Aircraft Type
 @ArrivalAirportID BIGINT = NULL, -- For finding visa
 @CurrentTripID  BIGINT,
 @MinDepartDate DATETIME,	-- Minimum UTC/GMT DATE
 @MaxArrivalDate DATETIME,	-- Maximum UTC/GMT DATE
 @AircraftID BIGINT
)  
AS

DECLARE @TodayDate            DATE  
DECLARE @180DaysStartDate     DATE  
DECLARE @90DaysStartDate      DATE   
DECLARE @30DaysStartDate      DATE  
DECLARE @7DaysStartDate       DATE  
DECLARE @MonthStartDate       DATE  

  
DECLARE @CrewConflict TABLE   
(  
  
 CrewID BIGINT,   
 ConflictFlag INT
) 

DECLARE @IsStandby TABLE   
(  
  
 CrewID BIGINT,   
 StandbyFlag INT
)   
  
DECLARE @PostFlightCrewDetails TABLE  
  
(  
 CrewID    BIGINT,  
 LandingDay   NUMERIC(8, 0),  
 LandingNight  NUMERIC(8, 0),   
 TakeOffDay   NUMERIC(8, 0),   
 TakeOffNight  NUMERIC(8, 0),   
 ApproachPrecision NUMERIC(8, 0),              
 Instrument   NUMERIC(14, 3),   
 Day07_BlockHours NUMERIC(14, 3),  
 Day07_FlightHours NUMERIC(14, 3),  
 Day30_BlockHours NUMERIC(14, 3),   
 Day30_FlightHours NUMERIC(14, 3),   
 Day90_BlockHours NUMERIC(14, 3),   
 Day90_FlightHours NUMERIC(14, 3),   
 CalMon_BlockHours NUMERIC(14, 3),  
 CalMon_FlightHours NUMERIC(14, 3)  
)  
  
DECLARE @PostflightSimulatorLogDetails TABLE  
(  
 CrewID    BIGINT,  
 LandingDay   NUMERIC(8, 0),  
 LandingNight  NUMERIC(8, 0),   
 TakeOffDay   NUMERIC(8, 0),   
 TakeOffNight  NUMERIC(8, 0),   
 ApproachPrecision NUMERIC(8, 0),              
 Instrument   NUMERIC(14, 3),   
 Day07_BlockHours NUMERIC(14, 3),  
 Day07_FlightHours NUMERIC(14, 3),  
 Day30_BlockHours NUMERIC(14, 3),   
 Day30_FlightHours NUMERIC(14, 3),   
 Day90_BlockHours NUMERIC(14, 3),   
 Day90_FlightHours NUMERIC(14, 3),   
 CalMon_BlockHours NUMERIC(14, 3),  
 CalMon_FlightHours NUMERIC(14, 3)  
)  
  
DECLARE @AircraftTypeCD VARCHAR(10)   
DECLARE @AircraftDescription VARCHAR(15)   
DECLARE @AircraftTypeID BIGINT  
DECLARE @AircraftBasis INT  


DECLARE @CountryID	BIGINT 
  
SELECT @AircraftBasis = ISNULL(AircraftBasis, 1) -- AircraftBasis: 1=Block Time and 2=FlightTime  
FROM Company WHERE HomebaseID = @HomeBaseID  
  
SET @TodayDate = GETUTCDATE() --CONVERT(DATE, '2010.8.8', 120)  
SET @180DaysStartDate = DATEADD(DAY, -180, @TodayDate)   
SET @90DaysStartDate = DATEADD(DAY, -90, @TodayDate)   
SET @30DaysStartDate = DATEADD(DAY, -30, @TodayDate)   
SET @7DaysStartDate = DATEADD(DAY, -7, @TodayDate)   
SET @MonthStartDate = CONVERT(DATE, CONVERT(VARCHAR, YEAR(@TodayDate)) + '.' + CONVERT(VARCHAR, MONTH(@TodayDate))+ '.' + '01', 120)  

SET @CrewGroupID = ISNULL(@CrewGroupID, 0)
SET @CurrentTripID = ISNULL(@CurrentTripID, 0)
  
SELECT   
  @AircraftTypeCD = A.AircraftCD,   
  @AircraftDescription = A.AircraftDescription,   
  @AircraftTypeID  = A.AircraftID   
FROM Aircraft A
--Fleet F   
--INNER JOIN Aircraft A ON F.AircraftID =  A.AircraftID  
WHERE A.AircraftID=@AircraftID and A.IsDeleted=0
--FleetID = @FleetID  AND F.IsDeleted=0
  
  
  SELECT @CountryID = CountryID FROM Airport WITH (NOLOCK)
  WHERE AirportID = @ArrivalAirportID  
  
  
  INSERT INTO @CrewConflict  
  SELECT   
     
   PRFCRWLST.CrewID,  
   CONVERT(INT, 1) ConflictFlag  
    
  FROM PreflightCrewList PRFCRWLST  
  INNER JOIN PreflightLeg PRFLEG   
  ON PRFCRWLST.LegID = PRFLEG.LegID and PRFCRWLST.CustomerID = PRFLEG.CustomerID  
  INNER JOIN PreflightMain PRFMN  
  ON PRFLEG.TripID = PRFMN.TripID and PRFCRWLST.CustomerID = PRFMN.CustomerID  
    
  WHERE   
  (PRFMN.TripStatus='T' or PRFMN.TripStatus='H')  
  AND PRFCRWLST.IsDeleted = 0   
  AND PRFLEG.IsDeleted = 0  
  AND PRFMN.IsDeleted = 0   
  AND PRFLEG.DepartureGreenwichDTTM <= @ArrivalDate   
  AND PRFLEG.ArrivalGreenwichDTTM >= @DepartDate  
  AND (PRFMN.RecordType = 'T' OR PRFMN.RecordType = 'C')
  AND PRFCRWLST.IsDiscount = 0
    
  AND PRFMN.CustomerID = @CustomerID  
  AND PRFLEG.TripID <> @CurrentTripID
  GROUP BY PRFCRWLST.CrewID  
  
  
  
  INSERT INTO @IsStandby  
  SELECT  
   PRFCRWLST.CrewID,  
   CONVERT(INT, 1) StandbyFlag  
    
  FROM PreflightCrewList PRFCRWLST  
  INNER JOIN PreflightLeg PRFLEG   
  ON PRFCRWLST.LegID = PRFLEG.LegID and PRFCRWLST.CustomerID = PRFLEG.CustomerID  
  INNER JOIN PreflightMain PRFMN  
  ON PRFLEG.TripID = PRFMN.TripID and PRFCRWLST.CustomerID = PRFMN.CustomerID  
  INNER JOIN CrewDutyType CT 
  ON CT.DutyTypeCD = PRFLEG.DutyTYPE  
    
  WHERE   
  --PRFMN.TripStatus ='T' AND
  PRFMN.RecordType='C'  
  AND PRFCRWLST.IsDeleted = 0   
  AND PRFLEG.IsDeleted = 0  
  AND PRFMN.IsDeleted = 0   
  AND  CONVERT(DATE,PRFLEG.DepartureGreenwichDTTM) between CONVERT(DATE,DATEADD(HOUR,-24,@MinDepartDate)) and CONVERT(DATE,@MaxArrivalDate)
   AND CONVERT(DATE,PRFLEG.ArrivalGreenwichDTTM) between CONVERT(DATE,@MinDepartDate) and CONVERT(DATE,@MaxArrivalDate)
  --AND CONVERT(DATE,DATEADD(HOUR,-24,PRFLEG.DepartureGreenwichDTTM)) <= CONVERT(DATE,@MinDepartDate)
  --CONVERT(DATE,@ArrivalDate)
  --AND CONVERT(DATE,PRFLEG.ArrivalGreenwichDTTM) >= CONVERT(DATE,@MaxArrivalDate)
  --CONVERT(DATE,@DepartDate)
    --AND PRFCRWLST.IsDiscount = 0
  AND CT.IsStandby = 1
  AND CT.IsDeleted = 0
    
  AND PRFMN.CustomerID = @CustomerID  
  --AND PRFLEG.TripID <> @CurrentTripID
  GROUP BY PRFCRWLST.CrewID  
  
  
/* START: CALCULATE THE CREW DATA FROM PostflightCrew TABLE ***/  
  
INSERT INTO @PostFlightCrewDetails  
SELECT   
  
            POCRW.CrewID,  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.LandingDay,0)
            ELSE CONVERT(NUMERIC(2, 0), 0)  
            END) LandingDay,   
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.LandingNight,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)  
            END) LandingNight,  
              
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.TakeOffDay,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)   
            END) TakeOffDay,   
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.TakeOffNight,0)  
            ELSE CONVERT(NUMERIC(2, 0), 0)   
            END) TakeOffNight,   
              
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.ApproachPrecision,0) + ISNULL(POCRW.ApproachNonPrecision,0)  
            ELSE CONVERT(NUMERIC(2, 0), 0)  
            END) ApproachPrecision,              
               
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @180DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.Instrument,0)   
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Instrument,   
              
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @7DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.BlockHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Day07_BlockHours,  
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @7DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Day07_FlightHours,  
              
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @30DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.BlockHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day30_BlockHours,  
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @30DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Day30_FlightHours,  
              
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.BlockHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day90_BlockHours,  
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day90_FlightHours,  
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POCRW.BlockHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) CalMon_BlockHours,  
  
            SUM(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN @MonthStartDate AND @TodayDate  
            THEN ISNULL(POCRW.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) CalMon_FlightHours  
                                      
FROM PostflightCrew POCRW  
INNER JOIN  PostflightLeg POL ON POCRW.POLegID = POL.POLegID  
INNER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID
INNER JOIN Fleet F ON POM.FleetID = F.FleetID 
--INNER JOIN Aircraft A ON A.AircraftID = F.AircraftID  
WHERE CONVERT(DATE,POL.ScheduledTM) BETWEEN @180DaysStartDate AND @TodayDate  
AND POL.IsDeleted = 0   
AND POCRW.IsDeleted = 0   
AND POCRW.CustomerID = @CustomerID  
AND F.AircraftID = @AircraftTypeID
--AND C.ClientID = ISNULL(@ClientID, C.ClientID)   
GROUP BY POCRW.CrewID  
  
/* END: CALCULATE THE CREW DATA FROM PostflightCrew TABLE ***/  
  
/* START: CALCULATE THE CREW DATA FROM PostFlightSimulatorLog TABLE ***/  
  
INSERT INTO @PostflightSimulatorLogDetails  
SELECT        
            POSIM.CrewID,     
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.LandingDay,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)   
            END) LandingDay,   
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.LandingNight,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)  
            END) LandingNight,  
              
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.TakeOffDay,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)  
            END) TakeOffDay,   
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.TakeOffNight,0)   
            ELSE CONVERT(NUMERIC(2, 0), 0)   
            END) TakeOffNight,   
              
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.ApproachPrecision,0) +  ISNULL(POSIM.ApproachNonPrecision,0)  
            ELSE CONVERT(NUMERIC(2, 0), 0)   
            END) ApproachPrecision,          
               
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @180DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.Instrument,0)   
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Instrument,   
              
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @7DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Day07_BlockHours,  
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @7DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day07_FlightHours,  
              
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @30DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day30_BlockHours,  
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @30DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)   
            END) Day30_FlightHours,  
              
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day90_BlockHours,  
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) Day90_FlightHours,  
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @90DaysStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) CalMon_BlockHours,  
  
            SUM(CASE WHEN POSIM.SessionDT BETWEEN @MonthStartDate AND @TodayDate  
            THEN ISNULL(POSIM.FlightHours,0)  
            ELSE CONVERT(NUMERIC(7, 3), 0)  
            END) CalMon_FlightHours  
                                      
   
FROM PostflightSimulatorLog POSIM  
  
WHERE   
 POSIM.SessionDT BETWEEN @180DaysStartDate AND @TodayDate  
 AND  POSIM.IsDeleted = 0   
 AND  POSIM.CustomerID = @CustomerID   
 AND POSIM.AircraftID = @AircraftTypeID
 --AND C.ClientID = ISNULL(@ClientID, C.ClientID)   
  
GROUP BY POSIM.CrewID  
/* END: CALCULATE THE CREW DATA FROM PostFlightSimulatorLog TABLE ***/  
  
  
  
  
  
/*** Final Select ***/  
  
IF (@CrewGroupID <> 0 )  
BEGIN   
 SELECT   
  CRW.CrewID,  
  CRW.CrewCD,  
  (RTRIM(ISNULL(CRW.LastName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.FirstName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.MiddleInitial,'')))   
  AS CrewName,   
  --ISNULL(cdt.IsStandby, 0) [Standby],  
  CASE 
   WHEN SB.StandbyFlag IS NOT NULL THEN 1
   ELSE 0
  END [Standby],  
  CASE   
   WHEN CAA.CrewAircraftAssignedID IS NOT NULL THEN 1
   ELSE 0 
  END TailAssigned,  
  @AircraftTypeCD as Actype,  
  CASE 
   WHEN CLFT.ConflictFlag IS NOT NULL THEN 1
   ELSE 0
  END Conflicts,
  --CLFT.ConflictFlag AS Conflicts,  
    
  CASE   
   WHEN CC.CrewID IS NOT NULL THEN 1  
   ELSE 0   
  END ChkList,   
  ISNULL(POC.TakeOffDay,0) + ISNULL(POSL.TakeOffDay,0) Daytkoff,  
  ISNULL(POC.LandingDay,0) + ISNULL(POSL.LandingDay,0) DayLnding,   
  ISNULL(POC.TakeOffNight,0) + ISNULL(POSL.TakeOffNight,0) Nighttkoff,  
  ISNULL(POC.LandingNight,0) + ISNULL(POSL.LandingNight,0) NightLnding,  
  ISNULL(POC.ApproachPrecision,0) + ISNULL(POSL.ApproachPrecision,0) Appr,  
  ISNULL(POC.Instrument, 0) + ISNULL(POSL.Instrument, 0) Instr,  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day07_FlightHours,0) + ISNULL(POSL.Day07_FlightHours,0)   
   ELSE     
     ISNULL(POC.Day07_BlockHours,0) + ISNULL(POSL.Day07_BlockHours,0)   
   END     
  [SevenDays],  
    
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day30_FlightHours,0) + ISNULL(POSL.Day30_FlightHours,0)  
   ELSE     
     ISNULL(POC.Day30_BlockHours,0) + ISNULL(POSL.Day30_BlockHours,0)  
   END     
  [ThirtyDays],  
  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.CalMon_FlightHours,0) + ISNULL(POSL.CalMon_FlightHours,0)  
   ELSE     
     ISNULL(POC.CalMon_BlockHours,0) + ISNULL(POSL.CalMon_BlockHours,0)  
   END     
  CalMon,  
  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day90_FlightHours,0) + ISNULL(POSL.Day90_FlightHours,0)  
   ELSE     
     ISNULL(POC.Day90_BlockHours,0) + ISNULL(POSL.Day90_BlockHours,0)  
   END     
  [NinetyDays],  
  Air.IcaoID AS Home,  
    
  -- CC.IsNoConflictEvent,  
  CC.AlertDT AS Alert,  
  CC.GraceDT AS Grace,  
  @AircraftTypeID  AS AirCraftType,  
  @AircraftDescription AS AirCraftDesc,   
  CC.CrewChecklistDescription AS [Description],  
  CC.DueDT AS Due,   
  '' as Leg,
  '' as Purpose,  
  ((ISNULL(CRW.Addr1,'')) + (ISNULL(CRW.Addr2,'')))  as Street,  
  CRW.PostalZipCD as Postal,  
  CRW.CityName as City,  
  CRW.StateName as [State],  
  CRWPASS.PassportNum as Passport,  
  CRWPASS.PassportID as PassportID,  
  CRWPASSVISA.VisaID as VisaID,  
  CRWPASSVISA.VisaNum as Visa,  
  isnull(CRWPASS.PassportExpiryDT,'') as 'PassportExpiryDT',
  PassportCountry.CountryID as 'PassportCountryID',
  PassportCountry.CountryCD as 'Nation',
  
    case when CRWRATE.IsPilotinCommand =1 then 'P91,' else '' end + 
	+ case when CRWRATE.IsSecondInCommand =1 then 'S91,' else '' end 
	+ case when CRWRATE.IsEngineer =1 then 'E,' else '' end 
	+ case when CRWRATE.IsAttendant =1 then 'A,' else '' end 
	+ case when CRWRATE.IsInstructor =1 then 'I,' else '' end 
	+ case when CRWRATE.IsAttendant121 =1 then 'A121,' else '' end 
	+ case when CRWRATE.IsAttendant125 =1 then 'A125,' else '' end 
	+ case when CRWRATE.IsPIC121 =1 then 'P121,' else '' end 
	+ case when CRWRATE.IsPIC125 =1 then 'P125,' else '' end 
	+ case when CRWRATE.IsSIC121 =1 then 'S121,' else '' end 
	+ case when CRWRATE.IsSIC125 =1 then 'S125,' else '' end
	+ case when CRWRATE.IsQualInType135PIC =1 then 'P135,' else '' end
	+ case when CRWRATE.IsQualInType135SIC =1 then 'P135,' else '' end
   AS CrewRating,  
    
  @CrewGroupID as CrewGroupID,  
--  '' CrewGroupCD,  
  --(isnull(CRW.Notes2,''))  as AddlNotes  
  Convert(varchar(max),CRW.Notes2) as AddlNotes,  
   Convert(varchar(max),CRW.Notes) as Notes,  
  CRW.IsStatus,
  '' as IsDiscontinue  
  
    
 FROM Crew CRW  
  INNER JOIN CrewGroupOrder CGD ON CGD.CrewID = CRW.CrewID AND CGD.CrewGroupID = @CrewGroupID  
  LEFT JOIN   
  (SELECT   
    CRWDTYXREF.CrewID,   
    CRWDTYTP.IsStandby,   
    CRWDTYTP.IsDeleted,   
    CRWDTYTP.CustomerID   
   FROM CrewDutyTypeXRef CRWDTYXREF   
    INNER JOIN CrewDutyType CRWDTYTP  
    ON CRWDTYXREF.DutyTypeID = CRWDTYTP.DutyTypeID   
   WHERE CRWDTYTP.IsDeleted = 0  
  ) CDT ON CRW.CrewID = CRW.CrewID AND CRW.CustomerID = CRW.CustomerID  
    
  LEFT JOIN CrewAircraftAssigned CAA   
   ON CAA.CrewID = CRW.CrewID AND CAA.FleetID = @FleetID AND CAA.IsDeleted = 0 AND CAA.CustomerID = CRW.CustomerID  
    
  LEFT JOIN     
   ( SELECT  CCD.CrewID,  
       CCD.IsNoConflictEvent,  
       CCD.AlertDT,  
       CCD.GraceDT,  
       CCD.AircraftID,  
       CCL.CrewChecklistDescription,  
       CCD.DueDT,   
       CCL.CustomerID,   
       CCL.ClientID   
  
    FROM CrewChecklistDetail CCD  
       INNER JOIN   CrewChecklist CCL   
       ON CCD.CheckListCD= CCL.CrewCheckCD  
       
    WHERE   
    CCD.IsInActive = 0
    AND (CCD.AlertDT <= @DepartDate   
      OR CCD.GraceDT <= @DepartDate   
      OR CCD.AlertDT <= @ArrivalDate   
      OR CCD.GraceDT <= @ArrivalDate  
      OR (CCD.IsOneTimeEvent = 0 AND CCD.IsCompleted = 0)  
      )  
    AND CCD.CustomerID = CCL.CustomerID   AND CCD.IsDeleted=0 AND CCL.IsDeleted=0
    ) CC ON CC.CrewID = CRW.CrewID AND CC.CustomerID = CRW.CustomerID AND CC.ClientID = CRW.ClientID  
      
  LEFT JOIN @PostFlightCrewDetails POC   
  ON POC.CrewID = CRW.CrewID   
    
  LEFT JOIN @PostflightSimulatorLogDetails POSL  
  ON POSL.CrewID = CRW.CrewID   
      
  --LEFT OUTER JOIN Company COM  
  --ON CRW.HomebaseID = COM.HomebaseID and CRW.CustomerID = COM.CustomerID AND COM.IsDeleted = 0   
  
  LEFT OUTER JOIN Airport Air  
  ON CRW.HomebaseID = Air.AirportID  AND Air.IsDeleted = 0     
    
  LEFT OUTER JOIN CrewPassengerPassport CRWPASS  
  ON CRW.CrewID = CRWPASS.CrewID and CRW.CustomerID = CRWPASS.CustomerID  AND CRWPASS.IsDeleted = 0   
    AND CRWPASS.PassportID is not null  
    AND CRWPASS.Choice = 1   
    
  LEFT OUTER JOIN CrewPassengerVisa CRWPASSVISA  
  ON CRW.CrewID = CRWPASSVISA.CrewID and CRW.CustomerID = CRWPASSVISA.CustomerID    
    AND CRWPASSVISA.IsDeleted = 0 AND CRWPASSVISA.VisaID is not null    
       AND CRWPASSVISA.CountryID =   @CountryID 
       
       
  LEFT OUTER JOIN Country as PassportCountry on CRWPASS.CountryID = PassportCountry.CountryID
      
  LEFT OUTER JOIN CrewRating CRWRATE  
  ON CRW.CrewID = CRWRATE.CrewID and CRW.CustomerID = CRWRATE.CustomerID AND CRWRATE.IsDeleted = 0 AND CRWRATE.IsInActive=0 AND CRWRATE.AircraftTypeID = @AircraftTypeID    
    
  LEFT OUTER JOIN @CrewConflict CLFT   
  ON CRW.CrewID = CLFT.CrewID  
  
  LEFT OUTER JOIN @IsStandby SB
  ON CRW.CrewID = SB.CrewID  
    
  WHERE ISNULL(CRW.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0))   
  AND CRW.CustomerID = @CustomerID AND CRW.IsDeleted=0 and CRW.CrewID Not in (Select CrewID  from CrewRating where CustomerID=@CustomerID and IsDeleted = 0 and IsInActive=1 and AircraftTypeID=@AircraftTypeID)
END   
ELSE   
BEGIN  
 SELECT   
  CRW.CrewID,  
  CRW.CrewCD,  
  (RTRIM(ISNULL(CRW.LastName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.FirstName,'')) + ' ' +   
    RTRIM(ISNULL(CRW.MiddleInitial,'')))   
  AS CrewName,   
  --ISNULL(cdt.IsStandby, 0) [Standby],  
  CASE 
   WHEN SB.StandbyFlag IS NOT NULL THEN 1
   ELSE 0
  END [Standby],  
  CASE   
      WHEN CAA.CrewAircraftAssignedID IS NOT NULL THEN 1
   ELSE 0
  END TailAssigned,  
  @AircraftTypeCD as Actype,  
  CASE   
      WHEN CLFT.ConflictFlag IS NOT NULL THEN 1
   ELSE 0
  END Conflicts,
  --CLFT.ConflictFlag AS Conflicts,  
    
  CASE   
   WHEN CC.CrewID IS NOT NULL THEN 1  
   ELSE 0   
  END ChkList,   
  ISNULL(POC.TakeOffDay,0) + ISNULL(POSL.TakeOffDay,0) Daytkoff,  
  ISNULL(POC.LandingDay,0) + ISNULL(POSL.LandingDay,0) DayLnding,   
  ISNULL(POC.TakeOffNight,0) + ISNULL(POSL.TakeOffNight,0) Nighttkoff,  
  ISNULL(POC.LandingNight,0) + ISNULL(POSL.LandingNight,0) NightLnding,  
  ISNULL(POC.ApproachPrecision,0) + ISNULL(POSL.ApproachPrecision,0) Appr,  
  ISNULL(POC.Instrument, 0) + ISNULL(POSL.Instrument, 0) Instr,  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day07_FlightHours,0) + ISNULL(POSL.Day07_FlightHours,0)   
   ELSE     
     ISNULL(POC.Day07_BlockHours,0) + ISNULL(POSL.Day07_BlockHours,0)   
   END     
  [SevenDays],  
    
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day30_FlightHours,0) + ISNULL(POSL.Day30_FlightHours,0)  
   ELSE     
     ISNULL(POC.Day30_BlockHours,0) + ISNULL(POSL.Day30_BlockHours,0)  
   END     
  [ThirtyDays],  
  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.CalMon_FlightHours,0) + ISNULL(POSL.CalMon_FlightHours,0)  
   ELSE     
     ISNULL(POC.CalMon_BlockHours,0) + ISNULL(POSL.CalMon_BlockHours,0)  
   END     
  CalMon,  
  
  CASE   
   WHEN @AircraftBasis = 2 THEN   
     ISNULL(POC.Day90_FlightHours,0) + ISNULL(POSL.Day90_FlightHours,0)  
   ELSE     
     ISNULL(POC.Day90_BlockHours,0) + ISNULL(POSL.Day90_BlockHours,0)  
   END     
  [NinetyDays],  
  Air.IcaoID AS Home,  
    
  -- CC.IsNoConflictEvent,  
  CC.AlertDT AS Alert,  
  CC.GraceDT AS Grace,  
  @AircraftTypeID  AS AirCraftType,  
  @AircraftDescription AS AirCraftDesc,   
  CC.CrewChecklistDescription AS [Description],  
  CC.DueDT AS Due,   
  '' as Leg,
  '' as Purpose,      
  ((ISNULL(CRW.Addr1,'')) + (ISNULL(CRW.Addr2,'')))  as Street,  
  CRW.PostalZipCD as Postal,  
  CRW.CityName as City,  
  CRW.StateName as [State],  
  CRWPASS.PassportNum as Passport,  
  CRWPASS.PassportID as PassportID,  
  CRWPASSVISA.VisaID as VisaID,  
  CRWPASSVISA.VisaNum as Visa,  
  isnull(CRWPASS.PassportExpiryDT,'') as 'PassportExpiryDT',
  PassportCountry.CountryID as 'PassportCountryID',
  PassportCountry.CountryCD as 'Nation',
  
    
  --CASE WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0 AND ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND ISNULL(CRWRATE.IsSecondInCommand,0) = 0 THEN ''  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,S135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND  ISNULL(CRWRATE.IsPilotinCommand,0) = 1  AND ISNULL(CRWRATE.IsSecondInCommand,0) = 1 THEN 'P91,S91,P135'
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1  AND ISNULL(CRWRATE.IsQualInType135SIC,0) = 1 THEN 'P135,S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'P91,S91'      
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) = 1 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 0  THEN 'P135'  
  -- WHEN ISNULL(CRWRATE.IsQualInType135PIC,0) =0 AND  ISNULL(CRWRATE.IsQualInType135SIC,0) = 1  THEN 'S135'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 1 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 0  THEN 'P91'  
  -- WHEN ISNULL(CRWRATE.IsPilotinCommand,0) = 0 AND  ISNULL(CRWRATE.IsSecondInCommand,0) = 1  THEN 'S91'  
  --END  AS CrewRating,   
   case when CRWRATE.IsPilotinCommand =1 then 'P91,' else '' end + 
	+ case when CRWRATE.IsSecondInCommand =1 then 'S91,' else '' end 
	+ case when CRWRATE.IsEngineer =1 then 'E,' else '' end 
	+ case when CRWRATE.IsAttendant =1 then 'A,' else '' end 
	+ case when CRWRATE.IsInstructor =1 then 'I,' else '' end 
	+ case when CRWRATE.IsAttendant121 =1 then 'A121,' else '' end 
	+ case when CRWRATE.IsAttendant125 =1 then 'A125,' else '' end 
	+ case when CRWRATE.IsPIC121 =1 then 'P121,' else '' end 
	+ case when CRWRATE.IsPIC125 =1 then 'P125,' else '' end 
	+ case when CRWRATE.IsSIC121 =1 then 'S121,' else '' end 
	+ case when CRWRATE.IsSIC125 =1 then 'S125,' else '' end
	+ case when CRWRATE.IsQualInType135PIC =1 then 'P135,' else '' end
	+ case when CRWRATE.IsQualInType135SIC =1 then 'P135,' else '' end
   AS CrewRating,  
    
  CONVERT(BIGINT, 0) CrewGroupID,  
--  '' CrewGroupCD,  
  --(isnull(CRW.Notes2,''))  as AddlNotes    
  CONVERT(VARCHAR(MAX), CRW.Notes2) as AddlNotes,  
  Convert(varchar(max),CRW.Notes) as Notes,    
  CRW.IsStatus,
  '' as IsDiscontinue    
    
 FROM Crew CRW  
  LEFT JOIN    
  ( SELECT   
    CRWDTYXREF.CrewID,  
    CRWDTYTP.IsStandby,  
    CRWDTYTP.IsDeleted,  
    CRWDTYTP.CustomerID   
   FROM CrewDutyTypeXRef CRWDTYXREF   
    INNER JOIN CrewDutyType CRWDTYTP  
    ON CRWDTYXREF.DutyTypeID = CRWDTYTP.DutyTypeID   
   WHERE CRWDTYTP.IsDeleted = 0  
  ) CDT ON CRW.CrewID = CRW.CrewID AND CRW.CustomerID = CRW.CustomerID  
    
  LEFT JOIN CrewAircraftAssigned CAA   
   ON CAA.CrewID = CRW.CrewID AND CAA.FleetID = @FleetID AND CAA.IsDeleted = 0 AND CAA.CustomerID = CRW.CustomerID  
    
  LEFT JOIN     
   ( SELECT  CCD.CrewID,  
       CCD.IsNoConflictEvent,  
       CCD.AlertDT,  
       CCD.GraceDT,  
       CCD.AircraftID,  
       CCL.CrewChecklistDescription,  
       CCD.DueDT,   
       CCL.CustomerID,   
       CCL.ClientID   
  
    FROM CrewChecklistDetail CCD  
       INNER JOIN   CrewChecklist CCL   
       ON CCD.CheckListCD= CCL.CrewCheckCD    AND CCD.IsDeleted=0 AND CCL.IsDeleted=0
       
    WHERE   
    CCD.IsInActive = 0  
    AND (CCD.AlertDT <= @DepartDate   
      OR CCD.GraceDT <= @DepartDate   
      OR CCD.AlertDT <= @ArrivalDate   
      OR CCD.GraceDT <= @ArrivalDate  
      OR (CCD.IsOneTimeEvent = 0 AND CCD.IsCompleted = 0)  
      )  
    AND CCD.CustomerID = CCL.CustomerID  
    ) CC ON CC.CrewID = CRW.CrewID AND CC.CustomerID = CRW.CustomerID 
   --AND 
	--CC.ClientID = CRW.ClientID  
   --ISNULL(CC.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0)) 
      
  LEFT JOIN @PostFlightCrewDetails POC   
  ON POC.CrewID = CRW.CrewID   
    
  LEFT JOIN @PostflightSimulatorLogDetails POSL  
  ON POSL.CrewID = CRW.CrewID   
      
  --LEFT OUTER JOIN Company COM  
  --ON CRW.HomebaseID = COM.HomebaseID and CRW.CustomerID = COM.CustomerID AND COM.IsDeleted = 0   
  
  LEFT OUTER JOIN Airport Air  
  ON CRW.HomebaseID = Air.AirportID  AND Air.IsDeleted = 0        
    
  LEFT OUTER JOIN CrewPassengerPassport CRWPASS  
  ON CRW.CrewID = CRWPASS.CrewID and CRW.CustomerID = CRWPASS.CustomerID  AND CRWPASS.IsDeleted = 0  AND CRWPASS.Choice=1 
   AND CRWPASS.PassportID is not null  
    --AND CRWPASS.IsDefaultPassport = 1   
    
  LEFT OUTER JOIN CrewPassengerVisa CRWPASSVISA  
  ON CRW.CrewID = CRWPASSVISA.CrewID and CRW.CustomerID = CRWPASSVISA.CustomerID    
    AND CRWPASSVISA.IsDeleted = 0 AND CRWPASSVISA.VisaID is not null    
    AND CRWPASSVISA.CountryID =   @CountryID
    
  LEFT OUTER JOIN Country as PassportCountry on CRWPASS.CountryID = PassportCountry.CountryID
      
  LEFT OUTER JOIN CrewRating CRWRATE  
  ON CRW.CrewID = CRWRATE.CrewID and CRW.CustomerID = CRWRATE.CustomerID AND CRWRATE.IsDeleted = 0 AND CRWRATE.IsInActive=0 AND CRWRATE.AircraftTypeID = @AircraftTypeID    
    
  LEFT OUTER JOIN @CrewConflict CLFT   
  ON CRW.CrewID = CLFT.CrewID  
  
  LEFT OUTER JOIN @IsStandby SB
  ON CRW.CrewID = SB.CrewID  
    
  WHERE ISNULL(CRW.ClientID,0) = ISNULL(@ClientID, ISNULL(CRW.ClientID, 0))  
  AND CRW.CustomerID = @CustomerID AND CRW.IsDeleted=0 and CRW.CrewID Not in (Select CrewID  from CrewRating where CustomerID=@CustomerID and IsDeleted = 0 and IsInActive=1 and AircraftTypeID=@AircraftTypeID)
  
END










GO


