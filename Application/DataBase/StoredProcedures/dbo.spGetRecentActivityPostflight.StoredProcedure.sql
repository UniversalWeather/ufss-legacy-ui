/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRecentActivityPostflight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRecentActivityPostflight]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spGetRecentActivityPostflight]  
@UserName varchar(30),  
@MaxRecordCount int  
AS  
BEGIN  
 SELECT TOP (@MaxRecordCount)  PostflightMain.*,  
 isnull(RequestorName,'') as 'Requestor',IsNull(preflightMain.TripNUM,0) AS 'TripNum',fleet.TailNum As 'TailNum'  
 FROM PostflightMain   
 LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = PostflightMain.FleetID    
 LEFT OUTER JOIN PreflightMain preflightMain ON preflightMain.TripID = PostflightMain.TripID    
 WHERE PostflightMain.LastUpdUID = @UserName ORDER BY PostflightMain.LastUpdTS DESC  
END

GO
GO


