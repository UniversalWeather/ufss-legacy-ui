/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewDefinition]    Script Date: 01/07/2013 19:53:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetCrewDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetCrewDefinition]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetCrewDefinition]    Script Date: 01/07/2013 19:53:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetCrewDefinition]      
(      
 @CustomerID BIGINT,    
 @CrewID BIGINT    
)      
AS      
BEGIN    
SET NOCOUNT ON;      
SELECT     CrewDefinition.[CrewInfoXRefID]    
           ,CrewDefinition.[CrewID]    
           ,CrewDefinition.[CustomerID]    
           ,CrewDefinition.[CrewInfoID]    
           ,CrewDefinition.[InformationDESC]    
           ,CrewDefinition.[InformationValue]    
           ,CrewDefinition.[LastUpdUID]    
           ,CrewDefinition.[LastUpdTS]    
           ,CrewDefinition.[IsReptFilter]    
           ,CrewDefinition.[IsDeleted]  
           ,CrewDefinition.[IsInActive]      
           ,CrewInformation.CrewInfoCD as CrewInfoCD      
  FROM CrewDefinition as CrewDefinition      
  LEFT OUTER JOIN CrewInformation as CrewInformation on CrewInformation.CrewInfoID = CrewDefinition.CrewInfoID      
WHERE CrewDefinition.CustomerID = @CustomerID AND   
CrewDefinition.CrewID = @CrewID AND CrewDefinition.isdeleted = 0  
order by CrewInfoCD  
END  
GO


