

/****** Object:  StoredProcedure [dbo].[spUpdateSalesPerson]    Script Date: 04/08/2013 19:43:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateSalesPerson]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateSalesPerson]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateSalesPerson]    Script Date: 04/08/2013 19:43:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateSalesPerson]    
 (@SalesPersonID BIGINT,  
 @CustomerID BIGINT,     
 @SalesPersonCD CHAR(5),  
 @FirstName VARCHAR(40),     
 @LastName VARCHAR(40),     
 @MiddleName VARCHAR(40),  
 @PhoneNum VARCHAR(25),        
 @Address1 VARCHAR(60),     
 @Address2 VARCHAR(60),     
 @Address3 VARCHAR(60),     
 @CityName VARCHAR(40),     
 @StateName VARCHAR(15),     
 @PostalZipCD VARCHAR(10),     
 @CountryID BIGINT,        
 @FaxNum VARCHAR(25),     
 @PersonalFaxNum VARCHAR(25),    
 @CellPhoneNum varchar(25),     
 @EmailAddress VARCHAR(200),     
 @PersonalEmailID VARCHAR(100),     
 @Logo VARCHAR(200),     
 @LogoPosition  INT,   
 @Notes VARCHAR(100),     
 @IsInActive BIT,  
 @LastUpdUID VARCHAR(30),     
 @LastUpdTS DATETIME,     
 @IsDeleted BIT,
 @Homebaseid BIGINT          )    
 -- =============================================      
 -- Author: Ramesh. J  
 -- Create date: 15/02/2012      
 -- Description: Update the Sales Person information      
 -- =============================================      
AS  BEGIN      
 SET NOCOUNT ON      
 set @LastUpdTS = GETUTCDATE()      
 UPDATE [SalesPerson]      
 SET  FirstName = @FirstName      
   ,[LastName] = @LastName  
   ,[MiddleName] = @MiddleName  
   ,Address1 = @Address1      
   ,Address2 = @Address2      
   ,Address3 = @Address3      
   ,[CityName] = @CityName  
   ,[StateName] = @StateName      
   ,[PostalZipCD] = @PostalZipCD      
   ,[CountryID] = @CountryID      
   ,[PhoneNum] = @PhoneNum      
   ,[FaxNum] = @FaxNum      
   ,[PersonalFaxNum] = @PersonalFaxNum  
   ,[CellPhoneNUM] = @CellPhoneNum  
   ,[EmailAddress] = @EmailAddress  
   ,[PersonalEmailID] = @PersonalEmailID  
   ,[Logo] = @Logo  
   ,[LogoPosition] = @LogoPosition       
   ,[Notes] = @Notes  
   ,[LastUpdUID] = @LastUpdUID      
   ,[LastUpdTS] = @LastUpdTS      
   ,[IsDeleted] = @IsDeleted      
   ,[SalesPersonCD] = @SalesPersonCD       
   ,IsInActive=@IsInActive
   ,HomeBaseId = @Homebaseid             
 WHERE   [CustomerID] = @CustomerID AND       
   SalesPersonID = @SalesPersonID           
END      
GO


