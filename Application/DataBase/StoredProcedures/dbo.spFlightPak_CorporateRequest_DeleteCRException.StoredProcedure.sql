/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_DeleteCRException]    Script Date: 02/20/2013 17:40:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_DeleteCRException]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRException]
GO


CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRException] (  
	  @CRMainID BIGINT  
	 ,@CRExceptionID BIGINT
 )  
AS  
BEGIN  
 
	 DELETE from [CRException]
	 where CRMainID = @CRMainID and CRExceptionID = @CRExceptionID
	  
 
END
GO