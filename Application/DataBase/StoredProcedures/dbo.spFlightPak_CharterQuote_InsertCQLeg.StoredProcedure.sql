
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQLeg]    Script Date: 02/18/2014 11:08:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQLeg]    Script Date: 02/18/2014 11:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQLeg]
(
	
	@CustomerID bigint,
	@CQMainID bigint,
	@QuoteID int,
	@LegNUM int,
	@LegID int,
	@DAirportID bigint,
	@AAirportID bigint,
	@Distance numeric(5, 0),
	@PowerSetting char(1),
	@TakeoffBIAS numeric(6, 3),
	@LandingBIAS numeric(6, 3),
	@TrueAirSpeed numeric(3, 0),
	@WindsBoeingTable numeric(5, 0),
	@ElapseTM numeric(7, 3),
	@IsDepartureConfirmed bit,
	@IsArrivalConfirmation bit,
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@DepartureGreenwichDTTM datetime,
	@ArrivalGreenwichDTTM datetime,
	@HomeDepartureDTTM datetime,
	@HomeArrivalDTTM datetime,
	@FBOID bigint,
	@FlightPurposeDescription varchar(100),
	@DutyHours numeric(12, 3),
	@RestHours numeric(12, 3),
	@FlightHours numeric(12, 3),
	@Notes varchar(MAX),
	@DutyTYPE numeric(1, 0),
	@CrewDutyRulesID bigint,
	@IsDutyEnd bit,
	@FedAviationRegNUM char(3),
	@WindReliability int,
	@CQOverRide numeric(4, 1),
	@NextLocalDTTM datetime,
	@NextGMTDTTM datetime,
	@NextHomeDTTM datetime,
	@ChargeRate numeric(17, 2),
	@ChargeTotal numeric(17, 2),
	@IsPositioning bit,
	@IsTaxable bit,
	@FeeGroupDescription varchar(100),
	@LegMargin numeric(17, 2),
	@CrewDutyAlert char(3),
	@RemainOverNightCNT int,
	@TaxRate numeric(5, 2),
	@PassengerTotal numeric(4, 0),
	@ReservationAvailable numeric(4, 0),
	@IsPrintable bit,
	@FromDescription varchar(120),
	@ToDescription varchar(120),
	@IsRural bit,
	@CrewNotes varchar(MAX),
	@ClientID bigint,
	@DayRONCNT numeric(2, 0),
	@IsSameDay bit,
	@IsErrorTag bit,
	@OQAAirportID bigint,
	@OQDAirportID bigint,
	@OIAAirportID bigint,
	@OIDAirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@PAXBlockedSeat numeric(4, 0)
)
AS
	SET NOCOUNT OFF;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQLegID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQLegID OUTPUT 	
INSERT INTO [CQLeg] ([CQLegID], [CustomerID], [CQMainID], [QuoteID], [LegNUM], [LegID], [DAirportID], [AAirportID], [Distance], [PowerSetting], [TakeoffBIAS], [LandingBIAS], [TrueAirSpeed], [WindsBoeingTable], [ElapseTM], [IsDepartureConfirmed], [IsArrivalConfirmation], [DepartureDTTMLocal], [ArrivalDTTMLocal], [DepartureGreenwichDTTM], [ArrivalGreenwichDTTM], [HomeDepartureDTTM], [HomeArrivalDTTM], [FBOID], [FlightPurposeDescription], 
[DutyHours], [RestHours], [FlightHours], 
[Notes], [DutyTYPE], [CrewDutyRulesID], [IsDutyEnd], [FedAviationRegNUM], [WindReliability], [CQOverRide], [NextLocalDTTM], [NextGMTDTTM], [NextHomeDTTM], [ChargeRate], [ChargeTotal], [IsPositioning], [IsTaxable], [FeeGroupDescription], [LegMargin], [CrewDutyAlert], [RemainOverNightCNT], [TaxRate], [PassengerTotal], [ReservationAvailable], [IsPrintable], [FromDescription], [ToDescription], [IsRural], [CrewNotes], [ClientID], [DayRONCNT], [IsSameDay], [IsErrorTag], [OQAAirportID], [OQDAirportID], [OIAAirportID], [OIDAirportID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted], [PAXBlockedSeat]) 
VALUES (@CQLegID, @CustomerID, @CQMainID, @QuoteID, @LegNUM, @LegID, @DAirportID, @AAirportID, @Distance, @PowerSetting, @TakeoffBIAS, @LandingBIAS, @TrueAirSpeed, @WindsBoeingTable, @ElapseTM, @IsDepartureConfirmed, @IsArrivalConfirmation, @DepartureDTTMLocal, @ArrivalDTTMLocal, @DepartureGreenwichDTTM, @ArrivalGreenwichDTTM, @HomeDepartureDTTM, @HomeArrivalDTTM, @FBOID, @FlightPurposeDescription
,CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
,CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
,CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END
, @Notes, @DutyTYPE, @CrewDutyRulesID, @IsDutyEnd, @FedAviationRegNUM, @WindReliability, @CQOverRide, @NextLocalDTTM, @NextGMTDTTM, @NextHomeDTTM, @ChargeRate, @ChargeTotal, @IsPositioning, @IsTaxable, @FeeGroupDescription, @LegMargin, @CrewDutyAlert, @RemainOverNightCNT, @TaxRate, @PassengerTotal, @ReservationAvailable, @IsPrintable, @FromDescription, @ToDescription, @IsRural, @CrewNotes, @ClientID, @DayRONCNT, @IsSameDay, @IsErrorTag, @OQAAirportID, @OQDAirportID, @OIAAirportID, @OIDAirportID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted, @PAXBlockedSeat);	
SELECT @CQLegID as 'CQLegID'

GO


