IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_ReportSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_ReportSearch]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spFlightPak_Postflight_ReportSearch]        
(     
 @CustomerID bigint,    
 @ClientID bigint = NULL,    
 @HomeBaseID bigint = NULL,    
 @FleetID bigint = NULL,      
 @IsPersonal bit = NULL,      
 @IsCompleted bit = NULL,      
 @StartDate date = NULL,      
 @EndDate date = NULL,    
 @FromLogNum bigint = NULL,    
 @ToLogNum bigint = NULL    
)        
AS        
BEGIN        
-- =======================================================    
-- 17-05-2013 - Created SP for Dynamic PostFlight Reports   
-- 16-01-2014 - Removed date condition if no date is passed
-- =======================================================      
      
 SET NOCOUNT ON;        
       
 IF IsNull(@IsCompleted,0) = 1      
 BEGIN      
 SELECT       
  POLogID, LogNum, IsNull(preflightMain.TripNUM,0) AS TripID,  IsNull(POMain.DispatchNum,'') AS 'DispatchNum'      
  , fleet.TailNum, POMain.EstDepartureDT, isnull(RequestorName,'') as 'RequestorName'      
  , isnull(POMain.DepartmentDescription,'') as 'DepartmentDescription'      
  , POMain.AuthorizationID, isnull(POMain.POMainDescription,'') as 'POMainDescription'      
  , isnull(IsException,0) as 'IsException', isnull(POMain.IsPersonal,0) as 'IsPersonal'      
  , POMain.HomebaseID, isnull(airport.IcaoID,'') AS 'HomebaseCD'       
  , isnull(POMain.FlightNum,'') as 'FlightNum', isnull(POMain.IsCompleted,0) as 'IsCompleted'      
  , isnull(auth.AuthorizationCD,'') AS 'AuthorizationCD'      
  ,isnull(pax.PassengerRequestorCD,'') AS 'PassengerCD'      
  ,isnull(dep.DepartmentCD,'') AS 'DepartmentCD'      
 FROM PostflightMain POMain      
  LEFT OUTER JOIN PreflightMain preflightMain ON preflightMain.TripID = POMain.TripID      
  LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = POMain.FleetID      
  LEFT OUTER JOIN Company company on POMain.HomeBaseID = company.HomeBaseID      
  LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID      
  LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = POMain.AuthorizationID      
  LEFT OUTER JOIN Passenger pax ON pax.PassengerRequestorID = POMain.PassengerRequestorID      
  LEFT OUTER JOIN Department dep ON dep.DepartmentID = POMain.DepartmentID      
 WHERE      
  POMain.CustomerID = @CustomerID      
  AND (isnull(@ClientID,0) = 0 OR IsNull(POMain.ClientID,0) = IsNull(@ClientID,0))      
  AND (isnull(@HomebaseID,0) = 0 OR IsNull(POMain.HomebaseID,0) = IsNull(@HomebaseID,0))      
  AND (isnull(@FleetID,0) = 0 OR IsNull(POMain.FleetID,0) = IsNull(@FleetID,0))      
  AND (IsNull(@IsPersonal,0) = 0 OR IsNull(POMain.IsPersonal,0) = IsNull(@IsPersonal,0))      
  AND IsNull(POMain.IsCompleted,0) = IsNull(@IsCompleted,0)      
  --AND (isnull(@StartDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM' OR POMain.EstDepartureDT >= @StartDate)      
  --AND (ISNULL(@StartDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM' OR ISNULL(@EndDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM'  OR POMain.EstDepartureDT BETWEEN @StartDate AND @EndDate)      
  AND POMain.EstDepartureDT BETWEEN @StartDate AND @EndDate
  AND (ISNULL(@FromLogNum,0)= 0 OR ISNULL(@ToLogNum,0)= 0 OR POMain.LogNum BETWEEN @FromLogNum AND @ToLogNum)    
  AND isnull(POMain.IsDeleted,0) = 0      
 ORDER BY LogNum       
 END      
 ELSE      
 BEGIN       
 SELECT       
  POLogID, LogNum, IsNull(preflightMain.TripNUM,0) AS TripID,  IsNull(POMain.DispatchNum,'') AS 'DispatchNum'      
  , fleet.TailNum, POMain.EstDepartureDT, isnull(RequestorName,'') as 'RequestorName'      
  , isnull(POMain.DepartmentDescription,'') as 'DepartmentDescription'      
  , POMain.AuthorizationID, isnull(POMain.POMainDescription,'') as 'POMainDescription'      
  , isnull(IsException,0) as 'IsException', isnull(POMain.IsPersonal,0) as 'IsPersonal'      
  , POMain.HomebaseID, isnull(airport.IcaoID,'') AS 'HomebaseCD'       
  , isnull(POMain.FlightNum,'') as 'FlightNum', isnull(POMain.IsCompleted,0) as 'IsCompleted'      
  , isnull(auth.AuthorizationCD,'') AS 'AuthorizationCD'      
  ,isnull(pax.PassengerRequestorCD,'') AS 'PassengerCD'      
  ,isnull(dep.DepartmentCD,'') AS 'DepartmentCD'        
 FROM PostflightMain POMain      
  LEFT OUTER JOIN PreflightMain preflightMain ON preflightMain.TripID = POMain.TripID      
  LEFT OUTER JOIN Fleet fleet ON fleet.FleetID = POMain.FleetID      
  LEFT OUTER JOIN Company company on POMain.HomeBaseID = company.HomeBaseID      
  LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID      
  LEFT OUTER JOIN DepartmentAuthorization auth ON auth.AuthorizationID = POMain.AuthorizationID      
  LEFT OUTER JOIN Passenger pax ON pax.PassengerRequestorID = POMain.PassengerRequestorID      
  LEFT OUTER JOIN Department dep ON dep.DepartmentID = POMain.DepartmentID        
 WHERE      
  POMain.CustomerID = @CustomerID      
  AND (isnull(@ClientID,0) = 0 OR IsNull(POMain.ClientID,0) = IsNull(@ClientID,0))      
  AND (isnull(@HomebaseID,0) = 0 OR IsNull(POMain.HomebaseID,0) = IsNull(@HomebaseID,0))      
  AND (isnull(@FleetID,0) = 0 OR IsNull(POMain.FleetID,0) = IsNull(@FleetID,0))      
  AND (IsNull(@IsPersonal,0) = 0 OR IsNull(POMain.IsPersonal,0) = IsNull(@IsPersonal,0))      
  --AND (IsNull(@IsCompleted,0) = 0 OR  IsNull(POMain.IsCompleted,0) = IsNull(@IsCompleted,0))      
  --AND (isnull(@StartDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM' OR POMain.EstDepartureDT >= @StartDate)      
  --AND (ISNULL(@StartDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM' OR ISNULL(@EndDate,'1/1/0001 12:00:00 AM') = '1/1/0001 12:00:00 AM'  OR POMain.EstDepartureDT BETWEEN @StartDate AND @EndDate)      
  AND POMain.EstDepartureDT BETWEEN @StartDate AND @EndDate
  AND (ISNULL(@FromLogNum,0)= 0 OR ISNULL(@ToLogNum,0)= 0 OR POMain.LogNum BETWEEN @FromLogNum AND @ToLogNum)    
  AND isnull(POMain.IsDeleted,0) = 0      
 ORDER BY LogNum      
 END        
       
        
END 
  


GO


