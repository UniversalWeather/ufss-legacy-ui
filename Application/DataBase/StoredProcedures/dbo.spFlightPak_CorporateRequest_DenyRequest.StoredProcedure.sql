GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_DenyRequest]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_DenyRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_DenyRequest]
GO
CREATE PROCEDURE spFlightPak_CorporateRequest_DenyRequest
(@CRMainID	BIGINT
,@CustomerID	BIGINT
,@LastUpdUID VARCHAR(30))
AS BEGIN
	
	--SELECT CRMainID,CustomerID,CRTripNUM,Approver,TripID,TripStatus,TripSheetStatus,AcknowledgementStatus,
	--	CharterQuoteStatus,CorporateRequestStatus,TravelCoordinatorID,TravelCoordinatorName,TravelCoordinatorPhone,
	--	TravelCoordinatorFax,DispatchNUM,EstDepartureDT,EstArrivalDT,RecordType,FleetID,AircraftID,PassengerRequestorID,
	--	RequestorName,RequestorPhone,DepartmentID,DepartmentDescription,AuthorizationID,AuthorizationDescription,HomebaseID,
	--	ClientID,IsPrivate,CRMainDescription,RequestDT,BeginningGMTDT,EndingGMTDT,IsCrew,IsPassenger,IsAlert,IsAirportAlert,
	--	IsLog,LastUpdUID,LastUpdTS,IsDeleted
	--INTO #tmpCRMain
	--FROM CRMain
	--WHERE CRMainID = @CRMainID

	DECLARE @LastUpdTS DATETIME
	SET @LastUpdTS = GETUTCDATE()

	UPDATE CRMain
	SET	CorporateRequestStatus = 'D',
		LastUpdUID = @LastUpdUID,
		LastUpdTS = @LastUpdTS
	WHERE CRMainID = @CRMainID

END
GO
