
/****** Object:  StoredProcedure [dbo].[spAddExchangeRate]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddExchangeRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddExchangeRate]
go
CREATE Procedure [dbo].[spAddExchangeRate](
	@CustomerID BIGINT ,
	@ExchRateCD VARCHAR(6) ,
	@ExchRateDescription VARCHAR(30) ,
	@FromDT DATE ,
	@ToDT DATE ,
	@ExchRate NUMERIC(10, 5) ,
	@CurrencyID NUMERIC(3, 0) ,
	@LastUpdUID VARCHAR(30) ,
	@LastUpdTS DATETIME ,
	@IsDeleted BIT,
	@IsInActive BIT)
  
-- =============================================
-- Author:Hamsha.S
-- Create date: 02/05/2012
-- Description: Insert the ExchangeRateMaster information
-- =============================================
as
begin 
SET NoCOUNT ON
SET @LastUpdTS = GETUTCDATE() 
DECLARE @ExchangeRateID BIGINT
IF not exists (select CustomerID from ExchangeRate where CustomerID=@CustomerID)
begin
set @CurrencyID = 0
end
else 
begin
select @CurrencyID=( MAX(CurrencyID)+1 ) from ExchangeRate where CustomerID=@CustomerID
if(@CurrencyID is null)
begin
set @CurrencyID = 0
end
end
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @ExchangeRateID OUTPUT

   INSERT INTO  [ExchangeRate]
		(ExchangeRateID 
		,CustomerID 
		,ExchRateCD 
		,ExchRateDescription 
		,FromDT 
		,ToDT
		,ExchRate 
		,CurrencyID 
		,LastUpdUID 
		,LastUpdTS
		,IsDeleted 
		,IsInActive
		)
   VALUES
       (
		@ExchangeRateID
		,@CustomerID 
		,@ExchRateCD 
		,@ExchRateDescription 
		,@FromDT 
		,@ToDT
		,@ExchRate 
		,@CurrencyID 
		,@LastUpdUID 
		,@LastUpdTS
		,@IsDeleted 
		,@IsInActive
       )
END
GO
