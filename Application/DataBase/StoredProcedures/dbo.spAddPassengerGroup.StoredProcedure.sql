
GO
/****** Object:  StoredProcedure [dbo].[spAddPassengerGroup]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddPassengerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddPassengerGroup]
go
CREATE Procedure [dbo].[spAddPassengerGroup]
(--@PassengerGroupID	bigint
@CustomerID	bigint
,@HomebaseID	bigint
,@PassengerGroupCD	char(4)
,@PassengerGroupName	varchar(30)
,@LastUpdUID	varchar(30)
,@LastUpdTS	datetime
,@IsDeleted	bit
,@IsInActive bit)
-- =============================================
-- Author: MohanRaja.C
-- Create date: 10/4/2012
-- Description: Insert the Passenger Group information
-- =============================================
AS
BEGIN 
SET NoCOUNT ON
DECLARE @PassengerGroupID	bigint
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassengerGroupID OUTPUT
set @LastUpdTS= GETUTCDATE()
   INSERT INTO [PassengerGroup]
           ([PassengerGroupID]
           ,[CustomerID]
           ,[HomebaseID]
           ,[PassengerGroupCD]
           ,[PassengerGroupName]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[IsInActive])
     VALUES
           (@PassengerGroupID
			,@CustomerID
			,@HomebaseID
			,@PassengerGroupCD
			,@PassengerGroupName
			,@LastUpdUID
			,@LastUpdTS
			,@IsDeleted
			,@IsInActive)
			
	Select @PassengerGroupID as PassengerGroupID
END
GO
