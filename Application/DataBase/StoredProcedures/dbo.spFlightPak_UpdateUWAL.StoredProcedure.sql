
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateUWAL]    Script Date: 10/25/2012 19:11:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateUWAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateUWAL]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateUWAL]    Script Date: 10/25/2012 19:11:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_UpdateUWAL]
-- ====================================================================  
-- Author:  <Vijay Mahalingam>  
-- Create date: <3rd August 2012>  
-- Description: <Update Trip and Leg level records for TSS submission>  
-- ====================================================================
(
	 @TripID BIGINT
	,@LegNUM INT
	,@LegID BIGINT
	,@DepartICAOID BIGINT
	,@ArriveICAOID BIGINT
	,@LastUpdUID CHAR(30)
	,@LastUptTS datetime
	,@Notes TEXT 
	,@NotamLegSettings NUMERIC 
	,@NotamTYPE NUMERIC 
	,@Pretrip1Location CHAR(8)
	,@Pretrip2Location CHAR(8)
	,@Pretrip3Location CHAR(8)
	,@Pretrip4Location CHAR(8)
	,@Pretrip1DeliveryMethod CHAR(7)
	,@Pretrip2DeliveryMethod CHAR(7)
	,@Pretrip3DeliveryMethod CHAR(7)
	,@Pretrip4DeliveryMethod CHAR(7)
	,@PretripDelivery1Address VARCHAR(50)
	,@PretripDelivery2Address VARCHAR(50)
	,@PretripDelivery3Address VARCHAR(50)
	,@PretripDelivery4Address VARCHAR(50)
	,@Pretrip1Attention VARCHAR(50)
	,@Pretrip2Attention VARCHAR(50)
	,@Pretrip3Attention VARCHAR(50)
	,@Pretrip4Attention VARCHAR(50)
	,@FlightLegSettings NUMERIC
	,@IsFax BIT
	,@FaxDeliveryAddress VARCHAR(50)
	,@FaxAttention VARCHAR(30)
	,@IsEmail BIT
	,@EmailAddress VARCHAR(50)
	,@EmailAttention VARCHAR(30)
	,@IsPhone BIT
	,@PhoneDeliveryAddress VARCHAR(50)
	,@PhoneAttention VARCHAR(30)
	,@IsSITAAIRINC BIT
	,@SITAAIRINCDeliveryAddress VARCHAR(50)
	,@SITAAIRINCDeliveryAttention VARCHAR(30)
	,@FARAllLegs NUMERIC
	,@IsSuppressCrewPaxDetailOnAllLegs BIT
	,@IsOverflightPermitAllLegs BIT 
	,@IsLandingPermitsAllLegs BIT
	,@ClientArrangeLandingPermitNUM VARCHAR(15)
	,@IsHandlingAllLegs BIT 
	,@IsArriveDepartDeclFormsAllLeg BIT 
	,@IsPreflightAllLegs BIT
	,@IsPassengerSecurityAllLegs BIT
	,@IsAircraftSecurityAllLegs BIT
	,@IsPassengerVisaAllLegs BIT
	,@IsCrewVisaAllLegs BIT
	,@IsPassengerHotelAllLegs BIT
	,@IsCrewHotelAllLegs BIT
	,@IsUVAirFuelAllLegs BIT
	,@IsUVAirQuoteRequestAllLegs BIT 
	,@SpecialInstructions TEXT 
	,@TripRequestBy VARCHAR(30)
	--Leg Level
	,@IsUWAArrangeHandling BIT
	,@IsUWAArrangeCustoms BIT
	,@IsHomebaseFlightFollowing BIT
	,@IsWideBodyHandling BIT
	,@IsPassengerSecurity BIT
	,@PassengerSecurityNotes TEXT
	,@IsAircraftSecurity BIT
	,@AircraftSecurityNote TEXT
	,@IsCrewVisa BIT
	,@IsPassengerVisa BIT
	,@IsCatering BIT
	,@CateringDetail TEXT
	,@LandingPermitsArrangement NUMERIC
	,@AirportArrivalSlotArrange NUMERIC
	,@AirportDepartureSlotsArrangement NUMERIC
	,@OverflightPermitsArrangement NUMERIC
	
	,@IsUVAirFuel BIT
	,@IsUVAirFuelQuoteRequired BIT
	,@IsPristRequired BIT	
	,@FuelOnArrivalDeparture NUMERIC
	,@FuelSpecialInstructions TEXT 
	,@IsCustoms	BIT
    ,@IsPaymentCreditForServices BIT
	
)
AS      
BEGIN     
--Update Trip Level
UPDATE UWAL
SET 
	 LastUpdUID = @LastUpdUID
	,LastUptTS = @LastUptTS
	,Notes = @Notes 
	,NotamLegSettings = @NotamLegSettings 
	,NotamTYPE = @NotamTYPE 
	,Pretrip1Location = @Pretrip1Location
	,Pretrip2Location = @Pretrip2Location
	,Pretrip3Location = @Pretrip3Location
	,Pretrip4Location = @Pretrip4Location
	,Pretrip1DeliveryMethod = @Pretrip1DeliveryMethod
	,Pretrip2DeliveryMethod = @Pretrip2DeliveryMethod
	,Pretrip3DeliveryMethod = @Pretrip3DeliveryMethod
	,Pretrip4DeliveryMethod = @Pretrip4DeliveryMethod
	,PretripDelivery1Address = @PretripDelivery1Address
	,PretripDelivery2Address = @PretripDelivery2Address
	,PretripDelivery3Address = @PretripDelivery3Address
	,PretripDelivery4Address = @PretripDelivery4Address
	,Pretrip1Attention = @Pretrip1Attention
	,Pretrip2Attention = @Pretrip2Attention
	,Pretrip3Attention = @Pretrip3Attention
	,Pretrip4Attention = @Pretrip4Attention
	,FlightLegSettings = @FlightLegSettings
	,IsFax = @IsFax
	,FaxDeliveryAddress = @FaxDeliveryAddress
	,FaxAttention = @FaxAttention
	,IsEmail = @IsEmail
	,EmailAddress = @EmailAddress
	,EmailAttention = @EmailAttention
	,IsPhone = @IsPhone
	,PhoneDeliveryAddress = @PhoneDeliveryAddress
	,PhoneAttention = @PhoneAttention
	,IsSITAAIRINC = @IsSITAAIRINC
	,SITAAIRINCDeliveryAddress = @SITAAIRINCDeliveryAddress
	,SITAAIRINCDeliveryAttention = @SITAAIRINCDeliveryAttention
	,FARAllLegs = @FARAllLegs
	,IsSuppressCrewPaxDetailOnAllLegs = @IsSuppressCrewPaxDetailOnAllLegs
	,IsOverflightPermitAllLegs = @IsOverflightPermitAllLegs 
	,IsLandingPermitsAllLegs = @IsLandingPermitsAllLegs
	,ClientArrangeLandingPermitNUM = @ClientArrangeLandingPermitNUM
	,IsHandlingAllLegs = @IsHandlingAllLegs 
	,IsArriveDepartDeclFormsAllLeg = @IsArriveDepartDeclFormsAllLeg 
	,IsPreflightAllLegs = @IsPreflightAllLegs
	,IsPassengerSecurityAllLegs = @IsPassengerSecurityAllLegs
	,IsAircraftSecurityAllLegs = @IsAircraftSecurityAllLegs
	,IsPassengerVisaAllLegs = @IsPassengerVisaAllLegs
	,IsCrewVisaAllLegs = @IsCrewVisaAllLegs
	,IsPassengerHotelAllLegs = @IsPassengerHotelAllLegs
	,IsCrewHotelAllLegs = @IsCrewHotelAllLegs
	,IsUVAirFuelAllLegs = @IsUVAirFuelAllLegs
	,IsUVAirQuoteRequestAllLegs = @IsUVAirQuoteRequestAllLegs 
	,SpecialInstructions = @SpecialInstructions 
	,TripRequestBy = @TripRequestBy
	,IsCustoms=@IsCustoms 
    ,IsPaymentCreditForServices=@IsPaymentCreditForServices
FROM UWAL uwal WHERE uwal.TripID = @TripID 

--Update Leg Level
UPDATE UWAL
SET
	 LegNUM = @LegNUM
	,IsUWAArrangeHandling = @IsUWAArrangeHandling
	,IsUWAArrangeCustoms = @IsUWAArrangeCustoms
	,IsHomebaseFlightFollowing = @IsHomebaseFlightFollowing
	,IsWideBodyHandling = @IsWideBodyHandling
	,IsPassengerSecurity =@IsPassengerSecurity
	,PassengerSecurityNotes = @PassengerSecurityNotes
	,IsAircraftSecurity = @IsAircraftSecurity
	,AircraftSecurityNote = @AircraftSecurityNote
	,IsCrewVisa=@IsCrewVisa
	,IsPassengerVisa=@IsPassengerVisa
	,IsCatering = @IsCatering
	,CateringDetail = @CateringDetail
	,LandingPermitsArrangement = @LandingPermitsArrangement 
	,AirportArrivalSlotArrange = @AirportArrivalSlotArrange
	,AirportDepartureSlotsArrangement = @AirportDepartureSlotsArrangement
	,OverflightPermitsArrangement = @OverflightPermitsArrangement 
	,IsUVAirFuel = @IsUVAirFuel
	,IsUVAirFuelQuoteRequired = @IsUVAirFuelQuoteRequired
	,IsPristRequired = @IsPristRequired
	,FuelOnArrivalDeparture = @FuelOnArrivalDeparture
	,FuelSpecialInstruction = @FuelSpecialInstructions
	,IsCustoms=@IsCustoms 
    ,IsPaymentCreditForServices=@IsPaymentCreditForServices
FROM UWAL uwalLeg WHERE uwalLeg.TripID = @TripID and uwalLeg.LegID =@LegID
END
GO


