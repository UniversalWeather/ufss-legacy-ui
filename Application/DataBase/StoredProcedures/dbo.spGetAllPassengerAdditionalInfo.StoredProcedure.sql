

/****** Object:  StoredProcedure [dbo].[spGetAllPassengerAdditionalInfo]    Script Date: 05/24/2013 15:03:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllPassengerAdditionalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllPassengerAdditionalInfo]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllPassengerAdditionalInfo]    Script Date: 05/24/2013 15:03:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetAllPassengerAdditionalInfo]     
(    
 @CustomerID  BIGINT,    
 @ClientID BIGINT    
 )    
AS        
BEGIN        
-- =============================================        
-- Author:  <Srinivasan. J>        
-- Create date: <11/06/2012>        
-- Description: <Procedure for fetching all record from Passenger Additional Info table>        
-- =============================================       
if(@ClientID>0)    
begin     
SELECT PassengerAdditionalInfoID    
              ,CustomerID         
           ,PassengerRequestorID        
           ,AdditionalINFOCD         
           ,AdditionalINFODescription        
           ,AdditionalINFOValue      
           ,ClientID        
           ,LastUpdUID        
        ,LastUptTS        
          ,IsDeleted  
          ,IsInactive 
          ,[PassengerInformationID]
           ,[IsPrint]    
  FROM [PassengerAdditionalInfo] WHERE CustomerID=@CustomerID and ClientID=@ClientID and ISNULL(IsDeleted, 0) = 0       
    end    
    else    
    begin    
    SELECT PassengerAdditionalInfoID    
              ,CustomerID         
           ,PassengerRequestorID        
           ,AdditionalINFOCD         
           ,AdditionalINFODescription        
           ,AdditionalINFOValue      
           ,ClientID        
           ,LastUpdUID        
        ,LastUptTS        
          ,IsDeleted  
          ,IsInactive 
          ,[PassengerInformationID]
           ,[IsPrint]    
  FROM [PassengerAdditionalInfo] WHERE CustomerID=@CustomerID  and ISNULL(IsDeleted, 0) = 0     
    end    
END
GO


