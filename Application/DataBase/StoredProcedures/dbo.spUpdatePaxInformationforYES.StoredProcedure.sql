

/****** Object:  StoredProcedure [dbo].[spUpdatePaxInformationforYES]    Script Date: 10/30/2013 17:12:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePaxInformationforYES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePaxInformationforYES]
GO



/****** Object:  StoredProcedure [dbo].[spUpdatePaxInformationforYES]    Script Date: 10/30/2013 17:12:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


    
CREATE PROCEDURE [dbo].[spUpdatePaxInformationforYES]            
(            
  @PassengerInformationID BIGINT        
 ,@CustomerID BIGINT        
 ,@PassengerInfoCD CHAR(3)        
 ,@PassengerDescription VARCHAR(25)        
 ,@ClientID BIGINT        
 ,@LastUpdUID VARCHAR(30)        
 ,@LastUpdTS DATETIME        
 ,@IsShowOnTrip BIT        
 ,@IsDeleted BIT       
 ,@IsCheckList BIT          
)            
            
-- =============================================            
-- Author:A.Akhila            
-- Create date: 7/5/2012            
-- Description: Update the Crew Roster Additional Information            
-- =============================================            
            
AS            
BEGIN          
SET NOCOUNT ON          
set @LastUpdTS = GETUTCDATE()     
    
    
       
  UPDATE PassengerInformation      
 SET [PassengerDescription] = @PassengerDescription      
  ,[ClientID] = @ClientID      
  ,[LastUpdUID] = @LastUpdUID      
  ,[LastUpdTS] = @LastUpdTS      
  ,[IsShowOnTrip] = @IsShowOnTrip      
  ,[IsDeleted] = @IsDeleted      
  ,[IsCheckList] = @IsCheckList    
 WHERE [CustomerID] = @CustomerID      
  AND PassengerInformationID = @PassengerInformationID and IsDeleted = 0 
  
DECLARE  @PassengerID BIGINT     
if(@IsCheckList = 0)    
begin    
    --Soft delete the records
    update PassengerAdditionalInfo set IsDeleted = 1 ,lastuptts = @LastUpdTS,LastUpdUID = @LastUpdUID,AdditionalINFODescription = @PassengerDescription,IsInActive = @IsShowOnTrip where PassengerInformationID = @PassengerInformationID and CustomerID = @CustomerID 
    update PassengerCheckListDetail set LastUpdUID = @LastUpdUID,isdeleted = 1 where PassengerAdditionalInfoID = @PassengerInformationID and CustomerID = @CustomerID   
    
    
   -- Cursot to get the values of crew which are not having the specified crewinfoid      
   SELECT DISTINCT PassengerRequestorID FROM Passenger WHERE CustomerID = @CustomerID
          
   DECLARE curTailInfo CURSOR FOR SELECT PassengerRequestorID FROM Passenger WHERE CustomerID = @CustomerID and isdeleted = 0
   OPEN curTailInfo;     
   -- PRINT @@CURSOR_ROWS                
    FETCH NEXT FROM curTailInfo INTO @PassengerID;       
    WHILE @@FETCH_STATUS = 0                
    BEGIN      
    BEGIN              
     
   EXEC spAddPassengerAdditionalInfo NULL,@CustomerID,@PassengerID,@PassengerInfoCD,@PassengerDescription,NULL,@ClientID,@LastUpdUID,@LastUpdTS,0,@PassengerInformationID,0  
   
    END      
     FETCH NEXT FROM curTailInfo INTO  @PassengerID    
   END                 
   CLOSE curTailInfo;                
   DEALLOCATE curTailInfo;     
end    
else    
begin   

   update PassengerAdditionalInfo set IsDeleted = 1, lastuptts = @LastUpdTS,LastUpdUID = @LastUpdUID where PassengerInformationID = @PassengerInformationID   
   
   
   update PassengerCheckListDetail set IsDeleted = 1 ,LastUpdUID = @LastUpdUID where PassengerAdditionalInfoID = @PassengerInformationID    
      
   
    
   -- Cursot to get the values of crew which are not having the specified crewinfoid      
         
   DECLARE curTailInfo CURSOR FOR SELECT PassengerRequestorID FROM Passenger WHERE  CustomerID = @CustomerID and isdeleted = 0   
     
         
       
              
   OPEN curTailInfo;     
                  
   -- PRINT @@CURSOR_ROWS                
   FETCH NEXT FROM curTailInfo INTO @PassengerID;       
    WHILE @@FETCH_STATUS = 0                
    BEGIN      
    BEGIN              
    --INSERT INTO()    
   EXECUTE [spFlightPak_AddPaxCheckListDate]     
   NULL    
  ,@PassengerInformationID    
  ,NULL    
  ,@CustomerID    
  ,@PassengerID    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,@LastUpdUID    
  ,@LastUpdTS    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
  ,NULL    
,NULL    
  ,NULL    
  ,0    
       
    END      
     FETCH NEXT FROM curTailInfo INTO  @PassengerID    
                    
   END                 
   CLOSE curTailInfo;                
   DEALLOCATE curTailInfo;     
end    
END       
      

GO


