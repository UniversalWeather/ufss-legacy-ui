
GO
/****** Object:  StoredProcedure [dbo].[spGetFeeSchedule]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFeeSchedule]
GO

CREATE PROCEDURE spGetFeeSchedule
(@FeeScheduleID BIGINT
,@CustomerID BIGINT
,@IsInActive BIT
,@IsDeleted BIT)
AS BEGIN
	
	SELECT FeeSchedule.FeeScheduleID,
			FeeSchedule.CustomerID,
			FeeSchedule.FeeGroupID,
			FeeSchedule.AccountID,
			FeeSchedule.Amount,
			FeeSchedule.IsTaxable,
			FeeSchedule.IsDiscount,
			FeeSchedule.IsInActive,
			FeeSchedule.LastUpdUID,
			FeeSchedule.LastUpdTS,
			FeeSchedule.IsDeleted,
			Account.AccountNum,
			Account.AccountDescription,
			FeeGroup.FeeGroupCD,
			FeeGroup.FeeGroupDescription
	FROM FeeSchedule
		LEFT OUTER JOIN Account ON Account.AccountID = FeeSchedule.AccountID
		LEFT OUTER JOIN FeeGroup ON FeeGroup.FeeGroupID = FeeSchedule.FeeGroupID
	WHERE FeeSchedule.CustomerID = @CustomerID
			AND FeeSchedule.IsDeleted = @IsDeleted
			AND FeeSchedule.IsInActive = CASE WHEN @IsInActive IS NOT NULL THEN @IsInActive ELSE FeeSchedule.IsInActive END			
			AND FeeSchedule.FeeScheduleID = CASE WHEN @FeeScheduleID <> -1 THEN @FeeScheduleID ELSE FeeSchedule.FeeScheduleID END
	ORDER BY FeeScheduleID ASC

END