/****** Object:  StoredProcedure [dbo].[spGetCrewForClient]    Script Date: 01/07/2013 19:38:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCrewForClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCrewForClient]
GO

/****** Object:  StoredProcedure [dbo].[spGetCrewForClient]    Script Date: 01/07/2013 19:38:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetCrewForClient]        
(@CustomerID BIGINT  
,@ClientID BIGINT)        
AS BEGIN        
 SET NOCOUNT ON;       
 IF(@ClientID >0)      
 BEGIN       
  SELECT CrewID  
    ,CrewCD  
    ,CustomerID  
    ,ClientID  
    ,IsDeleted  
    ,IsInActive
  FROM Crew        
  WHERE CustomerID = @CustomerID AND   
    (IsDeleted IS NULL OR IsDeleted = 0) AND  
    ClientID = @ClientID  
  ORDER BY CrewCD ASC  
 END      
 ELSE      
 BEGIN      
  SELECT CrewID  
    ,CrewCD  
    ,CustomerID  
    ,ClientID  
    ,IsDeleted
    ,IsInActive  
  FROM Crew        
  WHERE CustomerID = @CustomerID AND   
    (IsDeleted IS NULL OR IsDeleted = 0)  
  ORDER BY CrewCD ASC  
 END      
END  
GO


