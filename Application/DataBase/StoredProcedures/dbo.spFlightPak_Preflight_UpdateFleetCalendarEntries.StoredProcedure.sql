/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdateFleetCalendarEntries]    Script Date: 01/10/2013 17:11:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdateFleetCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdateFleetCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdateFleetCalendarEntries]    Script Date: 01/10/2013 17:11:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Veerendra  
-- Create date: 17/7/2012 
-- Description: Update the Fleet Calendar Entry 
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdateFleetCalendarEntries]
(
@FleetID BIGINT,
@CustomerID BIGINT,
@TailNum CHAR(6),
@HomebaseID BIGINT,
@LastUpdUID CHAR(30),
@DepartureDTTMLocal DATETIME,
@ArrivalDTTMLocal DATETIME,
@DepartureGreenwichDTTM DATETIME,
@ArrivalGreenwichDTTM DATETIME,
@HomeDepartureDTTM DATETIME,
@HomeArrivalDTTM DATETIME,
@DepartICAOID BIGINT,
@DutyTYPE CHAR(2),
@Notes TEXT,
@LegId BIGINT,
@TripID BIGINT,
@ClientID BIGINT,
@NoteSec varchar(max)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@ClientID=0)
	begin
	set @ClientID=null
	end
    -- Insert statements for procedure here
 --   DECLARE @CheckTail int
 --   SELECT @CheckTail=Count(*) FROM Fleet WHERE TailNum=@TailNum and CustomerID=@CustomerID and HomebaseID=@HomebaseID
 --   If @CheckTail=0
 --   BEGIN
	--UPDATE Fleet 
	--	SET	TailNum=@TailNum,
	--	HomebaseID=@HomebaseID ,
	--	LastUpdUID=@LastUpdUID ,
	--	LastUpdTS=GETUTCDATE() 
	--WHERE FleetID=@FleetID and CustomerID=@CustomerID
	--END
	UPDATE PreflightLeg 
		SET DepartureDTTMLocal=@DepartureDTTMLocal,
		ArrivalDTTMLocal=@ArrivalDTTMLocal,
		DepartureGreenwichDTTM=@DepartureGreenwichDTTM,
		ArrivalGreenwichDTTM=@ArrivalGreenwichDTTM,
		HomeDepartureDTTM=@HomeDepartureDTTM,
		HomeArrivalDTTM=@HomeArrivalDTTM,
		DepartICAOID=@DepartICAOID,
		ArriveICAOID=@DepartICAOID,
		DutyTYPE=@DutyTYPE,
		ClientID=@ClientID,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=GETUTCDATE()
	WHERE LegID=@LegID and CustomerID=@CustomerID
	--WHERE TripID=(SELECT TripID FROM PreflightMain WHERE FleetID=@FleetID) and CustomerID=@CustomerID

	UPDATE PreflightMain 
		SET Notes=@Notes,
		FleetCalendarNotes=@NoteSec,
		HomebaseID=@HomebaseID,
		ClientID=@ClientID,
		FleetID=@FleetID,
		LastUpdUID=@LastUpdUID,
		LastUpdTS=GETUTCDATE()
	WHERE TripID=@TripID and CustomerID=@CustomerID 
		--WHERE FleetID=@FleetID and CustomerID=@CustomerID
END

GO


