

/****** Object:  StoredProcedure [dbo].[spAddFleetProfileDefinition]    Script Date: 04/23/2013 15:12:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFleetProfileDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFleetProfileDefinition]
GO


/****** Object:  StoredProcedure [dbo].[spAddFleetProfileDefinition]    Script Date: 04/23/2013 15:12:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddFleetProfileDefinition]    
(   
 @CustomerID bigint    
,@FleetID bigint    
,@FleetProfileInformationID BIGINT  
,@FleetDescription varchar(25)    
,@InformationValue varchar(25)    
,@ClientID bigint    
,@LastUpdUID varchar(30)    
,@LastUpdTS datetime    
,@IsPrint bit
,@IsDeleted bit)          
AS        
BEGIN         
SET NOCOUNT ON     
DECLARE @FleetProfileInfoXRefID BIGINT    
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @FleetProfileInfoXRefID OUTPUT    
 set @LastUpdTS =  GETUTCDATE()   
INSERT INTO [FleetProfileDefinition]    
           ([FleetProfileInfoXRefID]    
           ,[CustomerID]    
           ,[FleetID]    
           ,[FleetProfileInformationID]    
           ,[FleetDescription]    
           ,[InformationValue]    
           ,[ClientID]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]
           ,IsPrintable)    
     VALUES    
           (@FleetProfileInfoXRefID    
   ,@CustomerID    
   ,@FleetID    
   ,@FleetProfileInformationID    
   ,@FleetDescription    
   ,@InformationValue    
   ,@ClientID    
   ,@LastUpdUID    
   ,@LastUpdTS    
   ,@IsDeleted
   ,@IsPrint)      
END  
GO


