
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQTransportList]    Script Date: 03/07/2013 17:40:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQTransportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQTransportList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQTransportList]    Script Date: 03/07/2013 17:40:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQTransportList]
(
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@TransportID bigint,
	@CQTransportListDescription varchar(60),
	@PhoneNUM varchar(25),
	@Rate numeric(17, 2),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQTransportListID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQTransportListID OUTPUT 
INSERT INTO [CQTransportList] ([CQTransportListID], [CustomerID], [CQLegID], [QuoteID], [LegID], [RecordType], [TransportID], [CQTransportListDescription], [PhoneNUM], [Rate], [AirportID], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted],[FaxNUM],[Email]) VALUES (@CQTransportListID, @CustomerID, @CQLegID, @QuoteID, @LegID, @RecordType, @TransportID, @CQTransportListDescription, @PhoneNUM, @Rate, @AirportID, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted,@FaxNUM, @Email);
	
SELECT @CQTransportListID as CQTransportListID

GO


