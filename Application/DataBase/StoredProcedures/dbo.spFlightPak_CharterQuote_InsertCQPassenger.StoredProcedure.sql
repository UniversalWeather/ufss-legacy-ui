IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQPassenger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQPassenger]
(	
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@PassengerRequestorID bigint,
	@PassengerName varchar(63),
	@FlightPurposeID bigint,
	@Billing varchar(25),
	@IsNonPassenger bit,
	@IsBlocked bit,
	@OrderNUM int,
	@WaitList char(1),
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@PassportID bigint
)
AS
	SET NOCOUNT ON;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQPassengerID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQPassengerID OUTPUT 
INSERT INTO [CQPassenger] ([CQPassengerID], [CustomerID], [CQLegID], [QuoteID], [LegID], [PassengerRequestorID], [PassengerName], [FlightPurposeID], [Billing], [IsNonPassenger], [IsBlocked], [OrderNUM], [WaitList], [FileNUM], [LastUpdUID], [LastUpdTS], [IsDeleted], [PassportID]) VALUES (@CQPassengerID, @CustomerID, @CQLegID, @QuoteID, @LegID, @PassengerRequestorID, @PassengerName, @FlightPurposeID, @Billing, @IsNonPassenger, @IsBlocked, @OrderNUM, @WaitList, @FileNUM, @LastUpdUID, @LastUpdTS, @IsDeleted, @PassportID);
select @CQPassengerID as 'CQPassengerID'
GO
