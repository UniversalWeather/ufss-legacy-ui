
GO
/****** Object:  StoredProcedure [dbo].[spAddTripManagerCheckListGroup]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddTripManagerCheckListGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddTripManagerCheckListGroup]
go
CREATE procedure [dbo].[spAddTripManagerCheckListGroup]  
 (@CheckGroupCD char(3)  
 ,@CustomerID bigint  
 ,@CheckGroupDescription varchar(40)  
 ,@LastUpdUID varchar(30)  
 ,@LastUpdTS datetime  
 ,@IsDeleted bit
 ,@IsInActive bit)  
-- =============================================================  
-- Author:Prakash Pandian.S  
-- Create date: 12/5/2012  
-- Description: Insert the TripManagerCheckListGroup information  
-- =============================================================  
as  
begin   
  
SET NoCOUNT ON  
DECLARE @CheckGroupID bigint  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CheckGroupID OUTPUT  
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
   INSERT INTO [TripManagerCheckListGroup]  
           ([CheckGroupID]  
           ,[CheckGroupCD]  
           ,[CustomerID]  
           ,[CheckGroupDescription]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]
           ,[IsInActive])  
     VALUES  
           (@CheckGroupID  
   ,@CheckGroupCD  
   ,@CustomerID  
   ,@CheckGroupDescription  
   ,@LastUpdUID  
   ,@LastUpdTS  
   ,@IsDeleted
   ,@IsInActive)  
  
END
GO
