
GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_GetScheduleCalenderPreferences]    Script Date: 10/04/2013 02:02:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Preflight_GetScheduleCalenderPreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Preflight_GetScheduleCalenderPreferences]
GO


GO

/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_GetScheduleCalenderPreferences]    Script Date: 10/04/2013 02:02:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Srdihar Manoharan>  
-- Create date: <01 Oct 2013>  
-- Description: <Get preflight Schedule Calender Preferences based on username and customerid>  
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightpak_Preflight_GetScheduleCalenderPreferences]
(
	@CustomerID bigint,
	@UserName CHAR(30),
	@CalenderType VARCHAR(25)
)  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;    
    -- Insert statements for procedure here  
	SELECT SCPreferenceID,UserName,CustomerID,FleetIDS,CrewIDS,CrewGroupIDS FROM DBO.PreflightScheduleCalenderPreferences WHERE 	
	((@CustomerID IS NULL AND CustomerID IS NULL) AND (@UserName IS NULL AND UserName IS NULL) AND (@CalenderType IS NULL AND CalenderType IS NULL))
	OR (CustomerID = @CustomerID AND UserName = @UserName AND CalenderType = @CalenderType)	
 END 
 

GO


