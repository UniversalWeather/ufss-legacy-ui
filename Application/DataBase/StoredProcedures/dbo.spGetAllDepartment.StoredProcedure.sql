

/****** Object:  StoredProcedure [dbo].[spGetAllDepartment]    Script Date: 05/07/2013 12:07:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllDepartment]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllDepartment]    Script Date: 05/07/2013 12:07:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllDepartment](@CustomerID  BIGINT,@ClientID bigint)        
AS     
BEGIN       
-- =============================================        
-- Author: Sujitha.V        
-- Create date: 5/5/2012        
-- Description: Get All Department Informations        
-- =============================================        
SET NOCOUNT ON        
 IF(@ClientID>0)    
 BEGIN       
SELECT       
  d.DepartmentID,      
  d.[DepartmentCD],       
  d.[DepartmentName],      
  d.[CustomerID],    
  --d.ClientID,    
 ISNULL(NULLIF(d.ClientID,''),'-') as ClientID,        
     
 d.[LastUpdUID],        
 d.[LastUpdTS] ,        
 d.[IsInActive],        
 d.[IsDeleted]  ,      
 ISNULL(NULLIF(c.ClientCD,''),'') as ClientCD , 
 d.DepartPercentage  
 --c.ClientCD      
--FROM  Department d INNER JOIN Client c on d.ClientID = c.ClientID  WHERE d.CustomerID =@CustomerID  AND ISNULL(d.IsDeleted,0) = 0    
FROM  Department d LEFT OUTER JOIN Client c on d.ClientID = c.ClientID  WHERE d.CustomerID =@CustomerID  AND ISNULL(d.IsDeleted,0) = 0 and d.ClientID=@ClientID    
order by DepartmentCD  
END    
ELSE    
BEGIN    
SELECT       
  d.DepartmentID,      
  d.[DepartmentCD],       
  d.[DepartmentName],      
  d.[CustomerID],    
  d.ClientID,    
 --ISNULL(NULLIF(d.ClientID,''),'-') as ClientID,        
     
 d.[LastUpdUID],        
 d.[LastUpdTS] ,        
 d.[IsInActive],        
 d.[IsDeleted]  ,      
 ISNULL(NULLIF(c.ClientCD,''),'') as ClientCD,  
 d.DepartPercentage  
 --c.ClientCD      
--FROM  Department d INNER JOIN Client c on d.ClientID = c.ClientID  WHERE d.CustomerID =@CustomerID  AND ISNULL(d.IsDeleted,0) = 0    
FROM  Department d LEFT OUTER JOIN Client c on d.ClientID = c.ClientID  WHERE d.CustomerID =@CustomerID  AND ISNULL(d.IsDeleted,0) = 0    
order by DepartmentCD  
END    
END
GO


