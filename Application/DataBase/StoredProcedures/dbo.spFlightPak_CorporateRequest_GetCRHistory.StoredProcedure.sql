/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetCRHistory]    Script Date: 04/24/2013 19:47:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetCRHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetCRHistory]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetCRHistory]
@CustomerID bigint,
@CRMainID bigint
AS              
BEGIN            
SET NOCOUNT ON;  
SELECT CRHistoryID,CRMainID,CustomerID,LogisticsHistory,LASTUPDUID,LASTUPDTS,IsDeleted,RevisionNumber
FROM CRHistory
WHERE CustomerID=@CustomerID AND CRMainID=@CRMainID
ORDER BY LASTUPDTS DESC
END 
GO