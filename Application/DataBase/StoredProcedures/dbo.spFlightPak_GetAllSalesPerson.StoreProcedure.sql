

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllSalesPerson]    Script Date: 04/08/2013 19:23:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllSalesPerson]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllSalesPerson]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllSalesPerson]    Script Date: 04/08/2013 19:23:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spFlightPak_GetAllSalesPerson]        
(        
 @CustomerID  BIGINT    
    
)        
AS        
BEGIN        
SET NOCOUNT ON;        
     
SELECT       
       [SalesPersonID]  
      ,[CustomerID]  
      ,CASE WHEN [SalesPersonCD] IS NULL THEN '' ELSE LTRIM(RTRIM([SalesPersonCD])) END AS SalesPersonCD 
      ,[FirstName]  
      ,[LastName]  
      ,[MiddleName]  
      ,[PhoneNUM]  
      ,[Address1]  
      ,[Address2]  
      ,[Address3]  
      ,[CityName]  
      ,[StateName]  
      ,[PostalZipCD]  
      ,[CountryID]  
      ,[FaxNUM]  
      ,[PersonalFaxNum]  
      ,[CellPhoneNUM]  
      ,[EmailAddress]  
      ,[PersonalEmailID]  
      ,[Logo]  
      ,[LogoPosition]  
      ,[Notes]  
      ,[IsInActive]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]
      ,HomeBaseId  
 FROM [SalesPerson]      
   
 WHERE SalesPerson.CustomerID  = @CustomerID        
 AND ISNULL(SalesPerson.IsDeleted,0) = 0      
 order by SalesPersonCD   
 END 
GO


