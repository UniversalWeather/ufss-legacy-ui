
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightCrewsByLeg]    Script Date: 05/14/2013 19:00:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightCrewsByLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightCrewsByLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightCrewsByLeg]    Script Date: 05/14/2013 19:00:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec spFlightPak_Preflight_GetPreflightCrewsByLeg  10003, 100031369130, 0,0,0

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightCrewsByLeg](@CustomerID bigint, @LegID BIGINT,  @IsAllCrew bit = 0, @IsFixedWingOnly bit = 0, @IsRotaryWingOnly bit = 0)
                              
AS
 IF(@IsAllCrew = 1)   -- Get All crew - that is No status filter on crew   
	BEGIN
	
         IF(@IsFixedWingOnly = 1 AND @IsRotaryWingOnly = 1) -- fILTER ON FIXEDWING ONLY
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsFixedWing = 1 AND CR.IsRotaryWing = 1
		   AND CR.CrewCD IS NOT NUll  
		 Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		   
         END
         
         ELSE IF(@IsFixedWingOnly = 1 AND @IsRotaryWingOnly = 0)
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsFixedWing = 1 
		   AND CR.CrewCD IS NOT NUll
		   Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		   
		   
         END
         
          ELSE IF(@IsFixedWingOnly = 0 AND @IsRotaryWingOnly = 1)
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsRotaryWing = 1 
		   AND CR.CrewCD IS NOT NUll
		   Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		   
         END
		
		ELSE -- FIXEDWING =0, ROTARYWING = 0
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.CrewCD IS NOT NUll
		   Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		   
		 END
			
	END    

ELSE --  Get active crew ONLY

  	BEGIN
  	
		BEGIN
		
         IF(@IsFixedWingOnly = 1 AND @IsRotaryWingOnly = 1) -- fILTER ON FIXEDWING ONLY
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsFixedWing = 1 AND CR.IsRotaryWing = 1  
		   AND CR.IsStatus = 1
		   AND CR.CrewCD IS NOT NUll
		   Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		   
         END
         
         ELSE IF(@IsFixedWingOnly = 1 AND @IsRotaryWingOnly = 0)
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsFixedWing = 1 
		    AND CR.IsStatus = 1
		    AND CR.CrewCD IS NOT NUll
		    Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		    
         END
         
          ELSE IF(@IsFixedWingOnly = 0 AND @IsRotaryWingOnly = 1)
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		   AND CR.IsRotaryWing = 1 
		    AND CR.IsStatus = 1
		    AND CR.CrewCD IS NOT NUll
		    Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		    
         END
		
		ELSE -- FIXEDWING =0, ROTARYWING = 0
         
         BEGIN
          SELECT  CR.CrewID, CR.CrewCD , RTRIM(isnull(PC.CrewLastName,''))+ ', ' + RTRIM(isnull(PC.CrewFirstName,'')) + ' ' + RTRIM(isnull(PC.CrewMiddleName,'')) [FullName],
		 CR.IsFixedWing, CR.IsRotaryWing, CR.IsStatus
		 
		 , case 
				when upper(PC.DutyTYPE) = 'P' then 1 
				when upper(PC.DutyTYPE) = 'S' then 2
				when upper(PC.DutyTYPE) = 'A' then 3
				when upper(PC.DutyTYPE) = 'O' then 4
				else 5
				end as 'DutyOrder'
		 ,
		 PC.DutyTYPE
		 
		 FROM PreflightCrewList PC
		 INNER JOIN Crew CR ON CR.CrewID = PC.CrewID AND CR.IsDeleted = 0 AND PC.IsDeleted = 0
		 WHERE PC.CustomerID = @CustomerID
		   AND PC.LegID = @LegID
		    AND CR.IsStatus = 1
		    AND CR.CrewCD IS NOT NUll
		    Order by   DutyOrder,PC.DutyTYPE, CR.CrewCD
		    
		 END
			
	END    
	END

GO


