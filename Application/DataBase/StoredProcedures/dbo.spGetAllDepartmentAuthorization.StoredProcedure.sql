
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllDepartmentAuthorization]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllDepartmentAuthorization]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllDepartmentAuthorization](@CustomerID  BIGINT,@ClientID BIGINT)    
AS    
-- =============================================    
-- Author: Sujitha.V    
-- Create date: 5/5/2012    
-- Description: Get All DepartmentAuthorization Informations    
-- Sample: EXEC spGetAllDepartmentAuthorization 'EDE088D3-DE29-4A95-96AD-1B3EE2E5D87D'    
-- =============================================    
SET NOCOUNT ON    
  IF(@ClientID>0)  
  BEGIN  
SELECT      [AuthorizationID]  
           ,da .[DepartmentID]  
           ,CASE WHEN [AuthorizationCD] IS NULL THEN '' ELSE LTRIM(RTRIM([AuthorizationCD])) END AS AuthorizationCD 
           ,da .[CustomerID]  
           ,da .[DeptAuthDescription]  
           ,da .[ClientID]  
           ,da .[LastUpdUID]  
           ,da .[LastUpdTS]  
           ,da .[IsInActive]  
           ,[AuthorizerPhoneNum]  
           ,da .[IsDeleted]  
           
             
FROM  DepartmentAuthorization da    WHERE da.CustomerID =@CustomerID  AND ISNULL(da.IsDeleted,0) = 0 and da.ClientID=@ClientID  
order by AuthorizationCD
END  
ELSE  
BEGIN  
SELECT      [AuthorizationID]  
           ,da .[DepartmentID]  
           ,CASE WHEN [AuthorizationCD] IS NULL THEN '' ELSE LTRIM(RTRIM([AuthorizationCD])) END AS AuthorizationCD
           ,da .[CustomerID]  
           ,da .[DeptAuthDescription]  
           ,da .[ClientID]  
           ,da .[LastUpdUID]  
           ,da .[LastUpdTS]  
           ,da .[IsInActive]  
           ,[AuthorizerPhoneNum]  
           ,da .[IsDeleted]  
           
             
FROM  DepartmentAuthorization da    WHERE da.CustomerID =@CustomerID  AND ISNULL(da.IsDeleted,0) = 0  
order by AuthorizationCD 
END


GO

