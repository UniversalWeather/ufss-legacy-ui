/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRDispatchNotes]    Script Date: 02/08/2013 18:27:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_UpdateCRDispatchNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRDispatchNotes]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRDispatchNotes]
(
            @CRDispatchNotesID bigint,
            @CustomerID bigint,
            @CRMainID bigint,
            @CRDispatchNotes varchar(max),
            @LastUpdUID varchar(30),
            @LastUpdTS datetime,
            @IsDeleted bit
)
AS
BEGIN 
SET NOCOUNT OFF
UPDATE [dbo].[CRDispatchNotes]
   SET  [CRDispatchNotes] = @CRDispatchNotes
      ,[LastUpdUID] = @LastUpdUID
      ,[LastUpdTS] = @LastUpdTS
      ,[IsDeleted] = @IsDeleted
 WHERE CRMainID=@CRMainID and CustomerID=@CustomerID and CRDispatchNotesID=@CRDispatchNotesID
END



GO


