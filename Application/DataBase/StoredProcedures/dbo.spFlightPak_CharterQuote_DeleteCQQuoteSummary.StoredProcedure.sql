


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQQuoteSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQQuoteSummary]
(
	@CQQuoteSummaryID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQQuoteSummary] WHERE [CQQuoteSummaryID] = @CQQuoteSummaryID AND [CustomerID] = @CustomerID
GO
