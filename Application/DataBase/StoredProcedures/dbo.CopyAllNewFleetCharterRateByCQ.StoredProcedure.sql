

/****** Object:  StoredProcedure [dbo].[CopyAllNewFleetCharterRateByCQ]    Script Date: 04/17/2013 15:57:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CopyAllNewFleetCharterRateByCQ]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CopyAllNewFleetCharterRateByCQ]
GO



/****** Object:  StoredProcedure [dbo].[CopyAllNewFleetCharterRateByCQ]    Script Date: 04/17/2013 15:57:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE procedure [dbo].[CopyAllNewFleetCharterRateByCQ]    
(    
  @CustomerID bigint,    
  @NewAircraftID bigint,    
  @OldAircraftID bigint    
)    
AS     
BEGIN    
    
    DECLARE @FleetID BIGINT    
    CREATE Table #TempFleet    
    (    
     [FleetNewCharterRateID] [bigint] NOT NULL,    
     [CustomerID] [bigint] NULL,    
     [FleetID] [bigint] NULL,    
     [AircraftCD] [char](3) NULL,    
     [OrderNum] [int] NULL,    
     [FleetNewCharterRateDescription] [varchar](40) NULL,    
     [ChargeUnit] [varchar](25) NULL,    
     [NegotiatedChgUnit] [numeric](2, 0) NULL,    
     [BuyDOM] [numeric](17, 2) NULL,    
     [SellDOM] [numeric](17, 2) NULL,    
     [IsTaxDOM] [bit] NULL,    
     [IsDiscountDOM] [bit] NULL,    
     [BuyIntl] [numeric](17, 2) NULL,    
     [SellIntl] [numeric](17, 2) NULL,    
     [IsTaxIntl] [bit] NULL,    
     [IsDiscountIntl] [bit] NULL,    
     [StandardCrewDOM] [numeric](2, 0) NULL,    
     [StandardCrewIntl] [numeric](2, 0) NULL,    
     [MinimumDailyRequirement] [numeric](17, 2) NULL,    
     [YearMade] [char](4) NULL,    
     [ExteriorColor] [varchar](25) NULL,    
     [ColorIntl] [varchar](25) NULL,    
     [IsAFIS] [bit] NULL,    
     [IsUWAData] [bit] NULL,    
     [LastUpdUID] [varchar](30) NULL,    
     [LastUpdTS] [datetime] NULL,    
     [AircraftTypeID] [bigint] NULL,    
     [IsDeleted] [bit] NOT NULL,    
     [IsInActive] [bit] NULL,    
     [DBAircraftFlightChgID] [bigint] NULL,    
     [DSAircraftFlightChgID] [bigint] NULL,    
     [BuyAircraftFlightIntlID] [bigint] NULL,    
     [SellAircraftFlightIntlID] [bigint] NULL,    
     [CQCustomerID] [bigint] NULL    
    )    
    DECLARE @StandardCrewDOM numeric(2,0) 
    DECLARE @StandardCrewIntl numeric(2,0) 
    DECLARE @MinimumDay numeric(17,0) 
    DECLARE @IsTaxDailyAdj bit
    DECLARE @IsTaxLandingFee bit
    DECLARE @MarginalPercentage numeric(7,2)
    DECLARE @LastUpdUID varchar(30)
    
   
  
    select @StandardCrewDOM = isnull(DomesticStdCrewNum,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @StandardCrewIntl = isnull(IntlStdCrewNum,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @MinimumDay = isnull(MinimumDayUseHrs,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @IsTaxDailyAdj = isnull(DailyUsageAdjTax,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @IsTaxLandingFee = isnull(LandingFeeTax,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @MarginalPercentage = isnull(MarginalPercentage,0) from CQCustomer where CQCustomerID = @OldAircraftID
    select @LastUpdUID = isnull(LastUpdUID,0) from CQCustomer where CQCustomerID = @OldAircraftID
    
    EXEC UpadateCharterRateTax 0,0,@IsTaxDailyAdj,@IsTaxLandingFee,@LastUpdUID,NULL,@MinimumDay,@StandardCrewIntl,@StandardCrewDOM,@NewAircraftID,@MarginalPercentage   
    
    INSERT INTO #TempFleet    
    SELECT * FROM FleetNewCharterRate WHERE cqcustomerid=@OldAircraftID    
    
    ---Delete the Fleet chartee rate if exists for new fleet chargerate id     
    DELETE FROM FleetNewCharterRate WHERE  cqcustomerid=@NewAircraftID    
    
    WHILE (SELECT COUNT(*) from #TempFleet) > 0        
     BEGIN    
         
          SELECT TOP 1 @FleetID = FleetNewCharterRateID FROM #TempFleet    
              
          DECLARE @FleetNewCharterRateID BIGINT     
          EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetNewCharterRateID OUTPUT       
              
          INSERT INTO [FleetNewCharterRate] (        
          FleetNewCharterRateID,                
          CustomerID,        
          FleetID,        
          AircraftCD,        
          OrderNum,        
          FleetNewCharterRateDescription,        
          ChargeUnit,        
          NegotiatedChgUnit,        
          BuyDOM,        
          SellDOM,        
          IsTaxDOM,        
          IsDiscountDOM,        
          BuyIntl,        
          SellIntl,        
          IsTaxIntl,        
          IsDiscountIntl,        
          StandardCrewDOM,        
          StandardCrewIntl,        
          MinimumDailyRequirement,        
          YearMade,        
          ExteriorColor,        
          ColorIntl,        
          IsAFIS,        
          IsUWAData,        
          DBAircraftFlightChgID,        
          DSAircraftFlightChgID,        
          BuyAircraftFlightIntlID,        
          SellAircraftFlightIntlID,        
          LastUpdUID,        
          LastUpdTS,        
          AircraftTypeID,    
          CQcustomerId,        
          IsDeleted        
          )     
           SELECT    
          @FleetNewCharterRateID,                
          CustomerID,        
          FleetID,        
          AircraftCD,        
          OrderNum,        
          FleetNewCharterRateDescription,        
          ChargeUnit,        
          NegotiatedChgUnit,        
          BuyDOM,        
          SellDOM,        
          IsTaxDOM,        
          IsDiscountDOM,        
          BuyIntl,        
          SellIntl,        
          IsTaxIntl,        
          IsDiscountIntl,        
          StandardCrewDOM,        
          StandardCrewIntl,        
          MinimumDailyRequirement,        
          YearMade,        
          ExteriorColor,        
          ColorIntl,        
          IsAFIS,        
          IsUWAData,        
          DBAircraftFlightChgID,        
          DSAircraftFlightChgID,        
          BuyAircraftFlightIntlID,        
          SellAircraftFlightIntlID,        
          LastUpdUID,        
          LastUpdTS,        
          AircraftTypeID,    
          @NewAircraftID,        
          IsDeleted        
          FROM #TempFleet WHERE     FleetNewCharterRateID=@FleetID           
          DELETE FROM  #TempFleet where  FleetNewCharterRateID = @FleetID       
         
     END    
END    
                  
GO


