
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetCQLegsByQuoteNumAndFileID]    Script Date: 04/24/2013 19:01:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_GetCQLegsByQuoteNumAndFileID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_GetCQLegsByQuoteNumAndFileID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetCQLegsByQuoteNumAndFileID]    Script Date: 04/24/2013 19:01:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_GetCQLegsByQuoteNumAndFileID]
(
	@CQMainID BIGINT,
	@CustomerID BIGINT	
	
)
AS
SET NOCOUNT ON;
BEGIN	
	SELECT
	CL.CQLegID,
	CL.LegNUM,
	DA.IcaoID + '-' + AA.IcaoID  AS 'Description'
	--AA.IcaoID AS 'Arrival Airport'	

	FROM CQLeg CL 
	INNER JOIN Airport AA ON AA.AirportID=CL.AAirportID
	INNER JOIN Airport DA ON DA.AirportID=CL.DAirportID
	WHERE CL.CQMainID=@CQMainID
	AND CL.CustomerID=@CustomerID and CL.IsDeleted=0
END	
GO


