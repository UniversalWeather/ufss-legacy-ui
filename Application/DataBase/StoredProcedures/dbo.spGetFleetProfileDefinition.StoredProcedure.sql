

GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetProfileDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetProfileDefinition]
GO

CREATE PROCEDURE [dbo].[spGetFleetProfileDefinition]      
(@CustomerID BIGINT)  
AS      
BEGIN      
-- =============================================      
-- Author:  <Mathes>      
-- Create date: <24-07-2012>      
-- Description: <Procedure for fetching all record from Fleet Additional Info table>      
-- =============================================      
SELECT     FleetProfileDefinition.FleetProfileInfoXRefID, 
           FleetProfileDefinition.FleetID, 
           FleetProfileDefinition.CustomerID,
           FleetProfileDefinition.FleetProfileInformationID, 
           FleetProfileInformation.FleetProfileAddInfDescription as FleetDescription,--FleetProfileDefinition.FleetDescription, 
           FleetProfileDefinition.InformationValue,
           FleetProfileDefinition.ClientID, 
           FleetProfileDefinition.LastUpdUID, 
           FleetProfileDefinition.LastUpdTS, 
           FleetProfileDefinition.IsPrintable, 
           FleetProfileDefinition.IsDeleted, 
           FleetProfileInformation.FleetInfoCD, 
           FleetProfileInformation.FleetProfileAddInfDescription,
           FleetProfileInformation.IsInactive
FROM       FleetProfileDefinition INNER JOIN
           FleetProfileInformation ON FleetProfileDefinition.FleetProfileInformationID = FleetProfileInformation.FleetProfileInformationID
WHERE      FleetProfileDefinition.CustomerID = @CustomerID   
END
GO
