/****** Object:  StoredProcedure [dbo].[spGetAllCrewInformation]    Script Date: 01/07/2013 20:01:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewInformation]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCrewInformation]    Script Date: 01/07/2013 20:01:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllCrewInformation](@CustomerID BIGINT)    
    
AS    
    
-- =============================================    
-- Author:A.Akhila    
-- Create date: 7/5/2012    
-- Description: Get the Crew Roster Additional Information    
-- =============================================    
    
SET NOCOUNT ON    
IF len(@CustomerID) >0    
BEGIN     
     SELECT [CrewInfoID]  
           ,[CrewInfoCD]  
           ,[CustomerID]  
           ,[CrewInformationDescription]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsDeleted]
           ,[IsInActive]     
     FROM  [CrewInformation]   
     WHERE CustomerID=@CustomerID   
       AND IsDeleted = 'false'   
       order by CrewInfoCD    
END  
GO


