
GO
/****** Object:  StoredProcedure [dbo].[spGetAllMetroCity]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllMetroCity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllMetroCity]
GO

CREATE Procedure [dbo].[spGetAllMetroCity]  
(  
@CustomerID bigint  
)  
as  
  
set nocount on  
begin   
  
SELECT 
       [CustomerID]
      , [MetroID]  
      ,[MetroCD]  
      ,[MetroName]  
      ,[CountryID]  
      ,[StateName]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]
      ,IsInactive  
  FROM [Metro] WHERE IsDeleted='false' and CustomerID = @CustomerID  
  order by MetroCD
   
end
