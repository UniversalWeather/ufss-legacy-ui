USE [FSS_DEV]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteFlightpakFuelDataByFuelVendorID]    Script Date: 01/02/2015 17:18:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteFlightpakFuelDataByFuelVendorID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteFlightpakFuelDataByFuelVendorID]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteFlightpakFuelDataByFuelVendorID]    Script Date: 01/02/2015 17:18:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SnehalShah
-- Create date: 2-1-2015
-- Description:	To Set IsActive for Deleted Fuel Vendor
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteFlightpakFuelDataByFuelVendorID]
	-- Add the parameters for the stored procedure here
	@FuelVendorID bigint, 	
	@LastUpdUID VARCHAR(30),  
	@LastUpdTS DATETIME,
	@IsActive bit
AS
BEGIN	
	SET NOCOUNT ON;
 UPDATE dbo.FlightpakFuel
 SET   
      [LastUpdUID] = @LastUpdUID,  
      [LastUpdTS] = @LastUpdTS,  
      [IsActive] = @IsActive  
 WHERE [FuelVendorID] = @FuelVendorID
 
END

GO


