IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFlightpakFuelHostoricalDataByVendor]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spGetAllFlightpakFuelHostoricalDataByVendor]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFlightpakFuelHostoricalDataByVendor]    Script Date: 10/2/2015 15:25:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajeet Singh
-- Create date: 2015-10-02
-- Description:	Move all the Flightpak Fuel data to Historical Fuel Price table By VendorId
-- =============================================
CREATE PROCEDURE [dbo].[spGetAllFlightpakFuelHistoricalDataByVendor]
		@FuelVendorID BIGINT,
		@CustomerID BIGINT,
		@FBOName VARCHAR(30)='',
		@DepartureICAOID BIGINT=NULL,
		@EffectDate DATE=NULL,
		@StartRow INT,
		@EndRow INT
AS
BEGIN
SET FMTONLY OFF
DECLARE @TotalItems INT;
IF EXISTS
(
SELECT *
FROM tempdb.dbo.sysobjects
WHERE ID = OBJECT_ID(N'tempdb..#FlightpakFuelTempTable')
)
BEGIN
DROP TABLE #FlightpakFuelTempTable
END
CREATE TABLE #FlightpakFuelTempTable(
	FlightpakFuelID BIGINT NOT NULL,
	CustomerID BIGINT NULL,
	DepartureICAOID BIGINT NULL,
	FBOName VARCHAR(25) NULL,
	GallonFrom NUMERIC(17,3) NULL,
	GallonTO NUMERIC(17,3) NULL,
	EffectiveDT DATE NULL,
	UnitPrice NUMERIC(15,3) NULL,
	FuelVendorID BIGINT NULL,
	LastUpdUID VARCHAR(30) NULL,
	LastUpdTS DATETIME NULL,
	FromTable VARCHAR(50) NULL,
	IsActive BIT NULL)

INSERT INTO #FlightpakFuelTempTable(FlightpakFuelID,CustomerID,DepartureICAOID,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,LastUpdUID,LastUpdTS,IsActive,FromTable)
SELECT FlightpakFuelID,CustomerID,DepartureICAOID,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,LastUpdUID,LastUpdTS,IsActive,'FlightpakFuel'
FROM FlightpakFuel
Where FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID

INSERT INTO #FlightpakFuelTempTable(FlightpakFuelID,CustomerID,DepartureICAOID,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,LastUpdUID,LastUpdTS,IsActive,FromTable)
SELECT HistoricalFuelID,CustomerID,DepartureICAOID,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,LastUpdUID,LastUpdTS,IsActive,'HistoricalFuelPrice'
FROM HistoricalFuelPrice
WHERE FuelVendorID=@FuelVendorID AND CustomerID=@CustomerID;

SET @TotalItems=(SELECT COUNT(*) FROM #FlightpakFuelTempTable);

WITH FlightpakFuelData AS(
SELECT ROW_NUMBER() OVER(ORDER BY FT.FlightpakFuelID ASC) AS Row, FT.FlightpakFuelID,FT.CustomerID,FT.DepartureICAOID,A.IcaoID DepartureICAO,A.AirportName,FT.FBOName,FT.GallonFrom,FT.GallonTO,FT.EffectiveDT,FT.UnitPrice,FT.FuelVendorID,FT.LastUpdUID,FT.LastUpdTS,FT.IsActive,FT.FromTable
FROM #FlightpakFuelTempTable FT INNER JOIN Airport A ON FT.DepartureICAOID=A.AirportID AND A.CustomerID=@CustomerID
WHERE FT.FBOName LIKE CASE WHEN @FBOName!='' THEN @FBOName ELSE FT.FBOName END AND
FT.DepartureICAOID=CASE WHEN ISNULL(@DepartureICAOID,0)!=0 THEN @DepartureICAOID ELSE FT.DepartureICAOID END
AND FT.EffectiveDT=CASE WHEN @EffectDate IS NULL THEN FT.EffectiveDT ELSE @EffectDate END)

SELECT * 
FROM FlightpakFuelData
WHERE Row BETWEEN CASE WHEN @StartRow < 0 THEN 1 ELSE @StartRow END AND CASE WHEN @EndRow > @TotalItems THEN @TotalItems ELSE @EndRow END

IF EXISTS
(
SELECT *
FROM tempdb.dbo.sysobjects
WHERE ID = OBJECT_ID(N'tempdb..#FlightpakFuelTempTable')
)
BEGIN
DROP TABLE #FlightpakFuelTempTable
END
END
GO