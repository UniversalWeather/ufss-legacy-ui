
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetCrewGroup]    Script Date: 02/28/2014 13:08:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetCrewGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetCrewGroup]
GO




/****** Object:  StoredProcedure [dbo].[sp_fss_GetCrewGroup]    Script Date: 02/28/2014 13:08:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[sp_fss_GetCrewGroup]  
(  
 @CustomerID BIGINT  
 ,@CrewGroupID BIGINT  
    ,@CrewGroupCD char(4)    
 ,@FetchActiveOnly BIT  
)   
AS  
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get ALL Crew Group details for Popup        
-- Exec sp_fss_GetCrewGroup 10099,0,'',1   
-- Exec sp_fss_GetCrewGroup 10099,0,'',0  
-- Exec sp_fss_GetCrewGroup 10099, 10099164851,NULL, 0  
-- Exec sp_fss_GetCrewGroup 10099, 0,'3333',0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
  
   SELECT    
  CG.CrewGroupID        
        ,CG.CrewGroupCD        
        ,CG.CustomerID        
        ,CG.CrewGroupDescription        
        ,CG.LastUpdUID        
        ,CG.LastUpdTS        
        ,CG.HomebaseID        
        ,ISNULL(CG.IsDeleted,0) IsDeleted    
        ,ISNULL(CG.IsInactive,0) IsInactive        
        -- ,C.HomebaseCD           
        ,A.ICAOID AS HomebaseCD            
 FROM  CrewGroup CG  
          LEFT OUTER JOIN Company C on C.HomebaseID = CG.HomebaseID           
          LEFT OUTER JOIN Airport A ON A.AirportID = C.HomebaseAirportID            
 WHERE CG.CustomerID=@CustomerID         
    AND CG.CrewGroupID = case when @CrewGroupID <>0 then @CrewGroupID else CG.CrewGroupID end   
 AND CG.CrewGroupCD = case when Ltrim(Rtrim(@CrewGroupCD)) <> '' then @CrewGroupCD else CG.CrewGroupCD end                                    
 AND ISNULL(CG.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(CG.IsInActive,0) else 0 end  
 AND ISNULL(CG.IsDeleted,0) = 0                                  
 ORDER BY CG.CrewGroupCD ASC                         
END  
  
  
GO

