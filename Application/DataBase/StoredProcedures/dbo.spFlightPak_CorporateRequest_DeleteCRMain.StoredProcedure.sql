/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_DeleteCRMain]    Script Date: 02/08/2013 18:30:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_DeleteCRMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRMain]
GO
CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_DeleteCRMain]
(           
            @CustomerID bigint,
            @CRMainID bigint
)
AS
BEGIN 
SET NoCOUNT ON

DELETE FROM [dbo].[CRMain]
      WHERE  CRMainID = @CRMainID and  CustomerID=@CustomerID 
END

GO


