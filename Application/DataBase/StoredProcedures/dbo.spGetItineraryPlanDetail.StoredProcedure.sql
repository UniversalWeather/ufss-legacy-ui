
GO
/****** Object:  StoredProcedure [dbo].[spGetItineraryPlanDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetItineraryPlanDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetItineraryPlanDetail]
GO

CREATE PROCEDURE spGetItineraryPlanDetail
(@ItineraryPlanDetailID	BIGINT
,@CustomerID	BIGINT
,@IsDeleted	BIT)
AS BEGIN

	IF (@ItineraryPlanDetailID = -1)
		BEGIN
			SELECT	ItineraryPlanDetail.ItineraryPlanDetailID,
					ItineraryPlanDetail.CustomerID,
					ItineraryPlanDetail.ItineraryPlanID,
					ItineraryPlanDetail.IntinearyNUM,
					ItineraryPlanDetail.LegNUM,
					ItineraryPlanDetail.DAirportID,
					ItineraryPlanDetail.AAirportID,
					ItineraryPlanDetail.Distance,
					ItineraryPlanDetail.PowerSetting,
					ItineraryPlanDetail.TakeoffBIAS,
					ItineraryPlanDetail.LandingBias,
					ItineraryPlanDetail.TrueAirSpeed,
					ItineraryPlanDetail.WindsBoeingTable,
					ItineraryPlanDetail.ElapseTM,
					ItineraryPlanDetail.DepartureLocal,
					ItineraryPlanDetail.DepartureGMT,
					ItineraryPlanDetail.DepartureHome,
					ItineraryPlanDetail.ArrivalLocal,
					ItineraryPlanDetail.ArrivalGMT,
					ItineraryPlanDetail.ArrivalHome,
					ItineraryPlanDetail.IsDepartureConfirmed,
					ItineraryPlanDetail.IsArrivalConfirmation,
					ItineraryPlanDetail.PassengerNUM,
					ItineraryPlanDetail.Cost,
					ItineraryPlanDetail.WindReliability,
					ItineraryPlanDetail.LegID,
					ItineraryPlanDetail.LastUpdUID,
					ItineraryPlanDetail.LastUpdTS,
					ItineraryPlanDetail.IsDeleted,
					DAirport.IcaoID As DAirportIcaoID,
					DAirport.CityName As DAirportCityName,
					DAirport.CountryName As DAirportCountryName,
					AAirport.IcaoID As AAirportIcaoID,
					AAirport.CityName As AAirportCityName,
					AAirport.CountryName As AAirportCountryName
					,DAirport.Alerts As DAirportAlerts
					,AAirport.Alerts As AAirportAlerts
			FROM	ItineraryPlanDetail
			LEFT OUTER JOIN ItineraryPlan ON ItineraryPlan.ItineraryPlanID = ItineraryPlanDetail.ItineraryPlanID
			LEFT OUTER JOIN Airport AAirport ON AAirport.AirportID = ItineraryPlanDetail.AAirportID
			LEFT OUTER JOIN Airport DAirport ON DAirport.AirportID = ItineraryPlanDetail.DAirportID			
			WHERE	ItineraryPlanDetail.CustomerID = @CustomerID
					AND ItineraryPlanDetail.IsDeleted = @IsDeleted
		END
	ELSE
		BEGIN
			SELECT	ItineraryPlanDetail.ItineraryPlanDetailID,
					ItineraryPlanDetail.CustomerID,
					ItineraryPlanDetail.ItineraryPlanID,
					ItineraryPlanDetail.IntinearyNUM,
					ItineraryPlanDetail.LegNUM,
					ItineraryPlanDetail.DAirportID,
					ItineraryPlanDetail.AAirportID,
					ItineraryPlanDetail.Distance,
					ItineraryPlanDetail.PowerSetting,
					ItineraryPlanDetail.TakeoffBIAS,
					ItineraryPlanDetail.LandingBias,
					ItineraryPlanDetail.TrueAirSpeed,
					ItineraryPlanDetail.WindsBoeingTable,
					ItineraryPlanDetail.ElapseTM,
					ItineraryPlanDetail.DepartureLocal,
					ItineraryPlanDetail.DepartureGMT,
					ItineraryPlanDetail.DepartureHome,
					ItineraryPlanDetail.ArrivalLocal,
					ItineraryPlanDetail.ArrivalGMT,
					ItineraryPlanDetail.ArrivalHome,
					ItineraryPlanDetail.IsDepartureConfirmed,
					ItineraryPlanDetail.IsArrivalConfirmation,
					ItineraryPlanDetail.PassengerNUM,
					ItineraryPlanDetail.Cost,
					ItineraryPlanDetail.WindReliability,
					ItineraryPlanDetail.LegID,
					ItineraryPlanDetail.LastUpdUID,
					ItineraryPlanDetail.LastUpdTS,
					ItineraryPlanDetail.IsDeleted,
					DAirport.IcaoID As DAirportIcaoID,
					DAirport.CityName As DAirportCityName,
					DAirport.CountryName As DAirportCountryName,
					AAirport.IcaoID As AAirportIcaoID,
					AAirport.CityName As AAirportCityName,
					AAirport.CountryName As AAirportCountryName
					,DAirport.Alerts As DAirportAlerts
					,AAirport.Alerts As AAirportAlerts
			FROM	ItineraryPlanDetail
			LEFT OUTER JOIN ItineraryPlan ON ItineraryPlan.ItineraryPlanID = ItineraryPlanDetail.ItineraryPlanID
			LEFT OUTER JOIN Airport AAirport ON AAirport.AirportID = ItineraryPlanDetail.AAirportID
			LEFT OUTER JOIN Airport DAirport ON DAirport.AirportID = ItineraryPlanDetail.DAirportID			
			WHERE	ItineraryPlanDetail.CustomerID = @CustomerID
					AND ItineraryPlanDetail.IsDeleted = @IsDeleted
					AND ItineraryPlanDetail.ItineraryPlanDetailID = @ItineraryPlanDetailID
		END
END