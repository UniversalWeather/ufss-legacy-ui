/****** Object:  StoredProcedure [dbo].[spGetAllTransport]    Script Date: 01/07/2013 18:45:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTransport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTransport]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllTransport]    Script Date: 01/07/2013 18:45:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllTransport](@CustomerID BIGINT)      
AS      
-- =============================================      
-- Author:Prakash Pandian.S      
-- Create date: 24/4/2012      
-- Description: Get the Transport information      
-- =============================================      
SET NOCOUNT ON      
IF LEN(@CUSTOMERID) >0      
BEGIN       
 SELECT A.[TransportID]    
  ,A.[AirportID]    
  ,A.[TransportCD]    
  ,A.[CustomerID]    
  ,[IsChoice]    
  ,A.[TransportationVendor]    
  ,[PhoneNum]    
  ,[FaxNum]    
  ,[NegotiatedRate]    
  ,[ContactName]    
  ,[Remarks]    
  ,A.[LastUpdUID]    
  ,A.[LastUpdTS]    
  ,[UpdateDT]    
  ,[RecordType]    
  ,[SourceID]    
  ,[ControlNum]    
  ,[TollFreePhoneNum]    
  ,[Website]    
  ,[UWAMaintFlag]    
  ,[UWAID]    
  ,A.[IsInActive]    
  ,A.[IsDeleted]    
  ,[UWAUpdates]     
  ,[AltBusinessPhone]    
        ,[BusinessEmail]    
        ,[ContactBusinessPhone]    
        ,[CellPhoneNum]    
        ,[ContactEmail]    
        ,[Addr1]    
        ,[Addr2]    
        ,[Addr3]    
        ,[City]    
        ,[StateProvince]    
        ,A.[MetroID]    
        ,A.[CountryID]  
        ,M.MetroCD  
        ,C.CountryCD
        ,A.IsPassenger
        ,A.IsCrew
		,A.ExchangeRateID
        ,A.NegotiatedTerms
        ,A.SundayWorkHours
        ,A.MondayWorkHours
        ,A.TuesdayWorkHours
        ,A.WednesdayWorkHours
        ,A.ThursdayWorkHours
        ,A.FridayWorkHours
        ,A.SaturdayWorkHours          
            
 FROM  [Transport] A LEFT OUTER JOIN [metro] M on A.MetroID = M.MetroID      
  LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID ,    
 (SELECT CustomerID, AirportID,TransportID,TransportCD FROM Transport      
                                                      WHERE (AirportID+TransportCD IN (SELECT AirportID+TransportCD FROM Transport     
                                                                                            WHERE IsDeleted ='false' AND CustomerID = @CustomerID GROUP BY AirportID+TransportCD )      
                                                      AND IsDeleted ='false' AND CustomerID = @CustomerID)      
  UNION      
  SELECT CustomerID, AirportID,TransportID,TransportCD FROM Transport      
                                                      WHERE   (UWAMaintFlag='true' OR CustomerID =@CustomerID)       
                                                      AND AirportID+TransportCD NOT IN (SELECT AirportID+TransportCD FROM Transport     
                                                                                             WHERE IsDeleted ='false' and UWAMaintFlag = 'false' group by AirportID+TransportCD )      
                                                      AND IsDeleted = 'false')B      
  WHERE A.CustomerID=B.CustomerID  AND A.TransportID=B.TransportID  AND A.AirportID=B.AirportID and A.TransportCD = B.TransportCD ORDER BY UWAMaintFlag      
END  
GO


