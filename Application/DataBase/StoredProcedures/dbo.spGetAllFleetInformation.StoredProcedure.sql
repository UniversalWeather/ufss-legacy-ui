
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetInformation]
GO


CREATE procedure [dbo].[spGetAllFleetInformation](@CustomerID BIGINT)  
as  
SET NoCOUNT ON  
  
BEGIN  
SELECT      [FleetProfileInformationID]
           ,[CustomerID]  
           ,[FleetInfoCD]  
           ,[FleetProfileAddInfDescription]         
           ,[ClientID]   
           ,[LastUpdUID]             
           ,[LastUpdTS]             
           ,[IsDeleted]  
           ,IsInactive
  FROM  FleetProfileInformation where CustomerID=@CustomerID 
  AND IsNull(FleetProfileInformation.IsDeleted,0)=0        
  Order By  FleetInfoCD    
end
GO
