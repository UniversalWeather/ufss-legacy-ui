
/****** Object:  StoredProcedure [dbo].[spGetPaxAvailableList]    Script Date: 04/01/2013 16:23:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPaxAvailableList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPaxAvailableList]
GO



/****** Object:  StoredProcedure [dbo].[spGetPaxAvailableList]    Script Date: 04/01/2013 16:23:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetPaxAvailableList](@CustomerID BIGINT ,        
                                               @PaxCode BIGINT,@ClientID BIGINT )        
-- =============================================        
-- Author: MohanRaja.C        
-- Create date: 02/05/2012        
-- Description: Get the Available Passenger List        
-- =============================================        
AS        
SET NOCOUNT ON        
BEGIN        
  IF(@ClientID>0)      
  BEGIN      
SELECT   [PassengerRequestorID]      
		,[PassengerRequestorCD]  
		,isnull(FirstName,'') + ' ' + isnull(middleInitial,'') + ' ' + isnull(LastName,'') + ' - (' + [PassengerRequestorCD] + ')'AS PassengerNameforList      
		,[PassengerName] + ' - (' + [PassengerRequestorCD] + ')'  AS [PassengerName]      
		,[CustomerID]    
FROM [Passenger] WHERE [Passenger].[PassengerRequestorID]         
 NOT IN(SELECT PassengerGroupOrder.PassengerRequestorID FROM PassengerGroupOrder         
   WHERE PassengerGroupOrder.PassengerGroupID=@PaxCode)         
 AND [Passenger].CustomerID=@CustomerID and Passenger.ClientID=@ClientID   AND ISNULL([Passenger].IsDeleted,0)=0 AND IsActive = 1   
  ORDER BY LastName  
       
END        
ELSE      
BEGIN      
SELECT   [PassengerRequestorID]      
		,[PassengerRequestorCD]     
		,isnull(FirstName,'') + ' ' + isnull(middleInitial,'') + ' ' + isnull(LastName,'') + ' - (' + [PassengerRequestorCD] + ')' AS PassengerNameforList   
		,[PassengerName] + ' - (' + [PassengerRequestorCD] + ')' AS [PassengerName]      
		,[CustomerID]      
FROM [Passenger] WHERE [Passenger].[PassengerRequestorID]         
 NOT IN(SELECT PassengerGroupOrder.PassengerRequestorID FROM PassengerGroupOrder         
   WHERE PassengerGroupOrder.PassengerGroupID=@PaxCode)         
 AND [Passenger].CustomerID=@CustomerID   AND ISNULL([Passenger].IsDeleted,0)=0 AND IsActive = 1   
  ORDER BY LastName  
END      
END  
GO


