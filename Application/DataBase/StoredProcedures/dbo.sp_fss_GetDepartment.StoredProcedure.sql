/****** Object:  StoredProcedure [dbo].[sp_fss_GetDepartment]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetDepartment]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetDepartment]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetDepartment](@CustomerID  BIGINT,    
  @DepartmentCD varchar(8),    
  @DepartmentID BIGINT,    
  @ClientID BIGINT,    
  @FetchInactiveOnly BIT=0    
  )              
  AS           
  BEGIN             
  -- =============================================              
  -- Author: Karthikeyan.S    
  -- Create date: 25/2/2014              
  -- Description: Get Department Informations        
  -- EXEC [sp_fss_GetDepartment] 10003, '',0, 100033,0    
  -- EXEC [sp_fss_GetDepartment] 10003,'',0, 0,0    
  -- EXEC [sp_fss_GetDepartment] 10003, '11',0,0    
  -- EXEC [sp_fss_GetDepartment] 10003, '',10003184802,0    
  -- =============================================              
  SET NOCOUNT ON              
            
  SELECT             
    d.DepartmentID,            
    d.[DepartmentCD],             
    d.[DepartmentName],            
    d.[CustomerID],          
    ISNULL(NULLIF(d.ClientID,''),'-') as ClientID,              
    d.[LastUpdUID],              
    d.[LastUpdTS] ,                
    ISNULL(d.[IsInActive],0) IsInactive,     
    d.[IsDeleted]  ,            
    ISNULL(NULLIF(c.ClientCD,''),'') as ClientCD ,       
    d.DepartPercentage        
    FROM  Department d LEFT OUTER JOIN Client c on d.ClientID = c.ClientID  WHERE     
    d.CustomerID =@CustomerID     
    AND d.DepartmentID = case when @DepartmentID <>0 then @DepartmentID else d.DepartmentID end     
    AND isnull(d.ClientID,0) = case when @ClientID <>0 then @ClientID else isnull(d.ClientID,0) end     
    AND d.DepartmentCD = case when Ltrim(Rtrim(@DepartmentCD)) <> '' then @DepartmentCD else d.DepartmentCD end     
    AND ISNULL(d.IsInActive,0) = (case when @FetchInactiveOnly=0 then ISNULL(d.IsInActive,0) else 1 end)
    AND ISNULL(d.IsDeleted,0) = 0     
    order by DepartmentCD        
END    
GO

