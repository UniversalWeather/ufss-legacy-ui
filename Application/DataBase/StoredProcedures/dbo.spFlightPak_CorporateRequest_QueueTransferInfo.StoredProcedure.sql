

/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_QueueTransferInfo]    Script Date: 05/03/2013 14:11:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_QueueTransferInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_QueueTransferInfo]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_QueueTransferInfo]    Script Date: 05/03/2013 14:11:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE procedure [dbo].[spFlightPak_CorporateRequest_QueueTransferInfo]  
(  
  @CRMainID BIGINT  
 ,@CustomerID BIGINT  
 ,@LastUpdUID VARCHAR(30)  
 ,@NewLegID BIGINT  
 ,@CurrentCRLegID BIGINT  
)  
  
AS BEGIN  
  
  
--DECLARE @CurrentCRLegID BIGINT  
DECLARE @LastUpdTS DATETIME = GETUTCDATE()  
-- variables to get CR ids  
   DECLARE @CurrentCRPassengerID BIGINT  
     
   DECLARE @CurrentFBOID BIGINT  
   DECLARE @CurrentHotelID BIGINT  
   DECLARE @CurrentTransportID BIGINT  
   DECLARE @CurrentCateringID BIGINT  
     
   -- variables to set preflight ids  
   DECLARE @NewPreflightPassengerListID BIGINT  
   DECLARE @NewPreflightFBOID BIGINT  
   DECLARE @NewPreflightHotelID BIGINT  
   DECLARE @NewPreflightTransportID BIGINT  
   DECLARE @NewPreflightCateringID BIGINT  
  
-- Table to store CRPassenger related records of legs  
   DECLARE @tmpCRPassenger TABLE  
   (  
    CRPassengerID bigint,  
    CustomerID bigint,  
    CRLegID bigint,  
    TripNUM int,  
    LegID int,  
    OrderNUM int,  
    PassengerRequestorID bigint,  
    PassengerName varchar(63),  
    FlightPurposeID bigint,  
    PassportID bigint,  
    Billing varchar(25),  
    IsNonPassenger bit,  
    IsBlocked bit,  
    ReservationNUM varchar(6),  
    WaitList char(1),  
    LastUpdUID varchar(30),  
    LastUpdTS datetime,  
    IsDeleted bit,  
    PassengerLastName varchar(20),  
    PassengerFirstName varchar(20),  
    PassengerMiddleName varchar(20),  
    AssociatedPassenger varchar(5),  
    TripStatus VARCHAR (1),  
    TSADTTM datetime,  
    Street VARCHAR (40),  
    CityName VARCHAR (25),  
    StateName VARCHAR (10),  
    PostalZipCD VARCHAR (15)  
   )  
  
  
-- Move the crpassenger record of that cr leg id  
     INSERT INTO @tmpCRPassenger  
      (CRPassengerID,  
      CustomerID,  
      CRLegID,  
      TripNUM,  
      LegID,  
      OrderNUM,  
      PassengerRequestorID,  
      PassengerName,  
      FlightPurposeID,  
      PassportID,  
      Billing,  
      IsNonPassenger,  
      IsBlocked,  
      ReservationNUM,  
      WaitList,  
      LastUpdUID,  
      LastUpdTS,  
      IsDeleted,  
      PassengerLastName,  
      PassengerFirstName,  
      PassengerMiddleName,  
      AssociatedPassenger,  
      TripStatus,  
      TSADTTM,  
      Street,  
      CityName,  
      StateName,  
      PostalZipCD)  
       
     SELECT  
      CRPassengerID,  
      CRPassenger.CustomerID,  
      CRLegID,  
      TripNUM,  
      LegID,  
      OrderNUM,  
      CRPassenger.PassengerRequestorID,  
      ISNULL(Passenger.FirstName,'') + ',' + ISNULL(Passenger.LastName,'') + ',' + ISNULL(Passenger.MiddleInitial,'') AS PassengerName,  
      CRPassenger.FlightPurposeID,  
      PassportID,  
      Billing,  
      IsNonPassenger,  
      IsBlocked,  
      ReservationNUM,  
      WaitList,  
      CRPassenger.LastUpdUID,  
      CRPassenger.LastUpdTS,  
      CRPassenger.IsDeleted,  
      LastName,  
      FirstName,  
      MiddleInitial,        
      AssociatedWithCD,  
      TSAStatus,  
      TSADTTM,  
      Addr1,  
      City AS CityName,  
      StateName,  
      PostalZipCD  
     FROM CRPassenger  
     LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = CRPassenger.PassengerRequestorID  
     WHERE CRLegID = @CurrentCRLegID  
     ORDER BY CRPassengerID ASC  
       
     -- looping passenger record one by one  
     -- Start of While loop for @tmpCRPassenger  
     WHILE (SELECT COUNT(*) FROM @tmpCRPassenger) > 0      
      BEGIN  
  
       -- Get current crpassenger id  
       SELECT TOP 1 @CurrentCRPassengerID = CRPassengerID FROM @tmpCRPassenger  
         
       -- generate new preflight passenger list id  
       EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightPassengerListID OUTPUT    
         
       -- insert the new cr passenger details into preflightpassenger list table  
       INSERT INTO PreflightPassengerList  
        (PreflightPassengerListID,  
        LegID,  
        CustomerID,  
        PassengerID,  
        PassportID,  
        VisaID,  
        HotelName,  
        PassengerFirstName,  
        PassengerMiddleName,  
        PassengerLastName,  
        FlightPurposeID,  
        PassportNUM,  
        Billing,  
        IsNonPassenger,  
        IsBlocked,  
        OrderNUM,  
        ReservationNUM,  
        LastUpdUID,  
        LastUpdTS,  
        WaitList,  
        AssociatedPassenger,  
        TripStatus,  
        TSADTTM,  
        IsDeleted,  
        NoofRooms,  
        ClubCard,  
        IsRateCap,  
        MaxCapAmount,  
        Street,  
        CityName,  
        StateName,  
        PostalZipCD)  
         
       SELECT  
        @NewPreflightPassengerListID,  
        @NewLegID,  
        CustomerID,  
        PassengerRequestorID,  
        PassportID,  
        NULL AS VisaID,  
        NULL AS HotelName,  
        PassengerFirstName,  
        PassengerMiddleName,  
        PassengerLastName,  
        FlightPurposeID,  
        NULL AS PassportNUM,  
        Billing,  
        IsNonPassenger,  
        IsBlocked,  
        OrderNUM,  
        ReservationNUM,  
        LastUpdUID,  
        LastUpdTS,  
        WaitList,  
        AssociatedPassenger,  
        NULL AS TripStatus,  
        TSADTTM,  
        IsDeleted,  
        NULL AS NoofRooms,  
        NULL AS ClubCard,  
        NULL AS IsRateCap,  
        NULL AS MaxCapAmount,  
        Street,  
        CityName,  
        StateName,  
        PostalZipCD  
       FROM @tmpCRPassenger  
       WHERE CRPassengerID = @CurrentCRPassengerID  
         
       -- Adding of hotel record starts here  
       -- Check and Loop Legs and hotel here to add (arrival) records of hotel under preflight passenger.  
       -- Start of IF Condition for checking CRHotelList  
       IF EXISTS (SELECT COUNT(*) FROM CRHotelList WHERE CustomerID = @CustomerID  
                   AND CRLegID = @CurrentCRLegID   
                   AND RecordType = 'P')  
        BEGIN  
          
         --Table declaration for Hotel  
         DECLARE @tmpCRHotelList TABLE  
         (  
          CRHotelListID BIGINT,  
          CustomerID BIGINT,  
          CRLegID BIGINT,  
          TripNUM BIGINT,  
          LegID BIGINT,  
          RecordType VARCHAR(2),  
          HotelID BIGINT,  
          CRHotelListDescription VARCHAR(60),  
          PhoneNUM VARCHAR(25),  
          Rate NUMERIC(6,2),  
          IsCompleted BIT,  
          AirportID BIGINT,  
          LastUpdUID VARCHAR(30),  
          LastUpdTS DATETIME,  
          IsDeleted BIT,  
          Addr1 VARCHAR(40),  
          Addr2 VARCHAR(40),  
          Addr3 VARCHAR(40),  
          CityName VARCHAR(30),  
          StateName VARCHAR(25),  
          PostalZipCD VARCHAR(15),  
          CountryID BIGINT,  
          MetroID BIGINT,  
          Remarks VARCHAR(MAX),  
          Name VARCHAR(40)  
         )  
          
         -- Get arrival hotel record of current crpassenger id  
         INSERT INTO @tmpCRHotelList  
          (CRHotelListID,  
          CustomerID,  
          CRLegID,  
          TripNUM,  
          LegID,  
          RecordType,  
          HotelID,  
          CRHotelListDescription,  
          PhoneNUM,  
          Rate,  
          IsCompleted,  
          AirportID,  
          LastUpdUID,  
          LastUpdTS,  
          IsDeleted,  
          Addr1,  
          Addr2,  
          Addr3,  
          CityName,  
          StateName,  
          PostalZipCD,  
          CountryID,  
          MetroID,  
          Remarks,  
          Name)  
           
         SELECT  
          CRHotelListID,  
          CRHotelList.CustomerID,  
          CRLegID,  
          TripNUM,  
          LegID,  
          CRHotelList.RecordType,  
          CRHotelList.HotelID,  
          CRHotelListDescription,  
          CRHotelList.PhoneNUM,  
          Rate,  
          IsCompleted,  
          CRHotelList.AirportID,  
          CRHotelList.LastUpdUID,  
          CRHotelList.LastUpdTS,  
          CRHotelList.IsDeleted,  
          Addr1,  
          Addr2,  
          Addr3,  
          CityName,  
          StateName,  
          PostalZipCD,  
          CountryID,  
          MetroID,  
          Remarks,  
          Name  
         FROM CRHotelList  
         LEFT OUTER JOIN Hotel ON Hotel.HotelID = CRHotelList.HotelID  
         WHERE CRHotelList.CustomerID = @CustomerID  
          AND CRLegID = @CurrentCRLegID  
          AND CRHotelList.RecordType = 'P'  
         ORDER BY CRHotelListID ASC  
           
         -- Start of While loop for @tmpCRHotelList  
         WHILE (SELECT COUNT(*) FROM @tmpCRHotelList) > 0  
          BEGIN  
             
           -- Get the current hotel id  
           SELECT TOP 1 @CurrentHotelID = CRHotelListID FROM @tmpCRHotelList  
             
           -- generate new preflight hotel list id  
           EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightHotelID OUTPUT   
             
           -- Insert the hotel record into preflight  
           INSERT INTO PreflightPassengerHotelList  
            (PreflightPassengerHotelListID,  
            PreflightHotelName,  
            PreflightPassengerListID,  
            HotelID,  
            AirportID,  
            RoomType,  
            RoomRent,  
            RoomConfirm,  
            RoomDescription,  
            Street,  
            CityName,  
            StateName,  
            PostalZipCD,  
            PhoneNum1,  
            PhoneNum2,  
            PhoneNum3,  
            PhoneNum4,  
            FaxNUM,  
            LastUpdUID,  
            LastUpdTS,  
            IsDeleted,  
            HotelArrangedBy,  
            Rate,  
            DateIn,  
            DateOut,  
            IsSmoking,  
            RoomTypeID,  
            NoofBeds,  
            IsAllCrew,  
            IsCompleted,  
            IsEarlyCheckIn,  
            EarlyCheckInOption,  
            SpecialInstructions,  
            ConfirmationStatus,  
            Comments,  
            CustomerID,  
            Address1,  
            Address2,  
            Address3,  
            MetroID,  
            City,  
            StateProvince,  
            CountryID,  
            PostalCode)  
              
           SELECT  
            @NewPreflightHotelID AS PreflightPassengerHotelListID,  
            Name AS PreflightHotelName,  
            @NewPreflightPassengerListID AS PreflightPassengerListID,  
            HotelID AS HotelID,  
            AirportID AS AirportID ,  
            NULL AS RoomType,  
            NULL AS RoomRent,  
            NULL AS RoomConfirm,  
            NULL AS RoomDescription,  
            NULL AS Street,  
            NULL AS CityName,  
            NULL AS StateName,  
            NULL AS PostalZipCD,  
            NULL AS PhoneNum1,  
            NULL AS PhoneNum2,  
            NULL AS PhoneNum3,  
            NULL AS PhoneNum4,  
            NULL AS FaxNUM,  
            @LastUpdUID,  
            @LastUpdTS,  
            IsDeleted,  
            NULL AS HotelArrangedBy,  
            NULL AS Rate,  
            NULL AS DateIn,  
            NULL AS DateOut,  
            NULL AS IsSmoking,  
            NULL AS RoomTypeID,  
            NULL AS NoofBeds,  
            NULL AS IsAllCrew,  
            NULL AS IsCompleted,  
            NULL AS IsEarlyCheckIn,  
            NULL AS EarlyCheckInOption,  
            NULL AS SpecialInstructions,  
            NULL AS ConfirmationStatus,  
            Remarks AS Comments,  
            CustomerID,  
            Addr1 AS Address1,  
            Addr2 AS Address2,  
            Addr3 AS Address3,  
            MetroID AS MetroID,  
            CityName AS City,  
            StateName AS StateProvince,  
            CountryID AS CountryID,  
            PostalZipCD AS PostalCode  
           FROM @tmpCRHotelList  
           WHERE CRHotelListID = @CurrentHotelID  
             
           -- delete the cr hotel record from temp table  
           DELETE FROM @tmpCRHotelList WHERE CRHotelListID = @CurrentHotelID  
              
          END  
          -- End of While loop for @tmpCRHotelList  
          -- Loop ends for adding hotel to preflight  
         
        END  
        -- End of IF Condition for checking CRHotelList  
        -- Adding of hotel record ends here  
  
       -- delete the cr passenger record from temp table  
       DELETE FROM @tmpCRPassenger WHERE CRPassengerID = @CurrentCRPassengerID  
         
      END  
      -- End of While loop for @tmpCRPassenger  
            
      -- Start of adding FBO to preflight  
      DECLARE @IsDepart AS BIT  
      DECLARE @IsArrive AS BIT  
      DECLARE @RecordType AS VARCHAR(2)  
        
      --Table declaration for fbo  
      DECLARE @tmpCRFBOList TABLE  
      (  
       CRFBOListID BIGINT,  
       CustomerID BIGINT,  
       CRLegID BIGINT,  
       TripNUM BIGINT,  
       LegID BIGINT,  
       RecordType VARCHAR(2),  
       FBOID BIGINT,  
       CRFBOListDescription VARCHAR(60),  
       PhoneNUM VARCHAR(25),  
       IsCompleted BIT,  
       AirportID BIGINT,  
       LastUpdUID VARCHAR(30),  
       LastUpdTS DATETIME,  
       IsDeleted BIT,  
       FBOVendor VARCHAR(60),  
       CityName VARCHAR(25),  
       StateName VARCHAR(25),  
       PostalZipCD VARCHAR(15),  
       PhoneNUM1 VARCHAR(25),  
       PhoneNUM2 VARCHAR(25),  
       FaxNum VARCHAR(25)  
      )  
  
      INSERT INTO @tmpCRFBOList  
       (CRFBOListID,  
       CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       RecordType,  
       FBOID,  
       CRFBOListDescription,  
       PhoneNUM,  
       IsCompleted,  
       AirportID,  
       LastUpdUID,  
       LastUpdTS,  
       IsDeleted,  
       FBOVendor,  
       CityName,  
       StateName,  
       PostalZipCD,  
       PhoneNUM1,  
       PhoneNUM2,  
       FaxNum)  
            
      SELECT  
       CRFBOListID,  
       FBO.CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       CRFBOList.RecordType,  
       FBO.FBOID,  
       CRFBOListDescription,  
       PhoneNUM,  
       IsCompleted,  
       FBO.AirportID,  
       FBO.LastUpdUID,  
       FBO.LastUpdTS,  
       FBO.IsDeleted  
       ,FBOVendor,  
       CityName,  
       StateName,  
       PostalZipCD,  
       PhoneNUM1,  
       PhoneNUM2,  
       FBO.FaxNum  
      FROM CRFBOList  
      LEFT OUTER JOIN FBO ON FBO.FBOID = CRFBOList.FBOID  
      WHERE CRLegID = @CurrentCRLegID  
      ORDER BY CRFBOListID ASC  
        
      -- Start of While loop for @tmpCRFBOList  
      WHILE (SELECT COUNT(*) FROM @tmpCRFBOList) > 0  
       BEGIN  
         
        -- Get the current fbo id  
        SELECT TOP 1 @CurrentFBOID = CRFBOListID, @RecordType=RecordType FROM @tmpCRFBOList  
          
        --IF (@RecordType = 'D')  
        -- BEGIN  
        --  SET @IsDepart = 1  
        --  SET @IsArrive = 0  
        -- END  
        --ELSE IF (@RecordType = 'A')  
        -- BEGIN  
        --  SET @IsDepart = 0  
        --  SET @IsArrive = 1  
        -- END  
          
        -- generate new preflight hotel list id  
        EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightFBOID OUTPUT   
          
        INSERT INTO PreflightFBOList  
         (PreflightFBOID,  
         PreflightFBOName,  
         LegID,  
         FBOID,  
         IsArrivalFBO,  
         IsDepartureFBO,  
         AirportID,  
         Street,  
         CityName,  
         StateName,  
         PostalZipCD,  
         PhoneNum1,  
         PhoneNum2,  
         PhoneNum3,  
         PhoneNum4,  
         FaxNUM,  
         LastUpdUID,  
         LastUpdTS,  
         IsDeleted,  
         NoUpdated,  
         FBOArrangedBy,  
         IsCompleted,  
         FBOInformation,  
         ConfirmationStatus,  
         Comments,  
         CustomerID  
         --IsRequired,  
         --IsNotRequired,  
         --IsInProgress,  
         --IsChange,  
         --IsCancelled  
         )  
          
        SELECT  
         @NewPreflightFBOID,  
         FBOVendor AS PreflightFBOName,  
         @NewLegID AS LegID,  
         FBOID AS FBOID,           
         CASE WHEN RecordType = 'A' THEN 1 ELSE 0 END AS IsArrivalFBO,  
         CASE WHEN RecordType = 'D' THEN 1 ELSE 0 END AS IsDepartureFBO,          
         AirportID AS AirportID,  
         NULL AS Street,  
         CityName AS CityName,  
         StateName AS StateName,  
         PostalZipCD AS PostalZipCD,  
         PhoneNUM1 AS PhoneNum1,  
         PhoneNUM2 AS PhoneNum2,  
         NULL AS PhoneNum3,  
         NULL AS PhoneNum4,  
         FaxNum AS FaxNUM,  
         @LastUpdUID AS LastUpdUID,  
         @LastUpdTS AS LastUpdTS,  
         isnull(IsDeleted,0) AS IsDeleted,  
         NULL AS NoUpdated,  
         NULL AS FBOArrangedBy,  
         NULL AS IsCompleted,  
         NULL AS FBOInformation,  
         NULL AS ConfirmationStatus,  
         NULL AS Comments,  
         CustomerID AS CustomerID  
         --NULL AS IsRequired,  
         --NULL AS IsNotRequired,  
         --NULL AS IsInProgress,  
         --NULL AS IsChange,  
         --NULL AS IsCancelled  
        FROM @tmpCRFBOList  
        WHERE CRFBOListID = @CurrentFBOID  
          
        -- delete the cr hotel record from temp table  
        DELETE FROM @tmpCRFBOList WHERE CRFBOListID = @CurrentFBOID  
       END     
      -- End of adding FBO to preflight  
        
      -- Start of adding Catering to preflight  
        
      --Table declaration for catering  
      DECLARE @tmpCRCateringList TABLE  
      (  
       CRCateringListID BIGINT,  
       CustomerID BIGINT,  
       CRLegID BIGINT,  
       TripNUM BIGINT,  
       LegID BIGINT,  
       RecordType VARCHAR(2),  
       CateringID BIGINT,  
       CRCateringListDescription VARCHAR(60),  
       PhoneNUM VARCHAR(25),  
       Rate NUMERIC(6,2),  
       IsCompleted BIT,  
       AirportID BIGINT,  
       LastUpdUID VARCHAR(30),  
       LastUpdTS DATETIME,  
       IsDeleted BIT,  
       CateringVendor VARCHAR(40),   
       City VARCHAR(25),   
       StateProvince VARCHAR(10),   
       CateringPhoneNum VARCHAR(25),   
       FaxNum VARCHAR(25),   
       ContactName VARCHAR(25)  
      )  
  
      INSERT INTO @tmpCRCateringList  
       (CRCateringListID,  
       CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       RecordType,  
       CateringID,  
       CRCateringListDescription,  
       PhoneNUM,  
       Rate,  
       IsCompleted,  
       AirportID,  
       LastUpdUID,  
       LastUpdTS,  
       IsDeleted,  
       CateringVendor,  
       City,  
       StateProvince,  
       CateringPhoneNum,  
       FaxNum,  
       ContactName)  
            
      SELECT  
       CRCateringListID,  
       CRCateringList.CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       CRCateringList.RecordType,  
       CRCateringList.CateringID,  
       Catering.CateringVendor AS CRCateringListDescription,  
       Catering.PhoneNUM AS CRCateringListPhoneNUM,  
       Catering.NegotiatedRate AS Rate,  
       IsCompleted,  
       CRCateringList.AirportID,  
       CRCateringList.LastUpdUID,  
       CRCateringList.LastUpdTS,  
       CRCateringList.IsDeleted,  
       CateringVendor,  
       City,  
       StateProvince,  
       Catering.PhoneNum AS CateringPhoneNum,  
       Catering.FaxNum AS FaxNum,  
       Catering.ContactName AS ContactName  
      FROM CRCateringList  
      LEFT OUTER JOIN Catering ON Catering.CateringID = CRCateringList.CateringID  
      WHERE CRLegID = @CurrentCRLegID  
      ORDER BY CRCateringListID ASC  
        
      -- Start of While loop for @tmpCRCateringList  
      WHILE (SELECT COUNT(*) FROM @tmpCRCateringList) > 0  
       BEGIN  
         
        -- Get the current Catering id  
        SELECT TOP 1 @CurrentCateringID = CRCateringListID, @RecordType=RecordType FROM @tmpCRCateringList  
          
        IF (@RecordType = 'D')  
         BEGIN  
          SET @IsDepart = 1  
          SET @IsArrive = 0  
         END  
        ELSE IF (@RecordType = 'A')  
         BEGIN  
          SET @IsDepart = 0  
          SET @IsArrive = 1  
         END  
          
        -- generate new preflight Catering list id  
        EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCateringID OUTPUT   
          
        INSERT INTO PreflightCateringDetail  
            ([PreflightCateringID]  
            ,[LegID]  
            ,[CustomerID]  
            ,[AirportID]  
            ,[ArriveDepart]  
            ,[CateringID]  
            ,[IsCompleted]  
            ,[Cost]  
            ,[ContactName]  
            ,[ContactPhone]  
            ,[ContactFax]  
            ,[CateringConfirmation]  
            ,[CateringComments]  
            ,[LastUpdUID]  
            ,[LastUpdTS]  
            ,[IsDeleted]  
            ,[IsUWAArranger]  
            ,[CateringContactName])  
          
        SELECT  
         @NewPreflightCateringID,           
         @NewLegID AS LegID,  
         CustomerID AS CustomerID,           
         AirportID AS AirportID,  
         CASE WHEN RecordType = 'A' THEN 'A' ELSE 'D' END,  
         CateringID AS CateringID,  
         NULL AS IsCompleted,  
         0,  
         CRCateringListDescription AS ContactName,  
         PhoneNUM AS ContactPhone,  
         NULL,  
         NULL AS CateringConfirmation,  
         NULL,  
         @LastUpdUID AS LastUpdUID,  
         @LastUpdTS AS LastUpdTS,           
         IsDeleted AS IsDeleted,  
         NULL,  
         NULL  
        FROM @tmpCRCateringList  
        WHERE CRCateringListID = @CurrentCateringID  
          
        -- delete the cr Catering record from temp table  
        DELETE FROM @tmpCRCateringList WHERE CRCateringListID = @CurrentCateringID  
       END     
      -- End of adding Catering to preflight  
        
      -- Start of adding Transport to preflight  
        
      --Table declaration for transport  
      DECLARE @tmpCRTransportList TABLE  
      (  
       CRTransportListID BIGINT,  
       CustomerID BIGINT,  
       CRLegID BIGINT,  
       TripNUM BIGINT,  
       LegID BIGINT,  
       RecordType VARCHAR(2),  
       TransportID BIGINT,  
       CRTransportListDescription VARCHAR(60),  
       PhoneNUM VARCHAR(25),  
       Rate NUMERIC(6,2),  
       IsCompleted BIT,  
       AirportID BIGINT,  
       LastUpdUID VARCHAR(30),  
       LastUpdTS DATETIME,  
       IsDeleted BIT,  
       TransportationVendor VARCHAR(40),   
       City VARCHAR(25),   
       StateProvince VARCHAR(10),       
       TransportPhoneNum VARCHAR(25),   
       FaxNum VARCHAR(25)  
      )  
  
      INSERT INTO @tmpCRTransportList  
       (CRTransportListID,  
       CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       RecordType,  
       TransportID,  
       CRTransportListDescription,  
       PhoneNUM,  
       Rate,  
       IsCompleted,  
       AirportID,  
       LastUpdUID,  
       LastUpdTS,  
       IsDeleted,  
       TransportationVendor,  
       City,  
       StateProvince,  
       TransportPhoneNum,  
       FaxNum)  
        
      SELECT  
       CRTransportListID,  
       CRTransportList.CustomerID,  
       CRLegID,  
       TripNUM,  
       LegID,  
       CRTransportList.RecordType,  
       CRTransportList.TransportID,  
       CRTransportListDescription,  
       CRTransportList.PhoneNUM AS CRTransportListPhoneNUM,  
       CRTransportList.Rate,  
       IsCompleted,  
       CRTransportList.AirportID,  
       CRTransportList.LastUpdUID,  
       CRTransportList.LastUpdTS,  
       CRTransportList.IsDeleted  
       ,TransportationVendor,  
       City,  
       StateProvince,  
       Transport.PhoneNum AS TransportPhoneNum,  
       CRTransportList.FaxNum  
      FROM CRTransportList  
      LEFT OUTER JOIN Transport ON Transport.TransportID = CRTransportList.TransportID  
      WHERE CRLegID = @CurrentCRLegID  
      ORDER BY CRTransportListID ASC  
        
      -- Start of While loop for @tmpCRTransportList  
      WHILE (SELECT COUNT(*) FROM @tmpCRTransportList) > 0  
       BEGIN  
         
        -- Get the current Transport id  
        SELECT TOP 1 @CurrentTransportID = CRTransportListID, @RecordType=RecordType FROM @tmpCRTransportList  
          
        IF (@RecordType = 'P')  
         BEGIN  
          SET @IsDepart = 1  
          SET @IsArrive = 0  
         END  
        ELSE IF (@RecordType = 'C')  
         BEGIN  
          SET @IsDepart = 0  
          SET @IsArrive = 1  
         END  
                  -- generate new preflight Transport list id  
        EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightTransportID OUTPUT   
          
        INSERT INTO PreflightTransportList  
         (PreflightTransportID,  
         PreflightTransportName,  
         LegID,  
         TransportID,  
         IsArrivalTransport,  
         IsDepartureTransport,  
         AirportID,  
         CrewPassengerType,  
         Street,  
         CityName,  
         StateName,  
         PostalZipCD,  
         PhoneNum1,  
         PhoneNum2,  
         PhoneNum3,  
         PhoneNum4,  
         FaxNUM,  
         LastUpdUID,  
         LastUpdTS,  
         IsDeleted,  
         ConfirmationStatus,  
         Comments,  
         CustomerID,  
         IsCompleted)  
          
        SELECT  
         @NewPreflightTransportID,  
         TransportationVendor AS PreflightTransportName,  
         @NewLegID AS LegID,  
         TransportID AS TransportID,  
         @IsArrive AS IsArrivalTransport,  
         @IsDepart AS IsDepartureTransport,  
         AirportID AS AirportID,  
         NULL AS CrewPassengerType,  
         NULL AS Street,  
         City AS CityName,  
         StateProvince AS StateName,  
         NULL AS PostalZipCD,  
         TransportPhoneNum AS PhoneNum1,  
         NULL AS PhoneNum2,  
         NULL AS PhoneNum3,  
         NULL AS PhoneNum4,  
         FaxNum AS FaxNUM,  
         @LastUpdUID AS LastUpdUID,  
         @LastUpdTS AS LastUpdTS,  
         IsDeleted AS IsDeleted,  
         NULL AS ConfirmationStatus,  
         NULL AS Comments,  
         CustomerID AS CustomerID,  
         NULL AS IsCompleted  
        FROM @tmpCRTransportList  
        WHERE CRTransportListID = @CurrentTransportID  
          
        -- delete the cr Transport record from temp table  
        DELETE FROM @tmpCRTransportList WHERE CRTransportListID = @CurrentTransportID  
       END     
      -- End of adding Transport to preflight  
        
       
     -- delete the cr leg id from temp table once transferred  
      
END  
  
GO


