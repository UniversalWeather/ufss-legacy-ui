/****** Object:  StoredProcedure [dbo].[spGetAllAirportForGrid]    Script Date: 12/26/2012 14:33:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAirportForGrid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAirportForGrid]
GO


/****** Object:  StoredProcedure [dbo].[spGetAllAirportForGrid]    Script Date: 12/26/2012 14:33:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
      
CREATE PROCEDURE [dbo].[spGetAllAirportForGrid](@CustomerID BIGINT,@IsInActive bit)            
AS            
-- =============================================            
-- Author:Manish Varma         
-- Modified: For Runway Length, IATA, IsInactive=0        
-- Modified: To include UWA CustomerID        
-- Create date: 27/8/2012            
-- Description: Gets the Airport Information for Grid (Optimized)        
-- Usuage exec SPgetallairportForGrid 1000        
-- =============================================            
SET NOCOUNT ON            
IF LEN(@CustomerID) >0            
BEGIN         
DECLARE @UWACustomerID BIGINT        
SELECT @UWACustomerID  = dbo.GetUWACustomerID()         
        
Select A.AirportID,A.CustomerID,A.IcaoID, A.CityName, A.StateName, A.CountryID, C.CountryCD, A.AirportName,A.OffsetToGMT,A.IsEntryPort,      
A.IsInActive, A.LastUpdUID,A.LastUpdTS,A.IsWorldClock,(SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,A.UWAID, A.Iata,      
isnull(LongestRunway,0) as [MaxRunway], C.CountryName
--(SELECT Max(v)         
 --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length) ,(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]      
  from Airport A         
left join Country C on C.CountryID = A.CountryID  where A.CustomerID =@CustomerID and A.IsDeleted =0 and A.IsInActive <= @IsInActive        
union all        
Select A.AirportID,A.CustomerID,A.IcaoID, A.CityName, A.StateName, A.CountryID, C.CountryCD, A.AirportName,A.OffsetToGMT,A.IsEntryPort,          
A.IsInActive, A.LastUpdUID,A.LastUpdTS,A.IsWorldClock,(SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,A.UWAID,A.Iata,       
isnull(LongestRunway,0) as [MaxRunway], C.CountryName     
--(SELECT Max(v)         
 --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length),(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]      
  from Airport A         
left join Country C on C.CountryID = A.CountryID  where A.CustomerID <> @CustomerID and A.CustomerID = @UWACustomerID  and A.IsDeleted =0  and A.IsInActive <= @IsInActive and IcaoID Not in (        
Select distinct IcaoID  from Airport where CustomerID = @CustomerID and IsDeleted = 0 ) order by CityName,IcaoID       
        
        
END 