/****** Object:  StoredProcedure [dbo].[spGetPassengerForClient]    Script Date: 01/07/2013 19:29:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassengerForClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassengerForClient]
GO

/****** Object:  StoredProcedure [dbo].[spGetPassengerForClient]    Script Date: 01/07/2013 19:29:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetPassengerForClient]  
(@CustomerID BIGINT  
,@ClientID BIGINT)  
AS BEGIN  
SET NOCOUNT ON  
 IF (@ClientID > 0)  
 BEGIN  
  SELECT PassengerRequestorID  
    ,PassengerRequestorCD  
    ,PassengerName  
    ,CustomerID  
    ,ClientID  
    ,IsDeleted  
    ,PassengerWeight
  FROM Passenger  
  WHERE CustomerID = @CustomerID AND   
    ClientID=@ClientID AND  
    IsDeleted = 0  
  ORDER BY PassengerRequestorCD ASC  
 END      
 ELSE      
 BEGIN      
  SELECT PassengerRequestorID  
    ,PassengerRequestorCD  
    ,PassengerName  
    ,CustomerID  
    ,ClientID  
    ,IsDeleted
    ,PassengerWeight  
  FROM Passenger  
  WHERE CustomerID = @CustomerID AND  
    IsDeleted = 0  
  ORDER BY PassengerRequestorCD ASC  
 END  
END  
GO


