GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewPlannerCalendarData]    Script Date: 02/20/2014 12:22:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetCrewPlannerCalendarData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewPlannerCalendarData]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewPlannerCalendarData]    Script Date: 02/20/2014 12:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spFlightPak_Preflight_GetCrewPlannerCalendarData 10002, '2014-12-22 00:00:00',24, 'Local',1,1,1,0,0
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewPlannerCalendarData]
  @CustomerID BIGINT
 ,@StartDate DATETIME
 ,@DisplayInterval SMALLINT
 ,@DisplayTimeBase VARCHAR(10) = 'Local'
 ,@IsTrip BIT =0
 ,@IsHold BIT = 0 
 ,@IsWorkSheet BIT = 0 
 ,@IsCanceled BIT = 0  
 ,@IsUnFulFilled BIT = 0  
AS
BEGIN
SET NOCOUNT ON;
SET FMTONLY OFF;

  
DECLARE @EndDate DATETIME
SET @EndDate = DATEADD(HOUR, @DisplayInterval * 30 , @StartDate)


DECLARE @DateRangeStart1 DATETIME
DECLARE @DateRangeStart2 DATETIME
DECLARE @DateRangeStart3 DATETIME
DECLARE @DateRangeStart4 DATETIME
DECLARE @DateRangeStart5 DATETIME
DECLARE @DateRangeStart6 DATETIME
DECLARE @DateRangeStart7 DATETIME
DECLARE @DateRangeStart8 DATETIME
DECLARE @DateRangeStart9 DATETIME
DECLARE @DateRangeStart10 DATETIME
DECLARE @DateRangeStart11 DATETIME
DECLARE @DateRangeStart12 DATETIME
DECLARE @DateRangeStart13 DATETIME
DECLARE @DateRangeStart14 DATETIME
DECLARE @DateRangeStart15 DATETIME
DECLARE @DateRangeStart16 DATETIME
DECLARE @DateRangeStart17 DATETIME
DECLARE @DateRangeStart18 DATETIME
DECLARE @DateRangeStart19 DATETIME
DECLARE @DateRangeStart20 DATETIME
DECLARE @DateRangeStart21 DATETIME
DECLARE @DateRangeStart22 DATETIME
DECLARE @DateRangeStart23 DATETIME
DECLARE @DateRangeStart24 DATETIME
DECLARE @DateRangeStart25 DATETIME
DECLARE @DateRangeStart26 DATETIME
DECLARE @DateRangeStart27 DATETIME
DECLARE @DateRangeStart28 DATETIME
DECLARE @DateRangeStart29 DATETIME
DECLARE @DateRangeStart30 DATETIME


-- Find the 30 date ranges that are required using the start date and the display interval

SELECT @DateRangeStart1  = @StartDate , 
@DateRangeStart2 = DATEADD(HOUR,@DisplayInterval , @StartDate), 
@DateRangeStart3 = DATEADD(HOUR,@DisplayInterval * 2, @StartDate), 
@DateRangeStart4 = DATEADD(HOUR,@DisplayInterval * 3, @StartDate), 
@DateRangeStart5 = DATEADD(HOUR,@DisplayInterval * 4, @StartDate), 
@DateRangeStart6 = DATEADD(HOUR,@DisplayInterval * 5, @StartDate), 
@DateRangeStart7 = DATEADD(HOUR,@DisplayInterval * 6, @StartDate), 
@DateRangeStart8 = DATEADD(HOUR,@DisplayInterval * 7, @StartDate), 
@DateRangeStart9 = DATEADD(HOUR,@DisplayInterval * 8, @StartDate), 
@DateRangeStart10 = DATEADD(HOUR,@DisplayInterval * 9, @StartDate), 
@DateRangeStart11 = DATEADD(HOUR,@DisplayInterval * 10, @StartDate), 
@DateRangeStart12 = DATEADD(HOUR,@DisplayInterval * 11, @StartDate), 
@DateRangeStart13 = DATEADD(HOUR,@DisplayInterval * 12, @StartDate), 
@DateRangeStart14 = DATEADD(HOUR,@DisplayInterval * 13, @StartDate), 
@DateRangeStart15 = DATEADD(HOUR,@DisplayInterval * 14, @StartDate), 
@DateRangeStart16 = DATEADD(HOUR,@DisplayInterval * 15, @StartDate), 
@DateRangeStart17 = DATEADD(HOUR,@DisplayInterval * 16, @StartDate), 
@DateRangeStart18 = DATEADD(HOUR,@DisplayInterval * 17, @StartDate), 
@DateRangeStart19 = DATEADD(HOUR,@DisplayInterval * 18, @StartDate), 
@DateRangeStart20 = DATEADD(HOUR,@DisplayInterval * 19, @StartDate), 
@DateRangeStart21 = DATEADD(HOUR,@DisplayInterval * 20, @StartDate), 
@DateRangeStart22 = DATEADD(HOUR,@DisplayInterval * 21, @StartDate), 
@DateRangeStart23 = DATEADD(HOUR,@DisplayInterval * 22, @StartDate), 
@DateRangeStart24 = DATEADD(HOUR,@DisplayInterval * 23, @StartDate), 
@DateRangeStart25 = DATEADD(HOUR,@DisplayInterval * 24, @StartDate), 
@DateRangeStart26 = DATEADD(HOUR,@DisplayInterval * 25, @StartDate), 
@DateRangeStart27 = DATEADD(HOUR,@DisplayInterval * 26, @StartDate), 
@DateRangeStart28 = DATEADD(HOUR,@DisplayInterval * 27, @StartDate), 
@DateRangeStart29 = DATEADD(HOUR,@DisplayInterval * 28, @StartDate), 
@DateRangeStart30 = DATEADD(HOUR,@DisplayInterval * 29, @StartDate)
	
DECLARE @TripStatus AS VARCHAR(20) = '';  
    
  IF @IsWorkSheet = 1  
	 SET @TripStatus = 'W,'  
    
  IF @IsTrip = 1  
	 SET @TripStatus = @TripStatus + 'T,'  
  
  IF @IsUnFulFilled = 1  
	 SET @TripStatus = @TripStatus + 'U,'  
    
  IF @IsCanceled = 1  
	 SET @TripStatus = @TripStatus + 'X,'  
    
  IF @IsHold = 1  
	 SET @TripStatus = @TripStatus + 'H' 
  

-- Populate a temp table with data from the PreflightLeg amd PreflightMain tables that	
--				fall within the desired range (StartDateTime and EndDateTime)


	-- Create the temp table #CrewPlannerData
	--:?? Need to convert to a Create Table Script
	
DECLARE @CumTemp TABLE     
(    
   
 TripId BIGINT,
 LegNUM BIGINT,     
 CumulativeETE numeric(14,3)    
) 
DECLARE @TotalEteTemp TABLE     
(       
 TripId BIGINT,     
 TotalETE numeric(14,3)    
)  
INSERT INTO @TotalEteTemp(TripId, TotalETE)  
SELECT TripId, ISNULL(SUM(ElapseTM),0) 
FROM PreflightLeg   
WHERE (CustomerID = @CustomerID OR @CustomerID IS NULL)  
group by  TripID

	 SELECT 		
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM, 
		PM.FleetID FleetID,
		F.TailNum TailNum, 
		PM.TripNUM TripNUM, 		
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		CONVERT(VARCHAR(15), PL.DutyTYPE) DutyTYPE,
		PM.RecordType RecordType, 
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal, 
		PL.DepartureGreenwichDTTM DepartureGreenwichDTTM,
		PL.ArrivalGreenwichDTTM ArrivalGreenwichDTTM,
		PL.HomeDepartureDTTM HomeDepartureDTTM,
		PL.HomeArrivalDTTM HomeArrivalDTTM, 
		PL.LastUpdTS LegLastUpdTS, 
		PM.LastUpdTS TripLastUpdTS,		
		PL.DepartureDTTMLocal DepartureDTTM,
		PL.ArrivalDTTMLocal ArrivalDTTM,
		
		PM.TripStatus TripStatus,
		PL.PassengerRequestorID PassengerRequestorID,
		VR.IsInActive IsInActive,
		D.DepartmentID DepartmentID,
		C.ClientID ClientID,
		FC.FlightCategoryID FlightCategoryID,
		AD.AircraftDutyID AircraftDutyID,
		HM.HomebaseID HomebaseID,
		
		
		PC.CrewID CrewID,
		CW.IsFixedWing IsFixedWing, 
		CW.IsRotaryWing IsRotaryWing,
		CW.IsStatus CrewStatus,
		CD.DutyTypeID CrewDutyID,
		CW.CrewCD,
				
		F.ForeGrndCustomColor as AircraftForeColor,
		F.BackgroundCustomColor as AircraftBackColor,
		PM.IsLog IsLog,
		PM.IsPrivate IsPrivateTrip,
		PL.IsPrivate IsPrivateLeg,

		PM.Notes,
		PM.LastUpdUID, 
		PL.ArriveICAOID AS ArrivalAirportId,A.IcaoID AS ArrivalICAOID,  A.CityName as ArrivalCity,A.AirportName as ArrivalAirportName, A.CountryName as ArrivalCountry,  
        PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
        PL.FlightPurpose, CONVERT(INT,F.MaximumPassenger) - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PA.PassengerRequestorCD,PL.FlightNUM, PL.FuelLoad, PL.OutbountInstruction,
        
        HB.IcaoID HomebaseCD, CONVERT(numeric(14,3),0.0) CumulativeETE, PL.ElapseTM, F.Notes as FleetNotes, D.DepartmentCD, D.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, C.ClientCD,  
        FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
        AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,
        PM.TripDescription as [Description], PC.CrewLastName,PC.CrewFirstName,PC.CrewMiddleName, Cw.Notes as CrewNotes,     
        CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor, CD.ForeGrndCustomColor as CrewDutyTypeForeColor,  
        ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames], TE.TotalETE
     
	INTO #CrewPlannerData
	FROM PreflightLeg PL
	INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
	LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
	--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
	INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
	INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
	LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
	LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
	LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
	LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @CustomerID AND AD.IsDeleted = 0
	LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
	LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
	LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
	LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
	LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0
	INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
	INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID
	--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
	WHERE 1=2 AND PM.CustomerID=@CustomerID
	AND PL.LegNUM IS NOT NULL

	
	-- Based on the Display Time Base option use the correct columns in the WHERE clause.
	IF(@DisplayTimeBase = 'UTC')

	BEGIN
		INSERT INTO #CrewPlannerData

		SELECT 
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM,
		PM.FleetID FleetID,		
		F.TailNum TailNum, 
		PM.TripNUM TripNUM,
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID, 
		PL.DutyTYPE DutyTYPE, 
		PM.RecordType RecordType, 
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal, 
		PL.DepartureGreenwichDTTM DepartureGreenwichDTTM,
		PL.ArrivalGreenwichDTTM ArrivalGreenwichDTTM,
		PL.HomeDepartureDTTM HomeDepartureDTTM,
		PL.HomeArrivalDTTM HomeArrivalDTTM, 
		PL.LastUpdTS LegLastUpdTS, 
		PM.LastUpdTS TripLastUpdTS,
		PL.DepartureGreenwichDTTM DepartureDTTM,		-- Change this to L.DepartureGreenwichDTTM if UTC is selected OR L.HomeDepartureDTTM if Home OR L.DepartureDTTMLocal for Local
		PL.ArrivalGreenwichDTTM ArrivalDTTM,			-- Similar to above, DepartureDTTM
		PM.TripStatus TripStatus,
		PL.PassengerRequestorID PassengerRequestorID,
		VR.IsInActive IsInActive,
		D.DepartmentID DepartmentID,
		C.ClientID ClientID,
		FC.FlightCategoryID FlightCategoryID,
		AD.AircraftDutyID AircraftDutyID,
		HM.HomebaseID HomebaseID,
		PC.CrewID CrewID,
		CW.IsFixedWing IsFixedWing, 
		CW.IsRotaryWing IsRotaryWing,
		CW.IsStatus CrewStatus,
		CD.DutyTypeID CrewDutyID,
		CW.CrewCD,
		F.ForeGrndCustomColor as AircraftForeColor,
		F.BackgroundCustomColor as AircraftBackColor,
		PM.IsLog IsLog,
		PM.IsPrivate IsPrivateTrip,
		PL.IsPrivate IsPrivateLeg,
		
		PM.Notes,
		
		PM.LastUpdUID, 
		PL.ArriveICAOID AS ArrivalAirportId,A.IcaoID AS ArrivalICAOID,  A.CityName as ArrivalCity,A.AirportName as ArrivalAirportName, A.CountryName as ArrivalCountry,  
        PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
        PL.FlightPurpose, CONVERT(INT,F.MaximumPassenger) - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PA.PassengerRequestorCD,PL.FlightNUM, PL.FuelLoad, PL.OutbountInstruction,
         HB.IcaoID HomebaseCD, CONVERT(numeric(14,3),0.0) CumulativeETE, PL.ElapseTM, F.Notes as FleetNotes, D.DepartmentCD, D.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, C.ClientCD,  
        FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
        AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,        
       PM.TripDescription as [Description], PC.CrewLastName,PC.CrewFirstName,PC.CrewMiddleName, Cw.Notes as CrewNotes,     
        CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor, CD.ForeGrndCustomColor as CrewDutyTypeForeColor,  
        ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames], TE.TotalETE

		--INTO #CrewPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown

		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID	
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID	
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @CustomerID AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
		INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID
		--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
		WHERE PL.DepartureGreenwichDTTM is not null AND PL.ArrivalGreenwichDTTM is not null
        --AND PL.DepartureGreenwichDTTM <= PL.ArrivalGreenwichDTTM 
        AND ((PL.DepartureGreenwichDTTM >= @StartDate AND PL.DepartureGreenwichDTTM <=@EndDate)  OR (PL.ArrivalGreenwichDTTM >= @StartDate AND PL.ArrivalGreenwichDTTM <=@EndDate)  
        OR  (PL.DepartureGreenwichDTTM <=@EndDate  AND PL.ArrivalGreenwichDTTM >= @StartDate))
        AND	PL.DepartureGreenwichDTTM >= DATEADD(day,-31,@StartDate) AND PL.DepartureGreenwichDTTM <= @EndDate
		AND PM.RecordType <> 'M' AND PM.CustomerID=@CustomerID  
		AND PL.LegNUM IS NOT NULL
		and CW.IsStatus = 1
	END

	ELSE IF (@DisplayTimeBase = 'Home')
	
	BEGIN
		INSERT INTO #CrewPlannerData

		SELECT 
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM,
		PM.FleetID FleetID,		
		F.TailNum TailNum, 
		PM.TripNUM TripNUM,
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID, 
		PL.DutyTYPE DutyTYPE, 
		PM.RecordType RecordType, 
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal, 
		PL.DepartureGreenwichDTTM DepartureGreenwichDTTM,
		PL.ArrivalGreenwichDTTM ArrivalGreenwichDTTM,
		PL.HomeDepartureDTTM HomeDepartureDTTM,
		PL.HomeArrivalDTTM HomeArrivalDTTM, 
		PL.LastUpdTS LegLastUpdTS, 
		PM.LastUpdTS TripLastUpdTS,
		PL.HomeDepartureDTTM DepartureDTTM,		-- Change this to L.DepartureGreenwichDTTM if UTC is selected or L.HomeDepartureDTTM if Home
		PL.HomeArrivalDTTM ArrivalDTTM,			-- Similar to above, DepartureDTTM		
		PM.TripStatus TripStatus,
		PL.PassengerRequestorID PassengerRequestorID,
		VR.IsInActive IsInActive,
		D.DepartmentID DepartmentID,
		C.ClientID ClientID,
		FC.FlightCategoryID FlightCategoryID,
		AD.AircraftDutyID AircraftDutyID,
		HM.HomebaseID HomebaseID,
		PC.CrewID CrewID,
		CW.IsFixedWing IsFixedWing, 
		CW.IsRotaryWing IsRotaryWing,
		CW.IsStatus CrewStatus,
		CD.DutyTypeID CrewDutyID,
		CW.CrewCD,
		F.ForeGrndCustomColor as AircraftForeColor,
		F.BackgroundCustomColor as AircraftBackColor,
		PM.IsLog IsLog,
		PM.IsPrivate IsPrivateTrip,
		PL.IsPrivate IsPrivateLeg,
		PM.Notes,
		
		PM.LastUpdUID, 
		PL.ArriveICAOID AS ArrivalAirportId,A.IcaoID AS ArrivalICAOID,  A.CityName as ArrivalCity,A.AirportName as ArrivalAirportName, A.CountryName as ArrivalCountry,  
        PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
        PL.FlightPurpose, CONVERT(INT,F.MaximumPassenger) - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PA.PassengerRequestorCD,PL.FlightNUM, PL.FuelLoad, PL.OutbountInstruction,
         
        HB.IcaoID HomebaseCD, CONVERT(numeric(14,3),0.0) CumulativeETE, PL.ElapseTM, F.Notes as FleetNotes, D.DepartmentCD, D.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, C.ClientCD,  
        FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
        AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,
        PM.TripDescription as [Description], PC.CrewLastName,PC.CrewFirstName,PC.CrewMiddleName, Cw.Notes as CrewNotes,     
        CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor, CD.ForeGrndCustomColor as CrewDutyTypeForeColor,  
        ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames], TE.TotalETE
	
		--INTO #CrewPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID			
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID		
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
		INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID 
		--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
		WHERE PL.HomeDepartureDTTM is not null AND PL.HomeArrivalDTTM is not null
        --AND PL.HomeDepartureDTTM <= PL.HomeArrivalDTTM
        AND ((PL.HomeDepartureDTTM >= @StartDate AND PL.HomeDepartureDTTM <=@EndDate)  OR (PL.HomeArrivalDTTM >= @StartDate AND PL.HomeArrivalDTTM <=@EndDate)  
        OR  (PL.HomeDepartureDTTM <=@EndDate  AND PL.HomeArrivalDTTM >= @StartDate))
		AND PM.CustomerID=@CustomerID  
		AND PL.LegNUM IS NOT NULL AND
		 PL.HomeDepartureDTTM >= DATEADD(day,-31,@StartDate) AND PL.HomeDepartureDTTM <= @EndDate
		AND PM.RecordType <> 'M'
		and CW.IsStatus = 1

	END	

	ELSE	
	BEGIN 
		INSERT INTO #CrewPlannerData
		SELECT 
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM,
		PM.FleetID FleetID,
		F.TailNum TailNum, 
		PM.TripNUM TripNUM, 
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		PL.DutyTYPE DutyTYPE, 
		PM.RecordType RecordType, 
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,		
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal, 
		PL.DepartureGreenwichDTTM DepartureGreenwichDTTM,
		PL.ArrivalGreenwichDTTM ArrivalGreenwichDTTM,
		PL.HomeDepartureDTTM HomeDepartureDTTM,
		PL.HomeArrivalDTTM HomeArrivalDTTM, 
		PL.LastUpdTS LegLastUpdTS, 
		PM.LastUpdTS TripLastUpdTS,
		PL.DepartureDTTMLocal DepartureDTTM,		-- Change this to L.DepartureGreenwichDTTM if UTC is selected or L.HomeDepartureDTTM if Home  Similar to above, DepartureDTTM
		--(PL.DepartureDTTMLocal + (PL.ArrivalGreenwichDTTM - PL. DepartureGreenwichDTTM)) ArrivalDTTM,					-- TO SHOW legs having arrival time < depart time
		PL.ArrivalDTTMLocal ArrivalDTTM,
		
		PM.TripStatus TripStatus,
		PL.PassengerRequestorID PassengerRequestorID,
		VR.IsInActive IsInActive,
		D.DepartmentID DepartmentID,
		C.ClientID ClientID,
		FC.FlightCategoryID FlightCategoryID,
		AD.AircraftDutyID AircraftDutyID,
		HM.HomebaseID HomebaseID,
		
		PC.CrewID CrewID,
		CW.IsFixedWing IsFixedWing, 
		CW.IsRotaryWing IsRotaryWing,
		CW.IsStatus CrewStatus,
		CD.DutyTypeID CrewDutyID,
		CW.CrewCD,
		F.ForeGrndCustomColor as AircraftForeColor,
		F.BackgroundCustomColor as AircraftBackColor,
		PM.IsLog IsLog,
		PM.IsPrivate IsPrivateTrip,
		PL.IsPrivate IsPrivateLeg,
		PM.Notes,
		
		PM.LastUpdUID, 
		PL.ArriveICAOID AS ArrivalAirportId,A.IcaoID AS ArrivalICAOID,  A.CityName as ArrivalCity,A.AirportName as ArrivalAirportName, A.CountryName as ArrivalCountry,  
        PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry,  
        PL.FlightPurpose, CONVERT(INT,F.MaximumPassenger) - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PA.PassengerRequestorCD,PL.FlightNUM, PL.FuelLoad, PL.OutbountInstruction,

        HB.IcaoID HomebaseCD, CONVERT(numeric(14,3),0.0) CumulativeETE, PL.ElapseTM, F.Notes as FleetNotes, D.DepartmentCD, D.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, C.ClientCD,  
        FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor,  
        AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby,
        PM.TripDescription as [Description], PC.CrewLastName,PC.CrewFirstName,PC.CrewMiddleName, Cw.Notes as CrewNotes,     
        CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor, CD.ForeGrndCustomColor as CrewDutyTypeForeColor,  
        ''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames], TE.TotalETE

		--INTO #FleetPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change
		INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID		
		WHERE PL.DepartureDTTMLocal is not null AND PL.ArrivalDTTMLocal is not null 
        --AND PL.DepartureDTTMLocal <= PL.ArrivalDTTMLocal 
        AND ((PL.DepartureDTTMLocal >= @StartDate AND PL.DepartureDTTMLocal <=@EndDate)  OR (PL.ArrivalDTTMLocal >= @StartDate AND PL.ArrivalDTTMLocal <=@EndDate)  
        OR  (PL.DepartureDTTMLocal <=@EndDate  AND PL.ArrivalDTTMLocal >= @StartDate))
		AND PM.CustomerID=@CustomerID  
		AND PL.LegNUM IS NOT NULL AND PL.DepartureDTTMLocal >= DATEADD(day,-31,@StartDate) AND PL.DepartureDTTMLocal <= @EndDate
		AND PM.RecordType <> 'M'
		and CW.IsStatus = 1

		Update #CrewPlannerData
		Set ArrivalDTTM=DepartureDTTM,DepartureDTTM=ArrivalDTTM
		Where DATEDIFF(day,DepartureDTTM,ArrivalDTTM)<0 

	END



-- Insert  Rest Over Night(RON) Records.
--if (@TripStatus = 1)
--Begin
SELECT F.TailNum,PM.FleetID,PL.ArrivalDTTMLocal,PL.DepartureDTTMLocal,PL.DepartureGreenwichDTTM,PL.ArrivalGreenwichDTTM
		,PL.HomeDepartureDTTM,PL.HomeArrivalDTTM,DT.IcaoID As DepartICAO,AA.IcaoID As ArriveICAO,PM.TripID,PL.LegNUM
		,PM.TripNUM,PM.RecordType
		,CASE WHEN @DisplayTimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END AS DepartureDisplayTime
		,CASE WHEN @DisplayTimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END AS ArrivalDisplayTime
		,PL.LastUpdTS As LegLastUpdTS,PM.LastUpdTS As TripLastUpdTS
		,PL.ArriveICAOID ArriveICAOID
		,PL.DepartICAOID DepartICAOID
		,PM.TripStatus TripStatus
		,PL.PassengerRequestorID PassengerRequestorID
		,VR.IsInActive IsInActive
		,D.DepartmentID DepartmentID
		,C.ClientID ClientID
		,FC.FlightCategoryID FlightCategoryID
		,AD.AircraftDutyID AircraftDutyID
		,HM.HomebaseID HomebaseID
		,PC.CrewID CrewID
		,CW.IsFixedWing IsFixedWing
		,CW.IsRotaryWing IsRotaryWing
		,CW.IsStatus CrewStatus
		,CD.DutyTypeID CrewDutyID
		,CW.CrewCD
		,F.ForeGrndCustomColor as AircraftForeColor
		,F.BackgroundCustomColor as AircraftBackColor
		,PM.IsLog IsLog
		,PM.IsPrivate IsPrivateTrip
		,PL.IsPrivate IsPrivateLeg
		,PM.Notes
		
		,PM.LastUpdUID
		,PL.ArriveICAOID AS ArrivalAirportId,AA.IcaoID AS ArrivalICAOID,  AA.CityName as ArrivalCity,AA.AirportName as ArrivalAirportName, AA.CountryName as ArrivalCountry
        ,PL.DepartICAOID as DepartureAirportId,DT.IcaoID AS DepartureICAOID,DT.CityName as DepartureCity,DT.AirportName as DepartureAirportName, DT.CountryName as DepartureCountry
        ,PL.FlightPurpose, CONVERT(INT,F.MaximumPassenger) - PL.Passengertotal as ReservationAvailable, PL.PassengerTotal,PA.PassengerRequestorCD,PL.FlightNUM, PL.FuelLoad, PL.OutbountInstruction

        ,HB.IcaoID HomebaseCD, CONVERT(numeric(14,3),0.0) CumulativeETE, PL.ElapseTM, F.Notes as FleetNotes, D.DepartmentCD, D.DepartmentName, DA.AuthorizationID,DA.AuthorizationCD, C.ClientCD
        ,FC.FlightCatagoryCD, FC.FlightCatagoryDescription, FC.BackgroundCustomColor AS FlightCategoryBackColor, FC.ForeGrndCustomColor FlightCategoryForeColor
        ,AD.AircraftDutyCD, AD.AircraftDutyDescription, AD.BackgroundCustomColor as AircaftDutyBackColor, AD.ForeGrndCustomColor AircraftDutyForeColor,AD.IsAircraftStandby
        ,PM.TripDescription as [Description], PC.CrewLastName,PC.CrewFirstName,PC.CrewMiddleName, Cw.Notes as CrewNotes
        ,CD.DutyTypeCD as CrewDutyTypeCD, CD.DutyTypesDescription as CrewDutyTypeDescription, CD.BackgroundCustomColor as CrewDutyTypeBackColor, CD.ForeGrndCustomColor as CrewDutyTypeForeColor
        ,''[CrewCodes],''[CrewFullNames],''[PassengerCodes],''[PassengerNames], TE.TotalETE
INTO #CrewRONRecords
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID  AND PM.IsDeleted !=1 AND PL.IsDeleted != 1 AND PM.CustomerID=@CustomerID
		INNER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport AA ON AA.AirportID  = PL.ArriveICAOID
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @CustomerID  AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @CustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change
		INNER JOIN @TotalEteTemp TE ON PM.TRIPID=TE.TripID		
WHERE PM.CustomerID=@CustomerID


INSERT INTO #CrewPlannerData
SELECT 	

		9999 LegID, 
		A.TripID TripID, 
		9999 LegNUM,
		A.FleetID FleetID,
		A.TailNum TailNum, 
		A.TripNUM TripNUM,
		A.ArriveICAOID ArriveICAOID,
		A.DepartICAOID DepartICAOID,
		--'R' DutyTYPE, 
		A.TailNum DutyTYPE,
		'T' RecordType, 
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArriveICAO ELSE A.ArriveICAO END ArriveICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartICAO ELSE A.DepartICAO END DepartICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalDTTMLocal ELSE A.ArrivalDTTMLocal END AS DepartureDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureDTTMLocal ELSE B.DepartureDTTMLocal END ArrivalDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalGreenwichDTTM ELSE A.ArrivalGreenwichDTTM END AS DepartureGreenwichDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureGreenwichDTTM ELSE B.DepartureGreenwichDTTM END ArrivalGreenwichDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.HomeArrivalDTTM ELSE A.HomeArrivalDTTM END AS HomeDepartureDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.HomeDepartureDTTM ELSE B.HomeDepartureDTTM END HomeArrivalDTTM,

		B.LegLastUpdTS LegLastUpdTS, 
		B.TripLastUpdTS TripLastUpdTS,
		
		--A.ArrivalDTTMLocal DepartureDTTM,
		--B.DepartureDTTMLocal ArrivalDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN 
				case (@DisplayTimeBase)
					when 'UTC' then B.ArrivalGreenwichDTTM
					when 'Home' then B.HomeArrivalDTTM
					when 'Local' then B.ArrivalDTTMLocal
				end
			ELSE
				case (@DisplayTimeBase) 
					when 'UTC' then A.ArrivalGreenwichDTTM
					when 'Home' then A.HomeArrivalDTTM 
					when 'Local' then A.ArrivalDTTMLocal 
				end
			 END AS DepartureDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN 
				Case (@DisplayTimeBase) 
					when 'UTC' then A.DepartureGreenwichDTTM
					when 'Home' then A.HomeDepartureDTTM 
					when 'Local' then A.DepartureDTTMLocal 
				end 
			 ELSE
				Case (@DisplayTimeBase) 
					when 'UTC' then B.DepartureGreenwichDTTM
					when 'Home' then B.HomeDepartureDTTM 
					when 'Local' then B.DepartureDTTMLocal 
				 end 
			 END As ArrivalDTTM,

		A.TripStatus TripStatus,
		A.PassengerRequestorID PassengerRequestorID,
		A.IsInActive IsInActive,
		A.DepartmentID DepartmentID,
		A.ClientID ClientID,
		A.FlightCategoryID FlightCategoryID,
		--A.AircraftDutyID AircraftDutyID,
		(select top 1 AircraftDutyID from AircraftDuty where CustomerID=@CustomerID and AircraftDutyCD='R') AircraftDutyID,
		A.HomebaseID HomebaseID,
		A.CrewID,
		A.IsFixedWing,
		A.IsRotaryWing,
		A.CrewStatus,
		--A.CrewDutyID,
		(select top 1 DutyTypeID from CrewDutyType where CustomerID=@CustomerID and DutyTypeCD='R') CrewDutyID,
		A.CrewCD,
		A.AircraftForeColor,
		A.AircraftBackColor,
		A.IsLog IsLog,
		A.IsPrivateTrip,
		A.IsPrivateLeg, 
		A.Notes,
		A.LastUpdUID, 
		A.ArrivalAirportId,A.ArrivalICAOID,  A.ArrivalCity,A.ArrivalAirportName, A.ArrivalCountry,  
        A.DepartureAirportId,A.DepartureICAOID,A.DepartureCity,A.DepartureAirportName, A.DepartureCountry,  
        A.FlightPurpose, A.ReservationAvailable, A.PassengerTotal,A.PassengerRequestorCD,A.FlightNUM, A.FuelLoad, A.OutbountInstruction,
        
        A.HomebaseCD, A.CumulativeETE, 0 ElapseTM, A.FleetNotes, A.DepartmentCD, A.DepartmentName, A.AuthorizationID,A.AuthorizationCD, A.ClientCD,  
        A.FlightCatagoryCD, A.FlightCatagoryDescription, A.FlightCategoryBackColor, A.FlightCategoryForeColor,  
        A.AircraftDutyCD, A.AircraftDutyDescription, A.AircaftDutyBackColor, A.AircraftDutyForeColor,A.IsAircraftStandby, A.[Description], A.CrewLastName,A.CrewFirstName,A.CrewMiddleName, A.CrewNotes,     
        A.CrewDutyTypeCD, A.CrewDutyTypeDescription, A.CrewDutyTypeBackColor, A.CrewDutyTypeForeColor,  
        A.CrewCodes,A.CrewFullNames,A.PassengerCodes,A.PassengerNames,A.TotalETE
	
	FROM #CrewPlannerData A 
	--INNER JOIN  #CrewPlannerData B
	--ON A.TripID = B.TripID AND (A.LegNUM = B.LegNUM -1
	-- OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTM,A.ArrivalDTTM)<0)
	--) And A.CrewID=B.CrewID
	INNER JOIN #CrewRONRecords B
	ON A.TripID = B.TripID AND A.CrewID=B.CrewID AND (A.LegNUM = B.LegNUM -1
	 OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTMLocal,A.ArrivalDTTMLocal)<0)
	 OR (B.LegNUM=A.LegNUM-1 
			AND (((CASE WHEN @DisplayTimeBase='Local' THEN B.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) <= @StartDate AND (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) <=@StartDate)
				 OR
				 ((CASE WHEN @DisplayTimeBase='Local' THEN B.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) >= @EndDate AND (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) >=@EndDate)
				)
		)
	)
	WHERE A.RecordType = 'T'


-- Insert Rest Over Night(RON) Records for the Trips where a Leg Starts and Ends before StartDate & another Leg Starts and Ends after EndDate
INSERT INTO #CrewPlannerData
SELECT 	LegID,TripID,LegNUM,FleetID,TailNum,TripNUM,ArriveICAOID,DepartICAOID,DutyTYPE,RecordType,ArriveICAO,DepartICAO,DepartureDTTMLocal,ArrivalDTTMLocal
 ,DepartureGreenwichDTTM,ArrivalGreenwichDTTM,HomeDepartureDTTM,HomeArrivalDTTM,LegLastUpdTS,TripLastUpdTS,DepartureDTTM,ArrivalDTTM,TripStatus,PassengerRequestorID
 ,IsInActive,DepartmentID,ClientID,FlightCategoryID,AircraftDutyID,HomebaseID,CrewID,IsFixedWing,IsRotaryWing,CrewStatus,CrewDutyID,CrewCD
 ,AircraftForeColor,AircraftBackColor,IsLog,IsPrivateTrip,IsPrivateLeg,Notes,LastUpdUID
 ,ArrivalAirportId,ArrivalICAOID, ArrivalCity, ArrivalAirportName, ArrivalCountry
 ,DepartureAirportId, DepartureICAOID, DepartureCity ,DepartureAirportName, DepartureCountry
 ,FlightPurpose, ReservationAvailable, PassengerTotal, PassengerRequestorCD, FlightNUM, FuelLoad, OutbountInstruction
 ,HomebaseCD, CumulativeETE, 0 ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID, AuthorizationCD, ClientCD
 ,FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor
 ,AircraftDutyCD, AircraftDutyDescription, AircaftDutyBackColor, AircraftDutyForeColor, IsAircraftStandby, [Description]
 ,CrewLastName, CrewFirstName, CrewMiddleName, CrewNotes, CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor
 ,CrewCodes, CrewFullNames, PassengerCodes, PassengerNames, TotalETE
FROM
(SELECT
		9999 LegID, 
		A.TripID TripID, 
		9999 LegNUM,
		A.FleetID FleetID,
		A.TailNum TailNum, 
		A.TripNUM TripNUM,
		A.ArriveICAOID ArriveICAOID,
		A.DepartICAOID DepartICAOID,
		--'R' DutyTYPE, 
		A.TailNum DutyTYPE,
		'T' RecordType, 
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArriveICAO ELSE A.ArriveICAO END ArriveICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartICAO ELSE A.DepartICAO END DepartICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalDTTMLocal ELSE A.ArrivalDTTMLocal END AS DepartureDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureDTTMLocal ELSE B.DepartureDTTMLocal END ArrivalDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalGreenwichDTTM ELSE A.ArrivalGreenwichDTTM END AS DepartureGreenwichDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureGreenwichDTTM ELSE B.DepartureGreenwichDTTM END ArrivalGreenwichDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.HomeArrivalDTTM ELSE A.HomeArrivalDTTM END AS HomeDepartureDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.HomeDepartureDTTM ELSE B.HomeDepartureDTTM END HomeArrivalDTTM,

		B.LegLastUpdTS LegLastUpdTS, 
		B.TripLastUpdTS TripLastUpdTS,
		
		--A.ArrivalDTTMLocal DepartureDTTM,
		--B.DepartureDTTMLocal ArrivalDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN 
				case (@DisplayTimeBase)
					when 'UTC' then B.ArrivalGreenwichDTTM
					when 'Home' then B.HomeArrivalDTTM
					when 'Local' then B.ArrivalDTTMLocal
				end
			ELSE
				case (@DisplayTimeBase) 
					when 'UTC' then A.ArrivalGreenwichDTTM
					when 'Home' then A.HomeArrivalDTTM 
					when 'Local' then A.ArrivalDTTMLocal 
				end
			 END AS DepartureDTTM,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN 
				Case (@DisplayTimeBase) 
					when 'UTC' then A.DepartureGreenwichDTTM
					when 'Home' then A.HomeDepartureDTTM 
					when 'Local' then A.DepartureDTTMLocal 
				end 
			 ELSE
				Case (@DisplayTimeBase) 
					when 'UTC' then B.DepartureGreenwichDTTM
					when 'Home' then B.HomeDepartureDTTM 
					when 'Local' then B.DepartureDTTMLocal 
				 end 
			 END As ArrivalDTTM,

		A.TripStatus TripStatus,
		A.PassengerRequestorID PassengerRequestorID,
		A.IsInActive IsInActive,
		A.DepartmentID DepartmentID,
		A.ClientID ClientID,
		A.FlightCategoryID FlightCategoryID,
		--A.AircraftDutyID AircraftDutyID,
		(select top 1 AircraftDutyID from AircraftDuty where CustomerID=@CustomerID and AircraftDutyCD='R') AircraftDutyID,
		A.HomebaseID HomebaseID,
		A.CrewID,
		A.IsFixedWing,
		A.IsRotaryWing,
		A.CrewStatus,
		--A.CrewDutyID,
		(select top 1 DutyTypeID from CrewDutyType where CustomerID=@CustomerID and DutyTypeCD='R') CrewDutyID,
		A.CrewCD,
		A.AircraftForeColor,
		A.AircraftBackColor,
		A.IsLog IsLog,
		A.IsPrivateTrip,
		A.IsPrivateLeg, 
		A.Notes,
		A.LastUpdUID, 
		A.ArrivalAirportId,A.ArrivalICAOID,  A.ArrivalCity,A.ArrivalAirportName, A.ArrivalCountry,  
        A.DepartureAirportId,A.DepartureICAOID,A.DepartureCity,A.DepartureAirportName, A.DepartureCountry,  
        A.FlightPurpose, A.ReservationAvailable, A.PassengerTotal,A.PassengerRequestorCD,A.FlightNUM, A.FuelLoad, A.OutbountInstruction,
        
        A.HomebaseCD, A.CumulativeETE, 0 ElapseTM, A.FleetNotes, A.DepartmentCD, A.DepartmentName, A.AuthorizationID,A.AuthorizationCD, A.ClientCD,  
        A.FlightCatagoryCD, A.FlightCatagoryDescription, A.FlightCategoryBackColor, A.FlightCategoryForeColor,  
        A.AircraftDutyCD, A.AircraftDutyDescription, A.AircaftDutyBackColor, A.AircraftDutyForeColor,A.IsAircraftStandby, A.[Description], A.CrewLastName,A.CrewFirstName,A.CrewMiddleName, A.CrewNotes,     
        A.CrewDutyTypeCD, A.CrewDutyTypeDescription, A.CrewDutyTypeBackColor, A.CrewDutyTypeForeColor,  
        A.CrewCodes,A.CrewFullNames,A.PassengerCodes,A.PassengerNames,A.TotalETE,
		ROW_NUMBER() OVER (PARTITION BY A.FleetID,A.TripID,A.CrewID ORDER BY (CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE A.ArrivalDTTMLocal END) desc) AS ROW_NUM
		
	FROM #CrewRONRecords A
	INNER JOIN #CrewRONRecords B
	ON A.TripID = B.TripID AND A.CrewID=B.CrewID AND (A.LegNUM = B.LegNUM -1
	 OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTMLocal,A.ArrivalDTTMLocal)<0)
	 OR (B.LegNUM=A.LegNUM-1 
			AND (((CASE WHEN @DisplayTimeBase='Local' THEN B.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) <= @StartDate AND (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) <=@StartDate)
				 OR
				 ((CASE WHEN @DisplayTimeBase='Local' THEN B.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) >= @EndDate AND (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) >=@EndDate)
				)
		)
	)
	WHERE A.RecordType='T' AND
	(CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @DisplayTimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE (CASE WHEN @DisplayTimeBase='Local' THEN A.ArrivalDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN A.ArrivalGreenwichDTTM ELSE A.HomeArrivalDTTM END) END)<@StartDate
	AND
	(CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @DisplayTimeBase='Local' THEN A.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN A.DepartureGreenwichDTTM ELSE A.HomeDepartureDTTM END) ELSE (CASE WHEN @DisplayTimeBase='Local' THEN B.DepartureDTTMLocal WHEN @DisplayTimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) END)>@EndDate
	) C
	WHERE ROW_NUM=1

	DROP table #CrewRONRecords
--------------------------------

--End
	
	
-- Split the data for the required 30 date ranges 
SELECT 
	PlannerColumnPeriodStart,
	LegID, 
	TripID,
	LegNUM,  
	FleetID,
	TailNum, 
	TripNUM,
	ArriveICAOID,
	DepartICAOID,
	DutyTYPE,
	RecordType, 
	--ISNULL(NULLIF(RecordType, 'T'), '1') TripFlag,
	--WHEN (RecordType = 'T' AND (LegNUM = 1 OR LegNUM = 2 OR LegNUM = 9999)) THEN 1 
	CASE 		
		WHEN (RecordType = 'T' AND LegNUM = 1) THEN 1 
		ELSE 2
	END TripFlag,
	ArriveICAO,
	DepartICAO,
	DepartureDTTMLocal, 
	ArrivalDTTMLocal, 
	DepartureGreenwichDTTM,
	ArrivalGreenwichDTTM,
	HomeDepartureDTTM,
	HomeArrivalDTTM, 
	LegLastUpdTS, 
	TripLastUpdTS,
	
	CONVERT(BIT, 0) ConflictFlag,
	
	TripStatus,
	PassengerRequestorID,
	IsInActive,
	DepartmentID,
	ClientID,
	FlightCategoryID,
	AircraftDutyID,
	HomebaseID,
	CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,
	AircraftForeColor,
	AircraftBackColor,
	IsLog,
	IsPrivateTrip,
	IsPrivateLeg,	
	Notes,
	LastUpdUID, 
	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  
    DepartureAirportId,DepartureICAOID,DepartureCity,DepartureAirportName, DepartureCountry,  
    FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, OutbountInstruction,
    
    HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  
    AircraftDutyCD, AircraftDutyDescription, AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby,
    [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames, TotalETE
INTO #CrewPlannerDetail

FROM 
(	

	--:?? Performance
	--:?? Also check if ArrivalDTTM >= @DateRangeStart should be ArrivalDTTM > @DateRangeStart
	
SELECT @DateRangeStart1  PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
, ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS,	
 TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart2 AND ArrivalDTTM >= @DateRangeStart1 

UNION ALL

 SELECT @DateRangeStart2 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart3 AND ArrivalDTTM >= @DateRangeStart2

UNION ALL

 SELECT @DateRangeStart3 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart4 AND ArrivalDTTM >= @DateRangeStart3

UNION ALL

 SELECT @DateRangeStart4 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart5 AND ArrivalDTTM >= @DateRangeStart4

UNION ALL

 SELECT @DateRangeStart5 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart6 AND ArrivalDTTM >= @DateRangeStart5

UNION ALL

 SELECT @DateRangeStart6 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart7 AND ArrivalDTTM >= @DateRangeStart6

UNION ALL

 SELECT @DateRangeStart7 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart8 AND ArrivalDTTM >= @DateRangeStart7

UNION ALL

 SELECT @DateRangeStart8 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart9 AND ArrivalDTTM >= @DateRangeStart8

UNION ALL
 SELECT @DateRangeStart9 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart10 AND ArrivalDTTM >= @DateRangeStart9

UNION ALL

 SELECT @DateRangeStart10 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart11 AND ArrivalDTTM >= @DateRangeStart10

UNION ALL
 SELECT @DateRangeStart11 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart12 AND ArrivalDTTM >= @DateRangeStart11

UNION ALL

 SELECT @DateRangeStart12 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart13 AND ArrivalDTTM >= @DateRangeStart12

UNION ALL
 SELECT @DateRangeStart13 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart14 AND ArrivalDTTM >= @DateRangeStart13

UNION ALL

 SELECT @DateRangeStart14 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart15 AND ArrivalDTTM >= @DateRangeStart14

UNION ALL
 SELECT @DateRangeStart15 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart16 AND ArrivalDTTM >= @DateRangeStart15

UNION ALL

 SELECT @DateRangeStart16 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart17 AND ArrivalDTTM >= @DateRangeStart16

UNION ALL

 SELECT @DateRangeStart17 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart18 AND ArrivalDTTM >= @DateRangeStart17

UNION ALL
 SELECT @DateRangeStart18 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart19 AND ArrivalDTTM >= @DateRangeStart18

UNION ALL

 SELECT @DateRangeStart19 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart20 AND ArrivalDTTM >= @DateRangeStart19

UNION ALL
 SELECT @DateRangeStart20 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart21 AND ArrivalDTTM >= @DateRangeStart20

UNION ALL

 SELECT @DateRangeStart21 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart22 AND ArrivalDTTM >= @DateRangeStart21

UNION ALL
 SELECT @DateRangeStart22 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart23 AND ArrivalDTTM >= @DateRangeStart22

UNION ALL

 SELECT @DateRangeStart23 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart24 AND ArrivalDTTM >= @DateRangeStart23

UNION ALL

 SELECT @DateRangeStart24 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	

	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart25 AND ArrivalDTTM >= @DateRangeStart24

UNION ALL

 SELECT @DateRangeStart25 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
         CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart26 AND ArrivalDTTM >= @DateRangeStart25

UNION ALL

 SELECT @DateRangeStart26 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart27 AND ArrivalDTTM >= @DateRangeStart26

UNION ALL

 SELECT @DateRangeStart27 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart28 AND ArrivalDTTM >= @DateRangeStart27

UNION ALL

 SELECT @DateRangeStart28 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart29 AND ArrivalDTTM >= @DateRangeStart28

UNION ALL

 SELECT @DateRangeStart29 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM
 , ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @DateRangeStart30 AND ArrivalDTTM >= @DateRangeStart29

UNION ALL

 SELECT @DateRangeStart30 PlannerColumnPeriodStart, LegID, TripID, LegNUM, FleetID, TailNum, TripNUM, ArriveICAOID, DepartICAOID, DutyTYPE, RecordType, ArriveICAO, DepartICAO,
 DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM, ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, 
 LegLastUpdTS,  TripLastUpdTS
 ,TripStatus,	PassengerRequestorID,	IsInActive,	DepartmentID,	ClientID,	FlightCategoryID,	AircraftDutyID,	HomebaseID, CrewID, IsFixedWing, IsRotaryWing,CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog, IsPrivateTrip, IsPrivateLeg,	
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
 FROM #CrewPlannerData
 WHERE DepartureDTTM < @EndDate AND ArrivalDTTM >= @DateRangeStart30
 
) CrewPlannerData 

INSERT INTO @CumTemp(TripId,LegNUM, CumulativeETE)  
	SELECT 
		a.TripId, 	
		a.LegNUM,
		ISNULL(SUM(b.ElapseTM),0) ElapseTM 
	FROM 
		#CrewPlannerData a,
		(SELECT PL.LegID,PL.LegNUM,PL.TripID,PC.CrewID,PL.ElapseTM
		FROM PreflightLeg PL LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId) b --To show cumulative ETE on Trip Level instead of Date Range
		--#CrewPlannerData b
	WHERE a.TripId = b.TripId AND
		b.LegNUM <= a.LegNUM AND b.CrewId = a.CrewId
	Group By a.CrewId,a.TripId,a.LegNum,a.ElapseTM
	order by a.LegNUM 
	
	UPDATE t 
	SET t.CumulativeETE = c.CumulativeETE
	from  
	 #CrewPlannerDetail as t inner join @CumTemp as c on c.TripId = t.TripID and c.LegNum = t.LegNUM
	DROP TABLE #CrewPlannerData

	-- Identify the (PlannerColumnPeriodStart + TailNumber) which have more than one record -- for display purpose on the UI -- not used in query
	UPDATE #CrewPlannerDetail 
		SET ConflictFlag = 1
		FROM #CrewPlannerDetail A INNER JOIN 
		(
		
			SELECT 
					PlannerColumnPeriodStart, 
					CrewID
			FROM #CrewPlannerDetail
			--GROUP BY PlannerColumnPeriodStart, CrewID
			--HAVING COUNT(1) > 1
			GROUP BY PlannerColumnPeriodStart, CrewID, LegID
			HAVING LegID<>9999
		) B
		ON A.PlannerColumnPeriodStart = B.PlannerColumnPeriodStart AND 
		A.CrewID = B.CrewID


-- Final select
SELECT
		PlannerColumnPeriodStart,
		DATEADD(MINUTE, -1, DATEADD(HOUR, @DisplayInterval, PlannerColumnPeriodStart)) PlannerColumnPeriodEnd,
		TailNum,
		--ISNULL(CONVERT(VARCHAR, DutyTYPE), ArriveICAO) AppointmentDisplay, --??:Assumption: Duty Type is NULL for actual Trips. Validate!
		ISNULL(CONVERT(VARCHAR, DutyTYPE), TailNum) AppointmentDisplay, --??:Assumption: Duty Type is NULL for actual Trips. Validate!
		LegID, 
		TripID, 
		LegNUM, 
		TripNUM,
		FleetID,
		ArriveICAOID,
		DepartICAOID,
		DutyTYPE, 
		RecordType, 
		TripFlag,
		ArriveICAO,
		DepartICAO,		
		DepartureDisplayTime,
		ArrivalDisplayTime,		
		DepartureDTTMLocal, 
		ArrivalDTTMLocal, 
		DepartureGreenwichDTTM,
		ArrivalGreenwichDTTM,
		HomeDepartureDTTM,
		HomeArrivalDTTM,		
		
		TripStatus,
		PassengerRequestorID,
		IsInActive,
		DepartmentID,
		ClientID,
		FlightCategoryID,
		AircraftDutyID,
		HomebaseID,
		CrewID, 
		IsFixedWing, 
		IsRotaryWing,
		CrewStatus,
		CrewDutyID,
		CrewCD,
		AircraftForeColor,
		AircraftBackColor,
		IsLog,
		ConflictFlag,
		IsPrivateTrip,
		IsPrivateLeg,
		
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE
		
FROM ( 
-- Find out the first row for each (tail number + date range) ordering by Depature Date + Time and Updated Time Stamps
		SELECT 
			PlannerColumnPeriodStart,
			TailNum,
			LegID, 
			
			TripID, 
			LegNUM, 
			TripNUM,
			FleetID,
			ArriveICAOID,
			DepartICAOID,
			--DutyTYPE, 
			CASE 	
				WHEN (RecordType = 'T' and DutyTYPE = null) THEN ArriveICAO 		
				WHEN (RecordType = 'T' and DutyTYPE = '0')  THEN ArriveICAO 		
				WHEN (RecordType = 'T' and DutyTYPE = '1')  THEN ArriveICAO 		
				WHEN (RecordType = 'T' and DutyTYPE = '2')  THEN ArriveICAO 		
				ELSE DutyTYPE
			END DutyTYPE,
			RecordType, 
			TripFlag,
			ArriveICAO,
			DepartICAO,
			
			CASE(@DisplayTimeBase) 
		        WHEN 'Local' THEN DepartureDTTMLocal   
                WHEN 'Home'  THEN HomeDepartureDTTM  
                WHEN 'UTC'   THEN DepartureGreenwichDTTM 
            END as DepartureDisplayTime,  -- This is added to display original time     	          		
			
     	    CASE(@DisplayTimeBase) 
		        WHEN 'Local' THEN ArrivalDTTMLocal   
                WHEN 'Home'  THEN HomeArrivalDTTM  
    WHEN 'UTC'   THEN ArrivalGreenwichDTTM 
            END as ArrivalDisplayTime,  -- This is added to display original time
        
			DepartureDTTMLocal, 
			ArrivalDTTMLocal, 
			DepartureGreenwichDTTM,
			ArrivalGreenwichDTTM,
			HomeDepartureDTTM,
			HomeArrivalDTTM,
			ConflictFlag,
			
			TripStatus,
			PassengerRequestorID,
			IsInActive,
			DepartmentID,
			ClientID,
			FlightCategoryID,
			AircraftDutyID,
			HomebaseID,
			CrewID, 
			IsFixedWing, 
			IsRotaryWing,
			CrewStatus,CrewDutyID,CrewCD,AircraftForeColor,AircraftBackColor,IsLog,
			IsPrivateTrip,
		    IsPrivateLeg,
		    
	Notes, 	LastUpdUID, 	ArrivalAirportId, ArrivalICAOID,  ArrivalCity, ArrivalAirportName, ArrivalCountry,  DepartureAirportId,DepartureICAOID,
	DepartureCity,DepartureAirportName, DepartureCountry,  FlightPurpose, ReservationAvailable, PassengerTotal,PassengerRequestorCD,FlightNUM, FuelLoad, 
	OutbountInstruction, HomebaseCD, CumulativeETE, ElapseTM, FleetNotes, DepartmentCD, DepartmentName, AuthorizationID,AuthorizationCD, ClientCD,  
    FlightCatagoryCD, FlightCatagoryDescription, FlightCategoryBackColor, FlightCategoryForeColor,  AircraftDutyCD, AircraftDutyDescription, 
    AircaftDutyBackColor, AircraftDutyForeColor,IsAircraftStandby, [Description], CrewLastName,CrewFirstName,CrewMiddleName, CrewNotes,     
        CrewDutyTypeCD, CrewDutyTypeDescription, CrewDutyTypeBackColor, CrewDutyTypeForeColor,  
        CrewCodes,CrewFullNames,PassengerCodes,PassengerNames,TotalETE,
			--ROW_NUMBER() OVER (PARTITION BY PlannerColumnPeriodStart, FleetID ORDER BY TripFlag, DepartureDTTMLocal, LegLastUpdTS, TripLastUpdTS) AS ROWNUM
			ROW_NUMBER() OVER (PARTITION BY CrewID, PlannerColumnPeriodStart,LegNum ORDER BY TripFlag, DepartureDTTMLocal desc, LegLastUpdTS, TripLastUpdTS,LegNum) AS ROWNUM
   			FROM #CrewPlannerDetail where TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR TripStatus IS NULL
   	) P
WHERE
P.LegID Not in (Select A.LegID From #CrewPlannerDetail As A Where A.LegNUM='9999' And A.ConflictFlag=1 And A.PlannerColumnPeriodStart = P.PlannerColumnPeriodStart AND A.CrewID = P.CrewID)
DROP TABLE #CrewPlannerDetail
END
