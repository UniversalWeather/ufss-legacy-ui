
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightCrewHotelListByPreflightCrewHotelListID]    Script Date: 09/20/2013 00:09:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightCrewHotelListByPreflightCrewHotelListID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightCrewHotelListByPreflightCrewHotelListID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightCrewHotelListByPreflightCrewHotelListID]    Script Date: 09/20/2013 00:09:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





create Procedure [dbo].[spFlightPak_Preflight_GetPreflightCrewHotelListByPreflightCrewHotelListID]
(
@CustomerID BIGINT,
@PreflightCrewHotelListID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get GetPreflightCrewHotelListBy PreflightCrewHotelListID 
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightCrewHotelList PC 
 where 
 PC.PreflightCrewHotelListID = @PreflightCrewHotelListID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 




GO


