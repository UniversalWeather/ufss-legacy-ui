/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewRating]    Script Date: 08/07/2014 17:54:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddCrewRating]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddCrewRating]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewRating]    Script Date: 08/07/2014 17:54:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spFlightPak_AddCrewRating]    
(  
 @CrewID  BIGINT,  
 @CrewRatingID  BIGINT,   
 @CustomerID BIGINT,    
 @AircraftTypeID BIGINT,    
 @CrewRatingDescription VARCHAR(15),    
 @IsPilotinCommand BIT,    
 @IsSecondInCommand BIT,    
 @IsEngineer BIT,    
 @IsInstructor BIT,    
 @IsAttendant BIT,    
 @AsOfDT DATE,    
 @TimeINType NUMERIC(11, 3),    
 @Day1 NUMERIC(11, 3),    
 @Night1 NUMERIC(11, 3),    
 @Instrument NUMERIC(11, 3),    
 @Simulator NUMERIC(11, 3),    
 @PilotinCommand NUMERIC(11, 3),    
 @SecondinCommand NUMERIC(11, 3),    
 @FlightEngineer NUMERIC(11, 3),    
 @FlightInstructor NUMERIC(11, 3),    
 @FlightAttendant NUMERIC(11, 3),    
 @ClientID BIGINT,    
 @LastUpdUID VARCHAR(30),    
 @LastUpdTS DATETIME,    
 @IsQualINType135PIC BIT,    
 @IsQualINType135SIC BIT,    
 @PilotinCommandDayHrs NUMERIC(9, 1),    
 @PilotInCommandNightHrs NUMERIC(9, 1),    
 @PilotInCommandINSTHrs NUMERIC(9, 1),    
 @SecondInCommandDay NUMERIC(9, 1),    
 @SecondInCommandNight NUMERIC(9, 1),    
 @SecondInCommandInstr NUMERIC(9, 1),    
 @TotalTimeINTypeHrs NUMERIC(9, 1),    
 @TotalDayHrs NUMERIC(9, 1),    
 @TotalNightHrs NUMERIC(9, 1),    
 @TotalINSTHrs NUMERIC(9, 1),    
 @PilotInCommandTypeHrs NUMERIC(9, 1),    
 @TPilotinCommandDayHrs NUMERIC(9, 1),    
 @TPilotInCommandNightHrs NUMERIC(9, 1),    
 @TPilotInCommandINSTHrs NUMERIC(9, 1),    
 @SecondInCommandTypeHrs NUMERIC(9, 1),    
 @SecondInCommandDayHrs NUMERIC(9, 1),    
 @SecondInCommandNightHrs NUMERIC(9, 1),    
 @SecondInCommandINSTHrs NUMERIC(9, 1),    
 @OtherSimHrs NUMERIC(9, 1),    
 @OtherFlightEngHrs NUMERIC(9, 1),    
 @OtherFlightInstrutorHrs NUMERIC(9, 1),    
 @OtherFlightAttdHrs NUMERIC(9, 1),    
 @TotalUpDATEAsOfDT DATETIME,    
 @IsInActive BIT,    
 @IsCheckAirman BIT,    
 @IsCheckAttendant BIT,    
 @IsAttendantFAR91 BIT,    
 @IsAttendantFAR135 BIT,    
 @IsDeleted BIT,  
 @IsPIC121 BIT,  
 @IsPIC125 BIT,  
 @IsSIC121 BIT,  
 @IsSIC125 BIT,  
 @IsAttendant121 BIT,  
 @IsAttendant125 BIT ,
 @IsPIC91 BIT,
 @IsSIC91 BIT
)    
AS   
BEGIN   
SET NOCOUNT ON;    
  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @CrewRatingID  OUTPUT     
set @LastUpdTS= GETUTCDATE()  
    
--IF EXISTS (SELECT NULL FROM [CrewRating] WHERE CrewID = @CrewID)    
--BEGIN    
--    UPDATE [CrewRating] SET    [CrewID]=@CrewID,  
--          [CustomerID]=@CustomerID,  
--          [AircraftTypeID]=@AircraftTypeID,  
--          [CrewRatingDescription]=@CrewRatingDescription,  
--          [IsPilotinCommand]=@IsPilotinCommand,  
--          [IsSecondInCommand]=@IsSecondInCommand,  
--          [IsEngineer]=@IsEngineer,  
--          [IsInstructor]=@IsInstructor,  
--          [IsAttendant]=@IsAttendant,  
--          [AsOfDT]=@AsOfDT,  
--          [TimeInType]=@TimeInType,  
--          [Day1]=@Day1,  
--          [Night1]=@Night1,  
--          [Instrument]=@Instrument,  
--          [Simulator]=@Simulator,  
--          [PilotinCommand]=@PilotinCommand,  
--          [SecondinCommand]=@SecondinCommand,  
--          [FlightEngineer]=@FlightEngineer,  
--          [FlightInstructor]=@FlightInstructor,  
--          [FlightAttendant]=@FlightAttendant,  
--          [ClientID]=@ClientID,  
--          [LastUpdUID]=@LastUpdUID,  
--          [LastUpdTS]=@LastUpdTS,  
--          [IsQualInType135PIC]=@IsQualInType135PIC,  
--          [IsQualInType135SIC]=@IsQualInType135SIC,  
--          [PilotinCommandDayHrs]=@PilotinCommandDayHrs,  
--          [PilotInCommandNightHrs]=@PilotInCommandNightHrs,  
--          [PilotInCommandINSTHrs]=@PilotInCommandINSTHrs,  
--          [SecondInCommandDay]=@SecondInCommandDay,  
--          [SecondInCommandNight]=@SecondInCommandNight,  
--          [SecondInCommandInstr]=@SecondInCommandInstr,  
--          [TotalTimeInTypeHrs]=@TotalTimeInTypeHrs,  
--          [TotalDayHrs]=@TotalDayHrs,  
--          [TotalNightHrs]=@TotalNightHrs,  
--          [TotalINSTHrs]=@TotalINSTHrs,  
--          [PilotInCommandTypeHrs]=@PilotInCommandTypeHrs,  
--          [TPilotinCommandDayHrs]=@TPilotinCommandDayHrs,  
--          [TPilotInCommandNightHrs]=@TPilotInCommandNightHrs,  
--          [TPilotInCommandINSTHrs]=@TPilotInCommandINSTHrs,  
--          [SecondInCommandTypeHrs]=@SecondInCommandTypeHrs,  
--          [SecondInCommandDayHrs]=@SecondInCommandDayHrs,  
--          [SecondInCommandNightHrs]=@SecondInCommandNightHrs,  
--          [SecondInCommandINSTHrs]=@SecondInCommandINSTHrs,  
--          [OtherSimHrs]=@OtherSimHrs,  
--          [OtherFlightEngHrs]=@OtherFlightEngHrs,  
--          [OtherFlightInstrutorHrs]=@OtherFlightInstrutorHrs,  
--          [OtherFlightAttdHrs]=@OtherFlightAttdHrs,  
--          [TotalUpdateAsOfDT]=@TotalUpdateAsOfDT,  
--          [IsInActive]=@IsInActive,  
--          [IsCheckAirman]=@IsCheckAirman,  
--          [IsCheckAttendant]=@IsCheckAttendant,  
--          [IsAttendantFAR91]=@IsAttendantFAR91,  
--          [IsAttendantFAR135]=@IsAttendantFAR135,  
--          [IsDeleted]=@IsDeleted  
--CrewRatingID =@CrewRatingID,  
--          IsPIC121 = @IsPIC121,  
--          IsPIC125 =@IsPIC125,  
--         IsSIC121 = @IsSIC121,  
--          IsSIC125 =@IsSIC125,   
--         IsAttendant121 = @IsAttendant121,   
--       IsAttendant125 =   @IsAttendant125  
--             WHERE CrewID = @CrewID  
--END    
--ELSE    
--BEGIN    
 INSERT INTO [CrewRating] ( [CrewID]  
         ,[CustomerID]  
         ,[AircraftTypeID]  
         ,[CrewRatingDescription]  
         ,[IsPilotinCommand]  
         ,[IsSecondInCommand]  
         ,[IsEngineer]  
         ,[IsInstructor]  
         ,[IsAttendant]  
         ,[AsOfDT]  
         ,[TimeInType]  
         ,[Day1]  
         ,[Night1]  
         ,[Instrument]  
         ,[Simulator]  
         ,[PilotinCommand]  
         ,[SecondinCommand]  
         ,[FlightEngineer]  
         ,[FlightInstructor]  
         ,[FlightAttendant]  
         ,[ClientID]  
         ,[LastUpdUID]  
         ,[LastUpdTS]  
         ,[IsQualInType135PIC]  
         ,[IsQualInType135SIC]  
         ,[PilotinCommandDayHrs]  
         ,[PilotInCommandNightHrs]  
         ,[PilotInCommandINSTHrs]  
         ,[SecondInCommandDay]  
         ,[SecondInCommandNight]  
         ,[SecondInCommandInstr]  
         ,[TotalTimeInTypeHrs]  
         ,[TotalDayHrs]  
         ,[TotalNightHrs]  
         ,[TotalINSTHrs]  
         ,[PilotInCommandTypeHrs]  
         ,[TPilotinCommandDayHrs]  
         ,[TPilotInCommandNightHrs]  
         ,[TPilotInCommandINSTHrs]  
         ,[SecondInCommandTypeHrs]  
         ,[SecondInCommandDayHrs]  
         ,[SecondInCommandNightHrs]  
         ,[SecondInCommandINSTHrs]  
         ,[OtherSimHrs]  
         ,[OtherFlightEngHrs]  
         ,[OtherFlightInstrutorHrs]  
         ,[OtherFlightAttdHrs]  
         ,[TotalUpdateAsOfDT]  
         ,[IsInActive]  
         ,[IsCheckAirman]  
         ,[IsCheckAttendant]  
         ,[IsAttendantFAR91]  
         ,[IsAttendantFAR135]  
         ,[IsDeleted]  
         ,[CrewRatingID]  
         ,IsPIC121  
         ,IsPIC125  
         ,IsSIC121   
         ,IsSIC125   
         ,IsAttendant121   
         ,IsAttendant125  
         ,IsPIC91
         ,IsSIC91
)  
  VALUES( @CrewID,  
          @CustomerID,  
          @AircraftTypeID,  
          @CrewRatingDescription,  
          @IsPilotinCommand,  
          @IsSecondInCommand,  
          @IsEngineer,  
          @IsInstructor,  
          @IsAttendant,  
          @AsOfDT,  
          @TimeInType,  
          @Day1,  
          @Night1,  
          @Instrument,  
          @Simulator,  
          @PilotinCommand,  
          @SecondinCommand,  
          @FlightEngineer,  
          @FlightInstructor,  
          @FlightAttendant,  
          @ClientID,  
          @LastUpdUID,  
          @LastUpdTS,  
          @IsQualInType135PIC,  
          @IsQualInType135SIC,  
          @PilotinCommandDayHrs,  
          @PilotInCommandNightHrs,  
          @PilotInCommandINSTHrs,  
          @SecondInCommandDay,  
          @SecondInCommandNight,  
          @SecondInCommandInstr,  
          @TotalTimeInTypeHrs,  
          @TotalDayHrs,  
          @TotalNightHrs,  
          @TotalINSTHrs,  
          @PilotInCommandTypeHrs,  
          @TPilotinCommandDayHrs,  
          @TPilotInCommandNightHrs,  
          @TPilotInCommandINSTHrs,  
          @SecondInCommandTypeHrs,  
          @SecondInCommandDayHrs,  
          @SecondInCommandNightHrs,  
          @SecondInCommandINSTHrs,  
          @OtherSimHrs,  
          @OtherFlightEngHrs,  
          @OtherFlightInstrutorHrs,  
          @OtherFlightAttdHrs,  
          @TotalUpdateAsOfDT,  
          @IsInActive,  
          @IsCheckAirman,  
          @IsCheckAttendant,  
          @IsAttendantFAR91,  
          @IsAttendantFAR135,  
          @IsDeleted,  
          @CrewRatingID,  
          @IsPIC121,  
          @IsPIC125,  
          @IsSIC121,  
          @IsSIC125,   
          @IsAttendant121,   
          @IsAttendant125,
          @IsPIC91,
          @IsSIC91)   
END  
GO

