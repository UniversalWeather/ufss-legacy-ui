

/****** Object:  StoredProcedure [dbo].[spGetAllCrewForGrid]    Script Date: 06/17/2013 15:53:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewForGrid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewForGrid]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllCrewForGrid]    Script Date: 06/17/2013 15:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
      
CREATE procedure [dbo].[spGetAllCrewForGrid](@CustomerID bigint,@ClientID bigint)          
-- =============================================          
-- Author: Karthikeyan.s          
-- Create date: 08/05/2012          
-- Description: Get all the Crew List          
-- =============================================          
as          
SET NoCOUNT ON          
BEGIN          
IF(@ClientID>0)          
 BEGIN           
            
  SELECT     
  Crew.CustomerID  
  ,Crew.CrewCD  
  ,Crew.CrewID  
  ,rtrim(isnull(Crew.LastName,'')) as 'LastName'  
  ,rtrim(isnull(Crew.FirstName,'')) as 'FirstName'  
  ,rtrim(isnull(Crew.MiddleInitial,'')) as 'MiddleInitial'          
  ,isnull(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'          
  ,isnull(Crew.IsStatus,0) as 'IsStatus'           
  ,Crew.HomebaseID          
  ,Crew.LastUpdUID          
  ,Crew.LastUpdTS          
  ,dbo.GetCrewCheckListValue(Crew.CrewID) as 'CheckList'          
  ,Crew.PhoneNum     
  ,Crew.IsRotaryWing      
  ,Crew.IsFixedWing     
  ,Client.ClientCD as ClientCD    
  ,HomeBase.IcaoID as HomeBaseCD    
  ,Crew.BusinessPhone   
  ,Crew.CellPhoneNum  
  ,Crew.EmailAddress  
  ,Crew.BusinessFax  
  FROM Crew as Crew with(nolock)           
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Crew.HomebaseID      
  LEFT OUTER JOIN Client as Client on Client.ClientID = Crew.ClientID        
  WHERE Crew.CustomerID = @CustomerID AND Crew.ClientID = @ClientID AND Crew.CrewCD is not null and Crew.isdeleted = 0      
  ORDER BY Crew.CrewCD         
 end          
ELSE          
 BEGIN          
  SELECT   Crew.CustomerID  
  ,Crew.CrewCD  
  ,Crew.CrewID  
  ,rtrim(isnull(Crew.LastName,'')) as 'LastName'  
  ,rtrim(isnull(Crew.FirstName,'')) as 'FirstName'  
  ,rtrim(isnull(Crew.MiddleInitial,'')) as 'MiddleInitial'          
  ,isnull(LastName,'') + ', ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleInitial,'') as 'CrewName'          
  ,isnull(Crew.IsStatus,0) as 'IsStatus'           
  ,Crew.HomebaseID          
  ,Crew.LastUpdUID          
  ,Crew.LastUpdTS          
  ,dbo.GetCrewCheckListValue(Crew.CrewID) as 'CheckList'          
  ,Crew.PhoneNum    
  ,Crew.IsRotaryWing      
  ,Crew.IsFixedWing    
  ,Client.ClientCD as ClientCD    
  ,HomeBase.IcaoID as HomeBaseCD    
  ,Crew.BusinessPhone   
  ,Crew.CellPhoneNum  
  ,Crew.EmailAddress  
  ,Crew.BusinessFax        
  FROM Crew as Crew with(nolock)           
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Crew.HomebaseID     
  LEFT OUTER JOIN Client as Client on Client.ClientID = Crew.ClientID          
  WHERE Crew.CustomerID = @CustomerID AND Crew.CrewCD is not null and Crew.isdeleted = 0      
  order by Crew.CrewCD          
 END          
END      
    
    
GO


