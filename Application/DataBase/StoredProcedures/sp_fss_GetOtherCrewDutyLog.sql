

/****** Object:  StoredProcedure [dbo].[sp_fss_GetOtherCrewDutyLog]    Script Date: 03/25/2014 12:39:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetOtherCrewDutyLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetOtherCrewDutyLog]
GO



/****** Object:  StoredProcedure [dbo].[sp_fss_GetOtherCrewDutyLog]    Script Date: 03/25/2014 12:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_fss_GetOtherCrewDutyLog](@CustomerID bigint,@CrewID bigint,@AircraftID bigint)  
-- =============================================  
-- Author: Karthikeyan.S  
-- Create date: 17/03/2014  
-- Description: Get all the Other Crew  
-- =============================================  
as  
SET NoCOUNT ON  
BEGIN  
   
SELECT SimulatorID,  
 SimulatorLog,  
 PostflightSimulatorLog.CustomerID,  
 PostflightSimulatorLog.CrewID,  
 crew.CrewCD AS 'CrewCD',  
 crew.FirstName AS 'FirstName',  
 SessionDT,  
 PostflightSimulatorLog.AircraftID,  
 aircraft.AircraftCD,  
 DutyTYPE,  
 PostflightSimulatorLog.TakeOffDay,  
 PostflightSimulatorLog.TakeOffNight,  
 LandingDay,  
 LandingNight,  
 ApproachPrecision,  
 ApproachNonPrecision,  
 PostflightSimulatorLog.Instrument,  
 Night,  
 FlightHours,  
 DutyHours,  
 Specification1,  
 Sepcification2,  
 PostflightSimulatorLog.HomeBaseID,  
 LastUserID,  
 LastUptTS,  
 TripID,  
 Specification3,  
 Specification4,  
 PostflightSimulatorLog.DutyTypeID,  
 crewDutytype.DutyTypeCD,  
 PostflightSimulatorLog.AirportID,  
 airport.IcaoID,  
 PostflightSimulatorLog.ClientID,  
 client.ClientCD,  
 SimulatorDescription,  
 DepartureDTTMLocal,  
 ArrivalDTTMLocal,  
 DepartureTMGMT,  
 ArrivalTMGMT,  
 PostflightSimulatorLog.IsDeleted,  
 airport.ICAOID AS HomebaseCD  
FROM PostflightSimulatorLog with(nolock)   
LEFT OUTER JOIN Crew crew on crew.CrewID= PostflightSimulatorLog.CrewID  
LEFT OUTER JOIN CrewDutyType crewDutytype on crewDutytype.DutyTypeID= PostflightSimulatorLog.DutyTypeID  
LEFT OUTER JOIN Aircraft aircraft on aircraft.AircraftID= PostflightSimulatorLog.AircraftID  
LEFT OUTER JOIN Client client on client.ClientID= PostflightSimulatorLog.ClientID  
LEFT OUTER JOIN Company company on PostflightSimulatorLog.HomeBaseID = company.HomeBaseID  
LEFT OUTER JOIN Airport airport ON airport.AirportID = company.HomebaseAirportID  
WHERE PostflightSimulatorLog.CustomerID = @CustomerID AND isnull(PostflightSimulatorLog.IsDeleted,0) = 0  and PostflightSimulatorLog.CrewID = @CrewID and PostflightSimulatorLog.AircraftID = @AircraftID
end  
GO


