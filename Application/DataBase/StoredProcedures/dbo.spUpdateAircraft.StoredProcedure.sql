/****** Object:  StoredProcedure [dbo].[spUpdateAircraft]    Script Date: 03/28/2013 18:25:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAircraft]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAircraft]
GO

CREATE  procedure [dbo].[spUpdateAircraft]  
      (  
 @AircraftID bigint,  
 @CustomerID bigint,  
 @AircraftCD Varchar(10),  
 @AircraftDescription varchar(15) ,  
 @ChargeRate NUMERIC(17, 2) ,  
 @ChargeUnit char(1) ,  
 @PowerSetting char(1) ,  
 @PowerDescription Varchar(17) ,  
 @WindAltitude NUMERIC(5, 0) ,  
 @PowerSettings1Description Varchar(17) ,  
 @PowerSettings1TrueAirSpeed NUMERIC(3, 0) ,  
 @PowerSettings1HourRange NUMERIC(6, 3) ,  
 @PowerSettings1TakeOffBias NUMERIC(6, 3) ,  
 @PowerSettings1LandingBias NUMERIC(6, 3) ,  
 @PowerSettings2Description Varchar(17) ,  
 @PowerSettings2TrueAirSpeed NUMERIC(3, 0) ,  
 @PowerSettings2HourRange NUMERIC(6, 3) ,  
 @PowerSettings2TakeOffBias NUMERIC(6, 3) ,  
 @PowerSettings2LandingBias NUMERIC(6, 3) ,  
 @PowerSettings3Description Varchar(17) ,  
 @PowerSettings3TrueAirSpeed NUMERIC(3, 0) ,  
 @PowerSettings3HourRange NUMERIC(6, 3) ,  
 @PowerSettings3TakeOffBias NUMERIC(6, 3) ,  
 @PowerSettings3LandingBias NUMERIC(6, 3) ,  
 @IsFixedRotary char(1) ,  
 @ClientID bigint,  
 @CQChargeRate NUMERIC(17, 2) ,  
 @CQChargeUnit NUMERIC(1, 0) ,  
 @PositionRate NUMERIC(17, 2) ,  
 @PostionUnit NUMERIC(1, 0) ,  
 @StandardCrew NUMERIC(2, 0) ,  
 @StandardCrewRON NUMERIC(17, 2) ,  
 @AdditionalCrew NUMERIC(17, 2) ,  
 @AdditionalCrewRON NUMERIC(17, 2) ,  
 @CharterQuoteWaitTM NUMERIC(17, 2) ,  
 @LandingFee NUMERIC(17, 2) ,  
 @CostBy NUMERIC(1, 0) ,  
 @FixedCost NUMERIC(17, 2) ,  
 @PercentageCost NUMERIC(17, 2) ,  
 @TaxRON bit ,  
 @AdditionalCrewTax bit ,  
 @IsCharterQuoteWaitTax bit ,  
 @LandingTax bit ,  
 @DescriptionRON bit ,  
 @AdditionalCrewDescription bit ,  
 @MinDailyREQ NUMERIC(17, 2) ,  
 @LastUpdUID Varchar(30) ,  
 @LastUpdTS datetime ,  
 @AircraftSize NUMERIC(1, 0) ,  
 @AircraftTypeCD varchar(max) ,  
 --@IntlStdCrewNum NUMERIC(2, 0) ,  
 --@DomesticStdCrewNum NUMERIC(2, 0) ,  
 --@MinimumDayUseHrs NUMERIC(17, 2) ,  
 --@DailyUsageAdjTax bit ,  
 --@LandingFeeTax bit ,  
 @IsDeleted bit ,
 @IsInActive bit ,
 @MarginalPercentage numeric(7,2)  
)  
-- =============================================  
-- Author:RUKMINI M  
-- Create date: 10/4/2012  
-- Description: Update the AircraftType information  
-- =============================================  
as  
begin   
SET NoCOUNT ON  
BEGIN  
SET @LastUpdTS = GETUTCDATE()   
UPDATE [Aircraft]  
   SET  
 AircraftID = @AircraftID,  
 CustomerID = @CustomerID,  
 AircraftCD=@AircraftCD ,  
 AircraftDescription=@AircraftDescription,  
 ChargeRate=@ChargeRate ,  
 ChargeUnit=@ChargeUnit ,  
 PowerSetting=@PowerSetting ,  
 PowerDescription=@PowerDescription ,  
 WindAltitude=@WindAltitude ,  
 PowerSettings1Description=@PowerSettings1Description ,  
 PowerSettings1TrueAirSpeed=@PowerSettings1TrueAirSpeed ,  
 PowerSettings1HourRange=@PowerSettings1HourRange ,  
 PowerSettings1TakeOffBias=@PowerSettings1TakeOffBias ,  
 PowerSettings1LandingBias=@PowerSettings1LandingBias ,  
 PowerSettings2Description=@PowerSettings2Description ,  
 PowerSettings2TrueAirSpeed=@PowerSettings2TrueAirSpeed ,  
 PowerSettings2HourRange=@PowerSettings2HourRange ,  
 PowerSettings2TakeOffBias=@PowerSettings2TakeOffBias ,  
 PowerSettings2LandingBias=@PowerSettings2LandingBias ,  
 PowerSettings3Description=@PowerSettings3Description ,  
 PowerSettings3TrueAirSpeed=@PowerSettings3TrueAirSpeed ,  
 PowerSettings3HourRange=@PowerSettings3HourRange ,  
 PowerSettings3TakeOffBias=@PowerSettings3TakeOffBias ,  
 PowerSettings3LandingBias=@PowerSettings3LandingBias ,  
 IsFixedRotary=@IsFixedRotary ,  
 ClientID=@ClientID ,  
 CQChargeRate=@CQChargeRate ,  
 CQChargeUnit=@CQChargeUnit ,  
 PositionRate=@PositionRate ,  
 PostionUnit=@PostionUnit ,  
 StandardCrew=@StandardCrew ,  
 StandardCrewRON=@StandardCrewRON ,  
 AdditionalCrew=@AdditionalCrew ,  
 AdditionalCrewRON=@AdditionalCrewRON ,  
 CharterQuoteWaitTM=@CharterQuoteWaitTM ,  
 LandingFee=@LandingFee ,  
 CostBy=@CostBy ,  
 FixedCost=@FixedCost ,  
 PercentageCost=@PercentageCost ,  
 TaxRON=@TaxRON ,  
 AdditionalCrewTax=@AdditionalCrewTax ,  
 IsCharterQuoteWaitTax=@IsCharterQuoteWaitTax ,  
 LandingTax=@LandingTax ,  
 DescriptionRON=@DescriptionRON ,  
 AdditionalCrewDescription=@AdditionalCrewDescription ,  
 MinDailyREQ=@MinDailyREQ ,  
 LastUpdUID=@LastUpdUID ,  
 LastUpdTS =@LastUpdTS  ,  
 AircraftSize=@AircraftSize ,  
 AircraftTypeCD=@AircraftTypeCD ,  
 --IntlStdCrewNum=@IntlStdCrewNum ,  
 --DomesticStdCrewNum=@DomesticStdCrewNum ,  
 --MinimumDayUseHrs=@MinimumDayUseHrs ,  
 --DailyUsageAdjTax=@DailyUsageAdjTax ,  
 --LandingFeeTax=@LandingFeeTax ,  
 IsDeleted=@IsDeleted  ,
 IsInActive=@IsInActive,
 MarginalPercentage=@MarginalPercentage
 WHERE AircraftID = @AircraftID  
 END    
END  

GO


