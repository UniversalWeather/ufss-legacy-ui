
/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_CopyTripDetails]    Script Date: 03/01/2013 19:20:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_Preflight_CopyTripDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_Preflight_CopyTripDetails]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_Preflight_CopyTripDetails]    Script Date: 03/01/2013 19:20:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spFlightpak_Preflight_CopyTripDetails]     
 -- Add the parameters for the stored procedure here    
 @DepartureDate datetime,    
 @TripID Bigint,    
 @CustomerID Bigint,    
 @IsLogisticsToBeCopied bit,    
 @IsCrewToBeCopied bit,    
 @IsPAXToBeCopied bit,    
 @CopyORMove bit, 
 @IsAutoDispatch bit, 
 @IsRevision bit,
 @username varchar(30),
 @IsHistoryToBeCopied bit 
AS    
BEGIN    
 DECLARE  @NewTripID BIGINT       
 DECLARE  @NewTripNUM BIGINT 
 DECLARE  @OldTripNUM BIGINT 
 DECLARE  @DateDiffValue INT  
     
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripID OUTPUT        
 set @NewTripNUM = 0    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @NewTripNUM OUTPUT      
      
 select @NewTripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID    
     
 select @DateDiffValue = datediff (d, isnull(EstDepartureDT,GETDATE()),@DepartureDate), @OldTripNUM = TripNUM from PreflightMain where TripID = @TripID    
 
    
--Main    
 insert into PreflightMain    
  ([TripID]    
   ,[CustomerID]    
   ,[TripNUM]    
   ,[PreviousNUM]    
   ,[TripDescription]    
   ,[FleetID]    
   ,[DispatchNUM]    
   ,[TripStatus]    
   ,[EstDepartureDT]    
   ,[EstArrivalDT]    
   ,[RecordType]    
   ,[AircraftID]    
   ,[PassengerRequestorID]    
   ,[RequestorFirstName]    
   ,[RequestorMiddleName]    
   ,[RequestorLastName]    
   ,[RequestorPhoneNUM]    
   ,[DepartmentDescription]    
   ,[AuthorizationDescription]    
   ,[RequestDT]    
   ,[IsCrew]    
   ,[IsPassenger]    
   ,[IsQuote]    
   ,[IsScheduledServices]    
   ,[IsAlert]    
   ,[Notes]    
   ,[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,[IsAirportAlert]    
   ,[IsLog]    
   ,[BeginningGMTDT]    
   ,[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsPrivate]    
   ,[IsCorpReq]    
   ,[Acknowledge]    
   ,[DispatchNotes]    
   ,[TripRequest]    
   ,[WaitList]    
   ,[FlightNUM]    
   ,[FlightCost]    
   ,[AccountID]    
   ,[TripException]    
   ,[TripSheetNotes]    
   ,[CancelDescription]    
   ,[IsCompleted]    
   ,[IsFBOUpdFlag]    
   ,[RevisionNUM]    
   ,[RevisionDescriptioin]    
   ,[IsPersonal]    
   ,[DsptnName]    
   ,[ScheduleCalendarHistory]    
   ,[ScheduleCalendarUpdate]    
   ,[CommentsMessage]    
   ,[EstFuelQTY]    
   ,[CrewID]    
   ,[ReleasedBy]    
   ,[IsDeleted]    
   ,[VerifyNUM]    
   ,[APISSubmit]    
   ,[APISStatus]    
   ,[APISException]    
   ,[IsAPISValid]    
   ,[DepartmentID]    
   ,[AuthorizationID]    
   ,[DispatcherUserName]    
   ,[EmergencyContactID]
   ,[CQCustomerID]
   ,[IsTripCopied])    
     
     
  select @NewTripID    
   ,[CustomerID]    
   ,@NewTripNUM    
   ,[PreviousNUM]    
   ,[TripDescription]    
   ,[FleetID]    
   ,case when @IsAutoDispatch=0 THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END     
  -- ,case when rtrim(ltrim(isnull([DispatchNUM],'')))='' THEN  NULL ELSE cast(@NewTripNUM  as varchar(12))END     
   ,[TripStatus]    
   ,@DepartureDate    
   ,[EstArrivalDT]    
   ,[RecordType]    
   ,[AircraftID]    
   ,[PassengerRequestorID]    
   ,[RequestorFirstName]    
   ,[RequestorMiddleName]    
   ,[RequestorLastName]    
   ,[RequestorPhoneNUM]    
   ,[DepartmentDescription]    
   ,[AuthorizationDescription]    
   ,[RequestDT]    
   ,[IsCrew]    
   ,[IsPassenger]    
   ,[IsQuote]    
   ,[IsScheduledServices]    
   ,[IsAlert]    
   ,[Notes]    
   ,[LogisticsHistory]    
   ,[HomebaseID]    
   ,[ClientID]    
   ,[IsAirportAlert]    
   ,0    
   ,[BeginningGMTDT]    
   ,[EndingGMTDT]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsPrivate]    
   ,[IsCorpReq]    
   ,[Acknowledge]    
   ,[DispatchNotes]    
   ,[TripRequest]    
   ,[WaitList]    
   ,[FlightNUM]    
   ,[FlightCost]    
   ,[AccountID]    
   ,[TripException]    
   ,[TripSheetNotes]    
   ,[CancelDescription]    
   ,[IsCompleted]    
   ,[IsFBOUpdFlag]    
   ,case when @IsRevision=0  then null  else 1 end     
   --,case when ltrim(rtrim(isnull([RevisionNUM],0)))=0  then null  else 1 end     
   ,null --[RevisionDescriptioin]    
   ,[IsPersonal]    
   ,[DsptnName]    
   ,[ScheduleCalendarHistory]    
   ,[ScheduleCalendarUpdate]    
   ,[CommentsMessage]    
   ,[EstFuelQTY]    
   ,[CrewID]    
   ,[ReleasedBy]    
   ,[IsDeleted]    
   ,[VerifyNUM]    
   ,null--[APISSubmit]    
   ,null --[APISStatus]    
   ,null --[APISException]    
   ,null --[IsAPISValid]    
   ,[DepartmentID]    
   ,[AuthorizationID]    
   ,[DispatcherUserName]    
   ,[EmergencyContactID]    
   ,[CQCustomerID]
   ,1    
   from PreflightMain where TripID = @TripID    
     
 if (@CopyORMove=1)    
 begin    
  CREATE Table #TempLeg (    
  rownum int identity (1,1),    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempLeg (NewLegId,OldLegID ) select null, LegID from PreflightLeg where TripID = @TripID    
      
  Declare @Rownumber int    
  DECLARE  @NewLegID BIGINT        
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempLeg where NewLegId is null) >0    
  begin    
   Set @NewLegID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewLegID OUTPUT     
   update #TempLeg set NewLegId = @NewLegID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
     
     
--Legs    
  INSERT INTO [PreflightLeg]        
         ([LegID]    
    ,[TripID]    
    ,[CustomerID]    
    ,[TripNUM]    
    ,[LegNUM]    
    ,[DepartICAOID]    
    ,[ArriveICAOID]    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,[IsApproxTM]    
    ,[IsScheduledServices]    
    ,[DepartureDTTMLocal]    
    ,[ArrivalDTTMLocal]    
    ,[DepartureGreenwichDTTM]    
    ,[ArrivalGreenwichDTTM]    
    ,[HomeDepartureDTTM]    
    ,[HomeArrivalDTTM]    
    ,[GoTime]    
    ,[FBOID]    
    ,[LogBreak]    
    ,[FlightPurpose]    
    ,[PassengerRequestorID]    
    ,[RequestorName]    
    ,[DepartmentID]    
    ,[DepartmentName]    
    ,[AuthorizationID]    
    ,[AuthorizationName]    
    ,[PassengerTotal]   
    ,[SeatTotal]    
    ,[ReservationTotal]   
    ,[ReservationAvailable]   
    ,[WaitNUM]    
    ,[DutyTYPE]    
    ,[DutyHours]    
    ,[RestHours]    
    ,[FlightHours]    
    ,[Notes]    
    ,[FuelLoad]    
    ,[OutbountInstruction]    
    ,[DutyTYPE1]    
    ,[IsCrewDiscount]    
    ,[CrewDutyRulesID]    
    ,[IsDutyEnd]    
    ,[FedAviationRegNUM]    
    ,[ClientID]    
    ,[LegID1]    
    ,[WindReliability]    
    ,[OverrideValue]    
    ,[CheckGroup]    
    ,[AdditionalCrew]    
    ,[PilotInCommand]    
    ,[SecondInCommand]    
    ,[NextLocalDTTM]    
    ,[NextGMTDTTM]    
    ,[NextHomeDTTM]    
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,[CrewNotes]    
    ,[IsPrivate]    
    ,[WaitList]    
    ,[FlightNUM]    
    ,[FlightCost]    
    ,[CrewFuelLoad]    
    ,[IsDeleted]    
    ,[UWAID]    
    ,[USCrossing]    
    ,[Response]    
    ,[ConfirmID]    
    ,[CrewDutyAlert]    
    ,[AccountID]    
    ,[FlightCategoryID] 
    ,[CQCustomerID]  
	,[EstFuelQTY]
	,[FuelUnits]
   )        
       
    select  NewLegId    
    ,@NewTripID    
    ,[CustomerID]    
    ,[TripNUM]    
    ,[LegNUM]    
    ,[DepartICAOID]    
    ,[ArriveICAOID]    
    ,[Distance]    
    ,[PowerSetting]    
    ,[TakeoffBIAS]    
    ,[LandingBias]    
    ,[TrueAirSpeed]    
    ,[WindsBoeingTable]    
    ,[ElapseTM]    
    ,[IsDepartureConfirmed]    
    ,[IsArrivalConfirmation]    
    ,[IsApproxTM]    
    ,[IsScheduledServices]    
    ,case when [DepartureDTTMLocal] IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[DepartureDTTMLocal])END    
    ,case when [ArrivalDTTMLocal]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[ArrivalDTTMLocal])END    
    ,case when [DepartureGreenwichDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[DepartureGreenwichDTTM])END    
    ,case when [ArrivalGreenwichDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[ArrivalGreenwichDTTM])END    
    ,case when [HomeDepartureDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[HomeDepartureDTTM])END    
    ,case when [HomeArrivalDTTM]IS null THEN NULL ELSE DATEADD(d,@DateDiffValue,[HomeArrivalDTTM])END    
    ,[GoTime]    
    ,[FBOID]    
    ,[LogBreak]    
    ,[FlightPurpose]    
    ,[PassengerRequestorID]    
    ,[RequestorName]    
    ,[DepartmentID]   
    ,[DepartmentName]    
    ,[AuthorizationID]    
    ,[AuthorizationName]    
    ,case when @IsPAXToBeCopied=1 THEN [PassengerTotal] ELSE 0 END  
    ,case when @IsPAXToBeCopied=1 THEN [SeatTotal]  ELSE 0 END  
    ,case when @IsPAXToBeCopied=1 THEN [ReservationTotal] ELSE 0 END  
    ,case when @IsPAXToBeCopied=1 THEN [ReservationAvailable] ELSE 0 END   
    ,[WaitNUM]    
    ,[DutyTYPE]    
    ,[DutyHours]    
    ,[RestHours]    
    ,[FlightHours]    
    ,[Notes]    
    ,[FuelLoad]    
    ,[OutbountInstruction]    
    ,[DutyTYPE1]    
    ,[IsCrewDiscount]    
    ,[CrewDutyRulesID]    
    ,[IsDutyEnd]    
    ,[FedAviationRegNUM]    
    ,[ClientID]    
    ,[LegID1]    
    ,[WindReliability]    
    ,[OverrideValue]    
    ,[CheckGroup]    
    ,[AdditionalCrew]    
    ,[PilotInCommand]    
    ,[SecondInCommand]    
    ,[NextLocalDTTM]    
    ,[NextGMTDTTM]    
    ,[NextHomeDTTM]    
    ,[LastUpdUID]    
    ,[LastUpdTS]    
    ,[CrewNotes]    
    ,[IsPrivate]    
    ,[WaitList]    
    ,[FlightNUM]    
    ,[FlightCost]    
    ,[CrewFuelLoad]    
    ,[IsDeleted]    
    ,null --[UWAID]    
    ,[USCrossing]    
    ,[Response]    
    ,[ConfirmID]    
    ,[CrewDutyAlert]    
    ,[AccountID]    
    ,[FlightCategoryID]  
    ,[CQCustomerID]  
	,[EstFuelQTY]
	,[FuelUnits]
    from preflightleg PL, #TempLeg T    
    where PL.LegID = T.OldLegID    
 End    
--Trip Exception    

 --CREATE Table #TempException (    
 -- rownum int identity (1,1),    
 -- NewTripExceptionID Bigint,    
 -- OldTripExceptionID Bigint    
 -- )    
      
 -- insert into #TempException (NewTripExceptionID,OldTripExceptionID ) select null, TripExceptionID from PreflightTripException where TripID = @TripID    
      
 -- DECLARE  @NewTripExceptionID BIGINT        
 -- set @Rownumber = 1    
      
 -- while (select COUNT(1) from #TempException where NewTripExceptionID is null) >0    
 -- begin    
 --  Set @NewLegID =0    
 --  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripExceptionID OUTPUT     
 --  update #TempException set NewTripExceptionID = @NewTripExceptionID where rownum = @Rownumber    
 --  set @Rownumber = @Rownumber +1     
 -- End    
     
     
 -- --TripException    
 -- INSERT INTO [PreflightTripException]    
 --           ([TripExceptionID]    
 --           ,[TripID]    
 --           ,[CustomerID]    
 --           ,[ExceptionDescription]    
 --           ,[LastUpdUID]    
 --           ,[LastUpdTS]    
 --           ,[ExceptionTemplateID])    
       
 --   select  T.NewTripExceptionID    
 --            ,@NewTripID    
 --            ,[CustomerID]    
 --            ,[ExceptionDescription]    
 --            ,[LastUpdUID]    
 --            ,[LastUpdTS]    
 --            ,[ExceptionTemplateID]    
 --   from PreflightTripException PL, #TempException  T    
 --   where PL.TripExceptionID = T.OldTripExceptionID    
    
 -- Drop table #TempException     

--History
if (@IsHistoryToBeCopied =1)
begin
 CREATE Table #TempTripHistory (    
  rownum int identity (1,1),    
  NewTripHistoryID Bigint,    
  OldTripHistoryID Bigint    
  )    
      
  insert into #TempTripHistory (NewTripHistoryID,OldTripHistoryID ) select null, TripHistoryID from PreflightTripHistory where TripID = @TripID    
      
  DECLARE  @NewTripHistoryID BIGINT        
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempTripHistory where NewTripHistoryID is null) >0    
  begin    
   Set @NewTripHistoryID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripHistoryID OUTPUT     
   update #TempTripHistory set NewTripHistoryID = @NewTripHistoryID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
  
  Set @NewTripHistoryID =0    
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewTripHistoryID OUTPUT    
     
  
  INSERT INTO [PreflightTripHistory]    
            (
            [TripHistoryID] ,
	[TripID],
	[CustomerID],
	[HistoryDescription],
	[LastUpdUID],
	[LastUpdTS],
	[RevisionNumber]
            )    
       
    values   (@NewTripHistoryID    
             ,@NewTripID    
             ,@CustomerID    
             ,'Trip Details Copied From Trip No.: ' + CONVERT (varchar(30),@OldTripNUM)
             ,@username    
             ,GETUTCDATE()    
             ,1
           )  
  
  
  INSERT INTO [PreflightTripHistory]    
            (
            [TripHistoryID] ,
	[TripID],
	[CustomerID],
	[HistoryDescription],
	[LastUpdUID],
	[LastUpdTS],
	[RevisionNumber]
            )    
       
    select  T.NewTripHistoryID    
             ,@NewTripID    
             ,[CustomerID]    
             ,[HistoryDescription]    
             ,[LastUpdUID]    
             ,[LastUpdTS]    
             ,[RevisionNumber]
             
    from PreflightTripHistory PL, #TempTripHistory  T    
    where PL.TripHistoryID = T.OldTripHistoryID    
    
  Drop table #TempTripHistory 
end


--History
    
    
    
--Crew    
 if (@IsCrewToBeCopied = 1)    
 begin    
     
  CREATE Table #TempCrew (    
  rownum int identity (1,1),    
  NewPreflightCrewListID Bigint,    
  oldPreflightCrewListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCrew (NewPreflightCrewListID ,    
   oldPreflightCrewListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightCrewListID], NewLegId, OldLegID    
  from PreflightCrewList PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightCrewListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempCrew where NewPreflightCrewListID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCrewListID OUTPUT     
   update #TempCrew set NewPreflightCrewListID = @NewPreflightCrewListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightCrewList]    
        (    
      [PreflightCrewListID]    
        ,[LegID]    
        ,[CustomerID]    
        ,[CrewID]    
        ,[PassportID]    
        ,[VisaID]       
        ,[HotelName]    
        ,[CrewFirstName]    
        ,[CrewMiddleName]    
        ,[CrewLastName]    
        ,[DutyTYPE]    
        ,[OrderNUM]    
        ,[IsDiscount]    
        ,[LastUpdUID]    
        ,[LastUpdTS]    
        ,[IsBlackberrMailSent]    
        ,[IsNotified]    
        ,[IsDeleted]    
        ,[IsAdditionalCrew]      
        ,[Street]    
        ,[CityName]    
        ,[StateName]    
        ,[PostalZipCD]     
        ,[NoofRooms]    
        ,[ClubCard]    
        ,[IsRateCap]    
        ,[MaxCapAmount]            
        )    
     select       
              
        TC.NewPreflightCrewListID    
        ,TC.NewLegId    
        ,[CustomerID]    
        ,[CrewID]    
        ,[PassportID]    
        ,[VisaID]       
        ,[HotelName]    
        ,[CrewFirstName]    
        ,[CrewMiddleName]    
        ,[CrewLastName]    
        ,[DutyTYPE]    
        ,[OrderNUM]    
        ,[IsDiscount]    
        ,[LastUpdUID]    
        ,[LastUpdTS]    
        ,[IsBlackberrMailSent]    
        ,[IsNotified]    
        ,[IsDeleted]    
        ,[IsAdditionalCrew]      
        ,[Street]    
        ,[CityName]    
        ,[StateName]    
        ,[PostalZipCD]     
        ,[NoofRooms]          ,[ClubCard]    
        ,[IsRateCap]    
        ,[MaxCapAmount]     
            
        from [PreflightCrewList] PC, #TempCrew TC    
        where PC.PreflightCrewListID = TC.oldPreflightCrewListID    
            


--Crew Logistics Hotel List    
  CREATE Table #TempCrewPreflighthotelList (    
  rownum int identity (1,1),    
  NewPreflightHotelListID Bigint,    
  oldPreflightHotelListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCrewPreflighthotelList (NewPreflightHotelListID ,    
   oldPreflightHotelListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightHotelListID], NewLegId, OldLegID    
  from PreflightHotelList PH, #TempLeg TL    
  where PH.LegID = TL.OldLegID and PH.CrewPassengerType ='C'    
      
  DECLARE  @NewPreflightHotelListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempCrewPreflighthotelList where NewPreflightHotelListID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightHotelListID OUTPUT     
   update #TempCrewPreflighthotelList set NewPreflightHotelListID = @NewPreflightHotelListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    

INSERT INTO [dbo].[PreflightHotelList]    
       (	[PreflightHotelListID],  
	[PreflightHotelName], 
	[LegID],   
	[HotelID],   
	[AirportID],   
	[RoomType], 
	[RoomRent], 
	[RoomConfirm], 
	[RoomDescription], 
	[Street], 
	[CityName],
	[StateName], 
	[PostalZipCD], 
	[PhoneNum1], 
	[PhoneNum2], 
	[PhoneNum3], 
	[PhoneNum4], 
	[FaxNUM], 
	[LastUpdUID],
	[LastUpdTS],   
	[IsDeleted], 
	[HotelArrangedBy], 
	[Rate],
	[DateIn],   
	[DateOut],   
	[IsSmoking],   
	[RoomTypeID],   
	[NoofBeds],
	[IsAllCrewOrPax],   
	[IsCompleted],   
	[IsEarlyCheckIn],   
	[EarlyCheckInOption], 
	[SpecialInstructions],
	[ConfirmationStatus], 
	[Comments], 
	[CustomerID],   
	[Address1], 
	[Address2], 
	[Address3], 
	[MetroID],   
	[City], 
	[StateProvince], 
	[CountryID],   
	[PostalCode], 
	[Status], 
	[isArrivalHotel],   
	[isDepartureHotel],   
	[crewPassengerType],
	[NoofRooms],
	[ClubCard],
	[IsRateCap],
	[MaxCapAmount],
	[IsRequestOnly],
	[IsBookNight]	 
       )    
  select       
  TC.NewPreflightHotelListID,  
	[PreflightHotelName], 
	TC.newLegID,   
	[HotelID],   
	[AirportID],   
	[RoomType], 
	[RoomRent], 
	[RoomConfirm], 
	[RoomDescription], 
	[Street], 
	[CityName],
	[StateName], 
	[PostalZipCD], 
	[PhoneNum1], 
	[PhoneNum2], 
	[PhoneNum3], 
	[PhoneNum4], 
	[FaxNUM], 
	[LastUpdUID],
	[LastUpdTS],   
	[IsDeleted], 
	[HotelArrangedBy], 
	[Rate],
	[DateIn],   
	[DateOut],   
	[IsSmoking],   
	[RoomTypeID],   
	[NoofBeds],
	[IsAllCrewOrPax],   
	[IsCompleted],   
	[IsEarlyCheckIn],   
	[EarlyCheckInOption], 
	[SpecialInstructions],
	[ConfirmationStatus], 
	[Comments], 
	[CustomerID],   
	[Address1], 
	[Address2], 
	[Address3], 
	[MetroID],   
	[City], 
	[StateProvince], 
	[CountryID],   
	[PostalCode], 
	[Status], 
	[isArrivalHotel],   
	[isDepartureHotel],   
	[crewPassengerType],
	[NoofRooms],
	[ClubCard],
	[IsRateCap],
	[MaxCapAmount],
	[IsRequestOnly],
	[IsBookNight]	 
  from PreflightHotelList PC, #TempCrewPreflighthotelList TC    
  where PC.LegID = TC.OldLegID and PC.CrewPassengerType = 'C'    
  and PC.PreflightHotelListID = TC.oldPreflightHotelListID     
  
  
  --PreflightHotelCrewPassengerList 
	CREATE Table #TempPreflighthotelCrewMemberList (    
  rownum int identity (1,1),    
  NewPreflightHotelCrewPassengerListID Bigint,    
  OldPreflightHotelCrewPassengerListID Bigint,    
  NewPreflightHotelListID Bigint,    
  oldPreflightHotelListID Bigint    
  )    
      
	insert into #TempPreflighthotelCrewMemberList (NewPreflightHotelCrewPassengerListID ,    
   OldPreflightHotelCrewPassengerListID ,    
   NewPreflightHotelListID ,    
   oldPreflightHotelListID )    
  select null,  PreflightHotelCrewPassengerListID, NewPreflightHotelListID, oldPreflightHotelListID    
  from PreflightHotelCrewPassengerList PH, #TempCrewPreflighthotelList TL    
  where PH.PreflightHotelListID = TL.oldPreflightHotelListID 
      
	DECLARE  @NewPreflightHotelCrewPassengerListID BIGINT       
      
	set @Rownumber = 1    
	while (select COUNT(1) from #TempPreflighthotelCrewMemberList where NewPreflightHotelCrewPassengerListID is null) >0    
  begin    
   Set @NewPreflightHotelCrewPassengerListID = 0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightHotelCrewPassengerListID OUTPUT     
   update #TempPreflighthotelCrewMemberList set NewPreflightHotelCrewPassengerListID = @NewPreflightHotelCrewPassengerListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
	INSERT INTO [dbo].[PreflightHotelCrewPassengerList]    
       ([PreflightHotelCrewPassengerListID],
       	[PreflightHotelListID],
		[CrewID],
		[PassengerRequestorID],
		[CustomerID] ,
		[LastUpdUID] ,
		[LastUpdTS] 
       )    
  select       
	TC.NewPreflightHotelCrewPassengerListID,  
	TC.NewPreflightHotelListID, 
	[CrewID],
	[PassengerRequestorID],
	[CustomerID] ,
	[LastUpdUID] ,
	[LastUpdTS] 
	
  from PreflightHotelCrewPassengerList PC, #TempPreflighthotelCrewMemberList TC    
  where PC.PreflightHotelCrewPassengerListID = TC.OldPreflightHotelCrewPassengerListID 
    Drop Table #TempPreflighthotelCrewMemberList 
  --PreflightHotelCrewPassengerList Ends here
      
  Drop Table #TempCrewPreflighthotelList    
 --Crew Logistics Hotel List    
        
  --PreflightCrewHotelList          the below section needs to be removed
  CREATE Table #TempCrewHotelList (    
  rownum int identity (1,1),    
  NewPreflightCrewHotelListID Bigint,    
  oldPreflightCrewHotelListID Bigint,    
  NewPreflightCrewListID Bigint,    
  oldPreflightCrewListID Bigint    
  )    
      
  insert into #TempCrewHotelList (NewPreflightCrewHotelListID ,    
   oldPreflightCrewHotelListID ,    
   NewPreflightCrewListID ,    
   oldPreflightCrewListID )    
  select null,  [PreflightCrewHotelListID], NewPreflightCrewListID, oldPreflightCrewListID    
  from PreflightCrewHotelList PC, #TempCrew TL    
  where PC.PreflightCrewListID = TL.oldPreflightCrewListID    
      
  DECLARE  @NewPreflightCrewHotelListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempCrewHotelList  where NewPreflightCrewHotelListID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCrewHotelListID OUTPUT     
   update #TempCrewHotelList set NewPreflightCrewHotelListID = @NewPreflightCrewHotelListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightCrewHotelList]    
           ([PreflightCrewHotelListID]    
           ,[PreflightHotelName]    
           ,[PreflightCrewListID]    
           ,[HotelID]    
           ,[AirportID]    
           ,[RoomType]    
           ,[RoomRent]    
           ,[RoomConfirm]    
           ,[RoomDescription]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[HotelArrangedBy]    
           ,[Rate]    
           ,[DateIn]    
           ,[DateOut]    
           ,[IsSmoking]    
           ,[RoomTypeID]    
           ,[NoofBeds]    
           ,[IsAllCrew]    
           ,[IsCompleted]    
           ,[IsEarlyCheckIn]    
           ,[EarlyCheckInOption]    
           ,[SpecialInstructions]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]
		   ,[Status])    
  select       
              
     TC.NewPreflightCrewHotelListID    
           ,[PreflightHotelName]    
           ,TC.NewPreflightCrewListID    
           ,[HotelID]    
           ,[AirportID]    
           ,[RoomType]    
           ,[RoomRent]    
           ,[RoomConfirm]    
           ,[RoomDescription]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[HotelArrangedBy]    
           ,[Rate]    
           ,@DepartureDate    
           ,@DepartureDate    
           ,[IsSmoking]    
           ,[RoomTypeID]    
           ,[NoofBeds]    
           ,[IsAllCrew]    
           ,[IsCompleted]    
           ,[IsEarlyCheckIn]    
           ,[EarlyCheckInOption]    
           ,[SpecialInstructions]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]
		   ,[Status]    
            
  from [PreflightCrewHotelList] PC, #TempCrewHotelList TC    
  where PC.[PreflightCrewHotelListID] = TC.oldPreflightCrewHotelListID    
      
  Drop table #TempCrewHotelList          
  --PreflightCrewHotelList    
      
      
  -- Crew Logistics Transport 
  CREATE Table #TempCrewTransportList (    
  rownum int identity (1,1),    
  NewPreflightTransportID Bigint,    
  oldPreflightTransportID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCrewTransportList (NewPreflightTransportID ,    
   oldPreflightTransportID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightTransportID], NewLegId, OldLegID    
  from PreflightTransportList PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID and PC.CrewPassengerType ='C'    
      
  DECLARE  @NewPreflightTransportID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempCrewTransportList where NewPreflightTransportID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightTransportID OUTPUT     
   update #TempCrewTransportList set NewPreflightTransportID = @NewPreflightTransportID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightTransportList]    
           ([PreflightTransportID]    
           ,[PreflightTransportName]    
           ,[LegID]    
           ,[TransportID]    
           ,[IsArrivalTransport]    
           ,[IsDepartureTransport]    
           ,[AirportID]    
           ,[CrewPassengerType]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[IsCompleted]
		   ,[Status])    
         
  select       
              
   TC.NewPreflightTransportID    
           ,[PreflightTransportName]    
           ,TC.NewLegId    
           ,[TransportID]    
           ,[IsArrivalTransport]    
           ,[IsDepartureTransport]    
           ,[AirportID]    
           ,[CrewPassengerType]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[IsCompleted]  
		   ,[Status]  
            
  from PreflightTransportList PC, #TempCrewTransportList TC    
  where PC.LegID = TC.OldLegID and PC.CrewPassengerType = 'C'    
  and PC.PreflightTransportID = TC.oldPreflightTransportID     
      
  Drop Table #TempCrewTransportList    
  -- Crew Logistics Transport    
  Drop table #TempCrew    
      
      
 End    
     
     
--Pax    
 if (@IsPAXToBeCopied = 1)    
 begin    
     
  CREATE Table #TempPax (    
  rownum int identity (1,1),    
  NewPreflightPassengerListID Bigint,    
  oldPreflightPassengerListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
  insert into #TempPax (NewPreflightPassengerListID ,    
   oldPreflightPassengerListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightPassengerListID], NewLegId, OldLegID    
  from PreflightPassengerList PP, #TempLeg TL    
  where PP.LegID = TL.OldLegID    
  DECLARE  @NewPreflightPassengerListID BIGINT       
  set @Rownumber = 1    
  while (select COUNT(1) from #TempPax where NewPreflightPassengerListID is null) >0    
  begin    
   Set @NewPreflightPassengerListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightPassengerListID OUTPUT     
   update #TempPax set NewPreflightPassengerListID = @NewPreflightPassengerListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
  INSERT INTO [PreflightPassengerList]    
           (    
            [PreflightPassengerListID]    
           ,[LegID]    
           ,[CustomerID]    
           ,[PassengerID]    
           ,[PassportID]    
           ,[VisaID]    
           ,[HotelName]    
           ,[PassengerFirstName]    
           ,[PassengerMiddleName]    
           ,[PassengerLastName]    
           ,[FlightPurposeID]    
           ,[PassportNUM]    
           ,[Billing]    
           ,[IsNonPassenger]    
           ,[IsBlocked]    
           ,[OrderNUM]    
           ,[ReservationNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[WaitList]    
           ,[AssociatedPassenger]    
           ,[TripStatus]    
           ,[TSADTTM]    
           ,[IsDeleted]    
           ,[NoofRooms]    
           ,[ClubCard]    
           ,[IsRateCap]    
           ,[MaxCapAmount]    
         ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           )    
     Select    
         
           TP.NewPreflightPassengerListID    
           ,TP.NewLegId    
           ,[CustomerID]    
           ,[PassengerID]    
           ,[PassportID]    
           ,[VisaID]    
           ,[HotelName]    
           ,[PassengerFirstName]    
           ,[PassengerMiddleName]    
           ,[PassengerLastName]    
           ,[FlightPurposeID]    
           ,[PassportNUM]    
           ,[Billing]    
           ,[IsNonPassenger]    
           ,[IsBlocked]    
           ,[OrderNUM]    
           ,[ReservationNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[WaitList]    
           ,[AssociatedPassenger]    
           ,[TripStatus]    
           ,[TSADTTM]    
           ,[IsDeleted]    
           ,[NoofRooms]    
           ,[ClubCard]    
           ,[IsRateCap]    
           ,[MaxCapAmount]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
               
           from [PreflightPassengerList] PP, #TempPax TP    
     where PP.PreflightPassengerListID = TP.oldPreflightPassengerListID    
     
     
     
      
  --PaxHotel    
  
  
  --PAX Logistics Hotel List    
  CREATE Table #TempPAXPreflighthotelList (    
  rownum int identity (1,1),    
  NewPreflightHotelListID Bigint,    
  oldPreflightHotelListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempPAXPreflighthotelList (NewPreflightHotelListID ,    
   oldPreflightHotelListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightHotelListID], NewLegId, OldLegID    
  from PreflightHotelList PH, #TempLeg TL    
  where PH.LegID = TL.OldLegID and PH.CrewPassengerType ='P'    
      
  DECLARE  @NewPAXPreflightHotelListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempPAXPreflighthotelList where NewPreflightHotelListID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPAXPreflightHotelListID OUTPUT     
   update #TempPAXPreflighthotelList set NewPreflightHotelListID = @NewPAXPreflightHotelListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    

INSERT INTO [dbo].[PreflightHotelList]    
       (	[PreflightHotelListID],  
	[PreflightHotelName], 
	[LegID],   
	[HotelID],   
	[AirportID],   
	[RoomType], 
	[RoomRent], 
	[RoomConfirm], 
	[RoomDescription], 
	[Street], 
	[CityName],
	[StateName], 
	[PostalZipCD], 
	[PhoneNum1], 
	[PhoneNum2], 
	[PhoneNum3], 
	[PhoneNum4], 
	[FaxNUM], 
	[LastUpdUID],
	[LastUpdTS],   
	[IsDeleted], 
	[HotelArrangedBy], 
	[Rate],
	[DateIn],   
	[DateOut],   
	[IsSmoking],   
	[RoomTypeID],   
	[NoofBeds],
	[IsAllCrewOrPax],   
	[IsCompleted],   
	[IsEarlyCheckIn],   
	[EarlyCheckInOption], 
	[SpecialInstructions],
	[ConfirmationStatus], 
	[Comments], 
	[CustomerID],   
	[Address1], 
	[Address2], 
	[Address3], 
	[MetroID],   
	[City], 
	[StateProvince], 
	[CountryID],   
	[PostalCode], 
	[Status], 
	[isArrivalHotel],   
	[isDepartureHotel],   
	[crewPassengerType],
	[NoofRooms],
	[ClubCard],
	[IsRateCap],
	[MaxCapAmount],
	[IsRequestOnly],
	[IsBookNight]		 
       )    
  select       
  TC.NewPreflightHotelListID,  
	[PreflightHotelName], 
	TC.newLegID,   
	[HotelID],   
	[AirportID],   
	[RoomType], 
	[RoomRent], 
	[RoomConfirm], 
	[RoomDescription], 
	[Street], 
	[CityName],
	[StateName], 
	[PostalZipCD], 
	[PhoneNum1], 
	[PhoneNum2], 
	[PhoneNum3], 
	[PhoneNum4], 
	[FaxNUM], 
	[LastUpdUID],
	[LastUpdTS],   
	[IsDeleted], 
	[HotelArrangedBy], 
	[Rate],
	[DateIn],   
	[DateOut],   
	[IsSmoking],   
	[RoomTypeID],   
	[NoofBeds],
	[IsAllCrewOrPax],   
	[IsCompleted],   
	[IsEarlyCheckIn],   
	[EarlyCheckInOption], 
	[SpecialInstructions],
	[ConfirmationStatus], 
	[Comments], 
	[CustomerID],   
	[Address1], 
	[Address2], 
	[Address3], 
	[MetroID],   
	[City], 
	[StateProvince], 
	[CountryID],   
	[PostalCode], 
	[Status], 
	[isArrivalHotel],   
	[isDepartureHotel],   
	[crewPassengerType],
	[NoofRooms],
	[ClubCard],
	[IsRateCap],
	[MaxCapAmount],	 
	[IsRequestOnly],
	[IsBookNight]            
  from PreflightHotelList PC, #TempPAXPreflighthotelList TC    
  where PC.LegID = TC.OldLegID and PC.CrewPassengerType = 'P'    
  and PC.PreflightHotelListID = TC.oldPreflightHotelListID     
      
    --PreflightHotelCrewPassengerList 
	CREATE Table #TempPreflighthotelPAXMemberList (    
  rownum int identity (1,1),    
  NewPreflightHotelCrewPassengerListID Bigint,    
  OldPreflightHotelCrewPassengerListID Bigint,    
  NewPreflightHotelListID Bigint,    
  oldPreflightHotelListID Bigint    
  )    
      
	insert into #TempPreflighthotelPAXMemberList (NewPreflightHotelCrewPassengerListID ,    
   OldPreflightHotelCrewPassengerListID ,    
   NewPreflightHotelListID ,    
   oldPreflightHotelListID )    
  select null,  PreflightHotelCrewPassengerListID, NewPreflightHotelListID, oldPreflightHotelListID    
  from PreflightHotelCrewPassengerList PH, #TempPAXPreflighthotelList TL    
  where PH.PreflightHotelListID = TL.oldPreflightHotelListID 
      
	DECLARE  @NewPreflightHotelPassengerListID BIGINT       
      
	set @Rownumber = 1    
	while (select COUNT(1) from #TempPreflighthotelPAXMemberList where NewPreflightHotelCrewPassengerListID is null) >0    
  begin    
   Set @NewPreflightHotelPassengerListID = 0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightHotelPassengerListID OUTPUT     
   update #TempPreflighthotelPAXMemberList set NewPreflightHotelCrewPassengerListID = @NewPreflightHotelPassengerListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
	INSERT INTO [dbo].[PreflightHotelCrewPassengerList]    
       ([PreflightHotelCrewPassengerListID],
       	[PreflightHotelListID],
		[CrewID],
		[PassengerRequestorID],
		[CustomerID] ,
		[LastUpdUID] ,
		[LastUpdTS] 
       )    
  select       
	TC.NewPreflightHotelCrewPassengerListID,  
	TC.NewPreflightHotelListID, 
	[CrewID],
	[PassengerRequestorID],
	[CustomerID] ,
	[LastUpdUID] ,
	[LastUpdTS] 
	
  from PreflightHotelCrewPassengerList PC, #TempPreflighthotelPAXMemberList TC    
  where PC.PreflightHotelCrewPassengerListID = TC.OldPreflightHotelCrewPassengerListID 
  
    Drop Table #TempPreflighthotelPAXMemberList 
  --PreflightHotelCrewPassengerList Ends here
    
      
  Drop Table #TempPAXPreflighthotelList    
  --PAX Logistics Hotel List    
  --The below code needs to be removed when pax page redesign is implemented        
  CREATE Table #TempPAXHotelList (    
  rownum int identity (1,1),    
  NewPreflightPassengerHotelListID Bigint,    
  oldPreflightPassengerHotelListID Bigint,    
  NewPreflightPassengerListID Bigint,    
  oldPreflightPassengerListID Bigint    
  )    
      
  insert into #TempPAXHotelList (NewPreflightPassengerHotelListID ,    
   oldPreflightPassengerHotelListID ,    
   NewPreflightPassengerListID ,    
   oldPreflightPassengerListID )    
  select null,  [PreflightPassengerHotelListID], NewPreflightPassengerListID, oldPreflightPassengerListID    
  from PreflightPassengerHotelList PC, #TempPax TL    
  where PC.PreflightPassengerListID = TL.oldPreflightPassengerListID    
      
  DECLARE  @NewPreflightPassengerHotelListID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempPAXHotelList  where NewPreflightPassengerHotelListID is null) >0    
  begin    
   Set @NewPreflightPassengerListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightPassengerHotelListID OUTPUT     
   update #TempPAXHotelList set NewPreflightPassengerHotelListID = @NewPreflightPassengerHotelListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightPassengerHotelList]    
           ([PreflightPassengerHotelListID]    
           ,[PreflightHotelName]    
           ,[PreflightPassengerListID]    
           ,[HotelID]    
           ,[AirportID]    
           ,[RoomType]    
           ,[RoomRent]    
           ,[RoomConfirm]    
           ,[RoomDescription]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[HotelArrangedBy]    
           ,[Rate]    
           ,[DateIn]    
           ,[DateOut]    
           ,[IsSmoking]    
           ,[RoomTypeID]    
           ,[NoofBeds]    
           ,[IsAllCrew]    
           ,[IsCompleted]    
           ,[IsEarlyCheckIn]    
           ,[EarlyCheckInOption]    
           ,[SpecialInstructions]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]
		   ,[Status])    
    
  select       
              
    
           TC.NewPreflightPassengerHotelListID    
           ,[PreflightHotelName]    
           ,TC.NewPreflightPassengerListID    
           ,[HotelID]    
           ,[AirportID]    
           ,[RoomType]    
           ,[RoomRent]    
           ,[RoomConfirm]    
           ,[RoomDescription]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[HotelArrangedBy]    
           ,[Rate]    
           ,@DepartureDate    
           ,@DepartureDate    
           ,[IsSmoking]    
           ,[RoomTypeID]    
           ,[NoofBeds]    
           ,[IsAllCrew]    
           ,[IsCompleted]    
           ,[IsEarlyCheckIn]    
           ,[EarlyCheckInOption]    
           ,[SpecialInstructions]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]
		   ,[Status]    
            
  from [PreflightPassengerHotelList] PC, #TempPAXHotelList TC    
  where PC.[PreflightPassengerHotelListID] = TC.oldPreflightPassengerHotelListID    
      
  Drop table #TempPAXHotelList          
  --PreflightPassengerHotelList   The above code needs to be removed when pax page redesign is implemented       
      
  --- PAX logistics Transport    
   
  CREATE Table #TempPAXTransportList (    
  rownum int identity (1,1),    
  NewPreflightTransportID Bigint,    
  oldPreflightTransportID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempPAXTransportList (NewPreflightTransportID ,    
   oldPreflightTransportID ,    
   NewLegId ,    
   OldLegID )    
  select null,  [PreflightTransportID], NewLegId, OldLegID    
  from PreflightTransportList PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID and PC.CrewPassengerType ='P'    
      
  --DECLARE  @NewPreflightTransportID BIGINT       
      
  set @Rownumber = 1    
      
  while (select COUNT(1) from #TempPAXTransportList where NewPreflightTransportID is null) >0    
  begin    
   Set @NewPreflightCrewListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightTransportID OUTPUT     
   update #TempPAXTransportList set NewPreflightTransportID = @NewPreflightTransportID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [PreflightTransportList]    
           ([PreflightTransportID]    
           ,[PreflightTransportName]    
           ,[LegID]    
           ,[TransportID]    
           ,[IsArrivalTransport]    
           ,[IsDepartureTransport]    
           ,[AirportID]    
           ,[CrewPassengerType]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[IsCompleted]
		   ,[Status])    
         
  select       
              
   TC.NewPreflightTransportID    
           ,[PreflightTransportName]    
           ,TC.NewLegId    
           ,[TransportID]    
           ,[IsArrivalTransport]    
           ,[IsDepartureTransport]    
           ,[AirportID]    
           ,[CrewPassengerType]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[IsCompleted] 
		   ,[Status]   
            
  from PreflightTransportList PC, #TempPAXTransportList TC    
  where PC.LegID = TC.OldLegID and PC.CrewPassengerType = 'P'     
  and PC.PreflightTransportID = TC.oldPreflightTransportID    
      
  Drop Table #TempPAXTransportList    
  ---PAX logistics Transport    
      
  Drop table #TempPax    
 End    
     
     
--Logistics    
 if (@IsLogisticsToBeCopied = 1)    
 begin    
  --Fbo    
  CREATE Table #TempFBO (    
  rownum int identity (1,1),    
  NewPreflightFBOID Bigint,    
  oldPreflightFBOID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempFBO (NewPreflightFBOID ,    
   oldPreflightFBOID ,    
   NewLegId ,    
   OldLegID )    
  select null,  PreflightFBOID, NewLegId, OldLegID    
  from PreflightFBOList PF, #TempLeg TL    
  where PF.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightFBOID BIGINT       
      
  set @Rownumber = 1    
     
  while (select COUNT(1) from #TempFBO where NewPreflightFBOID is null) >0    
  begin    
   Set @NewPreflightFBOID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightFBOID OUTPUT     
   update #TempFBO set NewPreflightFBOID = @NewPreflightFBOID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [dbo].[PreflightFBOList]    
           ([PreflightFBOID]    
           ,[PreflightFBOName]    
           ,[LegID]    
           ,[FBOID]    
           ,[IsArrivalFBO]    
           ,[IsDepartureFBO]    
           ,[AirportID]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[NoUpdated]    
           ,[FBOArrangedBy]    
           ,[IsCompleted]    
           ,[FBOInformation]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]    
           ,[Status])    
     Select    
           TF.NewPreflightFBOID    
           ,[PreflightFBOName]    
           ,TF.NewLegId    
           ,[FBOID]    
           ,[IsArrivalFBO]    
           ,[IsDepartureFBO]    
           ,[AirportID]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[NoUpdated]    
           ,[FBOArrangedBy]    
           ,[IsCompleted]    
           ,[FBOInformation]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]  
		   ,[Status]  
               
           from [PreflightFBOList] PF, #TempFBO TF    
     where PF.PreflightFBOID = TF.oldPreflightFBOID    
         
  Drop table #TempFBO    
      
  --Catering      
  CREATE Table #TempCatering (    
  rownum int identity (1,1),    
  NewPreflightCateringID Bigint,    
  oldPreflightCateringID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCatering (NewPreflightCateringID ,    
   oldPreflightCateringID ,    
   NewLegId ,    
   OldLegID )    
  select null,  PreflightCateringID, NewLegId, OldLegID    
  from PreflightCateringDetail PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightCateringID BIGINT       
      
  set @Rownumber = 1    
     
  while (select COUNT(1) from #TempCatering where NewPreflightCateringID is null) >0    
  begin    
   Set @NewPreflightCateringID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCateringID OUTPUT     
   update #TempCatering set NewPreflightCateringID = @NewPreflightCateringID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [dbo].[PreflightCateringDetail]    
           ([PreflightCateringID]    
           ,[LegID]    
           ,[CustomerID]    
           ,[AirportID]    
           ,[ArriveDepart]    
           ,[CateringID]    
           ,[IsCompleted]    
           ,[Cost]    
           ,[ContactName]    
           ,[ContactPhone]    
           ,[ContactFax]    
           ,[CateringConfirmation]    
           ,[CateringComments]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[IsUWAArranger]    
           ,[Status])    
     Select    
               
    TC.NewPreflightCateringID    
           ,TC.NewLegId    
           ,[CustomerID]    
           ,[AirportID]    
           ,[ArriveDepart]    
           ,[CateringID]    
           ,[IsCompleted]    
           ,[Cost]    
           ,[ContactName]    
           ,[ContactPhone]    
           ,[ContactFax]    
           ,[CateringConfirmation]    
           ,[CateringComments]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[IsUWAArranger]  
		   ,[Status]  
               
           from [PreflightCateringDetail] PC, #TempCatering TC    
     where PC.PreflightCateringID = TC.oldPreflightCateringID    
      
  Drop table #TempCatering    
  --CheckList  
  CREATE Table #TempCheckList (    
  rownum int identity (1,1),    
  NewPreflightCheckListID Bigint,    
  oldPreflightCheckListID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempCheckList (NewPreflightCheckListID ,    
   oldPreflightCheckListID ,    
   NewLegId ,    
   OldLegID )    
  select null,  PreflightCheckListID, NewLegId, OldLegID    
  from PreflightCheckList PC, #TempLeg TL    
  where PC.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightCheckListID BIGINT       
      
  set @Rownumber = 1    
     
  while (select COUNT(1) from #TempCheckList where NewPreflightCheckListID is null) >0    
  begin    
   Set @NewPreflightCheckListID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightCheckListID OUTPUT     
   update #TempCheckList set NewPreflightCheckListID = @NewPreflightCheckListID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [dbo].[PreflightCheckList]    
           ([PreflightCheckListID]    
           ,[LegID]  
           ,[CustomerID]  
     ,[CheckListID]      
           ,[ComponentCD]    
           ,[ComponentDescription]    
           ,[IsCompleted]  
           ,[LastUpdUID]    
           ,[LastUptTS]    
           ,[IsDeleted]    
           ,[CheckGroupID]  
           )    
     Select    
           TC.NewPreflightCheckListID    
           ,TC.NewLegId  
           ,[CustomerID]  
     ,[CheckListID]      
           ,[ComponentCD]    
 ,[ComponentDescription]    
           ,[IsCompleted]  
           ,[LastUpdUID]    
           ,[LastUptTS]    
           ,[IsDeleted]    
           ,[CheckGroupID]  
               
           from [PreflightCheckList] PC, #TempCheckList TC    
     where PC.PreflightCheckListID = TC.oldPreflightCheckListID  
         
  Drop table #TempCheckList  
     
 --Outbound Instruction  
  CREATE Table #TempOutboundInstruction (    
  rownum int identity (1,1),    
  NewPreflightTripOutboundID Bigint,    
  oldPreflightTripOutboundID Bigint,    
  NewLegId Bigint,    
  OldLegID Bigint    
  )    
      
  insert into #TempOutboundInstruction (NewPreflightTripOutboundID ,    
   oldPreflightTripOutboundID ,    
   NewLegId ,    
   OldLegID )    
  select null,  PreflightTripOutboundID, NewLegId, OldLegID    
  from PreflightTripOutbound PT, #TempLeg TL    
  where PT.LegID = TL.OldLegID    
      
  DECLARE  @NewPreflightTripOutboundID BIGINT       
      
  set @Rownumber = 1    
     
  while (select COUNT(1) from #TempOutboundInstruction where NewPreflightTripOutboundID is null) >0    
  begin    
   Set @NewPreflightTripOutboundID =0    
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @NewPreflightTripOutboundID OUTPUT     
   update #TempOutboundInstruction set NewPreflightTripOutboundID = @NewPreflightTripOutboundID where rownum = @Rownumber    
   set @Rownumber = @Rownumber +1     
  End    
      
  INSERT INTO [dbo].[PreflightTripOutbound]    
           ([PreflightTripOutboundID]    
           ,[LegID]  
           ,[CustomerID]  
     ,[OutboundInstructionNUM]      
           ,[OutboundDescription]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]  
           )    
     Select    
           PO.NewPreflightTripOutboundID    
           ,PO.NewLegId  
           ,[CustomerID]  
     ,[OutboundInstructionNUM]      
           ,[OutboundDescription]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]  
               
           from [PreflightTripOutbound] PT, #TempOutboundInstruction PO    
     where PT.PreflightTripOutboundID = PO.oldPreflightTripOutboundID  
         
  Drop table #TempOutboundInstruction  
 End    
     
     
 if (@CopyORMove=1)    
 begin    
  Drop table #TempLeg    
 end    
 select @NewTripID as TripID    
    
    
--exec spFlightpak_Preflight_CopyTripDetails '2012-05-05',10001270658,10001,1,1,1,1    
END  
  


GO


