/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_DeleteCRTransportList]    Script Date: 02/11/2013 15:56:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_DeleteCRTransportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRTransportList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRTransportList]
(@CRTransportListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
)
AS BEGIN

	SET NOCOUNT OFF;
	Delete from CRTransportList
	WHERE 
		CRTransportListID = @CRTransportListID and CustomerID=@CustomerID and CRLegID=@CRLegID

END
GO


