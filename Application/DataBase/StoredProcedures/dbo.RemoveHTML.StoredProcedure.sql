GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[RemoveHTML]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION dbo.RemoveHTML  
GO
CREATE FUNCTION dbo.RemoveHTML  
(  
 @strHTML  varchar(MAX), -- HTML Data  
 @intOptions  smallint -- [Unused, reserved for future expansion, pass as NULL]  
)  
RETURNS varchar(MAX)  
/* WITH ENCRYPTION */  
AS  
/*  
 * RemoveHTML Remove HTML Tags  
 *  SELECT dbo.RemoveHTML(@MyTextString, NULL)  
 *  
 * See: http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=148522  
 *  
 * Returns:  
 *  
 * varchar(MAX)  
 */  
BEGIN  
DECLARE @intStart int,  
 @intLength int  
  
 -- Replace common HTML entities with plain-text equivalent PRE  
 SELECT @strHTML = REPLACE(REPLACE(  
    @strHTML  
     , '<br>', CHAR(13) + CHAR(10))  
     , '<br />', CHAR(13) + CHAR(10)) -- This is NOT infalible!  
  
 SELECT @intLength = 1 -- Force first iteration  
   
 WHILE @intLength >= 1  
 BEGIN  
  -- Remove <...>  
  SELECT @intStart = CHARINDEX('<', @strHTML),  
   @intLength = CHARINDEX('>', @strHTML) - NullIf(@intStart, 0) + 1  
  IF @intLength >= 1  
  BEGIN  
   SELECT @strHTML = STUFF(@strHTML, @intStart, @intLength, '')  
  END  
 END  
  
 -- Replace common HTML entities with plain-text equivalent POST  
 SELECT @strHTML = REPLACE(REPLACE(REPLACE(REPLACE(  
    @strHTML  
     , '&lt;', '<')  
     , '&gt;', '>')  
     , '&nbsp;', ' ')  
     , '&amp;', '&') -- Last - to preserve nested ampersands  
   
 RETURN @strHTML  
  
/** TEST RIG  
  
SELECT '<xxx>Leading tag', dbo.RemoveHTML('<xxx>Leading tag', NULL) UNION ALL  
SELECT 'Trailing tag<xxx>', dbo.RemoveHTML('Trailing tag<xxx>', NULL) UNION ALL  
SELECT 'Embedded<xxx>tag', dbo.RemoveHTML('Embedded<xxx>tag', NULL) UNION ALL  
SELECT '&lt;', dbo.RemoveHTML('&lt;', NULL) UNION ALL  
SELECT '&gt;', dbo.RemoveHTML('&gt;', NULL) UNION ALL  
SELECT '&amp;', dbo.RemoveHTML('&amp;', NULL) UNION ALL  
SELECT '&amp;amp; nested', dbo.RemoveHTML('&amp;amp; nested', NULL) UNION ALL  
SELECT '&amp;lt; nested', dbo.RemoveHTML('&amp;lt; nested', NULL) UNION ALL  
SELECT '<BR>', ']' + dbo.RemoveHTML('<BR>', NULL) + '[' UNION ALL  
SELECT '<BR />', ']' + dbo.RemoveHTML('<BR />', NULL) + '[' UNION ALL  
SELECT 'LT < Only', dbo.RemoveHTML('LT < Only', NULL) UNION ALL  
SELECT 'GT > Only', dbo.RemoveHTML('GT > Only', NULL) UNION ALL  
SELECT 'Error1 A < B <TAG>', dbo.RemoveHTML('Error1 A < B <TAG>', NULL) UNION ALL  
SELECT 'OK2 <TAG> A > B', dbo.RemoveHTML('OK2 <TAG> A > B', NULL) UNION ALL  
SELECT 'NULL', dbo.RemoveHTML(NULL, NULL)  
  
**/  
--==================== RemoveHTML ====================--  
END  