/****** Object:  StoredProcedure [dbo].[spAddFleetInternationalData]    Script Date: 8/13/2015 8:33:52 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFleetInternationalData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFleetInternationalData]
GO

/****** Object:  StoredProcedure [dbo].[spAddFleetInternationalData]    Script Date: 8/13/2015 8:33:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spAddFleetInternationalData](           
            @CustomerID BIGINT             
           ,@FleetID BIGINT    
           ,@AircraftCD char(3)    
           ,@BuyAircraftAdditionalFeeDOM char(8)    
           ,@AircraftMake varchar(30)    
           ,@AircraftModel varchar(20)    
           ,@AircraftColor1 varchar(15)    
           ,@AircraftColor2 varchar(15)    
           ,@AircraftTrimColor varchar(15)    
           ,@OwnerLesse varchar(100)    
           ,@Addr1 varchar(25)    
           ,@Addr2 varchar(25)    
           ,@CityName varchar(25)    
           ,@StateName char(10)    
           ,@CountryID BIGINT  
           ,@PostalZipCD varchar(15)    
           ,@LastUpdUID varchar(30)    
           ,@LastUpdTS datetime    
           ,@ManufactureYear int    
           ,@PLastName varchar(20)    
           ,@PFirstName varchar(20)    
           ,@PMiddlename varchar(20)    
           ,@PhoneNum varchar(25)    
           ,@FaxNum varchar(25)    
           ,@EmailAddress varchar(100)    
           ,@Company varchar(100)    
           ,@OLastName varchar(20)    
           ,@OFirstName varchar(20)    
           ,@OMiddleName varchar(20)    
           ,@Address1 varchar(25)    
           ,@Address2 varchar(25)    
           ,@OCityName varchar(25)    
           ,@OStateName varchar(10)    
           ,@ZipCD varchar(15)    
           ,@OCountryID BIGINT  
           ,@OPhoneNum varchar(25)    
           ,@OFaxNum varchar(25)    
           ,@OEmailAddress varchar(100)    
           ,@IsDeleted bit
           ,@BusinessPhone varchar(25)
           ,@OPCellPhoneNum varchar(25)
           ,@OPHomeFax varchar(25)
           ,@OPCellPhoneNum2 varchar(25)
           ,@OPOtherPhone varchar(25)
           ,@OPPersonalEmail varchar(250)
           ,@OPOtherEmail varchar(250)
           ,@Address3 varchar(40)
           ,@OPBusinessPhone2 varchar(25)
           ,@OLCellPhoneNum varchar(25)
           ,@OLHomeFax varchar(25)
           ,@OLCellPhoneNum2 varchar(25)
           ,@OLOtherPhone varchar(25)
           ,@OLPersonalEmail varchar(250)
           ,@OLOtherEmail varchar(250)
           ,@Addr3 varchar(40)
		 ,@Year1 int 
		 ,@Year2 int 
	      ,@BuyAircraftAdditionalFeeDOM2 varchar(8)
           )    
-- =============================================    
-- Author:Badrinath    
-- Create date: 16/5/2012    
-- Altered by Mathes on 04-07-12  
-- Description: Insert the Fleet International  Data    
-- =============================================    
AS      
 BEGIN   
 SET NoCOUNT ON  
 DECLARE @FleetProfileInternationalDataID BIGINT  
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetProfileInternationalDataID OUTPUT  
  set @LastUpdTS = GETUTCDATE()   

   INSERT INTO [FleetPair]    
           (FleetProfileInternationalDataID  
           ,[CustomerID]    
           ,[FleetID]    
           ,[AircraftCD]    
           ,[BuyAircraftAdditionalFeeDOM]    
           ,[AircraftMake]    
           ,[AircraftModel]    
           ,[AircraftColor1]    
           ,[AircraftColor2]    
           ,[AircraftTrimColor]    
           ,[OwnerLesse]    
           ,[Addr1]    
           ,[Addr2]    
           ,[CityName]    
           ,[StateName]    
           ,[CountryID]    
           ,[PostalZipCD]    
           ,[LastUpdUID]    
           ,[LastUpdTS]     
           ,[ManufactureYear]    
           ,[PLastName]    
           ,[PFirstName]    
           ,[PMiddlename]    
           ,[PhoneNum]    
           ,[FaxNum]    
           ,[EmailAddress]    
           ,[Company]    
           ,[OLastName]    
           ,[OFirstName]    
           ,[OMiddleName]    
           ,[Address1]    
           ,[Address2]    
           ,[OCityName]    
           ,[OStateName]    
           ,[ZipCD]    
           ,[OCountryID]    
           ,[OPhoneNum]    
           ,[OFaxNum]    
           ,[OEmailAddress]    
           ,[IsDeleted]
           ,BusinessPhone
           ,OPCellPhoneNum
           ,OPHomeFax
           ,OPCellPhoneNum2
           ,OPOtherPhone
           ,OPPersonalEmail
           ,OPOtherEmail
           ,Address3 
           ,OPBusinessPhone2 
           ,OLCellPhoneNum 
           ,OLHomeFax 
           ,OLCellPhoneNum2 
           ,OLOtherPhone 
           ,OLPersonalEmail 
           ,OLOtherEmail 
           ,Addr3
		 ,Year1 
		 ,Year2 
	      ,BuyAircraftAdditionalFeeDOM2
             )    
     VALUES    
           (    
            @FleetProfileInternationalDataID   
           ,@CustomerID    
           ,@FleetID    
           ,@AircraftCD    
           ,@BuyAircraftAdditionalFeeDOM    
           ,@AircraftMake    
           ,@AircraftModel    
           ,@AircraftColor1    
           ,@AircraftColor2    
           ,@AircraftTrimColor    
           ,@OwnerLesse    
           ,@Addr1    
           ,@Addr2    
           ,@CityName    
           ,@StateName    
           ,@CountryID    
           ,@PostalZipCD    
           ,@LastUpdUID    
           ,@LastUpdTS    
           ,@ManufactureYear    
           ,@PLastName    
           ,@PFirstName    
           ,@PMiddlename    
           ,@PhoneNum    
           ,@FaxNum    
           ,@EmailAddress    
           ,@Company   
           ,@OLastName    
           ,@OFirstName    
           ,@OMiddleName    
           ,@Address1    
           ,@Address2    
           ,@OCityName    
           ,@OStateName    
           ,@ZipCD    
           ,@OCountryID    
           ,@OPhoneNum    
           ,@OFaxNum    
           ,@OEmailAddress    
           ,@IsDeleted
           ,@BusinessPhone
           ,@OPCellPhoneNum
           ,@OPHomeFax
           ,@OPCellPhoneNum2
           ,@OPOtherPhone
           ,@OPPersonalEmail
           ,@OPOtherEmail
           ,@Address3 
           ,@OPBusinessPhone2 
           ,@OLCellPhoneNum 
           ,@OLHomeFax 
           ,@OLCellPhoneNum2 
           ,@OLOtherPhone 
           ,@OLPersonalEmail 
           ,@OLOtherEmail 
           ,@Addr3
		 ,@Year1
		 ,@Year2 
	      ,@BuyAircraftAdditionalFeeDOM2
           )    
    
END
GO


