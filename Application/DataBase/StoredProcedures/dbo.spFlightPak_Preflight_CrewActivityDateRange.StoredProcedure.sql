/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_CrewActivityDateRange]    Script Date: 03/26/2013 19:00:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_CrewActivityDateRange]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_CrewActivityDateRange]
GO

CREATE procedure [dbo].[spFlightPak_Preflight_CrewActivityDateRange]    
(  
 @CustomerID Bigint,   
 @ClientID Bigint,   
 @CrewID Bigint,   
 @FromDate Datetime,  
 @ToDate Datetime,  
 @IsNonLogTripSheetIncludeCrewHIST bit,  
 @CrewFH char(1),
 @TripNUM Bigint
)  
AS  
SET NOCOUNT ON;  
  
--exec spFlightPak_Preflight_CrewActivity 10002,null,1000252485,'12/09/2012',false  ,'H'
  
BEGIN    
  
   DECLARE @lcGmtBegin as Datetime  

   
   IF(@CrewFH = 'H' and @IsNonLogTripSheetIncludeCrewHIST='false')
		BEGIN  
			
		   SELECT CRW.CREWID,  
		   CRW.CREWCD,PFM.TRIPNUM,  
		   CAST(PFL.ArrivalDTTMLocal as DATE) as DtyPeriod,  
		   FLT.TailNUM as TailNo,  
		   CAST(PFL.ArrivalGreenwichDTTM as DATE) as gmtarrdt,  
		   CASE WHEN PFM.RecordType = 'T'  THEN 'F'
		   ELSE PFL.DutyTYPE  
		   END AS Duty,    
		   PFL.DepartureDTTMLocal as LocDep,  
		   PFL.DepartureGreenwichDTTM as UtcDep,     
		   a.IcaoID as DepIcao,     
		   a1.IcaoID as ArrIcao,  
		   PFL.ArrivalDTTMLocal as LocArr,  
		   PFL.ArrivalGreenwichDTTM as UtcArr,  
		   PFL.ElapseTM,  
		   PFL.FlightHours  as FHrs,  
		   PFL.DutyHours as DHrs,  
		   PFL.RestHours as RHrs,  
		   (Select DATEDIFF(day,PFL.DepartureGreenwichDTTM,PFL.ArrivalGreenwichDTTM)) as CDays,  
		   PFC.DutyType as Pos,     
		   Count(PFC.CrewID) as CrwCnt,    
		   CDR.CrewDutyRuleCD AS Rules,   
		   PFL.IsDutyEnd,  
		   CASE WHEN  PFL.DutyTYPE1 =2  THEN 'Y'  
				WHEN  PFL.DutyTYPE1 =1  THEN 'N'  
				WHEN PFL.DutyTYPE1=null  THEN ''  
		   END as Intl,     
		   PFL.FedAviationRegNUM AS FAR,  
		   ACRT.AircraftTypeCD as TypeCode,   
		   PFL.LegID, PFL.LegNUM,  
			0000 AS crew_cnt,   
			SPACE(10) AS duty_hrs,   
			SPACE(10) AS rest_hrs,      
			SPACE(10) AS flt_hrs,  
			PFL.ArrivalDTTMLocal,  
			PFM.TripNUM as TSNumber,  
			POFM.LogNum as LogNumber  
		     
		     
		      
			FROM PreflightMain PFM   
			INNER JOIN PreflightLeg PFL  
			ON PFM.TripID = PFL.TripID  and PFM.CustomerID = PFL.CustomerID   
			INNER JOIN PreflightCrewlist PFC  
			ON PFL.LegID = PFC.LegID and PFL.CustomerID = PFC.CustomerID  
			LEFT OUTER JOIN Fleet FLT  
			ON PFM.FleetID = FLT.FleetID and PFM.CustomerID = FLT.CustomerID and FLT.FleetID is not null  
			and FLT.TailNUM is not null      
			INNER JOIN Crew CRW  
			ON PFC.CrewID = CRW.CrewID  and PFM.CustomerID = CRW.CustomerID   
			LEFT join Airport a on a.AirportID = PFL.DepartICAOID
			LEFT join Airport a1 on a1.AirportID = PFL.ArriveICAOID 
			LEFT OUTER JOIN Aircraft ACRT  
			ON PFM.AircraftID = ACRT.AircraftID  and PFM.CustomerID = ACRT.CustomerID   
			and ACRT.AircraftID is not null  and ACRT.AircraftTypeCD is not null  
			LEFT OUTER JOIN   
			(CrewChecklistDetail CCD        
			INNER JOIN CrewChecklist CCL  
			ON  CCL.CrewCheckID = CCD.CheckListID and  CCL.CustomerID = CCD.CustomerID)  
			ON PFC.CrewID = CCD.CrewID  and PFM.CustomerID = CCD.CustomerID   
			LEFT OUTER JOIN PostflightMain POFM on POFM.tripid =PFM.tripid and POFM.CustomerID=PFM.CustomerID  and ISNULL(POFM.IsDeleted,0)=0
			LEFT OUTER JOIN CrewDutyRules CDR on CDR.CrewDutyRulesID = PFL.CrewDutyRulesID and CDR.CustomerID = PFL.CustomerID AND ISNULL(CDR.IsDeleted,0)=0  
		     
		      
			WHERE PFM.TripID = PFL.TRIPID  
			AND PFM.TripNUM <> @TripNUM 
			AND PFL.LegID = PFC.LegID  
			AND (PFM.RecordType='T' or PFM.RecordType='C')  
			AND (PFM.TripStatus='T' or PFM.TripStatus='H')      
			AND PFC.CrewID = @CrewID   
			AND PFM.CustomerID=@CustomerID AND ISNULL(PFM.IsDeleted,0) = 0   
			AND ISNULL(PFL.IsDeleted,0) = 0  AND ISNULL(PFC.IsDeleted,0) = 0  
			AND ISNULL(FLT.IsDeleted,0) = 0 AND  ISNULL(CRW.IsDeleted,0)=0  
			AND ISNULL(ACRT.IsDeleted,0)=0 AND ISNULL(CCD.IsDeleted,0)=0  
			AND ISNULL(CCL.IsDeleted,0)=0 
			AND (PFM.IsLog IS NOT NULL) ANd (PFM.IsLog =1)     
			AND Cast(PFM.EstDepartureDT as date) between   CAST(@FromDate as date) AND 
			Cast(@ToDate as date)
			AND  
			ISNULL(PFM.ClientID,0) = ISNULL(@ClientID, ISNULL(PFM.ClientID, 0))     
		   Group by CRW.CREWID,CRW.CREWCD,PFM.TRIPNUM,PFL.ArrivalDTTMLocal,FLT.TailNUM,  
		   PFL.ArrivalGreenwichDTTM,PFM.RecordType,PFL.DepartureDTTMLocal,PFL.DepartureGreenwichDTTM,    
		   a.IcaoID , a1.IcaoID ,PFL.ArrivalDTTMLocal ,PFL.ArrivalGreenwichDTTM ,  
		   PFL.ElapseTM,PFL.FlightHours,PFL.DutyHours ,  
		   PFL.RestHours ,PFC.DutyType,PFC.CrewID,CDR.CrewDutyRuleCD,  
		   PFL.IsDutyEnd,PFL.DutyTYPE1,PFL.FedAviationRegNUM ,  
		   ACRT.AircraftTypeCD ,PFL.LegID, PFL.LegNUM,PFL.ArrivalDTTMLocal,  
		   PFM.TripNUM ,POFM.LogNum,PFL.DutyTYPE,PFM.TripID 
		     
			ORDER BY PFL.ArrivalDTTMLocal, PFM.TripID 
   END 
   ELSE IF(@CrewFH = 'F' and @IsNonLogTripSheetIncludeCrewHIST='false')
		BEGIN   
			
			   
			   SELECT CRW.CREWID,  
			   CRW.CREWCD,PFM.TRIPNUM,  
			   CAST(PFL.ArrivalDTTMLocal as DATE) as DtyPeriod,  
			   FLT.TailNUM as TailNo,  
			   CAST(PFL.ArrivalGreenwichDTTM as DATE) as gmtarrdt,  
			   CASE WHEN PFM.RecordType = 'T' THEN 'F'
			   ELSE PFL.DutyTYPE  
			   END AS Duty,    
			   PFL.DepartureDTTMLocal as LocDep,  
			   PFL.DepartureGreenwichDTTM as UtcDep,     
			   a.IcaoID as DepIcao,     
			   a1.IcaoID as ArrIcao,  
			   PFL.ArrivalDTTMLocal as LocArr,  
			   PFL.ArrivalGreenwichDTTM as UtcArr,  
			   PFL.ElapseTM,  
			   PFL.FlightHours  as FHrs,  
			   PFL.DutyHours as DHrs,  
			   PFL.RestHours as RHrs,  
			   (Select DATEDIFF(day,PFL.DepartureGreenwichDTTM,PFL.ArrivalGreenwichDTTM)) as CDays,  
			   PFC.DutyType as Pos,     
			   Count(PFC.CrewID) as CrwCnt,    
			   CDR.CrewDutyRuleCD AS Rules,   
			   PFL.IsDutyEnd,  
			   CASE WHEN  PFL.DutyTYPE1 =2  THEN 'Y'  
					WHEN  PFL.DutyTYPE1 =1  THEN 'N'  
					WHEN PFL.DutyTYPE1=null  THEN ''  
			   END as Intl,     
			   PFL.FedAviationRegNUM AS FAR,  
			   ACRT.AircraftTypeCD as TypeCode,   
			   PFL.LegID, PFL.LegNUM,  
				0000 AS crew_cnt,   
				SPACE(10) AS duty_hrs,   
				SPACE(10) AS rest_hrs,      
				SPACE(10) AS flt_hrs,  
				PFL.ArrivalDTTMLocal,  
				PFM.TripNUM as TSNumber,  
				POFM.LogNum as LogNumber  
			     
			     
			      
				FROM PreflightMain PFM   
				INNER JOIN PreflightLeg PFL  
				ON PFM.TripID = PFL.TripID  and PFM.CustomerID = PFL.CustomerID   
				INNER JOIN PreflightCrewlist PFC  
				ON PFL.LegID = PFC.LegID and PFL.CustomerID = PFC.CustomerID  
				LEFT OUTER JOIN Fleet FLT  
				ON PFM.FleetID = FLT.FleetID and PFM.CustomerID = FLT.CustomerID and FLT.FleetID is not null  
				and FLT.TailNUM is not null      
				INNER JOIN Crew CRW  
				ON PFC.CrewID = CRW.CrewID  and PFM.CustomerID = CRW.CustomerID   
				LEFT join Airport a on a.AirportID = PFL.DepartICAOID 
				--and a.CustomerID=PFL.CustomerID  
				LEFT join Airport a1 on a1.AirportID = PFL.ArriveICAOID 
				--and a1.CustomerID=PFL.CustomerID    
				LEFT OUTER JOIN Aircraft ACRT  
				ON PFM.AircraftID = ACRT.AircraftID  and PFM.CustomerID = ACRT.CustomerID   
				and ACRT.AircraftID is not null  and ACRT.AircraftTypeCD is not null  
				LEFT OUTER JOIN   
				(CrewChecklistDetail CCD        
				INNER JOIN CrewChecklist CCL  
				ON  CCL.CrewCheckID = CCD.CheckListID and  CCL.CustomerID = CCD.CustomerID)  
				ON PFC.CrewID = CCD.CrewID  and PFM.CustomerID = CCD.CustomerID   
				LEFT OUTER JOIN PostflightMain POFM on POFM.tripid =PFM.tripid and POFM.CustomerID=PFM.CustomerID   and ISNULL(POFM.IsDeleted,0)=0
				LEFT OUTER JOIN CrewDutyRules CDR on CDR.CrewDutyRulesID = PFL.CrewDutyRulesID and CDR.CustomerID = PFL.CustomerID AND ISNULL(CDR.IsDeleted,0)=0  
			     
			      
				WHERE PFM.TripID = PFL.TRIPID   
				AND PFM.TripNUM <> @TripNUM 
				AND PFL.LegID = PFC.LegID  
				AND (PFM.RecordType='T' or PFM.RecordType='C')  
				AND (PFM.TripStatus='T' or PFM.TripStatus='H')      
				AND PFC.CrewID = @CrewID   
				AND PFM.CustomerID=@CustomerID AND ISNULL(PFM.IsDeleted,0) = 0   
				AND ISNULL(PFL.IsDeleted,0) = 0  AND ISNULL(PFC.IsDeleted,0) = 0  
				AND ISNULL(FLT.IsDeleted,0) = 0 AND  ISNULL(CRW.IsDeleted,0)=0  
				AND ISNULL(ACRT.IsDeleted,0)=0 AND ISNULL(CCD.IsDeleted,0)=0  
				AND ISNULL(CCL.IsDeleted,0)=0  
				AND Cast(PFM.EstDepartureDT as date) between   CAST(@FromDate as date) AND 
				Cast(@ToDate as date)
				AND  
				ISNULL(PFM.ClientID,0) = ISNULL(@ClientID, ISNULL(PFM.ClientID, 0))     
			   Group by CRW.CREWID,CRW.CREWCD,PFM.TRIPNUM,PFL.ArrivalDTTMLocal,FLT.TailNUM,  
			   PFL.ArrivalGreenwichDTTM,PFM.RecordType,PFL.DepartureDTTMLocal,PFL.DepartureGreenwichDTTM,    
			   a.IcaoID , a1.IcaoID ,PFL.ArrivalDTTMLocal ,PFL.ArrivalGreenwichDTTM ,  
			   PFL.ElapseTM,PFL.FlightHours,PFL.DutyHours ,  
			   PFL.RestHours ,PFC.DutyType,PFC.CrewID,CDR.CrewDutyRuleCD,  
			   PFL.IsDutyEnd,PFL.DutyTYPE1,PFL.FedAviationRegNUM ,  
			   ACRT.AircraftTypeCD ,PFL.LegID, PFL.LegNUM,PFL.ArrivalDTTMLocal,  
			   PFM.TripNUM ,POFM.LogNum,PFL.DutyTYPE,PFM.TripID 
			     
				ORDER BY PFL.ArrivalDTTMLocal, PFM.TripID  
	   END   
   ELSE IF(@CrewFH = 'H' and @IsNonLogTripSheetIncludeCrewHIST='true')
	    BEGIN
	 
		  SELECT PFM.EstDepartureDT,CRW.CREWID,  
		   CRW.CREWCD,PFM.TRIPNUM,  
		   CAST(PFL.ArrivalDTTMLocal as DATE) as DtyPeriod,  
		   FLT.TailNUM as TailNo,  
		   CAST(PFL.ArrivalGreenwichDTTM as DATE) as gmtarrdt,  
		   CASE WHEN PFM.RecordType = 'T' THEN 'F'
		   ELSE PFL.DutyTYPE  
		   END AS Duty,    
		   PFL.DepartureDTTMLocal as LocDep,  
		   PFL.DepartureGreenwichDTTM as UtcDep,     
		   a.IcaoID as DepIcao,     
		   a1.IcaoID as ArrIcao,  
		   PFL.ArrivalDTTMLocal as LocArr,  
		   PFL.ArrivalGreenwichDTTM as UtcArr,  
		   PFL.ElapseTM,  
		   PFL.FlightHours  as FHrs,  
		   PFL.DutyHours as DHrs,  
		   PFL.RestHours as RHrs,  
		   (Select DATEDIFF(day,PFL.DepartureGreenwichDTTM,PFL.ArrivalGreenwichDTTM)) as CDays,  
		   PFC.DutyType as Pos,     
		   Count(PFC.CrewID) as CrwCnt,    
		   CDR.CrewDutyRuleCD AS Rules,   
		   PFL.IsDutyEnd,  
		   CASE WHEN  PFL.DutyTYPE1 =2  THEN 'Y'  
				WHEN  PFL.DutyTYPE1 =1  THEN 'N'  
				WHEN PFL.DutyTYPE1=null  THEN ''  
		   END as Intl,     
		   PFL.FedAviationRegNUM AS FAR,  
		   ACRT.AircraftTypeCD as TypeCode,   
		   PFL.LegID, PFL.LegNUM,  
			0000 AS crew_cnt,   
			SPACE(10) AS duty_hrs,   
			SPACE(10) AS rest_hrs,      
			SPACE(10) AS flt_hrs,  
			PFL.ArrivalDTTMLocal,  
			PFM.TripNUM as TSNumber,  
			POFM.LogNum as LogNumber  
		     
		     
		      
			FROM PreflightMain PFM   
			INNER JOIN PreflightLeg PFL  
			ON PFM.TripID = PFL.TripID  and PFM.CustomerID = PFL.CustomerID   
			INNER JOIN PreflightCrewlist PFC  
			ON PFL.LegID = PFC.LegID and PFL.CustomerID = PFC.CustomerID  
			LEFT OUTER JOIN Fleet FLT  
			ON PFM.FleetID = FLT.FleetID and PFM.CustomerID = FLT.CustomerID and FLT.FleetID is not null  
			and FLT.TailNUM is not null      
			INNER JOIN Crew CRW  
			ON PFC.CrewID = CRW.CrewID  and PFM.CustomerID = CRW.CustomerID   
			LEFT join Airport a on a.AirportID = PFL.DepartICAOID 
			--and a.CustomerID=PFL.CustomerID  
			LEFT join Airport a1 on a1.AirportID = PFL.ArriveICAOID 
			--and a1.CustomerID=PFL.CustomerID    
			LEFT OUTER JOIN Aircraft ACRT  
			ON PFM.AircraftID = ACRT.AircraftID  and PFM.CustomerID = ACRT.CustomerID   
			and ACRT.AircraftID is not null  and ACRT.AircraftTypeCD is not null  
			LEFT OUTER JOIN   
			(CrewChecklistDetail CCD        
			INNER JOIN CrewChecklist CCL  
			ON  CCL.CrewCheckID = CCD.CheckListID and  CCL.CustomerID = CCD.CustomerID)  
			ON PFC.CrewID = CCD.CrewID  and PFM.CustomerID = CCD.CustomerID   
			LEFT OUTER JOIN PostflightMain POFM on POFM.tripid =PFM.tripid and POFM.CustomerID=PFM.CustomerID  and ISNULL(POFM.IsDeleted,0)=0
			LEFT OUTER JOIN CrewDutyRules CDR on CDR.CrewDutyRulesID = PFL.CrewDutyRulesID and CDR.CustomerID = PFL.CustomerID AND ISNULL(CDR.IsDeleted,0)=0  
		     
		      
			WHERE PFM.TripID = PFL.TRIPID  
			AND PFM.TripNUM <> @TripNUM  
			AND PFL.LegID = PFC.LegID  
			AND (PFM.RecordType='T' or PFM.RecordType='C')  
			AND (PFM.TripStatus='T' or PFM.TripStatus='H')      
			AND PFC.CrewID = @CrewID   
			AND PFM.CustomerID=@CustomerID AND ISNULL(PFM.IsDeleted,0) = 0   
			AND ISNULL(PFL.IsDeleted,0) = 0  AND ISNULL(PFC.IsDeleted,0) = 0  
			AND ISNULL(FLT.IsDeleted,0) = 0 AND  ISNULL(CRW.IsDeleted,0)=0  
			AND ISNULL(ACRT.IsDeleted,0)=0 AND ISNULL(CCD.IsDeleted,0)=0  
			AND ISNULL(CCL.IsDeleted,0)=0      
			AND ((PFM.IsLog IS  NULL)  or (PFM.IsLog =0) or (PFM.IsLog =1))
			AND Cast(PFM.EstDepartureDT as date) between   CAST(@FromDate as date) AND 
				Cast(@ToDate as date)
			AND  
			ISNULL(PFM.ClientID,0) = ISNULL(@ClientID, ISNULL(PFM.ClientID, 0))     
		   Group by CRW.CREWID,CRW.CREWCD,PFM.TRIPNUM,PFL.ArrivalDTTMLocal,FLT.TailNUM,  
		   PFL.ArrivalGreenwichDTTM,PFM.RecordType,PFL.DepartureDTTMLocal,PFL.DepartureGreenwichDTTM,    
		   a.IcaoID , a1.IcaoID ,PFL.ArrivalDTTMLocal ,PFL.ArrivalGreenwichDTTM ,  
		   PFL.ElapseTM,PFL.FlightHours,PFL.DutyHours ,  
		   PFL.RestHours ,PFC.DutyType,PFC.CrewID,CDR.CrewDutyRuleCD,  
		   PFL.IsDutyEnd,PFL.DutyTYPE1,PFL.FedAviationRegNUM ,  
		   ACRT.AircraftTypeCD ,PFL.LegID, PFL.LegNUM,PFL.ArrivalDTTMLocal,  
		   PFM.TripNUM ,POFM.LogNum,PFL.DutyTYPE,PFM.TripID ,PFM.EstDepartureDT 
		     
			ORDER BY PFL.ArrivalDTTMLocal, PFM.TripID 
	   END
    ELSE IF(@CrewFH = 'F' and @IsNonLogTripSheetIncludeCrewHIST='true')
		BEGIN   
				
			
			   SELECT PFM.EstDepartureDT,CRW.CREWID,  
			   CRW.CREWCD,PFM.TRIPNUM,  
			   CAST(PFL.ArrivalDTTMLocal as DATE) as DtyPeriod,  
			   FLT.TailNUM as TailNo,  
			   CAST(PFL.ArrivalGreenwichDTTM as DATE) as gmtarrdt,  
			   CASE WHEN PFM.RecordType = 'T' THEN 'F'
			   ELSE PFL.DutyTYPE  
			   END AS Duty,    
			   PFL.DepartureDTTMLocal as LocDep,  
			   PFL.DepartureGreenwichDTTM as UtcDep,     
			   a.IcaoID as DepIcao,     
			   a1.IcaoID as ArrIcao,  
			   PFL.ArrivalDTTMLocal as LocArr,  
			   PFL.ArrivalGreenwichDTTM as UtcArr,  
			   PFL.ElapseTM,  
			   PFL.FlightHours  as FHrs,  
			   PFL.DutyHours as DHrs,  
			   PFL.RestHours as RHrs,  
			   (Select DATEDIFF(day,PFL.DepartureGreenwichDTTM,PFL.ArrivalGreenwichDTTM)) as CDays,  
			   PFC.DutyType as Pos,     
			   Count(PFC.CrewID) as CrwCnt,    
			   CDR.CrewDutyRuleCD AS Rules,   
			   PFL.IsDutyEnd,  
			   CASE WHEN  PFL.DutyTYPE1 =2  THEN 'Y'  
					WHEN  PFL.DutyTYPE1 =1  THEN 'N'  
					WHEN PFL.DutyTYPE1=null  THEN ''  
			   END as Intl,     
			   PFL.FedAviationRegNUM AS FAR,  
			   ACRT.AircraftTypeCD as TypeCode,   
			   PFL.LegID, PFL.LegNUM,  
				0000 AS crew_cnt,   
				SPACE(10) AS duty_hrs,   
				SPACE(10) AS rest_hrs,      
				SPACE(10) AS flt_hrs,  
				PFL.ArrivalDTTMLocal,  
				PFM.TripNUM as TSNumber,  
				POFM.LogNum as LogNumber  
			     
			     
			      
				FROM PreflightMain PFM   
				INNER JOIN PreflightLeg PFL  
				ON PFM.TripID = PFL.TripID  and PFM.CustomerID = PFL.CustomerID   
				INNER JOIN PreflightCrewlist PFC  
				ON PFL.LegID = PFC.LegID and PFL.CustomerID = PFC.CustomerID  
				LEFT OUTER JOIN Fleet FLT  
				ON PFM.FleetID = FLT.FleetID and PFM.CustomerID = FLT.CustomerID and FLT.FleetID is not null  
				and FLT.TailNUM is not null      
				INNER JOIN Crew CRW  
				ON PFC.CrewID = CRW.CrewID  and PFM.CustomerID = CRW.CustomerID   
				LEFT join Airport a on a.AirportID = PFL.DepartICAOID 
				--and a.CustomerID=PFL.CustomerID  
				LEFT join Airport a1 on a1.AirportID = PFL.ArriveICAOID 
				--and a1.CustomerID=PFL.CustomerID    
				LEFT OUTER JOIN Aircraft ACRT  
				ON PFM.AircraftID = ACRT.AircraftID  and PFM.CustomerID = ACRT.CustomerID   
				and ACRT.AircraftID is not null  and ACRT.AircraftTypeCD is not null  
				LEFT OUTER JOIN   
				(CrewChecklistDetail CCD        
				INNER JOIN CrewChecklist CCL  
				ON  CCL.CrewCheckID = CCD.CheckListID and  CCL.CustomerID = CCD.CustomerID)  
				ON PFC.CrewID = CCD.CrewID  and PFM.CustomerID = CCD.CustomerID   
				LEFT OUTER JOIN PostflightMain POFM on POFM.tripid =PFM.tripid and POFM.CustomerID=PFM.CustomerID  and ISNULL(POFM.IsDeleted,0)=0
				LEFT OUTER JOIN CrewDutyRules CDR on CDR.CrewDutyRulesID = PFL.CrewDutyRulesID and CDR.CustomerID = PFL.CustomerID AND ISNULL(CDR.IsDeleted,0)=0  
			     
			      
				WHERE PFM.TripID = PFL.TRIPID  
				AND PFM.TripNUM <> @TripNUM  
				AND PFL.LegID = PFC.LegID  
				AND (PFM.RecordType='T' or PFM.RecordType='C')  
				AND (PFM.TripStatus='T' or PFM.TripStatus='H')      
				AND PFC.CrewID = @CrewID   
				AND PFM.CustomerID=@CustomerID AND ISNULL(PFM.IsDeleted,0) = 0   
				AND ISNULL(PFL.IsDeleted,0) = 0  AND ISNULL(PFC.IsDeleted,0) = 0  
				AND ISNULL(FLT.IsDeleted,0) = 0 AND  ISNULL(CRW.IsDeleted,0)=0  
				AND ISNULL(ACRT.IsDeleted,0)=0 AND ISNULL(CCD.IsDeleted,0)=0  
				AND ISNULL(CCL.IsDeleted,0)=0      
				--AND PFM.IsLog IS NULL       
				AND Cast(PFM.EstDepartureDT as date) between   CAST(@FromDate as date) AND 
				Cast(@ToDate as date)
				AND  
				ISNULL(PFM.ClientID,0) = ISNULL(@ClientID, ISNULL(PFM.ClientID, 0))     
			   Group by CRW.CREWID,CRW.CREWCD,PFM.TRIPNUM,PFL.ArrivalDTTMLocal,FLT.TailNUM,  
			   PFL.ArrivalGreenwichDTTM,PFM.RecordType,PFL.DepartureDTTMLocal,PFL.DepartureGreenwichDTTM,    
			   a.IcaoID , a1.IcaoID ,PFL.ArrivalDTTMLocal ,PFL.ArrivalGreenwichDTTM ,  
			   PFL.ElapseTM,PFL.FlightHours,PFL.DutyHours ,  
			   PFL.RestHours ,PFC.DutyType,PFC.CrewID,CDR.CrewDutyRuleCD,  
			   PFL.IsDutyEnd,PFL.DutyTYPE1,PFL.FedAviationRegNUM ,  
			   ACRT.AircraftTypeCD ,PFL.LegID, PFL.LegNUM,PFL.ArrivalDTTMLocal,  
			   PFM.TripNUM ,POFM.LogNum,PFL.DutyTYPE,PFM.TripID ,PFM.EstDepartureDT 
			     
				ORDER BY PFL.ArrivalDTTMLocal, PFM.TripID  
	   END      
END



GO


