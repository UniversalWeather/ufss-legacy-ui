
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightTransport]    Script Date: 04/08/2014 16:15:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightTransport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightTransport]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightTransport]    Script Date: 04/08/2014 16:15:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


    
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightTransport]    
 (      
     @PreflightTransportID bigint,    
           @PreflightTransportName varchar(60),    
           @LegID bigint,    
           @TransportID bigint,    
           @IsArrivalTransport bit,    
           @IsDepartureTransport bit,    
           @AirportID bigint,    
           @CrewPassengerType char(1),    
           @Street varchar(50),    
           @CityName varchar(20),    
           @StateName varchar(20),    
           @PostalZipCD char(20),    
           @PhoneNum1 varchar(25),    
           @PhoneNum2 varchar(25),    
           @PhoneNum3 varchar(25),    
           @PhoneNum4 varchar(25),    
           @FaxNUM varchar(25),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @ConfirmationStatus varchar(max),    
           @Comments varchar(max),    
           @IsCompleted bit,    
           @CustomerID Bigint,    
           @Status varchar(20)    
    )    
AS    
BEGIN     
SET NOCOUNT OFF    
    
    
    
UPDATE [dbo].[PreflightTransportList]    
   SET [PreflightTransportName] = @PreflightTransportName,       
    [IsArrivalTransport] = @IsArrivalTransport,    
    [IsDepartureTransport] = @IsDepartureTransport,    
    [AirportID] = @AirportID,    
    [CrewPassengerType] = @CrewPassengerType,    
    [Street] = @Street,    
    [CityName] = @CityName,    
    [StateName] = @StateName,    
    [PostalZipCD] = @PostalZipCD,    
    [PhoneNum1] = @PhoneNum1,    
    [PhoneNum2] = @PhoneNum2,    
    [PhoneNum3] = @PhoneNum3,     
    [PhoneNum4] = @PhoneNum4,     
    [FaxNUM] = @FaxNUM,    
    [LastUpdUID] = @LastUpdUID,    
    [LastUpdTS] = @LastUpdTS,    
    [IsDeleted] = @IsDeleted,    
    [ConfirmationStatus] = @ConfirmationStatus,    
    [IsCompleted]=  @IsCompleted,        
          [Comments] = @Comments,    
          [Status] = @Status,    
          [TransportID]= @TransportID
        
 WHERE PreflightTransportID=@PreflightTransportID AND LegID=@LegID     
END    
    



GO


