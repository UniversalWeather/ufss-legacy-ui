
/****** Object:  StoredProcedure [dbo].[spAddFeeGroup]    Script Date: 02/15/2013 19:11:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFeeGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFeeGroup]
GO

USE [FP_Rel2]
GO

/****** Object:  StoredProcedure [dbo].[spAddFeeGroup]    Script Date: 02/15/2013 19:11:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      
CREATE Procedure [dbo].[spAddFeeGroup]          
(           
 @CustomerID bigint          
,@FeeGroupCD char(4)          
,@FeeGroupDescription varchar(100)          
,@LastUpdUID varchar(30)          
,@LastUpdTS datetime          
,@IsDeleted bit    
,@IsInActive bit            
)          
AS          
BEGIN           
SET NoCOUNT ON          
DECLARE @FeeGroupID BIGINT          
          
          
          
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @FeeGroupID OUTPUT          
DECLARE @currentTime datetime            
SET @currentTime = GETUTCDATE()            
SET @LastUpdTS = @currentTime            
          
   INSERT INTO FeeGroup          
           (          
            FeeGroupID          
           ,CustomerID          
           ,FeeGroupCD          
           ,FeeGroupDescription          
           ,LastUpdUID           
           ,LastUpdTS                    
           ,IsDeleted    
           ,IsInActive            
             )          
     VALUES          
           (          
         @FeeGroupID                
        ,@CustomerID           
		,@FeeGroupCD           
		,@FeeGroupDescription          
        ,@LastUpdUID          
        ,@LastUpdTS           
        ,@IsDeleted    
        ,@IsInActive                     
   )          
 END 
GO


