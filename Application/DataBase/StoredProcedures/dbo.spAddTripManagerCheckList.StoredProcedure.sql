
GO
/****** Object:  StoredProcedure [dbo].[spAddTripManagerCheckList]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddTripManagerCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddTripManagerCheckList]
go
CREATE procedure [dbo].[spAddTripManagerCheckList]
(	
 @CustomerID	bigint
,@CheckListCD	char(4)
,@CheckListDescription	varchar(20)
,@CheckGroupID	bigint
,@LastUpdUID	varchar(30)
,@LastUpdTS	datetime
,@IsDeleted	bit
,@IsInActive bit)
-- =============================================================
-- Author:Prakash Pandian.S
-- Create date: 13/5/2012
-- Modified : Karthikeyan.S
-- Modified Date : 02/08/2012
-- Description: Insert the TripManagerCheckList information
-- =============================================================
as
begin
SET NoCOUNT ON   
DECLARE @CheckListID bigint 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CheckListID OUTPUT
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
   INSERT INTO [TripManagerCheckList]
           ([CheckListID]
           ,[CustomerID]
           ,[CheckListCD]
           ,[CheckListDescription]
           ,[CheckGroupID]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[IsInActive])
     VALUES
           (@CheckListID
			,@CustomerID
			,@CheckListCD
			,@CheckListDescription
			,@CheckGroupID
			,@LastUpdUID
			,@LastUpdTS
			,@IsDeleted
			,@IsInActive)
END
GO
