/****** Object:  StoredProcedure [dbo].[spGetAllAirportForGridCustomPaging]    Script Date: 10/29/2014 14:39:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAirportForGridCustomPaging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAirportForGridCustomPaging]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllAirportForGridCustomPaging]    Script Date: 10/29/2014 14:39:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Exec spGetAllAirportForGridCustomPaging 10002,0,0,1,100,null,'ABCD'
CREATE PROCEDURE [dbo].[spGetAllAirportForGridCustomPaging]
(
	 @CustomerID	BIGINT
	,@IsInActive	bit
	,@MinimumRunway	Decimal(6,0)
	,@Start			int
	,@End			int	
	,@Sort          Varchar(100)	= NULL
	,@IcaoID		Varchar(50)		= NULL
	,@Iata			Varchar(50)		= NULL
	,@CityName		Varchar(100)	= NULL
	,@StateName		Varchar(100)	= NULL
	,@CountryCD		Varchar(50)		= NULL	
	,@CountryName	Varchar(255)	= NULL
	,@AirportName	Varchar(255)	= NULL
	,@OffsetToGMT	Varchar(50)		= NULL
	,@Filter		Varchar(50)		= NULL
)            
AS            
SET NOCOUNT ON            

Declare @sql Varchar(8000)

IF LEN(@CustomerID) >0 
BEGIN 
	DECLARE @UWACustomerID BIGINT
	SELECT @UWACustomerID = dbo.GetUWACustomerID()
			
	If @Sort = '' Or @Sort IS NULL
	Begin
	   Set @Sort = ' CityName,IcaoID '
	End
	
	If @MinimumRunway = NULL
	Begin
	   Set @MinimumRunway = 0
	End		
		 
	    CREATE TABLE #Airport
		( 		     
			 AirportID bigint  
			,CustomerID bigint  
			,IcaoID varchar(4)
			,CityName Varchar(25)
			,StateName Varchar(25)
			,CountryID bigint
			,CountryCD Varchar(60)
			,AirportName Varchar(25)
			,OffsetToGMT numeric(6,2)
			,IsEntryPort bit
			,IsInActive bit
			,LastUpdUID Varchar(30)
			,LastUpdTS DateTime
			,IsWorldClock bit
			,Filter Varchar(50)
			,UWAID Varchar(20)
			,Iata Varchar(3)
			,MaxRunway numeric(6)
			,CountryName Varchar(255)
		)		 	
			 
		INSERT Into #Airport 
		Select 
				 t1.AirportID,
				 t1.CustomerID,
				 t1.IcaoID,
				 t1.CityName,
				 t1.StateName,
				 t1.CountryID,
				 t1.CountryCD,
				 t1.AirportName,
				 t1.OffsetToGMT,
				 t1.IsEntryPort,
				 t1.IsInActive,
				 t1.LastUpdUID,
				 t1.LastUpdTS,	
				 t1.IsWorldClock,
				 t1.Filter,
				 t1.UWAID,
				 t1.Iata,
				 t1.MaxRunway,
				 t1.CountryName
				  From 
		(
		 SELECT 
				 A.AirportID,
				 A.CustomerID,
				 A.IcaoID,
				 A.CityName,
				 A.StateName,
				 A.CountryID,
				 C.CountryCD,
				 A.AirportName,
				 A.OffsetToGMT,
				 A.IsEntryPort,
				 A.IsInActive,
				 A.LastUpdUID,
				 A.LastUpdTS,	
				 A.IsWorldClock,				 
				 (SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,
				 A.UWAID,
				 A.Iata,
				 isnull(LongestRunway,0) AS [MaxRunway],
				 C.CountryName
		  FROM Airport A LEFT JOIN Country C  ON C.CountryID = A.CountryID
		  WHERE A.CustomerID = @CustomerID
			AND A.IsDeleted =0
			AND A.IsInActive <= @IsInActive
		    AND isnull(LongestRunway,0) >= @MinimumRunway		    		    
		    AND ((LEN(ISNULL(@IcaoID,'')) = 0) OR (LEN(ISNULL(@IcaoID,0)) <> 0 AND A.IcaoID LIKE '%' + @IcaoID + '%'))		    
		    AND ((LEN(ISNULL(@Iata,'')) = 0) OR (LEN(ISNULL(@Iata,0)) <> 0 AND A.Iata LIKE '%' + @Iata + '%'))		    
		    AND ((LEN(ISNULL(@CityName,'')) = 0) OR (LEN(ISNULL(@CityName,0)) <> 0 AND A.CityName LIKE '%' + @CityName + '%'))		    			
			AND ((LEN(ISNULL(@StateName,'')) = 0) OR (LEN(ISNULL(@StateName,0)) <> 0 AND A.StateName LIKE '%' + @StateName + '%'))			
			AND ((LEN(ISNULL(@CountryCD,'')) = 0) OR (LEN(ISNULL(@CountryCD,0)) <> 0 AND C.CountryCD LIKE '%' + @CountryCD + '%'))			
			AND ((LEN(ISNULL(@CountryName,'')) = 0) OR (LEN(ISNULL(@CountryName,0)) <> 0 AND C.CountryName LIKE '%' + @CountryName + '%'))			
			AND ((LEN(ISNULL(@AirportName,'')) = 0) OR (LEN(ISNULL(@AirportName,0)) <> 0 AND A.AirportName LIKE '%' + @AirportName + '%'))			
			AND ((LEN(ISNULL(@OffsetToGMT,'')) = 0) OR (LEN(ISNULL(@OffsetToGMT,0)) <> 0 AND Convert(varchar(100),A.OffsetToGMT) = @OffsetToGMT))			
		  UNION ALL 
		  
		  SELECT A.AirportID,
				 A.CustomerID,
				 A.IcaoID,
				 A.CityName,
				 A.StateName,
				 A.CountryID,	
				 C.CountryCD,
				 A.AirportName,
				 A.OffsetToGMT,
				 A.IsEntryPort,
				 A.IsInActive,
				 A.LastUpdUID,
				 A.LastUpdTS,
				 A.IsWorldClock,
				 (SELECT(CASE WHEN (A.UWAID IS NULL or A.UWAID = '' or A.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)) as Filter,				 				
				 A.UWAID,
				 A.Iata,
				 isnull(LongestRunway,0) AS [MaxRunway],
				 C.CountryName
		  FROM Airport A
		  LEFT JOIN Country C ON C.CountryID = A.CountryID
		  WHERE A.CustomerID <> @CustomerID
			AND A.CustomerID = @UWACustomerID
			AND A.IsDeleted =0
			AND A.IsInActive <= @IsInActive
			AND IcaoID NOT IN
			  (SELECT DISTINCT IcaoID
			   FROM Airport
			   WHERE CustomerID = @CustomerID
				 AND IsDeleted = 0)		    		    
		    AND isnull(LongestRunway,0) >= @MinimumRunway
		    AND ((LEN(ISNULL(@IcaoID,'')) = 0) OR (LEN(ISNULL(@IcaoID,0)) <> 0 AND A.IcaoID LIKE '%' + @IcaoID + '%'))		    
		    AND ((LEN(ISNULL(@Iata,'')) = 0) OR (LEN(ISNULL(@Iata,0)) <> 0 AND A.Iata LIKE '%' + @Iata + '%'))		    
		    AND ((LEN(ISNULL(@CityName,'')) = 0) OR (LEN(ISNULL(@CityName,0)) <> 0 AND A.CityName LIKE '%' + @CityName + '%'))		    			
			AND ((LEN(ISNULL(@StateName,'')) = 0) OR (LEN(ISNULL(@StateName,0)) <> 0 AND A.StateName LIKE '%' + @StateName + '%'))			
			AND ((LEN(ISNULL(@CountryCD,'')) = 0) OR (LEN(ISNULL(@CountryCD,0)) <> 0 AND C.CountryCD LIKE '%' + @CountryCD + '%'))			
			AND ((LEN(ISNULL(@CountryName,'')) = 0) OR (LEN(ISNULL(@CountryName,0)) <> 0 AND C.CountryName LIKE '%' + @CountryName + '%'))			
			AND ((LEN(ISNULL(@AirportName,'')) = 0) OR (LEN(ISNULL(@AirportName,0)) <> 0 AND A.AirportName LIKE '%' + @AirportName + '%'))			
			AND ((LEN(ISNULL(@OffsetToGMT,'')) = 0) OR (LEN(ISNULL(@OffsetToGMT,0)) <> 0 AND Convert(varchar(100),A.OffsetToGMT) = @OffsetToGMT))			
		) As t1
	    Where ((LEN(ISNULL(@Filter,'')) = 0) OR (LEN(ISNULL(@Filter,0)) <> 0 AND (((SELECT(CASE WHEN (t1.UWAID IS NULL or t1.UWAID = '' or t1.UWAID = '&nbsp;') THEN 'CUSTOM' ELSE 'UWA' END)))  LIKE '%' + @Filter + '%')))
	    
	Set @sql = 'Select * From 
	            (
					select 
						TOP 100 PERCENT A.*,				
						Count(*) over () AS TotalCount,
						ROW_NUMBER() OVER(ORDER BY ' + @Sort + ') AS ROWNUMBER
					From #Airport As A ORDER BY ' + @Sort + '
			    ) AS FINAL		
				WHERE ROWNUMBER BETWEEN ' + Convert(varchar(100),@Start) + ' AND '  + Convert(varchar(100),@End)	
 
 --insert into temp_query values(@sql)
 --print(@sql)
 
 EXEC (@sql)
  
END



GO


