/****** Object:  StoredProcedure [dbo].[spUpdateFlightpakFuel]    Script Date: 09/27/2013 20:13:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFlightpakFuel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFlightpakFuel]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFlightpakFuel]    Script Date: 09/27/2013 20:13:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateFlightpakFuel]      
(     
  @FlightpakFuelID BIGINT      
   ,@CustomerID BIGINT        
   ,@DepartureICAOID BIGINT        
   ,@FBOName VARCHAR(25)        
   ,@GallonFrom NUMERIC(17,3)        
   ,@GallonTO NUMERIC(17,3)      
   ,@EffectiveDT DATE      
   ,@UnitPrice NUMERIC(15,4)      
   ,@FuelVendorID BIGINT      
   ,@UpdateDTTM DATETIME          
   ,@LastUpdUID VARCHAR(30)        
   ,@LastUpdTS DATETIME          
   ,@IsActive BIT    
   ,@FuelVendorText VARCHAR(max)    
)      
-- =============================================      
-- Author:Srinivas.p         
-- Create date: 10/7/2012        
-- Description: Update InTo Flight Pak Fuel   
-- Modification History:
-- Karthi - Added new parameter FuelVendorText for new enhancement     
-- =============================================      
AS      
BEGIN       
SET NoCOUNT ON      
      
  IF exists(SELECT * FROM FlightpakFuel WHERE FlightpakFuelID=@FlightpakFuelID AND CustomerID=@CustomerID)    
  BEGIN     
    
	--To store the lastupdateddate as UTC date    
	DECLARE @currentTime datetime    
	SET @currentTime = GETUTCDATE()    
	SET @LastUpdTS = @currentTime    
	--End of UTC date    
	    
	 UPDATE FlightpakFuel      
	 SET      
		 DepartureICAOID=@DepartureICAOID,    
		 FBOName=@FBOName,    
		 GallonFrom=@GallonFrom,    
		 GallonTO=@GallonTO,    
		 EffectiveDT=@EffectiveDT,    
		 UnitPrice=@UnitPrice,    
		 FuelVendorID=@FuelVendorID,    
		 LastUpdUID=@LastUpdUID,    
		 LastUpdTS=@LastUpdTS,    
		 IsActive=@IsActive,   
		 FuelVendorText = @FuelVendorText     
	 WHERE        FlightpakFuelID=@FlightpakFuelID AND CustomerID=@CustomerID    
  END    
  ELSE    
     
	BEGIN    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'FuelVendorCurrentNo',  @FlightpakFuelID OUTPUT    
	   INSERT INTO FlightpakFuel  
			  (FlightpakFuelID      
			  ,[CustomerID]      
			  ,[DepartureICAOID]      
			  ,[FBOName]      
			  ,[GallonFrom]      
			  ,[GallonTO]      
			  ,[EffectiveDT]      
			  ,[UnitPrice]      
			  ,[FuelVendorID]      
			  ,[UpdateDTTM]      
			  ,[LastUpdUID]      
			  ,[LastUpdTS]      
			  ,[IsActive]  
			  ,FuelVendorText      
			  )        
		VALUES (@FlightpakFuelID      
			  ,@CustomerID      
			  ,@DepartureICAOID                
			  ,@FBOName      
			  ,@GallonFrom      
			  ,@GallonTO      
			  ,@EffectiveDT      
			  ,@UnitPrice      
			  ,@FuelVendorID      
			  ,@UpdateDTTM      
			  ,@LastUpdUID      
			  ,@LastUpdTS      
			  ,@IsActive   
			  ,@FuelVendorText     
			  )      
	END    
      
END 
GO

