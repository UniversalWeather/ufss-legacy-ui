

/****** Object:  StoredProcedure [dbo].[spGetAllPassengerAdditionalInfoReqID]    Script Date: 05/24/2013 15:05:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllPassengerAdditionalInfoReqID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllPassengerAdditionalInfoReqID]
GO



/****** Object:  StoredProcedure [dbo].[spGetAllPassengerAdditionalInfoReqID]    Script Date: 05/24/2013 15:05:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllPassengerAdditionalInfoReqID]     
(    
 @CustomerID  BIGINT,    
 @ClientID BIGINT,  
 @RequestorID BIGINT     
 )    
AS        
BEGIN        
-- =============================================        
-- Author:  Manish V      
-- Create date: 5/9/2012       
-- Description: To get the passenger records based on Requestor ID  
-- =============================================       
if(@ClientID>0)    
begin     
SELECT PassengerAdditionalInfoID    
              ,CustomerID         
           ,PassengerRequestorID        
           ,AdditionalINFOCD         
           ,AdditionalINFODescription        
           ,AdditionalINFOValue      
           ,ClientID        
           ,LastUpdUID        
        ,LastUptTS        
          ,IsDeleted
          ,[PassengerInformationID]
           ,[IsPrint]     
  FROM [PassengerAdditionalInfo] WHERE CustomerID=@CustomerID and ClientID=@ClientID and PassengerRequestorID = @RequestorID and IsDeleted = 0  
    end    
    else    
    begin    
    SELECT PassengerAdditionalInfoID    
              ,CustomerID         
           ,PassengerRequestorID        
           ,AdditionalINFOCD         
           ,AdditionalINFODescription        
           ,AdditionalINFOValue      
           ,ClientID        
           ,LastUpdUID        
        ,LastUptTS        
          ,IsDeleted  
          ,[PassengerInformationID]
           ,[IsPrint]   
  FROM [PassengerAdditionalInfo] WHERE CustomerID=@CustomerID  and PassengerRequestorID = @RequestorID and IsDeleted = 0  
    end    
END  
GO


