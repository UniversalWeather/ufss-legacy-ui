USE [FP_DEV_Dev2]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllPassenger_TelerikPagedGrid]    Script Date: 10/30/2014 14:13:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spGetAllPassenger_TelerikPagedGrid 10002,0,0,0,'',0,0,'',10002185356,1,10,'','','','',''
--spGetAllPassenger_TelerikPagedGrid 10002,0,0,0,'',0,0,'',0,1,10,'PassengerRequestorCD ASC','','','da',''
CREATE PROCEDURE [dbo].[spGetAllPassenger_TelerikPagedGrid]
(	
	@CustomerID		BIGINT,
	@ClientID		BIGINT,
	@CQCustomerID	BIGINT,
	@PassengerID	BIGINT,
	@PassengerCD	varchar(5),
	@ActiveOnly		BIT =0,
	@RequestorOnly	BIT =0,
	@ICAOID			VARCHAR(4),
	@PaxGroupID		BIGINT,
	@Start			BIGINT,
	@End			BIGINT,
	@Sort			Varchar(MAX)= NULL,
	@PassengerName	Varchar(MAX)= '',
	@DepartmentCD	Varchar(MAX)= '',
	@DepartmentName	Varchar(MAX)= '',
	@PhoneNum		Varchar(MAX)= ''
	
) 
AS  
BEGIN  

	Declare @sortDir As NVarchar(100);
	Declare @sortOrder As NVarchar(100);
	
	SELECT @sortOrder=S FROM dbo.SplitString(@Sort,' ') Where zeroBasedOccurance=0 
	SELECT @sortDir=S FROM dbo.SplitString(@Sort,' ') Where zeroBasedOccurance=1 
	
	Select TOP 100 PERCENT 
		ROWNUMBER
		,TotalCount
		,PassengerRequestorID,PassengerRequestorCD,PassengerName,CustomerID,DepartmentID,PassengerDescription
		,PhoneNum,ClientID,HomebaseID,IsActive
		,IsRequestor
		,DepartmentCD,DepartmentName,DepartPercentage
		,HomeBaseName,HomeBaseCD
		,PassengerGroupID
	From (	
		SELECT 
			ROW_NUMBER() OVER(ORDER BY 
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'PassengerRequestorCD' THEN Passenger.PassengerRequestorCD END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'PassengerName' THEN Passenger.PassengerName END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'DepartmentCD' THEN DepartmentCD END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'DepartmentName' THEN DepartmentName END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'PhoneNum' THEN Passenger.PhoneNum END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'HomeBaseCD' THEN HomeBaseCD END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'IsActive' THEN Passenger.IsActive END ASC,
			CASE WHEN @sortDir <> 'ASC' then '' WHEN @sortOrder = 'IsRequestor' THEN Passenger.IsRequestor END ASC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'PassengerRequestorCD' THEN Passenger.PassengerRequestorCD END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'PassengerName' THEN Passenger.PassengerName END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'DepartmentCD' THEN DepartmentCD END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'DepartmentName' THEN DepartmentName END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'PhoneNum' THEN Passenger.PhoneNum END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'HomeBaseCD' THEN HomeBaseCD END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'IsActive' THEN Passenger.IsActive END DESC,
			CASE WHEN @sortDir <> 'DESC' then '' WHEN @sortOrder = 'IsRequestor' THEN Passenger.IsRequestor END DESC) AS ROWNUMBER
			,Count(*) over () AS TotalCount
			,Passenger.PassengerRequestorID
			,CASE WHEN Passenger.PassengerRequestorCD IS NULL THEN '' ELSE LTRIM(RTRIM(Passenger.PassengerRequestorCD)) END AS PassengerRequestorCD
			,Passenger.PassengerName
			,Passenger.DepartmentID
			,Passenger.PassengerDescription
			,Passenger.PhoneNum
			,Passenger.HomebaseID
			,Passenger.IsActive
			,Passenger.IsRequestor
			,Passenger.CustomerID
			,Passenger.ClientID
			,Passenger.CQCustomerID
			,DepartmentCD,DepartmentName,DepartPercentage,HomeBaseName,HomeBaseCD,PassengerGroupID
			FROM Passenger as Passenger
			LEFT OUTER JOIN (Select D.DepartmentID,D.DepartmentCD,D.DepartmentName,D.DepartPercentage from Department As D Where D.IsDeleted=0) as Department on Department.DepartmentID = Passenger.DepartmentID
			LEFT OUTER JOIN (Select A.AirportID,A.CityName as HomeBaseName,A.IcaoID as HomeBaseCD From Airport As A Where A.IsDeleted=0 AND ISNULL(A.IcaoID,'') = (case when ltrim(rtrim(@ICAOID)) <>'' then @ICAOID else ISNULL(A.IcaoID,'') end)) as HomeBase on HomeBase.AirportID = Passenger.HomebaseID
			LEFT OUTER JOIN (Select P.PassengerGroupID,P.PassengerRequestorID From PassengerGroupOrder As P Where P.IsDeleted=0) as PaxGroup on PaxGroup.PassengerRequestorID = Passenger.PassengerRequestorID
			WHERE Passenger.CustomerID = @CustomerID  
			AND Passenger.isdeleted=0 And Ltrim(Rtrim(ISNULL(Passenger.PassengerRequestorCD,''))) != ''
			AND ISNULL(Passenger.ClientID,0) = (case when @ClientID >0 then @ClientID else ISNULL(Passenger.ClientID,0) end)  
			AND ISNULL(Passenger.CQCustomerID,0) = (case when @CQCustomerID >0 then @CQCustomerID else ISNULL(Passenger.CQCustomerID,0) end)  
			AND Passenger.PassengerRequestorID = (case when @PassengerID <>0 then @PassengerID else Passenger.PassengerRequestorID end)
			AND (Ltrim(Rtrim(ISNULL(@PassengerCD,''))) = '' OR ISNULL(Passenger.PassengerRequestorCD,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@PassengerCD,''))) + '%')
			AND ISNULL(Passenger.IsActive,1) = (case when @ActiveOnly =1 then 1 else ISNULL(Passenger.IsActive,1) end)
			AND ISNULL(Passenger.IsRequestor,1) = (case when @RequestorOnly =1 then 1 else ISNULL(Passenger.IsRequestor,1) end)
	
			AND ISNULL(PaxGroup.PassengerGroupID,0) = (case when @PaxGroupID >0 then @PaxGroupID else ISNULL(PaxGroup.PassengerGroupID,0) end)  

			AND (Ltrim(Rtrim(ISNULL(@PassengerCD,''))) = '' OR ISNULL(Passenger.PassengerRequestorCD,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@PassengerCD,''))) + '%')
			AND (Ltrim(Rtrim(ISNULL(@PassengerName,''))) = '' OR ISNULL(Passenger.PassengerName,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@PassengerName,''))) + '%')
			AND (Ltrim(Rtrim(ISNULL(@PhoneNum,''))) = '' OR ISNULL(Passenger.PhoneNum,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@PhoneNum,''))) + '%')
			AND (Ltrim(Rtrim(ISNULL(@DepartmentCD,''))) = '' OR ISNULL(Department.DepartmentCD,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@DepartmentCD,''))) + '%')
			AND (Ltrim(Rtrim(ISNULL(@DepartmentName,''))) = '' OR ISNULL(Department.DepartmentName,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@DepartmentName,''))) + '%')
			AND (Ltrim(Rtrim(ISNULL(@ICAOID,''))) = '' OR ISNULL(HomeBase.HomeBaseCD,'') LIKE '%' + Ltrim(Rtrim(ISNULL(@ICAOID,''))) + '%')
		) As Page
		WHERE Page.ROWNUMBER BETWEEN @Start AND @End
END