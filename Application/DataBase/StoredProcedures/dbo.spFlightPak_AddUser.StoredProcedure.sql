/****** Object:  StoredProcedure [dbo].[spFlightPak_AddUser]    Script Date: 08/29/2012 16:50:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_AddUser]          
 (@UserName char(30)          
 ,@CustomerID bigint          
 ,@HomebaseID bigint          
 ,@ClientID bigint          
 ,@FirstName varchar(60)          
 ,@MiddleName varchar(60)          
 ,@LastName varchar(60)          
 ,@HomeBase char(4)          
 ,@TravelCoordID bigint          
 ,@IsActive bit          
 ,@IsUserLock bit          
 ,@IsTripPrivacy bit          
 ,@EmailID varchar(100)          
 ,@PhoneNum varchar(100)          
 ,@PassengerRequestorID bigint          
 ,@UVTripLinkID varchar(50)          
 ,@UVTripLinkPassword varchar(60)          
 ,@IsPrimaryContact bit          
 ,@IsSecondaryContact bit          
 ,@IsSystemAdmin bit          
 ,@IsDispatcher bit          
 ,@IsOverrideCorp bit          
 ,@IsCorpRequestApproval bit          
 ,@LastUpdUID char(30)          
 ,@LastUpdTS datetime          
 ,@IsDeleted bit)          
as          
Begin           
Set NoCount On          
  set @LastUpdTS = GETUTCDATE()        
-- =============================================          
-- Author:  PremPrakash.s          
-- Create date: 15-may-2012          
-- Description:Adding user name           
-- =============================================          
-- MODIFIED BY : SRINIVASAN. J          
-- DESCRIPTION : PROCEDURE MODIFIED AFTER TABLE MODIFIED          
-- MODIFIED DATE : 29-JUNE-2012          
-- =============================================          
----------------------------------------------------------------------------------------------------        
DECLARE @UserMasterId BIGINT     
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @UserMasterId OUTPUT      
IF(@IsPrimaryContact = 'true')          
BEGIN    
UPDATE UserMaster Set IsPrimaryContact = 'false' where CustomerID = @CustomerID    
END    
IF(@IsSecondaryContact = 'true')          
BEGIN    
UPDATE UserMaster Set IsSecondaryContact = 'false' where CustomerID = @CustomerID    
END    
INSERT INTO [dbo].[UserMaster]          
           ([UserName]          
           ,[CustomerID]          
           ,[HomebaseID]          
           ,[ClientID]          
           ,[FirstName]          
           ,[MiddleName]          
           ,[LastName]          
           ,[HomeBase]          
           ,[TravelCoordID]          
           ,[IsActive]          
           ,[IsUserLock]          
           ,[IsTripPrivacy]          
           ,[EmailID]          
           ,[PhoneNum]          
           ,[PassengerRequestorID]          
           ,[UVTripLinkID]          
           ,[UVTripLinkPassword]          
           ,[IsPrimaryContact]          
           ,[IsSecondaryContact]          
           ,[IsSystemAdmin]          
           ,[IsDispatcher]          
           ,[IsOverrideCorp]          
           ,[IsCorpRequestApproval]          
           ,[LastUpdUID]          
           ,[LastUpdTS]          
           ,[IsDeleted]  
           ,[UserMasterID]  
           ,[ForceChangePassword]  
     ,[ForceChangeSecurityHint]  
     ,[InvalidLoginCount])          
     VALUES          
           (LTRIM(RTRIM(@UserName))          
     ,@CustomerID          
     ,@HomebaseID          
     ,@ClientID          
     ,@FirstName          
     ,@MiddleName          
     ,@LastName          
     ,@HomeBase          
     ,@TravelCoordID          
     ,@IsActive          
     ,@IsUserLock          
     ,@IsTripPrivacy          
     ,@EmailID          
     ,@PhoneNum          
     ,@PassengerRequestorID          
     ,@UVTripLinkID          
     ,@UVTripLinkPassword          
     ,@IsPrimaryContact          
     ,@IsSecondaryContact          
     ,@IsSystemAdmin          
     ,@IsDispatcher          
     ,@IsOverrideCorp          
     ,@IsCorpRequestApproval          
     ,@LastUpdUID          
     ,@LastUpdTS          
     ,@IsDeleted  
     ,@UserMasterId  
     ,0  
  ,0  
  ,0)  
    
    
DECLARE @SystemPrefID BIGINT      
    
-- Insert value to system prefrences    
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @SystemPrefID OUTPUT    
         
INSERT INTO [dbo].[FPSystemPref] values (@SystemPrefID,@CustomerID,@UserName,0,GETUTCDATE(),GETUTCDATE(),@LastUpdUID,NULL,NULL,0)    
     
End    
GO
