
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetCrewCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewCheckList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewCheckList]  
(
	 @CustomerID BIGINT
	,@ClientID BIGINT 
	,@IsStatus BIT
)
AS
SET NOCOUNT ON;
BEGIN		
	SELECT	CCD.CrewID,
			C.CrewCD,
			C.LastName,
			C.FirstName,
			C.MiddleInitial, 
			CCD.CheckListCD, 
			CCD.AlertDT,
			CCD.GraceDT, 
			A.AircraftCD, 
			A.AircraftDescription, 
			CC.CrewChecklistDescription, 
			CCD.DueDT, 
			CCD.IsScheduleCheck,
			C.IsStatus
			--CC.ClientID, 
			--CC.IsCrewCurrencyPlanner
	FROM CrewCheckListDetail CCD 
		INNER JOIN Crew C
		ON C.CrewID = CCD.CrewID 
		LEFT JOIN CrewCheckList CC
		--ON CC.CrewCheckCD = CCD.CheckListCD 
		ON CC.CrewCheckID = CCD.CheckListID
		LEFT OUTER JOIN Aircraft A
		ON A.AircraftID = CCD.AircraftID
	WHERE C.CustomerID = @CustomerID
		AND CCD.CustomerID = @CustomerID
		AND C.IsDeleted = 0 
		AND CCD.IsDeleted =0 
		--AND C.IsStatus = 1
		AND ISNULL(C.ClientID, 0) = ISNULL(@ClientID, ISNULL(C.ClientID, 0))
		--AND ISNULL(C.IsStatus, 0)  = ISNULL(@IsStatus , ISNULL(C.IsStatus, 0))
		--AND GETUTCDATE() >= CCD.AlertDT 
		--AND CCD.AlertDT is not null -- Fix for UW-1054 (8073)
		AND ISNULL(CCD.IsOneTimeEvent, 0) = 0 
		AND ISNULL(CCD.IsNoChecklistREPT, 0) = 0
	ORDER BY C.CrewCD		
END

GO


