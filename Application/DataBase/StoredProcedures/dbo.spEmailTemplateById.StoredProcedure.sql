/****** Object:  StoredProcedure [dbo].[spAddFPEmailTranLog]    Script Date: 08/29/2012 16:50:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetEmailTemplateById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetEmailTemplateById]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spValidateUser]    Script Date: 02/05/2013 16:04:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spGetEmailTemplateById]  
@EmailTemplateId bigint
AS  
BEGIN  
	SELECT * FROM FPEmailTmpl where EmailTmplID = @EmailTemplateId
END

GO


