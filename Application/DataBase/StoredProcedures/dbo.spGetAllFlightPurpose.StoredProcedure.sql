
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFlightPurpose]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFlightPurpose]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFlightPurpose]
GO

CREATE PROCEDURE [dbo].[spGetAllFlightPurpose](@CustomerID BIGINT,@ClientID bigint)      
AS      
-- =============================================      
-- Author: Sujitha.V      
-- Create date: 17/4/2012      
-- Description: Get All Flight Purpose Informations      
-- =============================================      
SET NOCOUNT ON      
    IF(@ClientID>0)    
    begin    
    SELECT     
       [FlightPurposeID]    
       ,CASE WHEN [FlightPurposeCD] IS NULL THEN '' ELSE LTRIM(RTRIM([FlightPurposeCD])) END AS FlightPurposeCD    
   ,[FlightPurposeDescription]    
   ,[CustomerID]    
   ,[IsDefaultPurpose]    
   ,[IsPersonalTravel]    
   ,[ClientID]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsWaitList]    
   ,[IsDeleted]
   ,IsInactive      
FROM  [FlightPurpose] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0 and ClientID=@ClientID 
 order by FlightPurposeCD      
END    
ELSE    
BEGIN    
SELECT     
       [FlightPurposeID]    
       ,CASE WHEN [FlightPurposeCD] IS NULL THEN '' ELSE LTRIM(RTRIM([FlightPurposeCD])) END AS FlightPurposeCD        
   ,[FlightPurposeDescription]    
   ,[CustomerID]    
   ,[IsDefaultPurpose]    
   ,[IsPersonalTravel]    
   ,[ClientID]    
   ,[LastUpdUID]    
   ,[LastUpdTS]    
   ,[IsWaitList]    
   ,[IsDeleted]    
   ,IsInactive  
FROM  [FlightPurpose] WHERE CustomerID=@CustomerID AND ISNULL(IsDeleted,0) = 0    
  
 order by FlightPurposeCD  
END 