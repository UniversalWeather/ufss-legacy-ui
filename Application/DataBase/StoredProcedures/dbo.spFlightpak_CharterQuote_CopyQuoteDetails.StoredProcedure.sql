/****** Object:  StoredProcedure [dbo].[spFlightpak_CharterQuote_CopyQuoteDetails]    Script Date: 01/17/2014 14:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- spFlightpak_CharterQuote_CopyQuoteDetails  10002422,10002427,'Mohan                         ',10002
  
ALTER  PROCEDURE  [dbo].[spFlightpak_CharterQuote_CopyQuoteDetails]         
 -- Add the parameters for the stored procedure here        
 @CQFileID Bigint,        
 @CQMainID Bigint,        
 @username varchar(30),    
 @CustomerID Bigint        
AS        
BEGIN        
 DECLARE  @NewCQMainID BIGINT           
 DECLARE  @NewQuoteNUM BIGINT     
 DECLARE  @OldQuoteNUM BIGINT   
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQMainID OUTPUT            
 set @NewQuoteNUM = 0    
    
 select @NewQuoteNUM = MAX(QuoteNUM)+1  from CQMain where CQFileID = @CQFileID and IsDeleted = 0  
   select @OldQuoteNUM = QuoteNUM  from CQMain where CQMainID  = @CQMainID
        
--CQMain        
 insert into CQMain     
  (    
       [CQMainID]  
      ,[CustomerID]  
      ,[CQFileID]  
      ,[FileNUM]  
      ,[QuoteNUM]  
      ,[QuoteID]  
      ,[CQSource]  
      ,[VendorID]  
      ,[FleetID]  
      ,[AircraftID]  
      ,[IsCustomerCQ]  
      ,[FederalTax]  
      ,[QuoteRuralTax]  
      ,[IsTaxable]  
      ,[PassengerRequestorID]  
      ,[RequestorName]  
      ,[RequestorPhoneNUM]  
      ,[IsSubmitted]  
      ,[LastSubmitDT]  
      ,[IsAccepted]  
      ,[LastAcceptDT]  
      ,[RejectDT]  
      ,[Comments]  
      ,[IsFinancialWarning]  
      ,[LiveTrip]  
      ,[MinimumDayUsageTotal]  
      ,[BillingCrewRONTotal]  
      ,[AdditionalCrewTotal]  
      ,[AdditionalCrewRONTotal1]  
      ,[StdCrewRONTotal]  
      ,[AdditionalCrewRONTotal2]  
      ,[WaitTMTotal]  
      ,[WaitChgTotal]  
      ,[HomebaseID]  
      ,[ClientID]  
      ,[CQLostBusinessID]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[StandardCrewDOM]  
      ,[StandardCrewINTL]  
      ,[DaysTotal]  
      ,[FlightHoursTotal]  
      ,[AverageFlightHoursTotal]  
      ,[AdjAmtTotal]  
      ,[UsageAdjTotal]  
      ,[IsOverrideUsageAdj]  
      ,[SegmentFeeTotal]  
      ,[IsOverrideSegmentFee]  
      ,[LandingFeeTotal]  
      ,[IsOverrideLandingFee]  
      ,[AdditionalFeeTotalD]  
      ,[FlightChargeTotal]  
      ,[SubtotalTotal]  
      ,[DiscountPercentageTotal]  
      ,[IsOverrideDiscountPercentage]  
      ,[DiscountAmtTotal]  
      ,[IsOverrideDiscountAMT]  
      ,[TaxesTotal]  
      ,[IsOverrideTaxes]  
      ,[MarginalPercentTotal]  
      ,[MarginTotal]  
      ,[QuoteTotal]  
      ,[IsOverrideTotalQuote]  
      ,[CostTotal]  
      ,[IsOverrideCost]  
      ,[PrepayTotal]  
      ,[RemainingAmtTotal]  
      ,[TripID]  
      ,[IsPrintUsageAdj]  
      ,[UsageAdjDescription]  
      ,[CustomUsageAdj]  
      ,[IsPrintAdditonalFees]  
      ,[AdditionalFeeDescription]  
      ,[AdditionalFeesTax]  
      ,[PositioningDescriptionINTL]  
      ,[IsAdditionalFees]  
      ,[CustomAdditionalFees]  
      ,[IsPrintLandingFees]  
      ,[LandingFeeDescription]  
      ,[CustomLandingFee]  
      ,[IsPrintSegmentFee]  
      ,[SegmentFeeDescription]  
      ,[CustomSegmentFee]  
      ,[IsPrintStdCrew]  
      ,[StdCrewROMDescription]  
      ,[CustomStdCrewRON]  
      ,[IsPrintAdditionalCrew]  
      ,[AdditionalCrewDescription]  
      ,[CustomAdditionalCrew]  
      ,[IsAdditionalCrewRON]  
      ,[AdditionalDescriptionCrewRON]  
      ,[CustomAdditionalCrewRON]  
      ,[IsPrintWaitingChg]  
      ,[WaitChgDescription]  
      ,[CustomWaitingChg]  
      ,[IsPrintFlightChg]  
      ,[FlightChgDescription]  
      ,[CustomFlightChg]  
      ,[IsPrintSubtotal]  
      ,[SubtotalDescription]  
      ,[IsPrintDiscountAmt]  
      ,[DescriptionDiscountAmt]  
      ,[IsPrintTaxes]  
      ,[TaxesDescription]  
      ,[IsPrintInvoice]  
      ,[InvoiceDescription]  
      ,[ReportQuote]  
      ,[IsPrintPay]  
      ,[PrepayDescription]  
      ,[IsPrintRemaingAMT]  
      ,[RemainingAmtDescription]  
      ,[IsInActive]  
      ,[IsDailyTaxAdj]  
      ,[IsLandingFeeTax]  
      ,[FeeGroupID]   
  )        
         
         
  select     
       @NewCQMainID  
      ,[CustomerID]  
      ,@CQFileID  
      ,[FileNUM]  
      ,@NewQuoteNUM   
      ,[QuoteID]  
      ,[CQSource]  
      ,[VendorID]  
      ,[FleetID]  
      ,[AircraftID]  
      ,[IsCustomerCQ]  
      ,[FederalTax]  
      ,[QuoteRuralTax]  
      ,[IsTaxable]  
      ,[PassengerRequestorID]  
      ,[RequestorName]  
      ,[RequestorPhoneNUM]  
      ,[IsSubmitted]  
      ,[LastSubmitDT]  
      ,[IsAccepted]  
      ,[LastAcceptDT]  
      ,[RejectDT]  
      ,[Comments]  
      ,[IsFinancialWarning]  
      ,null --[LiveTrip]  
      ,[MinimumDayUsageTotal]  
      ,[BillingCrewRONTotal]  
      ,[AdditionalCrewTotal]  
      ,[AdditionalCrewRONTotal1]  
      ,[StdCrewRONTotal]  
      ,[AdditionalCrewRONTotal2]  
      ,[WaitTMTotal]  
      ,[WaitChgTotal]  
      ,[HomebaseID]  
      ,[ClientID]  
      ,[CQLostBusinessID]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[StandardCrewDOM]  
      ,[StandardCrewINTL]  
      ,[DaysTotal]  
      ,[FlightHoursTotal]  
      ,[AverageFlightHoursTotal]  
      ,[AdjAmtTotal]  
      ,[UsageAdjTotal]  
      ,[IsOverrideUsageAdj]  
      ,[SegmentFeeTotal]  
      ,[IsOverrideSegmentFee]  
      ,[LandingFeeTotal]  
      ,[IsOverrideLandingFee]  
      ,[AdditionalFeeTotalD]  
      ,[FlightChargeTotal]  
      ,[SubtotalTotal]  
      ,[DiscountPercentageTotal]  
      ,[IsOverrideDiscountPercentage]  
      ,[DiscountAmtTotal]  
      ,[IsOverrideDiscountAMT]  
      ,[TaxesTotal]  
      ,[IsOverrideTaxes]  
      ,[MarginalPercentTotal]  
      ,[MarginTotal]  
      ,[QuoteTotal]  
      ,[IsOverrideTotalQuote]  
      ,[CostTotal]  
      ,[IsOverrideCost]  
      ,[PrepayTotal]  
      ,[RemainingAmtTotal]  
      ,null --[TripID]  
      ,[IsPrintUsageAdj]  
      ,[UsageAdjDescription]  
      ,[CustomUsageAdj]  
      ,[IsPrintAdditonalFees]  
      ,[AdditionalFeeDescription]  
      ,[AdditionalFeesTax]  
      ,[PositioningDescriptionINTL]  
      ,[IsAdditionalFees]  
      ,[CustomAdditionalFees]  
      ,[IsPrintLandingFees]  
      ,[LandingFeeDescription]  
      ,[CustomLandingFee]  
      ,[IsPrintSegmentFee]  
      ,[SegmentFeeDescription]  
      ,[CustomSegmentFee]  
      ,[IsPrintStdCrew]  
      ,[StdCrewROMDescription]  
      ,[CustomStdCrewRON]  
      ,[IsPrintAdditionalCrew]  
      ,[AdditionalCrewDescription]  
      ,[CustomAdditionalCrew]  
      ,[IsAdditionalCrewRON]  
      ,[AdditionalDescriptionCrewRON]  
      ,[CustomAdditionalCrewRON]  
      ,[IsPrintWaitingChg]  
      ,[WaitChgDescription]  
      ,[CustomWaitingChg]  
      ,[IsPrintFlightChg]  
      ,[FlightChgDescription]  
      ,[CustomFlightChg]  
      ,[IsPrintSubtotal]  
      ,[SubtotalDescription]  
      ,[IsPrintDiscountAmt]  
      ,[DescriptionDiscountAmt]  
      ,[IsPrintTaxes]  
      ,[TaxesDescription]  
      ,[IsPrintInvoice]  
      ,[InvoiceDescription]  
      ,[ReportQuote]  
      ,[IsPrintPay]  
      ,[PrepayDescription]  
      ,[IsPrintRemaingAMT]  
      ,[RemainingAmtDescription]  
      ,[IsInActive]  
      ,[IsDailyTaxAdj]  
      ,[IsLandingFeeTax]  
      ,[FeeGroupID]   
   from CQMain where CQFileID = @CQFileID and CQMainID = @CQMainID     
      
      
  --CQLEg           
  CREATE Table #TempLeg (        
  rownum int identity (1,1),        
  NewCQLegId Bigint,        
  OldCQLegID Bigint        
  )        
          
  insert into #TempLeg (NewCQLegId,OldCQLegID ) select null, CQLegID from CQLeg  where CQMainID = @CQMainID        
          
  Declare @Rownumber int        
  DECLARE  @NewCQLegID BIGINT            
  set @Rownumber = 1        
          
  while (select COUNT(1) from #TempLeg where NewCQLegId is null) >0        
  begin        
   Set @NewCQLegID =0        
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQLegID OUTPUT         
   update #TempLeg set NewCQLegId = @NewCQLegID where rownum = @Rownumber        
   set @Rownumber = @Rownumber +1         
  End        
         
         
--Legs        
  INSERT INTO [CQLeg]            
         ([CQLegID]  
      ,[CustomerID]  
      ,[CQMainID]  
      ,[QuoteID]  
      ,[LegNUM]  
      ,[LegID]  
      ,[DAirportID]  
      ,[AAirportID]  
      ,[Distance]  
      ,[PowerSetting]  
      ,[TakeoffBIAS]  
      ,[LandingBIAS]  
      ,[TrueAirSpeed]  
      ,[WindsBoeingTable]  
      ,[ElapseTM]  
      ,[IsDepartureConfirmed]  
      ,[IsArrivalConfirmation]  
      ,[DepartureDTTMLocal]  
      ,[ArrivalDTTMLocal]  
      ,[DepartureGreenwichDTTM]  
      ,[ArrivalGreenwichDTTM]  
      ,[HomeDepartureDTTM]  
      ,[HomeArrivalDTTM]  
      ,[FBOID]  
      ,[FlightPurposeDescription]  
      ,[DutyHours]  
      ,[RestHours]  
      ,[FlightHours]  
      ,[Notes]  
      ,[DutyTYPE]  
      ,[CrewDutyRulesID]  
      ,[IsDutyEnd]  
      ,[FedAviationRegNUM]  
      ,[WindReliability]  
      ,[CQOverRide]  
      ,[NextLocalDTTM]  
      ,[NextGMTDTTM]  
      ,[NextHomeDTTM]  
      ,[ChargeRate]  
      ,[ChargeTotal]  
      ,[IsPositioning]  
      ,[IsTaxable]  
      ,[FeeGroupDescription]  
      ,[LegMargin]  
      ,[CrewDutyAlert]  
      ,[RemainOverNightCNT]  
      ,[TaxRate]  
      ,[PassengerTotal]  
      ,[ReservationAvailable]  
      ,[IsPrintable]  
      ,[FromDescription]  
      ,[ToDescription]  
      ,[IsRural]  
      ,[CrewNotes]  
      ,[ClientID]  
      ,[DayRONCNT]  
      ,[IsSameDay]  
      ,[IsErrorTag]  
      ,[OQAAirportID]  
      ,[OQDAirportID]  
      ,[OIAAirportID]  
      ,[OIDAirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[PAXBlockedSeat]        
    )            
           
    select  
       NewCQLegID   
      ,[CustomerID]  
      ,@NewCQMainID  
      ,[QuoteID]  
      ,[LegNUM]  
      ,[LegID]  
      ,[DAirportID]  
      ,[AAirportID]  
      ,[Distance]  
      ,[PowerSetting]  
      ,[TakeoffBIAS]  
      ,[LandingBIAS]  
      ,[TrueAirSpeed]  
      ,[WindsBoeingTable]  
      ,[ElapseTM]  
      ,[IsDepartureConfirmed]  
      ,[IsArrivalConfirmation]  
      ,[DepartureDTTMLocal]  
      ,[ArrivalDTTMLocal]  
      ,[DepartureGreenwichDTTM]  
      ,[ArrivalGreenwichDTTM]  
      ,[HomeDepartureDTTM]  
      ,[HomeArrivalDTTM]  
      ,[FBOID]  
      ,[FlightPurposeDescription]  
      ,[DutyHours]  
      ,[RestHours]  
      ,[FlightHours]  
      ,[Notes]  
      ,[DutyTYPE]  
      ,[CrewDutyRulesID]  
      ,[IsDutyEnd]  
      ,[FedAviationRegNUM]  
      ,[WindReliability]  
      ,[CQOverRide]  
      ,[NextLocalDTTM]  
      ,[NextGMTDTTM]  
      ,[NextHomeDTTM]  
      ,[ChargeRate]  
      ,[ChargeTotal]  
      ,[IsPositioning]  
      ,[IsTaxable]  
      ,[FeeGroupDescription]  
      ,[LegMargin]  
      ,[CrewDutyAlert]  
      ,[RemainOverNightCNT]  
      ,[TaxRate]  
      ,[PassengerTotal]  
      ,[ReservationAvailable]  
      ,[IsPrintable]  
      ,[FromDescription]  
      ,[ToDescription]  
      ,[IsRural]  
      ,[CrewNotes]  
      ,[ClientID]  
      ,[DayRONCNT]  
      ,[IsSameDay]  
      ,[IsErrorTag]  
      ,[OQAAirportID]  
      ,[OQDAirportID]  
      ,[OIAAirportID]  
      ,[OIDAirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[PAXBlockedSeat]   
          
    from CQLeg CL, #TempLeg T        
    where CL.CQLegID = T.OldCQLegID        
       
       
  --FleetChargeDetail  
       
       
      CREATE Table #TempCQFleetChargeDetail (      
  rownum int identity (1,1),      
  NewCQFleetChargeDetailId Bigint,      
  OldCQFleetChargeDetailID Bigint      
  --NewLegId Bigint,      
  --OldLegID Bigint      
  )      
    
  insert into #TempCQFleetChargeDetail(NewCQFleetChargeDetailId,OldCQFleetChargeDetailID ) select null, CQFleetChargeDetailID from CQFleetChargeDetail  where CQMainID = @CQMainID        
    
  DECLARE  @NewCQFleetChargeDetailId BIGINT    
  --Declare @Rownumber int        
  
  set @Rownumber = 1        
          
  while (select COUNT(1) from #TempCQFleetChargeDetail where @NewCQFleetChargeDetailId is null) >0        
  begin        
   Set @NewCQFleetChargeDetailId =0        
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQFleetChargeDetailId OUTPUT         
   update #TempCQFleetChargeDetail set NewCQFleetChargeDetailId = @NewCQFleetChargeDetailId where rownum = @Rownumber        
   set @Rownumber = @Rownumber +1         
  End    
           
--CQFLEETCHARGEDETAIL       
       
     Insert into CQFleetChargeDetail  
     (  
       [CQFleetChargeDetailID]  
      ,[CustomerID]  
      ,[CQMainID]  
      ,[FileNUM]  
      ,[QuoteID]  
      ,[AccountID]  
      ,[AircraftCharterRateID]  
      ,[FleetCharterRateID]  
      ,[CQCustomerCharterRateID]  
      ,[OrderNUM]  
      ,[CQFlightChargeDescription]  
      ,[ChargeUnit]  
      ,[NegotiatedChgUnit]  
      ,[Quantity]  
      ,[BuyDOM]  
      ,[SellDOM]  
      ,[IsTaxDOM]  
      ,[IsDiscountDOM]  
      ,[BuyIntl]  
      ,[SellIntl]  
      ,[IsTaxIntl]  
      ,[IsDiscountIntl]  
      ,[StandardCrewDOM]  
      ,[StandardCrewINTL]  
      ,[MinimumDailyRequirement]  
      ,[YearMade]  
      ,[ExteriorColor]  
      ,[ColorIntl]  
      ,[IsAFIS]  
      ,[IsUWAData]  
      ,[AircraftFlightChg1]  
      ,[AircraftFlightChg2]  
      ,[BuyAircraftFlightIntl]  
      ,[FeeAMT]  
      ,[SellAircraftFlight]  
      ,[IsToPrint]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
     )  
     select T.NewCQFleetChargeDetailId  
      ,[CustomerID]  
      ,@NewCQMainID  
      ,[FileNUM]  
      ,[QuoteID]  
      ,[AccountID]  
      ,[AircraftCharterRateID]  
      ,[FleetCharterRateID]  
      ,[CQCustomerCharterRateID]  
      ,[OrderNUM]  
      ,[CQFlightChargeDescription]  
      ,[ChargeUnit]  
      ,[NegotiatedChgUnit]  
      ,[Quantity]  
      ,[BuyDOM]  
      ,[SellDOM]  
      ,[IsTaxDOM]  
      ,[IsDiscountDOM]  
      ,[BuyIntl]  
      ,[SellIntl]  
      ,[IsTaxIntl]  
      ,[IsDiscountIntl]  
      ,[StandardCrewDOM]  
      ,[StandardCrewINTL]  
      ,[MinimumDailyRequirement]  
      ,[YearMade]  
      ,[ExteriorColor]  
      ,[ColorIntl]  
      ,[IsAFIS]  
      ,[IsUWAData]  
      ,[AircraftFlightChg1]  
      ,[AircraftFlightChg2]  
      ,[BuyAircraftFlightIntl]  
      ,[FeeAMT]  
      ,[SellAircraftFlight]  
      ,[IsToPrint]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
     from CQFleetChargeDetail CF, #TempCQFleetChargeDetail T        
    where CF.CQFleetChargeDetailID = T.NewCQFleetChargeDetailId      
       
--PAX    
   CREATE Table #TempPax (      
  rownum int identity (1,1),      
  NewCQPassengerListID Bigint,      
  oldCQPassengerListID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
  insert into #TempPax (NewCQPassengerListID ,      
   oldCQPassengerListID ,      
   NewLegId ,      
   OldLegID )      
  select null,  [CQPassengerID], NewCQLegId, OldCQLegID      
  from CQPassenger CQP, #TempLeg TL      
  where CQP.CQLegID = TL.OldCQLegID      
  DECLARE  @NewCQPassengerListID BIGINT         
  set @Rownumber = 1      
  while (select COUNT(1) from #TempPax where NewCQPassengerListID is null) >0      
  begin      
   Set @NewCQPassengerListID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQPassengerListID OUTPUT       
   update #TempPax set NewCQPassengerListID = @NewCQPassengerListID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End    
  ---CQPax    
  INSERT INTO [CQPassenger]      
           (      
            [CQPassengerID]  
      ,[CustomerID]  
      ,[CQLegID]  
      ,[QuoteID]  
      ,[LegID]  
      ,[PassengerRequestorID]  
      ,[PassengerName]  
      ,[FlightPurposeID]  
      ,[Billing]  
      ,[IsNonPassenger]  
      ,[IsBlocked]  
      ,[OrderNUM]  
      ,[WaitList]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[PassportID]  
           )      
     Select      
           
           TP.NewCQPassengerListID    
                   ,[CustomerID]  
      ,TP.NewLegId      
      ,[QuoteID]  
      ,[LegID]  
      ,[PassengerRequestorID]  
      ,[PassengerName]  
      ,[FlightPurposeID]  
      ,[Billing]  
      ,[IsNonPassenger]  
      ,[IsBlocked]  
      ,[OrderNUM]  
      ,[WaitList]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
      ,[PassportID]      
                 
           from [CQPassenger] CQP, #TempPax TP      
     where CQP.CQPassengerID  = TP.oldCQPassengerListID      
        
--LOGISTICS -> fbo, transport, catering   
  
  
---FBO  
CREATE Table #TempFBO (      
  rownum int identity (1,1),      
  NewCQFBOID Bigint,      
  oldCQFBOID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempFBO (NewCQFBOID ,      
   oldCQFBOID ,      
   NewLegId ,      
   OldLegID )      
  select null,CQFBOListID, NewCQLegId, OldCQLegID      
  from CQFBOList CQF, #TempLeg TL      
  where CQF.CQLegID = TL.OldCQLegID      
        
  DECLARE  @NewCQFBOID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempFBO where NewCQFBOID is null) >0      
  begin      
   Set @NewCQFBOID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQFBOID OUTPUT       
   update #TempFBO set NewCQFBOID = @NewCQFBOID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [dbo].[CQFBOList]      
           ([CQFBOListID]  
      ,[CustomerID]  
      ,[CQLegID]  
      ,[QuoteID]  
      ,[LegID]  
      ,[RecordType]  
      ,[FBOID]  
      ,[CQFBOListDescription]  
      ,[PhoneNUM]  
      ,[AirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]    
           )      
     Select      
           TF.NewCQFBOID                               
          ,[CustomerID]  
          ,TF.NewLegId     
          ,[QuoteID]  
         ,[LegID]  
         ,[RecordType]  
         ,[FBOID]  
         ,[CQFBOListDescription]  
         ,[PhoneNUM]  
         ,[AirportID]  
         ,[FileNUM]  
         ,[LastUpdUID]  
         ,[LastUpdTS]  
         ,[IsDeleted]  
                 
           from [CQFBOList] CQF, #TempFBO TF      
     where CQF.CQFBOListID = TF.oldCQFBOID      
           
  Drop table #TempFBO     
    
   --Catering  
           
  CREATE Table #TempCatering (      
  rownum int identity (1,1),      
  NewCQCateringID Bigint,      
  oldCQCateringID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempCatering (NewCQCateringID ,      
   oldCQCateringID ,      
   NewLegId ,      
   OldLegID )      
  select null,  CQCateringListID, NewCQLegId, OldCQLegID      
  from CQCateringList CQC, #TempLeg TL      
  where CQC.CQLegID = TL.OldCQLegID      
        
  DECLARE  @NewCQCateringID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempCatering where NewCQCateringID is null) >0      
  begin      
   Set @NewCQCateringID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQCateringID OUTPUT       
   update #TempCatering set NewCQCateringID = @NewCQCateringID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [dbo].[CQCateringList]      
           ([CQCateringListID]  
      ,[CustomerID]  
      ,[CQLegID]  
      ,[QuoteID]  
      ,[LegID]  
      ,[RecordType]  
      ,[CateringID]  
      ,[CQCateringListDescription]  
      ,[PhoneNUM]  
      ,[Rate]  
      ,[AirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
           )      
     Select                
                
       TC.NewCQCateringID      
      ,[CustomerID]  
      ,TC.NewLegId     
      ,[QuoteID]  
      ,[LegID]  
      ,[RecordType]  
      ,[CateringID]  
      ,[CQCateringListDescription]  
      ,[PhoneNUM]  
      ,[Rate]  
      ,[AirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]   
                 
           from [CQCateringList] CQC, #TempCatering TC      
     where CQC.CQCateringListID = TC.oldCQCateringID      
        
  Drop table #TempCatering   
    
    
   --Transport  
           
  CREATE Table #TempTransport (      
  rownum int identity (1,1),      
  NewCQTransportID Bigint,      
  oldCQTransportID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempTransport (NewCQTransportID ,      
   oldCQTransportID ,      
   NewLegId ,      
   OldLegID )      
  select null,CQTransportListID, NewCQLegId,OldCQLegID      
  from CQTransportList CQT, #TempLeg TL      
  where CQT.CQLegID = TL.OldCQLegID      
        
  DECLARE  @NewCQTransportID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempTransport where NewCQTransportID is null) >0      
  begin      
   Set @NewCQTransportID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQTransportID OUTPUT       
   update #TempTransport set NewCQTransportID = @NewCQTransportID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
        
  INSERT INTO [dbo].[CQTransportList]      
           ([CQTransportListID]  
      ,[CustomerID]  
      ,[CQLegID]  
      ,[QuoteID]  
      ,[LegID]  
      ,[RecordType]  
      ,[TransportID]  
      ,[CQTransportListDescription]  
      ,[PhoneNUM]  
      ,[Rate]  
      ,[AirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
           )      
     Select                
                
       TT.NewCQTransportID     
      ,[CustomerID]  
      ,TT.NewLegId          
      ,[QuoteID]  
      ,[LegID]  
      ,[RecordType]  
      ,[TransportID]  
      ,[CQTransportListDescription]  
      ,[PhoneNUM]  
      ,[Rate]  
      ,[AirportID]  
      ,[FileNUM]  
      ,[LastUpdUID]  
      ,[LastUpdTS]  
      ,[IsDeleted]  
                 
           from [CQTransportList] CQT, #TempTransport TT      
     where CQT.CQTransportListID = TT.oldCQTransportID      
  Drop table #TempTransport       
        
        
        
  --Hotel
        
       CREATE Table #TempHotel (      
  rownum int identity (1,1),      
  NewCQHotelID Bigint,      
  oldCQHotelID Bigint,      
  NewLegId Bigint,      
  OldLegID Bigint      
  )      
        
  insert into #TempHotel (NewCQHotelID ,      
   oldCQHotelID ,      
   NewLegId ,      
   OldLegID )      
  select null,CQHotelListID, NewCQLegId,OldCQLegID      
  from CQHotelList CQH, #TempLeg TL      
  where CQH.CQLegID = TL.OldCQLegID      
        
  DECLARE  @NewCQHotelID BIGINT         
        
  set @Rownumber = 1      
       
  while (select COUNT(1) from #TempHotel where NewCQHotelID is null) >0      
  begin      
   Set @NewCQHotelID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @NewCQHotelID OUTPUT       
   update #TempHotel set NewCQHotelID = @NewCQHotelID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
  End      
   
   
   INSERT INTO [dbo].[CQHotelList]      
           (
   [CQHotelListID]
      ,[CustomerID]
      ,[CQLegID]
      ,[QuoteID]
      ,[LegID]
      ,[RecordType]
      ,[HotelID]
      ,[CQHotelListDescription]
      ,[PhoneNUM]
      ,[Rate]
      ,[AirportID]
      ,[FileNUM]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      )
    select 
    
      TH .NewCQHotelID     
      ,[CustomerID]  
      ,TH .NewLegId          
      ,[QuoteID]
      ,[LegID]
      ,[RecordType]
      ,[HotelID]
      ,[CQHotelListDescription]
      ,[PhoneNUM]
      ,[Rate]
      ,[AirportID]
      ,[FileNUM]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
                 
           from [CQHotelList] CQH, #TempHotel TH      
     where CQH.CQHotelListID = TH.oldCQHotelID    
   
   --History 
   
   DECLARE  @CQHistoryID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQHistoryID OUTPUT 
	--select top 1 @RevisionNumber = isnull([RevisionNumber],0) +1 from CQHistory where CQMainID = @CQMainID order by LastUpdTS desc
	
	--select @NewCQMainID, @CQHistoryID, @OldQuoteNUM, @username
	
	--set @RevisionNumber = ISNULL(@RevisionNumber,1)
INSERT INTO [CQHistory] ([CQHistoryID], [CustomerID], [CQMainID], [LogisticsHistory], [LastUpdUID], [LastUpdTS], [IsDeleted]) VALUES
 (@CQHistoryID, @CustomerID, @NewCQMainID, 'Quote copied from '+ convert(varchar(20),@OldQuoteNUM) +' on '+ convert(varchar(30), getutcdate()), @username,  getutcdate(), 0)
 
  Drop table #TempHotel  
  Drop table #TempLeg  
    
    
  CREATE Table #TempFileWarehouse
   (      
     rownum int identity (1,1),       
     FileWarehouseID Bigint,     
     NewFileWarehouseID Bigint    
   ) 
   insert into #TempFileWarehouse(FileWarehouseID,NewFileWarehouseID) select FileWarehouseID,null from FileWarehouse  where FileWarehouse.RecordID = @CQMainID  and FileWarehouse.RecordType = 'CQQuoteOnFile' and FileWarehouse.CustomerID = @CustomerID              
   
   DECLARE  @NewFileWarehouseID BIGINT      
   set @Rownumber = 1   
   while (select COUNT(1) from #TempFileWarehouse where NewFileWarehouseID is null) >0      
   begin      
   Set @NewFileWarehouseID =0      
   EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @NewFileWarehouseID OUTPUT     
   update #TempFileWarehouse set NewFileWarehouseID = @NewFileWarehouseID where rownum = @Rownumber      
   set @Rownumber = @Rownumber +1       
   End      
   INSERT INTO [dbo].[FileWarehouse]  
    (
    FileWarehouseID,  
    [UWAFileName],  
    [UWAFilePath],  
    [UWAWebpageName],  
    [CustomerID],  
    [RecordID],  
    [SequenceNumber],  
    [RecordType],   
    [LastUpdUID],  
    [LastUpdTS]
     )     
     select 
      tf.NewFileWarehouseID
      ,[UWAFileName]  
      ,[UWAFilePath]  
      ,[UWAWebpageName]  
      ,@CustomerID  
      ,@NewCQMainID 
      ,[SequenceNumber]  
      ,[RecordType]  
      ,@username  
      ,GETUTCDATE() 
      from FileWarehouse f,#TempFileWarehouse tf where f.FileWarehouseID = tf.FileWarehouseID 
    
     Drop table #TempFileWarehouse 
     select @NewCQMainID as CQMainID        
        
    
END   
     
    

