
GO
/****** Object:  StoredProcedure [dbo].[spGetCQCustomerContact]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCQCustomerContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCQCustomerContact]
GO

CREATE PROCEDURE spGetCQCustomerContact
(@CustomerID bigint)
AS BEGIN

	SET NOCOUNT OFF;	
	
	SELECT	CQCustomerContact.CQCustomerContactID
			,CQCustomerContact.CustomerID
			,CQCustomerContact.CQCustomerID
			,CQCustomerContact.CQCustomerContactCD
			,CQCustomerContact.IsChoice
			,CQCustomerContact.FirstName
			,CQCustomerContact.Addr1
			,CQCustomerContact.Addr2
			,CQCustomerContact.Addr3
			,CQCustomerContact.CityName
			,CQCustomerContact.StateName
			,CQCustomerContact.PostalZipCD
			,CQCustomerContact.CountryID
			,CQCustomerContact.PhoneNum
			,CQCustomerContact.OtherPhoneNum
			,CQCustomerContact.PrimaryMobileNum
			,CQCustomerContact.SecondaryMobileNum
			,CQCustomerContact.FaxNum
			,CQCustomerContact.HomeFaxNum
			,CQCustomerContact.CreditName1
			,CQCustomerContact.CreditNum1
			,CQCustomerContact.CardType1
			,CQCustomerContact.SecurityCode1
			,CQCustomerContact.ExpirationDate1
			,CQCustomerContact.CreditName2
			,CQCustomerContact.CreditNum2
			,CQCustomerContact.CardType2
			,CQCustomerContact.SecurityCode2
			,CQCustomerContact.ExpirationDate2
			,CQCustomerContact.MoreInfo
			,CQCustomerContact.Notes
			,CQCustomerContact.Title
			,CQCustomerContact.LastName
			,CQCustomerContact.MiddleName
			,CQCustomerContact.EmailID
			,CQCustomerContact.PersonalEmailID
			,CQCustomerContact.OtherEmailID
			,CQCustomerContact.ContactName
			,CQCustomerContact.BusinessPhoneNum
			,CQCustomerContact.BusinessFaxNum
			,CQCustomerContact.BusinessEmailID
			,CQCustomerContact.IsInActive
			,CQCustomerContact.LastUpdUID
			,CQCustomerContact.LastUpdTS
			,CQCustomerContact.IsDeleted
			,Country.CountryCD AS CountryCD
			,Country.CountryName AS CountryName
	FROM CQCustomerContact
	LEFT OUTER JOIN Country ON Country.CountryID = CQCustomerContact.CountryID
	WHERE CQCustomerContact.CustomerID = @CustomerID
		AND CQCustomerContact.IsDeleted = 0
	ORDER BY CQCustomerContact.CQCustomerContactCD ASC
	
END