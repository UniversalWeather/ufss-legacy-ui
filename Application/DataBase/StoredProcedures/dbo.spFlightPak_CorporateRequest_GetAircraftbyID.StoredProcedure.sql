/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAircraftbyID]    Script Date: 04/18/2013 20:44:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetAircraftbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAircraftbyID]
GO

Create procedure [dbo].[spFlightPak_CorporateRequest_GetAircraftbyID]
(@CustomerID BIGINT,@AircraftID BIGINT)  
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 18/4/2013    
-- Description: Get the Aircraft information    
-- =============================================    
SET NOCOUNT ON    
SELECT [AircraftID]
      ,[CustomerID]
      ,[AircraftCD]
      ,[AircraftDescription]
      ,[ChargeRate]
      ,[ChargeUnit]
      ,[PowerSetting]
      ,[PowerDescription]
      ,[WindAltitude]
      ,[PowerSettings1Description]
      ,[PowerSettings1TrueAirSpeed]
      ,[PowerSettings1HourRange]
      ,[PowerSettings1TakeOffBias]
      ,[PowerSettings1LandingBias]
      ,[PowerSettings2Description]
      ,[PowerSettings2TrueAirSpeed]
      ,[PowerSettings2HourRange]
      ,[PowerSettings2TakeOffBias]
      ,[PowerSettings2LandingBias]
      ,[PowerSettings3Description]
      ,[PowerSettings3TrueAirSpeed]
      ,[PowerSettings3HourRange]
      ,[PowerSettings3TakeOffBias]
      ,[PowerSettings3LandingBias]
      ,[IsFixedRotary]
      ,[ClientID]
      ,[CQChargeRate]
      ,[CQChargeUnit]
      ,[PositionRate]
      ,[PostionUnit]
      ,[StandardCrew]
      ,[StandardCrewRON]
      ,[AdditionalCrew]
      ,[AdditionalCrewRON]
      ,[CharterQuoteWaitTM]
      ,[LandingFee]
      ,[CostBy]
      ,[FixedCost]
      ,[PercentageCost]
      ,[TaxRON]
      ,[AdditionalCrewTax]
      ,[IsCharterQuoteWaitTax]
      ,[LandingTax]
      ,[DescriptionRON]
      ,[AdditionalCrewDescription]
      ,[MinDailyREQ]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[AircraftSize]
      ,[AircraftTypeCD]
      ,[IntlStdCrewNum]
      ,[DomesticStdCrewNum]
      ,[MinimumDayUseHrs]
      ,[DailyUsageAdjTax]
      ,[LandingFeeTax]
      ,[IsDeleted]
      ,[IsInActive]
      ,[MarginalPercentage]
  FROM Aircraft
   where IsDeleted = 0  AND AircraftID=@AircraftID



GO


