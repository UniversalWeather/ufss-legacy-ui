
/****** Object:  StoredProcedure [dbo].[spDeleteFeeGroup]    Script Date: 02/15/2013 19:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spDeleteFeeGroup]        
(        
 @CustomerID bigint        
,@FeeGroupID bigint        
,@LastUpdUID varchar(30)        
,@LastUpdTS datetime        
,@IsDeleted bit        
)        
-- =============================================        
-- Author:Suresh Jinka        
-- Create date: 05/02/2013       
-- Description: Delete the DelayType  information        
-- =============================================        
as        
begin         
--Check if the record is not being used anywhere in the application before Delete          
 DECLARE @RecordUsed INT        
DECLARE @ENTITYNAME VARCHAR(50)                
SET @ENTITYNAME = N'FeeGroup'; -- Type Table Name        
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @FeeGroupID,        
@RecordUsed OUTPUT        
--End of Delete Check        
          
  if (@RecordUsed <> 0)        
 Begin        
 RAISERROR(N'-500010', 17, 1)        
 end        
 ELSE          
  BEGIN          
   --To store the lastupdateddate as UTC date         
          
   SET @LastUpdTS = GETUTCDATE()          
           
   --End of UTC date        
SET NoCOUNT ON        
        
UPDATE FeeGroup        
   SET     [LastUpdUID]=@LastUpdUID        
           ,[LastUpdTS]=@LastUpdTS             
           ,[IsDeleted]=@IsDeleted                   
 WHERE CustomerID =@CustomerID and FeeGroupID=@FeeGroupID        
        
end        
END 