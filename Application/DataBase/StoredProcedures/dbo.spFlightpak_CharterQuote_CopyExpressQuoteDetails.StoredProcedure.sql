
/****** Object:  StoredProcedure [dbo].[spFlightpak_CharterQuote_CopyExpressQuoteDetails]    Script Date: 02/28/2013 15:25:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CharterQuote_CopyExpressQuoteDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CharterQuote_CopyExpressQuoteDetails]
GO


/****** Object:  StoredProcedure [dbo].[spFlightpak_CharterQuote_CopyExpressQuoteDetails]    Script Date: 02/28/2013 15:25:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 --spFlightpak_CharterQuote_CopyExpressQuoteDetails  10003140,'CARRIE_3',10003
  
CREATE PROCEDURE  [dbo].[spFlightpak_CharterQuote_CopyExpressQuoteDetails]       
 -- Add the parameters for the stored procedure here      
 @CQFileID Bigint,           
 @username varchar(30),  
 @CustomerID Bigint      
AS      
BEGIN      
 Update CQFile SET ExpressQuote = 0,CQFileDescription='EXPRESS QUOTE' WHERE CQFileID = @CQFileID
END    
    
  

GO


