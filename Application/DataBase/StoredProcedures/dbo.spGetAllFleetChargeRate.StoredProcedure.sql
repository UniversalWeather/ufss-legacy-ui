
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleetGroup]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetChargeRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetChargeRate]
GO
CREATE procedure [dbo].[spGetAllFleetChargeRate](@FLeetID BIGINT)  
as  
SET NoCOUNT ON  
  
BEGIN  
   SELECT   [FleetChargeRateID] 
           ,[CustomerID]  
           ,[FleetID]   
           ,[AircraftCD]   
           ,[BeginRateDT]  
           ,[EndRateDT]  
           ,[ChargeRate]  
           ,[ChargeUnit]  
           ,[LastUpdUID]             
           ,[LastUpdTS]             
           ,[IsDeleted]
           ,[AircraftID]
           ,IsInactive  
  FROM  FleetChargeRate where FLeetID=@FLeetID AND IsNull(FleetChargeRate.IsDeleted,0)=0     
  Order By BeginRateDT    
end
GO
