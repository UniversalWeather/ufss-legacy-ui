
/****** Object:  StoredProcedure [dbo].[spGetAllCateringByAirportID]    Script Date: 1/8/2015 10:52:38 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCateringByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCateringByAirportID]
GO

CREATE Procedure [dbo].[spGetAllCateringByAirportID](@CustomerID BIGINT, @AirportID BIGINT)    
as    
-- =============================================    
-- Modified By: Rajitha
-- Create date: 08/01/2015
-- Description: Get the Catering information for an Airport    
-- =============================================    
-- =============================================    
-- Author:Prakash P    
-- Create date: 11/09/2012    
-- Description: Get the Catering information for an Airport    
-- =============================================    
--Altered By Rajesh: 05-May-2015 (Added column PostalZipCD in select)    
    
DECLARE @UWACustomerID BIGINT  
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output    
DECLARE @ICAOCD CHAR(4)  
DECLARE @UWAAirportId BIGINT   
DECLARE @CustomAirportID BIGINT
   
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD     

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD 
      
SET NOCOUNT ON       
      
 SELECT          
 a.[CateringID]      
 ,a.[CustomerID]       
          ,a.[AirportID]       
          ,a.[CateringCD]       
          ,a.[IsChoice]       
          ,a.[CateringVendor]        
          ,a.[PhoneNum]       
          ,a.[FaxNum]       
          ,a.[ContactName]       
          ,a.[Remarks]       
          ,a.[NegotiatedRate]       
          ,a.[LastUpdUID]       
          ,a.[LastUpdTS]       
          ,a.[UpdateDT]       
          ,a.[RecordType]       
          ,a.[SourceID]       
          ,a.[ControlNum]       
          ,a.[IsInActive]       
          ,a.[TollFreePhoneNum]       
          ,a.[WebSite]       
          ,(SELECT CAST(CASE WHEN a.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar )) as Filter    
          ,a.[UWAMaintFlag]    
          ,a.[UWAID]       
             ,a.[IsDeleted]      
             ,a.[UWAUpdates]      
             ,a.[BusinessEmail]      
          ,a.[ContactBusinessPhone]      
          ,a.[CellPhoneNum]      
          ,a.[ContactEmail]      
          ,a.[Addr1]      
          ,a.[Addr2]      
          ,a.[Addr3]      
          ,a.[City]      
          ,a.[StateProvince]       
          ,a.[MetroID]   
          ,a.[CountryID]          
		 ,a.ExchangeRateID    
		 ,a.NegotiatedTerms    
		 ,a.SundayWorkHours    
		 ,a.MondayWorkHours    
		 ,a.TuesdayWorkHours    
		 ,a.WednesdayWorkHours    
		 ,a.ThursdayWorkHours    
		 ,a.FridayWorkHours    
		 ,a.SaturdayWorkHours     
	     ,M.MetroCD      
	     ,PostalZipCD      
         ,C.CountryCD         
 FROM  [Catering] a LEFT OUTER JOIN [metro] M on a.MetroID = M.MetroID          
 LEFT OUTER JOIN [Country] C on a.CountryID = C.CountryID      
        
 WHERE   
 ((((A.AirportID = @CustomAirportID       
  AND A.CustomerID = @CustomerID ) or (A.AirportID  = @UWAAirportId       
  AND  A.CustomerID = @CustomerID )) AND A.IsDeleted = 0) OR   
      
 (a.AirportID = @UWAAirportId    
 AND a.CustomerID = @UWACustomerID    
 AND a.IsDeleted = 0    
 AND a.CateringCD NOT IN     
  (SELECT CateringCD FROM [Catering]   
  WHERE   
  ((AirportID  = @CustomAirportID       
  AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId       
  AND  CustomerID = @CustomerID))  
    
  AND IsDeleted = 0)    
 ))    
     
 ORDER BY a.[CateringVendor]
 
 GO


