

/****** Object:  StoredProcedure [dbo].[spflightpak_CheckSiflDate]    Script Date: 01/23/2014 18:49:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spflightpak_CheckSiflDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spflightpak_CheckSiflDate]
GO



/****** Object:  StoredProcedure [dbo].[spflightpak_CheckSiflDate]    Script Date: 01/23/2014 18:49:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[spflightpak_CheckSiflDate]
(
@CustomerID bigint,
@StateDate Date,
@EndDate Date
)
as 
begin

  Select Top 3* from FareLevel where StartDT <= @EndDate and EndDT >= @StateDate and CustomerID = @CustomerID and IsDeleted = 0  
  
end

GO


