

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllPassenger]    Script Date: 03/14/2014 16:07:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllPassenger]
GO
/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllPassenger]    Script Date: 03/14/2014 16:07:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetAllPassenger]              
(@CustomerID BIGINT  
,@ClientID BIGINT
,@CQCustomerID BIGINT  
,@PassengerID BIGINT  
,@PassengerCD varchar(5)  
,@ActiveOnly BIT =0  
,@RequestorOnly BIT =0  
,@ICAOID VARCHAR(4)   
,@PaxGroupID BIGINT)            
AS              
BEGIN              
-- ====================================================              
-- Author: Karthikeyan.S             
-- Create date: 26/02/2014  
-- Modified date: 13/03/2014              
-- Description: Get Passenger Requestor Information  
-- EXEC sp_fss_GetAllPassenger 10003,0,0,0,'',0,0,'',0
-- EXEC sp_fss_GetAllPassenger 10003,1,0,0,'',0,0,'',0
-- EXEC sp_fss_GetAllPassenger 10003,0,0,0,'14',0,0,'',0
-- EXEC sp_fss_GetAllPassenger 10003,0,0,10003361465,'',0,0,'',0 
-- EXEC sp_fss_GetAllPassenger 10003,0,10003189398,0,'',0,0,'',0
-- ====================================================            
IF(@PaxGroupID>0)        
BEGIN          
SELECT   Passenger.PassengerRequestorID            
  ,CASE WHEN Passenger.PassengerRequestorCD IS NULL THEN '' ELSE LTRIM(RTRIM(Passenger.PassengerRequestorCD)) END AS PassengerRequestorCD            
  ,Passenger.PassengerName            
  ,Passenger.CustomerID            
  ,Passenger.DepartmentID            
  ,Passenger.PassengerDescription            
  ,Passenger.PhoneNum            
  ,Passenger.IsEmployeeType            
  ,Passenger.StandardBilling            
  ,Passenger.AuthorizationID            
  ,Passenger.AuthorizationDescription            
  ,Passenger.Notes            
  ,Passenger.LastName            
  ,Passenger.FirstName            
  ,Passenger.MiddleInitial            
  ,Passenger.DateOfBirth            
  ,Passenger.ClientID            
  ,Passenger.AssociatedWithCD            
  ,Passenger.HomebaseID            
  ,Passenger.IsActive            
  ,Passenger.FlightPurposeID            
  ,Passenger.SSN            
  ,Passenger.Title            
  ,Passenger.SalaryLevel            
  ,Passenger.IsPassengerType            
  ,Passenger.LastUpdUID            
  ,Passenger.LastUpdTS            
  ,Passenger.IsScheduledServiceCoord            
  ,Passenger.CompanyName            
  ,Passenger.EmployeeID            
  ,Passenger.FaxNum            
  ,Passenger.EmailAddress            
  ,Passenger.PersonalIDNum            
  ,Passenger.IsSpouseDependant            
  ,Passenger.AccountID            
  ,Passenger.CountryID            
  ,Passenger.IsSIFL            
  ,Passenger.PassengerAlert            
  ,Passenger.Gender            
  ,Passenger.AdditionalPhoneNum            
  ,Passenger.IsRequestor            
  ,Passenger.CountryOfResidenceID            
  ,Passenger.PaxScanDoc            
  ,Passenger.TSAStatus            
  ,Passenger.TSADTTM            
  ,Passenger.Addr1            
  ,Passenger.Addr2            
  ,Passenger.City            
  ,Passenger.StateName            
  ,Passenger.PostalZipCD            
  ,Passenger.IsDeleted              
  ,Nationality.CountryName as NationalityName            
  ,Nationality.CountryCD as NationalityCD            
  ,Client.ClientCD            
  ,Client.ClientDescription            
  ,Account.AccountNum            
  ,Account.AccountDescription            
  ,Department.DepartmentCD            
  ,Department.DepartmentName            
  ,DepartmentAuthorization.AuthorizationCD            
  ,DepartmentAuthorization.DeptAuthDescription            
  ,FlightPurpose.FlightPurposeCD            
  ,FlightPurpose.FlightPurposeDescription            
  ,CountryOfResidence.CountryName as ResidenceCountryName            
  ,CountryOfResidence.CountryCD as ResidenceCountryCD            
  ,HomeBase.CityName as HomeBaseName            
  ,HomeBase.IcaoID as HomeBaseCD      
  ,Passenger.PrimaryMobile      
  ,Passenger.BusinessFax      
  ,Passenger.CellPhoneNum2      
  ,Passenger.OtherPhone      
  ,Passenger.PersonalEmail      
  ,Passenger.OtherEmail      
  ,Passenger.Addr3          
  ,Passenger.Birthday      
  ,Passenger.Anniversaries      
  ,Passenger.EmergencyContacts      
  ,Passenger.CreditcardInfo      
  ,Passenger.CateringPreferences      
  ,Passenger.HotelPreferences        
  ,Passenger.[INSANumber]      
  ,Passenger.[Category]      
  ,Passenger.[GreenCardCountryID]      
  ,Passenger.[CardExpires]      
  ,Passenger.[ResidentSinceYear]      
  ,Passenger.[GreenCardGender]    
  ,Passenger.cqcustomerid    
  ,CQ.CQCustomerCD     
  ,PaxTitle    
  ,CityOfBirth    
  ,Nickname     
   ,Passport.Choice     
    , Passport.PassportID      
       ,Department.DepartPercentage  
    , isnull(Passport.IssueCity,'') as 'IssueCity'       
           , isnull(Passport.PassportNum,'') as 'PassportNum'      
    , isnull(Passport.PassportExpiryDT,'') as 'PassportExpiryDT'      
           , isnull(Passport.IsDefaultPassport,0) as 'IsDefaultPassport'      
           , Passport.PilotLicenseNum      
           , isnull(Passport.IssueDT,'') as 'IssueDT'      
           , Passport.CountryID as 'PassportCountryID'      
           , PassportCountry.CountryCD as 'Nation'     
   , Passport.CrewID 
 FROM Passenger as Passenger            
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Passenger.CountryID     
  LEFT OUTER JOIN cqcustomer as CQ on CQ.cqcustomerid = Passenger.cqcustomerid            
  LEFT OUTER JOIN Client as Client on Client.ClientID = Passenger.ClientID       
  LEFT OUTER JOIN Account as Account on Account.AccountID = Passenger.AccountID            
  LEFT OUTER JOIN Department as Department on Department.DepartmentID = Passenger.DepartmentID            
  LEFT OUTER JOIN DepartmentAuthorization as DepartmentAuthorization on DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID            
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Passenger.HomebaseID            
  LEFT OUTER JOIN FlightPurpose as FlightPurpose on FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID            
  LEFT OUTER JOIN Country as CountryOfResidence on CountryOfResidence.CountryID = Passenger.CountryOfResidenceID      
  LEFT OUTER JOIN CrewPassengerPassport as Passport on Passport.PassengerRequestorID = Passenger.PassengerRequestorID  --- post flight       
   AND isnull(Passport.IsDeleted,0) = 0 AND isnull(Passport.Choice,0) = 1  --- post flight       
  LEFT OUTER JOIN Country as PassportCountry on Passport.CountryID = PassportCountry.CountryID        --- post flight        
 WHERE   
 Passenger.CustomerID = @CustomerID  
 AND Passenger.isdeleted=0   
 AND ISNULL(Passenger.ClientID,0) = case when @ClientID >0 then @ClientID else ISNULL(Passenger.ClientID,0) end  
 AND ISNULL(Passenger.CQCustomerID,0) = case when @CQCustomerID >0 then @CQCustomerID else ISNULL(Passenger.CQCustomerID,0) end  
 AND Passenger.PassengerRequestorID = case when @PassengerID <>0 then @PassengerID else Passenger.PassengerRequestorID end  
 AND Passenger.PassengerRequestorCD = case when Ltrim(Rtrim(@PassengerCD)) <> '' then @PassengerCD else Passenger.PassengerRequestorCD end        
 AND ISNULL(Passenger.IsActive,1) = case when @ActiveOnly =1 then 1 else ISNULL(Passenger.IsActive,1) end  
 AND ISNULL(Passenger.IsRequestor,1) = case when @RequestorOnly =1 then 1 else ISNULL(Passenger.IsRequestor,1) end  
 AND ISNULL(HomeBase.IcaoID,'') = case when ltrim(rtrim(@ICAOID)) <>'' then @ICAOID else ISNULL(HomeBase.IcaoID,'') end  
 AND ISNULL(Passenger.PassengerRequestorID,0) IN(Select PassengerRequestorID from PassengerGroupOrder where PassengerGroupID = @PaxGroupID)  
 ORDER BY Passenger.PassengerName       
END         
ELSE        
BEGIN        
SELECT   Passenger.PassengerRequestorID            
  ,CASE WHEN Passenger.PassengerRequestorCD IS NULL THEN '' ELSE LTRIM(RTRIM(Passenger.PassengerRequestorCD)) END AS PassengerRequestorCD           
  ,Passenger.PassengerName            
  ,Passenger.CustomerID            
  ,Passenger.DepartmentID            
  ,Passenger.PassengerDescription            
  ,Passenger.PhoneNum            
  ,Passenger.IsEmployeeType            
  ,Passenger.StandardBilling            
  ,Passenger.AuthorizationID            
  ,Passenger.AuthorizationDescription            
  ,Passenger.Notes            
  ,Passenger.LastName            
  ,Passenger.FirstName            
  ,Passenger.MiddleInitial            
  ,Passenger.DateOfBirth            
  ,Passenger.ClientID            
  ,Passenger.AssociatedWithCD            
  ,Passenger.HomebaseID            
  ,Passenger.IsActive            
  ,Passenger.FlightPurposeID            
  ,Passenger.SSN            
  ,Passenger.Title            
  ,Passenger.SalaryLevel            
  ,Passenger.IsPassengerType            
  ,Passenger.LastUpdUID            
  ,Passenger.LastUpdTS            
  ,Passenger.IsScheduledServiceCoord            
  ,Passenger.CompanyName            
  ,Passenger.EmployeeID            
  ,Passenger.FaxNum            
  ,Passenger.EmailAddress            
  ,Passenger.PersonalIDNum            
  ,Passenger.IsSpouseDependant            
  ,Passenger.AccountID            
  ,Passenger.CountryID            
  ,Passenger.IsSIFL            
  ,Passenger.PassengerAlert            
  ,Passenger.Gender            
  ,Passenger.AdditionalPhoneNum            
  ,Passenger.IsRequestor            
  ,Passenger.CountryOfResidenceID            
  ,Passenger.PaxScanDoc            
  ,Passenger.TSAStatus            
  ,Passenger.TSADTTM            
  ,Passenger.Addr1            
  ,Passenger.Addr2            
  ,Passenger.City            
  ,Passenger.StateName            
  ,Passenger.PostalZipCD            
  ,Passenger.IsDeleted              
  ,Nationality.CountryName as NationalityName            
  ,Nationality.CountryCD as NationalityCD            
  ,Client.ClientCD            
  ,Client.ClientDescription            
  ,Account.AccountNum            
  ,Account.AccountDescription            
  ,Department.DepartmentCD            
  ,Department.DepartmentName            
  ,DepartmentAuthorization.AuthorizationCD            
  ,DepartmentAuthorization.DeptAuthDescription            
  ,FlightPurpose.FlightPurposeCD            
  ,FlightPurpose.FlightPurposeDescription            
  ,CountryOfResidence.CountryName as ResidenceCountryName            
  ,CountryOfResidence.CountryCD as ResidenceCountryCD            
  ,HomeBase.CityName as HomeBaseName            
  ,HomeBase.IcaoID as HomeBaseCD      
  ,Passenger.PrimaryMobile      
  ,Passenger.BusinessFax      
  ,Passenger.CellPhoneNum2      
  ,Passenger.OtherPhone      
  ,Passenger.PersonalEmail      
  ,Passenger.OtherEmail      
  ,Passenger.Addr3        
  ,Passenger.Birthday      
  ,Passenger.Anniversaries      
  ,Passenger.EmergencyContacts      
  ,Passenger.CreditcardInfo      
  ,Passenger.CateringPreferences      
  ,Passenger.HotelPreferences        
  ,Passenger.[INSANumber]      
  ,Passenger.[Category]      
  ,Passenger.[GreenCardCountryID]      
  ,Passenger.[CardExpires]      
  ,Passenger.[ResidentSinceYear]      
  ,Passenger.[GreenCardGender]    
  ,Passenger.cqcustomerid    
  ,CQ.CQCustomerCD      
  ,PaxTitle    
  ,CityOfBirth    
  ,Nickname      
   ,Passport.Choice     
    , Passport.PassportID      
       ,Department.DepartPercentage  
    , isnull(Passport.IssueCity,'') as 'IssueCity'       
           , isnull(Passport.PassportNum,'') as 'PassportNum'      
    , isnull(Passport.PassportExpiryDT,'') as 'PassportExpiryDT'      
           , isnull(Passport.IsDefaultPassport,0) as 'IsDefaultPassport'      
           , Passport.PilotLicenseNum      
           , isnull(Passport.IssueDT,'') as 'IssueDT'      
           , Passport.CountryID as 'PassportCountryID'      
           , PassportCountry.CountryCD as 'Nation'     
   , Passport.CrewID    
  FROM Passenger as Passenger            
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Passenger.CountryID      
  LEFT OUTER JOIN cqcustomer as CQ on CQ.cqcustomerid = Passenger.cqcustomerid          
  LEFT OUTER JOIN Client as Client on Client.ClientID = Passenger.ClientID            
  LEFT OUTER JOIN Account as Account on Account.AccountID = Passenger.AccountID            
  LEFT OUTER JOIN Department as Department on Department.DepartmentID = Passenger.DepartmentID            
  LEFT OUTER JOIN DepartmentAuthorization as DepartmentAuthorization on DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID            
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Passenger.HomebaseID            
  LEFT OUTER JOIN FlightPurpose as FlightPurpose on FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID            
  LEFT OUTER JOIN Country as CountryOfResidence on CountryOfResidence.CountryID = Passenger.CountryOfResidenceID   
  LEFT OUTER JOIN CrewPassengerPassport as Passport on Passport.PassengerRequestorID = Passenger.PassengerRequestorID  --- post flight       
   AND isnull(Passport.IsDeleted,0) = 0 AND isnull(Passport.Choice,0) = 1  --- post flight       
  LEFT OUTER JOIN Country as PassportCountry on Passport.CountryID = PassportCountry.CountryID        --- post flight           
 WHERE  
 Passenger.CustomerID = @CustomerID  
 AND Passenger.isdeleted=0   
 AND ISNULL(Passenger.ClientID,0) = case when @ClientID >0 then @ClientID else ISNULL(Passenger.ClientID,0) end  
 AND ISNULL(Passenger.CQCustomerID,0) = case when @CQCustomerID >0 then @CQCustomerID else ISNULL(Passenger.CQCustomerID,0) end  
 AND Passenger.PassengerRequestorID = case when @PassengerID <>0 then @PassengerID else Passenger.PassengerRequestorID end  
 AND Passenger.PassengerRequestorCD = case when Ltrim(Rtrim(@PassengerCD)) <> '' then @PassengerCD else Passenger.PassengerRequestorCD end        
 AND ISNULL(Passenger.IsActive,1) = case when @ActiveOnly =1 then 1 else ISNULL(Passenger.IsActive,1) end  
 AND ISNULL(Passenger.IsRequestor,1) = case when @RequestorOnly =1 then 1 else ISNULL(Passenger.IsRequestor,1) end  
 AND ISNULL(HomeBase.IcaoID,'') = case when ltrim(rtrim(@ICAOID)) <>'' then @ICAOID else ISNULL(HomeBase.IcaoID,'') end  
 ORDER BY Passenger.PassengerName       
END        
END 
GO

