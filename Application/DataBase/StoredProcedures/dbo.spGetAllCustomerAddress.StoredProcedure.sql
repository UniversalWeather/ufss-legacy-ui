
GO
/****** Object:  StoredProcedure [dbo].[spGetAllCustomerAddress]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCustomerAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCustomerAddress]
GO

 -- DB Catalog - Customer Address    
    
    
CREATE PROCEDURE [dbo].[spGetAllCustomerAddress]  
 (@CustomerID BIGINT,  
 @UserName char(30))  
AS    BEGIN    
-- =============================================        
-- Author:Karthikeyan.S        
-- Create date: 22/08/2012        
-- Description: Get the Customer information        
-- =============================================        
 SET NOCOUNT ON        
    SELECT CustomAddress.[CustomAddressID],        
   CustomAddress.[CustomerID],        
   CustomAddress.UserName,         
   CustomAddress.[Name],         
   CustomAddress.[Address1],         
   CustomAddress.[Address2],         
   CustomAddress.[Address3],         
   CustomAddress.[CustomCity],         
   CustomAddress.[CustomState],         
   CustomAddress.[PostalZipCD],         
   CustomAddress.[CountryID],         
   CustomAddress.[MetropolitanArea],         
   CustomAddress.[Contact],         
   CustomAddress.[PhoneNum],         
   CustomAddress.[FaxNum],         
   CustomAddress.[ContactEmailID],         
   CustomAddress.[Website],         
   CustomAddress.[Symbol],         
   CustomAddress.[LatitudeDegree],         
   CustomAddress.[LatitudeMinutes],         
   CustomAddress.[LatitudeNorthSouth],         
   CustomAddress.[LongitudeDegrees],         
   CustomAddress.[LongitudeMinutes],         
   CustomAddress.[LongitudeEastWest],         
   CustomAddress.[AirportID],         
   CustomAddress.[HomeBaseID],         
   CustomAddress.[Remarks],         
   CustomAddress.[LastUpdUID],         
   CustomAddress.[LastUpdTS],         
   CustomAddress.[IsDeleted],        
   CustomAddress.CustomAddressCD,     
   CustomAddress.BusinessPhone  
   ,CustomAddress.CellPhoneNum  
   ,CustomAddress.HomeFax  
   ,CustomAddress.CellPhoneNum2  
   ,CustomAddress.OtherPhone  
   ,CustomAddress.BusinessEmail  
   ,CustomAddress.PersonalEmail  
   ,CustomAddress.OtherEmail  
   ,CustomAddress.IsInActive, 
   Nationality.CountryCD as NationalityCD,        
   --HomeBase.HomeBaseCD as HomeBaseCD,        
   Airport.IcaoID as ClosestICAO        
   ,Airport1.ICAOID AS HomeBaseCD            
 FROM CustomAddress as CustomAddress  
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = CustomAddress.CountryID  
  LEFT OUTER JOIN Company as HomeBase on HomeBase.HomebaseID = CustomAddress.HomebaseID  
  LEFT OUTER JOIN Airport as Airport on Airport.AirportID = CustomAddress.AirportID  
  LEFT OUTER JOIN Airport as Airport1 on Airport1.AirportID = HomeBase.HomebaseAirportID  
 WHERE CustomAddress.CustomerID=@CustomerID and   
   --CustomAddress.UserName=@UserName and        
   CustomAddress.IsDeleted = 'false'  
   order by CustomAddressCD      
END  