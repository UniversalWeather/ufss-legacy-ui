
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFleetGroup]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFleetGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFleetGroup]
GO
CREATE procedure [dbo].[spUpdateFleetGroup]
  (@FleetGroupID BIGINT,
   @CustomerID BIGINT,
   @FleetGroupCD char(4),
   @FleetGroupDescription  varchar(30),
   @LastUpdUID varchar(30),
   @LastUpdTS datetime,
   @HomebaseID BIGINT,
   @IsDeleted bit,
   @IsInActive bit)   
as  
  
begin   
  set @LastUpdTS=GETUTCDATE()
SET NoCOUNT ON  
if @IsDeleted=0  
BEGIN     
     Update  [FleetGroup] 
     SET [CustomerID]=@CustomerID  
        ,[FleetGroupCD] =@FleetGroupCD  
        ,[FleetGroupDescription] =@FleetGroupDescription  
        ,[LastUpdUID] =@LastUpdUID  
        ,[LastUpdTS]=@LastUpdTS  
        ,[HomebaseID]=@HomebaseID  
        ,[IsInActive]=@IsInActive
  WHERE  FleetGroupID=@FleetGroupID
  
end  
  
else  
  begin  
  
  Update  [FleetGroup]  
  
   SET  
      [IsDeleted] =1  
     ,[CustomerID]=@CustomerID   
     ,[FleetGroupCD]=@FleetGroupCD  
     ,[FleetGroupDescription] =@FleetGroupDescription  
     ,[LastUpdUID] =@LastUpdUID  
     ,[LastUpdTS]=@LastUpdTS  
     ,[HomebaseID]=@HomebaseID  
     ,[IsInActive]=@IsInActive
   WHERE FleetGroupID=@FleetGroupID
  
   end  
end
GO
