
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllPreflightMainDetails]    Script Date: 02/05/2014 11:32:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllPreflightMainDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllPreflightMainDetails]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllPreflightMainDetails]    Script Date: 02/05/2014 11:32:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









--exec spFlightPak_Preflight_GetAllPreflightMainDetails 10003,0,0,'','','','','','',0,'jan 12, 2012',0,''

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAllPreflightMainDetails]   
 (  
  @CustomerID Bigint,  
  @HomebaseID Bigint,  
  @ClientID Bigint,  
  @TripSheet varchar(1),  
  @Worksheetstr varchar(1),  
  @Hold varchar(1),  
  @Cancelled varchar(1),  
  @SchedServ varchar(1),  
  @UnFillFilled varchar(1),  
  @isLog  bit,  
  @EstDepartureDT date = null,
  @isPrivateUser  bit,
  @HomebaseIDstr varchar(1000)
 )  
 AS    
 SET NOCOUNT ON;   
 BEGIN  
   
   
  --set @CustomerID =10000  
  --set @HomebaseID = null  
  --set @ClientID = null  
  --set @TripSheet = 't'  
  --set @Worksheetstr = 'w'  
  --set @Hold = 'h'  
  --set @Cancelled = null  
  --set @SchedServ = null  
  --set @UnFillFilled = null  
  --set @isLog  = 1  
  --set @EstDepartureDT = DATEADD(DAY,-300,GETDATE())  
    
  
  Declare @Sql varchar(max)  
  Declare @StatusSql varchar(2000)  
  Declare @StatusEntered bit  
  
  set @Sql =  'SELECT   
      PM.TripID,  
      PM.TripNUM,  
      PM.DispatchNUM,
      PM.EstDepartureDT,  
      P.PassengerRequestorCD as ''RequestorFirstName'',  
      PM.TripDescription,  
      PM.TripStatus,  
      PM.RequestDT,  
      F.TailNum,  
      POM.LogNum,  
      PM.Acknowledge,  
      PM.WaitList,  
      A.ICAOID AS HomebaseCD,
      Case When PM.Notes is not null and PM.Notes <>'''' then ''!'' else '''' end as Alert,
      PM.APISSubmit,
      PM.APISStatus,      
      Case When PM.APISException is not null and PM.APISException <>'''' then ''!'' else '''' end as APISException,         
	  CASE WHEN PM.IsCorpReq is not null and PM.IsCorpReq = 1 then ''R'' ELSE 
		CASE WHEN PM.IsQuote is not null and PM.IsQuote = 1 then ''C'' else '''' END
	  END as CR,	  
	  UM.TripReqLastSubmitDT,
	  CASE WHEN ISNULL(PM.isAirportAlert, 0) = 1 then  ''!'' else '''' end as  DutyType,
	  CASE WHEN
	 (
       SELECT count(1)
       FROM PreflightTripException PREFEX 
       WHERE PM.TripID= PREFEX.TripID and  PREFEX.CustomerID=' +   CAST( @CustomerID  as varchar(20)) + ' 
	    ) >0  THEN ''!'' 
	    ELSE ''''
		END as TripExcep,
	  isnull(F.ForeGrndCustomColor,''''),
	  isnull(F.BackgroundCustomColor,'''')
   
      FROM PreflightMain PM  
      INNER JOIN Company C on PM.HomebaseID = C.HomebaseID and  C.CustomerID=' +   CAST( @CustomerID  as varchar(20)) + '
      INNER JOIN AIRPORT A on A.AirportID = C.HomebaseAirportID   and A.isDeleted =0
	
	
      LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID  AND Isnull(POM.IsDeleted,0) = 0 and  POM.CustomerID=' +   CAST( @CustomerID  as varchar(20)) + '
      LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  and  F.CustomerID=' +   CAST( @CustomerID  as varchar(20)) + ' and F.isDeleted =0
      LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID and  P.CustomerID=' +   CAST( @CustomerID  as varchar(20)) + ' and P.isDeleted =0
      LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID
      WHERE   
      PM.TripID in (select Pl.TripID from PreflightLeg Pl where Pl.TripID = PM.TripID and Pl.CustomerID =' +   CAST( @CustomerID  as varchar(20)) + ' )
	  and
      PM.RecordType= ''T'' and PM.CustomerID=' +   CAST( @CustomerID  as varchar(20))  
         
      
   
	    
      set @Sql =  @Sql + ' and PM.IsDeleted =0 '   
        
      set @StatusSql = ''  
        
      if (@TripSheet <> '')  
      begin  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @TripSheet + ''' ) '  
      end  
        
      if (@Worksheetstr <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (PM.TripStatus = ''' + @Worksheetstr + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @Worksheetstr + ''' )'  
      end  
        
      if (@Hold <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (PM.TripStatus = ''' + @Hold + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @Hold + ''' )'  
      end  
        
      if (@Cancelled  <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (PM.TripStatus = ''' + @Cancelled  + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @Cancelled  + ''' )'  
      end  
      
    if (@UnFillFilled <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (PM.TripStatus = ''' + @UnFillFilled + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @UnFillFilled  + ''' )'  
      end  
      
    if (@SchedServ <> '')  
      begin  
     if (@StatusSql <>'')  
      set @StatusSql = @StatusSql + ' or (PM.TripStatus = ''' + @SchedServ + ''' )'  
     else  
      set @StatusSql = @StatusSql + '(PM.TripStatus = ''' + @SchedServ  + ''' )'  
      end 
    
       
    if (@StatusSql<>'')     
     set @Sql =  @Sql + ' and ( ' + @StatusSql + ' ) '  
       
    if (@isLog <> 0)  
     set @Sql =  @Sql + ' and (PM.isLog is null or  PM.isLog = 0)'  
    
    if (@EstDepartureDT is not null)  
     set @Sql =  @Sql + ' and PM.EstDepartureDT > = ''' + cast (@EstDepartureDT as varchar(25)) + ''''  
    
    if (@isPrivateUser <>0)
		set @Sql =  @Sql + ' and (PM.isPrivate = 0) '  
    
    
     
    
    
    If (@HomebaseIDstr<> '')  
     set @Sql =  @Sql + ' and PM.HomebaseID  in ('  + @HomebaseIDstr     + ')'
    else
		begin
			If (@HomebaseID<> 0)  
				set @Sql =  @Sql + ' and PM.HomebaseID  = ' + cast (@HomebaseID as varchar(25))    
		end
    If (@ClientID<> 0)  
     set @Sql =  @Sql + ' and PM.ClientID  = ' + cast (@ClientID as varchar(25))         
     
    set @Sql =  @Sql + ' Order By PM.EstDepartureDT'          
    
   print @Sql  
    
      create table #tempPreflightmain  
      (  
      TripID bigint,  
      TripNUM bigint,
      DispatchNUM varchar(12),
      EstDepartureDT date,  
      RequestorFirstName varchar(100),  
      TripDescription varchar(40),  
      TripStatus char(1),  
      RequestDT date,  
      TailNum char(10),  
      LogNum bigint,  
      Acknowledge char(1),  
      WaitList char(1),  
      HomebaseCD  char(4),  
      Alert char(1),
      APISSubmit datetime,
      APISStatus varchar(20),
      APISException varchar(MAX),         
	  CR char(1),
	  TripReqLastSubmitDT datetime,
	  DutyType char(1),
	  TripExcep char(1),
	  ForeGrndCustomColor varchar(8),
	  BackgroundCustomColor	 varchar(8)
      )  
         
        
      insert into #tempPreflightmain Exec ( @Sql)  
        
SELECT
      
		TripID ,  
		TripNUM , 
		DispatchNUM, 
		EstDepartureDT ,  
		RequestorFirstName,  
		TripDescription,  
		TripStatus,  
		RequestDT,  
		TailNum,  
		LogNum ,  
		Acknowledge,  
		WaitList,  
		HomebaseCD,
		Alert,
        APISSubmit,
        APISStatus,
        APISException,         
	    CR,
	    TripReqLastSubmitDT,
	    DutyType,
	    TripExcep,
	    ForeGrndCustomColor,
		BackgroundCustomColor	    
	    
	FROM #tempPreflightmain 

      union  
      SELECT   
      PM.TripID,  
      PM.TripNUM,  
      PM.DispatchNUM, 
      PM.EstDepartureDT,  
      '' as 'RequestorFirstName',  
      PM.TripDescription,  
      PM.TripStatus,  
      PM.RequestDT,  
      '' as TailNum,  
      '' as LogNum,  
      PM.Acknowledge,  
      PM.WaitList,  
      '' as HomebaseCD ,
	  PM.Notes as Alert,
      PM.APISSubmit,
      PM.APISStatus,
      PM.APISException,         
	  '' as CR,
	  null as TripReqLastSubmitDT,
	  '' as DutyType,
	  '' as TripExcep,
	  '' as  ForeGrndCustomColor,
	  '' as BackgroundCustomColor	  
	  
	  FROM PreflightMain PM  
      --INNER JOIN Company C on PM.HomebaseID = C.HomebaseID  
      --INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      --LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID AND IsNull(POM.IsDeleted,0) = 0
      --LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  
      --LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID 
      --LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID 
      
      WHERE 1=2  and CustomerID = @CustomerID
      
        
      DROP TABLE #tempPreflightmain  
 --Exec[spFlightPak_Preflight_GetAllPreflightMainDetails] 10001,null,null,null,null,null,null,null,null,null,null       
 
 END




GO


