
/****** Object:  StoredProcedure [dbo].[spDeleteFlightCategory]    Script Date: 7/15/2015 3:40:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[spDeleteFlightCategory]
(
	@CustomerID bigint,
	@FlightCategoryID bigint,
	@FlightCatagoryCD char(4),		
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit
)
-- =============================================
-- Author: Sujitha.V
-- Create date: 16/4/2012
-- Description: Delete the Flight Category information
-- =============================================
as
begin 
--Check if the record is not being used anywhere in the application before Delete  
 DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)  
DECLARE @RecordUsedInCompany int SET @RecordUsedInCompany = 0      
SET @ENTITYNAME = N'FlightCatagory'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @FlightCategoryID, @RecordUsed OUTPUT

SELECT @RecordUsedInCompany = COUNT(*) FROM [dbo].[Company] WHERE FlightCatagoryPassenger = @FlightCatagoryCD OR FlightCatagoryPassengerNum = @FlightCatagoryCD
--End of Delete Check
  
  if (@RecordUsed <> 0  OR @RecordUsedInCompany <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 ELSE  
  BEGIN  
   --To store the lastupdateddate as UTC date 
  
   SET @LastUpdTS = GETUTCDATE()  
   
   --End of UTC date
SET NoCOUNT ON

	UPDATE FlightCatagory
	SET 
      [LastUpdUID] = @LastUpdUID,
      [LastUpdTS] = @LastUpdTS,
      [IsDeleted] = @IsDeleted
	WHERE [CustomerID] = @CustomerID and [FlightCategoryID]=@FlightCategoryID 
END
end
GO

