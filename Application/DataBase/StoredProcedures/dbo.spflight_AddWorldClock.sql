

/****** Object:  StoredProcedure [dbo].[spflight_AddWorldClock]    Script Date: 02/13/2014 15:04:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spflight_AddWorldClock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spflight_AddWorldClock]
GO



/****** Object:  StoredProcedure [dbo].[spflight_AddWorldClock]    Script Date: 02/13/2014 15:04:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spflight_AddWorldClock]    
(@AirportID bigint    
,@CustomerID BIGINT    
,@IsWorldClock BIT)    
AS BEGIN    
     
 DECLARE @TAirportID BIGINT    
 DECLARE @NAirportID BIGINT    
 DECLARE @UWAID VARCHAR(20)    
 DECLARE @ICAOCD VARCHAR(50)    
     
      
   SELECT @UWAID = UWAID, @ICAOCD = IcaoID FROM Airport WHERE AirportID = @AirportID    
       
   IF EXISTS (SELECT IcaoID FROM Airport WHERE IcaoID = @ICAOCD AND CustomerID = @CustomerID AND ISNULL(UWAID,'') = '')    
   BEGIN    
    SELECT @NAirportID=AirportID FROM Airport WHERE IcaoID = @ICAOCD AND CustomerID = @CustomerID AND ISNULL(UWAID,'') = ''    
    SET @UWAID = 'X'    
    SET @TAirportID = @NAirportID    
   END    
       
   IF (@UWAID = '') OR (@UWAID IS NULL)    
    BEGIN    
     EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @NAirportID OUTPUT     
     INSERT INTO Airport    
     (AirportID,IcaoID,CustomerID,CityName,StateName,CountryName,CountryID,AirportName,LatitudeDegree,LatitudeMinutes,    
     LatitudeNorthSouth,LongitudeDegrees,LongitudeMinutes,LongitudeEastWest,MagneticVariance,VarianceEastWest,    
     Elevation,LongestRunway,IsEntryPort,WindZone,IsUSTax,IsRural,IsAirport,IsHeliport,AirportInfo,OffsetToGMT,    
     IsDayLightSaving,DayLightSavingStartDT,DaylLightSavingStartTM,DayLightSavingEndDT,DayLightSavingEndTM,TowerFreq,    
     ATISFreq,ARINCFreq,GroundFreq,ApproachFreq,DepartFreq,Label1Freq,Freq1,Label2Freq,Freq2,TakeoffBIAS,LandingBIAS,    
     Alerts,GeneralNotes,IsFlightPakFlag,FBOCnt,HotelCnt,TransportationCnt,CateringCnt,FlightPakUPD,MetroID,    
     DSTRegionID,PPR,Iata,AlternateDestination1,AlternateDestination2,AlternateDestination3,LastUpdUID,LastUpdTS,    
     IsInActive,RecordType,NewICAO,PreviousICAO,UpdateDT,IsWorldClock,WidthRunway,FssPhone,LengthWidthRunway,    
     WidthLengthRunway,CustomLocation,CustomPhoneNum,ImmigrationPhoneNum,AgPhoneNum,IsCustomAvailable,PortEntryType,    
     Unicom,Clearence1DEL,Clearence2DEL,AtisPhoneNum,ASOS,AsosPhoneNum,AWOS,AwosPhoneNum,AwosType,IsSlots,FssName,    
     AirportManager,AirportManagerPhoneNum,FAA,UWAID,IsFIX,HoursOfOperation,LandingFeeSmall,LandingFeeMedium,    
     LandingFeeLarge,IsEUETS,IsDeleted,UWAUpdates,RunwayID1,RunwayID2,Runway2Length,Runway2Width,RunwayID3,    
     Runway3Length,Runway3Width,RunwayID4,Runway4Length,Runway4Width)      
     SELECT     
     @NAirportID,IcaoID,@CustomerID,CityName,StateName,CountryName,CountryID,AirportName,LatitudeDegree,LatitudeMinutes,    
     LatitudeNorthSouth,LongitudeDegrees,LongitudeMinutes,LongitudeEastWest,MagneticVariance,VarianceEastWest,    
     Elevation,LongestRunway,IsEntryPort,WindZone,IsUSTax,IsRural,IsAirport,IsHeliport,AirportInfo,OffsetToGMT,    
     IsDayLightSaving,DayLightSavingStartDT,DaylLightSavingStartTM,DayLightSavingEndDT,DayLightSavingEndTM,TowerFreq,    
     ATISFreq,ARINCFreq,GroundFreq,ApproachFreq,DepartFreq,Label1Freq,Freq1,Label2Freq,Freq2,TakeoffBIAS,LandingBIAS,    
     Alerts,GeneralNotes,IsFlightPakFlag,FBOCnt,HotelCnt,TransportationCnt,CateringCnt,FlightPakUPD,MetroID,    
     DSTRegionID,PPR,Iata,AlternateDestination1,AlternateDestination2,AlternateDestination3,LastUpdUID,LastUpdTS,    
     IsInActive,RecordType,NewICAO,PreviousICAO,UpdateDT,1,WidthRunway,FssPhone,LengthWidthRunway,    
     WidthLengthRunway,CustomLocation,CustomPhoneNum,ImmigrationPhoneNum,AgPhoneNum,IsCustomAvailable,PortEntryType,    
     Unicom,Clearence1DEL,Clearence2DEL,AtisPhoneNum,ASOS,AsosPhoneNum,AWOS,AwosPhoneNum,AwosType,IsSlots,FssName,    
     AirportManager,AirportManagerPhoneNum,FAA,'' AS UWAID,IsFIX,HoursOfOperation,LandingFeeSmall,LandingFeeMedium,    
     LandingFeeLarge,IsEUETS,IsDeleted,UWAUpdates,RunwayID1,RunwayID2,Runway2Length,Runway2Width,RunwayID3,    
     Runway3Length,Runway3Width,RunwayID4,Runway4Length,Runway4Width    
     FROM Airport    
     WHERE AirportID = @TAirportID    
    END    
   ELSE    
    BEGIN    
     UPDATE Airport SET IsWorldClock = 1 WHERE AirportID = @AirportID    
    END      
  END
GO


