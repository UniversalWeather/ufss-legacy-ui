

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerListByPreflightPassengerListID]    Script Date: 09/19/2013 15:21:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightPassengerListByPreflightPassengerListID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightPassengerListByPreflightPassengerListID]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerListByPreflightPassengerListID]    Script Date: 09/19/2013 15:21:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create Procedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerListByPreflightPassengerListID]
(
@CustomerID BIGINT,
@PreflightPassengerListID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get PreflightCrewList by PreflightCrewListID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightPassengerList PC 
 where 
 PC.PreflightPassengerListID = @PreflightPassengerListID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 



GO


