/****** Object:  StoredProcedure [dbo].[spUpdateTransport]    Script Date: 09/13/2012 12:38:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateTransport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateTransport]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTransport]    Script Date: 09/13/2012 12:38:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateTransport](    
 @TransportID BIGINT,  
 @AirportID BIGINT ,  
 @TransportCD CHAR(4),    
 @CustomerID BIGINT ,  
 @IsChoice BIT ,     
 @TransportationVendor VARCHAR(40),     
 @PhoneNum VARCHAR(25) ,    
 @FaxNum VARCHAR(25) ,  
 @NegotiatedRate NUMERIC(6, 2) ,     
 @ContactName VARCHAR(25) ,    
 @Remarks varchar(max) ,    
 @LastUpdUID VARCHAR(30) ,    
 @LastUpdTS DATETIME ,    
 @UpdateDT DATE ,    
 @RecordType CHAR(1) ,    
 @SourceID VARCHAR(12) ,    
 @ControlNum CHAR(5) ,    
 @TollFreePhoneNum VARCHAR(25) ,    
 @WebSite VARCHAR(200) ,    
 @UWAMaintFlag BIT ,    
 @UWAID VARCHAR(20),   
 @IsInActive BIT ,    
 @IsDeleted BIT ,    
 @UWAUpdates BIT,  
 @AltBusinessPhone varchar(25),  
 @BusinessEmail varchar(250),  
 @ContactBusinessPhone varchar(25),  
 @CellPhoneNum varchar(25),  
 @ContactEmail varchar(250),  
 @Addr1 varchar(40),   
 @Addr2 varchar(40),  
 @Addr3 varchar(40),  
 @City varchar(25),  
 @StateProvince varchar(10),  
 @MetroID BigInt,  
 @CountryID BigInt,  
 @IsPassenger BIT,  
@IsCrew BIT,  
 @ExchangeRateID BIGINT,  
@NegotiatedTerms VARCHAR(800),  
@SundayWorkHours VARCHAR(15),  
@MondayWorkHours VARCHAR(15),  
@TuesdayWorkHours VARCHAR(15),  
@WednesdayWorkHours VARCHAR(15),  
@ThursdayWorkHours VARCHAR(15),  
@FridayWorkHours VARCHAR(15),  
@SaturdayWorkHours VARCHAR(15),
@PostalZipCD VARCHAR(15)  
 )    
    
-- =============================================    
-- Author:Prakash Pandian.S    
-- Create date: 24/4/2012    
-- Description: Update the Transport information   
-- Altered by Rajesh: 05-May-2015 (Added PostalZipCD) 
-- =============================================    
AS    
BEGIN     
IF @UWAMaintFlag='True' AND not exists (SELECT CustomerID FROM  Transport WHERE TransportID=@TransportID and AirportID = @AirportID and CustomerID=@CustomerID)  
    
BEGIN    
DECLARE @UWACustomerID BIGINT      
    
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output       
        
        
SET NOCOUNT ON          
 DECLARE @ICAOCD VARCHAR(50)  
 DECLARE @UWAAirportId VARCHAR(50)      
 select @ICAOCD =  icaoid from Airport where AirportID = @AirportID    
 select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD  
   
  SET @LastUpdTS = GETUTCDATE()   
exec spAddTransport   
           @TransportID,  
            @AirportID,  
            @TransportCD ,   
            @CustomerID    
           ,@IsChoice    
           ,@TransportationVendor    
           ,@PhoneNum    
           ,@FaxNum    
           ,@NegotiatedRate    
           ,@ContactName    
           ,@Remarks    
           ,@LastUpdUID    
           ,@LastUpdTS    
           ,@UpdateDT    
           ,@RecordType    
           ,@SourceID    
           ,@ControlNum    
           ,@TollFreePhoneNum    
           ,@WebSite    
           ,@UWAMaintFlag    
           ,@UWAID    
           ,@IsInActive   
           ,@IsDeleted    
           ,@UWAUpdates   
           ,@AltBusinessPhone  
           ,@BusinessEmail  
           ,@ContactBusinessPhone  
           ,@CellPhoneNum  
           ,@ContactEmail  
           ,@Addr1  
           ,@Addr2  
           ,@Addr3  
           ,@City  
           ,@StateProvince  
           ,@MetroID  
           ,@CountryID  
           ,@IsPassenger  
           ,@IsCrew  
           ,@ExchangeRateID    
     ,@NegotiatedTerms    
     ,@SundayWorkHours    
     ,@MondayWorkHours    
     ,@TuesdayWorkHours    
     ,@WednesdayWorkHours    
     ,@ThursdayWorkHours    
     ,@FridayWorkHours    
     ,@SaturdayWorkHours 
     ,@PostalZipCD        
               
END    
ELSE    
BEGIN               
SET NoCOUNT ON    
IF(@IsChoice='true')  
   begin  
   update Transport set IsChoice='false' where CustomerID=@CustomerID and (AirportID = @AirportID or AirportID = @UWAAirportId)   
   end   
UPDATE [Transport]    
   SET   [AirportID]=@AirportID      
  ,[CustomerID]=@CustomerID  
  ,[IsChoice]=@IsChoice  
  ,[TransportationVendor]=@TransportationVendor  
  ,[PhoneNum]=@PhoneNum  
  ,[FaxNum]=@FaxNum  
  ,[NegotiatedRate]=@NegotiatedRate  
  ,[ContactName]=@ContactName  
  ,[Remarks]=@Remarks  
  ,[LastUpdUID]=@LastUpdUID  
  ,[LastUpdTS]=@LastUpdTS  
  ,[UpdateDT]=@UpdateDT  
  ,[RecordType]=@RecordType  
  ,[SourceID]=@SourceID  
  ,[ControlNum]=@ControlNum  
  ,[TollFreePhoneNum]=@TollFreePhoneNum  
  ,[Website]=@WebSite  
  ,[UWAMaintFlag]=@UWAMaintFlag  
  ,[UWAID]=@UWAID  
  ,[IsInActive]=@IsInActive  
  ,[IsDeleted]=@IsDeleted  
  ,[UWAUpdates] =@UWAUpdates  
  ,[AltBusinessPhone] =@AltBusinessPhone  
        ,[BusinessEmail] =@BusinessEmail  
        ,[ContactBusinessPhone] =@ContactBusinessPhone  
        ,[CellPhoneNum] =@CellPhoneNum  
        ,[ContactEmail] =@ContactEmail  
        ,[Addr1] =@Addr1  
        ,[Addr2] =@Addr2  
        ,[Addr3] =@Addr3  
        ,[City] =@City  
        ,[StateProvince] =@StateProvince  
         ,[MetroID] =@MetroID  
          ,[CountryID] =@CountryID  
          ,IsPassenger = @IsPassenger  
           ,IsCrew = @IsCrew  
             ,ExchangeRateID  = @ExchangeRateID  
     ,NegotiatedTerms = @NegotiatedTerms  
     ,SundayWorkHours  = @SundayWorkHours  
     ,MondayWorkHours  = @MondayWorkHours    
     ,TuesdayWorkHours  = @TuesdayWorkHours    
     ,WednesdayWorkHours  = @WednesdayWorkHours  
     ,ThursdayWorkHours  = @ThursdayWorkHours    
     ,FridayWorkHours  = @FridayWorkHours    
     ,SaturdayWorkHours =@SaturdayWorkHours
     ,PostalZipCD =@PostalZipCD
 WHERE  TransportID=@TransportID  
    
END    
END 
GO


