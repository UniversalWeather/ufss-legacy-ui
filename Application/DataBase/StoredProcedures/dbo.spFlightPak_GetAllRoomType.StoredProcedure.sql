IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllRoomType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllRoomType]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllRoomType]    Script Date: 08/24/2012 10:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		Leela M.
---- Create date: 21-July-2012
---- Description:Get the Room Type

---- =============================================
CREATE PROCEDURE [dbo].[spFlightPak_GetAllRoomType]
AS
SET NOCOUNT ON;
BEGIN

SELECT [RoomTypeID]
      ,[RoomDescription]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive
  FROM [dbo].[RoomType]
where IsDeleted='false'
END
GO
