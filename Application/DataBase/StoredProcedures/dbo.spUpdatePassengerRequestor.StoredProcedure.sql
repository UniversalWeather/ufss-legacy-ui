

/****** Object:  StoredProcedure [dbo].[spUpdatePassengerRequestor]    Script Date: 03/18/2013 12:44:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePassengerRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePassengerRequestor]
GO

/****** Object:  StoredProcedure [dbo].[spUpdatePassengerRequestor]    Script Date: 03/18/2013 12:44:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spUpdatePassengerRequestor]    
(@PassengerRequestorID bigint          
,@PassengerRequestorCD char(5)          
,@PassengerName varchar(63)          
,@CustomerID bigint          
,@DepartmentID bigint          
,@PassengerDescription varchar(25)          
,@PhoneNum varchar(25)          
,@IsEmployeeType char(1)          
,@StandardBilling varchar(25)          
,@AuthorizationID bigint          
,@AuthorizationDescription varchar(25)          
,@Notes varchar(max)          
,@LastName varchar(20)          
,@FirstName varchar(20)          
,@MiddleInitial varchar(20)          
,@DateOfBirth date          
,@ClientID bigint          
,@AssociatedWithCD char(5)          
,@HomebaseID bigint          
,@IsActive bit          
,@FlightPurposeID bigint          
,@SSN varchar(9)          
,@Title varchar(30)          
,@SalaryLevel numeric(4,0)          
,@IsPassengerType numeric(1,0)          
,@LastUpdUID varchar(30)          
,@LastUpdTS datetime          
,@IsScheduledServiceCoord bit          
,@CompanyName varchar(60)          
,@EmployeeID varchar(25)          
,@FaxNum varchar(25)          
,@EmailAddress varchar(250)          
,@PersonalIDNum nvarchar(40)       
,@IsSpouseDependant bit          
,@AccountID bigint          
,@CountryID bigint          
,@IsSIFL bit          
,@PassengerAlert varchar(max)          
,@Gender char(1)          
,@AdditionalPhoneNum varchar(25)          
,@IsRequestor bit          
,@CountryOfResidenceID bigint          
,@PaxScanDoc varchar(max)          
,@TSAStatus char(1)          
,@TSADTTM datetime          
,@Addr1 varchar(40)          
,@Addr2 varchar(40)          
,@City varchar(25)          
,@StateName varchar(10)          
,@PostalZipCD varchar(15)          
,@IsDeleted bit    
,@PrimaryMobile varchar(25)    
,@BusinessFax varchar(25)    
,@CellPhoneNum2 varchar(25)    
,@OtherPhone varchar(25)    
,@PersonalEmail varchar(250)    
,@OtherEmail varchar(250)    
,@Addr3 varchar(40)    
,@PassengerWeight NUMERIC(5,2)    
,@PassengerWeightUnit CHAR(3)    
,@Birthday DATE    
,@Anniversaries DATE    
,@EmergencyContacts VARCHAR(100)    
,@CreditcardInfo VARCHAR(100)    
,@CateringPreferences VARCHAR(100)    
,@HotelPreferences VARCHAR(100)    
,@INSANumber VARCHAR(60)    
,@Category VARCHAR(30)    
,@CardExpires DATE    
,@ResidentSinceYear INT    
,@GreenCardGender CHAR(1)    
,@GreenCardCountryID BIGINT  
,@cqcustomerid BIGINT  
,@PaxTitle VARCHAR(MAX)  
,@CityOfBirth VARCHAR(25)  
,@Nickname VARCHAR(50))    
             
 AS              
BEGIN              
-- ====================================================              
-- Author: Srinivasan. J              
-- Create date: 05/06/2012              
-- Description: Add/ Update Passenger Requestor Information              
-- ====================================================              
SET NOCOUNT OFF              
  set @LastUpdTS = GETUTCDATE()        
UPDATE[Passenger]           
SET  --PassengerRequestorID = @PassengerRequestorID    ,      
  --PassengerRequestorCD =   @PassengerRequestorCD    ,      
  PassengerName =   @PassengerName          
  ,CustomerID =   @CustomerID          
  ,DepartmentID =   @DepartmentID          
  ,PassengerDescription =   @PassengerDescription          
  ,PhoneNum =   @PhoneNum          
  ,IsEmployeeType =   @IsEmployeeType          
  ,StandardBilling =   @StandardBilling          
  ,AuthorizationID =   @AuthorizationID          
  ,AuthorizationDescription =   @AuthorizationDescription          
  ,Notes =   @Notes          
  ,LastName =   @LastName          
  ,FirstName =   @FirstName          
  ,MiddleInitial =   @MiddleInitial          
  ,DateOfBirth =   @DateOfBirth          
  ,ClientID =   @ClientID          
  ,AssociatedWithCD =   @AssociatedWithCD          
  ,HomebaseID =   @HomebaseID          
  ,IsActive =   @IsActive          
  ,FlightPurposeID =   @FlightPurposeID          
  ,SSN =   @SSN          
  ,Title =   @Title          
  ,SalaryLevel =   @SalaryLevel          
  ,IsPassengerType =   @IsPassengerType          
  ,LastUpdUID =   @LastUpdUID          
  ,LastUpdTS =   @LastUpdTS          
  ,IsScheduledServiceCoord =   @IsScheduledServiceCoord          
  ,CompanyName =   @CompanyName          
  ,EmployeeID =   @EmployeeID          
  ,FaxNum =   @FaxNum          
  ,EmailAddress =   @EmailAddress          
  ,PersonalIDNum =   @PersonalIDNum          
  ,IsSpouseDependant =   @IsSpouseDependant          
  ,AccountID =   @AccountID          
  ,CountryID =   @CountryID          
  ,IsSIFL =   @IsSIFL          
  ,PassengerAlert =   @PassengerAlert          
  ,Gender =   @Gender          
  ,AdditionalPhoneNum =   @AdditionalPhoneNum          
  ,IsRequestor =   @IsRequestor          
  ,CountryOfResidenceID =   @CountryOfResidenceID          
  ,PaxScanDoc =   @PaxScanDoc          
  --,TSAStatus =   @TSAStatus          
  --,TSADTTM =   @TSADTTM          
  ,Addr1 =   @Addr1          
  ,Addr2 =   @Addr2          
  ,City =   @City          
  ,StateName = @StateName          
  ,PostalZipCD = @PostalZipCD          
  ,IsDeleted = @IsDeleted    
  ,PrimaryMobile = @PrimaryMobile    
  ,BusinessFax = @BusinessFax    
  ,CellPhoneNum2 = @CellPhoneNum2    
  ,OtherPhone = @OtherPhone    
  ,PersonalEmail = @PersonalEmail    
  ,OtherEmail = @OtherEmail    
  ,Addr3 = @Addr3     
  ,PassengerWeight = @PassengerWeight    
  ,PassengerWeightUnit = @PassengerWeightUnit    
  ,Birthday = @Birthday     
  ,Anniversaries = @Anniversaries    
  ,EmergencyContacts = @EmergencyContacts    
  ,CreditcardInfo = @CreditcardInfo    
  ,CateringPreferences = @CateringPreferences    
  ,HotelPreferences = @HotelPreferences    
  ,INSANumber = @INSANumber    
  ,Category = @Category    
  ,CardExpires = @CardExpires    
  ,ResidentSinceYear = @ResidentSinceYear    
  ,GreenCardGender = @GreenCardGender    
  ,GreenCardCountryID = @GreenCardCountryID  
  ,cqcustomerid = @cqcustomerid   
  ,PaxTitle = @PaxTitle  
  ,CityOfBirth = @CityOfBirth  
  ,Nickname = @Nickname   
WHERE PassengerRequestorID = @PassengerRequestorID            
END
  
GO


