/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_UpdateCRMain]    Script Date: 02/08/2013 18:28:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_UpdateCRMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRMain]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_UpdateCRMain]
(
			@CRMainID bigint
		   ,@CustomerID bigint
           ,@CRTripNUM int
           ,@Approver varchar(10)
           ,@TripID bigint
           ,@TripStatus char(1)
           ,@TripSheetStatus char(1)
           ,@AcknowledgementStatus char(1)
           ,@CharterQuoteStatus char(1)
           ,@CorporateRequestStatus char(1)
           ,@TravelCoordinatorID bigint
           ,@TravelCoordinatorName varchar(25)
           ,@TravelCoordinatorPhone varchar(25)
           ,@TravelCoordinatorFax varchar(25)
           ,@DispatchNUM varchar(12)
           ,@EstDepartureDT date
           ,@EstArrivalDT date
           ,@RecordType char(1)
           ,@FleetID bigint
           ,@AircraftID bigint
           ,@PassengerRequestorID bigint
           ,@RequestorName varchar(63)
           ,@RequestorPhone varchar(25)
           ,@DepartmentID bigint
           ,@DepartmentDescription varchar(25)
           ,@AuthorizationID bigint
           ,@AuthorizationDescription varchar(25)
           ,@HomebaseID bigint
           ,@ClientID bigint
           ,@IsPrivate bit
           ,@CRMainDescription varchar(40)
           ,@RequestDT date
           ,@BeginningGMTDT date
           ,@EndingGMTDT date
           ,@IsCrew bit
           ,@IsPassenger bit
           ,@IsAlert bit
           ,@IsAirportAlert bit
           ,@IsLog bit
           ,@LastUpdUID varchar(30)
           ,@LastUpdTS datetime
           ,@IsDeleted bit
)
AS
BEGIN 
SET NOCOUNT OFF

UPDATE [dbo].[CRMain]
   SET 
       [CRTripNUM] = @CRTripNUM
      ,[Approver] = @Approver
      ,[TripID] = @TripID
      ,[TripStatus] = @TripStatus
      ,[TripSheetStatus] = @TripSheetStatus
      ,[AcknowledgementStatus] = @AcknowledgementStatus
      ,[CharterQuoteStatus] = @CharterQuoteStatus
      ,[CorporateRequestStatus] = @CorporateRequestStatus
      ,[TravelCoordinatorID] = @TravelCoordinatorID
      ,[TravelCoordinatorName] = @TravelCoordinatorName
      ,[TravelCoordinatorPhone] = @TravelCoordinatorPhone
      ,[TravelCoordinatorFax] = @TravelCoordinatorFax
      ,[DispatchNUM] = @DispatchNUM
      ,[EstDepartureDT] = @EstDepartureDT
      ,[EstArrivalDT] = @EstArrivalDT
      ,[RecordType] = @RecordType
      ,[FleetID] = @FleetID
      ,[AircraftID] = @AircraftID
      ,[PassengerRequestorID] = @PassengerRequestorID
      ,[RequestorName] = @RequestorName
      ,[RequestorPhone] = @RequestorPhone
      ,[DepartmentID] = @DepartmentID
      ,[DepartmentDescription] = @DepartmentDescription
      ,[AuthorizationID] = @AuthorizationID
      ,[AuthorizationDescription] = @AuthorizationDescription
      ,[HomebaseID] = @HomebaseID
      ,[ClientID] = @ClientID
      ,[IsPrivate] = @IsPrivate
      ,[CRMainDescription] = @CRMainDescription
      ,[RequestDT] = @RequestDT
      ,[BeginningGMTDT] = @BeginningGMTDT
      ,[EndingGMTDT] = @EndingGMTDT
      ,[IsCrew] = @IsCrew
      ,[IsPassenger] = @IsPassenger
      ,[IsAlert] = @IsAlert
      ,[IsAirportAlert] = @IsAirportAlert
      ,[IsLog] = @IsLog
      ,[LastUpdUID] = @LastUpdUID
      ,[LastUpdTS] = @LastUpdTS
      ,[IsDeleted] = @IsDeleted
       
    WHERE CRMainID=@CRMainID and CustomerID=@CustomerID
END
GO


