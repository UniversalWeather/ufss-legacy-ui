/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetFleetbyID]    Script Date: 04/18/2013 20:42:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetFleetbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetFleetbyID]
GO


Create procedure [dbo].[spFlightPak_CorporateRequest_GetFleetbyID]
(@CustomerID BIGINT,@FleetID BIGINT)  
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 18/4/2013    
-- Description: Get the Travelcoordinator information    
-- =============================================    
SET NOCOUNT ON    
SELECT [FleetID]
      ,[TailNum]
      ,[CustomerID]
      ,[AircraftCD]
      ,[SerialNum]
      ,[TypeDescription]
      ,[MaximumReservation]
      ,[LastInspectionDT]
      ,[InspectionHrs]
      ,[WarningHrs]
      ,[MaximumPassenger]
      ,[HomebaseID]
      ,[MaximumFuel]
      ,[MinimumFuel]
      ,[BasicOperatingWeight]
      ,[MimimumRunway]
      ,[IsInActive]
      ,[IsDisplay31]
      ,[SIFLMultiControlled]
      ,[SIFLMultiNonControled]
      ,[Notes]
      ,[ComponentCD]
      ,[ClientID]
      ,[FleetType]
      ,[FlightPhoneNum]
      ,[Class]
      ,[VendorID]
      ,[VendorType]
      ,[YearMade]
      ,[ExteriorColor]
      ,[InteriorColor]
      ,[IsAFIS]
      ,[IsUWAData]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[FlightPlanCruiseSpeed]
      ,[FlightPlanMaxFlightLevel]
      ,[MaximumTakeOffWeight]
      ,[MaximumLandingWeight]
      ,[MaximumWeightZeroFuel]
      ,[TaxiFuel]
      ,[MultiSec]
      ,[ForeGrndCustomColor]
      ,[BackgroundCustomColor]
      ,[FlightPhoneIntlNum]
      ,[FleetSize]
      ,[FltScanDoc]
      ,[MinimumDay]
      ,[StandardCrewIntl]
      ,[StandardCrewDOM]
      ,[IsFAR91]
      ,[IsFAR135]
      ,[EmergencyContactID]
      ,[IsTaxDailyAdj]
      ,[IsTaxLandingFee]
      ,[IsDeleted]
      ,[AircraftID]
      ,[CrewID]
      ,[SecondaryDomFlightPhone]
      ,[SecondaryIntlFlightPhone]
      ,[MarginalPercentage]
  FROM Fleet
  where IsDeleted = 0  AND FleetID=@FleetID
GO


