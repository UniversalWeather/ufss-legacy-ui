/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_DeleteCRHotelList]    Script Date: 02/11/2013 15:56:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_DeleteCRHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRHotelList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRHotelList]
(@CRHotelListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
)
AS BEGIN

	SET NOCOUNT OFF;
	Delete from CRHotelList
	WHERE 
		CRHotelListID = @CRHotelListID and CustomerID=@CustomerID and CRLegID=@CRLegID

END
GO


