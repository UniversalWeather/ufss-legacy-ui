/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetPassengerbyID]    Script Date: 04/18/2013 20:42:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetPassengerbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetPassengerbyID]
GO


Create procedure [dbo].[spFlightPak_CorporateRequest_GetPassengerbyID]
(@CustomerID BIGINT,@PassengerRequestorID BIGINT)  
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 18/4/2013    
-- Description: Get the Aircraft information    
-- =============================================    
SET NOCOUNT ON 
SELECT [PassengerRequestorID]
      ,[PassengerRequestorCD]
      ,[PassengerName]
      ,[CustomerID]
      ,[DepartmentID]
      ,[PassengerDescription]
      ,[PhoneNum]
      ,[IsEmployeeType]
      ,[StandardBilling]
      ,[AuthorizationID]
      ,[AuthorizationDescription]
      ,[Notes]
      ,[LastName]
      ,[FirstName]
      ,[MiddleInitial]
      ,[DateOfBirth]
      ,[ClientID]
      ,[AssociatedWithCD]
      ,[HomebaseID]
      ,[IsActive]
      ,[FlightPurposeID]
      ,[SSN]
      ,[Title]
      ,[SalaryLevel]
      ,[IsPassengerType]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsScheduledServiceCoord]
      ,[CompanyName]
      ,[EmployeeID]
      ,[FaxNum]
      ,[EmailAddress]
      ,[PersonalIDNum]
      ,[IsSpouseDependant]
      ,[AccountID]
      ,[CountryID]
      ,[IsSIFL]
      ,[PassengerAlert]
      ,[Gender]
      ,[AdditionalPhoneNum]
      ,[IsRequestor]
      ,[CountryOfResidenceID]
      ,[PaxScanDoc]
      ,[TSAStatus]
      ,[TSADTTM]
      ,[Addr1]
      ,[Addr2]
      ,[City]
      ,[StateName]
      ,[PostalZipCD]
      ,[IsDeleted]
      ,[PrimaryMobile]
      ,[BusinessFax]
      ,[CellPhoneNum2]
      ,[OtherPhone]
      ,[PersonalEmail]
      ,[OtherEmail]
      ,[Addr3]
      ,[PassengerWeight]
      ,[PassengerWeightUnit]
      ,[Birthday]
      ,[Anniversaries]
      ,[EmergencyContacts]
      ,[CreditcardInfo]
      ,[CateringPreferences]
      ,[HotelPreferences]
      ,[INSANumber]
      ,[Category]
      ,[GreenCardCountryID]
      ,[CardExpires]
      ,[ResidentSinceYear]
      ,[GreenCardGender]
      ,[CQCustomerID]
      ,[PaxTitle]
	  ,[CityOfBirth]
	  ,[Nickname]
  FROM Passenger
    where IsDeleted = 0  AND PassengerRequestorID=@PassengerRequestorID



GO


