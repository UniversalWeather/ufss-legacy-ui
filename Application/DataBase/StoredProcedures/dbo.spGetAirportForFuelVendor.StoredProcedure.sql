
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportForFuelVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportForFuelVendor]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAirportForFuelVendor](@CustomerID BIGINT)      
AS      
-- =============================================      
-- Author: Srinivasan. J  
-- Create date: 25/08/2012      
-- Description: Get the AIRPORT information for Fuel Vendor  
-- =============================================      
SET NOCOUNT ON      
IF LEN(@CustomerID) >0      
BEGIN         
SELECT  A.[AirportID]      
        ,A.[IcaoID]      
        ,A.[CustomerID]  
        ,A.[Iata]      
  FROM  [Airport] A        
 where A.CustomerID = @CustomerID and isnull(Iata,'') <> '' and isnull(A.IsDeleted,0) = 0    
union all      
SELECT  A.[AirportID]      
        ,A.[IcaoID]     
        ,A.[CustomerID]      
        ,A.[Iata]  
  FROM  [Airport] A     
 where A.UwaID is not null and A.CustomerID <> @CustomerID and A.IcaoID not in     
 (Select distinct IcaoID  from Airport where CustomerID = @CustomerID )      
 and  isnull(Iata,'') <> '' and isnull(A.IsDeleted,0) = 0  
end

GO


