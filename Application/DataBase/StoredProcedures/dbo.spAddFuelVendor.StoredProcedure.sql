
GO
/****** Object:  StoredProcedure [dbo].[spAddFuelVendor]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFuelVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFuelVendor]
go
CREATE PROCEDURE [dbo].[spAddFuelVendor](  
                                         @VendorCD CHAR(4),   
                                         @VendorName VARCHAR(50),   
                                         @AirportID BIGINT,  
                                         @CustomerID BIGINT,  
                                         @VendorDescription VARCHAR(50),    
                                         @ServerInfo VARCHAR(200),    
                                         @BaseFileName VARCHAR(100),    
                                         @FileLocation VARCHAR(200),    
                                         @FileTYPE VARCHAR(10),    
                                         @FldSep CHAR(1),   
                                         @IsHeaderRow BIT,    
                                         @VendorDateFormat VARCHAR(10),    
                                         @LastUpdUID VARCHAR(30),    
                                         @LastUpdTS DATETIME ,   
                                         @IsDeleted BIT,    
                                         @UWAUpdates BIT,
                                         @IsInActive BIT,
										 @NoOfFuelRecordsInserted BIGINT=0)    
-- =============================================      
-- Author:Badrinath      
-- Create date: 03/05/2012      
-- Description: Insert the Fuel Vendor information      
-- =============================================      
AS      
BEGIN       
SET NOCOUNT ON    
DECLARE @FuelVendorID BIGINT  
DECLARE @UWACustomerID BIGINT
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output  
if  not exists(select VendorCD from FuelVendor where VendorCD=@VendorCD)
begin
--To generate PrimaryKey 
 EXECUTE dbo.usp_GetSequenceNumber @UWACustomerID, 'MasterModuleCurrentNo',  @FuelVendorID OUTPUT   
--End of Primary Key generation

--To store the lastupdateddate as UTC date
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
--End of UTC date

 INSERT INTO [FuelVendor]  ( FuelVendorID,  
                             VendorCD ,   
                             VendorName,   
                             AirportID,  
                             CustomerID,  
                             VendorDescription,    
                             ServerInfo,    
                             BaseFileName,    
                             FileLocation,    
                             FileTYPE,    
                             FldSep,   
                             IsHeaderRow,    
                             VendorDateFormat,    
                             LastUpdUID,    
                             LastUpdTS,   
                             IsDeleted,    
                             UWAUpdates,
                             IsInActive,
							 NoOfFuelRecordsInserted)    
                      VALUES (@FuelVendorID,  
                              @VendorCD ,   
                              @VendorName,   
                              @AirportID,  
                              @UWACustomerID,  
                              @VendorDescription,    
                              @ServerInfo,    
                              @BaseFileName,    
                              @FileLocation,    
                              @FileTYPE,    
                              @FldSep,   
                              @IsHeaderRow,    
                              @VendorDateFormat,    
                              @LastUpdUID,    
                              @LastUpdTS,   
                              @IsDeleted,    
                              @UWAUpdates,
                              @IsInActive,
							  @NoOfFuelRecordsInserted)    
   SELECT @FuelVendorID as FuelVendorID

END

ELSE
BEGIN

select @FuelVendorID=FuelVendorID from FuelVendor where VendorCD=@VendorCD

 EXEC spUpdateFuelVendor
 @FuelVendorID,  @VendorCD ,   
                              @VendorName,   
                              @AirportID,  
                              @UWACustomerID,  
                              @VendorDescription,    
                              @ServerInfo,    
                              @BaseFileName,    
                              @FileLocation,    
                              @FileTYPE,    
                              @FldSep,   
                              @IsHeaderRow,    
                              @VendorDateFormat,    
                              @LastUpdUID,    
                              @LastUpdTS,   
                              @IsDeleted,    
                              @UWAUpdates,
                              @IsInActive,
							  @NoOfFuelRecordsInserted

select FuelVendorID from FuelVendor where VendorCD=@VendorCD
END

END