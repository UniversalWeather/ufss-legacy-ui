
GO

/****** Object:  StoredProcedure [dbo].[spUpdateBudget]    Script Date: 08/25/2012 17:15:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateBudget]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateBudget]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateBudget]    Script Date: 08/24/2012 10:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateBudget](@BudgetID BIGINT,
										@CustomerID BIGINT,
										@AccountID BIGINT,
										@FleetID  BIGINT,
										@AccountNum VARCHAR(32),
										@FiscalYear CHAR(4),
										@Month1Budget NUMERIC(17,2),
										@Month2Budget NUMERIC(17,2),
										@Month3Budget NUMERIC(17,2),
										@Month4Budget NUMERIC(17,2),
										@Month5Budget NUMERIC(17,2),
										@Month6Budget NUMERIC(17,2),
										@Month7Budget NUMERIC(17,2),
										@Month8Budget NUMERIC(17,2),
										@Month9Budget NUMERIC(17,2),
										@Month10Budget NUMERIC(17,2),
										@Month11Budget NUMERIC(17,2),
										@Month12Budget NUMERIC(17,2), 
										@LastUpdUID VARCHAR(30),
										@LastUpdTS DATETIME,
										@IsDeleted BIT,
										@IsInActive BIT)  
-- =============================================  
-- Author:Sujitha.V  
-- Create date: 10/5/2012  
-- Description: Update the Budget  information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON  
set @LastUpdTS= GETUTCDATE()
UPDATE [Budget]  
        SET [CustomerID]=@CustomerID
           ,[AccountID]=@AccountID
           ,[FleetID]=@FleetID
           ,[AccountNum]=@AccountNum
           ,[FiscalYear]=@FiscalYear
           ,[Month1Budget]=@Month1Budget
           ,[Month2Budget]=@Month2Budget
           ,[Month3Budget]=@Month3Budget
           ,[Month4Budget]=@Month4Budget
           ,[Month5Budget]=@Month5Budget
           ,[Month6Budget]=@Month6Budget
           ,[Month7Budget]=@Month7Budget
           ,[Month8Budget]=@Month8Budget
           ,[Month9Budget]=@Month9Budget
           ,[Month10Budget]=@Month10Budget
           ,[Month11Budget]=@Month11Budget
           ,[Month12Budget]=@Month12Budget
           ,[LastUpdUID]=@LastUpdUID
           ,[LastUpdTS]=@LastUpdTS
           ,[IsDeleted]=@IsDeleted  
           ,[IsInActive]=@IsInActive     
 WHERE BudgetID =@BudgetID  
  
END
GO
