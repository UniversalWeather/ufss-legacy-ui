/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_CancelRequest]    Script Date: 02/19/2013 19:52:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_CancelRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_CancelRequest]
GO


CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_CancelRequest]
(
			@CRMainID bigint
		   ,@CustomerID bigint 
           ,@CorporateRequestStatus char(1)
           ,@LastUpdUID varchar(30)
)
AS
BEGIN 
SET NOCOUNT OFF
	DECLARE @LastUpdTS DATETIME
	SET @LastUpdTS = GETUTCDATE()

	UPDATE CRMain
	SET	CorporateRequestStatus = 'X',
		LastUpdUID = @LastUpdUID,
		LastUpdTS = @LastUpdTS
	WHERE CRMainID = @CRMainID

END
GO


