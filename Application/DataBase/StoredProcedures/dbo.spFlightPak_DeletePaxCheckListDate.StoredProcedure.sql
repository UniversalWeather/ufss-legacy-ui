

/****** Object:  StoredProcedure [dbo].[spFlightPak_DeletePaxCheckListDate]    Script Date: 03/28/2013 18:43:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_DeletePaxCheckListDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_DeletePaxCheckListDate]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_DeletePaxCheckListDate]    Script Date: 03/28/2013 18:43:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_DeletePaxCheckListDate]    
(  
 @CustomerID BIGINT,   
 @PassengerID BIGINT,      
 @LastUpdUID char(10),    
 @LastUpdTS datetime,    
 @IsDeleted bit    
)    
AS  
BEGIN  
 SET NoCOUNT ON    
 --Check if the record is not being used anywhere in the application before Delete  
DECLARE @RecordUsed INT  
DECLARE @ENTITYNAME VARCHAR(50)          
SET @ENTITYNAME = N'PassengerCheckListDetail'; -- Type Table Name  
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @PassengerID, @RecordUsed OUTPUT  
--End of Delete Check  
  
 if (@RecordUsed <> 0)  
 Begin  
 RAISERROR(N'-500010', 17, 1)  
 end  
 else  
   BEGIN    
   SET @LastUpdTS = GETUTCDATE()      
UPDATE [PassengerCheckListDetail] SET [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted     
 WHERE [PassengerID] = @PassengerID AND [CustomerID] = @CustomerID  
END  
END  
GO


