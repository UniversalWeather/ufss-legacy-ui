

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQMain]    Script Date: 03/26/2013 11:18:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQMain]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_InsertCQMain]    Script Date: 03/26/2013 11:18:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_InsertCQMain]
(
	
	@CustomerID bigint,
	@CQFileID bigint,
	@FileNUM int,
	@QuoteNUM int,
	@QuoteID int,
	@CQSource numeric(1, 0),
	@VendorID bigint,
	@FleetID bigint,
	@AircraftID bigint,
	@IsCustomerCQ bit,
	@FederalTax numeric(5, 2),
	@QuoteRuralTax numeric(5, 2),
	@IsTaxable bit,
	@PassengerRequestorID bigint,
	@RequestorName varchar(25),
	@RequestorPhoneNUM varchar(25),
	@IsSubmitted bit,
	@LastSubmitDT date,
	@IsAccepted bit,
	@LastAcceptDT date,
	@RejectDT date,
	@Comments varchar(200),
	@IsFinancialWarning bit,
	@LiveTrip char(1),
	@MinimumDayUsageTotal numeric(6, 2),
	@BillingCrewRONTotal numeric(3, 0),
	@AdditionalCrewTotal numeric(17, 2),
	@AdditionalCrewRONTotal1 numeric(2, 0),
	@StdCrewRONTotal numeric(17, 2),
	@AdditionalCrewRONTotal2 numeric(17, 2),
	@WaitTMTotal numeric(9, 3),
	@WaitChgTotal numeric(17, 2),
	@HomebaseID bigint,
	@ClientID bigint,
	@CQLostBusinessID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@StandardCrewDOM numeric(2, 0),
	@StandardCrewINTL numeric(2, 0),
	@DaysTotal numeric(4, 0),
	@FlightHoursTotal numeric(9, 3),
	@AverageFlightHoursTotal numeric(7, 2),
	@AdjAmtTotal numeric(17, 3),
	@UsageAdjTotal numeric(17, 2),
	@IsOverrideUsageAdj bit,
	@SegmentFeeTotal numeric(17, 2),
	@IsOverrideSegmentFee bit,
	@LandingFeeTotal numeric(17, 2),
	@IsOverrideLandingFee bit,
	@AdditionalFeeTotalD numeric(17, 2),
	@FlightChargeTotal numeric(17, 2),
	@SubtotalTotal numeric(17, 2),
	@DiscountPercentageTotal numeric(6, 2),
	@IsOverrideDiscountPercentage bit,
	@DiscountAmtTotal numeric(17, 2),
	@IsOverrideDiscountAMT bit,
	@TaxesTotal numeric(17, 2),
	@IsOverrideTaxes bit,
	@MarginalPercentTotal numeric(7, 2),
	@MarginTotal numeric(17, 2),
	@QuoteTotal numeric(17, 2),
	@IsOverrideTotalQuote bit,
	@CostTotal numeric(17, 2),
	@IsOverrideCost bit,
	@PrepayTotal numeric(17, 2),
	@RemainingAmtTotal numeric(17, 2),
	@TripID bigint,
	@IsPrintUsageAdj bit,
	@UsageAdjDescription varchar(60),
	@CustomUsageAdj numeric(17, 2),
	@IsPrintAdditonalFees bit,
	@AdditionalFeeDescription varchar(60),
	@AdditionalFeesTax numeric(17, 2),
	@PositioningDescriptionINTL numeric(17, 2),
	@IsAdditionalFees numeric(17, 2),
	@CustomAdditionalFees numeric(17, 2),
	@IsPrintLandingFees bit,
	@LandingFeeDescription varchar(60),
	@CustomLandingFee numeric(17, 2),
	@IsPrintSegmentFee bit,
	@SegmentFeeDescription varchar(60),
	@CustomSegmentFee numeric(17, 2),
	@IsPrintStdCrew bit,
	@StdCrewROMDescription varchar(60),
	@CustomStdCrewRON numeric(17, 2),
	@IsPrintAdditionalCrew bit,
	@AdditionalCrewDescription varchar(60),
	@CustomAdditionalCrew numeric(17, 2),
	@IsAdditionalCrewRON bit,
	@AdditionalDescriptionCrewRON varchar(60),
	@CustomAdditionalCrewRON numeric(17, 2),
	@IsPrintWaitingChg bit,
	@WaitChgDescription varchar(60),
	@CustomWaitingChg numeric(17, 2),
	@IsPrintFlightChg bit,
	@FlightChgDescription varchar(60),
	@CustomFlightChg numeric(17, 2),
	@IsPrintSubtotal bit,
	@SubtotalDescription varchar(60),
	@IsPrintDiscountAmt bit,
	@DescriptionDiscountAmt varchar(60),
	@IsPrintTaxes bit,
	@TaxesDescription varchar(60),
	@IsPrintInvoice bit,
	@InvoiceDescription varchar(60),
	@ReportQuote numeric(17, 2),
	@IsPrintPay bit,
	@PrepayDescription varchar(60),
	@IsPrintRemaingAMT bit,
	@RemainingAmtDescription varchar(60),
	@IsInActive bit,
	@IsDailyTaxAdj bit,
	@IsLandingFeeTax bit,
	@FeeGroupID bigint,
	@CustomSubTotal NUMERIC(17,2), 
	@CustomDiscountAmt NUMERIC(17,2),
	@CustomTaxes NUMERIC(17,2),
	@CustomPrepay NUMERIC(17,2),
	@CustomRemainingAmt NUMERIC(17,2),
	@IsPrintTotalQuote BIT,
	@TotalQuoteDescription VARCHAR(60),
	@CustomTotalQuote NUMERIC(17,2),
	@QuoteStage VARCHAR(20)
)
AS
	SET NOCOUNT OFF;
	set @LastUpdTS = GETUTCDATE()
	DECLARE  @CQMainID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQMainID OUTPUT 
	
INSERT INTO [CQMain] 
	([CQMainID], [CustomerID], [CQFileID], [FileNUM], [QuoteNUM], [QuoteID], [CQSource], [VendorID], [FleetID],
	[AircraftID], [IsCustomerCQ], [FederalTax], [QuoteRuralTax], [IsTaxable], [PassengerRequestorID], [RequestorName], 
	[RequestorPhoneNUM], [IsSubmitted], [LastSubmitDT], [IsAccepted], [LastAcceptDT], [RejectDT], [Comments], 
	[IsFinancialWarning], [LiveTrip], [MinimumDayUsageTotal], [BillingCrewRONTotal], [AdditionalCrewTotal],
	[AdditionalCrewRONTotal1], [StdCrewRONTotal], [AdditionalCrewRONTotal2], [WaitTMTotal], [WaitChgTotal], 
	[HomebaseID], [ClientID], [CQLostBusinessID], [LastUpdUID], [LastUpdTS], [IsDeleted], [StandardCrewDOM], 
	[StandardCrewINTL], [DaysTotal], [FlightHoursTotal], [AverageFlightHoursTotal], [AdjAmtTotal], [UsageAdjTotal],
	[IsOverrideUsageAdj], [SegmentFeeTotal], [IsOverrideSegmentFee], [LandingFeeTotal], [IsOverrideLandingFee], 
	[AdditionalFeeTotalD], [FlightChargeTotal], [SubtotalTotal], [DiscountPercentageTotal], [IsOverrideDiscountPercentage],
	[DiscountAmtTotal], [IsOverrideDiscountAMT], [TaxesTotal], [IsOverrideTaxes], [MarginalPercentTotal], [MarginTotal], 
	[QuoteTotal], [IsOverrideTotalQuote], [CostTotal], [IsOverrideCost], [PrepayTotal], [RemainingAmtTotal], [TripID], 
	[IsPrintUsageAdj], [UsageAdjDescription], [CustomUsageAdj], [IsPrintAdditonalFees], [AdditionalFeeDescription], 
	[AdditionalFeesTax], [PositioningDescriptionINTL], [IsAdditionalFees], [CustomAdditionalFees], [IsPrintLandingFees],
	[LandingFeeDescription], [CustomLandingFee], [IsPrintSegmentFee], [SegmentFeeDescription], [CustomSegmentFee], 
	[IsPrintStdCrew], [StdCrewROMDescription], [CustomStdCrewRON], [IsPrintAdditionalCrew], [AdditionalCrewDescription],
	[CustomAdditionalCrew], [IsAdditionalCrewRON], [AdditionalDescriptionCrewRON], [CustomAdditionalCrewRON], [IsPrintWaitingChg],
	[WaitChgDescription], [CustomWaitingChg], [IsPrintFlightChg], [FlightChgDescription], [CustomFlightChg], [IsPrintSubtotal], 
	[SubtotalDescription], [IsPrintDiscountAmt], [DescriptionDiscountAmt], [IsPrintTaxes], [TaxesDescription], [IsPrintInvoice], 
	[InvoiceDescription], [ReportQuote], [IsPrintPay], [PrepayDescription], [IsPrintRemaingAMT], [RemainingAmtDescription],
	IsInActive ,	IsDailyTaxAdj ,	IsLandingFeeTax ,	FeeGroupID ,
	CustomSubTotal, 	CustomDiscountAmt, 	CustomTaxes, 	CustomPrepay, 	CustomRemainingAmt, 
	IsPrintTotalQuote, 	TotalQuoteDescription, 	CustomTotalQuote,QuoteStage	
	) 
	VALUES (@CQMainID, @CustomerID, @CQFileID, @FileNUM, @QuoteNUM, @QuoteID, @CQSource, @VendorID, @FleetID, @AircraftID, @IsCustomerCQ, @FederalTax, @QuoteRuralTax, @IsTaxable, @PassengerRequestorID, @RequestorName, @RequestorPhoneNUM, @IsSubmitted, @LastSubmitDT, @IsAccepted, @LastAcceptDT, @RejectDT, @Comments, @IsFinancialWarning, @LiveTrip, @MinimumDayUsageTotal, @BillingCrewRONTotal, @AdditionalCrewTotal, @AdditionalCrewRONTotal1, @StdCrewRONTotal, @AdditionalCrewRONTotal2, @WaitTMTotal, @WaitChgTotal, @HomebaseID, @ClientID, @CQLostBusinessID, @LastUpdUID, @LastUpdTS, @IsDeleted, @StandardCrewDOM, @StandardCrewINTL, @DaysTotal, @FlightHoursTotal, @AverageFlightHoursTotal, @AdjAmtTotal, @UsageAdjTotal, @IsOverrideUsageAdj, @SegmentFeeTotal, @IsOverrideSegmentFee, @LandingFeeTotal, @IsOverrideLandingFee, @AdditionalFeeTotalD, @FlightChargeTotal, @SubtotalTotal, @DiscountPercentageTotal, @IsOverrideDiscountPercentage, @DiscountAmtTotal, @IsOverrideDiscountAMT, @TaxesTotal, @IsOverrideTaxes, @MarginalPercentTotal, @MarginTotal, @QuoteTotal, @IsOverrideTotalQuote, @CostTotal, @IsOverrideCost, @PrepayTotal, @RemainingAmtTotal, @TripID, @IsPrintUsageAdj, @UsageAdjDescription, @CustomUsageAdj, @IsPrintAdditonalFees, @AdditionalFeeDescription, @AdditionalFeesTax, @PositioningDescriptionINTL, @IsAdditionalFees, @CustomAdditionalFees, @IsPrintLandingFees, @LandingFeeDescription, @CustomLandingFee, @IsPrintSegmentFee, @SegmentFeeDescription, @CustomSegmentFee, @IsPrintStdCrew, @StdCrewROMDescription, @CustomStdCrewRON, @IsPrintAdditionalCrew, @AdditionalCrewDescription, @CustomAdditionalCrew, @IsAdditionalCrewRON, @AdditionalDescriptionCrewRON, @CustomAdditionalCrewRON, @IsPrintWaitingChg, @WaitChgDescription, @CustomWaitingChg, @IsPrintFlightChg, @FlightChgDescription, @CustomFlightChg, @IsPrintSubtotal, @SubtotalDescription, @IsPrintDiscountAmt, @DescriptionDiscountAmt, @IsPrintTaxes, @TaxesDescription, @IsPrintInvoice, @InvoiceDescription, @ReportQuote, @IsPrintPay, @PrepayDescription, @IsPrintRemaingAMT, @RemainingAmtDescription,@IsInActive,
	@IsDailyTaxAdj,	@IsLandingFeeTax,	@FeeGroupID, 
	@CustomSubTotal, 
	@CustomDiscountAmt,
	@CustomTaxes ,
	@CustomPrepay ,
	@CustomRemainingAmt ,
	@IsPrintTotalQuote ,
	@TotalQuoteDescription ,
	@CustomTotalQuote,
	@QuoteStage	
	)	
	
SELECT @CQMainID as 'CQMainID'


GO


