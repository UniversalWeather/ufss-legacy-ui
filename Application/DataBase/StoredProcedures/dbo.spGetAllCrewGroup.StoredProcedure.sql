/****** Object:  StoredProcedure [dbo].[spGetAllCrewGroup]    Script Date: 01/07/2013 19:58:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCrewGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCrewGroup]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCrewGroup]    Script Date: 01/07/2013 19:58:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllCrewGroup](@CustomerID BIGINT)        
AS        
BEGIN      
SET NOCOUNT ON        
        
               SELECT  cg.CrewGroupID      
        ,cg.CrewGroupCD      
        ,cg.CustomerID      
        ,cg.CrewGroupDescription      
        ,cg.LastUpdUID      
        ,cg.LastUpdTS      
        ,cg.HomebaseID      
        ,cg.IsDeleted 
        ,cg.IsInActive     
       -- ,c.HomebaseCD         
      ,Airport.ICAOID AS HomebaseCD          
                 FROM  CrewGroup cg left outer join Company c on c.HomebaseID = cg.HomebaseID         
        LEFT OUTER JOIN Airport ON Airport.AirportID = c.HomebaseAirportID          
                 WHERE cg.CustomerID=@CustomerID       
                 AND cg.IsDeleted='false'     
                 order by cg.CrewGroupCD     
END  
GO


