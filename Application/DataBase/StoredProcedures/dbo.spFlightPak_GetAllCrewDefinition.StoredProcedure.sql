/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCrewDefinition]    Script Date: 01/07/2013 19:52:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllCrewDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllCrewDefinition]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCrewDefinition]    Script Date: 01/07/2013 19:52:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetAllCrewDefinition]      
   
AS      
BEGIN    
SET NOCOUNT ON;      
SELECT     [CrewInfoXRefID]    
           ,[CrewID]    
           ,[CustomerID]    
           ,[CrewInfoID]    
           ,[InformationDESC]    
           ,[InformationValue]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsReptFilter]    
           ,[IsDeleted] 
           ,[IsInActive]
                
  FROM CrewDefinition  
   
END  
GO


