
/****** Object:  StoredProcedure [dbo].[spUpdateFleetPair]    Script Date: 8/13/2015 8:31:31 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFleetPair]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFleetPair];
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFleetPair]    Script Date: 8/13/2015 8:31:31 AM ******/

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE [dbo].[spUpdateFleetPair]
(  @FleetProfileInternationalDataID BIGINT
 , @CustomerID BIGINT
 , @FleetID BIGINT
 , @AircraftCD char(3)
 , @BuyAircraftAdditionalFeeDOM varchar(8)
 , @AircraftMake varchar(30)
 , @AircraftModel varchar(20)
 , @AircraftColor1 varchar(15)
 , @AircraftColor2 varchar(15)
 , @AircraftTrimColor varchar(15)
 , @OwnerLesse varchar(100)
 , @Addr1 varchar(25)
 , @Addr2 varchar(25)
 , @CityName varchar(25)
 , @StateName varchar(10)
 , @CountryID BIGINT
 , @PostalZipCD varchar(15)
 , @ManufactureYear int
 , @PLastName varchar(20)
 , @PFirstName varchar(20)
 , @PMiddlename varchar(20)
 , @PhoneNum varchar(25)
 , @FaxNum varchar(25)
 , @EmailAddress varchar(100)
 , @Company varchar(100)
 , @OLastName varchar(20)
 , @OFirstName varchar(20)
 , @OMiddleName varchar(20)
 , @Address1 varchar(25)
 , @Address2 varchar(25)
 , @OCityName varchar(25)
 , @OStateName varchar(10)
 , @ZipCD varchar(15)
 , @OCountryID BIGINT
 , @OPhoneNum varchar(25)
 , @OFaxNum varchar(25)
 , @OEmailAddress varchar(100)
 , @LastUpdUID varchar(30)
 , @LastUpdTS datetime
 , @IsDeleted bit
 , @BusinessPhone varchar(25)
 , @OPCellPhoneNum varchar(25)
 , @OPHomeFax varchar(25)
 , @OPCellPhoneNum2 varchar(25)
 , @OPOtherPhone varchar(25)
 , @OPPersonalEmail varchar(250)
 , @OPOtherEmail varchar(250)
 , @Address3 varchar(40)
 , @OPBusinessPhone2 varchar(25)
 , @OLCellPhoneNum varchar(25)
 , @OLHomeFax varchar(25)
 , @OLCellPhoneNum2 varchar(25)
 , @OLOtherPhone varchar(25)
 , @OLPersonalEmail varchar(250)
 , @OLOtherEmail varchar(250)
 , @Addr3 varchar(40)
 , @Year1 int 
 , @Year2 int 
 , @BuyAircraftAdditionalFeeDOM2 varchar(8)
)
/* =============================================
 Author: Mathes
 Create date: 26/06/2012
 Description: Update the FleetPair information
 =============================================*/
AS
BEGIN
SET NOCOUNT ON;
SET @LastUpdTS = GETUTCDATE();

	UPDATE FleetPair 
	SET 
	      AircraftCD=@AircraftCD ,
           BuyAircraftAdditionalFeeDOM=@BuyAircraftAdditionalFeeDOM ,
           AircraftMake=@AircraftMake ,
           AircraftModel=@AircraftModel ,
           AircraftColor1=@AircraftColor1 ,
           AircraftColor2=@AircraftColor2 ,
           AircraftTrimColor=@AircraftTrimColor ,
           OwnerLesse=@OwnerLesse ,
           Addr1=@Addr1 ,
           Addr2=@Addr2 ,
           CityName=@CityName ,
           StateName=@StateName ,
           CountryID=@CountryID ,
           PostalZipCD=@PostalZipCD ,
           ManufactureYear=@ManufactureYear ,
           PLastName=@PLastName ,
           PFirstName=@PFirstName ,
           PMiddlename=@PMiddlename ,
           PhoneNum=@PhoneNum ,
           FaxNum=@FaxNum ,
           EmailAddress=@EmailAddress ,
           Company=@Company ,
           OLastName=@OLastName ,
           OFirstName=@OFirstName ,
           OMiddleName=@OMiddleName ,
           Address1=@Address1 ,
           Address2=@Address2 ,
           OCityName=@OCityName ,
           OStateName=@OStateName ,
           ZipCD=@ZipCD ,
           OCountryID=@OCountryID ,  
           OPhoneNum=@OPhoneNum ,
           OFaxNum = @OFaxNum ,
           OEmailAddress=@OEmailAddress ,     
           LastUpdUID = @LastUpdUID ,
           LastUpdTS = @LastUpdTS ,
           IsDeleted = @IsDeleted ,
           BusinessPhone = @BusinessPhone ,
           OPCellPhoneNum = @OPCellPhoneNum ,
           OPHomeFax = @OPHomeFax ,
           OPCellPhoneNum2 = @OPCellPhoneNum2 ,
           OPOtherPhone = @OPOtherPhone ,
           OPPersonalEmail = @OPPersonalEmail ,
           OPOtherEmail = @OPOtherEmail ,
           Address3 = @Address3 , 
           OPBusinessPhone2  = @OPBusinessPhone2 ,
           OLCellPhoneNum = @OLCellPhoneNum ,
           OLHomeFax = @OLHomeFax ,
           OLCellPhoneNum2 = @OLCellPhoneNum2 ,
           OLOtherPhone = @OLOtherPhone ,
           OLPersonalEmail = @OLPersonalEmail , 
           OLOtherEmail = @OLOtherEmail , 
           Addr3 = @Addr3,
		[Year1] = @Year1,
	     [Year2] = @Year2,
	     [BuyAircraftAdditionalFeeDOM2] = @BuyAircraftAdditionalFeeDOM2 
	WHERE FleetProfileInternationalDataID = @FleetProfileInternationalDataID;

END;

GO


