

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetDepartmentByCDOrID]    Script Date: 01/24/2014 11:10:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetDepartmentByCDOrID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetDepartmentByCDOrID]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetDepartmentByCDOrID]    Script Date: 01/24/2014 11:10:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetDepartmentByCDOrID](@CustomerID  BIGINT,
@DepartmentCD varchar(8),
@DepartmentID BIGINT
)          
AS       
BEGIN         
-- =============================================          
-- Author: Sujitha.V          
-- Create date: 5/5/2012          
-- Description: Get Department Informations    
-- EXEC [spFlightPak_Preflight_GetDepartmentByCDOrID] 10003, '11', 0
-- =============================================          
SET NOCOUNT ON          
  
    
SELECT         
  d.DepartmentID,        
  d.[DepartmentCD],         
  d.[DepartmentName],        
  d.[CustomerID],      
  d.ClientID,      
 --ISNULL(NULLIF(d.ClientID,''),'-') as ClientID,          
       
 d.[LastUpdUID],          
 d.[LastUpdTS] ,          
 d.[IsInActive],          
 d.[IsDeleted]  ,        
 ISNULL(NULLIF(c.ClientCD,''),'') as ClientCD,    
 d.DepartPercentage    
 --c.ClientCD        
--FROM  Department d INNER JOIN Client c on d.ClientID = c.ClientID  WHERE d.CustomerID =@CustomerID  AND ISNULL(d.IsDeleted,0) = 0      
FROM  Department d LEFT OUTER JOIN Client c on d.ClientID = c.ClientID  WHERE 
d.CustomerID =@CustomerID  
AND d.DepartmentID = case when @DepartmentID <>0 then @DepartmentID else d.DepartmentID end 
AND d.DepartmentCD = case when Ltrim(Rtrim(@DepartmentCD)) <> '' then @DepartmentCD else d.DepartmentCD end 
AND ISNULL(d.IsDeleted,0) = 0    

  
order by DepartmentCD    
END      


GO


