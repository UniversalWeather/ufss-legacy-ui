

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightCheckListByPreflightCheckListID]    Script Date: 09/19/2013 15:21:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightCheckListByPreflightCheckListID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightCheckListByPreflightCheckListID]
GO



/****** Object:  StoredProcedure [dbo].[GetPreflightCheckListByPreflightCheckListID]    Script Date: 09/19/2013 15:21:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create Procedure [dbo].[spFlightPak_Preflight_GetPreflightCheckListByPreflightCheckListID]
(
@CustomerID BIGINT,
@PreflightCheckListID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get GetPreflightCheckListBy PreflightCheckListID 
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightCheckList PC 
 where 
 PC.PreflightCheckListID = @PreflightCheckListID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 



GO


