
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAccountByCDorID]    Script Date: 01/24/2014 11:09:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAccountByCDorID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAccountByCDorID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAccountByCDorID]    Script Date: 01/24/2014 11:09:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAccountByCDorID](@CustomerID  BIGINT,
@AccountNum varchar(50),
@AccountID bigint
)            
AS          
-- =============================================            
-- Author:Prabhu            
-- Create date: 12/4/2012            
-- Description: Get the Account information           
-- EXEC [spFlightPak_Preflight_GetAccountByCDorID] 10002, '', 0
-- =============================================            
SET NOCOUNT ON             
	BEGIN             
		SELECT Account.AccountID        
		  ,CASE WHEN [AccountNum] IS NULL THEN '' ELSE LTRIM(RTRIM(Account.AccountNum)) END AS AccountNum       
		  ,Account.CustomerID        
		  ,Account.AccountDescription        
		  ,Account.CorpAccountNum        
		  ,Account.HomebaseID        
		  ,Account.IsBilling        
		  ,Account.LastUpdUID        
		  ,Account.LastUpdTS        
		  ,Account.Cost        
		  ,Account.IsInActive        
		  ,Account.IsDeleted        
		  ,Airport.ICAOID AS HomebaseCD        
		  ,Company.BaseDescription        
		FROM Account        
			LEFT OUTER JOIN Company ON Company.HomebaseID = Account.HomebaseID        
			LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID        
		WHERE 
		Account.CustomerID =@CustomerID   
		AND Account.IsDeleted = 'false'   
		AND Account.AccountNum = case when LTRIM(Rtrim(@AccountNum))<>'' then @AccountNum else Account.AccountNum end
		AND Account.AccountID = case when @AccountID <>0 then @AccountID else Account.AccountID end
		order by Account.AccountNum           
	END  

 
 
  

GO


