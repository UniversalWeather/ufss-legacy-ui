GO
/****** Object:  StoredProcedure [dbo].[spGetFleetSearch]    Script Date: 04/09/2013 14:11:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[spGetFleetSearch]                    
( @CustomerID BIGINT=0,                  
  @IsInActive BIT=0,                  
  @IsFixedRotary CHAR(1)='B',                   
  @ClientID BIGINT=0,                  
  @HomeBaseID BIGINT=0                  
  )                              
AS                              
               
-- =============================================                             
-- Author: Mathes                              
-- Create date: 23-09-2012                    
-- Description: Get Fleet Profile Informations                              
-- =============================================                    
                             
BEGIN                
if (@ClientID is null)              
begin               
 set @ClientID = 0              
end              
if (@HomeBaseID is null)              
begin               
 set @HomeBaseID = 0              
end               
set nocount on                 
              
 IF @IsFixedRotary ='B' AND @IsInActive=0 And @ClientID=0 And @HomeBaseID =0                  
 BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor          
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID    
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  Order By Fleet.AircraftCD          
 END                    
 ELSE IF @IsFixedRotary <>'B' AND @IsInActive>0 And @ClientID>0 And @HomeBaseID >0                  
 BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive        
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor          
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.ClientID=@ClientID AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
ELSE IF @IsFixedRotary ='B' AND @IsInActive>0 And @ClientID=0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,       
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive        
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor          
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID     
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0                   
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive=0 And @ClientID>0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor                   
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Fleet.ClientID=@ClientID              
  Order By Fleet.AircraftCD             
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive=0 And @ClientID=0 And @HomeBaseID >0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor           
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD            
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive>0 And @ClientID>0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor                
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                           
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Fleet.ClientID=@ClientID                
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive=0 And @ClientID>0 And @HomeBaseID >0                
 BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor                
  FROM [Fleet]    
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Fleet.ClientID=@ClientID AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive>0 And @ClientID=0 And @HomeBaseID >0                
 BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor              
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary ='B' AND @IsInActive>0 And @ClientID>0 And @HomeBaseID >0                
 BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor                     
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Fleet.ClientID=@ClientID AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive=0 And @ClientID=0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor             
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Aircraft.IsFixedRotary =@IsFixedRotary                 
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive>0 And @ClientID=0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor              
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Aircraft.IsFixedRotary =@IsFixedRotary                
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive=0 And @ClientID>0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor              
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.ClientID=@ClientID                 
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive=0 And @ClientID=0 And @HomeBaseID >0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor              
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive>0 And @ClientID>0 And @HomeBaseID =0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor               
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.ClientID=@ClientID                 
  Order By Fleet.AircraftCD          
 END                     
                
ELSE IF @IsFixedRotary <>'B' AND @IsInActive>0 And @ClientID=0 And @HomeBaseID >0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor             
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND IsNull(Fleet.IsInActive,0)=0 AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
                  
ELSE IF @IsFixedRotary <>'B' AND @IsInActive=0 And @ClientID>0 And @HomeBaseID >0                 
BEGIN                  
  SELECT Fleet.FleetID, Fleet.CustomerID, Fleet.AircraftCD,Fleet.TailNum,Fleet.TypeDescription, Fleet.MaximumPassenger,                  
  Client.ClientCD,Vendor.VendorCD,Aircraft.AircraftCD As AirCraft_AircraftCD,Fleet.LastUpdUID,Fleet.LastUpdTS,        
  Case Fleet.IsInactive When 1 Then 'False' When 0 Then 'True' When NULL Then 'True' End As IsInactive    
  ,Fleet.HomebaseID,Airport.IcaoID,Airport.AirportID, Aircraft.PowerSettings1TrueAirSpeed,Aircraft.PowerSettings2TrueAirSpeed,Aircraft.PowerSettings3TrueAirSpeed,
  Fleet.ForeGrndCustomColor, Fleet.BackgroundCustomColor            
  FROM [Fleet]     
  LEFT OUTER JOIN Crew ON Fleet.CrewID = Crew.CrewID     
  LEFT OUTER JOIN Company ON Fleet.HomebaseID = Company.HomebaseID                           
  LEFT OUTER JOIN Client ON Fleet.ClientID = Client.ClientID     
  LEFT OUTER JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID     
  LEFT OUTER JOIN EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID     
  LEFT OUTER JOIN Vendor ON Fleet.VendorID = Vendor.VendorID      
  LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID                                
  WHERE Fleet.CustomerID=@CustomerID AND IsNull(Fleet.IsDeleted,0)=0                  
  AND Aircraft.IsFixedRotary =@IsFixedRotary AND Fleet.ClientID=@ClientID AND Fleet.HomebaseID=@HomeBaseID                  
  Order By Fleet.AircraftCD          
 END                     
             
END