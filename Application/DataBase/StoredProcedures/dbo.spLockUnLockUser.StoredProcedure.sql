
GO
/****** Object:  StoredProcedure [dbo].[spLockUnLockUser]    Script Date: 08/24/2012 10:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLockUnLockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLockUnLockUser]
go
  
CREATE PROC [dbo].[spLockUnLockUser]  
@UserName varchar(30),
@IsUserLock bit
AS  
BEGIN  
--UPDATE Customer set IsCustomerLock = 0 where CustomerID = @CustomerId  

UPDATE UserMaster SET IsUserLock = @IsUserLock, InvalidLoginCount = 0 WHERE UserName = @UserName
 
END