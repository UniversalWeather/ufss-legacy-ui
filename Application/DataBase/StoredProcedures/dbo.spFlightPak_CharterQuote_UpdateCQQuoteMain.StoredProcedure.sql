IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQQuoteMain]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQQuoteMain
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQQuoteMain
(
	@CQQuoteMainID bigint,
	@CustomerID bigint,
	@CQMainID bigint,
	@CompanyInformation varchar(MAX),
	@ReportHeader varchar(MAX),
	@ReportFooter varchar(MAX),
	@PageOff numeric(2, 0),
	@IsPrintDetail bit,
	@IsPrintFees bit,
	@IsPrintSum bit,
	@ImagePosition numeric(1, 0),
	@IsPrintQuoteID bit,
	@CQMessageID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FileWarehouseID bigint,
	@QuoteInformation varchar(MAX),
	@FileWarehouseID2 bigint,
	@FileWarehouseID3 bigint,
	@FileWarehouseID4 bigint
)
AS
	SET NOCOUNT ON;
	
	SET @LastUpdTS = GETUTCDATE()
	
UPDATE [CQQuoteMain] SET [CompanyInformation] = @CompanyInformation, [ReportHeader] = @ReportHeader,
	 [ReportFooter] = @ReportFooter, 
	[PageOff] = @PageOff, [IsPrintDetail] = @IsPrintDetail, [IsPrintFees] = @IsPrintFees, [IsPrintSum] = @IsPrintSum, [ImagePosition] = @ImagePosition, [IsPrintQuoteID] = @IsPrintQuoteID, [CQMessageID] = @CQMessageID, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, [FileWarehouseID] = @FileWarehouseID, [QuoteInformation] = @QuoteInformation, FileWarehouseID2 = @FileWarehouseID2
	, FileWarehouseID3 = @FileWarehouseID3, FileWarehouseID4 = @FileWarehouseID4
	WHERE [CQQuoteMainID] = @CQQuoteMainID AND [CustomerID] = @CustomerID
	
GO

