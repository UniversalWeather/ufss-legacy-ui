
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTravelCoordinator]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateTravelCoordinator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateTravelCoordinator]
go
CREATE Procedure [dbo].[spUpdateTravelCoordinator]  
 (@TravelCoordinatorID BIGINT,      
 @CustomerID BIGINT,      
 @TravelCoordCD char(5),        
 @FirstName varchar(30),        
 @MiddleName varchar(30),        
 @LastName varchar(30),        
 @PhoneNum varchar(25),        
 @FaxNum varchar(25),        
 @PagerNum varchar(25),        
 @CellPhoneNum varchar(25),        
 @EmailID varchar(100),        
 @Notes varchar(max),        
 @HomebaseID BIGINT,      
 @LastUpdUID varchar(30),        
 @LastUpdTS datetime,        
 @IsDeleted bit  
 ,@BusinessPhone Varchar(25)  
 ,@HomeFax Varchar(25)  
 ,@CellPhoneNum2 Varchar (25)  
 ,@OtherPhone Varchar(25)  
 ,@BusinessEmail Varchar(250)  
 ,@PersonalEmail Varchar(250)  
 ,@OtherEmail Varchar(250)  
 ,@Addr3 Varchar(40)
 ,@IsInActive bit)  
-- =============================================        
-- Author:RUKMINI M        
-- Create date: 26/04/2012        
-- Altered by Matehs on 03-07-12      
-- Description: Update the TravelCoordinator information        
-- =============================================        
as        
begin  
 SET NoCOUNT ON        
 SET @LastUpdTS = GETUTCDATE()    
   
 UPDATE [TravelCoordinator]  
 SET  CustomerID =@CustomerID        
   ,TravelCoordCD =@TravelCoordCD        
   ,FirstName =@FirstName         
   ,MiddleName =@MiddleName         
   ,LastName =@LastName        
   ,PhoneNum =@PhoneNum         
   ,FaxNum =@FaxNum         
   ,PagerNum =@PagerNum         
   ,CellPhoneNum=@CellPhoneNum        
   ,EmailID=@EmailID        
   ,Notes =@Notes         
   ,HomebaseID=@HomebaseID       
   ,LastUpdUID=@LastUpdUID        
   ,LastUpdTS=@LastUpdTS        
   ,IsDeleted=@IsDeleted       
   ,BusinessPhone=@BusinessPhone  
   ,HomeFax=@HomeFax  
   ,CellPhoneNum2=@CellPhoneNum2  
   ,OtherPhone=@OtherPhone  
   ,BusinessEmail=@BusinessEmail  
   ,PersonalEmail=@PersonalEmail  
   ,OtherEmail=@OtherEmail  
   ,Addr3=@Addr3    
   ,IsInActive=@IsInActive
 WHERE TravelCoordinatorID=@TravelCoordinatorID       
END 
GO
