GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckResetpasswordPeriod]') AND type in (N'P', N'PC'))
DROP PROC [dbo].[spCheckResetpasswordPeriod]
GO
  
CREATE PROC [dbo].[spCheckResetpasswordPeriod]  
 @EmailId  varchar(max),   
 @ReturnValue int output  
AS  
BEGIN  
  
DECLARE @UserName varchar(30)        
 SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId        
 
SET @ReturnValue = (Select count(*) from UserPassword where (ActiveEndDT >= GETUTCDATE() OR ActiveEndDT is NULL) AND  PWDStatus = 1 AND UserName = @UserName)
 
  
END  
GO