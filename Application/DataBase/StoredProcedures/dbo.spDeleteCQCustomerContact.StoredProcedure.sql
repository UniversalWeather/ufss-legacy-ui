
GO
/****** Object:  StoredProcedure [dbo].[spDeleteCQCustomerContact]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCQCustomerContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCQCustomerContact]
GO

CREATE PROCEDURE spDeleteCQCustomerContact
(@CQCustomerContactID bigint
,@CustomerID bigint
,@CQCustomerContactCD int
,@LastUpdUID varchar(30)
,@LastUpdTS datetime
,@IsDeleted bit)
AS BEGIN

	SET NOCOUNT OFF;	
	SET @LastUpdTS = GETUTCDATE()	
		
	UPDATE CQCustomerContact
	SET	LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
	WHERE CQCustomerContactID = @CQCustomerContactID
	
END