
GO
/****** Object:  StoredProcedure [dbo].[spDeleteSalesPerson]    Script Date: 02/25/2013 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteSalesPerson]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteSalesPerson]
go
CREATE PROCEDURE [dbo].[spDeleteSalesPerson]
(
@SalesPersonID BIGINT,
@CustomerID BIGINT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT
)
-- =============================================
-- Author: Ramesh. J
-- Create date: 25/02/2013
-- Description: Delete the Sales Person information. Set IsDeleted to false
-- =============================================
AS
BEGIN 
SET NOCOUNT ON
--Check if the record is not being used anywhere in the application before Delete
DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)        
SET @ENTITYNAME = N'SalesPerson'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @SalesPersonID, @RecordUsed OUTPUT
--End of Delete Check
  
  if (@RecordUsed <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 else
 Begin 
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
UPDATE [SalesPerson]
	SET 
		IsDeleted = @IsDeleted
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS           
WHERE CustomerID = @CustomerID and SalesPersonID = @SalesPersonID

END
END
GO
