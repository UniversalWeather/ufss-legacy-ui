

/****** Object:  StoredProcedure [dbo].[spGetAirportByCityName]    Script Date: 02/11/2013 17:40:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportByCityName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportByCityName]
GO


/****** Object:  StoredProcedure [dbo].[spGetAirportByCityName]    Script Date: 02/11/2013 17:40:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAirportByCityName](@CustomerID BIGINT, @CityName varchar(25))    
AS    
-- =============================================    
-- Author:   
-- Create date:     
-- Description: Get the AircraftType information    
-- =============================================    
SET NOCOUNT ON    
  
if Exists ( select * from Airport where CustomerID = @CustomerID and CityName = @CityName and IsDeleted =0)  
begin    
SELECT  A.[AirportID]  
        ,A.[IcaoID]  
        ,A.[CustomerID]  
  ,[CityName]  
  ,A.[StateName]  
  ,A.[CountryName]  
  ,A.[CountryID]  
  ,[AirportName]  
  ,[LatitudeDegree]  
  ,[LatitudeMinutes]  
  ,[LatitudeNorthSouth]  
  ,[LongitudeDegrees]  
  ,[LongitudeMinutes]  
  ,[LongitudeEastWest]  
  ,[MagneticVariance]  
  ,[VarianceEastWest]  
  ,[Elevation]  
  ,[LongestRunway]  
  ,[IsEntryPort]  
  ,[WindZone]  
  ,[IsUSTax]  
  ,[IsRural]  
  ,[IsAirport]  
  ,[IsHeliport]  
  ,[AirportInfo]  
  ,[OffsetToGMT]  
  ,[IsDayLightSaving]  
  ,[DayLightSavingStartDT]  
  ,[DaylLightSavingStartTM]  
  ,[DayLightSavingEndDT]  
  ,[DayLightSavingEndTM]  
  ,[TowerFreq]  
  ,[ATISFreq]  
  ,[ARINCFreq]  
  ,[GroundFreq]  
  ,[ApproachFreq]  
  ,[DepartFreq]  
  ,[Label1Freq]  
  ,[Freq1]  
  ,[Label2Freq]  
  ,[Freq2]  
  ,[TakeoffBIAS]  
  ,[LandingBIAS]  
  ,[Alerts]  
  ,[GeneralNotes]  
  ,[IsFlightPakFlag]  
  ,[FBOCnt]  
  ,[HotelCnt]  
  ,[TransportationCnt]  
  ,[CateringCnt]  
  ,[FlightPakUPD]  
  ,A.[MetroID]  
  ,A.[DSTRegionID]  
  ,[PPR]  
  ,[Iata]  
  ,[AlternateDestination1]  
  ,[AlternateDestination2]  
  ,[AlternateDestination3]  
  ,A.[LastUpdUID]  
  ,A.[LastUpdTS]  
  ,A.[IsInActive]  
  ,[RecordType]  
  ,[NewICAO]  
  ,[PreviousICAO]  
  ,A.[UpdateDT]  
  ,[IsWorldClock]  
  ,[WidthRunway]  
  ,[FssPhone]  
  ,[LengthWidthRunway]  
  ,[WidthLengthRunway]  
  ,[CustomLocation]  
  ,[CustomPhoneNum]  
  ,[ImmigrationPhoneNum]  
  ,[AgPhoneNum]  
  ,[IsCustomAvailable]  
  ,[PortEntryType]  
  ,[Unicom]  
  ,[Clearence1DEL]  
  ,[Clearence2DEL]  
  ,[AtisPhoneNum]  
  ,[ASOS]  
  ,[AsosPhoneNum]  
  ,[AWOS]  
  ,[AwosPhoneNum]  
  ,[AwosType]  
  ,[IsSlots]  
  ,[FssName]  
  ,[AirportManager]  
  ,[AirportManagerPhoneNum]  
  ,[FAA]  
  ,[UWAID]  
  ,[IsFIX]  
  ,[HoursOfOperation]  
  ,[LandingFeeSmall]  
  ,[LandingFeeMedium]  
  ,[LandingFeeLarge]  
  ,[IsEUETS]  
  ,A.[IsDeleted]  
  ,A.[UWAUpdates]  
  ,A.[RunwayID1]  
        ,A.[RunwayID2]  
        ,A.[RunwayID3]  
        ,A.[RunwayID4]  
        ,A.[Runway2Length]  
        ,A.[Runway2Width]  
        ,A.[Runway3Length]  
        ,A.[Runway3Width]  
        ,A.[Runway4Length]  
        ,A.[Runway4Width]    
  ,M.MetroCD as MetroCD  
  ,C.CountryCD as CountryCD  
  ,D.DSTRegionCD as DSTRegionCD  
  ,IsPublic  
  ,IsPrivate  
  ,IsMilitary  
  ,ExchangeRateID  
  ,CustomsSundayWorkHours  
  ,CustomsMondayWorkHours  
  ,CustomsTuesdayWorkHours  
  ,CustomsWednesdayWorkHours  
  ,CustomsThursdayWorkHours  
  ,CustomsFridayWorkHours  
  ,CustomsSaturdayWorkHours  
  ,CustomNotes      
  FROM  [Airport] A
  LEFT OUTER JOIN [Metro] M ON A.MetroID = M.MetroID
  LEFT OUTER JOIN [Country] C ON A.CountryID = C.CountryID
  LEFT OUTER JOIN [DSTRegion] D ON A.DSTRegionID = D.DSTRegionID
  where A.CustomerID = @CustomerID and A.CityName = @CityName and A.IsDeleted =0  
 end  
 else  
 begin  
   
 DECLARE @UWACustomerID BIGINT      
 SELECT @UWACustomerID  = dbo.GetUWACustomerID()  
   
 SELECT  A.[AirportID]  
        ,A.[IcaoID]  
        ,A.[CustomerID]  
  ,[CityName]  
  ,A.[StateName]  
  ,A.[CountryName]  
  ,A.[CountryID]  
  ,[AirportName]  
  ,[LatitudeDegree]  
  ,[LatitudeMinutes]  
  ,[LatitudeNorthSouth]  
  ,[LongitudeDegrees]  
  ,[LongitudeMinutes]  
  ,[LongitudeEastWest]  
  ,[MagneticVariance]  
  ,[VarianceEastWest]  
  ,[Elevation]  
  ,[LongestRunway]  
  ,[IsEntryPort]  
  ,[WindZone]  
  ,[IsUSTax]  
  ,[IsRural]  
  ,[IsAirport]  
  ,[IsHeliport]  
  ,[AirportInfo]  
  ,[OffsetToGMT]  
  ,[IsDayLightSaving]  
  ,[DayLightSavingStartDT]  
  ,[DaylLightSavingStartTM]  
  ,[DayLightSavingEndDT]  
  ,[DayLightSavingEndTM]  
  ,[TowerFreq]  
  ,[ATISFreq]  
  ,[ARINCFreq]  
  ,[GroundFreq]  
  ,[ApproachFreq]  
  ,[DepartFreq]  
  ,[Label1Freq]  
  ,[Freq1]  
  ,[Label2Freq]  
  ,[Freq2]  
  ,[TakeoffBIAS]  
  ,[LandingBIAS]  
  ,[Alerts]  
  ,[GeneralNotes]  
  ,[IsFlightPakFlag]  
  ,[FBOCnt]  
  ,[HotelCnt]  
  ,[TransportationCnt]  
  ,[CateringCnt]  
  ,[FlightPakUPD]  
  ,A.[MetroID]  
  ,A.[DSTRegionID]  
  ,[PPR]  
  ,[Iata]  
  ,[AlternateDestination1]  
  ,[AlternateDestination2]  
  ,[AlternateDestination3]  
  ,A.[LastUpdUID]  
  ,A.[LastUpdTS]  
  ,A.[IsInActive]  
  ,[RecordType]  
  ,[NewICAO]  
  ,[PreviousICAO]  
  ,A.[UpdateDT]  
  ,[IsWorldClock]  
  ,[WidthRunway]  
  ,[FssPhone]  
  ,[LengthWidthRunway]  
  ,[WidthLengthRunway]  
  ,[CustomLocation]  
  ,[CustomPhoneNum]  
  ,[ImmigrationPhoneNum]  
  ,[AgPhoneNum]  
  ,[IsCustomAvailable]  
  ,[PortEntryType]  
  ,[Unicom]  
  ,[Clearence1DEL]  
  ,[Clearence2DEL]  
  ,[AtisPhoneNum]  
  ,[ASOS]  
  ,[AsosPhoneNum]  
  ,[AWOS]  
  ,[AwosPhoneNum]  
  ,[AwosType]  
  ,[IsSlots]  
  ,[FssName]  
  ,[AirportManager]  
  ,[AirportManagerPhoneNum]  
  ,[FAA]  
  ,[UWAID]  
  ,[IsFIX]  
  ,[HoursOfOperation]  
  ,[LandingFeeSmall]  
  ,[LandingFeeMedium]  
  ,[LandingFeeLarge]  
  ,[IsEUETS]  
  ,A.[IsDeleted]  
  ,A.[UWAUpdates]  
  ,A.[RunwayID1]  
        ,A.[RunwayID2]  
        ,A.[RunwayID3]  
        ,A.[RunwayID4]  
        ,A.[Runway2Length]  
        ,A.[Runway2Width]  
        ,A.[Runway3Length]  
        ,A.[Runway3Width]  
        ,A.[Runway4Length]  
        ,A.[Runway4Width]    
  ,M.MetroCD as MetroCD  
  ,C.CountryCD as CountryCD  
  ,D.DSTRegionCD as DSTRegionCD  
  ,IsPublic  
  ,IsPrivate  
  ,IsMilitary  
  ,ExchangeRateID  
  ,CustomsSundayWorkHours  
  ,CustomsMondayWorkHours  
  ,CustomsTuesdayWorkHours  
  ,CustomsWednesdayWorkHours  
  ,CustomsThursdayWorkHours  
  ,CustomsFridayWorkHours  
  ,CustomsSaturdayWorkHours  
  ,CustomNotes    
    
  FROM  [Airport] A  
  LEFT OUTER JOIN [Metro] M ON A.MetroID = M.MetroID
  LEFT OUTER JOIN [Country] C ON A.CountryID = C.CountryID
  LEFT OUTER JOIN [DSTRegion] D ON A.DSTRegionID = D.DSTRegionID
  where  A.CustomerID = @UWACustomerID and A.CityName = @CityName and A.IsDeleted =0  
 end  
GO


