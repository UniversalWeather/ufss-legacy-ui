
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPreFlightMain]    Script Date: 03/01/2013 19:22:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddPreFlightMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddPreFlightMain]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPreFlightMain]    Script Date: 03/01/2013 19:22:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[spFlightPak_AddPreFlightMain](@CustomerID bigint
			,@TripNUM bigint
			,@PreviousNUM bigint
			,@TripDescription varchar(40)
			,@FleetID bigint
			,@DispatchNUM char(12)
			,@TripStatus char(1)
			,@EstDepartureDT date
			,@EstArrivalDT date
			,@RecordType char(1)
			,@AircraftID bigint
			,@PassengerRequestorID bigint
			,@RequestorFirstName varchar(63)
			,@RequestorMiddleName varchar(60)
			,@RequestorLastName varchar(60)
			,@RequestorPhoneNUM varchar(25)
			,@DepartmentDescription varchar(25)
			,@AuthorizationDescription varchar(25)
			,@RequestDT date
			,@IsCrew bit
			,@IsPassenger bit
			,@IsQuote bit
			,@IsScheduledServices bit
			,@IsAlert bit
			,@Notes text
			,@LogisticsHistory text
			,@HomebaseID bigint
			,@ClientID bigint
			,@IsAirportAlert bit
			,@IsLog bit
			,@BeginningGMTDT date
			,@EndingGMTDT date
			,@LastUpdUID char(30)
			,@LastUpdTS datetime
			,@IsPrivate bit
			,@IsCorpReq bit
			,@Acknowledge char(1)
			,@DispatchNotes text
			,@TripRequest char(1)
			,@WaitList char(1)
			,@FlightNUM char(12)
			,@FlightCost numeric(12, 2)
			,@AccountID bigint
			,@TripException text
			,@TripSheetNotes text
			,@CancelDescription varchar(60)
			,@IsCompleted bit
			,@IsFBOUpdFlag bit
			,@RevisionNUM numeric(3, 0)
			,@RevisionDescriptioin varchar(200)
			,@IsPersonal bit
			,@DsptnName varchar(60)
			,@ScheduleCalendarHistory text
			,@ScheduleCalendarUpdate datetime
			,@CommentsMessage text
			,@EstFuelQTY numeric(17, 3)
			,@CrewID bigint
			,@ReleasedBy varchar(60)
			,@IsDeleted bit
			,@VerifyNUM varchar(40)
			,@APISSubmit datetime
			,@APISStatus varchar(20)
			,@APISException text
			,@IsAPISValid bit
			,@DepartmentID bigint
			,@AuthorizationID bigint
			,@DispatcherUserName char(30)
			,@EmergencyContactID bigint
			,@FleetCalendarNotes varchar(max)
            ,@CrewCalendarNotes varchar(max)
            ,@FuelUnits numeric(1)
            ,@CQCustomerID bigint
			,@IsTripCopied bit
)    
AS    
BEGIN    
	DECLARE  @TripID BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @TripID OUTPUT    
	set @TripNUM = 0
	if (@RecordType ='T')
	BEGIN
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @TripNUM  OUTPUT  
		select @TripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID
	END


	if exists(select * from  UserMaster inner join Company on UserMaster.HomebaseID = Company.HomebaseID where UserMaster.UserName = @LastUpdUID and IsAutomaticDispatchNum = 1)
	begin
		set @DispatchNUM = cast(@TripNUM as varchar(12));
	end


INSERT INTO [PreflightMain]    
           ([TripID]
			,[CustomerID]
			,[TripNUM]
			,[PreviousNUM]
			,[TripDescription]
			,[FleetID]
			,[DispatchNUM]
			,[TripStatus]
			,[EstDepartureDT]
			,[EstArrivalDT]
			,[RecordType]
			,[AircraftID]
			,[PassengerRequestorID]
			,[RequestorFirstName]
			,[RequestorMiddleName]
			,[RequestorLastName]
			,[RequestorPhoneNUM]
			,[DepartmentDescription]
			,[AuthorizationDescription]
			,[RequestDT]
			,[IsCrew]
			,[IsPassenger]
			,[IsQuote]
			,[IsScheduledServices]
			,[IsAlert]
			,[Notes]
			,[LogisticsHistory]
			,[HomebaseID]
			,[ClientID]
			,[IsAirportAlert]
			,[IsLog]
			,[BeginningGMTDT]
			,[EndingGMTDT]
			,[LastUpdUID]
			,[LastUpdTS]
			,[IsPrivate]
			,[IsCorpReq]
			,[Acknowledge]
			,[DispatchNotes]
			,[TripRequest]
			,[WaitList]
			,[FlightNUM]
			,[FlightCost]
			,[AccountID]
			,[TripException]
			,[TripSheetNotes]
			,[CancelDescription]
			,[IsCompleted]
			,[IsFBOUpdFlag]
			,[RevisionNUM]
			,[RevisionDescriptioin]
			,[IsPersonal]
			,[DsptnName]
			,[ScheduleCalendarHistory]
			,[ScheduleCalendarUpdate]
			,[CommentsMessage]
			,[EstFuelQTY]
			,[CrewID]
			,[ReleasedBy]
			,[IsDeleted]
			,[VerifyNUM]
			,[APISSubmit]
			,[APISStatus]
			,[APISException]
			,[IsAPISValid]
			,[DepartmentID]
			,[AuthorizationID]
			,[DispatcherUserName]
			,[EmergencyContactID]
			,[FleetCalendarNotes]
			,[CrewCalendarNotes]
			,[FuelUnits]
			,[CQCustomerID]
			,[IsTripCopied]
)    
		 VALUES(@TripID
			,@CustomerID
			,@TripNUM
			,@PreviousNUM
			,@TripDescription
			,@FleetID
			,@DispatchNUM
			,@TripStatus
			,@EstDepartureDT
			,@EstArrivalDT
			,@RecordType
			,@AircraftID
			,@PassengerRequestorID
			,@RequestorFirstName
			,@RequestorMiddleName
			,@RequestorLastName
			,@RequestorPhoneNUM
			,@DepartmentDescription
			,@AuthorizationDescription
			,@RequestDT
			,@IsCrew
			,@IsPassenger
			,@IsQuote
			,@IsScheduledServices
			,@IsAlert
			,@Notes
			,@LogisticsHistory
			,@HomebaseID
			,@ClientID
			,@IsAirportAlert
			,@IsLog
			,@BeginningGMTDT
			,@EndingGMTDT
			,@LastUpdUID
			,@LastUpdTS
			,@IsPrivate
			,@IsCorpReq
			,@Acknowledge
			,@DispatchNotes
			,@TripRequest
			,@WaitList
			,@FlightNUM
			,@FlightCost
			,@AccountID
			,@TripException
			,@TripSheetNotes
			,@CancelDescription
			,@IsCompleted
			,@IsFBOUpdFlag
			,@RevisionNUM
			,@RevisionDescriptioin
			,@IsPersonal
			,@DsptnName
			,@ScheduleCalendarHistory
			,@ScheduleCalendarUpdate
			,@CommentsMessage
			,@EstFuelQTY
			,@CrewID
			,@ReleasedBy
			,@IsDeleted
			,@VerifyNUM
			,@APISSubmit
			,@APISStatus
			,@APISException
			,@IsAPISValid
			,@DepartmentID
			,@AuthorizationID
			,@DispatcherUserName
			,@EmergencyContactID
			,@FleetCalendarNotes
			,@CrewCalendarNotes
			,@FuelUnits
			,@CQCustomerID
			,@IsTripCopied
)  
           
           Select @TripID as TripID  
END



GO


