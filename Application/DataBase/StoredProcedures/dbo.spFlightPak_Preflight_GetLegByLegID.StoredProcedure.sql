

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetLegByLegID]    Script Date: 09/19/2013 13:30:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetLegByLegID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetLegByLegID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetLegByLegID]    Script Date: 09/19/2013 13:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create Procedure [dbo].[spFlightPak_Preflight_GetLegByLegID]
(
@CustomerID BIGINT,
@LegID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get PreflightLeg by Leg ID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PL.*
FROM PreflightMain PM , PreflightLeg PL
 where 
 PL.LegID = @LegID 
 and PM.TripID = PL.TripID
 and PM.CustomerID=@CustomerID and PM.IsDeleted=0


GO


