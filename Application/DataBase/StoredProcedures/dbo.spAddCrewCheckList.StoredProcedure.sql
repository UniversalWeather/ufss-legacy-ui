
GO
/****** Object:  StoredProcedure [dbo].[spAddCrewCheckList]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCrewCheckList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCrewCheckList]
go

CREATE procedure [dbo].[spAddCrewCheckList]
    (
     @CustomerID BIGINT,
     @CrewCheckCD char(3),
     @CrewChecklistDescription  varchar(25),
     @ClientID BIGINT,
     @LastUpdUID varchar(30),
     @LastUpdTS datetime,
     @IsScheduleCheck bit,
     @IsCrewCurrencyPlanner bit,
     @IsDeleted bit,
     @IsInActive bit
    )
   
AS    
 BEGIN 
 SET NoCOUNT ON
 DECLARE @CrewCheckID BIGINT
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CrewCheckID OUTPUT
  set @LastUpdTS = GETUTCDATE()
INSERT INTO [CrewCheckList] 
           ([CrewCheckID]
           ,[CustomerID]  
           ,[CrewCheckCD]  
           ,[CrewChecklistDescription]  
           ,[ClientID]  
           ,[LastUpdUID]  
           ,[LastUpdTS]  
           ,[IsScheduleCheck]   
           ,[IsCrewCurrencyPlanner]  
           ,[IsDeleted]
           ,[IsInActive])  
         
VALUES  
  
          ( @CrewCheckID
           ,@CustomerID   
           ,@CrewCheckCD  
           ,@CrewChecklistDescription  
           ,@ClientID  
           ,@LastUpdUID  
           ,@LastUpdTS  
           ,@IsScheduleCheck  
           ,@IsCrewCurrencyPlanner   
           ,@IsDeleted
           ,@IsInActive)  
  
END
GO
