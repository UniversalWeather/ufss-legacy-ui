

/****** Object:  StoredProcedure [dbo].[spUpdateFleetProfileDefinition]    Script Date: 04/23/2013 15:10:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFleetProfileDefinition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFleetProfileDefinition]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateFleetProfileDefinition]    Script Date: 04/23/2013 15:10:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[spUpdateFleetProfileDefinition]  
 (@FleetProfileInfoXRefID BIGINT,  
  @CustomerID BIGINT,  
  @FleetID BIGINT,  
  @FleetProfileInformationID  BIGINT,  
  @FleetDescription VARCHAR(25),  
  @InformationValue  VARCHAR(25),  
  @ClientID BIGINT,  
  @LastUpdUID varchar(30),  
  @LastUpdTS datetime,
  @IsPrint bit,  
  @IsDeleted bit  
  )    
-- =============================================    
-- Author:Mathes  
-- Create date: 24/7/2012     
-- Description: Update the FleetProfileDefinition information    
-- =============================================    
as    
begin     
SET NoCOUNT ON   
set @LastUpdTS = GETUTCDATE()   
UPDATE [FleetProfileDefinition]    
   SET      [CustomerID]=@CustomerID   
           ,[FleetID]=@FleetID   
           ,[FleetDescription]=@FleetDescription    
           ,[InformationValue]=@InformationValue    
           ,[ClientID]=@ClientID     
           ,[LastUpdUID]=@LastUpdUID    
           ,[LastUpdTS]=@LastUpdTS    
           ,[IsDeleted]=@IsDeleted
           ,IsPrintable =@IsPrint               
 WHERE FleetProfileInfoXRefID=@FleetProfileInfoXRefID  
    
end  
GO


