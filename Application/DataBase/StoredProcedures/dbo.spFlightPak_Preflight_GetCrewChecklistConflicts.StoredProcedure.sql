
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewChecklistConflicts]    Script Date: 02/12/2014 16:53:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetCrewChecklistConflicts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewChecklistConflicts]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetCrewChecklistConflicts]    Script Date: 02/12/2014 16:53:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--EXEC spFlightPak_Preflight_GetCrewChecklistConflicts '10/07/2012','10/07/2012',10001263953    
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetCrewChecklistConflicts]            
(     
 @CustomerID BIGINT,       
 @DepartureGreenwichDTTM DATETIME,        
 @ArrivalGreenwichDTTM DATETIME,        
 @CrewID BIGINT            
)            
AS            
BEGIN          
SET NOCOUNT ON;         
SELECT  CrewChecklistDetail.CheckListCD,CrewChecklist.CrewChecklistDescription,Crew.CrewCD,        
  CrewChecklistDetail.DueDT,CrewChecklistDetail.AlertDT,CrewChecklistDetail.GraceDT,CrewChecklist.IsScheduleCheck        
  FROM CrewChecklistDetail        
  INNER JOIN   CrewChecklist ON CrewChecklistDetail.CheckListCD = CrewChecklist.CrewCheckCD and CrewChecklist.CustomerID= CrewChecklistDetail.CustomerID    
  INNER JOIN   Crew ON CrewChecklistDetail.CrewID = Crew.CrewID and Crew.CustomerID=CrewChecklistDetail.CustomerID        
  WHERE CrewCheckListDetail.CrewID=@CrewID AND 
  CrewChecklistDetail.CustomerID=@CustomerID AND        
  ISNULL(CrewChecklistDetail.IsDeleted,0) = 0 AND         
  ISNULL(CrewChecklist.IsDeleted,0) = 0 AND         
  (
 (( CrewChecklistDetail.AlertDT  IS NOT NULL and CrewChecklistDetail.AlertDT  <= @DepartureGreenwichDTTM )OR      
  (CrewChecklistDetail.AlertDT  IS NOT NULL and CrewChecklistDetail.AlertDT  <= @ArrivalGreenwichDTTM)) 
  
  OR        
  ((CrewChecklistDetail.DueDT  IS NOT NULL and CrewChecklistDetail.DueDT <=  @DepartureGreenwichDTTM) OR        
  (CrewChecklistDetail.DueDT  IS NOT NULL and CrewChecklistDetail.DueDT <= @ArrivalGreenwichDTTM))   
  
  OR
    ((CrewChecklistDetail.GraceDT  IS NOT NULL and CrewChecklistDetail.GraceDT <= @DepartureGreenwichDTTM) OR        
  (CrewChecklistDetail.GraceDT  IS NOT NULL and CrewChecklistDetail.GraceDT <= @ArrivalGreenwichDTTM))       
  )
  AND         
  CrewChecklist.IsScheduleCheck = 1 AND          
  CrewChecklistDetail.IsInActive=0 AND        
  CrewChecklistDetail.IsNoConflictEvent=0      --IsNoConflictEvent=1 =>True   
END  


GO


