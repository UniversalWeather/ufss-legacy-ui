
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFleet]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFleetComponent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFleetComponent]
GO

CREATE procedure [dbo].[spGetAllFleetComponent](@CustomerID BIGINT)
as
SET NoCOUNT ON

BEGIN
SELECT      
[FleetComponentID]
,[CustomerID]
           ,[FleetID]
           ,[AircraftCD] 		    
		   ,[ComponentCD]
           ,[FleetComponentDescription]
           ,[LastInspectionDT]           
           ,[IsHrsDaysCycles]           
           ,[InspectionHrs]           
           ,[WarningHrs]           
           ,[SericalNum]           
           ,[LastUpdUID]           
           ,[LastUpdTS]           
           , [IsDeleted]
           ,IsInactive
  FROM  FleetComponent WHERE CustomerID=@CustomerID  AND IsNull(FleetComponent.IsDeleted,0)=0   
  Order BY FleetComponentDescription,LastInspectionDT
END
GO
