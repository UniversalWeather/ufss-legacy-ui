

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightCatering]    Script Date: 09/24/2013 00:02:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightCatering]
GO
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_UpdatePreflightCatering]    Script Date: 09/24/2013 00:02:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightCatering]    
 (      
        @PreflightCateringID bigint,    
        @LegID bigint,          
           @CustomerID bigint,    
           @AirportID bigint,    
           @ArriveDepart char,    
           @CateringID bigint,    
           @IsCompleted bit,    
           @Cost numeric(17,2),    
           @ContactName varchar(50),    
           @ContactPhone varchar(20),    
           @ContactFax varchar(20),    
           @CateringConfirmation varchar(max),    
           @CateringComments varchar(max),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @IsUWAArranger bit,    
           @CateringContactName varchar(40),    
           @Status varchar(20)     
    )    
AS    
BEGIN     
SET NOCOUNT OFF    
    
UPDATE [dbo].[PreflightCateringDetail]    
   SET [ArriveDepart] = @ArriveDepart     
      ,[IsCompleted]=@IsCompleted    
      ,[Cost] = @Cost    
      ,[ContactName] = @ContactName    
      ,[ContactPhone] = @ContactPhone    
      ,[ContactFax] = @ContactFax    
      ,[CateringConfirmation] = @CateringConfirmation    
      ,[CateringComments] = @CateringComments    
      ,[LastUpdUID] = @LastUpdUID    
      ,[LastUpdTS] = @LastUpdTS    
      ,[IsDeleted] = @IsDeleted    
      ,[IsUWAArranger]=@IsUWAArranger    
      ,[CateringContactName]=@CateringContactName    
   ,AirportID = @AirportID    
   ,CateringID = @CateringID      
   ,[Status] = @status    
 WHERE  PreflightCateringID=@PreflightCateringID AND LegID=@LegID     
     
END    



GO


