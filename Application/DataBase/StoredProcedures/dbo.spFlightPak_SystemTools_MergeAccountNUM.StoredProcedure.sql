
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeAccountNUM]    Script Date: 01/22/2013 12:41:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_MergeAccountNUM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_MergeAccountNUM]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergeAccountNUM]    Script Date: 01/22/2013 12:41:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_MergeAccountNUM]
(
@OldAccountID BIGINT,
@NewAccountID BIGINT,
@CustomerID BIGINT,
@LastUpdUID CHAR(30),
@OldAccountNum VARCHAR(32),
@NewAccountNum VARCHAR(32)
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	
	begin Try
			
			begin Transaction

	   	  --Company  
	   	  
	   	  UPDATE company set ChtQouteINTLSegCHGACCT = 
	   	   CASE WHEN ChtQouteINTLSegCHGACCT = @OldAccountNum THEN @NewAccountNum ELSE ChtQouteINTLSegCHGACCT END
                     ,FederalTax = CASE WHEN FederalTax = @OldAccountNum THEN @NewAccountNum ELSE FederalTax END
                     ,StateTax = CASE WHEN StateTax = @OldAccountNum THEN @NewAccountNum ELSE StateTax END
                     ,SaleTax = CASE WHEN SaleTax = @OldAccountNum THEN @NewAccountNum ELSE SaleTax END
                     ,ChtQouteDOMSegCHGACCT = CASE WHEN ChtQouteDOMSegCHGACCT = @OldAccountNum THEN @NewAccountNum ELSE ChtQouteDOMSegCHGACCT END
                     ,FederalACCT = CASE WHEN FederalACCT = @OldAccountNum THEN @NewAccountNum ELSE FederalACCT END
                     ,RuralAccountNum = CASE WHEN RuralAccountNum = @OldAccountNum THEN @NewAccountNum ELSE RuralAccountNum END
                     ,LastUpdUID = @LastUpdUID
                     ,LastUpdTS = GETUTCDATE()
              WHERE (ChtQouteINTLSegCHGACCT = @OldAccountNum
                     OR FederalTax = @OldAccountNum
                     OR StateTax = @OldAccountNum
                     OR SaleTax = @OldAccountNum
                     OR ChtQouteDOMSegCHGACCT = @OldAccountNum
                     OR FederalACCT = @OldAccountNum
                     OR RuralAccountNum = @OldAccountNum)
              and CustomerID=@CustomerID 
               
           -- Account  		 
			declare @OldCost numeric(17,2)	
			select @OldCost = Cost from Account where AccountID = @OldAccountID and CustomerID=@CustomerID
			set @OldCost = isnull(@OldCost,0)
			
			UPDATE Account SET Account.Cost = isnull(Account.Cost,0) + @OldCost
				,LastUpdUID=@LastUpdUID
				,LastUpdTS=GETUTCDATE()
			where Account.AccountID = @NewAccountID and CustomerID=@CustomerID
              
           -- Budget
		   UPDATE budget set IsDeleted=1 where accountid = @OldAccountID 
	       and fiscalyear in (select fiscalyear from budget where accountid = @NewAccountID) and IsDeleted=0
	       and CustomerID=@CustomerID
	       	             
	       
	       UPDATE budget set AccountID = @NewAccountID, 
	             accountnum = @NewAccountNum,
	             LastUpdUID = @LastUpdUID,
	             LastUpdTS = GETUTCDATE()
	   	   WHERE  CustomerID=@CustomerID and  AccountID = @OldAccountID and IsDeleted=0
	   	   
	   	   -- PostFlightexpense
          
	   	   UPDATE PostFlightexpense   
				SET AccountID =@NewAccountID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE  CustomerID=@CustomerID and AccountID=@OldAccountID
	      
	       --UPDATE company set ChtQouteINTLSegCHGACCT = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE ChtQouteINTLSegCHGACCT = @OldAccountNum and CustomerID=@CustomerID
	       
	       --UPDATE company set FederalTax = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE FederalTax = @OldAccountNum and CustomerID=@CustomerID
	       
	       --UPDATE company set StateTax = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE StateTax = @OldAccountNum and CustomerID=@CustomerID
	       
	       --UPDATE company set SaleTax = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE SaleTax = @OldAccountNum and CustomerID=@CustomerID
	       
	       	       
	       --UPDATE company set FederalACCT = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE FederalACCT = @OldAccountNum and CustomerID=@CustomerID
	       
	       --UPDATE company set RuralAccountNum = @NewAccountNum
			     --,LastUpdUID = @LastUpdUID
			     --,LastUpdTS = GETUTCDATE()
	       --WHERE RuralAccountNum = @OldAccountNum and CustomerID=@CustomerID
	        
	                  
	          
	      -- CQLegfee ( No Table available in DB)
	      -- FeeSchedule ( No Table available in DB)
	       
	-- //Arcrftcq	( No AccountID available in AircraftCharterRate)     
	--dbacflt 
	--a.	Dsacflt
	--b.	Ibacflt
	--c.	Isacflt
	--d.	Dbacpos
	--e.	Dsacpos
	--f.	Ibacpos
	--g.	Isacpos
	--h.	Dbacaddcw
	--i.	Dsacaddcw
	--j.	Ibacaddcw
	--k.	Isacaddcw
	--l.	Dbacaddron
	--m.	Dsacaddron
	--n.	Ibacaddron
	--o.	Isacaddron
	--p.	Dbacstdcw
	--q.	Dsacstdcw
	--r.	Ibacstdcw
	--s.	Isacstdcw
	--t.	Dbacwait
	--u.	Dsacwait
	--v.	Ibacwait
	--w.	Isacwait
	--x.	Dbaclnding
	--y.	Dsaclnding
	--z.	Ibaclnding
	--aa.	Isaclnding
	--bb.	Dbacaddfee
	--cc.	Ibacaddfee
	--dd.	Isacaddfee
 

	--//FleetCQ ( No table available in DB)
	--a.	Dsacflt
	--b.	Ibacflt
	--c.	Isacflt
	--d.	Dbacpos
	--e.	Dsacpos
	--f.	Ibacpos
	--g.	Isacpos
	--h.	Dbacaddcw
	--i.	Dsacaddcw
	--j.	Ibacaddcw
	--k.	Isacaddcw
	--l.	Dbacaddron
	--m.	Dsacaddron
	--n.	Ibacaddron
	--o.	Isacaddron
	--p.	Dbacstdcw
	--q.	Dsacstdcw
	--r.	Ibacstdcw
	--s.	Isacstdcw
	--t.	Dbacwait
	--u.	Dsacwait
	--v.	Ibacwait
	--w.	Isacwait
	--x.	Dbaclnding
	--y.	Dsaclnding
	--z.	Ibaclnding
	--aa.	Isaclnding
	--bb.	Dbacaddfee
	--cc.	Dsacaddfee
	--dd.	Ibacaddfee
	--ee.	Isacaddfee        
			
		           		   
		  --Account
		   UPDATE Account SET IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
		   WHERE AccountID = @OldAccountID
		   
		   
		commit transaction
		set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	
	select   @MergeSuccess as 'MergeSuccess'
END
GO


