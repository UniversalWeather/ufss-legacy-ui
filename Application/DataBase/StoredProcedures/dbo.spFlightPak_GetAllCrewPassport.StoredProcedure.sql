/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCrewPassport]    Script Date: 01/16/2013 19:12:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllCrewPassport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllCrewPassport]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllCrewPassport]    Script Date: 01/16/2013 19:12:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spFlightPak_GetAllCrewPassport]          
(@CrewID bigint,        
@PassengerRequestorID bigint ,        
@CustomerID bigint)        
AS          
BEGIN        
 SET NOCOUNT ON;          
SELECT CrewPassengerPassport.PassportID        
      ,CrewPassengerPassport.CrewID        
      ,CrewPassengerPassport.PassengerRequestorID        
      ,CrewPassengerPassport.CustomerID        
      ,CrewPassengerPassport.Choice        
      ,CrewPassengerPassport.IssueCity        
      ,CrewPassengerPassport.PassportNum        
      ,CrewPassengerPassport.PassportExpiryDT        
      ,CrewPassengerPassport.CountryID        
      ,CrewPassengerPassport.IsDefaultPassport        
      ,CrewPassengerPassport.PilotLicenseNum        
      ,CrewPassengerPassport.LastUpdUID        
      ,CrewPassengerPassport.LastUpdTS        
      ,CrewPassengerPassport.IssueDT        
      ,CrewPassengerPassport.IsDeleted        
      ,Country.CountryCD        
      ,Country.CountryName
      ,CrewPassengerPassport.IsInActive          
  FROM [CrewPassengerPassport]        
  LEFT OUTER JOIN Country ON Country.CountryID = CrewPassengerPassport.CountryID        
WHERE (CrewPassengerPassport.CrewID=@CrewID Or CrewPassengerPassport.PassengerRequestorID=@PassengerRequestorID)         
and CrewPassengerPassport.CustomerID=@CustomerID and CrewPassengerPassport.IsDeleted = 0        
        
        
END   
GO


