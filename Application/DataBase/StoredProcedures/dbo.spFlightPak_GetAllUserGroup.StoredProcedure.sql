/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllUserGroup]    Script Date: 08/29/2012 16:50:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllUserGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllUserGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_GetAllUserGroup]  
(  
@CustomerID bigint  
)  
AS  
---- =============================================  
---- Author:  Sujitha.v  
---- Create date: 18-may-2012  
---- Description:Get the UserGroup  
  
---- =============================================  
SET NOCOUNT ON;  
BEGIN  
SELECT   
    UserGroupID ,  
 UserGroupCD ,  
 UserGroupDescription ,  
 CustomerID ,  
 LastUpdUID,  
 LastUpdTS ,  
 IsDeleted,
 IsInactive             
FROM   UserGroup  
  where IsDeleted='false' AND CustomerID=@CustomerID  
  ORDER BY UserGroupCD ASC
End  
GO
