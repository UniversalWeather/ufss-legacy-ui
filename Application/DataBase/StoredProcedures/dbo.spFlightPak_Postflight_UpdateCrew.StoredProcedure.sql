
/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_UpdateCrew]    Script Date: 02/18/2014 11:50:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_UpdateCrew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_UpdateCrew]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Postflight_UpdateCrew]    Script Date: 02/18/2014 11:50:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Postflight_UpdateCrew]  
(  
 @PostflightCrewListID bigint,  
 @POLogID bigint,  
 @POLegID bigint,  
 @CustomerID bigint,  
 @CrewID bigint,  
 @CrewFirstName varchar(60),  
 @CrewMiddleName varchar(60),  
 @CrewLastName varchar(60),  
 @DutyTYPE char(1),  
 @TakeOffDay numeric(2, 0),  
 @TakeOffNight numeric(2, 0),  
 @LandingDay numeric(2, 0),  
 @LandingNight numeric(2, 0),  
 @ApproachPrecision numeric(2, 0),  
 @ApproachNonPrecision numeric(2, 0),  
 @Instrument numeric(7, 3),  
 @Night numeric(7, 3),  
 @RemainOverNight numeric(3, 0),  
 @BeginningDuty char(5),  
 @DutyEnd char(5),  
 @DutyHours numeric(12, 3),  
 @Specification1 bit,  
 @Sepcification2 bit,  
 @IsOverride bit,  
 @Discount bit,  
 @BlockHours numeric(12, 3),  
 @FlightHours numeric(12, 3),  
 @IsAugmentCrew bit,  
 @IsRemainOverNightOverride bit,  
 @Seat char(1),  
 @LastUpdUID char(30),  
 @LastUpdTS datetime,  
 @OutboundDTTM datetime,  
 @InboundDTTM datetime,  
 @CrewDutyStartTM datetime,  
 @CrewDutyEndTM datetime,  
 @ItsNew bit,  
 @Specification3 numeric(7, 3),  
 @Specification4 numeric(7, 3),  
 @DaysAway numeric(5, 0),  
 @IsDeleted bit ,
 @OrderNUM int 
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @LastUpdTS = GETUTCDATE()  
   
 UPDATE [PostflightCrew] 
 SET [POLegID] = @POLegID, [CrewID] = @CrewID, [CrewFirstName] = @CrewFirstName, [CrewMiddleName] = @CrewMiddleName, [CrewLastName] = @CrewLastName, [DutyTYPE] = @DutyTYPE, [TakeOffDay] = @TakeOffDay, [TakeOffNight] = @TakeOffNight
, [LandingDay] = @LandingDay, [LandingNight] = @LandingNight, [ApproachPrecision] = @ApproachPrecision, [ApproachNonPrecision] = @ApproachNonPrecision, [Instrument] = @Instrument, [Night] = @Night, [RemainOverNight] = @RemainOverNight, 
[BeginningDuty] = @BeginningDuty, [DutyEnd] = @DutyEnd, 
[DutyHours] = CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END , 
[Specification1] = @Specification1, [Sepcification2] = @Sepcification2, [IsOverride] = @IsOverride, [Discount] = @Discount, 
[BlockHours] = CASE WHEN @BlockHours <0 THEN 0 ELSE @BlockHours END, 
[FlightHours] = CASE WHEN @FlightHours <0 THEN 0 ELSE @FlightHours END , 
[IsAugmentCrew] = @IsAugmentCrew, [IsRemainOverNightOverride] = @IsRemainOverNightOverride, [Seat] = @Seat, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [OutboundDTTM] = @OutboundDTTM, [InboundDTTM] = @InboundDTTM, [CrewDutyStartTM] = @CrewDutyStartTM, 
[CrewDutyEndTM] = @CrewDutyEndTM, [ItsNew] = @ItsNew, [Specification3] = @Specification3, [Specification4] = @Specification4, [DaysAway] = @DaysAway, [IsDeleted] = @IsDeleted ,[OrderNUM]=@OrderNUM  
 WHERE [PostflightCrewListID] = @PostflightCrewListID AND [CustomerID] = @CustomerID  
END  

GO


