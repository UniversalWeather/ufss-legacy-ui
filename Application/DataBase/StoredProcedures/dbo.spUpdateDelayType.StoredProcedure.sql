
/****** Object:  StoredProcedure [dbo].[spUpdateDelayType]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateDelayType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateDelayType]
go

CREATE procedure [dbo].[spUpdateDelayType]
(
	 @DelayTypeID bigint
	,@CustomerID bigint
	,@DelayTypeCD char(2)
	,@DelayTypeDescription varchar(100)
	,@ClientID bigint
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
	,@IsSiflExcluded bit
	,@IsDeleted bit
	,@IsInActive bit
)
-- =============================================
-- Author:Sujitha.V
-- Create date: 12/4/2012
-- Description: Update the DelayType  information
-- =============================================
as
begin 
SET NoCOUNT ON
DECLARE @currentTime datetime  
SET @currentTime = GETUTCDATE()  
SET @LastUpdTS = @currentTime  
UPDATE [DelayType]
   SET 
            [DelayTypeCD]=@DelayTypeCD
           ,[DelayTypeDescription]=@DelayTypeDescription
           ,[ClientID]=@ClientID
           ,[LastUpdUID]=@LastUpdUID
           ,[LastUpdTS]=@LastUpdTS
           ,[IsSiflExcluded]=@IsSiflExcluded
           ,[IsDeleted]=@IsDeleted
           ,[IsInActive]=@IsInActive
 WHERE CustomerID =@CustomerID and DelayTypeID=@DelayTypeID

end
GO
