
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteAdditionalFees]    Script Date: 04/10/2013 12:36:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQQuoteAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteAdditionalFees]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteAdditionalFees]    Script Date: 04/10/2013 12:36:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQQuoteAdditionalFees]
(
	@CQQuoteAdditionalFeesID bigint,
	@CustomerID bigint,
	@CQQuoteMainID bigint,
	@IsPrintable bit,
	@CQQuoteFeeDescription varchar(60),
	@QuoteAmount numeric(17, 2),
	@OrderNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@Quantity numeric(17,3), 
	@ChargeUnit VARCHAR(25) 
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQQuoteAdditionalFees] SET [IsPrintable] = @IsPrintable, [CQQuoteFeeDescription] = @CQQuoteFeeDescription, [QuoteAmount] = @QuoteAmount, [OrderNUM] = @OrderNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, 
		Quantity = @Quantity, ChargeUnit = @ChargeUnit
		WHERE [CQQuoteAdditionalFeesID] = @CQQuoteAdditionalFeesID AND [CustomerID] = @CustomerID
	

GO


