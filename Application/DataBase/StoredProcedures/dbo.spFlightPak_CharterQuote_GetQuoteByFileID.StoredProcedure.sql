

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetQuoteByFileID]    Script Date: 04/24/2013 19:01:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_GetQuoteByFileID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_GetQuoteByFileID]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_GetQuoteByFileID]    Script Date: 04/24/2013 19:01:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_GetQuoteByFileID]
(
	@FileID BIGINT,
	@CustomerID BIGINT	
)
AS
SET NOCOUNT ON;
BEGIN	
	SELECT CM.CQMainID,
		   CM.QuoteNUM,
		   CM.CQSource,
		   V.VendorCD,
		   F.TailNum,
		   A.AircraftCD,		   
		   CM.FlightHoursTotal,		   
		   CM.FlightChargeTotal,
		   CM.IsFinancialWarning,
		   CF.FileNUM,
		   CM.LastAcceptDT,
		   PF.TripStatus,
		   PF.TripNUM,		   		   
		   CASE WHEN CM.IsAccepted=1 THEN 'ACCEPTED' 
		        WHEN CM.IsSubmitted=1 THEN 'SUBMITTED' END AS 'QuoteStatus',
		   CASE WHEN 
		   ( 
			SELECT count(1) 
			FROM CQException CQEXP 
			WHERE CM.CQMainID = CQEXP.CQMainID 
		   ) >0 THEN '!' 
		   ELSE '' 
		   END as FileExcep,
		   PO.LogNum

	FROM CQMain CM 
	INNER JOIN CQFile CF ON CM.CQFileID=CF.CQFileID
	LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID
	LEFT OUTER JOIN Fleet F ON F.FleetID=CM.FleetID
	LEFT OUTER JOIN Aircraft A ON A.AircraftID=CM.AircraftID
	LEFT OUTER JOIN PreflightMain PF ON PF.TripID=CM.TripID
	LEFT OUTER JOIN PostflightMain PO ON PO.TripID=CM.TripID
	WHERE CM.CQFileID=@FileID 
	AND CM.CustomerID=@CustomerID and CF.IsDeleted=0 and CM.IsDeleted=0
END
GO


