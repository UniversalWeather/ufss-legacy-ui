
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_InsertCQQuoteMain]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQQuoteMain
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_InsertCQQuoteMain
(
	@CustomerID bigint,
	@CQMainID bigint,
	@CompanyInformation varchar(MAX),
	@ReportHeader varchar(MAX),
	@ReportFooter varchar(MAX),
	@PageOff numeric(2, 0),
	@IsPrintDetail bit,
	@IsPrintFees bit,
	@IsPrintSum bit,
	@ImagePosition numeric(1, 0),
	@IsPrintQuoteID bit,
	@CQMessageID bigint,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FileWarehouseID bigint,
	@QuoteInformation varchar(MAX),
	@FileWarehouseID2 bigint,
	@FileWarehouseID3 bigint,
	@FileWarehouseID4 bigint
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	DECLARE @CQQuoteMainID  BIGINT    
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CharterQuoteCurrentNo',  @CQQuoteMainID OUTPUT 
	
	

	INSERT INTO [CQQuoteMain] ([CQQuoteMainID], [CustomerID], [CQMainID], [CompanyInformation], [ReportHeader], [ReportFooter], [PageOff], [IsPrintDetail], [IsPrintFees], [IsPrintSum], [ImagePosition], [IsPrintQuoteID], [CQMessageID], [LastUpdUID], [LastUpdTS], [IsDeleted], [FileWarehouseID], [QuoteInformation]
		,[FileWarehouseID2],[FileWarehouseID3],[FileWarehouseID4]
		) 
		VALUES (@CQQuoteMainID, @CustomerID, @CQMainID, @CompanyInformation, @ReportHeader, 
		@ReportFooter, 
		@PageOff, @IsPrintDetail, @IsPrintFees, @IsPrintSum, @ImagePosition, @IsPrintQuoteID, @CQMessageID, @LastUpdUID, @LastUpdTS, @IsDeleted, @FileWarehouseID, @QuoteInformation,
		@FileWarehouseID2, @FileWarehouseID3, @FileWarehouseID4
		);
	
	SELECT @CQQuoteMainID as CQQuoteMainID
GO

