
/****** Object:  StoredProcedure [dbo].[spAddLostBusiness]    Script Date: 20/02/2013 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddLostBusiness]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddLostBusiness]
go
CREATE Procedure [dbo].[spAddLostBusiness]  
(   
 @CustomerID bigint  
,@CQLostBusinessDescription varchar(100)  
,@LastUpdUID varchar(30)  
,@LastUpdTS datetime  
,@IsDeleted bit 
,@CQLostBusinessCD char(4)
)  
-- =============================================  
-- Author:Ramesh.J  
-- Create date: 20/02/2013  
-- Description: Insert the CQLostBusiness information  
-- Update:  Added CQLostBusinessCD on 07-Mar-2013
-- =============================================  
AS  
BEGIN   
SET NoCOUNT ON  
DECLARE @CQLostBusinessID BIGINT  
  
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @CQLostBusinessID OUTPUT  
DECLARE @currentTime datetime    
SET @currentTime = GETUTCDATE()    
SET @LastUpdTS = @currentTime    
  
   INSERT INTO [CQLostBusiness]  
           (  
    CQLostBusinessID  
   ,CustomerID     
   ,CQLostBusinessDescription     
   ,LastUpdUID   
   ,LastUpdTS               
   ,IsDeleted 
   ,CQLostBusinessCD    
           )  
     VALUES  
    (  
    @CQLostBusinessID        
   ,@CustomerID      
   ,@CQLostBusinessDescription     
   ,@LastUpdUID  
   ,@LastUpdTS      
   ,@IsDeleted
   ,@CQLostBusinessCD            
   )  
 END  