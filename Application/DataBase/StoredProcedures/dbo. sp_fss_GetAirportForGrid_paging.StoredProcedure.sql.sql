
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD]    Script Date: 10/08/2013 10:32:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAirportForGrid_paging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAirportForGrid_paging]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetAirportForGrid_paging]    Script Date: 14/05/2014 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO        
                  
CREATE PROCEDURE [dbo].[sp_fss_GetAirportForGrid_paging]          
(            
  @CustomerID BIGINT             
 ,@AirportID BIGINT            
 ,@ICAOID char(4)             
 ,@FetchRunwayOnly BIGINT=0            
 ,@FetchActiveOnly BIT  ,          
  @StartRowIndex int,          
  @PageSize int,          
  @filterValue varchar(max),    
  @SortExpression varchar(100),    
  @SortOrder varchar(50),    
  @SelectedICAOID char(4)          
)             
AS            
-- =============================================                        
-- Author: Sridhar                       
-- Create date: 27/02/2014                        
-- Description: Get All AirportForGrid details for Popup                  
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,0,20,20 ,'¿CZML¿ZMH¿108 MILE¿BRITISH COLUMBIA¿CA¿108 MILE¿5293¿-8.00¿'          
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,0,20,20 ,'oa¿¿108 MILE¿¿¿¿¿¿'          
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,0,20,20 ,'¿OA¿¿¿¿¿¿¿¿'          
-- Exec [sp_fss_GetAirportForGrid_paging_sort_filter] 10099,0,'',0,0,20,20 ,'','',''           
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,1            
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,1009943868,'',0,1            
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'KDAL',0,1            
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',10000,0            
-- Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',10000,1            
    
--Exec [sp_fss_GetAirportForGrid_paging_sort_filter] 10099,0,'',0,0,1,10 ,'','CityName','DESC',''            
            
--Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,0,0,3 ,'([IcaoID] LIKE ''kh%'') AND ([CityName] LIKE ''%As%'')','','','KHOU'          
               
       --Exec [sp_fss_GetAirportForGrid_paging] 10099,0,'',0,0,0,3 ,'','','','KHOU'      
-- =============================================                      
            
SET NOCOUNT ON             
IF LEN(@CustomerID) > 0                          
BEGIN                       
DECLARE @UWACustomerID BIGINT                      
SELECT @UWACustomerID  = dbo.GetUWACustomerID()            
Declare @Count int           
          
      
           SELECT AirportID INTO #tempcount          
       FROM          
         (SELECT A.AirportID          
          FROM Airport A          
          LEFT JOIN Country C ON C.CountryID = A.CountryID          
          WHERE A.CustomerID = @CustomerID          
         AND A.AirportID = CASE WHEN @AirportID <> 0 THEN @AirportID ELSE A.AirportID END          
         AND A.ICAOID = CASE WHEN Ltrim(Rtrim(@ICAOID)) <> '' THEN @ICAOID ELSE A.ICAOID END          
         AND ISNULL(LongestRunway,0) >= @FetchRunwayOnly          
         AND ISNULL(A.IsInActive,0) = CASE WHEN @FetchActiveOnly = 1 THEN ISNULL(A.IsInActive,0) ELSE 0 END          
         AND ISNULL(A.IsDeleted,0) = 0          
          UNION ALL SELECT A.AirportID          
          FROM Airport A          
          LEFT JOIN Country C ON C.CountryID = A.CountryID          
          WHERE A.CustomerID <> @CustomerID          
         AND A.CustomerID = @UWACustomerID          
         AND A.AirportID = CASE WHEN @AirportID <> 0 THEN @AirportID ELSE A.AirportID END          
         AND A.ICAOID = CASE WHEN Ltrim(Rtrim(@ICAOID)) <> '' THEN @ICAOID ELSE A.ICAOID END          
         AND ISNULL(LongestRunway,0) >= @FetchRunwayOnly          
         AND ISNULL(A.IsDeleted,0) = 0          
         AND ISNULL(A.IsInActive,0) = CASE WHEN @FetchActiveOnly = 1 THEN ISNULL(A.IsInActive,0) ELSE 0 END          
         AND IcaoID NOT IN          
           (SELECT DISTINCT IcaoID          
         FROM Airport          
         WHERE CustomerID = @CustomerID          
           AND ISNULL(IsDeleted,0) = 0)) AS tabtlet    
           
       SELECT @COUNT = COUNT(*)  FROM #tempcount  ;     
       declare @rowPage int = @startRowIndex + @pageSize - 1        
       declare @finalSortExpression varchar(100)=''    
     
      -- order by     
        if (@SortExpression = '')    
  SET @finalSortExpression = 'AirportID'      
  else    
  SET @finalSortExpression = @SortExpression +' '+@SortOrder    
      
          
           
           
     declare @MainQuery varchar(max)=  'SELECT ROW_NUMBER() OVER (          
               ORDER BY '+@finalSortExpression+') AS ROW ,          
           AirportID,          
           CustomerID,          
           IcaoID,          
           CityName,          
           StateName,          
           CountryID,          
           CountryCD,          
           AirportName,          
           OffsetToGMT,          
           IsEntryPort,          
           IsInactive,          
           LastUpdUID,          
           LastUpdTS,          
           IsWorldClock,          
           Filter,          
           UWAID,          
           Iata,          
           MaxRunway,          
           CountryName INTO #TEMPAIRPORT    
            
       FROM          
         (SELECT A.AirportID,          
           A.CustomerID,          
           A.IcaoID,          
           A.CityName,          
         A.StateName,          
           A.CountryID,          
           C.CountryCD,          
           A.AirportName,          
           A.OffsetToGMT,          
           A.IsEntryPort,          
           ISNULL(A.IsInactive,0) IsInactive ,          
                A.LastUpdUID,          
                A.LastUpdTS,          
                A.IsWorldClock,          
                (SELECT(CASE WHEN (A.UWAID IS NULL          
                    OR A.UWAID = ''''          
                    OR A.UWAID = ''&nbsp;'') THEN ''CUSTOM'' ELSE ''UWA'' END)) AS Filter,          
                A.UWAID,          
                A.Iata,          
                isnull(LongestRunway,0) AS [MaxRunway],          
                C.CountryName --(SELECT Max(v)          
        --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length) ,(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]          
          
          FROM Airport A          
          LEFT JOIN Country C ON C.CountryID = A.CountryID          
          WHERE A.CustomerID = '+Convert(varchar,@CustomerID) +'          
         AND A.AirportID = CASE WHEN '+Convert(varchar,@AirportID)+' <> 0 THEN '+Convert(varchar,@AirportID)+' ELSE A.AirportID END          
         AND A.ICAOID = CASE WHEN Ltrim(Rtrim('''+@ICAOID+''')) <> '''' THEN '''+@ICAOID+''' ELSE A.ICAOID END          
         AND ISNULL(LongestRunway,0) >= '+Convert(varchar,@FetchRunwayOnly) +'         
         AND ISNULL(A.IsInActive,0) = CASE WHEN '+Convert(varchar,@FetchActiveOnly)+' = 1 THEN ISNULL(A.IsInActive,0) ELSE 0 END          
         AND ISNULL(A.IsDeleted,0) = 0          
          UNION ALL SELECT A.AirportID,          
            A.CustomerID,          
            A.IcaoID,          
            A.CityName,          
            A.StateName,          
            A.CountryID,          
            C.CountryCD,          
            A.AirportName,          
            A.OffsetToGMT,          
            A.IsEntryPort,          
            ISNULL(A.IsInactive,0) IsInactive ,          
                    A.LastUpdUID,          
                    A.LastUpdTS,          
                    A.IsWorldClock,          
                    (SELECT(CASE WHEN (A.UWAID IS NULL          
                        OR A.UWAID = ''''          
                        OR A.UWAID = ''&nbsp;'') THEN ''CUSTOM'' ELSE ''UWA'' END)) AS Filter,          
                    A.UWAID,          
                    A.Iata,          
                    isnull(LongestRunway,0) AS [MaxRunway],          
                    C.CountryName --(SELECT Max(v)          
        --  FROM (VALUES (A.WidthLengthRunway), (A.Runway2Length),(Runway3Length), (Runway4Length)) AS value(v)) as [MaxRunway]          
          
          FROM Airport A          
          LEFT JOIN Country C ON C.CountryID = A.CountryID          
          WHERE A.CustomerID <> '+Convert(varchar,@CustomerID) +'        
         AND A.CustomerID = '+Convert(varchar,@UWACustomerID) +'          
         AND A.AirportID = CASE WHEN '+Convert(varchar,@AirportID )+' <> 0 THEN '+Convert(varchar,@AirportID) +' ELSE A.AirportID END          
         AND A.ICAOID = CASE WHEN Ltrim(Rtrim('''+@ICAOID +''')) <> '''' THEN '''+@ICAOID+''' ELSE A.ICAOID END          
         AND ISNULL(LongestRunway,0) >= '+Convert(varchar,@FetchRunwayOnly) +'         
         AND ISNULL(A.IsDeleted,0) = 0          
         AND ISNULL(A.IsInActive,0) = CASE WHEN '+Convert(varchar,@FetchActiveOnly)+' = 1 THEN ISNULL(A.IsInActive,0) ELSE 0 END          
         AND IcaoID NOT IN          
           (SELECT DISTINCT IcaoID          
         FROM Airport          
         WHERE CustomerID = '+Convert(varchar,@CustomerID) +'          
           AND ISNULL(IsDeleted,0) = 0)) AS tabtlet'     
             
     
        
 IF ((@filterValue='' AND @SortExpression !='') OR (@filterValue='' AND @SortExpression ='') )       
 BEGIN            
 -- No Filters / (with/without) Sorting     
   declare @sqlquery varchar(max)=''    
   IF @StartRowIndex =0    
  BEGIN    
  IF (@SelectedICAOID = '')      
   SET @sqlquery =' SELECT  * ,'+Convert(varchar,@COUNT) +' AS TotalCount  FROM #TEMPAIRPORT where (ROW between ('+Convert(varchar,@startRowIndex)+') AND '+Convert(varchar,@rowPage) +')'     
   ELSE    
   SET @sqlquery =' SELECT ROW,AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,CountryCD,AirportName,OffsetToGMT,IsEntryPort,IsInactive,LastUpdUID,LastUpdTS,IsWorldClock,Filter,UWAID,Iata,MaxRunway,CountryName ,'+Convert(varchar,@COUNT) +'
    AS TotalCount  FROM #TEMPAIRPORT WHERE IcaoID='''+@SelectedICAOID +'''  UNION   SELECT  ROW,AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,CountryCD,AirportName,OffsetToGMT,IsEntryPort,IsInactive,LastUpdUID,LastUpdTS,IsWorldClock,Filter,UWAID,Iata,MaxRunway,CountryName,'+Convert(varchar,@COUNT) +' AS TotalCount  FROM #TEMPAIRPORT where (ROW between ('+Convert(varchar,@startRowIndex)+') AND '+Convert(varchar,@rowPage) +')'     
  END    
 ELSE    
  BEGIN    
   SET @sqlquery =' SELECT  *,'+Convert(varchar,@COUNT) +' AS TotalCount  FROM #TEMPAIRPORT where (ROW between ('+Convert(varchar,@startRowIndex)+') AND '+Convert(varchar,@rowPage) +')'     
  END    
  PRINT @MainQuery + @sqlquery      
   EXEC (@MainQuery +@sqlquery )    
       
 END          
              
 ELSE IF (@filterValue! ='' AND @SortExpression='')             
  BEGIN     
 -- with Sorting only     
     
  declare @sqlquerywithfilter varchar(max)=' SELECT ROW_NUMBER() OVER (order by AirportID) as FilterRow,  * into #TEMP_AIRPORT_FILTER FROM #TEMPAIRPORT where '+@filterValue      
  declare @sqlquerywithfilterCount varchar(max)='Declare @Countfilter int    SELECT @Countfilter = count(*)  FROM #TEMP_AIRPORT_FILTER where '+@filterValue      
  declare @sqlquerywithfilterResult varchar(max)=' SELECT *,@Countfilter AS TotalCount  FROM #TEMP_AIRPORT_FILTER where (FilterRow between ('+Convert(varchar,@startRowIndex)+') AND '+Convert(varchar,@rowPage) +')'     
  exec (@MainQuery + @sqlquerywithfilter + @sqlquerywithfilterCount+ @sqlquerywithfilterResult )    
      
  END    
  ELSE IF (@filterValue! ='' AND @SortExpression != '')             
  BEGIN    
   -- with Sorting Filters    
   declare @sqlquerywithfilterSort varchar(max)=' SELECT ROW_NUMBER() OVER (order by '+ @finalSortExpression +') as FilterRow,  * into #TEMP_AIRPORT_FILTER FROM #TEMPAIRPORT where '+@filterValue      
   declare @sqlquerywithfiltersortCount varchar(max)='Declare @Countfiltersort int    SELECT @Countfiltersort = count(*)  FROM #TEMP_AIRPORT_FILTER where '+@filterValue      
   declare @sqlquerywithfilterSortResult varchar(max)=' SELECT *,@Countfiltersort AS TotalCount FROM #TEMP_AIRPORT_FILTER where (FilterRow between ('+Convert(varchar,@startRowIndex)+') AND '+Convert(varchar,@rowPage) +')'     
   exec (@MainQuery + @sqlquerywithfilterSort + @sqlquerywithfiltersortCount +  @sqlquerywithfilterSortResult )    
  END    
      
END          
              