
GO
/****** Object:  StoredProcedure [dbo].[spAddFleetGroup]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddFleetGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddFleetGroup]
GO
CREATE procedure [dbo].[spAddFleetGroup]
                    (
                      @CustomerID BIGINT,
                      @FleetGroupCD char(4),
                      @FleetGroupDescription  varchar(30),
                      @LastUpdUID varchar(30),
                      @LastUpdTS datetime,
                      @HomebaseID BIGINT,
                      @IsDeleted bit,
                      @IsInActive bit)  
  
  
AS  
BEGIN   
SET NoCOUNT ON  
 
  DECLARE  @FleetGroupID BIGINT 
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @FleetGroupID OUTPUT 
  
INSERT INTO [FleetGroup]    
           ([FleetGroupID]
           ,[CustomerID]    
           ,[FleetGroupCD]
           ,[FleetGroupDescription]  
           ,[LastUpdUID]
           ,[LastUpdTS] 
           ,[HomebaseID]  
           ,[IsDeleted]
           ,[IsInActive])    
     VALUES  
           (@FleetGroupID
           ,@CustomerID    
           ,@FleetGroupCD 
           ,@FleetGroupDescription  
           ,@LastUpdUID  
           ,@LastUpdTS  
           ,@HomebaseID  
           ,@IsDeleted
           ,@IsInActive)  
  
end
GO
