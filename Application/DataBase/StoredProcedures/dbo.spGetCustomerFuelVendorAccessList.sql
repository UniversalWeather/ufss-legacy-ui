IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCustomerFuelVendorAccessList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCustomerFuelVendorAccessList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCustomerFuelVendorAccessList]  
	@CustomerID Bigint
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetCustomerFuelVendorAccessList
-- Author: Ajeet Singh
-- Create date: 17 Oct. 2015  
-- Description: Get Fuel vendor access list for the Customer
-- ================================================================================  
SET NOCOUNT ON     
SELECT * 
FROM(SELECT FVA.CustomerFuelVendorAccessID,FVA.CustomerID,FV.FuelVendorID,FV.VendorCD,FV.VendorName,FVA.CanAccess,FVA.LastUpdUID,FVA.LastUpdTS,FVA.IsDeleted,FVA.IsInActive,FV.IsDeleted as DeletedFuelVendor
	FROM FuelVendor FV LEFT JOIN CustomerFuelVendorAccess FVA ON FV.FuelVendorID=FVA.FuelVendorID AND FVA.CustomerID=@CustomerID AND FV.IsDeleted=0) as fuelVendorList
	WHERE DeletedFuelVendor=0
END
GO


