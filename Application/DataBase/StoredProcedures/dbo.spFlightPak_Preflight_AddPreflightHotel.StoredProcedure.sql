
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightHotel]    Script Date: 04/01/2013 16:10:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotel]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightHotel]    Script Date: 04/01/2013 16:10:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotel]    
 (     
              
            @PreflightHotelName varchar(60)    
           ,@PreflightCrewListID bigint    
           ,@HotelID bigint    
           ,@AirportID bigint              
           ,@RoomType varchar(60)    
           ,@RoomRent numeric(6,2)    
           ,@RoomConfirm text    
           ,@RoomDescription text    
           ,@Street varchar(50)    
           ,@CityName varchar(20)    
           ,@StateName varchar(20)    
           ,@PostalZipCD char(20)    
           ,@PhoneNum1 varchar(25)    
           ,@PhoneNum2 varchar(25)    
           ,@PhoneNum3 varchar(25)    
           ,@PhoneNum4 varchar(25)    
           ,@FaxNUM varchar(25)    
           ,@LastUpdUID char(30)    
           ,@LastUpdTS datetime    
           ,@IsDeleted bit             
           ,@HotelArrangedBy varchar(10)    
           ,@Rate numeric(17,2)    
           ,@DateIn date    
           ,@DateOut date    
           ,@IsSmoking bit    
           ,@RoomTypeID bigint    
           ,@NoofBeds numeric(3,0)    
           ,@IsAllCrew bit    
           ,@IsCompleted bit    
           ,@IsEarlyCheckIn bit    
           ,@EarlyCheckInOption char(1)    
           ,@SpecialInstructions varchar(100)    
           ,@ConfirmationStatus  varchar(max)    
           ,@Comments varchar(max)    
           ,@CustomerID BIGINT              
     ,@Address1 varchar(40)    
     ,@Address2 varchar(40)    
     ,@Address3 varchar(40)    
     ,@MetroID bigint    
     ,@City varchar(30)    
     ,@StateProvince varchar(25)    
     ,@CountryID bigint    
     ,@PostalCode varchar(15)    
     ,@Status varchar(20)     
   )    
AS    
BEGIN     
      
  DECLARE @PreflightCrewHotelListID BIGINT       
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo', @PreflightCrewHotelListID  OUTPUT    
    
  --DECLARE @HotelListCount INT     
      
      
  --IF (@ApplyToAll = 1)     
  --BEGIN    
  -- SELECT @HotelListCount = COUNT(1) FROM PreflightCrewList     
  -- WHERE CustomerID=@CustomerID AND LegID = @LegID    
  --END    
  --ELSE    
  --BEGIN    
  -- SET @HotelListCount = 0    
  --END    
    
  --IF (@HotelListCount = 0)    
  --BEGIN    
  INSERT INTO [dbo].[PreflightCrewHotelList]    
       ([PreflightCrewHotelListID]    
       ,[PreflightHotelName]    
       ,[PreflightCrewListID]    
       ,[HotelID]    
       ,[AirportID]         
       ,[RoomType]    
       ,[RoomRent]    
       ,[RoomConfirm]    
       ,[RoomDescription]    
       ,[Street]    
       ,[CityName]    
       ,[StateName]    
       ,[PostalZipCD]    
       ,[PhoneNum1]    
       ,[PhoneNum2]    
       ,[PhoneNum3]    
       ,[PhoneNum4]    
       ,[FaxNUM]    
       ,[LastUpdUID]    
       ,[LastUpdTS]    
       ,[IsDeleted]    
       ,[HotelArrangedBy]    
       ,[Rate]    
       ,[DateIn]    
       ,[DateOut]    
       ,[IsSmoking]    
       ,[RoomTypeID]    
       ,[NoofBeds]    
       ,[IsAllCrew]    
       ,[IsCompleted]    
       ,[IsEarlyCheckIn]    
       ,[EarlyCheckInOption]    
       ,[SpecialInstructions]    
       ,[ConfirmationStatus]    
       ,[Comments]    
       ,[CustomerID]         
       ,[Address1]    
       ,[Address2]    
       ,[Address3]    
       ,[MetroID]    
       ,[City]    
       ,[StateProvince]    
       ,[CountryID]    
       ,[PostalCode]    
       ,[Status]    
       )    
    VALUES    
       (    
     @PreflightCrewHotelListID    
       ,@PreflightHotelName    
       ,@PreflightCrewListID    
       ,@HotelID     
       ,@AirportID        
       ,@RoomType    
       ,@RoomRent     
       ,@RoomConfirm     
       ,@RoomDescription     
       ,@Street    
       ,@CityName     
       ,@StateName    
       ,@PostalZipCD     
       ,@PhoneNum1     
       ,@PhoneNum2     
       ,@PhoneNum3     
       ,@PhoneNum4     
       ,@FaxNUM     
       ,@LastUpdUID     
       ,@LastUpdTS     
       ,@IsDeleted      
       ,@HotelArrangedBy    
       ,@Rate    
       ,@DateIn    
       ,@DateOut    
     ,@IsSmoking    
       ,@RoomTypeID    
       ,@NoofBeds    
       ,@IsAllCrew    
       ,@IsCompleted    
       ,@IsEarlyCheckIn    
       ,@EarlyCheckInOption    
       ,@SpecialInstructions    
       ,@ConfirmationStatus    
       ,@Comments    
       ,@CustomerID        
       ,@Address1    
       ,@Address2    
       ,@Address3    
       ,@MetroID    
       ,@City    
       ,@StateProvince    
       ,@CountryID    
       ,@PostalCode    
       ,@Status    
       )    
            
         Select @PreflightCrewHotelListID as PreflightCrewHotelListID      
  --   Update PreflightCrewList Set PreflightHotelID =  @PreflightHotelID where CustomerID=@CustomerID AND    
  --   LegID = @LegID AND CrewID = @CrewID      
  --END     
             
     
END    
    
    



GO


