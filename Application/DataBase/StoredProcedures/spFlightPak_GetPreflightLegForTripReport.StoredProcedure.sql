/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPreflightLegForTripReport]    Script Date: 10/03/2013 21:02:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetPreflightLegForTripReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetPreflightLegForTripReport]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetPreflightLegForTripReport]    Script Date: 10/03/2013 21:02:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetPreflightLegForTripReport]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID AS VARCHAR(50), --BIGINT is not supported by SSRS
		@LegNum AS VARCHAR(50) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spFlightPak_GetPreflightLegForTrip
-- Author: Anil Singh
-- Create date: 24 Nov 2014
-- Description: Get Preflight Tripsheet Leg Details
-- Revision History
-- Date		Name		Ver		Change
-- =============================================



      SELECT
			[LegId] = CONVERT(VARCHAR,PL.LegID)
			,[FuelBurn] = ISNULL(dbo.FuelBurn(@UserCD,AR.AircraftCD,PL.ElapseTM,PL.PowerSetting),0)
			,MinFuel = ISNULL(F.MinimumFuel,0)
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN  Airport AD ON PL.DepartICAOID = AD.AirportID --AND PM.CustomerID = A.CustomerID
			INNER JOIN  Airport AA ON PL.ArriveICAOID = AA.AirportID --AND PM.CustomerID = AA.CustomerID
			LEFT OUTER JOIN Aircraft AR ON PM.AircraftID = AR.AircraftID
			LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID
			-- ADDED FOR ZULU CALCULATION
			LEFT JOIN DSTRegion DDST ON AD.DSTRegionID = DDST.DSTRegionID
			LEFT JOIN DSTRegion ADST ON AA.DSTRegionID = ADST.DSTRegionID
			
			LEFT OUTER JOIN (
				SELECT CountryCD, CountryID FROM Country
			) DC ON AD.CountryID = DC.CountryID 
			
			LEFT OUTER JOIN (
				SELECT CountryCD, CountryID FROM Country
			) AC ON AA.CountryID = AC.CountryID
			
			LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID = P.PassengerRequestorID
			LEFT OUTER JOIN Department D ON PL.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN DepartmentAuthorization DA ON PL.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
			
			--LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID
			--LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID AND PFL.IsArrivalFBO = 1
			--LEFT OUTER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID AND PFL.IsDepartureFBO = 1
			
			LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
			
			LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
			
			LEFT OUTER JOIN (
                  SELECT DISTINCT PC2.LegID, Crewnames = (
                        SELECT ISNULL(PC1.CrewLastName + ', ','')
                        FROM (
                              SELECT PC.LEGID, [CrewLastName] = C.LastName, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
                                    WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
									WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
                              FROM PreflightCrewList PC
                              INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
                        ) PC1 WHERE PC1.LegID = PC2.LegID
                        ORDER BY [RowID]
                        FOR XML PATH('')
                        )
                  FROM PreflightCrewList PC2
            ) CR ON PL.LegID = CR.LegID

			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''  OR @LegNum is NULL )
			ORDER BY PL.LegNUM
        END	 