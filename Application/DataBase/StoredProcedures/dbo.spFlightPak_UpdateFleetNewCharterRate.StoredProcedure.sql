

/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateFleetNewCharterRate]    Script Date: 03/17/2013 19:12:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateFleetNewCharterRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateFleetNewCharterRate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdateFleetNewCharterRate]    Script Date: 03/17/2013 19:12:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
CREATE PROCEDURE [dbo].[spFlightPak_UpdateFleetNewCharterRate]           
(           
    @FleetNewCharterRateID bigint,    
 @CustomerID bigint,    
 @FleetID bigint,    
 @AircraftCD char(3),    
 @OrderNum int,    
 @FleetNewCharterRateDescription varchar(40),    
 @ChargeUnit varchar(25),    
 @NegotiatedChgUnit numeric(2, 0),    
 @BuyDOM numeric(17, 2),    
 @SellDOM numeric(17, 2),    
 @IsTaxDOM bit,    
 @IsDiscountDOM bit,    
 @BuyIntl numeric(17, 2),    
 @SellIntl numeric(17, 2),    
 @IsTaxIntl bit,    
 @IsDiscountIntl bit,    
 @StandardCrewDOM numeric(2, 0),    
 @StandardCrewIntl numeric(2, 0),    
 @MinimumDailyRequirement numeric(17, 2),    
 @YearMade char(4),    
 @ExteriorColor varchar(25),    
 @ColorIntl varchar(25),    
 @IsAFIS bit,    
 @IsUWAData bit,    
 @DBAircraftFlightChgID bigint,    
 @DSAircraftFlightChgID bigint,    
 @BuyAircraftFlightIntlID bigint,    
 @SellAircraftFlightIntlID bigint,     
 @LastUpdUID varchar(30),    
 @LastUpdTS datetime,    
 @AircraftTypeID BIGINT,
 @CQcustomerId  BIGINT,    
 @IsDeleted bit    
    
)            
AS            
BEGIN          
-- =============================================            
-- Author: Karthikeyan.S            
-- Create date: 29/01/2013            
-- Description: Insert fleet Charter Details            
-- =============================================            
SET NOCOUNT OFF;            
        
        
set @LastUpdTS = GETUTCDATE();    
          
           
 UPDATE [FleetNewCharterRate] SET      
 CustomerID=@CustomerID,       
 OrderNum = @OrderNum,    
 FleetNewCharterRateDescription =@FleetNewCharterRateDescription,    
 ChargeUnit = @ChargeUnit,    
 NegotiatedChgUnit = @NegotiatedChgUnit,    
 BuyDOM = @BuyDOM,    
 SellDOM = @SellDOM,    
 IsTaxDOM = @IsTaxDOM,    
 IsDiscountDOM = @IsDiscountDOM,    
 BuyIntl =@BuyIntl,    
 SellIntl =@SellIntl,    
 IsTaxIntl =@IsTaxIntl,    
 IsDiscountIntl = @IsDiscountIntl,    
 StandardCrewDOM =@StandardCrewDOM,    
 StandardCrewIntl =@StandardCrewIntl,    
 MinimumDailyRequirement =@MinimumDailyRequirement,    
 YearMade = @YearMade,    
 ExteriorColor = @ExteriorColor,    
 ColorIntl = @ColorIntl,    
 IsAFIS =@IsAFIS,    
 IsUWAData = @IsUWAData,    
 DBAircraftFlightChgID = @DBAircraftFlightChgID,  
 DSAircraftFlightChgID = @DSAircraftFlightChgID,  
 BuyAircraftFlightIntlID = @BuyAircraftFlightIntlID,  
 SellAircraftFlightIntlID = @SellAircraftFlightIntlID,  
 LastUpdUID =@LastUpdUID,    
 LastUpdTS = @LastUpdTS,    
 AircraftTypeID = @AircraftTypeID,  
 CQcustomerId = @CQcustomerId,  
 IsDeleted = @IsDeleted    
             WHERE FleetNewCharterRateID = @FleetNewCharterRateID      
 END   
  
GO


