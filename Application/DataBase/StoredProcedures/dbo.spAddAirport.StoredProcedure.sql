/****** Object:  StoredProcedure [dbo].[spAddAirport]    Script Date: 2015-01-15 5:09:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddAirport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddAirport]
GO

/****** Object:  StoredProcedure [dbo].[spAddAirport]    Script Date: 2015-01-15 5:09:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddAirport]    
(   
@IcaoID char(4),   
@CustomerID BIGINT,     
@CityName varchar(25),    
@StateName varchar(25),    
@CountryName varchar(60),    
@CountryID BIGINT,    
@AirportName varchar(25),    
@LatitudeDegree NUMERIC(3,0),    
@LatitudeMinutes NUMERIC(5,1),    
@LatitudeNorthSouth char(1),    
@LongitudeDegrees NUMERIC(3,0),    
@LongitudeMinutes NUMERIC(5,1),    
@LongitudeEastWest char(1),    
@MagneticVariance NUMERIC(5,1),    
@VarianceEastWest char(1),    
@Elevation NUMERIC(6,0),    
@LongestRunway NUMERIC(6,0),    
@IsEntryPort bit,    
@WindZone NUMERIC(5,0),    
@IsUSTax bit,    
@IsRural bit,    
@IsAirport bit,    
@IsHeliport bit,    
@AirportInfo varchar(max),    
@OffsetToGMT NUMERIC(6,2),    
@IsDayLightSaving bit,    
@DayLightSavingStartDT Date,    
@DaylLightSavingStartTM char(5),    
@DayLightSavingEndDT Date,    
@DayLightSavingEndTM char(5),    
@TowerFreq Varchar(7),    
@ATISFreq Varchar(7),    
@ARINCFreq Varchar(7),    
@GroundFreq Varchar(7),    
@ApproachFreq Varchar(7),    
@DepartFreq Varchar(7),    
@Label1Freq Varchar(10),    
@Freq1 Varchar(7),    
@Label2Freq Varchar(10),    
@Freq2 Varchar(7),    
@TakeoffBIAS NUMERIC(5,1),    
@LandingBIAS NUMERIC(5,1),    
@Alerts varchar(max),    
@GeneralNotes varchar(max),   
@IsFlightPakFlag bit,    
@FBOCnt int,    
@HotelCnt int,    
@TransportationCnt int,    
@CateringCnt int,    
@FlightPakUPD datetime,    
@MetroID BIGINT,    
@DSTRegionID BIGINT,    
@PPR Varchar(7),    
@Iata char(3),    
@AlternateDestination1 char(4),    
@AlternateDestination2 char(4),    
@AlternateDestination3 char(4),    
@LastUpdUID Varchar(30),    
@LastUpdTS datetime,    
@IsInActive bit,    
@RecordType char(4),    
@NewICAO char(4),    
@PreviousICAO char(4),    
@UpdateDT date,    
@IsWorldClock bit,    
@WidthRunway NUMERIC(6,0),    
@FssPhone varchar(25),    
@LengthWidthRunway NUMERIC(6,0),    
@WidthLengthRunway NUMERIC(6,0),    
@CustomLocation varchar(100),    
@CustomPhoneNum varchar(25),    
@ImmigrationPhoneNum varchar(25),    
@AgPhoneNum varchar(25),    
@IsCustomAvailable bit,    
@PortEntryType Varchar(6),    
@Unicom varchar(10),    
@Clearence1DEL varchar(10),    
@Clearence2DEL varchar(10),    
@AtisPhoneNum varchar(25),    
@ASOS Varchar(10),    
@AsosPhoneNum varchar(25),    
@AWOS Varchar(10),    
@AwosPhoneNum varchar(25),    
@AwosType char(1),    
@IsSlots bit,    
@FssName varchar(40),    
@AirportManager varchar(60),    
@AirportManagerPhoneNum varchar(25),    
@FAA char(4),    
@UWAID Varchar(20),    
@IsFIX bit,    
@HoursOfOperation varchar(25),    
@LandingFeeSmall NUMERIC(17,2),    
@LandingFeeMedium NUMERIC(17,2),    
@LandingFeeLarge NUMERIC(17,2),    
@IsEUETS bit,    
@IsDeleted bit,  
@UWAUpdates BIT,
@RunwayID1 varchar(20),
@RunwayID2 varchar(20),
@RunwayID3 varchar(20),
@RunwayID4 varchar(20), 
@Runway2Length numeric(6,0),
@Runway2Width  numeric(6,0),
@Runway3Length numeric(6,0),
@Runway3Width  numeric(6,0),
@Runway4Length numeric(6,0),
@Runway4Width  numeric(6,0),
@IsPublic	BIT,
@IsPrivate	BIT,
@IsMilitary	BIT,
@ExchangeRateID	BIGINT,
@CustomsSundayWorkHours	VARCHAR(15),
@CustomsMondayWorkHours	VARCHAR(15),
@CustomsTuesdayWorkHours	VARCHAR(15),
@CustomsWednesdayWorkHours	VARCHAR(15),
@CustomsThursdayWorkHours	VARCHAR(15),
@CustomsFridayWorkHours	VARCHAR(15),
@CustomsSaturdayWorkHours	VARCHAR(15),
@CustomNotes	VARCHAR(800),
@IsSeaPlane BIT = NULL
)    
-- =============================================    
-- Author:PremPrakash.S    
-- Create date: 18/4/2012    
-- Description: add the Airport information    
-- =============================================    
AS    
BEGIN       
DECLARE @AirportID BIGINT 
SET @LastUpdTS = GETUTCDATE() 

	IF NOT EXISTS (SELECT AirportID FROM Airport WHERE IcaoID = @IcaoID AND CustomerID = @CustomerID)        
	BEGIN    
		SET NOCOUNT ON 
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @AirportID OUTPUT    

		INSERT INTO [Airport]      
			([AirportID]    
           ,[IcaoID]    
           ,[CustomerID]    
           ,[CityName]    
           ,[StateName]    
           ,[CountryName]    
           ,[CountryID]    
           ,[AirportName]    
           ,[LatitudeDegree]    
           ,[LatitudeMinutes]    
           ,[LatitudeNorthSouth]    
           ,[LongitudeDegrees]    
           ,[LongitudeMinutes]    
           ,[LongitudeEastWest]    
           ,[MagneticVariance]    
           ,[VarianceEastWest]    
           ,[Elevation]    
           ,[LongestRunway]    
           ,[IsEntryPort]    
           ,[WindZone]    
           ,[IsUSTax]    
           ,[IsRural]    
           ,[IsAirport]    
           ,[IsHeliport]    
           ,[AirportInfo]    
           ,[OffsetToGMT]    
           ,[IsDayLightSaving]    
           ,[DayLightSavingStartDT]    
           ,[DaylLightSavingStartTM]    
           ,[DayLightSavingEndDT]    
           ,[DayLightSavingEndTM]    
           ,[TowerFreq]    
           ,[ATISFreq]    
           ,[ARINCFreq]    
           ,[GroundFreq]    
           ,[ApproachFreq]    
           ,[DepartFreq]    
           ,[Label1Freq]    
           ,[Freq1]    
           ,[Label2Freq]    
           ,[Freq2]    
           ,[TakeoffBIAS]    
           ,[LandingBIAS]    
           ,[Alerts]    
           ,[GeneralNotes]    
           ,[IsFlightPakFlag]    
           ,[FBOCnt]    
           ,[HotelCnt]    
           ,[TransportationCnt]    
           ,[CateringCnt]    
           ,[FlightPakUPD]    
           ,[MetroID]    
           ,[DSTRegionID]    
           ,[PPR]    
           ,[Iata]    
           ,[AlternateDestination1]    
           ,[AlternateDestination2]    
           ,[AlternateDestination3]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsInActive]    
           ,[RecordType]    
           ,[NewICAO]    
           ,[PreviousICAO]    
           ,[UpdateDT]    
           ,[IsWorldClock]    
           ,[WidthRunway]    
           ,[FssPhone]    
           ,[LengthWidthRunway]    
           ,[WidthLengthRunway]    
           ,[CustomLocation]    
           ,[CustomPhoneNum]    
           ,[ImmigrationPhoneNum]    
           ,[AgPhoneNum]    
           ,[IsCustomAvailable]    
           ,[PortEntryType]    
           ,[Unicom]    
           ,[Clearence1DEL]    
           ,[Clearence2DEL]    
           ,[AtisPhoneNum]    
           ,[ASOS]          
           ,[AsosPhoneNum]    
           ,[AWOS]    
           ,[AwosPhoneNum]    
           ,[AwosType]    
           ,[IsSlots]    
           ,[FssName]    
           ,[AirportManager]    
           ,[AirportManagerPhoneNum]    
           ,[FAA]    
           ,[UWAID]    
           ,[IsFIX]    
           ,[HoursOfOperation]    
           ,[LandingFeeSmall]    
           ,[LandingFeeMedium]    
           ,[LandingFeeLarge]    
           ,[IsEUETS]    
           ,[IsDeleted]    
           ,[UWAUpdates]  
           ,[RunwayID1]  
           ,[RunwayID2]  
           ,[RunwayID3]  
           ,[RunwayID4]  
           ,[Runway2Length]  
           ,[Runway3Length]  
           ,[Runway4Length]  
           ,[Runway2Width]  
           ,[Runway3Width]  
           ,[Runway4Width]  
           ,IsPublic  
		   ,IsPrivate  
		   ,IsMilitary  
		   ,ExchangeRateID  
		   ,CustomsSundayWorkHours  
		   ,CustomsMondayWorkHours  
		   ,CustomsTuesdayWorkHours  
		   ,CustomsWednesdayWorkHours  
		   ,CustomsThursdayWorkHours  
		   ,CustomsFridayWorkHours  
		   ,CustomsSaturdayWorkHours  
		   ,CustomNotes
		   ,IsSeaPlane       
           )      
     VALUES(@AirportID    
           ,@IcaoID    
           ,@CustomerID    
           ,@CityName    
           ,@StateName    
           ,@CountryName    
           ,@CountryID    
           ,@AirportName    
           ,@LatitudeDegree    
           ,@LatitudeMinutes    
           ,@LatitudeNorthSouth    
           ,@LongitudeDegrees    
           ,@LongitudeMinutes    
           ,@LongitudeEastWest    
           ,@MagneticVariance    
           ,@VarianceEastWest    
		   ,@Elevation    
           ,@LongestRunway    
           ,@IsEntryPort    
           ,@WindZone    
           ,@IsUSTax    
           ,@IsRural    
           ,@IsAirport    
           ,@IsHeliport    
           ,@AirportInfo    
           ,@OffsetToGMT    
           ,@IsDayLightSaving    
           ,@DayLightSavingStartDT    
           ,@DaylLightSavingStartTM    
           ,@DayLightSavingEndDT    
           ,@DayLightSavingEndTM    
           ,@TowerFreq    
           ,@ATISFreq    
           ,@ARINCFreq    
           ,@GroundFreq    
           ,@ApproachFreq    
           ,@DepartFreq    
           ,@Label1Freq    
           ,@Freq1    
           ,@Label2Freq    
           ,@Freq2    
           ,@TakeoffBIAS    
           ,@LandingBIAS    
           ,@Alerts    
           ,@GeneralNotes    
           ,@IsFlightPakFlag    
           ,@FBOCnt    
           ,@HotelCnt    
           ,@TransportationCnt    
           ,@CateringCnt    
           ,@FlightPakUPD    
           ,@MetroID    
           ,@DSTRegionID    
           ,@PPR    
           ,@Iata    
           ,@AlternateDestination1    
           ,@AlternateDestination2    
           ,@AlternateDestination3    
           ,@LastUpdUID    
           ,@LastUpdTS    
           ,@IsInActive    
           ,@RecordType    
           ,@NewICAO    
           ,@PreviousICAO    
           ,@UpdateDT    
           ,@IsWorldClock    
           ,@WidthRunway    
           ,@FssPhone    
           ,@LengthWidthRunway    
           ,@WidthLengthRunway    
           ,@CustomLocation    
           ,@CustomPhoneNum    
           ,@ImmigrationPhoneNum    
           ,@AgPhoneNum    
           ,@IsCustomAvailable    
           ,@PortEntryType    
           ,@Unicom    
           ,@Clearence1DEL    
           ,@Clearence2DEL    
           ,@AtisPhoneNum    
           ,@ASOS    
           ,@AsosPhoneNum    
           ,@AWOS    
           ,@AwosPhoneNum    
           ,@AwosType    
           ,@IsSlots    
           ,@FssName    
           ,@AirportManager    
           ,@AirportManagerPhoneNum    
           ,@FAA    
           ,@UWAID    
           ,@IsFIX    
           ,@HoursOfOperation    
           ,@LandingFeeSmall    
           ,@LandingFeeMedium    
           ,@LandingFeeLarge    
           ,@IsEUETS    
           ,@IsDeleted    
           ,@UWAUpdates  
           ,@RunwayID1  
           ,@RunwayID2  
           ,@RunwayID3  
           ,@RunwayID4  
           ,@Runway2Length  
           ,@Runway3Length  
           ,@Runway4Length  
           ,@Runway2Width  
           ,@Runway3Width  
           ,@Runway4Width    
           ,@IsPublic  
		   ,@IsPrivate  
		   ,@IsMilitary  
		   ,@ExchangeRateID  
		   ,@CustomsSundayWorkHours  
		   ,@CustomsMondayWorkHours  
		   ,@CustomsTuesdayWorkHours  
		   ,@CustomsWednesdayWorkHours  
		   ,@CustomsThursdayWorkHours  
		   ,@CustomsFridayWorkHours  
		   ,@CustomsSaturdayWorkHours  
		   ,@CustomNotes
		   ,@IsSeaPlane   
           )      
	END
	ELSE
	BEGIN
		SELECT @AirportID = AirportID FROM Airport WHERE IcaoID = @IcaoID AND CustomerID = @CustomerID
		EXEC spUpdateAirport
						@AirportID
						,@IcaoID
						,@CustomerID
						,@CityName
						,@StateName
						,@CountryName
						,@CountryID
						,@AirportName
						,@LatitudeDegree
						,@LatitudeMinutes
						,@LatitudeNorthSouth
						,@LongitudeDegrees
						,@LongitudeMinutes
						,@LongitudeEastWest
						,@MagneticVariance
						,@VarianceEastWest
						,@Elevation
						,@LongestRunway
						,@IsEntryPort
						,@WindZone
						,@IsUSTax
						,@IsRural
						,@IsAirport
						,@IsHeliport
						,@AirportInfo
						,@OffsetToGMT
						,@IsDayLightSaving
						,@DayLightSavingStartDT
						,@DaylLightSavingStartTM
						,@DayLightSavingEndDT
						,@DayLightSavingEndTM
						,@TowerFreq
						,@ATISFreq
						,@ARINCFreq
						,@GroundFreq
						,@ApproachFreq
						,@DepartFreq
						,@Label1Freq
						,@Freq1
						,@Label2Freq
						,@Freq2
						,@TakeoffBIAS
						,@LandingBIAS
						,@Alerts
						,@GeneralNotes
						,@IsFlightPakFlag
						,@FBOCnt
						,@HotelCnt
						,@TransportationCnt
						,@CateringCnt
						,@FlightPakUPD
						,@MetroID
						,@DSTRegionID
						,@PPR
						,@Iata
						,@AlternateDestination1
						,@AlternateDestination2
						,@AlternateDestination3
						,@LastUpdUID
						,@LastUpdTS
						,@IsInActive
						,@RecordType
						,@NewICAO
						,@PreviousICAO
						,@UpdateDT
						,@IsWorldClock
						,@WidthRunway
						,@FssPhone
						,@LengthWidthRunway
						,@WidthLengthRunway
						,@CustomLocation
						,@CustomPhoneNum
						,@ImmigrationPhoneNum
						,@AgPhoneNum
						,@IsCustomAvailable
						,@PortEntryType
						,@Unicom
						,@Clearence1DEL
						,@Clearence2DEL
						,@AtisPhoneNum
						,@ASOS
						,@AsosPhoneNum
						,@AWOS
						,@AwosPhoneNum
						,@AwosType
						,@IsSlots
						,@FssName
						,@AirportManager
						,@AirportManagerPhoneNum
						,@FAA
						,@UWAID
						,@IsFIX
						,@HoursOfOperation
						,@LandingFeeSmall
						,@LandingFeeMedium
						,@LandingFeeLarge
						,@IsEUETS
						,@IsDeleted
						,@UWAUpdates
						,@RunwayID1
						,@RunwayID2
						,@RunwayID3
						,@RunwayID4
						,@Runway2Length
						,@Runway2Width
						,@Runway3Length
						,@Runway3Width
						,@Runway4Length
						,@Runway4Width
						,@IsPublic
						,@IsPrivate
						,@IsMilitary
						,@ExchangeRateID
						,@CustomsSundayWorkHours
						,@CustomsMondayWorkHours
						,@CustomsTuesdayWorkHours
						,@CustomsWednesdayWorkHours
						,@CustomsThursdayWorkHours
						,@CustomsFridayWorkHours
						,@CustomsSaturdayWorkHours
						,@CustomNotes
						,@IsSeaPlane

	END      
 
	SELECT @AirportID as AirportID    
      
END

GO


