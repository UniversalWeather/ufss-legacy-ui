
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllPaymentType]    Script Date: 03/14/2014 22:00:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllPaymentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllPaymentType]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllPaymentType]    Script Date: 03/14/2014 22:00:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetAllPaymentType]
(
	@CustomerID BIGINT
	,@PaymentTypeID BIGINT    
	,@PaymenttypeCD char(2)     	
	,@FetchActiveOnly BIT 
)
as
-- =============================================                
-- Author: Sridhar               
-- Create date: 14/03/2014                
-- Description: Get All PaymentTyp details for Popup          
-- Exec sp_fss_GetAllPaymentType 10099,0,'',1    
-- Exec sp_fss_GetAllPaymentType 10099,0,'',0    
-- Exec sp_fss_GetAllPaymentType 10099, 100993525,'',0    
-- Exec sp_fss_GetAllPaymentType 10099, 0, 'DB',0    
-- =============================================  
SET NOCOUNT ON 
BEGIN 
	
	SELECT P.PaymentTypeID
			,CASE WHEN P.PaymenttypeCD IS NULL THEN '' ELSE LTRIM(RTRIM(P.PaymenttypeCD)) END AS PaymenttypeCD
			,P.CustomerID
			,P.PaymentTypeDescription
			,P.LastUpdUID
			,P.LastUpdTS
			,ISNULL(P.IsDeleted,0) IsDeleted       
			,ISNULL(P.IsInactive,0) IsInactive   
  
	FROM 
		PaymentType P
	WHERE 
		P.CustomerID=@CustomerID  
		AND P.PaymentTypeID = case when @PaymentTypeID <> 0 then @PaymentTypeID else P.PaymentTypeID end     
		AND P.PaymenttypeCD = case when Ltrim(Rtrim(@PaymenttypeCD)) <> '' then @PaymenttypeCD else P.PaymenttypeCD end                                      
		AND ISNULL(P.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(P.IsInActive,0) else 0 end    
		AND ISNULL(P.IsDeleted,0) = 0
   
   order by P.PaymenttypeCD

end


GO


