
/****** Object:  StoredProcedure [dbo].[spAddDepartmentGroup]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddDepartmentGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddDepartmentGroup]
go
CREATE Procedure [dbo].[spAddDepartmentGroup]
 (	       
           @CustomerID bigint,
           @HomebaseID bigint,
           @DepartmentGroupCD char(4),
           @DepartmentGroupDescription varchar(40),
           @LastUpdUID varchar(30),
           @LastUpdTS datetime,
           @IsDeleted bit,
           @IsInActive bit
)
-- =============================================
-- Author: Sujitha.V
-- Create date: 8/4/2012
-- Description: Insert the Department Group information
-- =============================================

AS  
BEGIN   
SET NOCOUNT ON  

DECLARE @DepartmentGroupID BIGINT 
 
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @DepartmentGroupID OUTPUT
SET @LastUpdTS = GETUTCDATE()
   INSERT INTO DepartmentGroup
	  (
	   [DepartmentGroupID]
	  ,[CustomerID]
      ,[HomebaseID]
      ,[DepartmentGroupCD]
      ,[DepartmentGroupDescription]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,[IsInActive]
      )
     VALUES
     (
            @DepartmentGroupID,
            @CustomerID ,
	        @HomebaseID ,
		    @DepartmentGroupCD ,
            @DepartmentGroupDescription, 
            @LastUpdUID ,
            @LastUpdTS ,
            @IsDeleted ,
            @IsInActive
            )
 
 END
GO
