
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewPassport]    Script Date: 09/27/2013 20:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddCrewPassport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddCrewPassport]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_AddCrewPassport]    Script Date: 09/27/2013 20:28:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE PROCEDURE [dbo].[spFlightPak_AddCrewPassport]                      
(@PassportID bigint                    
,@CrewID bigint                    
,@PassengerRequestorID bigint                    
,@CustomerID bigint                    
,@Choice bit                    
,@IssueCity varchar(40)                    
,@PassportNum nvarchar(120)                    
,@PassportExpiryDT date                    
,@CountryID bigint                    
,@IsDefaultPassport bit                    
,@PilotLicenseNum varchar(25)                    
,@LastUpdUID varchar(30)                    
,@LastUpdTS datetime                    
,@IssueDT date                    
,@IsDeleted bit)               
-- =============================================                  
-- Author:Karthikeyan.S                  
-- Create date: 09/23/2013                 
-- Description: Insert the PassportInfo     
-- Modified date: 10/04/2013            
-- Modified Description: If the entered Reocrd is Expired Date,allowed to save it directly                       
-- =============================================                      
AS                     
BEGIN                     
 IF (@CustomerID is null)                    
 BEGIN                    
  SET @CustomerID=0                    
 END                    
      
 DECLARE @ErrorMessage nVarchar(2000);              
            
 --For validating duplicate Passport Number for the customer and Passenger                    
 IF (@PassengerRequestorID IS NOT NULL)                    
 BEGIN                    
  IF (@IsDeleted <> 1)                    
  BEGIN    
   IF(GETDATE() > @PassportExpiryDT)  
   BEGIN  
  
    EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassportID OUTPUT                    
    SET @LastUpdTS = GETUTCDATE()   
      
    INSERT INTO [CrewPassengerPassport]                    
    ([PassportID]                    
    ,[CrewID]                    
    ,[PassengerRequestorID]                    
    ,[CustomerID]                    
    ,[Choice]                    
    ,[IssueCity]                    
    ,[PassportNum]                    
    ,[PassportExpiryDT]                    
    ,[CountryID]                    
    ,[IsDefaultPassport]                    
    ,[PilotLicenseNum]                    
    ,[LastUpdUID]                    
    ,[LastUpdTS]                    
    ,[IssueDT]                    
    ,[IsDeleted])                    
    VALUES                    
    (@PassportID                    
      ,@CrewID                    
      ,@PassengerRequestorID                    
      ,@CustomerID                    
      ,@Choice                    
      ,@IssueCity                    
      ,@PassportNum                    
      ,@PassportExpiryDT                    
      ,@CountryID                    
      ,@IsDefaultPassport                    
      ,@PilotLicenseNum                    
      ,@LastUpdUID                    
      ,@LastUpdTS                    
      ,@IssueDT                    
      ,@IsDeleted)     
    
   END    
   ELSE    
   BEGIN                 
    IF EXISTS (SELECT PassportNum FROM [CrewPassengerPassport] WHERE CustomerID = @CustomerID                    
         AND PassportID <> @PassportID                    
         AND CrewID IS NULL                    
         AND PassportNum = @PassportNum    
         --AND PassportExpiryDT > GETDATE()                  
         AND GETDATE() < PassportExpiryDT                   
         AND CountryID = @CountryID                   
         AND IsDeleted = 0)                    
    BEGIN                    
     SET @ErrorMessage = '-500012~' + @PassportNum;                    
     RAISERROR(@ErrorMessage, 17, 1)                    
    END          
    ELSE        
    BEGIN        
     EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassportID OUTPUT                    
     SET @LastUpdTS = GETUTCDATE()           
             
     INSERT INTO [CrewPassengerPassport]                    
      ([PassportID]                    
      ,[CrewID]                    
      ,[PassengerRequestorID]                    
      ,[CustomerID]              
      ,[Choice]                    
      ,[IssueCity]                    
      ,[PassportNum]                    
      ,[PassportExpiryDT]                    
      ,[CountryID]                    
      ,[IsDefaultPassport]                    
      ,[PilotLicenseNum]                    
      ,[LastUpdUID]                    
      ,[LastUpdTS]                    
      ,[IssueDT]                    
      ,[IsDeleted])                    
     VALUES                    
      (@PassportID                    
       ,@CrewID                    
       ,@PassengerRequestorID                    
       ,@CustomerID                    
       ,@Choice                    
       ,@IssueCity                    
       ,@PassportNum                    
       ,@PassportExpiryDT                    
       ,@CountryID                    
       ,@IsDefaultPassport                    
       ,@PilotLicenseNum                    
       ,@LastUpdUID                    
       ,@LastUpdTS                    
       ,@IssueDT                    
       ,@IsDeleted)                 
    END       
  
   END              
  END                  
 END                
   
 --For validating duplicate Passport Number for the customer and Crew                    
 IF (@CrewID IS NOT NULL)                    
 BEGIN                    
  IF (@IsDeleted <> 1)                    
  BEGIN  
   IF(GETDATE() > @PassportExpiryDT)  
   BEGIN  
     
       EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassportID OUTPUT                    
    SET @LastUpdTS = GETUTCDATE()  
      
    INSERT INTO [CrewPassengerPassport]                    
    ([PassportID]                    
    ,[CrewID]                    
    ,[PassengerRequestorID]                    
    ,[CustomerID]                    
    ,[Choice]                    
    ,[IssueCity]                    
    ,[PassportNum]                    
    ,[PassportExpiryDT]                    
    ,[CountryID]                    
    ,[IsDefaultPassport]                    
    ,[PilotLicenseNum]                    
    ,[LastUpdUID]                    
    ,[LastUpdTS]                    
    ,[IssueDT]                    
    ,[IsDeleted])                    
    VALUES                    
    (@PassportID                    
      ,@CrewID                    
      ,@PassengerRequestorID                    
      ,@CustomerID                    
      ,@Choice                    
      ,@IssueCity                    
      ,@PassportNum                    
      ,@PassportExpiryDT                    
      ,@CountryID                    
      ,@IsDefaultPassport                    
      ,@PilotLicenseNum                    
      ,@LastUpdUID                    
      ,@LastUpdTS                    
      ,@IssueDT                    
      ,@IsDeleted)       
   END  
   ELSE  
   BEGIN                    
    IF EXISTS (SELECT PassportNum FROM [CrewPassengerPassport] WHERE CustomerID = @CustomerID                    
       AND PassportID <> @PassportID                    
       AND PassengerRequestorID IS NULL                    
       AND PassportNum = @PassportNum               
       --AND PassportExpiryDT > GETDATE()  
       AND GETDATE() < PassportExpiryDT                   
       AND CountryID = @CountryID                    
       AND IsDeleted = 0)                    
    BEGIN         
    --Print 'ErrorMessage'             
     SET @ErrorMessage = '-500012~' + @PassportNum;                    
     RAISERROR(@ErrorMessage, 17, 1)                    
    END         
    ELSE        
    BEGIN        
     EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @PassportID OUTPUT                    
     SET @LastUpdTS = GETUTCDATE()           
             
     INSERT INTO [CrewPassengerPassport]                    
      ([PassportID]                    
      ,[CrewID]                    
      ,[PassengerRequestorID]                    
      ,[CustomerID]                    
      ,[Choice]                    
      ,[IssueCity]                    
      ,[PassportNum]                    
      ,[PassportExpiryDT]                    
      ,[CountryID]                    
      ,[IsDefaultPassport]                    
      ,[PilotLicenseNum]                    
      ,[LastUpdUID]                    
      ,[LastUpdTS]                    
      ,[IssueDT]                    
      ,[IsDeleted])                    
     VALUES                    
      (@PassportID                    
       ,@CrewID                    
       ,@PassengerRequestorID                    
       ,@CustomerID                    
       ,@Choice                    
       ,@IssueCity                    
       ,@PassportNum                    
       ,@PassportExpiryDT                    
       ,@CountryID                    
       ,@IsDefaultPassport                    
       ,@PilotLicenseNum                    
       ,@LastUpdUID                    
       ,@LastUpdTS                    
       ,@IssueDT                    
       ,@IsDeleted)                 
    END      
   END               
  END                    
 END                    
END 
GO

