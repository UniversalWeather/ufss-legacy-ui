
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFuelVendor]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFuelVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFuelVendor]
GO
CREATE procedure [dbo].[spUpdateFuelVendor] (@FuelVendorID BIGINT,
                                         @VendorCD CHAR(4), 
                                         @VendorName VARCHAR(50), 
                                         @AirportID BIGINT,
                                         @CustomerID BIGINT,
                                         @VendorDescription VARCHAR(50),  
                                         @ServerInfo VARCHAR(200),  
                                         @BaseFileName VARCHAR(100),  
                                         @FileLocation VARCHAR(200),  
                                         @FileTYPE VARCHAR(10),  
                                         @FldSep CHAR(1), 
                                         @IsHeaderRow BIT,  
                                         @VendorDateFormat VARCHAR(10),  
                                         @LastUpdUID VARCHAR(30),  
                                         @LastUpdTS DATETIME , 
                                         @IsDeleted BIT,  
                                         @UWAUpdates BIT,
                                         @IsInActive BIT,
										 @NoOfFuelRecordsInserted BIGINT=0)    
-- =============================================    
-- Author: Badrinath    
-- Create date: 3/5/2012    
-- Description: Update the Fuel Vendor information    
-- =============================================    
as begin     
SET NOCOUNT ON    

--To store the lastupdateddate as UTC date
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime
DECLARE @UWACustomerID BIGINT          
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output 

If exists (select VendorCD from fuelvendor where VendorCD=@VendorCD)
begin

  UPDATE FuelVendor    
  SET VendorCD=@VendorCD, 
      VendorName=@VendorName, 
      AirportID=@AirportID,
      CustomerID=@UWACustomerID,
      VendorDescription=@VendorDescription,  
      ServerInfo=@ServerInfo,  
      BaseFileName=@BaseFileName,  
      FileLocation=@FileLocation,  
      FileTYPE=@FileTYPE,  
      FldSep=@FldSep, 
      IsHeaderRow=@IsHeaderRow,  
      VendorDateFormat=@VendorDateFormat,  
      LastUpdUID=@LastUpdUID,  
      LastUpdTS=@LastUpdTS, 
      IsDeleted=@IsDeleted,  
      UWAUpdates=@UWAUpdates,
      IsInActive=@IsInActive,
	  NoOfFuelRecordsInserted=@NoOfFuelRecordsInserted
  WHERE [FuelVendorID] = @FuelVendorID and [VendorCD]=@VendorCD  

end
else
begin

EXECUTE dbo.usp_GetSequenceNumber @UWACustomerID, 'MasterModuleCurrentNo',  @FuelVendorID OUTPUT   

	INSERT INTO [FuelVendor]  ( FuelVendorID,  
                             VendorCD ,   
                             VendorName,   
                             AirportID,  
                             CustomerID,  
                             VendorDescription,    
                             ServerInfo,    
                             BaseFileName,    
                             FileLocation,    
                             FileTYPE,    
                             FldSep,   
                             IsHeaderRow,    
                             VendorDateFormat,    
                             LastUpdUID,    
                             LastUpdTS,   
                             IsDeleted,    
                             UWAUpdates,
                             IsInActive,
							 NoOfFuelRecordsInserted)    
                      VALUES (@FuelVendorID,  
                              @VendorCD ,   
                              @VendorName,   
                              @AirportID,  
                              @UWACustomerID,  
                              @VendorDescription,    
                              @ServerInfo,    
                              @BaseFileName,    
                              @FileLocation,    
                              @FileTYPE,    
                              @FldSep,   
                              @IsHeaderRow,    
                              @VendorDateFormat,    
                              @LastUpdUID,    
                              @LastUpdTS,   
                              @IsDeleted,    
                              @UWAUpdates,
                              @IsInActive,
							  @NoOfFuelRecordsInserted)    
   

end

SELECT @FuelVendorID as FuelVendorID

END