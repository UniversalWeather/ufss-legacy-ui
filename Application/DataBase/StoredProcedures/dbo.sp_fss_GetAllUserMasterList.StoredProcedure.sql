
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllUserMasterList]    Script Date: 03/10/2014 21:45:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllUserMasterList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllUserMasterList]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllUserMasterList]    Script Date: 03/10/2014 21:45:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_fss_GetAllUserMasterList]    
(  
	@CustomerID Bigint  
	,@UserMasterID BIGINT  
	,@UserName varchar(30)    
	--,@FetchActiveOnly BIT 
)  
AS   
-- =============================================              
-- Author: Sridhar             
-- Create date: 25/02/2014              
-- Description: Get All Client details for Popup        
-- Exec sp_fss_GetAllUserMasterList 10099,0,''
-- Exec sp_fss_GetAllUserMasterList 10099,0,'JWILLIAMS_99'
-- Exec sp_fss_GetAllUserMasterList 10099, 100996,'' 
-- =============================================  
SET NOCOUNT ON;   
BEGIN  
	SELECT 
			UserMasterID
			,[UserName]  
			,[CustomerID]  
			,[HomebaseID]  
			,[ClientID]  
			,[FirstName]  
			,[MiddleName]  
			,[LastName]  
			,[HomeBase]  
			,[TravelCoordID]  
			,ISNULL(Isactive,0) IsInactive     
			,[IsUserLock]  
			,[IsTripPrivacy]  
			,[EmailID]  
			,[PhoneNum]  
			,[PassengerRequestorID]  
			,[UVTripLinkID]  
			,[UVTripLinkPassword]  
			,[IsPrimaryContact]  
			,[IsSecondaryContact]  
			,[IsSystemAdmin]  
			,[IsDispatcher]  
			,[IsOverrideCorp]  
			,[IsCorpRequestApproval]  
			,[LastUpdUID]  
			,[LastUpdTS]  
			,ISNULL(IsDeleted,0) IsDeleted   
		FROM 
			UserMaster  
		where 
				CustomerID = @CustomerID  and IsDispatcher =1   
				AND UserMasterID = case when @UserMasterID <> 0 then @UserMasterID else UserMasterID end   
				AND UserName     = case when Ltrim(Rtrim(@UserName)) <> '' then @UserName else UserName end                                    
				--AND ISNULL(IsActive,0) = case when @FetchActiveOnly = 0 then ISNULL(IsActive,0) else 1 end  
				AND ISNULL(IsDeleted,0)	= 0  
END  
GO


