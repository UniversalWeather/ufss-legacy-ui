/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_DeleteCRFBOList]    Script Date: 02/11/2013 15:55:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_DeleteCRFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRFBOList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_DeleteCRFBOList]
(@CRFBOListID BIGINT
,@CustomerID BIGINT
,@CRLegID BIGINT
)
AS BEGIN

	SET NOCOUNT OFF;
	Delete from CRFBOList
	WHERE 
		CRFBOListID = @CRFBOListID and CustomerID=@CustomerID and CRLegID=@CRLegID

END
GO


