

/****** Object:  StoredProcedure [dbo].[spAddCQReportDetail]    Script Date: 04/24/2013 18:58:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCQReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCQReportDetail]
GO

/****** Object:  StoredProcedure [dbo].[spAddCQReportDetail]    Script Date: 04/24/2013 18:58:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddCQReportDetail]
(@CQReportDetailID bigint,
@CustomerID bigint,
@CQReportHeaderID bigint,
@ReportNumber int,
@ReportDescription varchar(35),
@IsSelected bit,
@Copies int,
@ReportProcedure varchar(8),
@ParameterCD varchar(10),
@ParamterDescription varchar(100),
@ParamterType char(1),
@ParameterWidth int,
@ParameterCValue varchar(60),
@ParameterLValue bit,
@ParameterNValue int,
@ParameterValue varchar(1500),
@ReportOrder int,
--@TripSheetOrder int,
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit,
@ParameterSize varchar(50),
@WarningMessage varchar(500))
)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	
	
	IF EXISTS(SELECT CQReportDetailID FROM CQReportDetail WHERE CQReportDetailID=@CQReportDetailID AND CustomerID=@CustomerID)
		BEGIN		
			UPDATE	CQReportDetail
			SET	[CustomerID]=@CustomerID
				,[CQReportHeaderID]=@CQReportHeaderID
				,[ReportNumber]=@ReportNumber
				,[ReportDescription]=@ReportDescription
				,[IsSelected]=@IsSelected
				,[Copies]=@Copies
				,[ReportProcedure]=@ReportProcedure
				,[ParameterCD]=@ParameterCD
				,[ParamterDescription]=@ParamterDescription
				,[ParamterType]=@ParamterType
				,[ParameterWidth]=@ParameterWidth
				,[ParameterCValue]=@ParameterCValue
				,[ParameterLValue]=@ParameterLValue
				,[ParameterNValue]=@ParameterNValue
				,[ParameterValue]=@ParameterValue
				,[ReportOrder]=@ReportOrder
				--,[TripSheetOrder]=@TripSheetOrder
				,[LastUpdUID]=@LastUpdUID
				,[LastUpdTS]=@LastUpdTS
				,[IsDeleted]=@IsDeleted
				,[ParameterSize]=@ParameterSize
				,[WarningMessage]=@WarningMessage
			WHERE [CQReportDetailID]=@CQReportDetailID
		END
	ELSE
		BEGIN
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CQReportDetailID OUTPUT
	
			INSERT INTO CQReportDetail
			   ([CQReportDetailID]
			   ,[CustomerID]
			   ,[CQReportHeaderID]
			   ,[ReportNumber]
			   ,[ReportDescription]
			   ,[IsSelected]
			   ,[Copies]
			   ,[ReportProcedure]
			   ,[ParameterCD]
			   ,[ParamterDescription]
			   ,[ParamterType]
			   ,[ParameterWidth]
			   ,[ParameterCValue]
			   ,[ParameterLValue]
			   ,[ParameterNValue]
			   ,[ParameterValue]
			   ,[ReportOrder]
			   --,[TripSheetOrder]
			   ,[LastUpdUID]
			   ,[LastUpdTS]
			   ,[IsDeleted]
			   ,[ParameterSize]
			   ,[WarningMessage]
			   )
			VALUES
			   (@CQReportDetailID,
				@CustomerID,
				@CQReportHeaderID,
				@ReportNumber,
				@ReportDescription,
				@IsSelected,
				@Copies,
				@ReportProcedure,
				@ParameterCD,
				@ParamterDescription,
				@ParamterType,
				@ParameterWidth,
				@ParameterCValue,
				@ParameterLValue,
				@ParameterNValue,
				@ParameterValue,
				@ReportOrder,
				@LastUpdUID,
				@LastUpdTS,
				@IsDeleted,
				@ParameterSize,
				@WarningMessage)
		END

	SELECT @CQReportDetailID as CQReportDetailID

END

GO


