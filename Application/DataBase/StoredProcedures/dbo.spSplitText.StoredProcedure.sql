

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSplitText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSplitText]
GO
/****** Object:  StoredProcedure [dbo].[spSplitText]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.spSplitText --'String1,String2,String3,String4,MyNameIs,TestDriveFromBIGString,GOGOGO,Bye'
(@Text TEXT)
AS BEGIN
DECLARE @StrLine VARCHAR(8000)
DECLARE @Size BIGINT
DECLARE @Start BIGINT
DECLARE @Separator VARCHAR(1)
DECLARE @TempImport TABLE(Idx BIGINT IDENTITY(1,1), SplitedLine VARCHAR(8000), Size INT)

SET @Size = 1
SET @Start = 1
SET @Separator = ','

WHILE (@Start < DATALENGTH(@Text) + 1) BEGIN
SET @Size = CHARINDEX(@Separator, SUBSTRING(@Text, @Start, DATALENGTH(@Text)), 1)
IF @Size = 0 SET @Size = DATALENGTH(@Text) - @Start + 1
SET @StrLine = SUBSTRING(SUBSTRING(@Text, @Start, DATALENGTH(@Text)), 1, @Size)
SET @StrLine = REPLACE(@StrLine,@Separator,'')
INSERT INTO @TempImport(SplitedLine, Size) VALUES(@StrLine, LEN(@StrLine))
SET @Start = @Start + @Size
END

SELECT SplitedLine FROM @TempImport

END

GO
