
/****** Object:  StoredProcedure [dbo].[spAddCustomerAddress]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddCustomerAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddCustomerAddress]
go
CREATE PROCEDURE [spAddCustomerAddress]  
 (@CustomerID BIGINT,   
 @UserName varchar(30),   
 @Name VARCHAR(40),   
 @CustomAddressCD VARCHAR(5),   
 @Address1 VARCHAR(100),   
 @Address2 VARCHAR(100),   
 @Address3 VARCHAR(100),   
 @CustomCity VARCHAR(30),   
 @CustomState CHAR(30),   
 @PostalZipCD VARCHAR(15),   
 @CountryID BIGINT,   
 @MetropolitanArea CHAR(3),   
 @Contact VARCHAR(25),   
 @PhoneNum VARCHAR(25),   
 @FaxNum VARCHAR(25),   
 @ContactEmailID VARCHAR(100),   
 @Website VARCHAR(200),   
 @Symbol VARCHAR(12),   
 @LatitudeDegree NUMERIC(3, 0),   
 @LatitudeMinutes NUMERIC(5, 1),   
 @LatitudeNorthSouth CHAR(1),   
 @LongitudeDegrees NUMERIC(3, 0),   
 @LongitudeMinutes NUMERIC(5, 1),   
 @LongitudeEastWest CHAR(1),   
 @AirportID BIGINT,   
 @HomeBaseID BIGINT,   
 @Remarks VARCHAR(100),   
 @LastUpdUID VARCHAR(30),   
 @LastUpdTS DATETIME,   
 @IsDeleted BIT  
 ,@BusinessPhone varchar(25)  
 ,@CellPhoneNum varchar(25)  
 ,@HomeFax varchar(25)  
 ,@CellPhoneNum2 varchar(25)  
 ,@OtherPhone varchar(25)  
 ,@BusinessEmail varchar(250)  
 ,@PersonalEmail varchar(250)  
 ,@OtherEmail varchar(250)
 ,@IsInActive BIT)   
-- =============================================    
-- Author:Karthikeyan.S    
-- Create date: 13/05/2012    
-- Description: Insert the Customer Address information    
-- =============================================    
AS  BEGIN     
 SET NOCOUNT ON  
 DECLARE @CustomAddressID BIGINT    
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @CustomAddressID OUTPUT  
 set @LastUpdTS = GETUTCDATE()    
   
 INSERT INTO [CustomAddress]  
  (CustomAddressID,    
  CustomerID,     
  UserName,    
  Name,     
  Address1,     
  Address2,     
  Address3,     
  CustomCity,     
  CustomState,     
  PostalZipCD,     
  CountryID,     
  MetropolitanArea,     
  Contact,     
  PhoneNum,     
  FaxNum,     
  ContactEmailID,     
  Website,     
  Symbol,     
  LatitudeDegree,     
  LatitudeMinutes,     
  LatitudeNorthSouth,     
  LongitudeDegrees,     
  LongitudeMinutes,     
  LongitudeEastWest,     
  AirportID,    
  HomebaseID,    
  Remarks,     
  LastUpdUID,     
  LastUpdTS,     
  IsDeleted,    
  CustomAddressCD  
  ,BusinessPhone  
  ,CellPhoneNum  
  ,HomeFax  
  ,CellPhoneNum2  
  ,OtherPhone  
  ,BusinessEmail  
  ,PersonalEmail  
  ,OtherEmail
  ,IsInActive)  
 VALUES  
  (@CustomAddressID,   
  @CustomerID,   
  @UserName,   
  @Name,   
  @Address1,   
  @Address2,   
  @Address3,   
  @CustomCity,   
  @CustomState,   
  @PostalZipCD,   
  @CountryID,   
  @MetropolitanArea,   
  @Contact,   
  @PhoneNum,   
  @FaxNum,   
  @ContactEmailID,   
  @Website,   
  @Symbol,   
  @LatitudeDegree,   
  @LatitudeMinutes,   
  @LatitudeNorthSouth,   
  @LongitudeDegrees,   
  @LongitudeMinutes,   
  @LongitudeEastWest,   
  @AirportID,   
  @HomebaseID,   
  @Remarks,   
  @LastUpdUID,   
  @LastUpdTS,   
  @IsDeleted,   
  @CustomAddressCD  
  ,@BusinessPhone  
  ,@CellPhoneNum  
  ,@HomeFax  
  ,@CellPhoneNum2  
  ,@OtherPhone  
  ,@BusinessEmail  
  ,@PersonalEmail  
  ,@OtherEmail
  ,@IsInActive)  
END    
GO
