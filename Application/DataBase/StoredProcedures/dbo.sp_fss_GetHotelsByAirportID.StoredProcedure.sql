/****** Object:  StoredProcedure [dbo].[sp_fss_GetTransportByAirportID]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetHotelsByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetHotelsByAirportID]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetHotelsByAirportID]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetHotelsByAirportID]
(@CustomerID BIGINT, 
@AirportID BIGINT,
@HotelID BIGINT,
@HotelCD char(4),
@FetchInactiveOnly BIT=0,
@FetchIsCrewOnly BIT=0,
@FetchIsPaxOnly BIT=0
)
AS            
-- =============================================            
-- Author: Karthikeyan.S           
-- Create date: 11/03/2014            
-- Description : Get the Hotel by Airports
-- Exec sp_fss_GetHotelsByAirportID
-- =============================================            
SET NOCOUNT ON            
          
DECLARE @HotelICAOID CHAR(4)          
--DECLARE @UWACustomerID BIGINT          
          
SELECT @HotelICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID    and IsDeleted=0       
--EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output          
DECLARE @UWACustomerID BIGINT             
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output          
            
DECLARE @ICAOCD VARCHAR(50)        
DECLARE @UWAAirportId VARCHAR(50)     
DECLARE @CustomAirportID VARCHAR(50)            
    
select @ICAOCD =  icaoid from Airport where AirportID = @AirportID and IsDeleted=0       
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD   and IsDeleted=0       
    
--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.        
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD        
          
 SELECT   H.[HotelID]          
   ,H.[AirportID]          
   ,H.[HotelCD]          
   ,H.[CustomerID]          
   ,H.[IsChoice]          
   ,H.[Name]          
   ,H.[PhoneNum]          
   ,H.[MilesFromICAO]          
   ,H.[HotelRating]          
   ,H.[FaxNum]          
   ,H.[NegociatedRate]          
   ,H.[Addr1]          
   ,H.[Addr2]          
   ,H.[Addr3]          
   ,H.[CityName]          
   ,H.[MetroID]          
   ,H.[StateName]          
   ,H.[PostalZipCD]          
   ,H.[CountryID]          
   ,H.[ContactName]          
   ,H.[Remarks]          
   ,ISNULL(H.[IsPassenger],0) IsPassenger
   ,ISNULL(H.[IsCrew],0) IsCrew
   ,H.[IsUWAMaintained]          
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter          
   ,H.[UWAMaintFlag]           
   ,H.[LatitudeDegree]          
   ,H.[LatitudeMinutes]          
   ,H.[LatitudeNorthSouth]          
   ,H.[LongitudeDegrees]          
   ,H.[LongitudeMinutes]          
   ,H.[LongitudeEastWest]          
   ,H.[LastUpdUID]          
   ,H.[LastUpdTS]          
   ,H.[UpdateDT]          
   ,H.[RecordType]          
   ,H.[MinutesFromICAO]          
   ,H.[SourceID]          
   ,H.[ControlNum]          
   ,H.[Website]             
   ,H.[UWAID]          
   ,ISNULL(H.[IsInActive],0) IsInactive         
   ,H.[IsDeleted]          
   ,H.[UWAUpdates]          
   ,@HotelICAOID IcaoID             
   ,m.MetroCD          
   ,c.CountryCD           
   ,H.[AltBusinessPhone]          
   ,H.[TollFreePhone]          
   ,H.[BusinessEmail]          
   ,H.[ContactBusinessPhone]          
   ,H.[CellPhoneNum]          
   ,H.[ContactEmail]        
   ,H.ExchangeRateID        
   ,H.NegotiatedTerms        
   ,H.SundayWorkHours        
   ,H.MondayWorkHours        
   ,H.TuesdayWorkHours        
   ,H.WednesdayWorkHours        
   ,H.ThursdayWorkHours        
   ,H.FridayWorkHours        
   ,H.SaturdayWorkHours        
 FROM  [Hotel] H          
 --inner join [Airport] Ai on H.AirportID = Ai.AirportID           
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID           
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID          
 WHERE      
 (H.AirportID = @CustomAirportID
 AND H.CustomerID = @CustomerID
 AND H.IsDeleted = 0
 AND H.HotelID = case when @HotelID <> 0 then @HotelID else H.HotelID end 
 AND H.HotelCD = case when Ltrim(Rtrim(@HotelCD)) <> '' then @HotelCD else H.HotelCD end 
 AND ISNULL(H.IsInActive,0) = case when @FetchInactiveOnly =1 then ISNULL(H.IsInActive,0) else  0 end
 AND ISNULL(H.IsCrew,0) = case when @FetchIsCrewOnly =1 then @FetchIsCrewOnly else  ISNULL(H.IsCrew,0) end
 AND ISNULL(H.IsPassenger,0) = case when @FetchIsPaxOnly =1 then @FetchIsPaxOnly else  ISNULL(H.IsPassenger,0) end)       
 OR 
 (H.AirportID  = @UWAAirportId 
 AND  H.CustomerID = @CustomerID 
 AND H.IsDeleted = 0
 AND H.HotelID = case when @HotelID <> 0 then @HotelID else H.HotelID end 
 AND H.HotelCD = case when Ltrim(Rtrim(@HotelCD)) <> '' then @HotelCD else H.HotelCD end 
 AND ISNULL(H.IsInActive,0) = case when @FetchInactiveOnly =1 then ISNULL(H.IsInActive,0) else 0 end
 AND ISNULL(H.IsCrew,0) = case when @FetchIsCrewOnly =1 then @FetchIsCrewOnly else  ISNULL(H.IsCrew,0) end
 AND ISNULL(H.IsPassenger,0) = case when @FetchIsPaxOnly =1 then @FetchIsPaxOnly else  ISNULL(H.IsPassenger,0) end)
           
 UNION ALL          
           
 SELECT   H.[HotelID]          
   ,H.[AirportID]          
   ,H.[HotelCD]          
   ,H.[CustomerID]          
   ,H.[IsChoice]          
   ,H.[Name]          
   ,H.[PhoneNum]          
   ,H.[MilesFromICAO]          
   ,H.[HotelRating]          
   ,H.[FaxNum]          
   ,H.[NegociatedRate]          
   ,H.[Addr1]          
   ,H.[Addr2]          
   ,H.[Addr3]          
   ,H.[CityName]          
   ,H.[MetroID]          
   ,H.[StateName]          
   ,H.[PostalZipCD]          
   ,H.[CountryID]          
   ,H.[ContactName]          
   ,H.[Remarks]          
   ,ISNULL(H.[IsPassenger],0) IsPassenger
   ,ISNULL(H.[IsCrew],0) IsCrew          
   ,H.[IsUWAMaintained]          
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter          
   ,H.[UWAMaintFlag]            
   ,H.[LatitudeDegree]          
   ,H.[LatitudeMinutes]          
   ,H.[LatitudeNorthSouth]          
   ,H.[LongitudeDegrees]          
   ,H.[LongitudeMinutes]          
   ,H.[LongitudeEastWest]          
   ,H.[LastUpdUID]          
   ,H.[LastUpdTS]          
   ,H.[UpdateDT]          
   ,H.[RecordType]          
   ,H.[MinutesFromICAO]          
   ,H.[SourceID]          
   ,H.[ControlNum]          
   ,H.[Website]             
   ,H.[UWAID]          
   ,ISNULL(H.[IsInActive],0) IsInactive         
   ,H.[IsDeleted]          
   ,H.[UWAUpdates]          
   ,@HotelICAOID IcaoID             
   ,m.MetroCD          
   ,c.CountryCD           
   ,H.[AltBusinessPhone]          
            ,H.[TollFreePhone]          
            ,H.[BusinessEmail]          
            ,H.[ContactBusinessPhone]          
            ,H.[CellPhoneNum]          
            ,H.[ContactEmail]         
            ,H.ExchangeRateID        
     ,H.NegotiatedTerms        
     ,H.SundayWorkHours        
     ,H.MondayWorkHours        
     ,H.TuesdayWorkHours        
     ,H.WednesdayWorkHours        
     ,H.ThursdayWorkHours        
     ,H.FridayWorkHours        
     ,H.SaturdayWorkHours         
 FROM  [Hotel] H          
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID           
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID          
          
 WHERE           
 H.AirportID = @UWAAirportId          
 AND H.CustomerID = @UWACustomerID          
 AND H.IsDeleted = 0
 AND H.HotelID = case when @HotelID <> 0 then @HotelID else H.HotelID end 
 AND H.HotelCD = case when Ltrim(Rtrim(@HotelCD)) <> '' then @HotelCD else H.HotelCD end 
 AND ISNULL(H.IsInActive,0) = case when @FetchInactiveOnly =1 then ISNULL(H.IsInActive,0) else 0 end
 AND ISNULL(H.IsCrew,0) = case when @FetchIsCrewOnly =1 then @FetchIsCrewOnly else  ISNULL(H.IsCrew,0) end
 AND ISNULL(H.IsPassenger,0) = case when @FetchIsPaxOnly =1 then @FetchIsPaxOnly else  ISNULL(H.IsPassenger,0) end        
 AND H.HotelID NOT IN           
 (SELECT HotelID FROM Hotel WHERE         
  (AirportID  = @CustomAirportID             
  AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId             
  AND  CustomerID = @CustomerID )         
  AND IsDeleted = 0        
 )          
           
 ORDER BY H.[Name]--HotelCD        
GO

