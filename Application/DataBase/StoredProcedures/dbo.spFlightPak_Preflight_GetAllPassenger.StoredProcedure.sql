
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllPassenger]    Script Date: 02/03/2014 09:48:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllPassenger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllPassenger]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllPassenger]    Script Date: 02/03/2014 09:48:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetAllPassenger]          
(@CustomerID bigint
,@ClientID bigint
,@FetchActiveOnly BIT=0
,@IsRequestor bit =0
,@ICAOID varchar(4) 
,@CQCustomerID bigint 
)        
AS          
BEGIN          
-- ====================================================          
-- Author: Prabhu.D  
-- Create date: 31/01/2012          
-- Description: Get All Passenger Requestor Information    

-- ====================================================        
  

SELECT   Passenger.PassengerRequestorID        
		,CASE WHEN Passenger.PassengerRequestorCD IS NULL THEN '' ELSE LTRIM(RTRIM(Passenger.PassengerRequestorCD)) END AS PassengerRequestorCD        
		,Passenger.PassengerName        
		,Passenger.CustomerID        
		,Passenger.DepartmentID        
		,Passenger.PassengerDescription        
		,Passenger.PhoneNum        
		,Passenger.IsEmployeeType        
		,Passenger.StandardBilling        
		,Passenger.AuthorizationID        
		,Passenger.AuthorizationDescription        
		,Passenger.Notes        
		,Passenger.LastName        
		,Passenger.FirstName        
		,Passenger.MiddleInitial        
		,Passenger.DateOfBirth        
		,Passenger.ClientID        
		,Passenger.AssociatedWithCD        
		,Passenger.HomebaseID        
		,Passenger.IsActive        
		,Passenger.FlightPurposeID        
		,Passenger.SSN        
		,Passenger.Title        
		,Passenger.SalaryLevel        
		,Passenger.IsPassengerType        
		,Passenger.LastUpdUID        
		,Passenger.LastUpdTS        
		,Passenger.IsScheduledServiceCoord        
		,Passenger.CompanyName        
		,Passenger.EmployeeID        
		,Passenger.FaxNum        
		,Passenger.EmailAddress        
		,Passenger.PersonalIDNum        
		,Passenger.IsSpouseDependant        
		,Passenger.AccountID        
		,Passenger.CountryID        
		,Passenger.IsSIFL        
		,Passenger.PassengerAlert        
		,Passenger.Gender        
		,Passenger.AdditionalPhoneNum        
		,Passenger.IsRequestor        
		,Passenger.CountryOfResidenceID        
		,Passenger.PaxScanDoc        
		,Passenger.TSAStatus        
		,Passenger.TSADTTM        
		,Passenger.Addr1        
		,Passenger.Addr2        
		,Passenger.City        
		,Passenger.StateName        
		,Passenger.PostalZipCD        
		,Passenger.IsDeleted          
		,Nationality.CountryName as NationalityName        
		,Nationality.CountryCD as NationalityCD        
		,Client.ClientCD        
		,Client.ClientDescription        
		,Account.AccountNum        
		,Account.AccountDescription        
		,Department.DepartmentCD        
		,Department.DepartmentName        
		,DepartmentAuthorization.AuthorizationCD        
		,DepartmentAuthorization.DeptAuthDescription        
		,FlightPurpose.FlightPurposeCD        
		,FlightPurpose.FlightPurposeDescription        
		,CountryOfResidence.CountryName as ResidenceCountryName        
		,CountryOfResidence.CountryCD as ResidenceCountryCD        
		,HomeBase.CityName as HomeBaseName        
		,HomeBase.IcaoID as HomeBaseCD  
		,Passenger.PrimaryMobile  
		,Passenger.BusinessFax  
		,Passenger.CellPhoneNum2  
		,Passenger.OtherPhone  
		,Passenger.PersonalEmail  
		,Passenger.OtherEmail  
		,Passenger.Addr3      
		,Passenger.Birthday  
		,Passenger.Anniversaries  
		,Passenger.EmergencyContacts  
		,Passenger.CreditcardInfo  
		,Passenger.CateringPreferences  
		,Passenger.HotelPreferences    
		,Passenger.[INSANumber]  
		,Passenger.[Category]  
		,Passenger.[GreenCardCountryID]  
		,Passenger.[CardExpires]  
		,Passenger.[ResidentSinceYear]  
		,Passenger.[GreenCardGender]
		,Passenger.cqcustomerid
		,CQ.CQCustomerCD 
		,PaxTitle
		,CityOfBirth
		,Nickname 
 FROM Passenger as Passenger        
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = Passenger.CountryID 
  LEFT OUTER JOIN cqcustomer as CQ on CQ.cqcustomerid = Passenger.cqcustomerid        
  LEFT OUTER JOIN Client as Client on Client.ClientID = Passenger.ClientID        
  LEFT OUTER JOIN Account as Account on Account.AccountID = Passenger.AccountID        
  LEFT OUTER JOIN Department as Department on Department.DepartmentID = Passenger.DepartmentID        
  LEFT OUTER JOIN DepartmentAuthorization as DepartmentAuthorization on DepartmentAuthorization.AuthorizationID = Passenger.AuthorizationID        
  LEFT OUTER JOIN Airport as HomeBase on HomeBase.AirportID = Passenger.HomebaseID        
  LEFT OUTER JOIN FlightPurpose as FlightPurpose on FlightPurpose.FlightPurposeID = Passenger.FlightPurposeID        
  LEFT OUTER JOIN Country as CountryOfResidence on CountryOfResidence.CountryID = Passenger.CountryOfResidenceID        
 WHERE Passenger.CustomerID = @CustomerID    
 and Passenger.isdeleted=0 
 and isnull(Passenger.ClientID,0)= case when @ClientID >0 then @ClientID else isnull(Passenger.ClientID,0) end
 and ISNULL(Passenger.IsActive,0) = case when @FetchActiveOnly =1 then 1 else ISNULL(Passenger.IsActive,0) end
 and ISNULL(Passenger.IsRequestor,0) = case when @IsRequestor =1 then 1 else ISNULL(Passenger.IsRequestor,0) end
 and ISNULL(HomeBase.IcaoID,'') = CASE WHEN ltrim(rtrim(@ICAOID)) <>'' then @ICAOID else ISNULL(HomeBase.IcaoID,'') end
 and isnull(Passenger.CQCustomerID,0)= case when @CQCustomerID >0 then @CQCustomerID else isnull(Passenger.CQCustomerID,0) end
--Exec spFlightPak_Preflight_GetAllPassenger 10003, 0, 1, 1, '',0      
 ORDER BY Passenger.PassengerName   
 
END   


GO


