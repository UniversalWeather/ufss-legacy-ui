/****** Object:  StoredProcedure [dbo].[spCQGETLostBusiness]    Script Date: 02/21/2013 13:55:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCQGETLostBusiness]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCQGETLostBusiness]
GO

/****** Object:  StoredProcedure [dbo].[spCQGETLostBusiness]    Script Date: 02/21/2013 13:55:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spCQGETLostBusiness]
(
@CUSTOMERID BIGINT
)
AS
BEGIN
SELECT
CQLostBusinessID,
CustomerID,
CQLostBusinessDescription,
LastUpdUID,
LastUpdTS,
IsDeleted,
CQLostBusinessCD
FROM CQLostBusiness WHERE CustomerID = @CUSTOMERID AND IsDeleted = 0

END
GO


