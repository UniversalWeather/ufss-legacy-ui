
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQInvoiceMain]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQInvoiceMain
GO

CREATE PROCEDURE [dbo].spFlightPak_CharterQuote_UpdateCQInvoiceMain
(
	@CQInvoiceMainID bigint,
	@CustomerID bigint,
	@CQMainID bigint,
	@QuoteID int,
	@InvoiceNUM int,
	@InvoiceDT date,
	@DispatchNUM varchar(12),
	@CharterCompanyInformation varchar(MAX),
	@CustomerAddressInformation varchar(MAX),
	@InvoiceHeader varchar(MAX),
	@InvoiceFooter varchar(MAX),
	@PageOff numeric(2, 0),
	@IsPrintDetail bit,
	@IsPrintFees bit,
	@IsPrintSum bit,
	@CQMessageID bigint,
	@IsInvoiceAdditionalFeeTax bit,
	@InvoiceAdditionalFeeTax numeric(5, 2),
	@IsDetailTax bit,
	@IsFlightChg bit,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@IsSumTax bit,
	@IsManualTax bit,
	@IsManualDiscount bit,
	@IsPrintQuoteNUM bit	
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQInvoiceMain] SET [InvoiceNUM] = @InvoiceNUM, [InvoiceDT] = @InvoiceDT, [DispatchNUM] = @DispatchNUM, [CharterCompanyInformation] = @CharterCompanyInformation, [CustomerAddressInformation] = @CustomerAddressInformation, [InvoiceHeader] = @InvoiceHeader,
	 [InvoiceFooter] = @InvoiceFooter, 
	 [PageOff] = @PageOff, [IsPrintDetail] = @IsPrintDetail, [IsPrintFees] = @IsPrintFees, [IsPrintSum] = @IsPrintSum, [CQMessageID] = @CQMessageID, [IsInvoiceAdditionalFeeTax] = @IsInvoiceAdditionalFeeTax, [InvoiceAdditionalFeeTax] = @InvoiceAdditionalFeeTax, [IsDetailTax] = @IsDetailTax, [IsFlightChg] = @IsFlightChg, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted, 
		IsSumTax = @IsSumTax, IsManualTax = @IsManualTax, IsManualDiscount = @IsManualDiscount, IsPrintQuoteNUM = @IsPrintQuoteNUM
	WHERE [CQInvoiceMainID] = @CQInvoiceMainID AND [CustomerID] = @CustomerID
	
GO

