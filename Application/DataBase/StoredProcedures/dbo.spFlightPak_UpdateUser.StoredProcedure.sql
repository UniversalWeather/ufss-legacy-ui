IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdateUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  

CREATE PROCEDURE [dbo].[spFlightPak_UpdateUser]      
 (@UserName char(30)      
 ,@CustomerID bigint      
 ,@HomebaseID bigint      
 ,@ClientID bigint      
 ,@FirstName varchar(60)      
 ,@MiddleName varchar(60)      
 ,@LastName varchar(60)      
 ,@HomeBase char(4)      
 ,@TravelCoordID bigint      
 ,@IsActive bit      
 ,@IsUserLock bit      
 ,@IsTripPrivacy bit      
 ,@EmailID varchar(100)      
 ,@PhoneNum varchar(100)      
 ,@PassengerRequestorID bigint      
 ,@UVTripLinkID varchar(50)      
 ,@UVTripLinkPassword varchar(60)      
 ,@IsPrimaryContact bit      
 ,@IsSecondaryContact bit      
 ,@IsSystemAdmin bit      
 ,@IsDispatcher bit      
 ,@IsOverrideCorp bit      
 ,@IsCorpRequestApproval bit      
 ,@LastUpdUID char(30)      
 ,@LastUpdTS datetime      
 ,@IsDeleted bit)      
as      
Begin       
Set NoCount On      
      
-- =============================================      
-- Author:  PremPrakash.s      
-- Create date: 15-may-2012      
-- Description:Adding user name       
-- =============================================      
-- MODIFIED BY : SRINIVASAN.J      
-- MODIFIED DATE : 29-JUNE-2012      
-- DESCRIPTION : PROCEDURE MODIFIED AFTER TABLE MODIFIED      
-- =============================================      
----------------------------------------------------------------------------------------------------      

IF(@IsPrimaryContact = 'true')      
BEGIN
	UPDATE UserMaster Set IsPrimaryContact = 'false' where CustomerID = @CustomerID
END
IF(@IsSecondaryContact = 'true')      
BEGIN
	UPDATE UserMaster Set IsSecondaryContact = 'false' where CustomerID = @CustomerID
END
SET @LastUpdTS = GETUTCDATE() 
UPDATE [dbo].[UserMaster]      
   SET [CustomerID] = @CustomerID      
           ,[HomebaseID] = @HomebaseID      
           ,[ClientID] = @ClientID      
           ,[FirstName] = @FirstName      
           ,[MiddleName] = @MiddleName      
           ,[LastName] = @LastName      
           ,[HomeBase] = @HomeBase      
           ,[TravelCoordID] = @TravelCoordID      
           ,[IsActive] = @IsActive      
           ,[IsUserLock] = @IsUserLock      
           ,[IsTripPrivacy] = @IsTripPrivacy      
           ,[EmailID] = @EmailID      
           ,[PhoneNum] = @PhoneNum      
           ,[PassengerRequestorID] = @PassengerRequestorID      
           ,[UVTripLinkID] = @UVTripLinkID      
           ,[UVTripLinkPassword] = @UVTripLinkPassword      
           ,[IsPrimaryContact] = @IsPrimaryContact      
           ,[IsSecondaryContact] = @IsSecondaryContact      
           ,[IsSystemAdmin] = @IsSystemAdmin      
           ,[IsDispatcher] = @IsDispatcher      
           ,[IsOverrideCorp] = @IsOverrideCorp      
           ,[IsCorpRequestApproval] = @IsCorpRequestApproval      
           ,[LastUpdUID] = @LastUpdUID      
           ,[LastUpdTS] = @LastUpdTS      
           ,[IsDeleted] = @IsDeleted      
 WHERE [UserName] = @UserName      
    DELETE FROM UserGroupMapping where UserName = @UserName  
End

