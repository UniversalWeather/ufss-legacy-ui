
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetFleetIDInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetFleetIDInfo]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      
CREATE Procedure [dbo].[spGetFleetIDInfo]          
(    
 @FleetID BIGINT=0  
)                  
AS                  
-- =============================================                  
-- Author: Madhes                 
-- Create date: 22-09-2012                   
-- Description: Get Fleet Profile Information for FleetID                
-- =============================================                  
SET NOCOUNT ON          
BEGIN                
               
    SELECT [FleetID]                
   ,[TailNum]                
   ,Fleet.CustomerID                 
   ,Fleet.AircraftCD                
   ,Aircraft.AircraftCD as AirCraft_AircraftCD                
   ,[SerialNum]                
   ,[TypeDescription]                
   ,[MaximumReservation]                
   ,[LastInspectionDT]                
   ,[InspectionHrs]                
   ,[WarningHrs]                
   ,[MaximumPassenger]                
   ,Fleet.HomebaseID                
   ,[MaximumFuel]                
   ,[MinimumFuel]                
   ,[BasicOperatingWeight]                
   ,[MimimumRunway]                
   ,IsNull(Fleet.IsInActive,0) As IsInActive       
   ,[IsDisplay31]                
   ,[SIFLMultiControlled]                
   ,[SIFLMultiNonControled]                
   ,Fleet.Notes                
   ,[ComponentCD]                
   ,Fleet.ClientID                
   ,[FleetType]                
   ,[FlightPhoneNum]                
   ,[Class]                
   ,Fleet.VendorID                
   ,Fleet.VendorType                
   ,[YearMade]                
   ,[ExteriorColor]                
   ,[InteriorColor]                
   ,[IsAFIS]                
   ,[IsUWAData]                
   ,Fleet.LastUpdUID                
   ,Fleet.LastUpdTS                
   ,[FlightPlanCruiseSpeed]                
   ,[FlightPlanMaxFlightLevel]                
   ,[MaximumTakeOffWeight]                
   ,[MaximumLandingWeight]                
   ,[MaximumWeightZeroFuel]                
   ,[TaxiFuel]                
   ,[MultiSec]                
   ,[ForeGrndCustomColor]                
   ,[BackgroundCustomColor]                
   ,[FlightPhoneIntlNum]                
   ,[FleetSize]                
   ,[FltScanDoc]                
   ,[MinimumDay]                
   ,[StandardCrewIntl]                
   ,[StandardCrewDOM]                
   ,[IsFAR91]                
   ,[IsFAR135]                
   ,Fleet.EmergencyContactID                
   ,[IsTaxDailyAdj]                
   ,[IsTaxLandingFee]                
   ,Fleet.IsDeleted                
   ,Fleet.AircraftID                
   ,Fleet.CrewID          
   ,Fleet.SecondaryDomFlightPhone          
   ,Fleet.SecondaryIntlFlightPhone                
   ,EmergencyContact.EmergencyContactCD, Client.ClientCD, Vendor.VendorCD,          
    Aircraft.AircraftDescription, Crew.CrewCD,Aircraft.IsFixedRotary  ,
    Airport.IcaoID as HomeBaseCD,Fleet.NationalityCountryID,C.CountryName                         
    FROM         Airport RIGHT OUTER JOIN
                      Company ON Airport.AirportID = Company.HomebaseAirportID RIGHT OUTER JOIN
                      Fleet ON Company.HomebaseID = Fleet.HomebaseID LEFT OUTER JOIN
                      Vendor ON Fleet.VendorID = Vendor.VendorID LEFT OUTER JOIN
                      Crew ON Fleet.CrewID = Crew.CrewID LEFT OUTER JOIN
                      Client ON Fleet.ClientID = Client.ClientID LEFT OUTER JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID LEFT OUTER JOIN
                      EmergencyContact ON Fleet.EmergencyContactID = EmergencyContact.EmergencyContactID LEFT OUTER JOIN
            Country C ON C.CountryID=Fleet.NationalityCountryID                 
                WHERE Fleet.FleetID =@FleetID      
   END