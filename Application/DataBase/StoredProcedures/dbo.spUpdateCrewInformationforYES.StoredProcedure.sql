
/****** Object:  StoredProcedure [dbo].[spUpdateCrewInformationforYES]    Script Date: 04/18/2013 16:01:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewInformationforYES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewInformationforYES]
GO



/****** Object:  StoredProcedure [dbo].[spUpdateCrewInformationforYES]    Script Date: 04/18/2013 16:01:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spUpdateCrewInformationforYES]      
(      
  @CrewInfoID BIGINT,    
  @CrewInfoCD CHAR(3),      
  @CustomerID BIGINT,      
  @CrewInformationDescription VARCHAR(40),      
  @LastUpdUID VARCHAR(30),      
  @LastUpdTS DATETIME,      
  @IsDeleted BIT      
)      
      
-- =============================================      
-- Author:A.Akhila      
-- Create date: 7/5/2012      
-- Description: Update the Crew Roster Additional Information      
-- =============================================      
      
AS      
BEGIN      
SET NOCOUNT ON      
set @LastUpdTS = GETUTCDATE()    
 UPDATE [CrewInformation]      
       SET  [CrewInfoID]=@CrewInfoID    
           ,[CrewInfoCD]=@CrewInfoCD    
           ,[CustomerID]=@CustomerID    
           ,[CrewInformationDescription]=@CrewInformationDescription    
           ,[LastUpdUID]=@LastUpdUID    
           ,[LastUpdTS]=@LastUpdTS    
           ,[IsDeleted]=@IsDeleted             
      WHERE [CustomerID]=@CustomerID     
      AND [CrewInfoID]=@CrewInfoID  
      
     update crewdefinition set lastupdts = @LastUpdTS,LastUpdUID = @LastUpdUID,InformationDESC = @CrewInformationDescription where crewinfoid = @CrewInfoID and CustomerID=@CustomerID
    

 -- Cursot to get the values of crew which are not having the specified crewinfoid  
   DECLARE  @CrewID BIGINT  
    DECLARE curTailInfo CURSOR FOR SELECT DISTINCT CREWID FROM CREW WHERE crewid  not in (select crewid from crewdefinition where crewinfoid = @crewinfoid) AND CustomerID = @CustomerID
        
 OPEN curTailInfo; 
            
 -- PRINT @@CURSOR_ROWS            
  FETCH NEXT FROM curTailInfo INTO @CREWID;   
  WHILE @@FETCH_STATUS = 0            
  BEGIN  
  BEGIN          
  --INSERT INTO()
 EXEC spFlightPak_AddCrewDefinition NULL,@CrewID,@CustomerID,@CrewInfoID,@CrewInformationDescription,NULL,@LastUpdUID,@LastUpdTS,0,0 
 
  END  
   FETCH NEXT FROM curTailInfo INTO  @CREWID
              
 END             
 CLOSE curTailInfo;            
 DEALLOCATE curTailInfo; 
 
      

END    

GO


