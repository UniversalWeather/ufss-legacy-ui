

/****** Object:  StoredProcedure [dbo].[spCheckLock]    Script Date: 09/13/2012 12:43:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckLock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheckLock]
GO


/*            
Author:   Viswanathan Perumal            
Purpose:  Get Locking status for a row, 
Created Date: 12-07-2012            
Modified By:             
Modified Date:             
*/            
CREATE PROC [dbo].[spCheckLock]            
 @EntityType [varchar](20),            
 @SessionID [uniqueidentifier],            
 @EntityPrimaryKey [bigint]
AS            
BEGIN            
   -- Check record is already locked by same user with same session id. 
   SELECT ISNULL(COUNT(FPLockID),0) FROM FPLockLog WHERE EntityType = @EntityType AND EntityPrimaryKey = @EntityPrimaryKey AND SessionID = @SessionID
           
END  
GO


