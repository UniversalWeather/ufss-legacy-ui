
GO
/****** Object:  StoredProcedure [dbo].[spDeleteFeeSchedule]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteFeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteFeeSchedule]
GO

CREATE PROCEDURE spDeleteFeeSchedule
(@FeeScheduleID BIGINT,
@CustomerID BIGINT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [FeeSchedule]
	SET [LastUpdUID] = @LastUpdUID
		,[LastUpdTS] = @LastUpdTS
		,[IsDeleted] = @IsDeleted
	WHERE [FeeScheduleID] = @FeeScheduleID

END