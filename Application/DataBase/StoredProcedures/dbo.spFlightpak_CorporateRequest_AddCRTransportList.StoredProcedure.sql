/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_AddCRTransportList]    Script Date: 04/25/2013 17:46:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_AddCRTransportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRTransportList]
GO

CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_AddCRTransportList]
(--@CRTransportListID BIGINT,
@CustomerID BIGINT
,@CRLegID BIGINT
,@TripNUM BIGINT
,@LegID BIGINT
,@RecordType VARCHAR(2)
,@TransportID BIGINT
,@CRTransportListDescription VARCHAR(60)
,@PhoneNUM VARCHAR(25)
,@IsCompleted BIT
,@AirportID BIGINT
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@FaxNum VARCHAR(25)
,@Email VARCHAR(250)
,@Rate numeric(6, 2)
)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()	
	DECLARE @CRTransportListID BIGINT
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRTransportListID OUTPUT
		
	INSERT INTO CRTransportList
		([CRTransportListID]
		,[CustomerID]
		,[CRLegID]
		,[TripNUM]
		,[LegID]
		,[RecordType]
		,[TransportID]
		,[CRTransportListDescription]
		,[PhoneNUM]
		,[IsCompleted]
		,[AirportID]
		,[LastUpdUID]
		,[LastUpdTS]
		,[IsDeleted]
		,[FaxNum]
		,[Email]
		,[Rate]
		)
	VALUES
		(@CRTransportListID
		,@CustomerID
		,@CRLegID
		,@TripNUM
		,@LegID
		,@RecordType
		,@TransportID
		,@CRTransportListDescription
		,@PhoneNUM
		,@IsCompleted
		,@AirportID
		,@LastUpdUID
		,@LastUpdTS
		,@IsDeleted
		,@FaxNum
		,@Email
		,@Rate
		)
		
	SELECT @CRTransportListID AS CRTransportListID
	
END
GO