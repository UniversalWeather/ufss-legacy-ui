IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetSaltValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetSaltValue]
GO
/****** Object:  StoredProcedure [dbo].[spGetSaltValue]    Script Date: 08/25/2015 15:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


-- =============================================  
-- Author:  Vishwanathan Perumal  
-- Create date: 10-22-2012  
-- Description: This is used to get the salt value of a particular user  
-- =============================================  
CREATE PROCEDURE [dbo].[spGetSaltValue]  
 @EmailId varchar(100),  
 @SaltLength int,  
 @PasswordLength int  
AS  
BEGIN  
 SET NOCOUNT ON;  
    Declare @UserName varchar(30)
    SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId              
    SELECT SUBSTRING(FPPWD, @PasswordLength + 1, @SaltLength) AS SaltValue FROM UserPassword WHERE UserName = @UserName  
    AND UserPassword.ActiveStartDT <= GETUTCDATE() AND (UserPassword.ActiveEndDT IS NULL OR UserPassword.ActiveEndDT >= GETUTCDATE()) AND PWDStatus = 1      
END

GO

END