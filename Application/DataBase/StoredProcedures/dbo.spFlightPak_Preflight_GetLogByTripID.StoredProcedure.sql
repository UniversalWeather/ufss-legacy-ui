
/****** Object:  StoredProcedure [dbo].[spFlightPak_spGetPassengerGroupbyGroupID]    Script Date: 07/30/2013 17:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spFlightPak_Preflight_GetLogByTripID]
(
@CustomerID BIGINT,
@TripID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get PostflightMain by Trip ID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
*
FROM PostflightMain PM 
 where PM.TripID = @TripID and PM.CustomerID=@CustomerID and PM.IsDeleted=0
