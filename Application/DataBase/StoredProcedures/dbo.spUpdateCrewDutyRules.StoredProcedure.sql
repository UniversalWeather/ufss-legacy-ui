
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCrewDutyRules]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCrewDutyRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCrewDutyRules]
go
CREATE procedure [dbo].[spUpdateCrewDutyRules](
	@CrewDutyRulesID bigint ,
	@CrewDutyRuleCD char(4) ,
	@CustomerID bigint,
	@CrewDutyRulesDescription varchar(30), 
	@FedAviatRegNum char(3) ,
	@DutyDayBeingTM NUMERIC(6,3) ,
	@DutyDayEndTM NUMERIC(6,3) ,
	@MaximumDutyHrs NUMERIC(6,3) ,
	@MaximumFlightHrs NUMERIC(6,3) ,
	@MinimumRestHrs NUMERIC(6,3),
	@RestMultiple NUMERIC(4,2),
	@RestMultipleHrs NUMERIC(6,3),
	@LastUpdUID varchar(30) ,
	@LastUptTM datetime,
    @IsDeleted bit,
    @IsInActive bit )

-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 4/5/2012
-- Description: Update the CrewDutyRules information
-- =============================================
as
begin 
SET NoCOUNT ON
set @LastUptTM = GETUTCDATE()
UPDATE [CrewDutyRules]
   SET
    CrewDutyRulesID = @CrewDutyRulesID, 
	CrewDutyRuleCD=@CrewDutyRuleCD,
	[CustomerID]=@CustomerID,
	CrewDutyRulesDescription=@CrewDutyRulesDescription , 
	FedAviatRegNum=@FedAviatRegNum  ,
	DutyDayBeingTM=@DutyDayBeingTM  ,
	DutyDayEndTM=@DutyDayEndTM  ,
	MaximumDutyHrs=@MaximumDutyHrs  ,
	MaximumFlightHrs=@MaximumFlightHrs  ,
	MinimumRestHrs=@MinimumRestHrs ,
	RestMultiple=@RestMultiple,
	RestMultipleHrs=@RestMultipleHrs,
	LastUpdUID=@LastUpdUID,
	LastUptTM=@LastUptTM,
    IsDeleted=@IsDeleted,
    IsInActive=@IsInActive
            
 WHERE CrewDutyRulesID = @CrewDutyRulesID

end
GO
