GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomerFuelVendorAccess]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCustomerFuelVendorAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCustomerFuelVendorAccess]
go
CREATE PROCEDURE [dbo].[spUpdateCustomerFuelVendorAccess](  
						    @CustomerFuelVendorAccessID bigint,
							@CustomerID bigint,
							@FuelVendorID bigint,
							@CanAccess bit,
							@LastUpdUID varchar(30),
							@LastUpdTS DATETIME,
							@IsDeleted bit,
							@IsInActive bit
						)    
-- =============================================      
-- Author:Ajeet      
-- Create date: 10/16/2015      
-- Description: Update Fuel Vedor Access for Customer      
-- =============================================      
AS      
BEGIN       
SET NOCOUNT ON    
IF  EXISTS(SELECT CustomerFuelVendorAccessID FROM CustomerFuelVendorAccess WHERE CustomerFuelVendorAccessID=@CustomerFuelVendorAccessID)
BEGIN
  UPDATE CustomerFuelVendorAccess    
  SET CustomerID=@CustomerID,
	  FuelVendorID=@FuelVendorID,
	  CanAccess=@CanAccess,
	  LastUpdUID=@LastUpdUID,
	  LastUpdTS=@LastUpdTS,
	  IsDeleted=@IsDeleted,
	  IsInActive=@IsInActive
  WHERE CustomerFuelVendorAccessID = @CustomerFuelVendorAccessID
END
ELSE
BEGIN
	INSERT INTO CustomerFuelVendorAccess (CustomerID,FuelVendorID,CanAccess,LastUpdUID,LastUpdTS,IsDeleted,IsInActive)    
    VALUES (@CustomerID,@FuelVendorID,@CanAccess,@LastUpdUID,@LastUpdTS,@IsDeleted,@IsInActive)  
END
SELECT @CustomerFuelVendorAccessID as CustomerFuelVendorAccess
END