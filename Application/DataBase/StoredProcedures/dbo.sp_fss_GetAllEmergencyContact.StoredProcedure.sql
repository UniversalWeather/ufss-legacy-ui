
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllEmergencyContact]    Script Date: 03/13/2014 16:25:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllEmergencyContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllEmergencyContact]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllEmergencyContact]    Script Date: 03/13/2014 16:25:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fss_GetAllEmergencyContact]
(
	@CustomerID  BIGINT
	,@EmergencyContactID BIGINT  
    ,@EmergencyContactCD varchar(5)    
	,@FetchActiveOnly BIT  
)      
AS      
-- =============================================              
-- Author: Sridhar             
-- Create date: 13/03/2014              
-- Description: Get All EmergencyContact details for Popup        
-- Exec sp_fss_GetAllEmergencyContact 10003,0,'',1   
-- Exec sp_fss_GetAllEmergencyContact 10003,0,'',0  
-- Exec sp_fss_GetAllEmergencyContact 10003, 10003181146,NULL, 0  
-- Exec sp_fss_GetAllEmergencyContact 10003, 0,'0001',0  
-- =============================================  
SET NOCOUNT ON      
      
BEGIN      
	SELECT  EC.EmergencyContactID    
           ,EC.EmergencyContactCD    
           ,EC.CustomerID    
           ,EC.LastName    
           ,EC.FirstName    
           ,EC.MiddleName    
           ,EC.Addr1    
           ,EC.Addr2    
           ,EC.CityName    
           ,EC.StateName    
           ,EC.PostalZipCD    
           ,EC.CountryID    
           ,EC.PhoneNum    
           ,EC.FaxNum    
           ,EC.CellPhoneNum
           ,EC.EmailAddress    
           ,EC.LastUpdUID    
           ,EC.LastUpdTS    
           ,ISNULL(EC.IsDeleted,0) IsDeleted
           ,EC.BusinessPhone
		   ,EC.BusinessFax
		   ,EC.CellPhoneNum2
		   ,EC.OtherPhone
		   ,EC.PersonalEmail
		   ,EC.OtherEmail
		   ,EC.Addr3    
		   ,ISNULL(EC.IsInactive,0) IsInactive 
	FROM  
		EmergencyContact EC    
	WHERE 
			CustomerID =@CustomerID      
		AND EC.EmergencyContactID = case when @EmergencyContactID <>0 then @EmergencyContactID else EC.EmergencyContactID end   
		AND EC.EmergencyContactCD = case when Ltrim(Rtrim(@EmergencyContactCD)) <> '' then @EmergencyContactCD else EC.EmergencyContactCD end                                    
		AND ISNULL(EC.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(EC.IsInActive,0) else 0 end  
		AND ISNULL(EC.IsDeleted,0) = 0  
	order 
		by  EC.EmergencyContactCD  
END


GO


