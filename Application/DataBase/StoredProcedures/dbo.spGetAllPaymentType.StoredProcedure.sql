IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllPaymentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllPaymentType]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllPaymentType]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllPaymentType](@CustomerID BIGINT)
as
-- =============================================
-- Author:Hamsha
-- Create date: 04/05/2012
-- Description: Get the PaymentType information
-- =============================================
set nocount on
begin 
	
	SELECT PaymentTypeID
      ,CASE WHEN [PaymenttypeCD] IS NULL THEN '' ELSE LTRIM(RTRIM([PaymenttypeCD])) END AS PaymenttypeCD
      ,[CustomerID]
      ,[PaymentTypeDescription]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive
  
  FROM PaymentType WHERE CustomerID=@CustomerID  and IsDeleted = 'false'
   order by PaymenttypeCD

end
GO
