

/****** Object:  StoredProcedure [dbo].[spFlightPak_GetExceptionTemplate]    Script Date: 09/19/2013 03:44:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetExceptionTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetExceptionTemplate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_GetExceptionTemplate]    Script Date: 09/19/2013 03:44:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spFlightPak_GetExceptionTemplate]
(
@ModuleID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get Preflightmain by Trip ID
-- =============================================    
SET NOCOUNT ON    
    
select ExceptionTemplateID,
ModuleID,
SubModuleID,
ExceptionTemplate,
Severity,
IsInActive,
 ExceptionTemplate as ExceptionTemplateDescription from ExceptionTemplate where ModuleID= @ModuleID


GO


