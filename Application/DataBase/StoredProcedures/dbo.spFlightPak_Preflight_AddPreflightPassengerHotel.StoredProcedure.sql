
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightPassengerHotel]    Script Date: 04/01/2013 16:13:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightPassengerHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightPassengerHotel]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddPreflightPassengerHotel]    Script Date: 04/01/2013 16:13:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightPassengerHotel]    
 (        
           @PreflightHotelName varchar(60),    
           @PreflightPassengerListID bigint,    
           @HotelID bigint,    
           @AirportID bigint,    
           @RoomType varchar(60),    
           @RoomRent numeric(6,2),    
           @RoomConfirm text,    
           @RoomDescription text,    
           @Street varchar(50),    
           @CityName varchar(20),    
           @StateName varchar(20),    
           @PostalZipCD char(20),    
           @PhoneNum1 varchar(25),    
           @PhoneNum2 varchar(25),    
           @PhoneNum3 varchar(25),    
           @PhoneNum4 varchar(25),    
           @FaxNUM varchar(25),    
           @LastUpdUID char(30),    
           @LastUpdTS datetime,    
           @IsDeleted bit,    
           @HotelArrangedBy varchar(10),    
           @Rate numeric(17,2),    
           @DateIn date,    
           @DateOut date,    
           @IsSmoking bit,    
           @RoomTypeID bigint,    
           @NoofBeds numeric(3,0),    
           @IsAllCrew bit,    
           @IsCompleted bit,    
           @IsEarlyCheckIn bit,    
           @EarlyCheckInOption char(1),    
           @SpecialInstructions varchar(100),    
           @ConfirmationStatus varchar(max),    
           @Comments varchar(max),    
           @CustomerID bigint                
     ,@Address1 varchar(40)    
     ,@Address2 varchar(40)    
     ,@Address3 varchar(40)    
   ,@MetroID bigint    
   ,@City varchar(30)    
   ,@StateProvince varchar(25)    
   ,@CountryID bigint    
   ,@PostalCode varchar(15)    
   ,@Status varchar(20)     
   )    
AS    
BEGIN     
      
  DECLARE @PreflightPassengerHotelListID BIGINT       
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo', @PreflightPassengerHotelListID  OUTPUT    
    
     
  INSERT INTO [PreflightPassengerHotelList]    
           ([PreflightPassengerHotelListID]    
           ,[PreflightHotelName]    
           ,[PreflightPassengerListID]    
           ,[HotelID]    
           ,[AirportID]    
           ,[RoomType]    
           ,[RoomRent]    
           ,[RoomConfirm]    
           ,[RoomDescription]    
           ,[Street]    
           ,[CityName]    
           ,[StateName]    
           ,[PostalZipCD]    
           ,[PhoneNum1]    
           ,[PhoneNum2]    
           ,[PhoneNum3]    
           ,[PhoneNum4]    
           ,[FaxNUM]    
           ,[LastUpdUID]    
           ,[LastUpdTS]    
           ,[IsDeleted]    
           ,[HotelArrangedBy]    
           ,[Rate]    
           ,[DateIn]    
           ,[DateOut]    
           ,[IsSmoking]    
           ,[RoomTypeID]    
           ,[NoofBeds]    
           ,[IsAllCrew]    
           ,[IsCompleted]    
           ,[IsEarlyCheckIn]    
           ,[EarlyCheckInOption]    
           ,[SpecialInstructions]    
           ,[ConfirmationStatus]    
           ,[Comments]    
           ,[CustomerID]            
           ,[Address1]    
           ,[Address2]    
           ,[Address3]    
           ,[MetroID]    
           ,[City]    
           ,[StateProvince]    
           ,[CountryID]    
           ,[PostalCode]    
           ,[Status]    
           )    
     VALUES    
           (    
              
     @PreflightPassengerHotelListID ,    
           @PreflightHotelName,    
           @PreflightPassengerListID,    
           @HotelID,    
           @AirportID,    
           @RoomType,    
           @RoomRent,    
           @RoomConfirm,    
           @RoomDescription,    
           @Street,    
           @CityName,    
           @StateName,    
           @PostalZipCD,    
           @PhoneNum1,    
           @PhoneNum2,    
           @PhoneNum3,    
           @PhoneNum4,    
           @FaxNUM,    
           @LastUpdUID,    
           @LastUpdTS,    
           @IsDeleted,    
           @HotelArrangedBy,    
           @Rate,    
           @DateIn,    
           @DateOut,    
           @IsSmoking,    
           @RoomTypeID,               @NoofBeds,    
           @IsAllCrew,    
           @IsCompleted,    
           @IsEarlyCheckIn,    
           @EarlyCheckInOption,    
           @SpecialInstructions,    
           @ConfirmationStatus,    
           @Comments,    
           @CustomerID,          
           @Address1,    
           @Address2,    
           @Address3,    
           @MetroID,    
           @City,    
           @StateProvince,    
           @CountryID,    
           @PostalCode,    
           @Status    
           )    
            
  Select @PreflightPassengerHotelListID as PreflightPassengerHotelListID    
      
             
     
END    
    
    
    




GO


