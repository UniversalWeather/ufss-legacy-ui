/****** Object:  StoredProcedure [dbo].[spGetAllCatering]    Script Date: 01/07/2013 18:43:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllCatering]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllCatering]    Script Date: 01/07/2013 18:43:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllCatering](@CustomerID BIGINT)  
as  
-- =============================================  
-- Author:D.Mullai  
-- Create date: 24/4/2012  
-- Altered by Karthikeyan.S 04/07/2012  
-- Description: Get the Catering information  
-- =============================================  
set nocount on  
begin   
 SELECT      
 a.[CateringID]  
 ,a.[CustomerID]   
          ,a.[AirportID]   
          ,a.[CateringCD]   
          ,a.[IsChoice]   
          ,a.[CateringVendor]    
          ,a.[PhoneNum]   
          ,a.[FaxNum]   
          ,a.[ContactName]   
          ,a.[Remarks]   
          ,a.[NegotiatedRate]   
          ,a.[LastUpdUID]   
          ,a.[LastUpdTS]   
          ,a.[UpdateDT]   
          ,a.[RecordType]   
          ,a.[SourceID]   
          ,a.[ControlNum]   
          ,a.[IsInActive]   
          ,a.[TollFreePhoneNum]   
          ,a.[WebSite]   
          ,a.[UWAMaintFlag]  
          ,a.[UWAID]   
          ,a.[IsDeleted]  
          ,a.[UWAUpdates]  
          ,a.[BusinessEmail]  
          ,a.[ContactBusinessPhone]  
          ,a.[CellPhoneNum]  
          ,a.[ContactEmail]  
          ,a.[Addr1]  
          ,a.[Addr2]  
          ,a.[Addr3]  
          ,a.[City]  
          ,a.[StateProvince]   
          ,a.[MetroID]  
          ,a.[CountryID]  
          ,M.MetroCD  
          ,C.CountryCD  
          ,a.[IsInActive] 
		  ,a.[ExchangeRateID]
          ,a.[NegotiatedTerms]
          ,a.[SundayWorkHours]
          ,a.[MondayWorkHours]
          ,a.[TuesdayWorkHours]
          ,a.[WednesdayWorkHours]
          ,a.[ThursdayWorkHours]
          ,a.[FridayWorkHours]
          ,a.[SaturdayWorkHours]    
          ,M.MetroCD
          ,C.CountryCD   
	         
 FROM  [Catering] a LEFT OUTER JOIN [metro] M on a.MetroID = M.MetroID      
  LEFT OUTER JOIN [Country] C on a.CountryID = C.CountryID ,    
   
 (  
  SELECT CateringID,CustomerID, AirportID, CateringCD FROM Catering  WHERE   
  (AirportID+CateringCD in (SELECT AirportID+CateringCD FROM Catering WHERE IsDeleted ='false' AND CustomerID = @CustomerID GROUP BY AirportID+CateringCD )  
  and IsDeleted ='false' AND CustomerID = @CustomerID)  
  UNION  
    
  SELECT CateringID,CustomerID, AirportID, CateringCD FROM Catering  WHERE   
  (UWAMaintFlag='True' or CustomerID =@CustomerID)   
  and AirportID+CateringCD not in (SELECT AirportID+CateringCD FROM Catering WHERE IsDeleted ='false'and UWAMaintFlag = 'false' GROUP BY AirportID+CateringCD )  
  and IsDeleted = 'false'  
 ) b  
 WHERE a.CateringID = b.CateringID and a.CustomerID = b.CustomerID and a.CateringCD=b.CateringCD and  a.AirportID = b.AirportID ORDER BY UWAMaintFlag  
end  
GO


