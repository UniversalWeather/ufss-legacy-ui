
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetLocatorHotel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetLocatorHotel]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetLocatorHotel]
(
 @CustomerID BIGINT, 
 @AirportID BIGINT,
 @IcaoID char(4), 
 @Iata char(3),
 @StateName varchar(25),
 @CityName varchar(25),
 @CountryCD char(3),
 @CountryID BIGINT, 
 @MetroID BIGINT, 
 @MetroCD char(3),
 @HotelName varchar(40),
 @MilesFrom int

 )  
AS  
-- =============================================  
-- Author: 
-- Create date:   
-- Description: Get the Locator information of Hotel 
-- Modification History: Updated by Ramesh on 5th Nov 2013, for MapQuest plotting
--						 Added 2 new columns OrigLatitude and OrigLongitude
-- =============================================  
SET FMTONLY OFF  
DECLARE @CustomerID1 Bigint
If (@AirportID > 0)
BEGIN
select @CustomerID1  = customerid from airport where airportid = @AirportID 
set @CustomerID = @CustomerID1 		
END

 if  @IcaoID ='' 
		set @IcaoID = null
		 if @Iata ='' 
		set @Iata = null
		 if @CountryCD ='' 
		set @CountryCD = null
		 if @CityName ='' 
		set @CityName = null
		 if @MetroCD ='' 
		set @MetroCD = null
		 if @StateName ='' 
		set @StateName = null
		 if @HotelName ='' 
		set @HotelName = null		


SELECT   A.[AirportID]
        ,A.[IcaoID]
        ,A.[Iata]
        , H.[CustomerID]
		,H.[CityName]
		, H.[StateName]				
		, H.[CountryID]
		,H.Name			
		, H.[MetroID]
		,M.MetroCD
		,C.CountryCD
		,0 as 'Miles'
		,H.NegociatedRate,
		H.PhoneNum,
		H.HotelRating,
		H.MilesFromICAO,
		
  --start added for miles calc
	case 
	when UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180 
	END LatitudeRad,		
	case
	when UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180 
	END LongitudeRad,	
 --end added for miles calc	     
--start added for miles calc
	case 
	when UPPER(H.LatitudeNorthSouth) = 'S' Then (0-(H.LatitudeDegree + (H.LatitudeMinutes / 60))) * PI() / 180  else (H.LatitudeDegree + (H.LatitudeMinutes / 60)) * PI() / 180 
	END LatitudeRadCityCenter,
	case
	when UPPER(H.LatitudeNorthSouth) = 'S' Then (0-(H.LatitudeDegree + (H.LatitudeMinutes / 60))) else (H.LatitudeDegree + (H.LatitudeMinutes / 60))
	END OrigLatitude,
	case
	when UPPER(H.LongitudeEastWest) = 'W' Then (0-(H.LongitudeDegrees + (H.LongitudeMinutes / 60))) * PI() / 180  else (H.LongitudeDegrees + (H.LongitudeMinutes / 60)) * PI() / 180 
	END LongitudeRadCityCenter, 
	case
	when UPPER(H.LongitudeEastWest) = 'W' Then (0-(H.LongitudeDegrees + (H.LongitudeMinutes / 60))) else (H.LongitudeDegrees + (H.LongitudeMinutes / 60))
	END OrigLongitude
--end added for miles calc			
	into  #TempResultTable			
			
	 FROM  [Hotel] H
	 LEFT OUTER JOIN Airport A on H.AirportID = A.AirportID 
     LEFT OUTER JOIN [metro] M on H.MetroID = M.MetroID    
     LEFT OUTER JOIN [Country] C on H.CountryID = C.CountryID  
  -- LEFT OUTER JOIN [Airport] A on H.AirportID = A.AirportID  
  
	  where 
	(((H.CustomerID = @CustomerID)) 
   OR (ISNULL(@AirportID,0))=0 OR H.AirportID = @AirportID) and
	 ISNULL(A.IcaoID, '') = case when @IcaoID is not null then @IcaoID else ISNULL(A.IcaoID, '') end and
   ISNULL(A.Iata, '') = case when @Iata is not null then @Iata else ISNULL(A.Iata, '') end and
   ISNULL(C.CountryCD, '') = case when @CountryCD  is not null then @CountryCD  else ISNULL(C.CountryCD , '') end  and
   ISNULL(M.MetroCD, '') = case when @MetroCD is not null then @MetroCD else ISNULL(M.MetroCD, '') end and
   ISNULL(H.CityName, '') = case when @CityName is not null then @CityName else ISNULL(H.CityName, '') end and
   ISNULL(H.StateName, '') = case when @StateName is not null then @StateName else ISNULL(H.StateName, '') end and
   ISNULL(H.Name, '') = case when @HotelName is not null then @HotelName else ISNULL(H.Name, '') end and
	 --(H.CityName = @CityName   OR @CityName is NULL Or   @CityName = '')  and
	 --(H.StateName = @StateName   OR @StateName is NULL Or   @StateName = '')  and
	 --  (H.Name = @HotelName   OR @HotelName is NULL Or   @HotelName = '')  and 
		   H.IsDeleted = 0
	AND H.CustomerID=@CustomerID
	AND  H.MilesFromICAO  <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END
 
SELECT   [AirportID]
        ,[IcaoID]
        ,[Iata]
        ,[CustomerID]
		,[CityName]
		,[StateName]				
		,[CountryID]
		,Name			
		,[MetroID]
		,MetroCD
		,CountryCD
		-- Commented to directly access from field MilesFromICAO
		--,ISNull((cast( acos(sin(LatitudeRadCityCenter) * sin(LatitudeRad) + cos(LatitudeRadCityCenter) * cos(LatitudeRad) * cos(LongitudeRad - (LongitudeRadCityCenter))) * 3959   as Int)),0)  as 'Miles'
		,CONVERT(INT, MilesFromICAO,0) Miles
		,NegociatedRate,
		PhoneNum,
		HotelRating, OrigLatitude, OrigLongitude 		
		from #TempResultTable
		ORDER BY Name

   IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 
 
GO


