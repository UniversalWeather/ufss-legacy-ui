/****** Object:  StoredProcedure [dbo].[spGetHotelsByAirportID]    Script Date: 10/03/2013 21:01:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetHotelsByAirportID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetHotelsByAirportID]
GO

/****** Object:  StoredProcedure [dbo].[spGetHotelsByAirportID]    Script Date: 10/03/2013 21:01:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetHotelsByAirportID](@CustomerID BIGINT, @AirportID BIGINT)        
AS        
-- =============================================        
-- Author: Leela M       
-- Create date: 09/10/2012        
-- Description: Get Hotels By Airport ID      
-- =============================================        
  -- [dbo].[spGetHotelsByAirportID] 10001 ,1000164674      
  --declare @CustomerID bigint       
  --declare @AirportID bigint       
      
  --set @CustomerID = 10001       
  --select @AirportID = 1000164674 -- AirportID from airport where icaoid = 'khou' and CustomerID = @CustomerID      
  --select *  from Airport where AirportID = 1000164674       
-- =============================================        
SET NOCOUNT ON        
      
DECLARE @HotelICAOID CHAR(4)      
--DECLARE @UWACustomerID BIGINT      
      
SELECT @HotelICAOID = ICAOID FROM Airport WHERE AirportID = @AirportID    and IsDeleted=0   
--EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output      
DECLARE @UWACustomerID BIGINT         
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output      
        
DECLARE @ICAOCD VARCHAR(50)    
DECLARE @UWAAirportId VARCHAR(50) 
DECLARE @CustomAirportID VARCHAR(50)        

select @ICAOCD =  icaoid from Airport where AirportID = @AirportID and IsDeleted=0   
select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD   and IsDeleted=0   

--If UWA Airportid is passed, check whether the same ICAOID is there as Custom record, if available then get that ID.    
select @CustomAirportID =  AirportID from Airport where CustomerID = @CustomerID and IcaoID = @ICAOCD    
      
 SELECT   H.[HotelID]      
   ,H.[AirportID]      
   ,H.[HotelCD]      
   ,H.[CustomerID]      
   ,H.[IsChoice]      
   ,H.[Name]      
   ,H.[PhoneNum]      
   ,H.[MilesFromICAO]      
   ,H.[HotelRating]      
   ,H.[FaxNum]      
   ,H.[NegociatedRate]      
   ,H.[Addr1]      
   ,H.[Addr2]      
   ,H.[Addr3]      
   ,H.[CityName]      
   ,H.[MetroID]      
   ,H.[StateName]      
   ,H.[PostalZipCD]      
   ,H.[CountryID]      
   ,H.[ContactName]      
   ,H.[Remarks]      
   ,H.[IsPassenger]      
   ,H.[IsCrew]      
   ,H.[IsUWAMaintained]      
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter      
   ,H.[UWAMaintFlag]       
   ,H.[LatitudeDegree]      
   ,H.[LatitudeMinutes]      
   ,H.[LatitudeNorthSouth]      
   ,H.[LongitudeDegrees]      
   ,H.[LongitudeMinutes]      
   ,H.[LongitudeEastWest]      
   ,H.[LastUpdUID]      
   ,H.[LastUpdTS]      
   ,H.[UpdateDT]      
   ,H.[RecordType]      
   ,H.[MinutesFromICAO]      
   ,H.[SourceID]      
   ,H.[ControlNum]      
   ,H.[Website]         
   ,H.[UWAID]      
   ,H.[IsInActive]      
   ,H.[IsDeleted]      
   ,H.[UWAUpdates]      
   ,@HotelICAOID IcaoID         
   ,m.MetroCD      
   ,c.CountryCD       
   ,H.[AltBusinessPhone]      
            ,H.[TollFreePhone]      
            ,H.[BusinessEmail]      
            ,H.[ContactBusinessPhone]      
            ,H.[CellPhoneNum]      
            ,H.[ContactEmail]    
            ,H.ExchangeRateID    
     ,H.NegotiatedTerms    
     ,H.SundayWorkHours    
     ,H.MondayWorkHours    
     ,H.TuesdayWorkHours    
     ,H.WednesdayWorkHours    
     ,H.ThursdayWorkHours    
     ,H.FridayWorkHours    
     ,H.SaturdayWorkHours    
 FROM  [Hotel] H      
 --inner join [Airport] Ai on H.AirportID = Ai.AirportID       
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID       
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID      
 WHERE  
 (H.AirportID = @CustomAirportID  AND H.CustomerID = @CustomerID AND H.IsDeleted = 0)   
 or (H.AirportID  = @UWAAirportId AND  H.CustomerID = @CustomerID AND H.IsDeleted = 0)      
 --H.AirportID = @AirportID      
 ----Ai.IcaoID = @HotelICAOID       
 ----AND (AI.CustomerID = @CustomerID OR Ai.CustomerID = @UWACustomerID)      
 --AND (H.CustomerID = @CustomerID)       
 --AND H.IsDeleted = 0      
       
 UNION ALL      
       
 SELECT   H.[HotelID]      
   ,H.[AirportID]      
   ,H.[HotelCD]      
   ,H.[CustomerID]      
   ,H.[IsChoice]      
   ,H.[Name]      
   ,H.[PhoneNum]      
   ,H.[MilesFromICAO]      
   ,H.[HotelRating]      
   ,H.[FaxNum]      
   ,H.[NegociatedRate]      
   ,H.[Addr1]      
   ,H.[Addr2]      
   ,H.[Addr3]      
   ,H.[CityName]      
   ,H.[MetroID]      
   ,H.[StateName]      
   ,H.[PostalZipCD]      
   ,H.[CountryID]      
   ,H.[ContactName]      
   ,H.[Remarks]      
   ,H.[IsPassenger]      
   ,H.[IsCrew]      
   ,H.[IsUWAMaintained]      
   ,(SELECT CAST(CASE WHEN H.[UWAMaintFlag] = 1 THEN 'UWA' ELSE 'CUSTOM' END as varchar)) as Filter      
   ,H.[UWAMaintFlag]        
   ,H.[LatitudeDegree]      
   ,H.[LatitudeMinutes]      
   ,H.[LatitudeNorthSouth]      
   ,H.[LongitudeDegrees]      
   ,H.[LongitudeMinutes]      
   ,H.[LongitudeEastWest]      
   ,H.[LastUpdUID]      
   ,H.[LastUpdTS]      
   ,H.[UpdateDT]      
   ,H.[RecordType]      
   ,H.[MinutesFromICAO]      
   ,H.[SourceID]      
   ,H.[ControlNum]      
   ,H.[Website]         
   ,H.[UWAID]      
   ,H.[IsInActive]      
   ,H.[IsDeleted]      
   ,H.[UWAUpdates]      
   ,@HotelICAOID IcaoID         
   ,m.MetroCD      
   ,c.CountryCD       
   ,H.[AltBusinessPhone]      
            ,H.[TollFreePhone]      
            ,H.[BusinessEmail]      
            ,H.[ContactBusinessPhone]      
            ,H.[CellPhoneNum]      
            ,H.[ContactEmail]     
            ,H.ExchangeRateID    
     ,H.NegotiatedTerms    
     ,H.SundayWorkHours    
     ,H.MondayWorkHours    
     ,H.TuesdayWorkHours    
     ,H.WednesdayWorkHours    
     ,H.ThursdayWorkHours    
     ,H.FridayWorkHours    
     ,H.SaturdayWorkHours     
 FROM  [Hotel] H      
 LEFT OUTER JOIN metro m on m.MetroID= H.MetroID       
 LEFT OUTER JOIN Country c on c.CountryID = H.CountryID      
      
 WHERE       
 H.AirportID = @UWAAirportId      
 AND H.CustomerID = @UWACustomerID      
 AND H.IsDeleted = 0      
 AND H.HotelID NOT IN       
  (SELECT HotelID FROM Hotel       
  WHERE     
     
  (AirportID  = @CustomAirportID         
  AND CustomerID = @CustomerID ) or (AirportID  = @UWAAirportId         
  AND  CustomerID = @CustomerID )     
  AND IsDeleted = 0       
  -- AirportID = @AirportID      
  ----Ai.IcaoID = @HotelICAOID       
  ----AND (AI.CustomerID = @CustomerID OR Ai.CustomerID = @UWACustomerID)      
  --AND CustomerID = @CustomerID      
  --AND IsDeleted = 0      
 )      
       
 ORDER BY H.[Name]--HotelCD    
GO


