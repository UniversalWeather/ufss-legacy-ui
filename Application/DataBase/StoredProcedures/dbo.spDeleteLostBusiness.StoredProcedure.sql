
/****** Object:  StoredProcedure [dbo].[spDeleteLostBusiness]    Script Date: 20/02/2013 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteLostBusiness]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteLostBusiness]
GO
CREATE procedure [dbo].[spDeleteLostBusiness]
(
 @CustomerID bigint
,@CQLostBusinessID bigint
,@LastUpdUID varchar(30)
,@LastUpdTS datetime
,@IsDeleted bit
)
-- =============================================
-- Author:Ramesh. J
-- Create date: 20/02/2013
-- Description: Delete the CQLostBusiness information
-- =============================================
as
begin 
--Check if the record is not being used anywhere in the application before Delete  
 DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)        
SET @ENTITYNAME = N'CQLostBusiness'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @CQLostBusinessID, @RecordUsed OUTPUT
--End of Delete Check
  
  if (@RecordUsed <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 ELSE  
  BEGIN  
   --To store the lastupdateddate as UTC date 
  
   SET @LastUpdTS = GETUTCDATE()  
   
   --End of UTC date
SET NoCOUNT ON

UPDATE [CQLostBusiness]
   SET     [LastUpdUID]=@LastUpdUID
           ,[LastUpdTS]=@LastUpdTS     
           ,[IsDeleted]=@IsDeleted           
 WHERE CustomerID=@CustomerID and CQLostBusinessID=@CQLostBusinessID

end
END
GO
