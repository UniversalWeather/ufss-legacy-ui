/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRecentActivityCharterQuote]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRecentActivityCharterQuote]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spGetRecentActivityCharterQuote]      
@UserName VARCHAR(30),      
@MaxRecordCount int      
AS      
BEGIN      
 --SELECT TOP (@MaxRecordCount)  CQMain.*,      
 --P.PassengerRequestorCD AS PassengerRequestorCD,F.TailNum AS FleetTailNum      
 --FROM CQMain       
 --LEFT JOIN Passenger P on CQMain.PassengerRequestorID = P.PassengerRequestorID         
 --LEFT JOIN Fleet F on  CQMain.FleetID = F.FleetID      
 --WHERE CQMain.LastUpdUID = @UserName ORDER BY CQMain.LastUpdTS DESC      
 SELECT TOP (@MaxRecordCount)      
 
      CQF.CQFileID,      
      CQF.CustomerID,      
      CQF.FileNUM,      
      EstDepartureDT,    
      CQF.CQCustomerID,    
      CQF.CQCustomerName,    
      CQF.CQFileDescription,          
      CQF.IsFinancialWarning,    
      CQF.LastUpdUID,  
      CQF.LastUpdTS,  
      Case When CQF.Notes is not null and CQF.Notes <> '' then '!' else '' end as Alert,          
      A.ICAOID AS HomebaseCD ,    
       CASE WHEN    
  (    
       SELECT count(1)    
       FROM CQException PREFEX    
       INNER JOIN CQMain CM ON CM.CQFileID = CQF.CQFileID    
       WHERE CM.CQMainID = PREFEX.CQMainID    
  ) >0  THEN '!'     
     ELSE ''    
  END as FileExcep ,    
  isnull(LS.LeadSourceCD,'') as LeadSourceCD,    
  case when isnull(CQC.CustomerType,'')='1' then 'Broker'    
   when  isnull(CQC.CustomerType,'')='2' then 'Block CQ client'    
   else ''    
   end as 'CustomerType'    
   ,       
  isnull(SP.SalesPersonCD,'') as SalesPersonCD    
      FROM CQFile CQF    
      INNER JOIN Company C on CQF.HomebaseID = C.HomebaseID      
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID     
      LEFT JOIN LeadSource LS on CQF.LeadSourceID = LS.LeadSourceID    
      LEFT JOIN CQCustomer CQC on CQF.CQCustomerID =   CQC.CQCustomerID    
      LEFT JOIN SalesPerson SP on CQF.SalesPersonID =   SP.SalesPersonID    
      WHERE             
       CQF.LastUpdUID= @UserName  
      and CQF.IsDeleted =0     
	  ORDER BY CQF.LastUpdTS desc
END  

--drop proc [spGetRecentActivityCorporateRequest]
GO


