GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spResetPasswordWithoutAuth]') AND type in (N'P', N'PC'))
DROP PROC [dbo].[spResetPasswordWithoutAuth]  
GO
  

CREATE PROC [dbo].[spResetPasswordWithoutAuth]              
@EmailId   varchar(100),              
@Password nvarchar(500),              
@LastUpdUID char(30),         
@Answer nvarchar(1000)              
AS               
BEGIN              
 DECLARE  @UserPasswordID BIGINT              
 DECLARE @CustomerId BIGINT            
 DECLARE @Count INT        
 DECLARE @UserName varchar(30)    
 SELECT Top 1 @UserName = UserName from UserMaster where EmailId = @EmailId    
 SET @Count = (Select Count(SecurityAnswer) From UserSecurityHint where UserName = @UserName AND SecurityAnswer = @Answer)        
 if(@Count >0)        
 BEGIN        
 Select @CustomerId = CustomerId From UserMaster where UserName = @UserName            
         
 EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo', @UserPasswordID OUTPUT              
               
 --Insert New password              
 INSERT INTO UserPassword  
 (UserPasswordID,UserName,FPPWD,ActiveStartDT,ActiveEndDT,PWDStatus,LastUpdUID,LastUpdTS)  
  VALUES(@UserPasswordID, @UserName, @Password, GETUTCDATE(), DATEADD(day,1,GETUTCDATE()), 1, @UserName, GETUTCDATE())              
              
 -- Set other passwords inactive for the user except current              
 UPDATE UserPassword SET ActiveEndDT = GETUTCDATE(), PWDStatus = 0, LastUpdUID = @UserName, LastUpdTS = GETUTCDATE() WHERE UserName = @UserName AND               
 UserPasswordID <> @UserPasswordID              
       
 -- Set ForceChangePassword flag to true      
 UPDATE UserMaster SET ForceChangePassword = 1, InvalidLoginCount = 0 Where UserName = @UserName      
END               
END      
    
  
GO