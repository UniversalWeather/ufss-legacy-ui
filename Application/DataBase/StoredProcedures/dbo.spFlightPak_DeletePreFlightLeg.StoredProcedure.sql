
/****** Object:  StoredProcedure [dbo].[spFlightPak_DeletePreFlightLeg]    Script Date: 05/14/2014 09:32:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_DeletePreFlightLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_DeletePreFlightLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_DeletePreFlightLeg]    Script Date: 05/14/2014 09:32:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_DeletePreFlightLeg] (    
 @Legid BIGINT    
 ,@LastUpdUID CHAR(30)    
 ,@LastUpdTS DATETIME    
 ,@IsDeleted BIT    
 )    
AS    
BEGIN    
 SET NOCOUNT ON    
 
 
 Delete  PreflightHotelCrewPassengerList from PreflightHotelList inner join PreflightLeg on
 PreflightHotelList.legID = PreflightLeg.LegID where
 PreflightHotelList.PreflightHotelListID = PreflightHotelCrewPassengerList.PreflightHotelListID
 and PreflightLeg.LegID = @Legid
 
 Delete  PreflightHotelList from  PreflightLeg 
 where PreflightHotelList.legID = PreflightLeg.LegID 
  and PreflightLeg.LegID = @Legid
 
  
 --The below needs to be removed then the preflightcrew and pax hotel list removed
 Delete PreflightCrewHotelList from PreflightCrewHotelList inner join PreflightCrewList on   
 PreflightCrewHotelList.PreflightCrewListID = PreflightCrewList.PreflightCrewListID where PreflightCrewList.LegID =@Legid  
 
 --The below needs to be removed then the preflightcrew and pax hotel list removed  
 Delete PreflightPassengerHotelList from PreflightPassengerHotelList inner join PreflightPassengerList    on   
 PreflightPassengerHotelList.PreflightPassengerListID = PreflightPassengerList.PreflightPassengerListID where PreflightPassengerList.LegID =@Legid  
 
   
  
Delete from PreflightCateringDetail   where LegID = @Legid  
Delete from PreflightCateringDetail   where LegID = @Legid  
Delete from PreflightCateringList   where LegID = @Legid  
Delete from PreflightCheckList   where LegID = @Legid  
Delete from PreflightTransportList   where LegID = @Legid  
Delete from PreflightCrewList   where LegID = @Legid  
      
Delete from PreflightFBOHandler   where LegID = @Legid  
Delete from PreflightFBOList   where LegID = @Legid  
Delete from PreflightFuel   where LegID = @Legid  
Delete from PreflightTripSIFL   where LegID = @Legid  
--Delete from PreflightHotelXrefList   where LegID = @Legid  
Delete from PreflightPassengerList   where LegID = @Legid  
Delete from PreflightTripOutbound   where LegID = @Legid  
Delete from UWAL   where LegID = @Legid  
Delete from UWALPlan   where LegID = @Legid  
Delete from UWALTrans   where LegID = @Legid  
Delete from UWALTsPer   where LegID = @Legid  
Delete from UWALWx   where LegID = @Legid  
Delete from UWAPermit   where LegID = @Legid  
Delete from UWASpServ   where LegID = @Legid  
          
  
Delete from PreflightLeg where LegID = @Legid    
 --UPDATE PreflightLeg    
 --SET [LastUpdUID] = @LastUpdUID    
 -- ,[LastUpdTS] = @LastUpdTS    
 -- ,[IsDeleted] = @IsDeleted    
 --WHERE Legid = @Legid    
END

GO


