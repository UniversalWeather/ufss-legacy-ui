

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_UpdatePreflightHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightHotelList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_UpdatePreflightHotelList]  
 (    
    @PreflightHotelListID bigint  ,
	@PreflightHotelName varchar(60)  ,
	@LegID bigint  ,
	@HotelID bigint  ,
	@AirportID bigint  ,
	@RoomType varchar(60)  ,
	@RoomRent numeric(6, 2)  ,
	@RoomConfirm varchar(max)  ,
	@RoomDescription varchar(max)  ,
	@Street varchar(50)  ,
	@CityName varchar(20)  ,
	@StateName varchar(20)  ,
	@PostalZipCD varchar(20)  ,
	@PhoneNum1 varchar(25)  ,
	@PhoneNum2 varchar(25)  ,
	@PhoneNum3 varchar(25)  ,
	@PhoneNum4 varchar(25)  ,
	@FaxNUM varchar(25)  ,
	@LastUpdUID varchar(30)  ,
	@LastUpdTS datetime  ,
	@IsDeleted bit   ,
	@HotelArrangedBy varchar(10)  ,
	@Rate numeric(17, 2)  ,
	@DateIn date  ,
	@DateOut date  ,
	@IsSmoking bit  ,
	@RoomTypeID bigint  ,
	@NoofBeds numeric(3, 0)  ,
	@IsAllCrewOrPax bit  ,
	@IsCompleted bit  ,
	@IsEarlyCheckIn bit  ,
	@EarlyCheckInOption char(1)  ,
	@SpecialInstructions varchar(100)  ,
	@ConfirmationStatus varchar(max)  ,
	@Comments varchar(max)  ,
	@CustomerID bigint  ,
	@Address1 varchar(40)  ,
	@Address2 varchar(40)  ,
	@Address3 varchar(40)  ,
	@MetroID bigint  ,
	@City varchar(30)  ,
	@StateProvince varchar(25)  ,
	@CountryID bigint  ,
	@PostalCode varchar(15)  ,
	@Status varchar(20)  ,
	@isArrivalHotel bit  ,
	@isDepartureHotel bit  ,
	@crewPassengerType varchar(1),
	@NoofRooms numeric(2,0),
	@ClubCard varchar(100),
	@IsRateCap bit,
	@MaxCapAmount numeric(17,2),
	@IsRequestOnly bit,
	@IsBookNight bit	
    )  
AS  
BEGIN   
SET NOCOUNT OFF  
  
UPDATE [dbo].[PreflightHotelList]  

SET   	
	[PreflightHotelName] 			= @PreflightHotelName 
	,[LegID]   				= @LegID   
	,[HotelID]   				= @HotelID   
	,[AirportID]   				= @AirportID   
	,[RoomType] 				= @RoomType 
	,[RoomRent] 				= @RoomRent 
	,[RoomConfirm] 				= @RoomConfirm 
	,[RoomDescription] 			= @RoomDescription 
	,[Street] 				= @Street 
	,[CityName]				= @CityName
	,[StateName] 				= @StateName 
	,[PostalZipCD] 				= @PostalZipCD 
	,[PhoneNum1] 				= @PhoneNum1 
	,[PhoneNum2] 				= @PhoneNum2 
	,[PhoneNum3] 				= @PhoneNum3 
	,[PhoneNum4] 				= @PhoneNum4 
	,[FaxNUM] 				= @FaxNUM 
	,[LastUpdUID]				= @LastUpdUID
	,[LastUpdTS]   				= @LastUpdTS   
	,[IsDeleted] 				= @IsDeleted 
	,[HotelArrangedBy] 			= @HotelArrangedBy 
	,[Rate]					= @Rate
	,[DateIn]   				= @DateIn   
	,[DateOut]   				= @DateOut   
	,[IsSmoking]   				= @IsSmoking   
	,[RoomTypeID]   				= @RoomTypeID   
	,[NoofBeds]				= @NoofBeds
	,[IsAllCrewOrPax]   			= @IsAllCrewOrPax   
	,[IsCompleted]   			= @IsCompleted   
	,[IsEarlyCheckIn]   			= @IsEarlyCheckIn   
	,[EarlyCheckInOption] 			= @EarlyCheckInOption 
	,[SpecialInstructions]			= @SpecialInstructions
	,[ConfirmationStatus] 			= @ConfirmationStatus 
	,[Comments] 				= @Comments 
	,[CustomerID]   				= @CustomerID   
	,[Address1] 				= @Address1 
	,[Address2] 				= @Address2 
	,[Address3] 				= @Address3 
	,[MetroID]   				= @MetroID   
	,[City] 					= @City 
	,[StateProvince] 			= @StateProvince 
	,[CountryID]   				= @CountryID   
	,[PostalCode] 				= @PostalCode 
	,[Status] 				= @Status 
	,[isArrivalHotel]   			= @isArrivalHotel   
	,[isDepartureHotel]   			= @isDepartureHotel   
	,[crewPassengerType] 			= @crewPassengerType
	,[NoofRooms] = @NoofRooms
	,[ClubCard] = @ClubCard
	,[IsRateCap] = @IsRateCap
	,[MaxCapAmount] = @MaxCapAmount
	,[IsRequestOnly] = @IsRequestOnly
	,[IsBookNight] = @IsBookNight
     
 WHERE  PreflightHotelListID=@PreflightHotelListID AND CustomerID = @CustomerID
   
END  
GO
