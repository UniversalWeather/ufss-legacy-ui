
/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRLeg]    Script Date: 02/18/2014 11:20:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_AddCRLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRLeg]    Script Date: 02/18/2014 11:20:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

      
      
CREATE Procedure [dbo].[spFlightPak_CorporateRequest_AddCRLeg](      
      
                 
           @CustomerID bigint      
           ,@CRMainID bigint      
           ,@TripNUM bigint      
           ,@LegNUM bigint      
           ,@DAirportID bigint      
           ,@AAirportID bigint      
           ,@DepartureCity varchar(25)      
           ,@ArrivalCity varchar(25)      
           ,@DepartureDTTMLocal datetime      
           ,@ArrivalDTTMLocal datetime      
           ,@PassengerRequestorID bigint      
           ,@DepartmentID bigint      
           ,@AuthorizationID bigint      
           ,@FlightCategoryID bigint      
           ,@IsPrivate bit      
           ,@Distance numeric(5,0)      
           ,@ElapseTM numeric(7,3)      
           ,@PassengerTotal int      
           ,@PowerSetting varchar(1)      
           ,@TakeoffBIAS numeric(6,3)     
           ,@LandingBIAS numeric(6,3)      
           ,@TrueAirSpeed numeric(3,0)      
           ,@WindsBoeingTable numeric(5,0)      
           ,@IsDepartureConfirmed bit      
           ,@IsArrivalConfirmation bit      
           ,@IsApproxTM bit      
           ,@IsScheduledServices bit      
           ,@DepartureGreenwichDTTM datetime      
           ,@ArrivalGreenwichDTTM datetime      
           ,@HomeDepartureDTTM datetime      
           ,@HomeArrivalDTTM datetime      
           ,@NegotiateTime varchar(5)      
           ,@LogBreak varchar(3)      
           ,@FlightPurpose varchar(40)      
           ,@SeatTotal int      
           ,@ReservationTotal int      
           ,@ReservationAvailable int      
           ,@WaitNUM int      
           ,@Duty_Type varchar(2)      
           ,@DutyHours numeric(12,3)      
           ,@RestHours numeric(12,3)      
           ,@FlightHours numeric(12,3)      
           ,@Notes varchar(Max)      
           ,@FuelLoad numeric(6,0)      
           ,@OutboundInstruction varchar(Max)      
           ,@DutyType char(2)      
           ,@IsCrewDiscount bit      
           ,@CrewCurrency varchar(4)      
           ,@IsDutyEnd bit      
           ,@FedAviationRegNUM varchar(3)      
           ,@LegID bigint      
           ,@WindReliability int      
           ,@CROverRide numeric(4,1)      
           ,@CheckGroup varchar(3)      
           ,@AirportAlertCD varchar(3)      
           ,@AdditionalCrew varchar(24)      
           ,@PilotInCommand varchar(3)      
           ,@SecondInCommand varchar(3)      
           ,@NextLocalDTTM datetime      
           ,@NextGMTDTTM datetime      
           ,@NextHomeDTTM datetime      
           ,@CrewNotes varchar(Max)      
           ,@LastUpdUID varchar(30)      
           ,@LastUpdTS datetime      
           ,@IsDeleted bit      
           ,@FBOID bigint      
           ,@ClientID bigint  
)      
           AS      
BEGIN       
  DECLARE @CRLegID Bigint      
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRLegID OUTPUT       
        
  ------------------------------------      
  ----INSERT BEGINS HERE      
  ------------------------------       
      
INSERT INTO [CRLeg]      
           ([CRLegID]      
           ,[CustomerID]      
           ,[CRMainID]      
           ,[TripNUM]      
           ,[LegNUM]      
           ,[DAirportID]      
           ,[AAirportID]      
           ,[DepartureCity]      
           ,[ArrivalCity]      
           ,[DepartureDTTMLocal]      
           ,[ArrivalDTTMLocal]      
           ,[PassengerRequestorID]      
           ,[DepartmentID]      
           ,[AuthorizationID]      
           ,[FlightCategoryID]      
           ,[IsPrivate]      
           ,[Distance]      
           ,[ElapseTM]      
           ,[PassengerTotal]      
           ,[PowerSetting]      
           ,[TakeoffBIAS]      
           ,[LandingBIAS]      
           ,[TrueAirSpeed]      
           ,[WindsBoeingTable]      
           ,[IsDepartureConfirmed]      
           ,[IsArrivalConfirmation]      
           ,[IsApproxTM]      
           ,[IsScheduledServices]      
           ,[DepartureGreenwichDTTM]      
           ,[ArrivalGreenwichDTTM]      
           ,[HomeDepartureDTTM]      
           ,[HomeArrivalDTTM]      
           ,[NegotiateTime]      
           ,[LogBreak]      
   ,[FlightPurpose]      
           ,[SeatTotal]      
           ,[ReservationTotal]      
           ,[ReservationAvailable]      
           ,[WaitNUM]      
           ,[Duty_Type]      
           ,[DutyHours]      
           ,[RestHours]      
           ,[FlightHours]      
           ,[Notes]      
           ,[FuelLoad]      
           ,[OutboundInstruction]      
           ,[DutyType]      
           ,[IsCrewDiscount]      
           ,[CrewCurrency]      
           ,[IsDutyEnd]      
           ,[FedAviationRegNUM]      
           ,[LegID]      
           ,[WindReliability]      
           ,[CROverRide]      
           ,[CheckGroup]      
           ,[AirportAlertCD]      
           ,[AdditionalCrew]      
           ,[PilotInCommand]      
           ,[SecondInCommand]      
           ,[NextLocalDTTM]      
           ,[NextGMTDTTM]      
           ,[NextHomeDTTM]      
           ,[CrewNotes]      
           ,[LastUpdUID]      
           ,[LastUpdTS]      
           ,[IsDeleted]      
           ,[FBOID]      
           ,[ClientID])      
     VALUES      
           (@CRLegID      
           ,@CustomerID      
           ,@CRMainID      
           ,@TripNUM      
           ,@LegNUM      
           ,@DAirportID      
           ,@AAirportID      
           ,@DepartureCity      
           ,@ArrivalCity      
           ,@DepartureDTTMLocal      
           ,@ArrivalDTTMLocal      
           ,@PassengerRequestorID      
           ,@DepartmentID      
           ,@AuthorizationID      
           ,@FlightCategoryID      
           ,@IsPrivate      
           ,@Distance      
           ,@ElapseTM      
           ,@PassengerTotal      
           ,@PowerSetting      
           ,@TakeoffBIAS      
           ,@LandingBIAS      
           ,@TrueAirSpeed      
           ,@WindsBoeingTable      
           ,@IsDepartureConfirmed      
           ,@IsArrivalConfirmation      
           ,@IsApproxTM      
           ,@IsScheduledServices      
           ,@DepartureGreenwichDTTM      
           ,@ArrivalGreenwichDTTM      
           ,@HomeDepartureDTTM      
           ,@HomeArrivalDTTM      
           ,@NegotiateTime      
           ,@LogBreak      
           ,@FlightPurpose      
           ,@SeatTotal      
           ,@ReservationTotal      
           ,@ReservationAvailable      
           ,@WaitNUM      
           ,@Duty_Type      
           ,CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
		   ,CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
		   ,CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END    
           ,@Notes      
           ,@FuelLoad      
           ,@OutboundInstruction      
           ,@DutyType      
           ,@IsCrewDiscount      
           ,@CrewCurrency      
           ,@IsDutyEnd      
           ,@FedAviationRegNUM      
           ,@LegID      
           ,@WindReliability      
           ,@CROverRide      
           ,@CheckGroup      
           ,@AirportAlertCD      
           ,@AdditionalCrew      
           ,@PilotInCommand      
           ,@SecondInCommand      
           ,@NextLocalDTTM      
           ,@NextGMTDTTM      
           ,@NextHomeDTTM      
           ,@CrewNotes      
           ,@LastUpdUID      
           ,@LastUpdTS      
           ,@IsDeleted      
           ,@FBOID      
           ,@ClientID)      
                 
           Select @CRLegID as CRLegID        
      ------------------      
      ---ENDS HERE      
      ------------------       
         
END      
      
GO


