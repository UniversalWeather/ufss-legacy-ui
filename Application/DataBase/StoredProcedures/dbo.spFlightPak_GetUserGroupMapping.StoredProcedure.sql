/****** Object:  StoredProcedure [dbo].[spFlightPak_GetUserGroupMapping]    Script Date: 08/29/2012 16:50:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetUserGroupMapping]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetUserGroupMapping]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_GetUserGroupMapping]
AS BEGIN
SET NOCOUNT ON
SELECT [UserGroupMappingID]
      ,[CustomerID]
      ,[UserGroupID]
      ,[UserName]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsDeleted]
      ,IsInactive
  FROM [UserGroupMapping]
END

GO
