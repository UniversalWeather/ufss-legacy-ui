

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightHotelListByPreflightHotelListID]    Script Date: 05/14/2014 11:34:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightHotelListByPreflightHotelListID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].spFlightPak_Preflight_GetPreflightHotelListByPreflightHotelListID
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightHotelListByPreflightHotelListID]    Script Date: 05/14/2014 11:34:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






create Procedure [dbo].[spFlightPak_Preflight_GetPreflightHotelListByPreflightHotelListID]
(
@CustomerID BIGINT,
@PreflightHotelListID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get spFlightPak_Preflight_GetPreflightHotelList By PreflightHotelListID
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightHotelList PC 
 where 
 PC.PreflightHotelListID = @PreflightHotelListID
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 





GO


