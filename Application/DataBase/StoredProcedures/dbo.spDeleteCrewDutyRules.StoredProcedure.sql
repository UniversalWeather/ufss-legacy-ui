

/****** Object:  StoredProcedure [dbo].[spDeleteCrewDutyRules]    Script Date: 7/15/2015 3:40:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[spDeleteCrewDutyRules](
@CrewDutyRulesID bigint,
@CrewDutyRuleCD char(4),
@CustomerID bigint, 
@LastUpdUID varchar(30),
@LastUptTM datetime,
@IsDeleted bit
)
-- =============================================
-- Author:Prakash Pandian.S
-- Create date: 30/4/2012
-- Description: Delete the CrewDutyRules information. Set IsDeleted to false
-- =============================================
as
begin 
SET NoCOUNT ON
  --Check if the record is not being used anywhere in the application before Delete
DECLARE @RecordUsed INT
DECLARE @ENTITYNAME VARCHAR(50)   
DECLARE @RecordUsedInCompany int SET @RecordUsedInCompany = 0     
SET @ENTITYNAME = N'CrewDutyRules'; -- Type Table Name
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @CrewDutyRulesID, @RecordUsed OUTPUT

SELECT @RecordUsedInCompany = COUNT(*) FROM [dbo].[Company] WHERE CQCrewDuty = @CrewDutyRuleCD
--End of Delete Check
  
  if (@RecordUsed <> 0  OR @RecordUsedInCompany <> 0)
 Begin
	RAISERROR(N'-500010', 17, 1)
 end
 else
 Begin 
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUptTM = @currentTime
UPDATE [dbo].[CrewDutyRules]
   SET 
           LastUpdUID=@LastUpdUID
		   ,LastUptTM=@LastUptTM
           ,IsDeleted=@IsDeleted
 WHERE CrewDutyRulesID=@CrewDutyRulesID

END
END

GO

