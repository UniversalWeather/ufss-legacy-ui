
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllUserMasterList]    Script Date: 01/24/2014 11:26:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetAllUserMasterList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetAllUserMasterList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetAllUserMasterList]    Script Date: 01/24/2014 11:26:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spFlightPak_Preflight_GetAllUserMasterList]    
(  
 @CustomerID Bigint  
)  
AS    
SET NOCOUNT ON;   
BEGIN  
SELECT [UserName]  
,[CustomerID]  
,[HomebaseID]  
,[ClientID]  
,[FirstName]  
,[MiddleName]  
,[LastName]  
,[HomeBase]  
,[TravelCoordID]  
,[IsActive]  
,[IsUserLock]  
,[IsTripPrivacy]  
,[EmailID]  
,[PhoneNum]  
,[PassengerRequestorID]  
,[UVTripLinkID]  
,[UVTripLinkPassword]  
,[IsPrimaryContact]  
,[IsSecondaryContact]  
,[IsSystemAdmin]  
,[IsDispatcher]  
,[IsOverrideCorp]  
,[IsCorpRequestApproval]  
,[LastUpdUID]  
,[LastUpdTS]  
,[IsDeleted]  
FROM [dbo].[UserMaster]  
where IsDispatcher =1  
and CustomerID = @CustomerID  
and IsDeleted =0  
END  
GO


