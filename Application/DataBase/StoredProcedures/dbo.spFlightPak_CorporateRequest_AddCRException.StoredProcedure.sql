/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRException]    Script Date: 02/20/2013 17:36:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_AddCRException]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRException]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRException](
	 @CRMainID bigint
	,@CustomerID bigint
	,@CRExceptionDescription varchar(1500)
	,@LastUpdUID varchar(30)
	,@LastUpdTS datetime
	,@ExceptionTemplateID bigint
	,@DisplayOrder varchar(15)
)    
AS    
BEGIN   
	DECLARE @CRExceptionID  BIGINT   
	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRExceptionID OUTPUT     
	INSERT INTO [CRException]([CRExceptionID]
		,[CRMainID]
		,[CustomerID]
		,[CRExceptionDescription]
		,[LastUpdUID]
		,[LastUpdTS]
		,[ExceptionTemplateID]
		,[DisplayOrder]
		)    
	VALUES(@CRExceptionID
		,@CRMainID
		,@CustomerID
		,@CRExceptionDescription
		,@LastUpdUID
		,@LastUpdTS
		,@ExceptionTemplateID
		,@DisplayOrder
	)    

	SELECT @CRExceptionID as CRExceptionID
END


GO


