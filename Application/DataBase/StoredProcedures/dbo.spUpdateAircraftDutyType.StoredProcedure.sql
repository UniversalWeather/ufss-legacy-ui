
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAircraftDutyType]    Script Date: 08/24/2012 10:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAircraftDutyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAircraftDutyType]
go
CREATE procedure [dbo].[spUpdateAircraftDutyType](@AircraftDutyID  BIGINT,
                                                  @AircraftDutyCD  char(2), 
                                                  @CustomerID BIGINT,   
                                                  @AircraftDutyDescription varchar(25),  
                                                  @ClientID BIGINT,  
                                                  @GraphColor Varchar(11),  
                                                  @LastUpdUID varchar(30),  
                                                  @LastUpdTS datetime,  
                                                  @ForeGrndCustomColor Varchar(8),  
                                                  @BackgroundCustomColor Varchar(8),  
                                                  @IsCalendarEntry bit,  
                                                  @DefaultStartTM char(5),  
                                                  @DefualtEndTM char(5),  
                                                  @IsAircraftStandby bit,  
                                                  @IsDeleted bit,
                                                  @IsInActive bit)  

as  
begin   
SET NoCOUNT ON  
  SET @LastUpdTS = GETUTCDATE() 
 UPDATE AircraftDuty  
 SET [CustomerID] = @CustomerID  
      ,[AircraftDutyCD] = @AircraftDutyCD  
      ,[AircraftDutyDescription] = @AircraftDutyDescription  
      ,[ClientID] = @ClientID  
      ,[GraphColor] = @GraphColor  
      ,[LastUpdUID] = @LastUpdUID  
      ,[LastUpdTS] = @LastUpdTS  
      ,[ForeGrndCustomColor] = @ForeGrndCustomColor  
      ,[BackgroundCustomColor] = @BackgroundCustomColor  
      ,[IsCalendarEntry] = @IsCalendarEntry  
      ,[DefaultStartTM] = @DefaultStartTM  
      ,[DefualtEndTM] = @DefualtEndTM  
      ,[IsAircraftStandby] = @IsAircraftStandby  
      ,[IsDeleted] = @IsDeleted  
      ,[IsInActive] = @IsInActive
        
 WHERE [CustomerID] = @CustomerID and [AircraftDutyID]=@AircraftDutyID   
  
END