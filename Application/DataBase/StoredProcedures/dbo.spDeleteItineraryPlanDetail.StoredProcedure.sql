
GO
/****** Object:  StoredProcedure [dbo].[spDeleteItineraryPlanDetail]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteItineraryPlanDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteItineraryPlanDetail]
GO

CREATE PROCEDURE spDeleteItineraryPlanDetail
(@ItineraryPlanDetailID	BIGINT
,@CustomerID	BIGINT
,@LastUpdUID	VARCHAR(30)
,@LastUpdTS	DATETIME
,@IsDeleted	BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE ItineraryPlanDetail
    SET CustomerID  = @CustomerID       
       ,LastUpdUID  = @LastUpdUID
       ,LastUpdTS  = @LastUpdTS
       ,IsDeleted = @IsDeleted
    WHERE ItineraryPlanDetailID = @ItineraryPlanDetailID

	/* Code for updating leg num */
	DECLARE @ItineraryPlanID BIGINT
	DECLARE @tmpItineraryPlanDetailID BIGINT
	DECLARE @tmpLegNUM INT
	SELECT @tmpLegNUM = LegNum, @ItineraryPlanID = ItineraryPlanID FROM ItineraryPlanDetail WHERE ItineraryPlanDetailID = @ItineraryPlanDetailID
	SELECT ItineraryPlanDetailID,CustomerID,ItineraryPlanID,IntinearyNUM,LegNUM,DAirportID,AAirportID,
			Distance,PowerSetting,TakeoffBIAS,LandingBias,TrueAirSpeed,WindsBoeingTable,ElapseTM,DepartureLocal,
			DepartureGMT,DepartureHome,ArrivalLocal,ArrivalGMT,ArrivalHome,IsDepartureConfirmed,IsArrivalConfirmation,
			PassengerNUM,Cost,WindReliability,LegID,LastUpdUID,LastUpdTS,IsDeleted
		INTO #TmpItineraryPlanDetail
	FROM ItineraryPlanDetail
	WHERE CustomerID=@CustomerID
		AND ItineraryPlanID=@ItineraryPlanID
		--AND LegNUM >= @tmpLegNUM
		AND IsDeleted = 0
	ORDER BY LegNUM ASC
		SET @tmpLegNUM = 0
	WHILE (select COUNT(*) from #TmpItineraryPlanDetail) >0
		BEGIN
			SELECT TOP 1 @tmpItineraryPlanDetailID = ItineraryPlanDetailID FROM #TmpItineraryPlanDetail
			SET @tmpLegNUM = @tmpLegNUM + 1
			UPDATE ItineraryPlanDetail SET LegNUM = @tmpLegNUM WHERE ItineraryPlanDetailID = @tmpItineraryPlanDetailID			
			DELETE FROM #TmpItineraryPlanDetail WHERE ItineraryPlanDetailID = @tmpItineraryPlanDetailID
		END
	DROP TABLE #TmpItineraryPlanDetail
	/* Code for updating leg num */
	
END