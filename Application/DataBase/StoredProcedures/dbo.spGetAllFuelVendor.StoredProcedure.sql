IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFuelVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFuelVendor]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllFuelVendor]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetAllFuelVendor]
(@CustomerID Bigint)    
AS    
-- =============================================    
-- Author: Badrinath    
-- Create date: 3/5/2012    
-- Description: Get All Fuel Vendor Informations    
-- =============================================    
SET NOCOUNT ON    
    
		SELECT FuelVendorID,
               VendorCD , 
               VendorName, 
               AirportID,
               CustomerID,
               VendorDescription,  
               ServerInfo,  
               BaseFileName,  
               FileLocation,  
               FileTYPE,  
               FldSep, 
               IsHeaderRow,  
               VendorDateFormat,  
               LastUpdUID,  
               LastUpdTS, 
               IsDeleted,
               IsInactive,  
               UWAUpdates
  FROM [FuelVendor]
  WHERE CustomerID=@CustomerID AND IsDeleted = 0
GO



