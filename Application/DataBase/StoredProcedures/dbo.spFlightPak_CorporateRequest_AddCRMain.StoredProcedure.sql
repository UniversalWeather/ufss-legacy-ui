/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_AddCRMain]    Script Date: 02/11/2013 15:45:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_AddCRMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRMain]
GO

CREATE PROCEDURE [dbo].[spFlightPak_CorporateRequest_AddCRMain]
(
            @CustomerID bigint
           ,@CRTripNUM int
           ,@Approver varchar(10)
           ,@TripID bigint
           ,@TripStatus char(1)
           ,@TripSheetStatus char(1)
           ,@AcknowledgementStatus char(1)
           ,@CharterQuoteStatus char(1)
           ,@CorporateRequestStatus char(1)
           ,@TravelCoordinatorID bigint
           ,@TravelCoordinatorName varchar(25)
           ,@TravelCoordinatorPhone varchar(25)
           ,@TravelCoordinatorFax varchar(25)
           ,@DispatchNUM varchar(12)
           ,@EstDepartureDT date
           ,@EstArrivalDT date
           ,@RecordType char(1)
           ,@FleetID bigint
           ,@AircraftID bigint
           ,@PassengerRequestorID bigint
           ,@RequestorName varchar(63)
           ,@RequestorPhone varchar(25)
           ,@DepartmentID bigint
           ,@DepartmentDescription varchar(25)
           ,@AuthorizationID bigint
           ,@AuthorizationDescription varchar(25)
           ,@HomebaseID bigint
           ,@ClientID bigint
           ,@IsPrivate bit
           ,@CRMainDescription varchar(40)
           ,@RequestDT date
           ,@BeginningGMTDT date
           ,@EndingGMTDT date
           ,@IsCrew bit
           ,@IsPassenger bit
           ,@IsAlert bit
           ,@IsAirportAlert bit
           ,@IsLog bit
           ,@LastUpdUID varchar(30)
           ,@LastUpdTS datetime
           ,@IsDeleted bit
)
AS
BEGIN 
		DECLARE @CRMainID Bigint
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'CorpReqCurrentNo',  @CRMainID OUTPUT 
		
		set @CRTripNUM = 0
		if (@RecordType ='W')
		BEGIN
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'RequestCurrentNo',  @CRTripNUM  OUTPUT  
			select @CRTripNUM = RequestCurrentNo  from [FlightpakSequence] where customerID = @CustomerID
		END


		if exists(select * from  UserMaster inner join Company on UserMaster.HomebaseID = Company.HomebaseID where UserMaster.UserName = @LastUpdUID and IsAutomaticDispatchNum = 1)
		begin
			set @DispatchNUM = cast(@CRTripNUM as varchar(12));
		end

		
		------------------------------------
		----INSERT BEGINS HERE
		------------------------------		
		INSERT INTO [CRMain]
		(
			CRMainID 
           ,CustomerID 
           ,CRTripNUM 
           ,Approver 
           ,TripID 
           ,TripStatus 
           ,TripSheetStatus 
           ,AcknowledgementStatus 
           ,CharterQuoteStatus 
           ,CorporateRequestStatus 
           ,TravelCoordinatorID 
           ,TravelCoordinatorName 
           ,TravelCoordinatorPhone 
           ,TravelCoordinatorFax 
           ,DispatchNUM 
           ,EstDepartureDT 
           ,EstArrivalDT 
           ,RecordType 
           ,FleetID 
           ,AircraftID 
           ,PassengerRequestorID 
           ,RequestorName
           ,RequestorPhone 
           ,DepartmentID 
           ,DepartmentDescription 
           ,AuthorizationID 
           ,AuthorizationDescription 
           ,HomebaseID 
           ,ClientID 
           ,IsPrivate 
           ,CRMainDescription 
           ,RequestDT 
           ,BeginningGMTDT 
           ,EndingGMTDT 
           ,IsCrew 
           ,IsPassenger 
           ,IsAlert 
           ,IsAirportAlert 
           ,IsLog 
           ,LastUpdUID 
           ,LastUpdTS 
           ,IsDeleted 
				   )
			 VALUES   
				   (   
			@CRMainID 
           ,@CustomerID 
           ,@CRTripNUM 
           ,@Approver 
           ,@TripID 
           ,@TripStatus 
           ,@TripSheetStatus 
           ,@AcknowledgementStatus 
           ,@CharterQuoteStatus 
           ,@CorporateRequestStatus 
           ,@TravelCoordinatorID 
           ,@TravelCoordinatorName 
           ,@TravelCoordinatorPhone 
           ,@TravelCoordinatorFax 
           ,@DispatchNUM 
           ,@EstDepartureDT 
           ,@EstArrivalDT 
           ,@RecordType 
           ,@FleetID 
           ,@AircraftID 
           ,@PassengerRequestorID 
           ,@RequestorName
           ,@RequestorPhone 
           ,@DepartmentID 
           ,@DepartmentDescription 
           ,@AuthorizationID 
           ,@AuthorizationDescription 
           ,@HomebaseID 
           ,@ClientID 
           ,@IsPrivate 
           ,@CRMainDescription 
           ,@RequestDT 
           ,@BeginningGMTDT 
           ,@EndingGMTDT 
           ,@IsCrew 
           ,@IsPassenger 
           ,@IsAlert 
           ,@IsAirportAlert 
           ,@IsLog 
           ,@LastUpdUID 
           ,@LastUpdTS 
           ,@IsDeleted 
				   )
		Select @CRMainID as CRMainID  
				  ------------------
				  ---ENDS HERE
				  ------------------ 
	  
END

GO