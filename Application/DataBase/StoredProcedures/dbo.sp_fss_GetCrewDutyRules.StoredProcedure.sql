/****** Object:  StoredProcedure [dbo].[sp_fss_GetCrewDutyRules]    Script Date: 02/28/2014 13:46:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetCrewDutyRules]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetCrewDutyRules]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetCrewDutyRules]    Script Date: 02/28/2014 13:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetCrewDutyRules]  
(  
 @CustomerID BIGINT  
 ,@CrewDutyRuleID BIGINT  
    ,@CrewDutyRuleCD char(4)    
 ,@FetchActiveOnly BIT  
)    
AS  
-- =============================================              
-- Author: Sridhar             
-- Create date: 27/02/2014              
-- Description: Get ALL Crew Duty Rule details for Popup        
-- Exec sp_fss_GetCrewDutyRules 10099,0,'',1  
-- Exec sp_fss_GetCrewDutyRules 10099,0,'',0  
-- Exec sp_fss_GetCrewDutyRules 10099, 10099139,'', 0  
-- Exec sp_fss_GetCrewDutyRules 10099, 0,'91D ',0  
-- Exec sp_fss_GetCrewDutyRules 10099, 0,'99',0  
-- =============================================            
  
SET NOCOUNT ON   
BEGIN   
  
 SELECT       
   CrewDutyRulesID,    
   CrewDutyRuleCD,    
   CustomerID,    
   CrewDutyRulesDescription,     
   FedAviatRegNum,    
   DutyDayBeingTM,    
   DutyDayEndTM,    
   MaximumDutyHrs,    
   MaximumFlightHrs,    
   MinimumRestHrs,    
   RestMultiple,    
   RestMultipleHrs,    
   LastUpdUID,    
   LastUptTM    
   ,ISNULL(IsDeleted,0) IsDeleted    
   ,ISNULL(IsInactive,0) IsInactive          
 FROM    
   CrewDutyRules  
 WHERE   
   CustomerID = @CustomerID   
   AND CrewDutyRulesID = case when @CrewDutyRuleID <>0 then @CrewDutyRuleID else CrewDutyRulesID end   
   AND CrewDutyRuleCD = case when Ltrim(Rtrim(@CrewDutyRuleCD)) <> '' then @CrewDutyRuleCD else CrewDutyRuleCD end                                    
   AND ISNULL(IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(IsInActive,0) else 0 end  
   AND ISNULL(IsDeleted,0) = 0         
 order by CrewDutyRuleCD    
   
END
GO

