IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_UpdateOtherCrewDutyLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_UpdateOtherCrewDutyLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Postflight_UpdateOtherCrewDutyLog]
(
	@SimulatorID bigint,
	@CustomerID bigint,
	@CrewID bigint,
	@SessionDT date,
	@AircraftID bigint,
	@DutyTYPE varchar(20),
	@TakeOffDay numeric(2, 0),
	@TakeOffNight numeric(2, 0),
	@LandingDay numeric(2, 0),
	@LandingNight numeric(2, 0),
	@ApproachPrecision numeric(2, 0),
	@ApproachNonPrecision numeric(2, 0),
	@Instrument numeric(7, 3),
	@Night numeric(7, 3),
	@FlightHours numeric(7, 3),
	@DutyHours numeric(7, 3),
	@Specification1 bit,
	@Sepcification2 bit,
	@HomeBaseID bigint,
	@LastUserID varchar(30),
	@LastUptTS datetime,
	@TripID bigint,
	@Specification3 numeric(7, 3),
	@Specification4 numeric(7, 3),
	@DutyTypeID bigint,
	@AirportID bigint,
	@ClientID bigint,
	@SimulatorDescription varchar(25),
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@DepartureTMGMT datetime,
	@ArrivalTMGMT datetime,
	@IsDeleted bit
	)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE [PostflightSimulatorLog] SET  
	 [CrewID] = @CrewID, [SessionDT] = @SessionDT,
	 [AircraftID] = @AircraftID,
	 [DutyTYPE] = @DutyTYPE,
	 [TakeOffDay] = @TakeOffDay,
	 [TakeOffNight] = @TakeOffNight,
	 [LandingDay] = @LandingDay,
	 [LandingNight] = @LandingNight,
	 [ApproachPrecision] = @ApproachPrecision,
	 [ApproachNonPrecision] = @ApproachNonPrecision,
	 [Instrument] = @Instrument,
	 [Night] = @Night,
	 [FlightHours] = @FlightHours,
	 [DutyHours] = @DutyHours,
	 [Specification1] = @Specification1,
	 [Sepcification2] = @Sepcification2,
	 [HomeBaseID] = @HomeBaseID,
	 [LastUserID] = @LastUserID,
	 [LastUptTS] = @LastUptTS,
	 [TripID] = @TripID,
	 [Specification3] = @Specification3,
	 [Specification4] = @Specification4,
	 [DutyTypeID] = @DutyTypeID,
	 [AirportID] = @AirportID,
	 [ClientID] = @ClientID,
	 [SimulatorDescription] = @SimulatorDescription,
	 [DepartureDTTMLocal] = @DepartureDTTMLocal,
	 [ArrivalDTTMLocal] = @ArrivalDTTMLocal,
	 [DepartureTMGMT] = @DepartureTMGMT,
	 [ArrivalTMGMT] = @ArrivalTMGMT,
	 [IsDeleted] = @IsDeleted 
	 WHERE
	 [SimulatorID] = @SimulatorID
	 AND [CustomerID] = @CustomerID

END

GO


