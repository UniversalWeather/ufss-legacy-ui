/****** Object:  StoredProcedure [dbo].[spFlightpak_CorporateRequest_GetAllCRMain]    Script Date: 02/25/2013 18:16:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightpak_CorporateRequest_GetAllCRMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRMain]
GO


CREATE PROCEDURE [dbo].[spFlightpak_CorporateRequest_GetAllCRMain]
(@CustomerID BIGINT
,@CRMainID BIGINT)
AS BEGIN

	SET NOCOUNT OFF;
			
	SELECT	CRMain.CRMainID
			,CRMain.CustomerID
			,CRMain.CRTripNUM
			,CRMain.Approver
			,CRMain.TripID	--Preflightmain
			,CRMain.TripStatus
			,CRMain.TripSheetStatus
			,CRMain.AcknowledgementStatus
			,CRMain.CharterQuoteStatus
			,CRMain.CorporateRequestStatus
			,CRMain.TravelCoordinatorID		--TravelCoordinator
			,CRMain.TravelCoordinatorName
			,CRMain.TravelCoordinatorPhone
			,CRMain.TravelCoordinatorFax
			,CRMain.DispatchNUM
			,CRMain.EstDepartureDT
			,CRMain.EstArrivalDT
			,CRMain.RecordType
			,CRMain.FleetID		-- Fleet
			,CRMain.AircraftID	-- Aircraft
			,CRMain.PassengerRequestorID	--Passenger
			,CRMain.RequestorName
			,CRMain.RequestorPhone
			,CRMain.DepartmentID	--Department
			,CRMain.DepartmentDescription
			,CRMain.AuthorizationID		--Authorization
			,CRMain.AuthorizationDescription
			,CRMain.HomebaseID		--Company
			,CRMain.ClientID		--Client
			,CRMain.IsPrivate
			,CRMain.CRMainDescription
			,CRMain.RequestDT
			,CRMain.BeginningGMTDT
			,CRMain.EndingGMTDT
			,CRMain.IsCrew
			,CRMain.IsPassenger
			,CRMain.IsAlert
			,CRMain.IsAirportAlert
			,CRMain.IsLog
			,CRMain.LastUpdUID
			,CRMain.LastUpdTS
			,CRMain.IsDeleted			
			,PreflightMain.TripNUM
			,PreflightMain.TripDescription
			,TravelCoordinator.TravelCoordCD
			,Fleet.TailNum
			,Fleet.TypeDescription
			,Aircraft.AircraftCD
			,Aircraft.AircraftDescription
			,Passenger.PassengerRequestorCD
			,Passenger.PassengerName
			,Department.DepartmentCD
			,Department.DepartmentName
			,DepartmentAuthorization.AuthorizationCD
			,DepartmentAuthorization.DeptAuthDescription
			,Company.BaseDescription
			,Airport.IcaoID
			,Client.ClientCD
			,Client.ClientDescription
	FROM CRMain
	LEFT OUTER JOIN PreflightMain ON PreflightMain.TripID = CRMain.TripID
	LEFT OUTER JOIN TravelCoordinator ON TravelCoordinator.TravelCoordinatorID = CRMain.TravelCoordinatorID
	LEFT OUTER JOIN Fleet ON Fleet.FleetID = CRMain.FleetID
	LEFT OUTER JOIN Aircraft ON Aircraft.AircraftID = CRMain.AircraftID
	LEFT OUTER JOIN Passenger ON Passenger.PassengerRequestorID = CRMain.PassengerRequestorID
	LEFT OUTER JOIN Department ON Department.DepartmentID = CRMain.DepartmentID
	LEFT OUTER JOIN [DepartmentAuthorization] ON [DepartmentAuthorization].AuthorizationID = CRMain.AuthorizationID
	LEFT OUTER JOIN Company ON Company.HomebaseID = CRMain.HomebaseID
	LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID
	LEFT OUTER JOIN Client ON Client.ClientID = CRMain.ClientID	
	WHERE	CRMain.CustomerID = @CustomerID
		AND CRMain.IsDeleted = 0
		AND CRMain.CorporateRequestStatus = 'S'
		
		--AND CRMain.CRMainID =	CASE WHEN ((@CRMainID IS NULL) OR (@CRMainID <> -1))
		--				THEN (@CRMainID)
		--				ELSE (SELECT CRMainID FROM CRMain
		--						WHERE CustomerID = @CustomerID
		--								AND CRMain.CorporateRequestStatus = 'S'
		--								AND IsDeleted = 0)
						--END
END

GO
