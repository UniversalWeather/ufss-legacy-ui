SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetUserGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetUserGroup]
go
CREATE PROCEDURE spGetUserGroup
(@CustomerID bigint)
AS BEGIN

	SELECT UserGroupID
			,UserGroupCD
			,UserGroupDescription
			,CustomerID
			,LastUpdUID
			,LastUpdTS
			,IsDeleted
			,IsInactive
		FROM UserGroup
	WHERE CustomerID = @CustomerID AND ISNULL(IsDeleted,0)=0

END
