/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddCrewCalendarEntries]    Script Date: 01/10/2013 18:36:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddCrewCalendarEntries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddCrewCalendarEntries]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_AddCrewCalendarEntries]    Script Date: 01/10/2013 18:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author: Veerendra  
-- Create date: 24/7/2012  
-- Description: Insert the Crew Calendar Entries 

-- Update date: 31/7/2012  
-- Description: Insert the TripNum in Leg & Main Table.
-- =============================================  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddCrewCalendarEntries]  
	-- Add the parameters for the stored procedure here
(
@CustomerID BIGINT,
@TailNum CHAR(6),
@HomebaseID BIGINT,
@DepartureDTTMLocal DATETIME,
@ArrivalDTTMLocal DATETIME,
@DepartureGreenwichDTTM DATETIME,
@ArrivalGreenwichDTTM DATETIME,
@HomeDepartureDTTM DATETIME,
@HomeArrivalDTTM DATETIME,
@DepartICAOID BIGINT,
@DutyTYPE CHAR(2),
@Notes TEXT,
@ClientID BIGINT,
@CrewIDs VARCHAR(400),
@FleetID BIGINT,
@LastUpdUID CHAR(30),
@NoteSec VARCHAR(max)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@ClientID=0)
	begin
	set @ClientID=null
	end
	
	if(@FleetID=0)
	begin
	set @FleetID=null
	end
    -- Insert statements for procedure here
    
DECLARE  @PreviousNUM BIGINT 
DECLARE @ChkMultipleCrew INT
SELECT @ChkMultipleCrew=Count(*) FROM dbo.Split(@CrewIDs,',')
If(@ChkMultipleCrew>1)
	BEGIN
		EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @PreviousNUM OUTPUT 
	END
ELSE
	BEGIN
		SET @PreviousNUM=0
	END

    
 --DECLARE  @FleetID BIGINT
	--SELECT @FleetID = FleetID FROM Fleet WHERE TailNum=@TailNum and CustomerID=@CustomerID

Declare @Counter int
Set @Counter=0

WHILE(@Counter<@ChkMultipleCrew)
BEGIN	

DECLARE @CrewID BIGINT
SELECT @CrewID=CAST(s as BIGINT) FROM dbo.SplitString(@CrewIDs,',') Where zeroBasedOccurance=@Counter

DECLARE  @TripID BIGINT 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @TripID OUTPUT 

DECLARE  @TripNUM BIGINT 
SET @TripNUM = 0
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'TripCurrentNo',  @TripNUM  OUTPUT 
SELECT @TripNUM = TripCurrentNo  from [FlightpakSequence] where customerID = @CustomerID

			
	INSERT INTO dbo.PreflightMain
				(	TripID,
					FleetID,
					TripNUM,
					CustomerID,
					Notes,
					HomebaseID,
					RecordType,
					IsDeleted,
					ClientID,
					PreviousNUM,
					CrewID,
					LastUpdUID,
					LastUpdTS,
					CrewCalendarNotes
					)
			VALUES( @TripID,
					@FleetID,
					@TripNUM,
					@CustomerID,
					@Notes,
					@HomebaseID,
					'C',
					0,
					@ClientID,
					@PreviousNUM,
					@CrewID,
					@LastUpdUID,
					GETUTCDATE(),
					@NoteSec
					)
					
DECLARE  @LegID BIGINT 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @LegID OUTPUT 

	INSERT INTO dbo.PreflightLeg
				(	LegID,
					TripID,
					LegNUM,
					TripNUM,
					CustomerID,
					DepartICAOID,
					ArriveICAOID,
					DepartureDTTMLocal,
					ArrivalDTTMLocal,
					DepartureGreenwichDTTM,
					ArrivalGreenwichDTTM,
					HomeDepartureDTTM,
					HomeArrivalDTTM,
					DutyTYPE,
					IsDeleted,
					ClientID,
					LastUpdUID,
					LastUpdTS)
			VALUES( @LegID,
					@TripID,
					1,
					@TripNUM,
					@CustomerID,
					@DepartICAOID,
					@DepartICAOID,
					@DepartureDTTMLocal,
					@ArrivalDTTMLocal,
					@DepartureGreenwichDTTM,
					@ArrivalGreenwichDTTM,
					@HomeDepartureDTTM,
					@HomeArrivalDTTM,
					@DutyTYPE,
					0,
					@ClientID,
					@LastUpdUID,
					GETUTCDATE())
			
DECLARE  @PreflightCrewListID BIGINT 
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @PreflightCrewListID OUTPUT 
	
	INSERT INTO dbo.PreflightCrewList
				(	PreflightCrewListID,
					LegID,
					CustomerID,
					CrewID,
					IsDeleted,
					LastUpdUID,
					LastUpdTS)
			VALUES( @PreflightCrewListID,
					@LegID,
					@CustomerID,
					@CrewID,
					0,
					@LastUpdUID,
					GETUTCDATE())

SET @Counter = @Counter + 1	
	
END			
END

GO


