
/****** Object:  StoredProcedure [dbo].[spAddBudget]    Script Date: 08/25/2012 17:15:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddBudget]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddBudget]
GO
/****** Object:  StoredProcedure [dbo].[spAddBudget]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddBudget](@CustomerID BIGINT,
                                    @AccountID BIGINT,
                                    @FleetID  BIGINT,
                                    @AccountNum VARCHAR(32),
                                    @FiscalYear CHAR(4),
                                    @Month1Budget NUMERIC(17,2),
                                    @Month2Budget NUMERIC(17,2),
                                    @Month3Budget NUMERIC(17,2),
                                    @Month4Budget NUMERIC(17,2),
                                    @Month5Budget NUMERIC(17,2),
                                    @Month6Budget NUMERIC(17,2),
                                    @Month7Budget NUMERIC(17,2),
                                    @Month8Budget NUMERIC(17,2),
                                    @Month9Budget NUMERIC(17,2),
                                    @Month10Budget NUMERIC(17,2),
                                    @Month11Budget NUMERIC(17,2),
                                    @Month12Budget NUMERIC(17,2), 
                                    @LastUpdUID VARCHAR(30),
                                    @LastUpdTS DATETIME,
                                    @IsDeleted BIT,
                                    @IsInActive BIT)  
-- =============================================  
-- Author:Sujitha.V  
-- Create date: 10/5/2012  
-- Description: Insert the Budget  information  
-- =============================================  
AS  
BEGIN   
SET NOCOUNT ON  
  
  DECLARE @BudgetID BIGINT
  
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @BudgetID OUTPUT
  set @LastUpdTS = GETUTCDATE()
   INSERT INTO Budget  
           ([BudgetID]
           ,[CustomerID]
           ,[AccountID]
           ,[FleetID]
           ,[AccountNum]
           ,[FiscalYear]
           ,[Month1Budget]
           ,[Month2Budget]
           ,[Month3Budget]
           ,[Month4Budget]
           ,[Month5Budget]
           ,[Month6Budget]
           ,[Month7Budget]
           ,[Month8Budget]
           ,[Month9Budget]
           ,[Month10Budget]
           ,[Month11Budget]
           ,[Month12Budget]
           ,[LastUpdUID]
           ,[LastUpdTS]
           ,[IsDeleted]
           ,[IsInActive])  
     VALUES(@BudgetID
           ,@CustomerID
           ,@AccountID
           ,@FleetID
           ,@AccountNum
           ,@FiscalYear
           ,@Month1Budget
           ,@Month2Budget
           ,@Month3Budget
           ,@Month4Budget
           ,@Month5Budget
           ,@Month6Budget
           ,@Month7Budget
           ,@Month8Budget
           ,@Month9Budget
           ,@Month10Budget
           ,@Month11Budget
           ,@Month12Budget
           ,@LastUpdUID
           ,@LastUpdTS
           ,@IsDeleted
           ,@IsInActive)  
  
END
GO
