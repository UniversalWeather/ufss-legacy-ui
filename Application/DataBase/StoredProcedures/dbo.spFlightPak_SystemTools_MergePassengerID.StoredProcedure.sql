
/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergePassengerID]    Script Date: 01/22/2013 14:37:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SystemTools_MergePassengerID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SystemTools_MergePassengerID]
GO

/****** Object:  StoredProcedure [dbo].[spFlightPak_SystemTools_MergePassengerID]    Script Date: 01/22/2013 14:37:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_SystemTools_MergePassengerID]
(
@OldPassengerID BIGINT,
@NewPassengerID BIGINT,
@CustomerID BIGINT,
@LastUpdUID CHAR(30),
@PaxCD Varchar(5)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MergeSuccess bit
	set @MergeSuccess = 0
	if @NewPassengerID  = 0 
	begin
		begin Try
		begin Transaction
			update Passenger set 
			PassengerRequestorCD =@PaxCD 
			,LastUpdTS = GETUTCDATE()
			,LastUpdUID = @LastUpdUID
			where CustomerID = @CustomerID and PassengerRequestorID = @OldPassengerID 
		
		commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	end
	else
	begin	
	
	begin Try
				
			begin Transaction

		  -- PassengerAdditionalInfo 		 
			UPDATE PassengerAdditionalInfo  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUptTS=GETUTCDATE()
			WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
			
		  --  CrewPassengerPassport		  
		    UPDATE CrewPassengerPassport  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		    WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
			
		  -- PostflightPassenger			
		    UPDATE PostflightPassenger  
				SET PassengerID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE PassengerID =@OldPassengerID and CustomerID=@CustomerID
			
		   -- PostflightSIFL		
			UPDATE PostflightSIFL  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
			WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID	
			
		   -- PreflightTripSIFL		   
		   UPDATE PreflightTripSIFL  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- PassengerGroupOrder		   
		   UPDATE PassengerGroupOrder  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- CrewPassengerVisa		   
		   UPDATE CrewPassengerVisa  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- sscord - No Table in the DataBase
		   
		   -- ssmain - No Table in the DataBase
		   
		   -- TravelCoordinatorRequestor		   
		   UPDATE TravelCoordinatorRequestor  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- usermaster		   
		   UPDATE usermaster  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- uvtables - No Table in the DataBase
		   
		   -- PreflightPassengerList		   
		    UPDATE PreflightPassengerList  
				SET PassengerID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- CQPassenger		   
		   UPDATE CQPassenger  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- PostflightMain		   
		   UPDATE PostflightMain  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		  -- PreflightMain 		  
		  UPDATE PreflightMain  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- PreflightLeg
		   UPDATE PreflightLeg
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- CQMain
		   UPDATE CQMain  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- CRMain
		   UPDATE CRMain  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- crleg
		   UPDATE crleg  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		   -- CRPassenger
		   UPDATE CRPassenger  
				SET PassengerRequestorID =@NewPassengerID,		
				LastUpdUID=@LastUpdUID,
				LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID =@OldPassengerID and CustomerID=@CustomerID
		   
		  --Passenegr
		   UPDATE Passenger SET IsDeleted = 1,LastUpdUID=@LastUpdUID,LastUpdTS=GETUTCDATE()
		   WHERE PassengerRequestorID = @OldPassengerID
			
			commit transaction
			set @MergeSuccess = 1
		
		END TRY
		
		BEGIN CATCH
			set @MergeSuccess = 0
			Rollback transaction
		END CATCH
	END
	select   @MergeSuccess as 'MergeSuccess'


END

GO


