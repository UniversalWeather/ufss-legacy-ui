
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAirportPair]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAirportPair]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAirportPair]
GO

CREATE PROCEDURE spUpdateAirportPair
(@AirportPairID	BIGINT,
@CustomerID	BIGINT,
@AirportPairNumber	INT,
@AircraftID	BIGINT,
@WindQTR NUMERIC(1),
@AirportPairDescription VARCHAR(40),
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()	
		
	UPDATE AirportPair
	SET CustomerID = @CustomerID,
		AirportPairNumber = @AirportPairNumber,
		AircraftID = @AircraftID,
		WindQTR = @WindQTR,
		AirportPairDescription = @AirportPairDescription,
		LastUpdUID = @LastUpdUID,
		LastUpdTS = @LastUpdTS,
		IsDeleted = @IsDeleted
	WHERE AirportPairID = @AirportPairID
	
END