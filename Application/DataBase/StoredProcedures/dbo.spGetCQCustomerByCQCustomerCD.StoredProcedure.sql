/****** Object:  StoredProcedure [dbo].[spGetCQCustomerByCQCustomerCD]    Script Date: 03/27/2013 14:25:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetCQCustomerByCQCustomerCD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetCQCustomerByCQCustomerCD]
GO

CREATE PROCEDURE [dbo].[spGetCQCustomerByCQCustomerCD]
(@CustomerID BIGINT
,@CQCustomerCD VARCHAR(5))
AS BEGIN

	SET NOCOUNT OFF;
	
	SELECT	CQCustomer.CQCustomerID
			,CQCustomer.CustomerID
			,CASE WHEN CQCustomer.CQCustomerCD IS NULL THEN '' ELSE LTRIM(RTRIM(CQCustomer.CQCustomerCD)) END AS CQCustomerCD
			,CQCustomer.CQCustomerName
			,CQCustomer.IsApplicationFiled
			,CQCustomer.IsApproved
			,CQCustomer.BillingName
			,CQCustomer.BillingAddr1
			,CQCustomer.BillingAddr2
			,CQCustomer.BillingAddr3
			,CQCustomer.BillingCity
			,CQCustomer.BillingState
			,CQCustomer.BillingZip
			,CQCustomer.CountryID
			,CQCustomer.BillingPhoneNum
			,CQCustomer.BillingFaxNum
			,CQCustomer.Notes
			,CQCustomer.Credit
			,CQCustomer.DiscountPercentage
			,CQCustomer.IsInActive
			,CQCustomer.HomebaseID
			,CQCustomer.AirportID
			,CQCustomer.DateAddedDT
			,CQCustomer.WebAddress
			,CQCustomer.EmailID
			,CQCustomer.TollFreePhone
			,CQCustomer.LastUpdUID
			,CQCustomer.LastUpdTS
			,CQCustomer.IsDeleted
			,Country.CountryCD AS CountryCD
			,Country.CountryName AS CountryName
			,Company.BaseDescription AS BaseDescription
			,HBAirport.IcaoID AS HomeBaseIcaoID
			,Airport.IcaoID AS AirportIcaoID
			,CQCustomer.CustomerType
			,CQCustomer.IntlStdCrewNum
			,CQCustomer.DomesticStdCrewNum
			,CQCustomer.MinimumDayUseHrs
			,CQCustomer.DailyUsageAdjTax
			,CQCustomer.LandingFeeTax
			,CQCustomer.MarginalPercentage	
	FROM	CQCustomer
	LEFT OUTER JOIN Country ON Country.CountryID = CQCustomer.CountryID
	LEFT OUTER JOIN Company ON Company.HomebaseID = CQCustomer.HomebaseID
	LEFT OUTER JOIN Airport AS HBAirport ON HBAirport.AirportID = Company.HomebaseAirportID
	LEFT OUTER JOIN Airport ON Airport.AirportID = CQCustomer.AirportID
	WHERE	CQCustomer.CustomerID = @CustomerID
		AND CQCustomer.IsDeleted = 0
		AND CQCustomer.CQCustomerCD = @CQCustomerCD
	ORDER BY CQCustomer.CQCustomerCD ASC
				
END

GO

