

/****** Object:  StoredProcedure [dbo].[SpGetCQReportHeader]    Script Date: 04/25/2013 15:02:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetCQReportHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetCQReportHeader]
GO


/****** Object:  StoredProcedure [dbo].[SpGetCQReportHeader]    Script Date: 04/25/2013 15:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SpGetCQReportHeader]
(
   @CustomerID bigint
 )
 as 
 begin
 
 select 
   CQReportHeaderID,ReportID,ReportDescription,CQ.HomebaseID,CQ.UserGroupID,CQ.ClientID,CQ.LastUpdUID,CQ.LastUpdTS,CQ.IsDeleted,u.UserGroupCD,c.ClientCD ,Airport.ICAOID AS HomebaseCD
    from CQReportHeader CQ
    LEFT OUTER JOIN UserGroup u on u.UserGroupID= CQ.UserGroupID 
    LEFT OUTER JOIN Client c on c.ClientID = CQ.ClientID
    LEFT OUTER JOIN Company as Company on Company.HomeBaseID = CQ.HomeBaseID       
    LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID 
    where CQ.customerid = @CustomerID and CQ.IsDeleted = 0
 end

GO


