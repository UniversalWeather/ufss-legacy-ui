IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_SetTSAstatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_SetTSAstatus]

/****** Object:  StoredProcedure [dbo].[spFlightPak_SetTSAstatus]    Script Date: 9/9/2014 7:06:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE dbo.spFlightPak_SetTSAstatus      
(@CustomerID Bigint)      
AS      
BEGIN      
-- =============================================      
-- Author:PremPrakash.S      
-- Create date: 30/7/2012      
-- Description: Updating TSAStatus       
-- Modified By : Srinivasan. J      
-- Begin Tran      
-- go      
-- Execute spFlightPak_SetTSAstatus 10000      
-- go      
-- Rollback tran      
--Updated BY Ramesh. J on 3rd Dec 2012
--Chnages : Commented the condition not to include the Status already set as 'R','C','S'. 
--			Commented the DateofBirth check condition
-- =============================================      
 BEGIN TRANSACTION            
		DECLARE @LastUpdTS DATETIME
		SET @LastUpdTS = GETUTCDATE()
        UPDATE Passenger SET TSAStatus=NULL, TSADTTM=NULL WHERE  CustomerID=@CustomerID      
        ---------------------------------------Passanger----------------------------------------------------------      
        -----------------------------NO FLY-----------------------------------------------------------------------        
        Update Passenger set TSAStatus='R', TSADTTM=@LastUpdTS FROM             
        Passenger pax      
        inner join TSANoFly tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID      
        AND isnull(tsac.FirstName,'') = isnull(pax.FirstName,'')      
        AND isnull(tsac.LastName,'') = isnull(pax.LastName,'')       
        AND isnull(tsac.MiddleName,'') = isnull(pax.MiddleInitial,'')    
      
       /* Update Passenger set TSAStatus='R' FROM      
        Passenger pax      
        inner join TSANoFly tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=10003       
        AND (isnull(tsac.FirstName,'') = isnull(pax.FirstName,'')      
        AND isnull(tsac.LastName,'') = isnull(pax.LastName,'')       
        AND isnull(tsac.MiddleName,'') = isnull(pax.MiddleInitial,'')      
        AND convert(varchar, tsac.DOB, 1)= convert(varchar,  pax.DateOfBirth, 1)       
        AND TSAStatus<>'R' )     */ 
      
        -----------------------------SELECT-----------------------------------------------------------------------       
        Update Passenger set TSAStatus='S', TSADTTM=@LastUpdTS FROM           
        Passenger pax      
        inner join TSASelect tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID       
        AND isnull(tsac.FirstName,'') = isnull(pax.FirstName,'')      
        AND isnull(tsac.LastName,'') = isnull(pax.LastName ,'')      
        AND isnull(tsac.MiddleName,'') = isnull(pax.MiddleInitial,'')
        --AND convert(varchar, tsac.DOB,1) =convert(varchar, pax.DateOfBirth,1)
        --AND TSAStatus<>'R')      
      
-----------------------------CLEARE-----------------------------------------------------------------------       
        Update Passenger set TSAStatus='C', TSADTTM=@LastUpdTS FROM      
        Passenger pax      
        inner join TSAClear tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID       
        AND isnull(tsac.FirstName,'') = isnull(pax.FirstName,'')      
        AND isnull(tsac.LastName,'') = isnull(pax.LastName ,'')      
        AND isnull(tsac.MiddleName,'') = isnull(pax.MiddleInitial,'')      
        --OR convert(varchar, tsac.DOB,1) =convert(varchar, pax.DateOfBirth,1)       
        --AND (TSAStatus<>'R' OR TSAStatus<>'S'))      
      
--------------------------------------------------------------------------------------------------------------------      
      
      
        -----------------------------Trip----------------------------------------------------------      
        UPDATE PreflightPassengerList SET TripStatus=NULL, TSADTTM=NULL WHERE CustomerID=@CustomerID      
        -----------------------------NO FLY-----------------------------------------------------------------------        
        Update PreflightPassengerList set TripStatus='R', TSADTTM=@LastUpdTS FROM              
        Passenger pas       
        inner join     PreflightPassengerList pax on pax.PassengerID=pas.PassengerRequestorID        
      and pax.CustomerID = pas.CustomerID    and pas.TSAStatus = 'R'  
        inner join  PreflightMain ppm on ppm.CustomerID = pax.CustomerID      
        --inner join TSANoFly tsac on tsac.CustomerID = pax.CustomerID                
      where pas.CustomerID=@CustomerID       
        --AND (isnull(tsac.FirstName,'') = isnull(pax.PassengerFirstName,'')      
        --AND isnull(tsac.LastName,'') = isnull(pax.PassengerLastName ,'')      
        --AND isnull(tsac.MiddleName,'') = isnull(pax.PassengerMiddleName,'') )       
        --OR tsac.DOB = pas.DateOfBirth          
        AND ppm.EstDepartureDT >GETDATE()       
      
      
       /* Update PreflightPassengerList set TripStatus='R' FROM      
        Passenger pas       
        inner join     PreflightPassengerList pax on pax.PassengerID=pas.PassengerRequestorID        
      and pax.CustomerID = pas.CustomerID    and pas.TSAStatus = 'R'              
        inner join  PreflightMain ppm on ppm.CustomerID = pax.CustomerID      
        --inner join TSANoFly tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID       
        --AND ((isnull(tsac.FirstName ,'')= isnull(pax.PassengerFirstName ,'')      
        --AND isnull(tsac.LastName,'') = isnull(pax.PassengerLastName ,'')      
        --AND isnull(tsac.MiddleName,'') = isnull(pax.PassengerMiddleName,'') )      
        --OR tsac.DOB= pas.DateOfBirth )      
        AND ppm.EstDepartureDT >GETDATE()        
        AND pax.TripStatus<>'R'   */   
      
        -----------------------------SELECT-----------------------------------------------------------------------       
        Update PreflightPassengerList set TripStatus='S', TSADTTM=@LastUpdTS FROM      
        Passenger pas       
        inner join     PreflightPassengerList pax on pax.PassengerID=pas.PassengerRequestorID        
      and pax.CustomerID = pas.CustomerID            and pas.TSAStatus = 'S'      
        inner join  PreflightMain ppm on ppm.CustomerID = pax.CustomerID      
        --inner join TSASelect tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID       
        --AND ((isnull(tsac.FirstName,'') = isnull(pax.PassengerFirstName,'')      
        --AND isnull(tsac.LastName,'') = isnull(pax.PassengerLastName ,'')      
        --AND isnull(tsac.MiddleName,'') = isnull(pax.PassengerMiddleName,'') )      
        --OR tsac.DOB= pas.DateOfBirth )      
        AND ppm.EstDepartureDT >GETDATE()        
      -- AND pax.TripStatus<>'R'      
      
        -----------------------------CLEARE-----------------------------------------------------------------------       
        Update PreflightPassengerList set TripStatus='C', TSADTTM=@LastUpdTS FROM      
        Passenger pas       
        inner join     PreflightPassengerList pax on pax.PassengerID=pas.PassengerRequestorID        
      and pax.CustomerID = pas.CustomerID                and pas.TSAStatus = 'C'  
        inner join  PreflightMain ppm on ppm.CustomerID = pax.CustomerID      
        --inner join TSAClear tsac on tsac.CustomerID = pax.CustomerID       
        where pax.CustomerID=@CustomerID       
        --AND ((isnull(tsac.FirstName,'') = isnull(pax.PassengerFirstName,'')      
        --AND isnull(tsac.LastName ,'')= isnull(pax.PassengerLastName ,'')      
        --AND isnull(tsac.MiddleName ,'')= isnull(pax.PassengerMiddleName,''))       
        --OR tsac.DOB= pas.DateOfBirth)      
        AND ppm.EstDepartureDT > GETDATE()        
       -- AND (pax.TripStatus<>'R' OR pax.TripStatus<>'S')       
              
SELECT @CustomerID AS CustomerID      
              
COMMIT      
END

