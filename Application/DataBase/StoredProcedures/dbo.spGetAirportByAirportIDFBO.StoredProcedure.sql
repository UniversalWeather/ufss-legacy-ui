/****** Object:  StoredProcedure [dbo].[spGetAirportByAirportIDFBO]    Script Date: 09/13/2012 12:45:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAirportByAirportIDFBO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAirportByAirportIDFBO]
GO
/****** Object:  StoredProcedure [dbo].[spGetAirportByAirportIDFBO]    Script Date: 09/13/2012 12:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAirportByAirportIDFBO](@AirportID BIGINT, @CustomerID BIGINT)      
AS      
-- =============================================      
-- Author: Manish Varma    
-- Create date:     
-- Description: Get Airport based on the Airport ID     
-- also included missing IsDeleted condition    
-- Usuage exec spGetAirportByAirportID 1000, 1000117194    
-- =============================================      
SET NOCOUNT ON  
  
DECLARE @UWACustomerID BIGINT            
  
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID OUTPUT            
                 
DECLARE @ICAOCD VARCHAR(50)      
DECLARE @UWAAirportId VARCHAR(50)          
  
SELECT @ICAOCD = icaoid FROM Airport WHERE AirportID = @AirportID    
  
SELECT @UWAAirportId = AirportID FROM Airport WHERE CustomerID = @UWACustomerID AND IcaoID = @ICAOCD      
  
IF NOT EXISTS(SELECT 1 FROM FBO WHERE AirportID = @AirportID AND CustomerID = @CustomerID)

BEGIN
 
SELECT  A.[AirportID]    
  ,[IcaoID]    
  ,A.[CustomerID]    
  ,A.[CityName]    
  ,A.[StateName]    
  ,A.[CountryName]    
  ,A.[CountryID]    
  ,[AirportName]    
  ,[LatitudeDegree]    
  ,[LatitudeMinutes]    
  ,[LatitudeNorthSouth]    
  ,[LongitudeDegrees]    
  ,[LongitudeMinutes]    
  ,[LongitudeEastWest]    
  ,[MagneticVariance]    
  ,[VarianceEastWest]    
  ,[Elevation]    
  ,[LongestRunway]    
  ,[IsEntryPort]    
  ,[WindZone]    
  ,[IsUSTax]    
  ,[IsRural]    
  ,[IsAirport]    
  ,[IsHeliport]
  ,[IsSeaPlane]    
  ,[AirportInfo]    
  ,[OffsetToGMT]    
  ,[IsDayLightSaving]    
  ,[DayLightSavingStartDT]    
  ,[DaylLightSavingStartTM]    
  ,[DayLightSavingEndDT]    
  ,[DayLightSavingEndTM]    
  ,[TowerFreq]    
  ,[ATISFreq]    
  ,[ARINCFreq]    
  ,[GroundFreq]    
  ,[ApproachFreq]    
  ,[DepartFreq]    
  ,[Label1Freq]    
  ,[Freq1]    
  ,[Label2Freq]    
  ,[Freq2]    
  ,[TakeoffBIAS]    
  ,[LandingBIAS]    
  ,[Alerts]    
  ,[GeneralNotes]    
  ,[IsFlightPakFlag]    
  ,[FBOCnt]    
  ,[HotelCnt]    
  ,[TransportationCnt]    
  ,[CateringCnt]    
  ,[FlightPakUPD]    
  ,A.[MetroID]    
  ,A.[DSTRegionID]    
  ,[PPR]    
  ,[Iata]    
  ,[AlternateDestination1]    
  ,[AlternateDestination2]    
  ,[AlternateDestination3]    
  ,A.[LastUpdUID]    
  ,A.[LastUpdTS]    
  ,A.[IsInActive]    
  ,[RecordType]    
  ,[NewICAO]    
  ,[PreviousICAO]    
  ,A.[UpdateDT]    
  ,[IsWorldClock]    
  ,[WidthRunway]    
  ,[FssPhone]    
  ,[LengthWidthRunway]    
  ,[WidthLengthRunway]    
  ,[CustomLocation]    
  ,[CustomPhoneNum]    
  ,[ImmigrationPhoneNum]    
  ,[AgPhoneNum]    
  ,[IsCustomAvailable]    
  ,[PortEntryType]    
  ,[Unicom]    
  ,[Clearence1DEL]    
  ,[Clearence2DEL]    
  ,[AtisPhoneNum]    
  ,[ASOS]    
  ,[AsosPhoneNum]    
  ,[AWOS]    
  ,[AwosPhoneNum]    
  ,[AwosType]    
  ,[IsSlots]    
  ,[FssName]    
  ,[AirportManager]    
  ,[AirportManagerPhoneNum]    
  ,[FAA]    
  ,[UWAID]    
  ,[IsFIX]    
  ,[HoursOfOperation]    
  ,[LandingFeeSmall]    
  ,[LandingFeeMedium]    
  ,[LandingFeeLarge]    
  ,[IsEUETS]    
  ,A.[IsDeleted]    
  ,A.[UWAUpdates]    
  ,A.IsPublic  
  ,A.IsPrivate  
  ,A.IsMilitary  
  ,A.ExchangeRateID  
  ,A.CustomsSundayWorkHours  
  ,A.CustomsMondayWorkHours  
  ,A.CustomsTuesdayWorkHours  
  ,A.CustomsWednesdayWorkHours  
  ,A.CustomsThursdayWorkHours  
  ,A.CustomsFridayWorkHours  
  ,A.CustomsSaturdayWorkHours  
  ,A.CustomNotes  
  ,[RunwayID1]    
  ,[RunwayID2]    
  ,[RunwayID3]    
  ,[RunwayID4]    
  ,[Runway2Length]    
  ,[Runway2Width]    
  ,[Runway3Length]    
  ,[Runway3Width]    
  ,[Runway4Length]    
  ,[Runway4Width]      
  ,M.MetroCD      
  ,C.CountryCD      
  ,D.DSTRegionCD    
  ,F.FBOCD     
  ,F.PhoneNUM1 as FBOPhoneNum1    
  ,F.FaxNum as FBOFaxNum    
  ,F.Contact as FBOContact    
  ,F.Frequency as FBOFrequency  
  ,F.Addr1 as FBOAddr1    
  ,F.Addr2 as FBOAddr2    
  ,F.CityName as FBOCityName    
  ,F.StateName as FBOStateName    
  ,F.PostalZipCD as FBOPostalZipCD    
  ,F.IsCrewCar as FBOIsCrewCar    
  ,F.CreditCardUVAir as FBOCreditCardUVAir    
  ,F.FuelBrand as FBOFuelBrand    
  ,F.LastFuelDT as FBOLastFuelDT    
  ,F.LastFuelPrice as FBOLastFuelPrice    
  ,F.NegotiatedFuelPrice as FBONegotiatedFuelPrice    
  ,F.FBOVendor as FBOName    
      
  FROM  (SELECT *,1 Rk FROM  [Airport] WHERE AirportID=@AirportID) A    
  --INNER JOIN (SELECT AirportID, CustomerID FROM Airport WHERE AirportID = @AirportID AND CustomerID = @CustomerID) B ON A.AirportID = B.AirportID AND 
  LEFT OUTER JOIN [metro] M on A.MetroID = M.MetroID         
  LEFT OUTER JOIN [DSTRegion] D on A.DSTRegionID = D.DSTRegionID        
  LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID          
  LEFT OUTER JOIN (SELECT AirportID, FBOCD, PhoneNUM1, FaxNum, Contact, Frequency, Addr1,Addr2, CityName, StateName,        
  PostalZipCD, IsCrewCar, CreditCardUVAir, FuelBrand, LastFuelDT, LastFuelPrice, NegotiatedFuelPrice, FBOVendor,  
  ROW_NUMBER()OVER(PARTITION BY IsChoice ORDER BY CASE WHEN AirportID=@AirportID THEN 1 ELSE 2 END) Rnk     
  FROM FBO Fb 
  WHERE Fb.IsDeleted = 0  
  AND Fb.IsChoice = 1  
  AND Fb.CustomerID IN (@CustomerID,@UWACustomerID)  
  AND Fb.AirportID  in (@AirportID,@UWAAirportId)) F ON A.Rk=Rnk        
        
  WHERE A.IsDeleted = 0  
  END
  
  ELSE
  
  BEGIN
  
  SELECT  A.[AirportID]    
  ,[IcaoID]    
  ,A.[CustomerID]    
  ,A.[CityName]    
  ,A.[StateName]    
  ,A.[CountryName]    
  ,A.[CountryID]    
  ,[AirportName]    
  ,[LatitudeDegree]    
  ,[LatitudeMinutes]    
  ,[LatitudeNorthSouth]    
  ,[LongitudeDegrees]    
  ,[LongitudeMinutes]    
  ,[LongitudeEastWest]    
  ,[MagneticVariance]    
  ,[VarianceEastWest]    
  ,[Elevation]    
  ,[LongestRunway]    
  ,[IsEntryPort]    
  ,[WindZone]    
  ,[IsUSTax]    
  ,[IsRural]    
  ,[IsAirport]    
  ,[IsHeliport]
  ,[IsSeaPlane]     
  ,[AirportInfo]    
  ,[OffsetToGMT]    
  ,[IsDayLightSaving]    
  ,[DayLightSavingStartDT]    
  ,[DaylLightSavingStartTM]    
  ,[DayLightSavingEndDT]    
  ,[DayLightSavingEndTM]    
  ,[TowerFreq]    
  ,[ATISFreq]    
  ,[ARINCFreq]    
  ,[GroundFreq]    
  ,[ApproachFreq]    
  ,[DepartFreq]    
  ,[Label1Freq]    
  ,[Freq1]    
  ,[Label2Freq]    
  ,[Freq2]    
  ,[TakeoffBIAS]    
  ,[LandingBIAS]    
  ,[Alerts]    
  ,[GeneralNotes]    
  ,[IsFlightPakFlag]    
  ,[FBOCnt]    
  ,[HotelCnt]    
  ,[TransportationCnt]    
  ,[CateringCnt]    
  ,[FlightPakUPD]    
  ,A.[MetroID]    
  ,A.[DSTRegionID]    
  ,[PPR]    
  ,[Iata]    
  ,[AlternateDestination1]    
  ,[AlternateDestination2]    
  ,[AlternateDestination3]    
  ,A.[LastUpdUID]    
  ,A.[LastUpdTS]    
  ,A.[IsInActive]    
  ,[RecordType]    
  ,[NewICAO]    
  ,[PreviousICAO]    
  ,A.[UpdateDT]    
  ,[IsWorldClock]    
  ,[WidthRunway]    
  ,[FssPhone]    
  ,[LengthWidthRunway]    
  ,[WidthLengthRunway]    
  ,[CustomLocation]    
  ,[CustomPhoneNum]    
  ,[ImmigrationPhoneNum]    
  ,[AgPhoneNum]    
  ,[IsCustomAvailable]    
  ,[PortEntryType]    
  ,[Unicom]    
  ,[Clearence1DEL]    
  ,[Clearence2DEL]    
  ,[AtisPhoneNum]    
  ,[ASOS]    
  ,[AsosPhoneNum]    
  ,[AWOS]    
  ,[AwosPhoneNum]    
  ,[AwosType]    
  ,[IsSlots]    
  ,[FssName]    
  ,[AirportManager]    
  ,[AirportManagerPhoneNum]    
  ,[FAA]    
  ,[UWAID]    
  ,[IsFIX]    
  ,[HoursOfOperation]    
  ,[LandingFeeSmall]    
  ,[LandingFeeMedium]    
  ,[LandingFeeLarge]    
  ,[IsEUETS]    
  ,A.[IsDeleted]    
  ,A.[UWAUpdates]    
  ,A.IsPublic  
  ,A.IsPrivate  
  ,A.IsMilitary  
  ,A.ExchangeRateID  
  ,A.CustomsSundayWorkHours  
  ,A.CustomsMondayWorkHours  
  ,A.CustomsTuesdayWorkHours  
  ,A.CustomsWednesdayWorkHours  
  ,A.CustomsThursdayWorkHours  
  ,A.CustomsFridayWorkHours  
  ,A.CustomsSaturdayWorkHours  
  ,A.CustomNotes  
  ,[RunwayID1]    
  ,[RunwayID2]    
  ,[RunwayID3]    
  ,[RunwayID4]    
  ,[Runway2Length]    
  ,[Runway2Width]    
  ,[Runway3Length]    
  ,[Runway3Width]    
  ,[Runway4Length]    
  ,[Runway4Width]      
  ,M.MetroCD      
  ,C.CountryCD      
  ,D.DSTRegionCD    
  ,F.FBOCD     
  ,F.PhoneNUM1 as FBOPhoneNum1    
  ,F.FaxNum as FBOFaxNum    
  ,F.Contact as FBOContact    
  ,F.Frequency as FBOFrequency    
  ,F.Addr1 as FBOAddr1    
  ,F.Addr2 as FBOAddr2    
  ,F.CityName as FBOCityName    
  ,F.StateName as FBOStateName    
  ,F.PostalZipCD as FBOPostalZipCD    
  ,F.IsCrewCar as FBOIsCrewCar    
  ,F.CreditCardUVAir as FBOCreditCardUVAir    
  ,F.FuelBrand as FBOFuelBrand    
  ,F.LastFuelDT as FBOLastFuelDT    
  ,F.LastFuelPrice as FBOLastFuelPrice    
  ,F.NegotiatedFuelPrice as FBONegotiatedFuelPrice    
  ,F.FBOVendor as FBOName    
      
  FROM  (SELECT *,1 Rk FROM Airport WHERE AirportID = @AirportID) A    
  LEFT OUTER JOIN [metro] M on A.MetroID = M.MetroID         
  LEFT OUTER JOIN [DSTRegion] D on A.DSTRegionID = D.DSTRegionID        
  LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID          
  LEFT OUTER  JOIN (SELECT AirportID, FBOCD, PhoneNUM1, FaxNum, Contact, Frequency, Addr1,Addr2, CityName, StateName,        
  PostalZipCD, IsCrewCar, CreditCardUVAir, FuelBrand, LastFuelDT, LastFuelPrice, NegotiatedFuelPrice, FBOVendor,  
  ROW_NUMBER()OVER(PARTITION BY IsChoice ORDER BY CASE WHEN AirportID=@AirportID THEN 1 ELSE 2 END) Rnk     
  FROM FBO Fb 
  WHERE Fb.IsDeleted = 0  
  AND Fb.IsChoice = 1  
  AND Fb.CustomerID IN (@CustomerID)  
  AND Fb.AirportID  in (@AirportID)) F ON A.Rk=Rnk                
        
  WHERE A.IsDeleted = 0  
  
  END

GO