/****** Object:  StoredProcedure [dbo].[spGetPassengerPassportByPassengerRequestorID]    Script Date: 01/17/2013 20:30:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetPassengerPassportByPassengerRequestorID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetPassengerPassportByPassengerRequestorID]
GO

/****** Object:  StoredProcedure [dbo].[spGetPassengerPassportByPassengerRequestorID]    Script Date: 01/17/2013 20:30:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
CREATE PROCEDURE [dbo].[spGetPassengerPassportByPassengerRequestorID]        
(@CustomerID BIGINT,@PassengerRequestorID BIGINT)          
AS            
BEGIN            
-- ====================================================            
-- Author: Leela M          
-- Create date: 09/08/2012            
-- Description: Get Passenger Passport by Passenger Requestor ID    
-- ====================================================      
  
SELECT [CrewPassengerPassport].[PassportID]  
      ,[CrewPassengerPassport].[CrewID]  
      ,[CrewPassengerPassport].[PassengerRequestorID]  
      ,[CrewPassengerPassport].[CustomerID]  
      ,[CrewPassengerPassport].[Choice]  
      ,[CrewPassengerPassport].[IssueCity]  
      ,[CrewPassengerPassport].[PassportNum]  
      ,[CrewPassengerPassport].[PassportExpiryDT]  
      ,[CrewPassengerPassport].[CountryID]  
      ,[CrewPassengerPassport].[IsDefaultPassport]  
      ,[CrewPassengerPassport].[PilotLicenseNum]  
      ,[CrewPassengerPassport].[LastUpdUID]  
      ,[CrewPassengerPassport].[LastUpdTS]  
      ,[CrewPassengerPassport].[IssueDT]  
      ,[CrewPassengerPassport].[IsDeleted]     
      ,[CrewPassengerPassport].[IsInActive]      
  FROM [CrewPassengerPassport]   
WHERE [CrewPassengerPassport].PassengerRequestorID = @PassengerRequestorID and   
[CrewPassengerPassport].CustomerID=@CustomerID and [CrewPassengerPassport].[Choice]=1 and  
[CrewPassengerPassport].[IsDeleted] =0  
  
END  
  

GO


