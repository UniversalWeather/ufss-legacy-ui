/****** Object:  StoredProcedure [dbo].[spGetAllBudget]    Script Date: 01/07/2013 19:34:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllBudget]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllBudget]
GO

/****** Object:  StoredProcedure [dbo].[spGetAllBudget]    Script Date: 01/07/2013 19:34:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAllBudget](@CustomerID  BIGINT)        
AS      
-- =============================================        
-- Author:Sujitha        
-- Create date: 10/5/2012        
-- Modified : Karthikeyan.S      
-- Modifired Date : 03/08/2012      
-- Description: Get the Budget information        
-- =============================================        
BEGIN      
SET NOCOUNT ON        
IF LEN(@CustomerID )>0        
         
 SELECT     [Budget].[BudgetID]      
           ,[Budget].[CustomerID]      
           ,[Budget].[AccountID]      
           ,[Budget].[FleetID]      
           ,[Budget].[AccountNum]      
           ,[Budget].[FiscalYear]      
           ,[Budget].[Month1Budget]      
           ,[Budget].[Month2Budget]      
           ,[Budget].[Month3Budget]      
           ,[Budget].[Month4Budget]      
           ,[Budget].[Month5Budget]      
           ,[Budget].[Month6Budget]      
           ,[Budget].[Month7Budget]      
           ,[Budget].[Month8Budget]      
           ,[Budget].[Month9Budget]      
           ,[Budget].[Month10Budget]      
           ,[Budget].[Month11Budget]      
           ,[Budget].[Month12Budget]      
           ,[Budget].[LastUpdUID]      
           ,[Budget].[LastUpdTS]      
           ,[Budget].[IsDeleted]    
           ,[Budget].[IsInActive]        
           ,Account.AccountDescription as AccountDescription      
           ,Account.HomeBaseID as HomeBaseID      
           ,Fleet.TailNum as TailNumber      
           --,Company.HomebaseCD as HomebaseCD      
      ,Airport.ICAOID AS HomebaseCD          
           FROM  [Budget] as Budget      
 LEFT OUTER JOIN Account as Account on Account.AccountID = [Budget].AccountID      
 LEFT OUTER JOIN Fleet as Fleet on Fleet.FleetID = [Budget].FleetID      
 LEFT OUTER JOIN Company as Company on Company.HomeBaseID = Account.HomeBaseID         
        LEFT OUTER JOIN Airport ON Airport.AirportID = Company.HomebaseAirportID          
 WHERE     [Budget].IsDeleted = 'false'       
 AND       [Budget].CustomerID =@CustomerID      
 order by [Budget].AccountNum      
 END  
GO


