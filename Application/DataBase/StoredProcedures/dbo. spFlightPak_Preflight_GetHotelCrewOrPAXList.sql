
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetHotelCrewOrPAXList]    Script Date: 05/08/2014 14:01:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetHotelCrewOrPAXList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetHotelCrewOrPAXList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetHotelCrewOrPAXList]    Script Date: 05/08/2014 14:01:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetHotelCrewOrPAXList]  
@PreflightHotelListID BIGINT,  
@CustomerID BIGINT,
@CrewPassengerType Varchar(1)
AS                
BEGIN              
SET NOCOUNT ON;    
	select case when @CrewPassengerType = 'P' then PassengerRequestorID else CrewID end as �rewPAXList from PreflightHotelCrewPassengerList PHCL, PreflightHotelList PHL
	where PHCL.PreflightHotelListID = PHL.PreflightHotelListID
	and PHCL.CustomerID = @CUSTOMERID
	and PHL.PreflightHotelListID = @PreflightHotelListID
	and PHL.crewPassengerType =@CrewPassengerType
END         
  
GO


