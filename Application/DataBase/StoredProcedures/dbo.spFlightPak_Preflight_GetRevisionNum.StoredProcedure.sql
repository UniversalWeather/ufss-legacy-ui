
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetRevisionNum]    Script Date: 01/17/2014 11:40:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetRevisionNum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetRevisionNum]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetRevisionNum]    Script Date: 01/17/2014 11:40:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spFlightPak_Preflight_GetRevisionNum]
@CUSTOMERID BIGINT,
@TRIPID BIGINT
AS              
BEGIN            
SET NOCOUNT ON;  
declare @RevisionNUM bigint
SELECT top 1 @RevisionNUM = RevisionNumber from PreflightTripHistory 
WHERE CUSTOMERID=@CUSTOMERID AND TRIPID=@TRIPID
order by LastUpdTS desc

Select isnull(@RevisionNUM,1) as RevisionNUM
END       

--exec [spFlightPak_Preflight_GetRevisionNum] 10002, 1000248384


GO


