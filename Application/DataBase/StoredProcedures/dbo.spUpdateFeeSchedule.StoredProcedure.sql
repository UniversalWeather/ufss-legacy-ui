
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFeeSchedule]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateFeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateFeeSchedule]
GO

CREATE PROCEDURE spUpdateFeeSchedule
(@FeeScheduleID BIGINT,
@CustomerID BIGINT,
@FeeGroupID BIGINT,
@AccountID BIGINT,
@Amount NUMERIC(17,2),
@IsTaxable BIT,
@IsDiscount BIT,
@IsInActive BIT,
@LastUpdUID VARCHAR(30),
@LastUpdTS DATETIME,
@IsDeleted BIT)
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [FeeSchedule]
	SET [CustomerID] = @CustomerID
		,[FeeGroupID] = @FeeGroupID
		,[AccountID] = @AccountID
		,[Amount] = @Amount
		,[IsTaxable] = @IsTaxable
		,[IsDiscount] = @IsDiscount
		,[IsInActive] = @IsInActive
		,[LastUpdUID] = @LastUpdUID
		,[LastUpdTS] = @LastUpdTS
		,[IsDeleted] = @IsDeleted
	WHERE [FeeScheduleID] = @FeeScheduleID

END