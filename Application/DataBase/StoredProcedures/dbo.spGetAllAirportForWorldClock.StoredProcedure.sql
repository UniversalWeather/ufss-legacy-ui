IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllAirportForWorldClock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllAirportForWorldClock]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllAirportForWorldClock]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllAirportForWorldClock](@CustomerID BIGINT)    
AS BEGIN
-- =============================================    
-- Author:SUJITHA V  
-- Optimized By: Manish Varma  
-- Create date: 18/4/2012    
-- Description: Get the AIRPORT information    
-- exec spGetAllAirportForWorldClock 10003
-- =============================================    
	SET NOCOUNT ON    
	
	IF LEN(@CustomerID) >0      
		BEGIN   
			DECLARE @UWACustomerID BIGINT  
			SELECT @UWACustomerID  = dbo.GetUWACustomerID()   
			
			SELECT	A.[AirportID]  
					,A.[IcaoID]  
					,A.[CustomerID]  
					,[CityName]  
					,A.[StateName]  
					,A.[CountryName]  
					,A.[CountryID]  
					,[AirportName]  
					,[OffsetToGMT]  
					,[IsDayLightSaving]  
					,[DayLightSavingStartDT] = CASE Year(DayLightSavingStartDT) WHEN  1900 THEN null
																				WHEN 1899 THEN null 
												ELSE [DayLightSavingStartDT]
												END
					--,[DayLightSavingStartDT]  
					,[DayLightSavingEndDT] = CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DayLightSavingEndDT]
												END
					,[DaylLightSavingStartTM] = CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DaylLightSavingStartTM]
												END

					--,[DayLightSavingEndDT]  
					,[DayLightSavingEndTM]  =CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DayLightSavingEndTM]
												END
					,[GeneralNotes]  
					,[IsWorldClock]  
					,A.[IsDeleted]
			FROM  [Airport] A LEFT OUTER JOIN [metro] M ON A.MetroID = M.MetroID   
			LEFT OUTER JOIN [DSTRegion] D ON A.DSTRegionID = D.DSTRegionID  
			LEFT OUTER JOIN [Country] C ON A.CountryID = C.CountryID
			WHERE A.CustomerID =@CustomerID AND A.IsDeleted =0 --and A.IsInActive <= 0  
					 AND IsWorldClock=1
			
			UNION ALL  
			
			SELECT	A.[AirportID]  
					,A.[IcaoID]  
					,A.[CustomerID]  
					,[CityName]  
					,A.[StateName]  
					,A.[CountryName]  
					,A.[CountryID]  
					,[AirportName]  
					,[OffsetToGMT]  
					,[IsDayLightSaving]  
					,[DayLightSavingStartDT] = CASE Year(DayLightSavingStartDT) WHEN  1900 THEN null
																				WHEN 1899 THEN null 
												ELSE [DayLightSavingStartDT]
												END
					--,[DayLightSavingStartDT]  
					,[DayLightSavingEndDT] = CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DayLightSavingEndDT]
												END
					,[DaylLightSavingStartTM] = CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DaylLightSavingStartTM]
												END

					--,[DayLightSavingEndDT]  
					,[DayLightSavingEndTM]  = CASE Year(DayLightSavingEndDT) WHEN 1900 THEN null
																			WHEN 1899 THEN null 
												ELSE [DayLightSavingEndTM]
												END 
					,[GeneralNotes]  
					,[IsWorldClock]  
					,A.[IsDeleted]
			FROM  [Airport] A LEFT OUTER JOIN [metro] M ON A.MetroID = M.MetroID   
			LEFT OUTER JOIN [DSTRegion] D ON A.DSTRegionID = D.DSTRegionID  
			LEFT OUTER JOIN [Country] C on A.CountryID = C.CountryID
			WHERE A.CustomerID <> @CustomerID AND A.CustomerID = @UWACustomerID  AND A.IsDeleted =0  
				--and A.IsInActive <= 0 
					 AND IsWorldClock=1
				AND IcaoID NOT IN 
			(SELECT DISTINCT IcaoID  FROM Airport WHERE CustomerID = @CustomerID AND IsDeleted = 0 ) ORDER BY CityName,IcaoID 
	END
END