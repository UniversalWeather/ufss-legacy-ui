
/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePaxCheckListDate]    Script Date: 03/28/2013 18:45:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_UpdatePaxCheckListDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_UpdatePaxCheckListDate]
GO



/****** Object:  StoredProcedure [dbo].[spFlightPak_UpdatePaxCheckListDate]    Script Date: 03/28/2013 18:45:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_UpdatePaxCheckListDate]    
(   @PassengerCheckListDetailID bigint,
	@PassengerAdditionalInfoID bigint,
	@PreviousCheckDT datetime,
	@CustomerID bigint,
	@PassengerID bigint,
	@DueDT datetime,
	@AlertDT datetime,
	@FrequencyMonth int,
	@AlertDays int,
	@GraceDays int,
	@GraceDT datetime,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsMonthEnd bit,
	@IsStopCALC bit,
	@IsOneTimeEvent bit,
	@IsNoConflictEvent bit,
	@IsNoChecklistREPT bit,
	@IsInActive bit,
	@Specific int,
	@IsCompleted bit,
	@OriginalDT datetime,
	@IsPrintStatus bit,
	@IsPassedDueAlert bit,
	@Frequency numeric(1, 0),
	@IsNextMonth bit,
	@IsEndCalendarYear bit,
	@IsScheduleCheck bit,
	@IsDeleted bit
	)

AS    
BEGIN  
SET NOCOUNT ON;  
   
 set @LastUpdTS = GETUTCDATE() 
 
UPDATE [PassengerCheckListDetail]
   SET 
      
       PreviousCheckDT = @PreviousCheckDT
      ,DueDT = @DueDT
      ,AlertDT = @AlertDT
      ,FrequencyMonth = @FrequencyMonth
      ,AlertDays = @AlertDays
      ,GraceDays = @GraceDays
      ,GraceDT = @GraceDT
      ,LastUpdUID = @LastUpdUID
      ,LastUpdTS = @LastUpdTS
      ,IsMonthEnd = @IsMonthEnd
      ,IsStopCALC = @IsStopCALC
      ,IsOneTimeEvent = @IsOneTimeEvent
      ,IsNoConflictEvent = @IsNoConflictEvent
      ,IsNoChecklistREPT = @IsNoChecklistREPT
      ,IsInActive = @IsInActive
      ,Specific = @Specific
      ,IsCompleted = @IsCompleted
      ,OriginalDT = @OriginalDT
      ,IsPrintStatus = @IsPrintStatus
      ,IsPassedDueAlert = @IsPassedDueAlert
      ,Frequency = @Frequency
      ,IsNextMonth = @IsNextMonth
      ,IsEndCalendarYear = @IsEndCalendarYear
      ,IsScheduleCheck = @IsScheduleCheck
      ,IsDeleted = @IsDeleted
 WHERE PassengerCheckListDetailID = @PassengerCheckListDetailID
END



GO


