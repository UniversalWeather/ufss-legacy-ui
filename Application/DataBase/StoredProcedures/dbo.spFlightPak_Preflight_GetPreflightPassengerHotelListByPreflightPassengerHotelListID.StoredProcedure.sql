
/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerHotelListByPreflightPassengerHotelListID]    Script Date: 09/20/2013 00:09:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPreflightPassengerHotelListByPreflightPassengerHotelListID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPreflightPassengerHotelListByPreflightPassengerHotelListID]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerHotelListByPreflightPassengerHotelListID]    Script Date: 09/20/2013 00:09:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





create Procedure [dbo].[spFlightPak_Preflight_GetPreflightPassengerHotelListByPreflightPassengerHotelListID]
(
@CustomerID BIGINT,
@PreflightPassengerHotelListID BIGINT
)    
AS    
-- =============================================    
-- Author: Prabhu D
-- Create date: 30/7/2013
-- Description: Get GetPreflightPassengerHotelListBy PreflightPassengerHotelListID 
-- =============================================    
SET NOCOUNT ON    
    
SELECT 
PC.*
FROM PreflightPassengerHotelList PC 
 where 
 PC.PreflightPassengerHotelListID = @PreflightPassengerHotelListID 
 and PC.CustomerID=@CustomerID 
 --and PM.IsDeleted=0
 
 




GO


