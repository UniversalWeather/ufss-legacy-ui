
GO
/****** Object:  StoredProcedure [dbo].[spDeleteCQCustomer]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteCQCustomer]
GO

CREATE PROCEDURE spDeleteCQCustomer
(@CQCustomerID BIGINT
,@CustomerID BIGINT
,@CQCustomerCD VARCHAR(5)
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT)
AS BEGIN

	SET NOCOUNT OFF;
	DECLARE @RecordUsed INT 
	DECLARE @ENTITYNAME VARCHAR(50)  
	SET @ENTITYNAME = N'CQCustomer'; -- Type Table Name
            EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @CQCustomerID, @RecordUsed OUTPUT ;

            IF (@RecordUsed <> 0)
                BEGIN
                RAISERROR(N'-500010', 17, 1)
                END
            ELSE
            BEGIN
	SET @LastUpdTS = GETUTCDATE()
		
	UPDATE CQCustomer
	SET	LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
	WHERE CQCustomerID = @CQCustomerID
	END		
END