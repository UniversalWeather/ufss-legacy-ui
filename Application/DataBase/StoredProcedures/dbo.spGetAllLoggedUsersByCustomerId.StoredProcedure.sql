/****** Object:  StoredProcedure [dbo].[spGetAllLoggedUsersByCustomerId]    Script Date: 09/13/2012 12:38:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllLoggedUsersByCustomerId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllLoggedUsersByCustomerId]
GO
/****** Object:  StoredProcedure [dbo].[spGetAllLoggedUsersByCustomerId]    Script Date: 09/13/2012 12:38:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--This SP is used to get the list of all logged in users by customer id except the user who are requesting    
CREATE PROC [dbo].[spGetAllLoggedUsersByCustomerId]    
@CustomerId bigint,    
@UserName varchar(30),    
@IncludeCurrentUser bit  
AS    
BEGIN   
DECLARE @ListStr varchar(max) 
IF(@IncludeCurrentUser = 0)  
BEGIN  
 SELECT distinct UserMaster.*, FPLoginSession.LoginStartTime,
 (SELECT TOP 1 LastActivityDate FROM FPLoginSession where LoginStatus = 0 AND LoginSessionUID = UserMaster.UserName ORDER BY LoginStartTime DESC) AS LoginEndTIme, 
 (select UserGroupCD + ', ' as 'data()'  from UserGroupMapping 
 JOIN UserGroup ON UserGroup.UserGroupID = UserGroupMapping.UserGroupID
 WHERE UserName = UserMaster.UserName for xml path('')) AS UserGroup
 FROM FPLoginSession    
 JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID    
 
 WHERE FPLoginSession.CustomerID = @CustomerId AND LoginsessionUID <> @UserName AND LoginStatus = 1 
END  
ELSE  
BEGIN  
 SELECT distinct UserMaster.*, FPLoginSession.LoginStartTime,
 (SELECT TOP 1 LastActivityDate FROM FPLoginSession where LoginStatus = 0 AND LoginSessionUID = UserMaster.UserName ORDER BY LoginStartTime DESC) AS LoginEndTIme, 
 (select UserGroupCD + ', ' as 'data()'  from UserGroupMapping 
 JOIN UserGroup ON UserGroup.UserGroupID = UserGroupMapping.UserGroupID
 WHERE UserName = UserMaster.UserName for xml path('')) AS UserGroup
 FROM FPLoginSession    
 JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID  
 WHERE FPLoginSession.CustomerID = @CustomerId AND LoginStatus = 1    
END  
END    

  
GO


