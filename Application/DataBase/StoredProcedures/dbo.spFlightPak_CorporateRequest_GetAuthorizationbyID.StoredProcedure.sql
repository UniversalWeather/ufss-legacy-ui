/****** Object:  StoredProcedure [dbo].[spFlightPak_CorporateRequest_GetAuthorizationbyID]    Script Date: 04/18/2013 20:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CorporateRequest_GetAuthorizationbyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CorporateRequest_GetAuthorizationbyID]
GO


Create procedure [dbo].[spFlightPak_CorporateRequest_GetAuthorizationbyID]
(@CustomerID BIGINT,@AuthorizationID BIGINT ,@DepartmentID BIGINT)  
AS    
-- =============================================    
-- Author:Leela 
-- Create date: 18/4/2013    
-- Description: Get the Authorization information    
-- =============================================    
SET NOCOUNT ON 

SELECT [AuthorizationID]
      ,[DepartmentID]
      ,[AuthorizationCD]
      ,[CustomerID]
      ,[DeptAuthDescription]
      ,[ClientID]
      ,[LastUpdUID]
      ,[LastUpdTS]
      ,[IsInActive]
      ,[AuthorizerPhoneNum]
      ,[IsDeleted]
  FROM DepartmentAuthorization
where IsDeleted = 0  AND DepartmentID=@DepartmentID AND AuthorizationID=@AuthorizationID
GO


