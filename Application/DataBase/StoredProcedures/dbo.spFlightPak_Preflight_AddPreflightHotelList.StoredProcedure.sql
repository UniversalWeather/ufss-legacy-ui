IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_AddPreflightHotelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotelList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spFlightPak_Preflight_AddPreflightHotelList]    
 (
	@PreflightHotelName varchar(60)  ,
	@LegID bigint  ,
	@HotelID bigint  ,
	@AirportID bigint  ,
	@RoomType varchar(60)  ,
	@RoomRent numeric(6, 2)  ,
	@RoomConfirm varchar(max)  ,
	@RoomDescription varchar(max)  ,
	@Street varchar(50)  ,
	@CityName varchar(20)  ,
	@StateName varchar(20)  ,
	@PostalZipCD varchar(20)  ,
	@PhoneNum1 varchar(25)  ,
	@PhoneNum2 varchar(25)  ,
	@PhoneNum3 varchar(25)  ,
	@PhoneNum4 varchar(25)  ,
	@FaxNUM varchar(25)  ,
	@LastUpdUID varchar(30)  ,
	@LastUpdTS datetime  ,
	@IsDeleted bit   ,
	@HotelArrangedBy varchar(10)  ,
	@Rate numeric(17, 2)  ,
	@DateIn date  ,
	@DateOut date  ,
	@IsSmoking bit  ,
	@RoomTypeID bigint  ,
	@NoofBeds numeric(3, 0)  ,
	@IsAllCrewOrPax bit  ,
	@IsCompleted bit  ,
	@IsEarlyCheckIn bit  ,
	@EarlyCheckInOption char(1)  ,
	@SpecialInstructions varchar(100)  ,
	@ConfirmationStatus varchar(max)  ,
	@Comments varchar(max)  ,
	@CustomerID bigint  ,
	@Address1 varchar(40)  ,
	@Address2 varchar(40)  ,
	@Address3 varchar(40)  ,
	@MetroID bigint  ,
	@City varchar(30)  ,
	@StateProvince varchar(25)  ,
	@CountryID bigint  ,
	@PostalCode varchar(15)  ,
	@Status varchar(20)  ,
	@isArrivalHotel bit  ,
	@isDepartureHotel bit  ,
	@crewPassengerType varchar(1),
	@NoofRooms numeric(2,0),
	@ClubCard varchar(100),
	@IsRateCap bit,
	@MaxCapAmount numeric(17,2),
	@IsRequestOnly bit,
	@IsBookNight bit
  )    
AS    
BEGIN     
  DECLARE @PreflightHotelListID BIGINT       
  EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo', @PreflightHotelListID  OUTPUT    
        
  INSERT INTO [dbo].[PreflightHotelList]    
       (	[PreflightHotelListID],  
	[PreflightHotelName], 
	[LegID],   
	[HotelID],   
	[AirportID],   
	[RoomType], 
	[RoomRent], 
	[RoomConfirm], 
	[RoomDescription], 
	[Street], 
	[CityName],
	[StateName], 
	[PostalZipCD], 
	[PhoneNum1], 
	[PhoneNum2], 
	[PhoneNum3], 
	[PhoneNum4], 
	[FaxNUM], 
	[LastUpdUID],
	[LastUpdTS],   
	[IsDeleted], 
	[HotelArrangedBy], 
	[Rate],
	[DateIn],   
	[DateOut],   
	[IsSmoking],   
	[RoomTypeID],   
	[NoofBeds],
	[IsAllCrewOrPax],   
	[IsCompleted],   
	[IsEarlyCheckIn],   
	[EarlyCheckInOption], 
	[SpecialInstructions],
	[ConfirmationStatus], 
	[Comments], 
	[CustomerID],   
	[Address1], 
	[Address2], 
	[Address3], 
	[MetroID],   
	[City], 
	[StateProvince], 
	[CountryID],   
	[PostalCode], 
	[Status], 
	[isArrivalHotel],   
	[isDepartureHotel],   
	[crewPassengerType],
	[NoofRooms],
	[ClubCard],
	[IsRateCap],
	[MaxCapAmount],
	[IsRequestOnly],
	[IsBookNight]	 
       )    
    VALUES    
       (    
     @PreflightHotelListID  
	,@PreflightHotelName 
	,@LegID   
	,@HotelID   
	,@AirportID   
	,@RoomType 
	,@RoomRent 
	,@RoomConfirm 
	,@RoomDescription 
	,@Street 
	,@CityName
	,@StateName 
	,@PostalZipCD 
	,@PhoneNum1 
	,@PhoneNum2 
	,@PhoneNum3 
	,@PhoneNum4 
	,@FaxNUM 
	,@LastUpdUID
	,@LastUpdTS   
	,@IsDeleted 
	,@HotelArrangedBy 
	,@Rate
	,@DateIn   
	,@DateOut   
	,@IsSmoking   
	,@RoomTypeID   
	,@NoofBeds
	,@IsAllCrewOrPax   
	,@IsCompleted   
	,@IsEarlyCheckIn   
	,@EarlyCheckInOption 
	,@SpecialInstructions
	,@ConfirmationStatus 
	,@Comments 
	,@CustomerID   
	,@Address1 
	,@Address2 
	,@Address3 
	,@MetroID   
	,@City 
	,@StateProvince 
	,@CountryID   
	,@PostalCode 
	,@Status 
	,@isArrivalHotel   
	,@isDepartureHotel   
	,@crewPassengerType
	,@NoofRooms
	,@ClubCard
	,@IsRateCap
	,@MaxCapAmount
	,@IsRequestOnly
	,@IsBookNight
    )    
            
  Select @PreflightHotelListID as PreflightHotelListID      
END    
GO
