
GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllDelayType]    Script Date: 03/14/2014 21:58:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetAllDelayType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetAllDelayType]
GO


GO

/****** Object:  StoredProcedure [dbo].[sp_fss_GetAllDelayType]    Script Date: 03/14/2014 21:58:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_fss_GetAllDelayType]
(
	@CustomerID BIGINT
	,@DelayTypeID BIGINT    
	,@DelayTypeCD char(2)     	
	,@FetchActiveOnly BIT 
)
as
-- =============================================                
-- Author: Sridhar               
-- Create date: 14/03/2014                
-- Description: Get All DelayType details for Popup          
-- Exec sp_fss_GetAllDelayType 10099,0,'', 1    
-- Exec sp_fss_GetAllDelayType 10099,0,'', 0    
-- Exec sp_fss_GetAllDelayType 10099, 10099166,'',0    
-- Exec sp_fss_GetAllDelayType 10099, 0, 'WX',0    
-- =============================================  
SET NOCOUNT ON     

IF LEN(@CustomerID) >0
BEGIN

	SELECT  D.DelayTypeID 
			,D.CustomerID 
			,D.DelayTypeCD
			,D.DelayTypeDescription 
			,D.ClientID 
			,D.LastUpdUID 
			,D.LastUpdTS 
			,D.IsSiflExcluded 
			,ISNULL(D.IsDeleted,0) IsDeleted       
			,ISNULL(D.IsInactive,0) IsInactive     
	FROM  
			DelayType D
	WHERE 
			D.CustomerID=@CustomerID  
			AND D.DelayTypeID = case when @DelayTypeID <>0 then @DelayTypeID else D.DelayTypeID end     
			AND D.DelayTypeCD = case when Ltrim(Rtrim(@DelayTypeCD)) <> '' then @DelayTypeCD else D.DelayTypeCD end                                      
			AND ISNULL(D.IsInActive,0) = case when @FetchActiveOnly =0 then ISNULL(D.IsInActive,0) else 0 end    
			AND ISNULL(D.IsDeleted,0) = 0                                    
	
	ORDER BY DelayTypeCD asc

END

GO


