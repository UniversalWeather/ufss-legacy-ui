
/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPreFlightLeg]    Script Date: 02/18/2014 09:30:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_AddPreFlightLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_AddPreFlightLeg]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_AddPreFlightLeg]    Script Date: 02/18/2014 09:30:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_AddPreFlightLeg](@TripID bigint
		,@CustomerID bigint
		,@TripNUM bigint
		,@LegNUM bigint
		,@DepartICAOID bigint
		,@ArriveICAOID bigint
		,@Distance numeric(5)
		,@PowerSetting char(1)
		,@TakeoffBIAS numeric(6, 3)
		,@LandingBias numeric(6, 3)
		,@TrueAirSpeed numeric(3, 0)
		,@WindsBoeingTable numeric(5, 0)
		,@ElapseTM numeric(7, 3)
		,@IsDepartureConfirmed bit
		,@IsArrivalConfirmation bit
		,@IsApproxTM bit
		,@IsScheduledServices bit
		,@DepartureDTTMLocal datetime
		,@ArrivalDTTMLocal datetime
		,@DepartureGreenwichDTTM datetime
		,@ArrivalGreenwichDTTM datetime
		,@HomeDepartureDTTM datetime
		,@HomeArrivalDTTM datetime
		,@GoTime char(5)
		,@FBOID bigint
		,@LogBreak char(3)
		,@FlightPurpose varchar(40)
		,@PassengerRequestorID bigint
		,@RequestorName varchar(60)
		,@DepartmentID bigint
		,@DepartmentName varchar(60)
		,@AuthorizationID bigint
		,@AuthorizationName varchar(60)
		,@PassengerTotal int
		,@SeatTotal int
		,@ReservationTotal int
		,@ReservationAvailable int
		,@WaitNUM int
		,@DutyTYPE char(2)
		,@DutyHours numeric(12, 3)
		,@RestHours numeric(12, 3)
		,@FlightHours numeric(12, 3)
		,@Notes text
		,@FuelLoad numeric(6, 0)
		,@OutbountInstruction text
		,@DutyTYPE1 numeric(1, 0)
		,@IsCrewDiscount bit
		,@CrewDutyRulesID bigint
		,@IsDutyEnd bit
		,@FedAviationRegNUM char(3)
		,@ClientID bigint
		,@LegID1 bigint
		,@WindReliability int
		,@OverrideValue numeric(4, 1)
		,@CheckGroup char(3)
		,@AdditionalCrew char(24)
		,@PilotInCommand char(3)
		,@SecondInCommand char(3)
		,@NextLocalDTTM datetime
		,@NextGMTDTTM datetime
		,@NextHomeDTTM datetime
		,@LastUpdUID char(30)
		,@LastUpdTS datetime
		,@CrewNotes text
		,@IsPrivate bit
		,@WaitList char(1)
		,@FlightNUM char(12)
		,@FlightCost numeric(12, 2)
		,@CrewFuelLoad varchar(500)
		,@IsDeleted bit
		,@UWAID char(40)
		,@USCrossing varchar(75)
		,@Response text
		,@ConfirmID char(25)
		,@CrewDutyAlert varchar(5)
		,@AccountID bigint
		,@FlightCategoryID bigint
		,@LastUpdTSCrewNotes datetime
        ,@LastUpdTSPaxNotes datetime
        ,@CQCustomerID bigint
		,@EstFuelQTY numeric(17,3)
		,@FuelUnits numeric(1,0)
		,@LegFuelComment nvarchar(500)=''
)    
AS    
BEGIN   
DECLARE @LegID BIGINT   
EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PreflightCurrentNo',  @LegID OUTPUT     
INSERT INTO [PreflightLeg]    
					   ([LegID]
			,[TripID]
			,[CustomerID]
			,[TripNUM]
			,[LegNUM]
			,[DepartICAOID]
			,[ArriveICAOID]
			,[Distance]
			,[PowerSetting]
			,[TakeoffBIAS]
			,[LandingBias]
			,[TrueAirSpeed]
			,[WindsBoeingTable]
			,[ElapseTM]
			,[IsDepartureConfirmed]
			,[IsArrivalConfirmation]
			,[IsApproxTM]
			,[IsScheduledServices]
			,[DepartureDTTMLocal]
			,[ArrivalDTTMLocal]
			,[DepartureGreenwichDTTM]
			,[ArrivalGreenwichDTTM]
			,[HomeDepartureDTTM]
			,[HomeArrivalDTTM]
			,[GoTime]
			,[FBOID]
			,[LogBreak]
			,[FlightPurpose]
			,[PassengerRequestorID]
			,[RequestorName]
			,[DepartmentID]
			,[DepartmentName]
			,[AuthorizationID]
			,[AuthorizationName]
			,[PassengerTotal]
			,[SeatTotal]
			,[ReservationTotal]
			,[ReservationAvailable]
			,[WaitNUM]
			,[DutyTYPE]
			,[DutyHours]
			,[RestHours]
			,[FlightHours]
			,[Notes]
			,[FuelLoad]
			,[OutbountInstruction]
			,[DutyTYPE1]
			,[IsCrewDiscount]
			,[CrewDutyRulesID]
			,[IsDutyEnd]
			,[FedAviationRegNUM]
			,[ClientID]
			,[LegID1]
			,[WindReliability]
			,[OverrideValue]
			,[CheckGroup]
			,[AdditionalCrew]
			,[PilotInCommand]
			,[SecondInCommand]
			,[NextLocalDTTM]
			,[NextGMTDTTM]
			,[NextHomeDTTM]
			,[LastUpdUID]
			,[LastUpdTS]
			,[CrewNotes]
			,[IsPrivate]
			,[WaitList]
			,[FlightNUM]
			,[FlightCost]
			,[CrewFuelLoad]
			,[IsDeleted]
			,[UWAID]
			,[USCrossing]
			,[Response]
			,[ConfirmID]
			,[CrewDutyAlert]
			,[AccountID]
			,[FlightCategoryID]
			,[LastUpdTSCrewNotes]
            ,[LastUpdTSPaxNotes]
            ,[CQCustomerID]
			,[EstFuelQTY]
			,[FuelUnits]
			,[LegFuelComment]

)    
     VALUES(@LegID
		,@TripID
		,@CustomerID
		,@TripNUM
		,@LegNUM
		,@DepartICAOID
		,@ArriveICAOID
		,@Distance
		,@PowerSetting
		,@TakeoffBIAS
		,@LandingBias
		,@TrueAirSpeed
		,@WindsBoeingTable
		,@ElapseTM
		,@IsDepartureConfirmed
		,@IsArrivalConfirmation
		,@IsApproxTM
		,@IsScheduledServices
		,@DepartureDTTMLocal
		,@ArrivalDTTMLocal
		,@DepartureGreenwichDTTM
		,@ArrivalGreenwichDTTM
		,@HomeDepartureDTTM
		,@HomeArrivalDTTM
		,@GoTime
		,@FBOID
		,@LogBreak
		,@FlightPurpose
		,@PassengerRequestorID
		,@RequestorName
		,@DepartmentID
		,@DepartmentName
		,@AuthorizationID
		,@AuthorizationName
		,@PassengerTotal
		,@SeatTotal
		,@ReservationTotal
		,@ReservationAvailable
		,@WaitNUM
		,@DutyTYPE
		,CASE WHEN @DutyHours <0 THEN 0 ELSE @DutyHours END
		,CASE WHEN @RestHours<0 THEN 0 ELSE @RestHours  END
		,CASE WHEN @FlightHours<0 THEN 0 ELSE @FlightHours END
		,@Notes
		,@FuelLoad
		,@OutbountInstruction
		,@DutyTYPE1
		,@IsCrewDiscount
		,@CrewDutyRulesID
		,@IsDutyEnd
		,@FedAviationRegNUM
		,@ClientID
		,@LegID1
		,@WindReliability
		,@OverrideValue
		,@CheckGroup
		,@AdditionalCrew
		,@PilotInCommand
		,@SecondInCommand
		,@NextLocalDTTM
		,@NextGMTDTTM
		,@NextHomeDTTM
		,@LastUpdUID
		,@LastUpdTS
		,@CrewNotes
		,@IsPrivate
		,@WaitList
		,@FlightNUM
		,@FlightCost
		,@CrewFuelLoad
		,@IsDeleted
		,@UWAID
		,@USCrossing
		,@Response
		,@ConfirmID
		,@CrewDutyAlert
		,@AccountID
		,@FlightCategoryID
		,@LastUpdTSCrewNotes
		,@LastUpdTSPaxNotes
		,@CQCustomerID
		,@EstFuelQTY
		,@FuelUnits
		,@LegFuelComment

)    

select @LegID as LegID
END



GO
