
/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQFBOList]    Script Date: 03/07/2013 17:40:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_UpdateCQFBOList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFBOList]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_CharterQuote_UpdateCQFBOList]    Script Date: 03/07/2013 17:40:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_UpdateCQFBOList]
(
	@CQFBOListID bigint,
	@CustomerID bigint,
	@CQLegID bigint,
	@QuoteID int,
	@LegID int,
	@RecordType char(2),
	@FBOID bigint,
	@CQFBOListDescription varchar(60),
	@PhoneNUM varchar(25),
	@AirportID bigint,
	@FileNUM int,
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@FaxNUM varchar(25),
	@Email varchar(250)
)
AS
	SET NOCOUNT ON;
	SET @LastUpdTS = GETUTCDATE()
	
	UPDATE [CQFBOList] SET [CQLegID] = @CQLegID, [QuoteID] = @QuoteID, [LegID] = @LegID, [RecordType] = @RecordType, [FBOID] = @FBOID, [CQFBOListDescription] = @CQFBOListDescription, [PhoneNUM] = @PhoneNUM, [AirportID] = @AirportID, [FileNUM] = @FileNUM, [LastUpdUID] = @LastUpdUID, [LastUpdTS] = @LastUpdTS, [IsDeleted] = @IsDeleted ,
		[FaxNUM] = @FaxNUM,
		[Email] = @Email
		WHERE [CQFBOListID] = @CQFBOListID AND [CustomerID] = @CustomerID


GO


