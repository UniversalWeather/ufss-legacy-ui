 /****** Object:  StoredProcedure [dbo].[spUpdateCatering]    Script Date: 09/13/2012 12:37:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCatering]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCatering]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCatering]    Script Date: 09/13/2012 12:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpdateCatering](
	@CateringID BIGINT,
	@AirportID BIGINT,
	@CateringCD CHAR(4),
	@CustomerID BIGINT,
	@IsChoice bit ,
	@CateringVendor varchar(40) ,
	@PhoneNum varchar(25) ,
	@FaxNum varchar(25) ,
	@ContactName varchar(25) ,
	@Remarks varchar(max) ,
	@NegotiatedRate NUMERIC(6, 2) ,
	@LastUpdUID Varchar(30) ,
	@LastUpdTS date ,
	@UpdateDT date ,
	@RecordType char(1) ,
	@SourceID varchar(12) ,
	@ControlNum char(5) ,
	@IsInActive bit ,
	@TollFreePhoneNum varchar(25) ,
	@WebSite varchar(200) ,
	@UWAMaintFlag bit ,
	@UWAID varchar(20) ,
	@IsDeleted bit,
	@UWAUpdates bit,
	@BusinessEmail varchar(250),
	@ContactBusinessPhone varchar(25),
	@CellPhoneNum varchar(25),
	@ContactEmail varchar(250),
	@Addr1 varchar(40),
	@Addr2 varchar(40),
	@Addr3 varchar(40),
	@City varchar(25),
	@StateProvince varchar(10),
	@MetroID BIGINT,	
	@CountryID BIGINT,
	@ExchangeRateID	BIGINT,
@NegotiatedTerms	VARCHAR(800),
@SundayWorkHours	VARCHAR(15),
@MondayWorkHours	VARCHAR(15),
@TuesdayWorkHours	VARCHAR(15),
@WednesdayWorkHours	VARCHAR(15),
@ThursdayWorkHours	VARCHAR(15),
@FridayWorkHours	VARCHAR(15),
@SaturdayWorkHours	VARCHAR(15),
@PostalZipCD VARCHAR(15)
	
	)

-- =============================================
-- Author:Mullai.D
-- Create date: 24/4/2012
-- Description: Update the Catering information
-- Altered by Rajesh: 05-May-2015 (Added PostalZipCD)
-- =============================================
AS
BEGIN
DECLARE @UWACustomerID BIGINT    
  
EXECUTE dbo.sp_GetUWACustomerId @UWACustomerID Output     
      
      
SET NOCOUNT ON        
 DECLARE @ICAOCD VARCHAR(50)
 DECLARE @UWAAirportId VARCHAR(50)    
 select @ICAOCD =  icaoid from Airport where AirportID = @AirportID  
 select @UWAAirportId =  AirportID from Airport where CustomerID = @UWACustomerID and IcaoID = @ICAOCD
SET @LastUpdTS = GETUTCDATE() 
IF @UWAMaintFlag = 'True' AND not exists (SELECT CustomerID FROM Catering WHERE CateringID=@CateringID and CateringCD=@CateringCD and (AirportID = @AirportID or AirportID = @UWAAirportId) and CustomerID=@CustomerID)

BEGIN
SET NOCOUNT ON
EXEC spAddCatering
	@CateringID,
	@AirportID,
	@CateringCD,
	@CustomerID,
	@IsChoice,
	@CateringVendor,
	@PhoneNum,
	@FaxNum,
	@ContactName,
	@Remarks,
	@NegotiatedRate,
	@LastUpdUID,
	@LastUpdTS,
	@UpdateDT,
	@RecordType,
	@SourceID,
	@ControlNum,
	@IsInActive,
	@TollFreePhoneNum,
	@WebSite,
	@UWAMaintFlag,
	@UWAID,
	@IsDeleted,
	@UWAUpdates,
	@BusinessEmail,
	@ContactBusinessPhone,
	@CellPhoneNum,
	@ContactEmail ,
	@Addr1,
	@Addr2,
	@Addr3,
	@City,
	@StateProvince,
	@MetroID,	
	@CountryID
	,@ExchangeRateID  
     ,@NegotiatedTerms  
     ,@SundayWorkHours  
     ,@MondayWorkHours  
     ,@TuesdayWorkHours  
     ,@WednesdayWorkHours  
     ,@ThursdayWorkHours  
     ,@FridayWorkHours  
     ,@SaturdayWorkHours	
	 ,@PostalZipCD
	   
END

ELSE

BEGIN
SET NoCOUNT ON
IF(@IsChoice='true')
   begin
   update Catering set IsChoice='false' where CustomerID=@CustomerID  and (AirportID = @AirportID or AirportID = @UWAAirportId)  
   end 
UPDATE [Catering]
   SET 
            
            IsChoice=@IsChoice
           ,CateringVendor=@CateringVendor
           ,PhoneNum=@PhoneNum
           ,FaxNum=@FaxNum
           ,ContactName=@ContactName
           ,Remarks=@Remarks
           ,NegotiatedRate=@NegotiatedRate
           ,LastUpdUID=@LastUpdUID
           ,LastUpdTS=@LastUpdTS
           ,UpdateDT=@UpdateDT
           ,RecordType=@RecordType
           ,SourceID=@SourceID
           ,ControlNum=@ControlNum
           ,IsInActive=@IsInActive
           ,TollFreePhoneNum=@TollFreePhoneNum
           ,WebSite=@WebSite
           ,UWAMaintFlag=@UWAMaintFlag
           ,UWAID=@UWAID
           ,IsDeleted=@IsDeleted
           ,UWAUpdates=@UWAUpdates
           ,BusinessEmail= @BusinessEmail
	       ,ContactBusinessPhone=@ContactBusinessPhone
	       ,CellPhoneNum = @CellPhoneNum
	,ContactEmail=@ContactEmail 
	,Addr1=@Addr1
	,Addr2=@Addr2
	,Addr3=@Addr3
	,City=@City
	,StateProvince=@StateProvince
	,MetroID=@MetroID	
	,CountryID=@CountryID	
	,ExchangeRateID  = @ExchangeRateID
     ,NegotiatedTerms = @NegotiatedTerms
     ,SundayWorkHours  = @SundayWorkHours
     ,MondayWorkHours  = @MondayWorkHours  
     ,TuesdayWorkHours  = @TuesdayWorkHours  
     ,WednesdayWorkHours  = @WednesdayWorkHours
     ,ThursdayWorkHours  = @ThursdayWorkHours  
     ,FridayWorkHours  = @FridayWorkHours  
     ,SaturdayWorkHours =@SaturdayWorkHours
     ,PostalZipCD =@PostalZipCD   	
 WHERE [CateringID] =@CateringID 

end
end
GO


