
GO
/****** Object:  StoredProcedure [dbo].[spGetAllVendorContact]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllVendorContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllVendorContact]
go
CREATE PROCEDURE [dbo].[spGetAllVendorContact](@CustomerID BIGINT,@VendorID BIGINT,@VendorType char(1))    
AS    
-- =============================================    
-- Author:Karthikeyan.S    
-- Create date: 23/4/2012    
-- Description: Get the Vendor Contact  information    
-- =============================================    
SET NoCOUNT ON    
    
BEGIN    
 SELECT    
  VendorContact.VendorContactID,    
  VendorContact.CustomerID,    
  VendorContact.VendorID,    
  VendorContact.ContactName,    
  VendorContact.VendorType,    
  VendorContact.VendorContactCD,    
  VendorContact.IsChoice,    
  VendorContact.Addr1,    
  VendorContact.Addr2,    
  VendorContact.CityName,    
  VendorContact.StateName,    
  VendorContact.PostalZipCD,    
  VendorContact.CountryID,    
  VendorContact.PhoneNum,    
  VendorContact.FaxNum,    
  VendorContact.CreditName1,    
  VendorContact.CreditNum1,    
  VendorContact.CreditName2,    
  VendorContact.CreditNum2,    
  VendorContact.MoreInfo,    
  VendorContact.Notes,    
  VendorContact.LastUpdUID,    
  VendorContact.LastUpdTS,    
  VendorContact.Title,    
  VendorContact.LastName,    
  VendorContact.FirstName,    
  VendorContact.MiddleName,    
  VendorContact.EmailID,    
  VendorContact.IsDeleted    
    ,Nationality.CountryCD as CountryCD    
       ,Vendor.VendorCD as VendorCD    
         
       ,VendorContact.TollFreePhone  
  ,VendorContact.BusinessEmail  
  ,VendorContact.Website  
  ,VendorContact.Addr3  
  ,VendorContact.BusinessPhone  
  ,VendorContact.CellPhoneNum  
  ,VendorContact.HomeFax  
  ,VendorContact.CellPhoneNum2  
  ,VendorContact.OtherPhone  
    
  ,VendorContact.PersonalEmail  
  ,VendorContact.OtherEmail  
  ,VendorContact.IsInactive
          
      FROM VendorContact as VendorContact    
  LEFT OUTER JOIN Country as Nationality on Nationality.CountryID = VendorContact.CountryID    
  LEFT OUTER JOIN Vendor as Vendor on Vendor.VendorID = VendorContact.VendorID    
      
     
 WHERE VendorContact.CustomerID=@CustomerID AND     
   VendorContact.VendorType=@VendorType AND     
   VendorContact.VendorID=@VendorID AND    
   ISNULL(VendorContact.IsDeleted,0) = 0 
   order by VendorContactCD    
END
GO
