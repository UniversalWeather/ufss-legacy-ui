
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePaymentType]    Script Date: 08/24/2012 10:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePaymentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePaymentType]
go
CREATE PROCEDURE [dbo].[spUpdatePaymentType]
(
	@PaymentTypeID BIGINT,
	@PaymentTypeCD char(2),
	@CustomerID BIGINT,
	@PaymentTypeDescription varchar(30),
	@LastUpdUID varchar(30),
	@LastUpdTS datetime,
	@IsDeleted bit,
	@IsInActive bit
)

-- =============================================
-- Author:Hamsha.S
-- Create date: 04/05/2012
-- Description: Update the PaymentType information
-- =============================================
AS
BEGIN 
DECLARE @currentTime datetime
SET @currentTime = GETUTCDATE()
SET @LastUpdTS = @currentTime

UPDATE [PaymentType]
	SET 
		 PaymentTypeID =@PaymentTypeID
		,PaymenttypeCD=@PaymenttypeCD
		,CustomerID=@CustomerID
		,PaymenttypeDescription=@PaymenttypeDescription 
		,LastUpdUID=@LastUpdUID 
		,LastUpdTS=@LastUpdTS
		,IsDeleted=@IsDeleted 
		,IsInActive=@IsInActive
	WHERE 
		PaymentTypeID = @PaymentTypeID
END
GO
