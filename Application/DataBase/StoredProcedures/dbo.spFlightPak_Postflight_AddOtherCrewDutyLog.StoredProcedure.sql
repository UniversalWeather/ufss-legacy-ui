IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Postflight_AddOtherCrewDutyLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Postflight_AddOtherCrewDutyLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spFlightPak_Postflight_AddOtherCrewDutyLog]
(
	@SimulatorLog bigint,
	@CustomerID bigint,
	@CrewID bigint,
	@SessionDT date,
	@AircraftID bigint,
	@DutyTYPE varchar(20),
	@TakeOffDay numeric(2, 0),
	@TakeOffNight numeric(2, 0),
	@LandingDay numeric(2, 0),
	@LandingNight numeric(2, 0),
	@ApproachPrecision numeric(2, 0),
	@ApproachNonPrecision numeric(2, 0),
	@Instrument numeric(7, 3),
	@Night numeric(7, 3),
	@FlightHours numeric(7, 3),
	@DutyHours numeric(7, 3),
	@Specification1 bit,
	@Sepcification2 bit,
	@HomeBaseID bigint,
	@LastUserID varchar(30),
	@LastUptTS datetime,
	@TripID bigint,
	@Specification3 numeric(7, 3),
	@Specification4 numeric(7, 3),
	@DutyTypeID bigint,
	@AirportID bigint,
	@ClientID bigint,
	@SimulatorDescription varchar(25),
	@DepartureDTTMLocal datetime,
	@ArrivalDTTMLocal datetime,
	@DepartureTMGMT datetime,
	@ArrivalTMGMT datetime,
	@IsDeleted bit
)
AS
SET NOCOUNT ON;
BEGIN
	DECLARE @SimulatorID BIGINT

	EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'PostflightCurrentNo', @SimulatorID OUTPUT

	INSERT INTO [PostflightSimulatorLog] ([SimulatorID], [SimulatorLog], [CustomerID], [CrewID], [SessionDT], [AircraftID], [DutyTYPE], [TakeOffDay], [TakeOffNight], [LandingDay], [LandingNight], [ApproachPrecision], [ApproachNonPrecision], [Instrument], [Night], [FlightHours], [DutyHours], [Specification1], [Sepcification2], [HomeBaseID], [LastUserID], [LastUptTS], [TripID], [Specification3], [Specification4], [DutyTypeID], [AirportID], [ClientID], [SimulatorDescription], [DepartureDTTMLocal], [ArrivalDTTMLocal], [DepartureTMGMT], [ArrivalTMGMT], [IsDeleted])
	VALUES (@SimulatorID, @SimulatorID, @CustomerID, @CrewID, @SessionDT, @AircraftID, @DutyTYPE, @TakeOffDay, @TakeOffNight, @LandingDay, @LandingNight, @ApproachPrecision, @ApproachNonPrecision, @Instrument, @Night, @FlightHours, @DutyHours, @Specification1, @Sepcification2, @HomeBaseID, @LastUserID, @LastUptTS, @TripID, @Specification3, @Specification4, @DutyTypeID, @AirportID, @ClientID, @SimulatorDescription, @DepartureDTTMLocal, @ArrivalDTTMLocal, @DepartureTMGMT, @ArrivalTMGMT, @IsDeleted);
	
	SELECT isnull(@SimulatorID,0) as 'SimulatorID' 

END
GO


