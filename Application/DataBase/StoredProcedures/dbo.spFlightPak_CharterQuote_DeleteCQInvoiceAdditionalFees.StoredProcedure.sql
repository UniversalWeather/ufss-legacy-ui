



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceAdditionalFees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceAdditionalFees]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFlightPak_CharterQuote_DeleteCQInvoiceAdditionalFees]
(
	@CQInvoiceAdditionalFeesID bigint,
	@CustomerID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [CQInvoiceAdditionalFees] WHERE [CQInvoiceAdditionalFeesID] = @CQInvoiceAdditionalFeesID AND [CustomerID] = @CustomerID
GO


