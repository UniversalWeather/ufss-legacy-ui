/****** Object:  StoredProcedure [dbo].[spDeleteLeadSource]    Script Date: 02/12/2013 14:57:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDeleteLeadSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDeleteLeadSource]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteLeadSource]    Script Date: 02/12/2013 14:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO  
CREATE procedure [dbo].[spDeleteLeadSource]    
(    
 @CustomerID bigint    
,@LeadSourceID bigint    
,@LastUpdUID varchar(30)    
,@LastUpdTS datetime    
,@IsDeleted bit    
)    
-- =============================================    
-- Author:Suresh Jinka    
-- Create date: 05/02/2013   
-- Description: Delete the DelayType  information    
-- =============================================    
as    
begin     
--Check if the record is not being used anywhere in the application before Delete      
 DECLARE @RecordUsed INT    
DECLARE @ENTITYNAME VARCHAR(50)            
SET @ENTITYNAME = N'LeadSource'; -- Type Table Name    
EXECUTE dbo.FP_CHECKDELETE @ENTITYNAME, @LeadSourceID,    
@RecordUsed OUTPUT    
--End of Delete Check    
      
  if (@RecordUsed <> 0)    
 Begin    
 RAISERROR(N'-500010', 17, 1)    
 end    
 ELSE      
  BEGIN      
   --To store the lastupdateddate as UTC date     
      
   SET @LastUpdTS = GETUTCDATE()      
       
   --End of UTC date    
SET NoCOUNT ON    
    
UPDATE [LeadSource]    
   SET     [LastUpdUID]=@LastUpdUID    
           ,[LastUpdTS]=@LastUpdTS         
           ,[IsDeleted]=@IsDeleted               
 WHERE CustomerID =@CustomerID and LeadSourceID=@LeadSourceID    
    
end    
END    
  