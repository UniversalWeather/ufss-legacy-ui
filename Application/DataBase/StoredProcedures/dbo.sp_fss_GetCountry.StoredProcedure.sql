/****** Object:  StoredProcedure [dbo].[sp_fss_GetTransportByAirportID]    Script Date: 02/28/2014 13:52:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fss_GetCountry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fss_GetCountry]
GO


/****** Object:  StoredProcedure [dbo].[sp_fss_GetCountry]    Script Date: 02/28/2014 13:52:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fss_GetCountry](
		@CountryCD varchar(8),
		@CountryID BIGINT
		)          
		AS       
		BEGIN         
-- =============================================          
-- Author: Karthikeyan.S
-- Create date: 25/2/2014          
-- Description: Get Country Informations    
-- EXEC [sp_fss_GetCountry] '',0
-- EXEC [sp_fss_GetCountry] '',10000122838
-- EXEC [sp_fss_GetCountry] 'US',0
-- =============================================          
SET NOCOUNT ON 
SELECT      [CountryID]  
           ,[CountryCD]    
           ,[CountryName]    
           ,[UVCD]    
           ,[IATOCD]    
           ,[LastUpdUID]    
           ,[LastUpdDT]    
           ,[IsUVFlag]    
           ,[IsInActive]    
           ,[UPDTDATE]    
           ,[ISONum]    
           ,[ISOCD]    
           ,[IsDeleted]          
  FROM  [Country] C WHERE
  C.CountryID = case when @CountryID <>0 then @CountryID else C.CountryID end
  AND C.CountryCD = case when Ltrim(Rtrim(@CountryCD)) <> '' then @CountryCD else C.CountryCD end 
  AND ISNULL(C.IsDeleted,0) = 0   
  order by CountryCD  
END  
GO

