/*

exec dbo.spFlightPak_Preflight_GetPaxItineraryInfo 'yaminimira', 100031785634, 10003, ''

*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_Preflight_GetPaxItineraryInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_Preflight_GetPaxItineraryInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spFlightPak_Preflight_GetPaxItineraryInfo]
    @UserCD AS VARCHAR(30),
	@TripId AS BIGINT,
	@CustomerId AS BIGINT,
	@PaxID AS VARCHAR(MAX)=''
AS
BEGIN
-- =============================================
-- Name: spFlightPak_Preflight_GetPaxItineraryInfo
-- Author: Mohanraja C
-- Create date: 16 Dec 2013
-- Description: Get Preflight Passenger Itinerary  information for Email
-- Revision History
-- Date			Name		Ver		Change
-- 2014-01-10   Askar		-		Added Captain and Other Crew Fields
-- 2012-01-17	Askar		-		Updated GetHourMinutes function on Flight Time Field
-- 2012-01-17	Mohan		-		IsNull Checked on the fileds
-- 2012-01-20	Aishwarya	-		Make the Fleet table as LEFT Join, becoz of some trip doesn't have Tail Numbers
-- =============================================

SET NOCOUNT ON

SELECT DISTINCT    
  [TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),  
  [DepDate] =  PL.DepartureDTTMLocal,  
  [LegID] = PL.LegID,  
  [From] = ISNULL(AA.CityName,'') + ', ' + ISNULL(AA.StateName,'') + '*' + ISNULL(AA.AirportName,''),  
  [To] = ISNULL(A.CityName,'') + ', ' + ISNULL(A.StateName,'') + '*' +  ISNULL(A.AirportName,''),  
  [Depart] =PL.DepartureDTTMLocal,  
  [Arrive] = PL.ArrivalDTTMLocal,  
  [FltTime] = ISNULL(dbo.GetHourMinutes(ISNULL(PL.ElapseTM,0)),'') ,     
  [PassengerNm] = PX.Paxnm,  
  [TotPax] = PL.PassengerTotal,  
  [ArrDate] =  CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) = CONVERT(DATE,PL.DepartureDTTMLocal) THEN NULL ELSE PL.ArrivalDTTMLocal END,  
  [TCEnrouteis] =ISNULL(CASE WHEN ((CASE(A.IsDayLightSaving) WHEN 1 THEN A.OffsetToGMT END)) <> 0 THEN   
       (CASE(A.IsDayLightSaving) WHEN 1 THEN  A.OffsetToGMT END)-(CASE(AA.IsDayLightSaving) WHEN 1 THEN AA.OffsetToGMT END) END,0),  
  [TCHomeBaseis] = ISNULL(CASE WHEN ((CASE(A.IsDayLightSaving) WHEN 1 THEN  A.OffsetToGMT END))<> 0 THEN  
          (CASE(A.IsDayLightSaving) WHEN 1 THEN A.OffsetToGMT END)-(CASE(AA.IsDayLightSaving) WHEN 1 THEN  AA.OffsetToGMT END) END,0),  
  [TerminalA] = ISNULL(FBA.FBOVendor,''),  
  [TerminalAPh] = ISNULL(FBA.PhoneNUM1,''),         
  [TerminalD] = ISNULL(FBD.FBOVendor,''),  
  [TerminalDPh] = ISNULL(FBD.PhoneNUM1,''),   
  [Aircraft] = ISNULL(F.TailNum,'') ,  
  [Aircraft1] = ISNULL(AR.AircraftDescription,'') ,     
  [Passengers] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),  
  [FlightInformation] = ISNULL(PL.Notes,''),  
  [PassengerRequestorCD] = ISNULL(PX.PassengerRequestorCD,''),  
  [LEG_NUM] = ROW_NUMBER() OVER(PARTITION BY PX.Paxnm ORDER BY PL.LegID),
  Captain = ISNULL(PIC.Crewlist,''),
  OtherCrew = ISNULL(SUBSTRING(OC.Crewlist,1,LEN(OC.Crewlist)-1),'')
 FROM PREFLIGHTMAIN PM  
  INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0    
  LEFT JOIN FLEET F On PM.FleetID = F.FleetID  
  LEFT OUTER JOIN Aircraft AR ON F.AircraftID = AR.AircraftID          
  LEFT OUTER JOIN AIRPORT AA ON PL.DepartICAOID = AA.AirportID   
  LEFT OUTER JOIN AIRPORT A On PL.ArriveICAOID = A.AirportID  
  LEFT OUTER JOIN (  
  SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* FROM PreflightFBOList PFL  
   INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1  
   ) FBD ON PL.LegID = FBD.LegID  
  LEFT OUTER JOIN (SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* FROM PreflightFBOList PFL  
  INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1  
  ) FBA ON PL.LegID = FBA.LegID  
  LEFT OUTER JOIN (  
  SELECT DISTINCT PP2.LegID, Paxnames = (   
   SELECT  ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.LastName,'') + '; ' FROM PreflightPassengerList PP1   
                     INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID  
                       WHERE PP1.LegID = PP2.LegID  
                     AND (PassengerID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaxID, ',')) OR @PaxID='')   
                     ORDER BY OrderNUM  
                    FOR XML PATH('')  
            )  
  FROM  PreflightPassengerList PP2  
    ) PAX ON PL.LegID = PAX.LegID            
 INNER JOIN (                   
         SELECT DISTINCT PP3.LegID,PP3.PassengerID, ISNULL(P.FirstName,'') + ' ' + ISNULL(P.LastName,'')  As Paxnm,P.PassengerRequestorCD PassengerRequestorCD   
         FROM PreflightPassengerList PP3   
         INNER JOIN Passenger P ON PP3.PassengerID = P.PassengerRequestorID                                                  
    )PX ON PL.LegID = PX.LegID  
 
 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.PreflightCrewListID)Rnk,C.FirstName+' '+C.LastName Crewlist,LastName,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=@CustomerId )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1 
   
 LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID
						       , Crewlist = (SELECT ISNULL(C.FirstName,'') +' '+ISNULL(C.LastName,'') + ',' FROM PreflightCrewList PC
																	    INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
																   WHERE PC.LegID = PC2.LEGID 
																   AND PC.DutyTYPE NOT IN ('P')
											                       ORDER BY (CASE WHEN PC.DutyType='P' THEN 1 
																				  WHEN PC.DutyType='S' THEN 2
																				  WHEN PC.DutyType='E' THEN 3
																			      WHEN PC.DutyType='A' THEN 4 
																				  WHEN PC.DutyType='O' THEN 5 ELSE 6 END),PC.PreflightCrewListID DESC
															       FOR XML PATH('')
											)	
										FROM PreflightCrewList PC2		
				) OC  ON OC.LegID=PL.LegID 
 
   WHERE PM.CustomerID = @CustomerId  
    AND PM.TripId = @TripId  
    AND (PX.PassengerID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaxID, ',')) OR @PaxID='')  
    AND PM.IsDeleted = 0  
      ORDER BY PassengerNm , Depart  

END 
GO