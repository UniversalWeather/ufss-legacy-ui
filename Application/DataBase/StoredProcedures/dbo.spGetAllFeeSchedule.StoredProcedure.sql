IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllFeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllFeeSchedule]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetAllFeeSchedule](@CustomerID bigint)    
as    
-- =============================================    
-- Author: Mohan Raja
-- Create date: 26/02/2013    
-- Description: Gets All Fee Schedule by Customer
-- =============================================    
SET NOCOUNT ON    

IF LEN(@CustomerID) >0    
BEGIN     

 SELECT  FeeScheduleID
	, CustomerID
	, FeeGroupID
	, AccountID
	, isnull(Amount,0) as 'Amount'
	, isnull(IsTaxable,0) as 'IsTaxable'
	, isnull(IsDiscount,0) as 'IsDiscount'
	, isnull(IsInActive,0) as 'IsInActive'
	, LastUpdUID
	, LastUpdTS
	, isnull(IsDeleted,0) as 'IsDeleted'
 FROM  FeeSchedule     
	WHERE CustomerID=@CustomerID  and isnull(IsDeleted,0) = 0
 ORDER BY FeeGroupID ASC
    
END    
  
  
GO


