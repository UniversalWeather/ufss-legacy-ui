
GO
/****** Object:  StoredProcedure [dbo].[spGetAllTravelCoordinator]    Script Date: 08/24/2012 10:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllTravelCoordinator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllTravelCoordinator]
GO
CREATE Procedure [dbo].[spGetAllTravelCoordinator]  
 (@CustomerID BIGINT)      
as    begin  
-- =============================================      
-- Author:RUKMINI M      
-- Create date: 26/04/2012     
-- Modofied by Mathes on 03-07-12     
-- Description: Get the Travel information      
-- =============================================      
 set nocount on      
 if len(@CustomerID) >0      
 begin       
  SELECT TravelCoordinatorID,    
    CustomerID,      
    TravelCoordCD ,      
    FirstName ,      
    MiddleName ,      
    LastName,      
    PhoneNum,      
    FaxNum ,      
    PagerNum,      
    CellPhoneNum,      
    EmailID ,      
    Notes ,      
    HomebaseID,      
    LastUpdUID,      
    LastUpdTS ,      
    IsDeleted  
    ,BusinessPhone  
    ,HomeFax  
    ,CellPhoneNum2  
    ,OtherPhone  
    ,BusinessEmail  
    ,PersonalEmail  
    ,OtherEmail  
    ,Addr3
    ,IsInactive  
  FROM [TravelCoordinator]   
  WHERE CustomerID=@CustomerID and  
    IsDeleted = 'false'       
 END   
end
GO
