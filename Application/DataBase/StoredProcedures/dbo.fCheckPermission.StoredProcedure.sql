
/****** Object:  StoredProcedure [dbo].[spAddDelayType]    Script Date: 08/24/2012 10:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fCheckPermission]') AND type in (N'FN'))
DROP FUNCTION [dbo].[fCheckPermission]
go
CREATE FUNCTION [dbo].[fCheckPermission]
(   
    @SessionId uniqueIdentifier,
    @IsSystemAdmin bit,
    @PermissionName varchar(60)
)
RETURNS bit 
AS
BEGIN
RETURN 
(
	case 
	when 	@IsSystemAdmin = 1 then
    (SELECT TOP 1 1 AS CanView FROM FPLoginSession                                      
	JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID                                      
	JOIN CustomerModuleAccess ON CustomerModuleAccess.CustomerID = UserMaster.CustomerID AND CustomerModuleAccess.CanAccess = 1          
	LEFT JOIN UMPermission ON UMPermission.ModuleID = CustomerModuleAccess.ModuleID WHERE                                       
	FPLoginSession.SessionID = @SessionId 
	AND                                       
	UserMaster.IsDeleted = 0
	AND                                       
	UserMaster.IsActive = 1
	AND                                             
	UMPermission.IsDeleted = 0
	AND               
	UserMaster.IsUserLock = 0
	AND
	UMPermission.UMPermissionRole = @PermissionName)
	
	ELSE
	(SELECT TOP 1 UserGroupPermission.CanView FROM  FPLoginSession                                      
	JOIN UserMaster ON UserMaster.UserName = FPLoginSession.LoginSessionUID                                      
	LEFT JOIN UserGroupMapping ON UserGroupMapping.UserName = UserMaster.UserName                                      
	LEFT JOIN UserGroup ON UserGroup.UserGroupID = UserGroupMapping.UserGroupID                                      
	LEFT JOIN UserGroupPermission ON UserGroupPermission.UserGroupID = UserGroupMapping.UserGroupID                                      
	JOIN CustomerModuleAccess ON CustomerModuleAccess.CustomerID = UserMaster.CustomerID AND CustomerModuleAccess.CanAccess = 1             
	LEFT JOIN UMPermission ON  UMPermission.UMPermissionID = UserGroupPermission.UMPermissionID  AND   UMPermission.ModuleID = CustomerModuleAccess.ModuleID                                  
	WHERE                                       
	FPLoginSession.SessionID = @SessionId 
	AND                                       
	UserMaster.IsDeleted = 0
	AND                                       
	UserMaster.IsActive = 1
	AND                                
	UserGroupMapping.IsDeleted = 0
	AND                                
	UserGroup.IsDeleted = 0
	AND                                
	UserGroupPermission.IsDeleted = 0
	AND                                
	UMPermission.IsDeleted = 0
	AND               
	UserMaster.IsUserLock = 0
	AND
	UMPermission.UMPermissionRole = @PermissionName)
	END
)
END
GO
