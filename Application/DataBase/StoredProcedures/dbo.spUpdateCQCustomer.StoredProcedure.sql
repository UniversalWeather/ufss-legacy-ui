/****** Object:  StoredProcedure [dbo].[spUpdateCQCustomer]    Script Date: 03/27/2013 14:27:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCQCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCQCustomer]
GO

CREATE PROCEDURE [dbo].[spUpdateCQCustomer]
(@CQCustomerID BIGINT
,@CustomerID BIGINT
,@CQCustomerCD VARCHAR(5)
,@CQCustomerName VARCHAR(40)
,@IsApplicationFiled BIT
,@IsApproved BIT
,@BillingName VARCHAR(40)
,@BillingAddr1 VARCHAR(40)
,@BillingAddr2 VARCHAR(40)
,@BillingAddr3 VARCHAR(40)
,@BillingCity VARCHAR(40)
,@BillingState VARCHAR(10)
,@BillingZip VARCHAR(15)
,@CountryID BIGINT
,@BillingPhoneNum VARCHAR(25)
,@BillingFaxNum VARCHAR(25)
,@Notes VARCHAR(MAX)
,@Credit NUMERIC(17,2)
,@DiscountPercentage NUMERIC(5,2)
,@IsInActive BIT
,@HomebaseID BIGINT
,@AirportID BIGINT
,@DateAddedDT DATETIME
,@WebAddress VARCHAR(100)
,@EmailID VARCHAR(100)
,@TollFreePhone VARCHAR(40)
,@LastUpdUID VARCHAR(30)
,@LastUpdTS DATETIME
,@IsDeleted BIT
,@CustomerType varchar(1)
--,@IntlStdCrewNum numeric(2,0)
--,@DomesticStdCrewNum numeric(2,0)
--,@MinimumDayUseHrs numeric(17,2)
--,@DailyUsageAdjTax bit
--,@LandingFeeTax bit
 ,@MarginalPercentage numeric(7,2)
)
AS BEGIN

	SET NOCOUNT OFF;
	SET @LastUpdTS = GETUTCDATE()
		
	UPDATE CQCustomer
	SET	CustomerID = @CustomerID
		,CQCustomerCD = @CQCustomerCD
		,CQCustomerName = @CQCustomerName
		,IsApplicationFiled = @IsApplicationFiled
		,IsApproved = @IsApproved
		,BillingName = @BillingName
		,BillingAddr1 = @BillingAddr1
		,BillingAddr2 = @BillingAddr2
		,BillingAddr3 = @BillingAddr3
		,BillingCity = @BillingCity
		,BillingState = @BillingState
		,BillingZip = @BillingZip
		,CountryID = @CountryID
		,BillingPhoneNum = @BillingPhoneNum
		,BillingFaxNum = @BillingFaxNum
		,Notes = @Notes
		,Credit = @Credit
		,DiscountPercentage = @DiscountPercentage
		,IsInActive = @IsInActive
		,HomebaseID = @HomebaseID
		,AirportID = @AirportID
		,DateAddedDT = @DateAddedDT
		,WebAddress = @WebAddress
		,EmailID = @EmailID
		,TollFreePhone = @TollFreePhone
		,LastUpdUID = @LastUpdUID
		,LastUpdTS = @LastUpdTS
		,IsDeleted = @IsDeleted
		,CustomerType=@CustomerType
		--,IntlStdCrewNum=@IntlStdCrewNum
		--,DomesticStdCrewNum=@DomesticStdCrewNum
		--,MinimumDayUseHrs=@MinimumDayUseHrs
		--,DailyUsageAdjTax=@DailyUsageAdjTax
		--,LandingFeeTax =@LandingFeeTax
		,MarginalPercentage=@MarginalPercentage
		
		
	WHERE CQCustomerID = @CQCustomerID
			
END

GO


