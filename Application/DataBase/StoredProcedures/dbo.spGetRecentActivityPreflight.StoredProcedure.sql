/****** Object:  StoredProcedure [dbo].[spUpdateVendor]    Script Date: 01/16/2013 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRecentActivityPreflight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRecentActivityPreflight]
GO

/****** Object:  StoredProcedure [dbo].[spGetRecentActivityPreflight]    Script Date: 01/16/2013 15:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spGetRecentActivityPreflight]  
@UserName varchar(30),  
@MaxRecordCount int  
AS  
BEGIN  
 SELECT TOP (@MaxRecordCount) PreflightMain.*, P.PassengerRequestorCD AS PassengerRequestorCD,F.TailNum AS FleetTailNum FROM PreflightMain   
 LEFT JOIN Passenger P on PreflightMain.PassengerRequestorID = P.PassengerRequestorID     
 LEFT JOIN Fleet F on  PreflightMain.FleetID = F.FleetID  
 WHERE PreflightMain.LastUpdUID = @UserName AND PreflightMain.TripNUM <> 0 ORDER BY PreflightMain.LastUpdTS DESC  
END

GO
GO


