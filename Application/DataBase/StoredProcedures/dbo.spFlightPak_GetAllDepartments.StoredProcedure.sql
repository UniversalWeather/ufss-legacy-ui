
/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllDepartments]    Script Date: 05/07/2013 14:18:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlightPak_GetAllDepartments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlightPak_GetAllDepartments]
GO


/****** Object:  StoredProcedure [dbo].[spFlightPak_GetAllDepartments]    Script Date: 05/07/2013 14:18:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spFlightPak_GetAllDepartments]      
(      
 @CustomerID  BIGINT,    
 @ClientID BIGINT    
)      
AS      
BEGIN      
SET NOCOUNT ON;      
IF(@ClientID>0)    
BEGIN    
SELECT     
            [DepartmentID]    
           ,CASE WHEN [DepartmentCD] IS NULL THEN '' ELSE LTRIM(RTRIM([DepartmentCD])) END AS DepartmentCD    
           ,[DepartmentName]    
           ,[DepartPercentage]
           ,d.[CustomerID]    
           ,d.[ClientID]    
           ,d.[LastUpdUID]    
           ,d.[LastUpdTS]    
           ,d.[IsInActive]  
           ,d.[IsDeleted]     
           ,c.ClientCD    
 FROM [Department] d  left outer join [Client] c on d.ClientID = c.ClientID    
     
     
    
 WHERE d.CustomerID  = @CustomerID      
 AND ISNULL(d.IsDeleted,0) = 0  and d.ClientID = @ClientID    
 order by DepartmentCD  
 END    
 ELSE    
 BEGIN    
 SELECT     
            [DepartmentID]    
           ,CASE WHEN [DepartmentCD] IS NULL THEN '' ELSE LTRIM(RTRIM([DepartmentCD])) END AS DepartmentCD        
           ,[DepartmentName] 
           ,[DepartPercentage]   
           ,d.[CustomerID]    
           ,d.[ClientID]    
           ,d.[LastUpdUID]    
           ,d.[LastUpdTS]    
           ,d.[IsInActive]    
           ,d.[IsDeleted]     
           ,c.ClientCD    
 FROM [Department] d  left outer join [Client] c on d.ClientID = c.ClientID    
     
     
    
 WHERE d.CustomerID  = @CustomerID      
 AND ISNULL(d.IsDeleted,0) = 0   
  order by DepartmentCD   
 END    
 END  

GO


