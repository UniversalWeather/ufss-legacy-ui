IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[View_CharterQuoteMainList]'))
DROP VIEW [dbo].[View_CharterQuoteMainList]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_CharterQuoteMainList]
AS 

SELECT  
		0 as 'id', 
      CQFile.CQFileID,  
      CQFile.CustomerID,  
      CQFile.FileNUM,  
      CQFile.EstDepartureDT,
      QuoteDt,
      CQFile.CQCustomerID,
      CQFile.CQCustomerName,
      CQFile.CQFileDescription,      
      CQFile.IsFinancialWarning,
      Case When CQFile.Notes is not null and CQFile.Notes <>'' then '!' else '' end as Alert,
      A.ICAOID AS HomebaseCD ,
      '' as FileExcep,
      '' as LeadSourceCD,
	  '' as CustomerType,
	  '' as SalesPersonCD,
	  CQM.QuoteNUM ,
       PM.TripNUM ,
	   PM.TripStatus,
        CQM.QuoteStage 
      FROM CQFile
      INNER JOIN Company C on CQFile.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      LEFT JOIN CQMain CQM on CQM.CQFileID =   CQFile.CQFileID
      LEFT JOIN PreflightMain PM on PM.TripID =   CQM.TripID      
      where CQFile.IsDeleted  = 0 and  1=2
	  

GO


