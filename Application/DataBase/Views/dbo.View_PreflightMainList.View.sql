

/****** Object:  View [dbo].[View_PreflightMainList]    Script Date: 09/19/2013 14:24:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[View_PreflightMainList]'))
DROP VIEW [dbo].[View_PreflightMainList]
GO



/****** Object:  View [dbo].[View_PreflightMainList]    Script Date: 09/19/2013 14:24:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_PreflightMainList]
AS 

SELECT   
      PM.TripID,  
      PM.TripNUM,
      PM.DispatchNUM,   
      PM.EstDepartureDT,  
      P.PassengerRequestorCD as 'RequestorFirstName',  
      PM.TripDescription,  
      PM.TripStatus,  
      PM.RequestDT,  
      F.TailNum,  
      POM.LogNum,  
      PM.Acknowledge,  
      PM.WaitList,  
      A.IcaoID as HomebaseCD ,
	  PM.Notes as Alert,
      PM.APISSubmit,
      PM.APISStatus,
      PM.APISException,         
	  '' as CR,
	  UM.TripReqLastSubmitDT,
	  '' as DutyType,
	  '' as TripExcep,
	  '' as ForeGrndCustomColor,
	  '' as BackgroundCustomColor
      FROM PreflightMain PM  
      INNER JOIN Company C on PM.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID  
      LEFT JOIN PostflightMain POM on PM.TripID = POM.TripID  
      LEFT JOIN Fleet F on  PM.FleetID = F.FleetID  
      LEFT JOIN Passenger P on PM.PassengerRequestorID = P.PassengerRequestorID 
      LEFT JOIN  UWATSSMain UM on PM.TripID = UM.TripID 
      WHERE 1=2



GO


