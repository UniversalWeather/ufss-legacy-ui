

/****** Object:  View [dbo].[View_CharterQuoteMainList]    Script Date: 03/28/2013 13:53:43 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[View_CharterQuoteMainList]'))
DROP VIEW [dbo].[View_CharterQuoteMainList]
GO



/****** Object:  View [dbo].[View_CharterQuoteMainList]    Script Date: 03/28/2013 13:53:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_CharterQuoteMainList]
AS 

SELECT   
      CQFileID,  
      CQFile.CustomerID,  
      FileNUM,  
      EstDepartureDT,
      CQFile.CQCustomerID,
      CQFile.CQCustomerName,
      CQFile.CQFileDescription,      
      IsFinancialWarning,
      Case When CQFile.Notes is not null and CQFile.Notes <>'' then '!' else '' end as Alert,
      A.ICAOID AS HomebaseCD ,
      '' as FileExcep,
      '' as LeadSourceCD,
	  '' as CustomerType,
	  '' as SalesPersonCD
      FROM CQFile
      INNER JOIN Company C on CQFile.HomebaseID = C.HomebaseID  
      INNER JOIN Airport A on A.AirportID = C.HomebaseAirportID        
      where CQFile.IsDeleted  = 0 and  1=2





GO


