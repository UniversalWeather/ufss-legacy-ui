------------------------------------------- UE-148
-- used in SP spFlightPak_DeleteUWASpServ FK index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UWASpServ_LegID_IDX' AND object_id = OBJECT_ID('UWASpServ'))
    BEGIN
        DROP INDEX [UWASpServ_LegID_IDX] ON [dbo].[UWASpServ]
    END
GO

CREATE NONCLUSTERED INDEX [UWASpServ_LegID_IDX] ON [dbo].[UWASpServ]
(
    [LegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-147
-- used in SP spFlightPak_Preflight_GetUserMasterByUsername FK index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserMaster_GetUserMasterByUsername_IDX' AND object_id = OBJECT_ID('UserMaster'))
    BEGIN
        DROP INDEX [UserMaster_GetUserMasterByUsername_IDX] ON [dbo].[UserMaster]
    END
GO

CREATE NONCLUSTERED INDEX [UserMaster_GetUserMasterByUsername_IDX] ON [dbo].[UserMaster]
(
    [CustomerID] ASC,
    [Username] ASC,
    [IsDeleted] ASC,
    [IsDispatcher] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-145
-- used in SP sp_fss_GetAllEmergencyContact  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'EmergencyContactIsDeletedIDX' AND object_id = OBJECT_ID('EmergencyContact'))
    BEGIN
        DROP INDEX [EmergencyContactIsDeletedIDX] ON [dbo].[EmergencyContact]
    END
GO

------------------------------------------- UE-145
-- used in SP sp_fss_GetAllEmergencyContact  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'EmergencyContactIsInActiveIDX' AND object_id = OBJECT_ID('EmergencyContact'))
    BEGIN
        DROP INDEX [EmergencyContactIsInActiveIDX] ON [dbo].[EmergencyContact]
    END
GO

------------------------------------------- UE-144
-- used in SP sp_fss_GetDepartment  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'DepartmentIsInActiveIDX' AND object_id = OBJECT_ID('Department'))
    BEGIN
        DROP INDEX [DepartmentIsInActiveIDX] ON [dbo].[Department]
    END
GO

------------------------------------------- UE-144
-- used in SP sp_fss_GetDepartment  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'DepartmentIsDeletedIDX' AND object_id = OBJECT_ID('Department'))
    BEGIN
        DROP INDEX [DepartmentIsDeletedIDX] ON [dbo].[Department]
    END
GO

------------------------------------------- UE-143
-- used in SP spFlightPak_Preflight_GetCrewTypeRating cover index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewRating_GetCrewTypeRating_IDX' AND object_id = OBJECT_ID('CrewRating'))
    BEGIN
        DROP INDEX [CrewRating_GetCrewTypeRating_IDX] ON [dbo].[CrewRating]
    END
GO

CREATE NONCLUSTERED INDEX [CrewRating_GetCrewTypeRating_IDX] ON [dbo].[CrewRating]
(
    [CrewID] ASC,
    [CustomerID] ASC,
    [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-139
-- used in SP spGetAccounts  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'accountAccountIDIDX' AND object_id = OBJECT_ID('Account'))
    BEGIN
        DROP INDEX [accountAccountIDIDX] ON [dbo].[Account]
    END
GO

------------------------------------------- UE-139
-- used in SP spGetAccounts  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'accountIsDeletedIDX' AND object_id = OBJECT_ID('Account'))
    BEGIN
        DROP INDEX [accountIsDeletedIDX] ON [dbo].[Account]
    END
GO

------------------------------------------- UE-139
-- used in SP spGetAccounts  droping unefecient bit index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'accountIsInActiveIDX' AND object_id = OBJECT_ID('Account'))
    BEGIN
        DROP INDEX [accountIsInActiveIDX] ON [dbo].[Account]
    END
GO

------------------------------------------- UE-125
-- used in SP spGetAllFileWarehouse cover index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FileWarehouse_AllFileWarehouse_IDX' AND object_id = OBJECT_ID('FileWarehouse'))
    BEGIN
        DROP INDEX [FileWarehouse_AllFileWarehouse_IDX] ON [dbo].[FileWarehouse]
    END
GO

CREATE NONCLUSTERED INDEX [FileWarehouse_AllFileWarehouse_IDX] ON [dbo].[FileWarehouse]
(
    [CustomerID] ASC,
    [RecordID] ASC,
    [RecordType] ASC,
    [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-129
-- used in SP spCheckLock cover index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FPLockLog_CheckLock_IDX' AND object_id = OBJECT_ID('FPLockLog'))
    BEGIN
        DROP INDEX [FPLockLog_CheckLock_IDX] ON [dbo].[FPLockLog]
    END
GO

CREATE NONCLUSTERED INDEX [FPLockLog_CheckLock_IDX] ON [dbo].[FPLockLog]
(
    [SessionID] ASC,
    [EntityPrimaryKey] ASC,
    [EntityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-122
-- used in SP spGetAllFlightPurpose cover index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FlightPurpose_AllFlightPurpose_IDX' AND object_id = OBJECT_ID('FlightPurpose'))
    BEGIN
        DROP INDEX [FlightPurpose_AllFlightPurpose_IDX] ON [dbo].[FlightPurpose]
    END
GO

CREATE NONCLUSTERED INDEX [FlightPurpose_AllFlightPurpose_IDX] ON [dbo].[FlightPurpose]
(
    [CustomerID] ASC,
    [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-114
-- used in SP spFlightPak_GetPaxCheckListDate cover index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerCheckListDetail_GetPaxCheckList_IDX' AND object_id = OBJECT_ID('PassengerCheckListDetail'))
    BEGIN
        DROP INDEX [PassengerCheckListDetail_GetPaxCheckList_IDX] ON [dbo].[PassengerCheckListDetail]
    END
GO

CREATE NONCLUSTERED INDEX [PassengerCheckListDetail_GetPaxCheckList_IDX] ON [dbo].[PassengerCheckListDetail]
(
    [CustomerID] ASC,
    [PassengerID] ASC,
    [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


