--Defect# 6271 - Change datatype of column Title in Passenger table to Varchar(30) and PaxTitle varchar(30) to Varchar(MAX)
ALTER TABLE Passenger
ALTER COLUMN Title VARCHAR(30)
GO

ALTER TABLE Passenger
ALTER COLUMN PaxTitle VARCHAR(MAX)
GO