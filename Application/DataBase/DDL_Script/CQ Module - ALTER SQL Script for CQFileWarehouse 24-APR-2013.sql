
--// New Column FileWarehouseID2 Added for CQ Quote Report, based on company profile configuration

ALTER TABLE [dbo].[CQQuoteMain]  ADD FileWarehouseID2 BIGINT NULL
GO

ALTER TABLE [dbo].[CQQuoteMain]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteMainFileWarehouse2] FOREIGN KEY([FileWarehouseID2])
REFERENCES [dbo].[FileWarehouse] ([FileWarehouseID])
GO