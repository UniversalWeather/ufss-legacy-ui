------------------------------------------- UE-211 ticket numer
-- used in SP spFlightPak_Postflight_GetSIFLRate  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FareLevel_CustomerID_IDX' AND object_id = OBJECT_ID('FareLevel'))
    BEGIN
        DROP INDEX [FareLevel_CustomerID_IDX] ON [dbo].[FareLevel]
    END
GO

CREATE NONCLUSTERED INDEX [FareLevel_CustomerID_IDX] ON [dbo].[FareLevel]
(
    [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-211 ticket numer
-- used in SP spFlightPak_Postflight_GetSIFLRate  - [EndDT]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FareLevel_EndDT_IDX' AND object_id = OBJECT_ID('FareLevel'))
    BEGIN
        DROP INDEX [FareLevel_EndDT_IDX] ON [dbo].[FareLevel]
    END
GO

CREATE NONCLUSTERED INDEX [FareLevel_EndDT_IDX] ON [dbo].[FareLevel]
(
    [EndDT] ASC
) INCLUDE ( FareLevelID )
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



------------------------------------------- UE-211 ticket numer
-- used in SP spFlightPak_Postflight_GetSIFLRate  - 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FareLevel_GetSIFLRate_IDX' AND object_id = OBJECT_ID('FareLevel'))
    BEGIN
        DROP INDEX [FareLevel_GetSIFLRate_IDX] ON [dbo].[FareLevel]
    END
GO

CREATE NONCLUSTERED INDEX [FareLevel_GetSIFLRate_IDX] ON [dbo].[FareLevel]
(
    [CustomerID] ASC,
    [StartDT] ASC,
    [EndDT] ASC,
    [IsDeleted] ASC
) 
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

