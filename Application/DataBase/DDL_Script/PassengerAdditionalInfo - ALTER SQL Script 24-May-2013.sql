

ALTER TABLE passengeradditionalinfo ADD PassengerInformationID BIGINT,IsPrint bit
GO

ALTER TABLE passengeradditionalinfo ADD CONSTRAINT FKPassengerAdditionalInfoPassengerInformation FOREIGN KEY(PassengerInformationID) REFERENCES PassengerInformation(PassengerInformationID)
GO