
--// Altered Scripts for Change Request as on 15-03-2013

ALTER TABLE FleetNewCharterRate ADD CQCustomerID BIGINT NULL
GO

ALTER TABLE Passenger ADD CQCustomerID BIGINT NULL
GO

ALTER TABLE Fleet ADD MarginalPercentage NUMERIC(7,2) NULL
GO

ALTER TABLE CqFile ADD UseCustomFleetCharge BIT NULL
GO
