

ALTER TABLE PreflightFBOList
 ALTER COLUMN ConfirmationStatus VARCHAR(MAX) NULL
 GO

 ALTER TABLE PreflightCateringDetail
 ALTER COLUMN CateringConfirmation VARCHAR(MAX) NULL
 GO
 
 ALTER TABLE PreflightCrewHotelList
 ALTER COLUMN ConfirmationStatus VARCHAR(MAX) NULL
 GO
 
 ALTER TABLE PreflightPassengerHotelList
 ALTER COLUMN ConfirmationStatus VARCHAR(MAX) NULL
 GO
 
 ALTER TABLE PreflightTransportList
 ALTER COLUMN ConfirmationStatus VARCHAR(MAX) NULL
 GO