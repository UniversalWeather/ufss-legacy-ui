
------------------------------------------- UE-200 ticket numer
-- used in SP sp_fss_GetAllVendor  - VendorID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'VendorContact_VendorID_IDX' AND object_id = OBJECT_ID('VendorContact'))
    BEGIN
        DROP INDEX [VendorContact_VendorID_IDX] ON [dbo].[VendorContact]
    END
GO

CREATE NONCLUSTERED INDEX [VendorContact_VendorID_IDX] ON [dbo].[VendorContact]
(
    [VendorID] ASC,
    [IsChoice] ASC
)INCLUDE ([LastName],[FirstName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-200 ticket numer
-- used in SP sp_fss_GetAllVendor  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Vendor_CustomerID_IDX' AND object_id = OBJECT_ID('Vendor'))
    BEGIN
        DROP INDEX [Vendor_CustomerID_IDX] ON [dbo].[Vendor]
    END
GO

CREATE NONCLUSTERED INDEX [Vendor_CustomerID_VendorID_IDX] ON [dbo].[Vendor]
(
    [CustomerID] ASC,
    [VendorID]   ASC,
    [VendorCD]   ASC,
    [VendorType] ASC
) INCLUDE (IsInActive, IsDeleted)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO