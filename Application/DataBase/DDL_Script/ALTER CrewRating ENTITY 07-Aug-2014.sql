--Defect# 3048, Adding 2 new columns in CrewRating table
ALTER TABLE Crewrating
  ADD IsPIC91 BIT NULL,
      IsSIC91 BIT NULL;
GO