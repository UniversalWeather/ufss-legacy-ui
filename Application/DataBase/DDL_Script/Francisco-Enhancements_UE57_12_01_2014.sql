------------------------------------------- UE-57 ticket number
-- used in SP AirportID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightFuel_AirportID_IDX' AND object_id = OBJECT_ID('PreflightFuel'))
BEGIN
DROP INDEX [PreflightFuel_AirportID_IDX] ON [dbo].[PreflightFuel]
END
GO
CREATE NONCLUSTERED INDEX [PreflightFuel_AirportID_IDX] ON [dbo].[PreflightFuel]
(
	[AirportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-57 ticket number
-- used in SP CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightFuel_CustomerID_IDX' AND object_id = OBJECT_ID('PreflightFuel'))
BEGIN
DROP INDEX [PreflightFuel_CustomerID_IDX] ON [dbo].[PreflightFuel]
END
GO
CREATE NONCLUSTERED INDEX [PreflightFuel_CustomerID_IDX] ON [dbo].[PreflightFuel]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-57 ticket number
-- used in SP UWAFuelInterfaceRequestID is covering Index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightFuel_UWAFuelInterfaceRequestID_IDX' AND object_id = OBJECT_ID('PreflightFuel'))
BEGIN
DROP INDEX [PreflightFuel_UWAFuelInterfaceRequestID_IDX] ON [dbo].[PreflightFuel]
END
GO
CREATE NONCLUSTERED INDEX [PreflightFuel_UWAFuelInterfaceRequestID_IDX] ON [dbo].[PreflightFuel]
(
	[UWAFuelInterfaceRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-57 ticket number
-- used in DepartureICAOID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FlightpakFuel_DepartureICAOID_IDX' AND object_id = OBJECT_ID('FlightpakFuel'))
BEGIN
DROP INDEX [FlightpakFuel_DepartureICAOID_IDX] ON [dbo].[FlightpakFuel]
END
GO
CREATE NONCLUSTERED INDEX [FlightpakFuel_DepartureICAOID_IDX] ON [dbo].[FlightpakFuel]
(
	[DepartureICAOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-57 ticket numer
-- used in SP spFlightPak_Preflight_GetFlightPakFuelData  - TripID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightFuel_TripID_IDX' AND object_id = OBJECT_ID('PreflightFuel'))
BEGIN
    DROP INDEX [PreflightFuel_TripID_IDX] ON [dbo].[PreflightFuel]
END
GO

CREATE NONCLUSTERED INDEX [PreflightFuel_TripID_IDX] ON [dbo].[PreflightFuel]
(
    [TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO