------------------------------------------- UE-69 ticket number
-- used in SP POLogID is FK, CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostFlightLeg_POLogID_CustomerID_IDX' AND object_id = OBJECT_ID('PostFlightLeg'))
BEGIN
	DROP INDEX [PostFlightLeg_POLogID_CustomerID_IDX] ON [dbo].[PostFlightLeg]
END
GO
CREATE NONCLUSTERED INDEX [PostFlightLeg_POLogID_CustomerID_IDX] ON [dbo].[PostFlightLeg]
(
	[POLogID] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO