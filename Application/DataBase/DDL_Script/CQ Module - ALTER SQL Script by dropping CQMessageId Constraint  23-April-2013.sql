IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKCQInvoiceMainCQMessage]') AND parent_object_id = OBJECT_ID(N'[dbo].[CQInvoiceMain]'))
ALTER TABLE [dbo].[CQInvoiceMain] DROP CONSTRAINT [FKCQInvoiceMainCQMessage]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKCQQuoteMainCQMessage]') AND parent_object_id = OBJECT_ID(N'[dbo].[CQQuoteMain]'))
ALTER TABLE [dbo].[CQQuoteMain] DROP CONSTRAINT [FKCQQuoteMainCQMessage]
GO