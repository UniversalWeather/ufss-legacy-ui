
------------------------------------------- UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-UMPermission- ModuleID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UMPermission_ModuleID_IDX' AND object_id = OBJECT_ID('UMPermission'))
    BEGIN
        DROP INDEX [UMPermission_ModuleID_IDX] ON [dbo].[UMPermission]
    END
GO

CREATE NONCLUSTERED INDEX [UMPermission_ModuleID_IDX] ON [dbo].[UMPermission]
(
    [ModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--------------------------------------------------------------------------------------

------------------------------------------- UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-CustomerModuleAccess- ModuleID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CustomerModuleAccess_ModuleID_IDX' AND object_id = OBJECT_ID('CustomerModuleAccess'))
    BEGIN
        DROP INDEX [CustomerModuleAccess_ModuleID_IDX] ON [dbo].[CustomerModuleAccess]
    END
GO

CREATE NONCLUSTERED INDEX [CustomerModuleAccess_ModuleID_IDX] ON [dbo].[CustomerModuleAccess]
(
    [ModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------ UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-UserGroupPermission- UMPermissionID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupPermission_UMPermissionID_IDX' AND object_id = OBJECT_ID('UserGroupPermission'))
    BEGIN
        DROP INDEX [UserGroupPermission_UMPermissionID_IDX] ON [dbo].[UserGroupPermission]
    END
GO

CREATE NONCLUSTERED INDEX [UserGroupPermission_UMPermissionID_IDX] ON [dbo].[UserGroupPermission]
(
    [UMPermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-----------------------------------------------------------------------------------

------------------------------------------- UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-UMPermission- UMPermissionID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UMPermission_UMPermissionID_IDX' AND object_id = OBJECT_ID('UMPermission'))
    BEGIN
        DROP INDEX [UMPermission_UMPermissionID_IDX] ON [dbo].[UMPermission]
    END
GO

CREATE NONCLUSTERED INDEX [UMPermission_UMPermissionID_IDX] ON [dbo].[UMPermission]
(
    [UMPermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-------------------------------------------------------------------------------------
------------------------------------------- UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-CustomerModuleAccess- CustomerID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CustomerModuleAccess_CustomerID_IDX' AND object_id = OBJECT_ID('CustomerModuleAccess'))
    BEGIN
        DROP INDEX [CustomerModuleAccess_CustomerID_IDX] ON [dbo].[CustomerModuleAccess]
    END
GO

CREATE NONCLUSTERED INDEX [CustomerModuleAccess_CustomerID_IDX] ON [dbo].[CustomerModuleAccess]
(
    [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------
------------------------------------------- UE-25 ticket numer
-- used in SP spGetUserPreferenceUtilitiesList-UserGroupPermission- CustomerID FK
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupPermission_CustomerID_IDX' AND object_id = OBJECT_ID('UserGroupPermission'))
    BEGIN
        DROP INDEX [UserGroupPermission_CustomerID_IDX] ON [dbo].[UserGroupPermission]
    END
GO

CREATE NONCLUSTERED INDEX [UserGroupPermission_CustomerID_IDX] ON [dbo].[UserGroupPermission]
(
    [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO