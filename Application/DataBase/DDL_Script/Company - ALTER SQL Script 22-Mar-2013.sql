ALTER TABLE COMPANY
ADD InvoiceTitle varchar(60)
GO

ALTER TABLE COMPANY
ADD IsRptQuoteDetailArrivalDate bit
GO

ALTER TABLE COMPANY
ADD IsInvoiceRptArrivalDate bit
GO

ALTER TABLE COMPANY
ADD IsRptQuoteDetailQuoteDate bit
GO

ALTER TABLE COMPANY
ADD IsInvoiceRptQuoteDate bit
GO

