

/****** Object:  Table [dbo].[PreflightHotelList]    Script Date: 05/05/2014 14:47:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreflightHotelList](
	[PreflightHotelListID] [bigint] NOT NULL,
	[PreflightHotelName] [varchar](60) NULL,
	--[PreflightCrewListID] [bigint] NULL,
	[LegID] bigint,
	[HotelID] [bigint] NULL,
	[AirportID] [bigint] NULL,
	[RoomType] [varchar](60) NULL,
	[RoomRent] [numeric](6, 2) NULL,
	[RoomConfirm] [varchar](max) NULL,
	[RoomDescription] [varchar](max) NULL,
	[Street] [varchar](50) NULL,
	[CityName] [varchar](20) NULL,
	[StateName] [varchar](20) NULL,
	[PostalZipCD] [varchar](20) NULL,
	[PhoneNum1] [varchar](25) NULL,
	[PhoneNum2] [varchar](25) NULL,
	[PhoneNum3] [varchar](25) NULL,
	[PhoneNum4] [varchar](25) NULL,
	[FaxNUM] [varchar](25) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[HotelArrangedBy] [varchar](10) NULL,
	[Rate] [numeric](17, 2) NULL,
	[DateIn] [date] NULL,
	[DateOut] [date] NULL,
	[IsSmoking] [bit] NULL,
	[RoomTypeID] [bigint] NULL,
	[NoofBeds] [numeric](3, 0) NULL,
	[IsAllCrewOrPax] [bit] NULL,
	[IsCompleted] [bit] NULL,
	[IsEarlyCheckIn] [bit] NULL,
	[EarlyCheckInOption] [char](1) NULL,
	[SpecialInstructions] [varchar](100) NULL,
	[ConfirmationStatus] [varchar](max) NULL,
	[Comments] [varchar](max) NULL,
	[CustomerID] [bigint] NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[MetroID] [bigint] NULL,
	[City] [varchar](30) NULL,
	[StateProvince] [varchar](25) NULL,
	[CountryID] [bigint] NULL,
	[PostalCode] [varchar](15) NULL,
	[Status] [varchar](20) NULL,
	[isArrivalHotel] [bit],
	[isDepartureHotel] [bit],
	[crewPassengerType] [varchar](1) 
 CONSTRAINT [PKPreflightHotelList] PRIMARY KEY CLUSTERED 
(
	[PreflightHotelListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListAirport] FOREIGN KEY([AirportID])
REFERENCES [dbo].[Airport] ([AirportID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListAirport]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListCountry] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([CountryID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListCountry]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListCustomer]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListHotel] FOREIGN KEY([HotelID])
REFERENCES [dbo].[Hotel] ([HotelID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListHotel]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListMetro] FOREIGN KEY([MetroID])
REFERENCES [dbo].[Metro] ([MetroID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListMetro]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListPreflightLeg] FOREIGN KEY([LegID])
REFERENCES [dbo].[PreflightLeg] ([LegID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListPreflightLeg]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListRoomType] FOREIGN KEY([RoomTypeID])
REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListRoomType]
GO

ALTER TABLE [dbo].[PreflightHotelList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelListUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[PreflightHotelList] CHECK CONSTRAINT [FKPreflightHotelListUserMaster]
GO

ALTER TABLE [dbo].[PreflightHotelList] ADD  CONSTRAINT [PreflightHotelListIsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


