------------------------------------------- UE-14
-- used in SP spFlightPak_GetExceptionTemplate
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Customer_CustomerName_IDX' AND object_id = OBJECT_ID('Customer'))
    BEGIN
        DROP INDEX [Customer_CustomerName_IDX] ON [dbo].[Customer]
    END
GO

CREATE NONCLUSTERED INDEX [Customer_CustomerName_IDX] ON [dbo].[Customer]
(
 [CustomerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-13
-- used in SP spFlightPak_GetExceptionTemplate
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'ExeptionTemplate_ModuleID_IDX' AND object_id = OBJECT_ID('ExceptionTemplate'))
    BEGIN
        DROP INDEX [ExeptionTemplate_ModuleID_IDX] ON [dbo].[ExceptionTemplate]
    END
GO

CREATE NONCLUSTERED INDEX [ExeptionTemplate_ModuleID_IDX] ON [dbo].[ExceptionTemplate]
(
 [ModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-11 UE-12
-- used these SP spGetUserSettings spCheckLogin 
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FPLoginSession_SessionID_IDX' AND object_id = OBJECT_ID('FPLoginSession'))
    BEGIN
        DROP INDEX [FPLoginSession_SessionID_IDX] ON [dbo].[FPLoginSession]
    END
GO

CREATE NONCLUSTERED INDEX [FPLoginSession_SessionID_IDX] ON [dbo].[FPLoginSession]
(
 [SessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-------------------------------------------  UE-10
-- used by GetCustomerIDbyUserCD function	   
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserMaster_UserName_IDX' AND object_id = OBJECT_ID('UserMaster'))
    BEGIN
        DROP INDEX [UserMaster_UserName_IDX] ON [dbo].[UserMaster]
    END
GO

CREATE NONCLUSTERED INDEX [UserMaster_UserName_IDX] ON [dbo].[UserMaster]
(
 [UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--------------------------------------------- UE-9 
-- for spGetReportPRETSCriteria			   
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightMain_CostumerID_IDX' AND object_id = OBJECT_ID('PreflightMain'))
    BEGIN
        DROP INDEX [PreflightMain_CostumerID_IDX] ON [dbo].[PreflightMain]
    END
GO

CREATE NONCLUSTERED INDEX [PreflightMain_CostumerID_IDX] ON [dbo].[PreflightMain]
(
 [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO  

