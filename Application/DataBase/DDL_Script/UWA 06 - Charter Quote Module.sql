-------------------------------
-- UWA Flightpak Application --
-------------------------------

--------------------------------------
-- Module: Charter Quote Module     --
-- Created Date: 30-Sep-2012        --
-- Updated Date: 23-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE SalesPerson
(
SalesPersonID BIGINT,
CustomerID BIGINT,
SalesPersonCD CHAR(5),
FirstName VARCHAR(40),
LastName VARCHAR(40),
MiddleName VARCHAR(40),
PhoneNUM VARCHAR(25),
Address1 VARCHAR(60),
Address2 VARCHAR(60),
Address3 VARCHAR(60),
CityName VARCHAR(40),
StateName VARCHAR(15),
PostalZipCD VARCHAR(10),
CountryID BIGINT,
FaxNUM VARCHAR(25),
PersonalFaxNum VARCHAR(25),
CellPhoneNUM VARCHAR(25),
EmailAddress VARCHAR(200),
PersonalEmailID VARCHAR(100),
Logo VARCHAR(200),
LogoPosition INT,
Notes VARCHAR(MAX),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKSalesPerson PRIMARY KEY CLUSTERED(SalesPersonID ASC),
CONSTRAINT FKSalesPersonCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKSalesPersonUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKSalesPersonCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
)
GO

CREATE TABLE LeadSource
(
LeadSourceID BIGINT,
CustomerID BIGINT,
LeadSourceCD CHAR(4),
LeadSourceDescription VARCHAR(30),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKLeadSource PRIMARY KEY CLUSTERED(LeadSourceID ASC),
CONSTRAINT FKLeadSourceCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKLeadSourceUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FeeGroup
(
FeeGroupID BIGINT,
CustomerID BIGINT,
FeeGroupCD CHAR(4),
FeeGroupDescription VARCHAR(30),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKFeeGroup PRIMARY KEY CLUSTERED(FeeGroupID ASC),
CONSTRAINT FKFeeGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFeeGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FeeSchedule
(
FeeScheduleID BIGINT,
CustomerID BIGINT,
FeeGroupID BIGINT,
AccountID BIGINT,
Amount NUMERIC(17,2),
IsTaxable BIT,
IsDiscount BIT,
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKFeeSchedule PRIMARY KEY CLUSTERED(FeeScheduleID ASC),
CONSTRAINT FKFeeScheduleCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFeeScheduleFeeGroup FOREIGN KEY(FeeGroupID) REFERENCES FeeGroup(FeeGroupID),
CONSTRAINT FKFeeScheduleUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE AircraftCharterRate
(
AircraftCharterRateID BIGINT,
CustomerID BIGINT,
AircraftID BIGINT,
SellChgRateINTL NUMERIC(17,2),
SellChgRateDOM NUMERIC(17,2),
SellChgUnit NUMERIC(1),
BuyChgRateINTL NUMERIC(17,2),
BuyChgRateDOM NUMERIC(17,2),
SellPositioningRateINTL NUMERIC(17,2),
SellPositioningRateDOM NUMERIC(17,2),
SellPositioningUnit NUMERIC(1),
BuyPositionlingRateINTL NUMERIC(17,2),
BuyPositioningRateDOM NUMERIC(17,2),
StandardCrewINTL NUMERIC(2),
StandardCrewDOM NUMERIC(2),
SellStdCrewRONINTL NUMERIC(17,2),
SellStdCrewRONDOM NUMERIC(17,2),
BuyRONStdCrewINTL NUMERIC(17,2),
BuyRONStdCrewDOM NUMERIC(17,2),
SellAdditionalCrewINTL NUMERIC(17,2),
SellAdditionalCrewDOM NUMERIC(17,2),
BuyAdditionalCrewINTL NUMERIC(17,2),
BuyAdditionalCrewDOM NUMERIC(17,2),
SellAdditionalCrewRONINTL NUMERIC(17,2),
SellAdditoinalCrewRONDOM NUMERIC(17,2),
BuyRONAddCrewINTL NUMERIC(17,2),
BuyRONAddCrewDOM NUMERIC(17,2),
SellWaitTMINTL NUMERIC(17,2),
SellWaitingTMDOM NUMERIC(17,2),
BuyWaitTimeINTL NUMERIC(17,2),
BuyWaitTimeDOM NUMERIC(17,2),
SellLandingFeesINTL NUMERIC(17,2),
SellLandingFeesDOM NUMERIC(17,2),
BuyLandingFeeINTL NUMERIC(17,2),
BuyLandingFeeDOM NUMERIC(17,2),
MinimumDailyRequirement NUMERIC(17,2),
PercentageCost NUMERIC(5,2),
IsChargeTaxINTL BIT,
ChargeTaxDOM BIT,
IsPositioningTaxINTL BIT,
IsPostioningTaxDOM BIT,
IsRONTaxINTL BIT,
IsRONTaxDOM BIT,
IsAdditionalCrewTaxINTL BIT,
IsAdditionalCrewTaxDOM BIT,
IsAdditionalRONTaxINTL BIT,
IsAdditionalCrewRONTaxDOM BIT,
IsWaitTMTaxINTL BIT,
WaitTMTaxDOM BIT,
IsLandingTaxINTL BIT,
IsLandingTaxDOM BIT,
IsChgDescriptionINTL BIT,
ChargeDescriptionDOM BIT,
IsPositioningDescriptionINTL BIT,
IsPositoningDescriptionDOM BIT,
IsRONDescriptionINTL BIT,
IsRONDescriptionDOM BIT,
IsAdditionalCrewDescriptionINTL BIT,
IsAdditionalCrewDescriptionDOM BIT,
IsAdditionalRONDescriptionINTL BIT,
AdditionalRONDescriptionDOM BIT,
IsWaitTMDescriptionINTL BIT,
IsWaitTMDescriptionDOM BIT,
IsLandingDescriptionINTL BIT,
IsLandingDescriptionDOM BIT,
BuyAircraftFlightDOM VARCHAR(32),
SellAircraftFlightDOM VARCHAR(32),
BuyAircraftINTL VARCHAR(32),
SellAircraftFlightINTL VARCHAR(32),
BuyAircraftPositionDOM VARCHAR(32),
SellAircraftPositioningDOM VARCHAR(32),
BuyAircraftPositioningINTL VARCHAR(32),
SellAircraftPositioningINTL VARCHAR(32),
BuyAircraftAdditionalCrewDOM VARCHAR(32),
SellAircraftAdditionalCrewDOM VARCHAR(32),
BuyAircraftAdditionalCrewINTL VARCHAR(32),
SellAircraftAdditionalCrewINTL VARCHAR(32),
BuyAircraftCrewRONDOM VARCHAR(32),
SellAircraftAdditionalCrewRONDOM VARCHAR(32),
BuyAircraftAdditionalRONINTL VARCHAR(32),
SellAircraftAdditionalRONINTL VARCHAR(32),
BuyAircraftStdCrewDOM VARCHAR(32),
SellAircraftStdCrewDOM VARCHAR(32),
BuyAircraftAdditionalCrewRONINTL VARCHAR(32),
SellAircraftStdCrewINTL VARCHAR(32),
BuyAircraftWaitingDOM VARCHAR(32),
SellAircraftWaitTMDOM VARCHAR(32),
BuyAircraftWaitingTMINTL VARCHAR(32),
SellAircraftWaitingTMINTL VARCHAR(32),
BuyAircraftLandingDOM VARCHAR(32),
SellAircraftLandingDOM VARCHAR(32),
BuyAircraftLandingINTL VARCHAR(32),
SellAircraftLandingINTL VARCHAR(32),
BuyAircraftAdditionalFeeDOM VARCHAR(32),
SellAircraftAdditionalFeeDOM VARCHAR(32),
BuyAircraftAdditionalFeeINTL VARCHAR(32),
SellAircraftAdditionalFeeINTL VARCHAR(32),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKAircraftCharterRate PRIMARY KEY CLUSTERED(AircraftCharterRateID ASC),
CONSTRAINT FKAircraftCharterRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAircraftCharterRateAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKAircraftCharterRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQCustomer
(
CQCustomerID BIGINT,
CustomerID BIGINT,
CQCustomerCD VARCHAR(5),
CQCustomerName VARCHAR(40),
IsApplicationFiled BIT,
IsApproved BIT,
BillingName VARCHAR(40),
BillingAddr1 VARCHAR(40),
BillingAddr2 VARCHAR(40),
BillingAddr3 VARCHAR(40),
BillingCity VARCHAR(40),
BillingState VARCHAR(10),
BillingZip VARCHAR(15),
CountryID BIGINT,
BillingPhoneNum VARCHAR(25),
BillingFaxNum VARCHAR(25),
Notes VARCHAR(MAX),
Credit NUMERIC(17,2),
DiscountPercentage NUMERIC(5,2),
IsInActive BIT,
HomebaseID BIGINT,
AirportID BIGINT,
DateAddedDT DATETIME,
WebAddress VARCHAR(100),
EmailID VARCHAR(100),
TollFreePhone VARCHAR(40),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQCustomer PRIMARY KEY CLUSTERED(CQCustomerID ASC),
CONSTRAINT FKCQCustomerCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQCustomerCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKCQCustomerCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCQCustomerAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQCustomerUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQCustomerContact
(
CQCustomerContactID BIGINT,
CustomerID BIGINT,
CQCustomerID BIGINT,
CQCustomerContactCD INT,
IsChoice BIT,
FirstName VARCHAR(40),
Addr1 VARCHAR(40),
Addr2 VARCHAR(40),
Addr3 VARCHAR(40),
CityName VARCHAR(40),
StateName VARCHAR(10),
PostalZipCD VARCHAR(15),
CountryID BIGINT,
PhoneNum VARCHAR(25),
OtherPhoneNum VARCHAR(25),
PrimaryMobileNum VARCHAR(25),
SecondaryMobileNum VARCHAR(25),
FaxNum VARCHAR(25),
HomeFaxNum VARCHAR(25),
CreditName1 VARCHAR(25),
CreditNum1 VARCHAR(30),
CreditName2 VARCHAR(25),
CreditNum2 VARCHAR(30),	
MoreInfo VARCHAR(MAX),
Notes VARCHAR(MAX),
Title VARCHAR(40),
LastName VARCHAR(40),
MiddleName VARCHAR(40),
EmailID VARCHAR(100),
PersonalEmailID VARCHAR(100),
OtherEmailID VARCHAR(100),
ContactName VARCHAR(40),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQCustomerContact PRIMARY KEY CLUSTERED(CQCustomerContactID ASC),
CONSTRAINT FKCQCustomerContactCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQCustomerContactCQCustomer FOREIGN KEY(CQCustomerID) REFERENCES CQCustomer(CQCustomerID),
CONSTRAINT FKCQCustomerContactCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKCQCustomerContactUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQCustomerAdditionalInfo
(
CQCustomerAdditionalInfoID BIGINT,
CustomerID BIGINT,
CQCustomerID BIGINT,
CQCustomerAdditionalInformation VARCHAR(MAX),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQCustomerAdditionalInfo PRIMARY KEY CLUSTERED(CQCustomerAdditionalInfoID ASC),
CONSTRAINT FKCQCustomerAdditionalInfoCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQCustomerAdditionalInfoCQCustomer FOREIGN KEY(CQCustomerID) REFERENCES CQCustomer(CQCustomerID),
CONSTRAINT FKCQCustomerAdditionalInfoUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQCustomerCharterRate
(
CQCustomerCharterRateID BIGINT,
CustomerID BIGINT,
CQCustomerID BIGINT,
SellChgRateINTL NUMERIC(17,2),
SellChgRateDOM NUMERIC(17,2),
SellChgUnit NUMERIC(1),
BuyChargeRateINTL NUMERIC(17,2),
BuyChgRateDOM NUMERIC(17,2),
SellPosRateINTL NUMERIC(17,2),
SellPositioningRateDOM NUMERIC(17,2),
SellPositioningUnit NUMERIC(1),
BuyPositioningRateINTL NUMERIC(17,2),
BuyPositioningRateDOM NUMERIC(17,2),
StandardCrewINTL NUMERIC(2),
StandardCrewDOM	NUMERIC(2),
SellStdCrewRonINTL NUMERIC(17,2),
SellStdCrewRONDOM NUMERIC(17,2),
BuyRONStdCrewINTL NUMERIC(17,2),
BuyRONStdCrewDOM NUMERIC(17,2),
SellAdditionalCrewINTL NUMERIC(17,2),
SellAdditionalCrewDOM NUMERIC(17,2),
BuyAdditionalCrewINTL NUMERIC(17,2),
BuyAdditionalCrewDOM NUMERIC(17,2),
SellAdditionalCrewRONINTL NUMERIC(17,2),
SellAdditionalCrewRONDOM NUMERIC(17,2),
BuyRONAddCrewINTL NUMERIC(17,2),
BuyRONAddCrewDOM NUMERIC(17,2),
SellWaitingTMINTL NUMERIC(17,2),
SellWaitTMDOM NUMERIC(17,2),
BuyWaitTimeINTL NUMERIC(17,2),
BuyWaitTimeDOM NUMERIC(17,2),
SellLandingFeesINTL NUMERIC(17,2),
SellLandingFeeDOM NUMERIC(17,2),
BuyLandingFeeINTL NUMERIC(17,2),
BuyLandingFeeDOM NUMERIC(17,2),
MinimumDay NUMERIC(17,2),
PercentageCost NUMERIC(5,2),
IsChargeTaxINTL	BIT,
IsChargeTaxDOM BIT,
IsSellAircraftPositioningINTL BIT,
IsPostioningTaxDOM BIT,
IsRONTaxINTL BIT,
IsRONTaxDOM BIT,
IsAdditionalCrewTaxINTL BIT,
IsAdditionalCrewTaxDOM BIT,
IsAdditionalRONTaxINTL BIT,
IsAdditionalCrewRONTaxDOM BIT,
IsWaitTMTaxINTL BIT,
IsWaitTaxDOM BIT,
IsLandingTaxINTL BIT,
IsLandingTaxDOM BIT,
IsChgDescriptionINTL BIT,
IsChgDescriptionDOM BIT,
IsPositioningDescriptionINTL BIT,
IsPositoningDescriptionDOM BIT,
IsRONDescriptionINTL BIT,
IsRONDescriptionDOM BIT,
IsAdditionalCrewDescriptionINTL BIT,
IsAdditionalCrewDescriptionDOM BIT,
IsAdditionalRONDescriptionINTL BIT,
IsAdditionalRONDescriptionDOM BIT,
IsWaitTMDescriptionINTL BIT,
IsWaitDescriptionDOM BIT,
IsLandingDescriptionINTL BIT,
IsLandingDescriptionDOM BIT,
DBAircraftFlightChg VARCHAR(32),
DSAircraftFlightChg VARCHAR(32),
BuyAircraftFlightIntl VARCHAR(32),
IsSellAircraftFlightIntl VARCHAR(32),
BuyAircraftPositionDOM VARCHAR(32),
SellAircraftPositioningDOM VARCHAR(32),
BuyAircraftPositioningIntl VARCHAR(32),
SellAircraftPositioningIntl VARCHAR(32),
BuyAircraftAdditionalCrewDOM VARCHAR(32),
SellAircraftAdditionalCrewDOM VARCHAR(32),
InternationalBuyAircraftAdditionalCrew VARCHAR(32),
SellAircraftAdditionalCrewIntl VARCHAR(32),
BuyAircraftCrewRONDOM VARCHAR(32),
SellAircraftAdditionalCrewRONDOM VARCHAR(32),
BuyAircraftAdditionalCrewRONIntl VARCHAR(32),
SellAircraftAdditionalCrewRONIntl VARCHAR(32),
BuyAircraftStdCrewDOM VARCHAR(32),
SellAircraftStdCrewDOM VARCHAR(32),
BuyAircraftStdCrewIntl VARCHAR(32),
SellAircraftStdCrewIntl VARCHAR(32),
BuyAircraftWaitingDOM VARCHAR(32),
SellAircraftWaitTMDOM VARCHAR(32),
BuyAircraftWaitingIntl VARCHAR(32),
SellAircraftWaitingTMIntl VARCHAR(32),
BuyAircraftLandingDOM VARCHAR(32),
SellAircraftLandingDOM VARCHAR(32),
BuyAircraftLandingIntl VARCHAR(32),
SellAircraftLandingIntl VARCHAR(32),
BuyAircraftAdditionalFeeDOM VARCHAR(32),
SellAircraftAdditionalFeeDOM VARCHAR(32),
BuyAircraftAdditionalFeeIntl VARCHAR(32),
SellAircraftAdditionalFeeIntl VARCHAR(32),
IsInActive BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQCustomerCharterRate PRIMARY KEY CLUSTERED(CQCustomerCharterRateID ASC),
CONSTRAINT FKCQCustomerCharterRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQCustomerCharterRateCQCustomer FOREIGN KEY(CQCustomerID) REFERENCES CQCustomer(CQCustomerID),
CONSTRAINT FKCQCustomerCharterRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQMessage
(
CQMessageID BIGINT,
CustomerID BIGINT,
MessageTYPE CHAR(1),
HomebaseID BIGINT,
MessageCD NUMERIC(3),
MessageDescription VARCHAR(MAX),
MessageLine VARCHAR(50),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQMessage PRIMARY KEY CLUSTERED(CQMessageID ASC),
CONSTRAINT FKCQMessageCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQMessageCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCQMessageUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQFile
(
CQFileID BIGINT,
CustomerID BIGINT,
FileNUM INT,
EstDepartureDT DATE,
QuoteDT DATE,
HomebaseID BIGINT,
CQFileDescription VARCHAR(40),
CQCustomerID BIGINT,
CQCustomerName VARCHAR(40),
CQCustomerContactID BIGINT,
CQCustomerContactName VARCHAR(40),
PhoneNUM VARCHAR(25),
FaxNUM VARCHAR(25),
IsApplicationFiled BIT,
IsApproved BIT,
Credit NUMERIC(17,2),
DiscountPercentage NUMERIC(5,2),
SalesPersonID BIGINT,
LeadSourceID BIGINT,
Notes VARCHAR(MAX),
QuoteCenter INT,
Sync CHAR(1),
ExpressQuote BIT,
IsFinancialWarning BIT,
IsErrorMessage BIT,
CharterQuoteDTRONExcept CHAR(1),
ExchangeRateID BIGINT,
ExchangeRate NUMERIC(10,5),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQFile PRIMARY KEY CLUSTERED(CQFileID ASC),
CONSTRAINT FKCQFileCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQFileUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQFileCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCQFileCQCustomer FOREIGN KEY(CQCustomerID) REFERENCES CQCustomer(CQCustomerID),
CONSTRAINT FKCQFileCQCustomerContact FOREIGN KEY(CQCustomerContactID) REFERENCES CQCustomerContact(CQCustomerContactID),
CONSTRAINT FKCQFileSalesPerson FOREIGN KEY(SalesPersonID) REFERENCES SalesPerson(SalesPersonID),
CONSTRAINT FKCQFileLeadSource FOREIGN KEY(LeadSourceID) REFERENCES LeadSource(LeadSourceID),
CONSTRAINT FKCQFileExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
)
GO

CREATE TABLE CQLostBusiness
(
CQLostBusinessID BIGINT,
CustomerID BIGINT,
CQLostBusinessDescription VARCHAR(100),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQLostBusiness PRIMARY KEY CLUSTERED(CQLostBusinessID ASC),
CONSTRAINT FKCQLostBusinessCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQLostBusinessUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQMain
(
CQMainID BIGINT,
CustomerID BIGINT,
CQFileID BIGINT,
FileNUM INT,
QuoteNUM INT,
QuoteID INT,
CQSource NUMERIC(1),
VendorID BIGINT,
FleetID BIGINT,
AircraftID BIGINT,
IsCustomerCQ BIT,
FederalTax NUMERIC(5,2),
QuoteRuralTax NUMERIC(5,2),
IsTaxable BIT,
PassengerRequestorID BIGINT,
RequestorName VARCHAR(25),
RequestorPhoneNUM VARCHAR(25),
IsSubmitted BIT,
LastSubmitDT DATE,
IsAccepted BIT,
LastAcceptDT DATE,
RejectDT DATE,
Comments VARCHAR(200),
IsFinancialWarning BIT,
LiveTrip CHAR(1),
TripNUM INT,
MinimumDayUsageTotal NUMERIC(6,2),
BillingCrewRONTotal NUMERIC(3),
AdditionalCrewTotal NUMERIC(17,2),
AdditionalCrewRONTotal1 NUMERIC(2),
StdCrewRONTotal NUMERIC(17,2),
AdditionalCrewRONTotal2 NUMERIC(17,2),
WaitTMTotal NUMERIC(9,3),
WaitChgTotal NUMERIC(17,2),
QuoteInformation VARCHAR(MAX),
HomebaseID BIGINT,
ClientID BIGINT,
CQLostBusinessID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQMain PRIMARY KEY CLUSTERED(CQMainID ASC),
CONSTRAINT FKCQMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQMainCQFile FOREIGN KEY(CQFileID) REFERENCES CQFile(CQFileID),
CONSTRAINT FKCQMainVendor FOREIGN KEY(VendorID) REFERENCES Vendor(VendorID),
CONSTRAINT FKCQMainFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKCQMainAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKCQMainPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKCQMainCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCQMainUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQMainLostBusiness FOREIGN KEY(CQLostBusinessID) REFERENCES CQLostBusiness(CQLostBusinessID)
)
GO

CREATE TABLE CQFleetChargeDetail
(
CQFleetChargeDetailID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
FileNUM INT,
QuoteID INT,
AccountID BIGINT,
AircraftCharterRateID BIGINT,
FleetCharterRateID BIGINT,
CQCustomerCharterRateID BIGINT,
OrderNUM INT,
CQFlightChargeDescription VARCHAR(40),
ChargeUnit VARCHAR(25),
NegotiatedChgUnit NUMERIC(2),
Quantity NUMERIC(17,3),
BuyDOM NUMERIC(17,2),
SellDOM NUMERIC(17,2),
IsTaxDOM BIT,
IsDiscountDOM BIT,
BuyIntl NUMERIC(17,2),
SellIntl NUMERIC(17,2),
IsTaxIntl BIT,
IsDiscountIntl BIT,
StandardCrewDOM NUMERIC(2),
StandardCrewINTL NUMERIC(2),
MinimumDailyRequirement NUMERIC(17,2),
YearMade CHAR(4),
ExteriorColor VARCHAR(25),
ColorIntl VARCHAR(25),
IsAFIS BIT,
IsUWAData BIT,
AircraftFlightChg1 VARCHAR(32),
AircraftFlightChg2 VARCHAR(32),
BuyAircraftFlightIntl VARCHAR(32),
FeeAMT NUMERIC(17,2),
FeeGroupID BIGINT,
SellAircraftFlight VARCHAR(32),
IsToPrint BIT,
IsDailyTaxAdj BIT,
IsLandingFeeTax BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQFleetChargeDetail PRIMARY KEY CLUSTERED(CQFleetChargeDetailID ASC),
CONSTRAINT FKCQFleetChargeDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQFleetChargeDetailCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQFleetChargeDetailAircraftCharterRate FOREIGN KEY(AircraftCharterRateID) REFERENCES AircraftCharterRate(AircraftCharterRateID),
CONSTRAINT FKCQFleetChargeDetailFleetCharterRate FOREIGN KEY(FleetCharterRateID) REFERENCES FleetCharterRate(FleetCharterRateID),
CONSTRAINT FKCQFleetChargeDetailCQCustomerCharterRate FOREIGN KEY(CQCustomerCharterRateID) REFERENCES CQCustomerCharterRate(CQCustomerCharterRateID),
CONSTRAINT FKCQFleetChargeDetailFeeGroup FOREIGN KEY(FeeGroupID) REFERENCES FeeGroup(FeeGroupID),
CONSTRAINT FKCQFleetChargeDetailAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID),
CONSTRAINT FKCQFleetChargeDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

--CREATE TABLE CQFleetChargeTotal
--(
--CQFleetChargeTotalID BIGINT,
--CustomerID BIGINT,
--CQMainID BIGINT,
--StandardCrewDOM NUMERIC(2),
--StandardCrewINTL NUMERIC(2),
--DaysTotal NUMERIC(4),
--FlightHoursTotal NUMERIC(9,3),
--AverageFlightHoursTotal NUMERIC(7,2),
--AdjAmtTotal NUMERIC(17,3),
--UsageAdjTotal NUMERIC(17,2),
--IsOverrideUsageAdj BIT,
--SegementFeeTotal NUMERIC(17,2),
--IsOverrideSegmentFee BIT,
--LandingFeeTotal NUMERIC(17,2),
--IsOverrideLandingFee BIT,
--AdditionalFeeTotalD NUMERIC(17,2),
--FlightChargeTotal NUMERIC(17,2),
--SubtotalTotal NUMERIC(17,2),
--DiscountPercentageTotal NUMERIC(6,2),
--IsOverrideDiscountPercentage BIT,
--DiscountAmtTotal NUMERIC(17,2),
--IsOverrideDiscountAMT BIT,
--TaxesTotal NUMERIC(17,2),
--IsOverrideTaxes BIT,
--MarginalPercentTotal NUMERIC(7,2),
--MarginTotal NUMERIC(17,2),
--QuoteTotal NUMERIC(17,2),
--IsTotalQuote BIT,
--CostTotal NUMERIC(17,2),
--IsOverrideCost BIT,
--PrepayTotal NUMERIC(17,2),
--RemainingAmtTotal NUMERIC(17,2),
--LastUpdUID VARCHAR(30),
--LastUpdTS DATETIME,
--IsDeleted BIT DEFAULT (0) NOT NULL,
--CONSTRAINT PKCQFleetChargeTotal PRIMARY KEY CLUSTERED(CQFleetChargeTotalID ASC),
--CONSTRAINT FKCQFleetChargeTotalCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
--CONSTRAINT FKCQFleetChargeTotalCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
--CONSTRAINT FKCQFleetChargeTotalUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
--)
--GO

CREATE TABLE CQFleetCharterRate
(
CQFleetCharterRateID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
SellChgRateINTL NUMERIC(17,2),
SellChgRateDOM NUMERIC(17,2),
SellChgUnit NUMERIC(1),
BuyChargeRateINTL NUMERIC(17,2),
BuyChargeRateDOM NUMERIC(17,2),
SellPositionRateIntl NUMERIC(17,2),
SellPositioningRateDOM NUMERIC(17,2),
SellPositioningUnit NUMERIC(1),
BuyPositioningRateINTL NUMERIC(17,2),
BuyPositioningRateDOM NUMERIC(17,2),
SellStdCrewRONINTL NUMERIC(17,2),
SellStdCrewRONDOM NUMERIC(17,2),
BuyRONStdCrewINTL NUMERIC(17,2),
BuyRONStdCrewDOM NUMERIC(17,2),
SellAdditionalCrewINTL NUMERIC(17,2),
SellAdditionalCrewDOM NUMERIC(17,2),
BuyAdditionalCrewINTL NUMERIC(17,2),
BuyAdditionalCrewDOM NUMERIC(17,2),
SellAdditionalCrewRONINTL NUMERIC(17,2),
SellAdditionalCrewRONDOM NUMERIC(17,2),
BuyRONAddCrewINTL NUMERIC(17,2),
BuyRONAddCrewDOM NUMERIC(17,2),
SellWaitingTMINTL NUMERIC(17,2),
SellWaitingTMDOM NUMERIC(17,2),
BuyWaitTimeINTL NUMERIC(17,2),
BuyWaitTimeDOM NUMERIC(17,2),
SellLandingFeesINTL NUMERIC(17,2),
SellLandingFeeDOM NUMERIC(17,2),
BuyLandingFeeINTL NUMERIC(17,2),
BuyLandingFeeDOM NUMERIC(17,2),
MinimumDailyRequirement NUMERIC(17,2),
IsChargeTaxINTL BIT,
ChargeTaxDOM BIT,
IsPositionlingTaxINTL BIT,
IsPostioningTaxDOM BIT,
IsRONTaxINTL BIT,
IsRONTaxDOM BIT,
IsAdditionalCrewTaxINTL BIT,
IsAdditionalCrewTaxDOM BIT,
IsAdditionalRONTaxINTL BIT,
IsAdditionalCrewRONTaxDOM BIT,
IsWaitTMTaxINTL BIT,
IsWaitTaxDOM BIT,
IsLandingTaxINTL BIT,
IsLandingTaxDOM BIT,
IsChgDescriptionINTL BIT,
IsChgDescriptionDOM BIT,
IsPositioningDescriptionINTL BIT,
IsPositoningDescriptionDOM BIT,
IsRONDescriptionINTL BIT,
IsRONDescriptionDOM BIT,
IsAdditionalCrewDescriptionINTL BIT,
IsAdditionalCrewDescriptionDOM BIT,
IsAdditionalRONDescriptionINTL BIT,
IsAdditionRONDOM BIT,
IsWaitTMDescriptionINTL BIT,
IsWaitDescriptionDOM BIT,
IsLandingDescriptionINTL BIT,
IsLandingDescriptionDOM BIT,
ElapseTM NUMERIC(7,3),
IsOverrideBillingRON BIT,
IsOverrideStdCrewRON BIT,
IsOverrideStdCrew BIT,
IsOverrideAdditionalCrew BIT,
IsOverrideAdditionalCrewRON BIT,
TaxRate NUMERIC(5,2),
IsQuoteReport BIT,
IsConverted BIT,
QuoteDescription VARCHAR(30),
IsErrMessage BIT,
IsAutoRONCalc BIT,
IsCompleted BIT,
CurrencyCD VARCHAR(6),
ExchangeRate NUMERIC(10,5),
CurrencyID NUMERIC(3),
IsDailyAdjTax BIT,
IsLandingFeeTax BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQFleetCharterRate PRIMARY KEY CLUSTERED(CQFleetCharterRateID ASC),
CONSTRAINT FKCQFleetCharterRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQFleetCharterRateCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQFleetCharterRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQLeg
(
CQLegID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
QuoteID INT,
LegNUM INT,
LegID INT,
DAirportID BIGINT,
AAirportID BIGINT,
Distance NUMERIC(5),
PowerSetting CHAR(1),
TakeoffBIAS NUMERIC(6,3),
LandingBIAS NUMERIC(6,3),
TrueAirSpeed NUMERIC(3),
WindsBoeingTable NUMERIC(5),
ElapseTM NUMERIC(7,3),
IsDepartureConfirmed BIT,
IsArrivalConfirmation BIT,
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
DepartureGreenwichDTTM DATETIME,
ArrivalGreenwichDTTM DATETIME,
HomeDepartureDTTM DATETIME,
HomeArrivalDTTM DATETIME,
FBOID BIGINT,
FlightPurposeDescription VARCHAR(100),
DutyHours NUMERIC(5,1),
RestHours NUMERIC(5,1),
FlightHours NUMERIC(5,1),
Notes VARCHAR(MAX),
DutyTYPE NUMERIC(1),
CrewDutyRulesID BIGINT,
IsDutyEnd BIT,
FedAviationRegNUM CHAR(3),
WindReliability INT,
CQOverRide NUMERIC(4,1),
NextLocalDTTM DATETIME,
NextGMTDTTM DATETIME,
NextHomeDTTM DATETIME,
ChargeRate NUMERIC(17,2),
ChargeTotal NUMERIC(17,2),
IsPositioning BIT,
IsTaxable BIT,
FeeGroupDescription VARCHAR(100),
LegMargin NUMERIC(17,2),
AirportAlertCD CHAR(3),
RemainOverNightCNT INT,
TaxRate NUMERIC(5,2),
PassengerTotal NUMERIC(4),
IsReservationAvailable NUMERIC(4),
IsPrintable BIT,
FromDescription VARCHAR(120),
ToDescription VARCHAR(120),
IsRural BIT,
CrewNotes VARCHAR(MAX),
ClientID BIGINT,
DayROBCNT NUMERIC(2),
IsSameDay BIT,
IsErrorTag BIT,
OQAAirportID BIGINT,
OQDAirportID BIGINT,
OIAAirportID BIGINT,
OIDAirportID BIGINT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQLeg PRIMARY KEY CLUSTERED(CQLegID ASC),
CONSTRAINT FKCQLegCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQLegUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQLegCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQLegDAirport FOREIGN KEY(DAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQLegAAirport FOREIGN KEY(AAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQLegCrewDutyRules FOREIGN KEY(CrewDutyRulesID) REFERENCES CrewDutyRules(CrewDutyRulesID),
CONSTRAINT FKCQLegOQAAirport FOREIGN KEY(OQAAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQLegOQDAirport FOREIGN KEY(OQDAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQLegOIAAirport FOREIGN KEY(OIAAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQLegOIDAirport FOREIGN KEY(OIDAirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQPassenger
(
CQPassengerID BIGINT,
CustomerID BIGINT,
CQLegID BIGINT,
QuoteID INT,
LegID INT,
PassengerRequestorID BIGINT,
PassengerName VARCHAR(63),
FlightPurposeID BIGINT,
PassportNUM VARCHAR(25),
Billing VARCHAR(25),
IsNonPassenger BIT,
IsBlocked BIT,
OrderNUM INT,
WaitList CHAR(1),
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQPassenger PRIMARY KEY CLUSTERED(CQPassengerID ASC),
CONSTRAINT FKCQPassengerCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQPassengerUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQPassengerCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQPassengerPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKCQPassengerFlightPurpose FOREIGN KEY(FlightPurposeID) REFERENCES FlightPurpose(FlightPurposeID)
)
GO

CREATE TABLE CQFBOList
(
CQFBOListID BIGINT,
CustomerID BIGINT,
CQLegID BIGINT,
QuoteID INT,
LegID INT,
RecordType CHAR(2),
FBOID BIGINT,
CQFBOListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
AirportID BIGINT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQFBOList PRIMARY KEY CLUSTERED(CQFBOListID ASC),
CONSTRAINT FKCQFBOListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQFBOListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQFBOListCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQFBOListFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID),
CONSTRAINT FKCQFBOListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQTransportList
(
CQTransportListID BIGINT,
CustomerID BIGINT,
CQLegID BIGINT,
QuoteID INT,
LegID INT,
RecordType CHAR(2),
TransportID BIGINT,
CQTransportListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(17,2),
AirportID BIGINT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQTransportList PRIMARY KEY CLUSTERED(CQTransportListID ASC),
CONSTRAINT FKCQTransportListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQTransportListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQTransportListCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQTransportListTransport FOREIGN KEY(TransportID) REFERENCES Transport(TransportID),
CONSTRAINT FKCQTransportListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQHotelList
(
CQHotelListID BIGINT,
CustomerID BIGINT,
CQLegID BIGINT,
QuoteID INT,
LegID INT,
RecordType CHAR(2),
HotelID BIGINT,
CQHotelListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(17,2),
AirportID BIGINT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQHotelList PRIMARY KEY CLUSTERED(CQHotelListID ASC),
CONSTRAINT FKCQHotelListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQHotelListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQHotelListCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQHotelListHotel FOREIGN KEY(HotelID) REFERENCES Hotel(HotelID),
CONSTRAINT FKCQHotelListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQCateringList
(
CQCateringListID BIGINT,
CustomerID BIGINT,
CQLegID BIGINT,
QuoteID INT,
LegID INT,
RecordType CHAR(2),
CateringID BIGINT,
CQCateringListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(17,2),
AirportID BIGINT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQCateringList PRIMARY KEY CLUSTERED(CQCateringListID ASC),
CONSTRAINT FKCQCateringListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQCateringListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQCateringListCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQCateringListCatering FOREIGN KEY(CateringID) REFERENCES Catering(CateringID),
CONSTRAINT FKCQCateringListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQQuoteMain
(
CQQuoteMainID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
CompanyInformation VARCHAR(MAX),
ReportHeader VARCHAR(MAX),
ReportFooter VARCHAR(MAX),
PageOff NUMERIC(2),
IsPrintDetail BIT,
IsPrintFees BIT,
IsPrintSum BIT,
ImagePosition NUMERIC(1),
IsPrintQuoteID BIT,
CQMessageID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQQuoteMain PRIMARY KEY CLUSTERED(CQQuoteMainID ASC),
CONSTRAINT FKCQQuoteMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQQuoteMainCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQQuoteMainCQMessage FOREIGN KEY(CQMessageID) REFERENCES CQMessage(CQMessageID),
CONSTRAINT FKCQQuoteMainUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQQuoteSummary
(
CQQuoteSummaryID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
IsPrintUsageAdj BIT,
UsageAdjDescription VARCHAR(60),
CustomUsageAdj NUMERIC(17,2),
IsPrintAdditonalFees BIT,
AdditionalFeeDescription VARCHAR(60),
AdditionalFeesTax NUMERIC(17,2),
PositioningDescriptionINTL NUMERIC(17,2),
IsAdditionalFees NUMERIC(17,2),
CustomAdditionalFees NUMERIC(17,2),
IsPrintLandingFees BIT,
LandingFeeDescription VARCHAR(60),
CustomLandingFee NUMERIC(17,2),
IsPrintSegmentFee BIT,
SegmentFeeDescription VARCHAR(60),
CustomSegmentFee NUMERIC(17,2),
IsPrintStdCrew BIT,
StdCrewROMDescription VARCHAR(60),
CustomStdCrewRON NUMERIC(17,2),
IsPrintAdditionalCrew BIT,
AdditionalCrewDescription VARCHAR(60),
CustomAdditionalCrew NUMERIC(17,2),
IsAdditionalCrewRON BIT,
AdditionalDescriptionCrewRON VARCHAR(60),
CustomAdditionalCrewRON NUMERIC(17,2),
IsPrintWaitingChg BIT,
WaitChgDescription VARCHAR(60),
CustomWaitingChg NUMERIC(17,2),
IsPrintFlightChg BIT,
FlightChgDescription VARCHAR(60),
CustomFlightChg NUMERIC(17,2),
IsPrintSubtotal BIT,
SubtotalDescription VARCHAR(60),
IsPrintDiscountAmt BIT,
DescriptionDiscountAmt VARCHAR(60),
IsPrintTaxes BIT,
TaxesDescription VARCHAR(60),
IsPrintInvoice BIT,
InvoiceDescription VARCHAR(60),
ReportQuote NUMERIC(17,2),
IsPrintPay BIT,
PrepayDescription VARCHAR(60),
IsPrintRemaingAMT BIT,
RemainingAmtDescription VARCHAR(60),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQQuoteSummary PRIMARY KEY CLUSTERED(CQQuoteSummaryID ASC),
CONSTRAINT FKCQQuoteSummaryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQQuoteSummaryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQQuoteSummaryCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID)
)
GO

CREATE TABLE CQInvoiceMain
(
CQInvoiceMainID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
QuoteID INT,
InvoiceNUM INT,
InvoiceDT DATE,
DispatchNUM VARCHAR(12),
CharterCompanyInformation VARCHAR(MAX),
CustomerAddressInformation VARCHAR(MAX),
InvoiceHeader VARCHAR(MAX),
InvoiceFooter VARCHAR(MAX),
PageOff NUMERIC(2),
IsPrintDetail BIT,
IsPrintFees BIT,
IsPrintSum BIT,
CQMessageID BIGINT,
IsInvoiceAdditionalFeeTax BIT,
InvoiceAdditionalFeeTax NUMERIC(5,2),
IsDetailTax BIT,
IsFlightChg BIT,
FileNUM	INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQInvoiceMain PRIMARY KEY CLUSTERED(CQInvoiceMainID ASC),
CONSTRAINT FKCQInvoiceMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQInvoiceMainCQMessage FOREIGN KEY(CQMessageID) REFERENCES CQMessage(CQMessageID),
CONSTRAINT FKCQInvoiceMainUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQInvoiceMainCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID)
)
GO

CREATE TABLE CQInvoiceQuoteDetail
(
CQInvoiceQuoteDetailID BIGINT,
CustomerID BIGINT,
CQInvoiceMainID BIGINT,
InvoiceNUM INT,
CQLegID BIGINT,
InvoiceDetailID INT,
QuoteID INT,
IsPrintable BIT,
DepartureDT DATE,
FromDescription VARCHAR(120),
ToDescription VARCHAR(120),
FlightHours NUMERIC(5,1),
Distance NUMERIC(5),
PassengerTotal NUMERIC(3),
FlightCharge NUMERIC(17,2),
InvoiceFlightChg NUMERIC(17,2),
IsTaxable BIT,
TaxRate NUMERIC(5,2),
DAirportID BIGINT,
AAirportID BIGINT,
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
LegID INT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQInvoiceQuoteDetail PRIMARY KEY CLUSTERED(CQInvoiceQuoteDetailID ASC),
CONSTRAINT FKCQInvoiceQuoteDetailCQLeg FOREIGN KEY(CQLegID) REFERENCES CQLeg(CQLegID),
CONSTRAINT FKCQInvoiceQuoteDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQInvoiceQuoteDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQInvoiceQuoteDetailCQInvoiceMain FOREIGN KEY(CQInvoiceMainID) REFERENCES CQInvoiceMain(CQInvoiceMainID),
CONSTRAINT FKCQInvoiceQuoteDetailDAirport FOREIGN KEY(DAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCQInvoiceQuoteDetailAAirport FOREIGN KEY(AAirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE CQInvoiceAdditionalFees
(
CQInvoiceAdditionalFeesID BIGINT,
CustomerID BIGINT,
CQInvoiceMainID BIGINT,
QuoteID INT,
InvoiceNUM INT,
FeeIdentification INT,
IsPrintable BIT,
IsTaxable BIT,
CQInvoiceFeeDescription VARCHAR(60),
QouteAmount NUMERIC(17,2),
InvoiceAmt NUMERIC(17,2),
OrderNUM INT,
IsDiscount BIT,
FileNUM INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQInvoiceAdditionalFees PRIMARY KEY CLUSTERED(CQInvoiceAdditionalFeesID ASC),
CONSTRAINT FKCQInvoiceAdditionalFeesCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQInvoiceAdditionalFeesUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQInvoiceAdditionalFeesCQInvoiceMain FOREIGN KEY(CQInvoiceMainID) REFERENCES CQInvoiceMain(CQInvoiceMainID)
)
GO

CREATE TABLE CQInvoiceSummary
(
CQInvoiceSummaryID BIGINT,
CustomerID BIGINT,
CQInvoiceMainID BIGINT,
IsSumTax BIT,
IsManualTax BIT,
IsManualDiscount BIT,
InvoiceSummaryTax NUMERIC(5,2),
IsPrintUsage BIT,
UsageAdjDescription VARCHAR(60),
QuoteUsageAdj NUMERIC(17,2),
UsageAdjTotal NUMERIC(17,2),
IsUsageAdjTax BIT,
IsPrintAdditonalFees BIT,
DescriptionAdditionalFee VARCHAR(60),
AdditionalFees NUMERIC(17,2),
AdditionalFeeTotalD NUMERIC(17,2),
IsAdditionalFeesTax BIT,
IsPrintLandingFees BIT,
LandingFeeDescription VARCHAR(60),
QuoteLandingFee NUMERIC(17,2),
LandingFeeTotal NUMERIC(17,2),
IsLandingFeeTax BIT,
IsLandingFeeDiscount BIT,
IsPrintSegmentFee BIT,
SegmentFeeDescription VARCHAR(60),
QuoteSegementFee NUMERIC(17,2),
SegementFeeTotal NUMERIC(17,2),
IsSegmentFeeTax BIT,
IsSegmentFeeDiscount BIT,
IsPrintStdCrew BIT,
StdCrewRONDescription VARCHAR(60),
QuoteStdCrewRON NUMERIC(17,2),
StdCrewRONTotal NUMERIC(17,2),
IsStdCrewRONTax BIT,
IsStdCrewRONDiscount BIT,
IsPrintAdditionalCrew BIT,
AdditionalCrewDescription VARCHAR(60),
CQAdditionalCrew NUMERIC(17,2),
AdditionalCrewTotal NUMERIC(17,2),
IsAdditionalCrewTax BIT,
AdditionalCrewDiscount BIT,
IsAdditionalCrewRON BIT,
AdditionalCrewRON VARCHAR(60),
AdditionalRONCrew NUMERIC(17,2),
AdditionalCrewRONTotal NUMERIC(17,2),
IsAdditionalCrewRONTax BIT,
IsAdditionalCrewRONDiscount BIT,
IsWaitingChg BIT,
WaitChgdescription VARCHAR(60),
QuoteWaitingChg NUMERIC(17,2),
WaitChgTotal NUMERIC(17,2),
IsWaitingChgTax BIT,
IsWaitingChgDiscount BIT,
IsPrintFlightChg BIT,
FlightChgDescription VARCHAR(60),
FlightCharge NUMERIC(17,2),
FlightChargeTotal NUMERIC(17,2),
IsFlightChgTax BIT,
IsPrintSubtotal BIT,
SubtotalDescription VARCHAR(60),
QuoteSubtotal NUMERIC(17,2),
SubtotalTotal NUMERIC(17,2),
IsSubtotalTax BIT,
IsPrintDiscountAmt BIT,
DescriptionDiscountAmt VARCHAR(60),
QuoteDiscountAmount NUMERIC(17,2),
DiscountAmtTotal NUMERIC(17,2),
IsPrintTaxes BIT,
TaxesDescription VARCHAR(60),
QuoteTaxes NUMERIC(17,2),
TaxesTotal NUMERIC(17,2),
IsInvoiceTax BIT,
IsPrintInvoice BIT,
InvoiceDescription VARCHAR(60),
QuoteInvoice NUMERIC(17,2),
InvoiceTotal NUMERIC(17,2),
IsPrintPrepay BIT,
PrepayDescription VARCHAR(60),
QuotePrepay NUMERIC(17,2),
PrepayTotal NUMERIC(17,2),
IsPrintRemaingAMT BIT,
RemainingAmtDescription VARCHAR(60),
QuoteRemainingAmt NUMERIC(17,2),
RemainingAmtTotal NUMERIC(17,2),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQInvoiceSummary PRIMARY KEY CLUSTERED(CQInvoiceSummaryID ASC),
CONSTRAINT FKCQInvoiceSummaryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQInvoiceSummaryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCQInvoiceSummaryCQInvoiceMain FOREIGN KEY(CQInvoiceMainID) REFERENCES CQInvoiceMain(CQInvoiceMainID)
)
GO

CREATE TABLE CQException
(
CQExceptionID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
CQExceptionDescription VARCHAR(1500),
ExceptionTemplateID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQException PRIMARY KEY CLUSTERED(CQExceptionID ASC),
CONSTRAINT FKCQExceptionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQExceptionCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQExceptionExceptionTemplate FOREIGN KEY(ExceptionTemplateID) REFERENCES ExceptionTemplate(ExceptionTemplateID),
CONSTRAINT FKCQExceptionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQHistory
(
CQHistoryID BIGINT,
CustomerID BIGINT,
CQMainID BIGINT,
LogisticsHistory VARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCQHistory PRIMARY KEY CLUSTERED(CQHistoryID ASC),
CONSTRAINT FKCQHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQHistoryCQMain FOREIGN KEY(CQMainID) REFERENCES CQMain(CQMainID),
CONSTRAINT FKCQHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQReportHeader
(
CQReportHeaderID BIGINT,
CustomerID BIGINT,
ReportID CHAR(8),
ReportDescription VARCHAR(40),
HomebaseID BIGINT,
UserGroupID BIGINT,
ClientID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCQReportHeader PRIMARY KEY CLUSTERED(CQReportHeaderID ASC),
CONSTRAINT FKCQReportHeaderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQReportHeaderCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCQReportHeaderUserGroup FOREIGN KEY(UserGroupID) REFERENCES UserGroup(UserGroupID),
CONSTRAINT FKCQReportHeaderClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKCQReportHeaderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)						
GO

CREATE TABLE CQReportDefinition
(
CQReportDefinitionID BIGINT,
CustomerID BIGINT,
ReportNumber INT,
ReportDescription VARCHAR(35),
ParameterVariable VARCHAR(10),
ParameterDescription VARCHAR(100),
ParameterType CHAR(1),
ParameterWidth INT,
ParameterCValue VARCHAR(60),
ParameterLValue BIT,
ParameterNValue INT,
ParameterValue VARCHAR(1500),
ReportProcedure VARCHAR(8),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCQReportDefinition PRIMARY KEY CLUSTERED(CQReportDefinitionID ASC),
CONSTRAINT FKCQReportDefinitionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQReportDefinitionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CQReportDetail
(
CQReportDetailID BIGINT,
CustomerID BIGINT,
CQReportHeaderID BIGINT,
ReportNumber INT,
ReportDescription VARCHAR(35),
IsSelected BIT,
Copies INT,
ReportProcedure VARCHAR(8),
ParameterCD VARCHAR(10),
ParamterDescription VARCHAR(100),
ParamterType CHAR(1),
ParameterWidth INT,
ParameterCValue VARCHAR(60),
ParameterLValue BIT,
ParameterNValue INT,
ParameterValue VARCHAR(1500),
ReportOrder INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCQReportDetail PRIMARY KEY CLUSTERED(CQReportDetailID ASC),
CONSTRAINT FKCQReportDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCQReportDetailCQReportHeader FOREIGN KEY(CQReportHeaderID) REFERENCES CQReportHeader(CQReportHeaderID),
CONSTRAINT FKCQReportDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 23-Jan-2013 --

sp_rename 'CQFleetChargeTotal.SegementFeeTotal','SegmentFeeTotal','COLUMN'
GO

sp_rename 'CQInvoiceAdditionalFees.QouteAmount','QuoteAmount','COLUMN'
GO

sp_rename 'CQInvoiceSummary.AdditionalFeeTotalD','AdditionalFeeTotal','COLUMN'
GO

sp_rename 'CQInvoiceSummary.SegementFeeTotal','SegmentFeeTotal','COLUMN'
GO

-- Database Change Log 29-Jan-2013 --

ALTER TABLE CQMain ADD StandardCrewDOM NUMERIC(2)
GO

ALTER TABLE CQMain ADD StandardCrewINTL NUMERIC(2)
GO

ALTER TABLE CQMain ADD DaysTotal NUMERIC(4)
GO

ALTER TABLE CQMain ADD FlightHoursTotal NUMERIC(9,3)
GO

ALTER TABLE CQMain ADD AverageFlightHoursTotal NUMERIC(7,2)
GO

ALTER TABLE CQMain ADD AdjAmtTotal NUMERIC(17,3)
GO

ALTER TABLE CQMain ADD UsageAdjTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideUsageAdj BIT
GO

ALTER TABLE CQMain ADD SegmentFeeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideSegmentFee BIT
GO

ALTER TABLE CQMain ADD LandingFeeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideLandingFee BIT
GO

ALTER TABLE CQMain ADD AdditionalFeeTotalD NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD FlightChargeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD SubtotalTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD DiscountPercentageTotal NUMERIC(6,2)
GO

ALTER TABLE CQMain ADD IsOverrideDiscountPercentage BIT
GO

ALTER TABLE CQMain ADD DiscountAmtTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideDiscountAMT BIT
GO

ALTER TABLE CQMain ADD TaxesTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideTaxes BIT
GO

ALTER TABLE CQMain ADD MarginalPercentTotal NUMERIC(7,2)
GO

ALTER TABLE CQMain ADD MarginTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD QuoteTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsTotalQuote BIT
GO

ALTER TABLE CQMain ADD CostTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideCost BIT
GO

ALTER TABLE CQMain ADD PrepayTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD RemainingAmtTotal NUMERIC(17,2)
GO

DROP TABLE CQFleetChargeTotal
GO

-- SQL Script File 29-Jan-2013 --

ALTER TABLE [dbo].[PreflightMain] ADD CQCustomerID BIGINT

ALTER TABLE [dbo].[PreflightMain]  WITH CHECK ADD  CONSTRAINT [FKPreflightMainCQCustomer] FOREIGN KEY([CQCustomerID])
REFERENCES [dbo].[CQCustomer] ([CQCustomerID])
GO

ALTER TABLE [dbo].[PreflightMain] CHECK CONSTRAINT [FKPreflightMainCQCustomer]
GO

ALTER TABLE [dbo].[PreflightLeg] ADD CQCustomerID BIGINT

ALTER TABLE [dbo].[PreflightLeg]  WITH CHECK ADD  CONSTRAINT [FKPreflightLegCQCustomer] FOREIGN KEY([CQCustomerID])
REFERENCES [dbo].[CQCustomer] ([CQCustomerID])
GO

ALTER TABLE [dbo].[PreflightLeg] CHECK CONSTRAINT [FKPreflightLegCQCustomer]
GO

-- SQL Script File 06-Mar-2013 --
--CQLostBusiness
ALTER TABLE CQLostBusiness ADD CQLostBusinessCD CHAR(4)
GO

--CQCustomerContact 
ALTER TABLE CQCustomerContact ALTER COLUMN CreditName1 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ALTER COLUMN CreditNum1 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ALTER COLUMN CreditName2 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ALTER COLUMN CreditNum2 NVARCHAR(240)
GO

ALTER TABLE CQCustomerContact ADD CardType1 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ADD SecurityCode1 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ADD ExpirationDate1 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ADD CardType2 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ADD SecurityCode2 NVARCHAR(240)
GO
ALTER TABLE CQCustomerContact ADD ExpirationDate2 NVARCHAR(240)
GO

--CQCustomer
--ALTER TABLE CQCustomer ADD SalesPersonID BIGINT
--GO
--ALTER TABLE CQCustomer ADD CONSTRAINT FKCQCustomerSalesPerson FOREIGN KEY(SalesPersonID) REFERENCES SalesPerson(SalesPersonID)
--GO