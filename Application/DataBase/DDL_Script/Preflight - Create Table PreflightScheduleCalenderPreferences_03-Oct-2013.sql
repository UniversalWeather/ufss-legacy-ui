
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKPreflightScheduleCalenderPreferencesCustomer]') AND parent_object_id = OBJECT_ID(N'[dbo].[PreflightScheduleCalenderPreferences]'))
ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences] DROP CONSTRAINT [FKPreflightScheduleCalenderPreferencesCustomer]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKPreflightScheduleCalenderPreferencesUserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[PreflightScheduleCalenderPreferences]'))
ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences] DROP CONSTRAINT [FKPreflightScheduleCalenderPreferencesUserMaster]
GO


GO

/****** Object:  Table [dbo].[PreflightScheduleCalenderPreferences]    Script Date: 10/03/2013 23:07:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PreflightScheduleCalenderPreferences]') AND type in (N'U'))
DROP TABLE [dbo].[PreflightScheduleCalenderPreferences]
GO


GO

/****** Object:  Table [dbo].[PreflightScheduleCalenderPreferences]    Script Date: 10/03/2013 23:07:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreflightScheduleCalenderPreferences](
	[SCPreferenceID] [bigint] NOT NULL,
	[UserName] [varchar](30) NULL,
	[CustomerID] [bigint] NULL,
	[FleetIDS] [nvarchar](4000) NULL,
	[CrewIDS] [nvarchar](4000) NULL,
 CONSTRAINT [PKPreflightScheduleCalenderPreferences] PRIMARY KEY CLUSTERED 
(
	[SCPreferenceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences]  WITH CHECK ADD  CONSTRAINT [FKPreflightScheduleCalenderPreferencesCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences] CHECK CONSTRAINT [FKPreflightScheduleCalenderPreferencesCustomer]
GO

ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences]  WITH CHECK ADD  CONSTRAINT [FKPreflightScheduleCalenderPreferencesUserMaster] FOREIGN KEY([UserName])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[PreflightScheduleCalenderPreferences] CHECK CONSTRAINT [FKPreflightScheduleCalenderPreferencesUserMaster]
GO


