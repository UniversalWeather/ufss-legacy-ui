create nonclustered index FleetCLIENTIDX on Fleet
(
ClientID ,
CustomerID
)

create nonclustered index FleetISINACTIDX on Fleet
(
IsInActive  ,
CustomerID
)

create nonclustered index FleetVENDORIDX on Fleet
(
VendorID ,
CustomerID
)

create nonclustered index AircraftISFIXEDIDX on Aircraft 
(
IsFixedRotary ,
CustomerID
)