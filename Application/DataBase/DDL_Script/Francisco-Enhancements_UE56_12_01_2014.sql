------------------------------------------- UE-UE-56 ticket numer
-- duplicated index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerCustIDX' AND object_id = OBJECT_ID('Passenger'))
BEGIN
    DROP INDEX [PassengerCustIDX] ON [dbo].[Passenger]
END
GO

------------------------------------------- UE-UE-56 ticket numer
-- removed index to add more cover in the below [Passenger_CustomerID_IsDeleted_IDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerISDELIDX' AND object_id = OBJECT_ID('Passenger'))
BEGIN
    DROP INDEX [PassengerISDELIDX] ON [dbo].[Passenger]
END
GO

------------------------------------------- UE-UE-56 ticket numer
-- used in SP [spFlightPak_Preflight_AddPreflightTransport]  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Passenger_CustomerID_IsDeleted_IDX' AND object_id = OBJECT_ID('Passenger'))
BEGIN
    DROP INDEX [Passenger_CustomerID_IsDeleted_IDX] ON [dbo].[Passenger]
END
GO

CREATE NONCLUSTERED INDEX [Passenger_CustomerID_IsDeleted_IDX] ON [dbo].[Passenger]
(
    [CustomerID] ASC,
	[IsDeleted] ASC
) 
INCLUDE ([PassengerRequestorID],[PassengerRequestorCD],[PassengerName],[DepartmentID],[PassengerDescription],[PhoneNum],[ClientID],[HomebaseID],[IsActive],[IsRequestor],[CQCustomerID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO