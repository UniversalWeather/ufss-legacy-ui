------------------------------------------- UE-201 ticket numer
-- used in SP spCrewRosterCurrency  - CrewID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightCrew_CrewID_IDX' AND object_id = OBJECT_ID('PostflightCrew'))
    BEGIN
        DROP INDEX [PostflightCrew_CrewID_IDX] ON [dbo].[PostflightCrew]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightCrew_CrewID_IDX] ON [dbo].[PostflightCrew]
(
    [CrewID] ASC
) INCLUDE ([POLogID],[POLegID],[TakeOffDay],[TakeOffNight],[LandingDay],[LandingNight],[ApproachPrecision],[ApproachNonPrecision],[Instrument],[DutyEnd],[DutyHours],[BlockHours],[FlightHours])

WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


------------------------------------------- UE-201 ticket numer
-- used in SP spCrewRosterCurrency  - CrewID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightSimulatorLog_CrewID_IDX' AND object_id = OBJECT_ID('PostflightSimulatorLog'))
    BEGIN
        DROP INDEX [PostflightSimulatorLog_CrewID_IDX] ON [dbo].[PostflightSimulatorLog]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightSimulatorLog_CrewID_IDX] ON [dbo].[PostflightSimulatorLog]
(
    [CrewID] ASC,
    [IsDeleted] ASC,
    [SessionDT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO