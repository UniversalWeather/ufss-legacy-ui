IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKCQFleetChargeDetailFeeGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[CQFleetChargeDetail]'))
ALTER TABLE [dbo].[CQFleetChargeDetail] DROP CONSTRAINT [FKCQFleetChargeDetailFeeGroup]
GO

alter table CQFleetChargeDetail
	drop column FeeGroupID,
				IsDailyTaxAdj,
				IsLandingFeeTax
Go				
				
	
alter table CQMain
	add IsInActive bit,
	IsDailyTaxAdj bit,
	IsLandingFeeTax bit,
	FeeGroupID bigint
Go


ALTER TABLE [dbo].[CQMain]  WITH CHECK ADD  CONSTRAINT [FKCQMainFeeGroup] FOREIGN KEY([FeeGroupID])
REFERENCES [dbo].[FeeGroup] ([FeeGroupID])
GO

ALTER TABLE [dbo].[CQMain] CHECK CONSTRAINT [FKCQMainFeeGroup]
GO

