
/****** Object:  Index [PostflightMainTripIDX]    Script Date: 10/29/2013 16:52:00 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PostflightMain]') AND name = N'PostflightMainTripIDX')
DROP INDEX [PostflightMainTripIDX] ON [dbo].[PostflightMain] WITH ( ONLINE = OFF )
GO



/****** Object:  Index [PostflightMainTripIDX]    Script Date: 10/29/2013 16:52:00 ******/
CREATE NONCLUSTERED INDEX [PostflightMainTripIDX] ON [dbo].[PostflightMain] 
(
	[TripID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 25) ON [PRIMARY]
GO


