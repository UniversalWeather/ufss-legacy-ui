create nonclustered index PassengerCLIENTIDX on Passenger

(
ClientID ,
CustomerID
)

create nonclustered index PassengerISACTIDX on Passenger

(
IsActive ,
CustomerID
)

create nonclustered index PassengerISREQIDX on Passenger

(
IsRequestor ,
CustomerID
)

create nonclustered index PassengerCQCUSTIDX on Passenger

(
CQCustomerID  ,
CustomerID
)
