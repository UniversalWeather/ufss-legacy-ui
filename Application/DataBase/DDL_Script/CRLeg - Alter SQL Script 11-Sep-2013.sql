
--To include 2 columns in CRLeg table
Alter table CRleg add EstFuelQTY numeric(17,3)
Alter table CRleg add FuelUnits numeric(1,0)