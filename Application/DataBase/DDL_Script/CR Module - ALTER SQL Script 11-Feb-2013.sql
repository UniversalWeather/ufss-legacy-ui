-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Corporate Request Module --
-- Created Date: 11-Feb-2013        --
-- Updated Date: 11-Feb-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

ALTER TABLE FlightpakSequence ADD RequestCurrentNo BIGINT
GO

UPDATE FlightpakSequence SET RequestCurrentNo = 0 WHERE RequestCurrentNo IS NULL

CREATE TABLE CRException
(
CRExceptionID BIGINT,
CustomerID BIGINT,
CRMainID BIGINT,
CRExceptionDescription VARCHAR(1500),
ExceptionTemplateID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
DisplayOrder VARCHAR(15)
CONSTRAINT PKCRException PRIMARY KEY CLUSTERED(CRExceptionID ASC),
CONSTRAINT FKCRExceptionCRMain FOREIGN KEY(CRMainID) REFERENCES CRMain(CRMainID),
CONSTRAINT FKCRExceptionExceptionTemplate FOREIGN KEY(ExceptionTemplateID) REFERENCES ExceptionTemplate(ExceptionTemplateID),
CONSTRAINT FKCRExceptionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRExceptionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- SQL Script File 11-Feb-2013 --