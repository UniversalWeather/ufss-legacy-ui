
/****** Object:  Table [dbo].[HelpContent]    Script Date: 03/12/2013 21:26:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HelpContent](
	[Identifier] [varchar](60) NOT NULL,
	[Category] [varchar](100) NULL,
	[Topic] [varchar](100) NULL,
	[Content] [nvarchar](max) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Identifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[HelpContent] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO

ALTER TABLE [dbo].[HelpContent] ADD  DEFAULT ((0)) FOR [IsInActive]
GO



/****** Object:  Table [dbo].[HelpItems]    Script Date: 03/12/2013 21:30:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HelpItems](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[ParentID] [int] NULL,
	[MimeType] [varchar](25) NULL,
	[IsDirectory] [bit] NULL,
	[Size] [bigint] NULL,
	[Content] [varbinary](max) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
