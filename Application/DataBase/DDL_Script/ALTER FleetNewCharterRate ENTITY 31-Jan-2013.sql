ALTER TABLE FleetNewCharterRate DROP COLUMN AircraftTypeCD
GO

ALTER TABLE FleetNewCharterRate DROP CONSTRAINT FleetNewCharterRateIsDeleted
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN IsDeleted
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN IsInActive
GO

ALTER TABLE FleetNewCharterRate ADD AircraftTypeID BIGINT
GO

ALTER TABLE FleetNewCharterRate ADD CONSTRAINT FKFleetNewCharterRateAircraft FOREIGN KEY(AircraftTypeID) REFERENCES Aircraft(AircraftID)
GO

ALTER TABLE FleetNewCharterRate ADD IsDeleted BIT NOT NULL CONSTRAINT DF_FleetNewCharterRate_IsDeleted DEFAULT ((0))
GO

ALTER TABLE FleetNewCharterRate ADD IsInActive BIT
GO

ALTER TABLE FleetNewCharterRate ADD DBAircraftFlightChgID BIGINT
GO

ALTER TABLE FleetNewCharterRate ADD DSAircraftFlightChgID BIGINT
GO

ALTER TABLE FleetNewCharterRate ADD BuyAircraftFlightIntlID BIGINT
GO

ALTER TABLE FleetNewCharterRate ADD SellAircraftFlightIntlID BIGINT
GO

ALTER TABLE FleetNewCharterRate ADD CONSTRAINT FKFleetNewCharterRateDBAircraftFlightChgID FOREIGN KEY(DBAircraftFlightChgID) REFERENCES Account(AccountID)
GO

ALTER TABLE FleetNewCharterRate ADD CONSTRAINT FKFleetNewCharterRateDSAircraftFlightChgID FOREIGN KEY(DSAircraftFlightChgID) REFERENCES Account(AccountID)
GO

ALTER TABLE FleetNewCharterRate ADD CONSTRAINT FKFleetNewCharterRateBuyAircraftFlightIntlID FOREIGN KEY(BuyAircraftFlightIntlID) REFERENCES Account(AccountID)
GO

ALTER TABLE FleetNewCharterRate ADD CONSTRAINT FKFleetNewCharterRateSellAircraftFlightIntlID FOREIGN KEY(SellAircraftFlightIntlID) REFERENCES Account(AccountID)
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN DBAircraftFlightChg
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN DSAircraftFlightChg
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN BuyAircraftFlightIntl
GO

ALTER TABLE FleetNewCharterRate DROP COLUMN IsSellAircraftFlightIntl
GO