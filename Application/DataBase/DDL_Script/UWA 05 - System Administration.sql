-- --------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: System Administration    --
-- Created Date: 11-Jul-2012        --
-- Updated Date: 20-Nov-2012        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE FPAuditTrial
(
AuditTrialID BIGINT NOT NULL ,
CustomerID BIGINT NOT NULL ,
MainScreeneName VARCHAR(20) NOT NULL ,
TabName VARCHAR(30) NULL ,
SubtabName VARCHAR(30) NULL ,
EntityName VARCHAR(30) NOT NULL ,
ActionType BIT NOT NULL ,
ActionDescription VARCHAR(30) NOT NULL ,
TablePrimaryKey BIGINT NOT NULL ,
ActionOwnerUID CHAR(30) NULL ,
IsDeleted BIT NOT NULL ,
LastupdTS DATETIME NULL ,
ActionTime DATETIME NOT NULL 
CONSTRAINT CURRENT_TIMESTAMP_1248894560 DEFAULT CURRENT_TIMESTAMP,
LastUpdUID char(30) NULL 
)
GO

ALTER TABLE FPAuditTrial ADD CONSTRAINT PKFPAuditTrial PRIMARY KEY CLUSTERED (AuditTrialID ASC)
GO

CREATE UNIQUE NONCLUSTERED INDEX FPAuditTrialIDX ON FPAuditTrial
(
CustomerID ASC
)
GO

CREATE TABLE FPBookmark
(
BookMarkID BIGINT NOT NULL,
BookmarkName VARCHAR(30) NULL,
BookmarkURL VARCHAR(30) NULL,
SystemPrefID BIGINT NULL,
LastUpdUID CHAR(30) NULL ,
LastUpdTS DATETIME NULL,
Isdeleted BIT NULL
)
GO

ALTER TABLE FPBookmark ADD CONSTRAINT PKFPBookmark PRIMARY KEY CLUSTERED(BookMarkID ASC)
GO

CREATE TABLE FPEmailTmpl
(
EmailTmplID BIGINT NOT NULL,
TmplName VARCHAR(20) NOT NULL,
EmailBody VARCHAR(20) NOT NULL,
EmailImage BIGINT NULL,
EmailSubjct VARCHAR(20) NULL,
CustomerID BIGINT NULL,
LastUpdUID CHAR(30) NULL,
LastUpdTS DATETIME NULL,
Isdeleted BIT NULL
)
GO

ALTER TABLE FPEmailTmpl ADD CONSTRAINT PKFPEmailTmpl PRIMARY KEY CLUSTERED(EmailTmplID ASC)
GO

CREATE TABLE FPEmailTranLog
(
EmailTranID BIGINT NOT NULL,
EmailTmplID BIGINT NOT NULL,
EmailReqUID CHAR(30) NOT NULL,
CustomerID BIGINT NOT NULL,
EmailReqTime DATETIME NOT NULL,
EmailStatus CHAR(18) NULL,
EmailPrio BIT NULL,
EmailSentTo CHAR(18) NULL,
LastUPDUID CHAR(30) NULL,
LastUpdTS DATETIME NULL,
IsDeleted BIT NULL
)
GO

ALTER TABLE FPEmailTranLog ADD CONSTRAINT PKFPEmailTranLog PRIMARY KEY CLUSTERED(EmailTranID ASC)
GO

CREATE TABLE FPLockLog
(
FPLockID BIGINT NOT NULL ,
CustomerID BIGINT NULL ,
EntityType VARCHAR(20) NOT NULL,
EntitySubype VARCHAR(20) NOT NULL,
LockType BIT NOT NULL,
SessionID UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL,
LockStatus BIT NOT NULL,
LockReqStartTime DATETIME NOT NULL
CONSTRAINT CURRENT_TIMESTAMP_2043211171 DEFAULT CURRENT_TIMESTAMP,
LockReqUID CHAR(30) NOT NULL,
EntityPrimaryKey BIGINT NOT NULL,
LockReqEndTime DATETIME NOT NULL
)
GO

ALTER TABLE FPLockLog ADD CONSTRAINT PKFPLockLog PRIMARY KEY CLUSTERED(FPLockID ASC)
GO

CREATE TABLE FPLoginSession
(
LoginsessionID BIGINT NOT NULL,
SessionID UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL,
LoginStartTime DATETIME NOT NULL
CONSTRAINT CURRENT_TIMESTAMP_285084979 DEFAULT CURRENT_TIMESTAMP,
LoginStatus BIT NOT NULL,
LoginEndtime DATETIME NOT NULL
CONSTRAINT CURRENT_TIMESTAMP_1946479907 DEFAULT CURRENT_TIMESTAMP,
LoginSessionUID CHAR(30) NOT NULL,
CustomerID BIGINT NOT NULL 
)
GO

ALTER TABLE FPLoginSession ADD CONSTRAINT PKFPLoginSession PRIMARY KEY CLUSTERED(LoginsessionID ASC)
GO

CREATE TABLE FPSystemPref
(
SystemPrefID BIGINT NOT NULL,
CustomerID BIGINT NULL,
ReqUID CHAR(30) NOT NULL,
Isdeleted BIT NULL,
LastUpdTS DATETIME NULL,
ReqTime DATETIME NULL,
LastUpdUID CHAR(30) NULL,
HomepageURL VARCHAR(100) NULL,
CalanderPage VARCHAR(20) NULL
)
GO

ALTER TABLE FPSystemPref ADD CONSTRAINT PKFPSystemPref PRIMARY KEY CLUSTERED(SystemPrefID ASC)
GO

CREATE UNIQUE NONCLUSTERED INDEX PKFPSystemPrefIDEX ON FPSystemPref
(
CustomerID ASC
)
GO

ALTER TABLE FPAuditTrial ADD CONSTRAINT FKFPAuditTrialCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPAuditTrial ADD CONSTRAINT FKFPAuditTrialAUserMaster FOREIGN KEY(ActionOwnerUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPAuditTrial ADD CONSTRAINT FKFPAuditTrialUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPBookmark ADD CONSTRAINT FKFPBookmarkFPSystemPref FOREIGN KEY(SystemPrefID) REFERENCES FPSystemPref(SystemPrefID)
GO

ALTER TABLE FPBookmark ADD CONSTRAINT FKFPBookmarkUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPEmailTmpl ADD CONSTRAINT FKFPEmailTmplCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPEmailTmpl ADD CONSTRAINT FKFPEmailTmplUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPEmailTranLog ADD CONSTRAINT FKFPEmailTranLogFPEmailTmpl FOREIGN KEY(EmailTmplID) REFERENCES FPEmailTmpl(EmailTmplID)
GO

ALTER TABLE FPEmailTranLog ADD CONSTRAINT FKFPEmailTranLogEUserMaster FOREIGN KEY(EmailReqUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPEmailTranLog ADD CONSTRAINT FKFPEmailTranLogCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPEmailTranLog ADD CONSTRAINT FKFPEmailTranLogUserMaster FOREIGN KEY(LastUPDUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPLockLog ADD CONSTRAINT FKFPLockLogCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPLockLog ADD CONSTRAINT FKFPLockLogUserMaster FOREIGN KEY(LockReqUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPLoginSession ADD CONSTRAINT FKFPLoginSessionUserMaster FOREIGN KEY(LoginSessionUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPLoginSession ADD CONSTRAINT FKFPLoginSessionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPSystemPref ADD CONSTRAINT FKFPSystemPrefCustomer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE FPSystemPref ADD CONSTRAINT FKFPSystemPrefRUserMaster FOREIGN KEY (ReqUID) REFERENCES UserMaster(UserName)
GO

ALTER TABLE FPSystemPref ADD CONSTRAINT FKFPSystemPrefUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
GO

-- Database Change Log 17-Jul-2012 --

CREATE TABLE BatchJobLog
(
BatchJobID BIGINT,
BatchJobName VARCHAR(50),
JobStartTime DATETIME,
JobEndTime DATETIME,
JobError VARCHAR(250),
JobStatus VARCHAR(20)
)
GO

-- Database Change Log 25-Jul-2012 --

ALTER TABLE FPAuditTrial ADD OldValue VARCHAR(MAX)
GO

ALTER TABLE FPAuditTrial ADD NewValue VARCHAR(MAX)
GO

ALTER TABLE FPAuditTrial ADD Processed CHAR(1)
GO

ALTER TABLE FPAuditTrial ADD LinkedToID BIGINT
GO

-- Database Change Log 26-Jul-2012 --

sp_rename 'dbo.FPAuditTrial.EntityName','TableName', 'COLUMN'
GO

sp_rename 'FPAuditTrial','FPAuditTrail'
GO

ALTER TABLE FPAuditTrail ADD ColumnName VARCHAR(30)
GO

-- Database Change Log 27-Jul-2012 --

ALTER TABLE FlightpakSequence ADD NonFunctionalCurrentNo BIGINT
GO

-- Database Change Log 30-Jul-2012 --

ALTER TABLE FlightpakSequence ADD TripCurrentNo BIGINT
GO

ALTER TABLE FlightpakSequence ADD TripLogCurrentNo BIGINT
GO

ALTER TABLE FPAuditTrail ALTER COLUMN MainScreeneName VARCHAR(20) NULL
GO

ALTER TABLE FPAuditTrail ALTER COLUMN ActionDescription VARCHAR(30) NULL
GO

sp_rename 'FPAuditTrail.AuditTrialID','AuditTrailID','COLUMN'
GO

ALTER TABLE FPAuditTrail ADD AuditTrailID_NEW BIGINT IDENTITY(1,1)
GO

ALTER TABLE FPAuditTrail DROP CONSTRAINT PKFPAuditTrial
GO

ALTER TABLE FPAuditTrail DROP COLUMN AuditTrailID
GO

sp_rename 'FPAuditTrail.AuditTrailID_NEW','AuditTrailID','COLUMN'
GO

ALTER TABLE FPAuditTrail ADD CONSTRAINT PKFPAuditTrail PRIMARY KEY CLUSTERED(AuditTrailID ASC)
GO

-- Database Change Log 01-Aug-2012 --

ALTER TABLE FPAuditTrail ALTER COLUMN ActionType CHAR(1)
GO

ALTER TABLE FPAuditTrail DROP COLUMN IsDeleted
GO

-- Database Change Log 03-Aug-2012 --

CREATE TABLE ExceptionTemplate
(
ExceptionTemplateID BIGINT,
ModuleID BIGINT,
SubModuleID BIGINT,
ExceptionTemplate VARCHAR(100),
Severity INT
CONSTRAINT PKExceptionTemplate PRIMARY KEY CLUSTERED(ExceptionTemplateID)
)
GO

-- Database Change Log 13-Aug-2012 --

DROP INDEX FPAuditTrialIDX ON FPAuditTrail
GO

ALTER TABLE FPEmailTmpl ALTER COLUMN EmailBody NVARCHAR(MAX)
GO

ALTER TABLE FPEmailTranLog ADD EmailBody NVARCHAR(MAX)
GO

ALTER TABLE FPEmailTranLog ADD EmailSubject VARCHAR(20)
GO

-- Database Change Log 17-Aug-2012 --

CREATE TABLE FPMigrationLog
(
MigrationTimeStamp DATETIME,
SQLEntity VARCHAR(100),
CustomerName VARCHAR(100),
PassedRecordCount NUMERIC(5),
ErrorRecordCount NUMERIC(5),
Migration_StartTime DATETIME,
Migration_EndTime DATETIME
)
GO

DROP INDEX PKFPSystemPrefIDEX ON FPSystemPref
GO

CREATE UNIQUE NONCLUSTERED INDEX FPSystemPrefIDX ON FPSystemPref
(
CustomerID ASC, ReqUID ASC
)
GO

-- Database Change Log 20-Aug-2012 --

CREATE TABLE FPGeneralException
(
ExceptionID BIGINT,
EntityID BIGINT,
CustomerID BIGINT,
EntityName VARCHAR(50),
ExceptionDescription VARCHAR(100),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKFPGeneralException PRIMARY KEY CLUSTERED(ExceptionID ASC),
CONSTRAINT FKFPGeneralExceptionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFPGeneralExceptionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 22-Aug-2012 --

CREATE TABLE FPDateFormat
(
DateFormatID BIGINT,
CountryName VARCHAR(50),
FormatName VARCHAR(20),
DateFormat VARCHAR(15),
CONSTRAINT PKFPDateFormat PRIMARY KEY CLUSTERED(DateFormatID ASC)
)
GO

--INSERT INTO FPDateFormat VALUES (100000001,'United States','AMERICAN','MM/DD/YYYY')

--INSERT INTO FPDateFormat VALUES (100000002,'','ANSI','YYYY.MM.DD')

--INSERT INTO FPDateFormat VALUES (100000003,'UK','BRITISH','DD/MM/YYYY')

--INSERT INTO FPDateFormat VALUES (100000004,'France','FRENCH','MM/DD/YYYY')

--INSERT INTO FPDateFormat VALUES (100000005,'Germany','GERMAN','DD.MM.YYYY')

--INSERT INTO FPDateFormat VALUES (100000006,'Italy','ITALIAN','DD-MM-YYYY')

--INSERT INTO FPDateFormat VALUES (100000007,'Japan','JAPAN','yyyy/mm/dd')

--INSERT INTO FPDateFormat VALUES (100000008,'Taiwan','TAIWAN','yyyy/mm/dd')

--INSERT INTO FPDateFormat VALUES (100000009,'','MDY','MM/dd/yyyy')

--INSERT INTO FPDateFormat VALUES (100000010,'','DMY','dd/MM/yyyy')

--INSERT INTO FPDateFormat VALUES (100000011,'','YMD','yyyy/MM/dd')

-- Database Change Log 30-Aug-2012 --

ALTER TABLE FPAuditTrail ALTER COLUMN CustomerID BIGINT NULL
GO

-- Database Change Log 31-Aug-2012 --

ALTER TABLE FPMigrationLog ADD Status VARCHAR(7) NULL
GO

ALTER TABLE FPMigrationLog ADD Remarks VARCHAR(8000) NULL
GO

-- Database Change Log 03-Sep-2012 --

ALTER TABLE FlightpakSequence ADD OtherCrewDutyLogCurrentNO BIGINT
GO

-- Database Change Log 07-Sep-2012 --

ALTER TABLE FPGeneralException ALTER COLUMN ExceptionDescription VARCHAR(1500)
GO

-- Database Change Log 20-Sep-2012 --

CREATE TABLE FPBusinessName
(
AttributeID INT NOT NULL,
AttributeName VARCHAR(100) NOT NULL,
AttributeDetail VARCHAR(100) NOT NULL
)
GO

ALTER TABLE FPBusinessName ADD CONSTRAINT PKFPBusinessName PRIMARY KEY CLUSTERED(AttributeID ASC)
GO

-- Database Change Log 24-Sep-2012 --

ALTER TABLE FPDateFormat ADD CONSTRAINT UQFormatName UNIQUE(FormatName)
GO

-- Database Change Log 28-Sep-2012 --

ALTER TABLE FPMigrationLog ADD ErrorRecCount INT
GO

UPDATE FPMigrationLog SET ErrorRecCount = ErrorRecordCount
GO

ALTER TABLE FPMigrationLog DROP COLUMN ErrorRecordCount
GO

sp_rename 'FPMigrationLog.ErrorRecCount', 'ErrorRecordCount', 'column'
GO

ALTER TABLE FPMigrationLog ADD PassedRecCount INT
GO

UPDATE FPMigrationLog SET PassedRecCount = PassedRecordCount
GO

ALTER TABLE FPMigrationLog DROP COLUMN PassedRecordCount
GO

sp_rename 'FPMigrationLog.PassedRecCount', 'PassedRecordCount', 'column'
GO

-- Database Change Log 17-Oct-2012 --

ALTER TABLE FPAuditTrail ALTER COLUMN ColumnName VARCHAR(100)
GO

-- Database Change Log 20-Nov-2012 --

ALTER TABLE ExceptionTemplate ALTER COLUMN ExceptionTemplate VARCHAR(1500)
GO

-- SQL Script File 20-Nov-2012 --