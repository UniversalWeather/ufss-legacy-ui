-------------------------------
-- UWA Flightpak Application --
-------------------------------

--------------------------------------
-- Module: R1 All Modules           --
-- Created Date: 03-Jan-2013        --
-- Updated Date: 24-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

ALTER TABLE Catering ALTER COLUMN CateringVendor VARCHAR(40)
GO

ALTER TABLE Transport ALTER COLUMN TransportationVendor VARCHAR(40)
GO

ALTER TABLE Transport ADD IsPassenger BIT
GO

ALTER TABLE Transport ADD IsCrew BIT
GO

ALTER TABLE Airport ADD IsPublic BIT
GO

ALTER TABLE Airport ADD IsPrivate BIT
GO

ALTER TABLE Airport ADD IsMilitary BIT
GO

ALTER TABLE FBO ADD CustomField1 VARCHAR(250)
GO

ALTER TABLE FBO ADD CustomField2 VARCHAR(250)
GO

ALTER TABLE CrewPassengerVisa ADD PassportID BIGINT
GO

ALTER TABLE CrewPassengerVisa ADD CONSTRAINT FKCrewPassengerVisaCrewPassengerPassport FOREIGN KEY(PassportID) REFERENCES CrewPassengerPassport(PassportID)
GO

ALTER TABLE FleetInformation ADD MACHFuel16Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD MACHFuel17Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD MACHFuel18Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD MACHFuel19Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD MACHFuel20Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD LongRangeFuel16Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD LongRangeFuel17Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD LongRangeFuel18Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD LongRangeFuel19Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD LongRangeFuel20Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD HighSpeedFuel16Hrs	NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD HighSpeedFuel17Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD HighSpeedFuel18Hrs NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD HighSpeedFuel19Hrs	NUMERIC(5,5)
GO

ALTER TABLE FleetInformation ADD HighSpeedFuel20Hrs	NUMERIC(5,5)
GO

ALTER TABLE FlightCatagory ADD IsDeadorFerryHead BIT
GO

ALTER TABLE FuelLocator ADD IsInActive BIT
GO
 
ALTER TABLE Passenger ADD INSANumber VARCHAR(60)
GO

ALTER TABLE Passenger ADD Category VARCHAR(30)
GO

ALTER TABLE Passenger ADD GreenCardGender CHAR(1)
GO

ALTER TABLE Passenger ADD GreenCardCountryID BIGINT
GO

ALTER TABLE Passenger ADD CONSTRAINT FKPassengerGreenCardCountry FOREIGN KEY(GreenCardCountryID) REFERENCES Country(CountryID)
GO

ALTER TABLE Passenger ADD CardExpires DATE
GO

ALTER TABLE Passenger ADD ResidentSinceYear INT
GO

ALTER TABLE Vendor ADD WebsiteAddress VARCHAR(250)
GO

ALTER TABLE Vendor ADD CertificateNumber VARCHAR(50)
GO

ALTER TABLE Vendor ADD AuditingCompany CHAR(3)
GO

ALTER TABLE Company ADD CrewChecklistAlertDays NUMERIC(3)
GO

ALTER TABLE Company ADD BusinessWeekStart NUMERIC(1)
GO

ALTER TABLE DelayType ALTER COLUMN DelayTypeDescription VARCHAR(100)
GO

ALTER TABLE Passenger ADD PassengerWeight NUMERIC(5,2)
GO

ALTER TABLE Aircraft ADD IsInActive BIT
GO

ALTER TABLE AircraftDuty ADD IsInActive BIT
GO

ALTER TABLE BatchJobLog ADD IsInActive BIT
GO

ALTER TABLE Budget ADD IsInActive BIT
GO

ALTER TABLE Client ADD IsInActive BIT
GO

ALTER TABLE ClientCodeMapping ADD IsInActive BIT
GO

ALTER TABLE ConversionTable ADD IsInActive BIT
GO

ALTER TABLE Crew ADD IsInActive BIT
GO

ALTER TABLE CrewAircraftAssigned ADD IsInActive BIT
GO

ALTER TABLE CrewCheckList ADD IsInActive BIT
GO

ALTER TABLE CrewDefinition ADD IsInActive BIT
GO

ALTER TABLE CrewDutyRules ADD IsInActive BIT
GO

ALTER TABLE CrewDutyType ADD IsInActive BIT
GO

ALTER TABLE CrewDutyTypeXRef ADD IsInActive BIT
GO

ALTER TABLE CrewGroup ADD IsInActive BIT
GO

ALTER TABLE CrewGroupOrder ADD IsInActive BIT
GO

ALTER TABLE CrewInformation ADD IsInActive BIT
GO

ALTER TABLE CrewPassengerPassport ADD IsInActive BIT
GO

ALTER TABLE CrewPassengerVisa ADD IsInActive BIT
GO

ALTER TABLE CustomAddress ADD IsInActive BIT
GO

ALTER TABLE Customer ADD IsInActive BIT
GO

ALTER TABLE CustomerModuleAccess ADD IsInActive BIT
GO

ALTER TABLE DBGeneralInfo ADD IsInActive BIT
GO

ALTER TABLE DelayType ADD IsInActive BIT
GO

ALTER TABLE DepartmentGroup ADD IsInActive BIT
GO

ALTER TABLE DepartmentGroupOrder ADD IsInActive BIT
GO

ALTER TABLE DSTRegion ADD IsInActive BIT
GO

ALTER TABLE EmergencyContact ADD IsInActive BIT
GO

ALTER TABLE ExceptionTemplate ADD IsInActive BIT
GO

ALTER TABLE ExchangeRate ADD IsInActive BIT
GO

ALTER TABLE FareLevel ADD IsInActive BIT
GO

ALTER TABLE FileWarehouse ADD IsInActive BIT
GO

ALTER TABLE FleetChargeRate ADD IsInActive BIT
GO

ALTER TABLE FleetCharterRate ADD IsInActive BIT
GO

ALTER TABLE FleetComponent ADD IsInActive BIT
GO

ALTER TABLE FleetForeCast ADD IsInActive BIT
GO

ALTER TABLE FleetGroup ADD IsInActive BIT
GO

ALTER TABLE FleetGroupOrder ADD IsInActive BIT
GO

ALTER TABLE FleetInformation ADD IsInActive BIT
GO

ALTER TABLE FleetNewCharterRate ADD IsInActive BIT
GO

ALTER TABLE FleetPair ADD IsInActive BIT
GO

ALTER TABLE FleetProfileDefinition ADD IsInActive BIT
GO

ALTER TABLE FleetProfileInformation ADD IsInActive BIT
GO

ALTER TABLE FlightpakSequence ADD IsInActive BIT
GO

ALTER TABLE FlightPurpose ADD IsInActive BIT
GO

ALTER TABLE FPAuditTrail ADD IsInActive BIT
GO

ALTER TABLE FPBlockTran ADD IsInActive BIT
GO

ALTER TABLE FPBookmark ADD IsInActive BIT
GO

ALTER TABLE FPDateFormat ADD IsInActive BIT
GO

ALTER TABLE FPEmailTmpl ADD IsInActive BIT
GO

ALTER TABLE FPEmailTranLog ADD IsInActive BIT
GO

ALTER TABLE FPExclTab ADD IsInActive BIT
GO

ALTER TABLE FPGeneralException ADD IsInActive BIT
GO

ALTER TABLE FPLockLog ADD IsInActive BIT
GO

ALTER TABLE FPLoginSession ADD IsInActive BIT
GO

ALTER TABLE FPMigrationLog ADD IsInActive BIT
GO

ALTER TABLE FPSystemPref ADD IsInActive BIT
GO

ALTER TABLE FuelVendor ADD IsInActive BIT
GO

ALTER TABLE Metro ADD IsInActive BIT
GO

ALTER TABLE Module ADD IsInActive BIT
GO

ALTER TABLE PassengerAdditionalInfo ADD IsInActive BIT
GO

ALTER TABLE PassengerGroup ADD IsInActive BIT
GO

ALTER TABLE PassengerGroupOrder ADD IsInActive BIT
GO

ALTER TABLE PassengerInformation ADD IsInActive BIT
GO

ALTER TABLE PaymentType ADD IsInActive BIT
GO

ALTER TABLE RoomType ADD IsInActive BIT
GO

ALTER TABLE Runway ADD IsInActive BIT
GO

ALTER TABLE TailHistory ADD IsInActive BIT
GO

ALTER TABLE TravelCoordinator ADD IsInActive BIT
GO

ALTER TABLE TravelCoordinatorFleet ADD IsInActive BIT
GO

ALTER TABLE TravelCoordinatorRequestor ADD IsInActive BIT
GO

ALTER TABLE TripManagerCheckList ADD IsInActive BIT
GO

ALTER TABLE TripManagerCheckListGroup ADD IsInActive BIT
GO

ALTER TABLE TSAClear ADD IsInActive BIT
GO

ALTER TABLE TSANoFly ADD IsInActive BIT
GO

ALTER TABLE TSASelect ADD IsInActive BIT
GO

ALTER TABLE TSDefinitionLog ADD IsInActive BIT
GO

ALTER TABLE TSFlightLog ADD IsInActive BIT
GO

ALTER TABLE UMPermission ADD IsInActive BIT
GO

ALTER TABLE UserGroup ADD IsInActive BIT
GO

ALTER TABLE UserGroupMapping ADD IsInActive BIT
GO

ALTER TABLE UserGroupPermission ADD IsInActive BIT
GO

ALTER TABLE UserPassword ADD IsInActive BIT
GO

ALTER TABLE UserSecurityHint ADD IsInActive BIT
GO

ALTER TABLE VendorContact ADD IsInActive BIT
GO

CREATE TABLE CrewHistory
(
CrewHistoryID BIGINT,
CrewID BIGINT,
CustomerID BIGINT,
HistoryDescription NVARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
RevisionNumber INT,
CONSTRAINT PKCrewHistory PRIMARY KEY CLUSTERED(CrewHistoryID ASC),
CONSTRAINT FKCrewHistoryCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetHistory
(
FleetHistoryID BIGINT,
FleetID BIGINT,
CustomerID BIGINT,
HistoryDescription NVARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
RevisionNumber INT,
CONSTRAINT PKFleetHistory PRIMARY KEY CLUSTERED(FleetHistoryID ASC),
CONSTRAINT FKFleetHistoryFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 08-Jan-2013 --

ALTER TABLE Hotel ADD ExchangeRateID BIGINT
GO

ALTER TABLE Hotel ADD CONSTRAINT FKHotelExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE Catering ADD ExchangeRateID	BIGINT
GO

ALTER TABLE Catering ADD CONSTRAINT FKCateringExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE Transport ADD ExchangeRateID BIGINT
GO

ALTER TABLE Transport ADD CONSTRAINT FKTransportExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE FBO ADD	ExchangeRateID BIGINT
GO

ALTER TABLE FBO ADD CONSTRAINT FKFBOExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE Airport ADD ExchangeRateID BIGINT
GO

ALTER TABLE Airport ADD CONSTRAINT FKAirportExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE Hotel ADD NegotiatedTerms VARCHAR(800)
GO

ALTER TABLE Catering ADD NegotiatedTerms VARCHAR(800)
GO

ALTER TABLE Transport ADD NegotiatedTerms VARCHAR(800)
GO

ALTER TABLE FBO ADD NegotiatedTerms VARCHAR(800)
GO

ALTER TABLE Hotel ADD SundayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD MondayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD TuesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD WednesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD ThursdayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD FridayWorkHours VARCHAR(15)
GO

ALTER TABLE Hotel ADD SaturdayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD SundayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD MondayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD TuesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD WednesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD ThursdayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD FridayWorkHours VARCHAR(15)
GO

ALTER TABLE Catering ADD SaturdayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD SundayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD MondayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD TuesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD WednesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD ThursdayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD FridayWorkHours VARCHAR(15)
GO

ALTER TABLE Transport ADD SaturdayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD SundayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD MondayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD TuesdayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD WednesdayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD ThursdayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD FridayWorkHours VARCHAR(15)
GO

ALTER TABLE FBO ADD SaturdayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport ADD CustomsSundayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport ADD CustomsMondayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport ADD CustomsTuesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport ADD CustomsWednesdayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport	ADD CustomsThursdayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport ADD CustomsFridayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport	ADD CustomsSaturdayWorkHours VARCHAR(15)
GO

ALTER TABLE Airport	ADD CustomNotes	VARCHAR(800)
GO

ALTER TABLE Passenger ADD PassengerWeightUnit CHAR(3)
GO

ALTER TABLE Passenger ADD Birthday DATE
GO

ALTER TABLE Passenger ADD Anniversaries	DATE
GO

ALTER TABLE Passenger ADD EmergencyContacts	VARCHAR(100)
GO

ALTER TABLE Passenger ADD CreditcardInfo VARCHAR(100)
GO

ALTER TABLE Passenger ADD CateringPreferences VARCHAR(100)
GO

ALTER TABLE Passenger ADD HotelPreferences VARCHAR(100)
GO

ALTER TABLE Crew ADD Birthday DATE
GO

ALTER TABLE Crew ADD Anniversaries DATE
GO

ALTER TABLE Crew ADD EmergencyContacts VARCHAR(100)
GO

ALTER TABLE Crew ADD CreditcardInfo VARCHAR(100)
GO

ALTER TABLE Crew ADD CateringPreferences VARCHAR(100)
GO

ALTER TABLE Crew ADD HotelPreferences VARCHAR(100)
GO

ALTER TABLE PreFlightMain ADD FleetCalendarNotes VARCHAR(MAX)
GO

ALTER TABLE PreFlightMain ADD CrewCalendarNotes VARCHAR(MAX)
GO

ALTER TABLE PreflightFBOList ADD IsRequired BIT
GO

ALTER TABLE PreflightFBOList ADD IsNotRequired BIT
GO

ALTER TABLE PreflightFBOList ADD IsInProgress BIT
GO

ALTER TABLE PreflightFBOList ADD IsChange BIT
GO

ALTER TABLE PreflightFBOList ADD IsCancelled BIT
GO

ALTER TABLE Company ADD ExchangeRateID BIGINT
GO

ALTER TABLE Company ADD CONSTRAINT FKCompanyExchangeRate FOREIGN KEY(ExchangeRateID) REFERENCES ExchangeRate(ExchangeRateID)
GO

ALTER TABLE PreflightCrewHotelList ADD Address1	VARCHAR(40)
GO

ALTER TABLE PreflightCrewHotelList ADD Address2	VARCHAR(40)
GO

ALTER TABLE PreflightCrewHotelList ADD Address3	VARCHAR(40)
GO

ALTER TABLE PreflightCrewHotelList ADD MetroID BIGINT
GO

ALTER TABLE PreflightCrewHotelList ADD CONSTRAINT FKPreflightCrewHotelListMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID)
GO

ALTER TABLE PreflightCrewHotelList ADD City VARCHAR(30)
GO

ALTER TABLE PreflightCrewHotelList ADD StateProvince VARCHAR(25)
GO

ALTER TABLE PreflightCrewHotelList ADD CountryID BIGINT
GO

ALTER TABLE PreflightCrewHotelList ADD CONSTRAINT FKPreflightCrewHotelListCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
GO

ALTER TABLE PreflightCrewHotelList ADD PostalCode VARCHAR(15)
GO

ALTER TABLE PreflightPassengerHotelList ADD Address1 VARCHAR(40)
GO

ALTER TABLE PreflightPassengerHotelList ADD Address2 VARCHAR(40)
GO

ALTER TABLE PreflightPassengerHotelList ADD Address3 VARCHAR(40)
GO

ALTER TABLE PreflightPassengerHotelList ADD MetroID BIGINT
GO

ALTER TABLE PreflightPassengerHotelList ADD CONSTRAINT FKPreflightPassengerHotelListMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID)
GO

ALTER TABLE PreflightPassengerHotelList ADD City VARCHAR(30)
GO

ALTER TABLE PreflightPassengerHotelList ADD StateProvince	VARCHAR(25)
GO

ALTER TABLE PreflightPassengerHotelList ADD CountryID BIGINT
GO

ALTER TABLE PreflightPassengerHotelList ADD CONSTRAINT FKPreflightPassengerHotelListCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
GO

ALTER TABLE PreflightPassengerHotelList ADD PostalCode VARCHAR(15)
GO

ALTER TABLE PreflightFBOList ALTER COLUMN FBOInformation VARCHAR(1000)
GO

ALTER TABLE PreflightCateringList ADD ContactName VARCHAR(25)
GO

ALTER TABLE PreflightFuel ADD FuelUnits NUMERIC(1)
GO

ALTER TABLE UWAL ADD IsCustoms BIT
GO

ALTER TABLE UWAL ADD IsPaymentCreditForServices	BIT
GO

ALTER TABLE PostflightCrew ADD OrderNUM INT
GO

ALTER TABLE Fleet ALTER COLUMN AircraftCD CHAR(4)
GO

ALTER TABLE PreflightCateringDetail ADD CateringContactName VARCHAR(40)
GO

-- Database Change Log 22-Jan-2013 --

ALTER TABLE PreflightFuel DROP COLUMN FuelUnits
GO

ALTER TABLE PreflightMain ADD FuelUnits NUMERIC(1)
GO

ALTER TABLE PreflightLeg ADD LastUpdTSCrewNotes DATETIME
GO

ALTER TABLE PreflightLeg ADD LastUpdTSPaxNotes DATETIME
GO

-- Database Change Log 24-Jan-2013 --

ALTER TABLE Passenger DROP CONSTRAINT FKPassengerGreenCardCountry
GO

ALTER TABLE Passenger DROP COLUMN INSANumber
GO
ALTER TABLE Passenger DROP COLUMN Category
GO
ALTER TABLE Passenger DROP COLUMN GreenCardCountryID
GO
ALTER TABLE Passenger DROP COLUMN CardExpires
GO
ALTER TABLE Passenger DROP COLUMN ResidentSinceYear
GO
ALTER TABLE Passenger DROP COLUMN GreenCardGender
GO

ALTER TABLE Passenger ADD INSANumber VARCHAR(60)
GO
ALTER TABLE Passenger ADD Category VARCHAR(30)
GO
ALTER TABLE Passenger ADD GreenCardCountryID BIGINT
GO
ALTER TABLE Passenger ADD CardExpires DATE
GO
ALTER TABLE Passenger ADD ResidentSinceYear INT
GO
ALTER TABLE Passenger ADD GreenCardGender CHAR(1)
GO

ALTER TABLE Passenger ADD CONSTRAINT FKPassengerGreenCardCountry FOREIGN KEY(GreenCardCountryID) REFERENCES Country(CountryID)
GO
GO

ALTER TABLE FleetInformation ALTER COLUMN MACHFuel16Hrs	NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN MACHFuel17Hrs	NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN MACHFuel18Hrs	NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN MACHFuel19Hrs	NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN MACHFuel20Hrs	NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN LongRangeFuel16Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN LongRangeFuel17Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN LongRangeFuel18Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN LongRangeFuel19Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN LongRangeFuel20Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN HighSpeedFuel16Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN HighSpeedFuel17Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN HighSpeedFuel18Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN HighSpeedFuel19Hrs NUMERIC(5,0)
GO
ALTER TABLE FleetInformation ALTER COLUMN HighSpeedFuel20Hrs NUMERIC(5,0)
GO

ALTER TABLE Customer DROP CONSTRAINT DF_Customer_IsCustomerLock
GO

ALTER TABLE Customer DROP COLUMN IsInActive
GO
ALTER TABLE Customer DROP COLUMN IsCustomerLock
GO

ALTER TABLE Customer ADD IsInActive BIT
GO
ALTER TABLE Customer ADD IsCustomerLock BIT NOT NULL CONSTRAINT DF_Customer_IsCustomerLock DEFAULT ((0))
GO

ALTER TABLE AirportPairDetail ALTER COLUMN Cost NUMERIC(12,2)
GO

-- SQL Script File 24-Jan-2013 --