-------------------------- UE-204 Ticket Number -----------------------------
--- Used in SP spFlightPak_GetCrewDefinition  - CrewID, CustomerID, IsDeleted
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewDefinition_CrewID_CustomerID_IsDeleted_IDX' AND object_id = OBJECT_ID('CrewDefinition'))
    BEGIN
        DROP INDEX [CrewDefinition_CrewID_CustomerID_IsDeleted_IDX] ON [dbo].[CrewDefinition]
    END
GO

CREATE NONCLUSTERED INDEX [CrewDefinition_CrewID_CustomerID_IsDeleted_IDX] ON [dbo].[CrewDefinition]
(
	[CrewID] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO