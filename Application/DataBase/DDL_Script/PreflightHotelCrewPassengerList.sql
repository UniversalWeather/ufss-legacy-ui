--select * from PreflightHotelCrewPassengerList

CREATE TABLE [dbo].[PreflightHotelCrewPassengerList](
	PreflightHotelCrewPassengerListID [bigint] NOT NULL,
	[PreflightHotelListID] [bigint],
	[CrewID] [bigint],
	[PassengerRequestorID] [Bigint],
	[CustomerID]  bigint,
    [LastUpdUID]  varchar(30),
    [LastUpdTS]  datetime
	
	CONSTRAINT [PKPreflightHotelCrewPassengerList] PRIMARY KEY CLUSTERED 
(
	[PreflightHotelCrewPassengerListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
	
	ALTER TABLE [dbo].[PreflightHotelCrewPassengerList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelCrewPassengerListPreflightHotelList] FOREIGN KEY([PreflightHotelListID])
REFERENCES [dbo].[PreflightHotelList] ([PreflightHotelListID])
GO

ALTER TABLE [dbo].[PreflightHotelCrewPassengerList] CHECK CONSTRAINT [FKPreflightHotelCrewPassengerListPreflightHotelList]
GO


ALTER TABLE [dbo].[PreflightHotelCrewPassengerList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelCrewPassengerListCrew] FOREIGN KEY([CrewID])
REFERENCES [dbo].[Crew] ([CrewID])
GO

ALTER TABLE [dbo].[PreflightHotelCrewPassengerList] CHECK CONSTRAINT [FKPreflightHotelCrewPassengerListCrew]
GO

ALTER TABLE [dbo].[PreflightHotelCrewPassengerList]  WITH CHECK ADD  CONSTRAINT [FKPreflightHotelCrewPassengerListPassenger] FOREIGN KEY([PassengerRequestorID])
REFERENCES [dbo].[Passenger] ([PassengerRequestorID])
GO

ALTER TABLE [dbo].[PreflightHotelCrewPassengerList] CHECK CONSTRAINT [FKPreflightHotelCrewPassengerListPassenger]
GO