-------------------------------
-- UWA Flightpak Application --
-------------------------------

--------------------------------------
-- Module: Utilities Module         --
-- Created Date: 30-Sep-2012        --
-- Updated Date: 23-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE AirportPair
(
AirportPairID BIGINT,
CustomerID BIGINT,
AirportPairNumber INT,
AircraftID BIGINT,
WindQTR NUMERIC(1),
AirportPairDescription VARCHAR(40),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKAirportPair PRIMARY KEY CLUSTERED(AirportPairID ASC),
CONSTRAINT FKAirportPairCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAirportPairUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKAirportPairAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID)
)
GO

CREATE TABLE AirportPairDetail
(
AirportPairDetailID BIGINT,
CustomerID BIGINT,
AirportPairID BIGINT,
LegID INT,
AircraftID BIGINT,
QTR NUMERIC(1),
DAirportID BIGINT,
AAirportID BIGINT,
Distance NUMERIC(5),
PowerSetting CHAR(1),
TakeoffBIAS NUMERIC(6,3),
LandingBIAS NUMERIC(6,3),
TrueAirSpeed NUMERIC(3),
Winds NUMERIC(5),
ElapseTM NUMERIC(7,3),
Cost NUMERIC(9,2),
WindReliability INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKAirportPairDetail PRIMARY KEY CLUSTERED(AirportPairDetailID ASC),
CONSTRAINT FKAirportPairDetailAirportPair FOREIGN KEY(AirportPairID) REFERENCES AirportPair(AirportPairID),
CONSTRAINT FKAirportPairDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAirportPairDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKAirportPairDetailAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKAirportPairDetailDAirport FOREIGN KEY(DAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKAirportPairDetailAAirport FOREIGN KEY(AAirportID) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE ItineraryPlan
(
ItineraryPlanID BIGINT,
CustomerID BIGINT,
IntinearyNUM INT,
ItineraryPlanQuarter INT,
ItineraryPlanQuarterYear CHAR(4),
AircraftID BIGINT,
FleetID BIGINT,
ItineraryPlanDescription VARCHAR(40),
ClientID BIGINT,
HomebaseID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKItineraryPlan PRIMARY KEY CLUSTERED(ItineraryPlanID ASC),
CONSTRAINT FKItineraryPlanCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKItineraryPlanUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKItineraryPlanAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKItineraryPlanFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKItineraryPlanClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKItineraryPlanCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID)
)
GO

CREATE TABLE ItineraryPlanDetail
(
ItineraryPlanDetailID BIGINT,
CustomerID BIGINT,
ItineraryPlanID BIGINT,
IntinearyNUM INT,
LegNUM INT,
DAirportID BIGINT,
AAirportID BIGINT,
Distance NUMERIC(5),
PowerSetting CHAR(1),
TakeoffBIAS NUMERIC(6,3),
LandingBias NUMERIC(6,3),
TrueAirSpeed NUMERIC(3),
WindsBoeingTable NUMERIC(5),
ElapseTM NUMERIC(7,3),
DepartureDTTMLocal DATETIME,
DepartureGMT DATETIME,
HomeDepartureTM DATETIME,
ArrivalLocal DATETIME,
ArrivalTMGMT DATETIME,
HomeArrivalTM DATETIME,
IsDepartureConfirmed BIT,
IsArrivalConfirmation BIT,
PassengerNUM NUMERIC(3),
Cost NUMERIC(12,2),
WindReliability INT,
LegID INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKItineraryPlanDetail PRIMARY KEY CLUSTERED(ItineraryPlanDetailID ASC),
CONSTRAINT FKItineraryPlanDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKItineraryPlanDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKItineraryPlanDetailItineraryPlan FOREIGN KEY(ItineraryPlanID) REFERENCES ItineraryPlan(ItineraryPlanID),
CONSTRAINT FKItineraryPlanDetailDAirport FOREIGN KEY(DAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKItineraryPlanDetailAAirport FOREIGN KEY(AAirportID) REFERENCES Airport(AirportID)
)
GO

ALTER TABLE Customer ADD IsCustomerLock BIT
GO

-- Database Change Log 23-Jan-2013 --

sp_rename 'ItineraryPlanDetail.DepartureDTTMLocal','DepartureLocal','COLUMN'
GO

sp_rename 'ItineraryPlanDetail.HomeDepartureTM','DepartureHome','COLUMN'
GO

sp_rename 'ItineraryPlanDetail.ArrivalTMGMT','ArrivalGMT','COLUMN'
GO

sp_rename 'ItineraryPlanDetail.HomeArrivalTM','ArrivalHome','COLUMN'
GO

-- SQL Script File 23-Jan-2013 --