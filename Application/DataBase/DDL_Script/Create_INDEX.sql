create nonclustered index AccountISDELIDX on Account
(

CustomerID,
IsDeleted

)
create nonclustered index AccountNUMIDX on Account
(
AccountNUM,
CustomerID
)

create nonclustered index AccountHOMEIDX on Account
(
Homebaseid,
CustomerID
)



create nonclustered index DepartmentISDELIDX on Department
(

IsDeleted,
CustomerID

)
create nonclustered index DepartmentCDIDX on Department
(
DepartmentCD,
CustomerID
)

create nonclustered index DepartmentCLIENTIDX on Department
(
ClientID,
CustomerID
)


create nonclustered index DepartmentAuthorizationISDELIDX on DepartmentAuthorization
(
IsDeleted,
CustomerID
)

create nonclustered index DepartmentAuthorizationCDIDX on DepartmentAuthorization
(
authorizationcd,
CustomerID
)

create nonclustered index DepartmentAuthorizationCLIENTIDX on DepartmentAuthorization
(
ClientID,
CustomerID
)


create nonclustered index ClientISDELIDX on Client
(
IsDeleted,
CustomerID
)


create nonclustered index ClientCDIDX on Client
(
ClientCD,
CustomerID
)


create nonclustered index FleetISDELIDX on Fleet
(
IsDeleted,
CustomerID
)


create nonclustered index FleetNUMIDX on Fleet
(
Tailnum,
CustomerID
)

create nonclustered index CompanyISDELIDX on Company
(
IsDeleted,
CustomerID
)

create nonclustered index AircraftISDELIDX on Aircraft
(
IsDeleted,
CustomerID
)


create nonclustered index AircraftCDIDX on Aircraft
(
AircraftCD,
CustomerID
)

create nonclustered index EmergencyContactISDELIDX on EmergencyContact
(
IsDeleted,
CustomerID
)


create nonclustered index EmergencyContactCDIDX on EmergencyContact
(
EmergencyContactCD,
CustomerID
)

create nonclustered index CrewISDELIDX on Crew
(
IsDeleted,
CustomerID
)


create nonclustered index CrewCDIDX on Crew
(
CrewCD,
CustomerID
)

create nonclustered index PassengerISDELIDX on Passenger
(
IsDeleted,
CustomerID
)


create nonclustered index PassengerCDIDX on Passenger
(
PassengerRequestorCD,
CustomerID
)

create nonclustered index UserMasterISDELIDX on UserMaster
(
IsDeleted,
CustomerID
)


create nonclustered index UserMasterISDISPIDX on UserMaster
(
isDispatcher,
CustomerID
)