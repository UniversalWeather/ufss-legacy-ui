

-- Column Modified for Fixing Error Converting Int to Numeric Issue, While Saving a Log.
-- Existing App allowing 4 precession values for DaysAway Column.

ALTER TABLE PostflightCrew ALTER COLUMN DaysAway NUMERIC(5,0)
GO