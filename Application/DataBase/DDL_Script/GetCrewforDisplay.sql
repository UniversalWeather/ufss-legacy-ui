  
Create procedure GetCrewforDisplay(@CustomerID bigint,  
@CrewCD varchar(8),  
@CrewID Bigint  
)            
-- =============================================          
-- Author: Karthikeyan.S
-- Create date: 04/03/2014
-- Description: Get all the Crew List
-- =============================================          
as          
SET NoCOUNT ON          
BEGIN          
SELECT   Crew.IsCrewDisplay
FROM Crew as Crew with(nolock)           
WHERE  
CrewID = case when @CrewID <>0 then @CrewID else CrewID end
order by Crew.CrewCD          
END          