/****** Object:  Index [CrewGroupOrder_Crew]    Script Date: 11/11/2014 15:35:59 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CrewGroupOrder]') AND name = N'CrewGroupOrder_Crew')
DROP INDEX [CrewGroupOrder_Crew] ON [dbo].[CrewGroupOrder] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [CrewGroupOrder_Crew]    Script Date: 11/11/2014 15:36:13 ******/
CREATE NONCLUSTERED INDEX [CrewGroupOrder_Crew] ON [dbo].[CrewGroupOrder] 
(
	[CrewGroupID] ASC,
	[CrewID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [CrewGroupOrder_CrewID]    Script Date: 11/11/2014 15:35:21 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CrewGroupOrder]') AND name = N'CrewGroupOrder_CrewID')
DROP INDEX [CrewGroupOrder_CrewID] ON [dbo].[CrewGroupOrder] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [CrewGroupOrder_CrewID]    Script Date: 11/11/2014 15:35:32 ******/
CREATE NONCLUSTERED INDEX [CrewGroupOrder_CrewID] ON [dbo].[CrewGroupOrder] 
(
	[CrewID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO