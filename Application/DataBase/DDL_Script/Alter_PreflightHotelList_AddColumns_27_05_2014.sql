
ALTER TABLE preflighthotellist ADD [NoofRooms] [numeric](2, 0) NULL
ALTER TABLE preflighthotellist ADD [ClubCard] [varchar](100) NULL
ALTER TABLE preflighthotellist ADD [IsRateCap] [bit] NULL
ALTER TABLE preflighthotellist ADD [MaxCapAmount] [numeric](17, 2) NULL

ALTER TABLE preflighthotellist ADD [IsRequestOnly] [bit] NULL
ALTER TABLE preflighthotellist ADD [IsBookNight] [bit] NULL
GO