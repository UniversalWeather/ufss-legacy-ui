-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Charter Quote Module     --
-- Created Date: 04-Feb-2013        --
-- Updated Date: 06-Feb-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

-- Fee Schedule map with Account Entity --

ALTER TABLE FeeSchedule ADD CONSTRAINT FKFeeScheduleAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID)
GO

-- Add TripID and map with PreflightMain Entity --

ALTER TABLE CQMain ADD TripID BIGINT
GO

ALTER TABLE CQMain ADD CONSTRAINT FKCQMainPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID)
GO

-- Drop TripNum from CQMain Entity --

ALTER TABLE CQMain DROP COLUMN TripNUM
GO

-- CQMain map with Client Entity --

ALTER TABLE CQMain ADD CONSTRAINT FKCQMainClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
GO

-- CQLeg map with FBO Entity --

ALTER TABLE CQLeg ADD CONSTRAINT FKCQLegFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID)
GO

-- CQLeg map with Client Entity --

ALTER TABLE CQLeg ADD CONSTRAINT FKCQLegClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
GO

---- CQReportHeader map with Client Entity --

--ALTER TABLE CQReportHeader ADD CONSTRAINT FKCQReportHeaderClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
--GO

-- ALTER CQQuoteSummary into CQMain Entity --

ALTER TABLE CQMain ADD IsPrintUsageAdj BIT
GO

ALTER TABLE CQMain ADD UsageAdjDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomUsageAdj NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintAdditonalFees BIT
GO

ALTER TABLE CQMain ADD AdditionalFeeDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD AdditionalFeesTax NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD PositioningDescriptionINTL NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsAdditionalFees NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD CustomAdditionalFees NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintLandingFees BIT
GO

ALTER TABLE CQMain ADD LandingFeeDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomLandingFee NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintSegmentFee BIT
GO

ALTER TABLE CQMain ADD SegmentFeeDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomSegmentFee NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintStdCrew BIT
GO

ALTER TABLE CQMain ADD StdCrewROMDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomStdCrewRON NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintAdditionalCrew BIT
GO

ALTER TABLE CQMain ADD AdditionalCrewDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomAdditionalCrew NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsAdditionalCrewRON BIT
GO

ALTER TABLE CQMain ADD AdditionalDescriptionCrewRON VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomAdditionalCrewRON NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintWaitingChg BIT
GO

ALTER TABLE CQMain ADD WaitChgDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomWaitingChg NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintFlightChg BIT
GO

ALTER TABLE CQMain ADD FlightChgDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD CustomFlightChg NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintSubtotal BIT
GO

ALTER TABLE CQMain ADD SubtotalDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD IsPrintDiscountAmt BIT
GO

ALTER TABLE CQMain ADD DescriptionDiscountAmt VARCHAR(60)
GO

ALTER TABLE CQMain ADD IsPrintTaxes BIT
GO

ALTER TABLE CQMain ADD TaxesDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD IsPrintInvoice BIT
GO

ALTER TABLE CQMain ADD InvoiceDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD ReportQuote NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsPrintPay BIT
GO

ALTER TABLE CQMain ADD PrepayDescription VARCHAR(60)
GO

ALTER TABLE CQMain ADD IsPrintRemaingAMT BIT
GO

ALTER TABLE CQMain ADD RemainingAmtDescription VARCHAR(60)
GO

-- Delete CQQuoteSummary Entity --

DROP TABLE CQQuoteSummary
GO

-- Rename CQLeg.AirportAlertCD into CrewDutyAlert --

sp_rename 'CQLeg.AirportAlertCD','CrewDutyAlert','COLUMN'
GO

-- Rename CQLeg.DayROBCNT into DayRONCNT --

sp_rename 'CQLeg.DayROBCNT','DayRONCNT','COLUMN'
GO

-- Rename CQMain.IsTotalQuote into IsOverrideTotalQuote --

sp_rename 'CQMain.IsTotalQuote','IsOverrideTotalQuote','COLUMN'
GO

-- Add FileWarehouseID Information in CQQuoteMain Entity --

ALTER TABLE CQQuoteMain ADD FileWarehouseID BIGINT
GO

ALTER TABLE CQQuoteMain ADD CONSTRAINT FKCQQuoteMainFileWarehouse FOREIGN KEY(FileWarehouseID) REFERENCES FileWarehouse(FileWarehouseID)
GO

-- Add QuoteInformation in CQQuoteMain Entity --

ALTER TABLE CQQuoteMain ADD QuoteInformation VARCHAR(MAX)
GO

-- Delete QuoteInformation from CQMain Entity --

ALTER TABLE CQMain DROP COLUMN QuoteInformation
GO

-- Rename IsReservationAvailable into ReservationAvailable in CQLeg Entity --

sp_rename 'CQLeg.IsReservationAvailable','ReservationAvailable','COLUMN'
GO

-- Add PassportID in CQPassenger Entity --

ALTER TABLE CQPassenger ADD PassportID BIGINT
GO

ALTER TABLE CQPassenger ADD CONSTRAINT FKCQPassengerCrewPassengerPassport FOREIGN KEY(PassportID) REFERENCES CrewPassengerPassport(PassportID)
GO

-- Delete PassportNUM from CQPassenger Entity --

ALTER TABLE CQPassenger DROP COLUMN PassportNUM
GO

-- Add BusinessPhoneNum in CQCustomerContact Entity --

ALTER TABLE CQCustomerContact ADD BusinessPhoneNum VARCHAR(25)
GO

-- Add BusinessFaxNum in CQCustomerContact Entity --

ALTER TABLE CQCustomerContact ADD BusinessFaxNum VARCHAR(25)
GO

-- Add BusinessEmailID in CQCustomerContact Entity --

ALTER TABLE CQCustomerContact ADD BusinessEmailID VARCHAR(100)
GO

-- Add DisplayOrder in CQException Entity --

ALTER TABLE CQException ADD DisplayOrder VARCHAR(15)
GO

-- Add PAXBlockedSeat in CQLeg Entity --

ALTER TABLE CQLeg ADD PAXBlockedSeat NUMERIC(4)
GO

-- SQL Script File 06-Feb-2013 --