IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightCateringDetail]') AND name = N'PreflightCateringDetail_LegID_CustomerID_IDX')
DROP INDEX [PreflightCateringDetail_LegID_CustomerID_IDX] ON [dbo].[PreflightCateringDetail]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightFBOList]') AND name = N'PreflightFBOList_LegID_CustomerID_IDX')
DROP INDEX [PreflightFBOList_LegID_CustomerID_IDX] ON [dbo].[PreflightFBOList]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PostflightExpense]') AND name = N'PostflightExpense_SlipNUM_CustomerID_IDX')
DROP INDEX [PostflightExpense_SlipNUM_CustomerID_IDX] ON [dbo].[PostflightExpense]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PostflightPassenger]') AND name = N'PostflightPassenger_POLegID_CustomerID_IDX')
DROP INDEX [PostflightPassenger_POLegID_CustomerID_IDX] ON [dbo].[PostflightPassenger]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQLeg]') AND name = N'CQLeg_CQMainID_CustomerID_IDX')
DROP INDEX [CQLeg_CQMainID_CustomerID_IDX] ON [dbo].[CQLeg]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQCateringList]') AND name = N'CQCateringList_CQLegID_CustomerID_IDX')
DROP INDEX [CQCateringList_CQLegID_CustomerID_IDX] ON [dbo].[CQCateringList]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQFBOList]') AND name = N'CQFBOList_CQLegID_CustomerID_IDX')
DROP INDEX [CQFBOList_CQLegID_CustomerID_IDX] ON [dbo].[CQFBOList]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQTransportList]') AND name = N'CQTransportList_CQLegID_CustomerID_IDX')
DROP INDEX [CQTransportList_CQLegID_CustomerID_IDX] ON [dbo].[CQTransportList]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightCateringDetail]') AND name = N'PreflightCateringDetail_LegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [PreflightCateringDetail_LegID_CustomerID_IDX] ON [dbo].[PreflightCateringDetail]
(
	[LegID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightFBOList]') AND name = N'PreflightFBOList_LegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [PreflightFBOList_LegID_CustomerID_IDX] ON [dbo].[PreflightFBOList]
(
	[LegID] ASC,
	[IsDeleted] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PostflightExpense]') AND name = N'PostflightExpense_SlipNUM_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [PostflightExpense_SlipNUM_CustomerID_IDX] ON [dbo].[PostflightExpense]
(
	[POLegID] ASC,
	[SlipNUM] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PostflightPassenger]') AND name = N'PostflightPassenger_POLegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [PostflightPassenger_POLegID_CustomerID_IDX] ON [dbo].[PostflightPassenger]
(
	[POLegID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQLeg]') AND name = N'CQLeg_CQMainID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [CQLeg_CQMainID_CustomerID_IDX] ON [dbo].[CQLeg]
(
	[CQMainID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQCateringList]') AND name = N'CQCateringList_CQLegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [CQCateringList_CQLegID_CustomerID_IDX] ON [dbo].[CQCateringList]
(
	[CQLegID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQFBOList]') AND name = N'CQFBOList_CQLegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [CQFBOList_CQLegID_CustomerID_IDX] ON [dbo].[CQFBOList]
(
	[CustomerID] ASC,
	[CQLegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CQTransportList]') AND name = N'CQTransportList_CQLegID_CustomerID_IDX')
CREATE NONCLUSTERED INDEX [CQTransportList_CQLegID_CustomerID_IDX] ON [dbo].[CQTransportList]
(
	[CQLegID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO