-------------------------------
-- UWA Flightpak Application --
-------------------------------

--------------------------------------
-- Module: Corporate Request Module --
-- Created Date: 30-Sep-2012        --
-- Updated Date: 03-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE CRMain
(
CRMainID BIGINT,
CustomerID BIGINT,
CRTripNUM INT,
Approver VARCHAR(10),
TripID BIGINT,
TripStatus CHAR(1),
TripSheetStatus CHAR(1),
AcknowledgementStatus CHAR(1),
CharterQuoteStatus CHAR(1),
CorporateRequestStatus CHAR(1),
TravelCoordinatorID BIGINT,
TravelCoordinatorName VARCHAR(25),
TravelCoordinatorPhone VARCHAR(25),
TravelCoordinatorFax VARCHAR(25),
DispatchNUM VARCHAR(12),
EstDepartureDT DATE,
EstArrivalDT DATE,
RecordType CHAR(1),
FleetID BIGINT,
AircraftID BIGINT,
PassengerRequestorID BIGINT,
RequestorName VARCHAR(63),
RequestorPhone VARCHAR(25),
DepartmentID BIGINT,
DepartmentDescription VARCHAR(25),
AuthorizationID BIGINT,
AuthorizationDescription VARCHAR(25),
HomebaseID BIGINT,
ClientID BIGINT,
IsPrivate BIT,
CRMainDescription VARCHAR(40),
RequestDT DATE,
BeginningGMTDT DATE,
EndingGMTDT DATE,
IsCrew BIT,
IsPassenger BIT,
IsAlert BIT,
IsAirportAlert BIT,
IsLog BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRMain PRIMARY KEY CLUSTERED(CRMainID ASC),
CONSTRAINT FKCRMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRMainPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKCRMainTravelCoordinator FOREIGN KEY(TravelCoordinatorID) REFERENCES TravelCoordinator(TravelCoordinatorID),
CONSTRAINT FKCRMainFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKCRMainAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKCRMainPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKCRMainDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKCRMainDepartmentAuthorization FOREIGN KEY(AuthorizationID) REFERENCES DepartmentAuthorization(AuthorizationID),
CONSTRAINT FKCRMainCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCRMainClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKCRMainUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRDispatchNotes
(
CRDispatchNotesID BIGINT,
CustomerID BIGINT,
CRMainID BIGINT,
CRDispatchNotes VARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCRDispatchNotes PRIMARY KEY CLUSTERED(CRDispatchNotesID ASC),
CONSTRAINT FKCRDispatchNotesCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRDispatchNotesCRMain FOREIGN KEY(CRMainID) REFERENCES CRMain(CRMainID),
CONSTRAINT FKCRDispatchNotesUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRHistory
(
CRHistoryID BIGINT,
CustomerID BIGINT,
CRMainID BIGINT,
LogisticsHistory VARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCRHistory PRIMARY KEY CLUSTERED(CRHistoryID ASC),
CONSTRAINT FKCRHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRHistoryCRMain FOREIGN KEY(CRMainID) REFERENCES CRMain(CRMainID),
CONSTRAINT FKCRHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRLeg
(
CRLegID BIGINT,
CustomerID BIGINT,
CRMainID BIGINT,
TripNUM INT,
LegNUM INT,
DAirportID BIGINT,
AAirportID BIGINT,
DepartureCity VARCHAR(25),
ArrivalCity VARCHAR(25),
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
PassengerRequestorID BIGINT,
DepartmentID BIGINT,
AuthorizationID BIGINT,
FlightCategoryID BIGINT,
IsPrivate BIT,
Distance NUMERIC(5),
ElapseTM NUMERIC(5,1),
PassengerTotal INT,
PowerSetting CHAR(1),
TakeoffBIAS NUMERIC(4,1),
LandingBIAS NUMERIC(4,1),
TrueAirSpeed NUMERIC(3),
WindsBoeingTable NUMERIC(5),
IsDepartureConfirmed BIT,
IsArrivalConfirmation BIT,
IsApproxTM BIT,
IsScheduledServices BIT,
DepartureGreenwichDTTM DATETIME,
ArrivalGreenwichDTTM DATETIME,
HomeDepartureDTTM DATETIME,
HomeArrivalDTTM DATETIME,
NegotiateTime CHAR(5),
FBOCD CHAR(4),
LogBreak CHAR(3),
FlightPurpose VARCHAR(40),
SeatTotal INT,
ReservationTotal INT,
ReservationAvailable INT,
WaitNUM INT,
Duty_Type CHAR(2),
DutyHours NUMERIC(5,1),
RestHours NUMERIC(5,1),
FlightHours NUMERIC(5,1),
Notes VARCHAR(MAX),
FuelLoad NUMERIC(6),
OutboundInstruction VARCHAR(MAX),
DutyType NUMERIC(1),
IsCrewDiscount BIT,
CrewCurrency CHAR(4),
IsDutyEnd BIT,
FedAviationRegNUM CHAR(3),
Client CHAR(5),
LegID INT,
WindReliability INT,
CROverRide NUMERIC(4,1),
CheckGroup CHAR(3),
AirportAlertCD CHAR(3),
AdditionalCrew VARCHAR(24),
PilotInCommand CHAR(3),
SecondInCommand CHAR(3),
NextLocalDTTM DATETIME,
NextGMTDTTM DATETIME,
NextHomeDTTM DATETIME,
CrewNotes VARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRLeg PRIMARY KEY CLUSTERED(CRLegID ASC),
CONSTRAINT FKCRLegCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRLegCRMain FOREIGN KEY(CRMainID) REFERENCES CRMain(CRMainID),
CONSTRAINT FKCRLegDAirport FOREIGN KEY(DAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRLegAAirport FOREIGN KEY(AAirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRLegPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKCRLegDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKCRLegDepartmentAuthorization FOREIGN KEY(AuthorizationID) REFERENCES DepartmentAuthorization(AuthorizationID),
CONSTRAINT FKCRLegFlightCatagory FOREIGN KEY(FlightCategoryID) REFERENCES FlightCatagory(FlightCategoryID),
CONSTRAINT FKCRLegUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRPassenger
(
CRPassengerID BIGINT,
CustomerID BIGINT,
CRLegID BIGINT,
TripNUM INT,
LegID INT,
OrderNUM INT,
PassengerRequestorID BIGINT,
PassengerName VARCHAR(63),
FlightPurposeID BIGINT,
PassportID BIGINT,
Billing VARCHAR(25),
IsNonPassenger BIT,
IsBlocked BIT,
ReservationNUM VARCHAR(6),
WaitList CHAR(1),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRPassenger PRIMARY KEY CLUSTERED(CRPassengerID ASC),
CONSTRAINT FKCRPassengerCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRPassengerCRLeg FOREIGN KEY(CRLegID) REFERENCES CRLeg(CRLegID),
CONSTRAINT FKCRPassengerPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKCRPassengerFlightPurpose FOREIGN KEY(FlightPurposeID) REFERENCES FlightPurpose(FlightPurposeID),
CONSTRAINT FKCRPassengerCrewPassengerPassport FOREIGN KEY(PassportID) REFERENCES CrewPassengerPassport(PassportID),
CONSTRAINT FKCRPassengerUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRFBOList
(
CRFBOListID BIGINT,
CustomerID BIGINT,
CRLegID BIGINT,
TripNUM INT,
LegID INT,
RecordType CHAR(2),
FBOID BIGINT,
CRFBOListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
IsCompleted BIT,
AirportID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRFBOList PRIMARY KEY CLUSTERED(CRFBOListID ASC),
CONSTRAINT FKCRFBOListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRFBOListCRLeg FOREIGN KEY(CRLegID) REFERENCES CRLeg(CRLegID),
CONSTRAINT FKCRFBOListFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID),
CONSTRAINT FKCRFBOListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRFBOListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRTransportList
(
CRTransportListID BIGINT,
CustomerID BIGINT,
CRLegID BIGINT,
TripNUM INT,
LegID INT,
RecordType CHAR(2),
TransportID BIGINT,
CRTransportListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(6,2),
IsCompleted BIT,
AirportID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRTransportList PRIMARY KEY CLUSTERED(CRTransportListID ASC),
CONSTRAINT FKCRTransportListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRTransportListCRLeg FOREIGN KEY(CRLegID) REFERENCES CRLeg(CRLegID),
CONSTRAINT FKCRTransportListTransport FOREIGN KEY(TransportID) REFERENCES Transport(TransportID),
CONSTRAINT FKCRTransportListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRTransportListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRHotelList
(
CRHotelListID BIGINT,
CustomerID BIGINT,
CRLegID BIGINT,
TripNUM INT,
LegID INT,
RecordType CHAR(2),
HotelID BIGINT,
CRHotelListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(6,2),
IsCompleted BIT,
AirportID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRHotelList PRIMARY KEY CLUSTERED(CRHotelListID ASC),
CONSTRAINT FKCRHotelListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRHotelListCRLeg FOREIGN KEY(CRLegID) REFERENCES CRLeg(CRLegID),
CONSTRAINT FKCRHotelListHotel FOREIGN KEY(HotelID) REFERENCES Hotel(HotelID),
CONSTRAINT FKCRHotelListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRHotelListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CRCateringList
(
CRCateringListID BIGINT,
CustomerID BIGINT,
CRLegID BIGINT,
TripNUM INT,
LegID INT,
RecordType CHAR(2),
CateringID BIGINT,
CRCateringListDescription VARCHAR(60),
PhoneNUM VARCHAR(25),
Rate NUMERIC(6,2),
IsCompleted BIT,
AirportID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKCRCateringList PRIMARY KEY CLUSTERED(CRCateringListID ASC),
CONSTRAINT FKCRCateringListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCRCateringCRLeg FOREIGN KEY(CRLegID) REFERENCES CRLeg(CRLegID),
CONSTRAINT FKCRCateringListCatering FOREIGN KEY(CateringID) REFERENCES Catering(CateringID),
CONSTRAINT FKCRCateringListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKCRCateringListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- SQL Script File 03-Jan-2013 --