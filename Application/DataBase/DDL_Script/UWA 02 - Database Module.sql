-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Database Module          --
-- Created Date: 15-Jun-2012        --
-- Updated Date: 13-Dec-2012        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE Country
(
CountryID BIGINT,
CountryCD CHAR(3),
CountryName VARCHAR(60),
UVCD CHAR(2),
IATOCD CHAR(2),
LastUpdUID CHAR(30),
LastUpdDT DATETIME,
IsUVFlag BIT,
IsInActive BIT,
UPDTDATE DATETIME,
ISONum INT,
ISOCD CHAR(3),
IsDeleted BIT,
CONSTRAINT PKCountry PRIMARY KEY CLUSTERED(CountryID ASC),
CONSTRAINT FKCountryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE DSTRegion
(
DSTRegionID BIGINT,
DSTRegionCD CHAR(3),
DSTRegionName VARCHAR(40),
StartDT DATETIME,
EndDT DATETIME,
OffSet NUMERIC(6,2),
CityList VARCHAR(60),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKDSTRegion PRIMARY KEY CLUSTERED(DSTRegionID ASC),
CONSTRAINT FKDSTRegionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE Metro
(
MetroID BIGINT,
MetroCD	CHAR(3),
MetroName VARCHAR(25),
CountryID BIGINT,
StateName CHAR(2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKMetro PRIMARY KEY CLUSTERED(MetroID ASC),
CONSTRAINT FKMetroUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE Airport
(
AirportID BIGINT,
IcaoID CHAR(4),
CustomerID BIGINT,
CityName VARCHAR(25),
StateName VARCHAR(25),
CountryName VARCHAR(60),
CountryID BIGINT,
AirportName VARCHAR(25),
LatitudeDegree NUMERIC(3),	
LatitudeMinutes NUMERIC(5,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegrees NUMERIC(3),
LongitudeMinutes NUMERIC(5,1),
LongitudeEastWest CHAR(1),	
MagneticVariance NUMERIC(5,1),
VarianceEastWest CHAR(1),
Elevation NUMERIC(6),
LongestRunway NUMERIC(6),
IsEntryPort BIT,
WindZone NUMERIC(5),
IsUSTax BIT,
IsRural BIT,
IsAirport BIT,
IsHeliport BIT,
AirportInfo TEXT,
OffsetToGMT NUMERIC(6,2),
IsDayLightSaving BIT,
DayLightSavingStartDT DATE,
DaylLightSavingStartTM CHAR(5),
DayLightSavingEndDT DATE,
DayLightSavingEndTM	CHAR(5),
TowerFreq CHAR(7),
ATISFreq CHAR(7),
ARINCFreq CHAR(7),
GroundFreq CHAR(7),
ApproachFreq CHAR(7),
DepartFreq CHAR(7),
Label1Freq CHAR(10),
Freq1 CHAR(7),
Label2Freq	CHAR(10),
Freq2 CHAR(7),
TakeoffBIAS	NUMERIC(5,1),
LandingBIAS	NUMERIC(5,1),
Alerts TEXT,
GeneralNotes TEXT,
IsFlightPakFlag BIT,
FBOCnt INT,
HotelCnt INT,
TransportationCnt INT,
CateringCnt INT,
FlightPakUPD DATETIME,
MetroID BIGINT,
DSTRegionID BIGINT,
PPR CHAR(7),
Iata CHAR(3),
AlternateDestination1 CHAR(4),
AlternateDestination2 CHAR(4),
AlternateDestination3 CHAR(4),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsInActive BIT,
RecordType CHAR(4),
NewICAO CHAR(4),
PreviousICAO CHAR(4),
UpdateDT DATE,
IsWorldClock BIT,
WidthRunway	NUMERIC(6),
FssPhone VARCHAR(25),
LengthWidthRunway NUMERIC(6),
WidthLengthRunway NUMERIC(6),
CustomLocation VARCHAR(100),
CustomPhoneNum VARCHAR(25),
ImmigrationPhoneNum	VARCHAR(25),
AgPhoneNum VARCHAR(25),
IsCustomAvailable BIT,
PortEntryType CHAR(6),
Unicom CHAR(10),
Clearence1DEL CHAR(10),
Clearence2DEL CHAR(10),
AtisPhoneNum VARCHAR(25),
ASOS CHAR(10),
AsosPhoneNum VARCHAR(25),
AWOS CHAR(10),
AwosPhoneNum VARCHAR(25),
AwosType CHAR(1),
IsSlots BIT,
FssName VARCHAR(40),
AirportManager VARCHAR(60),
AirportManagerPhoneNum VARCHAR(25),
FAA CHAR(4),
UWAID CHAR(20),
IsFIX BIT,
HoursOfOperation VARCHAR(25),
LandingFeeSmall NUMERIC(17,2),
LandingFeeMedium NUMERIC(17,2),
LandingFeeLarge NUMERIC(17,2),
IsEUETS BIT,
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKAirport PRIMARY KEY CLUSTERED(AirportID ASC),
CONSTRAINT FKAirportCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAirportUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKAirportCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKAirportDSTRegion FOREIGN KEY(DSTRegionID) REFERENCES DSTRegion(DSTRegionID),
CONSTRAINT FKAirportMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX AirportIDX ON Airport
(
IcaoID ASC, CustomerID ASC
)
GO

CREATE TABLE Runway
(
RunwayID BIGINT,
CustomerID BIGINT,
AirportID BIGINT,
UWAID CHAR(20),
UWARunwayID CHAR(20),
RunwayLength INT,
RunwayWidth INT,
Surface VARCHAR(20),
Lights VARCHAR(5),
InstLandSysType VARCHAR(10),
NavigationAids VARCHAR(100),
UWAUpdates BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKRunway PRIMARY KEY CLUSTERED(RunwayID ASC),
CONSTRAINT FKRunwayCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKRunwayAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKRunwayUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
)
GO

CREATE TABLE FBO
(
FBOID BIGINT,
AirportID BIGINT,
FBOCD CHAR(4),
CustomerID BIGINT,
IsChoice BIT,
FBOVendor VARCHAR(60),
PhoneNUM1 VARCHAR(25), -- Rename Column
Frequency CHAR(7),
IsUWAAirPartner BIT,
IsCrewCar BIT,
FuelBrand VARCHAR(100),
ControlNum CHAR(5),
FaxNum VARCHAR(25),
LastFuelDT DATE,
LastFuelPrice NUMERIC(7,4),
NegotiatedFuelPrice NUMERIC(7,4),
Addr1 VARCHAR(25),
Addr2 VARCHAR(25),
CityName VARCHAR(25),
StateName VARCHAR(25),
PostalZipCD VARCHAR(15),
CountryID BIGINT,
Contact VARCHAR(25),
Remarks TEXT,
IsFPK BIT,
FPKUpdateDT DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
RecordType CHAR(1),
IsInActive BIT,
PhoneNUM2 VARCHAR(25), -- New Column
HoursOfOperation VARCHAR(20),
SourceID CHAR(12),
UpdateDT DATE,
IsPrimaryChoice BIT,
PostedPrice NUMERIC(10,4),
PaymentType CHAR(2),
FuelQty NUMERIC(15,2),
FixBaseOperatorID VARCHAR(20),
HHEATED CHAR(1),
HSJET CHAR(1),
HMJET CHAR(1),
HLJET CHAR(1),
HCJET CHAR(1),
FLSJETA CHAR(1),
FLS100LL CHAR(1),
FLS80 CHAR(1),
FSSJETA CHAR(1),
FSS100LL CHAR(1),
FSS80 CHAR(1),
FVOLDISCT CHAR(1),
ARFEE CHAR(1),
ARFEECOM VARCHAR(100),
AHFEE CHAR(1),
AHFEECOM VARCHAR(100),
A24 CHAR(1),
AQT CHAR(1),
ASEC CHAR(1),
AddDeIcing CHAR(1),
AOX CHAR(1),
ANIT CHAR(1),
ALAV CHAR(1),
AGPU CHAR(1),
AOWGR CHAR(1),
AMMINOR CHAR(1),
AMMAJOR CHAR(1),
AMAV CHAR(1),
AMAUTH CHAR(1),
AMAUTHCOM VARCHAR(100),
AICLEAN CHAR(1),
AICS CHAR(1),
AIIR CHAR(1),
AEWAS CHAR(1),
AEBRITE CHAR(1),
AEPAINT CHAR(1),
PWPS CHAR(1),
PFPR CHAR(1),
PCT CHAR(1),
PCC CHAR(1),
PLOUNGE CHAR(1),
PREC CHAR(1),
PEXEC CHAR(1),
PKIT CHAR(1),
PLAUND CHAR(1),
PSR CHAR(1),
PSHOWER CHAR(1),
PSUPP CHAR(1),
PMEOS CHAR(1),
RHERTZ CHAR(1),
RAVIS CHAR(1),
ROTHER CHAR(1),
ROCOMM VARCHAR(100),
PPLIMO CHAR(1),
PPTAXI CHAR(1),
PPROS CHAR(1),
PPRWD CHAR(1),
PPCONC CHAR(1),
PPCS CHAR(1),
PPCR CHAR(1),
PPLSR CHAR(1),
PPSEC CHAR(1),
PPCWIA CHAR(1),
PPMS CHAR(1),
PPFS CHAR(1),
PPCA CHAR(1),
PPGOLF CHAR(1),
PPCOFFEE CHAR(1),
PPSNACKS CHAR(1),
PPNEWS CHAR(1),
PPICE CHAR(1),
PXMF CHAR(1),
PXCOFFEE CHAR(1),
PXBAR CHAR(1),
PXCCC CHAR(1),
PXVCC CHAR(1),
PXLOUNGE CHAR(1),
PXGIFT CHAR(1),
OHF CHAR(1),
OPCS CHAR(1),
OTCS CHAR(1),
OJCS CHAR(1),
OHCS CHAR(1),
OAPS CHAR(1),
OCH CHAR(1),
CreditCardVISA CHAR(1),
CreditCardMasterCard CHAR(1),
CreditCardAMEX CHAR(1),
CreditCardDinerClub CHAR(1),
CreditCardJCB CHAR(1),
CreditCardMS CHAR(1),
CreditCardAVCard CHAR(1),
CreditCardUVAir CHAR(1),
COMJET CHAR(1),
TollFreePhoneNum VARCHAR(25),
SITA VARCHAR(15),
UNICOM VARCHAR(10),
ARINC VARCHAR(10),
IsUVAirPNR BIT,
Website VARCHAR(200),
EmailAddress VARCHAR(200),
LatitudeDegree NUMERIC(3),
LatitudeMinutes NUMERIC(5,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegrees NUMERIC(3),
LongitudeMinutes NUMERIC(5,1),
LongitudeEastWest CHAR(1),
IsUWAMaintFlag BIT,
UWAID CHAR(20),
ReceiptNum INT,
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKFBO PRIMARY KEY CLUSTERED(FBOID ASC),
CONSTRAINT FKFBOCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFBOUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKFBOAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKFBOCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FBOIDX ON FBO
(
AirportID ASC, FBOCD ASC, CustomerID ASC
)
GO

CREATE TABLE Transport
(
TransportID BIGINT,
AirportID BIGINT,
TransportCD CHAR(4),
CustomerID BIGINT,
IsChoice BIT,
TransportationVendor VARCHAR(20),
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
NegotiatedRate NUMERIC(6,2),
ContactName VARCHAR(25),
Remarks TEXT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
UpdateDT DATE,
RecordType CHAR(1),
SourceID CHAR(12),
ControlNum CHAR(5),
TollFreePhoneNum VARCHAR(25),
Website VARCHAR(200),
UWAMaintFlag BIT,
UWAID CHAR(20),
IsInActive BIT,
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKTransport PRIMARY KEY CLUSTERED(TransportID ASC),
CONSTRAINT FKTransportCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTransportUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKTransportAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX TransportIDX ON Transport
(
AirportID ASC, TransportCD ASC, CustomerID ASC
)
GO

CREATE TABLE Hotel
(
HotelID BIGINT,
AirportID BIGINT,
HotelCD CHAR(4),
CustomerID BIGINT,
IsChoice BIT,
Name VARCHAR(40),
PhoneNum VARCHAR(25),
MilesFromICAO NUMERIC(6,1),
HotelRating CHAR(3),
FaxNum VARCHAR(25),
NegociatedRate NUMERIC(17,2),
Addr1 VARCHAR(40),
Addr2 VARCHAR(40),
Addr3 VARCHAR(40),
CityName VARCHAR(30),
MetroID BIGINT,
StateName VARCHAR(25),		
PostalZipCD VARCHAR(15),
CountryID BIGINT,
ContactName VARCHAR(30),		
Remarks TEXT,
IsPassenger BIT,
IsCrew BIT,
IsUWAMaintained BIT,
LatitudeDegree NUMERIC(3),
LatitudeMinutes NUMERIC(5,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegrees NUMERIC(3),
LongitudeMinutes NUMERIC(5,1),
LongitudeEastWest CHAR(1),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
UpdateDT DATE,
RecordType CHAR(1),
MinutesFromICAO NUMERIC(6,1),
SourceID CHAR(12),
ControlNum CHAR(5),
Website VARCHAR(200),
UWAMaintFlag BIT,
UWAID CHAR(20),
IsInActive BIT,
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKHotel PRIMARY KEY CLUSTERED(HotelID ASC),
CONSTRAINT FKHotelCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKHotelUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKHotelAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKHotelMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID),
CONSTRAINT FKHotelCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX HotelIDX ON Hotel
(
AirportID ASC, HotelCD ASC, CustomerID ASC
)
GO

CREATE TABLE Catering
(
CateringID BIGINT,
AirportID BIGINT,
CateringCD CHAR(4),
CustomerID BIGINT,
IsChoice BIT,
CateringVendor VARCHAR(20),
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
ContactName VARCHAR(25),
Remarks TEXT,
NegotiatedRate NUMERIC(6,2),
LastUpdUID CHAR(30),
LastUpdTS DATE,
UpdateDT DATE,
RecordType CHAR(1),
SourceID CHAR(12),
ControlNum CHAR(5),
IsInActive BIT,
TollFreePhoneNum CHAR(25),
WebSite VARCHAR(200),
UWAMaintFlag BIT,
UWAID CHAR(20),
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKCatering PRIMARY KEY CLUSTERED(CateringID ASC),
CONSTRAINT FKCateringCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCateringUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCateringAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CateringIDX ON Catering
(
AirportID ASC, CateringCD ASC, CustomerID ASC
)
GO

CREATE TABLE FuelVendor
(
FuelVendorID BIGINT,
VendorCD CHAR(4),
VendorName VARCHAR(50),
AirportID BIGINT,
CustomerID BIGINT,
VendorDescription VARCHAR(50)NOT NULL,
ServerInfo VARCHAR(200),
BaseFileName VARCHAR(100),
FileLocation VARCHAR(200)NOT NULL,
FileTYPE CHAR(10),
FldSep CHAR(1),
IsHeaderRow BIT,
VendorDateFormat CHAR(10),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
UWAUpdates BIT,
CONSTRAINT PKFuelVendor PRIMARY KEY CLUSTERED(FuelVendorID ASC),
CONSTRAINT FKFuelVendorCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFuelVendorUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKFuelVendorAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FuelVendorIDX ON FuelVendor
(
AirportID ASC, VendorCD ASC, CustomerID ASC
)
GO

CREATE TABLE FareLevel
(
FareLevelID BIGINT,
FareLevelCD INT IDENTITY(1,1), -- Add Identify 14-05-2012 14:48
CustomerID BIGINT,
StartDT DATE,
EndDT DATE,
Rate1 NUMERIC(8,4),
Rate2 NUMERIC(8,4),
Rate3 NUMERIC(8,4),
TerminalCHG NUMERIC(8,4),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFareLevel PRIMARY KEY CLUSTERED(FareLevelID ASC),
CONSTRAINT FKFareLevelCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFareLevelUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FareLevelIDX ON FareLevel
(
FareLevelCD ASC, CustomerID ASC
)
GO

CREATE TABLE Vendor
(
VendorID BIGINT,
VendorCD CHAR(5),
Name VARCHAR(40),
CustomerID BIGINT,
VendorType CHAR(1),
IsApplicationFiled BIT,
IsApproved BIT,
IsDrugTest BIT,
IsInsuranceCERT BIT,
IsFAR135Approved BIT,
IsFAR135CERT BIT,
AdditionalInsurance VARCHAR(60),
Contact VARCHAR(60),
VendorContactName VARCHAR(40),
BillingName VARCHAR(40),
BillingAddr1 VARCHAR(40),
BillingAddr2 VARCHAR(40),
BillingCity VARCHAR(40),
BillingState VARCHAR(10),
BillingZip VARCHAR(15),
CountryID BIGINT,
MetroID BIGINT,
BillingPhoneNum VARCHAR(25),
BillingFaxNum VARCHAR(25),
Notes TEXT,
Credit NUMERIC(17,2),
DiscountPercentage NUMERIC(5,2),
TaxID VARCHAR(15),
Terms VARCHAR(25),
IsInActive BIT,
HomebaseID BIGINT,
LatitudeDegree NUMERIC(3),
LatitudeMinutes NUMERIC(5,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegree NUMERIC(3),
LongitudeMinutes NUMERIC(5,1),
LongitudeEastWest CHAR(1),
AirportID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,	
IsTaxExempt BIT,
DateAddedDT DATETIME,
EmailID VARCHAR(100),
WebAddress VARCHAR(100),
IsDeleted BIT,
CONSTRAINT PKVendor PRIMARY KEY CLUSTERED(VendorID ASC),
CONSTRAINT FKVendorCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKVendorUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKVendorCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKVendorAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKVendorCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKVendorMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID))
GO

CREATE UNIQUE NONCLUSTERED INDEX VendorIDX ON Vendor
(
VendorCD ASC, CustomerID ASC
)
GO

CREATE TABLE EmergencyContact
(
EmergencyContactID BIGINT,
EmergencyContactCD CHAR(5),
CustomerID BIGINT,
LastName VARCHAR(20),
FirstName VARCHAR(20),
MiddleName VARCHAR(20),
Addr1 VARCHAR(25),
Addr2 VARCHAR(25),
CityName VARCHAR(25),
StateName VARCHAR(10),
PostalZipCD VARCHAR(15),
CountryID BIGINT,
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
CellPhoneNum VARCHAR(15),
EmailAddress VARCHAR(100),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKEmergencyContact PRIMARY KEY CLUSTERED(EmergencyContactID ASC),
CONSTRAINT FKEmergencyContactCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKEmergencyContactUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKEmergencyContactCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
)
GO

CREATE UNIQUE NONCLUSTERED INDEX EmergencyContactIDX ON EmergencyContact
(
EmergencyContactCD ASC, CustomerID ASC
)
GO

CREATE TABLE Aircraft
(
AircraftID BIGINT,
CustomerID BIGINT,
AircraftCD CHAR(10),
AircraftDescription VARCHAR(15),
ChargeRate NUMERIC(17,2),
ChargeUnit CHAR(1),
PowerSetting CHAR(1),
PowerDescription CHAR(17),
WindAltitude NUMERIC(5,0),
PowerSettings1Description CHAR(17),	
PowerSettings1TrueAirSpeed NUMERIC(3,0),
PowerSettings1HourRange NUMERIC(6,3),
PowerSettings1TakeOffBias NUMERIC(6,3),
PowerSettings1LandingBias NUMERIC(6,3),
PowerSettings2Description CHAR(17),
PowerSettings2TrueAirSpeed NUMERIC(3,0),
PowerSettings2HourRange NUMERIC(6,3),
PowerSettings2TakeOffBias NUMERIC(6,3),
PowerSettings2LandingBias NUMERIC(6,3),
PowerSettings3Description CHAR(17),
PowerSettings3TrueAirSpeed NUMERIC(3,0),
PowerSettings3HourRange NUMERIC(6,3),
PowerSettings3TakeOffBias NUMERIC(6,3),
PowerSettings3LandingBias NUMERIC(6,3),
IsFixedRotary CHAR(1),
ClientID BIGINT,
CQChargeRate NUMERIC(17,2),
CQChargeUnit NUMERIC(1,0),
PositionRate NUMERIC(17,2),
PostionUnit NUMERIC(1,0),
StandardCrew NUMERIC(2,0),
StandardCrewRON NUMERIC(17,2),
AdditionalCrew NUMERIC(17,2),
AdditionalCrewRON NUMERIC(17,2),
CharterQuoteWaitTM NUMERIC(17,2),
LandingFee NUMERIC(17,2),
CostBy NUMERIC(1,0),
FixedCost NUMERIC(17,2),
PercentageCost NUMERIC(17,2),
TaxRON BIT,
AdditionalCrewTax BIT,
IsCharterQuoteWaitTax BIT,
LandingTax BIT,
DescriptionRON BIT,
AdditionalCrewDescription BIT,
MinDailyREQ NUMERIC(17,2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
AircraftSize NUMERIC(1,0),
AircraftTypeCD TEXT,
IntlStdCrewNum NUMERIC(2,0),
DomesticStdCrewNum NUMERIC(2,0),
MinimumDayUseHrs NUMERIC(17,2),
DailyUsageAdjTax BIT,
LandingFeeTax BIT,
IsDeleted BIT
CONSTRAINT PKAircraft PRIMARY KEY CLUSTERED(AircraftID ASC),
CONSTRAINT FKAircraftCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAircraftClient FOREIGN KEY(CustomerID) REFERENCES Client(ClientID),
CONSTRAINT FKAircraftUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX AircraftIDX ON Aircraft
(
AircraftCD ASC, CustomerID ASC
)
GO

CREATE TABLE Fleet
(
FleetID BIGINT,
TailNum CHAR(6),
CustomerID BIGINT,
AircraftCD CHAR(3),
SerialNum VARCHAR(20),
AircraftTypeCD CHAR(10),
TypeDescription VARCHAR(15),
MaximumReservation NUMERIC(3),
LastInspectionDT DATE,
InspectionHrs NUMERIC(4),
WarningHrs NUMERIC(4),
MaximumPassenger NUMERIC(3),
HomebaseID BIGINT,
MaximumFuel NUMERIC(6),
MinimumFuel NUMERIC(6),
BasicOperatingWeight NUMERIC(6),
MimimumRunway NUMERIC(5),
IsInActive BIT,
IsDisplay31 BIT,
SIFLMultiControlled NUMERIC(6,1),
SIFLMultiNonControled NUMERIC(6,1),
Notes TEXT,
ComponentCD INT,
ClientID BIGINT,
FleetType CHAR(1),
FlightPhoneNum VARCHAR(25),
Class NUMERIC(1),
VendorID BIGINT,
VendorType CHAR(1),
YearMade CHAR(4),
ExteriorColor VARCHAR(25),
InteriorColor VARCHAR(25),
IsAFIS BIT,
IsUWAData BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
FlightPlanCruiseSpeed CHAR(10),
FlightPlanMaxFlightLevel NUMERIC(5),
MaximumTakeOffWeight NUMERIC(6),
MaximumLandingWeight NUMERIC(6),
MaximumWeightZeroFuel NUMERIC(6),
TaxiFuel NUMERIC(6),
MultiSec NUMERIC(6,1),
ForeGrndCustomColor VARCHAR(8),
BackgroundCustomColor VARCHAR(8),
FlightPhoneIntlNum VARCHAR(25),
FleetSize CHAR(1),
FltScanDoc TEXT,
MinimumDay NUMERIC(17,2),
StandardCrewIntl NUMERIC(2),
StandardCrewDOM NUMERIC(2),
IsFAR91 BIT,
IsFAR135 BIT,	
MaximumCrew CHAR(3),
EmergencyContactID BIGINT,
IsTaxDailyAdj BIT,
IsTaxLandingFee BIT,
IsDeleted BIT,
CONSTRAINT PKFleet PRIMARY KEY CLUSTERED(FleetID ASC),
CONSTRAINT FKFleetCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKFleetCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKFleetClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKFleetVendor FOREIGN KEY(VendorID) REFERENCES Vendor(VendorID),
CONSTRAINT FKFleetEmergencyContact FOREIGN KEY(EmergencyContactID) REFERENCES EmergencyContact(EmergencyContactID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FleetIDX ON Fleet
(
TailNum ASC, CustomerID ASC
)
GO

CREATE TABLE Department
(
DepartmentID BIGINT,
DepartmentCD CHAR(8),
DepartmentName VARCHAR(25),
CustomerID BIGINT,
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsInActive BIT,
IsDeleted BIT,
CONSTRAINT PKDepartment PRIMARY KEY CLUSTERED(DepartmentID ASC),
CONSTRAINT FKDepartmentCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDepartmentUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKDepartmentClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX DepartmentIDX ON Department
(
DepartmentCD ASC, CustomerID ASC
)
GO

CREATE TABLE DepartmentAuthorization
(
AuthorizationID BIGINT,
DepartmentID BIGINT,
AuthorizationCD CHAR(8),
CustomerID BIGINT,
DeptAuthDescription VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsInActive BIT,
AuthorizerPhoneNum VARCHAR(25),
IsDeleted BIT,
CONSTRAINT PKDepartmentAuthorization PRIMARY KEY CLUSTERED(AuthorizationID ASC),
CONSTRAINT FKDepartmentAuthorizationCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDepartmentAuthorizationUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKDepartmentAuthorizationClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKDepartmentAuthorizationDepartment FOREIGN KEY(ClientID) REFERENCES Department(DepartmentID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX DepartmentAuthorizationIDX ON DepartmentAuthorization
(
AuthorizationCD ASC, DepartmentID ASC, CustomerID ASC
)
GO

CREATE TABLE Account
(
AccountID BIGINT,
AccountNum CHAR(32),
CustomerID BIGINT,
AccountDescription VARCHAR(40),
CorpAccountNum VARCHAR(250),
HomebaseID BIGINT,
IsBilling BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
Cost NUMERIC(17,2),
IsInActive BIT,
IsDeleted BIT,
CONSTRAINT PKAccount PRIMARY KEY CLUSTERED(AccountID ASC),
CONSTRAINT FKAccountCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAccountUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKAccountCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX AccountIDX ON Account
(
AccountNum ASC, CustomerID ASC
)
GO

CREATE TABLE FlightPurpose
(
FlightPurposeID BIGINT,
FlightPurposeCD CHAR(2),
FlightPurposeDescription VARCHAR(25),
CustomerID BIGINT,
IsDefaultPurpose BIT,
IsPersonalTravel BIT,
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsWaitList BIT,
IsDeleted BIT,
CONSTRAINT PKFlightPurpose PRIMARY KEY CLUSTERED(FlightPurposeID ASC),
CONSTRAINT FKFlightPurposeCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFlightPurposeUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKFlightPurposeClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FlightPurposeIDX ON FlightPurpose
(
FlightPurposeCD ASC, CustomerID ASC
)
GO

CREATE TABLE FlightCatagory
(
FlightCategoryID BIGINT,
FlightCatagoryCD CHAR(4),
FlightCatagoryDescription VARCHAR(25),
CustomerID BIGINT,
GraphColor CHAR(11),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
ForeGrndCustomColor CHAR(8),
BackgroundCustomColor CHAR(8),
CallKey INT,
IsInActive BIT,
IsDeleted BIT,
CONSTRAINT PKFlightCatagory PRIMARY KEY CLUSTERED(FlightCategoryID ASC),
CONSTRAINT FKFlightCatagoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFlightCatagoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKFlightCatagoryClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
)
GO

CREATE TABLE Passenger
(
PassengerRequestorID BIGINT,
PassengerRequestorCD CHAR(5),
PassengerName VARCHAR(63),
CustomerID BIGINT,
DepartmentID BIGINT,
PassengerDescription VARCHAR(25),
PhoneNum VARCHAR(25),
IsEmployeeType CHAR(1),
StandardBilling VARCHAR(25),
AuthorizationID BIGINT,
AuthorizationDescription VARCHAR(25),
Notes TEXT,
LastName VARCHAR(20),
FirstName VARCHAR(20),
MiddleInitial VARCHAR(20),
DateOfBirth DATE,
ClientID BIGINT,
AssociatedWithCD CHAR(5),
HomebaseID BIGINT,
IsActive BIT,
FlightPurposeID BIGINT,
SSN CHAR(9),
Title VARCHAR(30),
SalaryLevel NUMERIC(4),
IsPassengerType NUMERIC(1),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsScheduledServiceCoord BIT,
CompanyName VARCHAR(60),
EmployeeID CHAR(25),
FaxNum VARCHAR(25),
EmailAddress VARCHAR(250),
PersonalIDNum CHAR(10),
IsSpouseDependant BIT,
AccountID BIGINT,
CountryID BIGINT,
IsSIFL BIT,
PassengerAlert TEXT,
Gender CHAR(1),
AdditionalPhoneNum VARCHAR(25),
IsRequestor BIT,
CountryOfResidenceID BIGINT,
PaxScanDoc TEXT,
TSAStatus CHAR(1),
TSADTTM DATETIME,
Addr1 VARCHAR(40),
Addr2 VARCHAR(40),
City VARCHAR(25),
StateName VARCHAR(10),
PostalZipCD CHAR(15),
IsDeleted BIT,
CONSTRAINT PKPassenger PRIMARY KEY CLUSTERED(PassengerRequestorID ASC),
CONSTRAINT FKPassengerCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPassengerUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPassengerClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKPassengerCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKPassengerDepartmentAuthorization FOREIGN KEY(AuthorizationID) REFERENCES DepartmentAuthorization(AuthorizationID),
CONSTRAINT FKPassengerAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID),
CONSTRAINT FKPassengerFlightPurpose FOREIGN KEY(FlightPurposeID) REFERENCES FlightPurpose(FlightPurposeID),
CONSTRAINT FKPassengerFlightDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKPassengerFlightCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKPassengerFlightResidenceCountry FOREIGN KEY(CountryOfResidenceID) REFERENCES Country(CountryID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PassengerIDX ON Passenger
(
PassengerRequestorID ASC, CustomerID ASC
)
GO

CREATE TABLE TripManagerCheckListGroup
(
CheckGroupID BIGINT,
CheckGroupCD CHAR(3),
CustomerID BIGINT,
CheckGroupDescription VARCHAR(40),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTripManagerCheckListGroup PRIMARY KEY CLUSTERED(CheckGroupID ASC),
CONSTRAINT FKTripManagerCheckListGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTripManagerCheckListGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX TripManagerCheckListGroupIDX ON TripManagerCheckListGroup
(
CheckGroupCD ASC, CustomerID ASC
)
GO

CREATE TABLE TripManagerCheckList
(
CheckListID BIGINT,
CustomerID BIGINT,
CheckListCD CHAR(4) NOT NULL,
CheckListDescription VARCHAR(20),
CheckGroupID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTripManagerCheckList PRIMARY KEY CLUSTERED(CheckListID ASC),
CONSTRAINT FKTripManagerCheckListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTripManagerCheckListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKTripManagerCheckListTripManagerCheckListGroup FOREIGN KEY(CheckGroupID) REFERENCES TripManagerCheckListGroup(CheckGroupID)
)
GO

CREATE TABLE PaymentType
(
PaymentTypeID BIGINT,
PaymentTypeCD CHAR(2),
CustomerID BIGINT,
PaymentTypeDescription VARCHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPaymentType PRIMARY KEY CLUSTERED(PaymentTypeID),
CONSTRAINT FKPaymentTypeCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPaymentTypeUserManager FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FuelLocator
(
FuelLocatorID BIGINT,
FuelLocatorCD CHAR(4),
CustomerID BIGINT,
FuelLocatorDescription VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFuelLocator PRIMARY KEY CLUSTERED(FuelLocatorID),
CONSTRAINT FKFuelLocatorCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFuelLocatorClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKFuelLocatorUserManager FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE AircraftDuty
(
AircraftDutyID BIGINT,
AircraftDutyCD CHAR(2),
CustomerID BIGINT,
AircraftDutyDescription VARCHAR(25),
ClientID BIGINT,
GraphColor CHAR(11),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
ForeGrndCustomColor CHAR(8),
BackgroundCustomColor CHAR(8),
IsCalendarEntry BIT,
DefaultStartTM CHAR(5),
DefualtEndTM CHAR(5),
IsAircraftStandby BIT,
IsDeleted BIT,
CONSTRAINT PKAircraftDuty PRIMARY KEY CLUSTERED(AircraftDutyID),
CONSTRAINT FKAircraftDutyCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKAircraftDutyClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKAircraftDutyUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE Crew
(
CrewID BIGINT,
CrewCD CHAR(3),
CustomerID BIGINT,
LastName VARCHAR(30),
FirstName VARCHAR(20),
MiddleInitial VARCHAR(20)NOT NULL,
IsFixedWing BIT,
IsRotaryWing BIT,
Addr1 VARCHAR(40),
Addr2 VARCHAR(40),
CityName VARCHAR(25),
StateName VARCHAR(10),
PostalZipCD VARCHAR(15),
CountryID BIGINT,
PhoneNum CHAR(25),
SSN CHAR(11),
IsStatus BIT,
HomebaseID BIGINT,
BirthDT DATETIME,
ClientID BIGINT,
CellPhoneNum VARCHAR(25),
PagerNum VARCHAR(25),
CrewTypeCD CHAR(4),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
PilotLicense1 VARCHAR(25),
Notes TEXT,
CheckList CHAR(1),
PilotLicense2 VARCHAR(25), -- New Column
LicenseCountry1 CHAR(3),
LicenseCountry2 CHAR(3),
IsNoCalendarDisplay BIT,
CrewTypeDescription VARCHAR(25),
Gender CHAR(1),
EmailAddress VARCHAR(200),
FaxNum VARCHAR(25),
HireDT DATETIME,
TerminationDT DATETIME,
LicenseType1 CHAR(30),
LicenseType2 CHAR(30),
DepartmentID BIGINT,
IsCheckListFrequency NUMERIC(1,0),
IsNextMonth BIT,
CrewScanDocuments TEXT,
Citizenship BIGINT,
Notes2 TEXT,
IsDepartmentAuthorization BIT,
IsRequestPhoneNum BIT,
IsCancelDescription BIT,
IsTripN BIT,
IsTripA BIT,
IsAirport BIT,
IsCheckList BIT,
IsAccountNum BIT,
IsStatusT BIT, -- New Column
IsDayLightSavingTimeARR BIT,
IsDayLightSavingTimeDEPART BIT,
IsHomeArrivalTM BIT,
IsHomeDEPARTTM BIT,
IsEndDuty BIT,
IsOverride BIT,
IsCrewRules BIT,
IsFAR BIT,
IsDuty BIT,
IsFlightHours BIT,
IsRestHrs BIT,
Comments BIT,
IsDepartureFBO BIT,
IsArrivalFBO BIT,
IsCrewHotel BIT,
IsCrewDepartTRANS BIT,
IsCrewArrivalTRANS BIT,
IsCrewHotelN BIT, -- New Column
IsPassHotel BIT,
IsPassDepartTRANS BIT,
IsPassArrivalHotel BIT,
IsPassDetails BIT,
IsPassPhoneNum BIT,
IsDepartCatering BIT,
IsArrivalCatering BIT,
IsArrivalDepartTime BIT,
IcaoID BIT,
IsCrew BIT,
IsPassenger BIT,
IsLegN BIT,
IsOutboundINST BIT,
IsCheckList1 BIT, -- New Column
IsFBO BIT,
IsHotel BIT,
IsTransportation BIT,
IsCatering BIT,
Label1 BIT,
Label2 BIT,
Label3 BIT,
Label4 BIT,
Label5 BIT,
Label6 BIT,
Label7 BIT,
IsCrewHotelConfirm BIT,
IsCrewHotelComments BIT,
IsCrewDepartTRANSConfirm BIT,
IsCrewDepartTRANSComments BIT,
IsCrewArrivalConfirm BIT,
IsCrewArrivalComments BIT,
IsCrewNotes BIT,
IsPassHotelConfirm BIT,
IsPassHotelComments BIT,
IsPassDepartTRANSConfirm BIT,
IsPassDepartTRANSComments BIT,
IsPassArrivalConfirm BIT,
IsPassArrivalComments BIT,
IsPassNotes BIT,
IsOutboundComments BIT,
IsDepartAirportNotes BIT,
IsArrivalAirportNoes BIT,
IsDepartAirportAlerts BIT,
IsArrivalAirportAlerts BIT,
IsFBODepartConfirm BIT,
IsFBOArrivalConfirm BIT,
IsFBODepartComment BIT,
IsFBOArrivalComment BIT,
IsDepartCateringConfirm BIT,
IsDepartCateringComment BIT,
IsArrivalCateringConfirm BIT,
IsArrivalCateringComment BIT,
IsTripAlerts BIT,
IsTripNotes BIT,
IsRunway BIT,
IsPassengerPhoneNum BIT,
BlackBerryTM CHAR(1),
IsAssociatedCrew BIT,
CityOfBirth CHAR(30),
CountryOfBirth BIGINT,
License1CityCountry VARCHAR(30),
License2CityCountry VARCHAR(30),
License1ExpiryDT DATETIME,
License2ExpiryDT DATETIME,
APISException TEXT,
APISSubmit DATETIME,
APISUserID CHAR(10),
IsAircraft BIT,
StateofBirth CHAR(10),
IsDeleted BIT,
CONSTRAINT PKCrew PRIMARY KEY CLUSTERED(CrewID ASC),
CONSTRAINT FKCrewCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCrewCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCrewClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKCrewDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKCrewCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKCrewCitzenshipCountry FOREIGN KEY(Citizenship) REFERENCES Country(CountryID),
CONSTRAINT FKCrewBirthCountry FOREIGN KEY(CountryOfBirth) REFERENCES Country(CountryID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewIDX ON Crew
(
CrewID ASC, CustomerID ASC
)
GO

CREATE TABLE CrewPassengerPassport
(
PassportID BIGINT,
CrewID BIGINT,
PassengerRequestorID BIGINT,
CustomerID BIGINT,
Choice BIT,
IssueCity VARCHAR(40),
PassportNum VARCHAR(25),
PassportExpiryDT DATE,
PassportCountry	CHAR(3),
IsDefaultPassport BIT,
PilotLicenseNum	VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IssueDT DATE,
IsDeleted BIT,
CONSTRAINT PKCrewPassport PRIMARY KEY CLUSTERED(PassportID ASC),
CONSTRAINT FKCrewPassportCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewPassportUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCrewPassportCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewPassportPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
(
CrewID ASC, CustomerID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
(
PassengerRequestorID ASC, CustomerID ASC
)
GO

CREATE TABLE CrewPassengerVisa
(
VisaID BIGINT,
CrewID BIGINT,
PassengerRequestorID BIGINT,
CustomerID BIGINT,
VisaTYPE CHAR(1),
VisaNum VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CountryID BIGINT,
ExpiryDT DATE,
Notes VARCHAR(120),
IssuePlace VARCHAR(40),
IssueDate DATE,
IsDeleted BIT,
CONSTRAINT PKCrewPassengerVisa PRIMARY KEY CLUSTERED(VisaID ASC),
CONSTRAINT FKCrewPassengerVisaCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewPassengerVisaUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKCrewPassengerVisaCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewPassengerVisaPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
(
CrewID ASC, CustomerID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
(
PassengerRequestorID ASC, CustomerID ASC
)
GO

CREATE TABLE CrewDutyType
(
DutyTypeID BIGINT,
DutyTypeCD CHAR(2),
CustomerID BIGINT,
DutyTypesDescription VARCHAR(25),
ValuePTS NUMERIC(2),
GraphColor CHAR(11),
WeekendPTS NUMERIC(2),
IsWorkLoadIndex BIT,
IsOfficeDuty BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
ForeGrndCustomColor CHAR(11),
BackgroundCustomColor CHAR(11),
IsCrewCurrency BIT,
HolidayPTS NUMERIC(2,0),
IsCrewDuty BIT,
CalendarEntry BIT,
DutyStartTM CHAR(5),
DutyEndTM CHAR(5),
IsStandby BIT,
IsDeleted BIT,
CONSTRAINT PKCrewDutyType PRIMARY KEY CLUSTERED(DutyTypeID),
CONSTRAINT FKDutyTypeIDCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDutyTypeIDUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewDutyRules
(
CrewDutyRulesID BIGINT,
CrewDutyRuleCD CHAR(4) NOT NULL,
CustomerID BIGINT,
CrewDutyRulesDescription VARCHAR(30)NOT NULL,
FedAviatRegNum CHAR(3),
DutyDayBeingTM NUMERIC(6,3),
DutyDayEndTM NUMERIC(6,3),
MaximumDutyHrs NUMERIC(6,3),
MaximumFlightHrs NUMERIC (6,3),
MinimumRestHrs NUMERIC(6,3),
RestMultiple NUMERIC(4,2),
RestMultipleHrs NUMERIC(6,3),
LastUpdUID CHAR(30),
LastUptTM DATETIME,
IsDeleted BIT,
CONSTRAINT PKCrewDutyRules PRIMARY KEY CLUSTERED(CrewDutyRulesID),
CONSTRAINT FKCrewDutyRulesCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewDutyRulesUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- SQL Script File 27-Jun-2012 --

CREATE TABLE CrewDutyTypeXRef
(
CrewDutyTypeXRefID BIGINT,
CrewID BIGINT,
DutyTypeID BIGINT,
CONSTRAINT PKCrewDutyTypeXRef PRIMARY KEY CLUSTERED(CrewDutyTypeXRefID),
CONSTRAINT FKCrewDutyTypeXRefCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewDutyTypeXRefCrewDutyType FOREIGN KEY(DutyTypeID) REFERENCES CrewDutyType(DutyTypeID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewDutyTypeXRefIDX ON CrewDutyTypeXRef
(
CrewID ASC, DutyTypeID ASC
)
GO

CREATE TABLE CrewCheckList
(
CrewCheckID BIGINT,
CrewCheckCD CHAR(3) NOT NULL,
CustomerID BIGINT,
CrewChecklistDescription VARCHAR(25),
IsScheduleCheck BIT,
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsCrewCurrencyPlanner BIT,
IsDeleted BIT,
CONSTRAINT PKCrewCheckList PRIMARY KEY CLUSTERED(CrewCheckID),
CONSTRAINT FKCrewCheckListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewCheckListClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKCrewCheckListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewCheckListDetail
(
CheckListID BIGINT,
CheckListCD CHAR(3),
PreviousCheckDT DATETIME,
CustomerID BIGINT,
CrewID BIGINT,
DueDT DATETIME,
AlertDT DATETIME,
BaseMonthDT DATETIME,
FrequencyMonth INT,
AlertDays INT,
GraceDays INT,
GraceDT DATETIME,
IsMonthEnd BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsStopCALC BIT,
IsOneTimeEvent BIT,
IsNoConflictEvent BIT,
IsNoCrewCheckListREPTt BIT,
IsNoChecklistREPT BIT,
AircraftID BIGINT,
IsInActive BIT,
Specific INT,
IsPilotInCommandFAR91 BIT,
IsPilotInCommandFAR135 BIT,
IsSecondInCommandFAR91 BIT,
IsSecondInCommandFAR135 BIT,
IsCompleted BIT,
OriginalDT DATETIME,
TotalREQFlightHrs NUMERIC (7,1),
IsPrintStatus BIT,
IsPassedDueAlert BIT,
Frequency NUMERIC (1,0),
IsNextMonth BIT,
IsEndCalendarYear BIT,
IsScheduleCheck BIT,
IsDeleted BIT,
CONSTRAINT PKCrewCheckListDetail PRIMARY KEY CLUSTERED(CheckListID),
CONSTRAINT FKCrewCheckListDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewCheckListDetailCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewCheckListDetailAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKCrewChecklistDateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewGroup
(
CrewGroupID BIGINT,
CrewGroupCD CHAR(4) NOT NULL,
CustomerID BIGINT,
CrewGroupDescription VARCHAR(30)NOT NULL,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
HomebaseID BIGINT,
IsDeleted BIT,
CONSTRAINT PKCrewGroup PRIMARY KEY CLUSTERED(CrewGroupID),
CONSTRAINT FKCrewGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewGroupCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKCrewGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewGroupOrder
(
CrewGroupOrderID BIGINT,
CustomerID BIGINT,
CrewGroupID BIGINT,
OrderNum INT,
CrewID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKCrewGroupOrder PRIMARY KEY CLUSTERED(CrewGroupOrderID),
CONSTRAINT FKCrewGroupOrderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewGroupOrderCrewGroup FOREIGN KEY(CrewGroupID) REFERENCES CrewGroup(CrewGroupID),
CONSTRAINT FKCrewGroupOrderCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewGroupOrderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewRating
(
CrewID BIGINT,
CustomerID BIGINT,
AircraftTypeID BIGINT,
CrewRatingDescription VARCHAR(15),
IsPilotinCommand BIT,
IsSecondInCommand BIT,
IsEngineer BIT,
IsInstructor BIT,
IsAttendant BIT,
AsOfDT DATE,
TimeInType NUMERIC(11,3),
Day1 NUMERIC(11,3),
Night1 NUMERIC(11,3),
Instrument NUMERIC(11,3),
Simulator NUMERIC(11,3),
PilotinCommand NUMERIC(11,3),
SecondinCommand NUMERIC(11,3),
FlightEngineer NUMERIC(11,3),
FlightInstructor NUMERIC(11,3),
FlightAttendant NUMERIC(11,3),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsQualInType135PIC BIT,
IsQualInType135SIC BIT,
PilotinCommandDayHrs NUMERIC(9,1),
PilotInCommandNightHrs NUMERIC(9,1),
PilotInCommandINSTHrs NUMERIC(9,1), -- Rename Column
SecondInCommandDay NUMERIC(9,1), -- Rename Column
SecondInCommandNight NUMERIC(9,1), -- New Column
SecondInCommandInstr NUMERIC(9,1), -- New Column
TotalTimeInTypeHrs NUMERIC(9,1),
TotalDayHrs NUMERIC(9,1),
TotalNightHrs NUMERIC(9,1),
TotalINSTHrs NUMERIC(9,1),
PilotInCommandTypeHrs NUMERIC(9,1),
TPilotinCommandDayHrs NUMERIC(9,1), -- New Column
TPilotInCommandNightHrs NUMERIC(9,1), -- New Column
TPilotInCommandINSTHrs NUMERIC(9,1), -- Rename Column
SecondInCommandTypeHrs NUMERIC(9,1),
SecondInCommandDayHrs NUMERIC(9,1),
SecondInCommandNightHrs NUMERIC(9,1),
SecondInCommandINSTHrs NUMERIC(9,1),
OtherSimHrs NUMERIC(9,1),
OtherFlightEngHrs NUMERIC(9,1),
OtherFlightInstrutorHrs NUMERIC(9,1),
OtherFlightAttdHrs NUMERIC(9,1),
TotalUpdateAsOfDT DATETIME,
IsInActive BIT,
IsCheckAirman BIT,
IsCheckAttendant BIT,
IsAttendantFAR91 BIT,
IsAttendantFAR135 BIT,
IsDeleted BIT,
CONSTRAINT PKCrewRating PRIMARY KEY CLUSTERED(CrewID),
CONSTRAINT FKCrewRatingCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewRatingCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewRatingAircraft FOREIGN KEY(AircraftTypeID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKCrewRatingClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKCrewRatingUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewInformation
(
CrewInfoID BIGINT,
CrewInfoCD CHAR(3)NOT NULL,
CustomerID BIGINT,
CrewInformationDescription VARCHAR(40),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCrewInformation PRIMARY KEY CLUSTERED(CrewInfoID),
CONSTRAINT FKCrewInformationCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewInformationUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewDefinition
(
CrewInfoXRefID BIGINT,
CrewID BIGINT,
CustomerID BIGINT,
CrewInfoID BIGINT,
InformationDESC VARCHAR(40),
InformationValue VARCHAR(40),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsReptFilter BIT,
IsDeleted BIT,
CONSTRAINT PKCrewDefinition PRIMARY KEY CLUSTERED(CrewInfoXRefID),
CONSTRAINT FKCrewDefinitionCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewDefinitionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewRatingCrewInformation FOREIGN KEY(CrewInfoID) REFERENCES CrewInformation(CrewInfoID),
CONSTRAINT FKCrewDefinitionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CrewAircraftAssigned
(
CrewAircraftAssignedID BIGINT,
CrewID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCrewAircraftAssigned PRIMARY KEY CLUSTERED(CrewAircraftAssignedID),
CONSTRAINT FKCrewAircraftAssignedCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKCrewAircraftAssignedCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCrewAircraftAssignedFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKCrewAircraftAssignedUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE Budget
(
BudgetID BIGINT,
CustomerID BIGINT,
AccountID BIGINT,
FleetID BIGINT,
AccountNum CHAR(32),
FiscalYear CHAR(4),
Month1Budget NUMERIC (17,2),
Month2Budget NUMERIC (17,2),
Month3Budget NUMERIC (17,2),
Month4Budget NUMERIC (17,2),
Month5Budget NUMERIC (17,2),
Month6Budget NUMERIC (17,2),
Month7Budget NUMERIC (17,2),
Month8Budget NUMERIC (17,2),
Month9Budget NUMERIC (17,2),
Month10Budget NUMERIC (17,2),
Month11Budget NUMERIC (17,2),
Month12Budget NUMERIC (17,2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKBudget PRIMARY KEY CLUSTERED(BudgetID ASC),
CONSTRAINT FKBudgetCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKBudgetAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID),
CONSTRAINT FKBudgetFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKBudgetUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
)
GO

CREATE TABLE DepartmentGroup
(
DepartmentGroupID BIGINT,
CustomerID BIGINT,
HomebaseID BIGINT,
DepartmentGroupCD CHAR(4),
DepartmentGroupDescription VARCHAR(40),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKDepartmentGroup PRIMARY KEY CLUSTERED(DepartmentGroupID ASC),
CONSTRAINT FKDepartmentGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDepartmentGroupCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKDepartmentGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE DepartmentGroupOrder
(
DepartmentGroupOrderID BIGINT,
CustomerID BIGINT,
DepartmentGroupID BIGINT,
OrderNum INT,
DepartmentID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKDepartmentGroupOrder PRIMARY KEY CLUSTERED(DepartmentGroupOrderID ASC),
CONSTRAINT FKDepartmentGroupOrderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDepartmentGroupOrderDepartmentGroup FOREIGN KEY(DepartmentGroupID) REFERENCES DepartmentGroup(DepartmentGroupID),
CONSTRAINT FKDepartmentGroupOrderDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKDepartmentGroupOrderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PassengerGroup
(
PassengerGroupID BIGINT,
CustomerID BIGINT,
HomebaseID BIGINT,
PassengerGroupCD CHAR(4),
PassengerGroupName VARCHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPassengerGroup PRIMARY KEY CLUSTERED(PassengerGroupID ASC),
CONSTRAINT FKPassengerGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPassengerGroupCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKPassengerGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
)
GO

CREATE TABLE PassengerGroupOrder
(
PassengerGroupOrderID BIGINT,
CustomerID BIGINT,
PassengerGroupID BIGINT,
OrderNum INT,
PassengerRequestorID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPassengerGroupOrder PRIMARY KEY CLUSTERED(PassengerGroupOrderID ASC),
CONSTRAINT FKPassengerGroupOrderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPassengerGroupOrderPassengerGroup FOREIGN KEY(PassengerGroupID) REFERENCES PassengerGroup(PassengerGroupID),
CONSTRAINT FKPassengerGroupOrderPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKPassengerGroupOrderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PassengerInformation
(
PassengerInformationID BIGINT,
CustomerID BIGINT,
ClientID BIGINT,
PassengerInfoCD CHAR(3),
PassengerDescription VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsShowOnTrip BIT,
IsDeleted BIT,
CONSTRAINT PKPassengerInformation PRIMARY KEY CLUSTERED(PassengerInformationID ASC),
CONSTRAINT FKPassengerInformationCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPassengerInformationClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKPassengerInformationUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PassengerAdditionalInfo
(
PassengerAdditionalInfoID BIGINT,
CustomerID BIGINT,
PassengerRequestorID BIGINT,
AdditionalINFOCD CHAR(3),
AdditionalINFODescription VARCHAR(25),
AdditionalINFOValue VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPassengerAdditionalInfo PRIMARY KEY CLUSTERED(PassengerAdditionalInfoID ASC),
CONSTRAINT FKPassengerAdditionalInfoCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPassengerAdditionalInfoPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKPassengerAdditionalInfoClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKPassengerAdditionalInfoUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

ALTER TABLE Fleet DROP COLUMN AircraftTypeCD
GO

ALTER TABLE Fleet ADD AircraftID  BIGINT
GO

ALTER TABLE Fleet DROP COLUMN MaximumCrew
GO

ALTER TABLE Fleet ADD CrewID  BIGINT
GO

ALTER TABLE Fleet ADD CONSTRAINT FKFleetAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID)
GO

ALTER TABLE Fleet ADD CONSTRAINT FKFleetCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID)
GO

CREATE TABLE FleetGroup
(
FleetGroupID BIGINT,
CustomerID BIGINT,
HomebaseID BIGINT,
FleetGroupCD CHAR(4),
FleetGroupDescription VARCHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetGroup PRIMARY KEY CLUSTERED(FleetGroupID ASC),
CONSTRAINT FKFleetGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetGroupCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKFleetGroupUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetGroupOrder
(
FleetGroupOrderID BIGINT,
CustomerID BIGINT,
FleetGroupID BIGINT,
OrderNum INT,
FleetID BIGINT,
IsVendor BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetGroupOrder PRIMARY KEY CLUSTERED(FleetGroupOrderID ASC),
CONSTRAINT FKFleetGroupOrderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetGroupOrderFleetGroup FOREIGN KEY(FleetGroupID) REFERENCES FleetGroup(FleetGroupID),
CONSTRAINT FKFleetGroupOrderFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetGroupOrderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetComponent
(
FleetComponentID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
ComponentCD CHAR(7),
FleetComponentDescription VARCHAR(30),--see this--
LastInspectionDT DATE,
IsHrsDaysCycles CHAR(1),
InspectionHrs NUMERIC(6),
WarningHrs NUMERIC(6),
SericalNum VARCHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetComponent PRIMARY KEY CLUSTERED(FleetComponentID ASC),
CONSTRAINT FKFleetComponentCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetComponentFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetComponentUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetPair
(
FleetProfileInternationalDataID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
BuyAircraftAdditionalFeeDOM CHAR(8),
AircraftMake VARCHAR(30),
AircraftModel VARCHAR(20),
AircraftColor1 VARCHAR(15),
AircraftColor2 VARCHAR(15),
AircraftTrimColor VARCHAR(15),
OwnerLesse VARCHAR(100),
Addr1 VARCHAR(25),
Addr2 VARCHAR(25),
CityName VARCHAR(25),
StateName CHAR(10),
CountryID BIGINT,
PostalZipCD VARCHAR(15),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
ManufactureYear INT,
PLastName VARCHAR(20), -- Rename Column
PFirstName VARCHAR(20), -- Rename Column
PMiddlename VARCHAR(20), -- Rename Column
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
EmailAddress VARCHAR(100),
Company VARCHAR(100),
OLastName VARCHAR(20), -- New Column
OFirstName VARCHAR(20), -- New Column
OMiddleName VARCHAR(20), -- New Column
Address1 VARCHAR(25),
Address2 VARCHAR(25),
OCityName VARCHAR(25), -- New Column
OStateName VARCHAR(10), -- New Column
ZipCD VARCHAR(15),
ISO CHAR(3),
OPhoneNum VARCHAR(25), -- New Column
OFaxNum VARCHAR(25), -- New Column
OEmailAddress VARCHAR(100), -- New Column
IsDeleted BIT,
CONSTRAINT PKFleetPair PRIMARY KEY CLUSTERED(FleetProfileInternationalDataID ASC),
CONSTRAINT FKFleetPairCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetPairFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetPairCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKFleetPairUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetChargeRate
(
FleetChargeRateID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
BeginRateDT DATE,
EndRateDT DATE,
ChargeRate NUMERIC(17,2),
ChargeUnit CHAR(1),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetChargeRate PRIMARY KEY CLUSTERED(FleetChargeRateID ASC),
CONSTRAINT FKFleetChargeRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetChargeRateFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetChargeRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetCharterRate
(
FleetCharterRateID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
SellChgRateIntl NUMERIC(17,2),
SellChgRateDOM NUMERIC(17,2),
SellChgUnit NUMERIC(1),
BuyChargeRateIntl NUMERIC(17,2),
BuyChgRateDOM NUMERIC(17,2),
SellPosRateIntl NUMERIC(17,2), -- New Column
SellPositioningRateDOM NUMERIC(17,2),
SellPositioningUnit NUMERIC(1),
BuyPositioningRateIntl NUMERIC(17,2),
BuyPositioningRateDOM NUMERIC(17,2),
StandardCrewIntl NUMERIC(2),	
StandardCrewDOM	NUMERIC(2),	
SellStdCrewRonIntl NUMERIC(17,2),
SellStdCrewRONDOM NUMERIC(17,2),
BuyRONStdCrewIntl NUMERIC(17,2),
BuyRONStdCrewDOM NUMERIC(17,2),
SellAdditionalCrewIntl NUMERIC(17,2),
SellAdditionalCrewDOM NUMERIC(17,2),
BuyAdditionalCrewIntl NUMERIC(17,2),
BuyAdditionalCrewDOM NUMERIC(17,2),
SellAdditionalCrewRONIntl NUMERIC(17,2),
SellAdditionalCrewRONDOM NUMERIC(17,2),
BuyRONAddCrewIntl NUMERIC(17,2),
BuyRONAddCrewDOM NUMERIC(17,2),
SellWaitingTMIntl NUMERIC(17,2),
SellWaitTMDOM NUMERIC(17,2),
BuyWaitTimeIntl NUMERIC(17,2),
BuyWaitTimeDOM NUMERIC(17,2),
SellLandingFeesIntl NUMERIC(17,2),
SellLandingFeeDOM NUMERIC(17,2),
BuyLandingFeeIntl NUMERIC(17,2),
BuyLandingFeeDOM NUMERIC(17,2),
MinimumDay NUMERIC(17,2),
PercentageCost NUMERIC(5,2),
IsChargeTaxIntl	BIT,
IsChargeTaxDOM BIT,
IsSellAircraftPositioningIntl BIT,
IsPostioningTaxDOM BIT,
IsRONTaxIntl BIT,
IsRONTaxDOM BIT,
IsAdditionalCrewTaxIntl BIT,
IsAdditionalCrewTaxDOM BIT,
IsAdditionalRONTaxIntl BIT,
IsAdditionalCrewRONTaxDOM BIT,
IsWaitTMTaxIntl BIT,
IsWaitTaxDOM BIT,
IsLandingTaxIntl BIT,
IsLandingTaxDOM BIT,
IsChgDescriptionIntl BIT,
IsChgDescriptionDOM BIT,
IsPositioningDescriptionIntl BIT,
IsPositoningDescriptionDOM BIT,
IsRONDescriptionIntl BIT,
IsRONDescriptionDOM BIT,
IsAdditionalCrewDescriptionIntl BIT,
IsAdditionalCrewDescriptionDOM BIT,
IsAdditionalRONDescriptionIntl BIT,
IsAdditionalRONDescriptionDOM BIT,
IsWaitTMDescriptionIntl BIT,
IsWaitDescriptionDOM BIT,
IsLandingDescriptionIntl BIT,
IsLandingDescriptionDOM BIT,
YearMade CHAR(4),
ExteriorColor VARCHAR(25),
ColorIntl VARCHAR(25),
IsAFIS BIT,
IsUWAData BIT,
DBAircraftFlightChg VARCHAR(32), -- Rename Column
DSAircraftFlightChg VARCHAR(32), -- New Column
BuyAircraftFlightIntl VARCHAR(32),
IsSellAircraftFlightIntl VARCHAR(32),
BuyAircraftPositionDOM VARCHAR(32),
SellAircraftPositioningDOM VARCHAR(32),
BuyAircraftPositioningIntl VARCHAR(32),
SellAircraftPositioningIntl VARCHAR(32),
BuyAircraftAdditionalCrewDOM VARCHAR(32),
SellAircraftAdditionalCrewDOM VARCHAR(32),
InternationalBuyAircraftAdditionalCrew VARCHAR(32),
SellAircraftAdditionalCrewIntl VARCHAR(32),
BuyAircraftCrewRONDOM VARCHAR(32),
SellAircraftAdditionalCrewRONDOM VARCHAR(32),
BuyAircraftAdditionalCrewRONIntl VARCHAR(32),
SellAircraftAdditionalCrewRONIntl VARCHAR(32),
BuyAircraftStdCrewDOM VARCHAR(32),
SellAircraftStdCrewDOM VARCHAR(32),
BuyAircraftStdCrewIntl VARCHAR(32),
SellAircraftStdCrewIntl VARCHAR(32),
BuyAircraftWaitingDOM VARCHAR(32),
SellAircraftWaitTMDOM VARCHAR(32),
BuyAircraftWaitingIntl VARCHAR(32),
SellAircraftWaitingTMIntl VARCHAR(32),
BuyAircraftLandingDOM VARCHAR(32),
SellAircraftLandingDOM VARCHAR(32),
BuyAircraftLandingIntl VARCHAR(32),
SellAircraftLandingIntl VARCHAR(32),
BuyAircraftAdditionalFeeDOM VARCHAR(32),
SellAircraftAdditionalFeeDOM VARCHAR(32),
BuyAircraftAdditionalFeeIntl VARCHAR(32),
SellAircraftAdditionalFeeIntl VARCHAR(32),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetCharterRate PRIMARY KEY CLUSTERED(FleetCharterRateID ASC),
CONSTRAINT FKFleetCharterRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetCharterRateFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetCharterRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetNewCharterRate
(
FleetNewCharterRateID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
OrderNum INT,
FleetNewCharterRateDescription VARCHAR(40),
ChargeUnit VARCHAR(25),
NegotiatedChgUnit NUMERIC(2),
BuyDOM NUMERIC(17,2),
SellDOM NUMERIC(17,2),
IsTaxDOM BIT,
IsDiscountDOM BIT,
BuyIntl NUMERIC(17,2),
SellIntl NUMERIC(17,2),
IsTaxIntl BIT,
IsDiscountIntl BIT,
StandardCrewDOM NUMERIC(2),
StandardCrewIntl NUMERIC(2),
MinimumDailyRequirement NUMERIC(17,2),
YearMade CHAR(4),
ExteriorColor VARCHAR(25),
ColorIntl VARCHAR(25),
IsAFIS BIT,
IsUWAData BIT,
DBAircraftFlightChg VARCHAR(32), -- Rename Column
DSAircraftFlightChg VARCHAR(32), -- New Column
BuyAircraftFlightIntl VARCHAR(32),
IsSellAircraftFlightIntl VARCHAR(32),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
AircraftTypeCD CHAR(10),
IsDeleted BIT,
CONSTRAINT PKFleetNewCharterRate PRIMARY KEY CLUSTERED(FleetNewCharterRateID ASC),
CONSTRAINT FKFleetNewCharterRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetNewCharterRateFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetNewCharterRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetInformation
(
FleetInformationID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
AirframeHrs NUMERIC(11,3),
AirframeCycle NUMERIC(6),
AIrframeAsOfDT DATE,
Engine1Hrs NUMERIC(11,3),
Engine1Cycle NUMERIC(6),
Engine1AsOfDT DATE,
Engine2Hrs NUMERIC(11,3),
Engine2Cycle NUMERIC(6),
Engine2AsOfDT DATE,	
Engine3Hrs NUMERIC(11,3),
Engine3Cycle NUMERIC(6),
Engine3AsOfDT DATE,	
Engine4Hrs NUMERIC(11,3),
Engine4Cycle NUMERIC(6),
Engine4AsOfDT DATE,
Reverse1Hrs NUMERIC(11,3),
Reverser1Cycle NUMERIC(6),
Reverser1AsOfDT DATE,
Reverse2Hrs NUMERIC(11,3),
Reverser2Cycle NUMERIC(6),
Reverser2AsOfDT DATE,
Reverse3Hrs NUMERIC(11,3),
Reverser3Cycle NUMERIC(6),
Reverser3AsOfDT DATE,
Reverse4Hrs NUMERIC(11,3),
Reverser4Cycle NUMERIC(6),
Reverser4AsOfDT DATE,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
MACHFuel1Hrs NUMERIC(5),
MACHFuel2Hrs NUMERIC(5),
MACHFuel3Hrs NUMERIC(5),
MACHFuel4Hrs NUMERIC(5),
MACHFuel5Hrs NUMERIC(5),
MACHFuel6Hrs NUMERIC(5),
MACHFuel7Hrs NUMERIC(5),
MACHFuel8Hrs NUMERIC(5),
MACHFuel9Hrs NUMERIC(5),
MACHFuel10Hrs NUMERIC(5),
MACHFuel11Hrs NUMERIC(5),
MACHFuel12Hrs NUMERIC(5),
MACHFuel13Hrs NUMERIC(5),
MACHFuel14Hrs NUMERIC(5),
MACHFuel15Hrs NUMERIC(5),
LongRangeFuel1Hrs NUMERIC(5),
LongRangeFuel2Hrs NUMERIC(5),
LongRangeFuel3Hrs NUMERIC(5),
LongRangeFuel4Hrs NUMERIC(5),
LongRangeFuel5Hrs NUMERIC(5),
LongRangeFuel6Hrs NUMERIC(5),
LongRangeFuel7Hrs NUMERIC(5),
LongRangeFuel8Hrs NUMERIC(5),
LongRangeFuel9Hrs NUMERIC(5),
LongRangeFuel10Hrs NUMERIC(5),
LongRangeFuel11Hrs NUMERIC(5),
LongRangeFuel12Hrs NUMERIC(5),
LongRangeFuel13Hrs NUMERIC(5),
LongRangeFuel14Hrs NUMERIC(5),
LongRangeFuel15Hrs NUMERIC(5),
HighSpeedFuel1Hrs NUMERIC(5),
HighSpeedFuel2Hrs NUMERIC(5),
HighSpeedFuel3Hrs NUMERIC(5),
HighSpeedFuel4Hrs NUMERIC(5),
HighSpeedFuel5Hrs NUMERIC(5),
HighSpeedFuel6Hrs NUMERIC(5),
HighSpeedFuel7Hrs NUMERIC(5),
HighSpeedFuel8Hrs NUMERIC(5),
HighSpeedFuel9Hrs NUMERIC(5),
HighSpeedFuel10Hrs NUMERIC(5),
HighSpeedFuel11Hrs NUMERIC(5),
HighSpeedFuel12Hrs NUMERIC(5),
HighSpeedFuel13Hrs NUMERIC(5),
HighSpeedFuel14Hrs NUMERIC(5),
HighSpeedFuel51Hrs NUMERIC(5),
Fuel1Hrs NUMERIC(5),
Fuel2Hrs NUMERIC(5),
Fuel3Hrs NUMERIC(5),
Fuel4Hrs NUMERIC(5),
Fuel5Hrs NUMERIC(5),
IsDeleted BIT,
CONSTRAINT PKFleetInformation PRIMARY KEY CLUSTERED(FleetInformationID ASC),
CONSTRAINT FKFleetInformationCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetInformationFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetInformationUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetForeCast
(
FleetForeCastID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
AircraftCD CHAR(3), -- Unused Column
YearMonthDate DATE,
ForecastHrs NUMERIC(6),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetForeCast PRIMARY KEY CLUSTERED(FleetForeCastID ASC),
CONSTRAINT FKFleetForeCastCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetForeCastFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetForeCastUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetProfileInformation
(
FleetProfileInformationID BIGINT,
FleetInfoCD CHAR(3),
CustomerID BIGINT,
FleetProfileAddInfDescription VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFleetProfileInformation PRIMARY KEY CLUSTERED(FleetProfileInformationID),
CONSTRAINT FKFleetProfileInformationCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetProfileInformationClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKFleetProfileInformationUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FleetProfileDefinition
(
FleetProfileInfoXRefID BIGINT,
FleetID BIGINT,
CustomerID BIGINT,
FleetProfileInformationID BIGINT,
FleetDescription VARCHAR(25),
InformationValue VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsPrintable BIT,
IsDeleted BIT,
CONSTRAINT PKFleetProfileDefinition PRIMARY KEY CLUSTERED(FleetProfileInfoXRefID),
CONSTRAINT FKFleetProfileDefinitionFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKFleetProfileDefinitionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFleetProfileDefinitionFleetProfileInformation FOREIGN KEY(FleetProfileInformationID) REFERENCES FleetProfileInformation(FleetProfileInformationID),
CONSTRAINT FKFleetProfileDefinitionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE ConversionTable
(
ConversionTableID BIGINT,
CustomerID BIGINT,
HomebaseID BIGINT,
Tenths NUMERIC(3,1),
StartMinimum NUMERIC(2),
EndMinutes NUMERIC(2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKConversionTable PRIMARY KEY CLUSTERED(ConversionTableID ASC),
CONSTRAINT FKConversionTableCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKConversionTableCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKConversionTableUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE DBGeneralInfo
(
DBGeneralInfoID BIGINT,
CustomerID BIGINT,
FileLocationTSA VARCHAR(100),
IsDeleted BIT,
CONSTRAINT PKDBGeneralInfo PRIMARY KEY CLUSTERED(DBGeneralInfoID ASC),
CONSTRAINT FKDBGeneralInfoCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE TABLE DelayType
(
DelayTypeID BIGINT,
CustomerID BIGINT,
DelayTypeCD CHAR(2),
DelayTypeDescription VARCHAR(25),
ClientID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsSiflExcluded BIT,
IsDeleted BIT,
CONSTRAINT PKDelayType PRIMARY KEY CLUSTERED(DelayTypeID ASC),
CONSTRAINT FKDelayTypeCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKDelayTypeClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKDelayTypeUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE ExchangeRate
(
ExchangeRateID BIGINT,
CustomerID BIGINT,
ExchRateCD CHAR(6),
ExchRateDescription VARCHAR(30) NOT NULL,
FromDT DATE,
ToDT DATE,
ExchRate NUMERIC(10,5),
CurrencyID NUMERIC(3,0),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKExchangeRate PRIMARY KEY CLUSTERED(ExchangeRateID ASC),
CONSTRAINT FKExchangeRateCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKExchangeRateUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TravelCoordinator
(
TravelCoordinatorID BIGINT,
CustomerID BIGINT,
TravelCoordCD CHAR(5),
FirstName VARCHAR(30),
MiddleName VARCHAR(30),
LastName VARCHAR(30),
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
PagerNum VARCHAR(25),
CellPhoneNum VARCHAR(25),
EmailID VARCHAR(100),
Notes TEXT,
HomebaseID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTravelCoordinator PRIMARY KEY CLUSTERED(TravelCoordinatorID ASC),
CONSTRAINT FKTravelCoordinatorCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTravelCoordinatorCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKTravelCoordinatorUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TravelCoordinatorFleet
(
TravelCoordinatorFleetID BIGINT,
CustomerID BIGINT,
TravelCoordinatorID BIGINT,
FleetID BIGINT,
IsChoice BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTravelCoordinatorFleet PRIMARY KEY CLUSTERED(TravelCoordinatorFleetID ASC),
CONSTRAINT FKTravelCoordinatorFleetCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTravelCoordinatorFleetTravelCoordinator FOREIGN KEY(TravelCoordinatorID) REFERENCES TravelCoordinator(TravelCoordinatorID),
CONSTRAINT FKTravelCoordinatorFleetFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKTravelCoordinatorFleetUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TravelCoordinatorRequestor
(
TravelCoordinatorRequestorID BIGINT,
CustomerID BIGINT,
TravelCoordinatorID BIGINT,
PassengerRequestorID BIGINT,
IsChoice BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTravelCoordinatorRequestor PRIMARY KEY CLUSTERED(TravelCoordinatorRequestorID ASC),
CONSTRAINT FKTravelCoordinatorRequestorCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTravelCoordinatorRequestorTravelCoordinator FOREIGN KEY(TravelCoordinatorID) REFERENCES TravelCoordinator(TravelCoordinatorID),
CONSTRAINT FKTravelCoordinatorRequestorPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKTravelCoordinatorRequestorUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TSFlightLog
(
TSFlightLogID BIGINT,
CustomerID BIGINT,
PrimeKey INT,
HomebaseID BIGINT,
OriginalDescription VARCHAR(25),
CustomDescription VARCHAR(25),
SequenceOrder INT,
IsPrint BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
Category CHAR(10),
IsDeleted BIT,
CONSTRAINT PKTSFlightLog PRIMARY KEY CLUSTERED(TSFlightLogID ASC),
CONSTRAINT FKTSFlightLogCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTSFlightLogCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKTSFlightLogUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TSDefinitionLog
(
TSDefinitionLogID BIGINT,
CustomerID BIGINT,
PrimeKey INT,
HomebaseID BIGINT,
OriginalDescription VARCHAR(25),
CustomDescription VARCHAR(25),
SequenceOrder INT,
IsPrint BIT,
Category CHAR(10),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTSDefinitionLog PRIMARY KEY CLUSTERED(TSDefinitionLogID ASC),
CONSTRAINT FKTSDefinitionLogCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTSDefinitionLogCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKTSDefinitionLogUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 03-Jul-2012 --

CREATE TABLE WorldWind
(
WorldWindID BIGINT,
CustomerID BIGINT,
DepartureZoneAirport BIGINT,
ArrivalZoneAirport BIGINT,
DepartureCity VARCHAR(25),
ArrivalCity VARCHAR(25),
WorldWindQuarter CHAR(1),
Altitude CHAR(1),
Direction INT,
ReturnDirection INT,
WorldWindStandard INT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKWorldWind PRIMARY KEY CLUSTERED(WorldWindID ASC),
CONSTRAINT FKWorldWindCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKWorldWindDAirport FOREIGN KEY(DepartureZoneAirport) REFERENCES Airport(AirportID),
CONSTRAINT FKWorldWindAAirport FOREIGN KEY(ArrivalZoneAirport) REFERENCES Airport(AirportID),
CONSTRAINT FKWorldWindUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE WindStat
(
WindStatID BIGINT,
Hemisphere CHAR(1),
Quadrilateral CHAR(1),
Altitude CHAR(1),
WindStatQuarter CHAR(1),
Wind INT,
WindStatStandard INT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKWindStat PRIMARY KEY CLUSTERED(WindStatID ASC),
CONSTRAINT FKWindStatUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE VendorContact
(
VendorContactID BIGINT,
CustomerID BIGINT,
VendorID BIGINT,
ContactName VARCHAR(40),
VendorType CHAR(1),
VendorContactCD INT IDENTITY(1,1),
IsChoice BIT,
Addr1 VARCHAR(40),
Addr2 VARCHAR(40),
CityName VARCHAR(40),
StateName CHAR(10),
PostalZipCD CHAR(15),
CountryID BIGINT,
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
CreditName1 VARCHAR(25),
CreditNum1 VARCHAR(30),
CreditName2 VARCHAR(25),
CreditNum2 VARCHAR(30),
MoreInfo TEXT,
Notes TEXT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
Title VARCHAR(40),
LastName VARCHAR(40),
FirstName VARCHAR(40),
MiddleName VARCHAR(40),
EmailID VARCHAR(100),
IsDeleted BIT,
CONSTRAINT PKVendorContact PRIMARY KEY CLUSTERED(VendorContactID ASC),
CONSTRAINT FKVendorContactCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKVendorContactVendor FOREIGN KEY(VendorID) REFERENCES Vendor(VendorID),
CONSTRAINT FKVendorContactCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKVendorContactUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 04-Jul-2012 --

ALTER TABLE Aircraft DROP CONSTRAINT FKAircraftClient
GO

ALTER TABLE Aircraft ADD CONSTRAINT FKAircraftClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
GO

-- Database Change Log 05-Jul-2012 --

CREATE TABLE FileWarehouse
(
FileWarehouseID BIGINT,
UWAFileName VARCHAR(100),
UWAFilePath VARBINARY(MAX),
UWAWebpageName VARCHAR(100),
CustomerID BIGINT,
RecordID BIGINT,
SequenceNumber INT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKFileWarehouse PRIMARY KEY CLUSTERED(FileWarehouseID ASC),
CONSTRAINT FKFileWarehouseCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFileWarehouseUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE FlightpakFuel
(
FlightpakFuelID	BIGINT,
CustomerID BIGINT,
DepartureICAOID BIGINT,
FBOName CHAR(25),
GallonFrom NUMERIC(17,3),
GallonTO NUMERIC(17,3),
EffectiveDT DATE,
UnitPrice NUMERIC(15,4),
FuelVendorID BIGINT,
UpdateDTTM DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsActive BIT,		
CONSTRAINT PKFlightpakFuel PRIMARY KEY CLUSTERED(FlightpakFuelID ASC),
CONSTRAINT FKFlightpakFuelCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKFlightpakFuelAirport FOREIGN KEY(DepartureICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKFlightpakFuelFuelVendor FOREIGN KEY(FuelVendorID) REFERENCES FuelVendor(FuelVendorID),
CONSTRAINT FKFlightpakFuelUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

ALTER TABLE DepartmentAuthorization DROP CONSTRAINT FKDepartmentAuthorizationDepartment
GO

ALTER TABLE DepartmentAuthorization ADD CONSTRAINT FKDepartmentAuthorizationDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID)
GO

-- Database Change Log 06-Jul-2012 --

ALTER TABLE Crew DROP CONSTRAINT FKCrewCompany
GO

UPDATE Crew SET HomebaseID = 1001
GO

ALTER TABLE Crew ADD CONSTRAINT FKCrewAirport FOREIGN KEY(HomebaseID) REFERENCES Airport(AirportID)
GO

ALTER TABLE Passenger DROP CONSTRAINT FKPassengerCompany
GO

UPDATE Passenger SET HomebaseID = 1001
GO

ALTER TABLE Passenger ADD CONSTRAINT FKPassengerAirport FOREIGN KEY(HomebaseID) REFERENCES Airport(AirportID)
GO

-- Database Change Log 10-Jul-2012 --

ALTER TABLE Crew ALTER COLUMN CrewCD CHAR(5)
GO

ALTER TABLE Company ADD FileWarehouseID BIGINT
GO

ALTER TABLE Company ADD CONSTRAINT FKCompanyFileWarehouse FOREIGN KEY(FileWarehouseID) REFERENCES FileWarehouse(FileWarehouseID)
GO

ALTER TABLE Fleet ADD FileWarehouseID BIGINT
GO

ALTER TABLE Fleet ADD CONSTRAINT FKFleetFileWarehouse FOREIGN KEY(FileWarehouseID) REFERENCES FileWarehouse(FileWarehouseID)
GO

ALTER TABLE Crew ADD FileWarehouseID BIGINT
GO

ALTER TABLE Crew ADD CONSTRAINT FKCrewFileWarehouse FOREIGN KEY(FileWarehouseID) REFERENCES FileWarehouse(FileWarehouseID)
GO

ALTER TABLE Passenger ADD FileWarehouseID BIGINT
GO

ALTER TABLE Passenger ADD CONSTRAINT FKPassengerFileWarehouse FOREIGN KEY(FileWarehouseID) REFERENCES FileWarehouse(FileWarehouseID)
GO

ALTER TABLE CrewPassengerPassport DROP COLUMN PassportCountry
GO

ALTER TABLE CrewPassengerPassport ADD CountryID BIGINT
GO

ALTER TABLE CrewPassengerPassport ADD CONSTRAINT FKCrewPassengerPassportCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
GO

-- Database Change Log 11-Jul-2012 --

ALTER TABLE FileWarehouse ADD RecordType VARCHAR(20)
GO

ALTER TABLE Company DROP CONSTRAINT FKCompanyFileWarehouse
GO

ALTER TABLE Company DROP COLUMN FileWarehouseID
GO

ALTER TABLE Fleet DROP CONSTRAINT FKFleetFileWarehouse
GO

ALTER TABLE Fleet DROP COLUMN FileWarehouseID
GO

ALTER TABLE Crew DROP CONSTRAINT FKCrewFileWarehouse
GO

ALTER TABLE Crew DROP COLUMN FileWarehouseID
GO

ALTER TABLE Passenger DROP CONSTRAINT FKPassengerFileWarehouse
GO

ALTER TABLE Passenger DROP COLUMN FileWarehouseID
GO

ALTER TABLE Vendor DROP CONSTRAINT FKVendorCompany
GO

UPDATE Vendor SET HomebaseID = 1001
GO

ALTER TABLE Vendor ADD CONSTRAINT FKVendorHomebaseAirport FOREIGN KEY(HomebaseID) REFERENCES Airport(AirportID)
GO

DROP INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
(
CustomerID ASC, CrewID ASC, PassportNum ASC
)
GO

DROP INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
(
CustomerID ASC, PassengerRequestorID ASC, PassportNum ASC
)
GO

DROP INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
(
CustomerID ASC, CrewID ASC, VisaNum ASC
)
GO

DROP INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
(
CustomerID ASC, PassengerRequestorID ASC, VisaNum ASC
)
GO

-- Database Change Log 12-Jul-2012 --

ALTER TABLE WorldWind DROP CONSTRAINT FKWorldWindDAirport
GO

ALTER TABLE WorldWind DROP CONSTRAINT FKWorldWindAAirport
GO

-- Database Change Log 17-Jul-2012 --

ALTER TABLE DSTRegion ALTER COLUMN DSTRegionCD CHAR(10)
GO

ALTER TABLE DSTRegion ADD UpdateDT DATETIME
GO

ALTER TABLE DSTRegion ADD UWAUpdates BIT
GO

CREATE TABLE TailHistory
(
TailHistoryID BIGINT,
CustomerID BIGINT,
NewTailNUM CHAR(6),
OldTailNUM CHAR(6),
TailNUMChgDT DATE,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
IsReassigned BIT,
IsDeleted BIT,
CONSTRAINT PKTailHistory PRIMARY KEY CLUSTERED(TailHistoryID ASC),
CONSTRAINT FKTailHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTailHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

DROP INDEX HotelIDX ON Hotel
GO

ALTER TABLE Airport ADD RunwayID1 CHAR(20)
GO

ALTER TABLE Airport ADD RunwayID2 CHAR(20)
GO

ALTER TABLE Airport ADD Runway2Length NUMERIC(6)
GO

ALTER TABLE Airport ADD Runway2Width NUMERIC(6)
GO
 
ALTER TABLE Airport ADD RunwayID3 CHAR(20)
GO

ALTER TABLE Airport ADD Runway3Length NUMERIC(6)
GO

ALTER TABLE Airport ADD Runway3Width NUMERIC(6)
GO
 
ALTER TABLE Airport ADD RunwayID4 CHAR(20)
GO

ALTER TABLE Airport ADD Runway4Length NUMERIC(6)
GO

ALTER TABLE Airport ADD Runway4Width NUMERIC(6)
GO

-- Database Change Log 19-Jul-2012 --

ALTER TABLE FleetChargeRate ADD AircraftID BIGINT
GO

ALTER TABLE FleetChargeRate ADD CONSTRAINT FKFleetChargeRateAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID)
GO

CREATE TABLE RoomType
(
RoomTypeID BIGINT,
RoomDescription VARCHAR(100),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT
CONSTRAINT PKRoomType PRIMARY KEY CLUSTERED(RoomTypeID ASC)
)
GO

ALTER TABLE Crew ALTER COLUMN SSN NVARCHAR(40)
GO

ALTER TABLE Passenger ALTER COLUMN PersonalIDNum NVARCHAR(40)
GO

DROP INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
GO

DROP INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
GO

ALTER TABLE CrewPassengerVisa ALTER COLUMN VisaNum NVARCHAR(120)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
(
CustomerID ASC, CrewID ASC, VisaNum ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
(
CustomerID ASC, PassengerRequestorID ASC, VisaNum ASC
)
GO

DROP INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
GO

DROP INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
GO

ALTER TABLE CrewPassengerPassport ALTER COLUMN PassportNum NVARCHAR(120)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
(
CustomerID ASC, CrewID ASC, PassportNum ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
(
CustomerID ASC, PassengerRequestorID ASC, PassportNum ASC
)
GO

ALTER TABLE CrewDefinition ALTER COLUMN InformationValue NVARCHAR(250)
GO

ALTER TABLE PassengerAdditionalInfo ALTER COLUMN AdditionalINFOValue NVARCHAR(120)
GO

-- Database Change Log 23-Jul-2012 --

CREATE TABLE TSAClear
(
TSACleartID BIGINT,
CustomerID BIGINT,
SequenceID CHAR(10),
Cleared CHAR(3),
FirstName VARCHAR(60),
MiddleName VARCHAR(60),
LastName VARCHAR(60),
TSAType CHAR(10),
DOB DATETIME,
POB VARCHAR(50),
Citizen CHAR(18),
PPID VARCHAR(100),
MIC VARCHAR(300),
TSATime DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTSAClear PRIMARY KEY CLUSTERED(TSACleartID ASC),
CONSTRAINT FKTSAClearCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTSAClearUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TSANoFly
(
TSANoFlyID BIGINT,
CustomerID BIGINT,
SequenceID CHAR(10),
Cleared CHAR(3),
FirstName VARCHAR(60),
MiddleName VARCHAR(60),
LastName VARCHAR(60),
TSAType CHAR(10),
DOB DATETIME,
POB VARCHAR(50),
Citizen CHAR(18),
PPID VARCHAR(100),
MIC VARCHAR(300),
TSATime DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTSANoFly PRIMARY KEY CLUSTERED(TSANoFlyID),
CONSTRAINT FKTSANoFlyCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTSANoFlyUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TSASelect
(
TSASelectID BIGINT,
CustomerID BIGINT,
SequenceID CHAR(10),
Cleared CHAR(3),
FirstName VARCHAR(60),
MiddleName VARCHAR(60),
LastName VARCHAR(60),
TSAType CHAR(10),
DOB DATETIME,
POB VARCHAR(50),
Citizen CHAR(18),
PPID VARCHAR(100),
MIC VARCHAR(300),
TSATime DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKTSASelectID PRIMARY KEY CLUSTERED(TSASelectID),
CONSTRAINT FKTSASelectCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTSASelectUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 24-Jul-2012 --

--sp_rename 'CrewRating.CrewID','CrewRatingID','COLUMN'

-- Database Change Log 25-Jul-2012 --

ALTER TABLE CrewRating DROP CONSTRAINT PKCrewRating
GO

ALTER TABLE CrewRating ADD CrewRatingID BIGINT NOT NULL DEFAULT 1
GO

UPDATE CrewRating SET CrewRatingID = CrewID

ALTER TABLE CrewRating ADD CONSTRAINT PKCrewRating PRIMARY KEY CLUSTERED(CrewRatingID)
GO

-- Database Change Log 26-Jul-2012 --

ALTER TABLE FleetPair DROP COLUMN ISO
GO

ALTER TABLE FleetPair ADD OCountryID BIGINT
GO

ALTER TABLE FleetPair ADD CONSTRAINT FleetPairOCountry FOREIGN KEY(OCountryID) REFERENCES Country(CountryID)
GO

-- Database Change Log 09-Aug-2012 --

ALTER TABLE Company ADD HomebaseAirportID BIGINT
GO

ALTER TABLE Company ADD CONSTRAINT FKCompanyAirport FOREIGN KEY(HomebaseAirportID) REFERENCES Airport(AirportID)
GO

-- Database Change Log 16-Aug-2012 --

ALTER TABLE Metro ADD CustomerID BIGINT
GO

ALTER TABLE Metro ADD CONSTRAINT FKMetroCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

-- Database Change Log 20-Aug-2012 --

CREATE UNIQUE NONCLUSTERED INDEX CompanyIDX ON Company
(
CustomerID ASC, HomebaseAirportID ASC
)
GO

-- Database Change Log 22-Aug-2012 --

CREATE UNIQUE NONCLUSTERED INDEX CustomerIsDeletedIDX ON Airport
(
CustomerID ASC, IcaoID ASC, IsDeleted ASC
)
GO

-- Database Change Log 23-Aug-2012 --

ALTER TABLE Company DROP COLUMN HomebaseCD
GO

-- Database Change Log 29-Aug-2012 --

CREATE NONCLUSTERED INDEX IcaoIDCustomerIDIsDeletedIDX ON Airport
(
IcaoID ASC, CustomerID ASC, IsDeleted ASC
)
GO

-- Database Change Log 03-Sep-2012 --

CREATE NONCLUSTERED INDEX AirportForGridIDX ON Airport
(
IsDeleted ASC,
CustomerID ASC,
IcaoID ASC,
CityName ASC,
StateName ASC,
CountryID ASC,
AirportName ASC,
LastUpdTS ASC,
IsInActive ASC,
IsWorldClock ASC
)
GO

CREATE NONCLUSTERED INDEX CustomerIDIsDeletedRecordCountIDX ON Airport
(
CustomerID ASC, IsDeleted ASC
)
GO

-- Database Change Log 07-Sep-2012 --

ALTER TABLE Crew ADD BusinessPhone VARCHAR(25)
ALTER TABLE Crew ADD BusinessFax VARCHAR(25)
ALTER TABLE Crew ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE Crew ADD OtherPhone VARCHAR(25)
ALTER TABLE Crew ADD PersonalEmail VARCHAR(250)
ALTER TABLE Crew ADD OtherEmail VARCHAR(250)
ALTER TABLE Crew ADD Addr3 VARCHAR(40)
ALTER TABLE Crew ADD PIC121 BIT
ALTER TABLE Crew ADD PIC125 BIT
ALTER TABLE Crew ADD SIC121 BIT
ALTER TABLE Crew ADD SIC125 BIT
ALTER TABLE Crew ADD Attendant121 BIT
ALTER TABLE Crew ADD Attendant125 BIT

ALTER TABLE Passenger ADD PrimaryMobile VARCHAR(25)
ALTER TABLE Passenger ADD BusinessFax VARCHAR(25)
ALTER TABLE Passenger ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE Passenger ADD OtherPhone VARCHAR(25)
ALTER TABLE Passenger ADD PersonalEmail VARCHAR(250)
ALTER TABLE Passenger ADD OtherEmail VARCHAR(250)
ALTER TABLE Passenger ADD Addr3 VARCHAR(40)

ALTER TABLE EmergencyContact ADD BusinessPhone VARCHAR(25)
ALTER TABLE EmergencyContact ADD BusinessFax VARCHAR(25)
ALTER TABLE EmergencyContact ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE EmergencyContact ADD OtherPhone VARCHAR(25)
ALTER TABLE EmergencyContact ADD PersonalEmail VARCHAR(250)
ALTER TABLE EmergencyContact ADD OtherEmail VARCHAR(250)
ALTER TABLE EmergencyContact ADD Addr3 VARCHAR(40)

ALTER TABLE Fleet ADD SecondaryDomFlightPhone VARCHAR(25)
ALTER TABLE Fleet ADD SecondaryIntlFlightPhone VARCHAR(25)
ALTER TABLE Fleet ADD BusinessPhone VARCHAR(25)
ALTER TABLE Fleet ADD OPCellPhoneNum VARCHAR(25)
ALTER TABLE Fleet ADD OPHomeFax VARCHAR(25)
ALTER TABLE Fleet ADD OPCellPhoneNum2 VARCHAR(25)
ALTER TABLE Fleet ADD OPOtherPhone VARCHAR(25)
ALTER TABLE Fleet ADD OPPersonalEmail VARCHAR(250)
ALTER TABLE Fleet ADD OPOtherEmail VARCHAR(250)
ALTER TABLE Fleet ADD OPAddr3 VARCHAR(40)
ALTER TABLE Fleet ADD OPBusinessPhone2 VARCHAR(25)
ALTER TABLE Fleet ADD OLCellPhoneNum VARCHAR(25)
ALTER TABLE Fleet ADD OLHomeFax VARCHAR(25)
ALTER TABLE Fleet ADD OLCellPhoneNum2 VARCHAR(25)
ALTER TABLE Fleet ADD OLOtherPhone VARCHAR(25)
ALTER TABLE Fleet ADD OLPersonalEmail VARCHAR(250)
ALTER TABLE Fleet ADD OLOtherEmail VARCHAR(250)
ALTER TABLE Fleet ADD OLAddr3 VARCHAR(40)

ALTER TABLE Vendor ADD TollFreePhone VARCHAR(25)
ALTER TABLE Vendor ADD BusinessEmail VARCHAR(250)
ALTER TABLE Vendor ADD Website VARCHAR(200)
ALTER TABLE Vendor ADD BillingAddr3 VARCHAR(40)
ALTER TABLE Vendor ADD BusinessPhone VARCHAR(25)
ALTER TABLE Vendor ADD CellPhoneNum VARCHAR(25)
ALTER TABLE Vendor ADD HomeFax VARCHAR(25)
ALTER TABLE Vendor ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE Vendor ADD OtherPhone VARCHAR(25)
ALTER TABLE Vendor ADD MCBusinessEmail VARCHAR(250)
ALTER TABLE Vendor ADD PersonalEmail VARCHAR(250)
ALTER TABLE Vendor ADD OtherEmail VARCHAR(250)
ALTER TABLE Vendor ADD MainContactAddr1 VARCHAR(40)
ALTER TABLE Vendor ADD MainContactAddr2 VARCHAR(40)
ALTER TABLE Vendor ADD MainContactAddr3 VARCHAR(40)

ALTER TABLE TravelCoordinator ADD BusinessPhone VARCHAR(25)
ALTER TABLE TravelCoordinator ADD HomeFax VARCHAR(25)
ALTER TABLE TravelCoordinator ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE TravelCoordinator ADD OtherPhone VARCHAR(25)
ALTER TABLE TravelCoordinator ADD BusinessEmail VARCHAR(250)
ALTER TABLE TravelCoordinator ADD PersonalEmail VARCHAR(250)
ALTER TABLE TravelCoordinator ADD OtherEmail VARCHAR(250)
ALTER TABLE TravelCoordinator ADD Addr3 VARCHAR(40)

ALTER TABLE FuelVendor ADD TollFreePhone VARCHAR(25)
ALTER TABLE FuelVendor ADD BusinessEmail VARCHAR(250)
ALTER TABLE FuelVendor ADD Website VARCHAR(200)
ALTER TABLE FuelVendor ADD BusinessPhone VARCHAR(25)
ALTER TABLE FuelVendor ADD CellPhoneNum VARCHAR(25)
ALTER TABLE FuelVendor ADD HomeFax VARCHAR(25)
ALTER TABLE FuelVendor ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE FuelVendor ADD OtherPhone VARCHAR(25)
ALTER TABLE FuelVendor ADD MCBusinessEmail VARCHAR(250)
ALTER TABLE FuelVendor ADD PersonalEmail VARCHAR(250)
ALTER TABLE FuelVendor ADD OtherEmail VARCHAR(250)

ALTER TABLE FBO ADD ContactBusinessPhone VARCHAR(25)
ALTER TABLE FBO ADD ContactCellPhoneNum VARCHAR(25)
ALTER TABLE FBO ADD ContactEmail VARCHAR(250)
ALTER TABLE FBO ADD Addr3 VARCHAR(40)

ALTER TABLE Hotel ADD AltBusinessPhone VARCHAR(25)
ALTER TABLE Hotel ADD TollFreePhone VARCHAR(25)
ALTER TABLE Hotel ADD BusinessEmail VARCHAR(250)
ALTER TABLE Hotel ADD ContactBusinessPhone VARCHAR(25)
ALTER TABLE Hotel ADD CellPhoneNum VARCHAR(25)
ALTER TABLE Hotel ADD ContactEmail VARCHAR(250)

ALTER TABLE Transport ADD AltBusinessPhone VARCHAR(25)
ALTER TABLE Transport ADD BusinessEmail VARCHAR(250)
ALTER TABLE Transport ADD ContactBusinessPhone VARCHAR(25)
ALTER TABLE Transport ADD CellPhoneNum VARCHAR(25)
ALTER TABLE Transport ADD ContactEmail VARCHAR(250)
ALTER TABLE Transport ADD Addr1 VARCHAR(40)
ALTER TABLE Transport ADD Addr2 VARCHAR(40)
ALTER TABLE Transport ADD Addr3 VARCHAR(40)
ALTER TABLE Transport ADD City VARCHAR(25)
ALTER TABLE Transport ADD StateProvince VARCHAR(10)

ALTER TABLE Catering ADD BusinessEmail VARCHAR(250)
ALTER TABLE Catering ADD ContactBusinessPhone VARCHAR(25)
ALTER TABLE Catering ADD CellPhoneNum VARCHAR(25)
ALTER TABLE Catering ADD ContactEmail VARCHAR(250)
ALTER TABLE Catering ADD Addr1 VARCHAR(40)
ALTER TABLE Catering ADD Addr2 VARCHAR(40)
ALTER TABLE Catering ADD Addr3 VARCHAR(40)
ALTER TABLE Catering ADD City VARCHAR(25)
ALTER TABLE Catering ADD StateProvince VARCHAR(10)

CREATE NONCLUSTERED INDEX [AirportIDIsDeletedCustomerID]
ON [dbo].[Airport] ([IsDeleted],[CustomerID])
INCLUDE ([AirportID],[IcaoID],[CityName],[StateName],[CountryID],[AirportName],[LastUpdUID],[LastUpdTS],[IsInActive],[IsWorldClock],[UWAID])
GO

CREATE NONCLUSTERED INDEX [CustIDClientIDPassengerRequestorIDIsDeleted] ON [dbo].[PassengerAdditionalInfo]
(
 [CustomerID] ASC
)
INCLUDE ( [PassengerRequestorID],
[ClientID],
[IsDeleted]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE FBO ALTER COLUMN LastFuelPrice NUMERIC(9,4)
GO

-- Database Change Log 10-Sep-2012 --

ALTER TABLE Transport ADD MetroID BIGINT
GO
ALTER TABLE Transport ADD CountryID BIGINT
GO
ALTER TABLE Transport ADD CONSTRAINT FKTransportMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID)
GO
ALTER TABLE Transport ADD CONSTRAINT FKTransportCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
GO

ALTER TABLE Catering ADD MetroID BIGINT
GO
ALTER TABLE Catering ADD CountryID BIGINT
GO
ALTER TABLE Catering ADD CONSTRAINT FKCateringMetro FOREIGN KEY(MetroID) REFERENCES Metro(MetroID)
GO
ALTER TABLE Catering ADD CONSTRAINT FKCateringCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
GO

ALTER TABLE Vendor DROP COLUMN MainContactAddr1
ALTER TABLE Vendor DROP COLUMN MainContactAddr2
ALTER TABLE Vendor DROP COLUMN MainContactAddr3

ALTER TABLE FuelVendor DROP COLUMN TollFreePhone
ALTER TABLE FuelVendor DROP COLUMN BusinessEmail
ALTER TABLE FuelVendor DROP COLUMN Website
ALTER TABLE FuelVendor DROP COLUMN BusinessPhone
ALTER TABLE FuelVendor DROP COLUMN CellPhoneNum
ALTER TABLE FuelVendor DROP COLUMN HomeFax
ALTER TABLE FuelVendor DROP COLUMN CellPhoneNum2
ALTER TABLE FuelVendor DROP COLUMN OtherPhone
ALTER TABLE FuelVendor DROP COLUMN PersonalEmail
ALTER TABLE FuelVendor DROP COLUMN OtherEmail

ALTER TABLE Crew DROP COLUMN PIC121
ALTER TABLE Crew DROP COLUMN PIC125
ALTER TABLE Crew DROP COLUMN SIC121
ALTER TABLE Crew DROP COLUMN SIC125
ALTER TABLE Crew DROP COLUMN Attendant121
ALTER TABLE Crew DROP COLUMN Attendant125

ALTER TABLE VendorContact ADD TollFreePhone VARCHAR(25)
ALTER TABLE VendorContact ADD BusinessEmail	VARCHAR(250)
ALTER TABLE VendorContact ADD Website VARCHAR(200)
ALTER TABLE VendorContact ADD Addr3 VARCHAR(40)
ALTER TABLE VendorContact ADD BusinessPhone VARCHAR(25)
ALTER TABLE VendorContact ADD CellPhoneNum VARCHAR(25)
ALTER TABLE VendorContact ADD HomeFax VARCHAR(25)
ALTER TABLE VendorContact ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE VendorContact ADD OtherPhone VARCHAR(25)
ALTER TABLE VendorContact ADD PersonalEmail VARCHAR(250)
ALTER TABLE VendorContact ADD OtherEmail VARCHAR(250)

ALTER TABLE CrewRating ADD IsPIC121	BIT
ALTER TABLE CrewRating ADD IsPIC125	BIT
ALTER TABLE CrewRating ADD IsSIC121	BIT
ALTER TABLE CrewRating ADD IsSIC125	BIT
ALTER TABLE CrewRating ADD IsAttendant121 BIT
ALTER TABLE CrewRating ADD IsAttendant125 BIT

ALTER TABLE Fleet DROP COLUMN BusinessPhone
ALTER TABLE Fleet DROP COLUMN OPCellPhoneNum
ALTER TABLE Fleet DROP COLUMN OPHomeFax
ALTER TABLE Fleet DROP COLUMN OPCellPhoneNum2
ALTER TABLE Fleet DROP COLUMN OPOtherPhone
ALTER TABLE Fleet DROP COLUMN OPPersonalEmail
ALTER TABLE Fleet DROP COLUMN OPOtherEmail
ALTER TABLE Fleet DROP COLUMN OPAddr3
ALTER TABLE Fleet DROP COLUMN OPBusinessPhone2
ALTER TABLE Fleet DROP COLUMN OLCellPhoneNum
ALTER TABLE Fleet DROP COLUMN OLHomeFax
ALTER TABLE Fleet DROP COLUMN OLCellPhoneNum2
ALTER TABLE Fleet DROP COLUMN OLOtherPhone
ALTER TABLE Fleet DROP COLUMN OLPersonalEmail
ALTER TABLE Fleet DROP COLUMN OLOtherEmail
ALTER TABLE Fleet DROP COLUMN OLAddr3

ALTER TABLE FleetPair ADD BusinessPhone VARCHAR(25)
ALTER TABLE FleetPair ADD OPCellPhoneNum VARCHAR(25)
ALTER TABLE FleetPair ADD OPHomeFax VARCHAR(25)
ALTER TABLE FleetPair ADD OPCellPhoneNum2 VARCHAR(25)
ALTER TABLE FleetPair ADD OPOtherPhone VARCHAR(25)
ALTER TABLE FleetPair ADD OPPersonalEmail VARCHAR(250)
ALTER TABLE FleetPair ADD OPOtherEmail VARCHAR(250)
ALTER TABLE FleetPair ADD Address3 VARCHAR(40)
ALTER TABLE FleetPair ADD OPBusinessPhone2 VARCHAR(25)
ALTER TABLE FleetPair ADD OLCellPhoneNum VARCHAR(25)
ALTER TABLE FleetPair ADD OLHomeFax VARCHAR(25)
ALTER TABLE FleetPair ADD OLCellPhoneNum2 VARCHAR(25)
ALTER TABLE FleetPair ADD OLOtherPhone VARCHAR(25)
ALTER TABLE FleetPair ADD OLPersonalEmail VARCHAR(250)
ALTER TABLE FleetPair ADD OLOtherEmail VARCHAR(250)
ALTER TABLE FleetPair ADD Addr3 VARCHAR(40)

-- Database Change Log 12-Sep-2012 --

ALTER TABLE Crew ALTER COLUMN MiddleInitial VARCHAR(20) NULL
GO

-- Database Change Log 14-Sep-2012 --

DROP INDEX FlightPurposeIDX ON FlightPurpose
GO

--ALTER TABLE Fleet ADD MaximumCrew CHAR(3)
--GO

--Database Change by Ramesh on 14-Sep-2012 added new Table as per confirmation from Sujith

/****** Object:  Table [dbo].[FPExclTab]    Script Date: 09/14/2012 22:48:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FPExclTab](
	[ExclTabID] [int] NOT NULL,
	[EntityName] [varchar](50) NULL,
	[TableName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ExclTabID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
--END of Database Change by Ramesh on 14-Sep-2012

-- Database Change Log 25-Sep-2012 --

ALTER TABLE FareLevel ADD FareLevelNEW INT
GO

UPDATE FareLevel SET
FareLevelNEW = FareLevelCD

DROP INDEX FareLevelIDX ON FareLevel
GO

ALTER TABLE FareLevel DROP COLUMN FareLevelCD
GO

sp_rename 'FareLevel.FareLevelNEW','FareLevelCD', 'COLUMN'
GO

CREATE UNIQUE NONCLUSTERED INDEX FareLevelIDX ON FareLevel
(
FareLevelCD ASC, CustomerID ASC
)
GO

-- Database Change Log 15-Oct-2012 --

DROP INDEX FuelVendorIDX ON FuelVendor
GO

-- Database Change Log 17-Nov-2012 --

ALTER TABLE DepartmentGroupOrder ADD IsDeleted BIT DEFAULT (0) NOT NULL
GO

ALTER TABLE CrewGroupOrder ADD IsDeleted BIT DEFAULT (0) NOT NULL
GO

-- Database Change Log 29-Nov-2012 --

CREATE NONCLUSTERED INDEX [PassengerNameDBOIDX] ON [dbo].[Passenger]
(
CustomerID, PassengerRequestorID, FirstName, LastName, MiddleInitial, DateOfBirth, TSAStatus
)
GO

CREATE NONCLUSTERED INDEX [PreflightMainESTIDX] ON PreflightMain
(
CustomerID, EstDepartureDT
)
GO

CREATE NONCLUSTERED INDEX [PreflightPassengerListTSAIDX] ON [dbo].[PreflightPassengerList]
(
CustomerID, PassengerID
)
GO

DROP INDEX [PassengerNameDBOIDX] ON [dbo].[Passenger]
GO

DROP INDEX [PreflightMainESTIDX] ON PreflightMain
GO

DROP INDEX [PreflightPassengerListTSAIDX] ON [dbo].[PreflightPassengerList]
GO

-- Database Change Log 29-Nov-2012 --

DROP INDEX CrewPassengerPassportCrewIDX ON CrewPassengerPassport
GO

DROP INDEX CrewPassengerPassportPassengerIDX ON CrewPassengerPassport
GO

DROP INDEX CrewPassengerVisaCrewIDX ON CrewPassengerVisa
GO

DROP INDEX CrewPassengerVisaPassengerIDX ON CrewPassengerVisa
GO

-- SQL Script File 13-Nov-2012 --