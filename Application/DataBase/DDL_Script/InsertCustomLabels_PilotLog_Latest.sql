DECLARE @LastUpdUID as varchar(30)='Change the correct FSS UWA Administrator UserName';

/*DECLARE @UserCD as varchar(30);
DECLARE @CustomerID as BigINT;
DECLARE @HomebaseID as BigINT;
DECLARE @TSFlightLogID as BigINT;
DECLARE @PrimeKey int;
DECLARE @TFSCount INT;
DECLARE @Category as varchar(20)='PILOT LOG';


DECLARE @LastUpdTS as datetime =GETDATE();

DECLARE MY_CURSOR CURSOR 
  LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR 
SELECT DISTINCT username
FROM usermaster where username is not NULL and UserName!=''

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @UserCD
WHILE @@FETCH_STATUS = 0
BEGIN 
    IF NOT EXISTS (SELECT * FROM TsFlightLog WHERE OriginalDescription='Crew Log Custom Label 1' and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND 
	HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))  AND Category = @Category)
    BEGIN
		SET @CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
		SET @HomebaseID=CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))
		Select @TSFlightLogID=max(TSFlightLogID)+1,@PrimeKey=max(PrimeKey)+1  from TSFlightLog where CustomerID=@CustomerID
		Select @TFSCount=count(*)
		from TsFlightLog where CustomerID=@CustomerID AND 
			HomebaseID =@HomebaseID  AND Category = @Category
		If(@TFSCount>0)
		BEGIN
        INSERT INTO TsFlightLog(TSFlightLogID,CustomerID, PrimeKey, HomebaseID,OriginalDescription,CustomDescription,SequenceOrder,IsPrint,Category,IsDeleted,
		IsInActive,LastUpdUID,LastUpdTS)
        Values(@TSFlightLogID,@CustomerID,@PrimeKey,@HomebaseID,'Crew Log Custom Label 1','Crew Log Custom Label 1',0,1,@Category,0,0,@LastUpdUID,@LastUpdTS)
		PRINT 'Crew Log Custom Label 1 added for user '+@UserCD
		END
    END
    ELSE
    BEGIN
        PRINT 'Crew Log Custom Label 1 exists for user '+@UserCD
    END
	IF NOT EXISTS (SELECT * FROM TsFlightLog WHERE OriginalDescription='Crew Log Custom Label 2' and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND 
	HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))  AND Category = @Category)
    BEGIN
		SET @CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
		SET @HomebaseID=CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))
		Select @TSFlightLogID=max(TSFlightLogID)+1,@PrimeKey=max(PrimeKey)+1  from TSFlightLog where CustomerID=@CustomerID
		Select @TFSCount=count(*)
		from TsFlightLog where CustomerID=@CustomerID AND 
			HomebaseID =@HomebaseID  AND Category = @Category
		If(@TFSCount>0)
		BEGIN
        INSERT INTO TsFlightLog(TSFlightLogID,CustomerID, PrimeKey, HomebaseID,OriginalDescription,CustomDescription,SequenceOrder,IsPrint,Category,IsDeleted,
		IsInActive,LastUpdUID,LastUpdTS)
        Values(@TSFlightLogID,@CustomerID,@PrimeKey,@HomebaseID,'Crew Log Custom Label 2','Crew Log Custom Label 2',0,1,@Category,0,0,@LastUpdUID,@LastUpdTS)
		PRINT 'Crew Log Custom Label 2 added for user '+@UserCD
		END
    END
    ELSE
    BEGIN
        PRINT 'Crew Log Custom Label 2 exists for user '+@UserCD
    END
	
    FETCH NEXT FROM MY_CURSOR INTO @UserCD
END
CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR
*/