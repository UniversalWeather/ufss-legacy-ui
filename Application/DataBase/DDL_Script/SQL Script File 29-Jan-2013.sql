------------------------------
-- UWA Flightpak Application -
------------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Charter Quote Module     --
-- Created Date: 29-Jan-2013        --
-- Updated Date: 29-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

ALTER TABLE CQMain ADD StandardCrewDOM NUMERIC(2)
GO

ALTER TABLE CQMain ADD StandardCrewINTL NUMERIC(2)
GO

ALTER TABLE CQMain ADD DaysTotal NUMERIC(4)
GO

ALTER TABLE CQMain ADD FlightHoursTotal NUMERIC(9,3)
GO

ALTER TABLE CQMain ADD AverageFlightHoursTotal NUMERIC(7,2)
GO

ALTER TABLE CQMain ADD AdjAmtTotal NUMERIC(17,3)
GO

ALTER TABLE CQMain ADD UsageAdjTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideUsageAdj BIT
GO

ALTER TABLE CQMain ADD SegmentFeeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideSegmentFee BIT
GO

ALTER TABLE CQMain ADD LandingFeeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideLandingFee BIT
GO

ALTER TABLE CQMain ADD AdditionalFeeTotalD NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD FlightChargeTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD SubtotalTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD DiscountPercentageTotal NUMERIC(6,2)
GO

ALTER TABLE CQMain ADD IsOverrideDiscountPercentage BIT
GO

ALTER TABLE CQMain ADD DiscountAmtTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideDiscountAMT BIT
GO

ALTER TABLE CQMain ADD TaxesTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideTaxes BIT
GO

ALTER TABLE CQMain ADD MarginalPercentTotal NUMERIC(7,2)
GO

ALTER TABLE CQMain ADD MarginTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD QuoteTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsTotalQuote BIT
GO

ALTER TABLE CQMain ADD CostTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD IsOverrideCost BIT
GO

ALTER TABLE CQMain ADD PrepayTotal NUMERIC(17,2)
GO

ALTER TABLE CQMain ADD RemainingAmtTotal NUMERIC(17,2)
GO

DROP TABLE CQFleetChargeTotal
GO

-- SQL Script File 29-Jan-2013 --