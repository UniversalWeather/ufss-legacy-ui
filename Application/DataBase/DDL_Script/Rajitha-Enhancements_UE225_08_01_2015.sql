/****** Object:  Index [TransportIDX]    Script Date: 1/9/2015 12:24:25 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Transport]') AND name = N'TransportIDX')
DROP INDEX [TransportIDX] ON [dbo].[Transport]
GO

/****** Object:  Index [TransportIDX]    Script Date: 1/9/2015 12:24:25 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Transport]') AND name = N'TransportIDX')
CREATE UNIQUE NONCLUSTERED INDEX [TransportIDX] ON [dbo].[Transport]
(
	[AirportID] ASC,
	[TransportCD] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO