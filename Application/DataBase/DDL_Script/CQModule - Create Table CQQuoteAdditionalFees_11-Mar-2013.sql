SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CQQuoteAdditionalFees](
	[CQQuoteAdditionalFeesID] [bigint] NOT NULL,
	[CustomerID] [bigint] NULL,
	[CQQuoteMainID] [bigint] NULL,
	[IsPrintable] [bit] NULL,
	[CQQuoteFeeDescription] [varchar](60) NULL,
	[QuoteAmount] [numeric](17, 2) NULL,
	[OrderNUM] [int] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PKCQQuoteAdditionalFees] PRIMARY KEY CLUSTERED 
(
	[CQQuoteAdditionalFeesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteAdditionalFeesCQQuoteMain] FOREIGN KEY([CQQuoteMainID])
REFERENCES [dbo].[CQQuoteMain] ([CQQuoteMainID])
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees] CHECK CONSTRAINT [FKCQQuoteAdditionalFeesCQQuoteMain]
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteAdditionalFeesCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees] CHECK CONSTRAINT [FKCQQuoteAdditionalFeesCustomer]
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteAdditionalFeesUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees] CHECK CONSTRAINT [FKCQQuoteAdditionalFeesUserMaster]
GO

ALTER TABLE [dbo].[CQQuoteAdditionalFees] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO


