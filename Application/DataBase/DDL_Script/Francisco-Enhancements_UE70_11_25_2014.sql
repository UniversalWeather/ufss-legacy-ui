------------------------------------------- UE-70 ticket number
-- used in SP PreflightHotelListID is FK,CustomerID is covering index (Recommended by SQL processor)
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightHotelCrewPassengerList_PreflightHotelListID_CustomerID_IDX' AND object_id = OBJECT_ID('PreflightHotelCrewPassengerList'))
BEGIN
	DROP INDEX [PreflightHotelCrewPassengerList_PreflightHotelListID_CustomerID_IDX] ON [dbo].[PreflightHotelCrewPassengerList]
END
GO
CREATE NONCLUSTERED INDEX [PreflightHotelCrewPassengerList_PreflightHotelListID_CustomerID_IDX] ON [dbo].[PreflightHotelCrewPassengerList]
(
	[PreflightHotelListID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO