create nonclustered index CrewCLIENTIDX on Crew
(
ClientID ,
CustomerID
)

create nonclustered index CrewISROTIDX on Crew
(
IsRotaryWing ,
CustomerID
)

create nonclustered index CrewISFIXIDX on Crew
(
IsFixedWing ,
CustomerID
)

create nonclustered index CrewISSTATIDX on Crew
(
IsStatus ,
CustomerID
)