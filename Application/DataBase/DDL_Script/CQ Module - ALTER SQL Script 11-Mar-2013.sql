

ALTER TABLE CQMain ADD	CustomSubTotal NUMERIC(17,2) NULL, 
						CustomDiscountAmt NUMERIC(17,2) NULL,
						CustomTaxes NUMERIC(17,2) NULL,
						CustomPrepay NUMERIC(17,2) NULL,
						CustomRemainingAmt NUMERIC(17,2) NULL,
						IsPrintTotalQuote BIT NULL,
						TotalQuoteDescription VARCHAR(60) NULL,
						CustomTotalQuote NUMERIC(17,2) NULL
						
GO

ALTER TABLE CQInvoiceSummary DROP COLUMN IsSumTax,IsManualTax,IsManualDiscount
GO

ALTER TABLE CQInvoiceSummary ADD IsFlightChgDiscount BIT NULL
GO

ALTER TABLE CQInvoiceMain ADD	IsSumTax BIT NULL,
								IsManualTax BIT NULL,
								IsManualDiscount BIT NULL,
								IsPrintQuoteNUM BIT NULL
GO

ALTER TABLE CQFile ADD EmailID VARCHAR(100) null
GO

ALTER TABLE CQInvoiceAdditionalFees 
ADD Quantity numeric (17,3) null , 
	ChargeUnit varchar(25) null , 
	Rate numeric(17,2) null 
GO 

