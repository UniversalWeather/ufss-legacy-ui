------------------------------------------- UE-51 ticket number
-- used in SP TripID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightFuel_TripID_IDX' AND object_id = OBJECT_ID('PreflightFuel'))
BEGIN
	DROP INDEX [PreflightFuel_TripID_IDX] ON [dbo].[PreflightFuel]
END
GO
CREATE NONCLUSTERED INDEX [PreflightFuel_TripID_IDX] ON [dbo].[PreflightFuel]
(
	[TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------- UE-51 ticket number
-- used in SP CustomerID is FK,DepartureICAOID is FK (recommended by SQL processor)
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FlightpakFuel_CustomerID_DepartureICAOID_IDX' AND object_id = OBJECT_ID('FlightpakFuel'))
BEGIN
	DROP INDEX [FlightpakFuel_CustomerID_DepartureICAOID_IDX] ON [dbo].[FlightpakFuel]
END
GO
CREATE NONCLUSTERED INDEX [FlightpakFuel_CustomerID_DepartureICAOID_IDX] ON [dbo].[FlightpakFuel]
(
	[CustomerID] ASC ,
	[DepartureICAOID] ASC
)
INCLUDE ([FBOName],[GallonFrom],[GallonTO],[EffectiveDT],[UnitPrice],[FuelVendorID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO