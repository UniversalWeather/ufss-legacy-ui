
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CQQuoteDetail](
	[CQQuoteDetailID] [bigint] NOT NULL,
	[CustomerID] [bigint] NULL,
	[CQQuoteMainID] [bigint] NULL,
	[CQLegID] [bigint] NULL,
	[IsPrintable] [bit] NULL,
	[DepartureDT] [date] NULL,
	[FromDescription] [varchar](120) NULL,
	[ToDescription] [varchar](120) NULL,
	[FlightHours] [numeric](5, 1) NULL,
	[Distance] [numeric](5, 0) NULL,
	[PassengerTotal] [numeric](3, 0) NULL,
	[FlightCharge] [numeric](17, 2) NULL,
	[IsTaxable] [bit] NULL,
	[TaxRate] [numeric](5, 2) NULL,
	[DAirportID] [bigint] NULL,
	[AAirportID] [bigint] NULL,
	[DepartureDTTMLocal] [datetime] NULL,
	[ArrivalDTTMLocal] [datetime] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PKCQQuoteDetail] PRIMARY KEY CLUSTERED 
(
	[CQQuoteDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailAAirport] FOREIGN KEY([AAirportID])
REFERENCES [dbo].[Airport] ([AirportID])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailAAirport]
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailCQQuoteMain] FOREIGN KEY([CQQuoteMainID])
REFERENCES [dbo].[CQQuoteMain] ([CQQuoteMainID])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailCQQuoteMain]
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailCQLeg] FOREIGN KEY([CQLegID])
REFERENCES [dbo].[CQLeg] ([CQLegID])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailCQLeg]
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailCustomer]
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailDAirport] FOREIGN KEY([DAirportID])
REFERENCES [dbo].[Airport] ([AirportID])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailDAirport]
GO

ALTER TABLE [dbo].[CQQuoteDetail]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteDetailUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[CQQuoteDetail] CHECK CONSTRAINT [FKCQQuoteDetailUserMaster]
GO

ALTER TABLE [dbo].[CQQuoteDetail] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO


