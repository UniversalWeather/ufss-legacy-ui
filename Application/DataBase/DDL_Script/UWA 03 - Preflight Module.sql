-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Preflight Module         --
-- Created Date: 15-Jun-2012        --
-- Updated Date: 04-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE PreflightMain
(
TripID BIGINT,
CustomerID BIGINT,
TripNUM BIGINT,
PreviousNUM BIGINT,
TripDescription VARCHAR(40),
FleetID BIGINT,
DispatchNUM CHAR(12),
TripStatus CHAR(1),
EstDepartureDT DATE,
EstArrivalDT DATE,
RecordType CHAR(1),
AircraftID BIGINT,
PassengerRequestorID BIGINT,
RequestorFirstName VARCHAR(63),
RequestorMiddleName VARCHAR(60),
RequestorLastName VARCHAR(60),
RequestorPhoneNUM VARCHAR(25),
RequestorDepartment CHAR(8),
DepartmentDescription VARCHAR(25),
RequestorAuthorization CHAR(8),
AuthorizationDescription VARCHAR(25),
RequestDT DATE,
IsCrew BIT,
IsPassenger BIT,
IsQuote BIT,
IsScheduledServices  BIT,
IsAlert BIT,
Notes TEXT,
LogisticsHistory TEXT,
HomebaseID BIGINT,
ClientID BIGINT,
IsAirportAlert BIT,
IsLog BIT,
BeginningGMTDT DATE,
EndingGMTDT DATE,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsPrivate BIT,
IsCorpReq BIT,
Acknowledge CHAR(1),
DispatchNotes TEXT,
TripRequest CHAR(1),
WaitList CHAR(1),
FlightNUM CHAR(12),
FlightCost NUMERIC(12,2),
AccountID BIGINT,
TripException TEXT,
TripSheetNotes TEXT,
CancelDescription VARCHAR(60),
Dispatcher CHAR(10),
IsCompleted BIT,
IsFBOUpdFlag BIT,
RevisionNUM NUMERIC(3),
RevisionDescriptioin VARCHAR(200),
IsPersonal BIT,
DsptnName VARCHAR(60),
ScheduleCalendarHistory TEXT,
ScheduleCalendarUpdate DATETIME,
CommentsMessage TEXT,
EstFuelQTY NUMERIC(17,3),
CrewID BIGINT,
ReleasedBy VARCHAR(60),
IsDeleted BIT,
VerifyNUM VARCHAR(40),
APISSubmit DATETIME,
APISStatus VARCHAR(20),
APISException TEXT,
IsAPISValid BIT,
EmergencyContactCD CHAR(5),
CONSTRAINT PKPreflightMain PRIMARY KEY CLUSTERED(TripID ASC),
CONSTRAINT FKPreflightMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightMainUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightMainFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKPreflightMainCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKPreflightMainClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKPreflightMainAircraft FOREIGN KEY(AircraftID) REFERENCES Aircraft(AircraftID),
CONSTRAINT FKPreflightMainPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKPreflightMainAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightMainIDX ON PreflightMain
(
TripID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightLeg
(
LegID BIGINT,
TripID BIGINT,
CustomerID BIGINT,
TripNUM BIGINT,
LegNUM BIGINT,
DepartICAOID BIGINT,
ArriveICAOID BIGINT,
Distance NUMERIC(5),
PowerSetting CHAR(1),
TakeoffBIAS NUMERIC(6,3),
LandingBias NUMERIC(6,3),
TrueAirSpeed NUMERIC(3),
WindsBoeingTable NUMERIC(5),
ElapseTM NUMERIC(7,3),
IsDepartureConfirmed BIT,
IsArrivalConfirmation BIT,
IsApproxTM BIT,
IsScheduledServices  BIT,
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
DepartureGreenwichDTTM DATETIME,
ArrivalGreenwichDTTM DATETIME,
HomeDepartureDTTM DATETIME,
HomeArrivalDTTM DATETIME,
GoTime CHAR(5),
FBOID BIGINT,
LogBreak CHAR(3),
FlightPurpose VARCHAR(40),
PassengerRequestorID BIGINT,
RequestorName VARCHAR(60),
DepartmentID BIGINT,
DepartmentName VARCHAR(60),
AuthorizationID BIGINT,
AuthorizationName VARCHAR(60),
CategoryCD CHAR(4), -- TBD
CategoryDescription VARCHAR(60),
PassengerTotal INT,
SeatTotal INT,
ReservationTotal INT,
ReservationAvailable INT,
WaitNUM INT,
DutyTYPE CHAR(2),
DutyHours NUMERIC(7,3),
RestHours NUMERIC(7,3),
FlightHours NUMERIC(7,3),
Notes TEXT,
FuelLoad NUMERIC(6),
OutbountInstruction TEXT,
DutyTYPE1 NUMERIC(1),
IsCrewDiscount BIT,
CrewDutyRulesID BIGINT,
IsDutyEnd BIT,
FedAviationRegNUM CHAR(3),
ClientID BIGINT,
LegID1 BIGINT,
WindReliability INT,
OverrideValue NUMERIC(4,1),
CheckGroup CHAR(3),
AirportAlert BIGINT,
AdditionalCrew CHAR(24),
PilotInCommand CHAR(3),
SecondInCommand CHAR(3),
NextLocalDTTM DATETIME,
NextGMTDTTM DATETIME,
NextHomeDTTM DATETIME,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CrewNotes TEXT,
IsPrivate BIT,
WaitList CHAR(1),
FlightNUM CHAR(12),
FlightCost NUMERIC(12,2),
AccountNumber CHAR(32),
CrewFuelLoad CHAR(40),
IsDeleted BIT,
UWAID CHAR(40),
USCrossing VARCHAR(75),
Response TEXT,
ConfirmID CHAR(25),
CONSTRAINT PKPreflightLeg PRIMARY KEY CLUSTERED(LegID ASC),
CONSTRAINT FKPreflightLegMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKPreflightLegCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightLegUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightLegClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKPreflightLegPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKPreflightLegAirportD FOREIGN KEY(DepartICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightLegAirportA FOREIGN KEY(ArriveICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightLegFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID),
CONSTRAINT FKPreflightLegDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID),
CONSTRAINT FKPreflightLegDepartmentAuthorization FOREIGN KEY(AuthorizationID) REFERENCES DepartmentAuthorization(AuthorizationID),
CONSTRAINT FKPreflightLegCrewDutyRules FOREIGN KEY(CrewDutyRulesID) REFERENCES CrewDutyRules(CrewDutyRulesID),
CONSTRAINT FKPreflightLegAirportAlert FOREIGN KEY(AirportAlert) REFERENCES Airport(AirportID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightLegIDX ON PreflightLeg
(
TripNUM ASC, LegNUM ASC, DepartICAOID ASC, ArriveICAOID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightHotelXrefList
(
PreflightHotelID BIGINT,
PreflightHotelName VARCHAR(60),
LegID BIGINT,
HotelID BIGINT,
AirportID BIGINT,
CrewPassengerType CHAR(1),
RoomType VARCHAR(60),
RoomRent NUMERIC(6,2),
RoomConfirm TEXT,
RoomDescription TEXT,
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightHotelXrefList PRIMARY KEY CLUSTERED(PreflightHotelID),
CONSTRAINT FKPreflightHotelXrefListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightHotelXrefListHotel FOREIGN KEY(LegID) REFERENCES Hotel(HotelID),
CONSTRAINT FKPreflightHotelXrefListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightHotelXrefListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightHotelXrefListHIDX ON PreflightHotelXrefList
(
HotelID ASC, AirportID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightHotelXrefListLIDX ON PreflightHotelXrefList
(
LegID ASC
)
GO

CREATE TABLE PreflightFBOList
(
PreflightFBOID BIGINT,
PreflightFBOName VARCHAR(60),
LegID BIGINT,
FBOID BIGINT,
IsArrivalFBO BIT,
IsDepartureFBO BIT,
AirportID BIGINT,
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightFBOList PRIMARY KEY CLUSTERED(PreflightFBOID),
CONSTRAINT FKPreflightFBOListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightFBOListFBO FOREIGN KEY(LegID) REFERENCES FBO(FBOID),
CONSTRAINT FKPreflightFBOListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightFBOListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightFBOListIDX ON PreflightFBOList
(
FBOID ASC, AirportID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightFBOListLIDX ON PreflightFBOList
(
LegID ASC
)
GO

CREATE TABLE PreflightTransportList
(
PreflightTransportID BIGINT,
PreflightTransportName VARCHAR(60),
LegID BIGINT,
TransportID BIGINT,
IsArrivalTransport BIT,
IsDepartureTransport BIT,
AirportID BIGINT,
CrewPassengerType CHAR(1),
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightTransportList PRIMARY KEY CLUSTERED(PreflightTransportID),
CONSTRAINT FKPreflightTransportListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightTransportListTransport FOREIGN KEY(LegID) REFERENCES Transport(TransportID),
CONSTRAINT FKPreflightTransportListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightTransportListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightTransportListIDX ON PreflightTransportList
(
TransportID ASC, AirportID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightTransportListLIDX ON PreflightTransportList
(
LegID ASC
)
GO

CREATE TABLE PreflightCateringList
(
PreflightCateringID BIGINT,
PreflightCateringName VARCHAR(60),
LegID BIGINT,
CateringID BIGINT,
AirportID BIGINT,
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightCateringList PRIMARY KEY CLUSTERED(PreflightCateringID),
CONSTRAINT FKPreflightCateringListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightCateringListCatering FOREIGN KEY(LegID) REFERENCES Catering(CateringID),
CONSTRAINT FKPreflightCateringListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightCateringListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightCateringListIDX ON PreflightCateringList
(
CateringID ASC, AirportID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightCateringListLIDX ON PreflightCateringList
(
LegID ASC
)
GO

CREATE TABLE PreflightCrewList
(
PreflightCrewListID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
CrewID BIGINT,
PassportID BIGINT,
VisaID BIGINT,
PreflightHotelID BIGINT,
HotelName VARCHAR(60),
CrewFirstName VARCHAR(60),
CrewMiddleName VARCHAR(60),
CrewLastName VARCHAR(60),
DutyTYPE CHAR(1),
OrderNUM INT,
IsDiscount BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsBlackberrMailSent BIT,
IsNotified BIT,
IsDeleted BIT,
CONSTRAINT PKPreflightCrewList PRIMARY KEY CLUSTERED(PreflightCrewListID ASC),
CONSTRAINT FKPreflightCrewListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightCrewListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightCrewListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightCrewListHotel FOREIGN KEY(PreflightHotelID) REFERENCES PreflightHotelXrefList(PreflightHotelID),
CONSTRAINT FKPreflightCrewListCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID),
CONSTRAINT FKPreflightCrewListCrewPassport FOREIGN KEY(PassportID) REFERENCES CrewPassengerPassport(PassportID),
CONSTRAINT FKPreflightCrewListCrewVisa FOREIGN KEY(VisaID) REFERENCES CrewPassengerVisa(VisaID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightCrewList ON PreflightCrewList
(
PreflightCrewListID ASC, CustomerID ASC, LegID ASC, CrewID ASC
)
GO

CREATE TABLE PreflightPassengerList
(
PreflightPassengerListID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
PassengerID BIGINT,
PassportID BIGINT,
VisaID BIGINT,
PreflightHotelID BIGINT,
HotelName VARCHAR(60),
PassengerFirstName VARCHAR(63),
PassengerMiddleName VARCHAR(60),
PassengerLastName VARCHAR(60),
FlightPurposeID BIGINT,
PassportNUM CHAR(25),
Billing CHAR(25),
IsNonPassenger BIT,
IsBlocked BIT,
OrderNUM INT,
ReservationNUM CHAR(6),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
WaitList CHAR(1),
AssociatedPassenger CHAR(5),
TripStatus CHAR(1),
TSADTTM DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightPassengerList PRIMARY KEY CLUSTERED(PreflightPassengerListID ASC),
CONSTRAINT FKPreflightPassengerListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightPassengerListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightPassengerListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightPassengerListHotel FOREIGN KEY(PreflightHotelID) REFERENCES PreflightHotelXrefList(PreflightHotelID),
CONSTRAINT FKPreflightPassengerListPassenger FOREIGN KEY(PassengerID) REFERENCES Passenger(PassengerRequestorID),
CONSTRAINT FKPreflightPassengerListCrewPassport FOREIGN KEY(PassportID) REFERENCES CrewPassengerPassport(PassportID),
CONSTRAINT FKPreflightPassengerListCrewVisa FOREIGN KEY(VisaID) REFERENCES CrewPassengerVisa(VisaID),
CONSTRAINT FKPreflightPassengerListFlightPurpose FOREIGN KEY(FlightPurposeID) REFERENCES FlightPurpose(FlightPurposeID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightPassengerListIDX ON PreflightPassengerList
(
PreflightPassengerListID ASC, CustomerID ASC, LegID ASC, PassengerID ASC
)
GO

CREATE TABLE PreflightTripException
(
TripExceptionID BIGINT,
TripID BIGINT,
CustomerID BIGINT,
ExceptionDescription VARCHAR(100),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKPreflightTripException PRIMARY KEY CLUSTERED(TripExceptionID ASC),
CONSTRAINT FKPreflightTripException FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKPreflightTripExceptionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightTripExceptionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PreflightCheckList
(
PreflightCheckListID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
CheckListID BIGINT,
ComponentCD CHAR(4),
ComponentDescription VARCHAR(30),
IsCompleted BIT,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
IsDeleted BIT
CONSTRAINT PKPreflightCheckList PRIMARY KEY CLUSTERED(PreflightCheckListID ASC),
CONSTRAINT FKPreflightCheckListLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightCheckListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightCheckListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightCheckListTripManagerCheckList FOREIGN KEY(CheckListID) REFERENCES TripManagerCheckList(CheckListID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightCheckListIDX ON PreflightCheckList
(
ComponentCD ASC, CustomerID ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightCheckListLIDX ON PreflightCheckList
(
LegID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightUVLMain
(
TripUVID BIGINT,
TripID BIGINT,
CustomerID BIGINT,
FleetID BIGINT,
EstDepartureDT DATE,
UWATeam VARCHAR(35),
TripDestination VARCHAR(30),
RequestDT DATE,
LastUserID CHAR(30),
LastUptTS DATE,
UWATripID CHAR(9),
TripReqHistory TEXT,
TripRequestID CHAR(9),
TripReqLastSubmitDT DATETIME,
TripStatusLastRetrieveDT DATETIME,
TripUrl TEXT,
TripSSIID CHAR(20),
NewCharge CHAR(1),	
IsDeleted BIT,
CONSTRAINT PKPreflightUVLMain PRIMARY KEY CLUSTERED(TripUVID ASC),
CONSTRAINT FKPreflightUVLMainPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKPreflightUVLMainCustomer FOREIGN KEY(TripID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightUVLMainFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightUVLMainIDX ON PreflightUVLMain
(
TripID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightUVLeg
(
LegUVID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
UVLegOrder BIGINT,
IsFlightPlanP BIT,
IsFlightPlanD BIT,
IsUWAHandling BIT,
IsUWAPermits BIT,
IsUWASlots BIT,
IsUWAOverflight BIT,
IsUVAirFuel BIT,
IsFlightPlanR BIT,
IsNotam BIT,
IsHomeBaseF BIT,
PersonOnBoard NUMERIC(3),
Payload NUMERIC(4),
FlightPlanT CHAR(1),
TailNUM CHAR(6),
FlightNUM CHAR(4),
FuelRate NUMERIC(5),
IsPreliminary BIT,
IsClim BIT,
IsPassengerBrief BIT,
IsUWACrewCar BIT,
IsUWAPassengerCar BIT,
legStatus CHAR(1),
IsUWACrewHotel BIT,
IsUWAPassengerHotel BIT,
PassengerRoom CHAR(1),
OtherDeli CHAR(25),
SpecialInstructions VARCHAR(100),
FlightFollow VARCHAR(100),
FuelOnBoard NUMERIC(6),
FlightLevel NUMERIC(3),
MachFactor NUMERIC(6),
LegDLV DATETIME,
CrewReservation CHAR(30),
PassengerReservation CHAR(30),
MaintenanceReservation CHAR(30),
IsUWAMaintenance BIT,
MaintenanceHotel CHAR(1),
PassengerRoomNo NUMERIC(3),
CrewRoom NUMERIC(3),
MaintenanceRoom NUMERIC(3),
IsArrivalDepartureCA BIT,
LastUserID CHAR(30),
LastUptTS DATETIME,
IsDeleted BIT
CONSTRAINT PKPreflightUVLeg PRIMARY KEY CLUSTERED(LegUVID ASC),
CONSTRAINT FKPreflightUVLegPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightUVLegCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightUVLegIDX ON PreflightUVLeg
(
LegID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightFuel
(
PreflightFuelID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
AirportID BIGINT,
OrderNUM CHAR(4),
FBOName VARCHAR(25),
EffectiveDT DATE,
BestPrice NUMERIC(15,4),
GallongFrom NUMERIC(17,3),
GallongTo NUMERIC(17,3),
UnitPrice NUMERIC(15,4),
VendorID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
LRange VARCHAR(25),
Lprice VARCHAR(25),
LeastPrice NUMERIC(15,4),
UpdateDTTM DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightFuel PRIMARY KEY CLUSTERED(PreflightFuelID ASC),
CONSTRAINT FKPreflightFuelLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightFuelCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightFuelUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightFuelAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightFuelVendor FOREIGN KEY(VendorID) REFERENCES Vendor(VendorID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightFuelIDX ON PreflightFuel
(
VendorID ASC, AirportID ASC, LegID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightTripSIFL
(
PreflightTripSIFLID BIGINT,
CustomerID BIGINT,
FareLevelID BIGINT,
LegID BIGINT,
PassengerRequestorID BIGINT,
PassengerName VARCHAR(63),
EmployeeTaxInfoTYPE CHAR(4),
AssociatedWithCD CHAR(5),
DepartICAOID BIGINT,
ArriveICAOID BIGINT,
Distance NUMERIC(5),
LoadFactor NUMERIC(5,1),
AircraftMultiplier NUMERIC(5,1),
Rate1 NUMERIC(8,4),
Rate2 NUMERIC(8,4),
Rate3 NUMERIC(8,4),
Distance1 NUMERIC(3),
Distance2 NUMERIC(4),
Distance3 NUMERIC(5),
TeminalCharge NUMERIC(6,2),
AmtTotal NUMERIC(17,2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKPreflightTripSIFL PRIMARY KEY CLUSTERED(PreflightTripSIFLID ASC),
CONSTRAINT FKPreflightTripSIFLLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightTripSIFLCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightTripSIFLUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightTripSIFLDAirport FOREIGN KEY(DepartICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightTripSIFLAAirport FOREIGN KEY(ArriveICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightTripSIFLFareLevel FOREIGN KEY(FareLevelID) REFERENCES FareLevel(FareLevelID),
CONSTRAINT FKPreflightTripSIFLPassenger FOREIGN KEY(PassengerRequestorID) REFERENCES Passenger(PassengerRequestorID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightTripSIFLIDX ON PreflightTripSIFL
(
LegID ASC, CustomerID ASC
)
GO

CREATE TABLE PreflightTripOutbound
(
PreflightTripOutboundID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
OutboundInstructionNUM BIGINT,
OutboundDescription VARCHAR(250),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightTripOutbound PRIMARY KEY CLUSTERED(PreflightTripOutboundID ASC),
CONSTRAINT FKPreflightTripOutboundLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightTripOutboundCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightTripOutboundUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX PreflightTripOutboundIDX ON PreflightTripOutbound
(
OutboundInstructionNUM ASC, LegID ASC, CustomerID ASC
)
GO

-- Database Change Log 03-Jul-2012 --

ALTER TABLE PreflightMain DROP COLUMN RequestorDepartment
GO

ALTER TABLE PreflightMain ADD DepartmentID BIGINT
GO

ALTER TABLE PreflightMain DROP COLUMN RequestorAuthorization
GO

ALTER TABLE PreflightMain ADD AuthorizationID BIGINT
GO

ALTER TABLE PreflightMain DROP COLUMN Dispatcher
GO

ALTER TABLE PreflightMain ADD DispatcherUserName CHAR(30)
GO

ALTER TABLE PreflightMain DROP COLUMN EmergencyContactCD
GO

ALTER TABLE PreflightMain ADD EmergencyContactID BIGINT
GO

ALTER TABLE PreflightLeg DROP CONSTRAINT FKPreflightLegAirportAlert
GO

ALTER TABLE PreflightLeg DROP COLUMN AirportAlert
GO

ALTER TABLE PreflightLeg ADD CrewDutyAlert VARCHAR(5)
GO

ALTER TABLE PreflightLeg DROP COLUMN AccountNumber
GO

ALTER TABLE PreflightLeg ADD AccountID BIGINT
GO

ALTER TABLE PreflightLeg ADD CONSTRAINT FKPreflightLegAccount FOREIGN KEY(AccountID) REFERENCES Account(AccountID)
GO

-- Database Change Log 04-Jul-2012 --

ALTER TABLE PreflightMain ADD CONSTRAINT FKPreflightMainDepartment FOREIGN KEY(DepartmentID) REFERENCES Department(DepartmentID)
GO

ALTER TABLE PreflightMain ADD CONSTRAINT FKPreflightMainDepartmentAuthorization FOREIGN KEY(AuthorizationID) REFERENCES DepartmentAuthorization(AuthorizationID)
GO

ALTER TABLE PreflightMain ADD CONSTRAINT FKPreflightMainDUserMaster FOREIGN KEY(DispatcherUserName) REFERENCES UserMaster(UserName)
GO

ALTER TABLE PreflightMain ADD CONSTRAINT FKPreflightMainEmergencyContact FOREIGN KEY(EmergencyContactID) REFERENCES EmergencyContact(EmergencyContactID)
GO

-- Database Change Log 05-Jul-2012 --

ALTER TABLE PreflightCrewList ADD IsAdditionalCrew BIT
GO

-- Database Change Log 10-Jul-2012 --

ALTER TABLE PreflightTransportList DROP CONSTRAINT FKPreflightTransportListTransport
GO

ALTER TABLE PreflightTransportList ADD CONSTRAINT FKPreflightTransportListTransport FOREIGN KEY(TransportID) REFERENCES Transport(TransportID)
GO

ALTER TABLE PreflightHotelXrefList DROP CONSTRAINT FKPreflightHotelXrefListHotel
GO

ALTER TABLE PreflightHotelXrefList ADD CONSTRAINT FKPreflightHotelXrefListHotel FOREIGN KEY(HotelID) REFERENCES Hotel(HotelID)
GO

ALTER TABLE PreflightCrewList ADD Street VARCHAR(50)
GO

ALTER TABLE PreflightCrewList ADD CityName VARCHAR(20)
GO

ALTER TABLE PreflightCrewList ADD StateName VARCHAR(20)
GO

ALTER TABLE PreflightCrewList ADD PostalZipCD VARCHAR(20)
GO

ALTER TABLE PreflightMain ADD CONSTRAINT FKPreflightMainCrew FOREIGN KEY(CrewID) REFERENCES Crew(CrewID)
GO

ALTER TABLE PreflightLeg DROP COLUMN CategoryCD
GO

ALTER TABLE PreflightLeg DROP COLUMN CategoryDescription
GO

ALTER TABLE PreflightLeg ADD FlightCategoryID BIGINT
GO

ALTER TABLE PreflightLeg ADD CONSTRAINT FKPreflightLegFlightCatagory FOREIGN KEY(FlightCategoryID) REFERENCES FlightCatagory(FlightCategoryID)
GO

DROP INDEX PreflightLegIDX ON PreflightLeg
GO

ALTER TABLE PreflightFBOList DROP CONSTRAINT FKPreflightFBOListFBO
GO

ALTER TABLE PreflightFBOList ADD CONSTRAINT FKPreflightFBOListFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID)
GO

ALTER TABLE PreflightCateringList DROP CONSTRAINT FKPreflightCateringListCatering
GO

ALTER TABLE PreflightCateringList ADD CONSTRAINT FKPreflightCateringListCatering FOREIGN KEY(CateringID) REFERENCES Catering(CateringID)
GO

ALTER TABLE PreflightUVLMain DROP CONSTRAINT FKPreflightUVLMainCustomer
GO

ALTER TABLE PreflightUVLMain ADD CONSTRAINT FKPreflightUVLMainCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

-- Database Change Log 17-Jul-2012 --

DROP INDEX PreflightTransportListIDX ON PreflightTransportList
GO

DROP INDEX PreflightHotelXrefListHIDX ON PreflightHotelXrefList
GO

-- Database Change Log 19-Jul-2012 --

DROP INDEX PreflightHotelXrefListLIDX ON PreflightHotelXrefList
GO

ALTER TABLE PreflightHotelXrefList ADD HotelArrangedBy VARCHAR(10)
GO

ALTER TABLE PreflightHotelXrefList ADD Rate NUMERIC(17,2)
GO

ALTER TABLE PreflightHotelXrefList ADD DateIn DATE
GO

ALTER TABLE PreflightHotelXrefList ADD DateOut DATE
GO

ALTER TABLE PreflightHotelXrefList ADD IsSmoking BIT
GO

ALTER TABLE PreflightHotelXrefList ADD RoomTypeID BIGINT
GO

ALTER TABLE PreflightHotelXrefList ADD CONSTRAINT PreflightHotelXrefListRoomType FOREIGN KEY(RoomTypeID) REFERENCES RoomType(RoomTypeID)
GO

ALTER TABLE PreflightHotelXrefList ADD NoofBeds NUMERIC(3)
GO

ALTER TABLE PreflightHotelXrefList ADD IsAllCrew BIT
GO

ALTER TABLE PreflightHotelXrefList ADD IsCompleted BIT
GO

ALTER TABLE PreflightHotelXrefList ADD IsEarlyCheckIn BIT
GO

ALTER TABLE PreflightHotelXrefList ADD EarlyCheckInOption CHAR(1)
GO

ALTER TABLE PreflightHotelXrefList ADD SpecialInstructions VARCHAR(100)
GO

ALTER TABLE PreflightHotelXrefList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightHotelXrefList ADD Comments VARCHAR(100)
GO

ALTER TABLE PreflightCrewList ADD NoofRooms NUMERIC(2,0)
GO

ALTER TABLE PreflightCrewList ADD ClubCard VARCHAR(100)
GO

ALTER TABLE PreflightCrewList ADD IsRateCap BIT
GO

ALTER TABLE PreflightCrewList ADD MaxCapAmount NUMERIC(17,2)
GO

ALTER TABLE PreflightPassengerList ADD NoofRooms NUMERIC(2,0)
GO

ALTER TABLE PreflightPassengerList ADD ClubCard VARCHAR(100)
GO

ALTER TABLE PreflightPassengerList ADD IsRateCap BIT
GO

ALTER TABLE PreflightPassengerList ADD MaxCapAmount NUMERIC(17,2)
GO

-- Database Change Log 20-Jul-2012 --

ALTER TABLE PreflightTransportList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightTransportList ADD Comments VARCHAR(100)
GO

-- Database Change Log 23-Jul-2012 --

CREATE TABLE PreflightFBOHandler
(
PreflightFBOHandlerID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
AirportID BIGINT,
IsCompleted BIT,
ArriveDepart CHAR(1),
FBOID BIGINT,
FBOInformation VARCHAR(1500),
FBOConfirmation VARCHAR(1500),
FBOComments VARCHAR(1500),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightFBOHandler PRIMARY KEY CLUSTERED(PreflightFBOHandlerID),
CONSTRAINT FKPreflightFBOHandlerPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightFBOHandlerAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightFBOHandlerFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID),
CONSTRAINT FKPreflightFBOHandlerCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightFBOHandlerUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PreflightCateringDetail
(
PreflightCateringID BIGINT,
LegID BIGINT,
CustomerID BIGINT,
AirportID BIGINT,
ArriveDepart CHAR(1),
CateringID BIGINT,
IsCompleted BIT,
Cost NUMERIC(17,2),
ContactName VARCHAR(50),
ContactPhone VARCHAR(20),
ContactFax VARCHAR(20),
CateringConfirmation VARCHAR(1500),
CateringComments VARCHAR(1500),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightCateringDetail PRIMARY KEY CLUSTERED(PreflightCateringID),
CONSTRAINT FKPreflightCateringDetailPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKPreflightCateringDetailAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightCateringDetailCatering FOREIGN KEY(CateringID) REFERENCES Catering(CateringID),
CONSTRAINT FKPreflightCateringDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightCateringDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

ALTER TABLE PreflightFBOList ADD NoUpdated BIT
GO

ALTER TABLE PreflightFBOList ADD FBOArrangedBy BIT
GO

ALTER TABLE PreflightFBOList ADD IsCompleted BIT
GO

ALTER TABLE PreflightFBOList ADD FBOInformation VARCHAR(100)
GO

ALTER TABLE PreflightFBOList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightFBOList ADD Comments VARCHAR(100)
GO

ALTER TABLE PreflightCateringList ADD CateringArrangedBy BIT
GO

ALTER TABLE PreflightCateringList ADD IsCompleted BIT
GO

ALTER TABLE PreflightCateringList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightCateringList ADD Comments VARCHAR(100)
GO

-- Database Change Log 24-Jul-2012 --

--DROP INDEX PreflightTransportListLIDX ON PreflightTransportList
--GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightTransportList]') AND name = N'PreflightTransportListLIDX')
DROP INDEX PreflightTransportListLIDX ON PreflightTransportList
GO

-- Database Change Log 25-Jul-2012 --

ALTER TABLE PreflightFBOList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightFBOList ADD CONSTRAINT FKPreflightFBOListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE PreflightTransportList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightTransportList ADD CONSTRAINT FKPreflightTransportListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE PreflightHotelXrefList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightHotelXrefList ADD CONSTRAINT FKPreflightHotelXrefListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE PreflightCateringList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightCateringList ADD CONSTRAINT FKPreflightCateringListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

-- Database Change Log 26-Jul-2012 --

DROP INDEX PreflightCateringListIDX ON PreflightCateringList
GO

DROP INDEX PreflightCateringListLIDX ON PreflightCateringList
GO

DROP INDEX PreflightFBOListIDX ON PreflightFBOList
GO

DROP INDEX PreflightFBOListLIDX ON PreflightFBOList
GO

-- Database Change Log 27-Jul-2012 --

sp_rename 'PreflightUVLMain','UWATSSMain'
GO

sp_rename 'PreflightUVLeg','UWATSSLeg'
GO

CREATE TABLE UWAL
(
UWALID BIGINT,
TripID BIGINT,
LegNUM INT,
LegID BIGINT,
DepartICAOID BIGINT,
ArriveICAOID BIGINT,
ElapseTM NUMERIC(5,1),
FleetID BIGINT,
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
DepartureGreenwichDTTM DATETIME,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
FuelLoad NUMERIC(6),
Gallons INT,
FuelCost NUMERIC(12,2),
Price NUMERIC(17,5),
FeeTax NUMERIC(17,5),
TotalPrice NUMERIC(17,5),
RequestRate CHAR(1),
EffectiveDT DATE,
Supplier VARCHAR(25),
Notes TEXT,
FlightPlanFAR NUMERIC(1),
NotamLegSettings NUMERIC(1),
IsHomebaseFlightFollowing BIT,
IsPhone BIT,
IsFax BIT,
FlightPlanMach CHAR(10),
IsFlightPlanPreflight BIT,
IsHandlingAllLegs BIT,
FuelTYPE VARCHAR(30),		
WindAltitued NUMERIC(6),
IsAutoPrintTextWeather BIT,
ArrivalGreenwichDTTM DATETIME,
DepartureRadius CHAR(6),
ArrivalRadius CHAR(6),
RouteWidth CHAR(6),
IntermediateICAO1 BIGINT,
IntermediateICAO2 BIGINT,
IntermediateICAO3 BIGINT,
IntermediateICAO4 BIGINT,
IsWeatherEnroute BIT,
IsPilotRPT BIT,
TrueAirspeed NUMERIC(3),
RouteBriefText TEXT,
FlightLegSettings NUMERIC(1),
FaxDeliveryAddress VARCHAR(50),
FaxAttention VARCHAR(30),
IsEmail BIT,
EmailAddress VARCHAR(50),
EmailAttention VARCHAR(30),
PhoneDeliveryAddress VARCHAR(50),
PhoneAttention VARCHAR(30),
IsSITAAIRINC BIT,
SITAAIRINCDeliveryAddress VARCHAR(50),
SITAAIRINCDeliveryAttention VARCHAR(30),
IsWeatherReqAllLegs BIT,
NotamTYPE NUMERIC(1),		
TripRequestBy VARCHAR(30),
IsArriveDepartDeclFormsAllLeg BIT,
IsOverflightPermitAllLegs BIT,
IsLandingPermitsAllLegs BIT,
IsCrewHotelAllLegs BIT,
IsPassengerHotelAllLegs	BIT,
IsUVAirFuelAllLegs BIT,
IsUVAirQuoteRequestAllLegs BIT,
IsCateringAllLegs BIT,
IsAircraftSecurityAllLegs BIT,
IsPassengerSecurityAllLegs BIT,
IsCrewVisaAllLegs BIT,
IsPassengerVisaAllLegs BIT,
FARAllLegs NUMERIC(1),
SpecialInstructions TEXT,
IsUWAArrangeHandling BIT,
IsCatering BIT,
CateringDetail TEXT,
IsAircraftSecurity BIT,
IsPassengerSecurity BIT,
IsCrewVisa BIT,
IsPassengerVisa BIT,
IsWideBodyHandling BIT,
LandingPermitsArrangement NUMERIC(1),
AirportArrivalSlotArrange NUMERIC(1),
ClientArrangeLandingPermitNUM VARCHAR(15),
CrewHotelArrangement NUMERIC(1),		
PassengerArrangement NUMERIC(1),
CrewHotelName VARCHAR(40),
PassengerHotelName VARCHAR(40),
CrewBedTYPE	VARCHAR(35),
PassengerBedTYPE VARCHAR(35),
CrewRoomTYPE VARCHAR(35),
PassengerRoomTYPE VARCHAR(35),
CrewRoomDescription	VARCHAR(30),
PassengerRoomDescription VARCHAR(30),
CrewHotelCheckinDT DATE,
PassengerHotelCheckinDT	DATE,
CrewHotelCheckOutDT	DATE,
PassengerHotelCheckOutDT DATE,
CrewHotelRoomNUM NUMERIC(3),
PassengerHotelRoomNUM NUMERIC(3),
CrewHotelRequestTYPE NUMERIC(1),
PassengerHotelRequestTYPE NUMERIC(1),
IsCrewHotelEarlyCheckin	BIT,
IsPassengerEarlyCheckin	BIT,
IsCrewHotelRateCap BIT,
IsPassengerHotelRateCap	BIT,
IsCrewHotelSmoking BIT,
IsPassengerHotelSmoking	BIT,
CrewHotelMaxCapAmt NUMERIC(10,2),
PassengerHotelMaxCapAmt	NUMERIC(10,2),
IsFlightPlanRequired BIT,
IsFlightPlanTYPE BIT,
IsStdWeatherBrief BIT,
StdWeatherBriefDeliveryInstructions	VARCHAR(20),
StdWeatherBriefDeliveryTM CHAR(5),
PrelimWeatherBriefDeliveryInstructions VARCHAR(20),
PrelimWeatherBriefDeliveryTM CHAR(5),
PassengerWeatherBriefTYPE NUMERIC(1),
IsPassengerWeather BIT,
WeatherMaxFlightLevel NUMERIC(7),
WeatherPreferredAltICAO BIGINT,			
SpecialWeatherDeliveryInstructions TEXT,
WeatherNotams NUMERIC(1),
FlightPlanPayLoad NUMERIC(10),
PreferredAlternateICAO BIGINT,		
FlightPlanQuantity NUMERIC(7),
MaximumFlightLevel NUMERIC(7),
FlightPlanTYPE NUMERIC(1),
FlightPlanAdditionInstruction TEXT,
IsUVAirFuel	BIT,
IsUVAirFuelQuoteRequired BIT,
FuelOnArrivalDeparture NUMERIC(1),
IsPristRequired	BIT,
FuelSpecialInstruction TEXT,
IsPreliminWeather BIT,
IsWeatherUpdate BIT,
PilotInCommand CHAR(3),
PilotIncomandName VARCHAR(35),
SecondInCommand	CHAR(3),
SecondInCommandName	VARCHAR(35),
Pretrip1Location CHAR(8),
Pretrip2Location CHAR(8),
Pretrip3Location CHAR(8),
Pretrip4Location CHAR(8),
Pretrip1DeliveryMethod CHAR(7),
Pretrip2DeliveryMethod CHAR(7),
Pretrip3DeliveryMethod CHAR(7),
Pretrip4DeliveryMethod CHAR(7),
PretripDelivery1Address	VARCHAR(50),
PretripDelivery2Address	VARCHAR(50),
PretripDelivery3Address	VARCHAR(50),
PretripDelivery4Address	VARCHAR(50),
Pretrip1Attention VARCHAR(50),
Pretrip2Attention VARCHAR(50),
Pretrip3Attention VARCHAR(50),
Pretrip4Attention VARCHAR(50),
OverflightPermitsArrangement NUMERIC(1),
AirportDepartureSlotsArrangement NUMERIC(1),
IsCrewHotelUWAArrangement BIT,
IsPassengerHoteUWAArrangement BIT,
FlightPlanRoutePreference TEXT,
CrewHotelNUMBeds NUMERIC(2),
PassengerHotelNUMBeds NUMERIC(2),
CrewHotelClubCard VARCHAR(30),
PassengerHotelClubCard VARCHAR(30),
DepartureAirportName VARCHAR(25),
ArrivalAirportName VARCHAR(25),
PassengerTotal INT,
FBOArrivalName VARCHAR(60),
IsUWAArrangeCustoms BIT,
StdWeatherBriefDeliveryDT DATE,
PrelimWeatherBriefDeliveryDT DATE,
MaximumFlightLevelOption NUMERIC(1),
CrewHotelSpecialInstruction	VARCHAR(60),
PassengerHotelSpecialInstruction VARCHAR(60),
IsPreflightAllLegs BIT,
QuantityOptions	NUMERIC(1),
IsSuppressCrewPaxDetailOnAllLegs BIT,
AircraftSecurityNote TEXT,
PassengerSecurityNotes TEXT
CONSTRAINT PKUWAL PRIMARY KEY CLUSTERED(UWALID ASC),
CONSTRAINT FKUWALPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKUWALPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWALDAirport FOREIGN KEY(DepartICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALAAirport FOREIGN KEY(ArriveICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALFleet FOREIGN KEY(FleetID) REFERENCES Fleet(FleetID),
CONSTRAINT FKUWALUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKUWALI1Airport FOREIGN KEY(IntermediateICAO1) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALI2Airport FOREIGN KEY(IntermediateICAO2) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALI3Airport FOREIGN KEY(IntermediateICAO3) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALI4Airport FOREIGN KEY(IntermediateICAO4) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALWPAirport FOREIGN KEY(WeatherPreferredAltICAO) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALPAAirport FOREIGN KEY(PreferredAlternateICAO) REFERENCES Airport(AirportID)
)
GO

CREATE TABLE UWALTrans
(
UWALTransID	BIGINT,
TripID BIGINT,
LegNUM INT,
LegID BIGINT,
ActionCD CHAR(1),
TableName VARCHAR(100),
PassengerCrewIndicator CHAR(1),
FieldName VARCHAR(100),
FieldDescription VARCHAR(40),
IsOptionGroup BIT,
FieldType CHAR(1),
FieldLength	NUMERIC(3),
FieldDecimalPlaces NUMERIC(2),
OldValue TEXT,
NewValue TEXT,
LastUptTS DATETIME,
LastUpdUID CHAR(30)
CONSTRAINT PKUWALTrans PRIMARY KEY CLUSTERED(UWALTransID ASC),
CONSTRAINT FKUWALTransPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKUWALTransPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWALTransUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE UWALTssp
(
UWALTsspID BIGINT,
LegUVID BIGINT,
UVDescription VARCHAR(40),
UVStatus VARCHAR(30),
Remarks TEXT,
TripUVID BIGINT,
CONSTRAINT PKUWALTssp PRIMARY KEY CLUSTERED(UWALTsspID ASC),
CONSTRAINT FKUWALTsspUWATSSLeg FOREIGN KEY(LegUVID) REFERENCES UWATSSLeg(LegUVID),
CONSTRAINT FKUWALTsspUWATSSMain FOREIGN KEY(TripUVID) REFERENCES UWATSSMain(TripUVID)
)
GO

CREATE TABLE UWALTsLeg
(
UWALTsLegID	BIGINT,
TripID BIGINT,
LegNUM INT,
LegID BIGINT,
LegUVID BIGINT,
DepartICAOID BIGINT,
ArriveICAOID BIGINT,
DepartureGreenwichDTTM DATETIME,
ArrivalGreenwichDTTM DATETIME,
PilotInCommand CHAR(3),		
SecondInCommand CHAR(3),		
PilotInCommandSecondInCommandFullNames VARCHAR(40),
DepartureDTTMLocal DATETIME,
ArrivalDTTMLocal DATETIME,
UWALTsLegStatus CHAR(10),
PermitRemarks TEXT,
LandingStatus VARCHAR(30),
LandingPermitNUM VARCHAR(30),
PPRStatus VARCHAR(30),
PPRPermitNUM VARCHAR(30),
HandlingStatus VARCHAR(30),
HandlerInformation TEXT,
HandlerRemarks TEXT,
DepartureSlotUTCDTTM VARCHAR(20),
DepartureSlotStatus VARCHAR(30),
DepartureSlotNUM VARCHAR(20),
ArrivalSlotUTCDTTM VARCHAR(20),
ArrivalSlotStatus VARCHAR(30),
ArrivalSlotNUM VARCHAR(20),
SlotRemarks TEXT,
CustomStatus VARCHAR(30),
CustomInspectorName VARCHAR(30),
CustomDTCompleted CHAR(10),
ImmigrationStatus VARCHAR(30),
ImmigrationInspectorName VARCHAR(30),
ImmigrationDTCompleted CHAR(10),
AgriculturalStatus VARCHAR(30),
AgriculturalInspectorName VARCHAR(30),
AgricultureDateCompleted CHAR(10),
CrewHotelStatus	VARCHAR(30),
CrewHotelConfirmNUM	VARCHAR(30),
IsCrewHotelMultiBooking	BIT,
CrewHotelReservationInfo TEXT,
PassengerHotelStatus VARCHAR(30),
PassengerHotelConfirmNUM VARCHAR(30),
IsPassengerHotelMultiBooking BIT,
PassengerHotelReservationInfo TEXT,
FuelStatus VARCHAR(30),
UVAirPriceEstimate VARCHAR(20),
UVAirQuoteDT VARCHAR(15),
UVAirLocationStatus CHAR(1),
FuelerInformation TEXT,
LastUpdUID CHAR(30),	
LastUptTS DATETIME,
UVTripID BIGINT,
RefreshedTM	DATETIME
CONSTRAINT PKUWALTsLeg PRIMARY KEY CLUSTERED(UWALTsLegID ASC),
CONSTRAINT FKUWALTsLegPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKUWALTsLegPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWALTsLegUWATSSLeg FOREIGN KEY(LegUVID) REFERENCES UWATSSLeg(LegUVID),
CONSTRAINT FKUWALTsLegDAirport FOREIGN KEY(DepartICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALTsLegAAirport FOREIGN KEY(ArriveICAOID) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALTsLegUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName),
CONSTRAINT FKUWALTsLegUWATSSMain FOREIGN KEY(UVTripID) REFERENCES UWATSSMain(TripUVID)
)
GO

CREATE TABLE UWALTsPer
(
UWALTsPerID	BIGINT,
TripID BIGINT,
LegUVID BIGINT,
CountryID BIGINT,		
UWALTsPerStatus	VARCHAR(30),
PermitNUM VARCHAR(20)
CONSTRAINT PKUWALTsPer PRIMARY KEY CLUSTERED(UWALTsPerID ASC),
CONSTRAINT FKUWALTsPerPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKUWALTsPerUWATSSLeg FOREIGN KEY(LegUVID) REFERENCES UWATSSLeg(LegUVID),
CONSTRAINT FKUWALTsPerCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
)
GO

CREATE TABLE UWALtSofl
(
UWALtSoflID	BIGINT,
LegUVID BIGINT,
CountryID BIGINT,
UWALtSoflStatus	VARCHAR(30),
PermitNUM VARCHAR(20),		
TripUVID BIGINT
CONSTRAINT PKUWALtSofl PRIMARY KEY CLUSTERED(UWALtSoflID ASC),
CONSTRAINT FKUWALtSoflUWATSSLeg FOREIGN KEY(LegUVID) REFERENCES UWATSSLeg(LegUVID),
CONSTRAINT FKUWALtSoflCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKUWALtSoflUWATSSMain FOREIGN KEY(TripUVID) REFERENCES UWATSSMain(TripUVID)
)
GO

CREATE TABLE UWALPlan
(
UWALPlanID BIGINT,
LegID BIGINT,
FlightPlanID INT,
SubmissionDT DATE,
EstDepartureTM CHAR(5),
Payload	CHAR(10),
Dispatcher CHAR(3),
AlternateICAO BIGINT,			
PlanID NUMERIC(6),
FlightPlanNUM CHAR(10),
FileStatus CHAR(10),
FilingDTTM DATETIME,
FileMessage VARCHAR(50),
AirTrafficControlAcknowlge VARCHAR(50),
CruiseSpeed	CHAR(10),
FuelOption NUMERIC(1),
ArrivalFuelQTY NUMERIC(6),
DepartureFuelQTY NUMERIC(6),
ExtraFuelQty NUMERIC(6),
TakeOffWeight NUMERIC(6),
LandingWeight NUMERIC(6),
ProfileFlightLevels	VARCHAR(70),
MaxFlightLevel	NUMERIC(5),
BasicOperatingWeight NUMERIC(7),
HoldTM NUMERIC(5),
HoldFuel NUMERIC(5),
IsRemoveReserveFuel BIT,
ReserveTM NUMERIC(5),
ReserveFuel	NUMERIC(5),
FlightPlanResult TEXT,
FlightPlanTYPE NUMERIC(1),
FlightNUM VARCHAR(12),
OperatorName VARCHAR(40),
CaptainName	VARCHAR(25),
RemarksToAirTrafficControl VARCHAR(60),
RouteOptions NUMERIC(1),
DesiredRoute VARCHAR(60),
StoredRoute CHAR(10),
WindAltitude NUMERIC(6),
IsWeatherAutoPrint BIT,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
UWALPlanSID	CHAR(8),
DepartFix CHAR(6),
ArriveFix CHAR(6),
Star CHAR(8),
WindTemp NUMERIC(1),
WindTYPE NUMERIC(1),
WindSpeed NUMERIC(3),
ISATemp	NUMERIC(1),
SellAircraftCelcius	NUMERIC(3),
WindMonth CHAR(3),
UWALPlanStatus CHAR(9),
FOA	NUMERIC(6),
Cout1 CHAR(4),
Cin1 CHAR(4),
Cout2 CHAR(4),
Cin2 CHAR(4),
Cout3 CHAR(4),
Cin3 CHAR(4),
Cout4 CHAR(4),
Cin4 CHAR(4),
Cout5 CHAR(4),
Cin5 CHAR(4),
Cout6 CHAR(4),
Cin6 CHAR(4),
OutBound NUMERIC(4),
NatTrack CHAR(1)
CONSTRAINT PKUWALPlan PRIMARY KEY CLUSTERED(UWALPlanID ASC),
CONSTRAINT FKUWALPlanPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWALPlanAirport FOREIGN KEY(AlternateICAO) REFERENCES Airport(AirportID),
CONSTRAINT FKUWALPlanUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE UWALWx
(
UWALWxID BIGINT,
LegID BIGINT,
FlightPlanID INT,
WeatherTYPE	CHAR(1),
WeatherID CHAR(10),
WeatherDepartureCD TEXT,
WeatherArrivalCD TEXT,
WeatherGraphic1Name	VARCHAR(13),
WeatherGraphic2Name	VARCHAR(13),
IsSelected BIT,
LastUpdUID CHAR(30),
LastUptTS DATETIME,
WeatherAlternateCD TEXT,
WeatherGraphics3Name VARCHAR(13)
CONSTRAINT PKUWALWx PRIMARY KEY CLUSTERED(UWALWxID ASC),
CONSTRAINT FKUWALWxPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWALWxUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE UWALAvail
(
UWALAvailID	BIGINT,
WeatherTYPE	CHAR(1),
WeatherID CHAR(10),
WeatherName	VARCHAR(50),
Command	VARCHAR(15),
IsDeparture	BIT,
IsArrivalFlag BIT,
IsAlternateFlag	BIT,
WeatherDepartureCD TEXT,
WeatherArrivalCD TEXT,
WeatherAlternateCD TEXT,
GRX1CD TEXT,
GRX2CD TEXT,
GRX3CD TEXT,
UWALAvailOrder NUMERIC(3),
GrxTYPE	CHAR(3),
Size NUMERIC(8),
SizePlus20 NUMERIC(8),
IsDefault BIT
CONSTRAINT PKUWALAvail PRIMARY KEY CLUSTERED(UWALAvailID ASC),
)
GO

CREATE TABLE UWALUser
(
UWALUserID BIGINT,
UserName CHAR(30), -- UserName - (UserMaster(UserName))
WeatherTYPE	CHAR(1),
WeatherID CHAR(10),
UWALUserOrder INT
CONSTRAINT PKUWALUser PRIMARY KEY CLUSTERED(UWALUserID ASC),
CONSTRAINT FKUWALUserUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 30-Jul-2012 --

ALTER TABLE PreflightCrewList DROP CONSTRAINT FKPreflightCrewListHotel
GO

ALTER TABLE PreflightCrewList DROP COLUMN PreflightHotelID
GO

ALTER TABLE PreflightPassengerList DROP CONSTRAINT FKPreflightPassengerListHotel
GO

ALTER TABLE PreflightPassengerList DROP COLUMN PreflightHotelID
GO

CREATE TABLE PreflightCrewHotelList
(
PreflightCrewHotelListID BIGINT,
PreflightHotelName VARCHAR(60),
PreflightCrewListID BIGINT,
HotelID BIGINT,
AirportID BIGINT,
RoomType VARCHAR(60),
RoomRent NUMERIC(6,2),
RoomConfirm TEXT,
RoomDescription TEXT,
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightCrewHotelList PRIMARY KEY CLUSTERED(PreflightCrewHotelListID),
CONSTRAINT FKPreflightCrewHotelListPreflightCrewList FOREIGN KEY(PreflightCrewListID) REFERENCES PreflightCrewList(PreflightCrewListID),
CONSTRAINT FKPreflightCrewHotelListHotel FOREIGN KEY(HotelID) REFERENCES Hotel(HotelID),
CONSTRAINT FKPreflightCrewHotelListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightCrewHotelListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE PreflightPassengerHotelList
(
PreflightCrewHotelListID BIGINT,
PreflightHotelName VARCHAR(60),
PreflightPassengerListID BIGINT,
HotelID BIGINT,
AirportID BIGINT,
RoomType VARCHAR(60),
RoomRent NUMERIC(6,2),
RoomConfirm TEXT,
RoomDescription TEXT,
Street VARCHAR(50),
CityName VARCHAR(20),
StateName VARCHAR(20),
PostalZipCD CHAR(20),
PhoneNum1 VARCHAR(25),
PhoneNum2 VARCHAR(25),
PhoneNum3 VARCHAR(25),
PhoneNum4 VARCHAR(25),
FaxNUM VARCHAR(25),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKPreflightPassengerHotelList PRIMARY KEY CLUSTERED(PreflightCrewHotelListID),
CONSTRAINT FKPreflightPassengerHotelListPreflightPassengerList FOREIGN KEY(PreflightPassengerListID) REFERENCES PreflightPassengerList(PreflightPassengerListID),
CONSTRAINT FKPreflightPassengerHotelListHotel FOREIGN KEY(HotelID) REFERENCES Hotel(HotelID),
CONSTRAINT FKPreflightPassengerHotelListAirport FOREIGN KEY(AirportID) REFERENCES Airport(AirportID),
CONSTRAINT FKPreflightPassengerHotelListUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 31-Jul-2012 --

ALTER TABLE PreflightCrewHotelList ADD HotelArrangedBy VARCHAR(10)
GO

ALTER TABLE PreflightCrewHotelList ADD Rate NUMERIC(17, 2)
GO

ALTER TABLE PreflightCrewHotelList ADD DateIn DATE
GO

ALTER TABLE PreflightCrewHotelList ADD DateOut DATE
GO

ALTER TABLE PreflightCrewHotelList ADD IsSmoking BIT
GO

ALTER TABLE PreflightCrewHotelList ADD RoomTypeID BIGINT
GO

ALTER TABLE PreflightCrewHotelList ADD NoofBeds NUMERIC(3, 0)
GO

ALTER TABLE PreflightCrewHotelList ADD IsAllCrew BIT
GO

ALTER TABLE PreflightCrewHotelList ADD IsCompleted BIT
GO

ALTER TABLE PreflightCrewHotelList ADD IsEarlyCheckIn BIT
GO

ALTER TABLE PreflightCrewHotelList ADD EarlyCheckInOption CHAR(1)
GO

ALTER TABLE PreflightCrewHotelList ADD SpecialInstructions VARCHAR(100)
GO

ALTER TABLE PreflightCrewHotelList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightCrewHotelList ADD Comments VARCHAR(100)
GO

ALTER TABLE PreflightCrewHotelList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightCrewHotelList ADD CONSTRAINT FKPreflightCrewHotelListRoomType FOREIGN KEY(RoomTypeID) REFERENCES RoomType(RoomTypeID)
GO

ALTER TABLE PreflightCrewHotelList ADD CONSTRAINT FKPreflightCrewHotelListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

ALTER TABLE PreflightPassengerHotelList ADD HotelArrangedBy VARCHAR(10)
GO

ALTER TABLE PreflightPassengerHotelList ADD Rate NUMERIC(17, 2)
GO

ALTER TABLE PreflightPassengerHotelList ADD DateIn DATE
GO

ALTER TABLE PreflightPassengerHotelList ADD DateOut DATE
GO

ALTER TABLE PreflightPassengerHotelList ADD IsSmoking BIT
GO

ALTER TABLE PreflightPassengerHotelList ADD RoomTypeID BIGINT
GO

ALTER TABLE PreflightPassengerHotelList ADD NoofBeds NUMERIC(3, 0)
GO

ALTER TABLE PreflightPassengerHotelList ADD IsAllCrew BIT
GO

ALTER TABLE PreflightPassengerHotelList ADD IsCompleted BIT
GO

ALTER TABLE PreflightPassengerHotelList ADD IsEarlyCheckIn BIT
GO

ALTER TABLE PreflightPassengerHotelList ADD EarlyCheckInOption CHAR(1)
GO

ALTER TABLE PreflightPassengerHotelList ADD SpecialInstructions VARCHAR(100)
GO

ALTER TABLE PreflightPassengerHotelList ADD ConfirmationStatus VARCHAR(100)
GO

ALTER TABLE PreflightPassengerHotelList ADD Comments VARCHAR(100)
GO

ALTER TABLE PreflightPassengerHotelList ADD CustomerID BIGINT
GO

ALTER TABLE PreflightPassengerHotelList ADD CONSTRAINT FKPreflightPassengerHotelListRoomType FOREIGN KEY(RoomTypeID) REFERENCES RoomType(RoomTypeID)
GO

ALTER TABLE PreflightPassengerHotelList ADD CONSTRAINT FKPreflightPassengerHotelListCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
GO

-- Database Change Log 03-Aug-2012 --

ALTER TABLE PreflightTripException ADD ExceptionTemplateID BIGINT
GO

ALTER TABLE PreflightTripException ADD CONSTRAINT FKPreflightTripExceptionExceptionTemplate FOREIGN KEY(ExceptionTemplateID) REFERENCES ExceptionTemplate(ExceptionTemplateID)
GO

sp_rename 'UWATSSMain.LastUserID','LastUpdUID', 'COLUMN'
GO

sp_rename 'UWATSSLeg.LastUserID','LastUpdUID', 'COLUMN'
GO

-- Database Change Log 06-Aug-2012 --

CREATE TABLE UWASpServ
(
UWASpServID BIGINT,
LegID BIGINT,
UWASpServDescription VARCHAR(30),
UWASpServInstruction VARCHAR(150),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKUWASpServ PRIMARY KEY CLUSTERED(UWASpServID ASC),
CONSTRAINT FKUWASpServPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID),
CONSTRAINT FKUWASpServUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

ALTER TABLE UWALTsPer DROP CONSTRAINT FKUWALTsPerUWATSSLeg
GO

ALTER TABLE UWALTsPer DROP COLUMN LegUVID
GO

ALTER TABLE UWALTsPer ADD LegID BIGINT
GO

ALTER TABLE UWALTsPer ADD CONSTRAINT UWALTsPerPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID)
GO

ALTER TABLE PreflightPassengerList ADD Street VARCHAR(50)
GO

ALTER TABLE PreflightPassengerList ADD CityName VARCHAR(20)
GO

ALTER TABLE PreflightPassengerList ADD StateName VARCHAR(20)
GO

ALTER TABLE PreflightPassengerList ADD PostalZipCD CHAR(20)
GO

-- Database Change Log 09-Aug-2012 --

sp_rename 'PreflightPassengerHotelList.PreflightCrewHotelListID','PreflightPassengerHotelListID','COLUMN'
GO

DROP INDEX PreflightCheckListIDX ON PreflightCheckList
GO

DROP INDEX PreflightCheckListLIDX ON PreflightCheckList
GO

DROP INDEX PreflightTripSIFLIDX ON PreflightTripSIFL
GO

ALTER TABLE PreflightCheckList ADD CheckGroupID BIGINT
GO

ALTER TABLE PreflightCheckList ADD CONSTRAINT FKPreflightCheckListTripManagerCheckListGroup FOREIGN KEY(CheckGroupID) REFERENCES TripManagerCheckListGroup(CheckGroupID)
GO

ALTER TABLE PreflightCateringDetail ADD IsUWAArranger BIT
GO

CREATE TABLE UWAPermit
(
UWAPermit BIGINT,
LegID BIGINT,
PermitID INT,
Country VARCHAR(60),
PermitNumber VARCHAR(20),
ValidDate DATE
CONSTRAINT PKUWAPermit PRIMARY KEY CLUSTERED(UWAPermit ASC),
CONSTRAINT FKUWAPermitPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID)
)
GO

-- Database Change Log 13-Aug-2012 --

ALTER TABLE PreflightFuel ADD TripID BIGINT
GO

ALTER TABLE PreflightFuel ADD CONSTRAINT FKPreflightFuelPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID)
GO

sp_rename 'PreflightFuel.GallongFrom','GallonFrom', 'COLUMN'
GO

sp_rename 'PreflightFuel.GallongTo', 'GallonTo', 'COLUMN'
GO

ALTER TABLE PreflightFuel DROP CONSTRAINT FKPreflightFuelVendor
GO

DROP INDEX PreflightFuelIDX ON PreflightFuel
GO

ALTER TABLE PreflightFuel DROP COLUMN VendorID
GO

ALTER TABLE PreflightFuel ADD FuelVendorID BIGINT
GO

ALTER TABLE PreflightFuel ADD CONSTRAINT FKPreflightFuelFuelVendor FOREIGN KEY(FuelVendorID) REFERENCES FuelVendor(FuelVendorID)
GO

-- Database Change Log 14-Aug-2012 --

ALTER TABLE PreflightFuel DROP CONSTRAINT PKPreflightFuel
GO

ALTER TABLE PreflightFuel DROP COLUMN PreflightFuelID
GO

ALTER TABLE PreflightFuel ADD PreflightFuelID UNIQUEIDENTIFIER
GO

ALTER TABLE PreflightFuel ADD CONSTRAINT PKPreflightFuel DEFAULT(NEWSEQUENTIALID()) FOR PreflightFuelID
GO

-- Database Change Log 20-Aug-2012 --

--ALTER TABLE PreflightCrewHotelList ADD LegID BIGINT
--GO

--ALTER TABLE PreflightCrewHotelList ADD CONSTRAINT FKPreflightCrewHotelListPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID)
--GO

--ALTER TABLE PreflightPassengerHotelList ADD LegID BIGINT
--GO

--ALTER TABLE PreflightPassengerHotelList ADD CONSTRAINT FKPreflightPassengerHotelListPreflightLeg FOREIGN KEY(LegID) REFERENCES PreflightLeg(LegID)
--GO

-- Database Change Log 24-Aug-2012 --

CREATE TABLE PreflightAdvancedFilters
(
FilterID BIGINT,
UserName CHAR(30),
CustomerID BIGINT,
Settings XML,
CONSTRAINT PKPreflightAdvancedFilters PRIMARY KEY CLUSTERED(FilterID ASC),
CONSTRAINT FKPreflightAdvancedFiltersUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName),
CONSTRAINT FKPreflightAdvancedFiltersCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

-- Database Change Log 30-Aug-2012 --

ALTER TABLE PreflightFuel DROP CONSTRAINT PKPreflightFuel
GO

ALTER TABLE PreflightFuel ALTER COLUMN PreflightFuelID UNIQUEIDENTIFIER NOT NULL
GO

ALTER TABLE PreflightFuel ADD CONSTRAINT PKPreflightFuel PRIMARY KEY CLUSTERED(PreflightFuelID)
GO

-- Database Change Log 31-Aug-2012 --

sp_rename 'PreflightTripSIFL.EmployeeTaxInfoTYPE','EmployeeTYPE'
GO

ALTER TABLE PreflightTripSIFL ADD AssociatedPassengerID BIGINT
GO

ALTER TABLE PreflightTripSIFL DROP COLUMN AssociatedWithCD
GO

ALTER TABLE PreflightTripSIFL ADD CONSTRAINT FKPreflightTripSIFLAssociatedPassenger FOREIGN KEY(AssociatedPassengerID) REFERENCES Passenger(PassengerRequestorID)
GO

-- Database Change Log 07-Sep-2012 --

ALTER TABLE PreflightTripException ALTER COLUMN ExceptionDescription VARCHAR(1500)
GO

-- Database Change Log 13-Sep-2012 --

ALTER TABLE PreflightTransportList ADD IsCompleted BIT
GO

-- Database Change Log 21-Sep-2012 --

ALTER TABLE PreflightFuel ADD UWAFuelInterfaceRequestID UNIQUEIDENTIFIER NULL
GO

-- Database Change Log 24-Sep-2012 --

ALTER TABLE PreflightFuel ADD CONSTRAINT DEPreflightFuel DEFAULT(NEWSEQUENTIALID()) FOR PreflightFuelID
GO

-- Database Change Log 17-Nov-2012 --

ALTER TABLE PreflightTripException ADD DisplayOrder VARCHAR(15)
GO

-- Database Change Log 20-Nov-2012 --

CREATE TABLE TripSheetReportHeader
(
TripSheetReportHeaderID BIGINT,
CustomerID BIGINT,
ReportID CHAR(8),
ReportDescription VARCHAR(40),
HomebaseID BIGINT,
UserGroupID BIGINT,
ClientID BIGINT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKTripSheetReportHeader PRIMARY KEY CLUSTERED(TripSheetReportHeaderID ASC),
CONSTRAINT FKTripSheetReportHeaderCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTripSheetReportHeaderCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID),
CONSTRAINT FKTripSheetReportHeaderUserGroup FOREIGN KEY(UserGroupID) REFERENCES UserGroup(UserGroupID),
CONSTRAINT FKTripSheetReportHeaderClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKTripSheetReportHeaderUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TripSheetReportDefinition
(
TripSheetReportDefinitionID BIGINT,
CustomerID BIGINT,
ReportNumber INT,
ReportDescription VARCHAR(35),
ParameterVariable VARCHAR(10),
ParameterDescription VARCHAR(100),
ParameterType CHAR(1),
ParameterWidth INT,
ParameterCValue VARCHAR(60),
ParameterLValue BIT,
ParameterNValue INT,
ParameterValue VARCHAR(1500),
ReportProcedure VARCHAR(8),
ReportOrder INT,
TripSheetOrder INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKTripSheetReportDefinition PRIMARY KEY CLUSTERED(TripSheetReportDefinitionID ASC),
CONSTRAINT FKTripSheetReportDefinitionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTripSheetReportDefinitionUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE TripSheetReportDetail
(
TripSheetReportDetailID BIGINT,
CustomerID BIGINT,
TripSheetReportHeaderID BIGINT,
ReportNumber INT,
ReportDescription VARCHAR(35),
IsSelected BIT,
Copies INT,
ReportProcedure VARCHAR(8),
ParameterVariable VARCHAR(10),
ParameterDescription VARCHAR(100),
ParameterType CHAR(1),
ParameterWidth INT,
ParameterCValue VARCHAR(60),
ParameterLValue BIT,
ParameterNValue INT,
ParameterValue VARCHAR(1500),
ReportOrder INT,
TripSheetOrder INT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT DEFAULT (0) NOT NULL,
CONSTRAINT PKTripSheetReportDetail PRIMARY KEY CLUSTERED(TripSheetReportDetailID ASC),
CONSTRAINT FKTripSheetReportDetailCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKTripSheetReportDetailTripSheetReportHeader FOREIGN KEY(TripSheetReportHeaderID) REFERENCES TripSheetReportHeader(TripSheetReportHeaderID),
CONSTRAINT FKTripSheetReportDetailUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 19-Dec-2012 --

sp_rename 'TripSheetReportDetail.ParameterVariable','ParameterCD'
GO

-- Database Change Log 21-Dec-2012 --

CREATE TABLE PreflightTripHistory
(
TripHistoryID BIGINT,
TripID BIGINT,
CustomerID BIGINT,
HistoryDescription NVARCHAR(MAX),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
RevisionNumber INT,
CONSTRAINT PKPreflightTripHistory PRIMARY KEY CLUSTERED(TripHistoryID ASC),
CONSTRAINT FKPreflightTripHistoryPreflightMain FOREIGN KEY(TripID) REFERENCES PreflightMain(TripID),
CONSTRAINT FKPreflightTripHistoryCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKPreflightTripHistoryUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

ALTER TABLE TripSheetReportDetail ADD ParameterSize VARCHAR(10)
GO

ALTER TABLE TripSheetReportDetail ADD WarningMessage VARCHAR(100)
GO

-- SQL Script File 04-Jan-2013 --