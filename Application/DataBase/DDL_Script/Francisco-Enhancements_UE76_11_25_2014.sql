------------------------------------------- UE-76 ticket number
-- used in SP EstDepartureDT is covering index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostFlightMain_EstDepartureDT_IDX' AND object_id = OBJECT_ID('PostFlightMain'))
BEGIN
	DROP INDEX [PostFlightMain_EstDepartureDT_IDX] ON [dbo].[PostFlightMain]
END
GO 
CREATE NONCLUSTERED INDEX [PostFlightMain_EstDepartureDT_IDX] ON [dbo].[PostFlightMain]
(
	[EstDepartureDT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO