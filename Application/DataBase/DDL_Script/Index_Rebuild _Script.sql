DECLARE @TableName VARCHAR(MAX)
DECLARE @IndexName VARCHAR(MAX)
DECLARE @command VARCHAR(8000)

DECLARE CurTmp CURSOR FOR 
SELECT OBJECT_NAME(ind.OBJECT_ID) AS TableName, 
ind.name AS IndexName
 
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) indexstats 
INNER JOIN sys.indexes ind  
ON ind.object_id = indexstats.object_id 
AND ind.index_id = indexstats.index_id 
WHERE indexstats.avg_fragmentation_in_percent > 30 
AND indexstats.page_count > 50
ORDER BY indexstats.avg_fragmentation_in_percent DESC


OPEN CurTmp

FETCH NEXT FROM CurTmp INTO @TableName,@IndexName

WHILE (@@FETCH_STATUS = 0)
BEGIN 


IF ISNULL(@IndexName,'') <> '' AND  ISNULL(@TableName,'') <> ''
SET @command = N' ALTER INDEX '+ ISNULL(@IndexName,'') + N' ON [dbo].[' + ISNULL(@TableName,'') + N'] REBUILD; ';

PRINT @Command
EXEC (@command)




FETCH NEXT FROM CurTmp INTO @TableName,@IndexName
END


CLOSE CurTmp
DEALLOCATE CurTmp