/*

Altered Table for updating Foreign Key with CQQuoteMain ID

*/


DROP TABLE CQQuoteSummary
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CQQuoteSummary](
	[CQQuoteSummaryID] [bigint] NOT NULL,
	[CustomerID] [bigint] NULL,
	[CQQuoteMainID] [bigint] NULL,
	[IsPrintUsageAdj] [bit] NULL,
	[UsageAdjDescription] [varchar](60) NULL,
	[CustomUsageAdj] [numeric](17, 2) NULL,
	[IsPrintAdditonalFees] [bit] NULL,
	[AdditionalFeeDescription] [varchar](60) NULL,
	[AdditionalFeesTax] [numeric](17, 2) NULL,
	[PositioningDescriptionINTL] [numeric](17, 2) NULL,
	[IsAdditionalFees] [numeric](17, 2) NULL,
	[CustomAdditionalFees] [numeric](17, 2) NULL,
	[IsPrintLandingFees] [bit] NULL,
	[LandingFeeDescription] [varchar](60) NULL,
	[CustomLandingFee] [numeric](17, 2) NULL,
	[IsPrintSegmentFee] [bit] NULL,
	[SegmentFeeDescription] [varchar](60) NULL,
	[CustomSegmentFee] [numeric](17, 2) NULL,
	[IsPrintStdCrew] [bit] NULL,
	[StdCrewROMDescription] [varchar](60) NULL,
	[CustomStdCrewRON] [numeric](17, 2) NULL,
	[IsPrintAdditionalCrew] [bit] NULL,
	[AdditionalCrewDescription] [varchar](60) NULL,
	[CustomAdditionalCrew] [numeric](17, 2) NULL,
	[IsAdditionalCrewRON] [bit] NULL,
	[AdditionalDescriptionCrewRON] [varchar](60) NULL,
	[CustomAdditionalCrewRON] [numeric](17, 2) NULL,
	[IsPrintWaitingChg] [bit] NULL,
	[WaitChgDescription] [varchar](60) NULL,
	[CustomWaitingChg] [numeric](17, 2) NULL,
	[IsPrintFlightChg] [bit] NULL,
	[FlightChgDescription] [varchar](60) NULL,
	[CustomFlightChg] [numeric](17, 2) NULL,
	[IsPrintSubtotal] [bit] NULL,
	[SubtotalDescription] [varchar](60) NULL,
	[IsPrintDiscountAmt] [bit] NULL,
	[DescriptionDiscountAmt] [varchar](60) NULL,
	[IsPrintTaxes] [bit] NULL,
	[TaxesDescription] [varchar](60) NULL,
	[IsPrintInvoice] [bit] NULL,
	[InvoiceDescription] [varchar](60) NULL,
	[ReportQuote] [numeric](17, 2) NULL,
	[IsPrintPay] [bit] NULL,
	[PrepayDescription] [varchar](60) NULL,
	[IsPrintRemaingAMT] [bit] NULL,
	[RemainingAmtDescription] [varchar](60) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PKCQQuoteSummary] PRIMARY KEY CLUSTERED 
(
	[CQQuoteSummaryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CQQuoteSummary]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteSummaryCQQuoteMain] FOREIGN KEY([CQQuoteMainID])
REFERENCES [dbo].[CQQuoteMain] ([CQQuoteMainID])
GO

ALTER TABLE [dbo].[CQQuoteSummary] CHECK CONSTRAINT [FKCQQuoteSummaryCQQuoteMain]
GO

ALTER TABLE [dbo].[CQQuoteSummary]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteSummaryCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[CQQuoteSummary] CHECK CONSTRAINT [FKCQQuoteSummaryCustomer]
GO

ALTER TABLE [dbo].[CQQuoteSummary]  WITH CHECK ADD  CONSTRAINT [FKCQQuoteSummaryUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[CQQuoteSummary] CHECK CONSTRAINT [FKCQQuoteSummaryUserMaster]
GO

ALTER TABLE [dbo].[CQQuoteSummary] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO


