------------------------------------------- UE-62 ticket number
-- used in spGetHotelByHotelCode SP CountryID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Hotel_CountryID_IDX' AND object_id = OBJECT_ID('Hotel'))
BEGIN
	DROP INDEX [Hotel_CountryID_IDX] ON [dbo].[Hotel]
END
GO
CREATE NONCLUSTERED INDEX [Hotel_CountryID_IDX] ON [dbo].[Hotel]
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO