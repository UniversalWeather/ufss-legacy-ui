UPDATE Airport set LengthWidthRunway=Runway_Temp.LengthWidthRunway,WidthLengthRunway=Runway_Temp.WidthLengthRunway,
Runway2Length=Runway_Temp.Runway2Length,
Runway2Width=Runway_Temp.Runway2Width,
Runway3Length=Runway_Temp.Runway3Length,
Runway3Width=Runway_Temp.Runway3Width,
Runway4Length=Runway_Temp.Runway4Length,
Runway4Width=Runway_Temp.Runway4Width from

(Select UWAID 
       , MIN([1]) AS LengthWidthRunway
       , MIN([W1]) AS WidthLengthRunway
       , MIN([2]) AS Runway2Length
       , MIN([W2]) AS Runway2Width
       , MIN([3]) AS Runway3Length
       , MIN([W3]) AS Runway3Width
       , MIN([4]) AS Runway4Length
       , MIN([W4]) AS Runway4Width
From
(Select Rnk_R.UWAID, Rnk_R.L_RNK, Rnk_R.W_RNK, Rnk_R.RunwayLength, Rnk_R.RunwayWidth
  From (Select ROW_NUMBER() over(PARTITION BY UWAID ORDER BY RunwayLength DESC)  AS L_RNK
               , 'W' + CAST(ROW_NUMBER() over(PARTITION BY UWAID ORDER BY RunwayLength DESC) AS CHAR(1)) AS W_RNK
               , UWAID
               , [RunwayLength]
               , RunwayWidth
       From Runway ) As Rnk_R 
      Where L_Rnk <= 4 
      --AND UWAID  = '12228.*A'
) AS R
PIVOT
(
MIN(RunwayLength)
FOR L_RNK IN ([1], [2], [3], [4])
) L
PIVOT
(
MIN(RunwayWidth)
FOR W_RNK IN ([W1], [W2], [W3], [W4])
) W
GROUP BY UWAID) as Runway_Temp  where Airport.UWAID=Runway_Temp.UWAID