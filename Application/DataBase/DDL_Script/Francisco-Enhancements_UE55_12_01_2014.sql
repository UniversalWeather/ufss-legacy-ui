------------------------------------------- UE-UE-55 ticket numer
-- used in SP spGetAllPassenger_TelerikPagedGrid  - PassengerRequestorID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerGroupOrder_PassengerRequestorID_IDX' AND object_id = OBJECT_ID('PassengerGroupOrder'))
    BEGIN
        DROP INDEX [PassengerGroupOrder_PassengerRequestorID_IDX] ON [dbo].[PassengerGroupOrder]
    END
GO

CREATE NONCLUSTERED INDEX [PassengerGroupOrder_PassengerRequestorID_IDX] ON [dbo].[PassengerGroupOrder]
(
    [PassengerRequestorID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO