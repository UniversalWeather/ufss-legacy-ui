
-- // Fix for SUP-183 (7281)
ALTER TABLE CQQuoteMain
ADD FileWarehouseID3 BIGINT NULL,
	FileWarehouseID4 BIGINT NULL
GO