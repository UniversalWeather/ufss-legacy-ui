------------------------------------------- UE-112
-- used in SP sp_fss_Company
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_HomebaseAirportID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_HomebaseAirportID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_HomebaseAirportID_IDX] ON [dbo].[Company]
(
 [HomebaseAirportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-112
-- used in SP sp_fss_Company
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_CrewDutyID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_CrewDutyID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_CrewDutyID_IDX] ON [dbo].[Company]
(
 [CrewDutyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-112
-- used in SP sp_fss_Company
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_ExchangeRateID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_ExchangeRateID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_ExchangeRateID_IDX] ON [dbo].[Company]
(
 [ExchangeRateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-112
-- used in SP sp_fss_Company
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_DefaultFlightCatID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_DefaultFlightCatID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_DefaultFlightCatID_IDX] ON [dbo].[Company]
(
 [DefaultFlightCatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-112
-- used in SP sp_fss_Company
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_DefaultCheckListGroupID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_DefaultCheckListGroupID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_DefaultCheckListGroupID_IDX] ON [dbo].[Company]
(
 [DefaultCheckListGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-109
-- used in SP spGetPassengerPassportByPassengerRequestorID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerPassport_PassengerRequestorID_IDX' AND object_id = OBJECT_ID('CrewPassengerPassport'))
    BEGIN
        DROP INDEX [CrewPassengerPassport_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerPassport]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerPassport_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerPassport]
(
 [PassengerRequestorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-109
-- used in SP spGetPassengerPassportByPassengerRequestorID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerPassport_CustomerID_IDX' AND object_id = OBJECT_ID('CrewPassengerPassport'))
    BEGIN
        DROP INDEX [CrewPassengerPassport_CustomerID_IDX] ON [dbo].[CrewPassengerPassport]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerPassport_CustomerID_IDX] ON [dbo].[CrewPassengerPassport]
(
 [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-108
-- used in SP spGetAllCrewbyCrewID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Crew_CustomerID_IDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [Crew_CustomerID_IDX] ON [dbo].[Crew]
    END
GO

CREATE NONCLUSTERED INDEX [Crew_CustomerID_IDX] ON [dbo].[Crew]
(
 [DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-108
-- used in SP spGetAllCrewbyCrewID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Crew_HomebaseID_IDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [Crew_HomebaseID_IDX] ON [dbo].[Crew]
    END
GO

CREATE NONCLUSTERED INDEX [Crew_HomebaseID_IDX] ON [dbo].[Crew]
(
 [HomebaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-108
-- used in SP spGetAllCrewbyCrewID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Crew_CountryID_IDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [Crew_CountryID_IDX] ON [dbo].[Crew]
    END
GO

CREATE NONCLUSTERED INDEX [Crew_CountryID_IDX] ON [dbo].[Crew]
(
 [CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-108
-- used in SP spGetAllCrewbyCrewID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Crew_Citizenship_IDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [Crew_Citizenship_IDX] ON [dbo].[Crew]
    END
GO

CREATE NONCLUSTERED INDEX [Crew_Citizenship_IDX] ON [dbo].[Crew]
(
 [Citizenship] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-108
-- used in SP spGetAllCrewbyCrewID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Crew_CountryOfBirth_IDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [Crew_CountryOfBirth_IDX] ON [dbo].[Crew]
    END
GO

CREATE NONCLUSTERED INDEX [Crew_CountryOfBirth_IDX] ON [dbo].[Crew]
(
 [CountryOfBirth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 
