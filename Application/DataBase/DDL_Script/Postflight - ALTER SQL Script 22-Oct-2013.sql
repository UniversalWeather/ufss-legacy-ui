

-- Increase the Field Length, due to Error Converting Issue when selecting 
-- the large date range. Bug Id : UW-1036

ALTER TABLE [PostflightLeg] ALTER COLUMN [BlockHours] [numeric](10, 3) NULL
GO
ALTER TABLE [PostflightLeg] ALTER COLUMN [FlightHours] [numeric](10, 3) NULL
GO
ALTER TABLE [PostflightLeg] ALTER COLUMN [DutyHrs] [numeric](10, 3) NULL
GO

ALTER TABLE [PostflightCrew] ALTER COLUMN [DutyHours] [numeric](10, 3) NULL
GO
ALTER TABLE [PostflightCrew] ALTER COLUMN [BlockHours] [numeric](10, 3) NULL
GO
ALTER TABLE [PostflightCrew] ALTER COLUMN [FlightHours] [numeric](10, 3) NULL
GO