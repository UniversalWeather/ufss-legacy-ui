
-- =============================================        
-- Author: Karthikeyan.S  
-- Create date: 10/09/2013       
-- Description: Change Tail Num Datatype  
-- ============================================= 
ALTER TABLE Fleet 
ALTER COLUMN TailNum varchar(9)
