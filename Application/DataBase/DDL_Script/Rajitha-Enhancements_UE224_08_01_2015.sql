/****** Object:  Index [idx_spGetHotelsByAirportID]    Script Date: 1/8/2015 9:39:21 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Hotel]') AND name = N'idx_spGetHotelsByAirportID')
DROP INDEX [idx_spGetHotelsByAirportID] ON [dbo].[Hotel]
GO

/****** Object:  Index [idx_spGetHotelsByAirportID]    Script Date: 1/8/2015 9:39:21 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Hotel]') AND name = N'idx_spGetHotelsByAirportID')
CREATE NONCLUSTERED INDEX [idx_spGetHotelsByAirportID] ON [dbo].[Hotel]
(
	[AirportID] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO