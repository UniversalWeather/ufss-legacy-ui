/****** Object:  Index [PreflightMain_GetTripByTripNUM_idx]    Script Date: 1/1/2015 12:02:15 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightMain]') AND name = N'PreflightMain_GetTripByTripNUM_IDX')
DROP INDEX [PreflightMain_GetTripByTripNUM_IDX] ON [dbo].[PreflightMain]
GO

/****** Object:  Index [PreflightMain_GetTripByTripNUM_idx]    Script Date: 1/1/2015 12:02:15 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PreflightMain]') AND name = N'PreflightMain_GetTripByTripNUM_IDX')
CREATE NONCLUSTERED INDEX [PreflightMain_GetTripByTripNUM_IDX] ON [dbo].[PreflightMain]
(
	[TripID] ASC,
	[CustomerID] ASC,
	[TripNUM] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO