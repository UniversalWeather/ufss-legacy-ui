IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerFuelVendorAccess]') AND type in (N'U'))
BEGIN
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerFuelVendorAccess_UserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerFuelVendorAccess]'))
	BEGIN
		ALTER TABLE [dbo].[CustomerFuelVendorAccess] DROP CONSTRAINT [FK_CustomerFuelVendorAccess_UserMaster]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerFuelVendorAccess_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerFuelVendorAccess]'))
	BEGIN
		ALTER TABLE [dbo].[CustomerFuelVendorAccess] DROP CONSTRAINT [FK_CustomerFuelVendorAccess_Customer]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerFuelVendorAccess_FuelVendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerFuelVendorAccess]'))
	BEGIN
		ALTER TABLE [dbo].[CustomerFuelVendorAccess] DROP CONSTRAINT [FK_CustomerFuelVendorAccess_FuelVendor]
	END
	DROP TABLE [dbo].[CustomerFuelVendorAccess]
END 

CREATE TABLE [dbo].[CustomerFuelVendorAccess](
	[CustomerFuelVendorAccessID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerID] [BIGINT] NULL,
	[FuelVendorID] [BIGINT] NULL,
	[CanAccess] [bit] NULL,
	[LastUpdUID] [VARCHAR](30) NULL,
	[LastUpdTS] [DATETIME] NULL,
	[IsDeleted] [BIT] NULL,
	[IsInActive] [BIT] NULL
	CONSTRAINT [PK_CustomerFuelVendorAccess] PRIMARY KEY ([CustomerFuelVendorAccessID]),
	CONSTRAINT [FK_CustomerFuelVendorAccess_UserMaster] FOREIGN KEY([LastUpdUID]) REFERENCES [dbo].[UserMaster] (UserName),
	CONSTRAINT [FK_CustomerFuelVendorAccess_Customer] FOREIGN KEY([CustomerID]) REFERENCES [dbo].[Customer] (CustomerID),
	CONSTRAINT [FK_CustomerFuelVendorAccess_FuelVendor] FOREIGN KEY([FuelVendorID]) REFERENCES [dbo].[FuelVendor] (FuelVendorID)
)