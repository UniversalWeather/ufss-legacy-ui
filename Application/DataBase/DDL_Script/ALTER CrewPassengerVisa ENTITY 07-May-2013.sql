ALTER TABLE CrewPassengerVisa
ADD EntriesAllowedInPassport INT
GO

ALTER TABLE CrewPassengerVisa
ADD TypeOfVisa VARCHAR(10)
GO

ALTER TABLE CrewPassengerVisa
ADD VisaExpireInDays INT
GO