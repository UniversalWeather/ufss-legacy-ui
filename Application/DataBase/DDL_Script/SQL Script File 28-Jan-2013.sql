-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Database Module          --
-- Created Date: 28-Jan-2013        --
-- Updated Date: 28-Jan-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE WorldCity
(
WorldCityID BIGINT,
CityName VARCHAR(40),
StateName VARCHAR(40),
CountryID BIGINT,
LatitudeDegree NUMERIC(3),
LatitudeMinutes NUMERIC(4,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegrees NUMERIC(3),
LongitudeMinutes NUMERIC(4,1),
LongitudeEastWest CHAR(1),
Latitude NUMERIC(10,6),
Longitude NUMERIC(10,6),
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKWorldCity PRIMARY KEY CLUSTERED(WorldCityID ASC),
CONSTRAINT FKWorldCityCountry FOREIGN KEY(CountryID) REFERENCES Country(CountryID),
CONSTRAINT FKWorldCityUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE UserPreference
(
UserPreferenceID BIGINT,
CustomerID BIGINT NOT NULL,
UserName VARCHAR(30) NOT NULL,
CategoryName VARCHAR(100) NOT NULL,
SubCategoryName VARCHAR(100),
KeyName VARCHAR(50) NOT NULL,
KeyValue VARCHAR(100) NOT NULL,
LastUpdTS DATETIME,
CONSTRAINT PKUserPreference PRIMARY KEY CLUSTERED(UserPreferenceID ASC),
CONSTRAINT FKUserPreferenceUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName),
CONSTRAINT FKUserPreferenceCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

-- SQL Script File 28-Jan-2013 --

--Begin Update on 07-Mar-2013
ALTER TABLE UserPreference ALTER COLUMN KeyValue VARCHAR(300)  NOT NULL
GO
--End Update on 07-Mar-2013
