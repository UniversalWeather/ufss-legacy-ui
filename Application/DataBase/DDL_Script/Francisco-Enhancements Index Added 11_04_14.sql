------------------------------------------- UE-15
-- used in SP spFlightPak_Preflight_GetAllCrewDetails
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightLeg_POLegID_IDX' AND object_id = OBJECT_ID('PostflightLeg'))
    BEGIN
        DROP INDEX [PostflightLeg_POLegID_IDX] ON [dbo].[PostflightLeg]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightLeg_POLegID_IDX] ON [dbo].[PostflightLeg]
(
 [POLegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-15
-- used in SP spFlightPak_Preflight_GetAllCrewDetails
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightMain_POLogID_IDX' AND object_id = OBJECT_ID('PostflightMain'))
    BEGIN
        DROP INDEX [PostflightMain_POLogID_IDX] ON [dbo].[PostflightMain]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightMain_POLogID_IDX] ON [dbo].[PostflightMain]
(
 [POLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-15
-- used in SP spFlightPak_Preflight_GetAllCrewDetails
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightMain_FleetID_IDX' AND object_id = OBJECT_ID('PostflightMain'))
    BEGIN
        DROP INDEX [PostflightMain_FleetID_IDX] ON [dbo].[PostflightMain]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightMain_FleetID_IDX] ON [dbo].[PostflightMain]
(
 [FleetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-27
-- used in SP spFlightPak_Preflight_GetAllCrewDetails
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Company_CustomerID_IDX' AND object_id = OBJECT_ID('Company'))
    BEGIN
        DROP INDEX [Company_CustomerID_IDX] ON [dbo].[Company]
    END
GO

CREATE NONCLUSTERED INDEX [Company_CustomerID_IDX] ON [dbo].[Company]
(
 [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


