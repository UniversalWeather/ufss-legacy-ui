------------------------------------------- UE-50 ticket number
-- used in SP CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'TSANoFly_CustomerID_IDX' AND object_id = OBJECT_ID('TSANoFly'))
BEGIN
	DROP INDEX [TSANoFly_CustomerID_IDX] ON [dbo].[TSANoFly]
END
GO
CREATE NONCLUSTERED INDEX [TSANoFly_CustomerID_IDX] ON [dbo].[TSANoFly]
(
	[CustomerID] ASC
)
INCLUDE ([FirstName],[MiddleName],[LastName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-50 ticket number
-- used in SP CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'TSASelect_CustomerID_IDX' AND object_id = OBJECT_ID('TSASelect'))
BEGIN
	DROP INDEX [TSASelect_CustomerID_IDX] ON [dbo].[TSASelect]
END
GO
CREATE NONCLUSTERED INDEX [TSASelect_CustomerID_IDX] ON [dbo].[TSASelect]
(
	[CustomerID] ASC
)
INCLUDE ([FirstName],[MiddleName],[LastName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


------------------------------------------- UE-50 ticket number
-- used in SP CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'TSAClear_CustomerID_IDX' AND object_id = OBJECT_ID('TSAClear'))
BEGIN
	DROP INDEX [TSAClear_CustomerID_IDX] ON [dbo].[TSAClear]
END
GO
CREATE NONCLUSTERED INDEX [TSAClear_CustomerID_IDX] ON [dbo].[TSAClear]
(
	[CustomerID] ASC
)
INCLUDE ([FirstName],[MiddleName],[LastName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
