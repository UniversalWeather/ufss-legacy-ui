------------------------------------------- UE-28 ticket number
-- used in SP UserName is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserPassword_UserName_IDX' AND object_id = OBJECT_ID('UserPassword'))
BEGIN
DROP INDEX [UserPassword_UserName_IDX] ON [dbo].[UserPassword]
END
GO
CREATE NONCLUSTERED INDEX [UserPassword_UserName_IDX] ON [dbo].[UserPassword]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------- UE-28 ticket number
-- used in SP CustomerID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserMaster_CustomerID_IDX' AND object_id = OBJECT_ID('UserMaster'))
BEGIN
DROP INDEX [UserMaster_CustomerID_IDX] ON [dbo].[UserMaster]
END
GO
CREATE NONCLUSTERED INDEX [UserMaster_CustomerID_IDX] ON [dbo].[UserMaster]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO