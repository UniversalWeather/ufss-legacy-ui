------------------------------------------- UE-23 ticket numer
-- used in SP SpCheckLoginForAllSessions- LoginStatus,LoginSessionUID,SessionID Recomended by SQL proscesor
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FPLoginSession_LoginStatusLoginSessionUIDSessionID_IDX' AND object_id = OBJECT_ID('FPLoginSession'))
BEGIN
	DROP INDEX [FPLoginSession_LoginStatusLoginSessionUIDSessionID_IDX] ON [dbo].[FPLoginSession]
END
GO
CREATE NONCLUSTERED INDEX [FPLoginSession_LoginStatusLoginSessionUIDSessionID_IDX] ON [dbo].[FPLoginSession]
(
	[LoginStatus] ASC,
	[LoginSessionUID] ASC,
	[SessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO