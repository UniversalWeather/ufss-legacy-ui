

--// Alter Table Column for Charter Quote
ALTER TABLE flightpaksequence ADD CQFileNumCurrentNo BIGINT
GO
ALTER TABLE flightpaksequence ADD EQFileNumCurrentNo BIGINT
GO

Update FlightPakSequence SET CQFileNumCurrentNo = 0, EQFileNumCurrentNo=0
GO