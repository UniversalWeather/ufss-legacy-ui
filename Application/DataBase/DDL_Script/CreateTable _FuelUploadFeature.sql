GO
/********************************* FuelFieldMaster **********************************/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKFuelFieldMasterUserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelFieldMaster]'))
ALTER TABLE [dbo].[FuelFieldMaster] DROP CONSTRAINT [FKFuelFieldMasterUserMaster]
GO



/****** Object:  Table [dbo].[FuelFieldMaster]    Script Date: 01/01/2015 14:28:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelFieldMaster]') AND type in (N'U'))
DROP TABLE [dbo].[FuelFieldMaster]
GO



/****** Object:  Table [dbo].[FuelFieldMaster]    Script Date: 01/01/2015 14:28:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuelFieldMaster](
	[FuelFieldID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PK_FuelFieldMaster] PRIMARY KEY CLUSTERED 
(
	[FuelFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FuelFieldMaster]  WITH CHECK ADD  CONSTRAINT [FKFuelFieldMasterUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FuelFieldMaster] CHECK CONSTRAINT [FKFuelFieldMasterUserMaster]
GO

/********************************* FuelSavedFileFormat  ******************************************/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelSavedFileFormat_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FK_FuelSavedFileFormat_Customer]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelSavedFileFormat_FuelVendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FK_FuelSavedFileFormat_FuelVendor]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKFuelSavedFileFormatUserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FKFuelSavedFileFormatUserMaster]
GO


/****** Object:  Table [dbo].[FuelSavedFileFormat]    Script Date: 01/01/2015 14:30:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]') AND type in (N'U'))
DROP TABLE [dbo].[FuelSavedFileFormat]
GO


/****** Object:  Table [dbo].[FuelSavedFileFormat]    Script Date: 01/01/2015 14:30:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuelSavedFileFormat](
	[FuelSavedFileFormatID] [int] IDENTITY(1,1) NOT NULL,
	[FormatTitle] [varchar](50) NULL,
	[CustomerID] [bigint] NULL,
	[VendorID] [bigint] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PK_FuelSavedFileFormat] PRIMARY KEY CLUSTERED 
(
	[FuelSavedFileFormatID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FuelSavedFileFormat]  WITH CHECK ADD  CONSTRAINT [FK_FuelSavedFileFormat_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[FuelSavedFileFormat] CHECK CONSTRAINT [FK_FuelSavedFileFormat_Customer]
GO

ALTER TABLE [dbo].[FuelSavedFileFormat]  WITH CHECK ADD  CONSTRAINT [FK_FuelSavedFileFormat_FuelVendor] FOREIGN KEY([VendorID])
REFERENCES [dbo].[FuelVendor] ([FuelVendorID])
GO

ALTER TABLE [dbo].[FuelSavedFileFormat] CHECK CONSTRAINT [FK_FuelSavedFileFormat_FuelVendor]
GO

ALTER TABLE [dbo].[FuelSavedFileFormat]  WITH CHECK ADD  CONSTRAINT [FKFuelSavedFileFormatUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FuelSavedFileFormat] CHECK CONSTRAINT [FKFuelSavedFileFormatUserMaster]
GO

/******************************************* FuelFieldSynonym  *******************************************************************/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelFieldSynonym_FuelFieldMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelFieldSynonym]'))
ALTER TABLE [dbo].[FuelFieldSynonym] DROP CONSTRAINT [FK_FuelFieldSynonym_FuelFieldMaster]
GO



/****** Object:  Table [dbo].[FuelFieldSynonym]    Script Date: 01/01/2015 14:29:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelFieldSynonym]') AND type in (N'U'))
DROP TABLE [dbo].[FuelFieldSynonym]
GO



/****** Object:  Table [dbo].[FuelFieldSynonym]    Script Date: 01/01/2015 14:29:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuelFieldSynonym](
	[SynonymID] [int] IDENTITY(1,1) NOT NULL,
	[SynonymTitle] [varchar](100) NULL,
	[FuelFieldID] [int] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PK_FuelFieldSynonym] PRIMARY KEY CLUSTERED 
(
	[SynonymID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FuelFieldSynonym]  WITH CHECK ADD  CONSTRAINT [FK_FuelFieldSynonym_FuelFieldMaster] FOREIGN KEY([FuelFieldID])
REFERENCES [dbo].[FuelFieldMaster] ([FuelFieldID])
GO

ALTER TABLE [dbo].[FuelFieldSynonym] CHECK CONSTRAINT [FK_FuelFieldSynonym_FuelFieldMaster]
GO

/****************************************  FuelCsvFileFields *****************************************/


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelCsvFileFields_FuelFieldMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]'))
ALTER TABLE [dbo].[FuelCsvFileFields] DROP CONSTRAINT [FK_FuelCsvFileFields_FuelFieldMaster]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelCsvFileFields_FuelSavedFileFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]'))
ALTER TABLE [dbo].[FuelCsvFileFields] DROP CONSTRAINT [FK_FuelCsvFileFields_FuelSavedFileFormat]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKFuelCsvFileFieldsUserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]'))
ALTER TABLE [dbo].[FuelCsvFileFields] DROP CONSTRAINT [FKFuelCsvFileFieldsUserMaster]
GO



/****** Object:  Table [dbo].[FuelCsvFileFields]    Script Date: 01/01/2015 14:26:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]') AND type in (N'U'))
DROP TABLE [dbo].[FuelCsvFileFields]
GO


/****** Object:  Table [dbo].[FuelCsvFileFields]    Script Date: 01/01/2015 14:27:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuelCsvFileFields](
	[FuelCsvFileFieldsID] [int] IDENTITY(1,1) NOT NULL,
	[FuelFieldID] [int] NULL,
	[FuelFileFormatMasterID] [int] NULL,
	[FieldTitle] [varchar](100) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PK_FuelCsvFileFields] PRIMARY KEY CLUSTERED 
(
	[FuelCsvFileFieldsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FuelCsvFileFields]  WITH CHECK ADD  CONSTRAINT [FK_FuelCsvFileFields_FuelFieldMaster] FOREIGN KEY([FuelFieldID])
REFERENCES [dbo].[FuelFieldMaster] ([FuelFieldID])
GO

ALTER TABLE [dbo].[FuelCsvFileFields] CHECK CONSTRAINT [FK_FuelCsvFileFields_FuelFieldMaster]
GO

ALTER TABLE [dbo].[FuelCsvFileFields]  WITH CHECK ADD  CONSTRAINT [FK_FuelCsvFileFields_FuelSavedFileFormat] FOREIGN KEY([FuelFileFormatMasterID])
REFERENCES [dbo].[FuelSavedFileFormat] ([FuelSavedFileFormatID])
GO

ALTER TABLE [dbo].[FuelCsvFileFields] CHECK CONSTRAINT [FK_FuelCsvFileFields_FuelSavedFileFormat]
GO

ALTER TABLE [dbo].[FuelCsvFileFields]  WITH CHECK ADD  CONSTRAINT [FKFuelCsvFileFieldsUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FuelCsvFileFields] CHECK CONSTRAINT [FKFuelCsvFileFieldsUserMaster]
GO




