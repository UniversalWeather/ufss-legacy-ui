------------------------------------------- UE-21
--Script for index "FPLoginSession_LoginStartTime_IDX" 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FPLoginSession_LoginStartTime_IDX' AND  object_id = OBJECT_ID('FPLoginSession'))
BEGIN
	DROP INDEX [FPLoginSession_LoginStartTime_IDX] ON [dbo].[FPLoginSession]
END
GO
CREATE NONCLUSTERED INDEX [FPLoginSession_LoginStartTime_IDX] ON [dbo].[FPLoginSession]
(
	[LoginStartTime] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------- UE-21
--Script for index "UserGroupPermission_UserGroupID_IDX"
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupPermission_UserGroupID_IDX' AND object_id = OBJECT_ID('UserGroupPermission'))
BEGIN
	DROP INDEX [UserGroupPermission_UserGroupID_IDX] ON [dbo].[UserGroupPermission]
END
GO
CREATE NONCLUSTERED INDEX [UserGroupPermission_UserGroupID_IDX] ON [dbo].[UserGroupPermission]
(
	[UserGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------- UE-21
--Script for index "UserGroupMapping_UserGroupID_IDX"
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupMapping_UserGroupID_IDX' AND object_id = OBJECT_ID('UserGroupMapping'))
BEGIN
	DROP INDEX [UserGroupMapping_UserGroupID_IDX] ON [dbo].[UserGroupMapping]
END
GO
CREATE NONCLUSTERED INDEX [UserGroupMapping_UserGroupID_IDX] ON [dbo].[UserGroupMapping]
(
	[UserGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-21 ticket numer
-- used in SP spGetIdentity-UserGroupPermission- UserGroupID
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupPermission_UserGroupID_IDX' AND object_id = OBJECT_ID('UserGroupPermission'))
BEGIN
DROP INDEX [UserGroupPermission_UserGroupID_IDX] ON [dbo].[UserGroupPermission]
END
GO
CREATE NONCLUSTERED INDEX [UserGroupPermission_UserGroupID_IDX] ON [dbo].[UserGroupPermission]
(
[UserGroupID] Asc
)
INCLUDE ([UMPermissionID],[CanView],[CanAdd],[CanEdit],[CanDelete])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------- UE-21 ticket numer
-- used in SP spGetIdentity-UserGroupMapping- UserName
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UserGroupMapping_UserName_IDX' AND object_id = OBJECT_ID('UserGroupMapping'))
BEGIN
DROP INDEX [UserGroupMapping_UserName_IDX] ON [dbo].[UserGroupMapping]
END
GO
CREATE NONCLUSTERED INDEX [UserGroupMapping_UserName_IDX] ON [dbo].[UserGroupMapping]
(
[UserName] Asc
)
INCLUDE ([UserGroupID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO