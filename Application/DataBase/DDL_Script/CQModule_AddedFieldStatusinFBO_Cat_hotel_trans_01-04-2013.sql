Alter table PreflightFBOList 
Drop column IsRequired,IsNotRequired,IsInProgress,IsChange,IsCancelled
GO

Alter table PreflightFBOList Add [Status] varchar(20)
GO 

Alter table PreflightCateringDetail Add [Status] varchar(20)
GO 

Alter table PreflightTransportList Add [Status] varchar(20)
GO 

Alter table PreflightCrewHotelList Add [Status] varchar(20)
GO 

Alter table PreflightPassengerHotelList Add [Status] varchar(20)
GO 
