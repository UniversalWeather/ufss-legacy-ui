--ALTER TABLE CQException 
--ALTER COLUMN CQExceptionDescription 
-- VARCHAR(max)
-- go 
--ALTER TABLE PreflightCrewHotelList 
--ALTER COLUMN Comments VARCHAR(max)
--  go
--ALTER TABLE PreflightPassengerHotelList  
--ALTER COLUMN Comments VARCHAR(max)
--  go  
--ALTER TABLE PreflightFBOList   
--ALTER COLUMN Comments VARCHAR(max)
--  go
--ALTER TABLE PreflightTransportList    
--ALTER COLUMN Comments VARCHAR(max)
--  go 
--ALTER TABLE PreflightCateringDetail   
--ALTER COLUMN CateringComments VARCHAR(max)
--  go 
 

ALTER TABLE CQException ALTER COLUMN CQExceptionDescription VARCHAR(MAX)
GO
ALTER TABLE CQReportDefinition ALTER COLUMN ParameterValue VARCHAR(MAX)
GO
ALTER TABLE CQReportDetail ALTER COLUMN ParameterValue VARCHAR(MAX)
GO
ALTER TABLE CustomAddress ALTER COLUMN Remarks VARCHAR(MAX)
GO
--ALTER TABLE PreflightCrewHotelList ALTER COLUMN ConfirmationStatus VARCHAR(MAX)
--GO
ALTER TABLE PreflightCrewHotelList ALTER COLUMN Comments VARCHAR(MAX)
GO
--ALTER TABLE PreflightPassengerHotelList ALTER COLUMN ConfirmationStatus VARCHAR(MAX)
--GO
ALTER TABLE PreflightPassengerHotelList ALTER COLUMN Comments VARCHAR(MAX)
GO
--ALTER TABLE PreflightFBOList ALTER COLUMN ConfirmationStatus VARCHAR(MAX)
--GO
ALTER TABLE PreflightFBOList ALTER COLUMN Comments VARCHAR(MAX)
GO
--ALTER TABLE PreflightTransportList ALTER COLUMN ConfirmationStatus VARCHAR(MAX)
--GO
ALTER TABLE PreflightTransportList ALTER COLUMN Comments VARCHAR(MAX)
GO
ALTER TABLE PreflightCateringList ALTER COLUMN ConfirmationStatus VARCHAR(MAX)
GO
ALTER TABLE PreflightCateringList ALTER COLUMN Comments VARCHAR(MAX)
GO
ALTER TABLE PreflightCateringDetail ALTER COLUMN CateringComments VARCHAR(MAX)
GO 
ALTER TABLE TripSheetReportDefinition ALTER COLUMN ParameterValue VARCHAR(MAX)
GO
ALTER TABLE TripSheetReportDetail ALTER COLUMN ParameterValue VARCHAR(MAX)
GO
 