------------------------------------------- UE-107
-- used in SP spFlightPak_GetAllCrewPassport  FK [CountryID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerPassport_CountryID_IDX' AND object_id = OBJECT_ID('CrewPassengerPassport'))
    BEGIN
        DROP INDEX [CrewPassengerPassport_CountryID_IDX] ON [dbo].[CrewPassengerPassport]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerPassport_CountryID_IDX] ON [dbo].[CrewPassengerPassport]
(
 [CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-107
-- used in SP spFlightPak_GetAllCrewPassport  FK [CrewID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerPassport_CrewID_IDX' AND object_id = OBJECT_ID('CrewPassengerPassport'))
    BEGIN
        DROP INDEX [CrewPassengerPassport_CrewID_IDX] ON [dbo].[CrewPassengerPassport]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerPassport_CrewID_IDX] ON [dbo].[CrewPassengerPassport]
(
 [CrewID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-107
-- used in SP spFlightPak_GetAllCrewPassport FK [PassengerRequestorID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerPassport_PassengerRequestorID_IDX' AND object_id = OBJECT_ID('CrewPassengerPassport'))
    BEGIN
        DROP INDEX [CrewPassengerPassport_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerPassport]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerPassport_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerPassport]
(
 [PassengerRequestorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-105
-- used in SP spFlightPak_Preflight_GetTripExceptionList FK [TripID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PreflightTripException_TripID_IDX' AND object_id = OBJECT_ID('PreflightTripException'))
    BEGIN
        DROP INDEX [PreflightTripException_TripID_IDX] ON [dbo].[PreflightTripException]
    END
GO

CREATE NONCLUSTERED INDEX [PreflightTripException_TripID_IDX] ON [dbo].[PreflightTripException]
(
 [TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [CustomerID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_CustomerID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_CustomerID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_CustomerID_IDX] ON [dbo].[CRMain]
(
 [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [FleetID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_FleetID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_FleetID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_FleetID_IDX] ON [dbo].[CRMain]
(
 [FleetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [TravelCoordinatorID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_TravelCoordinatorID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_TravelCoordinatorID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_TravelCoordinatorID_IDX] ON [dbo].[CRMain]
(
 [TravelCoordinatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [AircraftID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_AircraftID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_AircraftID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_AircraftID_IDX] ON [dbo].[CRMain]
(
 [AircraftID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [PassengerRequestorID]
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_PassengerRequestorID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_PassengerRequestorID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_PassengerRequestorID_IDX] ON [dbo].[CRMain]
(
 [PassengerRequestorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 



------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [HomebaseID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_HomebaseID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_HomebaseID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_HomebaseID_IDX] ON [dbo].[CRMain]
(
 [HomebaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-104
-- used in SP spFlightpak_CorporateRequest_GetAllCRMain FK [ClientID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CRMain_ClientID_IDX' AND object_id = OBJECT_ID('CRMain'))
    BEGIN
        DROP INDEX [CRMain_ClientID_IDX] ON [dbo].[CRMain]
    END
GO

CREATE NONCLUSTERED INDEX [CRMain_ClientID_IDX] ON [dbo].[CRMain]
(
 [ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-103
-- used in SP spGetAllFleet  recreate index [FleetISINACTIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FleetISINACTIDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [FleetISINACTIDX] ON [dbo].[Fleet]
    END
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Fleet_CustomerID_IsInactive_IDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [Fleet_CustomerID_IsInactive_IDX] ON [dbo].[Fleet]
    END
GO

CREATE NONCLUSTERED INDEX [Fleet_CustomerID_IsInactive_IDX] ON [dbo].[Fleet]
(
 [CustomerID] ASC,
 [IsInActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-103
-- used in SP spGetAllFleet  recreate index [FleetISDELIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FleetISDELIDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [FleetISDELIDX] ON [dbo].[Fleet]
    END
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Fleet_CustomerID_IsDeleted_IDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [Fleet_CustomerID_IsDeleted_IDX] ON [dbo].[Fleet]
    END
GO

CREATE NONCLUSTERED INDEX [Fleet_CustomerID_IsDeleted_IDX] ON [dbo].[Fleet]
(
 [CustomerID] ASC,
 [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-103
-- used in SP spGetAllFleet  droping unifecient bit index [FleetIsDeletedIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FleetIsDeletedIDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [FleetIsDeletedIDX] ON [dbo].[Fleet]
    END
GO

------------------------------------------- UE-103
-- used in SP spGetAllFleet  droping unifecient bit index [FleetIsInActiveIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FleetIsInActiveIDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [FleetIsInActiveIDX] ON [dbo].[Fleet]
    END
GO


------------------------------------------- UE-99
-- used in SP sp_fss_GetCrewDutyRules FK [CustomerID]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewDutyRules_CustomerID_IDX' AND object_id = OBJECT_ID('CrewDutyRules'))
    BEGIN
        DROP INDEX [CrewDutyRules_CustomerID_IDX] ON [dbo].[CrewDutyRules]
    END
GO

CREATE NONCLUSTERED INDEX [CrewDutyRules_CustomerID_IDX] ON [dbo].[CrewDutyRules]
(
 [CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-99
-- used in SP sp_fss_GetCrewDutyRules cover the search
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewDutyRules_5FKIndes_IDX' AND object_id = OBJECT_ID('CrewDutyRules'))
    BEGIN
        DROP INDEX [CrewDutyRules_5FKIndes_IDX] ON [dbo].[CrewDutyRules]
    END
GO

CREATE NONCLUSTERED INDEX [CrewDutyRules_5FKIndes_IDX] ON [dbo].[CrewDutyRules]
(
 [CrewDutyRulesID] ASC,
 [CustomerID] ASC,
 [CrewDutyRuleCD] ASC,
 [IsInActive] ASC,
 [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-99
-- used in SP sp_fss_GetCrewDutyRules  droping unefecient bit index [CrewDutyRulesIsInActiveIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewDutyRulesIsInActiveIDX' AND object_id = OBJECT_ID('CrewDutyRules'))
    BEGIN
        DROP INDEX [CrewDutyRulesIsInActiveIDX] ON [dbo].[CrewDutyRules]
    END
GO

------------------------------------------- UE-99
-- used in SP sp_fss_GetCrewDutyRules  droping unefecient bit index [CrewDutyRulesIsDeletedIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewDutyRulesIsDeletedIDX' AND object_id = OBJECT_ID('CrewDutyRules'))
    BEGIN
        DROP INDEX [CrewDutyRulesIsDeletedIDX] ON [dbo].[CrewDutyRules]
    END
GO


------------------------------------------- UE-100
-- used in SP sp_fss_GetAllFleet cover the order by
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Fleet_AircraftCD_IDX' AND object_id = OBJECT_ID('Fleet'))
    BEGIN
        DROP INDEX [Fleet_AircraftCD_IDX] ON [dbo].[Fleet]
    END
GO

CREATE NONCLUSTERED INDEX [Fleet_AircraftCD_IDX] ON [dbo].[Fleet]
(
 [AircraftCD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 



------------------------------------------- UE-87
-- used in SP sp_fss_GetAllCrew  droping unefecient bit index [CrewIsStatusIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewIsStatusIDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [CrewIsStatusIDX] ON [dbo].[Crew]
    END
GO


------------------------------------------- UE-87
-- used in SP sp_fss_GetAllCrew  droping unefecient bit index [CrewIsRotaryWingIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewIsRotaryWingIDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [CrewIsRotaryWingIDX] ON [dbo].[Crew]
    END
GO

------------------------------------------- UE-87
-- used in SP sp_fss_GetAllCrew  droping unefecient bit index [CrewIsFixedWingIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewIsFixedWingIDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [CrewIsFixedWingIDX] ON [dbo].[Crew]
    END
GO

------------------------------------------- UE-87
-- used in SP sp_fss_GetAllCrew  droping unefecient bit index [CrewIsDeletedIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewIsDeletedIDX' AND object_id = OBJECT_ID('Crew'))
    BEGIN
        DROP INDEX [CrewIsDeletedIDX] ON [dbo].[Crew]
    END
GO


------------------------------------------- UE-80
-- used in SP spGetAllPassengerAddInfoReqID cover the order by
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerAdditionalInfo_GetAllPassengerAddInfoReqID_IDX' AND object_id = OBJECT_ID('PassengerAdditionalInfo'))
    BEGIN
        DROP INDEX [PassengerAdditionalInfo_GetAllPassengerAddInfoReqID_IDX] ON [dbo].[PassengerAdditionalInfo]
    END
GO

CREATE NONCLUSTERED INDEX [PassengerAdditionalInfo_GetAllPassengerAddInfoReqID_IDX] ON [dbo].[PassengerAdditionalInfo]
(
 [CustomerId] ASC,
 [PassengerRequestorID] ASC,
 [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-111
-- used in SP spLock 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'FPLockLog_spLock_IDX' AND object_id = OBJECT_ID('FPLockLog'))
    BEGIN
        DROP INDEX [FPLockLog_spLock_IDX] ON [dbo].[FPLockLog]
    END
GO

CREATE NONCLUSTERED INDEX [FPLockLog_spLock_IDX] ON [dbo].[FPLockLog]
(
 [CustomerId] ASC,
 [EntityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-110
-- used in SP sp_fss_GetAircraft covering index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Aircraft_fss_GetAircraft_IDX' AND object_id = OBJECT_ID('Aircraft'))
    BEGIN
        DROP INDEX [Aircraft_fss_GetAircraft_IDX] ON [dbo].[Aircraft]
    END
GO

CREATE NONCLUSTERED INDEX [Aircraft_fss_GetAircraft_IDX] ON [dbo].[Aircraft]
(
 [AircraftID] ASC,
 [CustomerId] ASC,
 [AircraftCD] ASC,
 [IsInActive] ASC,
 [IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-110
-- used in SP sp_fss_GetAircraft  droping unefecient bit index [aircraftIsInActiveIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'aircraftIsInActiveIDX' AND object_id = OBJECT_ID('Aircraft'))
    BEGIN
        DROP INDEX [aircraftIsInActiveIDX] ON [dbo].[Aircraft]
    END
GO


------------------------------------------- UE-89
-- used in SP spFlightPak_Preflight_GetPreFlightAvailablePaxListByIDOrCD covering index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerVisa_PassengerRequestorID_IDX' AND object_id = OBJECT_ID('CrewPassengerVisa'))
    BEGIN
        DROP INDEX [CrewPassengerVisa_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerVisa]
    END
GO

CREATE NONCLUSTERED INDEX [CrewPassengerVisa_PassengerRequestorID_IDX] ON [dbo].[CrewPassengerVisa]
(
 [PassengerRequestorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-82
-- used in SP spFlightPak_Postflight_REPORTSEARCH covering search 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightMain_REPORTSEARCH_IDX' AND object_id = OBJECT_ID('PostflightMain'))
    BEGIN
        DROP INDEX [PostflightMain_REPORTSEARCH_IDX] ON [dbo].[PostflightMain]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightMain_REPORTSEARCH_IDX] ON [dbo].[PostflightMain]
(
 [CustomerID] ASC,
 [ClientID] ASC,
 [FleetID] ASC,
 [HomebaseID] ASC,
 [EstDepartureDT] ASC,
 [LogNum] ASC,
 [IsDeleted] ASC,
 [IsPersonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 


------------------------------------------- UE-75
-- used in SP spLogout FK index 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'UWAL_TripID_IDX' AND object_id = OBJECT_ID('UWAL'))
    BEGIN
        DROP INDEX [UWAL_TripID_IDX] ON [dbo].[UWAL]
    END
GO

CREATE NONCLUSTERED INDEX [UWAL_TripID_IDX] ON [dbo].[UWAL]
(
 [TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 



------------------------------------------- UE-72
-- used in SP sp_fss_GetAllPassenger covering search 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Passenger_GetAllPassenger_IDX' AND object_id = OBJECT_ID('Passenger'))
    BEGIN
        DROP INDEX [Passenger_GetAllPassenger_IDX] ON [dbo].[Passenger]
    END
GO

CREATE NONCLUSTERED INDEX [Passenger_GetAllPassenger_IDX] ON [dbo].[Passenger]
(
    [CustomerID] ASC,
    [ClientID] ASC,
    [CQCustomerID] ASC,
    [PassengerRequestorID] ASC,
    [PassengerRequestorCD] ASC,
    [IsActive] ASC,
    [IsRequestor] ASC,
    [isdeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

------------------------------------------- UE-72
-- used in SP sp_fss_GetAllPassenger  droping unefecient bit index [PassengerIsRequestorIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerIsRequestorIDX' AND object_id = OBJECT_ID('Passenger'))
    BEGIN
        DROP INDEX [PassengerIsRequestorIDX] ON [dbo].[Passenger]
    END
GO

------------------------------------------- UE-72
-- used in SP sp_fss_GetAllPassenger  droping unefecient bit index [PassengerIsDeletedIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerIsDeletedIDX' AND object_id = OBJECT_ID('Passenger'))
    BEGIN
        DROP INDEX [PassengerIsDeletedIDX] ON [dbo].[Passenger]
    END
GO
------------------------------------------- UE-72
-- used in SP sp_fss_GetAllPassenger  droping unefecient bit index [PassengerIsActiveIDX]
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerIsActiveIDX' AND object_id = OBJECT_ID('Passenger'))
    BEGIN
        DROP INDEX [PassengerIsActiveIDX] ON [dbo].[Passenger]
    END
GO


