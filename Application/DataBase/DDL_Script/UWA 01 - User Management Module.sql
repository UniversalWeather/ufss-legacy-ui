-- --------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: User Management          --
-- Created Date: 14-Jun-2012        --
-- Updated Date: 20-Nov-2012        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

CREATE TABLE Customer
(
CustomerID BIGINT,
CustomerUID UNIQUEIDENTIFIER,
CustomerName VARCHAR(50) NOT NULL,
PrimaryFirstName VARCHAR(60),
PrimaryMiddleName VARCHAR(60),
PrimaryLastName VARCHAR(60),
PrimaryEmailID VARCHAR(60),
SecondaryFirstName VARCHAR(60),
SecondaryMiddleName VARCHAR(60),
SecondaryLastName VARCHAR(60),
SecondaryEmailID VARCHAR(60),
IsAll BIT,
IsAPIS BIT,
IsATS BIT,
IsMapping BIT,
IsUVFuel BIT,
IsUVTripLink BIT,
UVTripLinkName VARCHAR(60),
UVTripLinkAccountNumber CHAR(32),
ApisID VARCHAR(60),
ApisPassword VARCHAR(60),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCustomer PRIMARY KEY CLUSTERED(CustomerID ASC),
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CustomersIDX ON Customer
(
CustomerUID ASC
)
GO

CREATE TABLE CustomerLicense
(
CustomerLicenseID BIGINT,
CustomerID BIGINT,
LicenseNumber VARCHAR(60),
LicenseType VARCHAR(60),
LicenseStartDate DATETIME,
UserCount INT,
AircraftCount INT,
MaintainenceExpiryDate DATETIME,
IsUWASupportAccess BIT,
IsActiveLicense BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCustomerLicense PRIMARY KEY CLUSTERED(CustomerLicenseID ASC),
CONSTRAINT FKCustomerLicenseCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CustomerLicenseIDX ON CustomerLicense
(
LicenseNumber ASC
)
GO

CREATE TABLE Module
(
ModuleID BIGINT,
ModuleName CHAR(30) NOT NULL,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKModule PRIMARY KEY CLUSTERED(ModuleID ASC)
)
GO

CREATE TABLE UMPermission
(
UMPermissionID BIGINT,
UMPermissionName CHAR(50) NOT NULL,
ModuleID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKUMPermission PRIMARY KEY CLUSTERED(UMPermissionID ASC),
CONSTRAINT FKUMPermissionsModule FOREIGN KEY(ModuleID) REFERENCES Module(ModuleID)
)
GO

CREATE TABLE CustomerModuleAccess
(
CustomerModuleAccessID BIGINT,
ModuleID BIGINT NOT NULL,
CustomerID BIGINT NOT NULL,
CanAccess BIT,
ExpiryDate DATETIME2,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCustomerModuleAccess PRIMARY KEY CLUSTERED(CustomerModuleAccessID ASC),
CONSTRAINT FKCustomerModuleAccessModules FOREIGN KEY(ModuleID) REFERENCES Module(ModuleID),
CONSTRAINT FKCustomerModuleAccessCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CustomerModuleAccessIDX ON CustomerModuleAccess
(
CustomerID ASC, ModuleID ASC
)
GO

CREATE TABLE UserGroup
(
UserGroupID BIGINT,
UserGroupCD CHAR(5) NOT NULL,
UserGroupDescription CHAR(50) NOT NULL,
CustomerID BIGINT NOT NULL,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKUserGroup PRIMARY KEY CLUSTERED(UserGroupID ASC),
CONSTRAINT FKUserGroupCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX UserGroupIDX ON UserGroup
(
CustomerID ASC, UserGroupCD ASC
)
GO

CREATE TABLE UserGroupPermission
(
UserGroupPermissionID BIGINT,
CustomerID BIGINT,
UserGroupID BIGINT,
UMPermissionID BIGINT,
CanView BIT,
CanAdd BIT,
CanEdit BIT,
CanDelete BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKUserGroupPermission PRIMARY KEY CLUSTERED(UserGroupPermissionID ASC),
CONSTRAINT FKUserGroupPermissionCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKUserGroupPermissionUserGroup FOREIGN KEY(UserGroupID) REFERENCES UserGroup(UserGroupID),
CONSTRAINT FKUserGroupPermissionUMPermission FOREIGN KEY(UMPermissionID) REFERENCES UMPermission(UMPermissionID)
)
GO

CREATE TABLE Client
(
ClientID BIGINT,
ClientCD CHAR(5) NOT NULL,
ClientDescription VARCHAR(60),
CustomerID BIGINT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKClient PRIMARY KEY CLUSTERED(ClientID ASC),
CONSTRAINT FKClientCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX ClientIDX ON Client
(
CustomerID ASC, ClientCD ASC
)
GO

CREATE TABLE Company
(
HomebaseID BIGINT,
HomebaseCD CHAR(4) NOT NULL,
CustomerID BIGINT,
BaseDescription VARCHAR(30),	
ActiveDT DATE,
CompanyName CHAR(40),
IsDisplayNoteFlag BIT,
RefreshTM NUMERIC(4),
IsReportCrewDuty BIT,
LogFixed INT,		
LogRotary INT,
IsZeroSuppressActivityAircftRpt BIT,
IsZeroSuppressActivityCrewRpt BIT,
IsZeroSuppressActivityDeptRpt BIT,
IsZeroSuppressActivityPassengerRpt BIT,
IsAutomaticDispatchNum BIT,
IsAutomaticCommentTrips BIT,
IsConflictCheck BIT,
WindReliability INT,
GroundTM NUMERIC(5,2),
IsTimeStampRpt BIT,
ApplicationMessage TEXT,
CustomRptMessage VARCHAR(80),
RptTabOutbdInstLab1 VARCHAR(10),
RptTabOutbdInstLab2 VARCHAR(10),
RptTabOutbdInstLab3 VARCHAR(10),
RptTabOutbdInstLab4 VARCHAR(10),
RptTabOutbdInstLab5 VARCHAR(10),
RptTabOutbdInstLab6 VARCHAR(10),
RptTabOutbdInstLab7 VARCHAR(10),
ClientID BIGINT,
FedAviationRegNum CHAR(3),
DutyBasis NUMERIC(1),
AircraftBasis NUMERIC(1),
IsTripsheetView BIT,
-- CrewDuty CHAR(4),
CrewDutyID BIGINT,
TripsheetDTWarning NUMERIC(3),
-- DefaultFlightCatCD CHAR(4),
DefaultFlightCatID BIGINT,
-- DefaultChecklistGroup CHAR(3),
DefaultCheckListGroupID BIGINT,
FuelPurchase NUMERIC(1),
FuelBurnKiloLBSGallon NUMERIC(1),
ApplicationDateFormat VARCHAR(25),
AccountFormat VARCHAR(32),
CorpAccountFormat VARCHAR(64),
FederalTax VARCHAR(32),
StateTax VARCHAR(32),
SaleTax VARCHAR(32),
IsTextAutoTab BIT,
-- Department CHAR(8),
DepartmentID BIGINT,
FlightCatagoryPassenger CHAR(4),
FlightCatagoryPassengerNum CHAR(4),
CQFederalTax NUMERIC(5,2), -- NEW Column
FederalACCT CHAR(32),
RuralTax NUMERIC(5,2),
RuralAccountNum VARCHAR(32),
IsAppliedTax NUMERIC(1),
MainFee CHAR(4),
ChtQouteDOMSegCHG NUMERIC(17,2),
ChtQouteDOMSegCHGACCT CHAR(32),
ChtQouteIntlSegCHG NUMERIC(17,2),
ChtQouteIntlSegCHGACCT VARCHAR(32),
ChtQouteHeaderDetailRpt TEXT,
ChtQouteFooterDetailRpt TEXT,
ChtQuoteCompany TEXT,
ChtQuoteCompanyNameINV TEXT,
ChtQuoteHeaderINV TEXT,
ChtQuoteFooterINV TEXT,
IsChecklistWarning BIT,
CrewLogCustomLBLShort1 VARCHAR(10),
CrewLogCustomLBLShort2 VARCHAR(10),
CrewLogCustomLBLLong1 VARCHAR(25),
CrewLogCustomLBLLong2 VARCHAR(25),
IsLogsheetWarning BIT,
CurrencySymbol CHAR(3),
RptSize NUMERIC(1),
AugmentCrewPercentage NUMERIC(5,1),
IsDeactivateHomeBaseFilter BIT,
TripMGRDefaultStatus CHAR(1),
SearchGridTM NUMERIC(4,2),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
QuotePageOffsettingInvoice NUMERIC(2),
IsInvoiceDTsystemDT BIT,
IsQuoteDetailPrint BIT,
isQuotePrintFees BIT,
IsQuotePrintSum BIT,
IsQuotePrintSubtotal BIT,
IsCityDescription BIT,
IsAirportDescription BIT,
IsQuoteICAODescription BIT,
IsQuotePrepaidMinimUsageFeeAmt BIT,
QuoteMinimumUseFeeDepositAmt VARCHAR(60),
IsQuoteFlightChargePrepaid BIT,
DepostFlightCharge VARCHAR(60),
IsQuotePrepaidSegementFee BIT,
QuoteSegmentFeeDeposit VARCHAR(60),
IsQuoteAdditionalCrewPrepaid BIT,
QuoteAdditonCrewDeposit VARCHAR(60),
IsQuoteStdCrewRonPrepaid BIT,
QuoteStdCrewRONDeposit VARCHAR(60),
IsQuoteAdditonalCrewRON BIT,
QuoteAdditionCrewRON VARCHAR(60),
IsQuoteFlightLandingFeePrint BIT,
QuoteLandingFeeDefault VARCHAR(60),
IsQuoteWaitingChargePrepaid BIT,
QuoteWaitingCharge VARCHAR(60),
IsQuotePrintAdditionalFee BIT,
QuoteAdditionalFeeDefault VARCHAR(60),
IsQuoteDispcountAmtPrint BIT,
DiscountAmountDefault VARCHAR(60),
IsQuoteSubtotalPrint BIT,
QuoteSubtotal CHAR(60),
IsQuoteTaxesPrint BIT,
QuoteTaxesDefault CHAR(60),
IsQuoteInvoicePrint BIT,
DefaultInvoice CHAR(60),
IsInvoiceRptLegNum BIT,
IsInvoiceRptDetailDT BIT,
IsInvoiceRptFromDescription BIT,
IsQuoteDetailRptToDescription BIT,
IsQuoteDetailInvoiceAmt BIT,
IsQuoteDetailFlightHours BIT,
IsQuoteDetailMiles BIT,
IsQuoteDetailPassengerCnt BIT,
IsQuoteFeelColumnDescription BIT,
IsQuoteFeelColumnInvoiceAmt BIT,
IsQuoteDispatch BIT,
IsQuoteInformation BIT,
IsCompanyName BIT,
QuotePageOff NUMERIC(2),
IsChangeQueueDetail BIT,
IsQuoteChangeQueueFees BIT,
IsQuoteChangeQueueSum BIT,
IsQuoteChangeQueueSubtotal BIT,
IsChangeQueueCity BIT,
IsChangeQueueAirport BIT,
IsQuoteChangeQueueICAO BIT,
IsQuotePrepaidMinimumUsage2FeeAdj BIT,
QuoteMinimumUse2FeeDepositAdj VARCHAR(60),
IsQuotePrepaidMin2UsageFeeAdj BIT,
IsQuoteFlight2ChargePrepaid BIT,
Deposit2FlightCharge VARCHAR(60),
IsQuotePrepaidSegement2Fee BIT,
QuoteSegment2FeeDeposit VARCHAR(60),
IsQuoteAdditional2CrewPrepaid BIT,
QuoteAddition2CrewDeposit VARCHAR(60),
IsQuoteUnpaidAdditional2Crew BIT,
IsQuoteStdCrew2RONPrepaid BIT,
QouteStdCrew2RONDeposit CHAR(60),
IsQuoteUnpaidStd2CrewRON BIT,
IsQuoteAdditional2CrewRON BIT,
QuoteAddition2CrewRON CHAR(60),
IsQuoteUpaidAdditionl2CrewRON BIT,
IsQuoteFlight2LandingFeePrint BIT,
uote2LandingFeeDefault CHAR(60),
IsQuoteWaiting2ChargePrepaid BIT,
QuoteWaiting2Charge CHAR(60),
IsQuoteUpaid2WaitingChg BIT,
IsQuoteAdditional2FeePrint BIT,
Additional2FeeDefault CHAR(60),
IsQuoteDiscount2AmountPrepaid BIT,
Discount2AmountDeposit CHAR(60),
IsQuoteUnpaid2DiscountAmt BIT,
IsQuoteSubtotal2Print BIT,
Quote2Subtotal CHAR(60),
IsQuoteTaxes2Print BIT,
Quote2TaxesDefault CHAR(60),
IsQuoteInvoice2Print BIT,
QuoteInvoice2Default CHAR(60),
IsRptQuoteDetailLegNum BIT,
IsRptQuoteDetailDepartureDT BIT,
IsRptQuoteDetailFromDescription BIT,
IsRptQuoteDetailToDescription BIT,
IsRptQuoteDetailQuotedAmt BIT,
IsRptQuoteDetailFlightHours BIT,
IsRptQuoteDetailMiles BIT,
IsRptQuoteDetailPassengerCnt BIT,
IsRptQuoteFeeDescription BIT,
IsRptQuoteFeeQuoteAmt BIT,
IsQuoteInformation2 BIT,
IsCompanyName2 BIT,
QuoteDescription BIT,
IsQuoteSales BIT,
IsQuoteTailNum BIT,
IsQuoteTypeCD BIT,
QuoteTitle VARCHAR(60),
IsCorpReqActivateAlert BIT,
CorpReqTMEntryFormat NUMERIC(1),
IsQuoteUnpaidFlight2Chg BIT,
IsQuoteUnpaidFlightChg BIT,
IsQuote2PrepayPrint BIT,
QuotePrepay2Default VARCHAR(60),
IsQuotePrepayPrint BIT,
QuotePrepayDefault CHAR(60),
IsQuoteRemaining2AmtPrint BIT,
QuoteRemaining2AmountDefault CHAR(60),
IsQuoteRemainingAmtPrint BIT,
QuoteRemainingAmountDefault CHAR(60),
IsQuoteDeactivateAutoUpdateRates BIT,
CorpReqWeeklyCalTimer NUMERIC(6),
CorpReqExcChgWithinHours NUMERIC(6,2),
CorpReqExcChgWithinWeekend BIT,
FleetCalendar5DayTimer NUMERIC(6),
Crew5DayTimer NUMERIC(6),
IsAutomaticCalcLegTimes BIT,
IsCorpReqAllowLogUpdCateringInfo BIT,
IsCorpReqAllowLogUpdCrewInfo BIT,
IsCorpReqAllowLogUpdFBOInfo BIT,
IsCorpReqAllowLogUpdPaxInfo BIT,
CorpReqMinimumRunwayLength NUMERIC(5),
CorpReqStartTM CHAR(5),
CorpReqChgQueueTimer NUMERIC(5),
IsCorpReqActivateApproval BIT,
IsCorpReqOverrideSubmittoChgQueue BIT,
IsQuoteTaxDiscount BIT,
ScheduleServiceTimeFormat NUMERIC(1),
SSHRFormat NUMERIC(1),
ScheduleServiceListCTDetail NUMERIC(3),
DayMonthFormat NUMERIC(1),
AcceptDTTM DATETIME,
HTMLHeader TEXT,
HTMLFooter TEXT,
HTMLBackgroun TEXT,
ScheduledServicesSecurity NUMERIC(1),
-- ScheduleServiceWaitListCD CHAR(2),
ScheduleServiceWaitListID BIGINT,
IsTransWaitListPassengerfrmPreflightPostflight BIT,
WebSecurityType NUMERIC(1),
RowsOnPageCnt NUMERIC(3),
WebPageBackgroundColor TEXT,
HeardTextRpt TEXT,
FooterTextRpt TEXT,
HomebasePhoneNUM CHAR(25),
MenuBackground TEXT,
MenuHeader TEXT,
MenuFooter TEXT,
SSBACKGRND TEXT,
SSHEADER TEXT,
SSFOOTER TEXT,
WebBackground TEXT,
WebHeader TEXT,
WebFooter TEXT,
QuoteIDTYPE NUMERIC(1),
IsQuoteEdit BIT,
IsAutoRON BIT,
IsAutoRollover BIT,
IsQuoteWarningMessage BIT,
SegmentFeeAlaska NUMERIC(17,2),
SegementFeeHawaii NUMERIC(17,2),
SeqmentAircraftAlaska VARCHAR(32),
SegementAircraftHawaii VARCHAR(32),
IsInActive BIT,
-- Added Company2 Table Information --
TimeDisplayTenMin NUMERIC(1),
MinuteHoursRON NUMERIC(2),
IsChecklistAssign BIT,
SpecificationShort3 CHAR(10),
SpecificationShort4 CHAR(10),
SpecificationLong3 VARCHAR(25),
SpecificationLong4 VARCHAR(25),
Specdec3 INT,
Specdec4 INT,
IsLogoUpload BIT,
DayLanding NUMERIC(3),
DayLandingMinimum NUMERIC(3),
NightLanding NUMERIC(3),
NightllandingMinimum NUMERIC(3),
Approach NUMERIC(3),
ApproachMinimum NUMERIC(3),
Instrument NUMERIC(3),
InstrumentMinimum NUMERIC(3),
TakeoffDay NUMERIC(3),
TakeoffDayMinimum NUMERIC(3),
TakeoffNight NUMERIC(3),
TakeofNightMinimum NUMERIC(3),
Day7 NUMERIC(3),
DayMaximum7 NUMERIC(8,3),
Day30 NUMERIC(3),
DayMaximum30 NUMERIC(8,3),
CalendarMonthMaximum NUMERIC(8,3),
Day90 NUMERIC(3),
DayMaximum90 NUMERIC(8,3),
CalendarQTRMaximum NUMERIC(8,3),
Month6 NUMERIC(3),
MonthMaximum6 NUMERIC(8,3),
Previous2QTRMaximum NUMERIC(8,3),
Month12 NUMERIC(3),
MonthMaximum12 NUMERIC(8,3),
CalendarYearMaximum NUMERIC(8,3),
Day365 NUMERIC(3),
DayMaximum365 NUMERIC(8,3),
RestDays NUMERIC(3),
RestDaysMinimum NUMERIC(2),
RestCalendarQTRMinimum NUMERIC(2),
IsSLeg BIT,
IsVLeg BIT,
IsSConflict BIT,
IsVConflict BIT,
IsSChecklist BIT,
IsVChecklist BIT,
IsSCrewCurrency BIT,
IsVCrewCurrency BIT,
IsSPassenger BIT,
IsVPassenger BIT,
Tsense VARCHAR(250),
-- LastUpdUID CHAR(10),
-- LastUpdTS DATETIME,
QuoteLogo TEXT,
QuoteLogoPosition INT,
Ilogo TEXT,
IlogoPOS INT,
IsBlockSeats BIT,
FlightPurpose CHAR(2),
GroundTMIntl NUMERIC(5,2),
IsDepartAuthDuringReqSelection BIT,
IsNonLogTripSheetIncludeCrewHIST BIT,
TripsheetTabColor TEXT,
TripsheetSearchBack INT,
GeneralAviationDesk VARCHAR(25),
IsAutoValidateTripManager BIT,
HoursMinutesCONV NUMERIC(1),
TripSheetLogoPosition TEXT,
TripS INT,
IsQuoteFirstPage BIT,
IFirstPG BIT,
FiscalYRStart NUMERIC(2),
FiscalYREnd NUMERIC(2),
LayoverHrsSIFL INT,
IsAutoCreateCrewAvailInfo BIT,
IsAutoPassenger BIT,
PassengerOFullName INT,
PassengerOMiddle INT,
PassengerOLast INT,
PassengerOLastFirst INT,
PassengerOLastMiddle INT,
PassengerOLastLast INT,
IsAutoCrew BIT,
CrewOFullName INT,
CrewOMiddle INT,
CrewOLast INT,
CrewOLastFirst INT,
CrewOLastMiddle INT,
CrewOLastLast INT,
TripsheetRptWriteGroup CHAR(8),
IsReqOnly BIT,
BaseFax VARCHAR(25),
IsCanPass BIT,
CanPassNum VARCHAR(25),
ElapseTMRounding NUMERIC(1),
IsAutoDispat BIT,
PrivateTrip TEXT,
PasswordValidDays NUMERIC(2),
PasswordHistoryNum NUMERIC(2),
IsAutoRevisionNum BIT,
IsAutoPopulated BIT,
TaxiTime CHAR(5),
AutoClimbTime CHAR(5),
CQMessageCD NUMERIC(3), -- Rename Column -- TBD
IVMessageCD NUMERIC(3), -- New Column -- TBD
StdCrewRON VARCHAR(20),
AdditionalCrewCD VARCHAR(20),
AdditionalCrewRON VARCHAR(20),
WaitTM VARCHAR(20),
LandingFee VARCHAR(20),
BackupDT DATE,
IsOutlook BIT,
IsCharterQuoteFee3 BIT,
IsCharterQuoteFee4 BIT,
IsCharterQuote5 BIT,
IsCOLFee3 BIT,
IsCOLFee4 BIT,
IsCOLFee5 BIT,
IsANotes BIT,
IsCNotes BIT, -- Rename Column
IsPNotes BIT, -- New Column
CQCrewDuty CHAR(4), -- Rename Column
IsOpenHistory BIT,
ReportWriteLogo TEXT,
ReportWriteLogoPosition INT,
IsEndDutyClient BIT,
IsBlackberrySupport BIT,
IsSSL BIT,
ServerName VARCHAR(100),
ServerReport NUMERIC(10),
UserID CHAR(30),
UserPassword VARCHAR(10),
UserEmail VARCHAR(100),
ServerRPF NUMERIC(1),
IsAllBase BIT,
IsSIFLCalMessage BIT,
Searchack INT,
SearchBack INT,
IsCrewOlap BIT,
ImageCnt NUMERIC(1),
ImagePosition NUMERIC(1),
IsMaxCrew BIT,
IsTailInsuranceDue BIT,
IsCrewQaulifiedFAR BIT,
IsTailBase BIT,
IsEnableTSA BIT,
IsScheduleDTTM BIT,
IsAPISSupport BIT,
APISUrl VARCHAR(250),
LoginUrl VARCHAR(250),
IsPassengerOlap BIT,
XMLPath VARCHAR(100),
IsAutoFillAF BIT,
AircraftBlockFlight NUMERIC(1),
UserNameCBP VARCHAR(15),
UserPasswordCBP VARCHAR(15),
Domain VARCHAR(50),
IsAPISAlert BIT,
IsSTMPBlock BIT,
IsEnableTSAPX BIT,
CONSTRAINT PKHomebase PRIMARY KEY CLUSTERED(HomebaseID ASC),
CONSTRAINT FKHomebaseCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKHomebaseClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
)
GO

CREATE TABLE UserMaster
(
UserName CHAR(30),
CustomerID BIGINT,
HomebaseID BIGINT,
ClientID BIGINT,
FirstName VARCHAR(60) NOT NULL,
MiddleName VARCHAR(60) NOT NULL,
LastName VARCHAR(60) NOT NULL,
HomeBase CHAR(4),
TravelCoordID BIGINT,
IsActive BIT,
IsUserLock BIT,
IsTripPrivacy BIT,
EmailID VARCHAR(100),
PhoneNum VARCHAR(100),
PassengerRequestorID BIGINT,
UVTripLinkID CHAR(10),
UVTripLinkPassword VARCHAR(60),
IsPrimaryContact BIT,
IsSecondaryContact BIT,
IsSystemAdmin BIT,
IsDispatcher BIT,
IsOverrideCorp BIT,
IsCorpRequestApproval BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKUserMaster PRIMARY KEY CLUSTERED(UserName ASC),
CONSTRAINT FKUserMasterCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKUserMasterCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX UserMasterIDX ON UserMaster
(
CustomerID ASC, HomebaseID ASC, UserName ASC
)
GO

CREATE TABLE UserGroupMapping
(
UserGroupMappingID BIGINT,
CustomerID BIGINT,
UserGroupID BIGINT,
UserName CHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKUserGroupMapping PRIMARY KEY CLUSTERED(UserGroupMappingID ASC),
CONSTRAINT FKUserGroupMappingCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKUserGroupMappingUserGroup FOREIGN KEY(UserGroupID) REFERENCES UserGroup(UserGroupID),
CONSTRAINT FKUserGroupMappingUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE ClientCodeMapping
(
ClientCodeMappingID BIGINT,
CustomerID BIGINT,
ClientID BIGINT,
UserName CHAR(30),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKClientCodeMapping PRIMARY KEY CLUSTERED(ClientCodeMappingID ASC),
CONSTRAINT FKClientCodeMappingCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKClientCodeMappingClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID),
CONSTRAINT FKClientCodeMappingUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName)
)
GO

CREATE TABLE CustomAddress
(
CustomAddressID BIGINT,
CustomerID BIGINT,
UserName CHAR(30),
Name VARCHAR(40),
Address1 VARCHAR(100),
Address2 VARCHAR(100),
Address3 VARCHAR(100),
CustomCity VARCHAR(30),
CustomState VARCHAR(30),
PostalZipCD CHAR(15),
CountryID BIGINT,
MetropolitanArea CHAR(3),
Contact VARCHAR(25),
PhoneNum VARCHAR(25),
FaxNum VARCHAR(25),
ContactEmailID VARCHAR(100),
Website VARCHAR(100),
Symbol VARCHAR(12),
LatitudeDegree NUMERIC(3,0),
LatitudeMinutes NUMERIC(5,1),
LatitudeNorthSouth CHAR(1),
LongitudeDegrees NUMERIC(3,0),
LongitudeMinutes NUMERIC(5,1),
LongitudeEastWest CHAR(1),
AirportID BIGINT,
HomebaseID BIGINT,
Remarks VARCHAR(100),
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
IsDeleted BIT,
CONSTRAINT PKCustomAddress PRIMARY KEY CLUSTERED(CustomAddressID ASC),
CONSTRAINT FKCustomAddressCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
CONSTRAINT FKCustomAddressUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName),
CONSTRAINT FKCustomAddressCompany FOREIGN KEY(HomebaseID) REFERENCES Company(HomebaseID)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX CustomAddressIDX ON CustomAddress
(
CustomerID ASC, UserName ASC
)
GO

CREATE TABLE FilghtpakSequence
(
FlightpakSequenceID BIGINT,
CustomerID BIGINT,
MasterModuleCurrentNo BIGINT,
PreflightCurrentNo BIGINT,
PostflightCurrentNo BIGINT,
PostflightSlipCurrentNo BIGINT,
UtilityCurrentNo BIGINT,
CharterQuoteCurrentNo BIGINT,
CorpReqCurrentNo BIGINT,
CONSTRAINT PKFilghtpakSequence PRIMARY KEY CLUSTERED(FlightpakSequenceID ASC),
CONSTRAINT FKFilghtpakSequenceCustomer FOREIGN KEY(CustomerID) REFERENCES Customer(CustomerID),
)
GO

CREATE UNIQUE NONCLUSTERED INDEX FilghtpakSequenceIDX ON FilghtpakSequence
(
CustomerID  ASC
)
GO

-- Database Change Log 03-Jul-2012 --

exec sp_rename 'FilghtpakSequence','FlightpakSequence'

-- Database Change Log 04-Jul-2012 --

ALTER TABLE Client ADD IsDeleted BIT
GO

ALTER TABLE UserMaster ADD CONSTRAINT FKUserMasterClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
GO

-- Database Change Log 11-Jul-2012 --

ALTER TABLE FlightpakSequence ADD FuelVendorCurrentNo BIGINT
GO

-- Database Change Log 13-Jul-2012 --

ALTER TABLE CustomAddress ADD CustomAddressCD CHAR(5)
GO

-- Database Change Log 19-Jul-2012 --

ALTER TABLE Company ALTER COLUMN UserNameCBP NVARCHAR(55)
GO

ALTER TABLE Company ALTER COLUMN UserPasswordCBP NVARCHAR(55)
GO

ALTER TABLE UMPermission ADD UMPermissionRole CHAR(50)
GO

DROP INDEX CustomerLicenseIDX ON CustomerLicense
GO

ALTER TABLE CustomerLicense ALTER COLUMN LicenseNumber NVARCHAR(470)
GO

-- Database Change Log 23-Jul-2012 --

CREATE TABLE UserPassword
(
UserPasswordID BIGINT,
UserName CHAR(30),
FPPWD NVARCHAR(500),
ActiveStartDT DATETIME,
ActiveEndDT DATETIME,
PWDStatus BIT,
LastUpdUID CHAR(30),
LastUpdTS DATETIME,
CONSTRAINT PKUserPassword PRIMARY KEY CLUSTERED(UserPasswordID ASC),
CONSTRAINT FKUserPasswordUNUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName),
CONSTRAINT FKUserPasswordUserMaster FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 27-Aug-2012 --

DROP INDEX CustomAddressIDX ON CustomAddress
GO

-- Database Change Log 03-Sep-2012 --

ALTER TABLE UserMaster ADD UserMasterID BIGINT
GO

CREATE UNIQUE NONCLUSTERED INDEX UserMasterIDX1 ON UserMaster
(
UserMasterID ASC
)
GO

-- Database Change Log 07-Sep-2012 --

ALTER TABLE CustomAddress ADD BusinessPhone VARCHAR(25)
ALTER TABLE CustomAddress ADD CellPhoneNum VARCHAR(25)
ALTER TABLE CustomAddress ADD HomeFax VARCHAR(25)
ALTER TABLE CustomAddress ADD CellPhoneNum2 VARCHAR(25)
ALTER TABLE CustomAddress ADD OtherPhone VARCHAR(25)
ALTER TABLE CustomAddress ADD BusinessEmail VARCHAR(250)
ALTER TABLE CustomAddress ADD PersonalEmail VARCHAR(250)
ALTER TABLE CustomAddress ADD OtherEmail VARCHAR(250)
ALTER TABLE CustomAddress ALTER COLUMN Website VARCHAR(200)
ALTER TABLE CustomAddress ADD Addr3 VARCHAR(40)

ALTER TABLE UMPermission ADD FormName CHAR(50)
GO

ALTER TABLE Company ADD IsDeleted BIT DEFAULT 0
GO

--ALTER TABLE UserMaster DROP CONSTRAINT FKUserMasterClient
--GO

--ALTER TABLE UserMaster DROP COLUMN ClientID
--GO

-- Database Change Log 10-Sep-2012 --

ALTER TABLE CustomAddress DROP COLUMN Addr3

-- Database Change Log 11-Sep-2012 --

ALTER TABLE Company ADD IsKilometer BIT
GO

ALTER TABLE Company ADD IsMiles BIT
GO

CREATE TABLE UserSecurityHint
(
UserSecurityHintID BIGINT,
UserName VARCHAR(30),
SecurityQuestion NVARCHAR(1000),
ActiveStartDT DATETIME,
ActiveEndDT DATETIME,
PWDStatus BIT,
LastUpdUID VARCHAR(30),
LastUpdTS DATETIME,
SecurityAnswer NVARCHAR(1000),
CONSTRAINT PKUserSecurityHint PRIMARY KEY CLUSTERED(UserSecurityHintID ASC),
CONSTRAINT FKUserSecurityHintUserMaster FOREIGN KEY(UserName) REFERENCES UserMaster(UserName),
CONSTRAINT FKUserSecurityHintUserMaster1 FOREIGN KEY(LastUpdUID) REFERENCES UserMaster(UserName)
)
GO

-- Database Change Log 12-Sep-2012 --

ALTER TABLE UMPermission ALTER COLUMN FormName VARCHAR(50)
GO

ALTER TABLE UserMaster ALTER COLUMN FirstName VARCHAR(60) NULL
GO

ALTER TABLE UserMaster ALTER COLUMN MiddleName VARCHAR(60) NULL
GO

ALTER TABLE UserMaster ALTER COLUMN LastName VARCHAR(60) NULL
GO

-- Database Change Log 13-Sep-2012 --

ALTER TABLE Company ALTER COLUMN IsDeleted BIT NOT NULL
GO

-- Database Change Log 25-Sep-2012 --

ALTER TABLE Company ADD CONSTRAINT FKCompanyFlightCatagory FOREIGN KEY(DefaultFlightCatID) REFERENCES FlightCatagory(FlightCategoryID)
GO

ALTER TABLE Company ADD CONSTRAINT FKCompanyTripManagerCheckListGroup FOREIGN KEY(DefaultCheckListGroupID) REFERENCES TripManagerCheckListGroup(CheckGroupID)
GO

-- Database Change Log 12-Oct-2012 --

ALTER TABLE FPLoginSession ADD LastActivityDate DATETIME
GO

ALTER TABLE UserMaster ADD ForceChangePassword BIT NOT NULL DEFAULT 1
GO

ALTER TABLE UserMaster ADD ForceChangeSecurityHint BIT NOT NULL DEFAULT 1
GO

-- Database Change Log 02-Nov-2012 --

ALTER TABLE FlightpakSequence ADD TSAUploadCurrentNo BIGINT
GO

-- Database Change Log 20-Nov-2012 --

ALTER TABLE UserMaster ADD InvalidLoginCount INT
GO

ALTER TABLE UserMaster ADD CONSTRAINT UniqueEmailID UNIQUE(EmailID)
GO

-- SQL Script File 20-Nov-2012 --