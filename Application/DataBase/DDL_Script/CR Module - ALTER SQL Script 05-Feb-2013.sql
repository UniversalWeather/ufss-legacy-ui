-----------------------------
-- UWA Flightpak Application
-----------------------------

-------------------------------------
-- Database Change Log Information --
-------------------------------------

--------------------------------------
-- Module: Corporate Request Module --
-- Created Date: 05-Feb-2013        --
-- Updated Date: 06-Feb-2013        --
-- Created By: PREM NATH R.K.       --
--------------------------------------

ALTER TABLE CRLeg ADD FBOID BIGINT
GO

ALTER TABLE CRLeg ADD CONSTRAINT FKCRLegFBO FOREIGN KEY(FBOID) REFERENCES FBO(FBOID)
GO

ALTER TABLE CRLeg DROP COLUMN FBOCD
GO

ALTER TABLE CRLeg ADD ClientID BIGINT
GO

ALTER TABLE CRLeg ADD CONSTRAINT FKCRLegClient FOREIGN KEY(ClientID) REFERENCES Client(ClientID)
GO

ALTER TABLE CRLeg DROP COLUMN Client
GO

-- SQL Script File 06-Feb-2013 --