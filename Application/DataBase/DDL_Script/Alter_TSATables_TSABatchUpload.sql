GO
ALTER TABLE dbo.TSANoFly ALTER COLUMN FirstName VARCHAR(100)
ALTER TABLE dbo.TSANoFly ALTER COLUMN LastName  VARCHAR(100)
GO
ALTER TABLE dbo.TSAClear ALTER COLUMN FirstName VARCHAR(100)
ALTER TABLE dbo.TSAClear ALTER COLUMN LastName  VARCHAR(100)
Go
ALTER TABLE dbo.TSASelect ALTER COLUMN FirstName VARCHAR(100)
ALTER TABLE dbo.TSASelect ALTER COLUMN LastName  VARCHAR(100)
GO


