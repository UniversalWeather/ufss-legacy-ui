GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightCrew_LogID_IDX' AND object_id = OBJECT_ID('PostflightCrew'))
BEGIN
	DROP INDEX [PostflightCrew_LogID_IDX] ON [dbo].[PostflightCrew]
END
GO
CREATE NONCLUSTERED INDEX [PostflightCrew_LogID_IDX] ON [dbo].[PostflightCrew]
(
	[POLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightCrew_LegID_IDX' AND object_id = OBJECT_ID('PostflightCrew'))
BEGIN
	DROP INDEX [PostflightCrew_LegID_IDX] ON [dbo].[PostflightCrew]
END
GO
/****** Object:  Index [PostflightCrew_LegID_IDX]    Script Date: 2015-05-09 12:27:21 PM ******/
CREATE NONCLUSTERED INDEX [PostflightCrew_LegID_IDX] ON [dbo].[PostflightCrew]
(
	[POLegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewCheckListDetail_CustomerID_IsInActive_IsDeleted_IDX' AND object_id = OBJECT_ID('CrewCheckListDetail'))
BEGIN
	DROP INDEX [CrewCheckListDetail_CustomerID_IsInActive_IsDeleted_IDX] ON [dbo].[CrewCheckListDetail]
END
GO
/****** Object:  Index [CrewCheckListDetail_CustomerID_IsInActive_IsDeleted_IDX]    Script Date: 2015-05-09 12:29:42 PM ******/
CREATE NONCLUSTERED INDEX [CrewCheckListDetail_CustomerID_IsInActive_IsDeleted_IDX] ON [dbo].[CrewCheckListDetail]
(
	[CustomerID] ASC,
	[IsInActive] ASC,
	[IsDeleted] ASC
)
INCLUDE ( 	[CheckListCD],
	[CrewID],
	[DueDT],
	[AlertDT],
	[GraceDT],
	[IsOneTimeEvent],
	[IsCompleted]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


