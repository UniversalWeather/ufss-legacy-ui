ALTER TABLE PreflightHotelList ADD [IsUniversalToArrange] BIT NULL
GO
ALTER TABLE PreflightTransportList ADD [IsDepartureUniversalToArrange] BIT NULL
GO
ALTER TABLE PreflightTransportList ADD [IsArrivalUniversalToArrange] BIT NULL
GO
ALTER TABLE FlightPakFuel ADD [IsRequestUVAirArrange] BIT NULL
GO
ALTER TABLE FlightPakFuel ADD [UVAirArrangeFuelText] VARCHAR(250) NULL
GO