/****** Object:  Index [CateringIDX]    Script Date: 1/8/2015 11:14:33 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Catering]') AND name = N'CateringIDX')
DROP INDEX [CateringIDX] ON [dbo].[Catering]
GO

/****** Object:  Index [CateringIDX]    Script Date: 1/8/2015 11:14:33 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Catering]') AND name = N'CateringIDX')
CREATE UNIQUE NONCLUSTERED INDEX [CateringIDX] ON [dbo].[Catering]
(
	[AirportID] ASC,
	[CateringCD] ASC,
	[CustomerID] ASC,
	[IsDeleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO