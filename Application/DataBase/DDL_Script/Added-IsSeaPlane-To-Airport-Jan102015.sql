--UBF:2804
GO
IF NOT EXISTS (SELECT
                     COLUMN_NAME
               FROM
                     INFORMATION_SCHEMA.columns
               WHERE
                     TABLE_NAME = 'Airport'
                     AND COLUMN_NAME = 'IsSeaPlane')
BEGIN

ALTER TABLE AIRPORT ADD IsSeaPlane BIT NULL

END
GO