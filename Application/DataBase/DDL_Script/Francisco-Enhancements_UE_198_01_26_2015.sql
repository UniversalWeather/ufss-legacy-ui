------------------------------------------- UE-198 ticket numer
-- used in SP sp_fss_GetAllDelayType  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'DelayType_DelayTypeCD_IDX' AND object_id = OBJECT_ID('DelayType'))
    BEGIN
        DROP INDEX [DelayType_DelayTypeCD_IDX] ON [dbo].[DelayType]
    END
GO

CREATE NONCLUSTERED INDEX [DelayType_DelayTypeCD_IDX] ON [dbo].[DelayType]
(
    [DelayTypeCD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

