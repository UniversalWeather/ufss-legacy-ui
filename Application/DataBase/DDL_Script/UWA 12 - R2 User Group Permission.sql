
CREATE Type [dbo].[UserGroupPermissionType] AS TABLE(
	[UserGroupPermissionID] [bigint] NOT NULL,
	[CustomerID] [bigint] NULL,
	[UserGroupID] [bigint] NULL,
	[UMPermissionID] [bigint] NULL,
	[CanView] [bit] NULL,
	[CanAdd] [bit] NULL,
	[CanEdit] [bit] NULL,
	[CanDelete] [bit] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL)
	
