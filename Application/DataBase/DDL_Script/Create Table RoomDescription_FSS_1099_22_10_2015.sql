SET ANSI_NULLS ON
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomDescription]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoomDescription](
	[RoomDescriptionID] [bigint] IDENTITY(1,1) NOT NULL,
	[RoomDescCode] [varchar](100) NULL,
	[RoomDesc] [varchar](100) NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PK_RoomDescription] PRIMARY KEY CLUSTERED 
(
	[RoomDescriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO