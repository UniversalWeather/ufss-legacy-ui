    
Create PROCEDURE [dbo].[spUpdateCrewforCrewDisplay]        
(        
 @CrewID BIGINT,          
 @customerID BIGINT,
 @IsCrewDisplay BIT
)        
AS        
-- =============================================        
-- Author: Karthikeyan.S
-- Create date: 04/03/2014        
-- Description: Update Crew for Crew display
-- =============================================       
 BEGIN
 SET NOCOUNT OFF;        
 UPDATE [Crew] SET             
IsCrewDisplay = @IsCrewDisplay
  WHERE [CrewID] = @CrewID and CustomerID = @customerID     
  END; 