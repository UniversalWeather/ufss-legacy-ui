------------------------------------------- UE-209 ticket numer
-- used in SP spFlightPak_Postflight_GetOtherCrewDutyLog  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PostflightSimulatorLog_CustomerID_IDX' AND object_id = OBJECT_ID('PostflightSimulatorLog'))
    BEGIN
        DROP INDEX [PostflightSimulatorLog_CustomerID_IDX] ON [dbo].[PostflightSimulatorLog]
    END
GO

CREATE NONCLUSTERED INDEX [PostflightSimulatorLog_CustomerID_IDX] ON [dbo].[PostflightSimulatorLog]
(
    [CustomerID] ASC,
    [IsDeleted] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

