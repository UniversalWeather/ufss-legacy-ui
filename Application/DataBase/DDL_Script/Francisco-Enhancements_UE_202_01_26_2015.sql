------------------------------------------- UE-202 ticket numer
-- used in SP spFlightPak_GetCrewAircraftAssigned  - CrewID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX' AND object_id = OBJECT_ID('CrewAircraftAssigned'))
    BEGIN
        DROP INDEX [CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX] ON [dbo].[CrewAircraftAssigned]
    END
GO

CREATE NONCLUSTERED INDEX [CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX] ON [dbo].[CrewAircraftAssigned]
(
	[CrewID] ASC,
	[FleetID] ASC,
	[CustomerID] ASC
)
INCLUDE(IsDeleted, LastUpdUID, LastUpdTS , IsInActive)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


