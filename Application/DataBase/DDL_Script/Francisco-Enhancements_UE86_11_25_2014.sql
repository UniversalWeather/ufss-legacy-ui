------------------------------------------- UE-86 ticket number
-- used in SP spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD - AircraftTypeID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewRating_AircraftTypeID_IDX' AND object_id = OBJECT_ID('CrewRating'))
BEGIN
	DROP INDEX [CrewRating_AircraftTypeID_IDX] ON [dbo].[CrewRating]
END
GO
CREATE NONCLUSTERED INDEX [CrewRating_AircraftTypeID_IDX] ON [dbo].[CrewRating]
(
	[AircraftTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-86 ticket number
-- used in SP spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD - CrewId,CustomerID and FleetID is FK
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX' AND object_id = OBJECT_ID('CrewAircraftAssigned'))
BEGIN
	DROP INDEX [CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX] ON [dbo].[CrewAircraftAssigned]
END
GO
CREATE NONCLUSTERED INDEX [CrewAircraftAssigned_CrewId_FleetId_CustomerId_IDX] ON [dbo].[CrewAircraftAssigned]
(
	[CrewId] ASC,
	[FleetID] ASC,
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-86 ticket number
-- used in SP spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD - CrewId and CustomerID is FK and CountryID is covering index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewPassengerVisa_CrewId_CustID_CountryID_IDX' AND object_id = OBJECT_ID('CrewPassengerVisa'))
BEGIN
	DROP INDEX [CrewPassengerVisa_CrewId_CustID_CountryID_IDX] ON [dbo].[CrewPassengerVisa]
END
GO
CREATE NONCLUSTERED INDEX [CrewPassengerVisa_CrewId_CustID_CountryID_IDX] ON [dbo].[CrewPassengerVisa]
(
	[CrewId] ASC,
	[CustomerID] ASC,
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------- UE-86 ticket number
-- used in SP spFlightPak_Preflight_GetAllCrewDetailsbyCrewIDorCrewCD - AlertDT and GraceDT is covering index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewCheckListDetail_AlertDT_GraceDT_IDX' AND object_id = OBJECT_ID('CrewCheckListDetail'))
BEGIN
	DROP INDEX [CrewCheckListDetail_AlertDT_GraceDT_IDX] ON [dbo].[CrewCheckListDetail]
END
GO
CREATE NONCLUSTERED INDEX [CrewCheckListDetail_AlertDT_GraceDT_IDX] ON [dbo].[CrewCheckListDetail]
(
	[AlertDT] ASC,
	[GraceDT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO