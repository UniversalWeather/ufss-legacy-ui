IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FPEmailTranLog]') AND type in (N'U'))
DROP TABLE [dbo].[FPEmailTranLog]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FPEmailTmpl]') AND type in (N'U'))
DROP TABLE [dbo].[FPEmailTmpl]


/****** Object:  Table [dbo].[FPEmailTmpl]    Script Date: 02/05/2013 12:20:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FPEmailTmpl](
	[EmailTmplID] [bigint] NOT NULL,
	[TmplName] [varchar](20) NOT NULL,
	[EmailBody] [nvarchar](max) NULL,
	[EmailSubject] [nvarchar](2000) NULL,
	[CustomerID] [bigint] NULL,
	[LastUpdUID] [varchar](30) NULL,
	[LastUpdTS] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PKFPEmailTmpl] PRIMARY KEY CLUSTERED 
(
	[EmailTmplID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FPEmailTmpl]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTmplCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[FPEmailTmpl] CHECK CONSTRAINT [FKFPEmailTmplCustomer]
GO

ALTER TABLE [dbo].[FPEmailTmpl]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTmplUserMaster] FOREIGN KEY([LastUpdUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FPEmailTmpl] CHECK CONSTRAINT [FKFPEmailTmplUserMaster]
GO

ALTER TABLE [dbo].[FPEmailTmpl] ADD  CONSTRAINT [FPEmailTmplIsDeletedIsDeleted]  DEFAULT ((0)) FOR [Isdeleted]
GO


/****** Object:  Table [dbo].[FPEmailTranLog]    Script Date: 02/05/2013 12:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FPEmailTranLog](
	[EmailTranID] [bigint] NOT NULL, -- Primary Key Id
	[EmailTmplID] [bigint] NULL, -- Email Template Id if it is using template. Else Null
	[EmailReqUID] [varchar](30) NULL, -- Email requestor UID
	[EmailType] [varchar] (1) NOT NULL, -- EMail Type. Possible values would be E(Email), CI(Calander Invites)
	[CustomerID] [bigint] NOT NULL, -- CustomerId
	[EmailReqTime] [datetime] NOT NULL, -- Email Requsted Time
	[EmailStatus] [varchar](18) NULL, -- Email Status. Possible values would be N(Not Done), I(In Progress), D(Done)
	[EmailSentTo] [varchar](100) NULL, -- Receiver's Email Id
	[LastUPDUID] [varchar](30) NULL, -- Audit Field
	[LastUpdTS] [datetime] NULL, -- Audit Field
	[IsDeleted] [bit] NOT NULL, -- Is Deleted
	[EmailBody] [nvarchar](max) NULL, -- Email Body
	[EmailSubject] [nvarchar](2000) NULL, -- EMail Subject
	[MeetingStartTime] [datetime] null, -- Meeting Start Time in case of CI. Else Null
	[MeetingEndTime] [datetime] null, -- Meeting End Time in case of CI. Else Null
	[MeetingLocation] [varchar] (2000) NULL, -- Meeting Location in case of CI. Else Null
	[IsBodyHTML] [bit] Default 0,
	[IsInActive] [bit] NULL,
 CONSTRAINT [PKFPEmailTranLog] PRIMARY KEY CLUSTERED 
(
	[EmailTranID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FPEmailTranLog]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTranLogCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[FPEmailTranLog] CHECK CONSTRAINT [FKFPEmailTranLogCustomer]
GO

ALTER TABLE [dbo].[FPEmailTranLog]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTranLogEUserMaster] FOREIGN KEY([EmailReqUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FPEmailTranLog] CHECK CONSTRAINT [FKFPEmailTranLogEUserMaster]
GO

ALTER TABLE [dbo].[FPEmailTranLog]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTranLogFPEmailTmpl] FOREIGN KEY([EmailTmplID])
REFERENCES [dbo].[FPEmailTmpl] ([EmailTmplID])
GO

ALTER TABLE [dbo].[FPEmailTranLog] CHECK CONSTRAINT [FKFPEmailTranLogFPEmailTmpl]
GO

ALTER TABLE [dbo].[FPEmailTranLog]  WITH CHECK ADD  CONSTRAINT [FKFPEmailTranLogUserMaster] FOREIGN KEY([LastUPDUID])
REFERENCES [dbo].[UserMaster] ([UserName])
GO

ALTER TABLE [dbo].[FPEmailTranLog] CHECK CONSTRAINT [FKFPEmailTranLogUserMaster]
GO

ALTER TABLE [dbo].[FPEmailTranLog] ADD  CONSTRAINT [FPEmailTranLogIsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


