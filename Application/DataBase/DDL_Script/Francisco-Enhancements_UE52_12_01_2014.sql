------------------------------------------- UE-52 ticket number
-- drop cduplicated index on column 
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'PassengerCustIDX' AND object_id = OBJECT_ID('Passenger'))
BEGIN
	DROP INDEX [PassengerCustIDX] ON [dbo].[Passenger]
END
GO

------------------------------------------- UE-52 ticket number
-- used in SP PassengerName is covering Index
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'Passenger_PassengerName_IDX' AND object_id = OBJECT_ID('Passenger'))
BEGIN
DROP INDEX [Passenger_PassengerName_IDX] ON [dbo].[Passenger]
END
GO
CREATE NONCLUSTERED INDEX [Passenger_PassengerName_IDX] ON [dbo].[Passenger]
(
	[PassengerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO