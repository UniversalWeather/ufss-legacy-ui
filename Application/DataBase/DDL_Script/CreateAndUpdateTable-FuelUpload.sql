USE FP_DEV_Dev2

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]') AND type in (N'U'))
BEGIN
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelCsvFileFields_FuelFileXsltParser]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]'))
	BEGIN
		ALTER TABLE [dbo].[FuelCsvFileFields] DROP CONSTRAINT [FK_FuelCsvFileFields_FuelFileXsltParser]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelFileXsltParser_UserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelCsvFileFields]'))
	BEGIN
		ALTER TABLE [dbo].[FuelCsvFileFields] DROP CONSTRAINT [FK_FuelFileXsltParser_UserMaster]
	END
	DROP TABLE [dbo].[FuelCsvFileFields]
END 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]') AND type in (N'U'))
BEGIN
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelSavedFileFormat_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
	BEGIN
		ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FK_FuelSavedFileFormat_Customer]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelSavedFileFormat_FuelVendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
	BEGIN
		ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FK_FuelSavedFileFormat_FuelVendor]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelSavedFileFormat_FuelFileXsltParser]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelSavedFileFormat]'))
	BEGIN
		ALTER TABLE [dbo].[FuelSavedFileFormat] DROP CONSTRAINT [FK_FuelSavedFileFormat_FuelFileXsltParser]
	END
   DROP TABLE [dbo].[FuelSavedFileFormat]
END

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoricalFuelPrice]') AND type in (N'U'))
BEGIN
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_HistoricalFuel_Airport]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoricalFuelPrice]'))
	BEGIN
		ALTER TABLE [dbo].[HistoricalFuelPrice] DROP CONSTRAINT [FK_HistoricalFuel_Airport]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_HistoricalFuel_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoricalFuelPrice]'))
	BEGIN
		ALTER TABLE [dbo].[HistoricalFuelPrice] DROP CONSTRAINT [FK_HistoricalFuel_Customer]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_HistoricalFuelID_FuelVendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoricalFuelPrice]'))
	BEGIN
		ALTER TABLE [dbo].[HistoricalFuelPrice] DROP CONSTRAINT [FK_HistoricalFuelID_FuelVendor]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[FK_HistoricalFuel_UserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoricalFuelPrice]'))
	BEGIN
		ALTER TABLE [dbo].[HistoricalFuelPrice] DROP CONSTRAINT [FK_HistoricalFuel_UserMaster]
	END
   DROP TABLE [dbo].[HistoricalFuelPrice]
END

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelFileXsltParser]') AND type in (N'U'))
BEGIN
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelFileXsltParser_UserMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelFileXsltParser]'))
	BEGIN
		ALTER TABLE [dbo].[FuelFileXsltParser] DROP CONSTRAINT [FK_FuelFileXsltParser_UserMaster]
	END
	IF EXISTS(SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[FK_FuelFileXsltParser_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[FuelFileXsltParser]'))
	BEGIN
		ALTER TABLE [dbo].[FuelFileXsltParser] DROP CONSTRAINT [FK_FuelFileXsltParser_Customer]
	END
   DROP TABLE [dbo].[FuelFileXsltParser]
END

CREATE TABLE [dbo].[FuelFileXsltParser](
	[FuelFileXsltParserID] [INT] IDENTITY(1,1) NOT NULL,
	[CustomerID] [BIGINT] NULL,
	[XsltFileName] [VARCHAR](255) NULL,
	[XsltFileDescription] [VARCHAR](500) NULL,
	[XsltFileContain] [VARCHAR](MAX) NULL,
	[LastUpdUID] [VARCHAR](30) NULL,
	[LastUpdTS] [DATETIME] NULL,
	[IsDeleted] [BIT] NULL,
	[IsInActive] [BIT] NULL,
	[CsvData] [VARCHAR](MAX) NULL,
	[XsltFunctionContain] [VARCHAR](MAX) NULL,
	CONSTRAINT [PK_FuelFileXsltParser] PRIMARY KEY ([FuelFileXsltParserID]),
	CONSTRAINT [FK_FuelFileXsltParser_UserMaster] FOREIGN KEY([LastUpdUID]) REFERENCES [dbo].[UserMaster] (UserName),
	CONSTRAINT [FK_FuelFileXsltParser_Customer] FOREIGN KEY([CustomerID]) REFERENCES [dbo].[Customer] (CustomerID)
)
CREATE TABLE [dbo].[FuelCsvFileFields](
	[FuelCsvFileFieldsID] [INT] IDENTITY(1,1) NOT NULL,
	[FuelFileXsltParserID] [INT] NULL,
	[FieldTitle] [VARCHAR](100) NULL,
	[LastUpdUID] [VARCHAR](30) NULL,
	[LastUpdTS] [DATETIME] NULL,
	[IsDeleted] [BIT] NOT NULL,
	[IsInActive] [BIT] NULL,
	CONSTRAINT [PK_FuelCsvFileFields] PRIMARY KEY ([FuelCsvFileFieldsID]),
	CONSTRAINT [FK_FuelCsvFileFields_FuelFileXsltParser] FOREIGN KEY([FuelFileXsltParserID]) REFERENCES [dbo].[FuelFileXsltParser] ([FuelFileXsltParserID]),
	CONSTRAINT [FK_FuelCsvFileFields_UserMaster] FOREIGN KEY([LastUpdUID]) REFERENCES [dbo].[UserMaster] (UserName)
);

CREATE TABLE [dbo].[FuelSavedFileFormat](
	[FuelSavedFileFormatID] [INT] IDENTITY(1,1) NOT NULL,
	[CustomerID] [BIGINT] NULL,
	[FuelVendorID] [BIGINT] NULL,
	[FuelFileXsltParserID] [INT] NULL,
	[LastUpdUID] [VARCHAR](30) NULL,
	[LastUpdTS] [DATETIME] NULL,
	[IsDeleted] [BIT] NOT NULL,
	[IsInActive] [BIT] NULL,
	CONSTRAINT [PK_FuelSavedFileFormatID] PRIMARY KEY ([FuelSavedFileFormatID]),
	CONSTRAINT [FK_FuelSavedFileFormat_Customer] FOREIGN KEY([CustomerID]) REFERENCES [dbo].[Customer] (CustomerID),
	CONSTRAINT [FK_FuelSavedFileFormat_FuelVendor] FOREIGN KEY([FuelVendorID]) REFERENCES [dbo].[FuelVendor] (FuelVendorID),
	CONSTRAINT [FK_FuelSavedFileFormat_FuelFileXsltParser] FOREIGN KEY([FuelFileXsltParserID]) REFERENCES [dbo].[FuelFileXsltParser] ([FuelFileXsltParserID])
)

CREATE TABLE [dbo].[HistoricalFuelPrice](
	[HistoricalFuelID] [BIGINT] IDENTITY(1,1) NOT NULL,
	[CustomerID] [BIGINT] NULL,
	[DepartureICAOID] [BIGINT] NULL,
	[FBOName] [VARCHAR](25) NULL,
	[GallonFrom] [NUMERIC](17,3) NULL,
	[GallonTO] [NUMERIC](17,3) NULL,
	[EffectiveDT] [DATE] NULL,
	[UnitPrice] [NUMERIC](15,3) NULL,
	[FuelVendorID] [BIGINT] NULL,
	[LastUpdUID] [VARCHAR](30) NULL,
	[LastUpdTS] [DATETIME] NULL,
	[IsActive] [BIT] NULL,
	CONSTRAINT [PK_HistoricalFuelID] PRIMARY KEY ([HistoricalFuelID]),
	CONSTRAINT [FK_HistoricalFuel_Airport] FOREIGN KEY([DepartureICAOID]) REFERENCES [dbo].[Airport] (AirportID),
	CONSTRAINT [FK_HistoricalFuel_Customer] FOREIGN KEY([CustomerID]) REFERENCES [dbo].[Customer] (CustomerID),
	CONSTRAINT [FK_HistoricalFuelID_FuelVendor] FOREIGN KEY([FuelVendorID]) REFERENCES [dbo].[FuelVendor] (FuelVendorID),
	CONSTRAINT [FK_HistoricalFuel_UserMaster] FOREIGN KEY([LastUpdUID]) REFERENCES [dbo].[UserMaster] (UserName)
)