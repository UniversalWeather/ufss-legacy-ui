
-- =============================================
-- 06-05-2013 - Added DepartPercentage column for BRM Item No. 18
-- =============================================

ALTER TABLE PostflightPassenger ADD DepartPercentage NUMERIC(7,2) NULL
GO

ALTER TABLE Department ADD DepartPercentage NUMERIC(7,2) NULL
GO