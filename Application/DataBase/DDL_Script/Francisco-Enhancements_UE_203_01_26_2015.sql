------------------------------------------- UE-203 ticket numer
-- used in SP spFlightPak_GetCrewCheckListDate  - CheckListCD
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewCheckListDetail_CheckListCD_IDX' AND object_id = OBJECT_ID('CrewCheckListDetail'))
    BEGIN
        DROP INDEX [CrewCheckListDetail_CheckListCD_IDX] ON [dbo].[CrewCheckListDetail]
    END
GO

CREATE NONCLUSTERED INDEX [CrewCheckListDetail_CheckListCD_IDX] ON [dbo].[CrewCheckListDetail]
(
    [CheckListCD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


------------------------------------------- UE-203 ticket numer
-- used in SP spFlightPak_GetCrewCheckListDate  - CustomerID
IF EXISTS(SELECT * FROM sys.indexes WHERE name = 'CrewCheckList_CustomerID_IDX' AND object_id = OBJECT_ID('CrewCheckList'))
    BEGIN
        DROP INDEX [CrewCheckList_CustomerID_IDX] ON [dbo].[CrewCheckList]
    END
GO

CREATE NONCLUSTERED INDEX [CrewCheckList_CustomerID_IDX] ON [dbo].[CrewCheckList]
(
    [CustomerID] ASC
)INCLUDE ( CrewCheckCD, isdeleted, CrewChecklistDescription)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
