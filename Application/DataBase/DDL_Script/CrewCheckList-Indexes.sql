IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_CrewCheckList_CrewCheckCD') 
    DROP INDEX IX_CrewCheckList_CrewCheckCD ON dbo.CrewCheckList;
GO
CREATE INDEX IX_CrewCheckList_CrewCheckCD ON dbo.CrewCheckList (CrewCheckCD);

IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_CrewCheckList_CustomerID') 
    DROP INDEX IX_CrewCheckList_CustomerID ON dbo.CrewCheckList;
GO
CREATE INDEX IX_CrewCheckList_CustomerID ON dbo.CrewCheckList (CustomerID);

IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_CrewCheckListDetail_CustomerID') 
    DROP INDEX IX_CrewCheckListDetail_CustomerID ON dbo.CrewCheckListDetail;
GO
CREATE INDEX IX_CrewCheckListDetail_CustomerID ON dbo.CrewCheckListDetail (CustomerID);

IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_CrewCheckListDetail_CrewID') 
    DROP INDEX IX_CrewCheckListDetail_CrewID ON dbo.CrewCheckListDetail;
GO
CREATE INDEX IX_CrewCheckListDetail_CrewID ON dbo.CrewCheckListDetail (CrewID);
