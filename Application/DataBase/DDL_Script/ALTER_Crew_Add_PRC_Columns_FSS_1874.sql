IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'INSANumber' AND Object_ID = Object_ID(N'Crew'))
BEGIN
	ALTER TABLE Crew ADD [INSANumber] [varchar](60) NULL
END
GO
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'Category' AND Object_ID = Object_ID(N'Crew'))
BEGIN
	ALTER TABLE Crew ADD [Category] [varchar](30) NULL
END
GO
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'GreenCardCountryID' AND Object_ID = Object_ID(N'Crew'))
BEGIN
	ALTER TABLE Crew ADD [GreenCardCountryID] [bigint] NULL
END
GO
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CardExpires' AND Object_ID = Object_ID(N'Crew'))
BEGIN
	ALTER TABLE Crew ADD [CardExpires] [date] NULL
END
GO
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ResidentSinceYear' AND Object_ID = Object_ID(N'Crew'))
BEGIN
	ALTER TABLE Crew ADD [ResidentSinceYear] [int] NULL
END
GO