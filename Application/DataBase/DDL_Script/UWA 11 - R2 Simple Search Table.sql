IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FPSearchParameters]') AND type in (N'U'))
DROP TABLE [dbo].[FPSearchParameters]

CREATE TABLE [dbo].[FPSearchParameters](
	[SearchId] [bigint] NOT NULL,
	[ModuleName] [varchar](30) NOT NULL,
	[TableName] [nvarchar](128) NOT NULL,
	[PermissionName] [varchar](60) NOT NULL,
	[PageVirtualPath] [nvarchar](2048) NOT NULL,
	[SysAdminAccessOnly] [bit] NOT NULL,
	[UserReadableTableName] [varchar](1000) NOT NULL,
	[StaticQuery] [varchar](max) NOT NULL,
	[IsCustomerIdRequired] bit NOT NULL,
 CONSTRAINT [PK_FPSearchParameters] PRIMARY KEY CLUSTERED 
(
	[SearchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
