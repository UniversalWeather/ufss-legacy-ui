--UBF:2961
GO
IF NOT EXISTS (SELECT
                     COLUMN_NAME
               FROM
                     INFORMATION_SCHEMA.columns
               WHERE
                     TABLE_NAME = 'Airport_Mig'
                     AND COLUMN_NAME = 'IsSeaPlane')
BEGIN

ALTER TABLE AIRPORT_MIG ADD IsSeaPlane BIT NULL

END
GO