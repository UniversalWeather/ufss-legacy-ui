IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FBO]') AND name = N'GetAllFBOByAirportID_IDX')
DROP INDEX [GetAllFBOByAirportID_IDX] ON [dbo].[FBO]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FBO]') AND name = N'GetAllFBOByAirportID_IDX')
CREATE NONCLUSTERED INDEX [GetAllFBOByAirportID_IDX] ON [dbo].[FBO]
(
	[CustomerID] ASC,
	[AirportID] ASC,
	[FBOCD] ASC,
	[IsDeleted] ASC,
	[IsChoice] ASC,
	[IsUWAAirPartner] ASC,
	[IsInActive] ASC,
	[FBOVendor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO