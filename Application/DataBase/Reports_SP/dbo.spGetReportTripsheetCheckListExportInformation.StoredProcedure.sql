IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportTripsheetCheckListExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportTripsheetCheckListExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportTripsheetCheckListExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportTripsheetCheckListExportInformation
-- Author: SUDHAKAR J
-- Create date: 18 Jun 2012
-- Description: Get Trip Manager Checklist export Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT
		[code] = TC.CheckListCD
		,[desc] =  TC.CheckListDescription
		,[chkgroup] = TCG.CheckGroupCD
	FROM 
		TripManagerCheckList TC INNER JOIN 
		TripManagerCheckListGroup TCG
			ON TC.CheckGroupID = TCG.CheckGroupID
	WHERE TC.IsDeleted = 0
		AND TC.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	ORDER BY TC.CheckListCD, TCG.CheckGroupCD
  -- EXEC spGetReportTripsheetCheckListExportInformation 'UC'


GO


