IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlyInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@GoToDate DATE
       ,@NoOfMonths INT=1
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsShowTrip BIT=1
	  ,@IsActivityOnly BIT=0
	  ,@IsFirstLastLeg BIT=0
	  ,@IsCrew	BIT=0
	  ,@IsShowTripStatus BIT=0
	  ,@IsDisplayRedEyeFlight BIT=0
	  ,@IsFlightNo BIT=0
	  ,@IsLegPurpose BIT=1
	  ,@IsSeatAvailable BIT=1
	  ,@IsPaxCount BIT=1
	  ,@IsRequestor BIT=1
	  ,@IsDepartment BIT=1
	  ,@IsAuthorization  BIT=1
	  ,@IsFlightCategory BIT=1
	  ,@IsCummulativeETE BIT=1
	  ,@IsETE BIT=1
	  ,@IsTotalETE BIT=1
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@StdMonthReport BIT=0--1-Std,0-Monthly
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlyInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
   
 SET NOCOUNT ON  
 
 DECLARE @StartDate DATETIME=DATEADD(mm,(YEAR(@GoToDate) - 1900)* 12 + MONTH(@GoToDate) - 1,1 - 1);
 DECLARE @EndDate   DATETIME=DATEADD(mm,(YEAR(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1900)* 12 + MONTH(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1,1 - 1) ;
        

 ;WITH
cteDates AS
(SELECT TOP (DATEDIFF(mm,@StartDate,@EndDate) + 1)
        MonthDate = DATEADD(mm,DATEDIFF(mm,0,@StartDate) 
                  + (ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) -1),0)
   FROM sys.all_columns ac1
  CROSS JOIN sys.all_columns ac2
)
 SELECT YearNo  = YEAR(MonthDate),
        MonthNo = MONTH(MonthDate),
		@GoToDate as GoToDate FROM cteDates ORDER BY YearNo,MonthNo;
 END
GO
