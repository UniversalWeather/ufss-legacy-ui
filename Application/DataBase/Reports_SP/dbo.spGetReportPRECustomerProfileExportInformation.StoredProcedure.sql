IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECustomerProfileExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECustomerProfileExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRECustomerProfileExportInformation]
    --@UserCD AS VARCHAR(30),
	--@Tripstatus NVARCHAR(10) ='',
	@UserHomebaseID AS VARCHAR(30),
	@UserCustomerID AS VARCHAR(30),
	@CQCustomerCD VARCHAR(100)='',
	@DATEFROM DATETIME, --MANDATORY  
    @DATETO DATETIME, --MANDATORY 
    @LOGFIXED CHAR(1),
    @IsTrip			      AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled          AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold              AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet         AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled       AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	--,@IsScheduledService  AS BIT = 1 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
AS

--EXEC spGetReportPRECustomerProfileExportInformation '','10099','','2013-01-01','2013-12-01','',0,0,0,0,0

DECLARE     @LOGFIXED2 INT
----------------
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		--IF  @IsScheduledService = 1
		--SET @TripStatus = @TripStatus + 'S,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		
	 IF @IsWorkSheet = 0 AND @IsTrip = 0 AND @IsUnFulFilled = 0 AND @IsCanceled = 0 AND @IsHold = 0
		SET @TripStatus = 'W,T,U,X,H'
		
    	
BEGIN

	SET NOCOUNT ON;

				
				
IF  @LOGFIXED = 1	--IF  @LOGFIXED2=1	
BEGIN

SELECT DISTINCT
    CC.CQCustomerID,
    CC.CQCustomerCD,
    CC.CQCustomerName,
    PM.EstDepartureDT,
    CF1.QuoteDT,
    CF1.FileNUM, 
    CQ1.QuoteTotal, 
    CQ1.LiveTrip, 
    PM.TripNUM,
    CONVERT(DATE,PL1.DepartureDTTMLocal) AS TRIPDATE,
    PM.AircraftID, 
    PM.TripDescription, 
    PM.TripStatus, 
    PM.FleetID,
    F.TailNum,
    AC.AircraftCD,
    PP.CountVal PASSENGERTOTAL, 
    PP.TRIPDAYS
FROM PreflightLeg PL1
    JOIN PreflightMain PM ON PM.TripID = PL1.TripID AND PM.IsDeleted = 0
    JOIN Fleet F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
    JOIN CQMain CQ1 ON PM.CustomerID = CQ1.CustomerID AND PM.TripID = CQ1.TripID
    LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
    LEFT JOIN CQFile CF1 ON CQ1.CQFileID = CF1.CQFileID AND CQ1.CustomerID = CF1.CustomerID AND CF1.CQCustomerID <> ''
    LEFT JOIN CQCustomer CC ON CF1.CQCustomerID = CC.CQCustomerID
    LEFT OUTER JOIN (SELECT PM.TripID,PM.TripNUM,COUNT(DISTINCT P1.PassengerRequestorID) CountVal,CAST(DATEDIFF(DD, MIN(PL.HomeDepartureDTTM),MAX(PL.HomeArrivalDTTM)+1)AS INT) AS TRIPDAYS  
                         FROM PreflightMain PM 
							   INNER JOIN PreflightLeg PL ON PL.TripID=PM.TripID AND PL.IsDeleted=  0
							   LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
							   LEFT OUTER JOIN Passenger P1 ON PPL.PassengerID=P1.PassengerRequestorID 
                           WHERE PM.CustomerID=@UserCustomerID AND PM.IsDeleted = 0
                         GROUP BY PM.TripID,PM.TripNUM)PP ON PP.TripID=PL1.TripID
 WHERE (PM.TripStatus  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@Tripstatus, ','))OR @Tripstatus='')
 AND PM.CustomerID = @UserCustomerID 
 AND PL1.IsDeleted = 0
 AND PL1.LegNUM=1
 AND CONVERT(DATE,PL1.HomeDepartureDTTM) BETWEEN CONVERT(DATE,@DATEFROM) And CONVERT(DATE,@DATETO)  --PL1.HomeDepartureDTTM
 AND (CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')

END
ELSE

BEGIN

SELECT DISTINCT
    CC.CQCustomerID,
    CC.CQCustomerCD,
    CF1.CQCustomerName, 
    PM.EstDepartureDT,
    CF1.QuoteDT, 
    CF1.FileNUM, 
    CQ1.QuoteTotal, 
    CQ1.LiveTrip, 
    PM.TripNUM,
    CONVERT(DATE,PL1.DepartureDTTMLocal) AS TRIPDATE,
    PM.AircraftID, 
    PM.TripDescription, 
    PM.TripStatus, 
    PM.FleetID,
    F.TailNum,
    F.AircraftCD,
    PP.CountVal PASSENGERTOTAL, 
    PP.TRIPDAYS
FROM PreflightLeg PL1
    JOIN PreflightMain PM ON PM.TripID = PL1.TripID AND PM.IsDeleted = 0
    JOIN Fleet F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
    JOIN CQMain CQ1 ON  PM.TripID = CQ1.TripID
    LEFT JOIN CQFile CF1 ON CQ1.CQFileID = CF1.CQFileID 
    LEFT JOIN CQCustomer CC ON CF1.CQCustomerID = CC.CQCustomerID
   LEFT OUTER JOIN (SELECT PM.TripID,PM.TripNUM,COUNT(DISTINCT P1.PassengerRequestorID) CountVal,CAST(DATEDIFF(DD, MIN(PL.DepartureGreenwichDTTM),MAX(PL.ArrivalGreenwichDTTM)+1)AS INT) AS TRIPDAYS  
                         FROM PreflightMain PM 
							   INNER JOIN PreflightLeg PL ON PL.TripID=PM.TripID AND PL.IsDeleted = 0
							   LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
							   LEFT OUTER JOIN Passenger P1 ON PPL.PassengerID=P1.PassengerRequestorID 
                           WHERE PM.CustomerID=@UserCustomerID AND PM.IsDeleted = 0
                         GROUP BY PM.TripID,PM.TripNUM)PP ON PP.TripID=PL1.TripID
 WHERE (PM.TripStatus  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@Tripstatus, ','))OR @Tripstatus='')
 AND PM.CustomerID = @UserCustomerID 
 AND PL1.IsDeleted = 0
 AND PL1.LegNUM=1
 AND CONVERT(DATE,PL1.DepartureGreenwichDTTM) BETWEEN CONVERT(DATE,@DATEFROM) And CONVERT(DATE,@DATETO) 
 AND (CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')

 END

END





GO


