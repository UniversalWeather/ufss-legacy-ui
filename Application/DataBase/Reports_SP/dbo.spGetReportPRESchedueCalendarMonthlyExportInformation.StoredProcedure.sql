IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlyExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyExportInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@GoToDate DATE
       ,@NoOfMonths INT=1
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsShowTrip BIT=1
	  ,@IsActivityOnly BIT=0
	  ,@IsFirstLastLeg BIT=0
	  ,@IsCrew	BIT=0
	  ,@IsShowTripStatus BIT=0
	  ,@IsDisplayRedEyeFlight BIT=0
	  ,@IsFlightNo BIT=0
	  ,@IsLegPurpose BIT=1
	  ,@IsSeatAvailable BIT=1
	  ,@IsPaxCount BIT=1
	  ,@IsRequestor BIT=1
	  ,@IsDepartment BIT=1
	  ,@IsAuthorization  BIT=1
	  ,@IsFlightCategory BIT=1
	  ,@IsCummulativeETE BIT=1
	  ,@Footer BIT=0 ---0 Skip Footer,1-Print Footer
	  ,@StdMonthReport BIT=0--1-Std,0-Monthly
	  ,@BlackWhiteClr BIT=1
	  ,@AircraftClr BIT=1
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)='' 
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlyExportInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--  EXEC spGetReportPRESchedueCalendarMonthlyExportInformation  'BRUCE_98','10098' ,'2013-07-29'  
-- ================================================================================  
  
           
            
          
SET NOCOUNT ON     
                             
 
 SET @StdMonthReport=1
 
 DECLARE @BeginDate DATETIME=DATEADD(mm,(YEAR(@GoToDate) - 1900)* 12 + MONTH(@GoToDate) - 1,1 - 1);
 
 DECLARE @StartDate DATETIME=DATEADD(mm,(YEAR(@GoToDate) - 1900)* 12 + MONTH(@GoToDate) - 1,1 - 1);
 DECLARE @EndDate   DATETIME=DATEADD(mm,(YEAR(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1900)* 12 + MONTH(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1,1 - 1) ;

SELECT @ECrew=CASE WHEN ISNULL(@EFleet,'')<>'' THEN '' ELSE @ECrew END 

SELECT @BeginDate=(CASE WHEN @StdMonthReport=1 THEN @BeginDate -(CASE WHEN DATEPART(DW, @BeginDate)=2 THEN 7
                                                             WHEN DATEPART(DW, @BeginDate)=3 THEN 1
                                                             WHEN DATEPART(DW, @BeginDate)=4 THEN 2
                                                             WHEN DATEPART(DW, @BeginDate)=5 THEN 3
                                                             WHEN DATEPART(DW, @BeginDate)=6 THEN 4
                                                             WHEN DATEPART(DW, @BeginDate)=7 THEN 5
                                                             WHEN DATEPART(DW, @BeginDate)=1 THEN 6 END) ELSE @BeginDate END)    
          


DECLARE @Date TABLE(ID INT IDENTITY,DateVal DATETIME,FromDate DATETIME,ToDate DATETIME,MonthNo INT,YearNo INT)

INSERT INTO @Date(DateVal)
SELECT TOP (DATEDIFF(mm,@StartDate,@EndDate) + 1)
        MonthDate = DATEADD(mm,DATEDIFF(mm,0,@StartDate) 
                  + (ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) -1),0)
   FROM sys.all_columns ac1
  CROSS JOIN sys.all_columns ac2
  


DECLARE @Count INT=(SELECT COUNT(1) FROM @Date);
DECLARE @CountVal INT=1,@FromDate DATETIME;
WHILE @CountVal <=@Count 
BEGIN

DECLARE @YearNo INT=(SELECT YEAR(DateVal) FROM @Date WHERE ID=@CountVal),
		@MonthNo INT=(SELECT MONTH(DateVal) FROM @Date WHERE ID=@CountVal);

SET @FromDate=DATEADD(mm,(@YearNo - 1900)* 12 + @MonthNo - 1,1 - 1)



SET @FromDate=(CASE WHEN @StdMonthReport=1 THEN @FromDate -(CASE WHEN DATEPART(DW, @FromDate)=2 THEN 7
                                                             WHEN DATEPART(DW, @FromDate)=3 THEN 1
                                                             WHEN DATEPART(DW, @FromDate)=4 THEN 2
                                                             WHEN DATEPART(DW, @FromDate)=5 THEN 3
                                                             WHEN DATEPART(DW, @FromDate)=6 THEN 4
                                                             WHEN DATEPART(DW, @FromDate)=7 THEN 5
                                                             WHEN DATEPART(DW, @FromDate)=1 THEN 6 END) ELSE @FromDate END)    
          


SET @EndDate=@FromDate+41
UPDATE @Date SET FromDate=@FromDate,ToDate=@EndDate,MonthNo=MONTH(@FromDate) WHERE MONTH(DateVal)=@MonthNo AND YEAR(DateVal)=@YearNo
SET @FromDate=DATEADD(mm,(YEAR(@FromDate) - 1900)* 12 + MONTH(@FromDate) - 1,1 - 1);
SET @CountVal=@CountVal+1

END
     

DECLARE @DATEFROM DATETIME=@BeginDate
DECLARE @DATETO DATETIME=@EndDate



 DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                             WHEN @Vendors=2 THEN 1 
                             WHEN @Vendors=3 THEN 0
                             END)
   
   DECLARE @TripStatus AS VARCHAR(20) = '';
   
   IF @IsWorkSheet = 1
   SET @TripStatus = 'W,'
   
   IF  @IsTrip = 1
   SET @TripStatus = @TripStatus + 'T,'

  IF  @IsUnFulFilled = 1
   SET @TripStatus = @TripStatus + 'U,'
   
   IF  @IsCanceled = 1
   SET @TripStatus = @TripStatus + 'X,'
   
   IF  @IsHold = 1
   SET @TripStatus = @TripStatus + 'H'
   


 IF @HomeBase=1
 BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                                INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                 WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                  AND UM.UserName = RTRIM(@UserCD)


 END

 


 -----Rest Record and Maintenace Reco
 DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9),CrewID BIGINT,AircraftCD VARCHAR(4))
 


 
  DECLARE @TableTrip TABLE(TripNUM BIGINT,
                           FlightNUM VARCHAR(12),
                           PassengerTotal INT,
                           PassengerRequestorCD VARCHAR(5),
                           DepartmentCD VARCHAR(8),
                           AuthorizationCD VARCHAR(8),
                           TripDescription VARCHAR(40),
                           FlightPurpose VARCHAR(40),
                           SeatsAvailabe INT,
                           CrewList VARCHAR(300),
                           CummETE NUMERIC(7,3),
                           TripStatus CHAR(2),
                           FlightCatagoryCD CHAR(4),
                           DepartureDTTMLocal DATETIME,
                           ArrivalDTTMLocal DATETIME,
                           FleetID BIGINT,
                           TailNum VARCHAR(9),
                           TypeCode VARCHAR(10),
                           DutyTYPE CHAR(2),
                           NextLocalDTTM DATETIME,
                           RecordTye CHAR(1),
                           DepICAOID VARCHAR(25),
                           ArrivelICAOID VARCHAR(25),
                           DepCity VARCHAR(25),
                           ArrCity VARCHAR(25),
                           DepState VARCHAR(25),
                           ArrState VARCHAR(25),
                           LegNum BIGINT,
                           TripID BIGINT,
                           LegID BIGINT,
                           VendorCode VARCHAR(5),
                           FgColor VARCHAR(8),
                           BgColr  VARCHAR(8),
                           AircraftCD VARCHAR(4),
                           CrewID BIGINT,
                           CrewCD VARCHAR(5)
                           )
  


    
  INSERT INTO @TableTrip
  SELECT DISTINCT PM.TripNUM
      ,PM.FlightNUM
      ,PL.PassengerTotal
      ,P.PassengerRequestorCD
      ,DEP.DepartmentCD
      ,DA.AuthorizationCD
      ,PM.TripDescription
      ,PL.FlightPurpose
      ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
      ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
                   WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
      ,CummETE=PL1.Elapse
      ,PM.TripStatus
      ,FC.FlightCatagoryCD
      ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
      ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
      ,PM.FleetID
      ,F.TailNum
      ,AFT.AircraftCD
      ,PL.DutyTYPE
      ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
      ,PM.RecordType
      ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
            WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
      ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
            WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
      ,D.CityName
      ,A.CityName
      ,D.StateName
      ,A.StateName
      ,PL.LegNUM
      ,PM.TripID
      ,PL.LegID
      ,V.VendorCD
      ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
      ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
       WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
      , F.AircraftCD
        ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
	   ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
         FROM PreflightMain PM 
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
             --  LEFT OUTER JOIN PreflightLeg PLN ON PL.TripID=PLN.TripID AND PL.LegNUM+1=PLN.LegNUM
               INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
               LEFT JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
               LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
               LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
               LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
               LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
               LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
               LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
               LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
               LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
               LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
               LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
               LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
                     INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
                     WHERE PC1.LegID = PC2.LegID   
                      ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
                                                                        WHEN 'Z' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
                     FOR XML PATH('') 
                    ) FROM PreflightCrewList PC2         
                                ) Crew ON PL.LegID = Crew.LegID 
           LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
           LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
           LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
          -- LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
           LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
                                                  FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                  WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
              LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
                                                  FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                  WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
       WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
        AND PM.IsDeleted=0
        AND ( 
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
       OR
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
       OR
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
        )
        AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	    AND (CW.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
        AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
        AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
        AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
        AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
        AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
        AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
        AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


  INSERT INTO @TableTrip
  SELECT DISTINCT PM.TripNUM
      ,PM.FlightNUM
      ,PL.PassengerTotal
      ,P.PassengerRequestorCD
      ,DEP.DepartmentCD
      ,DA.AuthorizationCD
      ,PM.TripDescription
      ,PL.FlightPurpose
      ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
      ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
                   WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
      ,CummETE=PL1.Elapse
      ,PM.TripStatus
      ,FC.FlightCatagoryCD
      ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
      ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
      ,PM.FleetID
      ,F.TailNum
      ,AFT.AircraftCD
      ,PL.DutyTYPE
      ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'  THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE  CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
      ,PM.RecordType
      ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
            WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
      ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
            WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
      ,D.CityName
      ,A.CityName
      ,D.StateName
      ,A.StateName
      ,PL.LegNUM
      ,PM.TripID
      ,PL.LegID 
      ,V.VendorCD
      ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
      ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
       WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
      ,F.AircraftCD
       ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
     ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
         FROM PreflightMain PM 
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
               INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
               LEFT JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
               LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
               LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
               LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
               LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
               LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
               LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
               LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
               LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
               LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
               LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
               LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
                     INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
                     WHERE PC1.LegID = PC2.LegID   
                      ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
                                                                        WHEN 'Z' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
                     FOR XML PATH('') 
                    ) FROM PreflightCrewList PC2         
                                ) Crew ON PL.LegID = Crew.LegID 
           LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
           LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
           LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
           --LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
           LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
                                                  FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                  WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
              LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
                                                  FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                  WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
       WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
        AND ( 
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
       OR
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
       OR
      (CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
        )
        AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	    AND (CW.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
        AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
        AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
        AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
        AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
        AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
        AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
        AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
        AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
        AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

 


 INSERT INTO @CurDate(DateWeek,FleetID,TailNum,AircraftCD)
 SELECT T.*,F.FleetID,F.TailNum,F.AircraftCD FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                             LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                  WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                    AND F.IsDeleted=0 
                                    AND F.IsInActive=0
                                    AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                    AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
  

 IF @IsActivityOnly=1
BEGIN

INSERT INTO @CurDate(DateWeek,CrewID)
SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND CC.IsDeleted=0 AND CC.IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
								  AND (CC.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
END
ELSE
BEGIN
DELETE FROM @TableTrip WHERE RecordTye='C' AND FleetID IS NULL 

END

INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,VendorCode,FgColor,BgColr,AircraftCD)
 SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE  CD.DateWeek END,TT.ArrivelICAOID,'R',
                 CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID,TT.VendorCode 
                ,TT.FgColor,TT.BgColr,TT.AircraftCD
                 FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                              WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                                AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                                AND RecordTye='T'
                                               -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


 INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,TripDescription,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,VendorCode,FgColor,BgColr,AircraftCD,CrewCD)
 SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,TripDescription,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                 CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID,TT.VendorCode
                ,TT.FgColor,TT.BgColr,TT.AircraftCD,CASE WHEN TT.CrewID IS NOT NULL THEN CrewCD ELSE '' END 
                  FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID OR CD.CrewID=TT.CrewID
                                              WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                                AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                                AND RecordTye IN('C','M')
                                                AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                              --  AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


                   
 DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


 DECLARE @TempTable TABLE(ID INT IDENTITY
                         ,WeekNumber INT
                         ,FleetID BIGINT
                         ,TailNum VARCHAR(9)
                         ,TripNUM BIGINT
                         ,location VARCHAR(200) 
                         ,FlightNUM VARCHAR(12)
                         ,pax_total INT 
                         ,reqcode VARCHAR(5)
                         ,DepartmentCD VARCHAR(8)
                         ,AuthorizationCD VARCHAR(8)
                         ,FlightPurpose VARCHAR(40)
                         ,SeatsAvailable INT
                         ,crewlist VARCHAR(300) 
                         ,CummETE NUMERIC(7,3)
                         ,TripStatus CHAR(2)
                         ,FlightCatagoryCD VARCHAR(25) 
                         ,Date DATE
                         ,DepartureDTTMLocal DATETIME
                         ,ArrivalDTTMLocal DATETIME
                         ,IsArrival DATE
                         ,LegNum BIGINT
                         ,SORTORDER INT
                         ,RecordType VARCHAR(1)
                         ,TypeCode VARCHAR(10)
                         ,legid BIGINT
                         ,vendcode VARCHAR(5)
                         ,DutyTYPE CHAR(2)
                         ,FgColor VARCHAR(8)
                         ,BgColr  VARCHAR(8)
                         ,AircraftCD VARCHAR(4)
                         ,RnkFirst INT
                         ,RnkLast  INT
                         ,DepRnk INT
                         ,gotofleet DATE
                       
                         ) 
  
 IF @IsDisplayRedEyeFlight=0
 BEGIN  
 INSERT INTO @TempTable   
 SELECT 0 WeekNumber
      ,TT.FleetID
       ,TT.TailNum
       ,TripNUM
   ,CASE WHEN DutyTYPE='R' THEN '(R) '+ISNULL(ArrivelICAOID,'') 
         WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ISNULL(ArrivelICAOID,'')+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
         ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ISNULL(DepICAOID,'')+' | '+ISNULL(ArrivelICAOID,'')+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
      ,FlightNUM
      ,PassengerTotal
      ,PassengerRequestorCD
      ,DepartmentCD
      ,AuthorizationCD
      ,FlightPurpose
      ,SeatsAvailabe
      ,CrewList
      ,CummETE
      ,TripStatus
      ,FlightCatagoryCD
      ,CONVERT(DATE,DepartureDTTMLocal)
      ,DepartureDTTMLocal
      ,ArrivalDTTMLocal
      ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
      ,LegNum
      ,1 AS SORTORDER
      ,TT.RecordTye
      ,TT.TypeCode
      ,TT.LegID
      ,TT.VendorCode
      ,TT.DutyTYPE
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
            ,TT.AircraftCD
      ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
      ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC) 
      ,ROW_NUMBER()OVER(PARTITION BY TT.FleetID ORDER BY TripNUM,DepartureDTTMLocal)
      ,gotofleet=NULL
       FROM @TableTrip TT 
            INNER JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
            LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
       WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
       ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum
 END
 ELSE
 BEGIN
INSERT INTO @TempTable 
  SELECT TEMP.*
       ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
       ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC)
       ,ROW_NUMBER()OVER(PARTITION BY FleetID ORDER BY TripNUM,DepartureDTTMLocal)
      ,gotofleet=NULL
       FROM( 
   SELECT 0 WeekNumber
        ,TT.FleetID
       ,TT.TailNum
       ,TripNUM
   ,CASE WHEN DutyTYPE='R' THEN '(R) '+ISNULL(ArrivelICAOID,'') 
         WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ISNULL(ArrivelICAOID,'')+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
         ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ISNULL(DepICAOID,'')+(CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN ' [continued]' ELSE  ' | '+ISNULL(ArrivelICAOID,'')+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END) END Location
      ,FlightNUM
      ,PassengerTotal
      ,PassengerRequestorCD
      ,DepartmentCD
      ,AuthorizationCD
      ,FlightPurpose
      ,SeatsAvailabe
      ,CrewList
      ,CummETE
      ,TripStatus
      ,FlightCatagoryCD
      ,CONVERT(DATE,DepartureDTTMLocal)Date
      ,DepartureDTTMLocal
      ,ArrivalDTTMLocal
      ,NULL IsArrival 
      ,LegNum
      ,1 AS SORTORDER
      ,TT.RecordTye
      ,TT.TypeCode
      ,TT.LegID
      ,TT.VendorCode
      ,TT.DutyTYPE
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
      ,TT.AircraftCD
       FROM @TableTrip TT 
      INNER JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
            LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
       WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)


  UNION ALL

SELECT 0 WeekNumber
       ,TT.FleetID
       ,TT.TailNum
       ,TripNUM
   ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
         WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ISNULL(ArrivelICAOID,'')+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
         ELSE ' [continued] '  +ISNULL(ArrivelICAOID,'')+' | '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
      ,FlightNUM
      ,PassengerTotal
      ,PassengerRequestorCD
      ,DepartmentCD
      ,AuthorizationCD
      ,FlightPurpose
      ,SeatsAvailabe
      ,CrewList
      ,CummETE
      ,TripStatus
      ,FlightCatagoryCD
      ,CONVERT(DATE,ArrivalDTTMLocal)
      ,ArrivalDTTMLocal---DepartureDTTMLocal
      ,ArrivalDTTMLocal
      ,NULL  IsArrival  
      ,LegNum
      ,2 AS SORTORDER 
      ,TT.RecordTye
      ,TT.TypeCode
      ,TT.LegID
      ,TT.VendorCode
      ,TT.DutyTYPE
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
      ,TT.AircraftCD
       FROM @TableTrip TT 
      INNER JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
            LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
       WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
        AND CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal)
  )TEMP
 ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
 END

 
 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;


SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 



WHILE (@WEEKNUM<=@WEEK)
 BEGIN
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)
  WHILE @DateVal < @BeginDate
       BEGIN
       
		   UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=CONVERT(DATE,@DateVal)
		   INSERT INTO @TempTable(WeekNumber,Date,RnkFirst,RnkLast)
		   SELECT DISTINCT @WEEKNUM,@DateVal,1,1  WHERE NOT EXISTS(SELECT Date FROM @TempTable WHERE WeekNumber=@WEEKNUM AND Date=@DateVal) 
	             
			SET @DateVal=DATEADD(DAY,1,@DateVal)
       END
  SET @WEEKNUM=@WEEKNUM+1
  END



UPDATE @TempTable SET gotofleet=TT.DateVal FROM(SELECT DateVal,FromDate,ToDate FROM @Date)TT
				   WHERE Date BETWEEN TT.FromDate AND TT.ToDate



IF @IsFirstLastLeg=0
 BEGIN
;WITH TripInfo AS(
SELECT display=TailNum
   ,col=col1+(CASE WHEN ISNULL(col2,'')<>'' THEN (CASE WHEN ISNULL(col1,'')<>'' THEN  '@'+col2 ELSE  col2  END)  ELSE '' END) +(CASE WHEN  SUBSTRING(col3,1,3)='(R)' THEN ' '+col3 ELSE (CASE WHEN ISNULL(col1,'')<>'' OR ISNULL(col2,'')<>'' THEN  '@'+col3 ELSE  col3  END) END)+(CASE WHEN ISNULL(col4,'')<>'' THEN '@'+col4  ELSE '' END)+((CASE WHEN ISNULL(col5,'')<>'' THEN '@'+col5  ELSE '' END))
    ,dispcolor='#FFFFFF'
    ,fcolor=FgColor
    ,bcolor=BgColr
    ,uline='TRUE'--CASE WHEN DayVal=1 THEN 'TRUE' ELSE 'FALSE' END
    ,orig_nmbr
    ,leg_num
    ,legid
    ,caldate
    ,type_code=TypeCode
    ,vendcode
    ,tail_nmbr=TailNum
    ,ordernum
    ,locdep
    ,flag
    ,timetype=@TimeBase+' Time'
    ,gotofleet
    ,ncounter=0
    ,ArrivalDTTMLocal
    ,SortDate
    ,WeekNumber
    ,SORTORDER
    ,DepartureDTTMLocal
   -- ,DayVal
    ,FleetID
    ,AircraftCD
    ,crewlist
  FROM(
 SELECT ID
       ,WeekNumber
       ,1 WEEKDAY
       ,FleetID
       ,TypeCode
       ,TailNum
       ,Date SortDate
      ,col1=(CASE WHEN TailNum <> '' THEN  TailNum ELSE '' END) +(CASE WHEN ISNULL(crewlist,'') <>''  THEN ' - '+ISNULL(crewlist,'') ELSE '' END)
       ,col2=(CASE WHEN SUBSTRING(location,1,3)='(R)' THEN '' ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE '' END) END)
             +(CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE '' END)
             +(CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  ' '+'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(CummETE,0)*10)*0.1) ELSE '' END)
       ,col3=location+(CASE WHEN @IsFlightNo=1 AND location IS NOT NULL AND RecordType='T' THEN ' FlightNo: '+ISNULL(FlightNUM,'') ELSE '' END )
       ,col4=CASE WHEN @IsDisplayRedEyeFlight=0 THEN '** Arrival Date: **'+ CONVERT(VARCHAR(10),IsArrival,101) ELSE '' END
       ,col5=(CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE '' END)
             +(CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  ' ('+FlightCatagoryCD+')' ELSE '' END)
             +(CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Req: '+ISNULL(reqcode,'') ELSE '' END)
             +(CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Dept: '+ISNULL(DepartmentCD,'') ELSE '' END)
             +(CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Auth: '+ISNULL(AuthorizationCD,'') ELSE '' END)
             +(CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Purpose: '+ISNULL(FlightPurpose,'') ELSE '' END)
             +(CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END )
       ------Export------------
       ,orig_nmbr=TripNUM
       ,leg_num=LegNum
       ,legid
       ,vendcode
       ,caldate=Date  
       ,TT.FgColor
       ,TT.BgColr 
       ,ncounter=0
       ,locdep=TT.DepartureDTTMLocal 
       ,ordernum=RIGHT(RTRIM(CONVERT(CHAR(5),'0000'+CONVERT(CHAR(5),DENSE_RANK()OVER(PARTITION BY WeekNumber ORDER BY  WeekNumber,AircraftCD)+1))),5) 
       ,flag='A'
       ,ArrivalDTTMLocal
       ,SORTORDER
       ,DepartureDTTMLocal
       ,AircraftCD
       ,crewlist  
       ,gotofleet
  FROM @TempTable TT
   WHERE DATE BETWEEN @DATEFROM AND @DATETO

)TEMP
)

							


SELECT DISTINCT  WeekNumber
	  ,display=CASE WHEN RnkVal=1 AND ISNULL(display,'')<>'' THEN display END
	  ,day=CASE WHEN RnkVal=1 AND ISNULL(col1,'')<>'' THEN SUBSTRING(col1,1,LEN(col1)-1) END
	  ,orig_nmbr=CASE WHEN RnkVal=1 AND ISNULL(orig_nmbr,'')<>'' THEN SUBSTRING(orig_nmbr,1,LEN(orig_nmbr)-1) END
	  ,tail=CASE WHEN RnkVal=1 AND ISNULL(tail,'')<>'' THEN SUBSTRING(tail,1,LEN(tail)-1) END
	  ,legid=CASE WHEN RnkVal=1 AND ISNULL(legid,'')<>'' THEN SUBSTRING(legid,1,LEN(legid)-1) END
	  ,crew=CASE WHEN RnkVal=1 AND ISNULL(crew,'')<>'' THEN SUBSTRING(crew,1,LEN(crew)-1) END
	  ,crewcode=''
	  ,fcolor=CASE WHEN RnkVal=1 AND ISNULL(fcolor,'')<>'' THEN SUBSTRING(fcolor,1,LEN(fcolor)-1) END
	  ,bcolor=CASE WHEN RnkVal=1 AND ISNULL(bcolor,'')<>'' THEN SUBSTRING(bcolor,1,LEN(bcolor)-1) END
	  ,caldate=SUBSTRING(caldate,1,LEN(caldate)-1)
	  ,timetype=SUBSTRING(timetype,1,LEN(timetype)-1)
	  ,gotomonth=SUBSTRING(gotofleet,1,LEN(gotofleet)-1)
	  ,ncounter=SUBSTRING(ncounter,1,LEN(ncounter)-1)
	  ,ArrivalDTTMLocal
	  ,SORTORDER
	  ,SortDate
	  ,DepartureDTTMLocal
	  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
	  ,DayNo
	  ,uline=SUBSTRING(uline,1,LEN(uline)-1) FROM(
SELECT DISTINCT TI.WeekNumber
	  ,display
	  ,col1=TC.col
	  ,dispcolor=REPLACE(TT.ColVal,'B',TI.dispcolor+'@')
	  ,fcolor= (CASE WHEN len(TC.fcolor)-len(replace(TC.fcolor,'@',''))=MaxCount THEN TC.fcolor ELSE TC.fcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.fcolor)-len(replace(TC.fcolor,'@','')))),'Z','0@') END)
	  ,bcolor=(CASE WHEN len(TC.bcolor)-len(replace(TC.bcolor,'@',''))=MaxCount THEN TC.bcolor ELSE TC.bcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.bcolor)-len(replace(TC.bcolor,'@','')))),'Z','0@') END)
	  ,orig_nmbr=(CASE WHEN len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@',''))=MaxCount THEN TC.orig_nmbr ELSE TC.orig_nmbr+REPLACE(REPLICATE('Z',MaxCount-(len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@','')))),'Z','0@') END)
	  ,legid=(CASE WHEN len(TC.legid)-len(replace(TC.legid,'@',''))=MaxCount THEN TC.legid ELSE TC.legid+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legid)-len(replace(TC.legid,'@','')))),'Z','0@') END)
	  ,caldate=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(10),TI.caldate,101)+'@')
	  ,type_code=REPLACE(TT.ColVal,'B',type_code+'@')
	  ,vendcode=REPLACE(TT.ColVal,'B',vendcode+'@')
	  ,tail_nmbr=REPLACE(TT.ColVal,'B',tail_nmbr+'@')
	  ,ordernum=REPLACE(TT.ColVal,'B',ordernum+'@')
	  ,locdep=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,101)+'@')
	  ,flag=REPLACE(TT.ColVal,'B',flag+'@')
	  ,timetype=REPLACE(TT.ColVal,'B',timetype+'@')
	  ,gotofleet=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(10),TI.gotofleet,101)+'@')
	  ,ncounter=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(2),TI.ncounter,101)+'@')
	  ,ColVal=ISNULL(TT.ColVal,'')
	  ,AircraftCD
	  ,ArrivalDTTMLocal
	  ,SORTORDER
	  ,SortDate
	  ,TI.DepartureDTTMLocal
	  --,TI.DayVal
	  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
	  ,DayNo=DENSE_RANK()OVER(PARTITION BY TI.WeekNumber ORDER BY TI.Caldate)
	  ,RnkVal=ROW_NUMBER()OVER(PARTITION BY TI.Caldate ORDER BY TI.Caldate)
	  ,tail=CASE WHEN ISNULL(TC.tail,'')<>'' THEN TC.tail END
	  ,crew=CASE WHEN ISNULL(TC.crew,'')<>'' THEN TC.crew END
	  ,MaxCount
	  ,uline=CASE WHEN ISNULL(tail_nmbr,'') <>'' THEN  'TRUE@'+SUBSTRING(REPLACE(TT.ColVal,'B','FALSE'+'@'),1,LEN(REPLACE(TT.ColVal,'B','FALSE'+'@'))-5) ELSE REPLACE(TT.ColVal,'B','FALSE'+'@') END 
 FROM TripInfo TI
      LEFT JOIN (SELECT WeekNumber,FleetID,MIN(DepartureDTTMLocal) DepartureDTTMLocal FROM TripInfo TM GROUP BY WeekNumber,FleetID)TM ON TM.WeekNumber=TI.WeekNumber AND TM.FleetID=TI.FleetID
	  LEFT JOIN (SELECT WeekNumber,REPLICATE('B',MAX(MaxCount))ColVal,MAX(MaxCount) MaxCount FROM (
					SELECT caldate,REPLICATE('B',MAX(TT.ColCount))ColVal,WeekNumber,MAX(TT.ColCount) MaxCount FROM 
								 (
								 SELECT caldate,SUM((len(col)-len(replace(col,'@','')))+1) ColCount,WeekNumber FROM  TripInfo GROUP BY WeekNumber,caldate
								 )TT
				GROUP BY WeekNumber,caldate
			)TTT GROUP BY WeekNumber)TT ON TI.WeekNumber=TT.WeekNumber
	  LEFT JOIN (SELECT T1.WeekNumber,T1.caldate,col=(SELECT CASE WHEN ISNULL(col,'')<>'' THEN col+'@' ELSE '' END FROM (SELECT WeekNumber,caldate,col,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,orig_nmbr=(SELECT  CASE WHEN ISNULL(orig_nmbr,'')<>'' THEN (CASE WHEN RIGHT(orig_nmbr,1)='@' THEN orig_nmbr ELSE  orig_nmbr+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),orig_nmbr))orig_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,legid=(SELECT  CASE WHEN ISNULL(legid,'')<>'' THEN (CASE WHEN RIGHT(legid,1)='@' THEN legid ELSE  legid+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),legid))legid,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)	
								,tail=(SELECT  CASE WHEN ISNULL(tail_nmbr,'')<>'' THEN (CASE WHEN RIGHT(tail_nmbr,1)='@' THEN tail_nmbr ELSE  tail_nmbr+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',tail_nmbr)tail_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,crew=(SELECT  CASE WHEN ISNULL(crew,'')<>'' THEN (CASE WHEN RIGHT(crew,1)='@' THEN crew ELSE  crew+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',crewlist)crew,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,fcolor=(SELECT  CASE WHEN ISNULL(fcolor,'')<>'' THEN (CASE WHEN RIGHT(fcolor,1)='@' THEN fcolor ELSE  fcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',fcolor)fcolor,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,bcolor=(SELECT  CASE WHEN ISNULL(bcolor,'')<>'' THEN (CASE WHEN RIGHT(bcolor,1)='@' THEN bcolor ELSE  bcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',bcolor)bcolor,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
									FROM(SELECT DISTINCT WeekNumber,caldate FROM  TripInfo) T1
								

			)TC ON TC.WeekNumber=TI.WeekNumber AND TC.caldate=TI.caldate 

 )WeekFleet
WHERE RnkVal=1

 ORDER BY WeekNumber,SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,orig_nmbr,SORTORDER


 END
 ELSE
 
 BEGIN
;WITH TripInfo AS(
SELECT display=TailNum
   ,col=col1+(CASE WHEN ISNULL(col2,'')<>'' THEN (CASE WHEN ISNULL(col1,'')<>'' THEN  '@'+col2 ELSE  col2  END)  ELSE '' END) +(CASE WHEN  SUBSTRING(col3,1,3)='(R)' THEN ' '+col3 ELSE (CASE WHEN ISNULL(col1,'')<>'' OR ISNULL(col2,'')<>'' THEN  '@'+col3 ELSE  col3  END) END)+(CASE WHEN ISNULL(col4,'')<>'' THEN '@'+col4  ELSE '' END)+((CASE WHEN ISNULL(col5,'')<>'' THEN '@'+col5  ELSE '' END))
    ,dispcolor='#FFFFFF'
    ,fcolor=FgColor
    ,bcolor=BgColr
    ,uline='TRUE'--CASE WHEN DayVal=1 THEN 'TRUE' ELSE 'FALSE' END
    ,orig_nmbr
    ,leg_num
    ,legid
    ,caldate
    ,type_code=TypeCode
    ,vendcode
    ,tail_nmbr=TailNum
    ,ordernum
    ,locdep
    ,flag
    ,timetype=@TimeBase+' Time'
    ,gotofleet
    ,ncounter=0
    ,ArrivalDTTMLocal
    ,SortDate
    ,WeekNumber
    ,SORTORDER
    ,DepartureDTTMLocal
   -- ,DayVal
    ,FleetID
    ,AircraftCD
    ,crewlist
  FROM(
 SELECT ID
       ,WeekNumber
       ,1 WEEKDAY
       ,FleetID
       ,TypeCode
       ,TailNum
       ,Date SortDate
      ,col1=(CASE WHEN TailNum <> '' THEN  TailNum ELSE '' END) +(CASE WHEN ISNULL(crewlist,'') <>''  THEN ' - '+ISNULL(crewlist,'') ELSE '' END)
      ,col2=(CASE WHEN SUBSTRING(location,1,3)='(R)' THEN '' ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE '' END) END)
             +(CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE '' END)
             +(CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  ' '+'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(CummETE,0)*10)*0.1) ELSE '' END)
       ,col3=location+(CASE WHEN @IsFlightNo=1 AND location IS NOT NULL AND RecordType='T' THEN ' FlightNo: '+ISNULL(FlightNUM,'') ELSE '' END )
       ,col4=CASE WHEN @IsDisplayRedEyeFlight=0 THEN '** Arrival Date: **'+ CONVERT(VARCHAR(10),IsArrival,101) ELSE '' END
       ,col5=(CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE '' END)
             +(CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  ' ('+FlightCatagoryCD+')' ELSE '' END)
             +(CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Req: '+ISNULL(reqcode,'') ELSE '' END)
             +(CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Dept: '+ISNULL(DepartmentCD,'') ELSE '' END)
             +(CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Auth: '+ISNULL(AuthorizationCD,'') ELSE '' END)
             +(CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Purpose: '+ISNULL(FlightPurpose,'') ELSE '' END)
             +(CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END )
       ------Export------------
       ,orig_nmbr=TripNUM
       ,leg_num=LegNum
       ,legid
       ,vendcode
       ,caldate=Date  
       ,TT.FgColor
       ,TT.BgColr 
       ,ncounter=0
       ,locdep=TT.DepartureDTTMLocal 
       ,ordernum=RIGHT(RTRIM(CONVERT(CHAR(5),'0000'+CONVERT(CHAR(5),DENSE_RANK()OVER(PARTITION BY WeekNumber ORDER BY  WeekNumber,AircraftCD)+1))),5) 
       ,flag='A'
       ,ArrivalDTTMLocal
       ,SORTORDER
       ,DepartureDTTMLocal
       ,AircraftCD
       ,crewlist
       ,gotofleet
       
  FROM @TempTable TT
   WHERE DATE BETWEEN @DATEFROM AND @DATETO
    AND (RnkFirst=1 OR RnkLast=1) 

)TEMP 
)



SELECT DISTINCT  WeekNumber
	  ,display=CASE WHEN RnkVal=1 AND ISNULL(display,'')<>'' THEN display END
	  ,day=CASE WHEN RnkVal=1 AND ISNULL(col1,'')<>'' THEN SUBSTRING(col1,1,LEN(col1)-1) END
	  ,orig_nmbr=CASE WHEN RnkVal=1 AND ISNULL(orig_nmbr,'')<>'' THEN SUBSTRING(orig_nmbr,1,LEN(orig_nmbr)-1) END
	  ,tail=CASE WHEN RnkVal=1 AND ISNULL(tail,'')<>'' THEN SUBSTRING(tail,1,LEN(tail)-1) END
	  ,legid=CASE WHEN RnkVal=1 AND ISNULL(legid,'')<>'' THEN SUBSTRING(legid,1,LEN(legid)-1) END
	  ,crew=CASE WHEN RnkVal=1 AND ISNULL(crew,'')<>'' THEN SUBSTRING(crew,1,LEN(crew)-1) END
	  ,crewcode=''
	  ,fcolor=CASE WHEN RnkVal=1 AND ISNULL(fcolor,'')<>'' THEN SUBSTRING(fcolor,1,LEN(fcolor)-1) END
	  ,bcolor=CASE WHEN RnkVal=1 AND ISNULL(bcolor,'')<>'' THEN SUBSTRING(bcolor,1,LEN(bcolor)-1) END
	  ,caldate=SUBSTRING(caldate,1,LEN(caldate)-1)
	  ,timetype=SUBSTRING(timetype,1,LEN(timetype)-1)
	  ,gotomonth=SUBSTRING(gotofleet,1,LEN(gotofleet)-1)
	  ,ncounter=SUBSTRING(ncounter,1,LEN(ncounter)-1)
	  ,ArrivalDTTMLocal
	  ,SORTORDER
	  ,SortDate
	  ,DepartureDTTMLocal
	  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
	  ,DayNo
	  ,uline=SUBSTRING(uline,1,LEN(uline)-1) FROM(
SELECT DISTINCT TI.WeekNumber
	  ,display
	  ,col1=TC.col
	  ,dispcolor=REPLACE(TT.ColVal,'B',TI.dispcolor+'@')
	  ,fcolor= (CASE WHEN len(TC.fcolor)-len(replace(TC.fcolor,'@',''))=MaxCount THEN TC.fcolor ELSE TC.fcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.fcolor)-len(replace(TC.fcolor,'@','')))),'Z','0@') END)
	  ,bcolor=(CASE WHEN len(TC.bcolor)-len(replace(TC.bcolor,'@',''))=MaxCount THEN TC.bcolor ELSE TC.bcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.bcolor)-len(replace(TC.bcolor,'@','')))),'Z','0@') END)
	  ,orig_nmbr=(CASE WHEN len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@',''))=MaxCount THEN TC.orig_nmbr ELSE TC.orig_nmbr+REPLACE(REPLICATE('Z',MaxCount-(len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@','')))),'Z','0@') END)
	  ,legid=(CASE WHEN len(TC.legid)-len(replace(TC.legid,'@',''))=MaxCount THEN TC.legid ELSE TC.legid+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legid)-len(replace(TC.legid,'@','')))),'Z','0@') END)
	  ,caldate=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(10),TI.caldate,101)+'@')
	  ,type_code=REPLACE(TT.ColVal,'B',type_code+'@')
	  ,vendcode=REPLACE(TT.ColVal,'B',vendcode+'@')
	  ,tail_nmbr=REPLACE(TT.ColVal,'B',tail_nmbr+'@')
	  ,ordernum=REPLACE(TT.ColVal,'B',ordernum+'@')
	  ,locdep=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,101)+'@')
	  ,flag=REPLACE(TT.ColVal,'B',flag+'@')
	  ,timetype=REPLACE(TT.ColVal,'B',timetype+'@')
	  ,gotofleet=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(10),TI.gotofleet,101)+'@')
	  ,ncounter=REPLACE(TT.ColVal,'B',CONVERT(VARCHAR(2),TI.ncounter,101)+'@')
	  ,ColVal=ISNULL(TT.ColVal,'')
	  ,AircraftCD
	  ,ArrivalDTTMLocal
	  ,SORTORDER
	  ,SortDate
	  ,TI.DepartureDTTMLocal
	  --,TI.DayVal
	  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
	  ,DayNo=DENSE_RANK()OVER(PARTITION BY TI.WeekNumber ORDER BY TI.Caldate)
	  ,RnkVal=ROW_NUMBER()OVER(PARTITION BY TI.Caldate ORDER BY TI.Caldate)
	  ,tail=CASE WHEN ISNULL(TC.tail,'')<>'' THEN TC.tail END
	  ,crew=CASE WHEN ISNULL(TC.crew,'')<>'' THEN TC.crew END
	  ,MaxCount
	  ,uline=CASE WHEN ISNULL(tail_nmbr,'') <>'' THEN  'TRUE@'+SUBSTRING(REPLACE(TT.ColVal,'B','FALSE'+'@'),1,LEN(REPLACE(TT.ColVal,'B','FALSE'+'@'))-5) ELSE REPLACE(TT.ColVal,'B','FALSE'+'@') END 
 FROM TripInfo TI
      LEFT JOIN (SELECT WeekNumber,FleetID,MIN(DepartureDTTMLocal) DepartureDTTMLocal FROM TripInfo TM GROUP BY WeekNumber,FleetID)TM ON TM.WeekNumber=TI.WeekNumber AND TM.FleetID=TI.FleetID
	  LEFT JOIN (SELECT WeekNumber,REPLICATE('B',MAX(MaxCount))ColVal,MAX(MaxCount) MaxCount FROM (
					SELECT caldate,REPLICATE('B',MAX(TT.ColCount))ColVal,WeekNumber,MAX(TT.ColCount) MaxCount FROM 
								 (
								 SELECT caldate,SUM((len(col)-len(replace(col,'@','')))+1) ColCount,WeekNumber FROM  TripInfo GROUP BY WeekNumber,caldate
								 )TT
				GROUP BY WeekNumber,caldate
			)TTT GROUP BY WeekNumber)TT ON TI.WeekNumber=TT.WeekNumber
	  LEFT JOIN (SELECT T1.WeekNumber,T1.caldate,col=(SELECT CASE WHEN ISNULL(col,'')<>'' THEN col+'@' ELSE '' END FROM (SELECT WeekNumber,caldate,col,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,orig_nmbr=(SELECT  CASE WHEN ISNULL(orig_nmbr,'')<>'' THEN (CASE WHEN RIGHT(orig_nmbr,1)='@' THEN orig_nmbr ELSE  orig_nmbr+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),orig_nmbr))orig_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,legid=(SELECT  CASE WHEN ISNULL(legid,'')<>'' THEN (CASE WHEN RIGHT(legid,1)='@' THEN legid ELSE  legid+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),legid))legid,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)	
								,tail=(SELECT  CASE WHEN ISNULL(tail_nmbr,'')<>'' THEN (CASE WHEN RIGHT(tail_nmbr,1)='@' THEN tail_nmbr ELSE  tail_nmbr+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',tail_nmbr)tail_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,crew=(SELECT  CASE WHEN ISNULL(crew,'')<>'' THEN (CASE WHEN RIGHT(crew,1)='@' THEN crew ELSE  crew+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',crewlist)crew,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,fcolor=(SELECT  CASE WHEN ISNULL(fcolor,'')<>'' THEN (CASE WHEN RIGHT(fcolor,1)='@' THEN fcolor ELSE  fcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',fcolor)fcolor,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
								,bcolor=(SELECT  CASE WHEN ISNULL(bcolor,'')<>'' THEN (CASE WHEN RIGHT(bcolor,1)='@' THEN bcolor ELSE  bcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',bcolor)bcolor,DepartureDTTMLocal,ArrivalDTTMLocal FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
									)
									FROM(SELECT DISTINCT WeekNumber,caldate FROM  TripInfo) T1
								

			)TC ON TC.WeekNumber=TI.WeekNumber AND TC.caldate=TI.caldate 

 )WeekFleet
WHERE RnkVal=1
 ORDER BY WeekNumber,SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,orig_nmbr,SORTORDER

 END
 

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable
     

 
  
  END; 

 






GO


