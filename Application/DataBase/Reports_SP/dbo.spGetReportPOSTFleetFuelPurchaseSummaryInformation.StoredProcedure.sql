IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformation]            
 (            
  @UserCD AS VARCHAR(30),  --Mandatory            
  @DATEFROM DATETIME, --Mandatory            
  @DATETO DATETIME, --Mandatory            
  @TailNum Varchar(5000)='',--Optional            
  @FleetGroupCD Varchar(5000)='' --Optional            
 )            
-- =============================================            
-- Author: Mathes            
-- Create date: 31-07-2012            
-- Description: Fleet Fuel Purchase Summary - Report             
-- =============================================            
AS            
SET NOCOUNT ON   
BEGIN            
  DECLARE @SQLSCRIPT AS NVARCHAR(Max) = '';            
  DECLARE @ParameterDefinition AS NVARCHAR(1000)          
  DECLARE @FltID as BigInt
  DECLARE @RecCnt Int=0
  DECLARE @PrevCnt Int=0
  DECLARE @FltTailNum VARCHAR(10)
    DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
  
  Declare @DateRange Varchar(30)
  Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
  
  DECLARE @TempAFS TABLE                
          ( TailNum Varchar(9),
            AircraftCD varchar(15),
            FuelLocatorCD Char(4),  
            FuelLocatorDescription Varchar(32), 
            PurchaseDT DateTime,              
            DispatchNUM Varchar(12),              
            AirportName Varchar(30),              
            FBOVendor Varchar(60),               
            IcaoID Char(4),         
            FuelQTY Numeric(15,4),            
            UnitPrice Numeric(10,4),                  
            FederalTAX Numeric(17,2),             
            SateTAX Numeric(17,2),              
            SaleTAX Numeric(17,2),   
            AccountDescription Varchar(40),     
            ExpenseAMT Numeric(17,2),
            Unit varchar(15),
            Units Varchar(15),
            FleetID BigInt)
            
   IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
      DROP table #TmpFltTail    
      
  Create Table #TmpFltTail (FleetID BigInt,TailNum Varchar(10),AircraftCD varchar(12))  
  Declare @AircraftCD varchar(12)
  --INSERT INTO #TmpFltTail
  --SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD 
  --FROM   Fleet F LEFT OUTER JOIN FleetGroupOrder FGO              
  --       ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID LEFT OUTER JOIN FleetGroup FG               
  --       ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
  --WHERE (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
  --      (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
  --       F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  --       And F.IsInActive<>1
  
  IF @TailNum ='' AND @FleetGroupCD =''
  Begin
   INSERT INTO #TmpFltTail SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD  FROM   Fleet F  
   WHERE F.IsInActive<>1 AND F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  End
  
  IF @TailNum <>''
  Begin
    INSERT INTO #TmpFltTail SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD FROM Fleet F
    WHERE TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  AND
    F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
    And F.IsInActive<>1
  End 
   
  IF @FleetGroupCD <>''
  Begin
   INSERT INTO #TmpFltTail SELECT DISTINCT FLEET.FleetID, TailNum,AircraftCD  
   FROM  FleetGroupOrder INNER JOIN Fleet ON FleetGroupOrder.FleetID = Fleet.FleetID    
   INNER JOIN FleetGroup ON FleetGroup.FleetGroupID=FleetGroupOrder.FleetGroupID 
   WHERE FleetGroupOrder.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
   FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) AND
   Fleet.FleetID Not IN(SELECT DISTINCT FleetID From #TmpFltTail)
  End      
  
  WHILE ( SELECT  COUNT(*) FROM #TmpFltTail ) > 0               
   BEGIN          
     Select Top 1 @FltID=FleetID,@FltTailNum=TailNum,@AircraftCD=AircraftCD From #TmpFltTail   
     Set @PrevCnt = (Select Count(*) From @TempAFS)          
     Insert Into @TempAFS SELECT  
            TailNum=(Select @FltTailNum),
            AircraftCD=(Select @AircraftCD),
            FuelLocator.FuelLocatorCD,  
            FuelLocator.FuelLocatorDescription, 
            PostflightExpense.PurchaseDT,              
            PostflightExpense.DispatchNUM,              
            Airport.AirportName,              
            FBO.FBOVendor,           
            Airport.IcaoID,              
            DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD) FuelQTY,        
            Case WHEN C.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End As UnitPrice,
            IsNull(PostflightExpense.FederalTAX,0),                     
            IsNull(PostflightExpense.SateTAX,0),                       
            IsNull(PostflightExpense.SaleTAX,0),                 
            IsNull(Account.AccountDescription,0),                   
            IsNull(PostflightExpense.ExpenseAMT,0),            
            Case C.FuelPurchase When 1 Then 'GAL' When 2 Then 'LTR' WHEN 3 THEN 'IMP' WHEN 4 THEN 'POUNDS' ELSE 'KILOS' End As Unit,  
            Case C.FuelPurchase When 1 Then 'GALS' When 2 Then 'LITERS' WHEN 3 THEN 'IMP GALS' WHEN 4 THEN 'POUNDS' ELSE 'KILOS' End As Units,
            F.FleetID 
     FROM   PostflightExpense 
			INNER JOIN PostflightLeg ON PostflightExpense.POLegID = PostflightLeg.POLegID AND postflightleg.IsDeleted = 0     
            LEFT OUTER JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID        
            INNER JOIN
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum, F.HomebaseID,F.CustomerID               
                FROM   Fleet F  
                LEFT OUTER JOIN FleetGroupOrder FGO              
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
                LEFT OUTER JOIN FleetGroup FG               
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
                Where F.FleetID=@FltID
                AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')           
             ) F ON F.FleetID = PostflightExpense.FleetID   
            LEFT OUTER JOIN Airport ON PostflightExpense.AirportID = Airport.AirportID       
            LEFT OUTER JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID       
            LEFT OUTER JOIN Account ON PostflightExpense.AccountID = Account.AccountID 
            --INNER JOIN Company C ON C.HomebaseID=F.HomebaseID
            INNER JOIN Company C ON F.CustomerID = C.CustomerID  AND C.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)
            INNER JOIN UserMaster UM ON UM.HomebaseID=C.HomebaseID AND UM.UserName=@UserCD AND UM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
            
     WHERE PostflightExpense.IsAutomaticCalculation=1 
     And  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
     AND PostflightExpense.IsDeleted=0
     And  CONVERT(DATE,PostflightExpense.PurchaseDT) BETWEEN CONVERT(DATE,'' + convert(varchar(20),@DATEFROM) +'' )AND CONVERT(DATE,''+convert(varchar(20),@DATETO)  +'')  AND PostflightExpense.IsDeleted=0       
    
      Set @RecCnt = (Select Count(*) From @TempAFS)
      print @RecCnt
      IF @PrevCnt = @RecCnt
      Begin
       Insert Into @TempAFS(TailNum,AircraftCD ) Values (@FltTailNum,@AircraftCD)
      End
      Delete From #TmpFltTail Where TailNum=@FltTailNum 
    END        
     
     /*
       UPDATE @TempAFS SET UNIT=Case UN.FuelPurchase When 1 Then 'GAL' When 2 Then 'LTR' WHEN 3 THEN 'IMP' WHEN 4 THEN 'F.' End ,
                        UNITS=Case UN.FuelPurchase When 1 Then 'GALS' When 2 Then 'Liters' WHEN 3 THEN 'Imp Gals' WHEN 4 THEN ' ' End
     FROM (
      SELECT FuelPurchase,U.HomebaseID,U.CustomerID FROM COMPANY  C INNER JOIN  USERMASTER U ON C.HomebaseID=U.HomebaseID WHERE C.CUSTOMERID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) AND USERNAME=@USERCD)UN
     WHERE FLEETID IS NOT NULL
     
     */
     
    IF @SuppressActivityAircraft=1
   BEGIN
       Select AircraftCD  [TailNum],TailNum Tail, FuelLocatorCD, FuelLocatorDescription, 
           dbo.GetDateFormatByUserCD(@UserCD,PurchaseDT) PurchaseDT, DispatchNUM,              
           AirportName, FBOVendor,IcaoID,FuelQTY, UnitPrice, FederalTAX, SateTAX, SaleTAX, 
           AccountDescription, ExpenseAMT,Unit, Units,FleetID,@DateRange DateRange from @TempAFS  
           WHERE FleetID IS NOT NULL
           Order By FuelLocatorCD, IcaoID, PurchaseDT 
   END
   ELSE
   BEGIN     
     
    Select AircraftCD  [TailNum],TailNum Tail, FuelLocatorCD, FuelLocatorDescription, 
           dbo.GetDateFormatByUserCD(@UserCD,PurchaseDT) PurchaseDT, DispatchNUM,              
           AirportName, FBOVendor,IcaoID,FuelQTY, UnitPrice, FederalTAX, SateTAX, SaleTAX, 
           AccountDescription, ExpenseAMT,Unit, Units,FleetID,@DateRange DateRange from @TempAFS  Order By FuelLocatorCD, IcaoID, PurchaseDT 
       END
    --Exec spGetReportPOSTFleetFuelPurchaseSummaryInformation 'jwilliams_13','01-jan-2000','31-dec-2012','N247RG' ,'45'                   
END 
 



GO


