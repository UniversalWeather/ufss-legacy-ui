
/****** Object:  StoredProcedure [dbo].[spGetReportPRETSBestFuelVendorInformation]    Script Date: 11/27/2014 18:35:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSBestFuelVendorInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSBestFuelVendorInformation]
GO

/****** Object:  StoredProcedure [dbo].[spGetReportPRETSBestFuelVendorInformation]    Script Date: 11/27/2014 18:35:36 ******/

CREATE PROCEDURE [dbo].[spGetReportPRETSBestFuelVendorInformation]
	@UserCD VARCHAR(50)--MANDATORY
	,@TripID VARCHAR(30)--MANDATORY
	,@LegNum VARCHAR(30) = ''
	,@IsBestFuel BIT = 0
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSBestFuelVendorInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight TripSheet Fuel Vendor information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON 

	IF @IsBestFuel = 1 -- BEST FEUL VENDORS
	BEGIN
		SELECT DISTINCT  [LegID] = CONVERT(VARCHAR,PL.LegID)
			,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[EstimatedGallons] = PF.EstFuelQTY
			,[DepartureICAOID] = A.IcaoID
			,[UpdatedAsOn] = PF.LastUpdTS
			,[BestPrice] = PF.BestPrice
			,[AsOf] = PF.EffectiveDT
			,[FromVendorFBO] = (CASE WHEN FV.VendorName IS NULL THEN 'UWA' ELSE FV.VendorName END) + '/' +  PF.FBOName
			
			-- Added to display All Fuel Vondors
			,[VendorName] = (CASE WHEN FV.VendorName IS NULL THEN 'UWA' ELSE FV.VendorName END)
			,[FBOName] = PF.FBOName
			,[ApplicableRangeFrom] = PF.GallonFrom
			,[ApplicableRangeTo] = PF.GallonTo
			,[UnitPrice] = PF.UnitPrice
			,Leg=PL.LegNUM		
			
			FROM PreflightLeg PL
			INNER JOIN (SELECT AirportID, IcaoID FROM Airport) A ON A.AirportID = PL.DepartICAOID 
			INNER JOIN ( -- GET THE MIN BESTPRICE RECORD FROM PreflightFuel
				SELECT TOP 1 PL.TRIPID, PL.EstFuelQTY, PF.LastUpdTS, PF.BestPrice, PF.EffectiveDT, 
					PF.FBOName, PF.GallonFrom, PF.GallonTo, PF.UnitPrice, PF.FuelVendorID,PF.AirportID
				FROM PreflightLeg PL
				INNER JOIN PreflightFuel PF ON PL.TripID = PF.TripID
				AND PL.DepartICAOID=PF.AirportID
				WHERE PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND PL.TripID = CONVERT(BIGINT, @TripID)
				AND PL.EstFuelQTY BETWEEN PF.GallonFrom AND PF.GallonTo
				ORDER BY PF.BestPrice ASC
			) PF ON PL.TripID = PF.TripID AND A.AirportID=PF.AirportID
			LEFT JOIN (SELECT FuelVendorID, VendorName FROM FuelVendor) FV ON FV.FuelVendorID = PF.FuelVendorID
			WHERE (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			ORDER BY PL.LegNUM
			
	END ELSE BEGIN
	-- ALL FEUL VENDORS
		SELECT DISTINCT [LegID] = CONVERT(VARCHAR,PL.LegID)
			,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[EstimatedGallons] = PF.EstFuelQTY
			,[DepartureICAOID] = A.IcaoID
			,[UpdatedAsOn] = PF.LastUpdTS
			,[BestPrice] = PF.BestPrice
			,[AsOf] = PF.EffectiveDT
			,[FromVendorFBO] = (CASE WHEN FV.VendorName IS NULL THEN 'UWA' ELSE FV.VendorName END) + '/' +  PF.FBOName
			
			-- Added to display All Fuel Vondors
			,[VendorName] = (CASE WHEN FV.VendorName IS NULL THEN 'UWA' ELSE FV.VendorName END)
			,[FBOName] = PF.FBOName
			,[ApplicableRangeFrom] = PF.GallonFrom
			,[ApplicableRangeTo] = PF.GallonTo
			,[UnitPrice] = PF.UnitPrice	
			,Leg=PL.LegNUM	
			
			FROM PreflightLeg PL
			INNER JOIN (SELECT AirportID, IcaoID FROM Airport) A ON A.AirportID = PL.DepartICAOID
				INNER JOIN ( 			
				--SELECT TOP 1 PM.TRIPID, PM.EstFuelQTY, PF.LastUpdTS, PF.BestPrice, PF.EffectiveDT, 
				SELECT PL.TRIPID, PL.EstFuelQTY, PF.LastUpdTS, PF.BestPrice, PF.EffectiveDT, 
					PF.FBOName, PF.GallonFrom, PF.GallonTo, PF.UnitPrice, PF.FuelVendorID,AirportID
				FROM PREFLIGHTLEG PL
				INNER JOIN PreflightFuel PF ON PL.TripID = PF.TripID
				AND PL.DepartICAOID=PF.AirportID
				WHERE PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND PL.TripID = CONVERT(BIGINT, @TripID)
				AND PL.EstFuelQTY BETWEEN PF.GallonFrom AND PF.GallonTo
				
				--ORDER BY PF.BestPrice ASC
			) PF ON PL.TripID = PF.TripID AND PF.AirportID=A.AirportID
			LEFT JOIN (SELECT FuelVendorID, VendorName FROM FuelVendor) FV ON FV.FuelVendorID = PF.FuelVendorID
			WHERE (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			ORDER BY PL.LegNUM
	END
END
 --EXEC spGetReportPRETSBestFuelVendorInformation 'SUPERVISOR_99', 1000142, ''


