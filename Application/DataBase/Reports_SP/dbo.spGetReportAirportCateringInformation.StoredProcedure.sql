IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportCateringInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportCateringInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportAirportCateringInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =============================================
-- SPC Name: spGetReportAirportCateringInformation
-- Author: SUDHAKAR J
-- Create date: 12 Jun 2012
-- Description: Get Airport Catering Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT C.IsChoice, C.CateringVendor, C.PhoneNum 
	FROM Catering C
	INNER JOIN Airport A ON C.AirportID = A.AirportID
	WHERE C.IsDeleted = 0
		AND C.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
		AND A.IcaoID = @IcaoID 
		
   ORDER BY C.CateringVendor

  -- EXEC spGetReportAirportCateringInformation 'TIM', 'KHOU'


GO


