IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSAircraftAdditionalExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSAircraftAdditionalExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRETSAircraftAdditionalExportInformation]
	 @UserCD AS VARCHAR(30)
	    ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
		,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
		,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
		,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
		,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
		,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
		,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
		,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
		,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
		,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
		,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
		,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
		,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
		,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
		,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
		,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
		,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
		,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
		,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'

AS
BEGIN
-- ============================================================================================
-- SPC Name: spGetReportPRETSAircraftAdditionalExportInformation
-- Author: AISHWARYA.M
-- Create date: 13 Sep 2012
-- Description: Get Preflight Tripsheet Report Aircraft Additional Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ===========================================================================================
SET NOCOUNT ON

-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService


	SELECT LegID = ''
		,[orig_nmbr] = PM.TripNUM
		,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[desc] = PM.TripDescription
		,[tail_nmbr] = F.TailNum
		,[phone] = P.PhoneNum
		,[type_code] = A.AircraftCD
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] = F.FlightPhoneIntlNum
		,[dispatchno] = PM.DispatchNUM
		,[infodesc] = FPD.FleetDescription
		,[infovalue] = FPD.InformationValue
		,[crewcode] = C.CrewCD
		,[releasedby] =C.LastName + ',' + C.FirstName + ' ' + C.MiddleInitial
		,[verifynmbr] = PM.VerifyNUM
		FROM (SELECT DISTINCT TRIPID FROM @tblTripInfo) M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		--INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN Fleet F ON PM.FleetID = F.FleetID
		INNER JOIN FleetProfileDefinition FPD ON PM.FleetID = FPD.FleetID
		LEFT OUTER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
		LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID
		LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
		
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		--AND PM.TripID = @TripID
		--ORDER BY PL.LegID
		
END
--EXEC spGetReportPRETSAircraftAdditionalExportInformation 'ELIZA_9', '3820', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''




GO


