IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationAPDXpmExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXpmExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





  
  
CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXpmExport]  
    @UserCD            AS VARCHAR(30)        
   ,@TripNUM           AS VARCHAR(300) = ''  --      PreflightMain.TripNUM    
   ,@LegNUM            AS VARCHAR(300) = ''  --      PreflightLeg.LegNUM  
   ,@PassengerCD       AS VARCHAR(300) = ''  --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)    
   ,@BeginDate         AS DATETIME = NULL    --      PreflightLeg.DepartureDTTMLocal    
   ,@EndDate           AS DATETIME = NULL    --      PreflightLeg.ArrivalDTTMLocal     
   ,@TailNUM           AS VARCHAR(300) = ''  --      Fleet.TailNUM (Use FleetID of PreflightMain)    
   ,@DepartmentCD      AS VARCHAR(300) = ''  --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)    
   ,@AuthorizationCD   AS VARCHAR(300) = ''  --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)    
   ,@CatagoryCD        AS VARCHAR(300) = ''  --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)    
   ,@ClientCD          AS VARCHAR(300) = ''  --      Client.ClientCD    
   ,@HomeBaseCD        AS VARCHAR(300) = ''  --      UserMaster.HomeBase    
   ,@RequestorCD       AS VARCHAR(300) = ''  --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)    
   ,@CrewCD            AS VARCHAR(300) = ''  --      Crew.CrewCD       
     
   ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'    
   ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'    
   ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'    
   ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'    
   ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'    
   ,@IsScheduledService      AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'     
AS  


-- ==========================================================================================  
-- SPC Name: spGetReportPRETSGeneralDeclarationAPDXpmExport  
-- Author: ABHISHEK.S  
-- Create date: 4th October 2012  
-- Description: Get TripSheetReportWriter General Declaration Export Information For Reports  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================================  
BEGIN  
   
 SET NOCOUNT ON;   
   
 -- [Start] Report Criteria --  
 DECLARE @tblTripInfo AS TABLE (  
 TripID BIGINT  
 , LegID BIGINT  
 , PassengerID BIGINT  
 )  
  
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
 
 -- [End] Report Criteria   
   DECLARE @TEMP1 TABLE (owner     VARCHAR(100),
						 orig_nmbr BIGINT,
						 tail_nmbr VARCHAR(20),
						 type_code VARCHAR(10),
						 fltno VARCHAR(12),
						 legid BIGINT,
						 leg_num BIGINT,
						 locdep VARCHAR(10),
						 depicao_id CHAR(4),
						 arricao_id CHAR(4),
						 depcity VARCHAR(100),
						 arrcity VARCHAR(100),
						 paxcode VARCHAR(5),
						 ordernum VARCHAR(5),
						 paxname VARCHAR(60),
						 ppnum VARCHAR(60),
						 ppnation CHAR(3))     
 INSERT INTO @TEMP1
 SELECT DISTINCT  
 [owner]      = FP.OwnerLesse,  
 [orig_nmbr]  = PM.TripNUM,  
 [tail_nmbr]  = F.TailNum,  
 [type_code]  = AR.AircraftCD, 
 [fltno]      = PM.FlightNUM,  
 [legid]      = PL.LegID,  
 [leg_num]    = PL.LegNUM,  
 [locdep]     = dbo.GetDateFormatByUserCD(@UserCD,PL.DepartureDTTMLocal) ,  
 [depicao_id] = A.IcaoID,  
 [arricao_id] = B.IcaoID,  
 [depcity]    = RTRIM(ISNULL(A.CityName, '')+ ', ' + ISNULL(A.CountryName, '')),  
 [arrcity]    = RTRIM(ISNULL(B.CityName, '')+ ', ' + ISNULL(B.CountryName, '')),  
 [paxcode]    = PR.PassengerRequestorCD,   
 [ordernum]   = (CASE(PR.PassengerRequestorCD) WHEN '' THEN '99' ELSE CONVERT(VARCHAR(10),PP.OrderNUM) END),    
 [paxname]    = RTRIM(ISNULL(PR.LastName,'') + ', '+ ISNULL(PR.FirstName,'') + ' '+ ISNULL(PR.MiddleInitial,'')) ,
 --[ppnum]      = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')), 
 [ppnum]      = ISNULL(CPP.PassportNum,''), 
 [ppnation]   = ISNULL(C1.CountryCD ,C.CountryCD ) 
   
 FROM @tblTripInfo T  
       INNER JOIN PREFLIGHTMAIN PM ON T.TripID=PM.TripID  
    INNER JOIN PreflightLeg PL  
            ON T.LegID = PL.LegID  
    INNER JOIN Fleet F  
            ON PM.FleetID = F.FleetID  
    LEFT JOIN FleetPair FP  
            ON F.FleetID = FP.FleetID  
    LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS A  
            ON A.AirportID = PL.DepartICAOID  
    LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS B  
            ON B.AirportID = PL.ArriveICAOID        
    LEFT JOIN PreflightPassengerList PP  
            ON PL.LegID = PP.LegID      
    LEFT JOIN Passenger PR  
            ON PP.PassengerID = PR.PassengerRequestorID   
    LEFT JOIN CrewPassengerPassport CPP              
            ON PP.PassportID = CPP.PassportID                       
    LEFT JOIN Country C1  
            ON CPP.CountryID = C1.CountryID 
    LEFT JOIN Country C  
            ON PR.CountryID = C.CountryID 
    LEFT JOIN Aircraft AR
			ON F.AircraftID = AR.AircraftID             
WHERE PM.IsDeleted = 0  
ORDER BY ordernum



DECLARE @COUNT INT,@leg_num BIGINT=1;   


DECLARE @crLegNum BIGINT,@crorig_nmbr BIGINT;

DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR SELECT DISTINCT orig_nmbr,leg_num FROM @TEMP1
OPEN C1

FETCH NEXT FROM C1 INTO @crorig_nmbr,@crLegNum 

WHILE @@FETCH_STATUS = 0
BEGIN

--WHILE @leg_num<=(SELECT  COUNT(DISTINCT leg_num) FROM @TEMP1 WHERE orig_nmbr=@crorig_nmbr AND leg_num=@crLegNum)
--BEGIN 
SELECT @COUNT=COUNT(*) FROM @TEMP1  WHERE orig_nmbr=@crorig_nmbr AND leg_num=@crLegNum
         
  
WHILE @COUNT<25  
BEGIN  
SET @COUNT=@COUNT+1  
END  
  
IF(@COUNT%25)=0   
 SET @COUNT=@COUNT  
ELSE IF(@COUNT%25)=1  
SET @COUNT=@COUNT+24  
ELSE IF(@COUNT%25)=2  
SET @COUNT=@COUNT+23  
ELSE IF(@COUNT%25)=3  
SET @COUNT=@COUNT+22  
ELSE IF(@COUNT%25)=4  
SET @COUNT=@COUNT+21  
ELSE IF(@COUNT%25)=5  
SET @COUNT=@COUNT+20  
ELSE IF(@COUNT%25)=6  
SET @COUNT=@COUNT+19  
ELSE IF(@COUNT%25)=7  
SET @COUNT=@COUNT+18  
ELSE IF(@COUNT%25)=8  
SET @COUNT=@COUNT+17  
ELSE IF(@COUNT%25)=9  
SET @COUNT=@COUNT+16  
ELSE IF(@COUNT%25)=10  
SET @COUNT=@COUNT+15  
ELSE IF(@COUNT%25)=11  
SET @COUNT=@COUNT+14  
ELSE IF(@COUNT%25)=12  
SET @COUNT=@COUNT+13  
ELSE IF(@COUNT%25)=13  
SET @COUNT=@COUNT+12  
ELSE IF(@COUNT%25)=14  
SET @COUNT=@COUNT+11  
ELSE IF(@COUNT%25)=15  
SET @COUNT=@COUNT+10  
ELSE IF(@COUNT%25)=16  
SET @COUNT=@COUNT+9  
ELSE IF(@COUNT%25)=17  
SET @COUNT=@COUNT+8  
ELSE IF(@COUNT%25)=18  
SET @COUNT=@COUNT+7  
ELSE IF(@COUNT%25)=19  
SET @COUNT=@COUNT+6  
ELSE IF(@COUNT%25)=20  
SET @COUNT=@COUNT+5  
ELSE IF(@COUNT%25)=21  
SET @COUNT=@COUNT+4  
ELSE IF(@COUNT%25)=22  
SET @COUNT=@COUNT+3  
ELSE IF(@COUNT%25)=23  
SET @COUNT=@COUNT+2  
ELSE IF(@COUNT%25)=24  
SET @COUNT=@COUNT+1  
  
DECLARE @COUNT1 INT;  
SELECT @COUNT1=COUNT(*) FROM @TEMP1 WHERE orig_nmbr=@crorig_nmbr AND leg_num=@crLegNum  
  
 
  
WHILE  @COUNT1<@COUNT  
BEGIN   
 INSERT INTO @TEMP1(leg_num,orig_nmbr,tail_nmbr,type_code,fltno,legid,locdep,depicao_id,arricao_id,depcity,arrcity,ordernum,owner) 
              VALUES(@leg_num,
                     (SELECT DISTINCT orig_nmbr FROM @TEMP1 WHERE leg_num=@crLegNum AND orig_nmbr=@crorig_nmbr AND orig_nmbr IS NOT NULL),
                     (SELECT DISTINCT tail_nmbr FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND tail_nmbr IS NOT NULL),
                     (SELECT DISTINCT type_code FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND type_code IS NOT NULL),
                     (SELECT DISTINCT fltno FROM @TEMP1 WHERE leg_num=@crLegNum AND orig_nmbr=@crorig_nmbr  AND fltno IS NOT NULL),
                     (SELECT DISTINCT legid FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND legid IS NOT NULL),
                     (SELECT DISTINCT locdep FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND locdep IS NOT NULL),
                     (SELECT DISTINCT depicao_id FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND depicao_id IS NOT NULL),
                     (SELECT DISTINCT arricao_id FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND arricao_id IS NOT NULL),
                     (SELECT DISTINCT depcity FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND depcity IS NOT NULL),
                     (SELECT DISTINCT arrcity FROM @TEMP1 WHERE leg_num=@crLegNum  AND orig_nmbr=@crorig_nmbr AND arrcity IS NOT NULL),
                     '99',
                     (SELECT DISTINCT owner FROM @TEMP1 WHERE leg_num=@leg_num AND owner IS NOT NULL));  
 SET @COUNT1 = @COUNT1 +1    
END 
SET @leg_num=@leg_num+1
--END

--SET @leg_num=1
FETCH NEXT FROM C1 INTO  @crorig_nmbr,@crLegNum
END
CLOSE C1;
DEALLOCATE C1;


--SET @leg_num=1
--UPDATE @TEMP1 SET owner=owner,orig_nmbr=orig_nmbr,tail_nmbr=tail_nmbr,type_code=type_code,fltno=fltno,legid=legid,leg_num=leg_num,
--                  locdep=locdep,depicao_id=depicao_id,arricao_id=arricao_id,depcity=depcity,arrcity=arrcity,ordernum=ordernum
--               WHERE leg_num=
						
						 
SELECT * FROM @TEMP1 ORDER BY orig_nmbr,leg_num,ordernum 
END  
  --     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                  
--   AND CONVERT(VARCHAR,PL.LegID) IN (@LegID)  
  
  
-- EXEC spGetReportPRETSGeneralDeclarationAPDXpmExport 'supervisor_99','1992','1'  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  




GO


