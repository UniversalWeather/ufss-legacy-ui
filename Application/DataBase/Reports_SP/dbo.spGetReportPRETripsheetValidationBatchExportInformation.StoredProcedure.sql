IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripsheetValidationBatchExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripsheetValidationBatchExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETripsheetValidationBatchExportInformation]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@TripNum AS VARCHAR(300) = '' -- [Optional], Comma delimited string with mutiple values
)
AS

BEGIN
SET NOCOUNT ON

SELECT DISTINCT
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[TripNo] = PM.TripNUM
,[TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD)
,[Desc] = PM.TripDescription
,[Requestor] =ISNULL(P.LastName,'') +','+ ISNULL(P.FirstName,'') +' '+ ISNULL(P.MiddleInitial,'')
,[ContactNo] = P.PhoneNum
,[TailNumber] = F.TailNum
,[Type] = AC.AircraftCD
,[Exceptions] = PT.Exception
,[EstDepartureDate] = MIN(PL.DepartureDTTMLocal)
,[EstArrivalDate] = MAX(PL.ArrivalDTTMLocal)
FROM Fleet F
INNER JOIN PreflightMain PM ON PM.FleetID = F.FleetID AND PM.IsDeleted = 0
INNER JOIN PreflightLeg PL ON PL.TripID=PM.TripID AND PL.IsDeleted=0
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
INNER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID	
INNER JOIN (SELECT DISTINCT PT.TripID,Exception=(
 SELECT (CASE WHEN LEFT(ExceptionDescription,3)='Leg' AND Rnk=1 THEN 'LEGS ERROR SECTION: '
              WHEN LEFT(ExceptionDescription,4)='Crew'AND Rnk=1  THEN 'CREW ERROR SECTION: '
              WHEN LEFT(ExceptionDescription,3)='Pax' AND Rnk=1  THEN ' PAX ERROR SECTION: ' ELSE '' END) + ExceptionDescription +' ' 
						FROM 
						(
						 SELECT DISTINCT TripID
										 ,ExceptionDescription
										 ,TripExceptionID
										,ROW_NUMBER()OVER(PARTITION BY TripID,LEFT(ExceptionDescription,3) ORDER BY TripID,TripExceptionID) Rnk FROM PreflightTripException PTE
									   )TT
						 WHERE TT.TripID=PT.TripID
						 ORDER BY TripExceptionID
						 FOR XML PATH('')
						 , root('MyString'), type ).value('/MyString[1]','varchar(max)')FROM PreflightTripException PT 
			)PT ON PT.TripID=PM.TripID			
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
AND CONVERT(DATE,PM.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND (PM.TripNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '')
GROUP BY PM.TripID,PM.TripNUM,P.LastName,P.FirstName,P.MiddleInitial,P.PhoneNum,F.TailNum,AC.AircraftCD,PT.Exception,PM.TripDescription
END

GO


