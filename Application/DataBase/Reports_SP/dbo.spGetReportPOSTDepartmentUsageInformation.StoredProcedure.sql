IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentUsageInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentUsageInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentUsageInformation]
	(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@DepartmentGroupID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentUsageInformation
-- Author:  A.Akhila
-- Create date: 17 Aug 2012
-- Description: Get Department Usage for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
-----------------------------TailNum and Fleet Group Filteration----------------------
		DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
		DECLARE @TempFleetID TABLE 
		( 
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(10)
		)
		 
		IF @TailNUM <> ''
		BEGIN
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum 
		FROM Fleet F
		WHERE F.CustomerID = @CUSTOMER
		AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
		END
		IF @FleetGroupCD <> ''
		BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
		FROM Fleet F 
		LEFT OUTER JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
		AND F.CustomerID = @CUSTOMER 
		END
		ELSE IF @TailNUM = '' AND @FleetGroupCD = ''
		BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
		FROM Fleet F 
		WHERE F.CustomerID = @CUSTOMER 
		END
		 
-----------------------------TailNum and Fleet Group Filteration----------------------
		
-----------------------------Department Group Filteration----------------------
DECLARE @TempDepartmentID TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	DEPARTMENTID BIGINT
	)
IF @DepartmentGroupID <> '' 
	BEGIN 
	INSERT INTO @TempDepartmentID
	SELECT DISTINCT D.DepartmentID
	FROM Department D 
		LEFT OUTER JOIN DepartmentGroupOrder DGO ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID
		LEFT OUTER JOIN DepartmentGroup DG ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID
	WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, ',')) 
		AND D.CustomerID = @CUSTOMER 
	END
-----------------------------Department Group Filteration----------------------


DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt=1

CREATE TABLE #DEPARTINFO
(
	[DEPARTMENTID] BIGINT
	,[DateRange] NVARCHAR(100)
	,[Department] NVARCHAR(30)
	,[DepartmentD] NVARCHAR(25)
	,[Authorization] NVARCHAR(30)
	,[AuthorizationD] NVARCHAR(25)
	,[TailNumber] VARCHAR(9)
	,[AIRCRAFT] NVARCHAR(10)
	,[LOG] BIGINT
	,[LEG] BIGINT
	,[NoofLegs] BIGINT
	,[BlockHours] NUMERIC(6,3)
	,[FlightHours] NUMERIC(6,3)
	,[AvgBlkHrsLeg] NVARCHAR(10)
	,[AvgFltHrsLeg] NVARCHAR(10)
	,[NauticalMiles] NUMERIC(5,0)
	,[AvgMilesLeg] NVARCHAR(10)
	,[NoofPax] NUMERIC(4,0)
	,[TenToMin] BIGINT
	,[Fleet] BIGINT
)

INSERT INTO #DEPARTINFO
SELECT DISTINCT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
	,[DepartmentD] =D.DepartmentName            
	,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
	,[AuthorizationD] = DA.DeptAuthDescription
	,[TailNumber] =ISNULL(F.TailNum,'')
	,[AIRCRAFT] = AC.AircraftCD
	,[LOG] = POM.POLogID
	,[LEG] = POL.POLegID
	,[NoofLegs]= COUNT(POL.LegNUM)
	,[BlockHours] = ISNULL(ROUND(POL.BlockHours,1),0)
	,[FlightHours] = ISNULL(ROUND(POL.FlightHours,1),0)
	,[AvgBlkHrsLeg]= NULL
	,[AvgFltHrsLeg] = NULL
	,[NauticalMiles] = POL.Distance
	,[AvgMilesLeg] = NULL
	,[NoofPax] = POL.PassengerTotal
	,[TenToMin] = @TenToMin
	,[Fleet] = F.FLEETID
FROM PostflightLeg POL
	LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
	LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID AND D.IsDeleted=0 AND D.IsInActive=0
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
					AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
	LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
	LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	INNER JOIN ( SELECT DISTINCT DEPARTMENTID FROM @TempDepartmentID ) D1 ON D1.DEPARTMENTID = D.DepartmentID
WHERE --ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 
	--AND 
	POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND POL.IsDeleted=0
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
GROUP BY  D.DEPARTMENTID, D.DepartmentCD, D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription,  AC.AircraftCD ,
	F.TailNum, POM.POLogID, POL.POLegID, POL.LEGNUM, POL.BlockHours, POL.FlightHours, POL.Distance, POL.PassengerTotal, F.FleetID
	
IF @DepartmentGroupID=''
BEGIN
INSERT INTO #DEPARTINFO
SELECT DISTINCT
[DEPARTMENTID] = D.DEPARTMENTID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
,[DepartmentD] =D.DepartmentName            
,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
,[AuthorizationD] = DA.DeptAuthDescription
,[TailNumber] =ISNULL(F.TailNum,'')
,[AIRCRAFT] = AC.AircraftCD
,[LOG] = POM.POLogID
,[LEG] = POL.POLegID
,[NoofLegs]= COUNT(POL.LegNUM)
,[BlockHours] = ISNULL(ROUND(POL.BlockHours,1),0)
,[FlightHours] = ISNULL(ROUND(POL.FlightHours,1),0)
,[AvgBlkHrsLeg]= NULL
,[AvgFltHrsLeg] = NULL
,[NauticalMiles] = POL.Distance
,[AvgMilesLeg] = NULL
,[NoofPax] = POL.PassengerTotal
,[TenToMin] = @TenToMin
,[Fleet] = F.FLEETID
FROM PostflightLeg POL
LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID AND D.IsDeleted=0 AND D.IsInActive=0
LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID

WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND POL.IsDeleted=0
AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
GROUP BY  D.DEPARTMENTID, D.DepartmentCD, D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription,  AC.AircraftCD ,
F.TailNum, POM.POLogID, POL.POLegID, POL.LEGNUM, POL.BlockHours, POL.FlightHours, POL.Distance, POL.PassengerTotal, F.FleetID	
END

CREATE TABLE #DEPARTEMPTY
(
	[DEPARTMENTID] BIGINT
	,[DateRange] NVARCHAR(100)
	,[Department] NVARCHAR(30)
	,[DepartmentD] NVARCHAR(25)
	,[Authorization] NVARCHAR(30)
	,[AuthorizationD] NVARCHAR(25)
	,[TailNumber] VARCHAR(9)
	,[AIRCRAFT] NVARCHAR(10)
	,[LOG] BIGINT
	,[LEG] BIGINT
	,[NoofLegs] BIGINT
	,[BlockHours] NUMERIC(6,3)
	,[FlightHours] NUMERIC(6,3)
	,[AvgBlkHrsLeg] NVARCHAR(10)
	,[AvgFltHrsLeg] NVARCHAR(10)
	,[NauticalMiles] NUMERIC(5,0)
	,[AvgMilesLeg] NVARCHAR(10)
	,[NoofPax] NUMERIC(4,0)
	,[TenToMin] NUMERIC(1,0)
	,[Fleet] BIGINT
)
INSERT INTO #DEPARTEMPTY
SELECT DISTINCT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
	,[DepartmentD] =D.DepartmentName            
	,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
	,[AuthorizationD] = DA.DeptAuthDescription            
	,[TailNumber] =ISNULL(NULL,'')
	,[AIRCRAFT] = NULL
	,[LOG] = NULL
	,[LEG] = NULL
	,[NoofLegs]= NULL
	,[BlockHours] = ISNULL(NULL,0)
	,[FlightHours] = ISNULL(NULL,0)
	,[AvgBlkHrsLeg]= ISNULL(NULL,0)
	,[AvgFltHrsLeg] = ISNULL(NULL,0)
	,[NauticalMiles] = ISNULL(NULL,0)
	,[AvgMilesLeg] = ISNULL(NULL,0)
	,[NoofPax] = ISNULL(NULL,0)
	,[TenToMin] = @TenToMin
	,[Fleet] = NULL
FROM DEPARTMENT D
	INNER JOIN PostflightMain PM ON PM.DepartmentID = PM.DepartmentID  AND PM.IsDeleted=0
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID
	INNER JOIN ( SELECT DISTINCT DEPARTMENTID FROM @TempDepartmentID ) D1 ON D1.DEPARTMENTID = D.DepartmentID
WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsDeleted=0 AND D.IsInActive=0
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (PM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
	DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
IF @DepartmentGroupID=''
	BEGIN
	INSERT INTO #DEPARTEMPTY
SELECT DISTINCT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
	,[DepartmentD] =D.DepartmentName            
	,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
	,[AuthorizationD] = DA.DeptAuthDescription            
	,[TailNumber] =ISNULL(NULL,'')
	,[AIRCRAFT] = NULL
	,[LOG] = NULL
	,[LEG] = NULL
	,[NoofLegs]= NULL
	,[BlockHours] = ISNULL(NULL,0)
	,[FlightHours] = ISNULL(NULL,0)
	,[AvgBlkHrsLeg]= ISNULL(NULL,0)
	,[AvgFltHrsLeg] = ISNULL(NULL,0)
	,[NauticalMiles] = ISNULL(NULL,0)
	,[AvgMilesLeg] = ISNULL(NULL,0)
	,[NoofPax] = ISNULL(NULL,0)
	,[TenToMin] = @TenToMin
	,[Fleet] = NULL
FROM DEPARTMENT D
	INNER JOIN PostflightMain PM ON PM.DepartmentID = PM.DepartmentID AND PM.IsDeleted=0 
	INNER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID
WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsDeleted=0 AND D.IsInActive=0
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (PM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
	DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
	END


IF @IsZeroSuppressActivityDeptRpt=0
BEGIN
	SELECT * FROM #DEPARTINFO
UNION ALL
	SELECT * FROM #DEPARTEMPTY
	ORDER BY [Department], [Authorization], [AIRCRAFT], [TailNumber]
END
ELSE
BEGIN
	SELECT * FROM #DEPARTINFO
	ORDER BY [Department], [Authorization], [AIRCRAFT], [TailNumber]
END

IF OBJECT_ID('tempdb..#DEPARTINFO') IS NOT NULL
DROP TABLE #DEPARTINFO 	

IF OBJECT_ID('tempdb..#DEPARTEMPTY') IS NOT NULL
DROP TABLE #DEPARTEMPTY 	

--EXEC spGetReportPOSTDepartmentUsageInformation 'SUPERVISOR_99','2009-1-1','2009-1-1', '','', '', '', '', 0


GO


