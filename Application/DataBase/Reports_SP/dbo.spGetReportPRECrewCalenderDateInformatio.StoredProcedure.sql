IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECrewCalenderDateInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECrewCalenderDateInformation]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE Procedure [dbo].[spGetReportPRECrewCalenderDateInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftTypeCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TripNUM AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@HomeBase AS BIT = 0--Boolean: 1 indicates to fetch only for user HomeBase
		--@SortBy AS CHAR(4) = 'CREW' -- 'CREW' FOR Crew Calendar Crew / 'DATE' for Crew Calendar Date
AS
-- ===============================================================================
-- SPC Name: spGetReportPRECrewCalenderDateInformation
-- Author: SUDHAKAR J
-- Create date: 04 Jul 2012
-- Description: Get Crew Calender information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON





DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityCrew BIT  

SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-------------------------------TailNum and Fleet Group Filteration----------------------
-----------------------------Crew and Crew Group Filteration----------------------

CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
END
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
--DROP TABLE #TempCrewDate
--DROP TABLE #TempCrewCalendar
	-- Populate the complete Crew List for each Date in the given range
	CREATE TABLE #TempCrewDate (
		RowID INT identity(1, 1) NOT NULL
		,CrewID BIGINT
		,CrewCD VARCHAR(200)
		,DeptDateOrg DATETIME	
	)
	
	CREATE TABLE #TempCrewCalendar (
		RowID INT identity(1, 1) NOT NULL
		,CrewID BIGINT,CrewCD VARCHAR(200),DeptDateOrg DATETIME,AircraftID BIGINT,FleetID BIGINT,TripID BIGINT
		,LegID BIGINT,LegNum BIGINT,Depart DATETIME,DepartICAOID BIGINT,Arrive DATETIME,ArriveICAOID BIGINT
		,NextLocalDTTM DATETIME, DutyType CHAR(2), FlightCategoryID BIGINT,
		TripNUM BIGINT, TripDescription VARCHAR(40),AircraftType VARCHAR(10),TailNum VARCHAR(6),CAT VARCHAR(200),PIC VARCHAR(200),SIC VARCHAR(200),
		AddlCrew VARCHAR(200),ETE NUMERIC(7,3),Alert BIT
	)
	DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM
	---------------------------NEW-----------------------
				
	  CREATE TABLE #TblDates  (AllDates DATE);

      WITH dates(DATEPARAM)
           AS (SELECT @TEMPTODAY AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO #TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
      

				
				INSERT INTO #TempCrewDate (CrewID,CrewCD,DeptDateOrg) 
				SELECT DISTINCT  C.CREWID,C.CrewCD + '  ' + ISNULL(C.LastName,'') + ',' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,''),AllDates
				FROM  Crew C
				INNER JOIN #TempCrewID T ON C.CrewID=T.CrewID
				 CROSS JOIN #TblDates 
				 WHERE C.IsStatus = 1 
				    AND C.IsDeleted = 0
					AND C.CustomerID = @CUSTOMERID
     --------------------------------------------
	-----------------------------NEW--------------------
--------------------ALT
	---------------------------------------------------------------------

				INSERT INTO #TempCrewCalendar ( CrewID,CrewCD, DeptDateOrg, AircraftID, FleetID, 
					TripID, LegID, LegNum, Depart, DepartICAOID, Arrive, ArriveICAOID, 
					NextLocalDTTM, DutyType, FlightCategoryID, TripNUM, TripDescription,AircraftType,TailNum,CAT,PIC,SIC,AddlCrew,ETE,Alert)
				
				SELECT TCD.CrewID,TCD.CrewCD + '  ' + ISNULL(TCD.LastName,'') + ',' + ISNULL(TCD.FirstName,'') + ' ' + ISNULL(TCD.MiddleInitial,'') ,PL.DepartureDTTMLocal, PM.AircraftID, PM.FleetID, PL.TripID, PL.LegID, 
					PL.LegNum, PL.DepartureDTTMLocal, PL.DepartICAOID, PL.ArrivalDTTMLocal, 
					PL.ArriveICAOID, PL.NextLocalDTTM, 
					[DutyTYPE] = 	CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END
									ELSE PL.DutyTYPE END,
					PL.FlightCategoryID, PM.TripNUM, PM.TripDescription,AT.AircraftCD,F.TailNum,FC.FlightCatagoryCD,
					SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1),SUBSTRING(ADDL.CrewCodes,1,LEN(ADDL.CrewCodes)-1)
					,PL.ElapseTM,TCD.CheckList
					FROM PreflightMain PM 
					INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON PM.FleetID = F1.FleetID
					INNER JOIN / PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
					INNER JOIN (
						SELECT LegID , CrewID FROM PreflightCrewList
					) PC ON PL.LegID = PC.LegID
					INNER JOIN Crew TCD ON PC.CrewID = TCD.CrewID
						LEFT OUTER JOIN FlightCatagory FC 
				ON PL.FlightCategoryID = FC.FlightCategoryID
					INNER JOIN ( SELECT DISTINCT CrewID FROM #TempCrewDate) Temp ON TCD.CrewID = Temp.CrewID				
					INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.AircraftID 
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
					  LEFT OUTER JOIN(
			    SELECT AircraftID,AircraftCD FROM Aircraft)AT ON AT.AircraftID=F.AircraftID
			      LEFT OUTER JOIN (
				SELECT DISTINCT PC2.LegID, CrewCodes = (
				SELECT C.CrewCD + ',' 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
				ORDER BY C.CrewCD 
				FOR XML PATH('')
				 )
				FROM PreflightCrewList PC2		
			) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
					ORDER BY C1.CrewCD 
					FOR XML PATH('')	
				 )
				FROM PreflightCrewList PC3 	
			) SIC ON PL.LegID = SIC.LegID
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE NOT IN ('P','S')
					ORDER BY C1.CrewCD 
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) ADDL ON PL.LegID = ADDL.LegID  
				WHERE PM.CustomerID = @CUSTOMERID 
				AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE, @DATEFROM) AND CONVERT(DATE, @DATETO)
				AND ISNULL(PM.RecordType,'') <> 'M'
				AND ISNULL(PL.DutyTYPE,'') <> 'D'
				AND PM.RecordType ='T'
				AND PM.TripStatus='T'
				AND  ( TCD.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR @HomeBase = 0)
				AND  ( PM.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '' )
	            AND ( AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftTypeCD, ',')) OR @AircraftTypeCD = '' )
				--AND (PL.DutyTYPE IS NULL OR PL.DutyTYPE='F')

			
		
		
	--		--PRINT @SQLSCRIPT

---------------------ALT


-----------------------Cursor for inserting extra rows if the trip start with leg>1---
DECLARE @PrevtTripId BIGINT,@CrCrewCD VARCHAR(200),@CrArr DATETIME,@PrevCrewID BIGINT,@TripNUMRef BIGINT

	DECLARE @crCrewID BIGINT, @crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT;
	DECLARE @crTripID BIGINT, @crLegID BIGINT, @crLegNum BIGINT, @crDepart DATETIME;
	DECLARE @crDepartICAOID BIGINT, @crArrive DATETIME, @crArriveICAOID BIGINT;
	DECLARE @crNextLocalDTTM DATETIME, @crDutyType CHAR(2), @crTripNUM BIGINT,@CrTailNum VARCHAR(6),@CrAircraftType VARCHAR(10);
	DECLARE @Loopdate DATETIME,@crFlightCategoryID BIGINT,@crTripDescription VARCHAR(40),@crRecordType  CHAR(1);	
	



DECLARE @DATE1 INT,@Count INT=0,@activedate DATETIME,@CrArrivalDTTMLocal1 DATETIME,@Cricao_id BIGINT,@PrevtTLegId BIGINT;
DECLARE C1 CURSOR FOR SELECT DISTINCT TRAIN.* FROM
								(SELECT  TCD.CrewID,TCD.CrewCD + '  ' + ISNULL(TCD.LastName,'') + ',' + ISNULL(TCD.FirstName,'') + ' ' + ISNULL(TCD.MiddleInitial,'') CrewCD, PL.DepartureDTTMLocal,AT.AircraftID,F.FleetID,PM.TripID,PL.LegID,PL.LegNUM,PL.DepartureDTTMLocal Departure,PL.DepartICAOID,
								PL.ArrivalDTTMLocal,PL.ArriveICAOID,PL.NextLocalDTTM,PL.DutyTYPE,PL.FlightCategoryID,PM.TripNUM,PM.TripDescription,AT.AircraftCD,F.TailNum,PM.RecordType 
								 FROM PreflightMain PM 
								-- INNER JOIN (SELECT DISTINCT FLEETID FROM #TempCrewCalendar ) F1 ON PM.FleetID = F1.FleetID
								 INNER JOIN PreflightLeg PL on PM.TripID=PL.TripID AND PL.IsDeleted=0
								 INNER JOIN Fleet F ON F.FleetID=PM.FleetID
								 INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON f.FleetID = F1.FleetID
								INNER JOIN (SELECT LegID , CrewID FROM PreflightCrewList) PC ON PL.LegID = PC.LegID
								INNER JOIN  Crew TCD ON PC.CrewID = TCD.CrewID
								LEFT OUTER JOIN(SELECT AircraftID,AircraftCD FROM Aircraft)AT ON AT.AircraftID=F.AircraftID
								OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
								 WHERE   PM.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
								 
								AND (	
											(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
										  OR
											(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
										  OR
											(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
									)
								--AND NOT EXISTS(SELECT DEPART,ARRIVE FROM #TempCrewCalendar) 	
									)Train
									
					ORDER BY TripID,CrewID,legnum
OPEN C1

FETCH NEXT FROM C1 INTO @crCrewID,@CrCrewCD,@crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
                     @crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType, @crFlightCategoryID, @crTripNUM, @crTripDescription,@CrAircraftType,@CrTailNum,@crRecordType
WHILE @@FETCH_STATUS = 0
BEGIN
SET @DATE1=(SELECT DATEDIFF(DAY,@crDepart,@crArrive))
SET @activedate=@crDepart
SET @TripNUMRef=( SELECT DISTINCT TRIPNUM FROM #TempCrewCalendar WHERE TripID=@crTripID)  


IF((@PrevtTLegId=@crLegID  OR @PrevtTripId <> @crTripID OR @PrevtTLegId IS NULL ) AND @crLegNum > 1  AND @TripNUMRef=@crTripNUM)

BEGIN


SET @CrArr=(SELECT PL.ArrivalDTTMLocal FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=0
          WHERE PM.TripNUM=@crTripNUM
            AND PM.IsDeleted=0
            AND PM.TripID=@crTripID
            AND PL.LegNUM=@crLegNum-1)



SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrArr, 101) + ' 00:00', 101));


WHILE(CONVERT(DATE, @Loopdate) < CONVERT(DATE, @crArrive))
		BEGIN
		

INSERT INTO #TempCrewCalendar(CrewID,TailNum,CrewCD,DeptDateOrg, AircraftID, FleetID, TripID, LegID, LegNum, Depart, 
                     DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, FlightCategoryID, TripNUM, TripDescription,AircraftType)
			VALUES (@crCrewID,@CrTailNum,@CrCrewCD,@Loopdate, @crAircraftID, @crFleetID, 
					@crTripID,@crLegID,@crLegNum, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:00', 101), 
					@crArriveICAOID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101), 
					@crArriveICAOID, NULL, 'R', @crFlightCategoryID,@crTripNUM,@crTripDescription,@CrAircraftType )
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate);
		END


INSERT INTO #TempCrewCalendar(CrewID,TailNum,CrewCD,DeptDateOrg, AircraftID, FleetID, TripID, LegID, LegNum, Depart, 
                     DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, FlightCategoryID, TripNUM, TripDescription,AircraftType)
            VALUES(@crCrewID,@CrTailNum,@CrCrewCD,@crDeptDateOrg,@crAircraftID,@crFleetID,@crTripID,@crLegID,@crLegNum,
                          CONVERT(DATETIME, CONVERT(VARCHAR(10), @crDepart, 101) + ' 00:00', 101), @crDepartICAOID,@crDepart,
                          @crDepartICAOID,@crNextLocalDTTM,'R',@crFlightCategoryID,@crTripNUM,@crTripDescription,@CrAircraftType)

END

IF @crRecordType='C'-- @crDutyType IS NOT NULL AND @crDutyType <> 'F' 

BEGIN

------------------------------------

    WHILE(@Count<=@DATE1)
	BEGIN

	 IF @Count=@DATE1 

	 BEGIN SET @CrArrivalDTTMLocal1=@crArrive END
	 ELSE BEGIN SET @CrArrivalDTTMLocal1=CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 23:59') END
 
	INSERT INTO #TempCrewCalendar(CrewID,CrewCD,TripID,LegID,TripNUM,DeptDateOrg,FleetID,Depart,DepartICAOID,Arrive,ArriveICAOID,DutyType,TripDescription,ETE) 
	VALUES(@crCrewID,@CrCrewCD,@crTripID,@crLegID,@crTripNUM,@activedate,@crFleetID,@crDepart,@crDepartICAOID,@CrArrivalDTTMLocal1,@crArriveICAOID,@crDutyType,@crTripDescription,0)
	 

	 SET @crDepart=CONVERT(DATETIME, CONVERT(VARCHAR(10),@crDepart, 101) + ' 00:00')

	 SET @Count=@Count+1 
	  SET @activedate=@activedate+1
	 END
	  SET @Count=0
END
----------------------------------



SET @PrevtTripId=@crTripID
SET @PrevtTLegId=@crLegID
SET @PrevCrewID=@crCrewID
FETCH NEXT FROM C1 INTO @crCrewID,@CrCrewCD,@crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
                     @crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType, @crFlightCategoryID, @crTripNUM, @crTripDescription,@CrAircraftType,@CrTailNum,@crRecordType
END
CLOSE C1;
DEALLOCATE C1;


--DELETE FROM #TempCrewCalendar WHERE (DutyType='TR' OR DutyType='VA' ) AND LegNum IS NOT NULL 

---------------------------end cursor--------------------------------------
	-----------------------------------------------------------
	--Populate IDLE TIME records
		--Insert 1: IdleDepartDateTime = CurrentLeg.ArrivalDateTime and IdleArrivalDateTime = CurrentLeg.ArrivalDate 23:59
		--LoopDate = CurrentLeg.ArrivalDate ++
		--LOOP WHILE LoopDate < NextLocDate
		--	Insert 2: IdleDepartDateTime = LoopDate 00:01 and IdleArrivalDateTime = LoopDate 23:59
		--	LoopDate ++
		--END LOOP
		--Insert 3: IdleDepartDateTime = NextLocDate 00:01 and IdleArrivalDateTime = NextLocDateTime 




	DECLARE curCrewCal CURSOR FOR
		SELECT CrewID,CrewCD,DeptDateOrg, AircraftID, FleetID, TripID, LegID, LegNum, 
			Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, TripNUM,TripDescription,TailNum
		FROM #TempCrewCalendar 
		WHERE CONVERT(VARCHAR, Arrive, 101) < CONVERT(VARCHAR, NextLocalDTTM, 101) 
		ORDER BY CrewID, DeptDateOrg;

	OPEN curCrewCal;
	FETCH NEXT FROM curCrewCal INTO @crCrewID,@crCrewCD,@crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
		@crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType, @crTripNUM,@crTripDescription,@CrTailNum;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @Loopdate = '';
		--STEP 1:
   		INSERT INTO #TempCrewCalendar (CrewID,CrewCD,DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, TripNUM,TripDescription,TailNum ,AircraftType )
			VALUES (@crCrewID,@CrCrewCD,@crArrive, @crAircraftID, @crFleetID, 
					@crTripID, @crArrive, @crArriveICAOID,  
					CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 23:59', 101), @crArriveICAOID, NULL, 'R',
					@crTripNUM,@crTripDescription,@CrTailNum,@CrAircraftType )
		
		--STEP 2:
		SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:00', 101));
		WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @crNextLocalDTTM, 101))
		BEGIN
			--PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)
			INSERT INTO #TempCrewCalendar (CrewID, CrewCD,DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, TripNUM,TripDescription,TailNum,AircraftType)
			VALUES (@crCrewID,@CrCrewCD,@Loopdate, @crAircraftID, @crFleetID, 
					@crTripID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:00', 101), 
					@crArriveICAOID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101), 
					@crArriveICAOID, NULL, 'R', @crTripNUM,@crTripDescription,@CrTailNum,@CrAircraftType )
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate);
		END
		--STEP 3:
		INSERT INTO #TempCrewCalendar (CrewID,CrewCD,DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType, TripNUM,TripDescription,TailNum,AircraftType)
			VALUES (@crCrewID,@CrCrewCD,@Loopdate, @crAircraftID, @crFleetID, 
					@crTripID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:00', 101), 
					@crArriveICAOID, @crNextLocalDTTM, @crArriveICAOID, NULL, 'R', @crTripNUM,@crTripDescription,@CrTailNum,@CrAircraftType )
					
	   FETCH NEXT FROM curCrewCal INTO @crCrewID,@crCrewCD,@crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
		@crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType, @crTripNUM,@crTripDescription,@CrTailNum;
	END
	CLOSE curCrewCal;
	DEALLOCATE curCrewCal;



IF @SuppressActivityCrew=0
BEGIN

SELECT CrewCalendar.* FROM
(
SELECT  [CrewID]= TEMP.CrewID,
       [CREW]=TEMP.CrewCD,
       [DATE]=TEMP.DeptDateOrg ,
       [SortDate]=CCL.DeptDateOrg,
       [DeptDateOrg] = dbo.GetShortDateFormatByUserCD(@UserCD,TEMP.DeptDateOrg),
       [AircraftType] = CCL.AircraftType,
       [TailNumber] =CCL.TailNUM,
	   [FileNo] = CCL.TripNUM,
	   [Depart] = LEFT(convert(varchar, CCL.Depart, 14),5)+ ' ' + AD.IcaoID
	  ,[Arrival] = LEFT(convert(varchar, CCL.Arrive, 14),5) + ' ' +AA.IcaoID
	  ,[Dty] = CCL.DutyType
	  ,[Comment] = CCL.TripDescription
	  ,[Cat] = CCL.CAT
	  ,[PIC] = CCL.PIC
	  ,[SIC] = CCL.SIC
      ,[AddlCrew] = CCL.AddlCrew
	  ,[ETE] = CASE(CCL.DutyType) WHEN 'R' THEN 0.0 ELSE CCL.ETE END
	  ,[Alert] = ISNULL(CCL.Alert, 0)
	  ,(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange]
	  ,@TenToMin TenToMin
	  ,CCL.RowID
       
       
FROM #TempCrewCalendar CCL 
	   LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AD ON CCL.DepartICAOID = AD.AirportID
	   LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON CCL.ArriveICAOID = AA.AirportID
          
        RIGHT OUTER JOIN #TempCrewDate TEMP ON TEMP.CREWID=CCL.CREWID 
		AND CONVERT(DATE,TEMP.DeptDateOrg)=CONVERT(DATE,CCL.DeptDateOrg)  
		   WHERE (CONVERT(DATE,CCL.DeptDateOrg) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR CCL.DeptDateOrg IS NULL)
		   AND ( CCL.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '' )
		   AND ( AircraftType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftTypeCD, ',')) OR @AircraftTypeCD = '' )
)CrewCalendar

	ORDER BY CrewCalendar.[DATE], CrewCalendar.CREW,CrewCalendar.[SortDate],CrewCalendar.RowID DESC
END
ELSE
BEGIN
SELECT CrewCalendar.* FROM
(
SELECT  [CrewID]= TEMP.CrewID,
       [CREW]=TEMP.CrewCD,
       [DATE]=TEMP.DeptDateOrg ,
       [SortDate]=CCL.DeptDateOrg,
       [DeptDateOrg] = dbo.GetShortDateFormatByUserCD(@UserCD,TEMP.DeptDateOrg),
       [AircraftType] = CCL.AircraftType,
       [TailNumber] =CCL.TailNUM,
	   [FileNo] = CCL.TripNUM,
	   [Depart] = LEFT(convert(varchar, CCL.Depart, 14),5)+ ' ' + AD.IcaoID
	  ,[Arrival] = LEFT(convert(varchar, CCL.Arrive, 14),5) + ' ' +AA.IcaoID
	  ,[Dty] = CCL.DutyType
	  ,[Comment] = CCL.TripDescription
	  ,[Cat] = CCL.CAT
	  ,[PIC] = CCL.PIC
	  ,[SIC] = CCL.SIC
      ,[AddlCrew] = CCL.AddlCrew
	  ,[ETE] = CASE(CCL.DutyType) WHEN 'R' THEN 0.0 ELSE CCL.ETE END
	  ,[Alert] = ISNULL(CCL.Alert, 0)
	  ,(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange]
	  ,@TenToMin TenToMin
	  ,CCL.RowID
       
       
FROM #TempCrewCalendar CCL 
	   LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AD ON CCL.DepartICAOID = AD.AirportID
	   LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON CCL.ArriveICAOID = AA.AirportID
          
        RIGHT OUTER JOIN #TempCrewDate TEMP ON TEMP.CREWID=CCL.CREWID 
		AND CONVERT(DATE,TEMP.DeptDateOrg)=CONVERT(DATE,CCL.DeptDateOrg)  
		   WHERE CONVERT(DATE,CCL.DeptDateOrg) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)-- OR CCL.DeptDateOrg IS NULL
		   AND ( CCL.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '' )
		   AND ( AircraftType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftTypeCD, ',')) OR @AircraftTypeCD = '' )
)CrewCalendar

	ORDER BY CrewCalendar.[DATE], CrewCalendar.CREW,CrewCalendar.[SortDate],CrewCalendar.RowID DESC
END




IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID

IF OBJECT_ID('tempdb..#TempCrewID') IS NOT NULL
DROP TABLE #TempCrewID

IF OBJECT_ID('tempdb..#TempCrewDate') IS NOT NULL
DROP TABLE #TempCrewDate

IF OBJECT_ID('tempdb..#TempCrewCalendar') IS NOT NULL
DROP TABLE #TempCrewCalendar

IF OBJECT_ID('tempdb..#TblDates') IS NOT NULL
DROP TABLE #TblDates


---PRINT @SQLSCRIPT
-- EXEC spGetReportPRECrewCalenderDateInformation 'JWILLIAMS_13', '2012-08-01', '2012-08-31', '', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', 'JKM', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', '', '234', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', '', '', 'N331UV', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '234', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '', 'test', '', 0
-- EXEC spGetReportPRECrewCalenderDateInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '', '', '1', 0





GO


