IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationDetailInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
  
CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailInformation]  
  (  
  @UserCD AS VARCHAR(30), --Mandatory  
  @DATEFROM AS DATETIME, --Mandatory  
  @DATETO AS DATETIME, --Mandatory  
  @DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase  
  )  
AS  
  
-- ===============================================================================  
-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailInformation  
-- Author:  A.Akhila  
-- Create date: 31 Jul 2012  
-- Description: Get Department/Authorization Detail for REPORTS  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
  
SET NOCOUNT ON  
DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =0
  -----------------------------TailNum and Fleet Group Filteration----------------------  
  DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
  DECLARE @TempFleetID TABLE   
  (   
  ID INT NOT NULL IDENTITY (1,1),   
  FleetID BIGINT,  
  TailNum VARCHAR(10)  
  )  
     
  IF @TailNUM <> ''  
  BEGIN  
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum   
  FROM Fleet F  
  WHERE F.CustomerID = @CUSTOMER  
  AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))  
  END  
  IF @FleetGroupCD <> ''  
  BEGIN   
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
  FROM Fleet F   
  LEFT OUTER JOIN FleetGroupOrder FGO  
  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
  LEFT OUTER JOIN FleetGroup FG   
  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
  WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
  AND F.CustomerID = @CUSTOMER   
  END  
  ELSE IF @TailNUM = '' AND @FleetGroupCD = ''  
  BEGIN   
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
  FROM Fleet F   
  WHERE F.CustomerID = @CUSTOMER   
  END  
     
  -----------------------------TailNum and Fleet Group Filteration----------------------   
DECLARE @TenToMin SMALLINT = 0;  
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
  
DECLARE @DEPARTMENT TABLE (DEPARTMENTID BIGINT)  
INSERT INTO @DEPARTMENT SELECT DepartmentID FROM Department WHERE IsDeleted = 0 AND IsInActive = 0  
IF OBJECT_ID('tempdb..#DEPARTMENTINFO') IS NOT NULL  
DROP TABLE #DEPARTMENTINFO   
  
SELECT  
  [DEPARTMENTID] = ISNULL(D.DEPARTMENTID,'')  
  ,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)  
  ,[Department] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END   
  ,[DepartmentD] =D.DepartmentName   
  ,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END   
  ,[AuthorizationD] = DA.DeptAuthDescription  
  ,[TailNumber] = ISNULL(F.TailNum,'')  
  ,[Date] = CONVERT (DATE, POL.ScheduledTM)   
  ,[DispNo] = POM.DispatchNum  
  ,[LogNumber] = POM.LogNum   
  ,[POLOGID] = POM.POLogID  
  ,[POLEGID] = POL.POLegID  
  ,[Leg] = POL.LegNUM  
  ,[FlightCategory] = CASE WHEN POM.LogNum IS NULL THEN 'NO ACTIVITY' ELSE CONVERT (VARCHAR, FC.FlightCatagoryCD) +' '+ FC.FlightCatagoryDescription END  
  ,[BlockHours] = ROUND(POL.BlockHours,1)
  ,[FlightHours] = ROUND(POL.FlightHours,1)
  ,[NbrPax] = COUNT(PAX.PostflightPassengerListID)
  ,[DepIcao] = CONVERT (VARCHAR, A.IcaoID) +'$$'+ A.CityName  
  ,[ArrIcao] = CONVERT (VARCHAR, AA.IcaoID) +'$$'+ AA.CityName  
  ,[Miles] = POL.Distance  
  ,[Ess] = ISNULL(F.MaximumPassenger - ISNULL(POL.PassengerTotal,0),0)  
  ,[PaxName] = PAX.PaxNames
  
  ,[FlightPurpose] =PAX.FlightPurposeCDS --SUBSTRING(PAX.FlightPurposeCDS,1,LEN(PAX.FlightPurposeCDS)-1) 
  ,[TenToMin] = @TenToMin,  
  [Fleet] = F.FLEETID  
INTO #DEPARTMENTINFO  
 FROM PostflightLeg POL  
  LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID AND POM.IsDeleted=0  
  LEFT OUTER JOIN (  
  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger  
  FROM Fleet F   
  LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
  LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID WHERE F.IsDeleted = 0  
  ) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID  
  INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID  
  LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID  
  LEFT OUTER JOIN DepartmentAuthorization DA  
  ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0   
  AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID  
  LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID  
  LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID  
  LEFT OUTER JOIN Airport A ON A.AirportID = POL.DepartICAOID  
  LEFT OUTER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID   
 LEFT OUTER JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,PaxNames = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH('')),
                      FlightPurposeCDS = (
                       SELECT ISNULL(P1.FlightPurposeCD,'')+'$$' 
                               FROM PostflightPassenger PP1 INNER JOIN FlightPurpose P1 ON PP1.FlightPurposeID=P1.FlightPurposeID 
                               WHERE PP1.POLegID = PP2.POLegID
                             FOR XML PATH('')
                             )
                              FROM PostflightPassenger PP2
				   ) PAX ON POL.POLegID = PAX.POLegID  


 WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 AND  POL.IsDeleted=0 AND  
  POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
  AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
  AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')  
  AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')  
  AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)  
    
 GROUP BY D.DEPARTMENTID, DepartmentCD,D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription,f.TailNum, POL.ScheduledTM, POM.DispatchNum,  
 POM.LogNum, POM.POLogID, POL.LEGNUM, POL.POLegID, FC.FlightCatagoryCD, FC.FlightCatagoryDescription, BlockHours, FlightHours,  
 A.IcaoID, A.CityName, AA.IcaoID, AA.CityName, pol.Distance, f.MaximumPassenger, pol.PassengerTotal,PAX.PaxNames,
 PAX.FlightPurposeCDS, F.FleetID  
  
 IF @IsZeroSuppressActivityDeptRpt = 0
 BEGIN 
 SELECT * FROM #DEPARTMENTINFO  
  UNION ALL   
 SELECT DISTINCT  
  [DEPARTMENTID] = D.DEPARTMENTID  
  ,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)  
  ,[Department] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END   
  ,[DepartmentD] =D.DepartmentName   
  ,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END   
  ,[AuthorizationD] = DA.DeptAuthDescription  
  ,[TailNumber] = ISNULL(NULL,'')  
  ,[Date] = NULL  
  ,[DispNo] = NULL  
  ,[LogNumber] = NULL  
  ,[POLOGID] = NULL  
  ,[POLEGID] = NULL  
  ,[Leg] = NULL  
  ,[FlightCategory] = ISNULL(NULL,'NO ACTIVITY')  
  ,[BlockHours] = NULL  
  ,[FlightHours] = NULL  
  ,[NbrPax] = NULL  
  ,[DepIcao] = NULL  
  ,[ArrIcao] = NULL  
  ,[Miles] = NULL  
  ,[Ess] = NULL  
  ,[PaxName] = NULL  
  
  ,[FlightPurpose] = NULL  
  ,[TenToMin] = @TenToMin  
  ,[Fleet] = NULL  
 FROM DEPARTMENT D  
  INNER JOIN @DEPARTMENT DT ON D.DepartmentID = DT.DEPARTMENTID  
  INNER JOIN PostflightMain PM ON PM.DepartmentID = PM.DepartmentID AND PM.IsDeleted=0 
  --INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID  
  INNER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0   
   AND D.DepartmentID = DA.DepartmentID  
 WHERE D.DEPARTMENTID NOT IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTMENTINFO) AND   
  D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
  AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')  
  AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')  
  AND (PM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)  
  
  ORDER BY [Department], [Authorization], [Date], [LogNumber], [Leg]
  END
  ELSE 
  BEGIN
  SELECT * FROM #DEPARTMENTINFO
  ORDER BY [Department], [Authorization], [Date], [LogNumber], [Leg]
  END
--EXEC spGetReportPOSTDepartmentAuthorizationDetailInformation 'supervisor_99','2009-1-1','2009-1-1', '', '', '', '',0  


GO


