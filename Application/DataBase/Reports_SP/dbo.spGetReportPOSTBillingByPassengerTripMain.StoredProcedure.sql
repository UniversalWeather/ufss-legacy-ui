IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByPassengerTripMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripMain]
(
@UserCD AS VARCHAR(30), --Mandatory
@UserCustomerID AS VARCHAR(30),           
@DATEFROM AS DATE , --Mandatory          
@DATETO AS DATE , --Mandatory           
@LogNum NVARCHAR(1000) = '',
@PassengerRequestorCD  AS VARCHAR(1000) = '',
@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
@FleetGroupCD AS NVARCHAR(4000) = '',    
@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase   
@UserHomebaseID AS VARCHAR(30)  ,
@PassengerGroupCD VARCHAR(500)='' 
)
AS
-- ================================================================================   
-- SPC Name: spGetReportPOSTBillingByPassengerTripMain          
-- Author: A.AKHILA         
-- Create date: 09/03/2013          
-- Description: To Get LogNumber Details for the Report
-- Revision History          
-- Date   Name  Ver  Change          
-- ================================================================================   
BEGIN

-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration---------------------- 



DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
--ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
--BEGIN  
--INSERT INTO @TempPassengerID
--	SELECT DISTINCT  P.PassengerRequestorID
--	FROM Passenger P 
--	WHERE  P.CustomerID = @CUSTOMER  
--	AND P.IsDeleted=0
--END
-----------------------------Passenger and Passenger Group Filteration----------------------

IF (@PassengerRequestorCD <> '' OR  @PassengerGroupCD <> '')

BEGIN 


SELECT DISTINCT POM.LogNum FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
--LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND DE.CustomerID = POL.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = POL.AuthorizationID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID
INNER JOIN @TempFleetID T ON T.FleetID = FL.FleetID
INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')    
	AND (DE.DepartmentCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
    AND (DA.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')	
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0

END
ELSE

BEGIN


SELECT DISTINCT POM.LogNum FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID --AND P.IsDeleted=0
--LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND DE.CustomerID = POL.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = POL.AuthorizationID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID
INNER JOIN @TempFleetID T ON T.FleetID = FL.FleetID
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (DE.DepartmentCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
    AND (DA.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0

END


END
--EXEC spGetReportPOSTBillingByPassengerTripMain 'supervisor_99','10099','01-01-2009','01-01-2010','','','','','','',0 ,''         


GO


