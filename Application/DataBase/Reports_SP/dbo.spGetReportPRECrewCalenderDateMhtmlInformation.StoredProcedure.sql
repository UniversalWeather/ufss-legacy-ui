IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECrewCalenderDateMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECrewCalenderDateMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPRECrewCalenderDateMhtmlInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftTypeCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TripNUM AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@HomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS
-- ===============================================================================
-- SPC Name: spGetReportPRECrewCalenderCrewInformation
-- Author: ASKAR
-- Create date: 04 Jul 2012
-- Description: Get Crew Calender information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

--DROP TABLE #TempCrewDate
--DROP TABLE #TempCrewCalendar
	-- Populate the complete Crew List for each Date in the given range
	CREATE TABLE #TempCrewDate (
		RowID INT identity(1, 1) NOT NULL
		,CrewID BIGINT
		,DeptDateOrg DATETIME	
	)
	
	CREATE TABLE #TempCrewCalendar (
		RowID INT identity(1, 1) NOT NULL
		,CrewID BIGINT,DeptDateOrg DATETIME,AircraftID BIGINT,FleetID BIGINT,TripID BIGINT
		,LegID BIGINT,LegNum BIGINT,Depart DATETIME,DepartICAOID BIGINT,Arrive DATETIME,ArriveICAOID BIGINT
		,NextLocalDTTM DATETIME, DutyType CHAR(2), FlightCategoryID BIGINT,
		TripNUM BIGINT, TripDescription VARCHAR(40),Discount BIT,NoRon BIT ,Ron BIT,TrueRon BIT
	)
	DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM

	WHILE (@TEMPTODAY <= @DATETO)
		BEGIN
			--PRINT CONVERT(VARCHAR(10), @TEMPTODAY, 103)
		
			---- Calendar for this CREW only ------------------------------------
			SET @SQLSCRIPT = '
				INSERT INTO #TempCrewDate (CrewID, DeptDateOrg)
				SELECT DISTINCT C.CrewID, @TEMPTODAY 
				FROM Crew C
				LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
				LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
				WHERE C.IsStatus = 1 AND C.IsDeleted = 0
					AND C.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
				'
				IF @CrewCD <> '' BEGIN  
					SET @SQLSCRIPT = @SQLSCRIPT + ' AND C.CrewCD IN (''' + REPLACE(CASE WHEN RIGHT(@CrewCD, 1) = ',' THEN LEFT(@CrewCD, LEN(@CrewCD) - 1) ELSE @CrewCD END, ',', ''', ''') + ''')';
				END
				IF @CrewGroupCD <> '' BEGIN  
					SET @SQLSCRIPT = @SQLSCRIPT + ' AND CG.CrewGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@CrewGroupCD, 1) = ',' THEN LEFT(@CrewGroupCD, LEN(@CrewGroupCD) - 1) ELSE @CrewGroupCD END, ',', ''', ''') + ''')';
				END
				IF @HomeBase = 1 BEGIN
					SET @SQLSCRIPT = @SQLSCRIPT + ' AND C.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseAirportIDByUserCD(LTRIM(@UserCD)));
				END							
			--PRINT @SQLSCRIPT
			SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME'
			EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY				
			---------------------------------------------------------------------

			-- Populate actual trips and empty trips for all the crew
			SET @SQLSCRIPT = '
				INSERT INTO #TempCrewCalendar ( CrewID, DeptDateOrg, AircraftID, FleetID, 
					TripID, LegID, LegNum, Depart, DepartICAOID, Arrive, ArriveICAOID, 
					NextLocalDTTM, DutyType, FlightCategoryID, TripNUM, TripDescription,Discount,NoRon )
				
				SELECT DISTINCT TCD.CrewID, @TEMPTODAY, PM.AircraftID, PM.FleetID, PL.TripID, PL.LegID, 
					PL.LegNum, PL.DepartureDTTMLocal, PL.DepartICAOID, PL.ArrivalDTTMLocal, 
					PL.ArriveICAOID, PL.NextLocalDTTM, 
					[DutyTYPE] = CASE WHEN ISNULL(PM.RecordType, '''') = ''T'' THEN ''F'' ELSE PL.DutyTYPE END,
					PL.FlightCategoryID, PM.TripNUM, PM.TripDescription ,PC.IsDiscount,PC.IsDiscount
				FROM PreflightMain PM 
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
					INNER JOIN (
						SELECT LegID , CrewID,IsDiscount FROM PreflightCrewList
					) PC ON PL.LegID = PC.LegID
					INNER JOIN ( 
						SELECT CrewID, CrewCD, CrewTypeCD, CustomerID, HomebaseID FROM Crew 
					) TCD ON PC.CrewID = TCD.CrewID
					INNER JOIN ( SELECT DISTINCT CrewID FROM #TempCrewDate) Temp ON TCD.CrewID = Temp.CrewID				
					INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.AircraftID 
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
						WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 
					  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
						LEFT OUTER JOIN(
			    SELECT AircraftID,AircraftCD FROM Aircraft)AT ON AT.AircraftID=F.AircraftID
				WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
				AND TCD.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
				AND CONVERT(VARCHAR(10), PL.DepartureDTTMLocal, 103) = CONVERT(VARCHAR(10), @TEMPTODAY, 103)
				AND ISNULL(PM.RecordType,'''') <> ''M''
				AND ISNULL(PL.DutyTYPE,'''') <> ''D''
				AND PM.ISDELETED = 0
			'
			--Construct OPTIONAL clauses
			IF @TailNUM <> '' BEGIN  
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
			END
			IF @AircraftTypeCD <> '' BEGIN  
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftTypeCD, 1) = ',' THEN LEFT(@AircraftTypeCD, LEN(@AircraftTypeCD) - 1) ELSE @AircraftTypeCD END, ',', ''', ''') + ''')';
			END

			IF @TripNUM <> '' BEGIN  
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
			END
			
			--PRINT @SQLSCRIPT
			SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME, @FleetGroupCD AS VARCHAR(500)'
			EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY, @FleetGroupCD
			
			SET @TEMPTODAY = @TEMPTODAY + 1
		END
	-----------------------------------------------------------
	--Populate IDLE TIME records
		--Insert 1: IdleDepartDateTime = CurrentLeg.ArrivalDateTime and IdleArrivalDateTime = CurrentLeg.ArrivalDate 23:59
		--LoopDate = CurrentLeg.ArrivalDate ++
		--LOOP WHILE LoopDate < NextLocDate
		--	Insert 2: IdleDepartDateTime = LoopDate 00:01 and IdleArrivalDateTime = LoopDate 23:59
		--	LoopDate ++
		--END LOOP
		--Insert 3: IdleDepartDateTime = NextLocDate 00:01 and IdleArrivalDateTime = NextLocDateTime 
	DECLARE @crCrewID BIGINT, @crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT;
	DECLARE @crTripID BIGINT, @crLegID BIGINT, @crLegNum BIGINT, @crDepart DATETIME;
	DECLARE @crDepartICAOID BIGINT, @crArrive DATETIME, @crArriveICAOID BIGINT;
	DECLARE @crNextLocalDTTM DATETIME, @crDutyType CHAR(2);
	DECLARE @Loopdate DATETIME;	

	DECLARE curCrewCal CURSOR FOR
		SELECT CrewID, DeptDateOrg, AircraftID, FleetID, TripID, LegID, LegNum, 
			Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType
		FROM #TempCrewCalendar 
		WHERE CONVERT(VARCHAR, Arrive, 101) < CONVERT(VARCHAR, NextLocalDTTM, 101) 
		ORDER BY CrewID, DeptDateOrg;

	OPEN curCrewCal;
	FETCH NEXT FROM curCrewCal INTO @crCrewID, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
		@crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @Loopdate = '';
		--STEP 1:
   		INSERT INTO #TempCrewCalendar (CrewID, DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType,Ron,TrueRon)
			VALUES (@crCrewID, @crDeptDateOrg, @crAircraftID, @crFleetID, 
					@crTripID, @crArrive, @crArriveICAOID,  
					CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 23:59', 101), 
					@crArriveICAOID, @crNextLocalDTTM, 'R','TRUE','FALSE')
		
		--STEP 2:
		SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:00', 101));
		WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @crNextLocalDTTM, 101))
		BEGIN
			--PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)
			INSERT INTO #TempCrewCalendar (CrewID, DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType,Ron,TrueRon)
			VALUES (@crCrewID, @Loopdate, @crAircraftID, @crFleetID, 
					@crTripID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:01', 101), 
					@crArriveICAOID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101), 
					@crArriveICAOID, @crNextLocalDTTM, 'R','TRUE','TRUE')
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate);
		END
		--STEP 3:
		INSERT INTO #TempCrewCalendar (CrewID, DeptDateOrg, AircraftID, FleetID, 
					TripID, Depart, DepartICAOID, Arrive, ArriveICAOID, NextLocalDTTM, DutyType,Ron,TrueRon)
			VALUES (@crCrewID, @Loopdate, @crAircraftID, @crFleetID, 
					@crTripID, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:01', 101), 
					@crArriveICAOID, @crNextLocalDTTM, @crArriveICAOID, @crArrive, 'R','TRUE','FALSE')
					
	   FETCH NEXT FROM curCrewCal INTO @crCrewID, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crLegNum, @crDepart, 
		@crDepartICAOID, @crArrive, @crArriveICAOID, @crNextLocalDTTM, @crDutyType;
	END
	CLOSE curCrewCal;
	DEALLOCATE curCrewCal;
		
		--SELECT * FROM #TempCrewCalendar
	
-----------------------------------------------------------
--Populate NO trips for rest of the crew [Just insert CrewID and Date]
	--INSERT INTO #TempCrewCalendar (CrewID, DeptDateOrg)
	--	SELECT CD.CrewID, CD.DeptDateOrg FROM #TempCrewDate CD
	--	WHERE NOT EXISTS(SELECT CC.CrewID, CC.DeptDateOrg FROM #TempCrewCalendar CC WHERE CC.CrewID = CD.CrewID AND CC.DeptDateOrg = CD.DeptDateOrg)
-----------------------------------------------------------
--SELECT * FROM #TempCrewCalendar ORDER BY CrewID, DeptDateOrg, Depart
--SELECT * FROM #TempCrewDate
--SELECT * FROM #TempCrewCalendar
--FINAL OUTPUT
		SELECT A.* FROM (SELECT DISTINCT 
		    [activedate] = CCL.DeptDateOrg,
			[locdep]=PL.DepartureDTTMLocal,
			[crewcode]=C.CrewCD,
			[crewname]= C.CrewCD + '  ' + ISNULL(C.LastName,'') + ',' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,''),
			[tail_nmbr]=F.TailNUM,
			[type_code]=AT.AircraftCD,
			[orig_nmbr]=CCL.TripNUM,
			[depicao_id]=AD.IcaoID,
			[locarr]=PL.ArrivalDTTMLocal,
			[arricao_id]=AA.IcaoID,
			[duty_type]=CCL.DutyType,
			[desc]=CCL.TripDescription,
			[cat_code] = FC.FlightCatagoryCD,
			[PIC] =SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),
			[SIC] = SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1),
			[addlcrew]=SUBSTRING(ADDL.CrewCodes,1,LEN(ADDL.CrewCodes)-1),
			[elp_time]=PL.ElapseTM,
			[alert]= (CASE WHEN CD.AlertDT<=CCL.DeptDateOrg THEN 'TRUE' ELSE 'FALSE' END),
			[gmtdep]=PL.DepartureGreenwichDTTM
			--[CrewID]=CCL.CrewID,
			--[homebase]=Home.IcaoID,
			--[timeapprox]=PL.IsApproxTM,
			--[ron]=CCL.Ron,
			-- [leg_num]=PL.LegNUM,
			-- [legid]=PL.LegID, 
			-- [rec_type]=PM.RecordType,
			--[dutytype] = PL.DutyTYPE1,
			--[crewcurr] = 'unknown', --CrewDutyRules.CrewDutyRulesCD
			-- [pax_total]=PL.PassengerTotal,
			-- [gmtarr]=PL.ArrivalGreenwichDTTM,
			-- [TrueRon]=CCL.TrueRon,
			-- [NoRon]=CCL.NoRon,
			-- [discont]=CCL.Discount,
			-- [nextloc]=CCL.NextLocalDTTM,
	FROM #TempCrewCalendar CCL 
			LEFT OUTER JOIN (
				SELECT TripID, LegID,LegNUM, PilotInCommand, SecondInCommand, AdditionalCrew, ElapseTM,DepartureDTTMLocal,ArrivalDTTMLocal,
				       PassengerTotal,DepartureGreenwichDTTM ,ArrivalGreenwichDTTM,IsApproxTM,DutyTYPE1    
				FROM PreflightLeg WHERE IsDeleted = 0
			) PL ON CCL.TripID = PL.TripID AND CCL.LegID = PL.LegID
			LEFT OUTER JOIN(SELECT TripID,RecordType FROM PreflightMain)PM ON PM.TripID = PL.TripID
			LEFT OUTER JOIN (
				SELECT CrewID, CrewCD, LastName, FirstName, MiddleInitial, CrewTypeCD, HomebaseID FROM Crew
			) C ON CCL.CrewID = C.CrewID
			LEFT OUTER JOIN (
			     SELECT airportID,Icaoid FROM Airport
			 )Home ON C.HomebaseID=Home.airportID
			LEFT OUTER JOIN (
				SELECT FleetID, CustomerID, AircraftID,TailNUM, AircraftCD FROM Fleet
			) F ON CCL.FleetID = F.FleetID
			LEFT OUTER JOIN(
			    SELECT AircraftID,AircraftCD FROM Aircraft)AT ON AT.AircraftID=F.AircraftID
			LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AD ON CCL.DepartICAOID = AD.AirportID
			LEFT OUTER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON CCL.ArriveICAOID = AA.AirportID
			LEFT OUTER JOIN (SELECT FlightCategoryID,FlightCatagoryCD FROM FlightCatagory) FC 
				ON CCL.FlightCategoryID = FC.FlightCategoryID
			LEFT OUTER JOIN(SELECT CrewID,AlertDT FROM CrewCheckListDetail)CD
		        ON C.CrewID=CD.CrewID
		     LEFT OUTER JOIN (
				SELECT DISTINCT PC2.LegID, CrewCodes = (
				SELECT C.CrewCD + ',' 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
				FOR XML PATH('')
				 )
				FROM PreflightCrewList PC2		
			) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) SIC ON PL.LegID = SIC.LegID 
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE NOT IN ('P','S')
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) ADDL ON PL.LegID = ADDL.LegID )A
			ORDER BY  A.activedate, A.depicao_id--, A.CrewID
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'eliza_9', '2012-11-30', '2012-12-02', '', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'UC', '2012-07-20', '2012-07-22', 'JKM', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'UC', '2012-07-20', '2012-07-22', '', '234', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', 'N331UV', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '234', '', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '', 'test', '', 0
-- EXEC spGetReportPRECrewCalenderDateExportInformation 'jwilliams_13', '2012-07-20', '2012-07-22', '', '', '', '', '', '', 0

GO


