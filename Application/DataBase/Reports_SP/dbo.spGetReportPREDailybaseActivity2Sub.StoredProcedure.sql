IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity2Sub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity2Sub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity2Sub]
	@UserCD VARCHAR(50),  -- Mandatory	
	@LegID AS VARCHAR(30) -- Mandatory 
	--@TripID  AS VARCHAR(50), 
	--@LegNum	VARCHAR(50) = ''	
	
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPREDailybaseActivity2Sub
-- Author: ABHISHEK.S
-- Create date: 31st Oct 2012
-- Description: Get Preflight TripSheet Outbound information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

	SELECT
	    -- [LegId] = CONVERT(VARCHAR,PL.LegID),
		-- [LegNUM] = CONVERT(VARCHAR,PL.LegNUM),
		-- Comments = PL.OutbountInstruction, 
		OBInstructions = 
		CASE WHEN PTO.OutboundInstructionNUM = 1 THEN C.RptTabOutbdInstLab1 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 2 THEN C.RptTabOutbdInstLab2 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 3 THEN C.RptTabOutbdInstLab3 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 4 THEN C.RptTabOutbdInstLab4 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 5 THEN C.RptTabOutbdInstLab5 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 6 THEN C.RptTabOutbdInstLab6 + ' : ' + PTO.OutboundDescription
			WHEN PTO.OutboundInstructionNUM = 7 THEN C.RptTabOutbdInstLab7 + ' : ' + PTO.OutboundDescription
		END
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
	INNER JOIN PreflightTripOutbound PTO ON PL.LegID = PTO.LegID
	INNER JOIN Company C ON PM.CustomerID = C.CustomerID --PM.HomebaseID = C.HomebaseID AND 
	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	AND PL.LegID = CONVERT(BIGINT,@LegID)
	AND C.HomebaseID = dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
	--AND PM.TripID = CONVERT(BIGINT,@TripID)
	--AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND PTO.IsDeleted = 0
	AND PM.IsDeleted = 0
	
END

--EXEC spGetReportPREDailybaseActivity2Sub 'ELIZA_9', '100093577'
--EXEC spGetReportPRETSOutboundInformation 'TIM', '1000345730',''



GO


