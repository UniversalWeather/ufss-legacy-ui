IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarWeeklyCorportateExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCorportateExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCorportateExportInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)---=10098
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfWeeks INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsShowTripStatus BIT=0
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@EFleet VARCHAR(MAX)='' 
	  ,@ECrew VARCHAR(MAX)=''
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarWeeklyCorportateExportInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
          
            
SET NOCOUNT ON     

 SELECT @ECrew=CASE WHEN ISNULL(@EFleet,'')<>'' THEN '' ELSE @ECrew END   

DECLARE @DateFrom DATETIME=@BeginDate,@DateTo DATETIME=@EndDate

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		


IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END




-----Rest Record and Maintenace Reco
DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9))

 
 DECLARE @TableTrip TABLE(TripNUM BIGINT,
                          CrewList VARCHAR(4000),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          FleetID BIGINT,
                          TailNum VARCHAR(9),
                          TypeCode VARCHAR(10),
                          DutyTYPE CHAR(1),
                          NextLocalDTTM DATETIME,
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(25),
                          ArrivelICAOID VARCHAR(25),
                          LegNum BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          Passenger VARCHAR(4000),
                          TripStatus CHAR(1),
                          fcolor VARCHAR(8),
                          bcolor  VARCHAR(8),
                          ReplaceVal VARCHAR(100),
                          Requestor VARCHAR(60),
                          CrewVal VARCHAR(5)
                          )
 


   
 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=Crew.CrewList+'*'
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,'F' DutyType
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PAX.PaxNames+'#'
     ,PM.TripStatus
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                  WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                  WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				  WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                  WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
     ,ReplaceVal =CASE WHEN LEN(PAX.PaxLeg) > LEN(Crew.CrewLeg) THEN PAX.PaxLeg ELSE Crew.CrewLeg END+'$'
     ,Requestor=ISNULL(PPM.LastName,'') +CASE WHEN ISNULL(PPM.FirstName,'')<>'' THEN  ', ' +PPM.FirstName+' '  ELSE ' ' END+ISNULL(PPM.MiddleInitial,'')
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN Passenger PPM ON PM.PassengerRequestorID=PPM.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***'  FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 )
														 ,CrewLeg = (SELECT ISNULL(CONVERT(VARCHAR(2),'XX'),'')+'$$' FROM PreflightCrewList PC1        
																															   INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
																															   INNER JOIN PreflightLeg PL ON PC1.LegID=PL.LegID      
															 WHERE PC1.LegID = PC2.LegID   
															  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
	                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
															 FOR XML PATH('') 
																		 )FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	  		 LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END+ISNULL(P1.MiddleInitial,'')+' '+'###'
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                            ,PaxLeg = (SELECT ISNULL(CONVERT(VARCHAR(2),'XX'),'')+'$$' FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID
                                                   INNER JOIN PreflightLeg PL ON PP1.LegID=PL.LegID AND PL.IsDeleted = 0
                               WHERE PP1.LegID = PP2.LegID
                              -- AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
                               
                               FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID   
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
      AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=Crew.CrewList+'*'
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,'F' DutyType
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PAX.PaxNames+'#'
     ,PM.TripStatus
          ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                  WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                  WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				  WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                  WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
     ,ReplaceVal =CASE WHEN LEN(PAX.PaxLeg) > LEN(Crew.CrewLeg) THEN PAX.PaxLeg ELSE Crew.CrewLeg END+'$'
     ,Requestor=ISNULL(PPM.LastName,'') +CASE WHEN ISNULL(PPM.FirstName,'')<>'' THEN  ', ' +PPM.FirstName+' '  ELSE ' ' END+ISNULL(PPM.MiddleInitial,'')
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN Passenger PPM ON PM.PassengerRequestorID=PPM.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
			  LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***'  FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 )
														 ,CrewLeg = (SELECT ISNULL(CONVERT(VARCHAR(2),'XX'),'')+'$$' FROM PreflightCrewList PC1        
																															   INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
																															   INNER JOIN PreflightLeg PL ON PC1.LegID=PL.LegID      
															 WHERE PC1.LegID = PC2.LegID   
															  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
	                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
															 FOR XML PATH('') 
																		 )FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID  
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	  		 LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END+ISNULL(P1.MiddleInitial,'')+' '+'###'
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                            ,PaxLeg = (SELECT ISNULL(CONVERT(VARCHAR(2),'XX'),'')+'$$' FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID
                                                   INNER JOIN PreflightLeg PL ON PP1.LegID=PL.LegID AND PL.IsDeleted = 0
                               WHERE PP1.LegID = PP2.LegID
                              -- AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
                               
                               FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID     
     
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
      AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
       AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


INSERT INTO @CurDate(DateWeek,FleetID,TailNum)
SELECT T.*,F.FleetID,F.TailNum FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                            LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
 



INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,DepICAOID,CrewList,Passenger,TripStatus,LegID,fcolor,bcolor,ReplaceVal,Requestor,CrewVal)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
        CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye
        ,TT.ArrivelICAOID,TT.CrewList,TT.Passenger,TT.TripStatus,TT.LegID,TT.fcolor,TT.bcolor,TT.ReplaceVal,TT.Requestor,TT.CrewVal
         FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,fcolor,bcolor,ReplaceVal,Requestor,CrewVal)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID,TT.fcolor,TT.bcolor,TT.ReplaceVal,TT.Requestor,TT.CrewVal FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye IN('C','M')
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                             --  AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


                  
DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


DECLARE @TempTable TABLE(ID INT IDENTITY
                        ,WeekNumber INT
                        ,FleetID BIGINT
                        ,TailNum VARCHAR(9)
                        ,TripNUM BIGINT
                        ,DepTime VARCHAR(5)
                        ,DepLoc VARCHAR(25)
                        ,ArrTime VARCHAR(5)
                        ,ArrLoc VARCHAR(25) 
                        ,crewlist VARCHAR(4000) 
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,SORTORDER INT
                        ,RecordType VARCHAR(1)
                        ,TypeCode VARCHAR(10)
                        ,Passenger VARCHAR(4000)
                        ,TripStatus CHAR(1)
                        ,LegID BIGINT
                        ,DutyTYPE CHAR(2)
                        ,fcolor VARCHAR(8)
                        ,bcolor VARCHAR(8)
                        ,ReplaceVal VARCHAR(100)
                        ,Requestor VARCHAR(60)
                        ,CrewVal VARCHAR(5)
                        ) 
                        
 

INSERT INTO @TempTable   
SELECT 0 WeekNumber
     ,TT.FleetID
     ,TT.TailNum
     ,TripNUM
     ,SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)
     ,DepICAOID
     ,SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)
     ,ArrivelICAOID
     ,CrewList
     ,CONVERT(DATE,DepartureDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,TT.Passenger
     ,TT.TripStatus
     ,TT.LegID
     ,TT.DutyTYPE
     ,FgColor=ISNULL(TT.fcolor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.fcolor END)
            
     ,BgColr=ISNULL(TT.bcolor,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.bcolor END)
     ,TT.ReplaceVal
     ,TT.Requestor
     ,TT.CrewVal
      FROM @TableTrip TT 
		    LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
            LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum



 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;

SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 
--SELECT  @WEEK= CASE WHEN (COUNT( DISTINCT WeekNumber))=0 THEN ((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) ELSE COUNT( DISTINCT WeekNumber) END  FROM #TempTable

WHILE (@WEEKNUM<=@WEEK)

 BEGIN
  
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)

      WHILE @DateVal < @BeginDate
       
       BEGIN
       
         UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=@DateVal
		
		INSERT INTO @TempTable(WeekNumber)
		   SELECT DISTINCT @WEEKNUM
		                    WHERE NOT EXISTS(SELECT Date FROM @TempTable WHERE WeekNumber=@WEEKNUM) 

    
       SET @DateVal=DATEADD(DAY,1,@DateVal)
      
      END

 
     
 SET @WEEKNUM=@WEEKNUM+1
 
 END
 
SELECT ID
      ,WeekNumber
      ,tripdate=dbo.GetShortDateFormatByUserCD(@UserCD,Date)  
      ,orig_nmbr=REPLACE(REPLACE(TT.ReplaceVal,'XX',(CASE WHEN TripNUM=0 THEN NULL ELSE TripNUM END)),'$$$','')
      ,trip_stat=TripStatus
      ,deptime=DepTime
      ,depicao=DepLoc
      ,arrtime=ArrTime
      ,arricao=ArrLoc
      ,crewname=REPLACE(crewlist,'****','') 
      ,paxname=REPLACE(Passenger,'####','')
      ,legid=REPLACE(REPLACE(TT.ReplaceVal,'XX',LegID),'$$$','') 
      ,duty_type=CASE WHEN RecordType='C' THEN NULL ELSE  DutyTYPE END
      ,rec_type=RecordType
      ,fcolor=REPLACE(REPLACE(TT.ReplaceVal,'XX',fcolor),'$$$','') 
      ,bcolor=REPLACE(REPLACE(TT.ReplaceVal,'XX',bcolor),'$$$','') 
      ,caldate=REPLACE(REPLACE(TT.ReplaceVal,'XX',Date),'$$$','')  
      ,reptdtrange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)
      ,lorig_nmbr=REPLACE(REPLACE(TT.ReplaceVal,'XX',(CASE WHEN TripNUM=0 THEN NULL ELSE TripNUM END)),'$$$','')
      ,llegid=REPLACE(REPLACE(TT.ReplaceVal,'XX',LegID),'$$$','') 
      ,timetype=REPLACE(REPLACE(TT.ReplaceVal,'XX',@TimeBase+' Time'),'$$$','')  
      ,[goto]=REPLACE(REPLACE(TT.ReplaceVal,'XX',CONVERT(DATE,@BeginDate)),'$$$','')  
      ,ncounter=REPLACE(REPLACE(TT.ReplaceVal,'XX', 0),'$$$','')
	  ,DATEPART(WEEKDAY, Date) WEEKDAY
	  ,tail_nmbr=TT.TailNum
	  ,TT.Requestor
	  ,CrewVal
 FROM @TempTable TT
 WHERE DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY WeekNumber,Date,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER


 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
 DROP TABLE #TempTable
    

 
 
 END; 



 --EXEC spGetReportPRESchedueCalendarWeeklyCorportateExportInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''



GO


