IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFlightPurposeExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFlightPurposeExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFlightPurposeExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportFlightPurposeExportInformation
-- Author: SUDHAKAR J
-- Create date: 18 Jun 2012
-- Description: Get Flight Purpose Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================

SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		[code] = F.FlightPurposeCD
		,[desc] = F.FlightPurposeDescription
		,[default] = F.IsDefaultPurpose
		,[perstravl] = F.IsPersonalTravel
		,[client] = C.ClientCD
		,[lastuser] = F.LastUpdUID
		,[lastupdt] = F.LastUpdTS
		,[waitlist] = F.IsWaitList
	FROM FlightPurpose F
	LEFT OUTER JOIN Client C ON F.ClientID = C.ClientID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = @CUSTOMERID
	AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	-- What about 'IsWaitList', need to be considered in filter??
	ORDER BY F.FlightPurposeCD
  -- EXEC spGetReportFlightPurposeExportInformation 'tim'

GO


