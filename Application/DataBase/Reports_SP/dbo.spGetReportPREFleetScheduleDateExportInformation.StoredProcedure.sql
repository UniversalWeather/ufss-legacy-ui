IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetScheduleDateExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetScheduleDateExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREFleetScheduleDateExportInformation]

		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
		
AS
-- =============================================
-- SPC Name: spGetReportPREFleetScheduleDateExportInformation
-- Author: SINDHUJA.K
-- Create date: 18 July 2012
-- Description: Get Preflight Fleet Schedule/Date Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON  
	    Declare @FromDate datetime  
		Declare @ToDate datetime  
		Declare @Count int  
		Declare @index int  
		DECLARE @NextString NVARCHAR(40)
		DECLARE @Pos INT
		DECLARE @NextPos INT
		DECLARE @String NVARCHAR(40)
		DECLARE @Delimiter NVARCHAR(40)
		
		
		Set @FromDate = @DATEFROM  
		Set @ToDate = @DATETO  

		-- [Start] Storing given Tail Numbers [User Ipnut] in to Temp tables --------------
		DECLARE @TempTailNum TABLE (
		 ID int identity(1,1) Not Null,
		 TailNum VARCHAR(25)
		)
		
		 SET @String =@TailNum
		 SET @Delimiter = ','
		 SET @String = @String + @Delimiter
		 SET @Pos = charindex(@Delimiter,@String)

		IF (@TailNUM= '')
			Begin
				Insert into @TempTailNum (TailNum) Select TailNum from Fleet	
			End
		Else
			Begin
				WHILE (@pos <> 0)
					BEGIN
					  SET @NextString = substring(@String,1,@Pos - 1)
					  Insert into @TempTailNum values(@NextString)					 
					  SET @String = substring(@String,@pos+1,len(@String))
					  SET @pos = charindex(@Delimiter,@String)
					END
			End
			
		-- [End] Storing given Tail Numbers in to Temp tables --------------

	DECLARE  @TempTable  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1),
		CustomerID nvarchar(30),   
		PDate DateTime,  
		activedate nvarchar(25),
		tail_nmbr char(6),
		deptime VARCHAR(7),
		arrtime VARCHAR(7),
		depicao_id bigint,
		arricao_id bigint,
		dep_city varchar(25),
		arr_city varchar(25),
		dep_airport varchar(25),
		arr_airport varchar(25),
		dep_state varchar(25),
		arr_state varchar(25),
		dep_country varchar(60),
		arr_country varchar(60),
		elp_time numeric(7,3),
		[desc]  varchar(40),
		pax_total int	     
	)  

	Set @Count=DATEDIFF(DAY,@FromDate,@ToDate)  
	Set @index=0  

	while(@index<=@Count)  
	Begin  
			insert into @TempTable 
			(
				PDate,
				activedate,
				tail_nmbr
			)   
			Select 
					convert(varchar(10),DATEADD(DAY,@index,@FromDate),121),
					LEFT(DATENAME(WEEKDAY, DATEADD(DAY,@index,@FromDate)),3) + ' ' 
						+ RIGHT(CONVERT(VARCHAR(12), DATEADD(DAY,@index,@FromDate), 101),9),
					TT.TailNum from		 
							 PreflightMain PM   
								 Inner Join PreflightLeg PL On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
								 Inner Join FLEET F On PM.FleetID=F.FleetID  
								 Inner Join  @TempTailNum TT On F.TailNum=TT.TailNum 
								 Inner Join AIRPORT A On PL.DepartICAOID=A.AirportID
				                 inner join Airport B on PL.ArriveICAOID=B.AirportID  
								 Inner Join Country C On A.CountryID=C.CountryID 
								 WHERE PM.IsDeleted = 0 
		Set @index=@index+1  
	End  
	
	Update @TempTable   
	 Set  
		deptime = t.deptime,
		arrtime = t.arrtime,
		depicao_id = t.depicao_id,
		arricao_id = t.arricao_id,
		CustomerID = t.CustomerID,
		dep_city = t.City,
		arr_city = t.City,
		dep_airport = t.Airport,
		arr_airport = t.Airport,
		dep_state = t.[State],
		arr_state = t.[State],
		dep_country = t.Country,
		arr_country = t.Country,
		elp_time = t.elp_time,
		[desc] = t.[desc],
		pax_total = t.pax_total
		
	FROM   
		( 
			Select F.TailNum, 
					RIGHT(convert(varchar, PL.DepartureDTTMLocal,20), 8) as deptime,
					RIGHT(convert(varchar, PL.ArrivalDTTMLocal,20), 8) as arrtime,
					PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,
						 PL.DepartICAOID as depicao_id,
						 PL.ArriveICAOID as arricao_id,
						 PM.CustomerID as CustomerID,
						 A.CityName as City,  
						 A.AirportName as Airport,  
						 A.StateName as [State],  
						 C.CountryName as Country,  
						 PL.ElapseTM as elp_time,  
						 PL.PassengerTotal as pax_total,
						 PM.TripDescription as [desc]
						 --PM.EstDepartureDT as  EstDepartureDT
			FROM   
			 PreflightMain PM   
				 Inner Join PreflightLeg PL On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				 Inner Join FLEET F On PM.FleetID=F.FleetID  
				 Inner Join AIRPORT A On PL.DepartICAOID=A.AirportID
				 inner join Airport B on PL.ArriveICAOID=B.AirportID 
				 Inner Join Country C On A.CountryID=C.CountryID 
				 WHERE PM.IsDeleted = 0
		)as t
				 INNER JOIN @TempTable TEMP ON TEMP.tail_nmbr = T.TailNum 
			     AND CONVERT(varchar, TEMP.PDate, 101)=CONVERT(varchar, t.DepartureDTTMLocal,101)
			    -- AND CONVERT(varchar, TEMP.PDate, 101)=CONVERT(varchar, t.ArrivalDTTMLocal,101)
		         And T.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	
	--  --Construct OPTIONAL clauses
	--IF @TailNUM <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	--END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupID IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	
	Select  distinct
	        activedate,
	        tail_nmbr,
	        deptime,
	        arrtime,
			depicao_id,
			arricao_id,
			dep_city,
			arr_city,
			dep_airport,
			arr_airport,
			dep_state,
			arr_state,
			dep_country,
			arr_country,
			elp_time,
			[desc],
			pax_total
	  from @TempTable 
	 -- ORDER BY PDate, tail_nmbr 
	
	
	-- EXEC spGetReportPREFleetScheduleDateExportInformation 'UC','2012/7/20','2012/7/22','N331UV,WING',''
  -- EXEC spGetReportPREFleetScheduleDateExportInformation 'UC','2012/7/20','2012/7/22','',''


GO


