IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelSummaryFuelLocExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelSummaryFuelLocExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFuelSummaryFuelLocExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IcaoID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
		
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFuelSummaryFuelLocExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Fuel Summary for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)

	SET @SQLSCRIPT = '
			SELECT distinct  
			[fuel_loc] =FL.FuelLocatorCD,
			[desc] = FL.FuelLocatorDescription,
			[purchasedt] = PE.PurchaseDT,
			[icao_id] = A.IcaoID,
			[tail_nmbr] = F.TailNum,
			[dispatchno] = PE.DispatchNUM,
			[pay_type] = PT.PaymentTypeCD,
			[paydesc] = PT.PaymentTypeDescription,
			[vendname] = V.Name,
			[fuelqty] = CAST(ROUND((dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,4)),
			[pay_vendor] = V.VendorCD,
			[invnum] = PE.InvoiceNUM,
			[fuelpurch] = PE.FuelPurchase,
			[unitprice] = Case WHEN C.FuelPurchase <> Pe.FuelPurchase THEN DBO.GetFuelPriceConversion(PE.FuelPurchase,Pe.UnitPrice,@UserCD)
                                               ELSE Pe.UnitPrice End,
			[expamt] = PE.ExpenseAMT
			
			
		FROM PostflightExpense PE 
		
	LEFT OUTER JOIN  PaymentType PT ON PE.PaymentTypeID = PT.PaymentTypeID 
	    LEFT OUTER JOIN  Vendor V ON PE.PaymentVendorID = V.VendorID 
		LEFT OUTER JOIN  FuelLocator FL ON PE.FuelLocatorID = FL.FuelLocatorID 
		left outer JOIN  Airport A ON PE.AirportID = A.AirportID 
		LEFT OUTER JOIN  Company c ON PE.HomeBaseID = C.HomeBaseID 
		INNER JOIN  Fleet F ON PE.FleetID = F.FleetID 
		LEFT OUTER JOIN FleetGroupOrder FGO
					ON PE.FleetID = FGO.FleetID AND PE.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG
                   ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE PE.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				+ ' AND CONVERT(DATE,PE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PE.IsDeleted=0
	'
	

	--Construct OPTIONAL clauses
			
	IF @TailNUM <> '' BEGIN  
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END	
	IF @FleetGroupCD <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	END	
	IF @IcaoID <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@IcaoID, 1) = ',' THEN LEFT(@IcaoID, LEN(@IcaoID) - 1) ELSE @IcaoID END, ',', ''', ''') + ''')';
	END	
  SET @SQLSCRIPT = @SQLSCRIPT + 'order by fuel_loc,icao_id,purchasedt'
	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@DATEFROM AS DATETIME, @DATETO AS DATETIME, @UserCD AS VARCHAR(30)'
    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @DATEFROM, @DATETO, @UserCD 
  
   -- EXEC spGetReportPOSTFuelSummaryFuelLocExportInformation 'jwilliams_13','2011/2/3','2012/3/5', '', '', ''

GO


