IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- EXEC [dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingInformation]  'JWILLIAMS_13','10013','01-01-2011','03-01-2013','','','','','','',0,''

CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID AS BIGINT,
		@DATEFROM AS DATETIME , --Mandatory
		@DATETO AS DATETIME , --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		--@DepartmentGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@DepartmentGroupID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0,
        --@TenToMin SMALLINT = 0,
		@HomebaseCD AS NVARCHAR(1000) = ''
		)

AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailInformation
-- Author: 
-- Create date: 
-- Description: Get Department/Authorization Detail for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
	DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
			
	DECLARE @AircraftBasis NUMERIC(1,0);
		SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
		AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 
			
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND F.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMERID 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------

SELECT DISTINCT DE.DepartmentCD, 
				DA.AuthorizationCD,
				--FL.TailNum,
				 FL.TailNum [Tail Number],
				CONVERT(DATE ,PL.OutboundDTTM) TRIPDATE,
				PO.DispatchNum, 
				PO.LogNum, --PO.DepartmentID, PO.AuthorizationID, 
				PL.POLegID, 
				PL.ScheduledTM,
				PL.LegNUM,
				--PL.DepartICAOID, PL.ArriveICAOID,
				AD.IcaoID AS DepartICAOCD ,
				AA.IcaoID AS ArriveICAOCD,
				PL.Distance, 
				ROUND(PL.FlightHours,1) FlightHours, 
				ROUND(PL.BlockHours,1) BlockHours,
				PL.PassengerTotal AS PAX_TOT,
				FL.MaximumPassenger - PL.PassengerTotal AS ES_TOTAL, 
				FL.AircraftCD,
				CR.ChargeUnit, 
				CR.ChargeRate,
				TOTAL_CHARGES = CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ISNULL((PFE11.SUMExpenseAmt),0)) 
				WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				END,
				--(200+PFE11.SUMExpenseAmt) AS TOTAL_CHARGES, 
				--PFP.PassengerFirstName, FP.FlightPurposeCD,
				PAX.FlightPurposeCDS AS FlightPurposeCD,
				PAX.PaxNames AS PassengerName ,
				ACC.AccountDesc ,ACC.ExpenseAmt,
				--AC.AccountDescription, PFE.ExpenseAMT,
				PFE11.IsBilling,
				[TenToMin] = @TenToMin
	FROM PostflightMain PO  
		JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
	    -- INNER JOIN  vDeptAuthGroup DE
		 --  ON PL.DepartmentID = DE.DepartmentID AND PL.AuthorizationID=DE.AuthorizationID AND PL.CustomerID = DE.CustomerID
		 INNER JOIN Fleet FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID --AND PL.CustomerID = FL.CustomerID
	     INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = FL.FleetID 
		 LEFT JOIN Department DE ON DE.DepartmentID = PL.DepartmentID
		 LEFT JOIN DepartmentGroupOrder DO ON DE.DepartmentID = DO.DepartmentID
		 LEFT JOIN DepartmentGroup DG ON DO.DepartmentGroupID = DG.DepartmentGroupID
		 LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID
	     LEFT JOIN Company CO
		   ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
	     LEFT JOIN Airport AI
		   ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
	     LEFT JOIN Airport AD 
           ON PL.DepartICAOID = AD.AirportID         
	     LEFT JOIN Airport AA 
           ON PL.ArriveICAOID = AA.AirportID 
	  /* JOIN PostflightPassenger PFP
		   ON PL.POLegID = PFP.POLegID AND PL.POLogID = PFP.POLogID AND PL.CustomerID = PFP.CustomerID*/
	     /*JOIN PostflightExpense PFE
		   ON PL.POLegID = PFE.POLegID AND PL.POLogID = PFE.POLogID AND PL.CustomerID = PFE.CustomerID
	     JOIN Account AC
		   ON PFE.AccountID = AC.AccountID AND PFE.CustomerID = AC.CustomerID*/
		 LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                      SELECT ISNULL(SUM(PP2.ExpenseAMT),0) 
                               FROM PostflightExpense PP2 
									    WHERE PP2.POLegID = PP4.POLegID AND IsBilling = 1)
                       FROM PostflightExpense PP4 WHERE IsBilling =1 --AND POLegID = 100131161
                       )PFE11
                      ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID
	/* JOIN FlightPurpose FP 
		  ON PFP.FlightPurposeID = FP.FlightPurposeID AND PFP.CustomerID = FP.CustomerID*/
	/*--JOIN (SELECT PFE1.AccountID,AccountDescription ,SUM(ExpenseAMT) AS EXP_AMT FROM PostflightExpense PFE1 JOIN ACCOUNT ACC1 
		  ON PFE1.AccountID = ACC1.AccountID AND PFE1.CustomerID = ACC1.CustomerID GROUP BY PFE1.AccountID,AccountDescription)PFE2
		  ON PL.AccountID = PFE2.AccountID
	    JOIN TailHistory TH
		  ON TH.NewTailNUM = FL.TailNum */ -- IF TAIL HISTORY ADDED MEANS NO DATA IS COMING.
				  
		LEFT  JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,PaxNames = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH('')),
                      FlightPurposeCDS = (
                       SELECT ISNULL(P1.FlightPurposeCD,'')+'$$' 
                               FROM PostflightPassenger PP1 INNER JOIN FlightPurpose P1 ON PP1.FlightPurposeID=P1.FlightPurposeID 
                               WHERE PP1.POLegID = PP2.POLegID
                             FOR XML PATH('')
                             )
                              FROM PostflightPassenger PP2 
                              --WHERE PP2.POLegID = 100135919
				   ) PAX ON PL.POLegID = PAX.POLegID  -- NEW CHANGE
		LEFT JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,PP3.IsBilling ,ExpenseAmt = (
                      SELECT ISNULL(CAST(A.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                               FROM (SELECT PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, AccountDesc = P1.AccountDescription, ExpenseAmt = SUM(PP4.ExpenseAMT)
											FROM PostflightExpense PP4 INNER JOIN Account P1 ON PP4.AccountID=P1.AccountID 
											WHERE PP4.POLegID = PP3.POLegID AND PP4.IsBilling =1  
											GROUP BY PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, P1.AccountDescription
											)A
                               --PostflightExpense PP1 
                               --INNER JOIN Account P1 ON PP1.AccountID = P1.AccountID 
                               --WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1
                               ORDER BY A.ExpenseAMT DESC
                             FOR XML PATH('')),
							AccountDesc = (
							 SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' 
                               FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1
                             FOR XML PATH('')
                             )
                       FROM PostflightExpense PP3 WHERE PP3.IsBilling =1) --AND POLegID = 100997291)
                       ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID
               LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON PL.POLegID = CR.POLegID       
                       	
	WHERE CONVERT(DATE,PL.OutboundDTTM) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
		   AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		   AND (DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, ',')) OR @DepartmentGroupID = '')
	       AND (DA.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')
	       AND (AI.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomebaseCD, ',')) OR @HomebaseCD ='')
		 --  AND (PO.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseCDID = 0)
		 AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
		   AND PO.CustomerID = @UserCustomerID AND DE.DepartmentCD <> 'NULL'
		   AND PO.IsDeleted = 0
		--AND PO.CustomerID = 10013
	    --ORDER BY PL.LegNUM, DE.AuthorizationCD
END
	  
	 
	  
	







GO


