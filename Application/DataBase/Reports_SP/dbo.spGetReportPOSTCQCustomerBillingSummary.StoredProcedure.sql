IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCQCustomerBillingSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCQCustomerBillingSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTCQCustomerBillingSummary](@UserCD VARCHAR(30)
										,@UserCustomerID BIGINT
										,@DateFrom DATETIME
										,@DateTo DATETIME
										,@CQCustomerCD VARCHAR(200)='')
AS
-- =============================================
-- SPC Name:spGetReportPOSTCQCustomerBillingSummary
-- Author: Askar
-- Create date: 23 Feb 2013
-- Description: Get Charter Quote Details based on CustomerName
-- Revision History
-- Date		Name		Ver		Change
-- =============================================
BEGIN
SELECT DISTINCT 
		ISNULL(CF.CQCustomerName,'')+' ('+ISNULL(CQC.CQCustomerCD,'')+')' CQCustomerName,
		F.TailNum,
		POM.LogNum,
		CF.CQFileDescription,
		LS.LeadSourceDescription,
		CASE WHEN POL.LegNUM=1 THEN CLS.PaxCount ELSE 0 END PaxCount,
		--CLS.PaxCount PaxCount,
		SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),
		PAX.CNT,
		--CASE WHEN POL.LegNUM=1 THEN POL.Distance ELSE 0 END Distance,
		POL.Distance Distance,
		--CASE WHEN POL.LegNUM>1 THEN 0 ELSE CM.QuoteTotal END QuoteTotal,
		CM.QuoteTotal QuoteTotal,
		CONVERT(DATE,MIN(POM.EstDepartureDT))TripDate,
		POL.LegNUM,
		PMM.TripNUM,
		A.IcaoID DepICAO,
		AA.IcaoID ArrICAO,
		SUBSTRING(ACrew.AssociatedCrew,1,LEN(ACrew.AssociatedCrew)-1) CrewCD
		,AF.AircraftCD, 
		dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) DateRange
FROM CQFile CF 
		INNER JOIN CQMain CM  ON CM.CQFileID=CF.CQFileID
		LEFT OUTER JOIN CQLeg CL ON CL.CQMainID = CM.CQMainID
		INNER JOIN PreflightMain PM ON PM.TripID=CM.TripID AND PM.IsDeleted=0
		INNER JOIN PostflightMain POM ON CM.TripID=POM.TripID AND POM.IsDeleted=0
		INNER JOIN PreflightMain PMM ON PMM.TripID=POM.TripID AND PMM.IsDeleted=0
		INNER JOIN Fleet F ON F.FleetID=PM.FleetID
		LEFT OUTER JOIN Aircraft AF ON AF.AircraftID = F.AircraftID
		INNER JOIN PostflightLeg POL ON POL.POLogID=POM.POLogID AND POL.IsDeleted=0
		LEFT OUTER JOIN PostflightPassenger pp on pp.POLegID = POL.POLegID
		LEFT OUTER JOIN Passenger p on p.PassengerRequestorID = pp.PassengerID
		LEFT OUTER JOIN Airport A ON A.AirportID = POL.DepartICAOID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
		LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID = POL.POLegID
		LEFT OUTER JOIN LeadSource LS ON LS.LeadSourceID=CF.LeadSourceID
		LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID
		LEFT OUTER JOIN (
						SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
						SELECT RTRIM(C.CrewCD) + ','
						FROM PostflightCrew PC 
						INNER JOIN Crew C ON PC.CrewID = C.CrewID
						WHERE PC.POLegID = PC1.POLegID
						order by C.CrewCD
						FOR XML PATH('')
						)
						FROM PostflightCrew PC1
						) ACrew ON POL.POLegID = ACrew.POLegID 
						
	LEFT OUTER JOIN ( 
		SELECT DISTINCT CQLegID, PaxNames = (
              SELECT ISNULL(P1.PassengerRequestorCD,'') +', '  
                   FROM CQPassenger CQP 
                   INNER JOIN Passenger P1 ON CQP.PassengerRequestorID=P1.PassengerRequestorID 
                   WHERE CQP.CQLegID = CQP2.CQLegID
                 FOR XML PATH('')
                 ),
                 CNT = (
              SELECT COUNT(P1.PassengerRequestorID)
                   FROM CQPassenger CQP 
                   INNER JOIN Passenger P1 ON CQP.PassengerRequestorID=P1.PassengerRequestorID 
                   WHERE CQP.CQLegID = CQP2.CQLegID
                 FOR XML PATH('')
                 )
              FROM CQPassenger CQP2
	) PAX ON CL.CQLegID = PAX.CQLegID
		

		---PAX TOTAL-----	
		--LEFT OUTER JOIN (
		--				SELECT DISTINCT CC.CQCustomerID, F.FleetID, COUNT(DISTINCT P.PassengerRequestorID) PaxCount 
		--				FROM CQFILE CF 
		--					INNER JOIN CQCustomer CC ON CC.CQCustomerID = CF.CQCustomerID
		--					INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID
		--					INNER JOIN Fleet F ON F.FleetID = CM.FleetID
		--					INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID
		--					INNER JOIN CQPassenger CQPAX ON CQPAX.CQLegID = CL.CQLegID
		--					INNER JOIN Passenger P ON CQPAX.PassengerRequestorID=P.PassengerRequestorID
		--				WHERE  CONVERT(DATE,CF.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND CF.CustomerID=@UserCustomerID
		--				GROUP BY CC.CQCustomerID, F.FleetID
		--					) CLS ON CM.FleetID = CLS.FleetID AND CF.CQCustomerID = CLS.CQCustomerID
		LEFT OUTER JOIN (
						SELECT DISTINCT POM.TripID Trip,POM.LogNum,F.FleetID, COUNT(DISTINCT P.PassengerRequestorID) PaxCount
						FROM CQFILE CF 
							INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID
							INNER JOIN PostflightMain POM ON CM.TripID = POM.TripID AND POM.IsDeleted=0
							INNER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted=0
							INNER JOIN PostflightPassenger PP ON PP.POLegID = POL.POLegID
							INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
							INNER JOIN Fleet F ON F.FleetID = POM.FleetID
						WHERE  CONVERT(DATE,CF.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
						 AND CF.CustomerID=@UserCustomerID
						GROUP BY F.FleetID,POM.LogNum,POM.TripID
							) CLS ON  CLS.Trip=POM.TripID
		---PAX TOTAL-----

WHERE CONVERT(DATE,ISNULL(POM.EstDepartureDT,CF.EstDepartureDT)) BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
	AND CF.CustomerID=@UserCustomerID
	AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
GROUP BY CL.LegNUM,PAX.CNT, pax.PaxNames, CLS.PaxCount,F.TailNum, CF.CQCustomerName,F.TailNum,POM.LogNum,CF.CQFileDescription,LS.LeadSourceDescription,
	POL.PassengerTotal,CM.QuoteTotal,CQC.CQCustomerCD,POL.LegNUM, PMM.TripNUM,A.IcaoID,AA.IcaoID,ACrew.AssociatedCrew,AF.AircraftCD, POL.Distance,CLS.Trip,POM.TripID
ORDER BY CQCustomerName,TailNum, TripDate , POM.LogNum,POL.LegNUM

END

-------EXEC spGetReportPOSTCQCustomerBillingSummary 'SUPERVISOR_16',10016,'2007-10-01','2007-10-01',''

GO


