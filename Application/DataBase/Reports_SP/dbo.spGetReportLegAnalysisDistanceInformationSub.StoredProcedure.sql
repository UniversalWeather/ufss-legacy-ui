IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLegAnalysisDistanceInformationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLegAnalysisDistanceInformationSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportLegAnalysisDistanceInformationSub]      
(
  @UserCD AS VARCHAR(30),  --Mandatory
  @DATEFROM DATETIME, --Mandatory
  @DATETO DATETIME, --Mandatory
  @TailNumber VARCHAR(300)=Null,--Optional
  @FleetGroupCD VARCHAR(300)=Null,--Optional
  @FleetID BIGINT 
 )
-- =============================================    
-- Author: Mathes    
-- Create date: 01-08-2012    
-- Description: Leg Analysis/Hours(FC) - Report     
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- =============================================    
AS    
SET NOCOUNT ON    
BEGIN    
      IF OBJECT_ID('tempdb..#TempDist') is not null    
      DROP table #TempDist    
         
		Create Table #TempDist (DistId Int,DistFrom Int,DistTo Int)    
		INSERT INTO #TempDist (DistId,DistFrom,DistTo)    
		Select 1,0,24  union All     
		Select 2,25,49  union All     
		Select 3,50,74  union All     
		Select 4,75,99  union All     
		Select 5,100,124  union All     
		Select 6,125,149  union All     
		Select 7,150,174  union All     
		Select 8,175,199  union All     
		Select 9,200,249  union All     
		Select 10,250,299  union All     
		Select 11,300,399  union All     
		Select 12,400,499  union All     
		Select 13,500,599  union All     
		Select 14,600,699  union All     
		Select 15,700,799  union All     
		Select 16,800,899  union All     
		Select 17,900,999  union All     
		Select 18,1000,1099  union All     
		Select 19,1100,1199  union All     
		Select 20,1200,1299  union All     
		Select 21,1300,1399  union All     
		Select 22,1400,1499  union All     
		Select 23,1500,1599  union All     
		Select 24,1600,1699  union All     
		Select 25,1700,1799  union All     
		Select 26,1800,1899  union All     
		Select 27,1900,1999  union All     
		Select 28,2000,2099  union All     
		Select 29,2100,2199  union All     
		Select 30,2200,2299  union All     
		Select 31,2300,2399  union All     
		Select 32,2400,2499  union All     
		Select 33,2500,2599  union All     
		Select 34,2600,2699  union All     
		Select 35,2700,2799  union All     
		Select 36,2800,2899  union All     
		Select 37,2900,2999  union All     
		Select 38,3000,3099  union All     
		Select 39,3100,3199  union All     
		Select 40,3200,3299  union All     
		Select 41,3300,3399  union All     
		Select 42,3400,3499  union All     
		Select 43,3500,3599  union All     
		Select 44,3600,3699  union All     
		Select 45,3700,3799  union All     
		Select 46,3800,3899  union All     
		Select 47,3900,3999  union All     
		Select 48,4000,9999   
		    
		Declare @DistId Int    
		Declare @DistFrom Int    
		Declare @DistTo Int    
		   
		Declare @RowId Int    
		Declare @NumRows Int     
       
        IF OBJECT_ID('tempdb..#TempReport') is not null    
          DROP table #TempReport    
           
        Create Table #TempReport (FromMile Int,
                                  ToMile Int,
                                  FleetID BIGINT, 
                                  TailNum CHAR(6),
                                  AircraftCD CHAR(3),
                                  NoOfLegs Int,
                                  TotalLegs Int, 
                                  BlockHours Numeric(10,2),
                                  FlightHours Numeric(10,2),
                                  Miles Int,
                                  FuelBurn Int, 
                                  NoOfPax Int)  
                                              
        Declare @TailNum CHAR(6)
        Declare @AircraftCD CHAR(3)
        Declare @NoOfLegs INT
        Declare @TotalLegs INT 
        Declare @BlockHours Numeric(10,2) 
        Declare @FlightHours Numeric(10,2)
        Declare @Miles Int
        Declare @FuelBurn Int 
        Declare @NoOfPax Int 
             
            
            BEGIN                   
         
            SET @RowId = 1       
            SET @NumRows =  (SELECT COUNT(*) FROM #TempDist)     
            IF @NumRows > 0     
                WHILE (@RowId <= (SELECT MAX(DistID) FROM #TempDist))      
                BEGIN    
    
				Set @DistId=0    
				Set @DistFrom=0     
				Set @DistTo=0    
 
				SELECT @DistId=DistId,@DistFrom=DistFrom,@DistTo=DistTo From #TempDist Where DistId=@RowId     
				-----------------------------------------------------------------------------Actual Report Area Starts    
				  IF @TailNumber <>'' And @FleetGroupCD <>''
				  BEGIN
				    SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,
							 @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,
							 @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax   
					FROM   ( SELECT
							 Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,
							 IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,  
							 PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM
					 FROM    Fleet INNER JOIN    
							 FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
							 FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
							 PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
							 PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
							 PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID   
					GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,    
							 PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD              
					HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And
							 Fleet.FleetID=@FleetID And  
							 PostflightLeg.Distance Between @DistFrom And @DistTo And  
							 PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO 
							 And Fleet.TailNum In (@TailNumber)
							 And FleetGroup.FleetGroupCD In(@FleetGroupCD))  ResultSet    
				  END
				  ELSE IF @TailNumber ='' And @FleetGroupCD <>''
				  BEGIN
				    SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,
							 @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,
							 @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax  
					FROM   ( SELECT
							 Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,
							 IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,  
							 PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM
					 FROM    Fleet INNER JOIN    
							 FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
							 FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
							 PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
							 PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
							 PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID   
					GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,    
							 PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD              
					HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And
							 Fleet.FleetID=@FleetID And  
							 PostflightLeg.Distance Between @DistFrom And @DistTo And  
							 PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO
							 And FleetGroup.FleetGroupCD In (@FleetGroupCD))  ResultSet    
				  END
				  ELSE IF @TailNumber <>'' And @FleetGroupCD =''
				  BEGIN
					 SELECT  @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,
							 @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,
							 @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax  
					FROM   ( SELECT
							 Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,
							 IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,  
							 PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM
					 FROM    Fleet INNER JOIN    
							 FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
							 FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
							 PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
							 PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
							 PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID   
					GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,    
							 PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD              
					HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And
							 Fleet.FleetID=@FleetID And  
							 PostflightLeg.Distance Between @DistFrom And @DistTo And  
							 PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO
							 And Fleet.TailNum In (@TailNumber))  ResultSet    
				  END
				  ELSE IF @TailNumber ='' And @FleetGroupCD =''
				  BEGIN
					SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,
							 @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,
							 @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax 
					FROM   ( SELECT
							 Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,
							 IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,  
							 PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM
					 FROM    Fleet INNER JOIN    
							 FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
							 FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
							 PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
							 PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
							 PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID   
					GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,    
							 PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD             
					HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And
							 Fleet.FleetID=@FleetID And  
							 PostflightLeg.Distance Between @DistFrom And @DistTo And  
							 PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO)  ResultSet    
				  END
				 
     
               Insert Into #TempReport(FromMile,ToMile,FleetID,TailNum,AircraftCD,NoOfLegs,TotalLegs,
                      BlockHours,FlightHours,Miles,FuelBurn,NoOfPax)
               Values(@DistFrom,@DistTo,@FleetID,@TailNum,@AircraftCD,@NoOfLegs,@TotalLegs,
                      @BlockHours,@FlightHours,@Miles,@FuelBurn,@NoOfPax)                              
                            
                        ------------------------------------------------------------------------------Actual Report Area Ends    
                        Set @RowId=@RowId+1    
                    END        
         
         END        
          
      
 
   IF OBJECT_ID('tempdb..#TempDist') is not null    
      DROP table #TempDist     
         
   Select * from #TempReport   
       
   IF OBJECT_ID('tempdb..#TempReport') is not null    
      DROP table #TempReport        
 END


GO


