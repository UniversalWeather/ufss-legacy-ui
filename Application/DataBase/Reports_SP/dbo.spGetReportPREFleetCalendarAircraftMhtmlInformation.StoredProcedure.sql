IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetCalendarAircraftMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetCalendarAircraftMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPREFleetCalendarAircraftMhtmlInformation]
	@UserCD VARCHAR(30)--Mandatory
	,@DATEFROM DATETIME--Mandatory
	,@DATETO DATETIME--Mandatory
	,@TailNum VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@FleetGroupCD VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@AircraftCD VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@HomeBase BIT = 0
	
AS
-- ===========================================================
--	SPC Name: spGetReportPREFleetCalendarAircraftExportInformation
-- Author: AISHWARYA.M
-- Create date: 18 Jul 2012
-- Description: Get Fleet Calender Aircraft Export information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 
-- ============================================================
SET NOCOUNT ON 

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)
DECLARE @Loopdate DATETIME

CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,FleetID BIGINT
	,TailNum VARCHAR(25)
	,AircraftID BIGINT
	)
SET @String = @TailNum
	SET @Delimiter = ','
	SET @String = @String + @Delimiter
	SET @Pos = charindex(@Delimiter, @String) 
	
CREATE TABLE #TempTable  (
	tail_nmbr CHAR(6)
	,activedate DATETIME
	,DepartureDTTMLocal DATETIME
	,depicao_id VARCHAR(50)
	,ArrivalDTTMLocal DATETIME
	,arricao_id VARCHAR(50)
	,requestor VARCHAR(60)
	,dept_code CHAR(8)
	,cat_code CHAR(4)
	,duty_type CHAR(2)
	,elp_time NUMERIC(7, 3)
	,timeapprox BIT
	,pax_total INT
	,ron CHAR(5) 
	,pic_code VARCHAR(100)
	,sic_code VARCHAR(100)
	,orig_nmbr BIGINT
	,leg_num BIGINT
	,legid BIGINT
	,type_code CHAR(10)
	,vendcode CHAR(5)
	,[desc] VARCHAR(40)
	,rec_type CHAR(1)
	--grndtime 
	,trueron CHAR(5)
	--dayfill
	,nextloc VARCHAR(50)
	,ac_code CHAR(3)
	,auth_code CHAR(8)
	,resv_avail INT
	,purpose VARCHAR(40)
	,[private] BIT
	,ordernum INT 
	,aircraft_code CHAR(3)
	,NextLocalDTTM DATETIME
	,CustomerID BIGINT
	,FleetID BIGINT
	,TripID BIGINT
	,DepartmentID BIGINT
	,AuthorizationID BIGINT
	)
	
DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM
	
IF (@TailNum = '')
		BEGIN
		SET @SQLSCRIPT = 
			' INSERT INTO #TempTailNum (FleetID,TailNum, AircraftID)
			SELECT DISTINCT F.FleetID,F.TailNum, F.AircraftID
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID 
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID 
		   WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
				OR @FleetGroupCD = '''' )
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			---AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))
			'		
		IF @HomeBase = 1 BEGIN
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	    END	
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD 
		
		END
	ELSE
	BEGIN
			INSERT INTO #TempTailNum (FleetID,TailNum, AircraftID)
				SELECT DISTINCT FleetID,F.TailNum, F.AircraftID
				FROM Fleet F 
			WHERE ( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) 
						OR @TailNum = '' )
				AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	END
	
	WHILE (@TEMPTODAY <= @DATETO)

		BEGIN	
		SET @SQLSCRIPT = '

		INSERT INTO #TempTable 
		(	tail_nmbr, activedate, DepartureDTTMLocal, depicao_id, ArrivalDTTMLocal, arricao_id, requestor, cat_code, duty_type  
			, elp_time, timeapprox, pax_total, ron, pic_code, sic_code, orig_nmbr, leg_num, legid, type_code, vendcode  
			, [desc], rec_type, trueron, nextloc, ac_code, resv_avail, purpose,[private], ordernum, aircraft_code
			,[NextLocalDTTM], CustomerID, FleetID, TripID, DepartmentID, AuthorizationID  
			
		)
		
	SELECT F.TailNum
			,@TempToday
			,PL.DepartureDTTMLocal
			,AD.ICAOID
			,PL.ArrivalDTTMLocal
			,AA.ICAOID
			,PL.RequestorName
			--,D.DepartmentCD
			,FC.FlightCatagoryCD
			,CASE WHEN PM.RecordType = ''T'' THEN ''F'' ELSE  PM.RecordType END
			,PL.ElapseTM
			,PL.IsApproxTM
			,PL.PassengerTotal
			,''FALSE''
			,(SELECT SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1)) 
			,(SELECT SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1)) 
			,PM.TripNUM
			,PL.LegNUM
			,PL.LegID
			,A.AircraftCD
			,V.VendorCD
			,PM.TripDescription
			,PM.RecordType
			,''FALSE''
			,PL.NextLocalDTTM
			,F.AircraftCD
			--,DA.AuthorizationCD
			,PL.ReservationAvailable
			,PL.FlightPurpose
			,PL.IsPrivate
			,99999
			,F.AircraftCD
			,PL.NextLocalDTTM 
			,F.CustomerID 
			,F.FleetID
			,PM.TripID
			,PL.DepartmentID 
			,PL.AuthorizationID 
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
			INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
			LEFT OUTER JOIN (
					SELECT DISTINCT PC2.LegID, CrewCodes = (
					SELECT C.CrewCD + '','' 
					FROM PreflightCrewList PC
					INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
					WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = ''P''
					FOR XML PATH('''')
					 )
					FROM PreflightCrewList PC2		
				) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
			SELECT DISTINCT PC3.LegID, CrewCodes = (
				SELECT C1.CrewCD + '',''
				FROM PreflightCrewList PC1
				INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
				WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = ''S''
				FOR XML PATH('''')
			 )
			FROM PreflightCrewList PC3 	
		) SIC ON PL.LegID = SIC.LegID 
			INNER JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.VendorID, F.AircraftCD
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
				OR @FleetGroupCD = '''' )
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			INNER JOIN #TempTailNum TEMP ON TEMP.FleetID = F.FleetID
			LEFT OUTER JOIN Vendor V ON V.VendorID = F.VendorID
			LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
			INNER JOIN AIRCRAFT AT ON PM.AircraftID=AT.AircraftID
			WHERE CONVERT(VARCHAR, @TEMPTODAY, 101) = CONVERT(VARCHAR, PL.DepartureDTTMLocal, 101)
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			 AND PM.TripStatus = ''T'' AND PM.IsDeleted = 0 
			---AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))	
		'
		--Construct OPTIONAL clauses
		IF @TailNUM <> '' BEGIN  
			SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
		END
		
		IF @AircraftCD <> '' BEGIN  
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
			END
			
		--IF @HomeBase = 1 BEGIN
		--		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
		--	END	
		
		
		--PRINT @SQLSCRIPT
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME, @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY, @FleetGroupCD
		--Print '@TEMPTODAY:' + convert(varchar(10),@TEMPTODAY)
		SET @TEMPTODAY = @TEMPTODAY + 1
		
END

	DECLARE @tail_nmbr CHAR(6), @activedate DATETIME, @DepartureDTTMLocal DATETIME, @depicao_id VARCHAR(50), @ArrivalDTTMLocal DATETIME, @arricao_id VARCHAR(50), @requestor VARCHAR(60),
			@dept_code CHAR(8), @cat_code CHAR(4), @duty_type CHAR(2), @elp_time NUMERIC(7, 3), @timeapprox BIT, @pax_total INT, @ron CHAR(5), @pic_code CHAR(3),
			@sic_code CHAR(3), @orig_nmbr BIGINT, @leg_num BIGINT, @legid BIGINT, @type_code CHAR(10), @vendcode CHAR(5), @desc VARCHAR(40), @rec_type CHAR(1),@trueron CHAR(5),
			@nextloc VARCHAR(50), @ac_code CHAR(3), @auth_code CHAR(8), @resv_avail INT,@purpose VARCHAR(40), @private BIT, @ordernum INT, @aircraft_code CHAR(3),
			@NextLocalDTTM DATETIME, @CustomerID BIGINT, @FleetID BIGINT, @TripID BIGINT, @DepartmentID BIGINT, @AuthorizationID BIGINT;
	
	DECLARE curFleetCalendarExp CURSOR
	FOR
	SELECT TT.tail_nmbr, TT.activedate, TT.DepartureDTTMLocal, TT.depicao_id, TT.ArrivalDTTMLocal, TT.arricao_id, TT.requestor, TT.cat_code, TT.duty_type  
			,TT.elp_time, TT.timeapprox, TT.pax_total, TT.ron, TT.pic_code, TT.sic_code, TT.orig_nmbr, TT.leg_num, TT.legid, TT.type_code, TT.vendcode  
			,TT.[desc], TT.rec_type, TT.trueron, TT.nextloc, TT.ac_code, TT.resv_avail, TT.purpose, TT.[private], TT.ordernum, TT.aircraft_code
			,TT.[NextLocalDTTM], TT.CustomerID, TT.DepartmentID, TT.AuthorizationID
			FROM #TEMPTABLE TT
			WHERE CONVERT(DATE, TT.ArrivalDTTMLocal, 101) < CONVERT(DATE, TT.[NextLocalDTTM], 101)
			ORDER BY activedate
			
	OPEN curFleetCalendarExp
	FETCH NEXT
	FROM curFleetCalendarExp
	INTO @tail_nmbr, @activedate, @DepartureDTTMLocal , @depicao_id, @ArrivalDTTMLocal, @arricao_id, @requestor,
	    @cat_code, @duty_type, @elp_time, @timeapprox, @pax_total, @ron, @pic_code,
		@sic_code, @orig_nmbr, @leg_num, @legid, @type_code, @vendcode, @desc, @rec_type, @trueron,
		@nextloc, @ac_code, @resv_avail, @purpose, @private, @ordernum, @aircraft_code,
		@NextLocalDTTM, @CustomerID, @DepartmentID, @AuthorizationID 
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
			WHILE(CONVERT(DATE, @Loopdate, 101) < CONVERT(DATE, @NextLocalDTTM, 101))
			BEGIN
			
			INSERT INTO #TempTable (tail_nmbr, activedate, DepartureDTTMLocal, depicao_id, ArrivalDTTMLocal, arricao_id, requestor, dept_code, cat_code, duty_type  
						,elp_time, timeapprox, pax_total, ron, pic_code, sic_code, orig_nmbr, leg_num, legid, type_code, vendcode  
						,[desc], rec_type, trueron, nextloc, ac_code, auth_code, resv_avail, purpose, [private], ordernum, aircraft_code
						,[NextLocalDTTM], CustomerID, DepartmentID, AuthorizationID)					 
			VALUES ( @tail_nmbr, @Loopdate,  CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101), @arricao_id, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101),
						@arricao_id, @requestor,
						@dept_code, @cat_code, 'R', @elp_time, @timeapprox, @pax_total, 'TRUE', @pic_code,
						@sic_code, @orig_nmbr, @leg_num, @legid, @type_code, @vendcode, @desc, @rec_type, 'TRUE',
						@nextloc, @ac_code, @auth_code, @resv_avail, @purpose, @private, 99999, @aircraft_code,
						NULL, @CustomerID, @DepartmentID, @AuthorizationID )
							
				SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
			END
			
			FETCH NEXT
			FROM curFleetCalendarExp
			INTO @tail_nmbr, @activedate, @DepartureDTTMLocal , @depicao_id, @ArrivalDTTMLocal, @arricao_id, @requestor,
				@cat_code, @duty_type, @elp_time, @timeapprox, @pax_total, @ron, @pic_code,
				@sic_code, @orig_nmbr, @leg_num, @legid, @type_code, @vendcode, @desc, @rec_type, @trueron,
				@nextloc, @ac_code, @resv_avail, @purpose, @private, @ordernum, @aircraft_code,
				@NextLocalDTTM, @CustomerID, @DepartmentID, @AuthorizationID
				
			END

			CLOSE curFleetCalendarExp;

			DEALLOCATE curFleetCalendarExp;
			
			SET @TEMPTODAY = @DATEFROM
			WHILE (@TEMPTODAY <= @DATETO)
			BEGIN			
				INSERT INTO #TempTable (tail_nmbr, activedate, depicao_id) 
				SELECT DISTINCT TempT.TailNum, @TEMPTODAY, dbo.GetHomeBaseCDByUserCD(LTRIM(@UserCD))
				FROM #TempTailNum TempT
				WHERE NOT EXISTS(SELECT T.tail_nmbr FROM #TempTable T 
					WHERE T.tail_nmbr = TempT.TailNum AND T.activedate = @TEMPTODAY 
				)
				SET @TEMPTODAY = @TEMPTODAY + 1
			END 
			
	 SELECT 
			tail_nmbr = T.tail_nmbr 
			,activedate = T.activedate
			,DepartureDTTMLocal AS locdep 
			,depicao_id 
			,ArrivalDTTMLocal AS locarr 
			,arricao_id 
			,requestor
			,D.DepartmentCD AS dept_code
			,cat_code 
			,duty_type 
			,ISNULL(elp_time,0) elp_time
			,ISNULL(timeapprox,0) timeapprox
			,ISNULL(pax_total,0) pax_total 
			,ISNULL(ron,'FALSE') ron
			,pic_code 
			,sic_code 
			,ISNULL(orig_nmbr,0) orig_nmbr
			,ISNULL(leg_num,0) leg_num
			,ISNULL(legid,0) legid 
			,type_code 
			,vendcode 
			,[desc] 
			,rec_type 
			,ISNULL(trueron,'FALSE') trueron
			,nextloc nextloc
			,ac_code 
			,DA.AuthorizationCD AS auth_code
			,ISNULL(resv_avail,0)  resv_avail
			,purpose 
			,ISNULL([private],0) [private]
			,ISNULL(ordernum,0) ordernum
			,aircraft_code 
			,additionalcrew = ''''
	        ,overnight = ''''
	        ,[grndtime] = 0
			,[dayfill] = 0
			,Notes = ''''
			FROM #TempTable T
			LEFT OUTER JOIN Department D ON T.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN  DepartmentAuthorization DA ON T.AuthorizationID = DA.AuthorizationID 
			ORDER BY tail_nmbr, activedate 
	
	--EXEC spGetReportPREFleetCalendarAircraftExportInformation 'jwilliams_13', '2012-07-20', '2012-07-22', '', '', '', 0
	--EXEC spGetReportPREFleetCalendarAircraftExportInformation 'TIM', '2012-07-20', '2012-07-23', 'N331UV', '', '', 0

		












GO


