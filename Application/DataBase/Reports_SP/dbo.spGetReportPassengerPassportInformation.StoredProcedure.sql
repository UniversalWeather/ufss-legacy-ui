IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerPassportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerPassportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportPassengerPassportInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD char(5)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerPassportInformation
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Get Passenger Passport Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON

	SELECT 
		CustomerID
		,P.PassengerRequestorCD AS PassengerCD
		--,dbo.FlightPakDecrypt(IsNull(PassportNUM,'')) AS PassportNUM
		,IsNull(PassportNUM,'') AS PassportNUM
		--,[ExpiryDT] = CONVERT(varchar(10), PassportExpiryDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[ExpiryDT] = dbo.GetShortDateFormatByUserCD(@UserCD,PassportExpiryDT)
		,[Nationality] = dbo.GetCountryCDByCountryID(CPP.CountryID) --PassportCountry
		,IssueCity
		,Choice AS IsChoice
		,IssueDate = dbo.GetShortDateFormatByUserCD(@UserCD,IssueDT)
		
	FROM CrewPassengerPassport CPP
		INNER JOIN (
			SELECT PassengerRequestorID, PassengerRequestorCD FROM Passenger
		) P ON CPP.PassengerRequestorID = P.PassengerRequestorID
	WHERE IsDeleted = 0
	AND CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND P.PassengerRequestorCD = RTRIM(@PassengerCD)

-- EXEC spGetReportPassengerPassportInformation 'TIM', 'CMAL'


GO


