IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBSIFLRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBSIFLRate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[spGetReportDBSIFLRate] 
(
        @UserCD AS VARCHAR(30), --Mandatory
        @UserCustomerID AS VARCHAR(30)--Mandatory
)
AS
-- ===============================================================================
-- SPC Name: [spGetReportSIFLRate]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get SIFL Rate
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin
FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
SELECT 
  [StartDate]	=StartDT
, [EndDate]	=EndDT
, [FareLevel1]=Rate1
, [FareLevel2]=Rate2
, [FareLevel3]=Rate3
, [TerminalCharge]	=TerminalCHG
, [TenToMin] = @TenToMin
FROM FareLevel
WHERE CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND CustomerID = CONVERT(BIGINT,@UserCustomerID)
-- And IsInActive=0
 And IsDeleted=0

--EXEC spGetReportDBSIFLRate 'SUPERVISOR_99','10099'


GO


