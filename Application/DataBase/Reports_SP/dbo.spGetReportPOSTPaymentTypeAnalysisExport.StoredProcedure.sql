IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPaymentTypeAnalysisExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPaymentTypeAnalysisExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTPaymentTypeAnalysisExport]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@PayablesVendor AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@PaymentType AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTPaymentTypeAnalysisExport
-- Author:  A.Akhila
-- Create date: 18 Feb 2013
-- Description: Get Payment Type Analysis for EXPORT REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================

SET NOCOUNT ON
SELECT 
[name] = V.Name
,[purchasedt] = POE.PurchaseDT
,[tailnmbr] = F.TailNum
,[acctnum] = A.AccountNum
,[payvendor] = V.VendorCD
,[paytype] = PT.PaymentTypeCD
,[desc] = PT.PaymentTypeDescription
,[acctsdesc] = A.AccountDescription
,[expamt] = POE.ExpenseAMT
FROM PostflightExpense POE
INNER JOIN PostflightMain POM ON POM.POLogID = POE.POLogID AND POM.IsDeleted=0
LEFT OUTER JOIN PaymentType PT ON PT.PaymentTypeID = POE.PaymentTypeID
LEFT OUTER JOIN Vendor V ON V.VendorID = POE.PaymentVendorID
INNER JOIN Account A ON A.AccountID = POE.AccountID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID
WHERE POE.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POE.IsDeleted=0
AND CONVERT(DATE,POE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND (V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PayablesVendor, ',')) OR @PayablesVendor = '')
AND (PT.PaymentTypeCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaymentType, ',')) OR @PaymentType = '')
AND (V.Name IS NOT NULL OR PT.PaymentTypeCD IS NOT NULL)
--EXEC spGetReportPOSTPaymentTypeAnalysisExport 'SUPERVISOR_99', '2009-1-1', '2010-1-1', '', ''




GO


