IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTWorkLoadIndexInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTWorkLoadIndexInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[spGetReportPOSTWorkLoadIndexInformation](@UserCD AS VARCHAR(30),
											 @UserCustomerID BIGINT,
											  @DATEFROM AS DATETIME,
											  @DATETO  AS DATETIME,
											  @CrewCD AS NVARCHAR(500)='',
											  @CrewGroupCD AS NVARCHAR(500)= '',
											  @AircraftCD AS NVARCHAR(500)= '',
											  @IncludeAllLegs AS BIT = 0
		  )

AS

--- =============================================
-- SPC Name: spGetReportPOSTWorkLoadIndexInformation
-- Author: Askar
-- Create date: 12 March 2013
-- Description: Get WorkLoandIndex
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
 -----------------------------Crew and Crew Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 


Declare @SuppressActivityCrew BIT,@AftBasis NUMERIC(1,0); 

SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

SELECT @AftBasis=AircraftBasis FROM Company WHERE CustomerID=@UserCustomerID 
                                           AND HomebaseID=(CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

DECLARE  @TempCrewID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		CrewCD VARCHAR(5),
		CrewMember VARCHAR(60),
		CustomerID BIGINT,
		HomeBaseID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO @TempCrewID
	SELECT DISTINCT C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
	FROM Crew C 
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
	AND C.IsDeleted=0
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID--F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND C.IsDeleted=0 
	AND C.IsStatus=1

END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = '' 
BEGIN 

INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
	FROM Crew C 
	WHERE C.CustomerID = @UserCustomerID
	AND C.IsDeleted=0
END




-----------------------------Crew and Crew Group Filteration----------------------

								   

---------------------------------Ron Calculation----------------------------

  CREATE  TABLE #CrewInfoTable(
                               CrewMember VARCHAR(60),
                               CrewID BIGINT,
                               DutyType CHAR(2),
                               PoLogID BIGINT,
                               LogNum BIGINT,
                               LegNum BIGINT,
                               Depart DATETIME,
                               Arrive DATETIME,
                               NextLocal DATETIME,
                               RecordType CHAR(1),
                               IsDutyEnd BIT,
                               PoLegID BIGINT,
                               TripDays INT,
                               DaysAway INT,
                               PrevDep DATE,
                               Arrival DATE
                               )
                               


  INSERT INTO #CrewInfoTable
  SELECT DISTINCT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  PL.DutyTYPE,
				  PM.TripID,
				  PM.TripNUM,
				  PL.LegNUM,
				  PL.DepartureDTTMLocal,
				  PL.ArrivalDTTMLocal,
				  PL.NextLocalDTTM,
				  PM.RecordType,
				  NULL,
				  PL.LegID,
				  0,
				  0,
				  NULL,
				  NULL
   FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                          INNER JOIN PreflightCrewList PC ON PC.LegID=PL.LegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                          LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                         -- INNER JOIN (SELECT * FROM CrewDutyType WHERE IsWorkLoadIndex=1 AND IsOfficeDuty=0 AND CustomerID=@CUSTOMERID)CD ON CD.DutyTypeCD=PL.DutyTYPE
                          
         WHERE 
 (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND PM.RecordType='C'
             AND (A.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
             AND PM.IsDeleted = 0

             --AND PL.IsDutyEnd=1  
             
                 

IF @IncludeAllLegs=0
BEGIN

 INSERT INTO #CrewInfoTable
   SELECT  DISTINCT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  NULL DutyTYPE,
				  PM.POLogID,
				  PM.LogNum,
				  PL.LegNUM,
				  PL.OutboundDTTM,
				  PL.InboundDTTM,
				  NULL NextLocal,--PL.NextDTTM
				  PM.RecordType,
				  PL.IsDutyEnd,
				  PL.POLegID,
				  DATEDIFF(DAY,PL.OutboundDTTM,PL.InboundDTTM)+1,
				  0,
				  PL.OutboundDTTM,
				  PL.InboundDTTM
   FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                          LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                          LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                          
         WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND (A.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
             AND PL.LegNUM=1
             AND PM.IsDeleted = 0




   
   INSERT INTO #CrewInfoTable
   SELECT DISTINCT  C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  NULL,
				  PM.POLogID,
				  PM.LogNum,
				  PL1.LegNUM,
				  PL1.OutboundDTTM,
				  PL1.InboundDTTM,
				  NULL,--PL.NextDTTM
				  PM.RecordType,
				  PL1.IsDutyEnd,
				  PL1.POLegID,
				  CASE WHEN CONVERT(DATE,ISNULL(CI.Arrive,PL.InboundDTTM))=CONVERT(DATE,PL1.OutboundDTTM) THEN DATEDIFF(DAY,PL1.OutboundDTTM,PL1.InboundDTTM)ELSE DATEDIFF(DAY,PL1.OutboundDTTM,PL1.InboundDTTM)+1 END,
	              0,
	              CASE WHEN PL.InboundDTTM <=@DATEFROM THEN @DATEFROM ELSE PL.InboundDTTM END,
	              CASE WHEN PL1.InboundDTTM >=@DATETO THEN @DATETO ELSE PL1.InboundDTTM END
   FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0 
                          LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          LEFT OUTER JOIN #CrewInfoTable CI ON CI.CrewID=C.CrewID AND CI.PoLegID=PL.POLegID
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                          LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                          INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM AND PL1.IsDeleted = 0
                          
         WHERE   (	
					(CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
				 ) 
                                        
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND (A.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
             AND PM.IsDeleted = 0
           


INSERT INTO #CrewInfoTable
SELECT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID,PL.DutyTYPE,PL.POLogID,CI.LogNum,PL.LegNUM,PL.OutboundDTTM
      ,PL.InboundDTTM ,NULL,CI.RecordType,CI.IsDutyEnd,PL.POLegID,0,0,NULL,NULL
   
 FROM PostflightLeg PL 
	INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
	INNER JOIN Crew C ON C.CrewID=PC.CrewID
	INNER JOIN #CrewInfoTable CI ON PL.POLogID=CI.PoLogID AND CI.LegNum-1=PL.LegNUM
	INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
 WHERE PL.POLegID NOT IN (SELECT DISTINCT ISNULL(POLegID,0) FROM #CrewInfoTable)
 AND PL.IsDeleted = 0


END

ELSE

BEGIN


  INSERT INTO #CrewInfoTable
   SELECT DISTINCT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  PL.DutyTYPE,
				  PM.POLogID,
				  PM.LogNum,
				  PL.LegNUM,
				  PL.OutboundDTTM,
				  PL.InboundDTTM,
				  NULL,--PL.NextDTTM
				  PM.RecordType,
				  PL.IsDutyEnd,
				  PL.POLegID,
				  CASE WHEN PL.LegNUM=1 THEN  DATEDIFF(DAY,PL.OutboundDTTM,PL.InboundDTTM)+1 ELSE 0 END,
				  0,
				  PL.OutboundDTTM,
				  PL.InboundDTTM
   FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                          LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                           LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                          
         WHERE PM.LogNum IN (SELECT DISTINCT TEMP.*FROM (
															SELECT  PM.LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																								   WHERE PM.CustomerID=@CUSTOMERID
																									 AND PL.LegNUM=1
																									 AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
																									 AND PM.IsDeleted = 0
															UNION ALL

															SELECT  PM.LogNum FROM PostflightMain PM 
																					 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																					 INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM
															                          
																	 WHERE PM.CustomerID=@CUSTOMERID
																	 AND PM.IsDeleted = 0
																	 AND   (	
																				(CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
																			  OR
																				(CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
																			  OR
																				(CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
																			 ) 

															)TEMP)
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND (A.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
             AND PM.IsDeleted = 0

END


IF @IncludeAllLegs=1
BEGIN
UPDATE #CrewInfoTable SET TripDays=TD.TripDays FROM 
(SELECT PL1.POLogID,PL1.POLegID,PL1.LegNUM,CASE WHEN CONVERT(DATE,ISNULL(CI.Arrive,CI.Arrive))=CONVERT(DATE,PL1.OutboundDTTM) THEN DATEDIFF(DAY,PL1.OutboundDTTM,PL1.InboundDTTM)ELSE DATEDIFF(DAY,PL1.OutboundDTTM,PL1.InboundDTTM)+1 END TripDays
         FROM PostflightLeg PL1 INNER JOIN #CrewInfoTable CI ON CI.PoLogID=PL1.POLogID AND CI.LegNum+1=PL1.LegNUM AND PL1.IsDeleted = 0)TD
         WHERE TD.POLogID=#CrewInfoTable.PoLogID
           AND TD.POLegID=#CrewInfoTable.PoLegID
           AND #CrewInfoTable.LegNum <> 1

END

 UPDATE #CrewInfoTable SET NextLocal=TEMP.OutboundDTTM FROM
	(SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.OutboundDTTM,PL.LegNUM FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                                                                        WHERE PM.CustomerID= @UserCustomerID AND PM.IsDeleted = 0)TEMP
                 WHERE #CrewInfoTable.LEGNUM+1=TEMP.LegNUM
                   AND #CrewInfoTable.POLOGID=TEMP.POLogID 
                   AND #CrewInfoTable.LOGNUM=TEMP.LogNum 
                   



--SELECT CrewID,ROW_NUMBER()OVER(PARTITION BY CREWID,LogNum ORDER BY CREWID,LogNum)Rnk FROM #CrewInfoTable

DECLARE @CurDate TABLE(DateWeek DATE,CrewCD VARCHAR(5),CrewMember VARCHAR(60),CrewID BIGINT)

IF @IncludeAllLegs=1
BEGIN
DECLARE @FrDate DATE,@ToDate DATE;
SELECT DISTINCT @FrDate= MIN(TEMP.OutboundDTTM),@ToDate=MAX(TEMP.InboundDTTM) FROM (
							SELECT PL.OutboundDTTM,PL.InboundDTTM  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																   WHERE PM.CustomerID=@CUSTOMERID
																	 AND PL.LegNUM=1
																	 AND PM.LogNum IN (SELECT DISTINCT LogNum FROM #CrewInfoTable)
																	 AND PM.IsDeleted = 0
																	 ---AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
							UNION ALL

							SELECT  PL1.OutboundDTTM,PL1.InboundDTTM FROM PostflightMain PM 
													 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
													 INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM AND PL.IsDeleted = 0
							                          
									 WHERE PM.CustomerID=@CUSTOMERID
									 AND PM.IsDeleted = 0
									 AND   (	
												(CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
											  OR
												(CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
											  OR
												(CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
											 ) 

															)TEMP


INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID FROM Crew C  
                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@FrDate,@ToDate) ) T
                WHERE C.CustomerID=@UserCustomerID 
                  AND C.IsDeleted=0
                  AND C.IsStatus=1
END
ELSE 
BEGIN
INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID FROM Crew C  
                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@DateFrom,@DateTo) ) T
                WHERE C.CustomerID=@UserCustomerID 
                  AND C.IsDeleted=0
                  AND C.IsStatus=1

 END
 CREATE TABLE  #tblCrew(CrewCD VARCHAR(5),
                      CrewMember VARCHAR(60),
                      DateWeek DATETIME,
                      Arrive DATETIME,
                      DutyType CHAR(2),
                      CrewID BIGINT,
                      RecordType CHAR(1),
                      Office_Hrs NUMERIC(7,3)
                      )


INSERT INTO #tblCrew                
SELECT DISTINCT CD.CrewCD,CD.CrewMember, 
               CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END, 
               CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE DateWeek END,
				CASE WHEN CI.RecordType='C' THEN CI.DutyType  ELSE  'R' END  DutyType,CD.CrewID,CI.RecordType,
				ROUND(DATEDIFF(MINUTE,(CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END),
				            (CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59') END))/ 60.0,1)
				FROM @CurDate CD LEFT JOIN #CrewInfoTable CI ON CD.CrewID=CI.CrewID
				                 INNER JOIN Crew C ON C.CrewID=CI.CrewID
				                 --INNER JOIN CrewDutyType CDT ON CDT.CustomerID=C.CustomerID
			    WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,CASE WHEN CI.RecordType='C' THEN CI.Depart  ELSE CI.Arrive END)
				 AND CONVERT(DATE,CD.DateWeek) <= CONVERT(DATE,CASE WHEN CI.RecordType='C' THEN CI.Arrive ELSE CI.NextLocal END )
				 AND RecordType='C'

INSERT INTO #tblCrew  
SELECT DISTINCT CD.CrewCD,CD.CrewMember,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END,
                             CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE DateWeek END,'R' DutyType,CD.CrewID,CI.RecordType,
                 ROUND(DATEDIFF(MINUTE,(CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END),
				            (CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59') END))/ 60.0,1)
				FROM @CurDate CD LEFT JOIN #CrewInfoTable CI ON CD.CrewID=CI.CrewID
			    WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,CI.Arrive)
				 AND CONVERT(DATE,CD.DateWeek) < CONVERT(DATE, ISNULL(CI.NextLocal,CI.PrevDep) )
				 AND IsDutyEnd=1

---------------------------------------Main Query--------------------------

  DECLARE  @InfoTable TABLE (
                               CrewID BIGINT,
                               CrewCD VARCHAR(5),
                               CrewMember VARCHAR(60),
                               LegNum BIGINT,
                               FlightHours NUMERIC(7,3),
                               BlocHours NUMERIC(7,3),
                               DutyHours NUMERIC(7,3),
                               HomeBase CHAR(4),
                               DaysAway INT,
                               FlightDays INT,
                               RonWeekDayCount INT,
                               RonWeekEndCount INT,
                               DutyType CHAR(2),
                               Office_Hrs NUMERIC(7,3),
                               WeekEndPts NUMERIC(7,3),
                               ValuePTS   NUMERIC(7,3),
                               WkLoad NUMERIC(7,3),
                               Ron INT,
                               Rnk INT,
                               DutyTypeCount INT
                               ) 
                               

IF @IncludeAllLegs=0
 
BEGIN       
                
INSERT INTO @InfoTable 
SELECT DISTINCT TEMP.CrewID,TEMP.CrewCD,TEMP.CrewMember,TEMP.Legs,TEMP.FlightHours,TEMP.BlockHours,TEMP.DutyHrs,TEMP.HomeBase,TEMP.DaysAway,TEMP.FlightDays
      ,TEMP.RonWeekDayCount,TEMP.RonWeekEndCount,TEMP.DutyType,TEMP.OfficeHrs,TEMP.WeekEndPts,TEMP.ValuePTS,TEMP.WkLoad,TEMP.Ron
      ,ROW_NUMBER() OVER (PARTITION BY TEMP.CrewID ORDER BY TEMP.CrewID) Rnk,NULL DutyTypeCount FROM (                           
SELECT DISTINCT ISNULL(CREW1.CrewID,TC.CrewID) CrewID,ISNULL(Crew1.CrewCD,TC.CrewCD) CrewCD,ISNULL(Crew1.CrewMember,TC.CrewMember) CrewMember,Crew1.Legs,Crew1.FlightHours,Crew1.DutyHrs,ISNULL(Crew1.HomeBase,A.IcaoID) HomeBase,ISNULL(Crew1.DaysAway,DATEDIFF(DAY,T.Arrive,T.DateWeek)+1 ) DaysAway,Crew1.FlightDays,
       RonWeekDayCount.CountVal RonWeekDayCount,RonWeekEndCount.CountVal RonWeekEndCount,ISNULL(RonWeekDayCount.DutyType,RonWeekEndCount.DutyType) DutyType,0 OfficeHrs,NULL WeekEndPts,NULL ValuePTS,NULL WkLoad,NULL Ron,NULL DutyTypeCount,BlockHours
       FROM @TempCrewID TC 
       INNER JOIN Airport A ON TC.HomeBaseID=A.AirportID
       LEFT OUTER JOIN #tblCrew T ON TC.CrewID=T.CrewID
       LEFT OUTER JOIN 
			(
			SELECT CREW.CrewID,Crew.CrewCD,CREW.CrewMember,COUNT(Legs)Legs,SUM(BlockHours)BlockHours,SUM(FlightHours)FlightHours,SUM(DutyHrs)DutyHrs,HomeBase,SUM(DaysAway)DaysAway,SUM(FlightDays) FlightDays
			 FROM
						(
								SELECT DISTINCT  PL.OutboundDTTM,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,PC.CrewID,COUNT(PL.LegNUM)Legs,SUM(ROUND(PC.FlightHours,1))FlightHours,CI.DaysAway DaysAway,
									   CASE WHEN PL.IsDutyEnd=1 THEN  SUM(ROUND(PC.DutyHours,1)) ELSE 0 END DutyHrs,A.IcaoID HomeBase,CI.TripDays FlightDays,SUM(ROUND(PC.BlockHours,1)) BlockHours
														FROM PostflightMain PM 
															 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
															 INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID 
															 INNER JOIN @TempCrewID TC ON TC.CrewID=PC.CrewID
															 INNER JOIN (SELECT  CrewID,NextLocal,Depart,PoLegID,Arrive,TripDays,DaysAway FROM #CrewInfoTable) CI ON TC.CrewID=CI.CrewID AND PC.POLegID=CI.PoLegID
															 INNER JOIN Crew C ON PC.CrewID=C.CrewID
															 INNER JOIN Airport A ON TC.HomebaseID=A.AirportID
															 LEFT OUTER JOIN Aircraft AT ON PM.AircraftID=AT.AircraftID
														WHERE PM.CustomerID=@CUSTOMERID 
														--  AND PM.RecordType='L'
														  AND (AT.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
														  AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
														  AND PM.IsDeleted = 0
														GROUP BY C.CrewCD,C.LastName,C.FirstName,PC.CrewID,PL.IsDutyEnd,A.IcaoID,PC.DaysAway,PL.OutboundDTTM,PL.InboundDTTM,CI.NextLocal,CI.Depart,CI.Arrive,CI.TripDays,CI.DaysAway
						 )Crew 
						 GROUP BY CREW.CrewID,CREW.HomeBase,CREW.CrewMember,Crew.CrewCD 
					)Crew1 ON TC.CrewID=Crew1.CrewID
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal FROM #tblCrew TC
																			   WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) NOT IN(7,1) 
																			   GROUP BY CrewMember,DutyType,TC.CrewID)RonWeekDayCount ON RonWeekDayCount.CrewID=TC.CrewID
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal FROM #tblCrew TC
																					WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) IN(7,1) 
																					 GROUP BY CrewMember,DutyType,TC.CrewID)RonWeekEndCount ON RonWeekEndCount.CrewID=TC.CrewID


		

UNION ALL

SELECT TC.CrewID,TC.CrewCD,TC.CrewMember,0 Legs, 0 FlightHours,0 DutyHrs,A.IcaoID,0 DaysAway,0 FlightDays,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =6 OR DATEPART(Dw, DateWeek) =5 OR DATEPART(Dw, DateWeek) =4 OR DATEPART(Dw, DateWeek) =3 OR DATEPART(Dw, DateWeek) =2 THEN 1 ELSE 0 END)  WeekdayCount,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =7 OR DATEPART(Dw, DateWeek) =1 THEN 1 ELSE 0 END )WeekEndCount,TC.DutyType,
       SUM(TC.Office_Hrs),NULL,NULL,NULL,NULL,NULL,NULL
                            FROM Crew C 
                             INNER JOIN #tblCrew TC  ON TC.CrewID=C.CrewID
                             INNER JOIN Airport A ON A.AirportID=C.HomebaseID
                             WHERE DutyType <>'R'
							GROUP BY CrewMember,DutyType,A.IcaoID,TC.DutyType,TC.CrewID,TC.CrewCD

 
)TEMP
END

ELSE

BEGIN
INSERT INTO @InfoTable 
SELECT TEMP.CrewID,TEMP.CrewCD,TEMP.CrewMember,TEMP.Legs,TEMP.FlightHours,TEMP.BlockHours,TEMP.DutyHrs,TEMP.HomeBase,TEMP.DaysAway,TEMP.FlightDays
      ,TEMP.RonWeekDayCount,TEMP.RonWeekEndCount,TEMP.DutyType,TEMP.OfficeHrs,TEMP.WeekEndPts,TEMP.ValuePTS,TEMP.WkLoad,TEMP.Ron
      ,ROW_NUMBER() OVER (PARTITION BY TEMP.CrewID ORDER BY TEMP.CrewID) Rnk,NULL DutyTypeCount
        

 FROM 
(                           
SELECT DISTINCT CREW1.CrewID,Crew1.CrewCD,Crew1.CrewMember,Crew1.Legs,Crew1.FlightHours,Crew1.DutyHrs,Crew1.HomeBase,Crew1.DaysAway,Crew1.FlightDays,
       RonWeekDayCount.CountVal RonWeekDayCount,RonWeekEndCount.CountVal RonWeekEndCount,ISNULL(RonWeekDayCount.DutyType,RonWeekEndCount.DutyType) DutyType,0 OfficeHrs,NULL WeekEndPts,NULL ValuePTS,NULL WkLoad,NULL Ron,NULL DutyTypeCount,BlockHours
       FROM 
(
SELECT CREW.CrewID,Crew.CrewCD,CREW.CrewMember,COUNT(Legs)Legs,SUM(BlockHours)BlockHours,SUM(FlightHours)FlightHours,SUM(DutyHrs)DutyHrs,HomeBase,SUM(DaysAway)DaysAway,COUNT(DISTINCT CONVERT(DATE,FlightDays)) FlightDays 
 FROM
			(
					SELECT PL.OutboundDTTM,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,PC.CrewID,COUNT(PL.LegNUM)Legs,SUM(ROUND(PC.FlightHours,1))FlightHours,DATEDIFF(DAY, PL.OutboundDTTM,PL.InboundDTTM)+1 DaysAway,
						   CASE WHEN PL.IsDutyEnd=1 THEN  SUM(ROUND(PC.DutyHours,1)) ELSE 0 END DutyHrs,A.IcaoID HomeBase,PL.OutboundDTTM FlightDays,SUM(ROUND(PC.BlockHours,1)) BlockHours
											FROM PostflightMain PM 
												 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
												 INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID 
												 INNER JOIN @TempCrewID TC ON TC.CrewID=PC.CrewID
												 INNER JOIN Crew C ON PC.CrewID=C.CrewID
												 INNER JOIN Airport A ON TC.HomebaseID=A.AirportID
												 LEFT OUTER JOIN Aircraft AT ON PM.AircraftID=AT.AircraftID
											WHERE PM.CustomerID=@CUSTOMERID 
												AND PM.IsDeleted = 0
											--  AND PM.RecordType='L'
											  AND (AT.AircraftCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
											  AND PM.LogNum IN (SELECT DISTINCT TEMP.*FROM (
																			SELECT  PM.LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																												   WHERE PM.CustomerID=@CUSTOMERID
																													 AND PL.LegNUM=1
																													 AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
																													 AND PM.IsDeleted = 0
																			UNION ALL

																			SELECT  PM.LogNum FROM PostflightMain PM 
																									 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																									 INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM AND PL1.IsDeleted = 0
																			                          
																					 WHERE PM.CustomerID=@CUSTOMERID
																					 AND   (	
																								(CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
																							  OR
																								(CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
																							  OR
																								(CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
																							 )
																					AND PM.IsDeleted = 0 

																			)TEMP)
											GROUP BY C.CrewCD,C.LastName,C.FirstName,PC.CrewID,PL.IsDutyEnd,A.IcaoID,PC.DaysAway,PL.OutboundDTTM,PL.InboundDTTM
			 )Crew 
			 GROUP BY CREW.CrewID,CREW.HomeBase,CREW.CrewMember,Crew.CrewCD
		)Crew1
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal FROM #tblCrew TC
																			   WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) NOT IN(7,1) 
																			   GROUP BY CrewMember,DutyType,TC.CrewID)RonWeekDayCount ON RonWeekDayCount.CrewID=Crew1.CrewID
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal FROM #tblCrew TC
																					WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) IN(7,1) 
																					 GROUP BY CrewMember,DutyType,TC.CrewID)RonWeekEndCount ON RonWeekEndCount.CrewID=Crew1.CrewID


		

UNION ALL

SELECT TC.CrewID,TC.CrewCD,TC.CrewMember,0 Legs, 0 FlightHours,0 DutyHrs,A.IcaoID,0 DaysAway,0 FlightDays,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =6 OR DATEPART(Dw, DateWeek) =5 OR DATEPART(Dw, DateWeek) =4 OR DATEPART(Dw, DateWeek) =3 OR DATEPART(Dw, DateWeek) =2 THEN 1 ELSE 0 END)  WeekdayCount,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =7 OR DATEPART(Dw, DateWeek) =1 THEN 1 ELSE 0 END )WeekEndCount,TC.DutyType,
       SUM(TC.Office_Hrs),NULL,NULL,NULL,NULL,NULL,NULL
                            FROM Crew C 
                             INNER JOIN #tblCrew TC  ON TC.CrewID=C.CrewID
                             INNER JOIN Airport A ON A.AirportID=C.HomebaseID
                             WHERE DutyType <>'R'
							GROUP BY CrewMember,DutyType,A.IcaoID,TC.DutyType,TC.CrewID,TC.CrewCD
							
)TEMP							
END 

IF @IncludeAllLegs=1
BEGIN
UPDATE @InfoTable SET FlightDays=DD.TripDays FROM(
										SELECT CrewID Crew,SUM(TripDays)TripDays FROM 
										(
										SELECT CrewID,TripDays FROM #CrewInfoTable ---GROUP BY LogNum,CrewID,TripDays
										)DA
										GROUP BY CrewID
								    )DD
	               WHERE DD.Crew=CrewID
	               AND Rnk=1

END


UPDATE @InfoTable SET DaysAway=DD.DaysAway FROM(
										SELECT CrewID Crew,SUM(DA.DaysAway) DaysAway FROM 
										(
										SELECT CrewID,DATEDIFF(DAY,MIN(PrevDep),MAX(Arrival))+1 DaysAway FROM #CrewInfoTable GROUP BY LogNum,CrewID
										)DA
										GROUP BY CrewID
								    )DD
	               WHERE DD.Crew=CrewID
	               AND Rnk=1



UPDATE @InfoTable SET WeekEndPts=ISNULL(ISNULL(RonWeekEndCount,0),0)*ISNULL(CONVERT(NUMERIC(2,0),ISNULL(Pts.WeekendPTS,0)),0),ValuePTS=ISNULL(ISNULL(RonWeekDayCount,0),0)*ISNULL(CONVERT(NUMERIC(2,0),ISNULL(Pts.ValuePTS,0)),0)
                FROM 
                   (SELECT DISTINCT DutyTypeCD,WeekendPTS,ValuePTS FROM CrewDutyType WHERE CustomerID=@CUSTOMERID AND IsOfficeDuty=0 AND IsWorkLoadIndex=1)Pts
                 WHERE Pts.DutyTypeCD=DutyType


UPDATE @InfoTable SET Office_Hrs=WK.Office_Hrs,WkLoad=WK.WkLoad,FlightHours=WK.FlightHours,DutyHours=WK.DutyHours,DaysAway=WK.DaysAway,
                     FlightDays=WK.FlightDays,LegNum=WK.LegNum
                  FROM (
                  SELECT WK1.Crew,SUM(WK1.Office_Hrs)Office_Hrs,SUM(WK1.WkLoad)WkLoad,SUM(WK1.FlightHours)FlightHours,SUM(WK1.DutyHours) DutyHours,SUM(WK1.FlightDays)FlightDays,SUM(WK1.DaysAway)DaysAway,SUM(WK1.LegNum)LegNum
                     FROM
                    (SELECT CrewID Crew,CrewMember,SUM(ISNULL(CONVERT(NUMERIC(7,3),CASE WHEN @AftBasis=2 THEN  FlightHours ELSE BlocHours END),0))+SUM(ISNULL(CONVERT(NUMERIC(7,3),DutyHours),0))+SUM(ISNULL(CONVERT(NUMERIC(7,3),T.WeekEndPts),0))+SUM(ISNULL(CONVERT(NUMERIC(7,3),T.ValuePTS),0))+SUM(ISNULL( CASE WHEN CD.IsOfficeDuty=1 THEN  ISNULL(CONVERT(NUMERIC(7,3),T.Office_Hrs),0) ELSE 0 END ,0))+SUM(ISNULL(CONVERT(NUMERIC(7,3),LegNum),0)) WkLoad,
							   CASE WHEN CD.IsOfficeDuty=1 THEN  T.Office_Hrs ELSE 0 END  Office_Hrs,SUM(ISNULL(FlightHours,0)) FlightHours,SUM(ISNULL(DutyHours,0)) DutyHours,SUM(ISNULL(LegNum,0)) LegNum,
							   SUM(T.DaysAway)DaysAway,SUM(T.FlightDays)FlightDays
							  FROM @InfoTable T
								   LEFT OUTER JOIN(SELECT DISTINCT * FROM  CrewDutyType  WHERE CustomerID=@CUSTOMERID) CD ON T.DutyType=CD.DutyTypeCD
							  GROUP BY CrewMember,CrewID,CD.IsOfficeDuty,T.Office_Hrs
					)WK1 
				GROUP BY Crew
				  )WK
				 WHERE WK.Crew=CrewID



UPDATE @InfoTable SET Ron=RCount.RonCount 
                  FROM (SELECT CrewID Crew,SUM(ISNULL(RonWeekDayCount,0))+SUM(ISNULL(RonWeekEndCount,0)) RonCount FROM @InfoTable WHERE DutyType='R' GROUP BY CrewID)RCount
                  WHERE RCount.Crew=CrewID

UPDATE @InfoTable SET DutyTypeCount=DC.DCount
                    FROM  (SELECT COUNT(DutyType)DCount,DutyType Duty FROM @InfoTable GROUP BY DutyType)DC
                  WHERE DC.Duty=DutyType



UPDATE @InfoTable SET RonWeekDayCount=NULL,RonWeekEndCount=NULL,DutyType=NULL
                   FROM(SELECT DISTINCT * FROM  CrewDutyType  WHERE CustomerID=@CUSTOMERID AND (IsOfficeDuty=1 OR(IsWorkLoadIndex=0 AND IsOfficeDuty=0))) CD 
                  WHERE DutyType=CD.DutyTypeCD

	 



IF @SuppressActivityCrew=0
BEGIN


	IF  EXISTS(SELECT DutyTypeCD FROM CrewDutyType WHERE CustomerID=@CUSTOMERID 
																		 AND IsDeleted=0
																		  AND IsWorkLoadIndex=1
																		  AND IsOfficeDuty=0
																		  AND DutyTypeCD NOT IN('G','F','R')
					)

      BEGIN

		SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember=T.CrewMember ,
			   HomeBase=T.HomeBase,
			   WkLoadIndex=ISNULL(CASE WHEN Rnk=1 THEN T.WkLoad ELSE 0 END ,0),
			   DutyHours=ISNULL(CASE WHEN Rnk=1 THEN T.DutyHours ELSE 0 END ,0),
			   FlightHours=ISNULL(CASE WHEN Rnk=1 THEN (CASE WHEN @AftBasis=1 THEN T.BlocHours ELSE  T.FlightHours END) ELSE 0 END ,0),
			   BlockHours=ISNULL(CASE WHEN Rnk=1 THEN T.BlocHours ELSE 0 END ,0),
			   Legs=ISNULL(CASE WHEN Rnk=1 THEN T.LegNum ELSE 0 END,0),
			   OfficeHours=ISNULL(CASE WHEN Rnk=1 THEN T.Office_Hrs ELSE 0 END,0),
			   Ron=ISNULL(CASE WHEN Rnk=1 THEN T.Ron ELSE 0 END,0),
			   DaysAway=ISNULL(CASE WHEN Rnk=1 THEN T.DaysAway ELSE 0 END,0),
			   FlightDays=ISNULL(CASE WHEN Rnk=1 THEN T.FlightDays ELSE 0 END,0),
			   CASE WHEN T.DutyType='R' THEN NULL  ELSE T.DutyType END DutyType,
			   CASE WHEN T.DutyType='R' THEN NULL 
					WHEN T.DutyType <> 'R' AND  (T.RonWeekDayCount IS NOT NULL OR T.RonWeekEndCount IS NOT NULL) THEN ISNULL(T.RonWeekDayCount,0)+ISNULL(T.RonWeekEndCount,0)
					 ELSE NULL END OtherCount,
			   DutyTypeCount,@TenToMin TenToMin,@AftBasis  AftBasis 
		       
		  FROM @InfoTable T 
		  

		UNION ALL
		 
		 
		 
		SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   C.CrewID,
			   C.CrewCD,LastName+', '+ISNULL(FirstName,'') CrewMember,
			   A.IcaoID,
			   0,0,0,0,0,0,0,0,0,CD.DutyTypeCD,NULL,NULL,@TenToMin TenToMin,@AftBasis  AftBasis  
					  FROM Crew C
						   CROSS JOIN (SELECT DutyTypeCD FROM CrewDutyType WHERE CustomerID=@CUSTOMERID 
																			 AND IsDeleted=0
																			 AND IsWorkLoadIndex=1
																			 AND IsOfficeDuty=0
																			 AND DutyTypeCD NOT IN('G','F','R')
																			 AND NOT EXISTS(SELECT DutyType  FROM @InfoTable T WHERE T.DutyType = DutyTypeCD)

																			 )CD
						   INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
						   INNER JOIN Airport A ON C.HomebaseID=A.AirportID
																			 
						  WHERE C.CustomerID=@CUSTOMERID 
							AND C.IsDeleted=0
							AND IsStatus=1
							--AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(CrewID,0) FROM @InfoTable)
							AND DutyTypeCD NOT IN (SELECT DISTINCT ISNULL(DutyType,'') FROM @InfoTable)


		        

		END

		ELSE

		BEGIN


		SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember=T.CrewMember ,
			   HomeBase=T.HomeBase,
			   WkLoadIndex=ISNULL(CASE WHEN Rnk=1 THEN T.WkLoad ELSE 0 END ,0),
			   DutyHours=ISNULL(CASE WHEN Rnk=1 THEN T.DutyHours ELSE 0 END ,0),
			   FlightHours=ISNULL(CASE WHEN Rnk=1 THEN (CASE WHEN @AftBasis=1 THEN T.BlocHours ELSE  T.FlightHours END)  ELSE 0 END ,0),
			   BlockHours=ISNULL(CASE WHEN Rnk=1 THEN T.BlocHours ELSE 0 END ,0),
			   Legs=ISNULL(CASE WHEN Rnk=1 THEN T.LegNum ELSE 0 END,0),
			   OfficeHours=ISNULL(CASE WHEN Rnk=1 THEN T.Office_Hrs ELSE 0 END,0),
			   Ron=ISNULL(CASE WHEN Rnk=1 THEN T.Ron ELSE 0 END,0),
			   DaysAway=ISNULL(CASE WHEN Rnk=1 THEN T.DaysAway ELSE 0 END,0),
			   FlightDays=ISNULL(CASE WHEN Rnk=1 THEN T.FlightDays ELSE 0 END,0),
			   CASE WHEN T.DutyType='R' THEN NULL  ELSE T.DutyType END DutyType,
				CASE WHEN T.DutyType='R' THEN NULL 
					WHEN T.DutyType <> 'R' AND  (T.RonWeekDayCount IS NOT NULL OR T.RonWeekEndCount IS NOT NULL) THEN ISNULL(T.RonWeekDayCount,0)+ISNULL(T.RonWeekEndCount,0)
					 ELSE  NULL END OtherCount,
			   DutyTypeCount,@TenToMin TenToMin,@AftBasis  AftBasis  
		       
		  FROM @InfoTable T 

		UNION ALL
		 
		 
		 
		SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   C.CrewID,
			   C.CrewCD,LastName+', '+ISNULL(FirstName,'') CrewMember,
			   A.IcaoID,
			   0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,@TenToMin TenToMin ,@AftBasis  AftBasis 
					  FROM Crew C
						   INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
						   INNER JOIN Airport A ON C.HomebaseID=A.AirportID
																			 
						  WHERE C.CustomerID=@CUSTOMERID 
							AND C.IsDeleted=0
							AND IsStatus=1
							AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(CrewID,0) FROM @InfoTable)


		END

END

ELSE

BEGIN

IF  EXISTS(SELECT DutyTypeCD FROM CrewDutyType WHERE CustomerID=@CUSTOMERID 
																		 AND IsDeleted=0
																		  AND IsWorkLoadIndex=1
																		  AND IsOfficeDuty=0
																		  AND DutyTypeCD NOT IN('G','F','R'))

BEGIN

SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember=T.CrewMember ,
			   HomeBase=T.HomeBase,
			   WkLoadIndex=ISNULL(CASE WHEN Rnk=1 THEN T.WkLoad ELSE 0 END ,0),
			   DutyHours=ISNULL(CASE WHEN Rnk=1 THEN T.DutyHours ELSE 0 END ,0),
			   FlightHours=ISNULL(CASE WHEN Rnk=1 THEN (CASE WHEN @AftBasis=1 THEN T.BlocHours ELSE  T.FlightHours END)  ELSE 0 END ,0),
			   BlockHours=ISNULL(CASE WHEN Rnk=1 THEN T.BlocHours ELSE 0 END ,0),
			   Legs=ISNULL(CASE WHEN Rnk=1 THEN T.LegNum ELSE 0 END,0),
			   OfficeHours=ISNULL(CASE WHEN Rnk=1 THEN T.Office_Hrs ELSE 0 END,0),
			   Ron=ISNULL(CASE WHEN Rnk=1 THEN T.Ron ELSE 0 END,0),
			   DaysAway=ISNULL(CASE WHEN Rnk=1 THEN T.DaysAway ELSE 0 END,0),
			   FlightDays=ISNULL(CASE WHEN Rnk=1 THEN T.FlightDays ELSE 0 END,0),
			   CASE WHEN T.DutyType='R' THEN NULL  ELSE DutyType END DutyType,
				CASE WHEN T.DutyType='R' THEN NULL 
					WHEN T.DutyType <> 'R' AND  (T.RonWeekDayCount IS NOT NULL OR T.RonWeekEndCount IS NOT NULL) THEN ISNULL(T.RonWeekDayCount,0)+ISNULL(T.RonWeekEndCount,0)
					 ELSE  NULL END OtherCount,
			   DutyTypeCount,@TenToMin TenToMin,@AftBasis  AftBasis  
		       
		  FROM @InfoTable T 

UNION ALL


SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember,
			   HomeBase,
			   0 WkLoadIndex,
			   0 DutyHours,
			   0 FlightHours,
			   0 BlockHours,
			   0 Legs,
			   0 OfficeHours,
			   0 Ron,
			   0 DaysAway,
			   0 FlightDays,
			   CD.DutyTypeCD DutyType,
			   0 OtherCount,
			   0 DutyTypeCount,
			   @TenToMin TenToMin,
			   @AftBasis  AftBasis  
		       
		  FROM @InfoTable T 
		     CROSS JOIN (SELECT DutyTypeCD FROM CrewDutyType WHERE CustomerID=@CUSTOMERID 
																			 AND IsDeleted=0
																			 AND IsWorkLoadIndex=1
																			 AND IsOfficeDuty=0
																			 AND DutyTypeCD NOT IN('G','F','R')
																			  AND NOT EXISTS(SELECT DutyType  FROM @InfoTable T WHERE T.DutyType = DutyTypeCD)
																			 )CD
END

ELSE

BEGIN
SELECT DISTINCT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember=T.CrewMember ,
			   HomeBase=T.HomeBase,
			   WkLoadIndex=ISNULL(CASE WHEN Rnk=1 THEN T.WkLoad ELSE 0 END ,0),
			   DutyHours=ISNULL(CASE WHEN Rnk=1 THEN T.DutyHours ELSE 0 END ,0),
			   FlightHours=ISNULL(CASE WHEN Rnk=1 THEN (CASE WHEN @AftBasis=1 THEN T.BlocHours ELSE  T.FlightHours END)  ELSE 0 END ,0),
			   BlockHours=ISNULL(CASE WHEN Rnk=1 THEN T.BlocHours ELSE 0 END ,0),
			   Legs=ISNULL(CASE WHEN Rnk=1 THEN T.LegNum ELSE 0 END,0),
			   OfficeHours=ISNULL(CASE WHEN Rnk=1 THEN T.Office_Hrs ELSE 0 END,0),
			   Ron=ISNULL(CASE WHEN Rnk=1 THEN T.Ron ELSE 0 END,0),
			   DaysAway=ISNULL(CASE WHEN Rnk=1 THEN T.DaysAway ELSE 0 END,0),
			   FlightDays=ISNULL(CASE WHEN Rnk=1 THEN T.FlightDays ELSE 0 END,0),
			   CASE WHEN T.DutyType='R' THEN NULL  ELSE T.DutyType END DutyType,
				CASE WHEN T.DutyType='R' THEN NULL 
					WHEN T.DutyType <> 'R' AND  (T.RonWeekDayCount IS NOT NULL OR T.RonWeekEndCount IS NOT NULL) THEN ISNULL(T.RonWeekDayCount,0)+ISNULL(T.RonWeekEndCount,0)
					 ELSE  NULL END OtherCount,
			   DutyTypeCount,@TenToMin TenToMin,@AftBasis  AftBasis  
		       
		  FROM @InfoTable T 
END

END


IF OBJECT_ID('TEMPDB..#CrewInfoTable') IS NOT NULL 
DROP TABLE #CrewInfoTable

IF OBJECT_ID('TEMPDB..#tblCrew') IS NOT NULL 
DROP TABLE #tblCrew




 --DATEDIFF(HH,CD.WorkDate,CT.ActualDutyStartDTTM)
     
     
--select DutyTypeCD,WeekendPTS,ValuePTS,IsWorkLoadIndex,IsOfficeDuty from CrewDutyType where DutyTypeCD='va' and CustomerID=10099                                          

END  





--EXEC spGetReportPOSTWorkLoadIndexInformation 'supervisor_99','10099','2009-1-1','2010-1-1','','','',0





GO


