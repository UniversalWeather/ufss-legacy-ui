IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDetailBillingByTripInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDetailBillingByTripInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[spGetReportPOSTDetailBillingByTripInformation]  'JWILLIAMS_13','01-01-2009','03-01-2013','','','',0

CREATE PROCEDURE [dbo].[spGetReportPOSTDetailBillingByTripInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID AS BIGINT,
		@DATEFROM AS DATETIME = NULL , 
		@DATETO AS DATETIME = NULL, 
		@LogNum AS NVARCHAR(1000) = '',
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0
        --@TenToMin SMALLINT = 0
		)

AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailInformation
-- Author: 
-- Create date: 
-- Description: Get Department/Authorization Detail for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
	--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

IF @LogNum <> ''

BEGIN 

SELECT DISTINCT ISNULL(DE.DepartmentCD,'') AS DepartmentCD, 
ISNULL(DA.AuthorizationCD,'') AS AuthorizationCD,
P.PassengerRequestorCD,
CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [Tail Number],
CONVERT(DATE ,PL.OutboundDTTM) TRIPDATE,
PO.DispatchNum, 
PO.LogNum, 
PL.POLegID, 
PL.ScheduledTM,
PL.LegNUM,
AD.IcaoID AS DepartICAOCD ,
AA.IcaoID AS ArriveICAOCD,
PL.Distance, 
ROUND(PL.FlightHours,1) FlightHours, 
ROUND(PL.BlockHours,1)BlockHours,
PL.PassengerTotal AS PAX_TOT,
FL.MaximumPassenger - PL.PassengerTotal AS ES_TOTAL, 
FL.AircraftCD,
FC.ChargeUnit, 
FC.ChargeRate,
TOTAL_CHARGES = CASE WHEN FC.ChargeUnit = 'N' THEN  ((PL.Distance*FC.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
WHEN FC.ChargeUnit = 'S' THEN (((PL.Distance*FC.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
WHEN (FC.ChargeUnit = 'H' AND CO.AircraftBasis is null) THEN ISNULL(PFE11.SUMExpenseAmt,0)
END,
PAX.FlightPurposeCDS AS FlightPurposeCD,
PAX.PaxNames AS PassengerName ,
ACC.AccountDesc ,ACC.ExpenseAmt,
PFE11.IsBilling
,[TenToMin] = @TenToMin
FROM PostflightMain PO 
JOIN PostflightLeg PL 
  ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted=0
LEFT JOIN Passenger P
  ON PL.PassengerRequestorID = P.PassengerRequestorID AND PL.CustomerID = P.CustomerID
 LEFT JOIN Department DE ON DE.DepartmentID = PL.DepartmentID
 LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
    JOIN vFleetGroup FL 
  ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID
 INNER JOIN FleetChargeRate FC ON PO.FleetID=FC.FleetID 
LEFT JOIN TailHistory TH
  ON FL.TailNum = TH.OldTailNUM AND FL.CustomerID = TH.CustomerID  
    LEFT JOIN Company CO
  ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
    LEFT JOIN Airport AI
  ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
    LEFT JOIN Airport AD 
           ON PL.DepartICAOID = AD.AirportID         
    JOIN Airport AA 
           ON PL.ArriveICAOID = AA.AirportID 
LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                      SELECT ISNULL(SUM(PP2.ExpenseAMT),0) 
                               FROM PostflightExpense PP2 
   WHERE PP2.POLegID = PP4.POLegID)
                       FROM PostflightExpense PP4 WHERE IsBilling =1 
                       )PFE11
                      ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID  
LEFT  JOIN ( SELECT DISTINCT 
             PP2.POLegID,
             PaxNames = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH('')),
                      FlightPurposeCDS = (
                       SELECT ISNULL(P1.FlightPurposeCD,'')+'$$' 
                               FROM PostflightPassenger PP1 INNER JOIN FlightPurpose P1 ON PP1.FlightPurposeID=P1.FlightPurposeID 
                               WHERE PP1.POLegID = PP2.POLegID
                             FOR XML PATH('')
                             )
                              FROM PostflightPassenger PP2                         
  ) PAX ON PL.POLegID = PAX.POLegID  
LEFT JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,PP3.IsBilling ,ExpenseAmt = (
                      SELECT ISNULL(CAST(A.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                               FROM (SELECT PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, AccountDesc = P1.AccountDescription, ExpenseAmt = SUM(PP4.ExpenseAMT)
FROM PostflightExpense PP4 INNER JOIN Account P1 ON PP4.AccountID=P1.AccountID 
WHERE PP4.POLegID = PP3.POLegID AND PP4.IsBilling =1  
GROUP BY PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, P1.AccountDescription
)A                              
                               ORDER BY A.ExpenseAMT DESC
                             FOR XML PATH('')),
AccountDesc = (
SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' 
                               FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1
                             FOR XML PATH('')
                             )
                       FROM PostflightExpense PP3 WHERE PP3.IsBilling =1)
                       ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID
                       
   --  LEFT OUTER JOIN (
--SELECT POLegID, ChargeUnit, ChargeRate
--FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
--                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
--AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
   --        ) CR ON PL.POLegID = CR.POLegID                   
 
WHERE (PO.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '') 
  AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (DA.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')   
      AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  AND PO.CustomerID = @UserCustomerID
   AND PO.IsDeleted = 0

END

ELSE

BEGIN

SELECT DISTINCT ISNULL(DE.DepartmentCD ,'') AS DepartmentCD,
ISNULL(DA.AuthorizationCD,'') AS AuthorizationCD,
   P.PassengerRequestorCD,
CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [Tail Number],
CONVERT(DATE ,PL.OutboundDTTM) TRIPDATE,
PO.DispatchNum, 
PO.LogNum, 
PL.POLegID, 
PL.ScheduledTM,
PL.LegNUM,
AD.IcaoID AS DepartICAOCD ,
AA.IcaoID AS ArriveICAOCD,
PL.Distance, 
ROUND(PL.FlightHours,1) FlightHours, 
ROUND(PL.BlockHours,1) BlockHours,
PL.PassengerTotal AS PAX_TOT,
FL.MaximumPassenger - PL.PassengerTotal AS ES_TOTAL, 
FL.AircraftCD,
CR.ChargeUnit, 
CR.ChargeRate,
TOTAL_CHARGES = CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
WHEN (CR.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *CR.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
WHEN (CR.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *CR.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
WHEN (CR.ChargeUnit = 'H' AND CO.AircraftBasis is null) THEN ISNULL(PFE11.SUMExpenseAmt,0)
END,
PAX.FlightPurposeCDS AS FlightPurposeCD,
PAX.PaxNames AS PassengerName ,
ACC.AccountDesc ,ACC.ExpenseAmt,
PFE11.IsBilling
,[TenToMin] = @TenToMin
FROM PostflightMain PO 
JOIN PostflightLeg PL 
  ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted=0
LEFT JOIN Passenger P
  ON PL.PassengerRequestorID = P.PassengerRequestorID AND PL.CustomerID = P.CustomerID
 LEFT JOIN Department DE ON DE.DepartmentID = PL.DepartmentID
 LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
    JOIN vFleetGroup FL 
  ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID
LEFT JOIN TailHistory TH
  ON FL.TailNum = TH.OldTailNUM AND FL.CustomerID = TH.CustomerID  
    LEFT JOIN Company CO
  ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
    LEFT JOIN Airport AI
  ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
    LEFT JOIN Airport AD 
           ON PL.DepartICAOID = AD.AirportID         
    JOIN Airport AA 
           ON PL.ArriveICAOID = AA.AirportID 
LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                      SELECT ISNULL(SUM(PP2.ExpenseAMT),0) 
                               FROM PostflightExpense PP2 
   WHERE PP2.POLegID = PP4.POLegID)
                       FROM PostflightExpense PP4 WHERE IsBilling =1 --AND POLegID = 100131161
                       )PFE11
                      ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID
 
LEFT  JOIN ( SELECT DISTINCT 
             PP2.POLegID,
             PaxNames = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH('')),
                      FlightPurposeCDS = (
                       SELECT ISNULL(P1.FlightPurposeCD,'')+'$$' 
                               FROM PostflightPassenger PP1 INNER JOIN FlightPurpose P1 ON PP1.FlightPurposeID=P1.FlightPurposeID 
                               WHERE PP1.POLegID = PP2.POLegID
                             FOR XML PATH('')
                             )
                              FROM PostflightPassenger PP2 
  ) PAX ON PL.POLegID = PAX.POLegID  
LEFT JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,PP3.IsBilling ,ExpenseAmt = (
                      SELECT ISNULL(CAST(A.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                               FROM (SELECT PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, AccountDesc = P1.AccountDescription, ExpenseAmt = SUM(PP4.ExpenseAMT)
FROM PostflightExpense PP4 INNER JOIN Account P1 ON PP4.AccountID=P1.AccountID 
WHERE PP4.POLegID = PP3.POLegID AND PP4.IsBilling =1  
GROUP BY PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, P1.AccountDescription
)A
                               ORDER BY A.ExpenseAMT DESC
                             FOR XML PATH('')),
AccountDesc = (
SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' 
                               FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1
                             FOR XML PATH('')
                             )
                       FROM PostflightExpense PP3 WHERE PP3.IsBilling =1)
                       ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID
                       
     LEFT OUTER JOIN (
SELECT POLegID, ChargeUnit, ChargeRate
FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
                      INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
           ) CR ON PL.POLegID = CR.POLegID                   
 
WHERE convert(date,PL.ScheduledTM) between convert(date,@DATEFROM) And convert(date,@DATETO)
  AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (DA.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')
	AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  AND PO.CustomerID = @UserCustomerID
   AND PO.IsDeleted = 0
 

END 
 

END

GO


