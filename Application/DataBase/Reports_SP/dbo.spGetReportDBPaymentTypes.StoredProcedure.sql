IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBPaymentTypes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBPaymentTypes]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportDBPaymentTypes] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30)
)
AS
 
 

-- ===============================================================================
-- SPC Name: [spGetReportDBPaymentTypes]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get Payment Types
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT 
  [Code] = PaymentTypeCD
  , [Description] = PaymentTypeDescription
FROM PaymentType
WHERE CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND CustomerID=CONVERT(BIGINT,@UserCustomerID)
-- And IsInActive=0
 And IsDeleted=0

--EXEC spGetReportDBPaymentTypes 'SUPERVISOR_99',10099




GO


