IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECrewCalenderCrewExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECrewCalenderCrewExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE Procedure [dbo].[spGetReportPRECrewCalenderCrewExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TripNum AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS
-- ===============================================================================
-- SPC Name: spGetReportPRECrewCalenderCrewExportInformation
-- Author: ASKAR
-- Create date: 04 Jul 2012
-- Description: Get Crew Calender information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON




DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityCrew BIT  

SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-------------------------------TailNum and Fleet Group Filteration----------------------
-----------------------------Crew and Crew Group Filteration----------------------

CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
	  AND C.IsDeleted=0
	  AND C.IsStatus=1
END
-----------------------------Crew and Crew Group Filteration----------------------

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)


	-- Populate the complete Crew List for each Date in the given range
	CREATE TABLE #TempCrewDate (
		RowID INT identity(1, 1) NOT NULL
		,CrewID BIGINT
		,CrewCD VARCHAR(200)
		,DeptDateOrg DATETIME	
	)
	
	
	CREATE TABLE #TempCrewCalendar (
		RowID INT identity(1, 1) NOT NULL,CrewID BIGINT,TripID BIGINT,FleetID BIGINT,tail_nmbr VARCHAR(9),locdep DATETIME,homebase CHAR(4),depicao_id CHAR(4),
		locarr DATETIME,arricao_id CHAR(4),duty_type CHAR(2),elp_time NUMERIC(7,3),timeapprox BIT,ron BIT,orig_nmbr BIGINT,leg_num BIGINT,
		legid BIGINT,type_code VARCHAR(10),[desc] VARCHAR(40),rec_type CHAR(1),addlcrew VARCHAR(24),crewname VARCHAR(200),crewcode VARCHAR(100),
		cat_code VARCHAR(200),dutytype CHAR(2),crewcurr CHAR(4),pax_total INT,gmtdep DATETIME,gmtarr DATETIME,grndtime BIT,trueron BIT,noron BIT,
		discont BIT,nextloc DATETIME,pic VARCHAR(250),sic VARCHAR(250),gmtnext DATETIME)
		


			-- Populate actual trips and empty trips for all the crew
	INSERT INTO #TempCrewCalendar(TripID,CrewID,FleetID,tail_nmbr,locdep,homebase,depicao_id,
		locarr,arricao_id,duty_type,elp_time,timeapprox,orig_nmbr,leg_num,
		legid,type_code,[desc],rec_type,addlcrew,crewname,crewcode,
		cat_code,dutytype,crewcurr,pax_total,gmtdep,gmtarr,discont,nextloc,pic,sic,gmtnext) 
	
	SELECT PM.TripID,TCD.CrewID,F.FleetID,F.TailNum,PL.DepartureDTTMLocal,A.IcaoID,DA.IcaoID,PL.ArrivalDTTMLocal,AA.IcaoID,
	       CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END ELSE PL.DutyTYPE END,
	       PL.ElapseTM,PL.IsApproxTM,PM.TripNUM,PL.LegNUM,PL.LegID,AT.AircraftCD,PM.TripDescription,PM.RecordType,SUBSTRING(ADDL.CrewCodes,1,LEN(ADDL.CrewCodes)-1),
	       ISNULL(TCD.LastName,'') + ',' + ISNULL(TCD.FirstName,'') + ' ' + ISNULL(TCD.MiddleInitial,''),
	       TCD.CrewCD,FC.FlightCatagoryCD,PC.DutyTYPE,CDR.CrewDutyRuleCD,PL.PassengerTotal,PL.DepartureGreenwichDTTM,
	       PL.ArrivalGreenwichDTTM,PC.IsDiscount,PL.NextLocalDTTM,SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1),
	       PL.NextGMTDTTM
					FROM PreflightMain PM 
					INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON PM.FleetID = F1.FleetID
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
					INNER JOIN (
						SELECT LegID , CrewID,DutyTYPE,IsDiscount FROM PreflightCrewList
					) PC ON PL.LegID = PC.LegID
					INNER JOIN Crew TCD ON PC.CrewID = TCD.CrewID
					INNER JOIN #TempCrewID TC ON TC.CrewID=TCD.CrewID
						LEFT OUTER JOIN FlightCatagory FC 
				ON PL.FlightCategoryID = FC.FlightCategoryID
					--LEFT OUTER JOIN ( SELECT DISTINCT CrewID,CrewCD FROM #TempCrewDate) Temp ON TCD.CrewID = Temp.CrewID				
					INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.AircraftID 
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			 LEFT OUTER JOIN(
			    SELECT AircraftID,AircraftCD FROM Aircraft)AT ON AT.AircraftID=F.AircraftID
			      LEFT OUTER JOIN (
				SELECT DISTINCT PC2.LegID, CrewCodes = (
				SELECT C.CrewCD + ',' 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
				ORDER BY C.CrewCD 
				FOR XML PATH('')
				 )
				FROM PreflightCrewList PC2		
			) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
					ORDER BY C1.CrewCD 
					FOR XML PATH('')	
				 )
				FROM PreflightCrewList PC3 	
			) SIC ON PL.LegID = SIC.LegID
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE NOT IN ('P','S')
					ORDER BY C1.CrewCD 
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) ADDL ON PL.LegID = ADDL.LegID  
			INNER JOIN #TempCrewID T ON TCD.CrewID=T.CrewID
			LEFT OUTER JOIN Company C ON C.HomebaseID=F.HomebaseID
            LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
            LEFT OUTER JOIN Airport DA ON PL.DepartICAOID=DA.AirportID
            LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID=AA.AirportID
            LEFT OUTER JOIN CrewDutyRules CDR ON CDR.CrewDutyRulesID=PL.CrewDutyRulesID
           -- LEFT OUTER 
				WHERE PM.CustomerID = @CUSTOMERID 
				  AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE, @DATEFROM) AND CONVERT(DATE, @DATETO)
				  AND ISNULL(PM.RecordType,'') <> 'M'
				  AND ISNULL(PL.DutyTYPE,'') <> 'D'
				  AND PM.RecordType ='T'
			      AND  ( TCD.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
				  AND PM.Tripstatus IN ('T','H')	
				  AND ( AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )
				  AND ( PM.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '' )
				  AND PM.IsDeleted = 0


INSERT INTO #TempCrewCalendar(TripID,FleetID,tail_nmbr,locdep,homebase,depicao_id,locarr,arricao_id,duty_type,elp_time,timeapprox,orig_nmbr,
                              leg_num,legid,type_code,[desc],rec_type,addlcrew,crewname,crewcode,cat_code,dutytype,crewcurr,pax_total,gmtdep,
                              gmtarr,nextloc,gmtnexT,pic,sic)
SELECT PM.TripID,PM.FleetID,T.tail_nmbr,PL.DepartureDTTMLocal,D.IcaoID,D.IcaoID D_IcaoID,PL.ArrivalDTTMLocal,A.IcaoID A_IcaoID,CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END ELSE PL.DutyTYPE END,PL.ElapseTM ElapseTM,PL.IsApproxTM,
       PM.TripNUM,PL.LegNUM,PL.LegID,AT.AircraftCD,PM.TripDescription,PM.RecordType,ADDL.CrewCodes,ISNULL(C.LastName,'') + ',' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,''),
       C.CrewCD,T.cat_code cat_code,PCL.DutyTYPE,T.crewcurr,PL.PassengerTotal,PL.DepartureGreenwichDTTM,PL.ArrivalGreenwichDTTM,PL.NextLocalDTTM,PL.NextGMTDTTM,
        SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1)
       
         FROM #TempCrewCalendar T INNER JOIN PreflightMain PM ON T.TripID=PM.TripID
                                  INNER JOIN PreflightLeg PL ON PL.TripID=T.TripID AND PL.IsDeleted = 0
                                  LEFT OUTER JOIN Airport D ON PL.DepartICAOID=D.AirportID
                                  LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
                                  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=PM.AircraftID
                                  INNER JOIN PreflightCrewList PCL ON PCL.LegID=PL.LegID
                                  INNER JOIN Crew C ON C.CrewID=PCL.CrewID
                                  INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON PM.FleetID = F1.FleetID
					              INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
					              INNER JOIN #TempCrewCalendar TCC ON TCC.CrewID=TC.CrewID
                                  LEFT OUTER JOIN (
													SELECT DISTINCT PC3.LegID, CrewCodes = (
														SELECT C1.CrewCD + ','
														FROM PreflightCrewList PC1
														INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
														WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE NOT IN ('P','S')
														ORDER BY C1.CrewCD 
														FOR XML PATH('')
													 )
													FROM PreflightCrewList PC3 	
												) ADDL ON PL.LegID = ADDL.LegID 
							  LEFT OUTER JOIN (
							SELECT DISTINCT PC2.LegID, CrewCodes = (
							SELECT C.CrewCD + ',' 
							FROM PreflightCrewList PC
							INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
							WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
							ORDER BY C.CrewCD 
							FOR XML PATH('')
							 )
							FROM PreflightCrewList PC2		
						) PIC ON PL.LEGID = PIC.LEGID 	
						LEFT OUTER JOIN (
							SELECT DISTINCT PC3.LegID, CrewCodes = (
								SELECT C1.CrewCD + ','
								FROM PreflightCrewList PC1
								INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
								WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
								ORDER BY C1.CrewCD 
								FOR XML PATH('')	
							 )
							FROM PreflightCrewList PC3 	
						) SIC ON PL.LegID = SIC.LegID 
                                 -- OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, PM.FleetID) AS AA 
         WHERE PL.LegNUM=T.leg_num-1
         AND PM.IsDeleted = 0

 

-------------------------------Crew-----------------------------------
INSERT INTO #TempCrewCalendar(locdep,homebase,depicao_id,locarr,arricao_id,duty_type,elp_time,orig_nmbr,
                              leg_num,legid,[desc],rec_type,crewname,crewcode,gmtdep,gmtarr,nextloc,gmtnexT)
SELECT PL.DepartureDTTMLocal,AA.IcaoID,AA.IcaoID,PL.ArrivalDTTMLocal,AA.IcaoID,PL.DutyTYPE,PL.ElapseTM,PM.TripNUM,PL.LegNUM,
       PL.LegID,PM.TripDescription,PM.RecordType,ISNULL(C.LastName,'') + ',' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,''),
       C.CrewCD,PL.DepartureGreenwichDTTM,PL.ArrivalGreenwichDTTM,PL.NextLocalDTTM,PL.NextGMTDTTM
                         FROM PreflightMain PM 
                              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                              INNER JOIN PreflightCrewList PC ON PC.LegID=PL.LegID
                              INNER JOIN Crew C ON C.CrewID=PC.CrewID
                              INNER JOIN #TempCrewID TCI ON TCI.CrewID=C.CrewID
                              INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON PM.FleetID = F1.FleetID
					          LEFT OUTER JOIN #TempCrewCalendar TC ON TC.CrewID=C.CrewID
                              OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, PM.FleetID) AS AA 
WHERE 
 (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
AND PM.CustomerID=@CUSTOMERID
AND PM.RecordType='C'
AND  (C.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0) 
AND ( TC.type_code IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )
AND ( PM.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '' )
AND PM.IsDeleted = 0


-------------------------------Crew-----------------------------------




DECLARE @Crtail_nmbr VARCHAR(9),@Crlocdep DATETIME,@Crhomebase VARCHAR(4),@Crlocarr DATETIME,@Crarricao_id VARCHAR(4),
        @Crelp_time NUMERIC(7,3),@Crtimeapprox BIT,@Crorig_nmbr BIGINT,@Crleg_num BIGINT,@Crgmtnext DATETIME,
        @Crtype_code VARCHAR(10),@Crdesc VARCHAR(40),@Crrec_type VARCHAR(1),@Crcrewname VARCHAR(200),@Crutytype CHAR(2),@Crcrewcurr VARCHAR(4),@Crcrewcode VARCHAR(5), 
        @Crpax_total BIGINT,@Crgmtdep DATETIME,@Crgmtarr DATETIME,@Crdiscont BIT,@Crnextloc DATETIME;
--------------------------------Maintenance and legnum >1--------------
DECLARE @PrevtTripId BIGINT,@CrCrewCD VARCHAR(200),@CrArr DATETIME,@PrevCrewID BIGINT,@TripNumRef BIGINT

	DECLARE @crCrewID BIGINT, @crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT;
	DECLARE @crTripID BIGINT, @crLegID BIGINT, @crLegNum BIGINT, @crDepart DATETIME,@CrRon BIT;
	DECLARE @crDepartICAOID VARCHAR(4), @crArrive DATETIME, @crArriveICAOID VARCHAR(4);
	DECLARE @crNextLocalDTTM DATETIME, @crDutyType CHAR(2), @crTripNUM BIGINT,@CrTailNum VARCHAR(9),@CrAircraftType VARCHAR(10);
	DECLARE @Loopdate DATETIME,@crFlightCategoryID BIGINT,@crTripDescription VARCHAR(40),@crRecordType  CHAR(1);	
 
 


 DECLARE curCrewCal CURSOR FAST_FORWARD READ_ONLY FOR

SELECT FleetID,tail_nmbr,locdep,homebase,locarr,arricao_id,elp_time,timeapprox,orig_nmbr,leg_num,legid,
       type_code,[desc],rec_type,crewname,dutytype,crewcurr,pax_total,gmtdep,gmtarr,discont,nextloc,duty_type,crewcode,gmtnext
 FROM #TempCrewCalendar
WHERE (CONVERT(VARCHAR, locarr, 101) < CONVERT(VARCHAR, nextloc, 101)OR 
       CONVERT(VARCHAR, locdep, 101) < CONVERT(VARCHAR, locarr, 101)) 
  ORDER BY crewcode, locdep;
OPEN curCrewCal;
FETCH NEXT FROM curCrewCal INTO @CrFleetID,@Crtail_nmbr,@Crlocdep,@Crhomebase,@Crlocarr,@Crarricao_id,@Crelp_time,@Crtimeapprox,@Crorig_nmbr,@Crleg_num,@Crlegid ,
                                @Crtype_code,@Crdesc,@Crrec_type,@Crcrewname,@Crutytype,@Crcrewcurr,@Crpax_total,@Crgmtdep,@Crgmtarr,@Crdiscont,@Crnextloc,@CrDutyType,@Crcrewcode,@Crgmtnext; 

  --SELECT DATEADD(HH,DATEDIFF(HH,@Crlocdep,@Crgmtdep),CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crlocarr, 101) + ' 23:59', 101))


    WHILE @@FETCH_STATUS = 0
	 BEGIN
	 
	 	SET @Loopdate = '';
		--STEP 1:
   		INSERT INTO #TempCrewCalendar (FleetID,tail_nmbr,locdep,depicao_id,homebase,locarr,arricao_id,elp_time,timeapprox,orig_nmbr,leg_num,legid,
                                       type_code,[desc],rec_type,crewname,duty_type,crewcurr,pax_total,gmtdep,gmtarr,discont,nextloc,ron,dutytype,crewcode)
			VALUES (@CrFleetID,@crtail_nmbr,@Crlocarr,@Crarricao_id,@Crhomebase, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crlocarr, 101) + ' 23:59', 101),@Crarricao_id,
			        0,@Crtimeapprox,@Crorig_nmbr,@Crleg_num,@Crlegid,@Crtype_code,@Crdesc,@Crrec_type,@Crcrewname,@Crutytype,@Crcrewcurr,@Crpax_total,@Crgmtarr,
			        DATEADD(HH,DATEDIFF(HH,@Crlocdep,@Crgmtdep),CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crlocarr, 101) + ' 23:59', 101)), 
			        @Crdiscont,@Crnextloc,1,@Crutytype,@Crcrewcode)
			        
		
		--STEP 2:
		IF @Crrec_type='C'
		 BEGIN
		SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crlocdep, 101) + ' 00:00', 101)) END
		ELSE BEGIN SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crlocarr, 101) + ' 00:00', 101)) END 
		WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @Crnextloc, 101))
		
		BEGIN
			--PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)
	   INSERT INTO #TempCrewCalendar (FleetID,tail_nmbr,locdep,depicao_id,homebase,locarr,arricao_id,elp_time,timeapprox,orig_nmbr,leg_num,legid,
                                       type_code,[desc],rec_type,crewname,duty_type,crewcurr,pax_total,gmtdep,gmtarr,discont,nextloc,ron,dutytype,crewcode,trueron)
			VALUES (@CrFleetID,@crtail_nmbr, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:00', 101) ,@Crarricao_id,@Crhomebase, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59'),@Crarricao_id,
			        0,@Crtimeapprox,@Crorig_nmbr,@Crleg_num,@Crlegid,@Crtype_code,@Crdesc,@Crrec_type,@Crcrewname,CASE (@Crrec_type) WHEN 'C'THEN @crDutyType ELSE 'R' END,@Crcrewcurr,@Crpax_total,DATEADD(HH,DATEDIFF(HH,@Crlocdep,@Crgmtdep),CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:00', 101)),
			        DATEADD(HH,DATEDIFF(HH,@Crlocdep,@Crgmtdep),CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101)),@Crdiscont,@Crnextloc,1,@Crutytype,@Crcrewcode,1)
			        
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate);
		END
		----STEP 3:
 		INSERT INTO #TempCrewCalendar (FleetID,tail_nmbr,locdep,depicao_id,homebase,locarr,arricao_id,elp_time,timeapprox,orig_nmbr,leg_num,legid,
                                       type_code,[desc],rec_type,crewname,duty_type,crewcurr,pax_total,gmtdep,gmtarr,discont,nextloc,ron,dutytype,crewcode)
			VALUES (@CrFleetID,@crtail_nmbr, CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crnextloc, 101) + ' 00:00', 101),@Crarricao_id,@Crhomebase, @Crnextloc,@Crarricao_id,
			        0,@Crtimeapprox,@Crorig_nmbr,@Crleg_num,@Crlegid,@Crtype_code,@Crdesc,@Crrec_type,@Crcrewname,CASE (@Crrec_type) WHEN 'C'THEN @crDutyType ELSE 'R' END,@Crcrewcurr,@Crpax_total,  DATEADD(HH,DATEDIFF(HH,@Crlocdep,@Crgmtdep),CONVERT(DATETIME, CONVERT(VARCHAR(10),@Crnextloc, 101) + ' 00:00', 101)),
			        @Crgmtnext,@Crdiscont,@Crnextloc,1,@Crutytype,@Crcrewcode)
			        
FETCH NEXT FROM curCrewCal INTO @CrFleetID,@Crtail_nmbr,@Crlocdep,@Crhomebase,@Crlocarr,@Crarricao_id,@Crelp_time,@Crtimeapprox,@Crorig_nmbr,@Crleg_num,@Crlegid ,
                                @Crtype_code,@Crdesc,@Crrec_type,@Crcrewname,@Crutytype,@Crcrewcurr,@Crpax_total,@Crgmtdep,@Crgmtarr,@Crdiscont,@Crnextloc,@CrDutyType,@Crcrewcode,@Crgmtnext; 
	 END
	CLOSE curCrewCal;
	DEALLOCATE curCrewCal;
 
 
 


IF @SuppressActivityCrew=0
BEGIN
INSERT INTO  #TempCrewCalendar(crewcode,crewname)
SELECT  C.CrewCD,ISNULL(C.LastName,'') + ',' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,'') FROM Crew C
					                                                                                     INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID	
																										WHERE C.IsDeleted=0
																										  AND C.IsStatus=1
																										  AND C.CustomerID=@CUSTOMERID
																										  AND C.CrewCD NOT IN(SELECT crewcode FROM #TempCrewCalendar)

END
    
SELECT DISTINCT tail_nmbr,CONVERT(DATE,locdep) activedate,locdep,homebase,depicao_id,locarr,arricao_id,duty_type,
                 FLOOR(ISNULL(elp_time,0)*10)*0.1 elp_time,ISNULL(timeapprox,0)timeapprox,ron,orig_nmbr,leg_num,legid,type_code,[desc],rec_type,addlcrew,
                 crewname,crewcode,cat_code,dutytype,crewcurr,pax_total,gmtdep,gmtarr,ISNULL(grndtime,0)grndtime,ISNULL(trueron,0)trueron,ISNULL(noron,0) noron,
                 ISNULL(discont,0)discont,nextloc,pic,sic
 FROM #TempCrewCalendar
 WHERE  (CONVERT(DATE,locdep) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO) OR locdep IS NULL)
   ORDER BY crewcode,CONVERT(DATE,locdep),locdep

IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID

IF OBJECT_ID('tempdb..#TempCrewID') IS NOT NULL
DROP TABLE #TempCrewID

IF OBJECT_ID('tempdb..#TempCrewDate') IS NOT NULL
DROP TABLE #TempCrewDate

IF OBJECT_ID('tempdb..#TempCrewCalendar') IS NOT NULL
DROP TABLE #TempCrewCalendar


	-----------------------------------------------------------



-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'FERNANDO_1', '2012-11-30', '2012-12-02', '', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'UC', '2012-07-20', '2012-07-22', 'JKM', '', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'UC', '2012-07-20', '2012-07-22', '', '234', '', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', 'N331UV', '', '', '', 0
-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '234', '', '', 0
-- EXEC spGetReportPRECrewCalenderCrewExportInformation 'UC', '2012-07-20', '2012-07-22', '', '', '', '', 'test', '', 0






GO


