IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendar1ListInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1ListInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1ListInformation]
  @FleetID VARCHAR(30)-- MANDATORY  
 ,@ActiveDate DATETIME --MANDATORY
AS  


-- ===============================================================================  
-- SPC Name: spGetReportPREWeeklyCalendar1ListInformation  
-- Author: Sudhakar J  
-- Create date: 23 Jul 2012  
-- Description: Get Preflight Weekly Calendar1 information for Main Report  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
------------------------------------
SET NOCOUNT ON 
 BEGIN

IF EXISTS(
  
SELECT TEMP.* FROM 
(SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM)
		,[RecordType]=PM.RecordType 
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	   AND PM.TripID IN ( SELECT TripID FROM PreflightLeg 
		WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101)
		AND PM.IsDeleted = 0  
		)	
	 --  ORDER BY PM.TripNUM
   


UNION ALL

  SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
		,[RecordType]=PM.RecordType
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	  AND PL.LegID IN ( SELECT LegID FROM PreflightLeg 
		WHERE  CONVERT(DATE, @ActiveDate) >= CONVERT(DATE,ArrivalDTTMLocal) 
		AND CONVERT(DATE, @ActiveDate)< CONVERT(DATE,NextLocalDTTM)
		  AND PL.LegID NOT IN(SELECT LegID FROM PreflightLeg WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101) )
		  AND PM.IsDeleted = 0
		) 	
		
UNION ALL
  SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
		,[RecordType]=PM.RecordType
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	     AND PM.RecordType='M'
	  AND PL.LegID IN ( SELECT LegID FROM PreflightLeg 
		WHERE  CONVERT(DATE, @ActiveDate) BETWEEN CONVERT(DATE,DepartureDTTMLocal) AND  CONVERT(DATE,ArrivalDTTMLocal)
		AND PM.IsDeleted = 0
		) 

		
		)TEMP
	  )
BEGIN
SELECT TEMP.* FROM 
(SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM)
		,[RecordType]=PM.RecordType 
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	   AND PM.TripID IN ( SELECT TripID FROM PreflightLeg 
		WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101)
		AND PM.IsDeleted = 0  
		)	
	 --  ORDER BY PM.TripNUM
   


UNION ALL

  SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
		,[RecordType]=PM.RecordType
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	  AND PL.LegID IN ( SELECT LegID FROM PreflightLeg 
		WHERE  CONVERT(DATE, @ActiveDate) >= CONVERT(DATE,ArrivalDTTMLocal) 
		AND CONVERT(DATE, @ActiveDate)< CONVERT(DATE,NextLocalDTTM)
		  AND PL.LegID NOT IN(SELECT LegID FROM PreflightLeg WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101) )
		  AND PM.IsDeleted = 0
		) 	
		
UNION ALL
  SELECT      
		CrewCode = C.CrewList  
		,TripID = CONVERT(VARCHAR(30),PM.TripID)
		,RequestorCD = P.PassengerRequestorCD 
		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
		,[RecordType]=PM.RecordType
	      
	   FROM PreflightMain PM  
	   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
	   INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
	     AND PM.RecordType='M'
	  AND PL.LegID IN ( SELECT LegID FROM PreflightLeg 
		WHERE  CONVERT(DATE, @ActiveDate) BETWEEN CONVERT(DATE,DepartureDTTMLocal) AND  CONVERT(DATE,ArrivalDTTMLocal)
		AND PM.IsDeleted = 0
		) 

		
		)TEMP
	   ORDER BY TEMP.TripNUM
END

ELSE

BEGIN
 
   SELECT CrewCode = '', TripID = '0', RequestorCD = '', TripNUM = ''  ,[RecordType]=''
 END  


  
END
------------------------------------  
--SET NOCOUNT ON   

--IF EXISTS (
--	   SELECT      
--		CrewCode = C.CrewList  
--		,TripID = CONVERT(VARCHAR(30),PM.TripID)
--		,RequestorCD = P.PassengerRequestorCD 
--		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
	      
--	   FROM PreflightMain PM  
--	   LEFT OUTER JOIN (  
--		   SELECT DISTINCT PC2.tripid, CrewList = (  
--			 SELECT distinct C.CrewCD + ' '  
--			 FROM PreflightCrewList PC1 
--			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
--			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
--			 WHERE Pl.tripid = PC2.tripid  
--			 FOR XML PATH('')  
--			 )           
--			FROM PreflightLeg PC2  
--	   ) C ON PM.tripid = C.tripid  
--	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
--	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
--	   AND PM.TripID IN ( SELECT TripID FROM PreflightLeg 
--		WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101)  
--		)	
--) BEGIN

--	   SELECT      
--		CrewCode = C.CrewList  
--		,TripID = CONVERT(VARCHAR(30),PM.TripID)
--		,RequestorCD = P.PassengerRequestorCD 
--		,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
	      
--	   FROM PreflightMain PM  
--	   LEFT OUTER JOIN (  
--		   SELECT DISTINCT PC2.tripid, CrewList = (  
--			 SELECT distinct C.CrewCD + ' '  
--			 FROM PreflightCrewList PC1 
--			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
--			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
--			 WHERE Pl.tripid = PC2.tripid  
--			 FOR XML PATH('')  
--			 )           
--			FROM PreflightLeg PC2  
--	   ) C ON PM.tripid = C.tripid  
--	   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
--	   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
--	   AND PM.TripID IN ( SELECT TripID FROM PreflightLeg 
--		WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101)  
--		)	
--	   ORDER BY PM.TripNUM
   
--END ELSE BEGIN
 
--   SELECT      
--    CrewCode = '', TripID = '0', RequestorCD = '', TripNUM = '' 
-- END  
--END    
   
 --EXEC spGetReportPREWeeklyCalendar1ListInformation '10001196838', '2012-07-20'  









GO


