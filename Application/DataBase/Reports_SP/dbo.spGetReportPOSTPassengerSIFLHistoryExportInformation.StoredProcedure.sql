IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerSIFLHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerSIFLHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerSIFLHistoryExportInformation]
    @UserCustomerID VARCHAR(30),
    @UserHomebaseID AS VARCHAR(30)='',
	@LogNum VARCHAR(10) ='',
	@TailNum AS VARCHAR(30)='',
	@FleetGroupCD AS VARCHAR(1000) ='',
	@PassengerRequestorCD AS VARCHAR(1000) ='',
	@DATEFROM DATETIME, --MANDATORY  
    @DATETO DATETIME,  --MANDATORY 
    @IsHomebase AS BIT = 0,
    @PassengerGroupCD VARCHAR(500)='' 

AS
---EXEC spGetReportPOSTPassengerSIFLHistoryExportInformation '10099',@DATEFROM='2013-01-01',@DATETO='2013-12-31'

BEGIN

SET NOCOUNT ON;
DECLARE @CUSTOMER BIGINT =@UserCustomerID
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMER 
	AND F.IsDeleted=0 
	AND IsInActive = 0
END
-----------------------------TailNum and Fleet Group Filteration----------------------

-----------------------------Passenger and Passenger Group Filteration----------------------


SELECT DISTINCT 
       PM.LogNum AS lognum,									
       F.TailNum AS [tail_nmbr],
       P.PassengerName AS paxname,
       PM.POMainDescription AS [desc],
       AD.IcaoID AS [depicao_id],
       AD.CityName AS [dep_city_name],
       AA.IcaoID AS [arricao_id],
       AA.CityName AS [arr_city_name],
       PA.PassengerName AS [assoc_name],
       CONVERT(DATETIME,PM.EstDepartureDT) AS estdepdt,
       P.PassengerRequestorCD AS paxcode,
       PS.EmployeeTYPE AS [emp_t_cng],
       PA.PassengerRequestorCD AS [assoc_with],
       PS.AircraftMultiplier AS [ac_mult],
       PS.Distance ,
       PS.Distance1,
       PS.Distance2,
       PS.Distance3,
       PS.Rate1,
       PS.Rate2,
       PS.Rate3,
       PS.TeminalCharge AS termchrg,
       PS.AmtTotal AS totalamt,
       ISNULL(PL.POLegID,0) AS legid,
       ISNULL(PL.LegNUM,0) AS legnum,
       '' AS PassengerCode       
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID =PL.POLogID AND PM.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
                       LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       INNER JOIN PostflightSIFL PS ON PM.POLogID = PS.POLogID AND PS.POLegID=PL.POLegID AND PP.PassengerID = PS.PassengerRequestorID 
                       LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       LEFT JOIN Passenger PA ON PA.PassengerRequestorID = PS.AssociatedPassengerID
                       LEFT JOIN Passenger AP ON AP.PassengerRequestorID=PS.AssociatedPassengerID
                       INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID 
			           INNER JOIN Fleet F ON PM.FleetID = F.FleetID and PM.CustomerID = F.CustomerID
			           INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
                       LEFT OUTER JOIN Airport AD ON PS.DepartICAOID = AD.AirportID        			
                       LEFT OUTER JOIN Airport AA ON PS.ArriveICAOID = AA.AirportID     
WHERE (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ','))OR @LogNum='')
  AND (CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo))
  AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND PM.IsDeleted=0
  AND (PM.HomebaseID  IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  
END



GO


