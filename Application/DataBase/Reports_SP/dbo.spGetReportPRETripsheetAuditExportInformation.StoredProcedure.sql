IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripsheetAuditExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripsheetAuditExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRETripsheetAuditExportInformation]
        
        
        @UserCD AS VARCHAR(30), --Mandatory
		@TripRangeFrom AS BIGINT, --Mandatory --TRIP range
		@TripRangeTo AS BIGINT, --Mandatory --TRIP range
		--@TripStatus As CHAR(12) -- comma delimited, no spaces allowed
		@Worksheet AS BIT = 1,
		@Tripsheet AS BIT = 1,
		@Unfulfilled AS BIT = 1,
		@Cancelled AS BIT = 1,
		--@SchedSerc AS BIT = 1,
		@Hold AS BIT = 1
		
AS
-- =============================================
-- SPC Name: [spGetReportPRETripsheetAuditInformation]
-- Author: SINDHUJA.K
-- Create date: 16 July 2012
-- Description: Get Preflight Tripsheet Audit information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON
----------------
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @Worksheet = 1
		SET @TripStatus = 'W,'
		
		IF  @Tripsheet = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @Unfulfilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @Cancelled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		--IF  @SchedSerc = 1
		--SET @TripStatus = @TripStatus + 'S,'
		
		IF  @Hold = 1
		SET @TripStatus = @TripStatus + 'H'
		
		IF @Worksheet = 0 AND @Tripsheet = 0 AND @Unfulfilled = 0 AND @Cancelled = 0 AND @Hold = 0
		SET @TripStatus = 'W,T,U,X,H'	
----------------------------
--print 'TripStatus: ' + @TripStatus
		

SELECT
		    [orig_nmbr] = PM.TripNUM,
            [desc] = PM.TripDescription,
			[dispatchno] = PM.DispatchNUM,
			[rec_type] =   PM.RecordType,     
			[trip_stat] = CASE PM.TripStatus WHEN 'X' THEN 'C' ELSE PM.TripStatus END,
			[canceldesc] = PM.CancelDescription,
			[tail_nmbr] = F.TailNUM,
			[lastuser] = PM.LastUpdUID,
			[lastupdt] = PM.LastUpdTS,
			Logged = CASE WHEN ISNULL(POM.TripID, 0) = 0 THEN 'N' ELSE 'Y' END
			
			FROM PreflightMain PM 
				INNER JOIN (SELECT FleetID, TailNUM FROM Fleet) F ON PM.FleetID = F.FleetID
				LEFT OUTER JOIN PostflightMain POM ON PM.TripID=POM.TripID
				
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND PM.TripNUM BETWEEN @TripRangeFrom AND @TripRangeTo 
		    AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')))
		    AND PM.IsDeleted = 0
		    ORDER BY PM.TripNUM
    
    --EXEC spGetReportPRETripsheetAuditExportInformation 'jwilliams_11','1936','1937',1,1,1,1,1,1
    --EXEC spGetReportPRETripsheetAuditExportInformation 'jwilliams_11','1936','1937',0,1,0,0,0





GO


