IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerAdditionalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportPassengerAdditionalInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD char(5)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerAdditionalInformation
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Get Passenger Additional Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON

	SELECT 
		PAI.CustomerID
		,[PassengerCD] = P.PassengerRequestorCD
		,PAI.AdditionalINFODescription
		--,dbo.FlightPakDecrypt(IsNull(PAI.AdditionalINFOValue,'')) AS AdditionalINFO
		,IsNull(PAI.AdditionalINFOValue,'') AS AdditionalINFO
	FROM PassengerAdditionalInfo PAI
	INNER JOIN Passenger P 
		ON PAI.PassengerRequestorID = P.PassengerRequestorID
	WHERE PAI.IsDeleted = 0
	AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
	AND PAI.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)

-- EXEC spGetReportPassengerAdditionalInformation 'tim', 'cmal'


GO


