IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripsheetAlertInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripsheetAlertInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[spGetReportPRETSTripsheetAlertInformation]
@UserCD            AS VARCHAR(30)    
,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
		
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSTripsheetAlertInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Alert Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --

 	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(500) = ''
 
	----SET @SQLSCRIPT = '
      SELECT
			 [Trip_Number] = PM.TripNUM
			,[Description] = PM.TripDescription
			,[Trip_Dates] = dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD)
			--,[Requestor] = PM.RequestorLastName+ ',' + PM.RequestorFirstName+' '+ PM.RequestorMiddleName
			,[Requestor] = ISNULL(PP.LastName,'') + ',' + ISNULL(PP.FirstName,'') + ' ' + ISNULL(PP.MiddleInitial,'')
			,[Tail_Number] = F.TailNum
			,[Contact_Phone] = PP.PhoneNum
			,[Type] = A.AircraftCD
			,[Dispatch_Number] = PM.DispatchNUM
			,[Alerts] = PM.Notes
			,[DispatcherName] = UM.FirstName + ' ' + UM.LastName --PM.DsptnName,
			,[DispatcherPhone] = UM.PhoneNum
			,[Dispatcheremail] = UM.EmailID
			FROM (SELECT DISTINCT TRIPID FROM @tblTripInfo) M
		    INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
			INNER JOIN  Fleet F ON PM.FleetID = F.FleetID 
			INNER JOIN  Aircraft A ON PM.AircraftID = A.AircraftID
			LEFT OUTER JOIN Passenger PP ON PM.PassengerRequestorID = PP.PassengerRequestorID
			LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
			--WHERE RTRIM(ISNULL(PM.Notes,'')) <> ''
	 END

--EXEC spGetReportPRETSTripsheetAlertInformation 'TIM', '3985', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''




GO


