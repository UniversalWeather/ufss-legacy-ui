IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetCancellationExportInformation ]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetCancellationExportInformation ]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETripSheetCancellationExportInformation ]  
     @UserCD AS VARCHAR(30), --Mandatory    
      @DATEFROM AS DATETIME, --Mandatory    
      @DATETO AS DATETIME, --Mandatory      
      @TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values    
      @FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values    
      
AS   
  
-- ===============================================================================    
-- SPC Name: spGetReportPRETripSheetCancellationExportInformation    
-- Author: Askar   
-- Create date: 28 Feb 2014    
-- Description: Get TripSheet Cancellation information for REPORTS    
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================    
BEGIN    
SET NOCOUNT ON    
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));    
-----------------------------TailNum and Fleet Group Filteration----------------------    
  
    
CREATE TABLE  #TempFleetID       
 (       
  ID INT NOT NULL IDENTITY (1,1),     
  FleetID BIGINT    
  )    
      
    
IF @TailNUM <> ''    
BEGIN    
 INSERT INTO #TempFleetID    
 SELECT DISTINCT F.FleetID     
 FROM Fleet F    
 WHERE F.CustomerID = @CUSTOMER    
 AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))    
END    
    
IF @FleetGroupCD <> ''    
BEGIN      
INSERT INTO #TempFleetID    
 SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID    
 FROM Fleet F     
 LEFT OUTER JOIN FleetGroupOrder FGO    
 ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID    
 LEFT OUTER JOIN FleetGroup FG     
 ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID    
 WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))     
 AND F.CustomerID = @CUSTOMER      
END    
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''    
BEGIN      
INSERT INTO #TempFleetID    
 SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID    
 FROM Fleet F     
 WHERE  F.CustomerID = @CUSTOMER      
   AND F.IsDeleted=0    
   AND F.IsInActive=0  
END    
-----------------------------TailNum and Fleet Group Filteration----------------------    
    
 SELECT DISTINCT  Trip_Number = PM.TripNUM   
				 ,Trip_Dates = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD)   
				 ,Tail_Number = F.TailNUM    
				 ,Type_Code = AC.AircraftCD  
				 ,Trip_Description = PM.TripDescription    
				 ,Requestor = P.PassengerName --P.LastName+ ',' + P.FirstName+ ' ' + P.MiddleInitial,    
				 ,Contact_Phone =P.PhoneNum   
				 ,Disp_No = RTRIM(PM.DispatchNUM)    
				 ,Leg_Num = PL.LegNum   
				 ,Dep_ICAOID = A.IcaoID  
				 ,Dep_Airport_Name = A.AirportName   
				 ,Dep_City = A.CityName   
				 ,Dep_State = A.StateName    
				 ,Dep_Local = PL.DepartureDTTMLocal    
				 ,Dep_Zulu = PL.DepartureGreenwichDTTM    
				 ,Arr_ICAOID = AA.IcaoID    
				 ,Arr_Airport_Name = AA.AirportName    
				 ,Arr_City = AA.CityName    
				 ,Arr_State = AA.StateName  
				 ,Arr_Local = PL.ArrivalDTTMLocal    
				 ,Arr_Zulu = PL.ArrivalGreenwichDTTM    
				 ,Distance = PL.Distance    
				 ,Cancel_Description= PM.CancelDescription    
			  FROM PreflightMain PM    
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0    
					INNER JOIN Fleet F ON PM.FleetID = F.FleetID    
					INNER JOIN  Aircraft AC ON PM.AircraftID = AC.AircraftID   
					INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID     
					INNER JOIN  Airport A ON PL.DepartICAOID = A.AirportID --AND PM.CustomerID = A.CustomerID    
					INNER JOIN  Airport AA ON PL.ArriveICAOID = AA.AirportID   
					LEFT  JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
			 WHERE PM.CustomerID =@CUSTOMER    
			  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)    
			  AND PM.TripStatus = 'X'    
			  AND PM.IsDeleted = 0    
			 ORDER BY PM.TripNUM,PL.LegNUM  
     
   
IF OBJECT_ID('TEMPDB..#TempFleetID') IS NOT NULL   
DROP TABLE #TempFleetID    
  
END    
     
 --EXEC spGetReportPRETripSheetCancellationExportInformation 'supervisor_99', '2009-12-01', '2010-01-01', '', ''
GO


