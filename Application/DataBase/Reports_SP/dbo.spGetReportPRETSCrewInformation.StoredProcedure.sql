IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSCrewInformation]
	@UserCD VARCHAR(30),
	@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
	@LegNum AS VARCHAR(30) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSCrewInformation
-- Author: AISHWARYA.M
-- Create date: 30 Jul 2012
-- Description: Get Preflight TripSheet Crew information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON 	
SELECT [LegId] = CONVERT(VARCHAR,PL.LegID)
		,Code = CONVERT(VARCHAR(MAX),C.CrewCD)
		,CrewName = ISNULL(C.LastName,'') + ', ' +ISNULL(C.FirstName,'') + ISNULL(( CASE WHEN C.MiddleInitial=NULL OR C.MiddleInitial='' THEN ''  ELSE ' '+C.MiddleInitial END),'')
	    ,Mobile = C.CellPhoneNum
	    ,LegNo = CONVERT(VARCHAR,PL.LegNUM)
	    ,DutyType = PC.DutyTYPE
	    
	    --new fields
	    ,AdditionalPhone = ISNULL(C.CellPhoneNum2,'')
	    ,dob = dbo.GetShortDateFormatByUserCD(@UserCD,C.BirthDT) 
	    --,PassportNum = ISNULL(dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),'')
	    ,PassportNum = ISNULL(CPP.PassportNum,'')
	    ,CNationality = ISNULL(CO.CountryName ,'')
	    ,CPassportExpirationDate = dbo.GetShortDateFormatByUserCD(@UserCD,CPP.PassportExpiryDT) 
	    ,CVisa = Visa.Paxnames
	    ,CrewCodes = CR.CrewList
	    ,CrewOrder = (CASE PC.DutyTYPE 
                                    WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
									WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)
		,PC.OrderNUM
	    
	   FROM PreflightMain PM
	   INNER JOIN  PreflightLeg PL ON PM.TripID = PL.TripID 
	   INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
	   INNER JOIN Crew C ON PC.CrewID = C.CrewID
	   
	   --added for new fields 
	   --LEFT OUTER JOIN CrewPassengerVisa CPV ON C.CrewID = CPV.CrewID AND C.CustomerID = CPV.CustomerID
	   LEFT OUTER JOIN CrewPassengerPassport CPP ON PC.PassportID = CPP.PassportID
	   LEFT OUTER JOIN Country CO ON CPP.CountryID = CO.CountryID
	   
   		LEFT OUTER JOIN (SELECT DISTINCT PP2.CrewID,PP2.LegID,  Paxnames = ( 
			--SELECT 'VISA :'+CONVERT(VARCHAR(5),ROW_NUMBER() OVER(PARTITION BY PL.LEGID ORDER BY PL.LEGID))+':#'+DBO.FlightPakDecrypt(CPV.VisaNum)+'###'+CO.CountryName+'###'+CONVERT(VARCHAR(10),CPV.ExpiryDT,101)+'###'
			SELECT 'VISA :'+CONVERT(VARCHAR(5),ROW_NUMBER() OVER(PARTITION BY PL.LEGID ORDER BY PL.LEGID))+':#@#$'+ISNULL(CPV.VisaNum,'')+'@#$###'+CO.CountryName+'###'+CONVERT(VARCHAR(10),CPV.ExpiryDT,101)+'###'
			FROM PreflightLeg PL
			INNER JOIN PreflightCrewList PPL ON PPL.LegID=PL.LegID
			INNER JOIN Crew P ON PL.CustomerID = P.CustomerID AND PPL.CrewID=P.CrewID
			INNER JOIN CrewPassengerVisa CPV ON P.CrewID = CPV.PassengerRequestorID AND P.CustomerID = CPV.CustomerID
			INNER JOIN Country CO ON CPV.CountryID = CO.CountryID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID	
			WHERE PL.LegID = PP2.LegID
		 	AND PP2.CrewID=P.CrewID
			AND PL.TripID = CONVERT(BIGINT,@TripID)
			AND PL.CustomerID = PP2.CustomerID
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			FOR XML PATH('')
		)FROM  PreflightCrewList PP2 )Visa ON Visa.LegID=PL.LegID AND Visa.CrewID=C.CrewID
		
		LEFT OUTER JOIN (
                  SELECT DISTINCT PC2.LegID, CrewList = (
                        SELECT ISNULL(PC1.CrewCode + ' ','')
                        FROM (
                              SELECT PC.LEGID, [CrewCode] = C.CrewCD, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
                                    WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
									WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
                              FROM PreflightCrewList PC
                              INNER JOIN Crew C ON PC.CrewID = C.CrewID 
                        ) PC1 WHERE PC1.LegID = PC2.LegID
                        ORDER BY [RowID]
                        FOR XML PATH('')
                        )
                  FROM PreflightCrewList PC2
            ) CR ON PL.LegID = CR.LegID
	   
	   WHERE PM.TripID = CONVERT(BIGINT,@TripID)
	   AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	   AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	   ORDER BY PL.LegNUM,PC.OrderNUM--C.CrewCD, PL.LegNUM
END

--spGetReportPRETSCrewInformation 'supervisor_99', '10099107564',''




GO


