IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFlightLogPreviewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewInformation]  
    @UserCD  AS VARCHAR(30)     
    ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
    ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
    ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
    ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
    ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal      
    ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
    ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
    ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
    ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
    ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
    ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
    ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
    ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
    ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
    ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
    ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
    ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
    ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
    ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S' 
    ,@IsBlankForm		AS BIT = 1 
      
AS  
BEGIN 


DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SET @HomeBaseCD=dbo.GetHomeBaseByUserCD(LTRIM(@UserCD));

 -- [Start] Report Criteria --  
    DECLARE @tblTripInfo AS TABLE (  
    TripID BIGINT  
    , LegID BIGINT  
    , PassengerID BIGINT  
    )  
  

    INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
    EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
        @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
        @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
        @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
    -- [End] Report Criteria -- 
   

DECLARE @FlightLog AS TABLE(
	PRETripID BIGINT,
	PRELEGID BIGINT,
	SequenceOrder BIGINT,
	RowName NVARCHAR(MAX),
	ColumnName BIGINT,
	ColumnValue NVARCHAR(MAX)	
)

--START Insert into FlightLog table
INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName)
SELECT TI.TripID,TI.LegID,TS.SequenceOrder,
CASE
WHEN (LEN(ISNULL(TS.CustomDescription,''))!=0) 
THEN TS.CustomDescription 
ELSE TS.OriginalDescription 
END RowName
,CONVERT(BIGINT,PL.LegNUM) ColumnName
FROM @tblTripInfo TI INNER JOIN TSFlightLog TS ON TS.CustomerID= @CustomerID AND TS.IsPrint = 1 
AND Category = 'FLIGHT LOG' AND TS. HomebaseID=@HomeBaseCD
INNER JOIN PreflightLeg PL ON TI.LegID=PL.LegID
 	



IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='LANDING (D/N)')
BEGIN
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Day Landing' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='LANDING (D/N)'
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Night Landing' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='LANDING (D/N)'
	DELETE FROM @FlightLog WHERE RowName='LANDING (D/N)'
END

IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='TAKEOFF (D/N)')
BEGIN
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Day Takeoff' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='TAKEOFF (D/N)'
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Night Takeoff' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='TAKEOFF (D/N)'
	DELETE FROM @FlightLog WHERE RowName='TAKEOFF (D/N)'
END

IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='APPROACH (P/N)')
BEGIN
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Prec App' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='APPROACH (P/N)'
	INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
	SELECT PRETripID,PRELEGID,SequenceOrder,'Non-Prec App' RowName,ColumnName,'   /'
	FROM @FlightLog 
	WHERE RowName='APPROACH (P/N)'
	DELETE FROM @FlightLog WHERE RowName='APPROACH (P/N)'
END
			             
		        
		DECLARE @TEMPCREW TABLE(RowID INT,
							CrewID BIGINT,
							TripID BIGINT,
							CREWCD VARCHAR(5),
							LegNUM BIGINT,
							Legid BIGINT,
							DutyType CHAR(1)) 



		INSERT INTO @TEMPCREW 
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY CrewCD ),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
														 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID WHERE PCL.DutyTYPE IN('P') AND PM.TripID IN(SELECT DISTINCT TripID FROM @tblTripInfo)
														 ORDER BY CrewCD DESC 


		INSERT INTO @TEMPCREW
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY CrewCD ),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
														 INNER  JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('S') 
														 AND PM.TripID IN(SELECT DISTINCT TripID FROM @tblTripInfo)
														 ORDER BY CrewCD DESC 
		--Select * from @TEMP


		DECLARE @Pic_AddCrew TABLE(
	PRETripID BIGINT,
	PRELEGID BIGINT,
	ColumnName NVARCHAR(MAX),
	ColumnValue NVARCHAR(MAX))

	
			INSERT @Pic_AddCrew(PRETripID,PRELEGID,ColumnName,ColumnValue)
			SELECT DISTINCT FL.PRETripID,FL.PRELEGID,'PIC/SIC' ColumnName,STUFF(
				(
					SELECT '/'+LTRIM(RTRIM(CREWCD))
					FROM  @TEMPCREW P
					WHERE P.LegId=FL.PRELEGID
					FOR XML PATH('')
				),1,1,'') AS ColumnValue 
			FROM @FlightLog FL INNER JOIN  @TEMPCREW PS ON FL.PRELEGID=PS.LegId AND FL.PRETripID=PS.TripID

			INSERT @Pic_AddCrew(PRETripID,PRELEGID,ColumnName,ColumnValue)
			SELECT DISTINCT PM.TripID,PL.LegID,'ADDL CREW' ColumnName,SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) ColumnValue   
			FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
			INNER JOIN PreflightMain PM ON M.TripID = PM.TripID AND PM.IsDeleted = 0 
			INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID AND PL.IsDeleted = 0 
			INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
			INNER JOIN (            
			SELECT DISTINCT PC2.LegID, CrewList = (            
			SELECT LTRIM(RTRIM(C.CrewCD)) + '/'            
			FROM PreflightCrewList PC1            
			INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
			WHERE PC1.LegID = PC2.LegID    
			AND PC1.DutyTYPE NOT IN('P','S')          
			FOR XML PATH('')            
			)                     
			FROM PreflightCrewList PC2             
			) Crew ON PL.LegID = Crew.LegID INNER JOIN @FlightLog FL ON PL.LegID=FL.PRELEGID
			WHERE PM.CustomerID = @CustomerID

			INSERT INTO @FlightLog(PRETripID,PRELEGID,SequenceOrder,RowName,ColumnName,ColumnValue)
			SELECT DISTINCT PL.TripID,PL.LegID,-1 SequenceOrder ,
						'Leg#Depart ICAO#Arrive ICAO#Sched UTC Date#Sched UTC Dep#Sched UTC Arr' AS  RowName,
						CONVERT(BIGINT,PL.LegNUM) ColumnName
			,CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114) AS ColumnValue
			FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
			INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
			INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
			INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID  

			UPDATE @FlightLog
			SET ColumnValue=PC.ColumnValue
			FROM @FlightLog FL INNER JOIN @Pic_AddCrew PC ON FL.PRETripID=PC.PRETripID AND FL.PRELEGID=PC.PRELEGID 
			AND FL.RowName=PC.ColumnName 

IF @IsBlankForm = 0
	BEGIN	
	
		 --UPDATE @FlightLog set PRETripID=NULL,PRELEGID=NULL,SequenceOrder=NULL,RowName=NULL,ColumnName=NULL,ColumnValue=NULL
		 UPDATE @FlightLog set ColumnValue=NULL
     
	END   

DECLARE @LogFixed INT;	
	
SELECT @LogFixed=LogFixed FROM Company WHERE CustomerID=@CustomerID 
										 AND LogFixed IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))	
                                                	
IF @LogFixed=1 
BEGIN
SELECT * FROM @FlightLog WHERE RowName NOT IN('UTC OUT','UTC OFF','UTC ON','UTC IN')  ORDER BY SequenceOrder

END ELSE 
BEGIN
SELECT * FROM @FlightLog WHERE RowName NOT IN('HOME OUT','HOME OFF','HOME ON','HOME IN') ORDER BY SequenceOrder 
END
 
END
 



GO


