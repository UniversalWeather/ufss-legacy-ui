IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFlightLogExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFlightLogExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSFlightLogExportInformation]  
    @UserCD  AS VARCHAR(30)     
    ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
    ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
    ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
    ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
    ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal      
    ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
    ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
    ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
    ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
    ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
    ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
    ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
    ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
    ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
    ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
    ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
    ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
    ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
    ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
    ,@ReportHeaderID VARCHAR(30)=''
	,@TempSettings VARCHAR(MAX)=''
    --,@IsBlankForm		AS BIT = 1  
AS  
BEGIN  
-- ================================================================================  
-- SPC Name: spGetReportPRETSFlightLogInformation  
-- Author: ASKAR  
-- Create date:27 SEP 2012  
-- Description: Get Preflight Tripsheet Flight Log information for REPORTS  
-- Revision History  
-- Date        Name        Ver        Change  
--   
-- =================================================================================  
  
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @BLANKTRIPNUM INT,@IsBlankForm BIT=0;

DECLARE @FormatSettings TABLE (
	BLANK BIT,
	SIGNATURE BIT,
	MINUTES BIT,
	COMMENTS BIT,
	NMDIST BIT,
	BLOCKOUT BIT,
	BLOCKIN BIT,
	BLOCKHRS BIT,
	TIMEOFF BIT,
	TIMEON BIT,
 	FLIGHTHRS BIT,
	DELAYTYPE BIT,
	DELAYTIME BIT,
	PASSENG BIT,
	TAKEOFF BIT,
	LANDING BIT,
	APPROACH BIT,	
	INSTTIME BIT,
	NIGHTTIME BIT,
	ADDLCREW BIT,
	FUELOUT BIT,
	FUELBURN BIT,
	FUELADD BIT,
	QTYPURCH BIT,
	PURCHPRICE BIT,
	LANDSCAPE BIT,
	FARNAV BIT,
	FARHOLD BIT,
	DISPATCHNO BIT,
	DUTYON BIT,
	DUTYOFF BIT,
	TOTALDUTY BIT,
	CHRMONTH BIT,
	DISPATCHIN SMALLINT,
	DSPTEMAIL BIT,
	DISPATCHER BIT,
	DSPTPHONE BIT

)

INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 4, @TempSettings
SELECT @IsBlankForm=BLANK FROM  @FormatSettings

	IF @IsBlankForm = 0
	BEGIN	
		SET @BLANKTRIPNUM = CONVERT(INT, @TripNUM);
		--SET @TripNUM = '0';
	END   

 -- [Start] Report Criteria --  
    DECLARE @tblTripInfo AS TABLE (  
    TripID BIGINT  
    , LegID BIGINT  
    , PassengerID BIGINT  
    )  
  
    INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
    EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
        @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
        @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
        @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
    -- [End] Report Criteria -- 
   
CREATE  TABLE #FlightLog(TripID BIGINT,
                          RowNum INT,
                          Notes VARCHAR(500),
                          NotesOriginal VARCHAR(500),
                          data1 VARCHAR(200),
                          data2 VARCHAR(200),
                          data3 VARCHAR(200),
                          data4 VARCHAR(200),
                          data5 VARCHAR(200),
                          data6 VARCHAR(200),
                          data7 VARCHAR(200),
                          data8 VARCHAR(200),
                          data9 VARCHAR(200),
                          data10 VARCHAR(200),
                          data11 VARCHAR(200),
                          Total VARCHAR(10),
                          TripNumber BIGINT,
                          Description VARCHAR(60),
                          TripDates   VARCHAR(60),
                          Requestor   VARCHAR(250),
                          TailNumber  VARCHAR(10),
                         [ContactPhone] VARCHAR(35),
			             [DispatchNum] VARCHAR(35),
			             [Type] VARCHAR(35),
			             Flag INT)
			             
		        
		DECLARE @TEMP TABLE(RowID INT,
							CrewID BIGINT,
							TripID BIGINT,
							CREWCD VARCHAR(5),
							LegNUM BIGINT,
							Legid BIGINT,
							DutyType CHAR(1)) 



		INSERT INTO @TEMP 
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY CrewCD ),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
														 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID WHERE PCL.DutyTYPE IN('P') AND PM.TripID IN(SELECT DISTINCT TripID FROM @tblTripInfo)
														 ORDER BY CrewCD DESC 


		INSERT INTO @TEMP
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY CrewCD ),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
														 INNER  JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('S') 
														 AND PM.TripID IN(SELECT DISTINCT TripID FROM @tblTripInfo)
														 ORDER BY CrewCD DESC 


		CREATE TABLE #Pic_AddCrew (RowID INT IDENTITY,
								   TripID BIGINT,
								   data1 VARCHAR(100),
								   data2 VARCHAR(100),
								   data3 VARCHAR(100),
								   data4 VARCHAR(100),
								   data5 VARCHAR(100),    
								   data6 VARCHAR(100),
								   data7 VARCHAR(100),
								   data8 VARCHAR(100),
								   data9 VARCHAR(100),
								   data10 VARCHAR(100),
								   data11 VARCHAR(100),
								   TsFlightLog BIGINT)
		                           
		                           

		      
		INSERT INTO #Pic_AddCrew(TripID,data1)
		 SELECT DISTINCT PM.TripID,  
					   ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'') data1   
						FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
						INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
						INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
						LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=1 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
						LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=1 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID 
			  WHERE PM.CustomerID = @CustomerID 
			  AND PL.LegNUM=1    
		  
		  

		   

		UPDATE #Pic_AddCrew SET data2=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=2 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=2 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=2

		UPDATE #Pic_AddCrew SET data3=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=3 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=3 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=3
					               		               
		UPDATE #Pic_AddCrew SET data4=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=4 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=4 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=4
					               
					               
		UPDATE #Pic_AddCrew SET data5=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=5 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=5 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=5
					               
		UPDATE #Pic_AddCrew SET data6=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=6 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=6 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=6
					               
		UPDATE #Pic_AddCrew SET data7=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=7 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=7 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=7
					               
		UPDATE #Pic_AddCrew SET data8=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=8 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										  LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=8 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=8
					               
					               
		UPDATE #Pic_AddCrew SET data9=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=9 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=9 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=9
					               
		UPDATE #Pic_AddCrew SET data10=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=10 AND RowID=1 AND DutyType='P') T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=10 AND RowID=1 AND DutyType='S') T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=10
					               
					               
		UPDATE #Pic_AddCrew SET data11=ISNULL(T.CREWCD+'/','')+ISNULL(T1.CREWCD,'')   
									 FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=11 AND RowID=1) T ON M.TripID=T.TripID 
										   LEFT OUTER JOIN (SELECT LegNUM,TripID,CREWCD,CrewID FROM @TEMP WHERE LegNUM=11 AND RowID=1) T1 ON M.TripID=T1.TripID   
									WHERE PM.CustomerID = @CustomerID
									 AND RowID=1
									 AND PL.LegNUM=11
					              
			          
		INSERT INTO #Pic_AddCrew(TripID,data1)
		 SELECT DISTINCT PM.TripID,   SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) data1   
						FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
						INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
						INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
						INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
						INNER JOIN (            
						SELECT DISTINCT PC2.LegID, CrewList = (            
					  SELECT C.CrewCD + '/'            
					  FROM PreflightCrewList PC1            
					  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
					  WHERE PC1.LegID = PC2.LegID    
					  AND PC1.DutyTYPE NOT IN('P','S')          
					  FOR XML PATH('')            
					  )                     
						FROM PreflightCrewList PC2             
			  ) Crew ON PL.LegID = Crew.LegID   
			  WHERE PM.CustomerID = @CustomerID
			  AND PL.LegNUM=1
		      
		      
		UPDATE #Pic_AddCrew  SET data2=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=2
		UPDATE #Pic_AddCrew  SET data3=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=3
					            
		UPDATE #Pic_AddCrew  SET data4=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=4
					            
		UPDATE #Pic_AddCrew  SET data5=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=5
					            
		UPDATE #Pic_AddCrew  SET data6=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=6
					            
		UPDATE #Pic_AddCrew  SET data7=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=7
					            
		UPDATE #Pic_AddCrew  SET data8=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=8
					            
		UPDATE #Pic_AddCrew  SET data9=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=9
					            
		UPDATE #Pic_AddCrew  SET data11=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=10 
					            
		UPDATE #Pic_AddCrew  SET data11=SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) 
									FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M  
										INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
										INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
										INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
										INNER JOIN (            
										SELECT DISTINCT PC2.LegID, CrewList = (            
									  SELECT C.CrewCD + '/'            
									  FROM PreflightCrewList PC1            
									  INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
									  WHERE PC1.LegID = PC2.LegID    
									  AND PC1.DutyTYPE NOT IN('P','S')          
									  FOR XML PATH('')            
									  )                     
										FROM PreflightCrewList PC2             
							  ) Crew ON PL.LegID = Crew.LegID   
							  WHERE PM.CustomerID = @CustomerID
								AND RowID=2
								AND PL.LegNUM=11
					             
		 
		 DECLARE @tblTrip AS TABLE(RowID INT IDENTITY,
								   LegID BIGINT,
								   data1 VARCHAR(100),
								   data2 VARCHAR(100),
								   data3 VARCHAR(100),
								   data4 VARCHAR(100),
								   data5 VARCHAR(100),    
								   data6 VARCHAR(100),
								   data7 VARCHAR(100),
								   data8 VARCHAR(100),
								   data9 VARCHAR(100),
								   data10 VARCHAR(100),
								   data11 VARCHAR(100)) 
		  
		-- Header --  
		DECLARE @data1 VARCHAR(100),@data2 VARCHAR(100),@data3 VARCHAR(100),@data4 VARCHAR(100),@data5 VARCHAR(100),    
		@data6 VARCHAR(100),@data7 VARCHAR(100),@data8 VARCHAR(100),@data9 VARCHAR(100),@data10 VARCHAR(100),@data11 VARCHAR(100);    
		---SELECT * FROM @tblT   
		--CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,101)
		INSERT INTO @tblTrip(LegID,data1)  
		SELECT DISTINCT PL.LegID,CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)   
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=1  

		UPDATE @tblTrip SET LegID=PL.LegID,data2=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=2  
		 
		UPDATE @tblTrip SET LegID=PL.LegID,data3=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M   
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=3  

		UPDATE @tblTrip SET LegID=PL.LegID,data4=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=4   

		UPDATE @tblTrip SET LegID=PL.LegID,data5=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=5 

		UPDATE @tblTrip SET LegID=PL.LegID,data6=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=6
		    
		UPDATE @tblTrip SET LegID=PL.LegID,data7=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=7 

		UPDATE @tblTrip SET LegID=PL.LegID,data8=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M   
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=8 

		UPDATE @tblTrip SET LegID=PL.LegID,data9=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M   
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=9 

		UPDATE @tblTrip SET LegID=PL.LegID,data10=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M  
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=10

		UPDATE @tblTrip SET LegID=PL.LegID,data11=CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID+'#'+CONVERT(VARCHAR(5), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))+'#'+CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,114)+'#'+CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,114)  
		FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M   
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
		INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
		INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID   
		WHERE PL.LegNUM=11  
		DECLARE @TripID BIGINT;  
		/*
		 ---drop table #FlightLog
		CREATE  TABLE #FlightLog(TripID BIGINT,
								  RowNum INT,
								  Notes VARCHAR(500),
								  NotesOriginal VARCHAR(500),
								  data1 VARCHAR(200),
								  data2 VARCHAR(200),
								  data3 VARCHAR(200),
								  data4 VARCHAR(200),
								  data5 VARCHAR(200),
								  data6 VARCHAR(200),
								  data7 VARCHAR(200),
								  data8 VARCHAR(200),
								  data9 VARCHAR(200),
								  data10 VARCHAR(200),
								  data11 VARCHAR(200),
								  Total VARCHAR(10),
								  TripNumber BIGINT,
								  Description VARCHAR(60),
								  TripDates   VARCHAR(60),
								  Requestor   VARCHAR(250),
								  TailNumber  VARCHAR(10),
								 [ContactPhone] VARCHAR(35),
								 [DispatchNum] VARCHAR(35),
								 [Type] VARCHAR(35))
		*/  
		SET @TripID=(SELECT TOP 1 TripID FROM @tblTripInfo)
		 
		IF @TripID > 0 
		BEGIN

		INSERT INTO #FlightLog
		SELECT TEMP.* FROM 
			(SELECT DISTINCT PM.TripID,SequenceOrder RowNum, CustomDescription notes,OriginalDescription,'' data1,'' data2,'' data3,'' data4,'' data5,'' data6,'' data7,'' data8,'' data9,'' data10,'' data11,    
						CASE WHEN OriginalDescription IN('HOME OFF','HOME ON','HOME OUT','HOME IN','UTC OFF','UTC ON','UTC OUT','UTC IN' ) THEN ':' 
										                                                                                                   ELSE '' END total ,'' TripNumber,''Description ,''TripDates,''Requestor,''TailNumber,''ContactPhone,''DispatchNum ,''Type 
					    ,CASE(TS.OriginalDescription) WHEN 'MILEAGE' THEN 1 
					                                  WHEN 'UTC OUT' THEN 2
					                                  WHEN 'UTC OFF' THEN 3
					                                  WHEN 'UTC ON'  THEN 4	 
					                                  WHEN 'UTC IN'  THEN 5 
					                                  WHEN 'HOME OUT' THEN 2 
					                                  WHEN 'HOME OFF' THEN 3 
					                                  WHEN 'HOME ON' THEN 4 
					                                  WHEN 'HOME IN' THEN 5 
					                                  WHEN 'MILEAGE' THEN 1 
					                                  WHEN 'BLOCK HRS' THEN 6 
					                                  WHEN 'FLIGHT HRS' THEN 7 
					                                  WHEN 'DELAY TYPE' THEN 8
					                                  WHEN 'DELAY TIME' THEN 9 
					                                  WHEN 'PASSENGERS' THEN 10 
					                                  WHEN 'FUEL OUT' THEN 11 
					                                  WHEN 'FUEL IN' THEN 12 
					                                  WHEN 'FUEL BURN' THEN 13 
					                                  WHEN 'FUEL QTY/UNIT' THEN 14 
					                                  WHEN 'FUEL PRICE' THEN 15 
					                                  WHEN 'TAKEOFF (D/N)' THEN 17 
					                                  WHEN 'LANDING (D/N)' THEN 18 
					                                  WHEN 'APPROACH (P/N)' THEN 19 
					                                  WHEN 'INST TIME' THEN 20
					                                  WHEN 'NIGHT TIME' THEN 21
					                                  WHEN 'DUTY ON' THEN 25
					                                  WHEN 'DUTY OFF' THEN 26 
					                                  WHEN 'TOTAL DUTY' THEN 27 ELSE 28 END Flag
					                                  
					   FROM TSFlightLog TS 
						INNER JOIN PreflightMain PM ON TS.CustomerID=PM.CustomerID AND TS.HomebaseID=PM.HomebaseID
					   -- LEFT OUTER  JOIN (SELECT DISTINCT TripID FROM @tblTripInfo)M  ON M.TripID = PM.TripID 
						LEFT OUTER JOIN #Pic_AddCrew PA ON PA.TripID=PM.TripID 
							WHERE TS.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
							AND PM.TripID IN(SELECT TripID FROM @tblTripInfo)
							AND TS.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)
							AND Category = 'FLIGHT LOG'
							AND OriginalDescription NOT IN('PIC/SIC','ADDL CREW')
							AND IsPrint = 1
							
				UNION ALL
			    
			   SELECT DISTINCT PM.TripID,SequenceOrder, CustomDescription notes,OriginalDescription,data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,    
						'' total ,'','','','','','','','',CASE(TS.OriginalDescription) WHEN 'PIC/SIC' THEN 16 END Flag
					   FROM TSFlightLog TS 
						INNER JOIN PreflightMain PM ON TS.CustomerID=PM.CustomerID AND TS.HomebaseID=PM.HomebaseID
					  --  LEFT OUTER  JOIN (SELECT DISTINCT TripID FROM @tblTripInfo)M  ON M.TripID = PM.TripID 
						 LEFT OUTER JOIN #Pic_AddCrew PA ON PA.TripID=PM.TripID 
							WHERE TS.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
							AND TS.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)
							AND PM.TripID IN(SELECT TripID FROM @tblTripInfo)
							AND Category = 'FLIGHT LOG'
							AND OriginalDescription='PIC/SIC'
							AND IsPrint = 1
							AND PA.RowID=1
					
			 UNION ALL
			 
				  SELECT DISTINCT PM.TripID,SequenceOrder, CustomDescription notes,OriginalDescription,data1,data2, data3,data4,data5,data6,data7,data8,data9,data10,data11,    
						'' total ,'','','','','','','','' ,CASE(TS.OriginalDescription) WHEN 'ADDL CREW' THEN 22  END  Flag
					   FROM TSFlightLog TS 
						INNER JOIN PreflightMain PM ON TS.CustomerID=PM.CustomerID AND TS.HomebaseID=PM.HomebaseID
					   -- LEFT OUTER JOIN (SELECT DISTINCT TripID FROM @tblTripInfo)M  ON M.TripID = PM.TripID 
						 LEFT OUTER JOIN #Pic_AddCrew PA ON PA.TripID=PM.TripID 
							WHERE TS.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
							AND TS.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)
							AND PM.TripID IN(SELECT TripID FROM @tblTripInfo)
							AND Category = 'FLIGHT LOG'
							AND OriginalDescription='ADDL CREW'
							AND IsPrint = 1
							AND PA.RowID=2
							)TEMP
		ORDER BY RowNum 
		END 
		ELSE

		BEGIN

		INSERT INTO #FlightLog
		   SELECT DISTINCT 0 TripID,SequenceOrder, CustomDescription notes,'' OriginalDescription,'' data1,'' data2,'' data3,'' data4,'' data5,'' data6,'' data7,'' data8,'' data9,'' data10,'' data11,    
						'' total ,'','','','','','','','' 
					   FROM TSFlightLog TS 
						INNER JOIN PreflightMain PM ON TS.CustomerID=PM.CustomerID AND TS.HomebaseID=PM.HomebaseID
							WHERE TS.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
							AND TS.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)
							AND Category = 'FLIGHT LOG'
							AND IsPrint = 1     
		   
		                
		 
		 END
		                          
		 
		  UPDATE #FlightLog SET data1='/' FROM(SELECT data1,TripID FROM #FlightLog WHERE RowNum='-999')D1Log
		  WHERE D1Log.TripID=#FlightLog.TripID
			AND D1Log.data1 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		  
			UPDATE #FlightLog SET data2='/' FROM(SELECT data2,TripID FROM #FlightLog WHERE RowNum='-999')D2Log
		  WHERE D2Log.TripID=#FlightLog.TripID
			AND D2Log.data2 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		   
		   
		   UPDATE #FlightLog SET data3='/' FROM(SELECT data3,TripID FROM #FlightLog WHERE RowNum='-999')D3Log
		  WHERE D3Log.TripID=#FlightLog.TripID
			AND D3Log.data3 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		  
		    
			UPDATE #FlightLog SET data4='/' FROM(SELECT data4,TripID FROM #FlightLog WHERE RowNum='-999')D4Log
		  WHERE D4Log.TripID=#FlightLog.TripID
			AND D4Log.data4 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		      
			UPDATE #FlightLog SET data5='/' FROM(SELECT data5,TripID FROM #FlightLog WHERE RowNum='-999')D5Log
		  WHERE D5Log.TripID=#FlightLog.TripID
			AND D5Log.data5 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		      
			UPDATE #FlightLog SET data6='/' FROM(SELECT data6,TripID FROM #FlightLog WHERE RowNum='-999')D6Log
		  WHERE D6Log.TripID=#FlightLog.TripID
			AND D6Log.data6 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		      
			UPDATE #FlightLog SET data7='/' FROM(SELECT data7,TripID FROM #FlightLog WHERE RowNum='-999')D7Log
		  WHERE D7Log.TripID=#FlightLog.TripID
			AND D7Log.data7 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		      
			UPDATE #FlightLog SET data8='/' FROM(SELECT data8,TripID FROM #FlightLog WHERE RowNum='-999')D8Log
		  WHERE D8Log.TripID=#FlightLog.TripID
			AND D8Log.data8 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		      
			UPDATE #FlightLog SET data9='/' FROM(SELECT data9,TripID FROM #FlightLog WHERE RowNum='-999')D9Log
		  WHERE D9Log.TripID=#FlightLog.TripID
			AND D9Log.data9 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		 
		   
		   UPDATE #FlightLog SET data10='/' FROM(SELECT data10,TripID FROM #FlightLog WHERE RowNum='-101010')D10Log
		  WHERE D10Log.TripID=#FlightLog.TripID
			AND D10Log.data10 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
		    
		    
			UPDATE #FlightLog SET data11='/' FROM(SELECT data11,TripID FROM #FlightLog WHERE RowNum='-111111')D11Log
		  WHERE D11Log.TripID=#FlightLog.TripID
			AND D11Log.data11 IS NOT NULL
			AND #FlightLog.NotesOriginal IN('TAKEOFF (D/N)','LANDING (D/N)','APPROACH (P/N)')
 
	IF @IsBlankForm = 0
	BEGIN	
	
	 UPDATE #FlightLog set data1=NULL,data2=NULL,data3=NULL,data4=NULL,data5=NULL,data6=NULL,data7=NULL,data8=NULL,data9=NULL,data10=NULL,data11=NULL,TripNumber=NULL,
	                   Description=NULL,TripDates=NULL,Requestor=NULL,TailNumber=NULL,ContactPhone=NULL,Total=NULL,Type=NULL
     UPDATE #FlightLog set Total='Total' WHERE RowNum=-999
    
	--	UPDATE #FlightLog SET DispatchNum = (SELECT TOP 1 DispatchNUM FROM PreflightMain WHERE TripNUM = @BLANKTRIPNUM AND CustomerID = @CustomerID)

	END   

DECLARE @LogFixed INT;	
	
SELECT @LogFixed=LogFixed FROM Company WHERE CustomerID=@CustomerID 
										 AND LogFixed IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))	
                                                 	

IF @LogFixed=1 
BEGIN
SELECT * FROM #FlightLog WHERE NotesOriginal NOT IN('UTC OUT','UTC OFF','UTC ON','UTC IN')ORDER BY RowNum   

END ELSE 
BEGIN
SELECT * FROM #FlightLog WHERE NotesOriginal NOT IN('HOME OUT','HOME OFF','HOME ON','HOME IN')ORDER BY RowNum
END

 IF OBJECT_ID('tempdb..#Pic_AddCrew') IS NOT NULL
 DROP TABLE #Pic_AddCrew
 
 IF OBJECT_ID('tempdb..#FlightLog') IS NOT NULL
 DROP TABLE #FlightLog
  
 END  
 
 

  
--EXEC spGetReportPRETSFlightLogExportInformation 'TIM','20','','','','','','','','','','','','','','','','','',''  
--EXEC spGetReportPRETSFlightLogExportInformation 'supervisor_99','1992','','','','','','','','','','','','','','','','','',''

GO


