IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

   
CREATE PROCEDURE  [dbo].[spGetReportPOSTPilotLogExportInformation]   
   (
	  @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @CrewCD AS NVARCHAR(1000) = '',  
	  @CrewGroupCD AS CHAR(1000) = '',
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @FlightCatagoryCD AS CHAR(1000) = '',
	  @HomeBaseCD AS CHAR(1000) = '', 
	  @IsHomebase BIT = 0,
	  @PrintOnTime BIT = 0,
	  @PrintOffTime BIT = 0  
  ) 
AS  
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTPilotLogExportInformation]  
-- Author: Avanikanta Pradhan  
-- Create date: 06 August  
-- Description: Get POST flight Delay Details  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
--DROP TABLE #CREWINFO
--DROP TABLE #CREWEMPTY
--DROP TABLE #CREWINFO1
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
DECLARE @speclong3 NVARCHAR(25);
DECLARE @speclong4 NVARCHAR(25);
DECLARE @specdec3 BIGINT;
DECLARE @specdec4 BIGINT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt
,@speclong3 = SpecificationLong3
,@speclong4 = SpecificationLong4
,@specdec3 = Specdec3
,@specdec4 = Specdec4
FROM Company 
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt =1

--DROP TABLE #DUTYTOTAL
CREATE TABLE #DUTYTOTAL
(
POLogID BIGINT
,LOGNUM INT
,LegID BIGINT
,IsStatus nvarchar(1)
,DutyBegin VARCHAR(20)
,DutyEnd VARCHAR(20)
,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
DutyBegin VARCHAR(20),
DutyEnd VARCHAR(20),
POLogID BIGINT,
LegID BIGINT
,CrewID BIGINT
)
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID 
INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE
PM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID 
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID FROM 
(SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID
UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NULL

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @CrewCD <> '' 
      BEGIN
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            WHERE c.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1  
            AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
      END
IF @CrewGroupCD <> '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew C
            LEFT OUTER JOIN CrewGroupOrder cGO
                  ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
            LEFT OUTER JOIN CrewGroup CG 
                  ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
            WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
                  AND C.CustomerID = @CustomerID AND C.IsDeleted=0 AND C.IsStatus=1 
      END
ELSE IF @CrewCD = '' AND @CrewGroupCD = '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
            FROM Crew C 
            WHERE C.CustomerID = @customerID AND C.IsDeleted=0 AND C.IsStatus=1 
      END



IF @IsZeroSuppressActivityCrewRpt=1
BEGIN

DELETE FROM  @TempCrewID WHERE CrewID NOT IN(
SELECT DISTINCT [CREWID]=CW.CrewID
FROM PostflightMain POM 
INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted = 0
INNER JOIN PostflightCrew POC ON POC.POLegID = POL.POLegID 
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 --AND CW.IsStatus = 1
LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID

WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0 
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND PM.IsDeleted = 0)
 AND (Ac.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
 AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
 AND POM.IsDeleted = 0)

 END
-----------------------------TailNum and Fleet Group Filteration----------------------
-----------------------------HomebaseID and Homebase Filteration----------------------
DECLARE @TempHome TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @IsHomebase=1
      BEGIN
            INSERT INTO @TempHome
            SELECT DISTINCT CW.CrewID 
            FROM Crew CW
            LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
			LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
            WHERE CW.CustomerID = @CustomerID AND CW.IsDeleted=0 AND CW.IsStatus=1  
            AND (C1.HomebaseID=(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase=0)
      END
ELSE IF @IsHomebase=0 AND @HomeBaseCD <> ''
      BEGIN 
            INSERT INTO @TempHome
            SELECT DISTINCT CW.CrewID 
            FROM Crew CW
            LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
			LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
            WHERE CW.CustomerID = @CustomerID AND CW.IsDeleted=0 AND CW.IsStatus=1
            AND (AF.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '') 
      END
ELSE IF @IsHomebase=0 AND @HomeBaseCD = ''
      BEGIN 
            INSERT INTO @TempHome
            SELECT DISTINCT CW.CrewID
            FROM Crew CW
            LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
			LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID 
            WHERE CW.CustomerID = @customerID AND CW.IsDeleted=0 AND CW.IsStatus=1
      END
-----------------------------HomebaseID and Homebase Filteration---------------------- 
CREATE TABLE #CREWINFO
(
[ROWNUMBER] INT IDENTITY
,[CREWID] BIGINT
,[crewcode] NVARCHAR(5)
,[lognum] BIGINT
,[legid] BIGINT
,[dutyend] BIT
,[last_name] NVARCHAR(30)
,[first_name] NVARCHAR(30)
,[middleinit] NVARCHAR(30)
,[schedttm] DATETIME
,[fltdate] DATETIME
,[tail_nmbr] VARCHAR(9)
,[type_code] NVARCHAR(10)
,[tot_line] NVARCHAR(10)
,[farnum] CHAR(3)
,[crewcurr] CHAR(4)
,[depicao_id] CHAR(4)
,[arricao_id] CHAR(4)
,[cat_code] CHAR(4)
,[dutycounter] NVARCHAR(20)
,[dutytotal] NUMERIC(7,3)
,[blkout] NVARCHAR(100)
,[blkin] NVARCHAR(100)
,[dutyhrs] NUMERIC(7,3)
,[dutytype] CHAR(1)
,[blk_hrs] NUMERIC(7,3)
,[flt_hrs] NUMERIC(7,3)
,[night] NUMERIC(7,3)
,[instr] NUMERIC(7,3)
,[tkoff_d] NUMERIC(2,0)
,[tkoff_n] NUMERIC(2,0)
,[app_p] NUMERIC(2,0)
,[app_n] NUMERIC(2,0)
,[lnding_d] NUMERIC(2,0)
,[lnding_n] NUMERIC(2,0)
,[override] BIT
,[dutybeg] NVARCHAR(5)
,[end] NVARCHAR(5)
,[ontime] NVARCHAR(100)
,[offtime] NVARCHAR(100)
,[ron] NUMERIC(3,0)
,[assoc_crew] NVARCHAR(200)
,[item] NVARCHAR(200)
,[lastaccepted] NVARCHAR(200)
,[duedate] NVARCHAR(200)
,[alertdate] NVARCHAR(200)
,[gracedate] NVARCHAR(200)
,[msgcode] NVARCHAR(200)
,[outdttm] DATETIME
,[crewduty_type] CHAR(2)
,[client] NVARCHAR(5)
,[spec3] NUMERIC(7,3)
,[spec4] NUMERIC(7,3)
,[speclong3] NVARCHAR(25)
,[speclong4] NVARCHAR(25)
,[specdec3] BIGINT
,[specdec4] BIGINT
,[estdepdt] DATETIME
,[leg_num] BIGINT
)
CREATE TABLE #CREWINFO1
(
  RowID INT IDENTITY
,[ROWNUMBER] BIGINT
,[CREWID] BIGINT
,[crewcode] NVARCHAR(5)
,[lognum] BIGINT
,[legid] BIGINT
,[dutyend] BIT
,[last_name] NVARCHAR(30)
,[first_name] NVARCHAR(30)
,[middleinit] NVARCHAR(30)
,[schedttm] DATETIME
,[fltdate] DATETIME
,[tail_nmbr] VARCHAR(9)
,[type_code] NVARCHAR(10)
,[tot_line] NVARCHAR(10)
,[farnum] CHAR(3)
,[crewcurr] CHAR(4)
,[depicao_id] CHAR(4)
,[arricao_id] CHAR(4)
,[cat_code] CHAR(4)
,[dutycounter] NVARCHAR(20)
,[dutytotal] NUMERIC(7,3)
,[blkout] NVARCHAR(100)
,[blkin] NVARCHAR(100)
,[dutyhrs] NUMERIC(7,3)
,[dutytype] CHAR(1)
,[blk_hrs] NUMERIC(7,3)
,[flt_hrs] NUMERIC(7,3)
,[night] NUMERIC(7,3)
,[instr] NUMERIC(7,3)
,[tkoff_d] NUMERIC(2,0)
,[tkoff_n] NUMERIC(2,0)
,[app_p] NUMERIC(2,0)
,[app_n] NUMERIC(2,0)
,[lnding_d] NUMERIC(2,0)
,[lnding_n] NUMERIC(2,0)
,[override] BIT
,[dutybeg] NVARCHAR(5)
,[end] NVARCHAR(5)
,[ontime] NVARCHAR(100)
,[offtime] NVARCHAR(100)
,[ron] NUMERIC(3,0)
,[assoc_crew] NVARCHAR(200)
,[item] NVARCHAR(200)
,[lastaccepted] NVARCHAR(200)
,[duedate] NVARCHAR(200)
,[alertdate] NVARCHAR(200)
,[gracedate] NVARCHAR(200)
,[msgcode] NVARCHAR(200)
,[outdttm] DATETIME
,[crewduty_type] CHAR(2)
,[client] NVARCHAR(5)
,[spec3] NUMERIC(7,3)
,[spec4] NUMERIC(7,3)
,[speclong3] NVARCHAR(25)
,[speclong4] NVARCHAR(25)
,[specdec3] BIGINT
,[specdec4] BIGINT
,[estdepdt] DATETIME
,[leg_num] BIGINT
)
CREATE TABLE #CREWEMPTY
(
RowID INT IDENTITY
,[ROWNUMBER] BIGINT
,[CREWID] BIGINT
,[crewcode] NVARCHAR(5)
,[lognum] BIGINT
,[legid] BIGINT
,[dutyend] BIT
,[last_name] NVARCHAR(30)
,[first_name] NVARCHAR(30)
,[middleinit] NVARCHAR(30)
,[schedttm] DATETIME
,[fltdate] DATETIME
,[tail_nmbr] VARCHAR(9)
,[type_code] NVARCHAR(10)
,[tot_line] NVARCHAR(10)
,[farnum] CHAR(3)
,[crewcurr] CHAR(4)
,[depicao_id] CHAR(4)
,[arricao_id] CHAR(4)
,[cat_code] CHAR(4)
,[dutycounter] NVARCHAR(20)
,[dutytotal] NUMERIC(7,3)
,[blkout] NVARCHAR(100)
,[blkin] NVARCHAR(100)
,[dutyhrs] NUMERIC(7,3)
,[dutytype] CHAR(1)
,[blk_hrs] NUMERIC(7,3)
,[flt_hrs] NUMERIC(7,3)
,[night] NUMERIC(7,3)
,[instr] NUMERIC(7,3)
,[tkoff_d] NUMERIC(2,0)
,[tkoff_n] NUMERIC(2,0)
,[app_p] NUMERIC(2,0)
,[app_n] NUMERIC(2,0)
,[lnding_d] NUMERIC(2,0)
,[lnding_n] NUMERIC(2,0)
,[override] BIT
,[dutybeg] NVARCHAR(5)
,[end] NVARCHAR(5)
,[ontime] NVARCHAR(100)
,[offtime] NVARCHAR(100)
,[ron] NUMERIC(3,0)
,[assoc_crew] NVARCHAR(200)
,[item] NVARCHAR(200)
,[lastaccepted] NVARCHAR(200)
,[duedate] NVARCHAR(200)
,[alertdate] NVARCHAR(200)
,[gracedate] NVARCHAR(200)
,[msgcode] NVARCHAR(200)
,[outdttm] DATETIME
,[crewduty_type] CHAR(2)
,[client] NVARCHAR(5)
,[spec3] NUMERIC(7,3)
,[spec4] NUMERIC(7,3)
,[speclong3] NVARCHAR(25)
,[speclong4] NVARCHAR(25)
,[specdec3] BIGINT
,[specdec4] BIGINT
,[estdepdt] DATETIME
,[leg_num] BIGINT
)

INSERT INTO #CREWINFO
SELECT DISTINCT
[CrewID] = CW.CrewID
,[crewcode] = CW.CrewCD			
,[lognum] = POM.LogNum 			
,[legid] = POL.POLegID			
,[dutyend] = POL.IsDutyEnd	
,[last_name] = CW.LastName			
,[first_name] = CW.FirstName			
,[middleinit] = CW.MiddleInitial			
,[schedttm] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END			
,[fltdate] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END			
,[tail_nmbr] = F.TailNUM			
,[type_code] = AC.AircraftCD			
,[tot_line] = ''			
,[farnum] = POL.FedAviationRegNum			
,[crewcurr] = POL.CrewCurrency			
,[depicao_id] = A.IcaoID			
,[arricao_id] = AA.IcaoID			
,[cat_code] = FC.FlightCatagoryCD			
,[dutycounter] = ''
,[dutytotal] = CASE WHEN POL.IsDutyEnd=1 THEN  ROUND(POC.DutyHours,1) END
,[blkout] = REPLACE(POL.BlockOut,':','') 			
,[blkin] =  REPLACE(POL.BlockIN,':','') 			
,[dutyhrs] =  ROUND(POC.DutyHours,1)			
,[dutytype]= POC.DutyTYPE 			
,[blk_hrs] =  ROUND(POC.BlockHours,1)				
,[flt_hrs] =  ROUND(POC.FlightHours,1)				
,[night] = POC.Night			
,[instr] = POC.Instrument 			
,[tkoff_d] = POC.TakeOffDay			
,[tkoff_n] = POC.TakeOffNight			
,[app_p] = POC.ApproachPrecision			
,[app_n] = POC.ApproachNonPrecision			
,[lnding_d] = POC.LandingDay			
,[lnding_n] = POC.LandingNight			
,[override] = POC.IsOverride			
,[dutybeg] = POC.BeginningDuty			
,[end] = POC.DutyEnd			
,[ontime] = CASE WHEN POL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE REPLACE(POC.BeginningDuty,':','') END
,[offtime] = CASE WHEN POL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') ELSE REPLACE(POC.DutyEnd,':','') END
,[ron] = POC.RemainOverNight			
,[assoc_crew] = ACrew.AssociatedCrew			
,[item] = ACrewChecklist.CrewChecklistDescription			
,[lastaccepted] = ACrewChecklist.PreviousCheckDT
,[duedate] = ACrewChecklist.DueDT			
,[alertdate] = ACrewChecklist.AlertDT
,[gracedate] = ACrewChecklist.GraceDT			
,[msgcode] = ACrewChecklist.msgcode
,[outdttm] = POL.OutboundDTTM			
,[crewduty_type] = CDT.DutyTypeCD			
,[client] = CT.ClientCD			
,[spec3] = POC.Specification3
,[spec4] = POC.Specification4
,[speclong3] = @speclong3
,[speclong4] = @speclong4
,[specdec3] = @specdec3
,[specdec4] = @specdec4			
,[estdepdt] = POM.EstDepartureDT			
,[leg_num] = POL.LegNUM	
FROM PostflightMain POM
INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 AND CW.IsStatus = 1
INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted = 0
INNER JOIN Company C on C.HomebaseID = POM.HomebaseID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
LEFT JOIN PostflightSimulatorLog POSL ON POSL.CrewID = POC.CrewID
LEFT JOIN Client CT ON CT.ClientID = POSL.ClientID
LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeID = POSL.DutyTypeID
INNER JOIN @TempCrewID T ON CW.CrewID=T.CrewID
INNER JOIN @TempHome TH ON CW.CrewID = TH.CrewID
LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=POM.POLogID AND DT.LegID=POL.POLegID
LEFT OUTER JOIN (
	SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
													SELECT RTRIM(C.CrewCD) + ',' FROM PostflightCrew PC 
													INNER JOIN Crew C ON PC.CrewID = C.CrewID
													WHERE PC.POLegID = PC1.POLegID
													ORDER BY C.CrewCD
													FOR XML PATH('')
												  )
	FROM PostflightCrew PC1
				) ACrew ON POL.POLegID = ACrew.POLegID
LEFT OUTER JOIN (
	SELECT DISTINCT PC1.CrewID,PC1.CustomerID,CrewChecklistDescription = (
														SELECT DISTINCT RTRIM(CCL.CrewChecklistDescription) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											PreviousCheckDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.PreviousCheckDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											DueDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.DueDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											AlertDT = (
														SELECT DISTINCT  CONVERT(VARCHAR(10),CONVERT(DATE,CCD.AlertDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											GraceDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.GraceDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											msgcode = (
														SELECT DISTINCT (CASE WHEN CCD.IsOneTimeEvent = 'True' AND CCD.IsCompleted ='True' THEN 'COMPLETED'
																			WHEN @DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT THEN 'GRACE'
																			WHEN @DATETO > CCD.DueDT THEN 'PAST DUE'
																			WHEN @DATETO BETWEEN CCD.AlertDT AND CCD.DueDT THEN 'ALERT'
																			ELSE ''
																		 END ) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														)
	FROM PostflightCrew PC1
				) ACrewChecklist ON POC.CrewID = ACrewChecklist.CrewID 
WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POM.LogNum in(SELECT DISTINCT LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND POL.IsDeleted = 0
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted = 0)
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	    AND POL.IsDutyEnd=1
	    AND POM.IsDeleted = 0
 ORDER BY [crewcode],[fltdate]  
INSERT INTO #CREWINFO1	 
SELECT TEMP.* FROM
(SELECT * FROM #CREWINFO

UNION ALL

SELECT DISTINCT
 [ROWNUMBER]=0
,[CrewID] = CW.CrewID
,[crewcode] = CW.CrewCD			
,[lognum] = POM.LogNum 			
,[legid] = POL.POLegID			
,[dutyend] = POL.IsDutyEnd	
,[last_name] = CW.LastName			
,[first_name] = CW.FirstName			
,[middleinit] = CW.MiddleInitial			
,[schedttm] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END			
,[fltdate] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END			
,[tail_nmbr] = F.TailNUM			
,[type_code] = AC.AircraftCD			
,[tot_line] = ''			
,[farnum] = POL.FedAviationRegNum			
,[crewcurr] = POL.CrewCurrency			
,[depicao_id] = A.IcaoID			
,[arricao_id] = AA.IcaoID			
,[cat_code] = FC.FlightCatagoryCD			
,[dutycounter] = ''
,[dutytotal] = CASE WHEN POL.IsDutyEnd=1 THEN  ROUND(POC.DutyHours,1)	 END
,[blkout] = REPLACE(POL.BlockOut,':','') 			
,[blkin] =  REPLACE(POL.BlockIN,':','') 			
,[dutyhrs] =  ROUND(POC.DutyHours,1)				
,[dutytype]= POC.DutyTYPE 			
,[blk_hrs] =  ROUND(POC.BlockHours,1)				
,[flt_hrs] =  ROUND(POC.FlightHours,1)		
,[night] = POC.Night			
,[instr] = POC.Instrument 			
,[tkoff_d] = POC.TakeOffDay			
,[tkoff_n] = POC.TakeOffNight			
,[app_p] = POC.ApproachPrecision			
,[app_n] = POC.ApproachNonPrecision			
,[lnding_d] = POC.LandingDay			
,[lnding_n] = POC.LandingNight			
,[override] = POC.IsOverride			
,[dutybeg] = POC.BeginningDuty			
,[end] = POC.DutyEnd			
,[ontime] = CASE WHEN POL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE REPLACE(POC.BeginningDuty,':','') END
,[offtime] = CASE WHEN POL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') ELSE REPLACE(POC.DutyEnd,':','') END
,[ron] = POC.RemainOverNight			
,[assoc_crew] = ACrew.AssociatedCrew			
,[item] = ACrewChecklist.CrewChecklistDescription			
,[lastaccepted] = ACrewChecklist.PreviousCheckDT
,[duedate] = ACrewChecklist.DueDT			
,[alertdate] = ACrewChecklist.AlertDT
,[gracedate] = ACrewChecklist.GraceDT			
,[msgcode] = ACrewChecklist.msgcode
,[outdttm] = POL.OutboundDTTM			
,[crewduty_type] = CDT.DutyTypeCD			
,[client] = CT.ClientCD			
,[spec3] = POC.Specification3
,[spec4] = POC.Specification4
,[speclong3] = @speclong3
,[speclong4] = @speclong4
,[specdec3] = @specdec3
,[specdec4] = @specdec4			
,[estdepdt] = POM.EstDepartureDT			
,[leg_num] = POL.LegNUM	
FROM PostflightMain POM
INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 AND CW.IsStatus = 1
INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted = 0
INNER JOIN Company C on C.HomebaseID = POM.HomebaseID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
LEFT JOIN PostflightSimulatorLog POSL ON POSL.CrewID = POC.CrewID
LEFT JOIN Client CT ON CT.ClientID = POSL.ClientID
LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeID = POSL.DutyTypeID
INNER JOIN @TempCrewID T ON CW.CrewID=T.CrewID
INNER JOIN @TempHome TH ON CW.CrewID = TH.CrewID
LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=POM.POLogID AND DT.LegID=POL.POLegID
LEFT OUTER JOIN (
	SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
													SELECT RTRIM(C.CrewCD) + ',' FROM PostflightCrew PC 
													INNER JOIN Crew C ON PC.CrewID = C.CrewID
													WHERE PC.POLegID = PC1.POLegID
													ORDER BY C.CrewCD
													FOR XML PATH('')
												  )
	FROM PostflightCrew PC1
				) ACrew ON POL.POLegID = ACrew.POLegID
LEFT OUTER JOIN (
	SELECT DISTINCT PC1.CrewID,PC1.CustomerID,CrewChecklistDescription = (
														SELECT DISTINCT RTRIM(CCL.CrewChecklistDescription) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											PreviousCheckDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.PreviousCheckDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											DueDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.DueDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											AlertDT = (
														SELECT DISTINCT  CONVERT(VARCHAR(10),CONVERT(DATE,CCD.AlertDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											GraceDT = (
														SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.GraceDT),101) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														),
											msgcode = (
														SELECT DISTINCT (CASE WHEN CCD.IsOneTimeEvent = 'True' AND CCD.IsCompleted ='True' THEN 'COMPLETED'
																			WHEN @DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT THEN 'GRACE'
																			WHEN @DATETO > CCD.DueDT THEN 'PAST DUE'
																			WHEN @DATETO BETWEEN CCD.AlertDT AND CCD.DueDT THEN 'ALERT'
																			ELSE ''
																		 END ) +'$$' FROM PostflightCrew PC 
														LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID AND CCD.IsDeleted=0
                                                        LEFT OUTER JOIN CrewCheckList CCL ON CCD.CheckListCD = CCL.CrewCheckCD AND CCL.CustomerID=PC.CustomerID AND CCL.IsDeleted=0
														WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
														FOR XML PATH('')
														)
	FROM PostflightCrew PC1
				) ACrewChecklist ON POC.CrewID = ACrewChecklist.CrewID 
WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POM.LogNum in(SELECT DISTINCT LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND PM.IsDeleted = 0)
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	    AND POL.IsDutyEnd=0 
	    AND POM.IsDeleted = 0 )TEMP
	ORDER BY [crewcode],[fltdate]       

DECLARE @CrRowID INT,@Crdutyend BIT,@CrROWNUMBER BIGINT,@dutyend INT;
DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR
SELECT RowID,dutyend,ROWNUMBER FROM #CREWINFO1 WHERE dutyend=0 ORDER BY RowID DESC 
OPEN C1
FETCH NEXT FROM C1 INTO @CrRowID,@Crdutyend,@CrROWNUMBER
WHILE @@FETCH_STATUS = 0
BEGIN
SELECT @dutyend=rownumber FROM #CREWINFO1 WHERE RowID=@CrRowID+1
UPDATE #CREWINFO1 SET ROWNUMBER=@dutyend WHERE RowID=@CrRowID

FETCH NEXT FROM C1 INTO @CrRowID,@Crdutyend,@CrROWNUMBER
END
CLOSE C1;
DEALLOCATE C1;

INSERT INTO #CREWEMPTY
SELECT DISTINCT
[ROWNUMBER] = 0
,[CrewID] = CW.CrewID
,[crewcode] = CW.CrewCD
,[lognum] = NULL
,[legid] = '0'
,[dutyend] = '0'
,[last_name] = CW.LastName
,[first_name] = CW.FirstName
,[middleinit] = CW.MiddleInitial
,[schedttm] = NULL
,[fltdate] = NULL
,[tail_nmbr] = NULL
,[type_code] = NULL
,[tot_line] = ''
,[farnum] = NULL
,[crewcurr] = NULL
,[depicao_id] = NULL
,[arricao_id] = NULL
,[cat_code] = NULL
,[dutycounter] = '0'
,[dutytotal] = '0'
,[blkout] = NULL
,[blkin] = NULL
,[dutyhrs] = '0'
,[dutytype]= NULL
,[blk_hrs] = '0'
,[flt_hrs] = '0'
,[night] = '0'
,[instr] = '0'
,[tkoff_d] = '0'
,[tkoff_n] = '0'
,[app_p] = '0'
,[app_n] = '0'
,[lnding_d] = '0'
,[lnding_n] = '0'
,[override] = '0'
,[dutybeg] = NULL
,[end] = NULL
,[ontime] = NULL
,[offtime] = NULL
,[ron] = '0'
,[assoc_crew] = NULL
,[item] = NULL
,[lastaccepted] = NULL
,[duedate] = NULL
,[alertdate] = NULL
,[gracedate] = NULL
,[msgcode] = NULL
,[outdttm] = NULL
,[crewduty_type] = NULL
,[client] = NULL
,[spec3] = '0'--SEE PREVIEW
,[spec4] = '0'--SEE PREVIEW
,[speclong3] = @speclong3
,[speclong4] = @speclong4
,[specdec3] = @specdec3
,[specdec4] = @specdec4
,[estdepdt] = NULL
,[leg_num] = '0'
FROM Crew CW
		INNER JOIN @TempCrewID T ON CW.CrewID=T.CrewID
		INNER JOIN @TempHome TH ON CW.CrewID = TH.CrewID
		LEFT OUTER JOIN Airport AF ON CW.HomebaseID=AF.AirportID
		LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
WHERE CW.IsDeleted=0 AND CW.IsStatus=1
AND CW.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
DELETE FROM #CREWEMPTY WHERE [crewcode] IN (SELECT DISTINCT [crewcode] FROM #CREWINFO)

 SELECT * FROM #CREWINFO1
 UNION ALL
 SELECT * FROM #CREWEMPTY
ORDER BY [crewcode],[fltdate]

IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO

IF OBJECT_ID('tempdb..#CREWEMPTY') IS NOT NULL
DROP TABLE #CREWEMPTY

IF OBJECT_ID('tempdb..#CREWINFO1') IS NOT NULL
DROP TABLE #CREWINFO1

IF OBJECT_ID('tempdb..#DUTYTOTAL') IS NOT NULL
DROP TABLE #DUTYTOTAL

--EXEC spGetReportPOSTPilotLogExportInformation 'SUPERVISOR_99','2009-01-01','2009-12-31','','','','','',1,0



GO


