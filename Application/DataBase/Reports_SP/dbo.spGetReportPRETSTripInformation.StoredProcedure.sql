IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetReportPRETSTripInformation]    
  --@UserCD AS VARCHAR(30), --Mandatory    
  --@TripID  AS BIGINT     --Mandatory    
      
 @UserCD            AS VARCHAR(30)        
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM    
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM    
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)    
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal    
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal     
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)    
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)    
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)    
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)    
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD    
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase    
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)    
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD       
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'    
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'    
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'    
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'    
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'    
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'    
       
AS    
BEGIN    
-- =============================================    
-- SPC Name: spGetReportPRETSTripInformation    
-- Author: SINDHUJA.K    
-- Create date: 24 July 2012    
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
    
 -- [Start] Report Criteria --    
 DECLARE @tblTripInfo AS TABLE (    
 TripID BIGINT    
 , LegID BIGINT    
 , PassengerID BIGINT    
 )    
    
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)    
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,    
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,    
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,     
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService    
 -- [End] Report Criteria --    
    
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';    
 DECLARE @ParameterDefinition AS NVARCHAR(500) = ''    
  
  
 DECLARE @Count INT;
 SELECT @Count = COUNT(LegID) FROM PreflightLeg WHERE TripID = (SELECT TOP 1 TripID FROM @tblTripInfo)   
 ----SET @SQLSCRIPT = '    
SELECT *,@Count CountVal  FROM(SELECT DISTINCT    
      [TripNumber] = PM.TripNUM,    
      [DispNo] = RTRIM(PM.DispatchNUM),    
   [Description] = PM.TripDescription,    
   [TripDates] =  dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD),    
   [Requestor] = P.PassengerName, --P.LastName+ ',' + P.FirstName+ ' ' + P.MiddleInitial,    
   [TailNumber] = F.TailNUM,    
   [ContactPhone] =P.AdditionalPhoneNum,    
   [Type] =A.AircraftCD,    
   [AircraftDomFlitePhone] = F.FlightPhoneNum,    
   [AircraftIntlFlitePhone] = F.FlightPhoneIntlNum,    
   [RevisionNumber] = PM.RevisionNUM,    
   [DispatcherName] = UM.FirstName + ' ' + UM.LastName, --PM.DsptnName,    
   [DispatcherPhone] = UM.PhoneNum,    
   [Dispatcheremail] = UM.EmailID,    
   [AdditionalPhone] = P.PhoneNum,    
   [ReleasedBy] = C.LastName + ',' + C.FirstName + ' ' + C.MiddleInitial,    
   [FlightNumber] = PM.FlightNUM,    
   [VerificationNumber] = PM.VerifyNUM,    
   [TSAlert] = PM.Notes     
   ,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)    
   ,PM.CustomerID, F.TailNum    
   ,LegID = CONVERT(VARCHAR,PL.LegID)    
   ,TripID = CONVERT(VARCHAR,PM.TripID)    
   ,[RevisionDescription] = PM.RevisionDescriptioin    
   ,NotesAndAnnouncements = CO.ApplicationMessage    
       
   ,CQCustomerName = CQ.CQCustomerName    
   ,CQContactNumber = CQ.BillingPhoneNum    
  
       
   FROM (SELECT DISTINCT TRIPID, LegID FROM @tblTripInfo) Main    
   INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID    
   INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID    
   LEFT OUTER JOIN  Fleet F ON PM.FleetID = F.FleetID     
   LEFT OUTER JOIN  Aircraft A ON PM.AircraftID = A.AircraftID    
   INNER JOIN (SELECT HomebaseID, CustomerID, ApplicationMessage FROM Company) CO     
   ON PM.CustomerID = CO.CustomerID    
   LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName) AND PM.CustomerID = UM.CustomerID    
   LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
   LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID    
   LEFT OUTER JOIN CQCustomer CQ ON PM.CQCustomerID = CQ.CQCustomerID    
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))    
       AND CO.HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)    
    ) A    
   ORDER BY A.TripNumber, CONVERT(INT,A.LegNUM)    
   --AND UM.UserName = @UserCD    
        
   --AND PM.TripID = @TripID     
     
END 
GO


