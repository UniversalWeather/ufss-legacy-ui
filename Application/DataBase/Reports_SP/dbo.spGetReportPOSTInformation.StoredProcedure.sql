IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTInformation]    
  (  
  @UserCD AS VARCHAR(30) 
  )  
  As  
    
   BEGIN  

 
    DECLARE @tbl AS TABLE (ROW_ID INT  IDENTITY,
                         CrewLogCustomlabel3 Varchar(30),   
                         CrewLogCustomlabel4 Varchar(30),  
                         ApprNpr Varchar(30),                         
                         ApprPr varchar(30),
                         ArrICAO varchar(30),
                         AssociatedCrew varchar(30),
                         BlkIn varchar(30),    
                         BlkOut Varchar(30),  
                         BlockTime Varchar(30),  
                         Client Varchar(30),  
                         CrewDutyType Varchar(30),  
                         DepICAO Varchar(30),  
                         DutyHours Varchar(30),  
                         FlightTime varchar(30),  
                         FltCat Varchar(30),  
                         FltTyp Varchar(30),  
                         InstTime Varchar(30),  
                         LndingDt Varchar(30),  
                         LndingNt Varchar(30),  
                         NiteTime Varchar(30),  
                         PS Varchar(30),  
                         RONs Varchar(30),  
                         T_OFFDt Varchar(30),  
                         T_OFFNt Varchar(30)  
                       )  
                       

 INSERT INTO @tbl(CrewLogCustomlabel3) VALUES('CrewLog Custom label 3')
                         
Update @tbl Set CrewLogCustomlabel3=(Select SpecificationLong3 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1        
Update @tbl Set CrewLogCustomlabel4=(Select SpecificationLong4 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1        
Update @tbl Set CrewLogCustomlabel3=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Log Custom Label 3' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1        
 Update @tbl Set CrewLogCustomlabel4=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Log Custom Label 4' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1        
 Update @tbl Set ApprNpr=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Appr  Npr' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set ApprPr=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Appr  Pr' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
 Update @tbl Set ArrICAO=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Arr. ICAO' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set AssociatedCrew=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Associated Crew' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
 Update @tbl Set BlkIn=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Blk In' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set BlkOut=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Blk Out' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set BlockTime=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Block Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set Client=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Client' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set CrewDutyType=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Duty Type' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
 Update @tbl Set DepICAO=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Dep. ICAO' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set DutyHours=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Duty Hours' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
 Update @tbl Set FlightTime=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flight Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1         
 Update @tbl Set FltCat=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flt Cat' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set FltTyp=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flt Typ' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set InstTime=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Inst Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set LndingDt=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Lnding Dt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1        
 Update @tbl Set LndingNt=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Lnding Nt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set NiteTime=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Nite Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1        
 Update @tbl Set PS=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='P S' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set RONs=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='RONs' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1   
 Update @tbl Set T_OFFDt=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='T/OFF Dt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
 Update @tbl Set T_OFFNt=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='T/OFF Nt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1  
 
 
 INSERT INTO @tbl(CrewLogCustomlabel3) VALUES(1)
 
Update @tbl Set CrewLogCustomlabel3=ISNULL((Select Top 1 IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Log Custom Label 3' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set CrewLogCustomlabel4=ISNULL((Select top 1 IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Log Custom Label 4' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set ApprNpr=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Appr  Npr' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set ApprPr=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Appr  Pr' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set ArrICAO=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Arr. ICAO' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set AssociatedCrew=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Associated Crew' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set BlkIn=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Blk In' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set BlkOut=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Blk Out' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set BlockTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Block Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set Client=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Client' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set CrewDutyType=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Crew Duty Type' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set DepICAO=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Dep. ICAO' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set DutyHours=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Duty Hours' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set FlightTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flight Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set FltCat=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flt Cat' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set FltTyp=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Flt Typ' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set InstTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Inst Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set LndingDt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Lnding Dt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set LndingNt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Lnding Nt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set NiteTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='Nite Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set PS=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='P S' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set RONs=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='RONs' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2   
 Update @tbl Set T_OFFDt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='T/OFF Dt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set T_OFFNt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='T/OFF Nt' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2  
     
 SELECT * FROM @tbl         
         
End

-- exec spGetReportPOSTInformation 'jwilliams_13'





GO


