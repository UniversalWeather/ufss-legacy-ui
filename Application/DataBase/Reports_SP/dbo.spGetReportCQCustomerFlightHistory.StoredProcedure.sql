
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQCustomerFlightHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQCustomerFlightHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- EXEC [dbo].[spGetReportCQCustomerFlightHistory]  'JWILLIAMS_13','10013','01-01-2011','02-01-2011','','','','','',''

CREATE PROCEDURE [dbo].[spGetReportCQCustomerFlightHistory]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID BIGINT,
		@DATEFROM AS DATETIME , --Mandatory
		@DATETO AS DATETIME , --Mandatory
		@IcaoID AS CHAR(4) = '', -- [Optional]
		@LogNum AS BIGINT = '', -- [Optional]
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CQCustomerCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@SortBy as varchar(1)
		)
AS		
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportCQCustomerFlightHistory
-- Author: 
-- Create date: 
-- Description: Charter Quote Customer Flight History Report
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON
--	DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
			
/************ Below set-up can be used if the query performance is poor ********************			
CREATE TABLE #TEMPTB 
  (ID				INT NOT NULL IDENTITY (1,1), 
   ICAOId			NVARCHAR(1000), 
   LogNum			BIGINT, 
   TailNum 			NVARCHAR(1000), 
   FleetGroupCD		NVARCHAR(1000),
   CustomerName		NVARCHAR(1000),
   CustomerId		BIGINT
  )

INSERT INTO #TEMPTB (ICAOId)
SELECT DISTINCT IcaoID FROM Airport WHERE IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ICAOId, ','))
INSERT INTO #TEMPTB (FleetGroupCD)
SELECT DISTINCT FleetGroupCD FROM vFleetGroup WHERE FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
INSERT INTO #TEMPTB (TailNum)
SELECT DISTINCT TailNum FROM vFleetGroup WHERE TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
INSERT INTO #TEMPTB (CustomerName)
SELECT DISTINCT CQCustomerName FROM CQCustomer WHERE CQCustomerName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CustomerName, ','))
INSERT INTO #TEMPTB (LogNum)
SELECT DISTINCT LogNum FROM PostflightMain WHERE LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ','))
INSERT INTO #TEMPTB (CustomerId) SELECT @CUSTOMERID

/*************************-----------------------**************************************/*/
SELECT * FROM 
(
	SELECT DISTINCT
		   --FL.TailNum,
		   CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [Tail Number],
		   CONVERT(DATE ,PO.EstDepartureDT) TRIPDATE, 
		   PO.DispatchNum, 
		   PO.LogNum,  
		   PL.POLegID,
		   PL.LegNUM,
		   AD.IcaoID AS DepartICAOCD ,
		   AA.IcaoID AS ArriveICAOCD,
		   (PL.Distance * 1.15078) AS Distance,
		   PL.FlightHours, PL.BlockHours, 
		   FL.MaximumPassenger - PL.PassengerTotal AS Emptseats_Tot,
		   PL.PassengerTotal AS Nbr_Pax, 
		   PAX.PaxNames,	
		   PAXDesc.PAXDesc,
		   PL.FlightPurpose AS Flt_Leg_Purp,
		   FC.FlightCatagoryCD +' '+ FC.FlightCatagoryDescription AS Flt_Cat,
		   CC.CQCustomerCD,
		   CC.CQCustomerName,
		   [TenToMin] = @TenToMin
	FROM PostflightMain PO 
		 JOIN PostflightLeg PL 
			  ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID
		 JOIN vFleetGroup FL 
			  ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID 
		 LEFT JOIN TailHistory TH
		      ON TH.OldTailNUM = FL.TailNum AND TH.CustomerID = FL.CustomerID	
		 LEFT JOIN Company C				
        ON FL.HomebaseID = C.HomebaseID				
         LEFT JOIN Airport AP				
       ON C.HomebaseAirportID = AP.AirportID				
       LEFT JOIN Airport AD				
        ON PL.DepartICAOID = AD.AirportID        				
       LEFT JOIN Airport AA				
        ON pl.ArriveICAOID = AA.AirportID    
		INNER JOIN CQMain CM ON CM.TripID = PO.TripID
        INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID    
	    INNER JOIN CQCustomer CC ON CC.CQCustomerID=CF.CQCustomerID
	    LEFT JOIN FlightCatagory FC
			  ON FC.CustomerID = PL.CustomerID AND FC.FlightCategoryID = PL.FlightCategoryID 
		 LEFT JOIN(SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.CustomerID,
						  PaxNames = (
						  SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
								   FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
								   WHERE PP1.POLegID = PP2.POLegID
								   ORDER BY P1.MiddleInitial
								 FOR XML PATH(''))
						  FROM PostflightPassenger PP2 
					) PAX 
				ON PL.POLegID = PAX.POLegID AND PL.CustomerID = PAX.CustomerID
		  LEFT JOIN(SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.CustomerID,PaxDesc = (
						  SELECT ISNULL(FP.FlightPurposeDescription+'','') +'$$'  
								   FROM PostflightPassenger PP1 
								   INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
								   INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP1.FlightPurposeID								   
								   WHERE PP1.POLegID = PP2.POLegID
								 FOR XML PATH(''))
						 FROM PostflightPassenger PP2 
					) PAXDesc 
				ON PL.POLegID = PAXDesc.POLegID AND PL.CustomerID = PAXDesc.CustomerID and PAX.PostflightPassengerListID = PAXDesc.PostflightPassengerListID
	WHERE (FL.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))OR @FleetGroupCD='')
			  AND CONVERT(DATE,PL.ScheduledTM) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
			  AND (FL.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM ='')
			  AND (AP.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ICAOId, ',')) OR @ICAOId ='')
			  AND (CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD ='')
			  AND (PO.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum ='')
			  AND PO.CustomerID = @UserCustomerID
    )A
	 ORDER BY 
	 CASE  @SortBy WHEN 'a' THEN A.CQCustomerCD
	               WHEN 'b' THEN A.CQCustomerName  END --Query perfomance is v poor if this is added

END





GO


