IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSub1PilotLogIIInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSub1PilotLogIIInformation]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPOSTSub1PilotLogIIInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@CrewID AS VARCHAR(30),-- [Optional], Comma delimited string with mutiple values
	@ChecklistCode AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTSub1PilotLogIIInformation
-- Author:  A.AKHILA
-- Create date: 
-- Description: Get Postflight SUB1 Pilot Log for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================
SET NOCOUNT ON

CREATE TABLE #PILOT(DateFROM DATE,DATETO DATE,CHECKLISTCODE CHAR(3),ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,crewid VARCHAR(100),CREW VARCHAR(5),case1 VARCHAR(25) )

CREATE TABLE #PILOT1(DateFROM DATE,DATETO DATE,CHECKLISTCODE CHAR(3),ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,crewid VARCHAR(100),CREW VARCHAR(5),case1 VARCHAR(25))

INSERT INTO #PILOT
SELECT DISTINCT
	[DateFROM]=@DATEFROM,
	[DATETO]= @DATETO, 
	[CHECKLISTCODE] = CCL.CrewCheckCD,  
	[ChecklistItem]=ccl.CrewChecklistDescription,
	[ChecklistLast]=CCD.PreviousCheckDT,
	[ChecklistDue]= CCD.DueDT,
	[ChecklistAlert]= CCD.AlertDT,
	[ChecklistGrace]= CCD.GraceDT,
	[crewid]=c.crewid,[CREW]=C.CREWCD,
	[case1]=  (CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
			   WHEN (@DATETO > CCD.DUEDT) THEN 'PASSED DUE' END)
FROM PostflightLeg PL 
	INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0
	INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
	INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID and CCD.CustomerID= pc.CustomerID
	INNER JOIN Crew C ON C.CrewID = PC.CrewID
	INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and  ccl.IsDeleted =0 and CCD.IsDeleted=0 and CCD.CustomerID= ccl.CustomerID
	LEFT OUTER JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  
WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
    AND PL.IsDeleted=0
	AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
	AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
	AND c.IsDeleted = 0 --AND c.IsStatus = 1 

INSERT INTO #PILOT1
SELECT DISTINCT
	[DateFROM]= @DATEFROM,
	[DATETO]= @DATETO,
	[CHECKLISTCODE] =NULL,    
	[ChecklistItem]=NULL,
	[ChecklistLast]=NULL,
	[ChecklistDue]=NULL,
	[ChecklistAlert]=NULL,
	[ChecklistGrace]=NULL,
	[crewid]=c.crewid,[CREW]=C.CREWCD,
	[case1]=  NULL
FROM CREW C
WHERE C.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and c.IsDeleted = 0 --AND c.IsStatus = 1  
	AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
DELETE FROM #PILOT1 WHERE Crew IN(SELECT DISTINCT Crew FROM #PILOT)

SELECT * FROM #PILOT1
UNION ALL           
SELECT * FROM #PILOT

IF OBJECT_ID('tempdb..#PILOT1') IS NOT NULL
DROP TABLE #PILOT1
IF OBJECT_ID('tempdb..#PILOT') IS NOT NULL
DROP TABLE #PILOT	
--EXEC spGetReportPOSTSub1PilotLogIIInformation 'SUPERVISOR_99','2000/1/1','2012/12/12','',''
GO


