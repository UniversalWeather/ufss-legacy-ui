IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripCostQuoteExprotInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripCostQuoteExprotInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETripCostQuoteExprotInformation]
    @UserCD AS VARCHAR(30),
    @UserCustomerID BIGINT,
	@DATEFROM DATETIME, --MANDATORY  
    @DATETO DATETIME, --MANDATORY 
    @IcaoID AS VARCHAR(80) = ''	

AS
BEGIN

--EXEC spGetReportPRETripCostQuoteExprotInformation 'SUPERVISOR_99',10099,'2013-01-01','2013-12-01',''

DECLARE @TenToMin SMALLINT = 0;

 

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

SET NOCOUNT ON;

DECLARE @TripCost TABLE(AircraftCD CHAR(4),
                        TailNo VARCHAR(9),
                        TripNum BIGINT,
                        LegNUM  BIGINT,
                        EsDepartureDT DATE,
                        ArrivelDTTMLocal DATETIME,
                        DepartureDTTMLocal DATETIME,
                        Depart CHAR(4),
                        Arrive CHAR(4),
                        Distance NUMERIC(5,0),
                        ElapseTM NUMERIC(7,3),
                        ChargeRate NUMERIC(17,2),
                        CostQuote NUMERIC(17,2),
                        ChargeUnit CHAR(1),
                        CityName VARCHAR(25)                       
       )
                        
DECLARE @TripCostQuote TABLE(AircraftCD CHAR(4),
							TailNo VARCHAR(9),
							TripNum BIGINT,
							LegNUM BIGINT,
							EsDepartureDT DATE,
							ArrivelDTTMLocal DATETIME,
							DepartureDTTMLocal DATETIME,
							Depart CHAR(4),
							Arrive CHAR(4),
							Distance NUMERIC(5,0),
							ElapseTM NUMERIC(7,3),
							ChargeRate NUMERIC(17,2),
							CostQuote NUMERIC(17,2),
							ChargeUnit CHAR(1),
							CityName VARCHAR(25),
							ICAOID CHAR(4))

INSERT INTO @TripCost
SELECT DISTINCT 
       FE.AircraftCD, 
       FE.TailNum AS [Tail Number], 
       PM.TripNUM,
       PL.LegNUM, 
       EstDepartureDT=PL.DepartureDTTMLocal,
       PL.ArrivalDTTMLocal, 
       PL.DepartureDTTMLocal,
       AD.IcaoID AS [DEPART],
       AA.IcaoID AS [ARRIVE], 
       PL.Distance,
       PL.ElapseTM, 
       CR.ChargeRate,
       CASE WHEN  CR.ChargeUnit = 'N' THEN PL.Distance*CR.ChargeRate
            WHEN  CR.ChargeUnit = 'S' THEN PL.Distance*CR.ChargeRate*1.15078
            WHEN  CR.ChargeUnit = 'H' THEN PL.ELAPSETM*CR.ChargeRate END AS [COSTQUOTE],
       CR.ChargeUnit,
       AA.CityName    
FROM PreflightMain PM 
   JOIN PreflightLeg PL
        ON PM.TripID = PL.TripID AND PL.IsDeleted = 0 
   JOIN Fleet FE 
        ON PM.FleetID = FE.FleetID 
   LEFT JOIN TailHistory TH 
        ON TH.OldTailNUM = FE.TailNum AND TH.CustomerID = FE.CustomerID
   LEFT JOIN Company C
       ON PM.HomebaseID = C.HomebaseID
   JOIN Airport AP
        ON C.HomebaseAirportID = AP.AirportID   
   JOIN Airport AD
        ON PL.DepartICAOID = AD.AirportID        
   JOIN Airport AA
        ON pl.ArriveICAOID = AA.AirportID        
   LEFT JOIN FleetChargeRate FC
       ON FE.FleetID = FC.FleetID 
   LEFT OUTER JOIN (
			SELECT LegID, ChargeUnit, ChargeRate
			FROM PreflightMain PM INNER JOIN  PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			WHERE CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.DepartureDTTMLocal) AND EndRateDT>=CONVERT(DATE,PL.DepartureDTTMLocal)
			AND PM.IsDeleted = 0
           )CR ON PL.LEGID = CR.LegID      
   WHERE PM.TripStatus = 'T' 
     AND PM.TripNUM IN ( SELECT PM.TripNUM FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TRIPID=PL.TRIPID AND PL.IsDeleted = 0
                   WHERE CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN @DATEFROM And @DATETO AND PM.CustomerID=@UserCustomerID AND PM.IsDeleted = 0)
         AND PM.CustomerID = @UserCustomerID 
         AND PM.IsDeleted = 0
    ORDER BY AA.IcaoID,TripNUM,LegNUM

 
DECLARE @CrArrive CHAR(4),@CrCityName VARCHAR(25);



DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR SELECT DISTINCT Arrive,CityName FROM @TripCost ORDER BY Arrive
OPEN C1

FETCH NEXT FROM C1 INTO @CrArrive,@CrCityName
WHILE @@FETCH_STATUS = 0
BEGIN

 
INSERT INTO @TripCostQuote(AircraftCD,TailNo,TripNum,LegNUM,EsDepartureDT,ArrivelDTTMLocal,DepartureDTTMLocal,Depart,
                           Arrive,Distance,ElapseTM,ChargeRate,CostQuote,ChargeUnit,CityName,ICAOID)
SELECT AircraftCD,TailNo,TripNum,LegNUM,EsDepartureDT,ArrivelDTTMLocal,DepartureDTTMLocal,Depart,
                           Arrive,Distance,ElapseTM,ChargeRate,CostQuote,ChargeUnit,@CrCityName,@CrArrive FROM @TripCost
	    WHERE TripNum IN(SELECT TripNum FROM @TripCost WHERE  Arrive =@CrArrive)
 
 
FETCH NEXT FROM C1 INTO @CrArrive,@CrCityName
END
CLOSE C1;
DEALLOCATE C1;

SELECT *,@TenToMin TenToMin, '' AS  Asterik FROM @TripCostQuote 
WHERE  (IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ','))OR @IcaoID='')
ORDER BY CityName,TripNum,LegNUM
 
 END
 

GO


