IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationAPDXExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXExport]    
    @UserCD            AS VARCHAR(30)          
   ,@TripNUM           AS VARCHAR(300) = ''  --      PreflightMain.TripNUM      
   ,@LegNUM            AS VARCHAR(300) = ''  --      PreflightLeg.LegNUM    
   ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)      
   ,@BeginDate         AS DATETIME = NULL --      PreflightLeg.DepartureDTTMLocal      
   ,@EndDate           AS DATETIME = NULL --      PreflightLeg.ArrivalDTTMLocal       
   ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)      
   ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)      
   ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)      
   ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)      
   ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD      
   ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase      
   ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)      
   ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD         
       
   ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'      
   ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'      
   ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'      
   ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'      
   ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'      
   ,@IsScheduledService      AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'       
AS    
-- ==========================================================================================    
-- SPC Name: spGetReportPRETSGeneralDeclarationAPDXExport    
-- Author: ABHISHEK.S    
-- Create date: 3rd October 2012    
-- Description: Get TripSheetReportWriter General Declaration APDX Export    
--              Information For Reports    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- ==========================================================================================    
BEGIN    
    
-- [Start] Report Criteria --    
 DECLARE @tblTripInfo AS TABLE (    
 TripID BIGINT    
 , LegID BIGINT    
 , PassengerID BIGINT    
 )    
    
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)    
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,    
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,    
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,     
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService    
 -- [End] Report Criteria --    
     
 SET NOCOUNT ON;    
     
DECLARE @TempTable TABLE(ID INT IDENTITY,  
                    owner VARCHAR(100),  
       orig_nmbr BIGINT,  
       tail_nmbr VARCHAR(20),  
       type_code VARCHAR(10),  
       fltno VARCHAR(20),  
       legid BIGINT,  
       leg_num BIGINT,  
       locdeparture DATE,  
       depicao_id VARCHAR(100),  
       arricao_id VARCHAR(100),  
       depcity VARCHAR(100),  
       arrcity VARCHAR(100),  
       crewcode1 VARCHAR(10),  
       crewname1 VARCHAR(100),  
       ppnum1 VARCHAR(100),  
       nation1 CHAR(3),  
       crewcode2 VARCHAR(10),  
       crewname2 VARCHAR(100),  
       ppnum2 VARCHAR(100),  
       nation2 CHAR(3),  
       crewcode3 VARCHAR(10),  
       crewname3 VARCHAR(100),  
       ppnum3 VARCHAR(100),  
       nation3 CHAR(3),  
       crewcode4 VARCHAR(10),  
       crewname4 VARCHAR(100),  
       ppnum4 VARCHAR(100),  
       nation4 CHAR(3),  
       crewcode5 VARCHAR(10),  
       crewname5 VARCHAR(100),  
       ppnum5 VARCHAR(100),  
       nation5 CHAR(3),  
       crewcode6 VARCHAR(10),  
       crewname6 VARCHAR(100),  
       ppnum6 VARCHAR(100),  
       nation6 CHAR(3),  
       crewcode7 VARCHAR(10),  
       crewname7 VARCHAR(100),  
       ppnum7 VARCHAR(100),  
       nation7 CHAR(3),
       embarked  INT,
       disembarked INT
       )   
 
            
DECLARE @Crowner VARCHAR(100),@Crorig_nmbr BIGINT,@Crtail_nmbr VARCHAR(20),@Crtype_code VARCHAR(10),@Crfltno VARCHAR(20);  
DECLARE @Crlegid BIGINT,@Crleg_num BIGINT,@Crlocdep DATE,@Crdepicao_id VARCHAR(100),@Crarricao_id VARCHAR(100),@Crdepcity VARCHAR(100);  
DECLARE @Crarrcity VARCHAR(100),@Crcrewcode VARCHAR(10),@Crcrewname VARCHAR(100);  
DECLARE @Crppnum VARCHAR(100),@Crnation CHAR(3);
DECLARE @Crembarked BIGINT,@Crdisembarked BIGINT;   
DECLARE @Prevorig_nmbr BIGINT=NULL,@Prevleg_num BIGINT=NULL; 
 
DECLARE @COUNT INT=0,@ID INT; 
    
DECLARE CurGeneralDecl CURSOR FAST_FORWARD READ_ONLY FOR   
		SELECT DISTINCT FP.OwnerLesse,
		       PM.TripNUM,F.TailNUM,		   
               ACF.AircraftCD,
		       PM.FlightNUM,
		       PL.LegID,
		       PL.LegNUM,
		       PL.DepartureDTTMLocal,
		       A.IcaoID,
		       B.IcaoID,		       
		       RTRIM(A.CityName +', '+ A.CountryName),
		       RTRIM(B.CityName +', '+ B.CountryName),
		       CR.CrewCD,    
			   RTRIM(ISNULL(CR.FirstName,'') +' '+ ISNULL(CR.MiddleInitial,'') +' '+ ISNULL(CR.LastName,'')),			   
			   --dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),
			   ISNULL(CPP.PassportNum,''),
			   ISNULL(C.CountryCD,C1.CountryCD),
			   embarked  = 0,
               disembarked = 0  
			      
         FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main    
				INNER JOIN  PREFLIGHTMAIN PM
						ON Main.TripID = PM.TripID    
				INNER JOIN  PreflightLeg PL
						ON PM.TripID = PL.TripID  AND Main.LEGID = PL.LEGID     
				INNER JOIN Fleet F    
						ON PM.FleetID = F.FleetID
			    LEFT OUTER JOIN Aircraft ACF
						ON F.AircraftID = ACF.AircraftID     
				 LEFT JOIN FleetPair FP    
						ON F.FleetID = FP.FleetID    
				 LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS A    
						ON A.AirportID = PL.DepartICAOID    
				 LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS B    
						ON B.AirportID = PL.ArriveICAOID    
				 LEFT JOIN PreflightCrewList PC    
						ON PL.LegID = PC.LegID    
				 LEFT  JOIN (SELECT CrewID, CrewCD,FirstName,MiddleInitial,LastName,CountryID FROM Crew) CR    
						ON PC.CrewID = CR.CrewID     
				 LEFT JOIN CrewPassengerPassport CPP    
                        ON PC.PassportID = CPP.PassportID    
				 LEFT JOIN Country C    
						ON CPP.CountryID = C.CountryID  
				LEFT JOIN Country C1    
						ON CR.CountryID = C1.CountryID  
             
        WHERE PM.IsDeleted = 0    
     ORDER BY PM.TripNUM, PL.LegNUM   
  
 OPEN CurGeneralDecl;      
 FETCH NEXT FROM curGeneralDecl INTO @Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,  
                                     @Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,
                                     @Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked;  
 WHILE @@FETCH_STATUS = 0      
 BEGIN    
--PRINT @Prevorig_nmbr  
--PRINT @Prevleg_num  
--PRINT @COUNT  

IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num 
BEGIN
SET @COUNT=@COUNT+1
END

 IF @Prevorig_nmbr IS NULL AND @Prevleg_num IS NULL  
 BEGIN  
 INSERT INTO @TempTable(owner,orig_nmbr,tail_nmbr,type_code,fltno,legid,leg_num,locdeparture,depicao_id,arricao_id,depcity,arrcity,crewcode1,crewname1,ppnum1,nation1,embarked,disembarked)  
             VALUES(@Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,@Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,@Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked)  
 --SET @COUNT=@COUNT+1  
 --SELECT * FROM @TempTable  
 END  
  
  --ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=1  
  --BEGIN  
  --UPDATE @TempTable SET crewcode1=@Crcrewcode,crewname1=@Crcrewname,ppnum1=@Crppnum,nation1=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  
  --SET @COUNT=@COUNT+1  
  --END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=1  
  BEGIN 
   
  UPDATE @TempTable SET crewcode2=@Crcrewcode,crewname2=@Crcrewname,ppnum2=@Crppnum,nation2=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  

 -- PRINT @COUNT
  END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=2  
  BEGIN  
  UPDATE @TempTable SET crewcode3=@Crcrewcode,crewname3=@Crcrewname,ppnum3=@Crppnum,nation3=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  

  END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=3  
  BEGIN  
  UPDATE @TempTable SET crewcode4=@Crcrewcode,crewname4=@Crcrewname,ppnum4=@Crppnum,nation4=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  
  
  END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=4  
  BEGIN  
  UPDATE @TempTable SET crewcode5=@Crcrewcode,crewname5=@Crcrewname,ppnum5=@Crppnum,nation5=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  
 
  END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=5  
  BEGIN  
  UPDATE @TempTable SET crewcode6=@Crcrewcode,crewname6=@Crcrewname,ppnum6=@Crppnum,nation6=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  
  
  END  
    
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT=6  
  BEGIN  
  UPDATE @TempTable SET crewcode7=@Crcrewcode,crewname7=@Crcrewname,ppnum7=@Crppnum,nation7=@Crnation WHERE orig_nmbr= @Crorig_nmbr AND leg_num=@Crleg_num AND ID=@ID  
  
  END  
  
 
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num=@Prevleg_num AND @COUNT =7  
  BEGIN  
   INSERT INTO @TempTable(owner,orig_nmbr,tail_nmbr,type_code,fltno,legid,leg_num,locdeparture,depicao_id,arricao_id,depcity,arrcity,crewcode1,crewname1,ppnum1,nation1,embarked,disembarked)  
                  VALUES(@Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,@Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,@Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked)  
 SET @COUNT=0 
 END  
 
  ELSE IF @Crorig_nmbr= @Prevorig_nmbr AND @Crleg_num!=@Prevleg_num 
  BEGIN  
   INSERT INTO @TempTable(owner,orig_nmbr,tail_nmbr,type_code,fltno,legid,leg_num,locdeparture,depicao_id,arricao_id,depcity,arrcity,crewcode1,crewname1,ppnum1,nation1,embarked,disembarked)  
                  VALUES(@Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,@Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,@Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked)  
 SET @COUNT=0 
 END  
 
  ELSE IF @Crorig_nmbr!=@Prevorig_nmbr
  BEGIN  
   INSERT INTO @TempTable(owner,orig_nmbr,tail_nmbr,type_code,fltno,legid,leg_num,locdeparture,depicao_id,arricao_id,depcity,arrcity,crewcode1,crewname1,ppnum1,nation1,embarked,disembarked)  
                  VALUES(@Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,@Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,@Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked)  
 SET @COUNT=0 
 END  
  
SET @ID=SCOPE_IDENTITY()   
SET @Prevleg_num=@Crleg_num;  
SET @Prevorig_nmbr=@Crorig_nmbr;  
FETCH NEXT FROM curGeneralDecl INTO @Crowner,@Crorig_nmbr,@Crtail_nmbr,@Crtype_code,@Crfltno,@Crlegid,  
                                     @Crleg_num,@Crlocdep,@Crdepicao_id,@Crarricao_id,@Crdepcity,@Crarrcity,@Crcrewcode,@Crcrewname,@Crppnum,@Crnation,@Crembarked,@Crdisembarked;     
END    
CLOSE CurGeneralDecl;                
DEALLOCATE CurGeneralDecl;   
SELECT dbo.GetShortDateFormatByUserCD(@UserCD,locdeparture)  locdep,* FROM @TempTable T  
END     
    
 -- EXEC spGetReportPRETSGeneralDeclarationAPDXExport 'TIM', 20, ''    
     
 -- EXEC spGetReportPRETSGeneralDeclarationAPDXExport 'SUPERVISOR_99', '1992', '1', '', '20120720', '20120725', '', '', '', '', '', '', '','',1,1,1,1,1,1 



GO


