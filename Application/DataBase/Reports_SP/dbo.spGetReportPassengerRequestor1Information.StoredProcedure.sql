IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor1Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPassengerRequestor1Information]
	@UserCD VARCHAR(30)
	,@PassengerCD varchar(500)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor1Information
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Get Passenger/CrewPassengerVisa Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

--SET @SQLSCRIPT = '
		SELECT
		P.CustomerID
		,P.PassengerRequestorCD
		,D.DepartmentCD
		,P.PassengerName
		,D.DepartmentName
		,P.LastName
		,DA.AuthorizationCD
		,P.FirstName
		,P.AuthorizationDescription
		,P.MiddleInitial
		--,[DateOfBirth] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[DateOfBirth] = dbo.GetShortDateFormatByUserCD(@UserCD,P.DateOfBirth)
		,[Status] = P.IsActive
		,P.EmployeeID
		,[Nationality] = CN.CountryName
		,[PhoneNum] = P.AdditionalPhoneNum
		,P.StandardBilling
		,[FaxNum] = P.BusinessFax
		,[IsEmployeeType] = CASE P.IsEmployeeType 
								WHEN 'C' THEN 'CONTROL'
								WHEN 'N' THEN 'NONCONTROL'
								WHEN 'G' THEN 'GUEST'
								ELSE '-' END
		,P.AssociatedWithCD 
		,C.ClientCD
		,A.IcaoID
		--,dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,'')) AS PersonalIDNum
		,ISNULL(P.PersonalIDNum,'') AS PersonalIDNum
		,P.EmailAddress
		,P.CompanyName		
		,P.Notes
	FROM Passenger P
		LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD,DepartmentName FROM Department
			) D ON P.DepartmentID = D.DepartmentID
		LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client
			) C ON P.ClientID = C.ClientID
		LEFT OUTER JOIN ( SELECT AirportID,IcaoID FROM AIRPORT) A
	            ON P.HomebaseID = A.AirportID
	    LEFT OUTER JOIN Country CN ON P.CountryID = CN.CountryID
	WHERE P.IsDeleted = 0 AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
	--SET @SQLSCRIPT = @SQLSCRIPT + ' AND P.PassengerRequestorCD IN (''' + REPLACE(CASE WHEN RIGHT(@PassengerCD, 1) = ',' THEN LEFT(@PassengerCD, LEN(@PassengerCD) - 1) ELSE @PassengerCD END, ',', ''', ''') + ''')';
	AND P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) 
	--PRINT @SQLSCRIPT
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30)'
	--EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD

-- EXEC spGetReportPassengerRequestor1Information 'JWILLIAMS_13', 'LABIN'
--CCHIN,CCARR,TBENN,DDYE



GO


