IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogInformationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogInformationSub] 
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
                          
  CREATE procedure [dbo].[spGetReportPOSTFlightLogInformationSub]    
  (  
  @UserCD AS VARCHAR(30) 
  )  
  As  
    
  BEGIN  

 
  DECLARE @tbl AS TABLE (MILEAGE Varchar(30),   
                         BLOCKHRS Varchar(30),  
                         FLIGHTHRS Varchar(30),                         
                         UTCOUT varchar(30),
                         UTCOFF varchar(30),
                         UTCON varchar(30),
                         UTCIN varchar(30),    
                         DELAYTYPE Varchar(30),  
                         DELAYTIME Varchar(30),  
                         PASSENGERS Varchar(30),  
                         FUELOUT Varchar(30),  
                         FUELIN Varchar(30),  
                         FUELBURN Varchar(30),  
                         PICSIC varchar(30),  
                         DayTakeoff Varchar(30),  
                         NightTakeoff Varchar(30),  
                         DayLanding Varchar(30),  
                         NightLanding Varchar(30),  
                         PrecApp Varchar(30),  
                         NonPrecApp Varchar(30),  
                         INSTTIME Varchar(30),  
                         NIGHTTIME Varchar(30),  
                         ADDLCREW Varchar(30),  
                         DUTYON Varchar(30),  
                         DUTYOFF Varchar(30),  
                         TOTALDUTY Varchar(30),
                         yy Varchar(30),
                         zz Varchar(30) )   
                         Insert Into @tbl(MILEAGE) Values('Mileage')
         
 Update @tbl Set MILEAGE= (Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='MILEAGE')         
 Update @tbl Set BLOCKHRS=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='BLOCK HRS')        
 Update @tbl Set FLIGHTHRS=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='FLIGHT HRS')        
 Update @tbl Set UTCOUT=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='UTC OUT')        
 Update @tbl Set UTCOFF=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='UTC OFF')        
 Update @tbl Set UTCON=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='UTC ON')        
 Update @tbl Set UTCIN=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='UTC IN')        
 Update @tbl Set DELAYTYPE=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='DELAY TYPE')        
 Update @tbl Set DELAYTIME=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='DELAY TIME')        
 Update @tbl Set PASSENGERS=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='PASSENGERS')        
 Update @tbl Set FUELOUT=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='FUEL OUT')        
 Update @tbl Set FUELIN=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='FUEL IN')        
 Update @tbl Set FUELBURN=(Select Top 1 CustomDescription From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='FUEL BURN')        
 Update @tbl Set PICSIC=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='PIC/SIC')        
 Update @tbl Set INSTTIME=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='INST TIME')        
 Update @tbl Set NIGHTTIME=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='NIGHT TIME')        
 Update @tbl Set ADDLCREW=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='ADDL CREW')        
 Update @tbl Set DUTYON=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='DUTY ON')        
 Update @tbl Set DUTYOFF=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='DUTY OFF')        
 Update @tbl Set TOTALDUTY=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='TOTAL DUTY')        
 Update @tbl Set yy=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='CrewLog Custom label 1')        
 Update @tbl Set zz=(Select Top 1 CustomDescription From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription='CrewLog Custom label 2')  
     
 Select * from @tbl         
         
End


GO
-- exec spGetReportPOSTFlightLogInformationSub 'jwilliams_11'