IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCanPassInformation_crewPass]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_crewPass]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_crewPass]
	 @UserCD AS VARCHAR(30)    
    ,@LegID AS VARCHAR(30)
    ,@PassengerCD VARCHAR(30) = ''
AS
BEGIN

	-- =========================================================================
	-- SPC Name: spGetReportPRETSCanPassInformation
	-- Author: SINDHUJA.K
	-- Create date: 28 Sep 2012
	-- Description: Get Preflight CanPass Report Export information for REPORTS
	-- Revision History
	-- Date		Name		Ver		Change
	-- 
	-- ==========================================================================
SET NOCOUNT ON

Declare @CrewOrPass TABLE(Type CHAR(1),
                          SurName VARCHAR(30),
                          FirstName VARCHAR(20),
                          INI VARCHAR(20),
                          Date_Of_Birth DATETIME,
                          Sex CHAR(1),
                          Citizenship VARCHAR(60),
                          Country_Of_Residence VARCHAR(60),
                          Length_Of_Stay_Absence INT,
                          Trip_Reason VARCHAR(30), 
                          Declaration VARCHAR(30),
                          CanPass VARCHAR(25),
                          DutyType CHAR(2) )

	
	------Crew Heading-------
	INSERT INTO @CrewOrPass
	SELECT Crew.* FROM
	(SELECT DISTINCT 		
		
		[TYPE] ='C'
		,[SURNAME] = CR.LastName
		,[FIRST_NAME] = CR.FirstName
		,[INI] = LEFT(ISNULL(CR.MiddleInitial,' '),1)
		,[DATE_OF_BIRTH] = CR.BirthDT
		,[SEX] = CR.Gender
		,[CITIZENSHIP] = CCC.CountryName
		,[COUNTRY_OF_RESIDENCE] =CCR.CountryName
		--,[LENGTH_OF_STAY_ABSENCE] = Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER)
		,[LENGTH_OF_STAY_ABSENCE] = CASE 
									WHEN AA.CountryName = 'CANADA' AND PLNEXT.CountryName = 'CANADA' 
										THEN DATEDIFF(D,PL.ArrivalDTTMLocal,PLNEXT.DepartureDTTMLocal) 
									ELSE 1 END
		,[TRIP_REASON] = 'CREW MEMBER'-- If Crew Member display Trip Reason as "CREW MEMBER"
		,[Declaration] = ''--Remains Empty
		,[canpass] = CO.CanPassNum
		,[DutyTYPE]=PCL.DutyTYPE
		from  PreflightMain PM 
		 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		 INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
		 INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
		 --LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
		 INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
		 LEFT OUTER JOIN Crew CR ON PCL.CrewID = CR.CrewID
		 LEFT OUTER JOIN Country CCC ON CR.Citizenship = CCC.CountryID
		 LEFT OUTER JOIN Country CCR ON CR.CountryID = CCR.CountryID
		 LEFT OUTER JOIN (
			SELECT PL.TripID, PL.LEGID, PL.LegNUM, PL.DepartureDTTMLocal, AD.CountryName  
			FROM PreflightLeg PL 
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
		 ) PLNEXT ON PLNEXT.TripID = PM.TripID AND PL.LegNUM + 1 = PLNEXT.LegNUM
		 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		 AND PL.LegID = CONVERT(BIGINT, @LegID)
		 )Crew
		 ORDER BY (CASE DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)  

		----------------Passenger Heading-------------
		INSERT INTO @CrewOrPass
		SELECT DISTINCT
		[TYPE] ='P'		
		,[SURNAME] = PP.LastName
		,[FIRST_NAME] = PP.FirstName
		,[INI] = LEFT(ISNULL(PP.MiddleInitial,' '),1)
		,[DATE_OF_BIRTH] = PP.DateOfBirth
		,[SEX] = PP.Gender
		,[CITIZENSHIP] = CPC.CountryName
		,[COUNTRY_OF_RESIDENCE] = CPR.CountryName
		--,[LENGTH_OF_STAY_ABSENCE] = Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER)
		,[LENGTH_OF_STAY_ABSENCE] = CASE 
							WHEN AA.CountryName = 'CANADA' AND PLNEXT.CountryName = 'CANADA' 
								THEN DATEDIFF(D,PL.ArrivalDTTMLocal,PLNEXT.DepartureDTTMLocal) 
							ELSE 1 END
		,[TRIP_REASON] = FPP.FlightPurposeDescription
		,[Declaration] = ''--Remains Empty
		,[canpass] = CO.CanPassNum
		,[DutyTYPE]='' 
		 from  PreflightMain PM 
		 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		 INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
		 INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
		 --LEFT OUTER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
		 INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
		 LEFT OUTER JOIN Passenger PP ON PPL.PassengerID = PP.PassengerRequestorID
		 LEFT OUTER JOIN Country CPC ON PP.CountryID = CPC.CountryID
		 LEFT OUTER JOIN Country CPR ON PP.CountryOfResidenceID = CPR.CountryID
		 LEFT OUTER JOIN FlightPurpose FPP ON PP.FlightPurposeID = FPP.FlightPurposeID 
		 LEFT OUTER JOIN (
			SELECT PL.TripID, PL.LEGID, PL.LegNUM, PL.DepartureDTTMLocal, AD.CountryName  
			FROM PreflightLeg PL 
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
		 ) PLNEXT ON PLNEXT.TripID = PM.TripID AND PL.LegNUM + 1 = PLNEXT.LegNUM
		 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		 AND PP.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN PP.PassengerRequestorCD ELSE @PassengerCD END
		 AND PL.LegID = CONVERT(BIGINT, @LegID) 
		 ORDER BY PP.FirstName
		 
		 
		 DECLARE  @Count INT=(SELECT COUNT(SurName) FROM @CrewOrPass WHERE Type='P')
		 
		  SELECT 'P' Type,'BLOCKED' SurName,NULL FirstName,NULL INI,NULL Date_Of_Birth ,NULL Sex ,NULL Citizenship,NULL Country_Of_Residence,NULL Length_Of_Stay_Absence,NULL Trip_Reason,NULL Declaration,NULL CanPass,NULL DutyType FROM PreflightLeg PL 
		                              WHERE LegID=@LegID 
		                              GROUP BY LegID,PassengerTotal
		                              HAVING(PL.PassengerTotal -@Count)>0
		                              
	    UNION ALL
		 SELECT * FROM @CrewOrPass WHERE Type='P'
		 UNION ALL
		 SELECT * FROM @CrewOrPass WHERE Type='C'
		
		
		 
		 
END

 --EXEC spGetReportPRETSCanPassInformation_crewPass 'JWILLIAMS_11', '100115286'
 
 
 




GO


