IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCriteria2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCriteria2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSCriteria2](
	@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(MAX) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService      AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
) 
AS

----------------
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsScheduledService = 1
		SET @TripStatus = @TripStatus + 'S,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		
----------------------------

    DECLARE @tblInfo AS TABLE (
		TripID VARCHAR(30)
	   ,LegNUM VARCHAR(30)
	   ,TailNUM VARCHAR(300)
	   )
    
    BEGIN
    IF @TripNUM = '' 
	BEGIN
	
	INSERT INTO @tblInfo(TripID,LegNUM, TailNUM) 
	SELECT CONVERT(VARCHAR(30),PM.TripID), CONVERT(VARCHAR(30), PL.LegNUM),CONVERT(VARCHAR(300),F.TailNUM)
		FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
		INNER JOIN  Fleet F ON PM.FleetID = F.FleetID 
		INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
		INNER JOIN Airport HB ON CO.HomebaseAirportID = HB.AirportID
		--INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
		LEFT OUTER JOIN (SELECT LegID, CrewID,DutyType FROM PreflightCrewList WHERE IsDeleted=0) PC ON PL.LegID = PC.LegID 
		LEFT OUTER JOIN (SELECT CrewID, CrewCD FROM Crew) C ON PC.CrewID = C.CrewID
		LEFT OUTER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID AND PPL.IsDeleted=0
		LEFT OUTER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID 
		LEFT OUTER JOIN Passenger REQ ON PM.PassengerRequestorID = REQ.PassengerRequestorID
		LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID
		LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID	
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		AND PM.RecordType = 'T'
		AND PM.IsDeleted=0 
		--AND (PM.TripNUM  IN (@TripNUM) OR @TripNUM = '')  
		--AND (PL.LegNUM  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')  
		--AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
		AND CONVERT(DATE, PM.EstDepartureDT, 101) BETWEEN CONVERT(DATE, @BeginDate, 101) AND CONVERT(DATE, @EndDate, 101)
		AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')   
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')   
		AND (DA.AuthorizationCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')    
		AND (FC.FlightCatagoryCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CatagoryCD , ',')) OR @CatagoryCD = '')  
		AND ( PM.ClientID IN (SELECT ClientID FROM Client WHERE ClientCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD  , ','))) OR @ClientCD = '')
		AND (REQ.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD , ',')) OR @RequestorCD = '')  
		AND ((C.CrewCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD , ',')) AND PC.DutyType IN('P','S'))  OR @CrewCD = '') 	
		AND (HB.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD , ',')) OR @HomeBaseCD = '') 
		AND PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ','))
	END
	ELSE    
    BEGIN
    INSERT INTO @tblInfo (TripID, LegNUM, TailNUM)
    SELECT  CONVERT(VARCHAR(30),PM.TripID), CONVERT(VARCHAR(30), PL.LegNUM),CONVERT(VARCHAR(300),F.TailNUM)
			FROM PreflightMain PM 
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
			INNER JOIN  Fleet F ON PM.FleetID = F.FleetID
			--INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			LEFT OUTER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  AND PPL.IsDeleted=0
			LEFT OUTER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND (PM.TripNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '')
			AND (PL.LegNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')  
			AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			--AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR @TripStatus = '' )
			AND PM.RecordType = 'T' 
			AND PM.IsDeleted=0
	END

--SELECT DISTINCT [TripNUM] = CONVERT(VARCHAR(30),TripNUM),
--	[LegNUM] = CONVERT(VARCHAR(30),LegNUM), ISNULL(PassengerRequestorCD,'') AS PassengerRequestorCD FROM @tblInfo

SELECT DISTINCT PP2.TripID,PP2.TailNUM, LegNUM = (
SELECT PP1.LegNUM + ','
       FROM (SELECT DISTINCT TripID, LegNUM FROM @tblInfo) PP1
       WHERE PP1.TripID = PP2.TripID
     FOR XML PATH(''))
      FROM @tblInfo PP2
END
    
    
  --  exec  spGetReportPRETSCriteria2 'TIM', '20,100', '1', '', '2012-07-20', '2012-07-25', 'ZZKWGS', 'test', '', '', '', '', ''
  --  exec  spGetReportPRETSCriteria2 'TIM', '20', '', '', '2012-07-20', '2012-07-25', '', '', '', '', '', '', '',1,1,1,1,1,1
  --  exec  spGetReportPRETSCriteria2 'TIM', '4120', '', '', '2012-07-20', '2012-07-25', '', '', '', '', '', '', ''
  --  exec  spGetReportPRETSCriteria2 'supervisor_99', '1838', '1' 
 
 







GO


