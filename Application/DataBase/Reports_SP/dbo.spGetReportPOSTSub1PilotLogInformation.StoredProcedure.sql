IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSub1PilotLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSub1PilotLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPOSTSub1PilotLogInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@CrewID AS VARCHAR(30) -- [Optional], Comma delimited string with mutiple values
	)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTSub1PilotLogInformation
-- Author:  A.AKHILA
-- Create date: 
-- Description: Get Postflight SUB1 Pilot Log for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
CREATE TABLE #PILOT1(crewid VARCHAR(100),CREWCD VARCHAR(10), DateRange varchar(50),DateFROM DATE,DATETO DATE,ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,ChecklistDate DATE)

CREATE TABLE #PILOT11(crewid VARCHAR(100),CREWCD VARCHAR(10),DateRange varchar(50),DateFROM DATE,DATETO DATE,ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,ChecklistDate DATE)


--IF
--(SELECT  COUNT(*) 
--			  		FROM PostflightLeg PL 
--			INNER JOIN PostflightMain POM ON POM.POLogID = PL.POLogID
--			INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
--			INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.IsDeleted=0
--			INNER JOIN Crew C ON C.CrewID = PC.CrewID AND C.IsDeleted=0 and C.IsStatus=1
--			INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND ccl.IsDeleted=0 and ccl.CustomerID= PC.CustomerID
			
--		WHERE (PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
--		AND POM.LogNum in
--			(SELECT distinct LogNum  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID  
--			WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
--			AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
--		AND (PC.CrewID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')) < 1
--BEGIN
--INSERT INTO #PILOT1
--SELECT	NULL [CREWID],
--		NULL [DateRange],
--		null [DateFROM],
--		NULL [DATETO],
--		NULL [ChecklistItem],
--		NULL [ChecklistLast],
--		NULL [ChecklistDue],
--		NULL [ChecklistAlert],
--		NULL [ChecklistGrace],
--		NULL [ChecklistDate]  
--END
--ELSE

--BEGIN
INSERT INTO #PILOT1
	SELECT DISTINCT
		  [CREWID]=C.CREWID
		 , [CREWCD]=C.CREWCD
		 ,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	     , [DateFROM]=CONVERT(DATE,@DATEFROM)
		,[DATETO]= CONVERT(DATE, @DATETO)  
		 ,[ChecklistItem] = CCL.CrewChecklistDescription
		 ,[ChecklistLast] = CCD.PreviousCheckDT
		 ,[ChecklistDue] = CCD.DueDT
		 ,[ChecklistAlert] = CCD.AlertDT
		 ,[ChecklistGrace] = CCD.GraceDT
		 ,[ChecklistDate] = CASE WHEN CCD.IsOneTimeEvent = 'True' AND CCD.IsCompleted ='True' THEN 'COMPLETED'
						WHEN @DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT THEN 'GRACE'
						WHEN @DATETO > CCD.DueDT THEN 'PAST DUE'
						WHEN @DATETO BETWEEN CCD.AlertDT AND CCD.DueDT THEN 'ALERT'
						ELSE ''
				   END 
			  		FROM PostflightLeg PL 
			INNER JOIN PostflightMain POM ON POM.POLogID = PL.POLogID AND POM.IsDeleted=0
			INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
			INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.IsDeleted=0
			INNER JOIN Crew C ON C.CrewID = PC.CrewID AND C.IsDeleted=0 and C.IsStatus=1
			INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND ccl.IsDeleted=0 and ccl.CustomerID= PC.CustomerID
			
		WHERE (PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
		AND PL.IsDeleted=0
		AND POM.LogNum in
			(SELECT distinct LogNum  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
			WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
			AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted=0)
		AND (PC.CrewID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')
			
--END

INSERT INTO #PILOT11
	     SELECT DISTINCT
			[CREWID]=C.CREWID,
			[CREWCD]=C.CREWCD,
			[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			[DateFROM]=CONVERT(DATE,@DATEFROM),
			[DATETO]= CONVERT(DATE, @DATETO),  
			[ChecklistItem]=NULL,
			[ChecklistLast]=NULL,
			[ChecklistDue]=NULL,
			[ChecklistAlert]=NULL,
			[ChecklistGrace]=NULL,
			[ChecklistDate] = NULL
			  	FROM CREW C
			  	
			  	WHERE C.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and c.IsDeleted = 0 AND c.IsStatus = 1  
			  	AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
            delete from #PILOT11 where CREWID in(select distinct CREWID from #PILOT1)
           
SELECT * FROM #PILOT11

union all           
select * from #PILOT1
ORDER BY CREWCD
--EXEC spGetReportPOSTSub1PilotLogInformation 'JWILLIAMS_13','2000/09/01','2012/09/07',''
	 
	   
GO


