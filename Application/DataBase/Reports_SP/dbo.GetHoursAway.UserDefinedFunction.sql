IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetHoursAway]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetHoursAway]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetHoursAway](@UserCD VARCHAR(30),@POLegID BIGINT, @POLogID BIGINT)
RETURNS Numeric(7,2)
 
AS BEGIN
DECLARE @HrsAway Numeric(7,2)
Declare @PrevOutboundDTTM DateTime
Declare @PrevInboundDTTM DateTime
Declare @OutboundDTTM DateTime
Declare @InboundDTTM DateTime
Declare @LegNUM Int=0
Declare @RecCnt Int=0


SELECT @LegNUM=LegNUM, @OutboundDTTM=OutboundDTTM,@InboundDTTM=InboundDTTM 
FROM   PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  And
POLogID=@POLogID And POLegID =@POLegID  
 
Set @RecCnt=(SELECT Count(*) FROM PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) And POLogID=@POLogID And LegNUM <@LegNUM )

SELECT TOP 1 @PrevOutboundDTTM=OutboundDTTM,@PrevInboundDTTM=InboundDTTM 
FROM   PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) And 
POLogID=@POLogID And LegNUM <@LegNUM  
Order By LegNUM Desc  

IF @RecCnt >0
Begin
  Set @HrsAway = (SELECT CAST(DateDiff(MINUTE ,@PrevInboundDTTM,@InboundDTTM)/60.0 AS DECIMAL(10,2)))
End
Else 
Begin
  Set @HrsAway = (SELECT CAST(DateDiff(MINUTE ,@OutboundDTTM,@InboundDTTM)/60.0 AS DECIMAL(10,2)))
End   
RETURN @HrsAway
END



GO


