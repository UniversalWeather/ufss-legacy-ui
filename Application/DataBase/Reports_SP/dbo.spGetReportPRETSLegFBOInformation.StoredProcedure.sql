IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegFBOInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegFBOInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReportPRETSLegFBOInformation]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
		@LegNum AS VARCHAR(30) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSLegFBOInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

    	   SELECT [LegId] = CONVERT(VARCHAR,PL.LegID)
    		,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[Description] = ISNULL(FBD.FBOVendor,'')
			,[Phone1] = ISNULL(FBD.PhoneNUM1,'')
			,[Phone2] = ISNULL(FBD.PhoneNUM2,'')
			,[Fax] = ISNULL(FBD.FaxNum,'')
			,[Fuel] = ISNULL(FBD.FuelBrand,'')
			,[LastPrice] = ISNULL(FBD.LastFuelPrice,0)
			,[Date] = ISNULL(FBD.LastFuelDT,NULL)
			,[NegPrice] = ISNULL(FBD.NegotiatedFuelPrice,0)
			,[PayType] = ISNULL(FBD.PaymentType,'')
			,[PostedPrice] = ISNULL(FBD.PostedPrice,0)
			,[FuelQty] = ISNULL(FBD.FuelQty,0)
			,[Contact] = ISNULL(FBD.Contact,'')
			,[Freq] = ISNULL(FBD.Frequency,'')
			,[UVAir] = FBD.IsUWAAirPartner
			,[Address1] = ISNULL(FBD.Addr1,'')
			,[Address2] = ISNULL(FBD.Addr2,'')
			,[Address3] = ISNULL(FBD.Addr3,'')			
			,[City] = ISNULL(FBD.CityName,'')
			,[State] = ISNULL(FBD.StateName,'')
			,[Zip] = ISNULL(FBD.PostalZipCD,'')
			,[Confirm] = ISNULL(FBD.ConfirmationStatus,'')
			,[Comments] = ISNULL(FBD.Comments,'')
			,[Remarks] = ISNULL(FBD.Remarks,'')
			,[Email] = ISNULL(FBD.ContactEmail,'')
			--New Fields Added for enhancement
			,DepARINC = ISNULL(FBD.ARINC,'')
			,DepUNICOM = ISNULL(FBD.UNICOM,'')
			--New Fields Added for enhancement
			,[IsFBODepEMPTY] = CASE WHEN ISNULL(FBD.FBOVendor,'') = '' AND
									ISNULL(FBD.PhoneNUM1,'') = '' AND
									ISNULL(FBD.PhoneNUM2,'') = '' AND
									ISNULL(FBD.FaxNum,'') = '' AND
									ISNULL(FBD.FuelBrand,'') = '' AND
									ISNULL(FBD.LastFuelPrice,0) =  0 AND
									ISNULL(FBD.LastFuelDT,NULL) IS NULL AND
									ISNULL(FBD.NegotiatedFuelPrice,0) = 0 AND
									ISNULL(FBD.PaymentType,'') = '' AND
									ISNULL(FBD.PostedPrice,0) = 0 AND 
									ISNULL(FBD.FuelQty,0) = 0 AND
									ISNULL(FBD.Contact,'') = '' AND
									ISNULL(FBD.Frequency,'') = '' AND
									--FBD.IsUWAAirPartner = 0 AND -- NOT REQUIRED
									ISNULL(FBD.Addr1,'') = '' AND
									ISNULL(FBD.Addr2,'') = '' AND
									ISNULL(FBD.Addr3,'') = '' AND
									ISNULL(FBD.CityName,'') = '' AND
									ISNULL(FBD.StateName,'') = '' AND
									ISNULL(FBD.PostalZipCD,'') = '' AND
									ISNULL(FBD.ConfirmationStatus,'') = '' AND
									ISNULL(FBD.Comments,'') = '' AND
									ISNULL(FBD.Remarks,'') = '' AND
									ISNULL(FBD.ContactEmail,'') = '' AND 
									ISNULL(FBD.ARINC,'') = '' AND
									ISNULL(FBD.UNICOM, '') = ''
								THEN 1 ELSE 0 END
			----To FBO	Heading----
			,[TFDescription] = ISNULL(FBA.FBOVendor,'')
			,[TFPhone1] = ISNULL(FBA.PhoneNUM1,'')
			,[TFPhone2] = ISNULL(FBA.PhoneNUM2,'')
			,[TFFax] = ISNULL(FBA.FaxNum,'')
			,[TFFuel] = ISNULL(FBA.FuelBrand,'')
			,[TFLastPrice] = ISNULL(FBA.LastFuelPrice,0)
			,[TFDate] = ISNULL(FBA.LastFuelDT,NULL)
			,[TFNegPrice] = ISNULL(FBA.NegotiatedFuelPrice,0)
			,[TFPayType] = ISNULL(FBA.PaymentType,'')
			,[TFPostedPrice] = ISNULL(FBA.PostedPrice,0)
			,[TFFuelQty] = ISNULL(FBA.FuelQty,0)
			,[TFContact] = ISNULL(FBA.Contact,'')
			,[TFFreq] = ISNULL(FBA.Frequency,'')
			,[TFUVAir] = FBA.IsUWAAirPartner
			,[TFAddress1] = ISNULL(FBA.Addr1,'')
			,[TFAddress2] = ISNULL(FBA.Addr2,'')
			,[TFAddress3] = ISNULL(FBA.Addr3,'')
			,[TFCity] = ISNULL(FBA.CityName,'')
			,[TFState] = ISNULL(FBA.StateName,'')
			,[TFZip] = ISNULL(FBA.PostalZipCD,'')
			,[TFConfirm] = ISNULL(FBA.ConfirmationStatus,'')
			,[TFComments] = ISNULL(FBA.Comments,'')
			,[TFRemarks] = ISNULL(FBA.Remarks,'')
			,[TFEmail] = ISNULL(FBA.ContactEmail,'')
			--New Fields Added for enhancement
			,ArrARINC = ISNULL(FBA.ARINC,'')
			,ArrUNICOM = ISNULL(FBA.UNICOM,'')
			--New Fields Added for enhancement
			,[IsFBOArrEMPTY] = CASE WHEN ISNULL(FBA.FBOVendor,'') = '' AND
									ISNULL(FBA.PhoneNUM1,'') = '' AND
									ISNULL(FBA.PhoneNUM2,'') = '' AND
									ISNULL(FBA.FaxNum,'') = '' AND
									ISNULL(FBA.FuelBrand,'') = '' AND
									ISNULL(FBA.LastFuelPrice,0) = 0 AND
									ISNULL(FBA.LastFuelDT,NULL) IS NULL AND
									ISNULL(FBA.NegotiatedFuelPrice,0) = 0 AND
									ISNULL(FBA.PaymentType,'') = '' AND
									ISNULL(FBA.PostedPrice,0) = 0 AND 
									ISNULL(FBA.FuelQty,0) = 0 AND
									ISNULL(FBA.Contact,'') = '' AND
									ISNULL(FBA.Frequency,'') = '' AND
									--FBA.IsUWAAirPartner = 0 AND -- NOT REQUIRED
									ISNULL(FBA.Addr1,'') = '' AND
									ISNULL(FBA.Addr2,'') = '' AND
									ISNULL(FBA.Addr3,'') = '' AND
									ISNULL(FBA.CityName,'') = '' AND
									ISNULL(FBA.StateName,'') = '' AND
									ISNULL(FBA.PostalZipCD,'') = '' AND
									ISNULL(FBA.ConfirmationStatus,'') = '' AND
									ISNULL(FBA.Comments,'') = '' AND
									ISNULL(FBA.Remarks,'') = '' AND
									ISNULL(FBA.ContactEmail,'') = '' AND
									ISNULL(FBA.ARINC,'') = '' AND 
									ISNULL(FBA.UNICOM,'') = ''
								THEN 1 ELSE 0 END			
			----Depart Pax Trans	Heading----
			,[DPDescription] =ISNULL(TPD.TransportationVendor,'')
			,[DPPhone] = ISNULL(TPD.PhoneNum,'')
			,[DPFax] = ISNULL(TPD.FaxNum,'')
			--,[DPRate] =	isnull(TPD.NegotiatedRate,'')
			,[DPRate] =	CASE WHEN ISNULL(TPD.NegotiatedRate,'') = '0.00' THEN '' ELSE ISNULL(TPD.NegotiatedRate,'') END
			,[DPRemarks] = ISNULL(TPD.Remarks,'')
			,[DPConfirm] = ISNULL(TPD.ConfirmationStatus,'')
			,[DPComments] =	ISNULL(TPD.Comments,'')
			,[IsDepPaxTransEmpty] = CASE WHEN ISNULL(TPD.TransportationVendor,'') = '' AND
									ISNULL(TPD.PhoneNum,'') = '' AND
									ISNULL(TPD.FaxNum,'') = '' AND
									( ISNULL(TPD.NegotiatedRate,'') = '' OR ISNULL(TPD.NegotiatedRate,'') = '0.00' ) AND
									ISNULL(TPD.Remarks,'') = '' AND
									ISNULL(TPD.ConfirmationStatus,'') = '' AND
									ISNULL(TPD.Comments,'') = ''
								THEN 1 ELSE 0 END
			----Arr Pax Trans	Heading----
			,[APDescription] = TPA.TransportationVendor
			,[APPhone] = TPA.PhoneNum
			,[APFax] = TPA.FaxNum
			,[APRate] =	CASE WHEN ISNULL(TPA.NegotiatedRate,'') = '0.00' THEN '' ELSE ISNULL(TPA.NegotiatedRate,'') END
			,[APRemarks] = ISNULL(TPA.Remarks,'')
			,[APConfirm] = TPA.ConfirmationStatus
			,[APComments] = TPA.Comments
			,[IsArrPaxTransEmpty] = CASE WHEN ISNULL(TPA.TransportationVendor,'') = '' AND
									ISNULL(TPA.PhoneNum,'') = '' AND
									ISNULL(TPA.FaxNum,'') = '' AND
									( ISNULL(TPA.NegotiatedRate,'') = '' OR ISNULL(TPA.NegotiatedRate,'') = '0.00' ) AND
									ISNULL(TPA.Remarks,'') = '' AND
									ISNULL(TPA.ConfirmationStatus,'') = '' AND
									ISNULL(TPA.Comments,'') = ''
								THEN 1 ELSE 0 END			
			------Dep Crew Trans	Heading---
			,[DCDescription] = ISNULL(TCD.TransportationVendor,'')
			,[DCPhone] =ISNULL(TCD.PhoneNum,'')
			,[DCFax] = ISNULL(TCD.FaxNum,'')
			,[DCRate] = CASE WHEN ISNULL(TCD.NegotiatedRate,'') = '0.00' THEN '' ELSE ISNULL(TCD.NegotiatedRate,'') END
			,[DCConfirm] = ISNULL(TCD.ConfirmationStatus,'')
			,[DCComments] = ISNULL(TCD.Comments,'')
			,[DCRemarks] = isnull(TCD.Remarks,'')
			,[IsDepCrewTransEmpty] = CASE WHEN ISNULL(TCD.TransportationVendor,'') = '' AND
									ISNULL(TCD.PhoneNum,'') = '' AND
									ISNULL(TCD.FaxNum,'') = '' AND
									( ISNULL(TCD.NegotiatedRate,'') = '' OR ISNULL(TCD.NegotiatedRate,'') = '0.00' ) AND
									ISNULL(TCD.Remarks,'') = '' AND
									ISNULL(TCD.ConfirmationStatus,'') = '' AND
									ISNULL(TCD.Comments,'') = ''
								THEN 1 ELSE 0 END			
			----Arr Crew Trans	Heading---
			,[ACDescription] = ISNULL(TCA.TransportationVendor,'')
			,[ACPhone] = ISNULL(TCA.PhoneNum,'')
			,[ACFax] = ISNULL(TCA.FaxNum,'')
			,[ACRate] =CASE WHEN ISNULL(TCA.NegotiatedRate,'') = '0.00' THEN '' ELSE ISNULL(TCA.NegotiatedRate,'') END
			,[ACConfirm] = ISNULL(TCA.ConfirmationStatus,'')
			,[ACComments] = ISNULL(TCA.Comments,'')
			,[ACRemarks] = isnull(TCA.Remarks,'')
			,[IsArrCrewTransEmpty] = CASE WHEN ISNULL(TCA.TransportationVendor,'') = '' AND
									ISNULL(TCA.PhoneNum,'') = '' AND
									ISNULL(TCA.FaxNum,'') = '' AND
									( ISNULL(TCA.NegotiatedRate,'') = '' OR ISNULL(TCA.NegotiatedRate,'') = '0.00' ) AND
									ISNULL(TCA.Remarks,'') = '' AND
									ISNULL(TCA.ConfirmationStatus,'') = '' AND
									ISNULL(TCA.Comments,'') = ''
								THEN 1 ELSE 0 END				
			-----Dep. Catering	Heading-----
			,[DCADescription] = ISNULL(CD.CateringVendor,'')
			,[DCAPhone] = ISNULL(CD.PhoneNum,'')
			,[DCAFax] = ISNULL(CD.FaxNum,'')
			,[DCARate] = ISNULL(CD.NegotiatedRate,0)
			,[DCARemarks] = ISNULL(CD.Remarks,'')
			,[DCAConfirm] = ISNULL(CD.CateringConfirmation,'')
			,[DCAComments] = ISNULL(CD.CateringComments,'')
			,[IsDepCatEmpty] = CASE WHEN ISNULL(CD.CateringVendor,'') = '' AND
									ISNULL(CD.PhoneNum,'') = '' AND
									ISNULL(CD.FaxNum,'') = '' AND
									ISNULL(CD.NegotiatedRate,0) = 0 AND
									ISNULL(CD.Remarks,'') = '' AND
									ISNULL(CD.CateringConfirmation,'') = '' AND
									ISNULL(CD.CateringComments,'') = ''
								THEN 1 ELSE 0 END
			-- Itinerary will consider NegotiatedRate field
			,[IsItnryDepCatEmpty] = CASE WHEN ISNULL(CD.CateringVendor,'') = '' AND
									ISNULL(CD.PhoneNum,'') = '' AND
									ISNULL(CD.FaxNum,'') = '' AND
									--ISNULL(CD.NegotiatedRate,0) = 0 AND
									ISNULL(CD.Remarks,'') = '' AND
									ISNULL(CD.CateringConfirmation,'') = '' AND
									ISNULL(CD.CateringComments,'') = ''
								THEN 1 ELSE 0 END												
			---Arr. Catering	Heading------------
			,[ACADescription] = ISNULL(CA.CateringVendor,'')
			,[ACAPhone] = ISNULL(CA.PhoneNum,'')
			,[ACAFax] = ISNULL(CA.FaxNum,'')
			,[ACARate] = ISNULL(CA.NegotiatedRate,0)
			,[ACARemarks] =ISNULL(CA.Remarks,'')
			,[ACAConfirm] = ISNULL(CA.CateringConfirmation,'')
			,[ACAComments] = ISNULL(CA.CateringComments,'')
			,[IsArrCatEmpty] = CASE WHEN ISNULL(CA.CateringVendor,'') = '' AND
									ISNULL(CA.PhoneNum,'') = '' AND
									ISNULL(CA.FaxNum,'') = '' AND
									ISNULL(CA.NegotiatedRate,0) = 0 AND
									ISNULL(CA.Remarks,'') = '' AND
									ISNULL(CA.CateringConfirmation,'') = '' AND
									ISNULL(CA.CateringComments,'') = ''
								THEN 1 ELSE 0 END
			-- Itinerary will consider NegotiatedRate field								
			,[IsItnryArrCatEmpty] = CASE WHEN ISNULL(CA.CateringVendor,'') = '' AND
									ISNULL(CA.PhoneNum,'') = '' AND
									ISNULL(CA.FaxNum,'') = '' AND
									--ISNULL(CA.NegotiatedRate,0) = 0 AND
									ISNULL(CA.Remarks,'') = '' AND
									ISNULL(CA.CateringConfirmation,'') = '' AND
									ISNULL(CA.CateringComments,'') = ''
								THEN 1 ELSE 0 END								
		/*				
			----Pax Hotel	Heading---
			,[PHDescription] = ISNULL(PH.Name,'')
			,[PHPhone] = ISNULL(PH.PhoneNum,'')
			,[PHFax] = ISNULL(PH.FaxNum,'')
			,[PHAddress1] = ISNULL(PH.Address1,'')
			,[PHAddress2] = ISNULL(PH.Address2,'')
			,[PHCity] = ISNULL(PH.CityName,'')
			,[PHState] = ISNULL(PH.StateName,'')
			,[PHZip] = ISNULL(PH.PostalZipCD,'')
			,[PHRemarks] = ISNULL(PH.Remarks,'')
			,[PHRate] = ISNULL(PH.NegociatedRate,0)
			,[PHRooms] = ISNULL(PH.NoofRooms,0)
			,[PHConfirm] = ISNULL(PH.ConfirmationStatus,'')
			,[PHComments] = ISNULL(PH.Comments,'')
			,[IsPaxHotelEmpty] = CASE WHEN ISNULL(PH.Name,'') = '' AND
									ISNULL(PH.PhoneNum,'') = '' AND
									ISNULL(PH.FaxNum,'') = '' AND
									ISNULL(PH.Address1,'') = '' AND
									ISNULL(PH.Address2,'') = '' AND
									ISNULL(PH.CityName,'') = '' AND
									ISNULL(PH.StateName,'') = '' AND
									ISNULL(PH.PostalZipCD,'') = '' AND
									ISNULL(PH.Remarks,'') = '' AND
									ISNULL(PH.NegociatedRate,0) = 0 AND																																																															
									ISNULL(PH.NoofRooms,0) = 0 AND
									ISNULL(PH.ConfirmationStatus,'') = '' AND
									ISNULL(PH.Comments,'') = ''
								THEN 1 ELSE 0 END				
			----Crew Hotel	Heading---
			,[CHDescription] = ISNULL(CH.Name,'')
			,[CHPhone] = ISNULL(CH.PhoneNum,'')
			,[CHFax] = ISNULL(CH.FaxNum,'')
			,[CHAddress1] = ISNULL(CH.Address1,'')
			,[CHAddress2] = ISNULL(CH.Address2,'')
			,[CHCity] = ISNULL(CH.CityName,'')
			,[CHState] = ISNULL(CH.StateName,'')
			,[CHZip] = ISNULL(CH.PostalZipCD,'')
			,[CHRate] = ISNULL(CH.NegociatedRate,0)
			,[CHRooms] = ISNULL(CH.NoofRooms,0)
			,[CHRemarks] = ISNULL(CH.Remarks,'')
			,[CHConfirm] = ISNULL(CH.ConfirmationStatus,'')
			,[CHComments] = ISNULL(CH.Comments,'')
			,[IsCrewHotelEmpty] = CASE WHEN ISNULL(CH.Name,'') = '' AND
									ISNULL(CH.PhoneNum,'') = '' AND
									ISNULL(CH.FaxNum,'') = '' AND
									ISNULL(CH.Address1,'') = '' AND
									ISNULL(CH.Address2,'') = '' AND
									ISNULL(CH.CityName,'') = '' AND
									ISNULL(CH.StateName,'') = '' AND
									ISNULL(CH.PostalZipCD,'') = '' AND
									ISNULL(CH.Remarks,'') = '' AND
									ISNULL(CH.NegociatedRate,0) = 0 AND																																																															
									ISNULL(CH.NoofRooms,0) = 0 AND
									ISNULL(CH.ConfirmationStatus,'') = '' AND
									ISNULL(CH.Comments,'') = ''
								THEN 1 ELSE 0 END			
			----Maint Hotel	Heading---
			,[MHDescription] = ISNULL(ACH.Name,'')
			,[MHPhone] = ISNULL(ACH.PhoneNum,'')
			,[MHFax] = ISNULL(ACH.FaxNum,'')
			,[MHAddress1] = ISNULL(ACH.Address1,'')
			,[MHAddress2] = ISNULL(ACH.Address2,'')
			,[MHCity] = ISNULL(ACH.CityName,'')
			,[MHState] = ISNULL(ACH.StateName,'')
			,[MHZip] = ISNULL(ACH.PostalZipCD,'')
			,[MHRate] = ISNULL(ACH.NegociatedRate,0)
			,[MHRemarks] = ISNULL(ACH.Remarks,'')
			,[MHRooms] = ISNULL(ACH.NoofRooms,0)
			,[MHConfirm] = ISNULL(ACH.ConfirmationStatus,'')
			,[MHComments] = ISNULL(ACH.Comments,'')
			,[IsAddlCrewHotelEmpty] = CASE WHEN ISNULL(ACH.Name,'') = '' AND
									ISNULL(ACH.PhoneNum,'') = '' AND
									ISNULL(ACH.FaxNum,'') = '' AND
									ISNULL(ACH.Address1,'') = '' AND
									ISNULL(ACH.Address2,'') = '' AND
									ISNULL(ACH.CityName,'') = '' AND
									ISNULL(ACH.StateName,'') = '' AND
									ISNULL(ACH.PostalZipCD,'') = '' AND
									ISNULL(ACH.Remarks,'') = '' AND
									ISNULL(ACH.NegociatedRate,0) = 0 AND																																																															
									ISNULL(ACH.NoofRooms,0) = 0 AND
									ISNULL(ACH.ConfirmationStatus,'') = '' AND
									ISNULL(ACH.Comments,'') = ''
								THEN 1 ELSE 0 END	
		*/		
			
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			
			LEFT OUTER JOIN (
				SELECT  F.FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,
				        F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,
				        F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,
				        PFL.LegID, F.Addr3, F.ContactEmail, F.ARINC, F.UNICOM
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID

			LEFT OUTER JOIN (
				SELECT  F.FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,
				        F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,
				        F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,
				        PFL.LegID, F.Addr3, F.ContactEmail, F.ARINC, F.UNICOM
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
			
			LEFT OUTER JOIN (
				SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
					[NegotiatedRate]=PTL.PhoneNum4,T.Remarks,
			         PTL.LegID,PTL.ConfirmationStatus,PTL.Comments
				FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
				WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
			) TPD ON PL.LegID = TPD.LegID
			
			LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,[NegotiatedRate]=PTL.PhoneNum4,T.Remarks,
			        PTL.LegID,PTL.ConfirmationStatus,PTL.Comments
				FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
				WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
			) TPA ON PL.LegID = TPA.LegID
			
			LEFT OUTER JOIN (
				SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,[NegotiatedRate]=PTL.PhoneNum4,T.Remarks,
			        PTL.LegID,PTL.ConfirmationStatus,PTL.Comments
				FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
				WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
			) TCD ON PL.LegID = TCD.LegID
			
			LEFT OUTER JOIN (
				SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,[NegotiatedRate]=PTL.PhoneNum4,T.Remarks,
			       PTL.LegID,PTL.ConfirmationStatus,PTL.Comments
				FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
				WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
			) TCA ON PL.LegID = TCA.LegID
			
			LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,
					PCL.CateringComments,PCL.CateringConfirmation, C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				LEFT OUTER JOIN Catering C ON PCL.CateringID = C.CateringID 
				WHERE PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,
					PCL.CateringComments,PCL.CateringConfirmation, C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				LEFT JOIN Catering C ON PCL.CateringID = C.CateringID 
				WHERE PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			
/*			
			/--Pax Hotel
			LEFT OUTER JOIN ( SELECT 
			     [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments,
			      PPH.Address1,PPH.Address2,PPH.CityName,PPH.StateName,PPH.PostalZipCD,[Remarks]=HP.Remarks,PPL.LegID
			FROM PreflightPassengerList PPL 
			INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			) PH ON PL.legID = PH.LegID
			
			--Crew Hotel
			
			LEFT OUTER JOIN (			
				SELECT   [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,
			      PCH.Address1,PCH.Address2,PCH.CityName,PCH.StateName,PCH.PostalZipCD,[Remarks]=HC.Remarks,PCLL.LegID
				FROM PreflightCrewList PCLL 
				INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
				WHERE PCLL.DutyType IN ('P', 'S')
			) CH ON PL.legID = CH.LegID

			--Addnl Crew / Maint Crew - Hotel
			LEFT OUTER JOIN (			
				SELECT   [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,
			      PCH.Address1,PCH.Address2,PCH.CityName,PCH.StateName,PCH.PostalZipCD,[Remarks]=HC.Remarks,PCLL.LegID
				FROM PreflightCrewList PCLL 
				INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
				WHERE PCLL.DutyType NOT IN ('P', 'S')
			) ACH ON PL.legID = ACH.LegID
	*/		
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	
	END
--EXEC spGetReportPRETSLegFBOInformation 'SUPERVISOR_99', '100995345','1'
GO


