IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREPassengerItineraryMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREPassengerItineraryMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE procedure [dbo].[spGetReportPREPassengerItineraryMhtmlInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@Pax AS VARCHAR(200) = '' --Mandatory
		
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPREPassengerItineraryMhtmlInformation
-- Author: SINDHUJA.K
-- Create date: 07 August 2012
-- Description: Get Preflight Passenger Itinerary Mhtml information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================


SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
	
			SELECT
				[flag] = '',--See comments, [to be confirmed by BA]
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = PAX.PaxNames,
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = CONVERT(VARCHAR,@DATEFROM, 101),
				[highdate] = CONVERT(VARCHAR,@DATETO, 101), 
				[tripdate] = DBO.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),
				[locdep] = convert(varchar, PL.DepartureDTTMLocal, 101) +' ' +
							LEFT( convert (varchar, PL.DepartureDTTMLocal, 114),5),
				[locarr] = convert(varchar, PL.ArrivalDTTMLocal, 101) +' '+
							LEFT( convert (varchar, PL.ArrivalDTTMLocal, 114),5),
				[gmtdep] = convert(varchar, PL.DepartureGreenwichDTTM, 101) +' '+
							LEFT( convert (varchar, PL.DepartureGreenwichDTTM, 114),5),
				[tcenroute] = '',--See comments,
				[tcfromhb] = '',--See comments,
				[depicao_os] = '',--(CASE(IsDayLightSaving) WHEN 'TRUE' THEN(CASE WHEN LocalDepartureDate BETWEEN  DST.StartDT and DST.EndDT THEN  depicao_os = DST.OffSet ELSE depicao_os = DA.OffsetToGMT End) ELSE '' End)
				[arricao_os] = '',--Coming from Airport.OffsetToGMT (See Preflight-Calculation Worksheet),
				[homicao_os] = '',--Coming from Airport.OffsetToGMT (See Preflight-Calculation Worksheet),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = P.FirstName +' '+ P.LastName,
				[capt_mobl] = P.CellPhoneNum,
				[capt_pagr] = P.PagerNum,
				[oth_crew] = 'pending',
				[SecondOfficer] = S.FirstName +' '+ S.LastName,
				[SecondOfficer_mobl] = S.CellPhoneNum,
				[SecondOfficer_Pager] = S.PagerNum,
				[Engineer] = E.FirstName +' '+ E.LastName,
				[Engineer_mobl] = E.CellPhoneNum,
				[Engineer_Pager] = E.PagerNum,
				[Attendent] = AT.FirstName +' '+ AT.LastName,
				[Attendent_mobl] = AT.CellPhoneNum,
				[Attendent_Pager] = AT.PagerNum,
				[OtherCrew] = O.FirstName +' '+ O.LastName,
				[OtherCrew_mobl] = O.CellPhoneNum,
				[OtherCrew_Pager] = O.PagerNum,
				[depicao_id] = PL.DepartICAOID,
				[arricao_id] = PL.ArriveICAOID,
				[depcity] = DA.CityName,
				[arrcity] = AA.CityName,
				[from_city] = DA.CityName +','+ DA.StateName,
				[from_airport] = DA.AirportName,
				[to_city] = AA.CityName +','+ AA.StateName,
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.IsDepartureFBO,
				[arr_terminal] = PF.IsArrivalFBO,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PF.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] = '',--PreflightLogistics.Description,--(PREFLIGHT CATERINGLIST)
				[catera_phone] = '',--CA.PhoneNum1,
				[catera_fax] = '',--CA.FaxNUM,
				[catera_rate] = '',--PreflightLogistics.Rate,
				[caterd_name] = '',--PreflightLogistics.Description,
				[caterd_phone] = '',--PreflightLogistics.PhoneNUM,
				[caterd_fax] = '',--PreflightLogistics.FaxNUM,
				[caterd_rate] = '',--PreflightLogistics.Rate
				[CATERD_REMARKS]='',
				[CATERD_CONFIRM]='',
				[CATERD_COMMENTS]='',
				[CATERA_REMARKS]='',
				[CATERA_CONFIRM]='',
				[CATERA_COMMENTS]='',
				[LEG_NOTES]='',
				[PAX2]='',
				[PAX1]='',
				[flightcrew]=''
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN 
				          (select CityName, StateName, AirportName, AirportID, DSTRegionID  from Airport ) DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN 
				          (select CityName, StateName, AirportName, AirportID  from Airport ) AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN 
				           (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				
				INNER JOIN PreflightFBOList PF ON PL.LegID = PF.LegID
				INNER JOIN FBO FB ON PF.FBOID = FB.FBOID
	            
				INNER JOIN (
					SELECT DISTINCT LegID, PaxNames ,PassengerID
						FROM PreflightPassengerList p1
						CROSS APPLY ( 
							SELECT PassengerFirstName + ',' + PassengerLastName + '' + PassengerMiddleName + ';' 
							FROM PreflightPassengerList p2
							WHERE p2.LegID = p1.LegID 
							ORDER BY PassengerFirstName 
							FOR XML PATH('') 
						) D ( PaxNames )
				) PAX ON PL.LegID = PAX.LegID
				INNER JOIN passenger PA on pax .PassengerID = PA.PassengerRequestorID

           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND PM.EstDepartureDT BETWEEN @DATEFROM AND @DATETO
		    AND PM.IsDeleted = 0
		    AND (
		    PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))
		    OR @Pax = ''
		    )
	          
 --  PRINT @SQLSCRIPT
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME, @DATETO AS DATETIME,@Pax AS VARCHAR(20) '
 --   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	, @DATEFROM, @DATETO,@Pax 
 END
    
   --EXEC spGetReportPREPassengerItineraryMhtmlInformation 'UC','2012/7/20','2012/7/22','HKIOE,JEOF'

GO


