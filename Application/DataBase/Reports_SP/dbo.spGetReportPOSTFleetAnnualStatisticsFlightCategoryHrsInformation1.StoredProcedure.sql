IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetAnnualStatisticsFlightCategoryHrsInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightCategoryHrsInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




  
CREATE PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightCategoryHrsInformation1]                      
( 
@UserCD VARCHAR(30), --Mandatory 
@DATEFROM AS DATETIME='', --Mandatory
@DATETO AS DATETIME='', --Mandatory                              
@Year INT, -- Mandatory                    
@TailNum VARCHAR(5000)='',                    
@FleetGroupCD VARCHAR(5000)='',                    
@DepartmentCD VARCHAR(5000)='',                    
@DepartmentGroupID VARCHAR(5000)='',                    
@HomeBase VARCHAR(5000)='',                    
@FlightCatagoryCD VARCHAR(5000)='',                    
@HomeBaseOnly Bit=0,
@Hrs Char(1)=''             
)                      
AS         
SET NOCOUNT ON                  
-- ===============================================================================
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
     
   BEGIN                                
   Declare @TenToMin Int
   Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))       
                           
   DECLARE @ParameterDefinition AS NVARCHAR(400) 
   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '' 
   
   Declare @TotalTripDays Int=0
   Declare @TotalLegs Int=0
   Declare @TotalPassengers Int=0
   Declare @TotalMilesFlown Int=0
   Declare @TotalLoadFactor Numeric(10,2)=0
   Declare @TotalFlightHours Numeric(10,2)=0
   
   IF OBJECT_ID('tempdb..#TmpTble') is not null                        
      DROP table #TmpTble 
                
          Create Table #TmpTble(ScheduledTM DateTime,FlightCatagoryCD Varchar(100),TotalFlightHours Numeric(10,2),)                             
                         
          SET @SQLSCRIPT ='INSERT INTO #TmpTble SELECT Distinct
              PostflightLeg.ScheduledTM,
              FlightCatagory.FlightCatagoryDescription,     
              IsNull(Sum(ROUND(PostflightLeg.FlightHours,1)),0) As TotalFlightHours 
              FROM   (SELECT * FROM PostflightLeg WHERE ISDELETED=0) PostflightLeg INNER JOIN                    
              PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID  AND PostflightMain.IsDeleted=0    
              INNER JOIN                    
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID,F.MaximumPassenger            
                FROM   Fleet F             
                LEFT OUTER JOIN FleetGroupOrder FGO            
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID            
                LEFT OUTER JOIN FleetGroup FG             
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
                WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''             
              ) F ON F.FleetID = PostflightMain.FleetID      
              LEFT JOIN       
              ( SELECT Distinct D.DepartmentID,D.DepartmentCD     
                FROM   Department D             
                LEFT OUTER JOIN DepartmentGroupOrder DGO            
                ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID            
                LEFT OUTER JOIN DepartmentGroup DG             
                ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID 
                WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, '','')) OR @DepartmentGroupID = ''''                          
              ) D ON D.DepartmentID = PostflightMain.DepartmentID                
              LEFT JOIN                    
              FlightCatagory ON PostflightLeg.FlightCategoryID = FlightCatagory.FlightCategoryID     
              LEFT JOIN
              Company ON PostflightMain.HomebaseID = Company.HomebaseID 
              LEFT JOIN
              Airport ON Company.HomebaseAirportID =  Airport.AirportID                    
              WHERE FlightCatagory.FlightCatagoryCD Is Not Null And PostflightMain.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                               
             -- + ' AND Year(PostflightLeg.ScheduledTM)=@Year '                           
  
  
IF @Year <> ''
BEGIN
SET @SQLSCRIPT = @SQLSCRIPT + 'AND Year(PostflightLeg.ScheduledTM) = @Year' 
END
ELSE
BEGIN
SET @SQLSCRIPT = @SQLSCRIPT + 'AND CONVERT(DATE,PostflightLeg.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)' 
END                                 
  IF @TailNum <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';                              
  END                               
                              
  IF @DepartmentCD  <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND D.DepartmentCD IN (''' + REPLACE(CASE WHEN RIGHT(@DepartmentCD , 1) = ',' THEN LEFT(@DepartmentCD, LEN(@DepartmentCD) - 1) ELSE @DepartmentCD END, ',', ''', ''') + ''')';                              
  END                               
                                  
  IF @FlightCatagoryCD  <> '' BEGIN                     
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND FlightCatagory.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';               
   
  END                    
  IF @HomeBase  <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND Airport.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBase, 1) = ',' THEN LEFT(@HomeBase, LEN(@HomeBase) - 1) ELSE @HomeBase END, ',', ''', ''') + ''')';                              
  END                
  IF @HomeBaseOnly = 1 BEGIN            
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PostflightMain.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));            
  END               
  SET @SQLSCRIPT = @SQLSCRIPT + ' GROUP BY PostflightLeg.ScheduledTM,PostflightLeg.FlightHours,FlightCatagory.FlightCatagoryDescription '
                   
  SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME, @DATETO AS DATETIME, @Year As Int, @TailNum As VARCHAR(5000), @FleetGroupCD As VARCHAR(5000),                
  @DepartmentCD As VARCHAR(5000),@DepartmentGroupID As VARCHAR(5000),@HomeBase aS VARCHAR(5000),                
  @FlightCatagoryCD As VARCHAR(5000),@HomeBaseOnly Bit'                              
              
  EXECUTE sp_executesql @SQLSCRIPT , @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @Year, @TailNum, @FleetGroupCD,@DepartmentCD,@DepartmentGroupID,@HomeBase,@FlightCatagoryCD,@HomeBaseOnly                     
  
       Declare @TotFound Int=0
       Declare @NetTotFound Int=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=1 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=2 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=3 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=4 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=5 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=6 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=7 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=8 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=9 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=10 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=11 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From #TmpTble Where MONTH(ScheduledTM)=12 And TotalFlightHours>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       
  IF OBJECT_ID('tempdb..#Tmp') is not null                        
      DROP table #Tmp         
           
  Declare @ItemDesc Varchar(50)               
  Declare @Jan1 Numeric(10,2)=0
  Declare @Feb1 Numeric(10,2)=0
  Declare @Mar1 Numeric(10,2)=0
  Declare @Apr1 Numeric(10,2)=0
  Declare @May1 Numeric(10,2)=0
  Declare @Jun1 Numeric(10,2)=0
  Declare @Jul1 Numeric(10,2)=0
  Declare @Aug1 Numeric(10,2)=0
  Declare @Sep1 Numeric(10,2)=0
  Declare @Oct1 Numeric(10,2)=0
  Declare @Nov1 Numeric(10,2)=0
  Declare @Dec1 Numeric(10,2)=0
  Declare @YTD1 Numeric(10,2)=0
  Declare @FlightCatCD Varchar(100)
  Declare @FPct Numeric(10,2)=0
  
  Create Table #Tmp (FlightCatagoryCD Varchar(100))  
  Insert Into #Tmp Select Distinct FlightCatagoryCD From #TmpTble 
  
  IF OBJECT_ID('tempdb..#TmpRep') is not null                        
     DROP table #TmpRep   
  IF OBJECT_ID('tempdb..#TmpPerc') is not null                        
     DROP table #TmpPerc                 
  
  CREATE TABLE #TmpRep (SerNo Int,ItemDesc Varchar(100),Jan1 Numeric(10,2), Feb1 Numeric(10,2), Mar1 Numeric(10,2), Apr1 Numeric(10,2), May1 Numeric(10,2), Jun1 Numeric(10,2),  
                                             Jul1 Numeric(10,2), Aug1 Numeric(10,2), Sep1 Numeric(10,2), Oct1 Numeric(10,2), Nov1 Numeric(10,2), Dec1 Numeric(10,2),YTD1 Numeric(10,2))
  CREATE TABLE #TmpPerc (SerNo Int,ItemDesc Varchar(100),Jan1 Numeric(10,2), Feb1 Numeric(10,2), Mar1 Numeric(10,2), Apr1 Numeric(10,2), May1 Numeric(10,2), Jun1 Numeric(10,2),  
                                             Jul1 Numeric(10,2), Aug1 Numeric(10,2), Sep1 Numeric(10,2), Oct1 Numeric(10,2), Nov1 Numeric(10,2), Dec1 Numeric(10,2),YTD1 Numeric(10,2))

  Declare @CatNo Int=0
  Declare @PercNo Int=500
  Declare @RowCnt Int=0
  WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
   BEGIN 
    Set @CatNo=@CatNo+1
    Set @PercNo=@PercNo+1 
    
    Select Top 1 @FlightCatCD=FlightCatagoryCD From #Tmp      
     
    Set @Jan1=0
    Set @Feb1=0
    Set @Mar1=0
    Set @Apr1=0
    Set @May1=0
    Set @Jun1=0
    Set @Jul1=0
    Set @Aug1=0
    Set @Sep1=0
    Set @Oct1=0
    Set @Nov1=0
    Set @Dec1=0
    Set @YTD1=0 
    Set @FPct=0
    
    Set @Jan1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=1 And FlightCatagoryCD=@FlightCatCD)
    Set @Feb1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=2 And FlightCatagoryCD=@FlightCatCD)
    Set @Mar1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=3 And FlightCatagoryCD=@FlightCatCD)
    Set @Apr1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=4 And FlightCatagoryCD=@FlightCatCD)
    Set @May1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=5 And FlightCatagoryCD=@FlightCatCD)
    Set @Jun1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=6 And FlightCatagoryCD=@FlightCatCD)
    Set @Jul1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=7 And FlightCatagoryCD=@FlightCatCD)
    Set @Aug1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=8 And FlightCatagoryCD=@FlightCatCD)
    Set @Sep1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=9 And FlightCatagoryCD=@FlightCatCD)
    Set @Oct1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=10 And FlightCatagoryCD=@FlightCatCD)
    Set @Nov1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=11 And FlightCatagoryCD=@FlightCatCD)
    Set @Dec1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduledTM)=12 And FlightCatagoryCD=@FlightCatCD)
    Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
     
    Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
    Values (@CatNo,@FlightCatCD,@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 
          
    Delete From #Tmp Where FlightCatagoryCD=@FlightCatCD
    Set @RowCnt=1
   END 
    Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) 
	 -- SET @SuppressActivityAircraft=1

   IF @RowCnt=1
	   BEGIN
	   IF @Hrs='H'      
		  Begin 
		   Select SerNo,ItemDesc,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin From #TmpRep Order By SerNo        
		  End
	   ELSE 
	     Begin
	        Set @Jan1=0
	        Set @Feb1=0
			Set @Mar1=0
			Set @Apr1=0
			Set @May1=0
			Set @Jun1=0
			Set @Jul1=0
			Set @Aug1=0
			Set @Sep1=0
			Set @Oct1=0
			Set @Nov1=0
			Set @Dec1=0 
			Set @YTD1=0 
			 
	        Set @Jan1=(Select IsNull(Sum(Jan1),0)From #TmpRep)
			Set @Feb1=(Select IsNull(Sum(Feb1),0)From #TmpRep)
			Set @Mar1=(Select IsNull(Sum(Mar1),0)From #TmpRep)
			Set @Apr1=(Select IsNull(Sum(Apr1),0)From #TmpRep)
			Set @May1=(Select IsNull(Sum(May1),0)From #TmpRep)
			Set @Jun1=(Select IsNull(Sum(Jun1),0)From #TmpRep)
			Set @Jul1=(Select IsNull(Sum(Jul1),0)From #TmpRep)
			Set @Aug1=(Select IsNull(Sum(Aug1),0)From #TmpRep)
			Set @Sep1=(Select IsNull(Sum(Sep1),0)From #TmpRep)
			Set @Oct1=(Select IsNull(Sum(Oct1),0)From #TmpRep)
			Set @Nov1=(Select IsNull(Sum(Nov1),0)From #TmpRep)
			Set @Dec1=(Select IsNull(Sum(Dec1),0)From #TmpRep)
 
		   --  Percentage strats
		   IF @Jan1>0
		   Begin
     	     Update #TmpRep Set Jan1=Jan1/@Jan1 * 100
     	   End	
     	   IF @Feb1>0
		   Begin
     	     Update #TmpRep Set Feb1=Feb1/@Feb1 * 100
     	   End	   
     	   IF @Mar1>0
		   Begin
     	     Update #TmpRep Set Mar1=Mar1/@Mar1 * 100
     	   End	   
     	   IF @Apr1>0
		   Begin
     	     Update #TmpRep Set Apr1=Apr1/@Apr1 * 100
     	   End	   
     	   IF @May1>0
		   Begin
     	     Update #TmpRep Set May1=May1/@May1 * 100
     	   End	   
     	   IF @Jun1>0
		   Begin
     	     Update #TmpRep Set Jun1=Jun1/@Jun1 * 100
     	   End	   
     	   IF @Jul1>0
		   Begin
     	     Update #TmpRep Set Jul1=Jul1/@Jul1 * 100
     	   End	   
     	   IF @Aug1>0
		   Begin
     	     Update #TmpRep Set Aug1=Aug1/@Aug1 * 100
     	   End	   
     	   IF @Sep1>0
		   Begin
     	     Update #TmpRep Set Sep1=Sep1/@Sep1 * 100
     	   End	   
     	   IF @Oct1>0
		   Begin
     	     Update #TmpRep Set Oct1=Oct1/@Oct1 * 100
     	   End	   
     	   IF @Nov1>0
		   Begin
     	     Update #TmpRep Set Nov1=Nov1/@Nov1 * 100
     	   End	   
     	   IF @Dec1>0
		   Begin
     	     Update #TmpRep Set Dec1=Dec1/@Dec1 * 100
     	   End	 
     	  
     
		   IF @NetTotFound>0
     	   Begin 
     	     Update #TmpRep Set YTD1=(Select IsNull(Jan1,0)+IsNull(Feb1,0)+IsNull(Mar1,0)+IsNull(Apr1,0)+IsNull(May1,0)+IsNull(Jun1,0)+IsNull(Jul1,0)+IsNull(Aug1,0)+IsNull(Sep1,0)+IsNull(Oct1,0)+IsNull(Nov1,0)+IsNull(Dec1,0))
             Update #TmpRep Set YTD1=YTD1/@NetTotFound     	                               
     	   End    
		  
           IF  @SuppressActivityAircraft=0
           BEGIN
					Select SerNo,ItemDesc,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin From #TmpRep Order By SerNo  		   
		   END
           ELSE
           BEGIN
					Select * FROM #TmpRep
           END
		 End--  Percentage over
		END
		Else
		IF @SuppressActivityAircraft=0
   
   Begin
       
      Select 0 As SerNo, 'NO CATEGORY ACTIVITY' As ItemDesc,0.00 As Jan1,0.00 As Feb1,0.00 As Mar1,0.00 As Apr1,0.00 As May1,0.00 As Jun1,0.00 As Jul1,0.00 As Aug1,0.00 As Sep1,0.00 As Oct1,0.00 As Nov1,0.00 As Dec1,0.00 As YTD1,2 As TenToMin  
    End
    ELSE
    BEGIN
    Select * FROM #TmpRep
    END


   IF OBJECT_ID('tempdb..#Tmp') is not null                        
     DROP table #Tmp 
   IF OBJECT_ID('tempdb..#TmpRep') is not null                        
     DROP table #TmpRep                             
   IF OBJECT_ID('tempdb..#TmpTble') is not null                        
     DROP table #TmpTble
                              
--Exec spGetReportPOSTFleetAnnualStatisticsFlightCategoryHrsInformation1 'SUPERVISOR_99','','','2009','','','','','','',0,'P'
--Exec spGetReportPOSTFleetAnnualStatisticsFlightCategoryHrsInformation1 'SUPERVISOR_99','2009/1/1','2010/1/1','','','','','','','',0,'P'
END


GO


