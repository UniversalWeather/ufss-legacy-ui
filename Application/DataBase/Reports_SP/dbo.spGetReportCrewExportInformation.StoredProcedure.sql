IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportCrewExportInformation]    
  @UserCD VARCHAR(30)    
 ,@CrewCD VARCHAR(5)   
 ,@Crewchecklist BIT=1
 ,@Inactivechecklist BIT = 0     
 ,@CrewTypeRating BIT=1
 ,@CrewCurrency BIT=0
 ,@CrewRosterReport BIT=1 
 
AS    
-- =============================================    
-- SPC Name: spGetReportCrewExportInformation    
-- Author: Askar   
-- Create date: 06 Mar 2013    
-- Description: Get Crew Information for Report, Cre Roaster    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    

SET NOCOUNT ON    

BEGIN 

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;  
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));  
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));       

DECLARE @CrewRoster TABLE(CrewID BIGINT
						 ,CrewCode VARCHAR(MAX)
						 ,Name VARCHAR(MAX)
						 ,Home_Phone_No VARCHAR(MAX)
						 ,Home_Address_1 VARCHAR(MAX)
						 ,Home_Address_2 VARCHAR(MAX)
						 ,Home_Address_3 VARCHAR(MAX)
						 ,Home_City VARCHAR(MAX)
						 ,Home_St_Prov VARCHAR(MAX)
						 ,Home_Postal VARCHAR(MAX)
						 ,Pager VARCHAR(MAX)
						 ,Mobile_Phone VARCHAR(MAX)
						 ,Home_Fax VARCHAR(MAX)
						 ,Birth_Date VARCHAR(MAX)
						 ,Hire_Date VARCHAR(MAX)
						 ,Term_Date VARCHAR(MAX)
						 ,Crew_Type CHAR(4)
						 ,Dept_Code VARCHAR(MAX)
						 ,Client VARCHAR(MAX)
						 ,Country1 VARCHAR(MAX)
						 ,Country2 VARCHAR(MAX)
						 ,Social_Security_No VARCHAR(MAX)
						 ,Home_Base VARCHAR(MAX)
						 ,Crew_Status VARCHAR(MAX)
						 ,Email VARCHAR(MAX)
						 ,Lic_No VARCHAR(MAX)
						 ,Lic_Type1 VARCHAR(MAX)
						 ,Addtional_Lic_No VARCHAR(MAX)
						 ,Lic_Type2 VARCHAR(MAX)
						 ,Citizenship VARCHAR(MAX)
						 ,Notes VARCHAR(MAX)
						 ,Additional_Notes VARCHAR(MAX)
						 ,Type_Code VARCHAR(MAX)
						 ,Type_Description VARCHAR(MAX)
						 ,PIC VARCHAR(MAX)
						 ,SIC VARCHAR(MAX)
						 ,Time_In_Type NUMERIC(9,1)
						 ,As_Of_Date VARCHAR(MAX)
						 ,Information_Description VARCHAR(MAX)
						 ,Information_Value VARCHAR(MAX)
						 ,Crew_Checklist_Description VARCHAR(MAX)
						 ,Aircraft_Type VARCHAR(MAX)
						 ,PreviousDT VARCHAR(MAX)
						 ,Next_DueDT VARCHAR(MAX)
						 ,AlertDT VARCHAR(MAX)
						 ,GraceDT VARCHAR(MAX)
						 ,Expired VARCHAR(MAX)
						 ,Tail_No VARCHAR(MAX)
						 ,Aircraft_Type_Code VARCHAR(MAX)
						 ,Aircraft_Type_Description VARCHAR(MAX)
						 ,Choice VARCHAR(MAX)
						 ,Passport_No VARCHAR(MAX)
						 ,Passport_Expiry_DT VARCHAR(MAX)
						 ,Place_of_Issue VARCHAR(MAX)
						 ,Passport_Nationality VARCHAR(MAX)
						 ,Passport_License_No VARCHAR(MAX)
						 ,Visa_TYPE VARCHAR(MAX)
						 ,Visa_No NVARCHAR(MAX)
						 ,Visa_Expiry_Date VARCHAR(MAX)
						 ,Visa_Country VARCHAR(MAX)
						 ,Visa_Notes VARCHAR(MAX)
						 ,CrewName VARCHAR(MAX)
						 ,CC_Aircraft_Type VARCHAR(MAX)
						 ,LandingDay NUMERIC(7,3)
						 ,L_N90 NUMERIC(7,3)
						 ,Appr90 NUMERIC(7,3)
						 ,Instr90 NUMERIC(7,3)
						 ,days7 NUMERIC(7,3)
						 ,dayrest7 NUMERIC(7,3)
						 ,CalMon NUMERIC(7,3)
						 ,days90 NUMERIC(7,3)
						 ,CalQtrRest NUMERIC(7,3)
						 ,CalQtr NUMERIC(7,3)
						 ,Mon6 NUMERIC(7,3)
						 ,Prev2Qtrs NUMERIC(7,3)
						 ,Mon12 NUMERIC(7,3)
						 ,Calyr NUMERIC(7,3)
						 ,Days365 NUMERIC(7,3)
						 ,T_D90 NUMERIC(7,3)
						 ,T_N90 NUMERIC(7,3)
						 ,days30 NUMERIC(7,3)
						 ,TenToMin INT
						 ,CheckVal INT)
  
  DECLARE @TimeDisplayTenMin SMALLINT = 0;  
    SELECT @TimeDisplayTenMin = TimeDisplayTenMin FROM Company 
								                   WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
                                                  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)   
  DECLARE @Today DATETIME=GETDATE()
  INSERT INTO @CrewRoster(CrewID
					     ,CrewCode
					     ,CrewName
						 ,CC_Aircraft_Type 
						 ,LandingDay
						 ,L_N90 
						 ,Appr90 
						 ,Instr90 
						 ,days7 
						 ,dayrest7 
						 ,CalMon 
						 ,days90 
						 ,CalQtrRest 
						 ,CalQtr
						 ,Mon6
						 ,Prev2Qtrs 
						 ,Mon12
						 ,Calyr
						 ,Days365
						 ,T_D90
						 ,T_N90
						 ,days30
						 ,TenToMin)
  EXEC spGetReportPOSTCrewCurrency2Information @UserCD,@Today,@CrewCD,'','',0  
  
  
  INSERT INTO @CrewRoster(CrewID 
						 ,CrewCode 
						 ,Name 
						 ,Home_Phone_No 
						 ,Home_Address_1 
						 ,Home_Address_2 
						 ,Home_Address_3 
						 ,Home_City
						 ,Home_St_Prov
						 ,Home_Postal
						 ,Pager
						 ,Mobile_Phone
						 ,Home_Fax
						 ,Birth_Date
						 ,Hire_Date
						 ,Term_Date
						 ,Crew_Type
						 ,Dept_Code
						 ,Client
						 ,Country1
						 ,Country2
						 ,Social_Security_No
						 ,Home_Base
						 ,Crew_Status
						 ,Email
						 ,Lic_No
						 ,Lic_Type1
						 ,Addtional_Lic_No
						 ,Lic_Type2
						 ,Citizenship
						 ,Notes
						 ,Additional_Notes
						 ,CheckVal)
 
 SELECT DISTINCT C.CrewID
	   ,CrewCode=C.CrewCD
	   ,Name=C.LastName+', '+C.FirstName+' '+C.MiddleInitial   
	   ,Home_Phone_No=C.PhoneNum    
       ,Home_Address_1=C.Addr1    
	   ,Home_Address_2=C.Addr2  
	   ,Home_Address_3=C.Addr3   
       ,Home_City=C.CityName
       ,Home_St_Prov=C.StateName 
       ,Home_Postal=C.PostalZipCD  
       ,Pager=C.PagerNum 
       ,Mobile_Phone=C.CellPhoneNum 
       ,Home_Fax=C.FaxNum 
       ,Birth_Date=dbo.GetDateFormatByUserCD(@UserCD,C.BirthDT)
	   ,Hire_Date=dbo.GetDateFormatByUserCD(@UserCD,C.HireDT)
	   ,Term_Date=dbo.GetDateFormatByUserCD(@UserCD,C.TerminationDT)
	   ,Crew_Type=C.CrewTypeCD  
	   ,Dept_Code=D.DepartmentCD 
	   ,Client=CL.ClientCD 
	   ,Country1 = C.LicenseCountry1    
	   ,Country2 = C.LicenseCountry2
	   ,Social_Security_No=ISNULL(C.SSN,'')
	   ,Home_Base = dbo.GetHomeBaseCDByHomeBaseID(C.HomeBaseID) 
	   ,Crew_Status=(CASE WHEN C.IsStatus=1 THEN 'ACTIVE' ELSE 'INACTIVE' END)    
	   ,Email=C.EmailAddress 
	   ,Lic_No=C.PilotLicense1
	   ,Lic_Type1=CONVERT(VARCHAR(MAX),C.LicenseType1) 
	   ,Addtional_Lic_No= C.PilotLicense2  
	   ,Lic_Type2=CONVERT(VARCHAR(MAX),C.LicenseType2) 
	   ,Citizenship=CO.CountryCD
	   ,Notes=C.Notes    
	   ,Additional_Notes=C.Notes2   
	   ,CheckVal=99 
	FROM Crew C    
		 LEFT JOIN (SELECT DepartmentID, CONVERT(VARCHAR(MAX),DepartmentCD) AS DepartmentCD FROM Department) D ON C.DepartmentID = D.DepartmentID    
		 LEFT JOIN Country CO ON C.Citizenship = CO.CountryID    
		 LEFT JOIN Client CL ON C.ClientID = CL.ClientID 
    WHERE C.IsDeleted = 0    
      AND C.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
      AND C.CrewCD = RTRIM(@CrewCD) 
 
    
 
SELECT FLAG
         ,CrewID 
		 ,CrewCode 
		 ,Name 
		 ,Home_Phone_No 
		 ,Home_Address_1 
		 ,Home_Address_2 
		 ,Home_Address_3 
		 ,Home_City
		 ,Home_St_Prov
		 ,Home_Postal
		 ,Pager
		 ,Mobile_Phone
		 ,Home_Fax
		 ,Birth_Date
		 ,Hire_Date
		 ,Term_Date
		 ,Crew_Type
		 ,Dept_Code
		 ,Client
		 ,Country1
		 ,Country2
		 ,Social_Security_No
		 ,Home_Base
		 ,Crew_Status
		 ,Email
		 ,Lic_No
		 ,Lic_Type1
		 ,Addtional_Lic_No
		 ,Lic_Type2
		 ,Citizenship
		 ,Notes
		 ,Additional_Notes
		 ,Type_Code
		 ,Type_Description
		 ,PIC
		 ,SIC
		 ,Time_In_Type
		 ,As_Of_Date
		 ,Information_Description 
		 ,Information_Value
		 ,Crew_Checklist_Description 
		 ,Aircraft_Type 
		 ,PreviousDT 
		 ,Next_DueDT
		 ,AlertDT 
		 ,GraceDT
		 ,Expired
		 ,Tail_No 
		 ,Aircraft_Type_Code 
		 ,Aircraft_Type_Description 
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes
		 ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D 
		 ,L_N 
		 ,T_D
		 ,T_N
		 ,Appr 
		 ,Instr 
		 ,Days_7
		 ,Day_Rest
		 ,Cal_Mon
		 ,Days_90
		 ,Cal_Quarter
		 ,Cal_Qtr_Rest
		 ,Mon_6
		 ,Prev2_Quarters
		 ,Mon_12
		 ,Cal_Year
		 ,Days_365
		 ,days_30
		 ,TenToMin FROM ( 
 SELECT FLAG='A'
         ,CrewID 
		 ,CrewCode 
		 ,Name 
		 ,Home_Phone_No 
		 ,Home_Address_1 
		 ,Home_Address_2 
		 ,Home_Address_3 
		 ,Home_City
		 ,Home_St_Prov
		 ,Home_Postal
		 ,Pager
		 ,Mobile_Phone
		 ,Home_Fax
		 ,Birth_Date
		 ,Hire_Date
		 ,Term_Date
		 ,Crew_Type
		 ,Dept_Code
		 ,Client
		 ,Country1
		 ,Country2
		 ,Social_Security_No
		 ,Home_Base
		 ,Crew_Status
		 ,Email
		 ,Lic_No
		 ,Lic_Type1
		 ,Addtional_Lic_No
		 ,Lic_Type2
		 ,Citizenship
		 ,Notes
		 ,Additional_Notes
		 ,Type_Code
		 ,Type_Description
		 ,PIC
		 ,SIC
		 ,Time_In_Type
		 ,As_Of_Date
		 ,Information_Description 
		 ,Information_Value
		 ,Crew_Checklist_Description 
		 ,Aircraft_Type 
		 ,PreviousDT 
		 ,Next_DueDT
		 ,AlertDT 
		 ,GraceDT
		 ,Expired
		 ,Tail_No 
		 ,Aircraft_Type_Code 
		 ,Aircraft_Type_Description 
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes
		 ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30 
		 ,TenToMin=@TimeDisplayTenMin 
		 FROM @CrewRoster 
		 WHERE CheckVal=99
		  AND @CrewRosterReport=1 
 UNION ALL
  SELECT FLAG='B'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=A.AircraftCD    
	     ,Type_Description=CR.CrewRatingDescription    
	     ,PIC=CASE WHEN CR.IsPilotinCommand=1 THEN 'Y' ELSE '' END   
	     ,SIC=CASE WHEN CR.IsSecondInCommand=1 THEN 'Y' ELSE '' END    
	     ,Time_In_Type= CASE WHEN ISNULL(CR.TotalTimeInTypeHrs,0)=0 THEN NULL ELSE CR.TotalTimeInTypeHrs END       
	     ,As_Of_Date=dbo.GetDateFormatByUserCD(@UserCD, CR.TotalUpdateAsOfDT)  
		 ,Information_Description 
		 ,Information_Value
		 ,Crew_Checklist_Description 
		 ,Aircraft_Type 
		 ,PreviousDT 
		 ,Next_DueDT
		 ,AlertDT 
		 ,GraceDT
		 ,Expired
		 ,Tail_No 
		 ,Aircraft_Type_Code 
		 ,Aircraft_Type_Description 
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes
		 ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30
		 ,TenToMin=@TimeDisplayTenMin  
        FROM @CrewRoster C 
               INNER JOIN CrewRating CR ON C.CrewID=CR.CrewID AND CR.IsDeleted=0
	           LEFT JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON CR.AircraftTypeID = A.AircraftID 
	     WHERE @CrewTypeRating=1
	       AND CheckVal=99
 UNION ALL
  SELECT FLAG='C'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC='' 
	     ,SIC=''  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = CI.CrewInformationDescription  
	     ,Information_Value=ISNULL(CD.InformationValue,'') 
		 ,Crew_Checklist_Description 
		 ,Aircraft_Type 
		 ,PreviousDT 
		 ,Next_DueDT
		 ,AlertDT 
		 ,GraceDT
		 ,Expired
		 ,Tail_No 
		 ,Aircraft_Type_Code 
		 ,Aircraft_Type_Description 
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes
		 ,CrewName 
		 ,CC_Aircraft_Type  
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30 
		 ,TenToMin=@TimeDisplayTenMin 
        FROM @CrewRoster C 
		     INNER JOIN CrewDefinition CD ON CD.CrewID=C.CrewID AND CD.IsDeleted=0  AND ISNULL(CD.InformationValue,'') <> ''
		     LEFT JOIN (SELECT CustomerID, CrewInfoID, CrewInfoCD, CrewInformationDescription FROM CrewInformation) CI ON CD.CrewInfoID = CI.CrewInfoID AND CD.CustomerID = CI.CustomerID  
	    WHERE @CrewRosterReport=1 
	      AND CheckVal=99 
 UNION ALL
  SELECT FLAG='D'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC=NULL 
	     ,SIC=NULL  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = NULL
	     ,Information_Value='' 
	     ,Crew_Checklist_Description=CC.CrewChecklistDescription 
	     ,Aircraft_Type=A.AircraftCD  
	     ,PreviousDT=dbo.GetDateFormatByUserCD(@UserCD, CCD.PreviousCheckDT)
	     ,Next_DueDT=dbo.GetDateFormatByUserCD(@UserCD, CCD.DueDT) 
         ,AlertDT=dbo.GetDateFormatByUserCD(@UserCD, CCD.AlertDT) 
         ,GraceDT=dbo.GetDateFormatByUserCD(@UserCD, CCD.GraceDT)
         ,Expired=(CASE WHEN GETDATE() > CCD.DueDT AND GETDATE() > CCD.GraceDT THEN 'Y'  ELSE '' END)   
		 ,Tail_No 
		 ,Aircraft_Type_Code 
		 ,Aircraft_Type_Description 
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes 
		 ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30
		 ,TenToMin=@TimeDisplayTenMin  
        FROM @CrewRoster C 
			 INNER JOIN CrewCheckListDetail CCD  ON CCD.CrewID=C.CrewID
			 INNER JOIN (SELECT CrewCheckCD, CrewChecklistDescription, CustomerID FROM CrewChecklist) CC ON CCD.CheckListCD = CC.CrewCheckCD AND CCD.CustomerID = CC.CustomerID 
			 LEFT  JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON CCD.AircraftID = A.AircraftID  
	    WHERE @Crewchecklist=1
	      AND CCD.IsDeleted = 0 
	      AND CCD.CustomerID = @CUSTOMERID   
	      AND (CCD.IsInActive = 0 OR CCD.IsInActive = @Inactivechecklist) 
	      AND CheckVal=99 
 UNION ALL
  SELECT FLAG='E'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC=NULL 
	     ,SIC=NULL  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = NULL
	     ,Information_Value='' 
	     ,Crew_Checklist_Description=NULL
	     ,Aircraft_Type=NULL 
	     ,PreviousDT=NULL
	     ,Next_DueDT=NULL
         ,AlertDT=NULL
         ,GraceDT=NULL
         ,Expired=NULL
         ,Tail_No=F.TailNum  
	     ,Aircraft_Type_Code = A.AircraftCD  
         ,Aircraft_Type_Description=F.TypeDescription  
		 ,Choice 
		 ,Passport_No
		 ,Passport_Expiry_DT
		 ,Place_of_Issue 
		 ,Passport_Nationality
		 ,Passport_License_No 
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes 
		 ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30 
		 ,TenToMin=@TimeDisplayTenMin 
        FROM @CrewRoster C 
			 INNER JOIN  CrewAircraftAssigned CAA ON CAA.CrewID=C.CrewID
			 INNER JOIN (SELECT CustomerID, FleetID, TailNum, AircraftID, TypeDescription, ClientID FROM Fleet ) F ON CAA.CustomerID = F.CustomerID  AND CAA.FleetID = F.FleetID  
			 LEFT  JOIN (SELECT AircraftID, AircraftCD  FROM Aircraft ) A ON F.AircraftID = A.AircraftID 
	    WHERE @CrewRosterReport=1
		  AND CAA.IsDeleted = 0 
		  AND (ClientID = @CLIENTID OR @CLIENTID IS NULL )  
		  AND CheckVal=99	         
  UNION ALL
  SELECT FLAG='F'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC=NULL 
	     ,SIC=NULL  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = NULL
	     ,Information_Value='' 
	     ,Crew_Checklist_Description=NULL
	     ,Aircraft_Type=NULL 
	     ,PreviousDT=NULL
	     ,Next_DueDT=NULL
         ,AlertDT=NULL
         ,GraceDT=NULL
         ,Expired=NULL
         ,Tail_No=NULL 
	     ,Aircraft_Type_Code = NULL 
         ,Aircraft_Type_Description=NULL
         ,Choice=CASE WHEN CPP.Choice=1 THEN '*' ELSE '' END   
	     ,Passport_No=ISNULL(CPP.PassportNum,'') 
	     ,Passport_Expiry_DT=dbo.GetDateFormatByUserCD(@UserCD, CPP.PassportExpiryDT)
	     ,Place_of_Issue=CPP.IssueCity  
	     ,Passport_Nationality= dbo.GetCountryCDByCountryID(CPP.CountryID) 
	     ,Passport_License_No=CPP.PilotLicenseNum  
		 ,Visa_TYPE 
		 ,Visa_No 
		 ,Visa_Expiry_Date 
		 ,Visa_Country 
		 ,Visa_Notes 
	     ,CrewName 
		 ,CC_Aircraft_Type 
		 ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30 
		 ,TenToMin=@TimeDisplayTenMin 
        FROM @CrewRoster C 
             INNER JOIN (SELECT CrewID,Choice,PassportNum,PassportExpiryDT,CountryID,IssueCity,PilotLicenseNum FROM CrewPassengerPassport WHERE IsDeleted=0)CPP ON CPP.CrewID=C.CrewID
	    WHERE @CrewRosterReport=1
	      AND CheckVal=99
  UNION ALL
  SELECT FLAG='G'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC=NULL 
	     ,SIC=NULL  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = NULL
	     ,Information_Value='' 
	     ,Crew_Checklist_Description=NULL
	     ,Aircraft_Type=NULL 
	     ,PreviousDT=NULL
	     ,Next_DueDT=NULL
         ,AlertDT=NULL
         ,GraceDT=NULL
         ,Expired=NULL
         ,Tail_No=NULL 
	     ,Aircraft_Type_Code = NULL 
         ,Aircraft_Type_Description=NULL
         ,Choice=''   
	     ,Passport_No=''
	     ,Passport_Expiry_DT=NULL
	     ,Place_of_Issue=NULL
	     ,Passport_Nationality= NULL 
	     ,Passport_License_No=NULL 
	     ,Visa_TYPE=CPV.VisaTYPE   
	     ,Visa_No = ISNULL(CPV.VisaNum,'')
	     ,Visa_Expiry_Date=dbo.GetShortDateFormatByUserCD(@UserCD, CPV.ExpiryDT) 
	     ,Visa_Country=CO.CountryCD  
         ,Visa_Notes=CPV.Notes
		 ,CrewName 
		 ,CC_Aircraft_Type 
         ,L_D=LandingDay 
		 ,L_N=L_N90
		 ,T_D=T_D90
		 ,T_N=T_N90
		 ,Appr=Appr90 
		 ,Instr=Instr90 
		 ,Days_7=days7
		 ,Day_Rest=dayrest7
		 ,Cal_Mon=CalMon
		 ,Days_90=days90 
		 ,Cal_Quarter=CalQtr 
		 ,Cal_Qtr_Rest=CalQtrRest 
		 ,Mon_6=Mon6 
		 ,Prev2_Quarters=Prev2Qtrs 
		 ,Mon_12=Mon12 
		 ,Cal_Year=Calyr 
		 ,Days_365=Days365
		 ,days_30=days30 
		 ,TenToMin=@TimeDisplayTenMin  
        FROM @CrewRoster C 
             INNER JOIN CrewPassengerVisa CPV ON CPV.CrewID=C.CrewID
			 LEFT OUTER JOIN Country CO ON CPV.CountryID = CO.CountryID  
	    WHERE @CrewRosterReport=1 
	      AND CPV.IsDeleted=0
	      AND CheckVal=99
   UNION ALL
  SELECT DISTINCT FLAG='H'
         ,C.CrewID 
		 ,C.CrewCode 
		 ,C.Name 
		 ,C.Home_Phone_No 
		 ,C.Home_Address_1 
		 ,C.Home_Address_2 
		 ,C.Home_Address_3 
		 ,C.Home_City
		 ,C.Home_St_Prov
		 ,C.Home_Postal
		 ,C.Pager
		 ,C.Mobile_Phone
		 ,C.Home_Fax
		 ,C.Birth_Date
		 ,C.Hire_Date
		 ,C.Term_Date
		 ,C.Crew_Type
		 ,C.Dept_Code
		 ,C.Client
		 ,C.Country1
		 ,C.Country2
		 ,C.Social_Security_No
		 ,C.Home_Base
		 ,C.Crew_Status
		 ,C.Email
		 ,C.Lic_No
		 ,C.Lic_Type1
		 ,C.Addtional_Lic_No
		 ,C.Lic_Type2
		 ,C.Citizenship
		 ,C.Notes
		 ,C.Additional_Notes
	     ,Type_Code=NULL   
	     ,Type_Description=NULL   
	     ,PIC=NULL 
	     ,SIC=NULL  
	     ,Time_In_Type= NULL      
	     ,As_Of_Date=NULL 
	     ,Information_Description = NULL
	     ,Information_Value='' 
	     ,Crew_Checklist_Description=NULL
	     ,Aircraft_Type=NULL 
	     ,PreviousDT=NULL
	     ,Next_DueDT=NULL
         ,AlertDT=NULL
         ,GraceDT=NULL
         ,Expired=NULL
         ,Tail_No=NULL 
	     ,Aircraft_Type_Code = NULL 
         ,Aircraft_Type_Description=NULL
         ,Choice=''   
	     ,Passport_No=''
	     ,Passport_Expiry_DT=NULL
	     ,Place_of_Issue=NULL
	     ,Passport_Nationality= NULL 
	     ,Passport_License_No=NULL 
	     ,Visa_TYPE=NULL   
	     ,Visa_No = NULL
	     ,Visa_Expiry_Date=NULL 
	     ,Visa_Country=NULL
         ,Visa_Notes=NULL 
		 ,CC.CrewName 
		 ,CC.CC_Aircraft_Type 
		 ,L_D=ISNULL(CC.LandingDay ,0) 
		 ,L_N=ISNULL(CC.L_N90,0) 
		 ,T_D=ISNULL(CC.T_D90,0) 
		 ,T_N=ISNULL(CC.T_N90,0) 
		 ,Appr=ISNULL(CC.Appr90,0) 
		 ,Instr=ISNULL(CC.Instr90,0) 
		 ,Days_7=ISNULL(CC.days7,0) 
		 ,Day_Rest=ISNULL(CC.dayrest7,0) 
		 ,Cal_Mon=ISNULL(CC.CalMon,0) 
		 ,Days_90=ISNULL(CC.days90,0) 
		 ,Cal_Quarter=ISNULL(CC.CalQtr,0) 
		 ,Cal_Qtr_Rest=ISNULL(CC.CalQtrRest,0) 
		 ,Mon_6=ISNULL(CC.Mon6,0) 
		 ,Prev2_Quarters=ISNULL(CC.Prev2Qtrs,0) 
		 ,Mon_12=ISNULL(CC.Mon12 ,0) 
		 ,Cal_Year=ISNULL(CC.Calyr,0) 
		 ,Days_365=ISNULL(CC.Days365,0) 
		 ,days_30=ISNULL(CC.days30,0)  
		 ,TenToMin=@TimeDisplayTenMin 
        FROM (SELECT * FROM @CrewRoster C WHERE CheckVal=99)C
             INNER JOIN (SELECT * FROM @CrewRoster C WHERE CheckVal IS NULL)CC ON CC.CrewID=C.CrewID
             INNER JOIN CrewPassengerVisa CPV ON CPV.CrewID=C.CrewID
			 LEFT OUTER JOIN Country CO ON CPV.CountryID = CO.CountryID  
	    WHERE @CrewCurrency=1 
	      AND CPV.IsDeleted=0
)Crew
  -- EXEC spGetReportCrewExportInformation 'SUPERVISOR_99', '2CS'  
  
  
  
END 
   
  
  
  
  
  

GO


