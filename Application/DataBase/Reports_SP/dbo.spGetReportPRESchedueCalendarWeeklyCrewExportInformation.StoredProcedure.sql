IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarWeeklyCrewExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCrewExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











	CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCrewExportInformation]  
			@UserCD VARCHAR(50)--Mandatory
		   ,@UserCustomerID  VARCHAR(30)---=10098
		   ,@BeginDate DATETIME --Mandatory
		   ,@NoOfWeeks INT=1
		   ,@EndDate DATETIME--Mandatory
		   ,@ClientCD VARCHAR(5)=''
		   ,@RequestorCD VARCHAR(5)=''
		   ,@DepartmentCD VARCHAR(8)=''
		   ,@FlightCatagoryCD VARCHAR(25)=''
		   ,@DutyTypeCD VARCHAR(2)=''
		   ,@HomeBaseCD VARCHAR(25)=''	
		   ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
		   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
		   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
		   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   		   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   		   ,@IsAllCrew BIT=1
   		   ,@HomeBase BIT=0
		   ,@FixedWingCrew BIT=0
		   ,@RotaryWingCrew BIT=0
		   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
		   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
		   ---Display Options---
		  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
		  ,@IsShowTrip BIT=1
		  ,@IsActivityOnly BIT=0
		  ,@IsFirstLastLeg BIT=0
		  ,@IsCrew	BIT=0
		  ,@IsShowTripStatus BIT=0
		  ,@IsDisplayRedEyeFlight BIT=0
		  ,@IsFlightNo BIT=0
		  ,@IsLegPurpose BIT=1
		  ,@IsSeatAvailable BIT=1
		  ,@IsPaxCount BIT=1
		  ,@IsRequestor BIT=1
		  ,@IsDepartment BIT=1
		  ,@IsAuthorization  BIT=1
		  ,@IsFlightCategory BIT=1
		  ,@IsCummulativeETE BIT=1
		  ,@Footer BIT ---0 Skip Footer,1-Print Footer
		  ,@BlackWhiteClr BIT=0
		  ,@AircraftClr BIT=0
		  ,@ECrew VARCHAR(MAX)=''
	AS  
	BEGIN  
	-- ===============================================================================  
	-- SPC Name: spGetReportPRESchedueCalendarWeeklyCrewExportInformation  
	-- Author: Askar 
	-- Create date: 23 Jul 2013  
	-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
	-- Revision History  
	-- Date   Name  Ver  Change  
	--   
	-- ================================================================================  
	  
	SET NOCOUNT ON     
	DECLARE @DATEFROM DATETIME=@BEGINDATE,@DATETO DATETIME=@ENDDATE                 	
	
	DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
								WHEN @Vendors=2 THEN 1 
								WHEN @Vendors=3 THEN 0
								END)
			
			DECLARE @TripStatus AS VARCHAR(20) = '';
			
			IF @IsWorkSheet = 1
			SET @TripStatus = 'W,'
			
			IF  @IsTrip = 1
			SET @TripStatus = @TripStatus + 'T,'

			IF  @IsUnFulFilled = 1
			SET @TripStatus = @TripStatus + 'U,'
			
			IF  @IsCanceled = 1
			SET @TripStatus = @TripStatus + 'X,'
			
			IF  @IsHold = 1
			SET @TripStatus = @TripStatus + 'H'
			


	IF @HomeBase=1
	BEGIN

	SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
								   INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
					WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
					 AND UM.UserName = RTRIM(@UserCD)


	END




	-----Rest Record and Maintenace Reco
	DECLARE @CurDate TABLE(DateWeek DATE,CrewID BIGINT)

	 
	 DECLARE @TableTrip TABLE(TripNUM BIGINT,
							  FlightNUM VARCHAR(12),
							  PassengerTotal INT,
							  PassengerRequestorCD VARCHAR(5),
							  DepartmentCD VARCHAR(8),
							  AuthorizationCD VARCHAR(8),
							  TripDescription VARCHAR(40),
							  FlightPurpose VARCHAR(40),
							  SeatsAvailabe INT,
							  CrewList VARCHAR(300),
							  CummETE NUMERIC(7,3),
							  TripStatus CHAR(2),
							  FlightCatagoryCD CHAR(4),
							  DepartureDTTMLocal DATETIME,
							  ArrivalDTTMLocal DATETIME,
							  CrewID BIGINT,
							  DutyTYPE CHAR(2),
							  NextLocalDTTM DATETIME,
							  RecordTye CHAR(1),
							  DepICAOID VARCHAR(25),
							  ArrivelICAOID VARCHAR(25),
							  DepCity VARCHAR(25),
							  ArrCity VARCHAR(25),
							  DepState VARCHAR(25),
							  ArrState VARCHAR(25),
							  LegNum BIGINT,
							  TripID BIGINT,
							  LegID BIGINT,
							  VendorCode VARCHAR(5),
							  FgColor VARCHAR(8),
							  BgColr  VARCHAR(8),
							  TypeCode VARCHAR(10),
							  tail_nmbr VARCHAR(9)
							  )
	 


	   
	 INSERT INTO @TableTrip
	 SELECT DISTINCT PM.TripNUM
		 ,PM.FlightNUM
		 ,PL.PassengerTotal
		 ,P.PassengerRequestorCD
		 ,DEP.DepartmentCD
		 ,DA.AuthorizationCD
		 ,PM.TripDescription
		 ,PL.FlightPurpose
		 ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
		 ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
					  WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
		 ,CummETE=PL1.Elapse
		 ,PM.TripStatus
		 ,FC.FlightCatagoryCD
		 ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
		 ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
		 ,PCL.CREWID
		 ,PL.DutyTYPE
		 ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
		 ,PM.RecordType
		 ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
			   WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
		 ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
			   WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
		 ,D.CityName
		 ,A.CityName
		 ,D.StateName
		 ,A.StateName
		 ,PL.LegNUM
		 ,PM.TripID
		 ,PL.LegID
		 ,V.VendorCD
		 ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
					   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
					   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
		 ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
		 WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
					   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
		, AFT.AircraftCD
		,F.TailNum
			FROM PreflightMain PM 
				  INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
				--  LEFT OUTER JOIN PreflightLeg PLN ON PL.TripID=PLN.TripID AND PL.LegNUM+1=PLN.LegNUM
				  INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
				  LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
				  LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
				  LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
				  LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
				  LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
				  LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
				  INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
				  INNER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
				  LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
				  LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
				  LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
				  LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																				 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																				 WHERE PC1.LegID = PC2.LegID   
																				  AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
																				  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																			WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																				 FOR XML PATH('') 
																			 ) FROM PreflightCrewList PC2         
									  ) Crew ON PL.LegID = Crew.LegID 
				 LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
				 LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
				 LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
				-- LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
				 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
													 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
													 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
				 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
													 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
													 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
		  WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
		   AND PM.IsDeleted=0
		   AND (	
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
					  OR
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
					  OR
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
					)
		   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		   AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		   AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		   AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		   AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		   AND (PL.DutyType  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		   AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		   AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


	 INSERT INTO @TableTrip
	 SELECT DISTINCT PM.TripNUM
		 ,PM.FlightNUM
		 ,PL.PassengerTotal
		 ,P.PassengerRequestorCD
		 ,DEP.DepartmentCD
		 ,DA.AuthorizationCD
		 ,PM.TripDescription
		 ,PL.FlightPurpose
		 ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
		 ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
					  WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
		 ,CummETE=PL1.Elapse
		 ,PM.TripStatus
		 ,FC.FlightCatagoryCD
		 ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
		 ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
		 ,PCL.CrewID
		 ,PL.DutyTYPE
		 ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'  THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE  CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
		 ,PM.RecordType
		 ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
			   WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
		 ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
			   WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
		 ,D.CityName
		 ,A.CityName
		 ,D.StateName
		 ,A.StateName
		 ,PL.LegNUM
		 ,PM.TripID
		 ,PL.LegID 
		  ,V.VendorCD
		  ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
					   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
					   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
		  ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
		   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
					   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
		  ,AFT.AircraftCD
		  ,F.TailNum
			FROM PreflightMain PM 
				  INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
				 --- LEFT OUTER JOIN PreflightLeg PLN ON PL.TripID=PLN.TripID AND PL.LegNUM+1=PLN.LegNUM
				  INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
				  LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
				  LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
				  LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
				  LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
				  LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
				  LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
				  INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
				  INNER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
				  LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
				  LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
				  LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
				  LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																				 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																				 WHERE PC1.LegID = PC2.LegID   
																				  AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
																				  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																			WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																				 FOR XML PATH('') 
																			 ) FROM PreflightCrewList PC2         
									  ) Crew ON PL.LegID = Crew.LegID 
				 LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
				 LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
				 LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
				-- LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
				 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
													 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
													 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
				 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
													 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
													 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
		  WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
		   AND PM.IsDeleted=0
		   AND (	
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
					  OR
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
					  OR
						(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
					)
		   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		   AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		   AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		   AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		   AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		   AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		   AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		   AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		   AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
		   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


	INSERT INTO @CurDate(DateWeek,CrewID)
	SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
									 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
									 AND CC.IsDeleted=0 
									 AND CC.IsStatus=1
									 AND (CC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
									 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) 
									 AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
	 



	INSERT INTO @TableTrip(CrewID,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,VendorCode,FgColor,BgColr,tail_nmbr,TypeCode)
	SELECT  CD.CrewID,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
					CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye 
					,TT.LegID,TT.VendorCode,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END) ,TT.tail_nmbr,TT.TypeCode
					FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.CrewID=TT.CrewID
												 WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
												   AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
												   AND RecordTye='T'
												  -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


	INSERT INTO @TableTrip(CrewID,TripDescription,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,VendorCode,FgColor,BgColr,tail_nmbr,TypeCode)
	SELECT  CD.CrewID,TripDescription,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
					CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye 
					,TT.LegID,TT.VendorCode,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END),TT.tail_nmbr,TT.TypeCode
					FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.CrewID=TT.CrewID
												 WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
												   AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
												   AND RecordTye IN('C','M')
												   AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
												  -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


	                  
	DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


	DECLARE @TempTable TABLE(ID INT IDENTITY
							,WeekNumber INT
							,CrewID BIGINT
							,CrewCD VARCHAR(5)
							,crewname VARCHAR(70)
							,CrewMember VARCHAR(60)
							,TripNUM BIGINT
							,location VARCHAR(200) 
							,FlightNUM VARCHAR(12)
							,pax_total INT 
							,reqcode VARCHAR(5)
							,DepartmentCD VARCHAR(8)
							,AuthorizationCD VARCHAR(8)
							,FlightPurpose VARCHAR(40)
							,SeatsAvailable INT
							,crewlist VARCHAR(300) 
							,CummETE NUMERIC(7,3)
							,TripStatus CHAR(2)
							,FlightCatagoryCD VARCHAR(25) 
							,Date DATE
							,DepartureDTTMLocal DATETIME
							,ArrivalDTTMLocal DATETIME
							,IsArrival DATE
							,LegNum BIGINT
							,SORTORDER INT
							,RecordType VARCHAR(1)
							,legid BIGINT
							,vendcode VARCHAR(5)
							,DutyTYPE CHAR(2)
							,FgColor VARCHAR(8)
							,BgColr  VARCHAR(8)
							,TypeCode VARCHAR(10)
							,tail_nmbr VARCHAR(9)
							,RnkFirst INT
							,RnkLast  INT) 
	 
	IF @IsDisplayRedEyeFlight=0
	BEGIN  
	INSERT INTO @TempTable   
	SELECT DATEPART(DAY, DATEDIFF(DAY, @BeginDate, DepartureDTTMLocal)/7 * 7)/7 + 1 WeekNumber
		 ,TT.CrewID
		  ,C.CrewCD
		  ,crewname=C.LastName+', '+C.FirstName+' '+C.MiddleInitial
		  ,C.CrewCD+'@'+C.LastName+', '+C.FirstName
		  ,TripNUM
		 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
			   WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+TripDescription
			   ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+DepICAOID+' | '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
		 ,FlightNUM
		 ,PassengerTotal
		 ,PassengerRequestorCD
		 ,DepartmentCD
		 ,AuthorizationCD
		 ,FlightPurpose
		 ,SeatsAvailabe
		 ,CrewList
		 ,CummETE
		 ,TripStatus
		 ,FlightCatagoryCD
		 ,CONVERT(DATE,DepartureDTTMLocal)
		 ,DepartureDTTMLocal
		 ,ArrivalDTTMLocal
		 ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
		 ,LegNum
		 ,1 AS SORTORDER
		 ,TT.RecordTye
		 ,TT.LegID
		 ,TT.VendorCode
		 ,TT.DutyTYPE
		 ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.ForeGrndCustomColor
								      WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.ForeGrndCustomColor ELSE TT.FgColor END)
            
		 ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.BackgroundCustomColor 
								    WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.BackgroundCustomColor ELSE TT.BgColr END)
		 ,TT.TypeCode
		 ,TT.tail_nmbr
		 ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
		 ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC) 

		  FROM @TableTrip TT 
				INNER JOIN Crew C ON TT.CrewID=C.CrewID 
				LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
				LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
		  WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
		  ORDER BY CrewCD,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum
	END
	ELSE
	BEGIN
	INSERT INTO @TempTable 
	 SELECT TEMP.*
		   ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
		   ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC)
		  FROM(
	 SELECT DATEPART(DAY, DATEDIFF(DAY, @BeginDate, DepartureDTTMLocal)/7 * 7)/7 + 1 WeekNumber
		   ,TT.CrewID
		   ,C.CrewCD
		   ,crewname=C.LastName+', '+C.FirstName
		   ,CrewMember=C.LastName+', '+C.FirstName+' '+C.MiddleInitial
		  ,TripNUM
		 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
			   WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+TripDescription
			   ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+DepICAOID+(CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN ' [continued]' ELSE  ' | '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END) END Location
		 ,FlightNUM
		 ,PassengerTotal
		 ,PassengerRequestorCD
		 ,DepartmentCD
		 ,AuthorizationCD
		 ,FlightPurpose
		 ,SeatsAvailabe
		 ,CrewList
		 ,CummETE
		 ,TripStatus
		 ,FlightCatagoryCD
		 ,CONVERT(DATE,DepartureDTTMLocal) Date
		 ,DepartureDTTMLocal
		 ,ArrivalDTTMLocal
		 ,NULL IsArrival 
		 ,LegNum
		 ,1 AS SORTORDER
		 ,TT.RecordTye
		  ,TT.LegID
		  ,TT.VendorCode
		  ,TT.DutyTYPE
		  ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.ForeGrndCustomColor
								      WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.ForeGrndCustomColor ELSE TT.FgColor END)
            
		  ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.BackgroundCustomColor 
								    WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.BackgroundCustomColor ELSE TT.BgColr END)
		  ,TT.TypeCode
		  ,TT.tail_nmbr
		  FROM @TableTrip TT 
				INNER JOIN Crew C ON TT.CrewID=C.CrewID 
				LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
				LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
		  WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)


	 UNION ALL

	SELECT DATEPART(DAY, DATEDIFF(DAY, @BeginDate, DepartureDTTMLocal)/7 * 7)/7 + 1 WeekNumber
		  ,TT.CrewID
		  ,C.CrewCD
		   ,C.LastName+', '+C.FirstName+' '+C.MiddleInitial
		  ,C.CrewCD+'@'+C.LastName+', '+C.FirstName
		  ,TripNUM
		 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
			   WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'@'+TripDescription
			   ELSE ' [continued] '  +ArrivelICAOID+' | '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
		 ,FlightNUM
		 ,PassengerTotal
		 ,PassengerRequestorCD
		 ,DepartmentCD
		 ,AuthorizationCD
		 ,FlightPurpose
		 ,SeatsAvailabe
		 ,CrewList
		 ,CummETE
		 ,TripStatus
		 ,FlightCatagoryCD
		 ,CONVERT(DATE,ArrivalDTTMLocal)
		 ,ArrivalDTTMLocal---DepartureDTTMLocal
		 ,ArrivalDTTMLocal
		 ,NULL  IsArrival  
		 ,LegNum
		 ,2 AS SORTORDER 
		 ,TT.RecordTye
		,TT.LegID
		  ,TT.VendorCode
		  ,TT.DutyTYPE
          ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.ForeGrndCustomColor
								           WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.ForeGrndCustomColor ELSE TT.FgColor END)
            
          ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.BackgroundCustomColor 
	  							        WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.BackgroundCustomColor ELSE TT.BgColr END)
		  ,TT.TypeCode
		  ,TT.tail_nmbr
		  FROM @TableTrip TT 
				INNER JOIN Crew C ON TT.CrewID=C.CrewID 
				LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
				LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
		  WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
		   AND CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal)
	 )TEMP
	ORDER BY CrewCD,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
	END


	 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;


	SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 
	--SELECT  @WEEK= CASE WHEN (COUNT( DISTINCT WeekNumber))=0 THEN ((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) ELSE COUNT( DISTINCT WeekNumber) END  FROM #TempTable

	WHILE (@WEEKNUM<=@WEEK)

	 BEGIN
	  
	  SET @BeginDate=DATEADD(DAY,7,@BeginDate)
	  
	IF @IsActivityOnly=1 AND EXISTS(SELECT CrewID FROM @TempTable)
	  
	  BEGIN

		  WHILE @DateVal < @BeginDate
	       
		   BEGIN
			
			UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=@DateVal
			INSERT INTO @TempTable(CrewCD,CrewID,WeekNumber,Date,CrewMember,RnkFirst,RnkLast,crewname)
			SELECT DISTINCT C.CrewCD,C.CrewID,@WEEKNUM,@DateVal,C.CrewCD+'@'+C.LastName+', '+C.FirstName,1,1,C.LastName+', '+C.FirstName FROM Crew C 
														  INNER JOIN @TempTable TT ON TT.CrewID=C.CrewID
													  WHERE C.IsDeleted=0
													   AND C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
													   AND C.IsStatus=1
													   AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
													   AND C.CrewID NOT IN(SELECT ISNULL(CrewID,0) FROM @TempTable WHERE WeekNumber=@WEEKNUM AND Date=@DateVal)  
	      
		   SET @DateVal=DATEADD(DAY,1,@DateVal)
	      
		  END
	  END
	 
	 ELSE IF EXISTS (SELECT CrewID FROM @TempTable)
	   
	 BEGIN
	   
		WHILE @DateVal < @BeginDate
	      
		  BEGIN
		   UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=@DateVal
		   INSERT INTO @TempTable(CrewCD,CrewID,WeekNumber,Date,CrewMember,RnkFirst,RnkLast,crewname)
		   SELECT DISTINCT C.CrewCD,C.CrewID,@WEEKNUM,@DateVal,C.CrewCD+'@'+C.LastName+', '+C.FirstName,1,1,C.LastName+', '+C.FirstName FROM Crew C 
												  WHERE C.IsDeleted=0
												   AND C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
												   AND C.IsStatus=1
												   AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
												   AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
												   AND C.CrewID NOT IN(SELECT ISNULL(CrewID,0) FROM @TempTable WHERE WeekNumber=@WEEKNUM AND Date=@DateVal)  
		   SET @DateVal=DATEADD(DAY,1,@DateVal)
	     
	  END
	 END
	 ELSE 
	   
	 BEGIN
	   
		WHILE @DateVal < @BeginDate
	      
		  BEGIN
		   INSERT INTO @TempTable(WeekNumber,Date,RnkFirst,RnkLast)
		   SELECT DISTINCT @WEEKNUM,@DateVal,1,1 
		   SET @DateVal=DATEADD(DAY,1,@DateVal)
	     
	  END
	 END        
	 

	 SET @WEEKNUM=@WEEKNUM+1
	 
	 END


	IF @IsFirstLastLeg=0
	 BEGIN
	;WITH TripInfo AS(
	SELECT display=CrewMember
		,col=(CASE WHEN ISNULL(col2,'')<>'' THEN col2+'@'  ELSE '' END) +col3+(CASE WHEN ISNULL(col4,'')<>'' THEN '@'+col4  ELSE '' END)+((CASE WHEN ISNULL(col5,'')<>'' THEN '@'+col5  ELSE '' END))
		,dispcolor='#FFFFFF'
		,fcolor=FgColor
		,bcolor=BgColr
		,uline='TRUE'--CASE WHEN DayVal=1 THEN 'TRUE' ELSE 'FALSE' END
		,orig_nmbr
		,leg_num
		,legid
		,caldate
		,type_code=TypeCode
		,vendcode
		,tail_nmbr
		,ordernum
		,locdep
		,flag
		,timetype=@TimeBase+' Time'
		,gotofleet=@DATEFROM
		,ncounter=0
		,ArrivalDTTMLocal
		,SortDate
		,WeekNumber
		,SORTORDER
		,DepartureDTTMLocal
	   -- ,DayVal
		,CrewID
		,CrewMember
		,CrewCD
		,DayNo
		,crewname
		,Rnk
	  FROM(
	 SELECT ID
		   ,WeekNumber
		   ,1 WEEKDAY
		   ,CrewID
		   ,CrewCD
		   ,CrewMember
		   ,Date SortDate
		   ,col2=(CASE WHEN SUBSTRING(location,1,3)='(R)' THEN '' ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE '' END) END)
				 +(CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE '' END)
				 +(CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  ' '+'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(CummETE,0)*10)*0.1) ELSE '' END)
		   ,col3=location+(CASE WHEN @IsFlightNo=1 AND location IS NOT NULL AND RecordType='T' THEN ' FlightNo: '+ISNULL(FlightNUM,'') ELSE '' END )
		   ,col4=CASE WHEN @IsDisplayRedEyeFlight=0 THEN '** Arrival Date: 01/24/13 **'+ CONVERT(VARCHAR(10),IsArrival,101) ELSE '' END
		   ,col5=LTRIM(RTRIM(ISNULL(tail_nmbr,'')+' '+ISNULL(crewlist,'')+' '+(CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE '' END)
				 +(CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  ' ('+FlightCatagoryCD+')' ELSE '' END)
				 +(CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Req: '+ISNULL(reqcode,'') ELSE '' END)
				 +(CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Dept: '+ISNULL(DepartmentCD,'') ELSE '' END)
				 +(CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Auth: '+ISNULL(AuthorizationCD,'') ELSE '' END)
				 +(CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Purpose: '+ISNULL(FlightPurpose,'') ELSE '' END)
				 +(CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END )))
		   ------Export------------
		   ,orig_nmbr=TripNUM
		   ,leg_num=LegNum
		   ,legid
		   ,vendcode
		   ,caldate=Date  
		   ,TT.FgColor
		   ,TT.BgColr 
		   ,ncounter=0
		   ,locdep=TT.DepartureDTTMLocal 
		   ,ordernum=RIGHT(RTRIM('0000'+CONVERT(CHAR(5),DENSE_RANK()OVER(PARTITION BY WeekNumber ORDER BY  WeekNumber,CrewCD)+1)),5) 
		   ,flag='A'
		   ,ArrivalDTTMLocal
		   ,SORTORDER
		   ,DepartureDTTMLocal
		   ,TypeCode
		   ,tail_nmbr
	       
		  ,DENSE_RANK()OVER(PARTITION BY TT.CrewCD,WeekNumber ORDER BY TT.CrewCD,WeekNumber,date) DayNo
		--   ,ROW_NUMBER()OVER(PARTITION BY TT.TailNum,WeekNumber ORDER BY TT.TailNum,WeekNumber,date) DayVal
		   ,ROW_NUMBER()OVER(PARTITION BY TT.CrewCD,WeekNumber,date ORDER BY TT.CrewCD,WeekNumber,date) Rnk
		   ,crewname

	  FROM @TempTable TT
	   WHERE DATE BETWEEN @DATEFROM AND @DATETO

	)TEMP
	)


	SELECT WeekNumber
		  ,display
		  ,col1=CASE WHEN ISNULL(col1,'')<>''  THEN SUBSTRING(col1,1,LEN(col1)-1) END
		  ,dispcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '#FFFFFF' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(dispcolor,'') <> '' THEN SUBSTRING(dispcolor,1,LEN(dispcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0'  ELSE '0' END
		  ,fcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(fcolor,'') <> '' THEN SUBSTRING(fcolor,1,LEN(fcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0' END
		  ,bcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '#FFFFFF' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(bcolor,'') <> '' THEN SUBSTRING(bcolor,1,LEN(bcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0'  ELSE '0' END
		  ,uline=CASE WHEN ISNULL(crewcode,'')='' THEN 'FALSE' 
					  WHEN ISNULL(tail_nmbr,'') <> ''   THEN SUBSTRING(uline,1,LEN(uline)-1) 
					  WHEN ISNULL(crewcode,'') <> ''  THEN 'TRUE@FALSE'  ELSE 'FALSE' END
		  ,orig_nmbr=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
						   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(orig_nmbr,'') <> ''  THEN SUBSTRING(orig_nmbr,1,LEN(orig_nmbr)-1) 
						  WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,leg_num=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
						WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(leg_num,'') <> ''  THEN SUBSTRING(leg_num,1,LEN(leg_num)-1) 
						WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,legid=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
					  WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(legid,'') <> ''  THEN SUBSTRING(legid,1,LEN(legid)-1) 
					  WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,caldate=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(caldate,1,LEN(caldate)-1) ELSE SUBSTRING(caldate,1,10) END
		  ,type_code=SUBSTRING(type_code,1,LEN(type_code)-1)
		  ,vendcode=SUBSTRING(vendcode,1,LEN(vendcode)-1)
		  ,tail_nmbr=SUBSTRING(tail_nmbr,1,LEN(tail_nmbr)-1)
		  ,ordernum=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(ordernum,1,LEN(ordernum)-1) ELSE ''  END
		  ,locdep=SUBSTRING(locdep,1,LEN(locdep)-1)
		  ,flag=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(flag,1,LEN(flag)-1) ELSE 'A' END
		  ,timetype=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(timetype,1,LEN(timetype)-1) ELSE LEFT(timetype,10) END
		  ,gotofleet=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(gotofleet,1,LEN(gotofleet)-1)  ELSE LEFT(gotofleet,10) END
		  ,ncounter=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(ncounter,1,LEN(ncounter)-1) ELSE '0' END
		  ,ColVal
		  ,ArrivalDTTMLocal
		  ,SORTORDER
		  ,SortDate
		  ,DepartureDTTMLocal
		  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
		  ,DayNo
		  ,crewname=SUBSTRING(crewname,1,LEN(crewname)-1)
		  ,crewcode=SUBSTRING(crewcode,1,LEN(crewcode)-1) FROM(
	SELECT DISTINCT TI.WeekNumber
		  ,display=CrewMember
		  ,col1=TC.col
		  ,dispcolor=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',dispcolor+'@'),'Z',dispcolor+'@')
		  ,fcolor=(CASE WHEN len(TC.fcolor)-len(replace(TC.fcolor,'@',''))=MaxCount THEN TC.fcolor ELSE TC.fcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.fcolor)-len(replace(TC.fcolor,'@','')))),'Z','0@') END)
		  ,bcolor=(CASE WHEN len(TC.bcolor)-len(replace(TC.bcolor,'@',''))=MaxCount THEN TC.bcolor ELSE TC.bcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.bcolor)-len(replace(TC.bcolor,'@','')))),'Z','0@') END)
		  ,uline='TRUE@'+SUBSTRING(REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BBB')),1,ISNULL(MaxCount,3)),'B','FALSE'+'@'),'Z','FALSE'+'@'),1,LEN(REPLACE(REPLACE(SUBSTRING((ISNULL(TW.ColVal+TT.ColVal,'BBB')),1,ISNULL(MaxCount,3)),'B','FALSE'+'@'),'Z','FALSE'+'@'))-6)
		  ,orig_nmbr=(CASE WHEN len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@',''))=MaxCount THEN TC.orig_nmbr ELSE TC.orig_nmbr+REPLACE(REPLICATE('Z',MaxCount-(len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@','')))),'Z','0@') END)
		  ,leg_num=(CASE WHEN len(TC.legnum)-len(replace(TC.legnum,'@',''))=MaxCount THEN TC.legnum ELSE TC.legnum+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legnum)-len(replace(TC.legnum,'@','')))),'Z','0@') END)
		  ,legid=(CASE WHEN len(TC.legid)-len(replace(TC.legid,'@',''))=MaxCount THEN TC.legid ELSE TC.legid+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legid)-len(replace(TC.legid,'@','')))),'Z','0@') END)
		  ,caldate=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(10),TI.caldate,101)+'@'),'Z',CONVERT(VARCHAR(10),TI.caldate,101)+'@')
		  ,type_code=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',type_code+'@'),'Z',type_code+'@')
		  ,vendcode=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',vendcode+'@'),'Z',vendcode+'@')
		  ,tail_nmbr=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',tail_nmbr+'@'),'Z',tail_nmbr+'@')
		  ,crewcode=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CrewCD+'@'),'Z',CrewCD+'@')	
		  ,ordernum=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',ordernum+'@'),'Z',ordernum+'@')
		  ,locdep=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',LEFT(CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,120),16)+'@'),'Z',LEFT(CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,120),16)+'@')
		  ,flag=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',flag+'@'),'Z',flag+'@')
		  ,timetype=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',timetype+'@'),'Z',timetype+'@')
		  ,gotofleet=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(10),gotofleet,101)+'@'),'Z',CONVERT(VARCHAR(10),gotofleet,101)+'@')
		  ,ncounter=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(2),ncounter)+'@'),'Z',CONVERT(VARCHAR(2),ncounter)+'@')
		  ,ColVal=SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,MaxCount)
		  ,ArrivalDTTMLocal
		  ,SORTORDER
		  ,SortDate
		  ,TI.DepartureDTTMLocal
		  --,TI.DayVal
		  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
		  ,DayNo
		  ,RnkVal=ROW_NUMBER()OVER(PARTITION BY TI.CrewID,TI.Caldate ORDER BY TI.CrewID,TI.Caldate)
		  ,crewname=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',crewname+'@'),'Z',crewname+'@')
	 FROM TripInfo TI
		   LEFT JOIN (SELECT WeekNumber,CrewID,MIN(DepartureDTTMLocal) DepartureDTTMLocal FROM TripInfo TM GROUP BY WeekNumber,CrewID)TM ON TM.WeekNumber=TI.WeekNumber AND TM.CrewID=TI.CrewID
		  LEFT JOIN (SELECT CrewID, REPLICATE('Z',MAX(TT.ColCount))ColVal,WeekNumber,MAX(TT.ColCount) MaxCount FROM 
									 (
									 SELECT CrewID,SUM((len(col)-len(replace(col,'@','')))+1) ColCount,WeekNumber FROM  TripInfo GROUP BY WeekNumber,CrewID,caldate
									 )TT
					GROUP BY WeekNumber,CrewID)TT ON TI.WeekNumber=TT.WeekNumber AND TI.CrewID=TT.CrewID
		 LEFT JOIN (SELECT REPLICATE('B',SUM((len(col)-len(replace(col,'@','')))+1)) ColVal,caldate,WeekNumber ,CrewID FROM  TripInfo GROUP BY WeekNumber,CrewID,caldate
					 )TW ON TI.WeekNumber=TW.WeekNumber AND TW.caldate=TI.caldate AND TW.CrewID=TI.CrewID
		 LEFT JOIN (SELECT T1.CrewID,T1.WeekNumber,T1.caldate,col=(SELECT CASE WHEN ISNULL(col,'')<>'' THEN col+'@' ELSE '' END FROM (SELECT WeekNumber,caldate,col,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,orig_nmbr=(SELECT  CASE WHEN ISNULL(orig_nmbr,'')<>'' THEN (CASE WHEN RIGHT(orig_nmbr,1)='@' THEN orig_nmbr ELSE  orig_nmbr+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),orig_nmbr))orig_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,legid=(SELECT  CASE WHEN ISNULL(legid,'')<>'' THEN (CASE WHEN RIGHT(legid,1)='@' THEN legid ELSE  legid+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),legid))legid,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)	
									,legnum=(SELECT  CASE WHEN ISNULL(legnum,'')<>'' THEN (CASE WHEN RIGHT(legnum,1)='@' THEN legnum ELSE  legnum+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),leg_num))legnum,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,fcolor=(SELECT  CASE WHEN ISNULL(fcolor,'')<>'' THEN (CASE WHEN RIGHT(fcolor,1)='@' THEN fcolor ELSE  fcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',fcolor)fcolor,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID  ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,bcolor=(SELECT  CASE WHEN ISNULL(bcolor,'')<>'' THEN (CASE WHEN RIGHT(bcolor,1)='@' THEN bcolor ELSE  bcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',bcolor)bcolor,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
										FROM(SELECT DISTINCT WeekNumber,caldate,CrewID FROM  TripInfo) T1
									

				)TC ON TC.WeekNumber=TI.WeekNumber AND TC.caldate=TI.caldate  AND TC.CrewID=TI.CrewID
	 )WeekFleet
	  WHERE RnkVal=1
	 ORDER BY WeekNumber,display,ISNULL(tail_nmbr,'ZZZZ'),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,orig_nmbr,leg_num,SORTORDER
	 

	 END
	 ELSE
	 
	 BEGIN
	;WITH TripInfo AS(
	SELECT display=CrewMember
		,col=(CASE WHEN ISNULL(col2,'')<>'' THEN col2+'@'  ELSE '' END) +col3+(CASE WHEN ISNULL(col4,'')<>'' THEN '@'+col4  ELSE '' END)+((CASE WHEN ISNULL(col5,'')<>'' THEN '@'+col5  ELSE '' END))
		,dispcolor='#FFFFFF'
		,fcolor=FgColor
		,bcolor=BgColr
		,uline='TRUE'--CASE WHEN DayVal=1 THEN 'TRUE' ELSE 'FALSE' END
		,orig_nmbr
		,leg_num
		,legid
		,caldate
		,type_code=TypeCode
		,vendcode
		,tail_nmbr
		,ordernum
		,locdep
		,flag
		,timetype=@TimeBase+' Time'
		,gotofleet=@DATEFROM
		,ncounter=0
		,ArrivalDTTMLocal
		,SortDate
		,WeekNumber
		,SORTORDER
		,DepartureDTTMLocal
	   -- ,DayVal
		,CrewID
		,CrewMember
		,CrewCD
		,DayNo
		,Rnk
		,crewname
	  FROM(
	 SELECT ID
		   ,WeekNumber
		   ,1 WEEKDAY
		   ,CrewID
		   ,CrewCD
		   ,CrewMember
		   ,Date SortDate
		   ,col2=(CASE WHEN SUBSTRING(location,1,3)='(R)' THEN '' ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE '' END) END)
				 +(CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE '' END)
				 +(CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  ' '+'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(CummETE,0)*10)*0.1) ELSE '' END)
		   ,col3=location+(CASE WHEN @IsFlightNo=1 AND location IS NOT NULL AND RecordType='T' THEN ' FlightNo: '+ISNULL(FlightNUM,'') ELSE '' END )
		   ,col4=CASE WHEN @IsDisplayRedEyeFlight=0 THEN '** Arrival Date: 01/24/13 **'+ CONVERT(VARCHAR(10),IsArrival,101) ELSE '' END
		   ,col5=LTRIM(RTRIM(ISNULL(tail_nmbr,'')+' '+ISNULL(crewlist,'')+' '+(CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE '' END)
				 +(CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  ' ('+FlightCatagoryCD+')' ELSE '' END)
				 +(CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Req: '+ISNULL(reqcode,'') ELSE '' END)
				 +(CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T' THEN ' Dept: '+ISNULL(DepartmentCD,'') ELSE '' END)
				 +(CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Auth: '+ISNULL(AuthorizationCD,'') ELSE '' END)
				 +(CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Purpose: '+ISNULL(FlightPurpose,'') ELSE '' END)
				 +(CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN ' Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END )))
		   ------Export------------
		   ,orig_nmbr=TripNUM
		   ,leg_num=LegNum
		   ,legid
		   ,vendcode
		   ,caldate=Date  
		   ,TT.FgColor
		   ,TT.BgColr 
		   ,ncounter=0
		   ,locdep=TT.DepartureDTTMLocal 
		   ,ordernum=RIGHT(RTRIM('0000'+CONVERT(CHAR(5),DENSE_RANK()OVER(PARTITION BY WeekNumber ORDER BY  WeekNumber,CrewCD)+1)),5) 
		   ,flag='A'
		   ,ArrivalDTTMLocal
		   ,SORTORDER
		   ,DepartureDTTMLocal
		   ,TypeCode
		   ,tail_nmbr
	       
		  ,DENSE_RANK()OVER(PARTITION BY TT.CrewCD,WeekNumber ORDER BY TT.CrewCD,WeekNumber,date) DayNo
		--   ,ROW_NUMBER()OVER(PARTITION BY TT.TailNum,WeekNumber ORDER BY TT.TailNum,WeekNumber,date) DayVal
		   ,ROW_NUMBER()OVER(PARTITION BY TT.CrewCD,WeekNumber,date ORDER BY TT.CrewCD,WeekNumber,date) Rnk
		   ,crewname
	  FROM @TempTable TT
	   WHERE DATE BETWEEN @DATEFROM AND @DATETO
		 AND (RnkFirst=1 OR RnkLast=1)

	)TEMP
	)


	SELECT WeekNumber
		  ,display
		  ,col1=CASE WHEN ISNULL(col1,'')<>''  THEN SUBSTRING(col1,1,LEN(col1)-1) END
		  ,dispcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '#FFFFFF' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(dispcolor,'') <> '' THEN SUBSTRING(dispcolor,1,LEN(dispcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0'  ELSE '0' END
		  ,fcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(fcolor,'') <> '' THEN SUBSTRING(fcolor,1,LEN(fcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0' END
		  ,bcolor=CASE WHEN ISNULL(crewcode,'')='' THEN '#FFFFFF' 
					   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(bcolor,'') <> '' THEN SUBSTRING(bcolor,1,LEN(bcolor)-1) 
					   WHEN ISNULL(crewcode,'') <> '' THEN '0@0'  ELSE '0' END
		  ,uline=CASE WHEN ISNULL(crewcode,'')='' THEN 'FALSE' 
					  WHEN ISNULL(tail_nmbr,'') <> ''   THEN SUBSTRING(uline,1,LEN(uline)-1) 
					  WHEN ISNULL(crewcode,'') <> ''  THEN 'TRUE@FALSE'  ELSE 'FALSE' END
		  ,orig_nmbr=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
						   WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(orig_nmbr,'') <> ''  THEN SUBSTRING(orig_nmbr,1,LEN(orig_nmbr)-1) 
						  WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,leg_num=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
						WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(leg_num,'') <> ''  THEN SUBSTRING(leg_num,1,LEN(leg_num)-1) 
						WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,legid=CASE WHEN ISNULL(crewcode,'')='' THEN '0' 
					  WHEN ISNULL(tail_nmbr,'') <> '' OR ISNULL(legid,'') <> ''  THEN SUBSTRING(legid,1,LEN(legid)-1) 
					  WHEN ISNULL(crewcode,'') <> ''  THEN '0@0'  ELSE '0' END
		  ,caldate=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(caldate,1,LEN(caldate)-1) ELSE SUBSTRING(caldate,1,10) END
		  ,type_code=SUBSTRING(type_code,1,LEN(type_code)-1)
		  ,vendcode=SUBSTRING(vendcode,1,LEN(vendcode)-1)
		  ,tail_nmbr=SUBSTRING(tail_nmbr,1,LEN(tail_nmbr)-1)
		  ,ordernum=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(ordernum,1,LEN(ordernum)-1) ELSE ''  END
		  ,locdep=SUBSTRING(locdep,1,LEN(locdep)-1)
		  ,flag=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(flag,1,LEN(flag)-1) ELSE 'A' END
		  ,timetype=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(timetype,1,LEN(timetype)-1) ELSE LEFT(timetype,10) END
		  ,gotofleet=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(gotofleet,1,LEN(gotofleet)-1)  ELSE LEFT(gotofleet,10) END
		  ,ncounter=CASE WHEN ISNULL(crewcode,'')<>'' THEN SUBSTRING(ncounter,1,LEN(ncounter)-1) ELSE '0' END
		  ,ColVal
		  ,ArrivalDTTMLocal
		  ,SORTORDER
		  ,SortDate
		  ,DepartureDTTMLocal
		  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
		  ,DayNo
		  ,crewname=SUBSTRING(crewname,1,LEN(crewname)-1)
		  ,crewcode=SUBSTRING(crewcode,1,LEN(crewcode)-1) FROM(
	SELECT DISTINCT TI.WeekNumber
		  ,display=CrewMember
		  ,col1=TC.col
		  ,dispcolor=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',dispcolor+'@'),'Z',dispcolor+'@')
		  ,fcolor=(CASE WHEN len(TC.fcolor)-len(replace(TC.fcolor,'@',''))=MaxCount THEN TC.fcolor ELSE TC.fcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.fcolor)-len(replace(TC.fcolor,'@','')))),'Z','0@') END)
		  ,bcolor=(CASE WHEN len(TC.bcolor)-len(replace(TC.bcolor,'@',''))=MaxCount THEN TC.bcolor ELSE TC.bcolor+REPLACE(REPLICATE('Z',MaxCount-(len(TC.bcolor)-len(replace(TC.bcolor,'@','')))),'Z','0@') END)
		  ,uline='TRUE@'+SUBSTRING(REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BBB')),1,ISNULL(MaxCount,3)),'B','FALSE'+'@'),'Z','FALSE'+'@'),1,LEN(REPLACE(REPLACE(SUBSTRING((ISNULL(TW.ColVal+TT.ColVal,'BBB')),1,ISNULL(MaxCount,3)),'B','FALSE'+'@'),'Z','FALSE'+'@'))-6)
		  ,orig_nmbr=(CASE WHEN len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@',''))=MaxCount THEN TC.orig_nmbr ELSE TC.orig_nmbr+REPLACE(REPLICATE('Z',MaxCount-(len(TC.orig_nmbr)-len(replace(TC.orig_nmbr,'@','')))),'Z','0@') END)
		  ,leg_num=(CASE WHEN len(TC.legnum)-len(replace(TC.legnum,'@',''))=MaxCount THEN TC.legnum ELSE TC.legnum+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legnum)-len(replace(TC.legnum,'@','')))),'Z','0@') END)
		  ,legid=(CASE WHEN len(TC.legid)-len(replace(TC.legid,'@',''))=MaxCount THEN TC.legid ELSE TC.legid+REPLACE(REPLICATE('Z',MaxCount-(len(TC.legid)-len(replace(TC.legid,'@','')))),'Z','0@') END)
		  ,caldate=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(10),TI.caldate,101)+'@'),'Z',CONVERT(VARCHAR(10),TI.caldate,101)+'@')
		  ,type_code=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',type_code+'@'),'Z',type_code+'@')
		  ,vendcode=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',vendcode+'@'),'Z',vendcode+'@')
		  ,tail_nmbr=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',tail_nmbr+'@'),'Z',tail_nmbr+'@')
		  ,crewcode=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CrewCD+'@'),'Z',CrewCD+'@')	
		  ,ordernum=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',ordernum+'@'),'Z',ordernum+'@')
		  ,locdep=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',LEFT(CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,120),16)+'@'),'Z',LEFT(CONVERT(VARCHAR(30),TM.DepartureDTTMLocal,120),16)+'@')
		  ,flag=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',flag+'@'),'Z',flag+'@')
		  ,timetype=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',timetype+'@'),'Z',timetype+'@')
		  ,gotofleet=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(10),gotofleet,101)+'@'),'Z',CONVERT(VARCHAR(10),gotofleet,101)+'@')
		  ,ncounter=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',CONVERT(VARCHAR(2),ncounter)+'@'),'Z',CONVERT(VARCHAR(2),ncounter)+'@')
		  ,ColVal=SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,MaxCount)
		  ,ArrivalDTTMLocal
		  ,SORTORDER
		  ,SortDate
		  ,TI.DepartureDTTMLocal
		  --,TI.DayVal
		  ,DATEPART(WEEKDAY, SortDate) WEEKDAY
		  ,DayNo
		  ,RnkVal=ROW_NUMBER()OVER(PARTITION BY TI.CrewID,TI.Caldate ORDER BY TI.CrewID,TI.Caldate)
		  ,crewname=REPLACE(REPLACE(SUBSTRING((ISNULL(ISNULL(TW.ColVal,'')+TT.ColVal,'BB')),1,ISNULL(MaxCount,3)),'B',crewname+'@'),'Z',crewname+'@')
	 FROM TripInfo TI
		   LEFT JOIN (SELECT WeekNumber,CrewID,MIN(DepartureDTTMLocal) DepartureDTTMLocal FROM TripInfo TM GROUP BY WeekNumber,CrewID)TM ON TM.WeekNumber=TI.WeekNumber AND TM.CrewID=TI.CrewID
		  LEFT JOIN (SELECT CrewID, REPLICATE('Z',MAX(TT.ColCount))ColVal,WeekNumber,MAX(TT.ColCount) MaxCount FROM 
									 (
									 SELECT CrewID,SUM((len(col)-len(replace(col,'@','')))+1) ColCount,WeekNumber FROM  TripInfo GROUP BY WeekNumber,CrewID,caldate
									 )TT
					GROUP BY WeekNumber,CrewID)TT ON TI.WeekNumber=TT.WeekNumber AND TI.CrewID=TT.CrewID
		 LEFT JOIN (SELECT REPLICATE('B',SUM((len(col)-len(replace(col,'@','')))+1)) ColVal,caldate,WeekNumber ,CrewID FROM  TripInfo GROUP BY WeekNumber,CrewID,caldate
					 )TW ON TI.WeekNumber=TW.WeekNumber AND TW.caldate=TI.caldate AND TW.CrewID=TI.CrewID
		 LEFT JOIN (SELECT T1.CrewID,T1.WeekNumber,T1.caldate,col=(SELECT CASE WHEN ISNULL(col,'')<>'' THEN col+'@' ELSE '' END FROM (SELECT WeekNumber,caldate,col,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,orig_nmbr=(SELECT  CASE WHEN ISNULL(orig_nmbr,'')<>'' THEN (CASE WHEN RIGHT(orig_nmbr,1)='@' THEN orig_nmbr ELSE  orig_nmbr+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),orig_nmbr))orig_nmbr,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,legid=(SELECT  CASE WHEN ISNULL(legid,'')<>'' THEN (CASE WHEN RIGHT(legid,1)='@' THEN legid ELSE  legid+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),legid))legid,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)	
									,legnum=(SELECT  CASE WHEN ISNULL(legnum,'')<>'' THEN (CASE WHEN RIGHT(legnum,1)='@' THEN legnum ELSE  legnum+'@' END) ELSE '' END FROM (SELECT WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',CONVERT(VARCHAR(20),leg_num))legnum,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,fcolor=(SELECT  CASE WHEN ISNULL(fcolor,'')<>'' THEN (CASE WHEN RIGHT(fcolor,1)='@' THEN fcolor ELSE  fcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',fcolor)fcolor,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID  ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
									,bcolor=(SELECT  CASE WHEN ISNULL(bcolor,'')<>'' THEN (CASE WHEN RIGHT(bcolor,1)='@' THEN bcolor ELSE  bcolor+'@' END) ELSE '' END FROM (SELECT  WeekNumber,caldate,REPLACE(REPLICATE('B@',len(col)-len(replace(col,'@',''))+1),'B',bcolor)bcolor,DepartureDTTMLocal,ArrivalDTTMLocal,CrewID FROM  TripInfo) T WHERE T.WeekNumber=T1.WeekNumber AND T.caldate=T1.caldate AND T.CrewID=T1.CrewID ORDER BY caldate,DepartureDTTMLocal,ArrivalDTTMLocal  FOR XML PATH('')
										)
										FROM(SELECT DISTINCT WeekNumber,caldate,CrewID FROM  TripInfo) T1
									

				)TC ON TC.WeekNumber=TI.WeekNumber AND TC.caldate=TI.caldate  AND TC.CrewID=TI.CrewID
	 )WeekFleet
	  WHERE RnkVal=1
	 ORDER BY WeekNumber,display,ISNULL(tail_nmbr,'ZZZZ'),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,orig_nmbr,leg_num,SORTORDER
	 END


	 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
	 DROP TABLE #TempTable
	    

	 
	 
	 END; 


	 --EXEC spGetReportPRESchedueCalendarWeeklyCrewExportInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''

	 
		











GO


