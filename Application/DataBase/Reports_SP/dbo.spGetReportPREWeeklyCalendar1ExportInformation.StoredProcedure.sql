IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendar1ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1ExportInformation]                
  @UserCD VARCHAR(50)-- MANDATORY                
 ,@DATEFROM DATETIME --MANDATORY                
 ,@DATETO DATETIME --MANDATORY                
 ,@TailNum VARCHAR(500) = ''--OPTIONAL                
 ,@FleetGroupCD VARCHAR(500) = ''--OPTIONAL     
 ,@IsPDF BIT=0            
AS                
            
BEGIN   
            
SET NOCOUNT ON     

DECLARE @DayCount INT;

SET @DayCount=( 7-((DATEDIFF(DAY,@DATEFROM,@DATETO)+1)%7)) 

SET @DATETO=(SELECT CASE  WHEN @DayCount=7 THEN @DATETO ELSE @DATETO+@DayCount END )    

-----------------------------TailNum and Fleet Group Filteration----------------------
 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempFleetID TABLE   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(100)
    )
  
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID, F.TailNUM
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.IsDeleted=0
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID, F.TailNUM
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID, F.TailNUM
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END

-----------------------------TailNum and Fleet Group Filteration----------------------


 DECLARE @FromDate DATETIME                
 DECLARE @ToDate DATETIME                
 DECLARE @Count INT                
 DECLARE @index INT                
                  
 SET @FromDate = @DATEFROM                
 SET @ToDate = @DATETO 
                         
 DECLARE @TempTailNum TABLE (                
  ID int identity(1,1) Not Null,                
  TailNum VARCHAR(25)                
 )                 
                 
 CREATE  TABLE #TempTable(          
   ID INT IDENTITY                
  ,WeekNumber INT                
  ,tail_nmbr VARCHAR(100)          
  ,type_code char(4)          
  ,ac_code VARCHAR(10)          
  ,locdep DATETIME          
  ,Date1 DATE          
  ,crewlist1 VARCHAR(40)           
  ,reqcode1 VARCHAR(5)          
  ,orig_nmbr1 BIGINT          
  ,Date2 DATE          
  ,crewlist2 VARCHAR(40)           
  ,reqcode2 VARCHAR(5)          
  ,orig_nmbr2 BIGINT          
  ,Date3 DATE          
  ,crewlist3 VARCHAR(40)           
  ,reqcode3 VARCHAR(5)          
  ,orig_nmbr3 BIGINT          
  ,Date4 DATE          
  ,crewlist4 VARCHAR(40)           
  ,reqcode4 VARCHAR(5)          
  ,orig_nmbr4 BIGINT          
  ,Date5 DATE          
  ,crewlist5 VARCHAR(40)           
  ,reqcode5 VARCHAR(5)          
  ,orig_nmbr5 BIGINT          
  ,Date6 DATE          
  ,crewlist6 VARCHAR(40)           
  ,reqcode6 VARCHAR(5)          
  ,orig_nmbr6 BIGINT          
  ,Date7 DATE          
  ,crewlist7 VARCHAR(40)           
  ,reqcode7 VARCHAR(5)          
  ,orig_nmbr7 BIGINT          
  ,location1  VARCHAR(100)          
  ,arrtime1 VARCHAR(10)          
  ,deptime1 VARCHAR(10)          
  ,arrtimeapprox1 BIT          
  ,deptimeapprox1 BIT          
  ,pax_total1 INT          
  ,location2  VARCHAR(100)           
  ,arrtime2 VARCHAR(10)          
  ,deptime2 VARCHAR(10)          
  ,arrtimeapprox2 BIT          
  ,deptimeapprox2 BIT          
  ,pax_total2 INT          
  ,location3  VARCHAR(100)           
  ,arrtime3 VARCHAR(10)          
  ,deptime3 VARCHAR(10)          
  ,arrtimeapprox3 BIT          
  ,deptimeapprox3 BIT          
  ,pax_total3 INT          
  ,location4  VARCHAR(100)          
  ,arrtime4 VARCHAR(10)          
  ,deptime4 VARCHAR(10)          
  ,arrtimeapprox4 BIT          
  ,deptimeapprox4 BIT          
  ,pax_total4 INT          
  ,location5  VARCHAR(100)          
  ,arrtime5 VARCHAR(10)          
  ,deptime5 VARCHAR(10)          
  ,arrtimeapprox5 BIT          
  ,deptimeapprox5 BIT          
  ,pax_total5 INT          
  ,location6  VARCHAR(100)         
  ,arrtime6 VARCHAR(10)          
  ,deptime6 VARCHAR(10)          
  ,arrtimeapprox6 BIT          
  ,deptimeapprox6 BIT          
  ,pax_total6 INT          
  ,location7  VARCHAR(100)          
  ,arrtime7 VARCHAR(10)          
  ,deptime7 VARCHAR(10)          
  ,arrtimeapprox7 BIT          
  ,deptimeapprox7 BIT          
  ,pax_total7 INT          
  ,calcdate  DATE          
  ,date_1 DATE          
  ,date_2 DATE          
  ,date_3 DATE          
  ,date_4 DATE          
  ,date_5 DATE          
  ,date_6 DATE          
  ,date_7 DATE
  ,airportcode VARCHAR(10)
  ,airportdesc VARCHAR(10)
  ,airportcity VARCHAR(10)
  ,crewcode VARCHAR(10)
  ,crewdesc VARCHAR(10)
  ,authcode VARCHAR(10)
  ,authdesc VARCHAR(10) 
   ,DutyType1 CHAR(2)
  ,DutyType2 CHAR(2)
  ,DutyType3 CHAR(2)
  ,DutyType4 CHAR(2)
  ,DutyType5 CHAR(2)
  ,DutyType6 CHAR(2)
  ,DutyType7 CHAR(2)
  ,TripDesc1 VARCHAR(60)
  ,TripDesc2 VARCHAR(60)
  ,TripDesc3 VARCHAR(60)
  ,TripDesc4 VARCHAR(60)
  ,TripDesc5 VARCHAR(60)
  ,TripDesc6 VARCHAR(60)
  ,TripDesc7 VARCHAR(60)
  ,IsNextArrival1 DATE
  ,IsNextArrival2 DATE
  ,IsNextArrival3 DATE
  ,IsNextArrival4 DATE
  ,IsNextArrival5 DATE
  ,IsNextArrival6 DATE
  ,IsNextArrival7 DATE
  ,DepTimeZulu1 DATETIME
  ,DepTimeZulu2 DATETIME
  ,DepTimeZulu3 DATETIME
  ,DepTimeZulu4 DATETIME
  ,DepTimeZulu5 DATETIME
  ,DepTimeZulu6 DATETIME
  ,DepTimeZulu7 DATETIME
  ,ArrTimeZulu1 DATETIME
  ,ArrTimeZulu2 DATETIME
  ,ArrTimeZulu3 DATETIME
  ,ArrTimeZulu4 DATETIME
  ,ArrTimeZulu5 DATETIME
  ,ArrTimeZulu6 DATETIME
  ,ArrTimeZulu7 DATETIME
  )       
  
  CREATE  TABLE #TempTail(          
   ID INT                
  ,WeekNumber INT                
  ,tail_nmbr VARCHAR(100)          
  ,type_code char(4)          
  ,ac_code VARCHAR(10)          
  ,locdep DATETIME          
  ,Date1 DATE          
  ,crewlist1 VARCHAR(40)           
  ,reqcode1 VARCHAR(5)          
  ,orig_nmbr1 BIGINT          
  ,Date2 DATE          
  ,crewlist2 VARCHAR(40)           
  ,reqcode2 VARCHAR(5)          
  ,orig_nmbr2 BIGINT          
  ,Date3 DATE          
  ,crewlist3 VARCHAR(40)           
  ,reqcode3 VARCHAR(5)          
  ,orig_nmbr3 BIGINT          
  ,Date4 DATE          
  ,crewlist4 VARCHAR(40)           
  ,reqcode4 VARCHAR(5)          
  ,orig_nmbr4 BIGINT          
  ,Date5 DATE          
  ,crewlist5 VARCHAR(40)           
  ,reqcode5 VARCHAR(5)          
  ,orig_nmbr5 BIGINT          
  ,Date6 DATE          
  ,crewlist6 VARCHAR(40)           
  ,reqcode6 VARCHAR(5)          
  ,orig_nmbr6 BIGINT          
  ,Date7 DATE          
  ,crewlist7 VARCHAR(40)           
  ,reqcode7 VARCHAR(5)          
  ,orig_nmbr7 BIGINT          
  ,location1  VARCHAR(100)          
  ,arrtime1 VARCHAR(10)          
  ,deptime1 VARCHAR(10)          
  ,arrtimeapprox1 BIT          
  ,deptimeapprox1 BIT          
  ,pax_total1 INT          
  ,location2  VARCHAR(100)           
  ,arrtime2 VARCHAR(10)          
  ,deptime2 VARCHAR(10)          
  ,arrtimeapprox2 BIT          
  ,deptimeapprox2 BIT          
  ,pax_total2 INT          
  ,location3  VARCHAR(100)           
  ,arrtime3 VARCHAR(10)          
  ,deptime3 VARCHAR(10)          
  ,arrtimeapprox3 BIT          
  ,deptimeapprox3 BIT          
  ,pax_total3 INT          
  ,location4  VARCHAR(100)          
  ,arrtime4 VARCHAR(10)          
  ,deptime4 VARCHAR(10)          
  ,arrtimeapprox4 BIT          
  ,deptimeapprox4 BIT          
  ,pax_total4 INT          
  ,location5  VARCHAR(100)          
  ,arrtime5 VARCHAR(10)          
  ,deptime5 VARCHAR(10)          
  ,arrtimeapprox5 BIT          
  ,deptimeapprox5 BIT          
  ,pax_total5 INT          
  ,location6  VARCHAR(100)         
  ,arrtime6 VARCHAR(10)          
  ,deptime6 VARCHAR(10)          
  ,arrtimeapprox6 BIT          
  ,deptimeapprox6 BIT          
  ,pax_total6 INT          
  ,location7  VARCHAR(100)          
  ,arrtime7 VARCHAR(10)          
  ,deptime7 VARCHAR(10)          
  ,arrtimeapprox7 BIT          
  ,deptimeapprox7 BIT          
  ,pax_total7 INT          
  ,calcdate  DATE          
  ,date_1 DATE          
  ,date_2 DATE          
  ,date_3 DATE          
  ,date_4 DATE          
  ,date_5 DATE          
  ,date_6 DATE          
  ,date_7 DATE
  --,airportcode VARCHAR(10)
  --,airportdesc VARCHAR(10)
  --,airportcity VARCHAR(10)
  --,crewcode VARCHAR(10)
  --,crewdesc VARCHAR(10)
  --,authcode VARCHAR(10)
  --,authdesc VARCHAR(10) 
  ,Rnk INT
  ,DutyType1 CHAR(2)
  ,DutyType2 CHAR(2)
  ,DutyType3 CHAR(2)
  ,DutyType4 CHAR(2)
  ,DutyType5 CHAR(2)
  ,DutyType6 CHAR(2)
  ,DutyType7 CHAR(2)
  ,TripDesc1 VARCHAR(60)
  ,TripDesc2 VARCHAR(60)
  ,TripDesc3 VARCHAR(60)
  ,TripDesc4 VARCHAR(60)
  ,TripDesc5 VARCHAR(60)
  ,TripDesc6 VARCHAR(60)
  ,TripDesc7 VARCHAR(60)
 ,IsNextArrival1 DATE
,IsNextArrival2 DATE
,IsNextArrival3 DATE
,IsNextArrival4 DATE
,IsNextArrival5 DATE
,IsNextArrival6 DATE
,IsNextArrival7 DATE
,DepTimeZulu1 DATETIME
  ,DepTimeZulu2 DATETIME
  ,DepTimeZulu3 DATETIME
  ,DepTimeZulu4 DATETIME
  ,DepTimeZulu5 DATETIME
  ,DepTimeZulu6 DATETIME
  ,DepTimeZulu7 DATETIME
  ,ArrTimeZulu1 DATETIME
  ,ArrTimeZulu2 DATETIME
  ,ArrTimeZulu3 DATETIME
  ,ArrTimeZulu4 DATETIME
  ,ArrTimeZulu5 DATETIME
  ,ArrTimeZulu6 DATETIME
  ,ArrTimeZulu7 DATETIME
  )  
          
DECLARE   @PrevId INT, @PrevDept CHAR(4);              
           
DECLARE @tail_nmbr CHAR(6),@type_code CHAR(30),@ac_code VARCHAR(10),@locdep  DATETIME,          
  @locarr DATETIME,@Crewlist VARCHAR(40), @reqcode VARCHAR(5),@orig_nmbr BIGINT,          
  @location CHAR(4),@locationArr CHAR(4),@arrtime VARCHAR(10),@deptime VARCHAR(10),@isapproxtm BIT,          
  @pax_total INT ,@IsNextArrival DATE     
             
 --DECLARE @DateVal DATE;   
 DECLARE @SQLSCRIPT NVARCHAR(MAX);  
 DECLARE @ParameterDefinition AS NVARCHAR(200);             
 DECLARE @ArrVal DATE;              
 DECLARE @calcdate DATE;      
 DECLARE @locationArrVal1 CHAR(4),@TailNumVal1 CHAR(6),@ID0 INT=NULL,@ID1 INT=NULL;      
 DECLARE @locationArrVal2 CHAR(4),@TailNumVal2 CHAR(6),@ID2 INT=NULL;       
 DECLARE @locationArrVal3 CHAR(4),@TailNumVal3 CHAR(6),@ID3 INT=NULL;       
 DECLARE @locationArrVal4 CHAR(4),@TailNumVal4 CHAR(6),@ID4 INT=NULL;       
 DECLARE @locationArrVal5 CHAR(4),@TailNumVal5 CHAR(6),@ID5 INT=NULL;       
 DECLARE @locationArrVal6 CHAR(4),@TailNumVal6 CHAR(6),@ID6 INT=NULL;       
 DECLARE @locationArrVal7 CHAR(4),@TailNumVal7 CHAR(6),@ID7 INT=NULL;             
 DECLARE @WEEK INT,@LegID INT,@SORTORDER INT;            
 DECLARE @date_1 VARCHAR(10),@date_2 VARCHAR(10),@date_3 VARCHAR(10),@date_4 VARCHAR(10),          
 @date_5 VARCHAR(10),@date_6 VARCHAR(10),@date_7 VARCHAR(10);   
 DECLARE @DATE1 DATE,@DATE2 DATE,@DATE3 DATE,@DATE4 DATE,@DATE5 DATE,@DATE6 DATE,@DATE7 DATE,@PrevOrigin BIGINT       
 
 -----------------
DECLARE @DepTimeZulu DATETIME,@ArrTimeZulu DATETIME;
-----------------
DECLARE @CUSTOMER_ID BIGINT=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
DECLARE @CurDate TABLE(DateWeek DATE,TailNum VARCHAR(9),FleetID BIGINT)
DECLARE @RMTable TABLE(crewlist VARCHAR(40),
                       reqcode VARCHAR(5),
                       RMDate DATE,
                       TripNum BIGINT,
                       TailNum VARCHAR(9),
                       FleetID BIGINT) 
                       
DECLARE @TableTrip TABLE(TailNum VARCHAR(9),
                        ArrivalDTTMLocal DATETIME,
                        NextLocalDTTM DATETIME,
                        TripNum BIGINT,
                        FleetID BIGINT,
                        crewlist VARCHAR(400),
                        reqcode VARCHAR(5),
                        LegNum BIGINT,
                        type_code CHAR(4),
                        ac_code VARCHAR(10),
                        location CHAR(4),
                        DutyType CHAR(2),
                        TripDesc VARCHAR(40))  

INSERT INTO @CurDate(TailNum,DateWeek,FleetID)
SELECT F.TailNum,T.*,PM.FleetID from PreflightMain PM INNER JOIN PreflightLeg PL  ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                            INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                                            INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID
 CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@DATEFROM,@DATETO)) T
         WHERE PM.CustomerID=@CUSTOMER_ID
          AND (	
					(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO)))
       AND PM.TripStatus IN ('T','H')

INSERT INTO @TableTrip
SELECT DISTINCT F.TailNum,CASE WHEN PM.RecordType='M' THEN PL.DepartureDTTMLocal ELSE PL.ArrivalDTTMLocal END,
                          CASE WHEN PM.RecordType='M' THEN PL.ArrivalDTTMLocal ELSE PL.NextLocalDTTM END,PM.TripNUM,PM.FleetID,CrewList,P.PassengerRequestorCD,PL.LegNUM,F.AircraftCD,
                          AT.AircraftCD,A.IcaoID,PL.DutyTYPE,PM.TripDescription
                          FROM PreflightMain PM 
							INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
							INNER JOIN Fleet F ON F.FleetID=PM.FleetID  
							INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID     
							LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
							LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID=PL.LegID 
							LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID   
							LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
							LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID         
							LEFT OUTER JOIN (        
											 SELECT DISTINCT PC2.LegID, CrewList = (        
											 SELECT C.CrewCD + ' '        
											 FROM PreflightCrewList PC1        
											 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
											 WHERE PC1.LegID = PC2.LegID   
											 ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                	WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)     
											 FOR XML PATH('')        
																				 ) FROM PreflightCrewList PC2         
											  ) Crew ON PL.LegID = Crew.LegID
													
												--- OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.ArrivalDTTMLocal, F.FleetID) AS A  
         WHERE PM.CustomerID=@CUSTOMER_ID
          AND (	
					(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO)))
        AND PM.RecordType ='M'
        AND PM.IsDeleted = 0

INSERT INTO @TableTrip
SELECT DISTINCT F.TailNum,CASE WHEN PM.RecordType='M' THEN PL.DepartureDTTMLocal ELSE PL.ArrivalDTTMLocal END,
                          CASE WHEN PM.RecordType='M' THEN PL.ArrivalDTTMLocal ELSE PL.NextLocalDTTM END,PM.TripNUM,PM.FleetID,CrewList,P.PassengerRequestorCD,PL.LegNUM,F.AircraftCD,
                          AT.AircraftCD,A.IcaoID,PL.DutyTYPE,PM.TripDescription
                          FROM PreflightMain PM 
							INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
							INNER JOIN Fleet F ON F.FleetID=PM.FleetID  
							INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID     
							LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
							LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID=PL.LegID 
							LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID   
							LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
							LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID         
							LEFT OUTER JOIN (        
											SELECT DISTINCT PC2.LegID, CrewList = (        
											 SELECT C.CrewCD + ' '        
											 FROM PreflightCrewList PC1        
											 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
											 WHERE PC1.LegID = PC2.LegID   
											 ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                	WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)     
											 FOR XML PATH('')        
																				 ) FROM PreflightCrewList PC2         
											  ) Crew ON PL.LegID = Crew.LegID
													
												--- OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.ArrivalDTTMLocal, F.FleetID) AS A  
         WHERE PM.CustomerID=@CUSTOMER_ID
          AND (	
					(CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.NextLocalDTTM) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.NextLocalDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.NextLocalDTTM) <= CONVERT(DATE,@DATETO))
			 )	
       AND PM.TripStatus IN ('T','H')
        AND PM.RecordType='T'
        AND PM.IsDeleted = 0
 ----------------
 DECLARE @RestMaintTable TABLE(TailNum VARCHAR(9),
                         type_code CHAR(4),
                         ac_code VARCHAR(10),
                         Departure DATETIME,
                         ArrivalDttmLocal DATETIME,
                         crewlist VARCHAR(400),
                         reqcode VARCHAR(5),
                         TripNum BIGINT,
                         location CHAR(4),
                         Deptime CHAR(4),
                         ArrTime CHAR(4),
                         IsApproxTM BIT,
                         Pax INT,
                         Sort_Order INT,
                         LegNum BIGINT,
                         DA_Type CHAR(1),
                         TripDesc VARCHAR(40),
                         DutyType CHAR(2))
 
 INSERT INTO @RestMaintTable
 SELECT DISTINCT PM.TailNum,PM.type_code,PM.ac_code,
                  CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,PM.ArrivalDTTMLocal) THEN PM.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00')) END,
                 CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,PM.NextLocalDTTM) THEN PM.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,
                PM.crewlist,PM.reqcode,PM.TripNum,PM.location,'','  ',NULL,0,1,PM.LegNum,'D',PM.TripDesc,PM.DutyType
                                                 FROM @TableTrip PM
                                                       INNER JOIN @CurDate CD ON CD.FleetID=PM.FleetID
         WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,PM.ArrivalDTTMLocal)
           AND CONVERT(DATE,CD.DateWeek) <= CONVERT(DATE,PM.NextLocalDTTM)
           AND DutyType='MX'

INSERT INTO @RestMaintTable
 SELECT DISTINCT PM.TailNum,PM.type_code,PM.ac_code,DateWeek,NextLocalDTTM ,
                PM.crewlist,PM.reqcode,PM.TripNum,PM.location,'','  ',NULL,0,1,PM.LegNum,'D',PM.TripDesc,'R'
                                                 FROM @CurDate CD LEFT JOIN @TableTrip PM
                                                         ON CD.FleetID=PM.FleetID
         WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,PM.ArrivalDTTMLocal)
           AND CONVERT(DATE,CD.DateWeek) < CONVERT(DATE,PM.NextLocalDTTM)
           AND ISNULL(DutyType,'') <>'MX'

 DECLARE @IDA INT,@CrTripDescription VARCHAR(40),@CrDutyType CHAR(2);
 --  
 DECLARE curTailInfo CURSOR FAST_FORWARD READ_ONLY FOR              
 SELECT A.* FROM(SELECT DISTINCT tail_nmbr = F.TailNum           
    ,type_code=F.AircraftCD           
        ,ac_code=A1.AircraftCD          
        ,locdep=PL.DepartureDTTMLocal            
        ,locarr=PL.ArrivalDTTMLocal          
        ,Crewlist = Crew.CrewList --PL.AdditionalCrew           
        ,reqcode=P.PassengerRequestorCD          
        ,orig_nmbr=PM.TripNUM          
        ,location = A.IcaoID  
        ,locationArr=''                    
        ,deptime=SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5)  
        ,arrtime=''        
        ,isapproxtm=ISNULL(PL.IsApproxTM, 0)    
        ,pax_total=PL.PassengerTotal
        ,1 AS SORTORDER
        ,PL.Legnum   Legnum  
        ,PM.TripDescription TripDescription 
        ,PL.DutyType DutyTYPE
        ,CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) < CONVERT(DATE,PL.ArrivalDTTMLocal) THEN CONVERT(DATE,PL.ArrivalDTTMLocal)  ELSE NULL END    IsArrival              
        ,PL.DepartureGreenwichDTTM
        ,PL.ArrivalGreenwichDTTM
  FROM    PreflightMain PM              
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0             
    LEFT OUTER JOIN Airport A ON A.AirportID = PL.DepartICAOID   
     LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID                               
    INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
	INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID
    LEFT OUTER JOIN Aircraft A1 ON A1.AircraftID=PM.AircraftID           
    LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID              
    LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID            
	LEFT OUTER JOIN (        
		SELECT DISTINCT PC2.LegID, CrewList = (        
		 SELECT C.CrewCD + ' '        
		 FROM PreflightCrewList PC1        
		 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
		 WHERE PC1.LegID = PC2.LegID   
		  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
				                                                	WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
		 FOR XML PATH('')      
		 )                 
		FROM PreflightCrewList PC2         
	  ) Crew ON PL.LegID = Crew.LegID         
 LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID        
 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))                 
 AND CONVERT(DATE,PL.DepartureDTTMLocal) between CONVERT(DATE,@DATEFROM) And CONVERT(DATE,@DATETO)
 AND PM.TripStatus IN ('T','H')
  AND PM.RecordType='T'
  --and Legnum=1
  AND PM.ISDELETED = 0
  
UNION ALL

 SELECT DISTINCT tail_nmbr = F.TailNum           
    ,type_code=F.AircraftCD           
        ,ac_code=A1.AircraftCD          
        ,locdep=PL.DepartureDTTMLocal            
        ,locarr=PL.ArrivalDTTMLocal          
        ,Crewlist = Crew.CrewList --PL.AdditionalCrew           
        ,reqcode=P.PassengerRequestorCD          
        ,orig_nmbr=PM.TripNUM  
        ,location=''            
        ,locationArr=AA.IcaoID  
        ,deptime=''         
        ,arrtime=SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalDTTMLocal,113),13,5)                  
        ,isapproxtm=ISNULL(PL.IsApproxTM, 0)    
        ,pax_total=PL.PassengerTotal 
         ,2 AS SORTORDER,
         PL.Legnum   Legnum 
        ,PM.TripDescription TripDescription 
        ,PL.DutyType DutyTYPE    
        ,CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) < CONVERT(DATE,PL.ArrivalDTTMLocal) THEN CONVERT(DATE,PL.ArrivalDTTMLocal) ELSE NULL END    IsArrival             
        ,PL.DepartureGreenwichDTTM
        ,PL.ArrivalGreenwichDTTM 
  FROM    PreflightMain PM              
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0                        
    LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID                 
    INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID 
	INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID 
    LEFT OUTER JOIN Aircraft A1 ON A1.AircraftID=PM.AircraftID           
    LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID              
    LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID            
	LEFT OUTER JOIN (        
		SELECT DISTINCT PC2.LegID, CrewList = (        
		 SELECT C.CrewCD + ' '        
		 FROM PreflightCrewList PC1        
		 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
		 WHERE PC1.LegID = PC2.LegID  
		  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                	WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)        
		 FOR XML PATH('')        
		 )                 
		FROM PreflightCrewList PC2         
	  ) Crew ON PL.LegID = Crew.LegID         
 LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID        
 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))                 
 AND CONVERT(DATE,PL.DepartureDTTMLocal) between CONVERT(DATE,@DATEFROM) And CONVERT(DATE,@DATETO)
  AND PM.TripStatus IN ('T','H')
  AND PM.RecordType='T'
  AND PM.ISDELETED = 0
 UNION  ALL
SELECT T.TailNum,T.type_code,T.ac_code,T.Departure,NULL Arrival,T.crewlist,T.reqcode,T.TripNum,T.location LocDep,'' LocArr,
        CASE WHEN DutyType='MX'THEN  SUBSTRING (CONVERT(VARCHAR(30),T.Departure,113),13,5) ELSE '0000' END Deptime,
       CASE WHEN DutyType='MX'THEN  SUBSTRING (CONVERT(VARCHAR(30),T.ArrivalDttmLocal,113),13,5) ELSE '0000' END ArrTime,
       T.IsApproxTM,T.Pax,1 Sort_Order,T.LegNum,CASE WHEN DutyType='MX'THEN  T.TripDesc ELSE '' END,T.DutyType
       ,NULL   IsArrival,NULL,NULL  FROM @RestMaintTable T
       
 )A 
 WHERE CONVERT(DATE,LOCDEP) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
   ORDER BY type_code,orig_nmbr,A.locdep,Legnum,A.SORTORDER
  --A.tail_nmbr,A.locdep,A.Legnum,A.SORTORDER  
       
  DECLARE @PrevDate DATE,@PrevTail VARCHAR(25),@ID INT=NULL;        
 OPEN curTailInfo;                          
   FETCH NEXT FROM curTailInfo INTO @tail_nmbr,@type_code,@ac_code,@locdep,@locarr,@Crewlist,@reqcode,    
 @orig_nmbr,@location,@locationArr,@deptime,@arrtime,@isapproxtm,@pax_total,@SORTORDER,@LegID,@CrTripDescription,@CrDutyType,@IsNextArrival,@DepTimeZulu,@ArrTimeZulu;            
  WHILE @@FETCH_STATUS = 0              
  BEGIN    
         
  SET @ID0=(SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK)
 --SELECT @orig_nmbr=CASE WHEN @CrDutyType='MX' THEN 0 ELSE @orig_nmbr END

 IF @deptime <>''
 BEGIN

IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=0)      
 BEGIN 
   
     SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
     SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep )
   IF @ID0 IS NULL   
       BEGIN        
			--INSERT FOR Departure 
			   INSERT INTO #TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
			                          location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,pax_total1,date_1,DutyType1,TripDesc1,IsNextArrival1,DepTimeZulu1)              
			   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END , CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
			                           CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)
       END      
    ELSE IF  (@tail_nmbr=@PrevTail  AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime1 IS NULL AND orig_nmbr1=@orig_nmbr   ) IS NOT NULL)
      BEGIN   
         SET @ID1=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime1 IS NULL AND orig_nmbr1=@orig_nmbr ),@ID)
    -- Curr Tail and Prev tail also should match for this update      
		 UPDATE #TempTable SET  Date1=@locdep,date_1=@locdep,Crewlist1=@Crewlist, reqcode1=@reqcode,orig_nmbr1=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,
		                    location1=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
								 deptime1=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,
								 DepTimeZulu1=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END,
								 arrtimeapprox1=0,deptimeapprox1=@isapproxtm,pax_total1=@pax_total,DutyType1=@CrDutyType,TripDesc1=@CrTripDescription,IsNextArrival1=@IsNextArrival      
						WHERE WeekNumber=@WEEK AND ID=@ID1 AND tail_nmbr=@tail_nmbr AND orig_nmbr1=@orig_nmbr 
    IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime1= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID1 AND tail_nmbr=@tail_nmbr  END       
      END  
      ELSE IF  (@tail_nmbr=@PrevTail   AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location1 IS NULL ) IS NOT NULL)
      BEGIN   
         SET @ID1=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND deptime1 IS NULL  AND tail_nmbr=@tail_nmbr AND location1 is NULL),@ID)
    -- Curr Tail and Prev tail also should match for this update      
		 UPDATE #TempTable SET  Date1=@locdep,date_1=@locdep,Crewlist1=@Crewlist, reqcode1=@reqcode,orig_nmbr1=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location1=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
								 deptime1=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox1=0,deptimeapprox1=@isapproxtm,pax_total1=@pax_total,DutyType1=@CrDutyType,TripDesc1=@CrTripDescription,IsNextArrival1=@IsNextArrival ,
								 DepTimeZulu1=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END      
						WHERE WeekNumber=@WEEK AND ID=@ID1 AND tail_nmbr=@tail_nmbr  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime1= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID1 AND tail_nmbr=@tail_nmbr  END   
       END          
  --  ----
  ELSE            
        
    BEGIN   


   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
                             location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,pax_total1,calcdate,date_1,DutyType1,TripDesc1,IsNextArrival1,DepTimeZulu1)              
     VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
                            CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
     
  
    END 
    SET @DATE1=CONVERT(DATE,@locdep)     
  END         

-------------------Departure------------------
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=1)               
 BEGIN
           
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_2=@locdep ) 

  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
                         location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,date_2,DutyType2,TripDesc2,IsNextArrival2,DepTimeZulu2)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
           CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
  
     END      
        

    
      ELSE  IF  (@tail_nmbr=@PrevTail   AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime2 IS NULL AND orig_nmbr2=@orig_nmbr ) IS NOT NULL)      
        
    BEGIN    
 
      SET @ID2=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime2 IS NULL AND orig_nmbr2=@orig_nmbr ),@ID) 
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date2=@locdep,date_2=@locdep,Crewlist2=@Crewlist, reqcode2=@reqcode,orig_nmbr2=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location2=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,        
                             deptime2=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox2=0,deptimeapprox2=@isapproxtm,pax_total2=@pax_total ,DutyType2=@CrDutyType,TripDesc2=@CrTripDescription,IsNextArrival2=@IsNextArrival       
                             ,DepTimeZulu2=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                       WHERE WeekNumber=@WEEK AND ID=@ID2 AND tail_nmbr=@tail_nmbr  AND orig_nmbr2=@orig_nmbr  
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime2= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID2 AND tail_nmbr=@tail_nmbr  END   
    END 
    
      ELSE  IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location2 IS NULL ) IS NOT NULL)      
        
     BEGIN   
      
      SET @ID2=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND deptime2 IS NULL  AND location2 is NULL AND tail_nmbr=@tail_nmbr),@ID) 
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date2=@locdep,date_2=@locdep,Crewlist2=@Crewlist, reqcode2=@reqcode,orig_nmbr2=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location2=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime2=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox2=0,deptimeapprox2=@isapproxtm,pax_total2=@pax_total ,DutyType2=@CrDutyType,TripDesc2=@CrTripDescription ,IsNextArrival2=@IsNextArrival       
                             ,DepTimeZulu2=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                       WHERE WeekNumber=@WEEK AND ID=@ID2 AND tail_nmbr=@tail_nmbr  
  
  IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime2= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID2 AND tail_nmbr=@tail_nmbr  END   
     END     
    ----
     ELSE            
        
    BEGIN    
       
   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
                            location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,calcdate,date_2,DutyType2,TripDesc2,IsNextArrival2,DepTimeZulu2)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
           CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
    
  
    END  
    SET @DATE2=CONVERT(DATE,@locdep)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=2)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
                           location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,date_3,DutyType3,TripDesc3,IsNextArrival3,DepTimeZulu3)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
            CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu) 
  

       
      
     END      
        
  
      ELSE IF   (@tail_nmbr=@PrevTail   AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr   AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime3 IS NULL AND orig_nmbr3=@orig_nmbr ) IS NOT NULL)     
        
    BEGIN    
      SET @ID3=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND deptime3 IS NULL AND orig_nmbr3=@orig_nmbr ),@ID)
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date3=@locdep,date_3=@locdep,Crewlist3=@Crewlist, reqcode3=@reqcode,orig_nmbr3=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location3=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime3=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox3=0,deptimeapprox3=@isapproxtm,pax_total3=@pax_total,DutyType3=@CrDutyType,TripDesc3=@CrTripDescription,IsNextArrival3=@IsNextArrival        
                             ,DepTimeZulu3=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                      WHERE WeekNumber=@WEEK AND ID=@ID3 AND tail_nmbr=@tail_nmbr  AND orig_nmbr3=@orig_nmbr  
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime3= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID3 AND tail_nmbr=@tail_nmbr  END   
    END  
    
      ELSE IF   (@tail_nmbr=@PrevTail  AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location3 is NULL ) IS NOT NULL)     
        
    BEGIN    
      SET @ID3=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime3 IS NULL  AND location3 is NULL ),@ID)
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date3=@locdep,date_3=@locdep,Crewlist3=@Crewlist, reqcode3=@reqcode,orig_nmbr3=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location3=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime3=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox3=0,deptimeapprox3=@isapproxtm,pax_total3=@pax_total,DutyType3=@CrDutyType,TripDesc3=@CrTripDescription,IsNextArrival3=@IsNextArrival         
                             ,DepTimeZulu3=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                      WHERE WeekNumber=@WEEK AND ID=@ID3 AND tail_nmbr=@tail_nmbr   
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime3= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID3 AND tail_nmbr=@tail_nmbr  END   
    END     
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
                            location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,calcdate,date_3,DutyType3,TripDesc3,IsNextArrival3,DepTimeZulu3)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
    CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  

  
    END  
    SET @DATE3=CONVERT(DATE,@locdep)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=3)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 

    
  IF @ID0 IS NULL            
     BEGIN        


   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
                          location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,date_4,DutyType4,TripDesc4,IsNextArrival4,DepTimeZulu4)              
                 VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
                        CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)
  
      
     END      
    
      ELSE   IF  (@tail_nmbr=@PrevTail  AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr  AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime4 IS NULL AND orig_nmbr4=@orig_nmbr ) IS NOT NULL)       
        
    BEGIN
  
     
    SET @ID4=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND deptime4 IS NULL AND orig_nmbr4=@orig_nmbr ),@ID)


    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date4=@locdep,date_4=@locdep,Crewlist4=@Crewlist, reqcode4=@reqcode,orig_nmbr4=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location4=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime4=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox4=0,deptimeapprox4=@isapproxtm,pax_total4=@pax_total,DutyType4=@CrDutyType,TripDesc4=@CrTripDescription ,IsNextArrival4=@IsNextArrival       
                             ,DepTimeZulu4=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                       WHERE WeekNumber=@WEEK AND ID=@ID4 and tail_nmbr=@tail_nmbr AND orig_nmbr4=@orig_nmbr    
  
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime4= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID4 AND tail_nmbr=@tail_nmbr  END   
    END  
      ELSE   IF  (@tail_nmbr=@PrevTail   AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location4 is NULL) IS NOT NULL)       
        
    BEGIN    
     
    SET @ID4=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime4 IS NULL  AND location4 is NULL ),@ID)
     

    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date4=@locdep,date_4=@locdep,Crewlist4=@Crewlist, reqcode4=@reqcode,orig_nmbr4=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location4=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime4=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox4=0,deptimeapprox4=@isapproxtm,pax_total4=@pax_total,DutyType4=@CrDutyType,TripDesc4=@CrTripDescription ,IsNextArrival4=@IsNextArrival       
                             ,DepTimeZulu4=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                       WHERE WeekNumber=@WEEK AND ID=@ID4 AND tail_nmbr=@tail_nmbr  
  
  
  IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime4= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID4 AND tail_nmbr=@tail_nmbr  END  
    END  
    
      ELSE
       BEGIN
        INSERT INTO #TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
                          location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,date_4,DutyType4,TripDesc4,IsNextArrival4,DepTimeZulu4)              
                 VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
                        CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)
       
       END
      
  END
 
 --
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=4)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,date_5,DutyType5,TripDesc5,IsNextArrival5,DepTimeZulu5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
  CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
     END      
       
          
   ELSE IF   (@tail_nmbr=@PrevTail  AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr  AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime5 IS NULL AND orig_nmbr5=@orig_nmbr ) IS NOT NULL)       
        
    BEGIN    
     
     SET @ID5=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime5 IS NULL AND orig_nmbr5=@orig_nmbr ),@ID)
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date5=@locdep,date_5=@locdep,Crewlist5=@Crewlist, reqcode5=@reqcode,orig_nmbr5=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location5=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime5=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox5=0,deptimeapprox5=@isapproxtm,pax_total5=@pax_total,DutyType5=@CrDutyType,TripDesc5=@CrTripDescription ,IsNextArrival5=@IsNextArrival      
                             ,DepTimeZulu5=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID5  AND orig_nmbr5=@orig_nmbr    
                    
  
    IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime5= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID5 AND tail_nmbr=@tail_nmbr  END  
    END  
       ELSE IF   (@tail_nmbr=@PrevTail   AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location5 is NULL) IS NOT NULL)       
        
    BEGIN    
     
     SET @ID5=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime5 IS NULL  AND location5 is NULL ),@ID)
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date5=@locdep,date_5=@locdep,Crewlist5=@Crewlist, reqcode5=@reqcode,orig_nmbr5=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location5=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime5=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox5=0,deptimeapprox5=@isapproxtm,pax_total5=@pax_total,DutyType5=@CrDutyType,TripDesc5=@CrTripDescription ,IsNextArrival5=@IsNextArrival        
                             ,DepTimeZulu5=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID5  AND tail_nmbr=@tail_nmbr 
                    
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime5= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID5 AND tail_nmbr=@tail_nmbr  END  
    END    
      
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
    location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,calcdate,date_5,DutyType5,TripDesc5,IsNextArrival5,DepTimeZulu5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
    CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  

  
    END 
    SET @DATE5=CONVERT(DATE,@locdep)     
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=5)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
     
  IF @ID0 IS NULL            
     BEGIN  
              

   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,date_6,DutyType6,TripDesc6,IsNextArrival6,DepTimeZulu6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
  CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
      
     END      
        
    
    ELSE IF   (@tail_nmbr=@PrevTail  AND @PrevDept=@location AND @PrevOrigin=@orig_nmbr  AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime6 IS NULL AND orig_nmbr6=@orig_nmbr ) IS NOT NULL)       
        
    BEGIN 
     
     SET @ID6=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime6 IS NULL  AND orig_nmbr6=@orig_nmbr ),@ID)  
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date6=@locdep,date_6=@locdep,Crewlist6=@Crewlist, reqcode6=@reqcode,orig_nmbr6=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location6=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime6=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox6=0,deptimeapprox6=@isapproxtm,pax_total6=@pax_total,DutyType6=@CrDutyType,TripDesc6=@CrTripDescription,IsNextArrival6=@IsNextArrival       
                            ,DepTimeZulu6=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID6  AND tail_nmbr=@tail_nmbr  AND orig_nmbr6=@orig_nmbr 
                    
  IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime6= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID6 AND tail_nmbr=@tail_nmbr  END  
  
    END  
   ELSE IF   (@tail_nmbr=@PrevTail   AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location6 is NULL) IS NOT NULL)       
        
    BEGIN  

     SET @ID6=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime6 IS NULL  AND location6 is NULL ),@ID)  

    -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date6=@locdep,date_6=@locdep,Crewlist6=@Crewlist, reqcode6=@reqcode,orig_nmbr6=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location6=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime6=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox6=0,deptimeapprox6=@isapproxtm,pax_total6=@pax_total ,DutyType6=@CrDutyType,TripDesc6=@CrTripDescription,IsNextArrival6=@IsNextArrival         
                             ,DepTimeZulu6=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID6  AND tail_nmbr=@tail_nmbr
                    
  
    IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime6= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID6 AND tail_nmbr=@tail_nmbr  END 
    END   
   
    ----
     ELSE            
        
    BEGIN
 
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
    location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,calcdate,date_6,DutyType6,TripDesc6,IsNextArrival6,DepTimeZulu6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
    CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
    
  
    END  
    SET @DATE6=CONVERT(DATE,@locdep)    
 END
 
 
 ELSE-- IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=6)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO #TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,date_7,DutyType7,TripDesc7,IsNextArrival7,DepTimeZulu7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,    
  CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@deptime)  
      
     END      
         
    
     ELSE IF   (@tail_nmbr=@PrevTail  AND @PrevDept=@location  AND @PrevOrigin=@orig_nmbr AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime7 IS NULL AND orig_nmbr7=@orig_nmbr ) IS NOT NULL)       
        
    BEGIN    
     
     SET @ID7=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime7 IS NULL AND orig_nmbr7=@orig_nmbr),@ID)  
      
   -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date7=@locdep,date_7=@locdep,Crewlist7=@Crewlist, reqcode7=@reqcode,orig_nmbr7=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location7=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime7=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox7=0,deptimeapprox7=@isapproxtm,pax_total7=@pax_total,DutyType7=@CrDutyType,TripDesc7=@CrTripDescription,IsNextArrival7=@IsNextArrival       
                            ,DepTimeZulu7=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID7  AND orig_nmbr7=@orig_nmbr   
  
   IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime7= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID7 AND tail_nmbr=@tail_nmbr  END 
    END 
    
       ELSE IF   (@tail_nmbr=@PrevTail  AND (SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND location7 is NULL) IS NOT NULL)       
        
    BEGIN    
     
     SET @ID7=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND deptime7 IS NULL  AND location7 is NULL ),@ID)  
      
   -- Curr Tail and Prev tail also should match for this update      
     UPDATE #TempTable SET  Date7=@locdep,date_7=@locdep,Crewlist7=@Crewlist, reqcode7=@reqcode,orig_nmbr7=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location7=CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE @location END,         
                             deptime7=CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,arrtimeapprox7=0,deptimeapprox7=@isapproxtm,pax_total7=@pax_total ,DutyType7=@CrDutyType,TripDesc7=@CrTripDescription ,IsNextArrival7=@IsNextArrival      
                             ,DepTimeZulu7=CASE WHEN @CrDutyType='R' THEN NULL ELSE @DepTimeZulu END
                    WHERE WeekNumber=@WEEK AND ID=@ID7 AND tail_nmbr=@tail_nmbr 
  IF @CrDutyType='MX' BEGIN UPDATE  #TempTable  SET arrtime7= @arrtime  WHERE WeekNumber=@WEEK AND ID=@ID7 AND tail_nmbr=@tail_nmbr  END 
    
    END      
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO #TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
    location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,calcdate,date_7,DutyType7,TripDesc7,IsNextArrival7,DepTimeZulu7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END, CASE WHEN @CrDutyType='MX' THEN @CrDutyType+','+@location+','+@CrTripDescription ELSE  @location END,CASE WHEN @CrDutyType='MX' THEN @arrtime ELSE NULL END,    
    CASE WHEN @CrDutyType='R' THEN '' ELSE @deptime END,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep),@CrDutyType,@CrTripDescription,@IsNextArrival,@DepTimeZulu)  
  
    END
    SET @DATE7=CONVERT(DATE,@locdep)      
END 
  
 ---
 END
 
 ELSE
 
 
 BEGIN
   SET @IDA=(SELECT TOP 1 ID FROM #TempTable WHERE WeekNumber=@WEEK AND (arrtime1 IS NOT NULL OR arrtime2 IS NOT NULL OR arrtime3 IS NOT NULL OR arrtime4 IS NOT NULL 
                                                                       OR arrtime5 IS NOT NULL OR arrtime6 IS NOT NULL OR arrtime7 IS NOT NULL))

 ---ARR
  ----------------------Arrival-------------------------           
IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=0)               
 BEGIN          
  
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep )
 IF @IDA IS  NULL            
     BEGIN  
  
          
      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
                         location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,date_1,ArrTimeZulu1)              
                 VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
                          @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END  
     
     
  ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime1 IS NULL  AND location1 is NULL) IS NOT NULL)
        
    BEGIN  
 
    
    SET @ID1=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime1 IS NULL AND location1 IS NULL),@ID)  
      
 UPDATE #TempTable SET  Date1=@locdep,Crewlist1=@Crewlist, reqcode1=@reqcode,orig_nmbr1=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location1=@locationArr,         
                           arrtime1=@arrtime,arrtimeapprox1=@isapproxtm,deptimeapprox1=@isapproxtm,ArrTimeZulu1=@ArrTimeZulu 
                      WHERE WeekNumber=@WEEK AND ID=@ID1 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
     ELSE            
        
    BEGIN    
   
      
        --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
                         location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,date_1,ArrTimeZulu1)              
                 VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
                          @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
  
    END 
    SET @DATE1=CONVERT(DATE,@locarr)     
  END         
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=1)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_2=@locdep ) 
            
  IF @IDA IS  NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
                          location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,date_2,ArrTimeZulu2)              
                  VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
                          @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime2 IS NULL  AND location2 is NULL) IS NOT NULL)        
        
    BEGIN   
    
    SET @ID2=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime2 IS NULL AND location2 IS NULL),@ID)  
                    
    UPDATE #TempTable SET  Date2=@locdep,Crewlist2=@Crewlist, reqcode2=@reqcode,orig_nmbr2=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location2=@locationArr,         
                           arrtime2=@arrtime,arrtimeapprox2=@isapproxtm,deptimeapprox2=@isapproxtm,ArrTimeZulu2=@ArrTimeZulu      
                      WHERE WeekNumber=@WEEK AND ID=@ID2 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
     ELSE            
        
    BEGIN    

 --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
	                          location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,calcdate,date_2,ArrTimeZulu2)              
	                  VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	                          @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END  
    SET @DATE2=CONVERT(DATE,@locarr)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=2)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @IDA IS  NULL            
     BEGIN              

      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
                          location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,date_3,ArrTimeZulu3)              
                  VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
                          @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND arrtime3 IS NULL  AND location3 is NULL) IS NOT NULL)        
        
    BEGIN    
    
    SET @ID3=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime3 IS NULL AND location3 IS NULL),@ID)  
      
                    
    UPDATE #TempTable SET  Date3=@locdep,Crewlist3=@Crewlist, reqcode3=@reqcode,orig_nmbr3=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location3=@locationArr,         
                           arrtime3=@arrtime,arrtimeapprox3=@isapproxtm,deptimeapprox3=@isapproxtm,ArrTimeZulu3=@ArrTimeZulu       
                      WHERE WeekNumber=@WEEK AND ID=@ID3 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
     ELSE            
        
    BEGIN    
  --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
	                          location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,calcdate,date_3,ArrTimeZulu3 )              
	                  VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	                         @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END  
    SET @DATE3=CONVERT(DATE,@locarr)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=3)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @IDA IS  NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
                          location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,date_4,ArrTimeZulu4)              
                 VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
                         @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime4 IS NULL  AND location4 is NULL) IS NOT NULL)        
        
    BEGIN  
    --SELECT 1
    
    SET @ID4=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime4 IS NULL AND location4 IS NULL),@ID)  
      
    UPDATE #TempTable SET  Date4=@locdep,Crewlist4=@Crewlist, reqcode4=@reqcode,orig_nmbr4=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location4=@locationArr,         
                           arrtime4=@arrtime,arrtimeapprox4=@isapproxtm,deptimeapprox4=@isapproxtm ,ArrTimeZulu4=@ArrTimeZulu       
                      WHERE WeekNumber=@WEEK AND ID=@ID4 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
   ELSE            
        
    BEGIN    

 --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
	                          location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,calcdate,date_4,ArrTimeZulu4)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	                           @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END 
    SET @DATE4=CONVERT(DATE,@locarr)    
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=4)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
    IF @IDA IS  NULL            
     BEGIN              
 
      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,date_5,ArrTimeZulu5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime5 IS NULL  AND location5 is NULL) IS NOT NULL)        
        
    BEGIN  
    --SELECT 1
    
    SET @ID5=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime5 IS NULL AND location5 IS NULL),@ID) 
      
 UPDATE #TempTable SET  Date5=@locdep,Crewlist5=@Crewlist, reqcode5=@reqcode,orig_nmbr5=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location5=@locationArr,         
                           arrtime5=@arrtime,arrtimeapprox5=@isapproxtm,deptimeapprox5=@isapproxtm ,ArrTimeZulu5=@ArrTimeZulu       
                    WHERE WeekNumber=@WEEK AND ID=@ID5 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
 --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
	  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,calcdate,date_5,ArrTimeZulu5)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END 
    SET @DATE5=CONVERT(DATE,@locdep)     
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=5)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
    IF @IDA IS  NULL            
     BEGIN              

  --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,date_6,ArrTimeZulu6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr  AND arrtime6 IS NULL  AND location6 is NULL) IS NOT NULL)        
        
    BEGIN  
    --SELECT 1
    
    SET @ID6=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime6 IS NULL AND location6 IS NULL),@ID) 
                        
    UPDATE #TempTable SET  Date6=@locdep,Crewlist6=@Crewlist, reqcode6=@reqcode,orig_nmbr6=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location6=@locationArr,         
                           arrtime6=@arrtime,arrtimeapprox6=@isapproxtm,deptimeapprox6=@isapproxtm,ArrTimeZulu6=@ArrTimeZulu        
                      WHERE WeekNumber=@WEEK AND ID=@ID6 AND tail_nmbr=@tail_nmbr
  
    END    
    ----
     ELSE            
        
    BEGIN    
    
     --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
	  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,calcdate,date_6,ArrTimeZulu6)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END  
    SET @DATE6=CONVERT(DATE,@locdep)    
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DATEFROM,@locdep)%7)=6)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DATEFROM,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM #TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
    IF @IDA IS  NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO #TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,date_7,ArrTimeZulu7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,CONVERT(DATE,@locdep),@ArrTimeZulu)  
      
     END      
        
 ELSE IF  (@tail_nmbr=@PrevTail AND (SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND tail_nmbr=@tail_nmbr AND arrtime7 IS NULL  AND location7 is NULL) IS NOT NULL)        
        
    BEGIN  
    --SELECT 1
    
    SET @ID7=ISNULL((SELECT TOP 1 ID FROM  #TempTable WHERE WeekNumber=@WEEK AND arrtime7 IS NULL AND location7 IS NULL),@ID) 
            
    UPDATE #TempTable SET  Date7=@locdep,Crewlist7=@Crewlist, reqcode7=@reqcode,orig_nmbr7=CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,location7=@locationArr,         
                           arrtime7=@arrtime,arrtimeapprox7=@isapproxtm,deptimeapprox7=@isapproxtm,ArrTimeZulu7=@ArrTimeZulu        
                      WHERE WeekNumber=@WEEK AND ID=@ID7 AND tail_nmbr=@tail_nmbr
  
    END    

  ELSE            
        
  BEGIN    
      
        --INSERT FOR ARRIVAL      
	   INSERT INTO #TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
	  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,calcdate,date_7,ArrTimeZulu7)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,CASE WHEN @CrDutyType='MX' THEN NULL ELSE  @orig_nmbr END,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@calcdate,@locdep,@ArrTimeZulu) 
  
    END
    SET @DATE7=CONVERT(DATE,@locdep)      
 END 
 
 ---END ARR
 
 END
  -----------------------------------------------------------------------------------

   SET @ID=SCOPE_IDENTITY()   
   SET @PrevTail=@tail_nmbr
   SET @PrevDept=@locationArr
   SET @PrevId=@ID
   SET @PrevOrigin=@orig_nmbr
   --SET @PrevDept=@locdep
   FETCH NEXT FROM curTailInfo INTO @tail_nmbr,@type_code,@ac_code,@locdep,@locarr,@Crewlist,@reqcode,    
 @orig_nmbr,@location,@locationArr,@deptime,@arrtime,@isapproxtm,@pax_total,@SORTORDER,@LegID,@CrTripDescription,@CrDutyType,@IsNextArrival,@DepTimeZulu,@ArrTimeZulu;;             
                
 END
                
            
 CLOSE curTailInfo;              
 DEALLOCATE curTailInfo;  

 DECLARE @WEEKNUM INT=1;
  SELECT  @WEEK= ((DATEDIFF(DAY,@DATEFROM,@DATETO)+1)/7 )
 ---SELECT  @WEEK= CASE WHEN (COUNT( DISTINCT WeekNumber))=0 THEN ((DATEDIFF(DAY,@DATEFROM,@DATETO)+1)/7 ) ELSE COUNT( DISTINCT WeekNumber) END  FROM #TempTable
---- SELECT @WEEK
WHILE (@WEEKNUM<=@WEEK)
 BEGIN

INSERT INTO #TempTable(tail_nmbr,ac_code,type_code,WeekNumber)
SELECT F.TailNum,AT.AircraftCD,F.AircraftCD,@WEEKNUM FROM Fleet F 
                                                  INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID
                                                  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
                                              WHERE F.IsDeleted=0
                                               AND TF.TailNum NOT IN(SELECT tail_nmbr FROM #TempTable WHERE WeekNumber=@WEEKNUM)                
 
   UPDATE #TempTable SET date_1=@DATEFROM,  
                        date_2=@DATEFROM+1,  
                        date_3=@DATEFROM+2,  
                        date_4=@DATEFROM+3,  
                        date_5=@DATEFROM+4,  
                        date_6=@DATEFROM+5,  
                        date_7=@DATEFROM+6
           WHERE WeekNumber=@WEEKNUM; 
 SET @DATEFROM=@DATEFROM+7
 SET @WEEKNUM=@WEEKNUM+1
 END
 



 --DECLARE @WeekNum INT=1    
 INSERT INTO  #TempTail
  SELECT ID,WeekNumber,tail_nmbr,type_code,ac_code,locdep,Date1,crewlist1,reqcode1,ISNULL(orig_nmbr1,0)orig_nmbr1,        
        Date2,crewlist2,reqcode2,ISNULL(orig_nmbr2,0) orig_nmbr2,Date3,crewlist3,reqcode3,ISNULL(orig_nmbr3,0)orig_nmbr3,          
        Date4,crewlist4,reqcode4,ISNULL(orig_nmbr4,0)orig_nmbr4,Date5,crewlist5,reqcode5,ISNULL(orig_nmbr5,0)orig_nmbr5,         
        Date6,crewlist6,reqcode6,ISNULL(orig_nmbr6,0)orig_nmbr6,Date7,crewlist7,reqcode7,ISNULL(orig_nmbr7,0)orig_nmbr7,          
        location1,arrtime1,deptime1,ISNULL(arrtimeapprox1,0) arrtimeapprox1,ISNULL(deptimeapprox1,0) deptimeapprox1,ISNULL(pax_total1,0) pax_total1,        
        location2,arrtime2,deptime2,ISNULL(arrtimeapprox2,0) arrtimeapprox2,ISNULL(deptimeapprox2,0) deptimeapprox2,ISNULL(pax_total2,0) pax_total2,          
        location3,arrtime3,deptime3,ISNULL(arrtimeapprox3,0) arrtimeapprox3,ISNULL(deptimeapprox3,0) deptimeapprox3,ISNULL(pax_total3,0) pax_total3,          
        location4,arrtime4,deptime4,ISNULL(arrtimeapprox4,0) arrtimeapprox4,ISNULL(deptimeapprox4,0) deptimeapprox4,ISNULL(pax_total4,0) pax_total4,          
        location5,arrtime5,deptime5,ISNULL(arrtimeapprox5,0) arrtimeapprox5,ISNULL(deptimeapprox5,0) deptimeapprox5,ISNULL(pax_total5,0) pax_total5,          
        location6,arrtime6,deptime6,ISNULL(arrtimeapprox6,0) arrtimeapprox6,ISNULL(deptimeapprox6,0) deptimeapprox6,ISNULL(pax_total6,0) pax_total6,          
        location7,arrtime7,deptime7,ISNULL(arrtimeapprox7,0) arrtimeapprox7,ISNULL(deptimeapprox7,0) deptimeapprox7,ISNULL(pax_total7,0) pax_total7,
        CASE WHEN DATEPART(weekday,date_1) =1  THEN date_1 
             WHEN DATEPART(weekday,date_2) =1  THEN date_2  
             WHEN DATEPART(weekday,date_3) =1  THEN date_3 
             WHEN DATEPART(weekday,date_4) =1  THEN date_4 
             WHEN DATEPART(weekday,date_5) =1  THEN date_5  
             WHEN DATEPART(weekday,date_6) =1  THEN date_6 
             WHEN DATEPART(weekday,date_7) =1  THEN date_7  END calcdate,          
        date_1 ,
        date_2,
        date_3,
        date_4,
        date_5,
        date_6,
        date_7,
        ROW_NUMBER() OVER (PARTITION BY tail_nmbr,weeknumber ORDER BY WeekNumber,type_code,ID) Rnk ,
        DutyType1,
        DutyType2,
        DutyType3,
        DutyType4,
        DutyType5,
        DutyType6,
        DutyType7,
        TripDesc1, 
        TripDesc2, 
        TripDesc3, 
        TripDesc4, 
        TripDesc5, 
        TripDesc6, 
        TripDesc7 ,
        IsNextArrival1,
        IsNextArrival2,
        IsNextArrival3,
        IsNextArrival4,
        IsNextArrival5,
        IsNextArrival6,
        IsNextArrival7,
        DepTimeZulu1,
        DepTimeZulu2,
        DepTimeZulu3,
        DepTimeZulu4,
        DepTimeZulu5,
        DepTimeZulu6,
        DepTimeZulu7,
        ArrTimeZulu1,
        ArrTimeZulu2,
        ArrTimeZulu3,
        ArrTimeZulu4,
        ArrTimeZulu5,
        ArrTimeZulu6,
        ArrTimeZulu7

        --airportcode,airportdesc,airportcity,crewcode,crewdesc,authcode,authdesc     
       FROM #TempTable             
   ORDER BY WeekNumber,type_code,ID  
   

DECLARE @HomeBaseTable TABLE(FleetID BIGINT,
                             ArrivalDTTMLocal DATE,
                             IcaoID CHAR(4),
                             TailNumber VARCHAR(9),
                             Rk INT)

INSERT INTO @HomeBaseTable
SELECT PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ArrivalDTTMLocal,A.IcaoID,F.TailNum,
	ROW_NUMBER()OVER(PARTITION BY PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ORDER BY PM.FleetID,ArrivalDTTMLocal DESC) Rnk
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PL.IsDeleted = 0
		INNER JOIN Fleet F ON PM.FleetID=F.FleetID
		INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
		INNER JOIN @TempFleetID TF ON PM.FleetID=TF.FleetID
		WHERE PM.TripStatus IN ('T','H')
		  AND PL.ArrivalDTTMLocal<=@DATETO
		  AND PM.IsDeleted = 0
		ORDER BY ArrivalDTTMLocal DESC


				
UPDATE XX SET location1=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_1)
            AND ISNULL(location1,'') = ''
            AND Rnk=1

 UPDATE XX SET location1=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location1,'') = ''
							 AND Rnk=1

UPDATE XX SET location2=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_2)
            AND ISNULL(location2,'') = ''
            AND Rnk=1

 UPDATE XX SET location2=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location2,'') = ''
							 AND Rnk=1


UPDATE XX SET location3=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_3)
            AND ISNULL(location3,'') = ''
            AND Rnk=1

 UPDATE XX SET location3=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location3,'') = ''
							 AND Rnk=1

UPDATE XX SET location4=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_4)
            AND ISNULL(location4,'') = ''
            AND Rnk=1

 UPDATE XX SET location4=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location4,'') = ''
							 AND Rnk=1
							 
UPDATE XX SET location5=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_5)
            AND ISNULL(location5,'') = ''
            AND Rnk=1

 UPDATE XX SET location5=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location5,'') = ''
							 AND Rnk=1

UPDATE XX SET location6=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_6)
            AND ISNULL(location6,'') = ''
            AND Rnk=1

 UPDATE XX SET location6=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location6,'') = ''
							 AND Rnk=1							 

UPDATE XX SET location7=T.IcaoID
		   FROM #TempTail XX
			INNER JOIN @HomeBaseTable T ON T.TailNumber=XX.tail_nmbr AND Rk = 1
           WHERE T.TailNumber=XX.tail_nmbr 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.date_7)
            AND ISNULL(location7,'') = ''
            AND Rnk=1

 UPDATE XX SET location7=IcaoID
		   FROM #TempTail XX
			INNER JOIN (SELECT A.IcaoID,FleetID,TailNum FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.TailNum=XX.tail_nmbr
							WHERE HB.TailNum=XX.tail_nmbr 
							 AND ISNULL(location7,'') = ''
							 AND Rnk=1		
							 
IF @IsPDF=0

BEGIN

SELECT ID,WeekNumber,tail_nmbr,ac_code type_code,type_code ac_code,ISNULL(locdep,'') locdep,Date1,ISNULL(crewlist1,'') crewlist1,ISNULL(reqcode1,'') reqcode1,ISNULL(orig_nmbr1,0)orig_nmbr1,        
        Date2,ISNULL(crewlist2,'')crewlist2,ISNULL(reqcode2,'')reqcode2,ISNULL(orig_nmbr2,0) orig_nmbr2,Date3,ISNULL(crewlist3,'')crewlist3,ISNULL(reqcode3,'')reqcode3,ISNULL(orig_nmbr3,0)orig_nmbr3,          
        Date4,ISNULL(crewlist4,'')crewlist4,ISNULL(reqcode4,'')reqcode4,ISNULL(orig_nmbr4,0)orig_nmbr4,Date5,ISNULL(crewlist5,'')crewlist5,ISNULL(reqcode5,'')reqcode5,ISNULL(orig_nmbr5,0)orig_nmbr5,         
        Date6,ISNULL(crewlist6,'')crewlist6,ISNULL(reqcode6,'')reqcode6,ISNULL(orig_nmbr6,0)orig_nmbr6,Date7,isnull(crewlist7,'')crewlist7,ISNULL(reqcode7,'')reqcode7,ISNULL(orig_nmbr7,0)orig_nmbr7,            
        ISNULL(location1,'')location1,REPLACE(ISNULL(arrtime1,''),':','')arrtime1,REPLACE(ISNULL(deptime1,''),':','') deptime1,ISNULL(arrtimeapprox1,0) arrtimeapprox1,ISNULL(deptimeapprox1,0) deptimeapprox1,ISNULL(pax_total1,0) pax_total1,        
        ISNULL(location2,'')location2,REPLACE(ISNULL(arrtime2,''),':','') arrtime2,REPLACE(ISNULL(deptime2,''),':','')deptime2,ISNULL(arrtimeapprox2,0) arrtimeapprox2,ISNULL(deptimeapprox2,0) deptimeapprox2,ISNULL(pax_total2,0) pax_total2,          
        ISNULL(location3,'')location3,REPLACE(ISNULL(arrtime3,''),':','') arrtime3,REPLACE(ISNULL(deptime3,''),':','')deptime3,ISNULL(arrtimeapprox3,0) arrtimeapprox3,ISNULL(deptimeapprox3,0) deptimeapprox3,ISNULL(pax_total3,0) pax_total3,          
        ISNULL(location4,'')location4,REPLACE(ISNULL(arrtime4,''),':','') arrtime4,REPLACE(ISNULL(deptime4,''),':','')deptime4,ISNULL(arrtimeapprox4,0) arrtimeapprox4,ISNULL(deptimeapprox4,0) deptimeapprox4,ISNULL(pax_total4,0) pax_total4,          
        ISNULL(location5,'')location5,REPLACE(ISNULL(arrtime5,''),':','') arrtime5,REPLACE(ISNULL(deptime5,''),':','')deptime5,ISNULL(arrtimeapprox5,0) arrtimeapprox5,ISNULL(deptimeapprox5,0) deptimeapprox5,ISNULL(pax_total5,0) pax_total5,          
        ISNULL(location6,'')location6,REPLACE(ISNULL(arrtime6,''),':','') arrtime6,REPLACE(ISNULL(deptime6,''),':','')deptime6,ISNULL(arrtimeapprox6,0) arrtimeapprox6,ISNULL(deptimeapprox6,0) deptimeapprox6,ISNULL(pax_total6,0) pax_total6,          
        ISNULL(location7,'')location7,REPLACE(ISNULL(arrtime7,''),':','') arrtime7,REPLACE(ISNULL(deptime7,''),':','')deptime7,ISNULL(arrtimeapprox7,0) arrtimeapprox7,ISNULL(deptimeapprox7,0) deptimeapprox7,ISNULL(pax_total7,0) pax_total7,
        CASE WHEN DATEPART(weekday,date_1) =1  THEN date_1 
             WHEN DATEPART(weekday,date_2) =1  THEN date_2  
             WHEN DATEPART(weekday,date_3) =1  THEN date_3 
             WHEN DATEPART(weekday,date_4) =1  THEN date_4 
             WHEN DATEPART(weekday,date_5) =1  THEN date_5  
             WHEN DATEPART(weekday,date_6) =1  THEN date_6 
             WHEN DATEPART(weekday,date_7) =1  THEN date_7  END calcdate,   
        LEFT(datename(dw,date_1),3)+' '+LEFT(DATENAME(MONTH,date_1),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_1))+'/'+convert(varchar(10),YEAR(date_1))date_1,          
        LEFT(datename(dw,date_2),3)+' '+LEFT(DATENAME(MONTH,date_2),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_2))+'/'+convert(varchar(10),YEAR(date_2))date_2,          
        LEFT(datename(dw,date_3),3)+' '+LEFT(DATENAME(MONTH,date_3),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_3))+'/'+convert(varchar(10),YEAR(date_3))date_3,           
        LEFT(datename(dw,date_4),3)+' '+LEFT(DATENAME(MONTH,date_4),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_4))+'/'+convert(varchar(10),YEAR(date_4))date_4,          
        LEFT(datename(dw,date_5),3)+' '+LEFT(DATENAME(MONTH,date_5),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_5))+'/'+convert(varchar(10),YEAR(date_5))date_5,           
        LEFT(datename(dw,date_6),3)+' '+LEFT(DATENAME(MONTH,date_6),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_6))+'/'+convert(varchar(10),YEAR(date_6))date_6,           
        LEFT(datename(dw,date_7),3)+' '+LEFT(DATENAME(MONTH,date_7),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_7))+'/'+convert(varchar(10),YEAR(date_7))date_7 ,
        Rnk ,
        ISNULL(DutyType1,'')DutyType1,
        ISNULL(DutyType2,'')DutyType2,
        ISNULL(DutyType3,'')DutyType3,
        ISNULL(DutyType4,'')DutyType4,
        ISNULL(DutyType5,'')DutyType5,
        ISNULL(DutyType6,'')DutyType6,
        ISNULL(DutyType7,'')DutyType7,
        ISNULL(TripDesc1,'')TripDesc1, 
        ISNULL(TripDesc2,'')TripDesc2, 
        ISNULL(TripDesc3,'')TripDesc3, 
        ISNULL(TripDesc4,'')TripDesc4, 
        ISNULL(TripDesc5,'')TripDesc5, 
        ISNULL(TripDesc6,'')TripDesc6, 
        ISNULL(TripDesc7,'')TripDesc7,
        IsNextArrival1,
        IsNextArrival2,
        IsNextArrival3,
        IsNextArrival4,
        IsNextArrival5,
        IsNextArrival6,
        IsNextArrival7,
		ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu1,113),13,5),':',''),'') DepTimeZulu1,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu2,113),13,5),':',''),'')  DepTimeZulu2,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu3,113),13,5),':',''),'')  DepTimeZulu3,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu4,113),13,5),':',''),'')  DepTimeZulu4,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu5,113),13,5),':',''),'')  DepTimeZulu5,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu6,113),13,5),':',''),'')  DepTimeZulu6,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu7,113),13,5),':',''),'')  DepTimeZulu7,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu1,113),13,5),':',''),'')  ArrTimeZulu1,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu2,113),13,5),':',''),'')  ArrTimeZulu2,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu3,113),13,5),':',''),'')  ArrTimeZulu3,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu4,113),13,5),':',''),'')  ArrTimeZulu4,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu5,113),13,5),':',''),'')  ArrTimeZulu5,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu6,113),13,5),':',''),'')  ArrTimeZulu6,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu7,113),13,5),':',''),'')  ArrTimeZulu7
 
        FROM #TempTail   ORDER BY WeekNumber,ac_code,ID

END
ELSE

BEGIN
SELECT  ID,WeekNumber,tail_nmbr,ac_code type_code,type_code ac_code,ISNULL(locdep,'') locdep,Date1,ISNULL(crewlist1,'') crewlist1,ISNULL(reqcode1,'') reqcode1,ISNULL(orig_nmbr1,0)orig_nmbr1,        
        Date2,ISNULL(crewlist2,'')crewlist2,ISNULL(reqcode2,'')reqcode2,ISNULL(orig_nmbr2,0) orig_nmbr2,Date3,ISNULL(crewlist3,'')crewlist3,ISNULL(reqcode3,'')reqcode3,ISNULL(orig_nmbr3,0)orig_nmbr3,          
        Date4,ISNULL(crewlist4,'')crewlist4,ISNULL(reqcode4,'')reqcode4,ISNULL(orig_nmbr4,0)orig_nmbr4,Date5,ISNULL(crewlist5,'')crewlist5,ISNULL(reqcode5,'')reqcode5,ISNULL(orig_nmbr5,0)orig_nmbr5,         
        Date6,ISNULL(crewlist6,'')crewlist6,ISNULL(reqcode6,'')reqcode6,ISNULL(orig_nmbr6,0)orig_nmbr6,Date7,isnull(crewlist7,'')crewlist7,ISNULL(reqcode7,'')reqcode7,ISNULL(orig_nmbr7,0)orig_nmbr7,          
        CASE WHEN LEN(location1)>4 THEN SUBSTRING(location1,4,4) ELSE location1 END  location1,REPLACE(ISNULL(arrtime1,''),':','')arrtime1,REPLACE(ISNULL(deptime1,''),':','') deptime1,ISNULL(arrtimeapprox1,0) arrtimeapprox1,ISNULL(deptimeapprox1,0) deptimeapprox1,ISNULL(pax_total1,0) pax_total1,        
        CASE WHEN LEN(location2)>4 THEN SUBSTRING(location2,4,4) ELSE location2 END location2,REPLACE(ISNULL(arrtime2,''),':','') arrtime2,REPLACE(ISNULL(deptime2,''),':','')deptime2,ISNULL(arrtimeapprox2,0) arrtimeapprox2,ISNULL(deptimeapprox2,0) deptimeapprox2,ISNULL(pax_total2,0) pax_total2,          
        CASE WHEN LEN(location3)>4 THEN SUBSTRING(location3,4,4) ELSE location3 END  location3,REPLACE(ISNULL(arrtime3,''),':','') arrtime3,REPLACE(ISNULL(deptime3,''),':','')deptime3,ISNULL(arrtimeapprox3,0) arrtimeapprox3,ISNULL(deptimeapprox3,0) deptimeapprox3,ISNULL(pax_total3,0) pax_total3,          
        CASE WHEN LEN(location4)>4 THEN SUBSTRING(location4,4,4) ELSE location4 END location4,REPLACE(ISNULL(arrtime4,''),':','') arrtime4,REPLACE(ISNULL(deptime4,''),':','')deptime4,ISNULL(arrtimeapprox4,0) arrtimeapprox4,ISNULL(deptimeapprox4,0) deptimeapprox4,ISNULL(pax_total4,0) pax_total4,          
        CASE WHEN LEN(location5)>4 THEN SUBSTRING(location5,4,4) ELSE location5 END location5,REPLACE(ISNULL(arrtime5,''),':','') arrtime5,REPLACE(ISNULL(deptime5,''),':','')deptime5,ISNULL(arrtimeapprox5,0) arrtimeapprox5,ISNULL(deptimeapprox5,0) deptimeapprox5,ISNULL(pax_total5,0) pax_total5,          
        CASE WHEN LEN(location6)>4 THEN SUBSTRING(location6,4,4) ELSE location6 END location6,REPLACE(ISNULL(arrtime6,''),':','') arrtime6,REPLACE(ISNULL(deptime6,''),':','')deptime6,ISNULL(arrtimeapprox6,0) arrtimeapprox6,ISNULL(deptimeapprox6,0) deptimeapprox6,ISNULL(pax_total6,0) pax_total6,          
        CASE WHEN LEN(location7)>5 THEN SUBSTRING(location7,4,4) ELSE location7 END location7,REPLACE(ISNULL(arrtime7,''),':','') arrtime7,REPLACE(ISNULL(deptime7,''),':','')deptime7,ISNULL(arrtimeapprox7,0) arrtimeapprox7,ISNULL(deptimeapprox7,0) deptimeapprox7,ISNULL(pax_total7,0) pax_total7,
        CASE WHEN DATEPART(weekday,date_1) =1  THEN date_1 
             WHEN DATEPART(weekday,date_2) =1  THEN date_2  
             WHEN DATEPART(weekday,date_3) =1  THEN date_3 
             WHEN DATEPART(weekday,date_4) =1  THEN date_4 
             WHEN DATEPART(weekday,date_5) =1  THEN date_5  
             WHEN DATEPART(weekday,date_6) =1  THEN date_6 
             WHEN DATEPART(weekday,date_7) =1  THEN date_7  END calcdate,   
        LEFT(datename(dw,date_1),3)+' '+LEFT(DATENAME(MONTH,date_1),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_1))+'/'+convert(varchar(10),YEAR(date_1))date_1,          
        LEFT(datename(dw,date_2),3)+' '+LEFT(DATENAME(MONTH,date_2),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_2))+'/'+convert(varchar(10),YEAR(date_2))date_2,          
        LEFT(datename(dw,date_3),3)+' '+LEFT(DATENAME(MONTH,date_3),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_3))+'/'+convert(varchar(10),YEAR(date_3))date_3,           
        LEFT(datename(dw,date_4),3)+' '+LEFT(DATENAME(MONTH,date_4),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_4))+'/'+convert(varchar(10),YEAR(date_4))date_4,          
        LEFT(datename(dw,date_5),3)+' '+LEFT(DATENAME(MONTH,date_5),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_5))+'/'+convert(varchar(10),YEAR(date_5))date_5,           
        LEFT(datename(dw,date_6),3)+' '+LEFT(DATENAME(MONTH,date_6),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_6))+'/'+convert(varchar(10),YEAR(date_6))date_6,           
        LEFT(datename(dw,date_7),3)+' '+LEFT(DATENAME(MONTH,date_7),3)+' '+CONVERT(VARCHAR(10),DATEPART(DAY,date_7))+'/'+convert(varchar(10),YEAR(date_7))date_7 ,
        Rnk ,
        DutyType1,
        DutyType2,
        DutyType3,
        DutyType4,
        DutyType5,
        DutyType6,
        DutyType7,
        TripDesc1, 
        TripDesc2, 
        TripDesc3, 
        TripDesc4, 
        TripDesc5, 
        TripDesc6, 
        TripDesc7 ,
       IsNextArrival1,
        IsNextArrival2,
        IsNextArrival3,
        IsNextArrival4,
        IsNextArrival5,
        IsNextArrival6,
        IsNextArrival7,
		ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu1,113),13,5),':',''),'') DepTimeZulu1,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu2,113),13,5),':',''),'')  DepTimeZulu2,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu3,113),13,5),':',''),'')  DepTimeZulu3,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu4,113),13,5),':',''),'')  DepTimeZulu4,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu5,113),13,5),':',''),'')  DepTimeZulu5,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu6,113),13,5),':',''),'')  DepTimeZulu6,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),DepTimeZulu7,113),13,5),':',''),'')  DepTimeZulu7,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu1,113),13,5),':',''),'')  ArrTimeZulu1,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu2,113),13,5),':',''),'')  ArrTimeZulu2,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu3,113),13,5),':',''),'')  ArrTimeZulu3,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu4,113),13,5),':',''),'')  ArrTimeZulu4,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu5,113),13,5),':',''),'')  ArrTimeZulu5,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu6,113),13,5),':',''),'')  ArrTimeZulu6,
        ISNULL(REPLACE(SUBSTRING (CONVERT(VARCHAR(30),ArrTimeZulu7,113),13,5),':',''),'')  ArrTimeZulu7
        
        FROM #TempTail   ORDER BY WeekNumber,ac_code,ID
END
 
    IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
    DROP TABLE #TempTable
    
    IF OBJECT_ID('tempdb..#TempTail ') IS NOT NULL
	DROP TABLE #TempTail  
 
 END; 
                
 ---EXEC spGetReportPREWeeklyCalendar1ExportInformation 'supervisor_99', '2012-10-01', '2012-10-31', '', ''         
 /*        
 --select * from Fleet where TailNum in ('XOFNGA', 'SZTBP','ESMNM')        
 EXEC spGetReportPREWeeklyCalendar1Information 'UC', '2012-09-19', '2012-09-25', '', ''        
 declare @fleetid bigint = 10001212620        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-19'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-20'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-21'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-22'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-23'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-24'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-25'        
 EXEC spGetReportPREWeeklyCalendar1ExportInformation 'UC', '2012-09-19', '2012-09-25', '', ''        
 */  
  













