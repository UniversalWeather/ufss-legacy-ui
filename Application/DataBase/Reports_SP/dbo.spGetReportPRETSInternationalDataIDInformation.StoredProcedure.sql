IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSInternationalDataIDInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSInternationalDataIDInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSInternationalDataIDInformation]
	@UserCD VARCHAR(30)
	,@TripID VARCHAR(30)
	,@PassengerCD VARCHAR(30) = ''
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSInternationalDataIDInformation
-- Author: AISHWARYA.M
-- Create date: 04 OCT 2012
-- Description: Get Preflight Tripsheet International Data information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================
	 SELECT DISTINCT [ID] = CONVERT(VARCHAR,PPL.PassengerID), PaxCrewCD = P.PassengerRequestorCD
			,[Type] = 'P'
			FROM PreflightMain PM 
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND P.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN P.PassengerRequestorCD ELSE @PassengerCD END	 	             
			AND PM.TripID = CONVERT(BIGINT,@TripID)
		UNION ALL
	 SELECT  DISTINCT [ID] = CONVERT(VARCHAR,PCL.CrewID), PaxCrewCD = C.CrewCD
			,[Type] = 'C'
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
			INNER JOIN Crew C ON PCL.CrewID = C.CrewID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
	ORDER BY PaxCrewCD
END

--EXEC spGetReportPRETSInternationalDataIDInformation 'UC','1000142'


GO


