IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBTripsheetCheckListGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBTripsheetCheckListGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE  [dbo].[spGetReportDBTripsheetCheckListGroup] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30) --Mandatory
)
AS
 
 

-- ===============================================================================
-- SPC Name: [spGetReportDBTripsheetCheckListGroup]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get Payment Types
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT
  [Code] = CheckGroupCD
  , [Description] = CheckGroupDescription
FROM TripManagerCheckListGroup
WHERE CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
AND CustomerID = CONVERT(BIGINT, @UserCustomerID)
-- And IsInActive=0
And IsDeleted=0

--EXEC spGetReportDBTripsheetCheckListGroup 'SUPERVISOR_99','10099'





GO


