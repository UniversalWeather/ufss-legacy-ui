IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportUtilitiesItineraryPlanning]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportUtilitiesItineraryPlanning]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportUtilitiesItineraryPlanning]
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @ItineraryNumber AS BIGINT
    
AS
BEGIN

SET NOCOUNT ON;

SELECT DISTINCT
       IP.IntinearyNUM
       ,RIGHT('0000000' +CAST(IP.IntinearyNUM AS VARCHAR(7)),7) AS FILENUMBER     
       ,IP.ItineraryPlanDescription
       ,IP.ItineraryPlanQuarter
       ,IP.ItineraryPlanQuarterYear
       ,ACR.AircraftCD
       ,ACR.AircraftDescription
       ,FE.TailNum
       ,ID.LegNUM
       ,D.IcaoID AS DEPART
       ,A.IcaoID AS ARRIVE
       ,ISNULL(ID.PassengerNUM,0) AS PAX
       ,ISNULL(ID.Cost,0) AS COST
       ,ISNULL(ID.Distance,0) AS DISTANCE
       ,ISNULL(ID.PowerSetting,0) AS Pwr
       ,ID.TakeoffBIAS
       ,ID.LandingBias
       ,ID.TrueAirSpeed
       ,ID.WindsBoeingTable
       ,(CASE WHEN ID.WindReliability = 1 THEN '50%'
             WHEN ID.WindReliability = 2 THEN '75%'
             WHEN ID.WindReliability = 3 THEN '85%' END) AS WindReli
       ,ID.ElapseTM AS ETE --FLOOR(ISNULL(ID.ElapseTM,0)*10)*0.1 AS ETE
       ,D.CityName AS DEPCITY
       ,A.CityName AS ARRCITY
       ,ID.DepartureLocal 
       ,ID.ArrivalLocal
       ,ID.DepartureGMT
       ,ID.ArrivalGMT
       ,ID.DepartureHome
       ,ID.ArrivalHome 
       ,ID.IsArrivalConfirmation
       ,ID.IsDepartureConfirmed         
  FROM ItineraryPlan IP
  JOIN ItineraryPlanDetail ID ON IP.ItineraryPlanID = ID.ItineraryPlanID
  LEFT JOIN Fleet FE ON FE.FleetID = IP.FleetID 
  LEFT JOIN Aircraft AC ON FE.AircraftID = AC.AircraftID
  LEFT JOIN Aircraft ACR ON IP.AircraftID = ACR.AircraftID
  LEFT JOIN Airport D ON ID.DAirportID = D.AirportID  
  LEFT JOIN Airport A ON ID.AAirportID = A.AirportID
 WHERE (IP.IntinearyNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ItineraryNumber, ','))OR @ItineraryNumber='')
    AND IP.CustomerID = @UserCustomerID 
    AND IP.IsDeleted = 0 
	AND ID.IsDeleted = 0 
	ORDER BY IP.ItineraryPlanDescription, FILENUMBER  
 END
 
-- SELECT * FROM ItineraryPlanDetail
 



GO


