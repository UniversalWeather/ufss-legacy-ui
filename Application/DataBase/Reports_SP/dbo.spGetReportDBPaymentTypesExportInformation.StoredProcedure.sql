IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBPaymentTypesExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBPaymentTypesExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE  [dbo].[spGetReportDBPaymentTypesExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30)
)
AS
 
 

-- ===============================================================================
-- SPC Name: [spGetReportDBPaymentTypesExportInformation]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get Payment Types
-- Revision History
-- Date        Name        Ver         Change
-- EXEC spGetReportDBPaymentTypesExportInformation  'SUPERVISOR_99','10099'
-- ================================================================================

SELECT PaymentCode= PaymentTypeCD
	, PaymentDescription = PaymentTypeDescription
	FROM PaymentType
	WHERE CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND CustomerID=CONVERT(BIGINT,@UserCustomerID)
	And IsDeleted=0

--EXEC spGetReportDBPaymentTypesExportInformation 'SUPERVISOR_99',10099





GO


