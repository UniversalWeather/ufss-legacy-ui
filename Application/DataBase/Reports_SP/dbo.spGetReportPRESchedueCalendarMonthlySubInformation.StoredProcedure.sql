IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlySubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlySubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
--EXEC spGetReportPRESchedueCalendarMonthlySubInformation 'Ajeet',10002,12,2014,@Footer=0
*/
CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlySubInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@MonthNo INT
       ,@YearNo	 INT
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsShowTrip BIT=1
	  ,@IsActivityOnly BIT=0
	  ,@IsFirstLastLeg BIT=0
	  ,@IsCrew	BIT=0
	  ,@IsShowTripStatus BIT=0
	  ,@IsDisplayRedEyeFlight BIT=0
	  ,@IsFlightNo BIT=0
	  ,@IsLegPurpose BIT=1
	  ,@IsSeatAvailable BIT=1
	  ,@IsPaxCount BIT=1
	  ,@IsRequestor BIT=1
	  ,@IsDepartment BIT=1
	  ,@IsAuthorization  BIT=1
	  ,@IsFlightCategory BIT=1
	  ,@IsCummulativeETE BIT=1
	  ,@IsETE BIT=1
	  ,@IsTotalETE BIT=1
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@StdMonthReport BIT=0--1-Std,0-Monthly
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''
	  ,@GoToDate DATE = NULL
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlySubInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
SET NOCOUNT ON   
DECLARE @BeginDate DATETIME,@EndDate DATETIME;
DECLARE @DATEFROM DATETIME,@DATETO DATETIME
DECLARE @IsSunSatON BIT
DECLARE @IsNoPastWeekON BIT

DECLARE @CumTemp TABLE     
(    
   
 TripId BIGINT,
 LegNUM BIGINT,     
 CumulativeETE numeric(14,3)    
) 
DECLARE @TotalEteTemp TABLE     
(       
 TripId BIGINT,     
 TotalETE numeric(14,3)    
)
INSERT INTO @TotalEteTemp(TripId, TotalETE)  
SELECT TripId, ISNULL(SUM(ElapseTM),0) 
FROM PreflightLeg 
WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 
GROUP BY TripID


SELECT @ECrew=CASE WHEN ISNULL(@EFleet,'')<>'' THEN '' ELSE @ECrew END 
SELECT @BlackWhiteClr = CASE WHEN @Color = 0 THEN 1 ELSE @BlackWhiteClr END

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
                            

 
SET @BeginDate=DATEADD(mm,(@YearNo - 1900)* 12 + @MonthNo - 1,1 - 1)
SET @EndDate=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @BeginDate) + 1, 0)) 
--SELECT DATEPART(DW, @EndDate)

SELECT @IsSunSatON=PSCP.SaturdaySundayWeek,@IsNoPastWeekON=PSCP.NoPastWeek from PreflightScheduleCalenderPreferences as PSCP 
where PSCP.CustomerID=@UserCustomerID and PSCP.Username=@UserCD and PSCP.CalenderType='View_Monthly'
   
IF @StdMonthReport=0
BEGIN 


SET @DATETO=@EndDate  
         
SELECT @BeginDate=(CASE WHEN @StdMonthReport=0 THEN @BeginDate -(CASE WHEN DATEPART(DW, @BeginDate)=2 THEN 0
                                                             WHEN DATEPART(DW, @BeginDate)=3 THEN 1
                                                             WHEN DATEPART(DW, @BeginDate)=4 THEN 2
															 WHEN DATEPART(DW, @BeginDate)=5 THEN 3
                                                             WHEN DATEPART(DW, @BeginDate)=6 THEN 4
                                                             WHEN DATEPART(DW, @BeginDate)=7 THEN 5
                                                             WHEN DATEPART(DW, @BeginDate)=1 THEN 6 END) ELSE @BeginDate END)    


SELECT @EndDate=(CASE WHEN @StdMonthReport=0 THEN @EndDate +(CASE WHEN DATEPART(DW, @EndDate)=2 THEN 6
                                                             WHEN DATEPART(DW, @EndDate)=3 THEN 5
                                                             WHEN DATEPART(DW, @EndDate)=4 THEN 4
                                                             WHEN DATEPART(DW, @EndDate)=5 THEN 3
                                                             WHEN DATEPART(DW, @EndDate)=6 THEN 2
                                                             WHEN DATEPART(DW, @EndDate)=7 THEN 1
                                                             WHEN DATEPART(DW, @EndDate)=1 THEN 0 END) ELSE @EndDate END)  
SET @DATEFROM=@BeginDate;
END
ELSE
BEGIN
------------------ SatSun ON AND NOPastWeek OFF-----------------------
IF(@IsSunSatON=1 AND @IsNoPastWeekON=0)
BEGIN
SET @BeginDate=DATEADD(wk,DATEDIFF(wk,0,@BeginDate),-1)
END

------------------ SatSun ON AND NOPastWeek ON-----------------------
IF(@IsSunSatON=1 AND @IsNoPastWeekON=1)
BEGIN
SET @BeginDate=DATEADD(wk,DATEDIFF(wk,7,@GoToDate),6)
END

------------------ SatSun OFF AND NOPastWeek ON-----------------------
IF(@IsSunSatON=0 AND @IsNoPastWeekON=1)
BEGIN
SET @BeginDate=DATEADD(wk,DATEDIFF(wk,0,@GoToDate),0)
END

------------------ SatSun OFF AND NOPastWeek OFF-----------------------
IF(@IsSunSatON=0 AND @IsNoPastWeekON=0)
BEGIN
SET @BeginDate=DATEADD(wk,DATEDIFF(wk,0,@BeginDate),0)
END   
          

SELECT @EndDate=@BeginDate+41
SET @DATEFROM =@BeginDate
SET @DATETO =@EndDate
END 

        DECLARE @VendorStatus VARCHAR(10)=(SELECT CASE WHEN @Vendors=1 THEN '1'+','+'0' 
                                                      WHEN @Vendors=2 THEN '1' ELSE '0' END) 
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		



IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END


-----Rest Record and Maintenace Reco
DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9),CrewID BIGINT)

 
 DECLARE @TableTrip TABLE(TripNUM BIGINT,
                          FlightNUM VARCHAR(12),
                          PassengerTotal INT,
                          PassengerRequestorCD VARCHAR(5),
                          DepartmentCD VARCHAR(8),
                          AuthorizationCD VARCHAR(8),
                          TripDescription VARCHAR(40),
                          FlightPurpose VARCHAR(40),
                          SeatsAvailabe INT,
                          CrewList VARCHAR(300),
                          ElapseTM NUMERIC(7,3),
                          TripStatus CHAR(2),
                          FlightCatagoryCD CHAR(4),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          FleetID BIGINT,
                          TailNum VARCHAR(9),
                          TypeCode VARCHAR(10),
                          DutyTYPE CHAR(2),
NextLocalDTTM DATETIME,
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(25),
                          ArrivelICAOID VARCHAR(25),
                          DepCity VARCHAR(25),
                          ArrCity VARCHAR(25),
                          DepState VARCHAR(25),
                          ArrState VARCHAR(25),
                          LegNum BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          CrewID BIGINT,
                          CrewCD VARCHAR(5),
						  FgColor VARCHAR(8),
						  BgColr  VARCHAR(8)
                          )
 


   
 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,PM.FlightNUM
     ,PL.PassengerTotal
     ,P.PassengerRequestorCD
     ,DEP.DepartmentCD
     ,DA.AuthorizationCD
     ,PM.TripDescription
     ,PL.FlightPurpose
     ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
     ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
                  WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
     ,PL.ElapseTM
     ,PM.TripStatus
     ,FC.FlightCatagoryCD
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN
 PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE
 PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,D.CityName
     ,A.CityName
     ,D.StateName
     ,A.StateName
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
     ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
            --  LEFT OUTER JOIN PreflightLeg PLN ON PL.TripID=PLN.TripID AND PL.LegNUM+1=PLN.LegNUM
              --INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
              LEFT OUTER JOIN Fleet F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN Crew  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			   AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	         --LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
	         LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
             LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
      AND ((F.IsDeleted=0 AND F.IsInActive=0) OR CW.IsDeleted=0 AND CW.IsStatus=1 AND ((CW.IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((CW.IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))



 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,PM.FlightNUM
     ,PL.PassengerTotal
     ,P.PassengerRequestorCD
     ,DEP.DepartmentCD
     ,DA.AuthorizationCD
     ,PM.TripDescription
     ,PL.FlightPurpose
     ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
      ,CrewCD=CASE WHEN @IsAllCrew=1 THEN SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) 
                  WHEN @IsCrew=1 THEN  ISNULL(PIC.CrewCD,'')+(CASE WHEN ISNULL(SIC.CrewCD,'')<>'' THEN ','+ISNULL(SIC.CrewCD,'') ELSE '' END) ELSE '' END
     ,PL.ElapseTM
     ,PM.TripStatus
     ,FC.FlightCatagoryCD
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'  THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN 
PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE  CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,D.CityName
     ,A.CityName
     ,D.StateName
     ,A.StateName
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID 
     ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
     ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              --INNER JOIN (SELECT SUM(ElapseTM) Elapse,TripID FROM PreflightLeg WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  GROUP BY TripID)PL1 ON PL1.TripID=PL.TripID ---AND PL1.TripNUM=PM.TripNUM
              LEFT OUTER JOIN Fleet F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN Crew  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID
																			 AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	         --LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
	         LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
             LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=CONVERT(BIGINT,@UserCustomerID) )SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND ((F.IsDeleted=0 AND F.IsInActive=0) OR CW.IsDeleted=0 AND CW.IsStatus=1 AND ((CW.IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((CW.IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))
      AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'
  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
      AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
        AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))



INSERT INTO @CurDate(DateWeek,FleetID,TailNum)
SELECT T.*,F.FleetID,F.TailNum FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                            LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


IF @IsActivityOnly=1
BEGIN

INSERT INTO @CurDate(DateWeek,CrewID)
SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND CC.IsDeleted=0 AND CC.IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
								   AND (CC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
END
ELSE
BEGIN
DELETE FROM @TableTrip WHERE RecordTye='C' AND FleetID IS NULL 

END


INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,FgColor,BgColr)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE 
NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END) FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                             WHERE CONVERT(DATE,CD.DateWeek)>CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                               --AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,TripDescription,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,CrewCD,FgColor,BgColr)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,TripDescription,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye 
                ,CASE WHEN TT.CrewID IS NOT NULL THEN CrewCD ELSE '' END,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END)      
                    FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID OR CD.CrewID=TT.CrewID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye IN('C','M')
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                               --AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)

    

             
DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


DECLARE @TempTable TABLE(ID INT IDENTITY
						,TripID BIGINT
                        ,WeekNumber INT
                        ,FleetID BIGINT
                        ,TailNum VARCHAR(9)
                        ,TripNUM BIGINT
                        ,location VARCHAR(200) 
                        ,FlightNUM VARCHAR(12)
                        ,pax_total INT 
                        ,reqcode VARCHAR(5)
                        ,DepartmentCD VARCHAR(8)
                        ,AuthorizationCD VARCHAR(8)
                        ,FlightPurpose VARCHAR(40)
                        ,SeatsAvailable INT
                        ,crewlist VARCHAR(300) 
                        ,ElapseTM NUMERIC(7,3)
                        ,TripStatus CHAR(2)
                        ,FlightCatagoryCD VARCHAR(25) 
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,SORTORDER INT
                        ,RecordType VARCHAR(1)
                        ,TypeCode VARCHAR(10)
                        ,FgColor VARCHAR(8)
                        ,BgColr  VARCHAR(8)
                        ,RnkFirst INT
                        ,RnkLast  INT
						,TotalETE numeric(7,3)
						,CumulativeETE numeric(7,3)
                        ) 

IF @IsDisplayRedEyeFlight=0
BEGIN  
INSERT INTO @TempTable   
SELECT TT.TripID,0 WeekNumber
     ,TT.FleetID
      ,TT.TailNum
      ,TripNUM
	 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
	       WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'***'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
	       ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+DepICAOID+' | '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END 
		   Location
     ,FlightNUM
     ,PassengerTotal
     ,PassengerRequestorCD
     ,DepartmentCD
     ,AuthorizationCD
     ,FlightPurpose
     ,SeatsAvailabe
     ,CrewList
     ,ElapseTM
     ,TripStatus
     ,FlightCatagoryCD
     ,CONVERT(DATE,DepartureDTTMLocal) Date
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
     ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
      ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC) 
	  ,TE.TotalETE,0 CumulativeETE
      FROM @TableTrip TT
            LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
		    LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)   
	  LEFT OUTER JOIN @TotalEteTemp TE ON TE.TRIPID=TT.TripID  
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum
END
ELSE
BEGIN
INSERT INTO @TempTable 
SELECT TEMP.*
      ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
      ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC) 
      FROM(
 SELECT TT.TripID,0 WeekNumber
       ,TT.FleetID
      ,TT.TailNum
      ,TripNUM
	 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
	       WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'***'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
	       ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+DepICAOID+(CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN ' [continued]' ELSE  ' | '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END) END Location
     ,FlightNUM
     ,PassengerTotal
     ,PassengerRequestorCD
     ,DepartmentCD
     ,AuthorizationCD
     ,FlightPurpose
     ,SeatsAvailabe
     ,CrewList
     ,ElapseTM
     ,TripStatus
     ,FlightCatagoryCD
     ,CONVERT(DATE,DepartureDTTMLocal) Date
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,NULL IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
		,TE.TotalETE,0 CumulativeETE
      FROM @TableTrip TT 
	  LEFT OUTER JOIN @TotalEteTemp TE ON TE.TRIPID=TT.TripID 
            LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
			LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)


 UNION ALL

SELECT TT.TripID,0 WeekNumber
      ,TT.FleetID
      ,TT.TailNum
      ,TripNUM
	 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
	       WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'***'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
	       ELSE ' [continued] '  +ArrivelICAOID+' | '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
     ,FlightNUM
     ,PassengerTotal
     ,PassengerRequestorCD
     ,DepartmentCD
     ,AuthorizationCD
     ,FlightPurpose
     ,SeatsAvailabe
     ,CrewList
     ,ElapseTM
     ,TripStatus
     ,FlightCatagoryCD
     ,CONVERT(DATE,ArrivalDTTMLocal)
     ,ArrivalDTTMLocal---DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,NULL  IsArrival  
     ,LegNum
     ,2 AS SORTORDER 
     ,TT.RecordTye
     ,TT.TypeCode
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)	
	,TE.TotalETE,0 CumulativeETE
      FROM @TableTrip TT 
	  LEFT OUTER JOIN @TotalEteTemp TE ON TE.TRIPID=TT.TripID   
            LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
			LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
       AND CONVERT(DATE,ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
       AND CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal)
 )TEMP
ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
END
INSERT INTO @CumTemp(TripId,LegNUM, CumulativeETE)  
	SELECT 
		a.TripId, 	
		a.LegNum,
		ISNULL(SUM(b.ElapseTM),0) ElapseTM
	FROM 
		@TempTable a,
		@TempTable b
	WHERE a.TripId = b.TripId AND
		b.LegNUM <= a.LegNUM
	Group By a.TripId,a.LegNum,a.ElapseTM
	order by a.LegNUM 
	--select * from @TempTable
	--select * from @CumTemp
UPDATE t 
SET t.CumulativeETE = c.CumulativeETE
from  
 @TempTable as t 
 inner join @CumTemp as c on c.TripId = t.TripID and c.LegNum = t.LegNum

 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;


SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 

IF EXISTS(SELECT FleetID FROM @TempTable)
BEGIN

WHILE (@WEEKNUM<=@WEEK)
 BEGIN
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)
  WHILE @DateVal < @BeginDate
       BEGIN
  
		   UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=CONVERT(DATE,@DateVal)
		   INSERT INTO @TempTable(FleetID,TailNum,WeekNumber,Date,TypeCode,RnkFirst,RnkLast)
			SELECT DISTINCT F.FleetID,F.TailNum,@WEEKNUM,@DateVal,TT.TypeCode,1,1 FROM Fleet F 
														  INNER JOIN @TempTable TT ON TT.FleetID=F.FleetID
													  WHERE F.IsDeleted=0
													   AND F.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
													   AND F.IsInActive=0
													   AND F.FleetID NOT IN(SELECT ISNULL(FleetID,0) FROM @TempTable WHERE WeekNumber=@WEEKNUM AND Date=@DateVal)  
	             
			SET @DateVal=DATEADD(DAY,1,@DateVal)
       END
  SET @WEEKNUM=@WEEKNUM+1
  END
END

ELSE

BEGIN

WHILE (@WEEKNUM<=@WEEK)
 BEGIN
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)
  WHILE @DateVal < @BeginDate
       BEGIN
       
		   UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=CONVERT(DATE,@DateVal)
		   INSERT INTO @TempTable(WeekNumber,Date,RnkFirst,RnkLast)
		   SELECT DISTINCT @WEEKNUM,@DateVal,1,1
	             
			SET @DateVal=DATEADD(DAY,1,@DateVal)
       END
  SET @WEEKNUM=@WEEKNUM+1
  END



END



IF @IsFirstLastLeg=0
BEGIN

SELECT CASE WHEN Date BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN 0 ELSE 1 END DateVal
      ,WeekNumber
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,FleetID
      ,TypeCode
      ,TailNum
      ,Date SortDate
      ,LEFT(datename(dw,DATE),3)+' '+dbo.GetShortDateFormatByUserCD(@UserCD,Date) Date
      ,IsArrival,CASE WHEN SUBSTRING(location,1,3)='(R)' THEN NULL ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE NULL END) END orig_nmbr
      ,location
      ,CASE WHEN @IsFlightNo=1 AND SUBSTRING(location,1,3)<>'(R)' AND location IS NOT NULL AND RecordType='T'   THEN  'FlightNo: '+ISNULL(FlightNUM,'') ELSE NULL END FlightNUM
	  ,CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE NULL END pax_total
	  ,CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Req: '+ISNULL(reqcode,'') ELSE NULL END reqcode1
	  ,CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Dept: '+ISNULL(DepartmentCD,'') ELSE NULL END DepartmentCD
	  ,CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Auth: '+ISNULL(AuthorizationCD,'') ELSE NULL END AuthorizationCD
	  ,CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Purpose: '+ISNULL(FlightPurpose,'') END FlightPurpose
	  ,CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END SeatsAvailable
	  ,crewlist=CASE WHEN FleetID IS NOT NULL THEN crewlist ELSE NULL END
	  ,CASE WHEN @IsShowTrip=1 AND @IsETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.ElapseTM,0)*10)*0.1) ELSE NULL END ElapseTM
	  ,CASE WHEN @IsShowTrip=1 AND @IsTotalETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.TotalETE,0)*10)*0.1) ELSE NULL END TotalETE
	  ,CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'Cumm ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.CumulativeETE,0)*10)*0.1) ELSE NULL END CummETE
	  ,CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE NULL END TripStatus
	  ,CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  '('+FlightCatagoryCD+')' ELSE NULL END FlightCatagoryCD
      ,RnkFirst
      ,RnkLast
      ,TT.FgColor
      ,TT.BgColr
 FROM @TempTable TT
 LEFT OUTER JOIN @TotalEteTemp TE ON TT.TRIPID=TE.TripID
 WHERE DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY WeekNumber,SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
END

ELSE


BEGIN

SELECT CASE WHEN Date BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN 0 ELSE 1 END DateVal
      ,WeekNumber
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,FleetID
      ,TypeCode
      ,TailNum
      ,Date SortDate
      ,LEFT(datename(dw,DATE),3)+' '+dbo.GetShortDateFormatByUserCD(@UserCD,Date) Date
      ,IsArrival,CASE WHEN SUBSTRING(location,1,3)='(R)' THEN NULL ELSE (CASE WHEN @IsShowTrip=1 AND RecordType='T' THEN  'Trip: '+CONVERT(VARCHAR(10),TripNUM) ELSE NULL END) END orig_nmbr
      ,location
      ,CASE WHEN @IsFlightNo=1 AND SUBSTRING(location,1,3)<>'(R)' AND location IS NOT NULL AND RecordType='T'   THEN  'FlightNo: '+ISNULL(FlightNUM,'') ELSE NULL END FlightNUM
	  ,CASE WHEN @IsPaxCount=1 AND SUBSTRING(location,1,3)<>'(R)'  AND RecordType='T'  THEN 'Pax: '+CONVERT(VARCHAR(10),pax_total) ELSE NULL END pax_total
	  ,CASE WHEN @IsRequestor=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Req: '+ISNULL(reqcode,'') ELSE NULL END reqcode1
	  ,CASE WHEN @IsDepartment=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Dept: '+ISNULL(DepartmentCD,'') ELSE NULL END DepartmentCD
	  ,CASE WHEN @IsAuthorization=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Auth: '+ISNULL(AuthorizationCD,'') ELSE NULL END AuthorizationCD
	  ,CASE WHEN @IsLegPurpose=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Purpose: '+ISNULL(FlightPurpose,'') END FlightPurpose
	  ,CASE WHEN @IsSeatAvailable=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN 'Seats Avail: '+CONVERT(VARCHAR(10),SeatsAvailable) ELSE NULL END SeatsAvailable
	  ,crewlist=CASE WHEN FleetID IS NOT NULL THEN crewlist ELSE NULL END
	  ,CASE WHEN @IsShowTrip=1 AND @IsETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.ElapseTM,0)*10)*0.1) ELSE NULL END ElapseTM
	  ,CASE WHEN @IsShowTrip=1 AND @IsTotalETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'Total Trip ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.TotalETE,0)*10)*0.1) ELSE NULL END TotalETE
	  ,CASE WHEN @IsShowTrip=1 AND @IsCummulativeETE=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN  'Cumm ETE: '+CONVERT(VARCHAR(10),FLOOR(ISNULL(TT.CumulativeETE,0)*10)*0.1) ELSE NULL END CummETE
	  ,CASE WHEN @IsShowTrip=1 AND @IsShowTripStatus=1 AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'   THEN '('+TripStatus+')' ELSE NULL END TripStatus
	  ,CASE WHEN @IsFlightCategory=1  AND SUBSTRING(location,1,3)<>'(R)' AND RecordType='T'  THEN  '('+FlightCatagoryCD+')' ELSE NULL END FlightCatagoryCD
      ,RnkFirst
      ,RnkLast
      ,TT.FgColor
      ,TT.BgColr
 FROM @TempTable TT
 LEFT OUTER JOIN @TotalEteTemp TE ON TE.TRIPID=TT.TripID
  WHERE (RnkFirst=1
   OR RnkLast=1) 
   AND DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY WeekNumber,SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
END



 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
 DROP TABLE #TempTable
 END
GO
