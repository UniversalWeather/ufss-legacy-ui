IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar3Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar3Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar3Information]  
  @UserCD VARCHAR(30) --Mandatory  
 ,@DATEFROM DATETIME --Mandatory  
 ,@DATETO DATETIME --Mandatory  
 ,@TailNum VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values  
 ,@FleetGroupCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values  
 ,@AircraftCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values  
 ,@IsHomeBase BIT = 0  
   
AS  
-- ===============================================================================  
-- SPC Name: spGetReportPREAircraftCalendar3Information  
-- Author: AISHWARYA.M  
-- Create date: 11 Jul 2012  
-- Description: Get Preflight Aircraft Calendar information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
-- ================================================================================  
SET NOCOUNT ON  
  
  
-----------------------------TailNum and Fleet Group Filteration----------------------  
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
  
Declare @SuppressActivityAircft BIT    
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMER  
           AND IsZeroSuppressActivityAircftRpt IS NOT NULL  
           AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
CREATE TABLE  #TempFleetID     
 (     
  ID INT NOT NULL IDENTITY (1,1),   
  FleetID BIGINT  
  )  
    
  
IF @TailNUM <> ''  
BEGIN  
 INSERT INTO #TempFleetID  
 SELECT DISTINCT F.FleetID   
 FROM Fleet F  
 WHERE F.CustomerID = @CUSTOMER  
 AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))  
END  
  
IF @FleetGroupCD <> ''  
BEGIN    
INSERT INTO #TempFleetID  
 SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
 FROM Fleet F   
 LEFT OUTER JOIN FleetGroupOrder FGO  
 ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
 LEFT OUTER JOIN FleetGroup FG   
 ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
 WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
 AND F.CustomerID = @CUSTOMER    
END  
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''  
BEGIN    
INSERT INTO #TempFleetID  
 SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
 FROM Fleet F   
 WHERE  F.CustomerID = @CUSTOMER  
   AND F.IsDeleted=0  
   AND F.IsInActive=0    
END  
-----------------------------TailNum and Fleet Group Filteration----------------------  
  
   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
 DECLARE @ParameterDefinition AS NVARCHAR(100)  
  
 DECLARE @NextString NVARCHAR(40)  
 DECLARE @Pos INT  
 DECLARE @NextPos INT  
 DECLARE @String NVARCHAR(40)  
 DECLARE @Delimiter NVARCHAR(40)  
 DECLARE @Loopdate DATETIME  
  
 CREATE TABLE #TempTailNum  (  
 ID INT identity(1, 1) NOT NULL  
 ,FleetID BIGINT  
 ,TailNum VARCHAR(25)  
 ,Type_Code CHAR(4)  
 )  
  
 SET @String = @TailNum  
 SET @Delimiter = ','  
 SET @String = @String + @Delimiter  
 SET @Pos = charindex(@Delimiter, @String)  
   
 CREATE TABLE #TempTable  (  
  ID INT identity(1, 1) NOT NULL  
  ,[Date] DATETIME  
  ,[CustomerID] BIGINT  
  ,[TailNo] VARCHAR(9)  
  ,[From] CHAR(4)  
  ,[To] CHAR(4)  
  ,DepartureDTTMLocal DATETIME  
  ,ArrivalDTTMLocal DATETIME  
  ,[NextLocalDTTM] DATETIME  
  ,[Duty] CHAR(2)  
  ,[Requestor] VARCHAR(700)   
  ,[Phone] VARCHAR(25)  
  ,[Pax] INT  
  ,[Crew] VARCHAR(500)  
  ,[Comments] VARCHAR(40)  
  ,LegID BIGINT  
  ,HomebaseID BIGINT  
  ,Type_Code VARCHAR(4)  
  ,TripNum BIGINT  
  ,TripID BIGINT  
  ,LegNUM BIGINT  
  ,DType CHAR(1)  
  ,RecordType CHAR(2)  
  ,FleetID BIGINT  
  )  
  
 DECLARE  @TEMPTODAY AS DATETIME = @DATEFROM  
    
  
  INSERT INTO #TempTailNum (FleetID,TailNum,Type_Code)  
   SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD  
   FROM Fleet F WHERE FleetID IN(SELECT FleetID FROM #TempFleetID)  
   AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
      AND (F.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)  
  
  
   
  
  
 INSERT INTO #TempTable  
  ( [Date]  
  ,[TailNo]  
  ,[From]  
  ,[To]    
  ,DepartureDTTMLocal  
  ,ArrivalDTTMLocal    
  ,[NextLocalDTTM]    
  ,[Duty]   
  ,[Requestor]  
  ,[Phone]  
  ,[Pax]  
  ,[Crew]  
  ,[Comments]  
  ,CustomerID  
  ,LegID  
  ,Type_Code  
  ,TripNum  
  ,TripID  
  ,LegNum  
  ,RecordType  
  ,FleetID  
  
  )  
    
  SELECT CONVERT(DATE,PT.DepartureDTTMLocal)  
   ,PT.TailNum   
   ,PT.DepartICAOID   
   ,PT.ArriveICAOID   
   ,PT.DepartureDTTMLocal   
   ,CASE WHEN CONVERT(DATE,PT.DepartureDTTMLocal)=CONVERT(DATE,PT.ArrivalDTTMLocal)  THEN PT.ArrivalDTTMLocal ELSE   
   (CASE WHEN RecordType='M' THEN  (CONVERT(DATETIME, CONVERT(VARCHAR(20),PT.DepartureDTTMLocal, 101) + ' 23:59')) ELSE  PT.ArrivalDTTMLocal END) END  
   ,PT.NextLocalDTTM  
   ,CASE WHEN ISNULL(PT.DutyTYPE, '') = '' THEN   
          CASE WHEN ISNULL(PT.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END  
            ELSE PT.DutyTYPE END  
   ,CASE WHEN PT.LastName <> '' AND PT.LastName IS NOT NULL THEN PT.LastName ELSE '' END + ISNULL(PT.FirstName,'') + ' ' +ISNULL(PT.MiddleInitial,'')   
   ,PT.PhoneNum  
   ,PT.PassengerTotal   
   ,NULL--( SUBSTRING(CrewList,1,LEN(CrewList)-1))  
   ,PT.TripDescription  
   ,PT.CustomerID  
   ,PT.LegID  
   ,PT.Ac_Code   
   ,PT.TripNUM  
   ,PT.TripID   
   ,PT.LegNUM  
   ,PT.RecordType  
   ,PT.FleetID   
   FROM  [dbo].[vPreflightTrans] PT  
   INNER JOIN #TempFleetID TF ON PT.FleetID=TF.FleetID  
   WHERE  CONVERT(DATE,PT.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
    AND PT.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
    AND PT.TripStatus IN ('T','H') 
    AND (PT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='') 
    AND (PT.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)  
  
  
   
  INSERT INTO #TempTable  
  ( [Date]  
  ,[TailNo]  
  ,[From]  
  ,[To]    
  ,DepartureDTTMLocal  
  ,ArrivalDTTMLocal    
  ,[NextLocalDTTM]    
  ,[Duty]   
  ,[Requestor]  
  ,[Phone]  
  ,[Pax]  
  ,[Crew]  
  ,[Comments]  
  ,CustomerID  
  ,LegID  
  ,Type_Code  
  ,TripNum  
  ,TripID  
  ,LegNum  
  ,RecordType  
  ,FleetID  
  
  )  
 SELECT CONVERT(DATE,PT.ArrivalDTTMLocal)  
   ,PT.TailNum   
   ,PT.DepartICAOID   
   ,PT.ArriveICAOID   
   ,CONVERT(DATETIME, CONVERT(VARCHAR(20),PT.DepartureDTTMLocal, 101) + ' 00:00')  
   ,PT.ArrivalDTTMLocal   
   ,PT.NextLocalDTTM  
   ,CASE WHEN ISNULL(PT.DutyTYPE, '') = '' THEN   
          CASE WHEN ISNULL(PT.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END  
            ELSE PT.DutyTYPE END  
   ,CASE WHEN PT.LastName <> '' AND PT.LastName IS NOT NULL THEN PT.LastName ELSE '' END + ISNULL(PT.FirstName,'') + ' ' +ISNULL(PT.MiddleInitial,'')   
   ,PT.PhoneNum  
   ,PT.PassengerTotal   
   ,NULL--( SUBSTRING(CrewList,1,LEN(CrewList)-1))  
   ,PT.TripDescription  
   ,PT.CustomerID  
   ,PT.LegID  
   ,PT.Ac_Code   
   ,PT.TripNUM  
   ,PT.TripID   
   ,PT.LegNUM  
   ,PT.RecordType  
   ,PT.FleetID   
   FROM  [dbo].[vPreflightTrans] PT  
   INNER JOIN #TempFleetID TF ON PT.FleetID=TF.FleetID  
   WHERE  CONVERT(DATE,PT.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
          AND PT.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
    AND PT.TripStatus IN ('T','H') 
    AND PT.RecordType='M'  
    AND CONVERT(DATE,PT.DepartureDTTMLocal)<>CONVERT(DATE,PT.ArrivalDTTMLocal)  
    AND (PT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='') 
    AND (PT.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)  
  
  
  
UPDATE XX SET Crew=( SUBSTRING(CrewList,1,LEN(CrewList)-1))  
     FROM #TempTable XX  
   LEFT OUTER JOIN ( SELECT DISTINCT PC2.tripid, CrewList = (    
         SELECT DISTINCT C.CrewCD + ','    
         FROM PreflightCrewList PC1   
         INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0  
         INNER JOIN Crew C ON PC1.CrewID = C.CrewID    
         WHERE Pl.tripid = PC2.tripid    
         FOR XML PATH('')    
         )             
        FROM PreflightLeg PC2)CR ON CR.TripID=XX.TripID     
       WHERE CR.TripID=XX.TripID        
  
INSERT INTO #TempTable( [Date],[TailNo],[From],[To] ,DepartureDTTMLocal,ArrivalDTTMLocal,[NextLocalDTTM],[Duty],[Requestor],[Phone]  
                 ,[Pax],[Crew],[Comments],CustomerID,LegID,Type_Code,TripNum,TripID,RecordType,FleetID)  
SELECT CONVERT(DATETIME,CONVERT(DATE,PL.DepartureDTTMLocal)) DepartureDTTMLocal,T.TailNo,AD.IcaoID,AA.IcaoID,PL.DepartureDTTMLocal,  
          PL.ArrivalDTTMLocal Arrive, PL.NextLocalDTTM,T.Duty,T.Requestor,T.Phone,PL.PassengerTotal,T.Crew,T.Comments,  
          T.CustomerID,PL.LegID,T.Type_Code,T.TripNum,T.TripID,T.RecordType,T.FleetID  
                                             FROM #TempTable T   
                                             INNER JOIN PreflightMain PM ON T.TripID=PM.TripID AND PM.IsDeleted = 0  
                                             INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
                                             LEFT OUTER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID  
                                             LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID=AA.AirportID  
                                             WHERE PL.LegNUM=T.LegNUM-1  
                                               AND T.RecordType='T'  
  
  
DECLARE @CurDate TABLE(DateWeek DATE,TailNum VARCHAR(9),FleetID BIGINT,AC_CODE VARCHAR(4))  
  
INSERT INTO @CurDate(TailNum,DateWeek,FleetID,AC_CODE)  
SELECT TT.TailNum,T.date,TT.FleetID,TT.Type_Code FROM #TempTailNum TT CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@DATEFROM,@DATETO)) T  
  
  
  
 INSERT INTO #TempTable([Date],   
                        CustomerID,   
                        TailNo,   
                        [From],   
                        [To],   
                        DepartureDTTMLocal,    
                        ArrivalDTTMLocal,   
                        NextLocalDTTM,  
            Duty,  
            Requestor,  
            Phone,  
            Pax,   
            Crew,   
            Comments,  
            Type_Code,  
            LegID,  
            FleetID,  
            TripNum)  
   SELECT DISTINCT CD.DateWeek  
     ,TT.CustomerID  
     ,TT.TailNo  
     ,TT.[TO]  
     ,TT.[TO]  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00')) END  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END  
     ,'R'   
     ,TT.Requestor  
     ,TT.Phone  
     ,TT.Pax  
     ,TT.Crew  
     ,TT.Comments  
     ,TT.Type_Code  
     ,TT.LegID  
     ,TT.FleetID  
     ,TT.TripNum  
                    FROM #TempTable TT  
                    LEFT OUTER JOIN @CurDate CD ON CD.FleetID=TT.FleetID  
         WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,TT.ArrivalDTTMLocal)  
           AND CONVERT(DATE,CD.DateWeek) <  CONVERT(DATE,TT.NextLocalDTTM)   
           AND RecordType='T'  
  
  
 INSERT INTO #TempTable([Date],   
                        CustomerID,   
                        TailNo,   
                        [From],   
                        [To],   
                        DepartureDTTMLocal,    
                        ArrivalDTTMLocal,   
                        NextLocalDTTM,  
            Duty,  
            Requestor,  
            Phone,  
            Pax,   
            Crew,   
            Comments,  
            Type_Code,  
            LegID,  
            FleetID,  
            TripNum)  
   SELECT DISTINCT CD.DateWeek  
     ,TT.CustomerID  
     ,TT.TailNo  
     ,TT.[TO]  
     ,TT.[TO]  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00')) END  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END  
     ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END  
     ,Duty   
     ,TT.Requestor  
     ,TT.Phone  
     ,TT.Pax  
     ,TT.Crew  
     ,TT.Comments  
     ,TT.Type_Code  
     ,TT.LegID  
     ,TT.FleetID  
     ,TT.TripNum  
                    FROM #TempTable TT  
                    LEFT OUTER JOIN @CurDate CD ON CD.FleetID=TT.FleetID  
         WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,TT.DepartureDTTMLocal)  
           AND CONVERT(DATE,CD.DateWeek) <  CONVERT(DATE,TT.ArrivalDTTMLocal)   
           AND RecordType='M'  
  
  
IF @SuppressActivityAircft=0  
BEGIN  
 INSERT INTO #TempTable(TailNo,Date,Type_Code,FleetID,[From])  
SELECT T1.TailNum,T1.DateWeek,T1.AC_CODE,T1.FleetID,'' FROM @CurDate T1  
                      --OUTER APPLY dbo.GetFleetCurrentAirpotCD(T1.DateWeek, T1.FleetID) AS A  
                      WHERE NOT EXISTS(SELECT T.TailNo FROM #TempTable T WHERE T.FleetID = T1.FleetID AND CONVERT(DATE,T.[Date]) = CONVERT(DATE,T1.DateWeek))  
  
END  
DECLARE  @TempTable TABLE   (  
   ID INT identity(1, 1) NOT NULL  
  ,[Date] DATETIME  
  ,[TailNo] VARCHAR(9)  
  ,[From] CHAR(4)  
  ,[To] CHAR(4)  
  ,DepartureDTTMLocal DATETIME  
  ,ArrivalDTTMLocal DATETIME  
  ,[Duty] CHAR(2)  
  ,[Requestor] VARCHAR(700)   
  ,[Phone] VARCHAR(25)  
  ,[Pax] INT  
  ,[Crew] VARCHAR(500)  
  ,[Comments] VARCHAR(40)  
  ,LegID BIGINT  
  ,Type_Code VARCHAR(4)  
  ,TripNum BIGINT  
  ,FleetID BIGINT  
  )        
INSERT INTO @TempTable  
SELECT DISTINCT  [Date]  
  ,[TailNo]   
  ,[From]   
  ,[To]   
  ,DepartureDTTMLocal   
  ,ArrivalDTTMLocal   
  ,[Duty]  
  ,[Requestor]  
  ,[Phone]   
  ,[Pax]   
  ,[Crew]   
  ,[Comments]  
  ,[LegID] = CONVERT(VARCHAR(30),ISNULL(LegID,''))  
  ,Type_Code  
  ,TripNum,FleetID FROM #TempTable  
         WHERE Date BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)       
         ORDER BY Type_Code,[Date],DepartureDTTMLocal  
  
DECLARE @HomeBaseTable TABLE(FleetID BIGINT,  
                             ArrivalDTTMLocal DATE,  
                             IcaoID CHAR(4),  
                             Rnk INT)  
  
INSERT INTO @HomeBaseTable  
SELECT PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ArrivalDTTMLocal,A.IcaoID,  
 ROW_NUMBER()OVER(PARTITION BY PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ORDER BY PM.FleetID,ArrivalDTTMLocal DESC) Rnk  
  FROM PreflightLeg PL  
  INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted = 0  
  INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID  
  INNER JOIN #TempFleetID TF ON PM.FleetID=TF.FleetID  
  WHERE PM.TRIPSTATUS IN ('T','H')   
    AND PL.ArrivalDTTMLocal<=@DATETO  
    AND PL.IsDeleted = 0  
  ORDER BY ArrivalDTTMLocal DESC  
  
  
      
UPDATE XX SET [From]=T.IcaoID  
     FROM @TempTable XX  
   INNER JOIN @HomeBaseTable T ON T.FleetID=XX.FleetID AND Rnk = 1  
           WHERE T.FleetID=XX.FleetID   
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.Date)  
            AND ISNULL([From],'') = ''  
  
 UPDATE XX SET [From]=IcaoID  
     FROM @TempTable XX  
   INNER JOIN (SELECT A.IcaoID,FleetID FROM FLEET F   
                                   INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID  
                                   INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID  
                                   WHERE F.CustomerID = @CUSTOMER)HB ON HB.FleetID=XX.FleetID  
       WHERE HB.FleetID=XX.FleetID   
        AND ISNULL([From],'') = ''  
           
  
SELECT [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)  
       ,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])  
        ,[Date]  
  ,[TailNo]   
  ,[From]  
  ,[To]  
  ,DepartureDTTMLocal  
  ,ArrivalDTTMLocal  
  ,[Duty]  
  ,[Requestor]  
  ,[Phone]   
  ,[Pax]  
  ,[Crew]   
  ,[Comments]  
  ,[LegID] = CONVERT(VARCHAR(30),ISNULL(LegID,''))  
  ,Type_Code  
  ,TripNum   
  ,FleetID  FROM @TempTable  
      ORDER BY  Type_Code,[Date],DepartureDTTMLocal,ArrivalDTTMLocal  
  
IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL  
DROP TABLE #TempTailNum  
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL  
DROP TABLE #TempTable  
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL  
DROP TABLE #TempFleetID  
  
  
 -- EXEC spGetReportPREAircraftCalendar3Information 'TIM', '2012-10-01', '2012-10-31', 'N771AV', '', ''  
 -- EXEC spGetReportPREAircraftCalendar3Information 'ELIZA_9', '2012-10-10', '2012-11-30', 'N771AV', '', ''  
 -- EXEC spGetReportPREAircraftCalendar3Information 'JWILLIAMS_13', '2011-07-01', '2011-07-16', '', '', ''  

GO


