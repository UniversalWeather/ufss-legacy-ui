IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFlightFollowingLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFlightFollowingLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================      
-- SPC Name: spGetReportPREFlightFollowingLogInformation     
-- Author: Abhishek S      
-- Create date: 1st Aug 2012      
-- Description: Get Flight Following Information    
-- Revision History      
-- Date   Name  Ver  Change      
--       
-- ================================================================================   
CREATE PROCEDURE [dbo].[spGetReportPREFlightFollowingLogInformation]  
  
        @UserCD AS VARCHAR(30), -- Mandatory  
        @Date AS DATETIME, --Mandatory  
        @TailNum AS NVARCHAR(300)='', -- [Optional], Comma Delimited String with multiple values  
        @FleetGroupCD AS NVARCHAR(300)='' -- [Optional], Comma Delimited String with multiple values  
  
AS  
BEGIN   
 SET NOCOUNT ON;  
   
 DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
    DECLARE @ParameterDefinition AS NVARCHAR(500)  
   
-- DECLARE  @ICAOID_N NVARCHAR(500)   
-- SET @ICAOID_N=(SELECT AirportID FROM Airport WHERE ICAOID=@ICAOID)  
--    SET @ICAOID_N=(SELECT AirportID FROM Airport WHERE ICAOID=PL.DepartICAOID)  
   
 SET @SQLSCRIPT = '  
 SELECT  
 [LegID] = PL.LegID   
   --,[Date] = CONVERT(varchar(8),PL.DepartureDTTMLocal,1) 
   ,[Date] = dbo.GetShortDateFormatByUserCD(@UserCD,@Date)  
   ,[TAIL NO] = F.TailNum   
   ,[TSHEET] = PM.TripNUM   
   ,[CREW] =  (SELECT SUBSTRING(C.CrewList,1,LEN(C.CrewList)-1)) 
   ,[SFTLOC1] = CONVERT(VARCHAR(5),PL.DepartureDTTMLocal,108)  
   ,[SFTLOC2] = (SELECT IcaoID FROM Airport WHERE AirportID=PL.DepartICAOID)  
   ,[SFTLOC3] = (SELECT IcaoID FROM Airport WHERE AirportID=PL.ArriveICAOID)  
   ,[SFTLOC4] = CONVERT(VARCHAR(5),PL.ArrivalDTTMLocal,108)  
   ,[SFTUTC1] = CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,108)  
   ,[SFTUTC2] = CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,108)  
   ,[OUT OFF] = ''''  
   ,[ETE] = '''' 
   ,[FOB] = ''''  
   ,[PAX] = ''''  
   ,[TNS] = ''''  
   ,[ETA] = ''''  
   ,[ON IN] = ''''  
   ,[FLT/BLK] = ''''  
   ,[MX/DTY] = ''''  
   ,[DLY] = ''''  
   ,[REMARKS] = '''' 
    FROM PreflightMain PM  
       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
       
       INNER JOIN (    
              -- SELECT DISTINCT PC2.tripid, CrewList = (  
                 SELECT DISTINCT PC2.LEGID, CrewList = (   
                 SELECT C.CrewCD + '', ''  
				 FROM PreflightCrewList PC1 INNER JOIN Crew C ON PC1.CrewID = C.CrewID 
				-- INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
				-- WHERE Pl.tripid = PC2.tripid 
				   WHERE PC1.LEGID = PC2.LEGID 
				   ORDER BY C.CrewCD 
                   FOR XML PATH('''')    
            )
            FROM PreflightCrewList PC2
		) C ON PL.LEGID = C.LEGID    
			-- FROM PreflightLeg PC2    
			-- ) C ON PM.tripid = C.tripid 
			
      INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD, F.AircraftID
				FROM Fleet F 
				LEFT OUTER JOIN FleetGroupOrder FGO
				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG 
				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
			WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
					OR @FleetGroupCD = '''' )
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
    INNER JOIN(SELECT AircraftID, AircraftCD FROM Aircraft)AT  
       ON AT.AircraftID = F.AircraftID      
    WHERE PM.IsDeleted = 0 
      AND PM.TripStatus = ''T'' 
      AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
      AND convert(date,PL.DepartureDTTMLocal) = convert(date,@Date)'  
     
    
        
-- Optional Clauses  
--PRINT @SQLSCRIPT  
  
    IF @TailNUM <> '' BEGIN    
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';  
 END  
 --IF @FleetGroupCD <> '' BEGIN    
 -- SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';  
 --END   
   
   
 SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATE AS DATETIME, @FleetGroupCD VARCHAR(500) '  
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATE, @FleetGroupCD    
  
      
END  
  
-- EXEC spGetReportPREFlightFollowingLogInformation 'JWILLIAMS_11','2007-09-04','',''  
-- EXEC spGetReportPREFlightFollowingLogInformation 'JWILLIAMS_13','2012-09-03','',''  

  
  --dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)

GO


