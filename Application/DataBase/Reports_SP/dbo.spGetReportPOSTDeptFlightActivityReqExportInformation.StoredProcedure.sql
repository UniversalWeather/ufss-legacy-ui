IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDeptFlightActivityReqExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDeptFlightActivityReqExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTDeptFlightActivityReqExportInformation]      
(       
@UserCD AS VARCHAR(30),      
@YEAR  AS INT,  
@TailNum AS VARCHAR(30),  
@FleetGroupCD AS VARCHAR(30),  
@DepartmentCD AS VARCHAR(30),  
@IsHomebase AS bit=0     
)      
-- ===============================================================================      
-- SPC Name: spGetReportPOSTDeptFlightActivityReqExportInformation      
-- Author:D.Mullai        
-- Create date:     
-- Description: Get Dept Flight Activityby Req  information for REPORTS      
-- Revision History      
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package   
-- ================================================================================      
       
 AS      

 BEGIN      
      
DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	--SET @IsZeroSuppressActivityDeptRpt=1																			 
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
create table #depexptemp(TAILNUM varchar(9),POLogID BIGINT,POLEgID BIGINT,DepartmentCD varchar(8),DepartmentName varchar(25),PassengerName varchar(63),PassengerRequestorCD varchar(5),
DEPT VARCHAR(60),flthrs1 numeric(6,3),flthrs2 numeric(6,3),flthrs3 numeric(6,3),flthrs4 numeric(6,3),flthrs5 numeric(6,3),flthrs6 numeric(6,3),flthrs7 numeric(6,3),flthrs8 numeric(6,3)
,flthrs9 numeric(6,3),flthrs10 numeric(6,3),flthrs11 numeric(6,3),flthrs12 numeric(6,3),nmiles1 numeric(5,0),nmiles2 numeric(5,0),nmiles3 numeric(5,0),nmiles4 numeric(5,0),nmiles5 numeric(5,0),
nmiles6 numeric(5,0),nmiles7 numeric(5,0),nmiles8 numeric(5,0),nmiles9 numeric(5,0),nmiles10 numeric(5,0),nmiles11 numeric(5,0),nmiles12 numeric(5,0))


create table #depexptemp1(TAILNUM varchar(9),POLogID BIGINT,POLEgID BIGINT,DepartmentCD varchar(8),DepartmentName varchar(25),PassengerName varchar(63),PassengerRequestorCD varchar(5),
DEPT VARCHAR(60),flthrs1 numeric(6,3),flthrs2 numeric(6,3),flthrs3 numeric(6,3),flthrs4 numeric(6,3),flthrs5 numeric(6,3),flthrs6 numeric(6,3),flthrs7 numeric(6,3),flthrs8 numeric(6,3)
,flthrs9 numeric(6,3),flthrs10 numeric(6,3),flthrs11 numeric(6,3),flthrs12 numeric(6,3),nmiles1 numeric(5,0),nmiles2 numeric(5,0),nmiles3 numeric(5,0),nmiles4 numeric(5,0),nmiles5 numeric(5,0),
nmiles6 numeric(5,0),nmiles7 numeric(5,0),nmiles8 numeric(5,0),nmiles9 numeric(5,0),nmiles10 numeric(5,0),nmiles11 numeric(5,0),nmiles12 numeric(5,0))

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
    )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @StartDate DATE     
SET @StartDate= CAST(RTRIM(Convert(varchar(10),@YEAR)) AS DATETIME) 

INSERT INTO #depexptemp

  SELECT DISTINCT F.TAILNUM,PM.POLogID,PL.POLegID,ISNULL(D.DepartmentCD,'ZZZ@####'),D.DepartmentName,P.PassengerName,P.PassengerRequestorCD,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = DATEPART(month,@StartDate) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))) flthrs1,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+1) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+1) flthrs2,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+2) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+2) flthrs3,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+3) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+3) flthrs4,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+4) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+4) flthrs5,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+5) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+5) flthrs6,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+6) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+6) flthrs7,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+7) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+7) flthrs8,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+8) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+8) flthrs9,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+9) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+9) flthrs10,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+10) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+10) flthrs11,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+11) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+11) flthrs12,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = DATEPART(month,@StartDate) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))) nmiles1,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+1) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+1) nmiles2,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+2) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+2) nmiles3,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+3) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+3) nmiles4,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+4) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+4) nmiles5,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+5) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+5) nmiles6,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+6) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+6) nmiles7,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+7) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+7) nmiles8,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+8) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+8) nmiles9,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+9) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+9) nmiles10,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+10) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(PL.Distance)) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+10) nmiles11,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+11) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ISNULL(PL.Distance,0))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) and DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+11) nmiles12                            
        FROM PostflightLeg PL   

        INNER JOIN PostflightMain PM on PL.POLogID = PM.POLogID  AND PM.IsDeleted=0
        INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
        LEFT OUTER JOIN Department D on PL.DepartmentID = D.DepartmentID  
 		INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.HomeBaseID
		FROM Fleet F 
		left outer JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		left outer JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		LEFT OUTER JOIN Passenger P on PL.PassengerRequestorID = P.PassengerRequestorID  
		WHERE PL.CustomerID =   CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		AND PL.IsDeleted=0
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
		AND (DATEPART(year,PL.ScheduledTM)= DATEPART(year,@StartDate))
        AND (PL.PassengerRequestorID is not null or PL.DepartmentID is Not null)
       GROUP BY F.TAILNUM,PM.POLogID,PL.POLegID,D.DepartmentCD,D.DepartmentName,P.PassengerName,P.PassengerRequestorCD,pl.ScheduledTM
 
  
--IF @TailNUM <> '' OR @FleetGroupCD <> ''

--BEGIN
--insert into #depexptemp1
--select DISTINCT null,null,null,D.DepartmentCD,D.DepartmentName,null,null,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
--null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
--from Department d
--LEFT OUTER JOIN PostflightMain PM on D.DepartmentID = PM.DepartmentID 
--INNER JOIN  (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
--END

--ELSE
--BEGIN
insert into #depexptemp1
select DISTINCT null,null,null,D.DepartmentCD,D.DepartmentName,null,null,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
from Department d
--LEFT OUTER JOIN PostflightMain PM on D.DepartmentID = PM.DepartmentID 
--INNER JOIN  (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
 WHERE d.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and d.IsDeleted=0 and d.IsInActive=0
 AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
 --END
delete from #depexptemp1 where DepartmentCD in(select distinct DepartmentCD from #depexptemp)
         
--SELECT * FROM #depexptemp1
--union all           
--select * from #depexptemp
-- order by PassengerName
 
  IF @IsZeroSuppressActivityDeptRpt = 0
BEGIN        
SELECT * FROM #depexptemp1
UNION ALL           
SELECT * FROM #depexptemp
ORDER BY PassengerName,TAILNUM
END
ELSE
BEGIN
SELECT * FROM #depexptemp
ORDER BY PassengerName,TAILNUM
END


IF OBJECT_ID('tempdb..#depexptemp') IS NOT NULL
DROP TABLE #depexptemp 	

IF OBJECT_ID('tempdb..#depexptemp1') IS NOT NULL
DROP TABLE #depexptemp1

 END
-- exec spGetReportPOSTDeptFlightActivityReqExportInformation 'jwilliams_11',2009,'','','',0




GO


