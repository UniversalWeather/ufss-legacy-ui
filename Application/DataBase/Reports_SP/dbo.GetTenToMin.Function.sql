 
IF object_id(N'GetTenToMin', N'FN') IS NOT NULL
    DROP FUNCTION GetTenToMin

go
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetTenToMin](@DecimalVal DECIMAL(10,2)) 
RETURNS DECIMAL(10,2)
--Declare @Hour decimal(10,2)
--SET @Hour = 4.2
--SELECT [dbo].[GetHourMinutes] (11.6)
AS BEGIN
DECLARE @time VARCHAR(10)
SET @time=(SELECT CONVERT(VARCHAR, CONVERT(INT,FLOOR(@DecimalVal)))+'.'+
REPLICATE('0',(2 - LEN(CONVERT (VARCHAR, CONVERT(INT,(@DecimalVal - FLOOR(@DecimalVal)) * 60.0)))))
+CONVERT (VARCHAR, CONVERT(INT,(@DecimalVal - FLOOR(@DecimalVal)) * 60.0)))
RETURN Convert(DECIMAL(10,2),@time)
END

