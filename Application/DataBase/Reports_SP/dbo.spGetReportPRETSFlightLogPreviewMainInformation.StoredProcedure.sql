IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFlightLogPreviewMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewMainInformation]  
    @UserCD  AS VARCHAR(30)     
    ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
    ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
    ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
    ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
    ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal      
    ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
    ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
    ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
    ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
    ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
    ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
    ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
    ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
    ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
    ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
    ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
    ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
    ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
    ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
      
AS  
BEGIN  
-- ================================================================================  
-- SPC Name: spGetReportPRETSFlightLogInformation  
-- Author: ASKAR  
-- Create date:27 SEP 2012  
-- Description: Get Preflight Tripsheet Flight Log information for REPORTS  
-- Revision History  
-- Date        Name        Ver        Change  
--   
-- =================================================================================  
  
    -- [Start] Report Criteria --  
    DECLARE @tblTripInfo AS TABLE (  
    TripID BIGINT  
    , LegID BIGINT  
    , PassengerID BIGINT  
    )  
  
    INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
    EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
        @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
        @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
        @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
    -- [End] Report Criteria -- 
 SELECT DISTINCT [TRIPID] = CONVERT(VARCHAR, TRIPID) FROM @tblTripInfo
 END  
  
--EXEC spGetReportPRETSFlightLogPreviewMainInformation 'TIM','179','','','','','','','','','','','','','','','','','',''  
--EXEC spGetReportPRETSFlightLogPreviewMainInformation 'TIM','3895','','','','','','','','','','','','','','','','','',''  
  
  
  
  

GO


