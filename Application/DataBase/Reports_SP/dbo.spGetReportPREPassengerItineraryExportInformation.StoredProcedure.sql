IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREPassengerItineraryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREPassengerItineraryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetReportPREPassengerItineraryExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@Pax AS VARCHAR(200) = '', --Mandatory
		@PassengerGroupCD VARCHAR(500)='' 
		
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPREPassengerItineraryExportInformation
-- Author: SINDHUJA.K
-- Create date: 07 August 2012
-- Description: Get Preflight Passenger Itinerary Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================


SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @Pax <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@Pax, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @Pax = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
	SELECT TEMP.* FROM 
			(SELECT DISTINCT
				[flag] = 'A',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = CONVERT(DATE,LHDate.DepartureDTTMLocal),
				[highdate] =  CONVERT(DATE,LHDate.ArrivalDTTMLocal),
				[tripdate] = CONVERT(DATE,PL.DepartureDTTMLocal),
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] =NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] = AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] =  FLOOR(ISNULL(PL.ElapseTM,0)*10)*0.1,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				
				 LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                 LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
	            
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON PA.PassengerRequestorID=TP.PassengerID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		  --  AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
UNION ALL

SELECT DISTINCT
				[flag] = 'B',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] =NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] = AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] =  FLOOR(ISNULL(PL.ElapseTM,0)*10)*0.1,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] = 0
			FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1  
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
		    
UNION ALL

SELECT DISTINCT
				[flag] = 'C',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] =NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] = 0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID  AND PL.IsDeleted = 0
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					--AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
UNION ALL

SELECT DISTINCT
				[flag] = 'D',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] =NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] = 0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    ---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
UNION ALL

SELECT DISTINCT
				[flag] = 'E',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] =NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] = 0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PM.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0

UNION ALL

SELECT DISTINCT
				[flag] = 'F',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
					[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] = 0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																				---	AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
 UNION ALL

SELECT DISTINCT
				[flag] = 'G',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =CD.CateringVendor,
			    [caterd_phone] = CD.PhoneNum,
			    [caterd_fax] = CD.FaxNum,
				[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				LEFT OUTER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType IN('O')  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		--    AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
  UNION ALL

SELECT DISTINCT
				[flag] = 'H',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					--AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		---    AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
 
  UNION ALL

SELECT DISTINCT
				[flag] = 'I',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
					[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		  ---  AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
   UNION ALL

SELECT DISTINCT
				[flag] = 'J',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   -- AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
 
  UNION ALL


SELECT DISTINCT
				[flag] = 'M',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0

  UNION ALL

SELECT DISTINCT
				[flag] = 'N',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   --- AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
   UNION ALL

SELECT DISTINCT
				[flag] = 'O',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				 LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					--AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   -- AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
 UNION ALL

SELECT DISTINCT
				[flag] = 'P',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
			    LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    --AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
 
  UNION ALL

SELECT DISTINCT
				[flag] = 'P',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0 
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																					---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

           INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   -- AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
   UNION ALL

SELECT DISTINCT
				[flag] = 'Q',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = NULL,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = NULL,
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] =  NULL,
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = NULL,
				[arr_terminal] = NULL,
				[dep_termph] = NULL,
				[arr_termph] = NULL,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] = NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =NULL,
			    [caterd_phone] = NULL,
			    [caterd_fax] = NULL,
				[caterd_rate] =0
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				LEFT OUTER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																				--	AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   --- AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
   UNION ALL

SELECT DISTINCT
				[flag] = 'R',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =CA.CateringVendor,
			    [catera_phone] = CA.PhoneNum,
			    [catera_fax] = CA.FaxNum,
				[catera_rate] = ISNULL(CA.NegotiatedRate,0),
				[caterd_name] =CD.CateringVendor,
			    [caterd_phone] = CD.PhoneNum,
			    [caterd_fax] = CD.FaxNum,
				[caterd_rate] =ISNULL(CD.NegotiatedRate,0)
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
				                                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																				---	AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 --   AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0
   UNION ALL

SELECT DISTINCT
				[flag] = 'X',
				[orig_nmbr] = PM.TripNUM,
				[leg_num] = PL.LegNUM,
				[legid] = PL.LegID,
				[paxcode] = PA.PassengerRequestorCD,
				[paxname] = ISNULL(PassengerFirstName+ ',' ,'') + ISNULL(PassengerLastName,'') + '' + ISNULL(PassengerMiddleName,''),
				[prepared_for] = PA.FirstName +' '+ PA.LastName,
				[lowdate] = LHDate.DepartureDTTMLocal,
				[highdate] = LHDate.ArrivalDTTMLocal,
				[tripdate] =PL.DepartureDTTMLocal,
				[locdep] = PL.DepartureDTTMLocal,
				[locarr] = PL.ArrivalDTTMLocal,
				[gmtdep] = PL.DepartureGreenwichDTTM,
				[tcenroute] = ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(DA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN DA.OffsetToGMT+1 ELSE DA.OffsetToGMT END)END) END,0),
                [tcfromhb] =  ISNULL(CASE WHEN ((CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END))<> 0 THEN
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END)-
                           (CASE(HA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN HA.OffsetToGMT+1 ELSE HA.OffsetToGMT END)END) END,0),
				
				[depicao_os] = (CASE(DA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet ELSE DA.OffsetToGMT End) ELSE NULL End),
				[arricao_os] = (CASE(AA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN  DST.StartDT and DST.EndDT THEN  DST.OffSet ELSE AA.OffsetToGMT End) ELSE NULL End),
				[homicao_os] = (CASE(HA.IsDayLightSaving) WHEN 1 THEN(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,DST.StartDT) AND CONVERT(DATE,DST.EndDT) THEN  DST.OffSet+1 ELSE HA.OffsetToGMT End) ELSE NULL End),
				[tail_nmbr] = F.TailNum,
				[type_code] = AR.AircraftCD,
				[type_desc] = F.TypeDescription,
				[addlcrew] = PL.AdditionalCrew,
				[pic] = PL.PilotInCommand,
				[captain] = ISNULL(P.FirstName,'') +' '+ ISNULL(P.LastName,''),
				[capt_mobl] = NULL,
				[capt_pagr] = NULL,
				[OtherCrew] = ISNULL(O.FirstName,'') +' '+ ISNULL(O.LastName,''),
				[depicao_id] =DA.IcaoID,
				[arricao_id] =AA.IcaoID,
				[depcity] = NULL,
				[arrcity] = NULL,
				[from_city] = ISNULL(DA.CityName,'') +','+ ISNULL(DA.StateName,''),
				[from_airport] = DA.AirportName,
				[to_city] = ISNULL(AA.CityName,'') +','+ ISNULL(AA.StateName,''),
				[to_airport] = AA.AirportName,
				[dep_terminal] = PF.PreflightFBOName,
				[arr_terminal] = PFA.PreflightFBOName,
				[dep_termph] = PF.PhoneNum1,
				[arr_termph] = PFA.PhoneNum1,
				[blank_line] = '', --Blank_line is of Character data type of size 75 declared only no logic exists,
				[elp_time] = PL.ElapseTM,
				[numpax] = PL.PassengerTotal,
				[catera_name] =NULL,
			    [catera_phone] =NULL,
			    [catera_fax] = NULL,
				[catera_rate] = 0,
				[caterd_name] =CD.CateringVendor,
			    [caterd_phone] = CD.PhoneNum,
			    [caterd_fax] = CD.FaxNum,
				[caterd_rate] =ISNULL(CD.NegotiatedRate,0)
				FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				INNER JOIN Aircraft AR ON F.AircraftID =AR.AircraftID
				INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
				INNER JOIN Crew P ON PC.CrewID = P.CrewID And PC.DutyType = 'P'
				LEFT OUTER JOIN Crew S ON PC.CrewID = S.CrewID And PC.DutyType = 'S'
				LEFT OUTER JOIN Crew E ON PC.CrewID = E.CrewID And PC.DutyType = 'E'
				LEFT OUTER JOIN Crew AT ON PC.CrewID = AT.CrewID And PC.DutyType = 'A'
				LEFT OUTER JOIN Crew O ON PC.CrewID = O.CrewID And PC.DutyType = 'O'  
				INNER JOIN  Airport  DA ON PL.DepartICAOID = DA.AirportID           
				INNER JOIN  Airport  AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Company C ON C.HomebaseID=PM.HomebaseID
				INNER JOIN Airport HA ON HA.AirportID=C.HomebaseAirportID
				INNER JOIN (SELECT StartDT, EndDT, OffSet, DSTRegionID FROM DSTRegion) DST ON DA.DSTRegionID = DST.DSTRegionID
				LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsDepartureFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO=1
                LEFT OUTER JOIN (SELECT PL.PreflightFBOName,PL.PhoneNum1,PL.LegID,IsArrivalFBO FROM  PreflightFBOList PL INNER JOIN FBO ON PL.FBOID=FBO.FBOID ) PFA ON PL.LegID = PFA.LegID AND PFA.IsArrivalFBO=1
				INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
			  	LEFT OUTER JOIN (
			SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'
			) CD ON PL.LegID = CD.LegID
			
			LEFT OUTER JOIN (
				SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,[NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,
				       C.Remarks,PCL.LegID
				FROM PreflightCateringDetail PCL 
				INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'			
			) CA ON PL.LegID = CA.LegID
			LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,PM.CustomerID FROM PreflightMain PM 
			                                                                       INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
			                                                                       INNER JOIN  PreflightPassengerList P1 ON P1.LegID=PL.LegID
				                                                                   INNER JOIN Passenger PA on P1.PassengerID = PA.PassengerRequestorID
																				   INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
																				  WHERE  CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO)
																					AND  PM.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
																					AND  PM.TripStatus='T'
																					AND PM.IsDeleted = 0
																				---	AND ( PA.PassengerRequestorCD IN (SELECT distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
																					GROUP BY PM.CustomerID)LHDate ON PM.CustomerID=LHDate.CustomerID

            INNER JOIN @TempPassengerID TP ON TP.PassengerID=PA.PassengerRequestorID
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    ---AND ( PA.PassengerRequestorCD IN (select distinct rtrim(s) from dbo.SplitString(@Pax, ','))  OR @Pax = '' )
		    AND  PM.TripStatus='T'
		    AND PM.IsDeleted = 0		    

 )TEMP
 WHERE flag='G'
 ORDER BY paxcode,orig_nmbr,leg_num
END
    
   --EXEC spGetReportPREPassengerItineraryExportInformation 'UC','2012/7/20','2012/7/22','HKIOE,JEOF'

GO


