IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTStatemileage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTStatemileage]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--EXEC spGetReportPOSTStatemileage 'SUPERVISOR_99','10099','2012-08-01','2012-08-31','','',''
-- =============================================

CREATE PROCEDURE [dbo].[spGetReportPOSTStatemileage]
    @UserCD AS VARCHAR(30),
    @UserCustomerID BIGINT,
	@DATEFROM DATETIME,
    @DATETO DATETIME, 	
    --@CountryCD AS VARCHAR(40)='',
    @StateName AS VARCHAR(200)='',
    @TailNum AS NVARCHAR(30)='',
    @AircraftCD AS NVARCHAR(100)='',
    @ChargeUnit AS VARCHAR(2) = '',
    @PrintTakeOff BIT = 0,
    @PrintLanding BIT = 0
 AS 

BEGIN
SET NOCOUNT ON;

DECLARE @ChargeUnit2 AS BIT;
SET @ChargeUnit2 = CASE WHEN @ChargeUnit = 'SM' THEN 1 ELSE 0 END;

DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	   
 DECLARE @AircraftBasis NUMERIC(1,0);
	  SELECT @AircraftBasis = AircraftBasis FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	   

SET NOCOUNT ON;


SELECT DISTINCT
   AD.StateName AS StateName
   ,PR.TripNUM AS TripNUM
   ,AC.AircraftCD
   ,CASE WHEN TH.OldTailNUM = F.TailNum THEN TH.NewTailNUM ELSE F.TailNum END AS [TailNumber]
   ,PM.LogNum AS LogNum
   ,PL.LegNUM AS LegNUM
   ,[FLIGHT DATE] = PL.ScheduledTM
   ,[DEPID] = AD.IcaoID
   ,[DEPART CITY] = AD.CityName
   ,[ARRID] = AA.IcaoID
   ,[ARRIVE CITY] = AA.CityName
   ,ROUND(PL.FlightHours,1) FlightHours
   ,ROUND(PL.BlockHours,1) BlockHours
   ,FC.FlightCatagoryCD
   ,FC.FlightCatagoryDescription 
   ,PL.FlightPurpose 
    ,TakeoffLable = CASE WHEN @PrintTakeOff = 0 THEN ''
                        WHEN @PrintTakeOff = 1 THEN 'Take Off' end
   ,LandingLable = CASE WHEN @PrintLanding = 0 THEN ''
                        WHEN @PrintLanding = 1 THEN 'Landing' end
  -- ,[TAKE OFF] = 1
  -- ,[LANDING] = 1
   ,[TAKE OFF] = CASE WHEN @PrintTakeOff = 0 THEN NULL
                      WHEN @PrintTakeOff = 1 THEN 1 end
   ,[LANDING]= CASE WHEN @PrintLanding = 0 THEN NULL
                    WHEN @PrintLanding = 1 THEN 1 end
   ,[DistnceLabel]= CASE WHEN @ChargeUnit2 = 1 THEN 'Statute Miles' 
                         ELSE  'Nautical Miles' END
   ,CASE WHEN @ChargeUnit2 = 1 THEN ISNULL(((PL.Distance)*1.15078),0)
         ELSE  ISNULL((PL.Distance),0) 
         END AS [Distnce] 
    ,[TenToMin] = @TenToMin     
     FROM PostflightMain PM 
        INNER JOIN PostflightLeg PL
                    ON PM.POLogID = PL.POLogID AND PL.IsDeleted=0
        INNER JOIN PreflightMain PR
                    ON PM.TripID = PR.TripID AND PR.IsDeleted=0
         LEFT JOIN Fleet F 
                    ON PM.FleetID = F.FleetID
         LEFT JOIN TailHistory TH 
                    ON TH.OldTailNUM = F.TailNum AND TH.CustomerID = F.CustomerID
         LEFT JOIN FlightCatagory FC 
                    ON PL.FlightCategoryID = FC.FlightCategoryID
         --LEFT JOIN Company C 
         --           ON PM.HomebaseID = C.HomebaseID
         --LEFT JOIN Airport AP 
         --           ON AP.AirportID = C.HomebaseAirportID 
         LEFT JOIN Airport AD 
                    ON AD.AirportID = PL.DepartICAOID
         LEFT JOIN Airport AA 
                    ON AA.AirportID = PL.ArriveICAOID
         LEFT JOIN Country CO 
                    ON CO.CountryID = AD.CountryID
         --INNER JOIN FleetChargeRate FCR
                    --ON PM.FleetID = FCR.FleetID   
         LEFT JOIN Aircraft AC
                    ON PM.AircraftID = AC.AircraftID                 
       WHERE  CO.CountryCD = 'US' AND  AD.StateName = AA.StateName
        AND PM.IsDeleted=0
        --AND (CO.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ','))OR @CountryCD='')
        AND (AD.StateName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ','))OR @StateName='')
        AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
        AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
        AND (CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo))
        AND PM.CustomerID = @UserCustomerID
        
       
END

        
        
        
        
      
       
        
        





GO


