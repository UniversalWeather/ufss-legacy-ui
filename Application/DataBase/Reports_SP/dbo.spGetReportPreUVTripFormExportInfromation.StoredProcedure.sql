IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPreUVTripFormExportInfromation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPreUVTripFormExportInfromation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPreUVTripFormExportInfromation]
    @UserCD AS VARCHAR(30),
    @UserCustomerID BIGINT,
    @TripNum AS VARCHAR(300) = ''
 AS 

BEGIN
SET NOCOUNT ON;

SELECT DISTINCT 
               PM.TripNUM
              ,PL.LegID
              ,F.TailNum AS [Tail_Number] 
              ,AC.AircraftCD
              ,F.BasicOperatingWeight AS BOW
              ,CA.CAPTAIN
              ,CC.CREWCOUNT
              ,AD.IcaoID AS DEPICAOID
              ,AA.IcaoID AS ARRICOID
              ,PL.PassengerTotal
              ,PL.DepartureGreenwichDTTM
              ,PL.DepartureDTTMLocal
              ,PL.DepartureGreenwichDTTM AS [DEP_ETD]
              ,PL.DepartureDTTMLocal AS [ARR_ETD]
              ,C.CompanyName
       FROM PreflightMain PM
       JOIN PreflightLeg PL ON PM.TripID = PL.TripID and PL.IsDeleted = 0
       JOIN Aircraft AC ON PM.AircraftID = AC.AircraftID
       LEFT JOIN Fleet F ON F.FleetID = PM.FleetID 
       LEFT JOIN TailHistory TH ON TH.OldTailNUM = F.TailNum AND TH.CustomerID = F.CustomerID
       LEFT JOIN (SELECT PL1.TripID,PL1.LegID,(PCL.CrewLastName +', '+PCL.CrewFirstName) AS CAPTAIN FROM PreflightLeg PL1 LEFT JOIN PreflightCrewList PCL ON PL1.LegID = PCL.LegID   WHERE PCL.DutyTYPE = 'P'AND PL1.LegNUM = 1 AND PL1.IsDeleted = 0)CA ON CA.TripID = PL.TripID
       LEFT JOIN (SELECT PL2.TripID,PL2.LEGID,COUNT(*) AS CREWCOUNT FROM PreflightLeg PL2 JOIN PreflightCrewList PCL2 ON PCL2.LegID = PL2.LegID AND PL2.LegNUM = 1 AND PL2.IsDeleted = 0 GROUP BY PL2.LEGID, PL2.TripID)CC ON CC.TripID = PL.TripID
       LEFT JOIN 
				 (SELECT C.CompanyName, C.CustomerID, C.HomebaseAirportID
					FROM UserMaster UM 
						INNER JOIN Company C ON UM.HomebaseID = C.HomebaseID AND C.CustomerID = UM.CustomerID AND RTRIM(UM.UserName) = RTRIM(@UserCD)
					)C ON C.CustomerID = F.CustomerID
       LEFT JOIN Airport AP ON C.HomebaseAirportID = AP.AirportID 
       LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
       LEFT JOIN Airport AA  ON PL.ArriveICAOID = AA.AirportID
       LEFT JOIN PreflightCrewList PCL1 ON PL.LegID = PCL1.LegID
  
      WHERE PM.CustomerID = @UserCustomerID AND PM.TripNUM = @TripNum
      AND PM.IsDeleted = 0
      
   END 
GO


