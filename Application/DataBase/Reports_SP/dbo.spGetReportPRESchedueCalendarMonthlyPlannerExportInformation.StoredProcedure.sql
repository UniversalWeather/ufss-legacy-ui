IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlyPlannerExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerExportInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfMonths INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	    ---Display Options---
	  ,@IsBoth INT=1  ---1-Both,2-Fleet,3-Crew
	  ,@IsActivityOnly BIT=0
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''
AS  

BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlyPlannerExportInformation  
-- Author: Askar 
-- Create date: 25 Oct 2013  
-- Description: Get Preflight Schedule Calendar Monthly Planner information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
          
SET NOCOUNT ON    
               
DECLARE @FROMDATE DATETIME=@BEGINDATE,@DATETO DATETIME=@ENDDATE   

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		


IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END

-----Rest Record and Maintenace Reco
DECLARE @CurDate TABLE(DateWeek DATE,CrewID BIGINT,FleetID BIGINT)

 
 DECLARE @TableTrip TABLE(TailNum VARCHAR(9),
						  CrewID BIGINT,
                          CrewMember VARCHAR(100),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          NextLocalDTTM DATETIME,
                          DutyTYPE CHAR(2), 
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(4),
                          ArrivelICAOID VARCHAR(4),
                          LegNUM BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          TripNum BIGINT,
                          FleetID BIGINT,
                          TypeCode VARCHAR(10),
                          CrewDuty CHAR(1)
                          )
 


   
 INSERT INTO @TableTrip
 SELECT DISTINCT F.TailNum
     ,PCL.CrewID
     ,CW.CrewCD+' - '+CW.LastName+', '+CW.FirstName+' '+ISNULL(CW.MiddleInitial,'')
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PL.DutyTYPE
     ,PM.RecordType
     ,D.IcaoID 
     ,A.IcaoID 
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PM.TripNUM
     ,PM.FleetID
     ,AFT.AircraftCD
     ,PCL.DutyTYPE
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT JOIN(SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)) CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	        -- LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
       AND PM.RecordType IN('T','C','M')
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
        AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

 INSERT INTO @TableTrip
 SELECT DISTINCT 
      F.TailNum
     ,PCL.CrewID
     ,CW.CrewCD+' - '+CW.LastName+', '+CW.FirstName+' '+ISNULL(CW.MiddleInitial,'')
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PL.DutyTYPE
     ,PM.RecordType
     ,D.IcaoID 
     ,A.IcaoID 
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PM.TripNUM
     ,PM.FleetID
     ,AFT.AircraftCD
     ,PCL.DutyTYPE
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT JOIN(SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	        -- LEFT OUTER JOIN CrewDutyType CD ON CD.CustomerID=PCL.CustomerID
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
       AND PM.RecordType IN('T','C','M')
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
	   AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
        AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

---Crew
INSERT INTO @CurDate(DateWeek,CrewID)
SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND CC.IsDeleted=0 AND CC.IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
                                   AND (CC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')	



---Fleet


INSERT INTO @CurDate(DateWeek,FleetID)
SELECT T.*,F.FleetID FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                  LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))




INSERT INTO @TableTrip(TailNum,CrewID,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID)
SELECT  TT.TailNum,CD.CrewID,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
                  CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.CrewID=TT.CrewID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(TailNum,CrewID,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID)
SELECT TT.TailNum,CD.CrewID,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.CrewID=TT.CrewID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND  RecordTye ='C'
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)




INSERT INTO @TableTrip(TypeCode,TailNum,FleetID,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID,CrewDuty)
SELECT  TT.TypeCode,TT.TailNum,CD.FleetID,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
                  CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID,TT.CrewDuty FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                               AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(TypeCode,TailNum,FleetID,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,LegID)
SELECT TT.TypeCode,TT.TailNum,CD.FleetID,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.LegID FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye='M'
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                               AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)
                 

DELETE FROM @TableTrip WHERE RecordTye ='C' AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)
DELETE FROM @TableTrip WHERE RecordTye='M' AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


DECLARE @TempTable TABLE(ID INT IDENTITY
                        ,TailNum VARCHAR(9)
                        ,FleetID BIGINT
                        ,TypeCode VARCHAR(10)
                        ,CrewID BIGINT
                        ,CrewCD VARCHAR(5)
                        ,CrewMember VARCHAR(100)
                        ,TripNUM BIGINT
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,RecordType VARCHAR(1)
                        ,DutyType CHAR(2)
                        ,MonthNo INT
                        ,DepICAOID VARCHAR(4)
                        ,ArrivelICAOID VARCHAR(4)
                        ,LegID BIGINT
                        ,CrewDuty CHAR(1)
                        ) 
 



INSERT INTO @TempTable
SELECT TEMP.* FROM (
SELECT TT.TailNum
      ,TT.FleetID
      ,TT.TypeCode
      ,TT.CrewID
      ,C.CrewCD
      ,CrewMember=C.CrewCD+' - '+C.LastName+', '+C.FirstName+' '+ISNULL(C.MiddleInitial,'')
      ,TripNUM
     ,DATE=CONVERT(DATE,DepartureDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,TT.RecordTye
     ,TT.DutyTYPE
     ,MonthNo=NULL
     ,TT.DepICAOID
     ,TT.ArrivelICAOID
     ,TT.LegID
     ,TT.CrewDuty
      FROM @TableTrip TT LEFT JOIN Crew C ON TT.CrewID=C.CrewID 
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      --ORDER BY CrewCD,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum

UNION ALL   

SELECT TT.TailNum
      ,TT.FleetID
      ,TT.TypeCode
      ,TT.CrewID
      ,C.CrewCD
      ,C.CrewCD+' - '+C.LastName+', '+C.FirstName+' '+ISNULL(C.MiddleInitial,'')
      ,TripNUM
     ,CONVERT(DATE,ArrivalDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,TT.RecordTye
     ,TT.DutyTYPE
     ,NULL
     ,TT.DepICAOID
     ,TT.ArrivelICAOID
     ,TT.LegID
     ,TT.CrewDuty
      FROM @TableTrip TT LEFT JOIN Crew C ON TT.CrewID=C.CrewID 
      WHERE CONVERT(DATE,ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      )TEMP
      ORDER BY CrewCD,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum
      
IF @IsActivityOnly=0
BEGIN
INSERT INTO @TempTable(CrewID,CrewCD,CrewMember,Date)
SELECT DISTINCT C.CrewID,C.CrewCD,C.CrewCD+' - '+C.LastName+', '+C.FirstName+' '+ISNULL(C.MiddleInitial,''),CD.DateWeek FROM @CurDate CD INNER JOIN  Crew C ON CD.CrewID=C.CrewID
																					          LEFT JOIN @TempTable TT ON TT.CrewID=C.CrewID
												  WHERE C.IsDeleted=0
												   AND C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
												   AND C.IsStatus=1
												   AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing=@RotaryWingCrew) OR @RotaryWingCrew = 0)
												   AND NOT EXISTS(SELECT ISNULL(CrewID,0) FROM @TempTable T WHERE T.CrewID=CD.CrewID AND T.Date=CD.DateWeek)  
    

INSERT INTO @TempTable(TailNum,FleetID,Date,TypeCode)
SELECT DISTINCT F.TailNum,F.FleetID,CD.DateWeek,AT.AircraftCD FROM @CurDate CD INNER JOIN  Fleet F ON CD.FleetID=F.FleetID
                                                                 LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
                                                                 LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
												  WHERE F.IsDeleted=0
												   AND F.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
												   AND F.IsInActive=0
												    AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
												   AND NOT EXISTS(SELECT ISNULL(FleetID,0) FROM @TempTable T WHERE T.FleetID=CD.FleetID AND T.Date=CD.DateWeek)  

END
ELSE
BEGIN

INSERT INTO @TempTable(CrewID,CrewCD,CrewMember,Date)
SELECT DISTINCT C.CrewID,C.CrewCD,C.CrewCD+' - '+C.LastName+', '+C.FirstName+' '+ISNULL(C.MiddleInitial,''),CD.DateWeek FROM @CurDate CD INNER JOIN  Crew C ON CD.CrewID=C.CrewID
																					          INNER JOIN @TempTable TT ON TT.CrewID=C.CrewID
												  WHERE C.IsDeleted=0
												   AND C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
												   AND C.IsStatus=1
												   AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
												   AND NOT EXISTS(SELECT ISNULL(CrewID,0) FROM @TempTable T WHERE T.CrewID=CD.CrewID AND T.Date=CD.DateWeek) 


INSERT INTO @TempTable(TailNum,FleetID,Date,TypeCode)
SELECT DISTINCT F.TailNum,F.FleetID,CD.DateWeek,AT.AircraftCD FROM @CurDate CD INNER JOIN  Fleet F ON CD.FleetID=F.FleetID
                                                                 INNER JOIN @TempTable TT ON TT.FleetID=CD.FleetID
                                                                 LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
												  WHERE F.IsDeleted=0
												   AND F.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
												   AND F.IsInActive=0
												   AND NOT EXISTS(SELECT ISNULL(FleetID,0) FROM @TempTable T WHERE T.FleetID=CD.FleetID AND T.Date=CD.DateWeek)  
												   
END



  DECLARE @MONTHNUM INT=1,@MONTH INT,@DateVal DATE=@BeginDate,@DateFrom DATETIME;
 SELECT  @MONTH=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/31 ) 


SET @DateFrom=DATEADD(DAY,30,@BeginDate)

WHILE (@MONTHNUM<=@MONTH)

 BEGIN

      WHILE @DateVal <= @DateFrom
       
       BEGIN
       
         UPDATE @TempTable SET MonthNo=@MONTHNUM WHERE DATE BETWEEN @DateVal AND DATEADD(DAY,30,@DateVal)
         SET @DateVal=DATEADD(DAY,31,@DateVal)

      END
   
  SET @DateFrom=DATEADD(DAY,31,@DateFrom)

 SET @MONTHNUM=@MONTHNUM+1
 
 END



---Crew 
SELECT DISTINCT  type='CREW'
      ,tail_nmbr=NULL
      ,GroupType=TEMP.CrewCD
      ,crewcode=TEMP.CrewCD
      ,[desc]=TEMP.CrewMember
      ,orig_nmbr=ISNULL(TEMP.TripNUM,0)
      ,LegID=ISNULL(TEMP.LegID,0)
      ,TEMP.Date
      ,TEMP.RecordType
      ,CrewVal=TEMP.CrewVal+(CASE WHEN CrewDuty='P' THEN '-P'
								  WHEN CrewDuty='S' THEN '-S' ELSE '' END)
      ,TEMP.Rnk
      ,MonthNo
      ,ROW_NUMBER()OVER(PARTITION BY CrewCD,MonthNo ORDER BY CrewCD,MonthNo,Date) SortMonth
      ,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
      ,TypeVal  --Crew 
      ,[view]='Day View'
      ,[goto]=CONVERT(DATE,@BeginDate)
 FROM (
SELECT DISTINCT  TailNum 
				,CrewID
				,CrewCD 
				,CrewMember 
				,TripNUM 
				,Date 
				,RecordType
				,TailNum CrewVal
			    ,ROW_NUMBER()OVER(PARTITION BY CrewID,Date ORDER BY Date,ArrivalDTTMLocal DESC) Rnk
			    ,MonthNo
			    ,TypeVal=3 
			    ,LegID
			    ,CrewDuty
				 FROM @TempTable
				 WHERE (RecordType='T' OR RecordType IS NULL)
				 AND DATE BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)
				 AND CrewCD IS NOT NULL

 UNION ALL
 
 SELECT DISTINCT TailNum 
				,CrewID
				,CrewCD 
				,CrewMember 
				,TripNUM 
				,Date 
				,RecordType
				,DutyType CrewVal
				,ROW_NUMBER()OVER(PARTITION BY CrewID,Date ORDER BY Date,ArrivalDTTMLocal DESC) Rnk
				,MonthNo
				,TypeVal=3 
				,LegID
				,CrewDuty
				 FROM @TempTable T1
				 WHERE RecordType ='C'
				  AND DATE BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)
				  AND NOT EXISTS(SELECT CrewID FROM @TempTable T WHERE RecordType='T' AND T.CrewID=T1.CrewID AND T.Date=T1.Date)
                  AND CrewCD IS NOT NULL
 )TEMP
 WHERE TEMP.Rnk=1
  AND DATE BETWEEN @FROMDATE AND @DATETO
  AND (@IsBoth=1 OR @IsBoth=3)

UNION ALL
---Fleet
SELECT type='FLEET'
      ,TEMP.TailNum
      ,GroupType=TEMP.TailNum
      ,CrewCD=NULL
      ,TEMP.TailNum+' - '+TEMP.TypeCode+' - '
      ,TripNUM=ISNULL(TEMP.TripNUM,0)
      ,LegID=ISNULL(TEMP.LegID,0)
      ,TEMP.Date
      ,TEMP.RecordType
      ,TEMP.ArrivelICAOID
      ,TEMP.Rnk Rnk
      ,MonthNo
      ,ROW_NUMBER()OVER(PARTITION BY TailNum,MonthNo ORDER BY TailNum,MonthNo,Date) SortMonth
      ,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
      ,TypeVal
      ,[view]='Day View'
      ,[goto]=CONVERT(DATE,@BeginDate)
         FROM (
SELECT DISTINCT  TailNum 
				,FleetID 
				,TripNUM 
				,Date 
				,RecordType
				,TailNum TailVal
				,DepICAOID
				,ArrivelICAOID
				,DepartureDTTMLocal
				,ArrivalDTTMLocal
				,TypeCode
				,ROW_NUMBER()OVER(PARTITION BY FleetID,Date ORDER BY Date,ArrivalDTTMLocal DESC) Rnk
				,MonthNo=MonthNo
				,TypeVal=2
				,LegID
				,CrewDuty
				 FROM @TempTable
				 WHERE (RecordType='T' OR RecordType IS NULL)
				 AND FleetID IS NOT NULL

 UNION ALL
 
 SELECT DISTINCT TailNum 
				,FleetID
				,TripNUM 
				,Date 
				,RecordType
				,DutyType TailVal
				,DepICAOID
				,DutyType
				,DepartureDTTMLocal
				,ArrivalDTTMLocal
				,TypeCode
				,ROW_NUMBER()OVER(PARTITION BY FleetID,Date ORDER BY Date,ArrivalDTTMLocal DESC) Rnk
				,MonthNo=MonthNo
				,TypeVal=2
				,LegID
				,CrewDuty
				 FROM @TempTable T1
				 WHERE RecordType='M'
				 AND NOT EXISTS(SELECT FleetID FROM @TempTable T WHERE RecordType='T' AND T.FleetID=T1.FleetID AND T.Date=T1.Date)
                 AND FleetID IS NOT NULL
)TEMP
WHERE TEMP.Rnk=1
AND DATE BETWEEN @FROMDATE AND @DATETO
AND (@IsBoth=1 OR @IsBoth=2)
ORDER BY TypeVal,TEMP.MonthNo,TEMP.CrewCD ,TEMP.Date


				
END;  




 --EXEC spGetReportPRESchedueCalendarMonthlyPlannerExportInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''









GO


