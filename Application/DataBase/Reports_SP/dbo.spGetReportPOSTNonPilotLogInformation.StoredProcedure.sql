IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE Procedure [dbo].[spGetReportPOSTNonPilotLogInformation]  
  @UserCD AS VARCHAR(30), --Mandatory  
  @DATEFROM AS DATETIME, --Mandatory  
  @DATETO AS DATETIME, --Mandatory  
  @CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @CrewGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
  @ChecklistCode AS NVARCHAR(1000) = '',  
  @TypeCode AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @PrintOnTime AS bit=0,   
  @PrintOffTime AS bit=0    
AS  
  
-- ===============================================================================  
-- SPC Name: spGetReportPOSTNonPilotLogInformation  
-- Author:  D.Mullai  
-- Create date:   
-- Description: Get Postflight NonPilot Log for REPORTS  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
  
SET NOCOUNT ON
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=0

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END

--select * from @TempCrewID
-----------------------------Crew and Crew Group Filteration---------------------- 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 1
IF OBJECT_ID('tempdb..#PILOTINFO') IS NOT NULL
DROP TABLE #PILOTINFO 
 SELECT distinct
    [DateFROM]=CONVERT(DATE,@DATEFROM),
	[DATETO]= CONVERT(DATE, @DATETO),   
	[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
	[crewid]=(Convert(varchar(50),cW.crewid)),
	CW.CrewCD,
	Cw.IsStatus,
    Cw.IsDeleted
    --,AC.AircraftCD
	 INTO #PILOTINFO

     from PostflightLeg pl
    
	INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0 
	INNER JOIN  PostflightCrew PC ON PC.POLegID = PL.POLegID 
	INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = PC.CrewID
	INNER JOIN Crew CW ON CW.CrewID=PC.CrewID
    LEFT OUTER JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  

WHERE (PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))   
AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND PC.Dutytype NOT IN ('P','S')  
AND PL.IsDeleted=0
AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TypeCode, ',')) OR @TypeCode = '')  
ORDER BY CrewCD


DELETE FROM #PILOTINFO WHERE ISDELETED=0 AND ISSTATUS=0 AND CrewCD NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))

IF @IsZeroSuppressActivityCrewRpt = 0
BEGIN
SELECT * FROM #PILOTINFO
UNION ALL  
SELECT distinct
    [DateFROM]=CONVERT(DATE,@DATEFROM),
	[DATETO]= CONVERT(DATE, @DATETO),   
	[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
	[crewid]=(Convert(varchar(50),cw.crewid)),
	CW.CrewCD
	,Cw.IsStatus
    ,Cw.IsDeleted
    --,AC.AircraftCD
	FROM crew cw
	INNER JOIN @CREW CT ON CW.CrewID = CT.CREWID
	INNER JOIN ( SELECT DISTINCT CREWID FROM @TempCrewID ) C1 ON C1.CREWID = Cw.CrewID
	LEFT OUTER JOIN PostflightLeg pl on pl.CrewID=cw.crewid AND PL.IsDeleted=0
	LEFT OUTER JOIN #PILOTINFO AC ON CW.CrewID=AC.CrewID
	LEFT OUTER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0 
	--LEFT OUTER JOIN Aircraft AC ON AC.AircraftID=PM.AircraftID
	
		WHERE
		CW.CrewID NOT IN (SELECT DISTINCT CrewID FROM #PILOTINFO) 
		--AND	(AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TypeCode, ',')) OR @TypeCode = '')  
		AND (CW.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))) 
ORDER BY CrewCD
END
ELSE
BEGIN
SELECT * FROM #PILOTINFO
ORDER BY CrewCD
END

	


   -- EXEC spGetReportPOSTNonPilotLogInformation 'JWILLIAMS_13','2011/1/1','2011/12/31','','','','',0,0
   
   
   --select * from Crew where CustomerID='10013'



GO


