IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLegAnalysisHoursExportInformationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLegAnalysisHoursExportInformationSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportLegAnalysisHoursExportInformationSub]        
(  
  @UserCD AS VARCHAR(30),  --Mandatory  
  @DATEFROM DATETIME, --Mandatory  
  @DATETO DATETIME, --Mandatory  
  @TailNumber VARCHAR(300)=Null,--Optional  
  @FleetGroupCD VARCHAR(300)=Null,--Optional  
  @FleetID BIGINT   
 )  
-- =============================================      
-- Author: Mathes      
-- Create date: 01-08-2012      
-- Description: Leg Analysis/Hours(FC) - Report 
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package      
-- =============================================      
AS      
SET NOCOUNT ON      
BEGIN      
      IF OBJECT_ID('tempdb..#TempDist') is not null      
      DROP table #TempDist      
           
  Create Table #TempDist (DistId Int,DistFrom Numeric(6,2),DistTo Numeric(6,2))      
  INSERT INTO #TempDist (DistId,DistFrom,DistTo)      
     Select 1,00.00,00.12  union All       
        Select 2,00.13,00.30  union All       
        Select 3,00.31,00.48  union All       
        Select 4,00.49,01.06  union All       
        Select 5,01.07,01.24  union All       
        Select 6,01.25,01.42  union All       
        Select 7,01.43,02.00  union All       
        Select 8,02.01,03.00  union All       
        Select 9,03.01,04.00 union All       
        Select 10,04.01,05.00 union All       
        Select 11,05.01,06.00  union All       
        Select 12,06.01,07.00  union All       
        Select 13,07.01,08.00 union All       
        Select 14,08.01,09.00  union All        
        Select 15,09.01,24.00    
        
  Declare @DistId Int      
  Declare @DistFrom Numeric(6,2)      
  Declare @DistTo Numeric(6,2)    
       
  Declare @RowId Int      
  Declare @NumRows Int       
         
        IF OBJECT_ID('tempdb..#TempReport') is not null      
          DROP table #TempReport      
             
        Create Table #TempReport (FromMile Numeric(6,2)  ,  
                                  ToMile Numeric(6,2)  ,  
                                  FleetID BIGINT,   
                                  TailNum CHAR(6),  
                                  AircraftCD CHAR(3),  
                                  NoOfLegs Int,  
                                  TotalLegs Int,   
                                  BlockHours Numeric(10,2),  
                                  FlightHours Numeric(10,2),  
                                  Miles Int,  
                                  FuelBurn Int,   
                                  NoOfPax Int)    
                                                
        Declare @TailNum CHAR(6)  
        Declare @AircraftCD CHAR(3)  
        Declare @NoOfLegs INT  
        Declare @TotalLegs INT   
        Declare @BlockHours Numeric(10,2)   
        Declare @FlightHours Numeric(10,2)  
        Declare @Miles Int  
        Declare @FuelBurn Int   
        Declare @NoOfPax Int   
               
              
            BEGIN                     
           
            SET @RowId = 1         
            SET @NumRows =  (SELECT COUNT(*) FROM #TempDist)       
            IF @NumRows > 0       
                WHILE (@RowId <= (SELECT MAX(DistID) FROM #TempDist))        
                BEGIN      
      
    Set @DistId=0      
    Set @DistFrom=0       
    Set @DistTo=0      
   
    SELECT @DistId=DistId,@DistFrom=DistFrom,@DistTo=DistTo From #TempDist Where DistId=@RowId       
    -----------------------------------------------------------------------------Actual Report Area Starts      
      IF @TailNumber <>'' And @FleetGroupCD <>''  
      BEGIN  
        SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,  
        @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,  
        @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax     
     FROM   ( SELECT  
        Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,  
        IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,    
        PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM  
      FROM    Fleet INNER JOIN      
        FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN      
        FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN      
        PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN      
        PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN      
        PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID     
     GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,      
        PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD                
     HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And  
        Fleet.FleetID=@FleetID And    
        PostflightLeg.FlightHours Between @DistFrom And @DistTo And    
        PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO   
        And Fleet.TailNum In (@TailNumber)  
        And FleetGroup.FleetGroupCD In(@FleetGroupCD))  ResultSet      
      END  
      ELSE IF @TailNumber ='' And @FleetGroupCD <>''  
      BEGIN  
        SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,  
        @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,  
        @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax    
     FROM   ( SELECT  
        Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,  
        IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,    
        PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM  
      FROM    Fleet INNER JOIN      
        FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN      
        FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN      
        PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN      
        PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN      
        PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID     
     GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,      
        PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD                
     HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And  
        Fleet.FleetID=@FleetID And    
        PostflightLeg.FlightHours Between @DistFrom And @DistTo And    
        PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO  
        And FleetGroup.FleetGroupCD In (@FleetGroupCD))  ResultSet      
      END  
      ELSE IF @TailNumber <>'' And @FleetGroupCD =''  
      BEGIN  
      SELECT  @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,  
        @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,  
        @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax    
     FROM   ( SELECT  
        Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,  
        IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,    
        PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM  
      FROM    Fleet INNER JOIN      
        FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN      
        FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN      
        PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN      
        PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN      
        PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID     
     GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,      
        PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD                
     HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And  
        Fleet.FleetID=@FleetID And    
        PostflightLeg.FlightHours Between @DistFrom And @DistTo And    
        PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO  
        And Fleet.TailNum In (@TailNumber))  ResultSet      
      END  
      ELSE IF @TailNumber ='' And @FleetGroupCD =''  
      BEGIN  
     SELECT   @FleetID=ResultSet.FleetID,@TailNum=ResultSet.TailNum, @AircraftCD=ResultSet.AircraftCD, @NoOfLegs=ResultSet.NoOfLegs,  
        @TotalLegs=ResultSet.TotalLegs,@BlockHours=ResultSet.BlockHours, @FlightHours=ResultSet.FlightHours,  
        @Miles=ResultSet.Distance, @FuelBurn=ResultSet.FuelUsed,@NoOfPax=ResultSet.NoOfPax   
     FROM   ( SELECT  
        Fleet.FleetID,Fleet.TailNum,Fleet.AircraftCD, IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,FleetGroup.FleetGroupCD,  
        IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs, PostflightLeg.BlockHours, PostflightLeg.FlightHours, PostflightLeg.Distance,    
        PostflightLeg.FuelUsed,  IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  PostflightLeg.CustomerID, PostflightLeg.ScheduledTM  
      FROM    Fleet INNER JOIN      
        FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN      
        FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN      
        PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN      
        PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN      
        PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID     
     GROUP BY Fleet.FleetID, PostflightLeg.CustomerID, Fleet.TailNum,Fleet.AircraftCD,  PostflightLeg.ScheduledTM, PostflightLeg.Distance ,      
        PostflightLeg.BlockHours, PostflightLeg.FlightHours,PostflightLeg.FuelUsed,PostflightLeg.PassengerTotal,FleetGroup.FleetGroupCD               
     HAVING   PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And  
        Fleet.FleetID=@FleetID And    
        PostflightLeg.FlightHours Between @DistFrom And @DistTo And    
        PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO)  ResultSet      
      END  
       
       
               Insert Into #TempReport(FromMile,ToMile,FleetID,TailNum,AircraftCD,NoOfLegs,TotalLegs,  
                      BlockHours,FlightHours,Miles,FuelBurn,NoOfPax)  
               Values(@DistFrom,@DistTo,@FleetID,@TailNum,@AircraftCD,@NoOfLegs,@TotalLegs,  
                      @BlockHours,@FlightHours,@Miles,@FuelBurn,@NoOfPax)                                
                              
                        ------------------------------------------------------------------------------Actual Report Area Ends      
                        Set @RowId=@RowId+1      
                    END          
           
         END       
   
   IF OBJECT_ID('tempdb..#TempDist') is not null      
      DROP table #TempDist       
           
   Select * from #TempReport     
         
   IF OBJECT_ID('tempdb..#TempReport') is not null      
      DROP table #TempReport      
END


GO


