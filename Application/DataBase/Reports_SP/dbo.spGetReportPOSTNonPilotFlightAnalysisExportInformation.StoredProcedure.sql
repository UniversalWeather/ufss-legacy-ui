IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotFlightAnalysisExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotFlightAnalysisExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTNonPilotFlightAnalysisExportInformation] (
	@UserCD			VARCHAR(30), 
	@UserCustomerID AS BIGINT,
	@DATEFROM      DATETIME,
	@DATETO         DATETIME,
	@CrewCD			NVARCHAR(1000)='', 
	@CrewGroupCD	NVARCHAR(1000)='', 
	@AircraftCD		NVARCHAR(1000)=''
	) 
	
	--EXEC spGetReportPOSTNonPilotFlightAnalysisExportInformation 'SUPERVISOR_99','10099','2013-01-01','2013-12-31'
AS

    DECLARE @IsZeroSuppressActivityCrewRpt BIT; 

    SELECT @IsZeroSuppressActivityCrewRpt = iszerosuppressactivitycrewrpt 
    FROM   company 
    WHERE  customerid = @UserCustomerID
          
   DECLARE @TenToMin INT
      DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

      
      SET @TenToMin= (SELECT COM.TimeDisplayTenMin
                      FROM   Company COM
                             JOIN UserMaster UM
                               ON COM.CustomerID = UM.CustomerID
                                  AND COM.HomebaseID = UM.HomebaseID
                      WHERE  UM.UserName = @UserCD)

-----------------------------Crew and Crew Group Filteration----------------------

DECLARE  @TempCrewID TABLE  
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO @TempCrewID
	SELECT DISTINCT C.CrewID 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @UserCustomerID  AND IsStatus = 1
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @UserCustomerID 
	AND CG.IsDeleted=0 AND  IsStatus = 1
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID
	FROM Crew C 
	WHERE C.CustomerID = @UserCustomerID  AND IsStatus = 1
END
-----------------------------Crew and Crew Group Filteration----------------------
----To Retrieve Crew and Crew Checklist information which are assigned to the log
SELECT 
       CrewCode,
       lognum,
       legid,
       type_code,
       LastName,
       flag,
       ISNULL(leg_cnt,0) leg_cnt,
       ISNULL(dutyend,'')  dutyend,
       ISNULL(dutyhrs,0) dutyhrs,
       ISNULL(flt_hrs,0) flt_hrs, 
       ISNULL(blk_hrs,0) blk_hrs,
       @TenToMin TenToMin,
       TOT_LINE='' FROM 
(
SELECT 
      C.CrewCD CrewCode,
      PM.LogNum AS lognum,
      ISNULL(PL.POLegID,0) AS legid,
      A.AircraftCD AS type_code,
      C.LastName,
      '' AS flag,
      COUNT(LegNUM) AS leg_cnt,
      '' dutyend,
      SUM(ROUND(PC.DutyHours,1)) dutyhrs,
      SUM(ROUND(PC.FlightHours,1)) flt_hrs,
      SUM(ROUND(PC.BlockHours,1)) blk_hrs
     
         FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN Fleet F ON F.FleetID = PM.FleetID
                                INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                                INNER JOIN Crew C ON C.CrewID=PC.CrewID
                                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                                LEFT OUTER JOIN Aircraft A ON A.AircraftID=PM.AircraftID
         WHERE PM.CustomerID=@UserCustomerID
           AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
           AND PC.DutyTYPE NOT IN('P','S')
           AND (A.AircraftCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
           AND PM.IsDeleted = 0
          GROUP BY C.CrewCD,C.LastName, LogNum,PL.POLegID,A.AircraftCD
          
 UNION ALL
 ----To Retrieve Crew and Crew Checklist information which are assigned to the log
    SELECT 
        C.CrewCD CrewCode,
        0,
        0,
        NULL,
        LastName,
        '',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL 
        FROM @TempCrewID TC INNER JOIN Crew C ON C.CrewID=TC.CrewID 
        WHERE CrewCD NOT IN(   SELECT DISTINCT C.CrewCD FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																				INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
																				INNER JOIN Crew C ON C.CrewID=PC.CrewID
																				INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
																				LEFT OUTER JOIN Aircraft A ON A.AircraftID=PM.AircraftID
														 WHERE PM.CustomerID=@UserCustomerID
														   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
														   AND PC.DutyTYPE NOT IN('P','S')
														   AND (A.AircraftCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
														   AND PM.IsDeleted = 0
														   )
														   
														   
        )NPCrew 
ORDER BY NPCrew.CrewCode   




GO


