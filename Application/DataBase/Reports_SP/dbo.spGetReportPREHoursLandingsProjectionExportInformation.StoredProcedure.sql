IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREHoursLandingsProjectionExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- ===============================================================================    
-- SPC Name: spGetReportPREHoursLandingsProjectionExportInformation   
-- Author: Abhishek S    
-- Create date: 1st Aug 2012    
-- Description: Get estimated hours and handlings excel report based on scheduled
--              trips   
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================ 
CREATE PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionExportInformation]
        @UserCD AS VARCHAR(30), -- Mandatory
		@DATEFROM AS DATETIME,  -- Mandatory
		@DATETO AS DATETIME,    -- Mandatory
		@TailNum AS NVARCHAR(300) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(300) = '', -- [Optional], Comma delimited string with mutiple values
	    @TripNum As NVARCHAR(500)= '', -- [Optional]
	    @IsHomeBase AS BIT = 0
   
AS

BEGIN
	
	SET NOCOUNT ON;
	
	  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
      DECLARE @ParameterDefinition AS NVARCHAR(100)

      -----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))   
    

 
CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
	AND F.IsDeleted=0
END
-------------------------------TailNum and Fleet Group Filteration----------------------	
 
IF @SuppressActivityAircft = 0  
BEGIN
SELECT DISTINCT TEMP.* FROM 
 (SELECT DISTINCT 
      [tail_nmbr] = F.TailNum
     ,[ac_code] = F.AircraftCD
     ,[type_code] = AT.AircraftCD 
	 ,[tot_line] = 'S'
	 ,[elp_time] = FLOOR(SUM(ISNULL(PL.ElapseTM,0))*10)*0.1
	 ,[flt_hrs] = 0
	 ,[landings] = COUNT(ISNULL(PL.LegID,0))
	 
	  FROM PreflightMain PM
       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0  
	   INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
			FROM Fleet F 					
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
	   LEFT OUTER JOIN(SELECT AircraftID, AircraftCD, AircraftTypeCD FROM Aircraft)AT ON AT.AircraftID = F.AircraftID 
	   WHERE PM.IsDeleted = 0
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND PM.TripStatus = 'T'
         AND PM.RecordType = 'T'
         AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
         AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
	     AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
	     GROUP BY F.TailNum,F.AircraftCD,AT.AircraftCD 
UNION ALL
SELECT DISTINCT F.TailNum,F.AircraftCD,A.AircraftCD,'S',FLOOR(ISNULL(0,0)*10)*0.1,0,0 FROM Fleet F
                                                     LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID
                                                     LEFT OUTER JOIN PreflightMain PM ON PM.FleetID=F.FleetID
                                                     INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
                                                     WHERE F.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                                                       AND F.TailNum NOT IN(SELECT DISTINCT F.TailNum FROM PreflightMain PM
																									   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0  
																									   INNER JOIN (
																											SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
																											FROM Fleet F 					
																										) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
																									   INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
																									   LEFT OUTER JOIN(SELECT AircraftID, AircraftCD, AircraftTypeCD FROM Aircraft)AT ON AT.AircraftID = F.AircraftID 
																									   WHERE PM.IsDeleted = 0
																										 AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
																										 AND PM.TripStatus = 'T'
																										 AND PM.RecordType = 'T'
																										 AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
																										 AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
																										 AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0))
	                                                     AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0))TEMP
ORDER BY TEMP.ac_code,TEMP.tail_nmbr
 

END 
ELSE
BEGIN
SELECT DISTINCT TEMP.* FROM 
 (SELECT DISTINCT 
      [tail_nmbr] = F.TailNum
     ,[ac_code] = CONVERT(VARCHAR(30),F.AircraftCD)
     ,[type_code] = AT.AircraftCD 
	 ,[tot_line] = 'R'
	 ,[elp_time] = FLOOR(SUM(ISNULL(PL.ElapseTM,0))*10)*0.1
	 ,[flt_hrs] = 0
	 ,[landings] = COUNT(ISNULL(PL.LegID,0))
	 
	  FROM PreflightMain PM
       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0  
	   INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
			FROM Fleet F 					
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
	   LEFT OUTER JOIN(SELECT AircraftID, AircraftCD, AircraftTypeCD FROM Aircraft)AT ON AT.AircraftID = F.AircraftID 
	   WHERE PM.IsDeleted = 0
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND PM.TripStatus = 'T'
         AND PM.RecordType = 'T'
         AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
         AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
	     AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
	     GROUP BY F.TailNum,F.AircraftCD,AT.AircraftCD)TEMP
ORDER BY TEMP.ac_code,TEMP.tail_nmbr
END


IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID

END

--EXEC spGetReportPREHoursLandingsProjectionExportInformation 'jwilliams_13','2000-07-20','2012-08-01','', '', '', 0
--EXEC spGetReportPREHoursLandingsProjectionExportInformation 'uc','2000-07-20','2012-08-01','', '', '', 0







GO


