IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDetailBillingByTripExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDetailBillingByTripExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTDetailBillingByTripExportInformation]
		(
		@UserCD AS VARCHAR(30), 
		@UserCustomerID AS BIGINT,
		@DATEFROM AS DATETIME, 
		@DATETO AS DATETIME, 
		@LogNum AS NVARCHAR(1000) = '',
		@DepartmentCD AS NVARCHAR(1000) = '', 
		@AuthorizationCD AS NVARCHAR(1000) = '',
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0
		)

AS
BEGIN

SET NOCOUNT ON
 
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

IF @LogNum <> ''

BEGIN 



SELECT TEMP.*,ROW_NUMBER()OVER(PARTITION BY legid ORDER BY legid, reqcode) Rnk   INTO #TripDetail FROM (
SELECT DISTINCT
       PL.LegNUM AS legnum,
       PO.LogNum AS lognum,
       CONVERT(DATE,PO.EstDepartureDT) AS estdepdt,
       CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [tail_nmbr],
       Req.PassengerRequestorCD AS reqcode,
       ISNULL(DEO.DepartmentCD,'') AS reqdept, 
       ISNULL(DAO.AuthorizationCD,'') AS reqauth,
       ISNULL(PL.POLegID,0) AS legid,
       AD.IcaoID AS [depicao_id],
       AA.IcaoID AS [arricao_id],
       PL.Distance AS DISTANCE,
       PL.ScheduledTM AS schedttm,
       ROUND(PL.FlightHours,1) AS  [flt_hrs], 
       ROUND(PL.BlockHours,1) AS [blk_hrs],
       ISNULL(DEL.DepartmentCD,'') AS [dept_code], 
       ISNULL(DAL.AuthorizationCD,'') AS [auth_code],
       FC.ChargeRate AS [chrg_rate],
       FC.ChargeUnit AS [chrg_unit],
       FL.MaximumPassenger AS [max_pax],
       AC.AircraftCD AS [type_code],
       ISNULL(PL.PassengerTotal,0) AS pax_total,
       [total_charges] = ISNULL(CASE WHEN FC.ChargeUnit = 'N' THEN  ((PL.Distance*FC.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
                              WHEN FC.ChargeUnit = 'S' THEN (((PL.Distance*FC.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                          END,0),
        paxcharges = FLOOR(ISNULL(CASE WHEN ISNULL(PL.PassengerTotal,0) = 0 THEN 0 ELSE (( 
										CASE WHEN FC.ChargeUnit = 'N' THEN  ((PL.Distance*FC.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
                              WHEN FC.ChargeUnit = 'S' THEN (((PL.Distance*FC.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                          END)/ PL.PassengerTotal ) END,0)*100)*0.01,          
        ISNULL(P.LastName,'')+CASE WHEN P.FirstName<>'' OR P.FirstName IS NOT NULL THEN  ISNULL(', '+P.FirstName,'') ELSE '' END AS paxname,
        FP.FlightPurposeCD AS fltpurpose,
        DPAX.DepartmentCD AS [RequiredDepartment],
        APAX.AuthorizationCD AS [RequiredAuthorization],
        '' AS CHARGE,
		EX.AccountDescription AS [EXP_ACCOUNT],
		EX.ExpenseAMT AS [EXP_AMOUNT],
        [TenToMin] = @TenToMin
       
FROM PostflightMain PO JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted=0
                       LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID 
                       LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID AND PL.CustomerID = P.CustomerID
                       LEFT JOIN Passenger Req ON Req.PassengerRequestorID = PO.PassengerRequestorID
                       LEFT JOIN Department DEO ON DEO.DepartmentID = PO.DepartmentID
                       LEFT JOIN DepartmentAuthorization DAO ON DAO.AuthorizationID = PO.AuthorizationID
                       LEFT JOIN Department DEL ON DEL.DepartmentID = PL.DepartmentID
                       LEFT JOIN DepartmentAuthorization DAL ON DAL.AuthorizationID = PL.AuthorizationID
                       --PAX Department And Authorization
					   LEFT OUTER JOIN Department DPAX ON P.DepartmentID = DPAX.DepartmentID
					   LEFT OUTER JOIN DepartmentAuthorization APAX ON APAX.AuthorizationID = P.AuthorizationID
                       JOIN Fleet FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID
                       --INNER JOIN FleetChargeRate FC ON PO.FleetID=FC.FleetID 
                       LEFT JOIN TailHistory TH ON FL.TailNum = TH.OldTailNUM AND FL.CustomerID = TH.CustomerID  
                       LEFT JOIN Company CO ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
                       LEFT JOIN Airport AI ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
                       LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID         
                       LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID 
                       LEFT JOIN Aircraft AC ON FL.AircraftID = AC.AircraftID
                       LEFT JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID                       
                       LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                                     SELECT ISNULL(SUM(PP2.ExpenseAMT),0)FROM PostflightExpense PP2 
                                  WHERE PP2.POLegID = PP4.POLegID)FROM PostflightExpense PP4 WHERE IsBilling =1 
                                  )PFE11 ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID  
 
                       LEFT JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,PP3.IsBilling ,ExpenseAmt = (
                               SELECT ISNULL(CAST(A.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                               FROM (SELECT PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, AccountDesc = P1.AccountDescription, ExpenseAmt = SUM(PP4.ExpenseAMT)
                               FROM PostflightExpense PP4 INNER JOIN Account P1 ON PP4.AccountID=P1.AccountID 
                               WHERE PP4.POLegID = PP3.POLegID AND PP4.IsBilling =1  
                               GROUP BY PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, P1.AccountDescription)A                              
                               ORDER BY A.ExpenseAMT DESC
                             FOR XML PATH(''))
                      --AccountDesc = (SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' 
                      --               FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                      --               WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1 FOR XML PATH(''))
                                     FROM PostflightExpense PP3 WHERE PP3.IsBilling =1) ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID
						 LEFT OUTER JOIN (
									SELECT POLegID, ChargeUnit, ChargeRate
									FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
														  INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
									AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
											   ) FC ON PL.POLegID = FC.POLegID
						LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
																		  INNER JOIN Account A ON PE.AccountID = A.AccountID
																	  WHERE PE.IsDeleted = 0
																	  AND PE.POLegID=PE1.POLegID
																	  FOR XML PATH('')
														)
								,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(30),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
																		  INNER JOIN Account A ON PE.AccountID = A.AccountID
																	  WHERE PE.IsDeleted = 0
																	  AND PE.POLegID=PE1.POLegID
																	  FOR XML PATH('')
														)
											FROM PostflightExpense PE1
									
								)EX ON EX.POLegID = PL.POLegID
                       
WHERE (PO.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '') 
  AND (DEO.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (DAO.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')   
  AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  AND PO.CustomerID = @UserCustomerID AND PO.IsDeleted = 0
)TEMP
END

ELSE

BEGIN
SELECT TEMP.*,ROW_NUMBER()OVER(PARTITION BY legid ORDER BY legid, reqcode) Rnk   INTO #TripDetail1 FROM(
SELECT DISTINCT
       PL.LegNUM AS legnum,
       PO.LogNum AS lognum,
       CONVERT(DATE,PO.EstDepartureDT) AS estdepdt,
       CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [tail_nmbr],
       Req.PassengerRequestorCD AS reqcode,
       ISNULL(DEO.DepartmentCD,'') AS reqdept, 
       ISNULL(DAO.AuthorizationCD,'') AS reqauth,
       ISNULL(PL.POLegID,0) AS legid,
       AD.IcaoID AS [depicao_id],
       AA.IcaoID AS [arricao_id],
       PL.Distance AS DISTANCE,
       PL.ScheduledTM AS schedttm,
       ROUND(PL.FlightHours,1) AS  [flt_hrs], 
       ROUND(PL.BlockHours,1) AS [blk_hrs],
       ISNULL(DEL.DepartmentCD,'') AS [dept_code], 
       ISNULL(DAL.AuthorizationCD,'') AS [auth_code],
       FC.ChargeRate AS [chrg_rate],
       FC.ChargeUnit AS [chrg_unit],
       FL.MaximumPassenger AS [max_pax],
       AC.AircraftCD AS [type_code],
       ISNULL(PL.PassengerTotal,0) AS pax_total,
       [total_charges] = ISNULL( CASE WHEN FC.ChargeUnit = 'N' THEN  ((PL.Distance*FC.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
                            WHEN FC.ChargeUnit = 'S' THEN (((PL.Distance*FC.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
                            WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                            WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                          END,0),
        paxcharges = FLOOR(ISNULL(CASE WHEN ISNULL(PL.PassengerTotal,0) = 0 THEN 0 ELSE (( 
										CASE WHEN FC.ChargeUnit = 'N' THEN  ((PL.Distance*FC.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
                              WHEN FC.ChargeUnit = 'S' THEN (((PL.Distance*FC.ChargeRate)*1.15078)+ISNULL(PFE11.SUMExpenseAmt,0)) 
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                              WHEN (FC.ChargeUnit = 'H'AND CO.AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *FC.ChargeRate)+ISNULL(PFE11.SUMExpenseAmt,0))
                          END)/ PL.PassengerTotal ) END,0)*100)*0.01,   
        ISNULL(P.LastName,'')+CASE WHEN P.FirstName<>'' OR P.FirstName IS NOT NULL THEN  ISNULL(', '+P.FirstName,'') ELSE '' END AS paxname,
         FP.FlightPurposeCD AS fltpurpose,
        DPAX.DepartmentCD AS [RequiredDepartment],
        APAX.AuthorizationCD AS [RequiredAuthorization],
        '' AS CHARGE,
		EX.AccountDescription AS [EXP_ACCOUNT],
		EX.ExpenseAMT AS [EXP_AMOUNT],
        [TenToMin] = @TenToMin
    
FROM PostflightMain PO JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted=0
                       LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID 
                       LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID AND PL.CustomerID = P.CustomerID
                       LEFT JOIN Passenger Req ON Req.PassengerRequestorID = PO.PassengerRequestorID
                       LEFT JOIN Department DEO ON DEO.DepartmentID = PO.DepartmentID
                       LEFT JOIN DepartmentAuthorization DAO ON DAO.AuthorizationID = PO.AuthorizationID
                       LEFT JOIN Department DEL ON DEL.DepartmentID = PL.DepartmentID
                       LEFT JOIN DepartmentAuthorization DAL ON DAL.AuthorizationID = PL.AuthorizationID
						--PAX Department And Authorization
						LEFT OUTER JOIN Department DPAX ON P.DepartmentID = DPAX.DepartmentID
						LEFT OUTER JOIN DepartmentAuthorization APAX ON APAX.AuthorizationID = P.AuthorizationID
                       JOIN vFleetGroup FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID
                       --INNER JOIN FleetChargeRate FC ON PO.FleetID=FC.FleetID 
                       LEFT JOIN TailHistory TH ON FL.TailNum = TH.OldTailNUM AND FL.CustomerID = TH.CustomerID  
                       LEFT JOIN Company CO ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
                       LEFT JOIN Airport AI ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
                       LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID         
                       LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID 
                       LEFT JOIN Aircraft AC ON AC.AircraftID = FL.AircraftID
                       LEFT JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID 
                       LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                                  SELECT ISNULL(SUM(PP2.ExpenseAMT),0) FROM PostflightExpense PP2 
                                  WHERE PP2.POLegID = PP4.POLegID)
                                 FROM PostflightExpense PP4 WHERE IsBilling =1 )PFE11 ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID
                       LEFT JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,PP3.IsBilling ,ExpenseAmt = (
                                SELECT ISNULL(CAST(A.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                                FROM (SELECT PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, AccountDesc = P1.AccountDescription, ExpenseAmt = SUM(PP4.ExpenseAMT)
                                FROM PostflightExpense PP4 INNER JOIN Account P1 ON PP4.AccountID=P1.AccountID 
                                WHERE PP4.POLegID = PP3.POLegID AND PP4.IsBilling =1 GROUP BY PP4.POLogID,PP4.POLegID,PP4.CustomerID,PP4.IsBilling, P1.AccountDescription)A
                               ORDER BY A.ExpenseAMT DESC FOR XML PATH(''))
                       --AccountDesc = (SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                       --        WHERE PP1.POLegID = PP3.POLegID AND PP1.IsBilling =1 FOR XML PATH(''))
                       FROM PostflightExpense PP3 WHERE PP3.IsBilling =1)
                       ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID
					   LEFT OUTER JOIN (
								SELECT POLegID, ChargeUnit, ChargeRate
								FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
													  INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
								AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
										   ) FC ON PL.POLegID = FC.POLegID  
												LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
																	  INNER JOIN Account A ON PE.AccountID = A.AccountID
																  WHERE PE.IsDeleted = 0
																  AND PE.POLegID=PE1.POLegID
																  FOR XML PATH('')
													)
							,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(30),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
																	  INNER JOIN Account A ON PE.AccountID = A.AccountID
																  WHERE PE.IsDeleted = 0
																  AND PE.POLegID=PE1.POLegID
																  FOR XML PATH('')
													)
										FROM PostflightExpense PE1
								
							)EX ON EX.POLegID = PL.POLegID                
  WHERE convert(date,PL.ScheduledTM) between convert(date,@DATEFROM) And convert(date,@DATETO)
  AND (DEO.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (DAO.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')
  AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  AND PO.CustomerID = @UserCustomerID AND PO.IsDeleted = 0
)TEMP 

END 


IF @LogNum<> ''
BEGIN
SELECT T1.*,TotalTrip=(CASE WHEN Rnk=1 THEN T2.TotalTrip ELSE 0 END) FROM #TripDetail T1 INNER JOIN (SELECT legid,CASE WHEN pax_total=0 THEN 0  ELSE SUM([total_charges]/pax_total) END TotalTrip FROM #TripDetail GROUP BY legid,pax_total)T2 ON T2.legid=t1.legid
ORDER BY T1.lognum, T1.legnum
END

ELSE
BEGIN
SELECT T1.*,TotalTrip=(CASE WHEN Rnk=1 THEN T2.TotalTrip ELSE 0 END) FROM #TripDetail1 T1 INNER JOIN (SELECT legid,SUM(CASE WHEN ISNULL(pax_total,0)=0 OR ISNULL(total_charges,0)=0 THEN 0  ELSE [total_charges]/pax_total END)  TotalTrip FROM #TripDetail1 GROUP BY legid,pax_total)T2 ON T2.legid=t1.legid
ORDER BY T1.lognum, T1.legnum
END

--DROP TABLE #TripDetail
IF OBJECT_ID('tempdb..#TripDetail') IS NOT NULL
DROP TABLE #TripDetail

IF OBJECT_ID('tempdb..#TripDetail1') IS NOT NULL
DROP TABLE #TripDetail1
 
END



GO


