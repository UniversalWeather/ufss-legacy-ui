IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBBudget]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBBudget]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spGetReportDBBudget] 
(
        @UserCD AS VARCHAR(30) --Mandatory
)
AS
 
-- ===============================================================================
-- SPC Name: [spGetReportDBBudget]
-- Author: RAVI
-- Create date: 01 August
-- Description: Get POST Delay Detail
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
SELECT
[AccountNumber] = B.AccountNum
,[AccountDescription] = A.AccountDescription
,[Month1] = B.Month1Budget
,[Month2] = B.Month2Budget
,[Month3] = B.Month3Budget
,[Month4] = B.Month4Budget
,[Month5] = B.Month5Budget
,[Month6] = B.Month6Budget
,[Month7] = B.Month7Budget
,[Month8] = B.Month8Budget
,[Month9] = B.Month9Budget
,[Month10] = B.Month10Budget
,[Month11] = B.Month11Budget
,[Month12] = B.Month12Budget
,[FiscalYear] = B.FiscalYear
,[TailNumber] = F.TailNum
FROM Account A
LEFT OUTER JOIN BUDGET B ON A.AccountID = B.AccountID
LEFT OUTER JOIN Fleet F ON F.FleetID = B.FleetID
WHERE B.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
-- And IsInActive=0

--EXEC spGetReportDBBudget 'SUPERVISOR_99'

GO


