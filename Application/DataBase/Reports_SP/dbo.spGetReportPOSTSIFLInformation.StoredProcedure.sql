IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSIFLInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSIFLInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportPOSTSIFLInformation]
       @UserCD AS VARCHAR(30),
	   @UserCustomerID VARCHAR(30),
	   @LogID VARCHAR(30) ---=404

-- ===============================================================================
-- SPC Name: spGetReportPOSTSIFLInformation
-- Author:  Askar
-- Create date: 11 Jul 2013
-- Description: Get Passenger SIFL Details 
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
AS
BEGIN
--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

SET NOCOUNT ON;
DECLARE @CUSTOMER BIGINT =@UserCustomerID


SELECT  FlightLog=PM.LogNum
               ,TailNumber=F.TailNum
               ,DispatchNum=PM.DispatchNum
               ,Description=PM.POMainDescription
               ,TripDate=dbo.GetShortDateFormatByUserCD(@UserCD, PM.EstDepartureDT) 
               ,CodeCNG=P.PassengerRequestorCD--PS.EmployeeTYPE
               ,PassengerName=CASE WHEN ISNULL(P.FirstName,'')<>'' THEN P.LastName+', '+ISNULL(P.FirstName,'') ELSE P.LastName END
               ,FromTo=AD.IcaoID
               ,DepArrCity=AD.CityName
               ,PlaneMult=F.TailNum
			   ,StatMiles=PS.Distance   
			   ,[0-500 Miles]=CONVERT(VARCHAR(20),PS.Distance1)
			   ,[501-1500 Miles]=CONVERT(VARCHAR(20),PS.Distance2)
			   ,[Over Miles]=CONVERT(VARCHAR(20),PS.Distance3)
			   ,[SIFL Total]=PS.AmtTotal 
			   ,[TeminalCharge]=NULL  
			   ,PL.LegNUM 
			   ,'A' OrderVal 
			   ,PS.EmployeeTYPE TypeCode   
			   ,AssociatedPassenger='Total for '+ISNULL(AP.PassengerRequestorCD,'')+' '+CASE WHEN AP.PassengerRequestorID IS NOT NULL THEN ( CASE WHEN ISNULL(AP.FirstName,'')<>'' THEN AP.LastName+', '+AP.FirstName ELSE AP.LastName END) ELSE 'NON-ASSOCIATED PAX' END
               ,PS.POLegID
               ,PassOrder=CASE WHEN ISNULL(P.FirstName,'')<>'' THEN P.LastName+', '+ISNULL(P.FirstName,'') ELSE P.LastName END
               ,POSIFLID
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID =PL.POLogID AND PM.CustomerID = PL.CustomerID 
																				AND PL.IsDeleted = 0
					   INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                       INNER JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       INNER JOIN PostflightSIFL PS ON PM.POLogID = PS.POLogID AND PP.PassengerID = PS.PassengerRequestorID AND PM.CustomerID = PS.CustomerID  AND PS.POLegID=PL.POLegID
                       INNER JOIN Airport AD ON PS.DepartICAOID=AD.AirportID
                       LEFT OUTER JOIN Passenger AP ON AP.PassengerRequestorID=PS.AssociatedPassengerID
WHERE PM.POLogID=CONVERT(BIGINT,@LogID)
 AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
 AND PM.IsDeleted = 0

UNION ALL

SELECT  FlightLog=PM.LogNum
               ,TailNumber=F.TailNum
               ,DispatchNum=PM.DispatchNum
               ,Description=PM.POMainDescription
               ,TripDate=NULL--dbo.GetShortDateFormatByUserCD(@UserCD, PM.EstDepartureDT) 
               ,CodeCNG=PS.EmployeeTYPE
               ,PassengerName=CASE WHEN PS.EmployeeTYPE='G' THEN 'Guest of '+AP.PassengerRequestorCD ELSE '' END
               ,FromTo=AA.IcaoID
               ,DepArrCity=AA.CityName
               ,PlaneMult=CONVERT(VARCHAR(9),PS.AircraftMultiplier)
               ,StatMiles=NULL 
               ,[0-500 Miles]=CONVERT(VARCHAR(20),PS.Rate1)
               ,[501-1500 Miles]=CONVERT(VARCHAR(20),PS.Rate2)
               ,[Over Miles]=CONVERT(VARCHAR(20),PS.Rate3)
               ,[SIFL Total]=NULL 
               ,[TeminalCharge]=PS.TeminalCharge 
               ,PL.LegNUM  
               ,'D' OrderVal   
               ,PS.EmployeeTYPE TypeCode 
               ,AssociatedPassenger='Total for '+ISNULL(AP.PassengerRequestorCD,'')+' '+CASE WHEN AP.PassengerRequestorID IS NOT NULL THEN ( CASE WHEN ISNULL(AP.FirstName,'')<>'' THEN AP.LastName+', '+ISNULL(AP.FirstName,'') ELSE AP.LastName END) ELSE 'NON-ASSOCIATED PAX' END
               ,PS.POLegID
               ,PassOrder=CASE WHEN ISNULL(P.FirstName,'')<>'' THEN P.LastName+', '+ISNULL(P.FirstName,'') ELSE P.LastName END
               ,POSIFLID
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID =PL.POLogID 
													AND PM.CustomerID = PL.CustomerID
													AND PL.IsDeleted = 0
					   INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                       INNER JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       INNER JOIN PostflightSIFL PS ON PM.POLogID = PS.POLogID AND PP.PassengerID = PS.PassengerRequestorID AND PM.CustomerID = PS.CustomerID  AND PS.POLegID=PL.POLegID 
                       INNER JOIN Airport AA ON PS.ArriveICAOID=AA.AirportID
                       LEFT OUTER JOIN Passenger AP ON AP.PassengerRequestorID=PS.AssociatedPassengerID  
WHERE PM.POLogID=CONVERT(BIGINT,@LogID)
 AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND PM.IsDeleted = 0
  
 ORDER BY LegNUM,PassOrder,POSIFLID,OrderVal

END


--EXEC spGetReportPOSTPassengerSIFLHistory '10013','','','','',1,'2010-01-18','2013-01-31'

GO