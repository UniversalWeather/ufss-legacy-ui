IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogExceptionMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogExceptionMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
Create   procedure  [dbo].[spGetReportPOSTFlightLogExceptionMhtmlInformation] (@UserCD VARCHAR(30),@DATEFROM AS DATETIME,@DATETO AS DATETIME)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTFlightLogExceptionInformation
-- Author: PremPrakash J
-- Create date: 31 Jul 2012
-- Description: Get FlightLog Exception Information EXCEL REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
 
--SET @UserCD='UC'
--SET @DATEFROM ='08/01/2012'
--SET @DATETO = '08/31/2012'
BEGIN

 SELECT distinct pfm.EstDepartureDT as 'estdepdt'  ,
		pfl.POLogID,
        pfm.LogNum as 'lognum' , 
        flt.TailNum  as 'tail_nmbr',
        pfm.IsException as  'IsException',
          pfl.LegNUM as 'Leg No.',
        CONVERT (VARCHAR, ADeprt.IcaoID)   as 'Exceptions/Dep',
        CONVERT (VARCHAR, AArr.IcaoID)    as 'Exceptions/Arr' ,
        pfl.ScheduledTM as 'Exceptions/Scheduled',
        pfte.ExceptionDescription as  'Exceptions/Warning'   
        
 
 INTO #tempExceptionvalue
   FROM
   Fleet flt
   INNER JOIN PostflightMain pfm ON pfm.FleetID = flt.FleetID AND pfm.IsDeleted=0
   INNER JOIN PostflightLeg pfl  ON pfl.POLogID = pfm.POLogID AND pfl.IsDeleted=0
   INNER JOIN PostflightTripException pfte ON pfte.POLogID = pfl.POLogID  
   INNER JOIN Airport ADeprt ON ADeprt.AirportID = pfl.DepartICAOID
   INNER JOIN Airport AArr ON AArr.AirportID = pfl.ArriveICAOID  
          
   WHERE pfm.CustomerID =dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) AND 
   CONVERT(DATE,PFL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
     
  SELECT DISTINCT estdepdt,lognum,[tail_nmbr],IsException,[Exceptions/Dep],[Exceptions/Arr],[Exceptions/Scheduled],[Exceptions/Warning],[Leg No.]  FROM #tempExceptionvalue
   
 END


GO


