IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFuelConversion]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFuelConversion]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================================
-- Function Name: GetFuelConversion
-- Author: RAMESH J
-- Create date: 06 Aug 2012
-- Description: Returns Quantity in the format set in CompanyProfile for the User Company and Homebase
-- Revision History
-- Date            Name        Ver        Change
-- 27-11-2012  Mathes               Decimal Place increased from 2 to 4
--
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetFuelConversion] (@FromUnit INT, @FromQuantity INT, @UserCD VARCHAR(30))
RETURNS NUMERIC (15,4)
WITH EXECUTE AS CALLER
AS
BEGIN
    /*
    *- From/To Units:
        *-     1=Gallons
        *-     2=Liters
        *-    3=Imp Gallons
        *-  4=Pounds
        *- 5=Kilos
    */
    DECLARE @ToUnits NUMERIC(15,4); --Variable to fetch the conversion units
    DECLARE @ToQuantity NUMERIC(15,4); --Variable to return the converted quantity

    --Fetch ToUnits set in CompanyProfile settings for the UserCompany & HomeBase
    SELECT @ToUnits = FuelPurchase
    FROM COMPANY
    WHERE CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
      AND HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))


    SELECT     @ToQuantity =
       
    CASE                           
        WHEN @FromUnit = 1 THEN        -- From Gallons
       
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromQuantity
                WHEN @ToUnits = 2    THEN            -- To Liters
                    @FromQuantity * 3.7854 --12
                WHEN @ToUnits = 3    THEN            -- To Imp Gallons
                    @FromQuantity * 0.83267 --738
                WHEN @ToUnits = 4    THEN            -- To Pounds
                    @FromQuantity * 6.7000
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromQuantity * 3.0390664
            END
       
        WHEN @FromUnit = 2 THEN        -- From Liters
           
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromQuantity * 0.2641721
                WHEN @ToUnits = 2 THEN                -- To Liters
                    @FromQuantity
                WHEN @ToUnits = 3    THEN            -- To Imp Gallons
                    @FromQuantity * 0.2199692
                WHEN @ToUnits = 4    THEN            -- To Pounds
                    @FromQuantity * 1.8
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromQuantity *    .816465
            END
        WHEN @FromUnit = 3    THEN    -- From Imp Gallons
       
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromQuantity * 1.20095
                WHEN @ToUnits = 2 THEN                -- To Liters
                    @FromQuantity * 4.546092
                WHEN @ToUnits = 3 THEN                -- To Imp Gallons
                    @FromQuantity
                WHEN @ToUnits = 4 THEN                -- To Pounds
                     @FromQuantity * 8.0467
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromQuantity * 3.649918
                   
            END
           
        WHEN @FromUnit = 4    THEN    -- From Pounds
       
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromQuantity * .149253
                WHEN @ToUnits = 2    THEN            -- To Liters
                    @FromQuantity * .555555
                WHEN @ToUnits = 3    THEN            -- To Imp Gallons
                    @FromQuantity * .124274
                WHEN @ToUnits = 4    THEN            -- To Pounds
                    @FromQuantity
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromQuantity * .453592   
            END
           
        WHEN @FromUnit = 5    THEN    -- From Pounds
       
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromQuantity * .3290484
                WHEN @ToUnits = 2    THEN            -- To Liters
                    @FromQuantity * 1.2247913
                WHEN @ToUnits = 3    THEN            -- To Imp Gallons
                    @FromQuantity * .2739787
                WHEN @ToUnits = 4    THEN            -- To Pounds
                    @FromQuantity * 2.20462
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromQuantity    
            END   
                       
    ELSE
        @FromQuantity
    END

       
    RETURN @ToQuantity;
END;
--Input FromUnit, FromQuantity, UserCD
--SELECT DBO.GetFuelConversion(1,1,'JWILLIAMS_13')





GO


