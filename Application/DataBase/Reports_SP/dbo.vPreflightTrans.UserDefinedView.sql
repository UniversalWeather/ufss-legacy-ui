IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vPreflightTrans]'))
DROP VIEW [dbo].[vPreflightTrans]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[vPreflightTrans]  AS 
SELECT PM.TripNUM
      ,PM.TripID
      ,PM.RecordType
      ,PM.TripDescription
      ,PM.CustomerID
      ,PM.TripStatus
      ,PL.LegID
      ,PL.LegNUM
      ,PL.DepartureDTTMLocal
      ,PL.ArrivalDTTMLocal
      ,PL.ElapseTM
      ,PL.DutyHours
      ,PL.DutyTYPE
      ,PL.DepartureGreenwichDTTM
      ,PL.ArrivalGreenwichDTTM
      ,PL.Distance
      ,PL.AuthorizationID
      ,PL.FlightCategoryID
      ,PL.DepartmentID
      ,PL.PassengerRequestorID
      ,PL.PassengerTotal
      ,F.TailNum
      ,F.FleetID
      ,A.IcaoID DepartICAOID
      ,AA.IcaoID ArriveICAOID
      ,FC.FlightCatagoryCD
      ,AT.AircraftCD
      ,RequestorName=P.PassengerRequestorCD
      ,PL.NextLocalDTTM
      ,PL.IsApproxTM
      ,F.AircraftCD Ac_Code 
      ,P.LastName
      ,P.FirstName
      ,P.MiddleInitial
      ,P.PhoneNum
      ,PM.HomebaseID
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN Fleet F ON F.FleetID=PM.FleetID 
              LEFT OUTER JOIN Airport A ON A.AirportID = PL.DepartICAOID
		      LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
		      LEFT JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
		      LEFT OUTER JOIN AIRCRAFT AT ON PM.AircraftID=AT.AircraftID
		      LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
		WHERE PM.IsDeleted=0
		  


GO


