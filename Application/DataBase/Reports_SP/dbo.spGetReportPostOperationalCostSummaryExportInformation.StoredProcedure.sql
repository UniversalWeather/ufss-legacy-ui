IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostOperationalCostSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostOperationalCostSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPostOperationalCostSummaryExportInformation]
    ( 
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
	@TailNum AS VARCHAR(100) = '',
	@FiscalYear AS NVARCHAR(4),
	@FleetGroupCD AS NVARCHAR(1000) = ''
	)
AS

SET NOCOUNT ON;
BEGIN


DECLARE  @TempFleetID TABLE     
    (   
    ID INT NOT NULL IDENTITY (1,1),
    FleetID BIGINT,
    TailNum VARCHAR(9)
  )
 
IF @TailNum <> ''
BEGIN
    INSERT INTO @TempFleetID
    SELECT DISTINCT F.FleetID,F.TailNum
    FROM Fleet F
    WHERE F.CustomerID = @UserCustomerID
    AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
    SELECT DISTINCT  F.FleetID,F.TailNum
    FROM Fleet F
    LEFT OUTER JOIN FleetGroupOrder FGO
    ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
    LEFT OUTER JOIN FleetGroup FG
    ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
    WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
    AND F.CustomerID = @UserCustomerID  
    AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
    SELECT DISTINCT  F.FleetID,F.TailNum
    FROM Fleet F
    WHERE F.CustomerID = @UserCustomerID  
    AND F.IsDeleted=0
    AND F.IsInActive=0
END

DECLARE @MonthStart INT,@MonthEnd INT;
SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@FiscalYear-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @FiscalYear+1 ELSE @FiscalYear END-1900,0))))
DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
       AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @AircraftBasis numeric(1,0);
      SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
       AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
       

SELECT * INTO #TEMP1 FROM
(SELECT DISTINCT
       @FromDate MONTHSTART1,
       @ToDate MONTHEND1,
       DATEADD(MONTH,-12,@FromDate) MONTHSTART2 ,
       DATEADD(MONTH,-12,@ToDate) MONTHEND2,
       DATEADD(MONTH,-24,@FromDate)MONTHSTART3,
       DATEADD(MONTH,-24,@ToDate)MONTHEND3,
       DATEADD(MONTH,-36,@FromDate)MONTHSTART4,
       DATEADD(MONTH,-36,@ToDate) MONTHEND4
FROM PostflightExpense WHERE CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate))T1
DECLARE @AccountNum TABLE(RowID INT,
                          AccountNum VARCHAR(32))


INSERT INTO @AccountNum
SELECT DISTINCT 1,FederalTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                   WHERE C.CustomerID=@UserCustomerID
                   AND FederalTax IS NOT NULL
                   AND FederalTax <> ''
                   
UNION ALL
SELECT DISTINCT 2,SaleTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
               WHERE C.CustomerID=@UserCustomerID
               AND SaleTax IS NOT NULL
               AND SaleTax <> ''
UNION ALL
SELECT DISTINCT 3,StateTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                   WHERE C.CustomerID=@UserCustomerID
                   AND StateTax IS NOT NULL
                   AND StateTax <> ''



---------------------------------------------------------------------------------------------------------------   


                       
SELECT Y1.* INTO #YEAR1 FROM
(
 SELECT DISTINCT 
       T1.Tail_Number ,
       T1.FleetID,
       T1.AircraftCD,
       T1.MONTHSTART1,
       T1.MONTHEND1,
       T1.MONTHSTART2,
       T1.MONTHEND2,
       T1.MONTHSTART3,
       T1.MONTHEND3,
       T1.MONTHSTART4,
       T1.MONTHEND4,
       SUM(AMT0) OVER (PARTITION BY AccountNum,FleetID) AMT0,
       T1.AMT1,
       T1.AMT2,
       T1.AMT3,
       T1.AccountNum,
       T1.AccountDescription,
       T1.FlightBlockHoursLabel,
       T1.FlightBlockHours0,
       T1.FlightBlockHours1,
       T1.FlightBlockHours2,
       T1.FlightBlockHours3,
       T1.year0fed,
       T1.year1fed,
       T1.year2fed,
       T1.year3fed,
       T1.year0state,
       T1.year1state,
       T1.year2state,
       T1.year3state,
       T1.year0sales,
       T1.year1sales,
       T1.year2sales,
       T1.year3sales,      
       T1.TenToMin ,
       ROW_NUMBER() OVER (PARTITION BY AccountNum,FleetID ORDER BY AccountNum,FleetID)  Rnk,
       ROW_NUMBER() OVER (PARTITION BY FleetID ORDER BY FleetID)  RnkBlkFlt
  FROM 
(
SELECT DISTINCT
      [Tail_Number]=FG.TailNum --CASE WHEN TH.OldTailNUM = FG.TailNum THEN TH.NewTailNUM ELSE FG.TailNum END AS
      ,FG.FleetID
      ,FG.AircraftCD
     --,PE.POLogID
      ,MONTHSTART1
      ,MONTHEND1
      ,MONTHSTART2
      ,MONTHEND2
      ,MONTHSTART3
      ,MONTHEND3
      ,MONTHSTART4
      ,MONTHEND4
      
      ,AMT0 = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX
                   WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX
                   WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX
                       ELSE ISNULL(PE.ExpenseAMT,0) END
      ,0 AS AMT1
      ,0 AS AMT2
      ,0 AS AMT3
      ,AC.AccountNum     
      ,AC.AccountDescription
      ,FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk')
                                                   WHEN 2 THEN CONVERT(VARCHAR(30),'flt') END
     
      ,FlightBlockHours0=BlkFlt.HoursVal---CASE WHEN @AircraftBasis = 1 THEN PL.BlockHours WHEN @AircraftBasis = 2 THEN PL.FlightHours  END AS FlightBlockHours0
      ,0 AS FlightBlockHours1                                                                           
      ,0 AS FlightBlockHours2
      ,0 AS FlightBlockHours3
      ,PE.FederalTAX AS year0fed
      ,0 AS  year1fed
      ,0 AS year2fed
      ,0 AS year3fed
      ,PE.SateTAX AS year0state
      ,0 AS year1state
      ,0 AS year2state
      ,0 AS year3state
      ,PE.SaleTAX AS year0sales
      ,0 AS year1sales
      ,0 AS year2sales
      ,0 AS year3sales
      ,[TenToMin] = @TenToMin 
      ,SlipNUM
FROM PostflightMain PM CROSS JOIN #TEMP1
JOIN PostflightLeg PL ON PM.POLogID  = PL.POLogID AND PL.IsDeleted = 0
JOIN PostflightExpense PE ON  PL.POLogID = PE.POLogID AND PE.POLegID=PL.POLegID AND PE.IsDeleted = 0
JOIN Account AC ON AC.AccountID = PE.AccountID
JOIN Fleet FG ON FG.FleetID = PM.FleetID
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
--LEFT JOIN TailHistory TH ON TH.OldTailNUM = FG.TailNum AND TH.CustomerID = FG.CustomerID
LEFT OUTER  JOIN Company C ON C.HomebaseID=AC.HomebaseID
LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
LEFT OUTER JOIN (SELECT PM.FleetID,SUM(CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1)  WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1)  END) HoursVal FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
								 WHERE PM.CustomerID=@UserCustomerID
								  AND PM.IsDeleted=0
                                  AND CONVERT(DATE,PL.ScheduledTM)BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)
                                  GROUP BY PM.FleetID)BlkFlt ON BlkFlt.FleetID=PM.FleetID

 WHERE CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate) 
 --AND CONVERT(DATE,PL.ScheduledTM)BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)
 AND PM.IsDeleted =0 AND PM.CustomerID = @UserCustomerID
)T1
UNION
 SELECT DISTINCT 
       T2.Tail_Number ,
       T2.FleetID,
       T2.AircraftCD,
       T2.MONTHSTART1,
       T2.MONTHEND1,
       T2.MONTHSTART2,
       T2.MONTHEND2,
       T2.MONTHSTART3,
       T2.MONTHEND3,
       T2.MONTHSTART4,
       T2.MONTHEND4,
       T2.AMT0,
       SUM(AMT1) OVER (PARTITION BY AccountNum,FleetID) AMT1,
       T2.AMT2,
       T2.AMT3,
       T2.AccountNum,
       T2.AccountDescription,
       T2.FlightBlockHoursLabel,
       T2.FlightBlockHours0,
       T2.FlightBlockHours1,
       T2.FlightBlockHours2,
       T2.FlightBlockHours3,
       T2.year0fed,
       T2.year1fed,
       T2.year2fed,
       T2.year3fed,
       T2.year0state,
       T2.year1state,
       T2.year2state,
       T2.year3state,
       T2.year0sales,
       T2.year1sales,
       T2.year2sales,
       T2.year3sales,
       T2.TenToMin,
       ROW_NUMBER() OVER (PARTITION BY AccountNum,FleetID ORDER BY AccountNum,FleetID) Rnk,
       ROW_NUMBER() OVER (PARTITION BY FleetID ORDER BY FleetID)  RnkBlkFlt
  FROM 
(
SELECT DISTINCT
      [Tail_Number]=FG.TailNum --CASE WHEN TH.OldTailNUM = FG.TailNum THEN TH.NewTailNUM ELSE FG.TailNum END AS
      ,FG.FleetID
      ,FG.AircraftCD
     --,PE.POLogID
      ,MONTHSTART1
      ,MONTHEND1
      ,MONTHSTART2
      ,MONTHEND2
      ,MONTHSTART3
      ,MONTHEND3
      ,MONTHSTART4
      ,MONTHEND4
      ,0 AS AMT0
      ,AMT1 = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX
                   WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX
                   WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX
                       ELSE ISNULL(PE.ExpenseAMT,0) END
      
      ,0 AS AMT2
      ,0 AS AMT3
      ,AC.AccountNum     
      ,AC.AccountDescription
      ,FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk')
                                                   WHEN 2 THEN CONVERT(VARCHAR(30),'flt') END
      ,0 AS FlightBlockHours0
      ,FlightBlockHours1=BlkFlt.HoursVal---CASE WHEN @AircraftBasis = 1 THEN PL.BlockHours WHEN @AircraftBasis = 2 THEN PL.FlightHours  END AS FlightBlockHours1                                                                           
      ,0 AS FlightBlockHours2
      ,0 AS FlightBlockHours3
      ,0 AS year0fed
      ,PE.FederalTAX AS year1fed
      ,0 AS year2fed
      ,0 AS year3fed
      ,0 AS year0state
      ,PE.SateTAX AS year1state
      ,0 AS year2state
      ,0 AS year3state
      ,0 AS year0sales
      ,PE.SaleTAX AS year1sales
      ,0 AS year2sales
      ,0 AS year3sales      
      ,[TenToMin] = @TenToMin 
      ,SlipNUM
FROM PostflightMain PM CROSS JOIN #TEMP1
JOIN PostflightLeg PL ON PM.POLogID  = PL.POLogID AND PL.IsDeleted = 0
JOIN PostflightExpense PE ON  PL.POLogID = PE.POLogID AND PE.POLegID=PL.POLegID AND PE.IsDeleted = 0
JOIN Account AC ON AC.AccountID = PE.AccountID
JOIN Fleet FG ON FG.FleetID = PM.FleetID
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
--LEFT JOIN TailHistory TH ON TH.OldTailNUM = FG.TailNum AND TH.CustomerID = FG.CustomerID
LEFT OUTER  JOIN Company C ON C.HomebaseID=AC.HomebaseID
LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
LEFT OUTER JOIN (SELECT PM.FleetID,SUM(CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1)  END) HoursVal FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
								 WHERE PM.CustomerID=@UserCustomerID
								  AND PM.IsDeleted=0
                                  AND CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate)
                                  GROUP BY PM.FleetID)BlkFlt ON BlkFlt.FleetID=PM.FleetID

 WHERE CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate)
 --AND (CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate))
  AND PM.IsDeleted =0 AND PM.CustomerID = @UserCustomerID
)T2

UNION
SELECT DISTINCT 
       T3.Tail_Number ,
       T3.FleetID,
       T3.AircraftCD,
       T3.MONTHSTART1,
       T3.MONTHEND1,
       T3.MONTHSTART2,
       T3.MONTHEND2,
       T3.MONTHSTART3,
       T3.MONTHEND3,
       T3.MONTHSTART4,
       T3.MONTHEND4,
       T3.AMT0,
       T3.AMT1,
       SUM(AMT2) OVER (PARTITION BY AccountNum,FleetID) AMT2,
       T3.AMT3,
       T3.AccountNum,
       T3.AccountDescription,
       T3.FlightBlockHoursLabel,
       T3.FlightBlockHours0,
       T3.FlightBlockHours1,
       T3.FlightBlockHours2,
       T3.FlightBlockHours3,
       T3.year0fed,
       T3.year1fed,
       T3.year2fed,
       T3.year3fed,
       T3.year0state,
       T3.year1state,
       T3.year2state,
       T3.year3state,
       T3.year0sales,
       T3.year1sales,
       T3.year2sales,
       T3.year3sales,
       T3.TenToMin,
       ROW_NUMBER() OVER (PARTITION BY AccountNum,FleetID ORDER BY AccountNum,FleetID)  Rnk,
       ROW_NUMBER() OVER (PARTITION BY FleetID ORDER BY FleetID)  RnkBlkFlt
  FROM 
(
SELECT DISTINCT
      [Tail_Number]=FG.TailNum --CASE WHEN TH.OldTailNUM = FG.TailNum THEN TH.NewTailNUM ELSE FG.TailNum END AS
      ,FG.FleetID
      ,FG.AircraftCD
     --,PE.POLogID
      ,MONTHSTART1
      ,MONTHEND1
      ,MONTHSTART2
      ,MONTHEND2
      ,MONTHSTART3
      ,MONTHEND3
      ,MONTHSTART4
      ,MONTHEND4
      ,0 AS AMT0
      ,0 AS AMT1
      ,AMT2 = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX
                   WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX
                   WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX
                       ELSE ISNULL(PE.ExpenseAMT,0) END
      ,0 AS AMT3
      ,AC.AccountNum     
      ,AC.AccountDescription
      ,FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk')
                                                   WHEN 2 THEN CONVERT(VARCHAR(30),'flt') END
      ,0 AS FlightBlockHours0                                                                          
      ,0 AS FlightBlockHours1
      ,FlightBlockHours2=BlkFlt.HoursVal---CASE WHEN @AircraftBasis = 1 THEN PL.BlockHours WHEN @AircraftBasis = 2 THEN PL.FlightHours  END AS FlightBlockHours2
      ,0 AS FlightBlockHours3
      ,0 AS year0fed
      ,0 AS year1fed
      ,PE.FederalTAX AS year2fed
      ,0 AS year3fed
      ,0 AS year0state
      ,0 AS year1state
      ,PE.SateTAX AS year2state
      ,0 AS year3state
      ,0 AS year0sales
      ,0 AS year1sales
      ,PE.SaleTAX AS year2sales
      ,0 AS year3sales
      ,[TenToMin] = @TenToMin 
      ,SlipNUM
FROM PostflightMain PM CROSS JOIN #TEMP1
JOIN PostflightLeg PL ON PM.POLogID  = PL.POLogID AND PL.IsDeleted = 0
JOIN PostflightExpense PE ON  PL.POLogID = PE.POLogID AND PE.POLegID=PL.POLegID AND PE.IsDeleted = 0
JOIN Account AC ON AC.AccountID = PE.AccountID
JOIN Fleet FG ON FG.FleetID = PM.FleetID
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
--LEFT JOIN TailHistory TH ON TH.OldTailNUM = FG.TailNum AND TH.CustomerID = FG.CustomerID
LEFT OUTER  JOIN Company C ON C.HomebaseID=AC.HomebaseID
LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
LEFT OUTER JOIN (SELECT PM.FleetID,SUM(CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1)  END) HoursVal FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
								 WHERE PM.CustomerID=@UserCustomerID
								  AND PM.IsDeleted=0
                                  AND CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate) 
                                  GROUP BY PM.FleetID)BlkFlt ON BlkFlt.FleetID=PM.FleetID

 WHERE CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate) 
 --AND (CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate))
 AND PM.IsDeleted =0 AND PM.CustomerID = @UserCustomerID
)T3

 UNION
SELECT DISTINCT 
       T4.Tail_Number ,
       T4.FleetID,
       T4.AircraftCD,
       T4.MONTHSTART1,
       T4.MONTHEND1,
       T4.MONTHSTART2,
       T4.MONTHEND2,
       T4.MONTHSTART3,
       T4.MONTHEND3,
       T4.MONTHSTART4,
       T4.MONTHEND4,
       T4.AMT0,
       T4.AMT1,
       T4.AMT2,
       SUM(AMT3) OVER (PARTITION BY AccountNum,FleetID) AMT3,
       T4.AccountNum,
       T4.AccountDescription,
       T4.FlightBlockHoursLabel,
       T4.FlightBlockHours0,
       T4.FlightBlockHours1,
       T4.FlightBlockHours2,
       T4.FlightBlockHours3,
       T4.year0fed,
       T4.year1fed,
       T4.year2fed,
       T4.year3fed,
       T4.year0state,
       T4.year1state,
       T4.year2state,
       T4.year3state,
       T4.year0sales,
       T4.year1sales,
       T4.year2sales,
       T4.year3sales,
       T4.TenToMin,
       ROW_NUMBER() OVER (PARTITION BY AccountNum,FleetID ORDER BY AccountNum,FleetID) Rnk,
       ROW_NUMBER() OVER (PARTITION BY FleetID ORDER BY FleetID)  RnkBlkFlt 
  FROM 
(
SELECT DISTINCT
      [Tail_Number]=FG.TailNum --CASE WHEN TH.OldTailNUM = FG.TailNum THEN TH.NewTailNUM ELSE FG.TailNum END AS
      ,FG.FleetID
      ,FG.AircraftCD
     --,PE.POLogID
      ,MONTHSTART1
      ,MONTHEND1
      ,MONTHSTART2
      ,MONTHEND2
      ,MONTHSTART3
      ,MONTHEND3
      ,MONTHSTART4
      ,MONTHEND4
      ,0 AS AMT0
      ,0 AS AMT1
      ,0 AS AMT2
      ,AMT3 = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX
                   WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX
                   WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX
                       ELSE ISNULL(PE.ExpenseAMT,0) END
      ,AC.AccountNum     
      ,AC.AccountDescription
      ,FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk')
                                                   WHEN 2 THEN CONVERT(VARCHAR(30),'flt') END
      ,0 AS FlightBlockHours0
      ,0 AS FlightBlockHours1                                                                        
      ,0 AS FlightBlockHours2
      ,FlightBlockHours3=BlkFlt.HoursVal---CASE WHEN @AircraftBasis = 1 THEN PL.BlockHours WHEN @AircraftBasis = 2 THEN PL.FlightHours  END AS FlightBlockHours3
      ,0 AS year0fed
      ,0 AS year1fed
      ,0 AS year2fed
      ,PE.FederalTAX AS year3fed
      ,0 AS year0state
      ,0 AS year1state
      ,0 AS year2state
      ,PE.SateTAX AS year3state
      ,0 AS year0sales
      ,0 AS year1sales
      ,0 AS year2sales
      ,PE.SaleTAX AS year3sales
      ,[TenToMin] = @TenToMin 
      ,SlipNUM
FROM PostflightMain PM CROSS JOIN #TEMP1
JOIN PostflightLeg PL ON PM.POLogID  = PL.POLogID AND PL.IsDeleted = 0
JOIN PostflightExpense PE ON  PL.POLogID = PE.POLogID AND PE.POLegID=PL.POLegID AND PE.IsDeleted = 0
JOIN Account AC ON AC.AccountID = PE.AccountID
JOIN Fleet FG ON FG.FleetID = PM.FleetID
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
---LEFT JOIN TailHistory TH ON TH.OldTailNUM = FG.TailNum AND TH.CustomerID = FG.CustomerID
LEFT OUTER  JOIN Company C ON C.HomebaseID=AC.HomebaseID
LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
LEFT OUTER JOIN (SELECT PM.FleetID,SUM(CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1)  END) HoursVal FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
								 WHERE PM.CustomerID=@UserCustomerID
								  AND PM.IsDeleted=0
                                  AND CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate)
                                  GROUP BY PM.FleetID)BlkFlt ON BlkFlt.FleetID=PM.FleetID

 WHERE CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate)
 --AND (CONVERT(DATE,PL.ScheduledTM)BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate))
  AND PM.IsDeleted =0 AND PM.CustomerID = @UserCustomerID
)T4
)Y1



-----------------------------------------------------------------------------------------------------------------------------------                   

INSERT INTO #YEAR1
SELECT TT.TailNum ,
       TT.FleetID,
       TT.AircraftCD,
       MONTHSTART1,
       MONTHEND1,
       MONTHSTART2,
       MONTHEND2,
       MONTHSTART3,
       MONTHEND3,
       MONTHSTART4,
       MONTHEND4,
       AMT0,
       AMT1,
       AMT2,
       AMT3,
       AccountNum,
       AccountDescription,
      FlightBlockHoursLabel,
       0 AS FlightBlockHours0,
       0 AS FlightBlockHours1,
       0 AS FlightBlockHours2,
       0 AS FlightBlockHours3,
       0 AS year0fed,
       0 AS year1fed,
       0 AS year2fed,
       0 AS year3fed,
       0 AS year0state,
       0 AS year1state,
       0 AS year2state,
       0 AS year3state,
       0 AS year0sales,
       0 AS year1sales,
       0 AS year2sales,
       0 AS year3sales, 
       TENTOMIN = @TenToMin,1 Rnk,1 RnkBlkFlt FROM 
(
SELECT DISTINCT
       0 AS [Tail_Number] ,
       0 AS FLEETID,
       0 AS AircraftCD,
       T.MONTHSTART1,
       T.MONTHEND1,
       T.MONTHSTART2,
       T.MONTHEND2,
       T.MONTHSTART3,
       T.MONTHEND3,
       T.MONTHSTART4,
       T.MONTHEND4,
       0 AS AMT0,
       0 AS AMT1,
       0 AS AMT2,
       0 AS AMT3,
       AC.AccountNum,
       AccountDescription,
      FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk')
                                                  WHEN 2 THEN CONVERT(VARCHAR(30),'flt') END,
       0 AS FlightBlockHours0,
       0 AS FlightBlockHours1,
       0 AS FlightBlockHours2,
       0 AS FlightBlockHours3,
       0 AS year0fed,
       0 AS year1fed,
       0 AS year2fed,
       0 AS year3fed,
       0 AS year0state,
       0 AS year1state,
       0 AS year2state,
       0 AS year3state,
       0 AS year0sales,
       0 AS year1sales,
       0 AS year2sales,
       0 AS year3sales,
       TENTOMIN = @TenToMin
FROM Account AC CROSS JOIN #TEMP1 T
        INNER JOIN @AccountNum AN ON AC.AccountNum = AN.AccountNum
        INNER JOIN Company C ON AC.HomebaseID=C.HomebaseID
        INNER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
        WHERE  C.CustomerID = @UserCustomerID
        AND AC.AccountNum NOT IN(SELECT DISTINCT AccountNum FROM #YEAR1)
                       
 )TEMP
CROSS JOIN (SELECT DISTINCT  F.FleetID,F.TailNum,F.AircraftCD FROM  PostflightMain PM
                             INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                             INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                             INNER JOIN Fleet F ON PE.FleetID=F.FleetID 
                             INNER JOIN @TempFleetID TI ON F.FleetID = TI.FleetID 
                      WHERE PM.CustomerID=@UserCustomerID AND F.IsDeleted = 0 
                      AND (CONVERT(DATE,PurchaseDT) BETWEEN    DATEADD(MONTH,-36,@FromDate) AND CONVERT(DATE,@ToDate))
                     )TT
    
     
   --SELECT FederalTax,HomebaseID from Company   where CustomerID=10099   
       


------------------------------------------------------------------------------------------------------------------------------------
UPDATE #YEAR1 SET AMT0=ActualVal.FederalTAX,[Tail_Number]=TailNum  FROM
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc,TailNum,FleetID Fleet FROM
(

SELECT DISTINCT SlipNUM,AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PE.CustomerID = @UserCustomerID AND PM.IsDeleted = 0
        )Actual  
GROUP BY Actual.FederalTaxAc,TailNum,FleetID 
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNum
AND ActualVal.Fleet=#YEAR1.FleetID



UPDATE #YEAR1 SET AMT0=ActualVal.SaleTAX,[Tail_Number]=TailNum  FROM
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2) AC ON AC.AccountNum=C.SaleTax
WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SalesTaxAc,TailNum,FleetID   
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


 
UPDATE #YEAR1 SET AMT0=ActualVal.SaleTAX,[Tail_Number]=TailNum FROM
(
SELECT SUM(Actual.SateTAX)SaleTAX,Actual.SateTaxAc,TailNum,FleetID Fleet  FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum,F.TailNum, F.FleetID  FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3) AC ON AC.AccountNum=C.StateTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SateTaxAc,TailNum,FleetID   
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


--------------------------------------------------------------------------------------------------------------------------------------------

UPDATE #YEAR1 SET AMT1=ActualVal.FederalTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.FederalTaxAc,TailNum,FleetID    
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


UPDATE #YEAR1 SET AMT1=ActualVal.SaleTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2)AC ON AC.AccountNum=C.SaleTax
WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SalesTaxAc,TailNum ,FleetID   
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID
 
UPDATE #YEAR1 SET AMT1=ActualVal.SaleTAX,[Tail_Number]=TailNum  FROM
(
SELECT SUM(Actual.SateTAX)SaleTAX,Actual.SateTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3) AC ON AC.AccountNum=C.StateTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-12,@FromDate) AND  DATEADD(MONTH,-12,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SateTaxAc,TailNum,FleetID   
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


----------------------------------------------------------------------------------------------------------------------------------
UPDATE #YEAR1 SET AMT2=ActualVal.FederalTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.FederalTaxAc,TailNum,FleetID   
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


UPDATE #YEAR1 SET AMT2=ActualVal.SaleTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2) AC ON AC.AccountNum=C.SaleTax
WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SalesTaxAc,TailNum,FleetID    
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID



UPDATE #YEAR1 SET AMT2=ActualVal.SaleTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.SateTAX)SaleTAX,Actual.SateTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3) AC ON AC.AccountNum=C.StateTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-24,@FromDate) AND  DATEADD(MONTH,-24,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SateTaxAc,TailNum,FleetID    
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID


----------------------------------------------------------------------------------------------------------------------
UPDATE #YEAR1 SET AMT3=ActualVal.FederalTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
                                --LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.FederalTaxAc,TailNum ,FleetID  
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID

UPDATE #YEAR1 SET AMT3=ActualVal.SaleTAX,[Tail_Number]=TailNum FROM
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2) AC ON AC.AccountNum=C.SaleTax
WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SalesTaxAc,TailNum,FleetID    
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID

UPDATE #YEAR1 SET AMT3=ActualVal.SaleTAX,[Tail_Number]=TailNum   FROM
(
SELECT SUM(Actual.SateTAX)SaleTAX,Actual.SateTaxAc,TailNum,FleetID Fleet FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum,F.TailNum,F.FleetID FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3) AC ON AC.AccountNum=C.StateTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN DATEADD(MONTH,-36,@FromDate) AND  DATEADD(MONTH,-36,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SateTaxAc,TailNum,FleetID   
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNum AND ActualVal.Fleet=#YEAR1.FleetID

--------------------------------------------------------------------------------------------------------------------------------

SELECT DISTINCT
       Y1.Tail_Number,
       AircraftCD,
       MONTHSTART1,
       MONTHEND1,
       MONTHSTART2,
       MONTHEND2,
       MONTHSTART3,
       MONTHEND3,
       MONTHSTART4,
       MONTHEND4,
       AMT0=SUM(AMT0) ,
       AMT1=SUM(AMT1),
       AMT2=SUM(AMT2),
       AMT3=SUM(AMT3),
       AccountNum,
       AccountDescription,
       FlightBlockHoursLabel,
       FlightBlockHours0=Y.FlightBlockHours0,
       FlightBlockHours1=Y.FlightBlockHours1,
       FlightBlockHours2=Y.FlightBlockHours2,
       FlightBlockHours3=Y.FlightBlockHours3,
       year0fed=SUM(year0fed),
       year1fed=SUM(year1fed),
       year2fed=SUM(year2fed),
       year3fed=SUM(year3fed),
       year0state=SUM(year0state),
       year1state=SUM(year1state),
       year2state=SUM(year2state),
       year3state=SUM(year3state),
       year0sales=SUM(year0sales),
       year1sales=SUM(year1sales),
       year2sales=SUM(year2sales),
       year3sales=SUM(year3sales),   
       TenToMin,
       Rnk,
       0 AS year0hours,
       0 AS years1hours,
       0 AS years2hours,
       0 AS years3hours
FROM #YEAR1 Y1
     INNER JOIN (SELECT DISTINCT  Tail_Number,FlightBlockHours0=SUM(FlightBlockHours0),FlightBlockHours1=SUM(FlightBlockHours1),FlightBlockHours2=SUM(FlightBlockHours2),FlightBlockHours3=SUM(FlightBlockHours3)  FROM (
						SELECT DISTINCT  Tail_Number,FlightBlockHours0,FlightBlockHours1,FlightBlockHours2,FlightBlockHours3 FROM #YEAR1
                 )TT
                 GROUP BY Tail_Number)Y ON Y.Tail_Number=Y1.Tail_Number
WHERE RNK=1			
GROUP BY   AccountNum,	
		   AccountDescription,
		   Y1.Tail_Number,
		   AircraftCD,
		   MONTHSTART1,
		   MONTHEND1,
		   MONTHSTART2,
		   MONTHEND2,
		   MONTHSTART3,
		   MONTHEND3,
		   MONTHSTART4,
		   MONTHEND4,
		   FlightBlockHoursLabel,   
		   TenToMin,
		   Rnk,
		   Y.FlightBlockHours0,
		   Y.FlightBlockHours1,
		   Y.FlightBlockHours2,
		   Y.FlightBlockHours3
		   
ORDER BY Tail_Number,AccountNum,Rnk


IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL
DROP TABLE #TEMP1    

IF OBJECT_ID('tempdb..#YEAR1') IS NOT NULL
DROP TABLE #YEAR1
END




GO


