IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRELocatorAirportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRELocatorAirportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRELocatorAirportInformation]  
	    @UserCustomerID BIGINT
       ,@ICAOD VARCHAR(4)
       ,@CountryCD VARCHAR(2)
       ,@Runway NUMERIC(6,0)=0
       ,@CityName VARCHAR(25)
       ,@MetroCD VARCHAR(3)
       ,@IATA VARCHAR(3)
       ,@MilesFrom INT 
       ,@StateName VARCHAR(25)
       ,@AirportName VARCHAR(25) 
       ,@IsHeliport BIT    
	   ,@IsEntryPort BIT    
       ,@IsInActive BIT=0    
AS

-- ================================================================================  
-- SPC Name: spGetReportPRETSPAXNoFlyInformation1  
-- Author: Askar  
-- Create date: 17 Sep 2013  
-- Description:  Get the Locator information of Airport  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- =================================================================================   

BEGIN       
    --1.city,country and state,2.icao 3.iata,city,country,state  
SELECT A.AirportName
      ,A.IcaoID
      ,City=A.CityName
      ,State=A.StateName
      ,Country=C.CountryName
      ,Miles=0 
      ,RunwayLength=A.LongestRunway 
      ,Helipt=(CASE WHEN A.IsHeliport=1  THEN 'Y' 
						 WHEN A.IsHeliport=0  THEN 'N'  ELSE NULL END)
      ,IsEntryPort=(CASE WHEN A.IsEntryPort=1  THEN 'Y' 
						 WHEN A.IsEntryPort=0  THEN 'N'  ELSE NULL END)
      FROM Airport A      
		   LEFT OUTER JOIN Metro M ON A.MetroID = M.MetroID        
		   LEFT OUTER JOIN Country C ON A.CountryID = C.CountryID 
      WHERE A.CustomerID=@UserCustomerID
        AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ICAOD, ',')) OR @ICAOD='')
        AND (C.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ',')) OR @CountryCD='')
        AND  A.LongestRunway>=@Runway
        AND (A.CityName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CityName, ',')) OR @CityName='')
        AND (M.MetroCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MetroCD, ',')) OR @MetroCD='')
        AND (A.StateName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ',')) OR @StateName='')
        AND (A.AirportName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportName, ',')) OR @AirportName='')
        AND (A.Iata IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IATA , ',')) OR @IATA='')
        AND A.IsEntryPort=@IsEntryPort
        AND A.IsHeliport=@IsHeliport
        AND ((A.IsInActive =@IsInActive) OR @IsInActive = 1) 
        
        
END

	   
GO




