IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogIIMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogIIMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







 CREATE PROCEDURE [dbo].[spGetReportPOSTPilotLogIIMainInformation]
(
     @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @CrewCD AS NVARCHAR(1000) = '',  
	  @CrewGroupCD AS CHAR(1000) = '',
	  @ChecklistCode AS NVARCHAR(1000) = '',
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @FlightCatagoryCD AS CHAR(1000) = '',
	  @HomeBaseCD AS CHAR(1000) = '', 
	  @IsHomebase BIT = 0,
	  @PrintOnTime BIT = 0,
	  @PrintOffTime BIT = 0,
	  @SortBy as varchar(1) 
		)
  AS


--- ===============================================================================
-- SPC Name: spGetReportPOSTPilotLogIIMainInformation
-- Author:  A.AKHILA
-- Create date: 
-- Description: Get Postflight SUB Pilot Log for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
DECLARE @From VARCHAR(10)=dbo.GetShortDateFormatByUserCD(@UserCD, @DATEFROM) 
DECLARE @To VARCHAR(10)=dbo.GetShortDateFormatByUserCD(@UserCD, @DATETO)
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		HomeBaseID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID,NULL 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,NULL ---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,NULL 
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
	  AND C.IsDeleted=0
	  AND C.IsStatus=1
END
-----------------------------Crew and Crew Group Filteration----------------------

-----------------------------------HomeBase and HomeBaseCD Filteration-----

IF @IsHomebase=0 AND @HomeBaseCD =''

BEGIN
UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID FROM Crew C 
						 LEFT OUTER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 LEFT OUTER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
                   WHERE C.CustomerID=@CUSTOMERID
)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

IF @IsHomebase=1

BEGIN
UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID  FROM Crew C 
						 INNER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 INNER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
				    WHERE C.CustomerID=@CUSTOMERID
				      
)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

IF @HomeBaseCD <> '' AND @IsHomebase=0
BEGIN

UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID  FROM Crew C 
						 LEFT OUTER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 LEFT OUTER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
				    WHERE C.CustomerID=@CUSTOMERID
				      AND A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))

)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

-----------------------------------HomeBase and HomeBaseCD Filteration-----
CREATE TABLE #DUTYTOTAL
(
	POLogID BIGINT
	,LOGNUM INT
	,LegID BIGINT
	,IsStatus nvarchar(1)
	,DutyBegin VARCHAR(20)
	,DutyEnd VARCHAR(20)
	,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
	DutyBegin VARCHAR(20)
	,DutyEnd VARCHAR(20)
	,POLogID BIGINT
	,LegID BIGINT
	,CrewID BIGINT
)
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM 
	INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
	INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND PM.IsDeleted = 0
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID 
FROM (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
	JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
	ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID
UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin 
FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID AND #DUTYTOTAL.DutyEnd IS NULL

DECLARE @speclong3 NVARCHAR(25);
DECLARE @speclong4 NVARCHAR(25);
DECLARE @specdec3 BIGINT;
DECLARE @specdec4 BIGINT;
SELECT 
@speclong3 = SpecificationLong3
,@speclong4 = SpecificationLong4
,@specdec3 = Specdec3
,@specdec4 = Specdec4
FROM Company 
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) 
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
CREATE TABLE #CREWINFO
(
	[DateRange] VARCHAR(100)
	,[DateFROM] DATE
	,[DATETO] DATE
	,[CREWID] BIGINT
	,[POLEGID] BIGINT
	,[POLOGID] BIGINT
	,[LOGNUM] BIGINT
	,[LEGNUM] BIGINT
	,[Date] DATE
	,[Tail Number] VARCHAR(9)
	,[Type] VARCHAR(10)
	,[FltTyp] CHAR(3)
	,[Flt Cat] CHAR(4)
	,[DepICAO] CHAR(4)
	,[DEPARTDATE] DATETIME
	,[ArrICAO] CHAR(4)
	,[ARRIVEDATE] DATETIME
	,[BlkOut] VARCHAR(5)
	,[BlkIn] VARCHAR(5)
	,[Client] VARCHAR(5)
	,[Crew Duty Type] CHAR(2)
	,[DutyHoursPS] NUMERIC(7,3)
	,[DutyTYPE] VARCHAR(20)
	,[DUTYTOTAL] NUMERIC(7,3)
	,[Block Time] NUMERIC(7,3)
	,[Flight Time] NUMERIC(7,3)
	,[Nite Time] NUMERIC(7,3)
	,[Inst Time] NUMERIC(7,3)
	,[T/OFF Dt] NUMERIC(2,0)
	,[T/OFF Nt] NUMERIC(2,0)
	,[Appr Pr] NUMERIC(2,0)
	,[Appr Npr] NUMERIC(2,0)
	,[Lnding Dt] NUMERIC(2,0)
	,[Lnding Nt] NUMERIC(2,0)
	,[SpecificationLong3] VARCHAR(25)
	,[SpecificationLong4] VARCHAR(25)
	,[Specdec3] BIGINT
	,[Specdec4] BIGINT
	,[CUSTOM3DESCRIPTION] NUMERIC(7,5)
	,[CUSTOM4DESCRIPTION] NUMERIC(7,5)
	,[RONs] NUMERIC(3,0)
	,[CREWCD] VARCHAR(5)
	,[Associated Crew] VARCHAR(100)
	,[PILOT] VARCHAR(50)
	,[PILOTLast] VARCHAR(30)
	,[PILOTFirst] VARCHAR(20)
	,[PILOTMiddle] VARCHAR(20)
	,[ON/OFF] VARCHAR(200)
	,[ON] VARCHAR(200)
	,[OFF] VARCHAR(200)
	,[BLK] VARCHAR(200)
	,[BLKON] VARCHAR(200)
	,[BLKOFF] VARCHAR(200)
	,[BLK2] VARCHAR(200)
	,[TenToMin] NUMERIC(1,0)
	,[ISDUTYEND] BIT
	,NextLocal DATETIME
   ,[CHECKLISTCODE] CHAR(3)
   ,[ChecklistItem] VARCHAR(25)
   ,[ChecklistLast] DATETIME
   ,[ChecklistDue] DATETIME
   ,[ChecklistAlert] DATETIME
   ,[ChecklistGrace] DATETIME
   ,[case1] VARCHAR(15)
   ,ChkVal CHAR(1)
   ,StartDate DATE
   ,EndDate DATE
   ,SessionDate DATE
)
INSERT INTO #CREWINFO
SELECT DISTINCT
	[DateRange] = @From+' - '+ @To 
	,[DateFROM]=CONVERT(DATE,@DATEFROM)
	,[DATETO]= CONVERT(DATE, @DATETO) 
	,[CREWID]= CW.CrewID
	,[POLEGID]=POL.POLegID
	,[POLOGID]=POM.POLogID
	,[LOGNUM]=POM.LogNum
	,[LEGNUM]=POL.LegNUM
	,[Date] = CONVERT(DATE,POL.OutboundDTTM)
	,[Tail Number] = F.TailNum
	,[Type] = AC.AircraftCD
	,[FltTyp] = POL.FedAviationRegNum
	,[Flt Cat] = ISNULL(FC.FlightCatagoryCD,' ')
	,[DepICAO] = A.IcaoID
	,[DEPARTDATE] = POL.OutboundDTTM
	,[ArrICAO] = AA.IcaoID
	,[ARRIVEDATE] = POL.InboundDTTM
	,[BlkOut] = ISNULL(REPLACE(POL.BlockOut,':',''),0000)
	,[BlkIn] = ISNULL(REPLACE(POL.BlockIN,':',''),0000) 
	,[Client] = ''
	,[Crew Duty Type] = ''
	,[DutyHoursPS]= ROUND(POC.DutyHours,1)
	,[DutyTYPE] = POC.DutyTYPE
	,[DUTYTOTAL] = (CASE WHEN (POL.IsDutyEnd=1) then ROUND(POC.DutyHours,1) END)
	,[Block Time] = ISNULL(ROUND(POC.BlockHours,1),0)
	,[Flight Time] = ISNULL(ROUND(POC.FlightHours,1),0)
	,[Nite Time] = ISNULL(POC.Night,0)
	,[Inst Time] = ISNULL(POC.Instrument,0)
	,[T/OFF Dt] = ISNULL(POC.TakeOffDay,0)
	,[T/OFF Nt] = ISNULL(POC.TakeOffNight,0)
	,[Appr Pr] = ISNULL(POC.ApproachPrecision,0)
	,[Appr Npr] = ISNULL(POC.ApproachNonPrecision,0)
	,[Lnding Dt] = ISNULL(POC.LandingDay,0)
	,[Lnding Nt] = ISNULL(POC.LandingNight,0)
	,[SpecificationLong3] = isnull(c.SpecificationLong3,'')
	,[SpecificationLong4] = isnull(c.SpecificationLong4,'')
	,[Specdec3] = C.Specdec3
	,[Specdec4] = C.Specdec4
	,[CUSTOM3DESCRIPTION]= (case when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,0)))+'.'
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,1)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,2)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,3)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,4)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,5)))
							end ) 
	,[CUSTOM4DESCRIPTION]= (case when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,0)))+'.'
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,1)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,2)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,3)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,4)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,5)))
							end) 
	,[RONs] = NULL
	,[CREWCD]= CW.CrewCD
	,[Associated Crew] = ACrew.AssociatedCrew
	,[PILOT] = ISNULL(CW.CrewCD +' '+ CW.LastName +','+ CW.FirstName +''+ CW.MiddleInitial,' ')
	,[PILOTLast] = ISNULL(CW.LastName,'')
	,[PILOTFirst] = ISNULL(CW.FirstName,'')
	,[PILOTMiddle] = ISNULL(CW.MiddleInitial,'')
	,[ON/OFF]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off' 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On' 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END)
	,[ON] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN 'On' ELSE '' END
	,[OFF] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN 'Off'  ELSE '' END 
	,[BLK]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') 
				WHEN (POL.IsDutyEnd=1 and(@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','')+'/'+REPLACE(ISNULL(DT.DutyEnd,''),':','')
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
	,[BLKON] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE '' END
	,[BLKOFF] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') ELSE '' END
	,[BLK2]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(POC.BeginningDuty,':','') 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(POC.DutyEnd,':','') 
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(POC.BeginningDuty,''),':','')+'/'+REPLACE(ISNULL(POC.DutyEnd,''),':','')
				WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
	,[TenToMin] = @TenToMin
	,[ISDUTYEND] = POL.IsDutyEnd
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,'H'
    ,NULL
	,NULL
	,NULL
FROM PostflightMain POM 
	LEFT OUTER JOIN (SELECT POLOGID,max(legnum) Ron1 FROM PostflightLeg WHERE IsDeleted = 0 GROUP BY POLogID)POL1 ON POM.POLogID=POL1.POLOGID
	INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID 
	INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 --AND CW.IsStatus = 1
	INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted=0
	LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
	INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
	INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
	INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
	INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	INNER JOIN Company c ON c.CustomerID = POM.CustomerID
	INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID AND UM.UserName=LTRIM(RTRIM(@UserCD))
	LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=POM.POLogID AND DT.LegID=POL.POLegID AND DT.CrewID=CW.CrewID
	LEFT OUTER JOIN (
		SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
		SELECT RTRIM(C.CrewCD) + ','
		FROM PostflightCrew PC 
		INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.POLegID = PC1.POLegID
		order by C.CrewCD
		FOR XML PATH('')
		)
		FROM PostflightCrew PC1
					)ACrew ON POL.POLegID = ACrew.POLegID
	INNER JOIN #TempCrewID TC ON TC.CrewID=CW.CrewID AND TC.HomeBaseID=CW.HomebaseID
WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	--AND (POC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')
	AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
	AND CONVERT(DATE,POL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND POM.IsDeleted = 0

INSERT INTO #CREWINFO
SELECT DISTINCT
	[DateRange] = @From+' - '+ @To 
	,[DateFROM]=CONVERT(DATE,@DATEFROM)
	,[DATETO]= CONVERT(DATE, @DATETO) 
	,[CREWID]= CW.CrewID
	,[POLEGID]=POL.POLegID
	,[POLOGID]=POM.POLogID
	,[LOGNUM]=POM.LogNum
	,[LEGNUM]=POL.LegNUM
	,[Date] = CASE WHEN CONVERT(DATE,POL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN POL.OutboundDTTM ELSE @DATEFROM END
	,[Tail Number] = F.TailNum
	,[Type] = AC.AircraftCD
	,[FltTyp] = POL.FedAviationRegNum
	,[Flt Cat] = ISNULL(FC.FlightCatagoryCD,' ')
	,[DepICAO] = A.IcaoID
	,[DEPARTDATE] = POL.OutboundDTTM
	,[ArrICAO] = AA.IcaoID
	,[ARRIVEDATE] =  POL.InboundDTTM 
	,[BlkOut] = NULL--ISNULL(REPLACE(POL.BlockOut,':',''),0000)
	,[BlkIn] = NULL--ISNULL(REPLACE(POL.BlockIN,':',''),0000) 
	,[Client] = 'R'--ISNULL(CT.ClientCD,' ')
	,[Crew Duty Type] = NULL--ISNULL(CDT.DutyTypeCD,' ') 
	,[DutyHoursPS]= '0'--POC.DutyHours
	,[DutyTYPE] = POC.DutyTYPE
	,[DUTYTOTAL] = NULL--(CASE WHEN (POL.IsDutyEnd=1) then POC.DutyHours END)
	,[Block Time] = '0'--ISNULL(POC.BlockHours,0)
	,[Flight Time] = '0'--ISNULL(POC.FlightHours,0)
	,[Nite Time] = '0'--ISNULL(POC.Night,0)
	,[Inst Time] = '0'--ISNULL(POC.Instrument,0)
	,[T/OFF Dt] = '0'--ISNULL(POC.TakeOffDay,0)
	,[T/OFF Nt] = '0'--ISNULL(POC.TakeOffNight,0)
	,[Appr Pr] = '0'--ISNULL(POC.ApproachPrecision,0)
	,[Appr Npr] = '0'--ISNULL(POC.ApproachNonPrecision,0)
	,[Lnding Dt] = '0'--ISNULL(POC.LandingDay,0)
	,[Lnding Nt] = '0'--ISNULL(POC.LandingNight,0)
	,[SpecificationLong3] = isnull(c.SpecificationLong3,'')
	,[SpecificationLong4] = isnull(c.SpecificationLong4,'')
	,[Specdec3] = C.Specdec3
	,[Specdec4] = C.Specdec4
	,[CUSTOM3DESCRIPTION]= (case when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,0)))+'.'
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,1)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,2)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,3)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,4)))
							when c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,5)))
							end ) 
	,[CUSTOM4DESCRIPTION]= (case when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,0)))+'.'
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,1)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,2)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,3)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,4)))
							when C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,5)))
							end) 
	,[RONs] = NULL
	,[CREWCD]= CW.CrewCD
	,[Associated Crew] = ACrew.AssociatedCrew
	,[PILOT] = ISNULL(CW.CrewCD +' '+ CW.LastName +','+ CW.FirstName +''+ CW.MiddleInitial,' ')
	,[PILOTLast] = ISNULL(CW.LastName,'')
	,[PILOTFirst] = ISNULL(CW.FirstName,'')
	,[PILOTMiddle] = ISNULL(CW.MiddleInitial,'')
	,[ON/OFF]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off' 
			WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' 
			WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On' 
			WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END)
	,[ON] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN 'On' ELSE '' END
	,[OFF] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN 'Off'  ELSE '' END 
	,[BLK]= NULL--(CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') 
			--WHEN (POL.IsDutyEnd=1 and(@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') 
			--WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','')+'/'+REPLACE(ISNULL(DT.DutyEnd,''),':','')
			--WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
	,[BLKON] = NULL--CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE '' END
	,[BLKOFF] = NULL--CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') ELSE '' END
	,[BLK2]= NULL--(CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(POC.BeginningDuty,':','') 
			--WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(POC.DutyEnd,':','') 
			--WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(POC.BeginningDuty,''),':','')+'/'+REPLACE(ISNULL(POC.DutyEnd,''),':','')
			--WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
	,[TenToMin] = @TenToMin
	,[ISDUTYEND] = POL.IsDutyEnd
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,'H'
    ,NULL
	,NULL
	,NULL
FROM PostflightMain POM 
	LEFT OUTER JOIN (SELECT POLOGID,max(legnum) Ron1 FROM PostflightLeg WHERE IsDeleted = 0 GROUP BY POLogID)POL1 ON POM.POLogID=POL1.POLOGID
	INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID 
	INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 --AND CW.IsStatus = 1
	INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted = 0
	LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
	INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
	INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
	INNER JOIN #TempCrewID TC ON TC.CrewID=CW.CrewID AND TC.HomeBaseID=CW.HomebaseID
	INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
	INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	INNER JOIN Company c ON c.CustomerID = POM.CustomerID
	INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID AND UM.UserName=LTRIM(RTRIM(@UserCD))
	LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=POM.POLogID AND DT.LegID=POL.POLegID AND DT.CrewID=CW.CrewID
	LEFT OUTER JOIN (
		SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
		SELECT RTRIM(C.CrewCD) + ','
		FROM PostflightCrew PC 
		INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.POLegID = PC1.POLegID
		order by C.CrewCD
		FOR XML PATH('')
		)
		FROM PostflightCrew PC1
					) ACrew ON POL.POLegID = ACrew.POLegID
	INNER JOIN (SELECT PM.LogNum,PL.LegNUM,PL.OutboundDTTM,PL.InboundDTTM,PL.POLegID,PL.POLogID
		 FROM PostflightMain PM 
			 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			 INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM AND PL1.IsDeleted = 0
		 WHERE 
		 (
		 (CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
		 OR
		 (CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
		 OR
		 (CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
		 ) AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted=0 )TempLog
		 ON TempLog.POLegID=POL.POLegID AND TempLog.POLogID=POL.POLogID
WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	--AND (poc.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')
	AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND POL.POLegID NOT IN(SELECT POLegID FROM #CREWINFO)
	AND POM.IsDeleted = 0
UPDATE #CREWINFO SET NextLocal=TEMP.OutboundDTTM FROM
	(SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.OutboundDTTM,PL.LegNUM FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                                                                        WHERE PM.CustomerID= dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted=0)TEMP
                 WHERE #CREWINFO.LEGNUM+1=TEMP.LegNUM
                   AND #CREWINFO.POLOGID=TEMP.POLogID 
                   AND #CREWINFO.LOGNUM=TEMP.LogNum





INSERT INTO #CREWINFO(DateRange,DateFROM,DATETO,CREWID,CREWCD,TenToMin,ChecklistItem,[ChecklistLast],[ChecklistDue],
                      [ChecklistAlert],[ChecklistGrace],[case1],CHECKLISTCODE,PILOTLast,PILOTFirst,PILOTMiddle,ChkVal)
SELECT DISTINCT    [DateRange] = @From + ' - ' + @To
	,[DateFROM] = CONVERT(DATE, @DATEFROM)
	,[DATETO] = CONVERT(DATE, @DATETO)
	,C.[CrewID]
	,C.[CREWCD]
	,[TenToMin] = @TenToMin
	,[Checklist/Checklist Item]=ccl.CrewChecklistDescription
	,[Checklist/Last]=CCD.PreviousCheckDT
	,[Checklist/Due] =CCD.DueDT 
	,[Checklist/Alert]=CCD.AlertDT
	,[Checklist/Grace]=CCD.GraceDT
	--,[Checklist/Date]=(CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
	--						   WHEN (@DATETO > CCD.DUEDT) THEN 'PAST DUE' 
	--						   WHEN (@DATETO > CCD.DueDT AND @DATETO <= CCD.AlertDT) THEN 'GRACE' 
	--						   WHEN CCD.IsCompleted = 1 AND CCD.IsOneTimeEvent = 1 THEN 'COMPLETED'
	--						   ELSE '' END)
	,[Checkllist/Date] = CASE WHEN (@DATETO > CCD.GraceDT) THEN 'PAST DUE'
							WHEN (@DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT) THEN 'GRACE'
							WHEN (@DATETO >= CCD.AlertDT AND @DATETO <= CCD.DueDT) THEN 'ALERT'
							WHEN (@DATETO < CCD.AlertDT) THEN ''
							ELSE '' END 
	,CHECKLISTCODE=CCL.CrewCheckCD
	,C.LastName
    ,C.FirstName
    ,C.MiddleInitial
    ,'Z'
				FROM Crew C 
					INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=C.CrewID and CCD.CustomerID= C.CustomerID
					LEFT OUTER JOIN #CREWINFO T ON T.CREWID=C.CrewID
					INNER JOIN CrewCheckList CCL on ccd.CheckListCD = ccl.CrewCheckCD and  ccl.IsDeleted =0 and CCD.IsDeleted=0 and CCD.CustomerID= CCL.CustomerID
					INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID AND TC.HomeBaseID=C.HomebaseID
				WHERE C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
					--AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
					--AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
					AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
					AND c.IsDeleted = 0


DECLARE @CurDate TABLE(DateWeek DATE,CrewCD VARCHAR(5),CrewMember VARCHAR(60),CrewID BIGINT)


INSERT INTO #CREWINFO(DateRange,DateFROM,DATETO,CREWID,CREWCD,DATE,Client,[Crew Duty Type],DutyHoursPS,DutyTYPE,[Flight Time],
                      [Block Time],[Nite Time],[Inst Time],[T/OFF Dt],[T/OFF Nt],[Lnding Dt],[Lnding Nt],[Appr Npr],[Appr Pr],[ON/OFF],BLK,StartDate,EndDate,ChkVal,SessionDate,TenToMin,PILOTFirst,PILOTLast,PILOTMiddle)
SELECT [DateRange] = @From + ' - ' + @To,[DateFROM] = CONVERT(DATE, @DATEFROM),[DATETO] = CONVERT(DATE, @DATETO)
    ,PSL.CrewID,C.CrewCD,CASE WHEN  PSL.SessionDT <= @DATEFROM THEN @DATEFROM
                              WHEN  PSL.SessionDT >= @DATETO THEN @DATETO ELSE PSL.SessionDT END
    ,CL.ClientCD,CD.DutyTypeCD,ROUND(PSL.DutyHours,1)DutyHours,PSL.DutyTYPE
    ,ROUND(PSL.FlightHours,1) FlightHours,ROUND(PSL.FlightHours,1) BlockHours,PSL.Night,PSL.Instrument,PSL.TakeOffDay,PSL.TakeOffNight
      ,PSL.LandingDay,PSL.LandingNight,PSL.ApproachNonPrecision,PSL.ApproachPrecision 
      ,[ON/OFF] = (CASE WHEN (@PrintOnTime = 1AND @PrintOffTime = 1) THEN 'On/Off'
			            WHEN ( @PrintOnTime = 0 AND @PrintOffTime = 0)THEN ''
			            WHEN (@PrintOnTime = 1 AND @PrintOffTime = 0)THEN 'On'
			            WHEN (@PrintOnTime = 0 AND @PrintOffTime = 1) THEN 'Off' END )
         ,[BLK] = (CASE WHEN (@PrintOnTime = 1 AND @PrintOffTime = 0) THEN REPLACE(ISNULL(SUBSTRING (CONVERT(VARCHAR(30),PSL.DepartureDTTMLocal,113),13,5) , ''), ':', '')
			            WHEN (@PrintOnTime = 0 AND @PrintOffTime = 1)THEN REPLACE(ISNULL(SUBSTRING (CONVERT(VARCHAR(30),PSL.ArrivalDTTMLocal,113),13,5) , ''), ':', '')
			            WHEN (@PrintOnTime = 1 AND @PrintOffTime = 1) THEN REPLACE(ISNULL(SUBSTRING (CONVERT(VARCHAR(30),PSL.DepartureDTTMLocal,113),13,5) , ''), ':', '') + '/' + REPLACE(ISNULL(SUBSTRING (CONVERT(VARCHAR(30),PSL.ArrivalDTTMLocal,113),13,5) , ''), ':', '')
			            WHEN (@PrintOnTime = 0 AND @PrintOffTime = 0) THEN ''END
		          )
		  ,CASE WHEN  PSL.SessionDT <= @DATEFROM THEN @DATEFROM
                              WHEN  PSL.SessionDT >= @DATETO THEN @DATETO ELSE PSL.SessionDT END
		  ,DATEADD(DAY,(DATEDIFF(DAY,PSL.DepartureDTTMLocal,PSL.ArrivalDTTMLocal)+1),PSL.SessionDT) 
          ,'Y'
          ,PSL.SessionDT,@TenToMin,C.FirstName,C.LastName,C.MiddleInitial
      FROM PostflightSimulatorLog  PSL
           INNER JOIN Crew C ON PSL.CrewID=C.CrewID
           INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID 
           LEFT OUTER JOIN CrewDutyType CD ON PSL.DutyTypeID=CD.DutyTypeID 
           LEFT OUTER  JOIN Client CL ON CL.ClientID=PSL.ClientID
    WHERE PSL.CustomerID=@CUSTOMERID
    AND (CONVERT(DATE,PSL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR
           CONVERT(DATE,PSL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO))
INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID FROM Crew C  
                INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@DateFrom,@DateTo) ) T
                WHERE C.CustomerID=@CUSTOMERID 
                  AND C.IsDeleted=0
                  AND C.IsStatus=1
                  

INSERT INTO #CREWINFO(DateRange,DateFROM,DATETO,CREWID,CREWCD,DATE,Client,[Crew Duty Type],DutyHoursPS,DutyTYPE,[Flight Time],
                      [Block Time],[Nite Time],[Inst Time],[T/OFF Dt],[T/OFF Nt],[Lnding Dt],[Lnding Nt],[Appr Npr],[Appr Pr],[ON/OFF],BLK,ChkVal,SessionDate,TenToMin,PILOTFirst,PILOTLast,PILOTMiddle)
SELECT DISTINCT CI.DateRange,CI.DateFROM,CI.DATETO,CI.CREWID,CI.CREWCD,CD.DateWeek,CI.Client,CI.[Crew Duty Type],CI.DutyHoursPS,
                CI.DutyTYPE,CI.[Flight Time],CI.[Block Time],CI.[Nite Time],CI.[Inst Time],CI.[T/OFF Dt],CI.[T/OFF Nt],CI.[Lnding Dt],CI.[Lnding Nt],CI.[Appr Npr]
                ,CI.[Appr Pr],CI.[ON/OFF],CI.BLK,'Y',CI.SessionDate,@TenToMin,C.FirstName,C.LastName,C.MiddleInitial
				FROM Crew C INNER JOIN  @CurDate CD ON CD.CrewID=C.CrewID
				            INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID 
				            LEFT JOIN #CrewInfo CI ON CD.CrewID=CI.CrewID
			    WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,CI.StartDate)
				 AND CONVERT(DATE,CD.DateWeek) < CONVERT(DATE, CI.EndDate )

UPDATE #CREWINFO SET DUTYTOTAL=CDT.DutyHours FROM 
(SELECT SUM(DutyHoursPS) DutyHours,[Crew Duty Type] CrewDuty,SessionDate FROM #CREWINFO WHERE ChkVal='Y' GROUP BY SessionDate,[Crew Duty Type])CDT
WHERE CDT.SessionDate=#CREWINFO.SessionDate
  AND CDT.CrewDuty=#CREWINFO.[Crew Duty Type]
IF @IsZeroSuppressActivityCrewRpt=0
BEGIN

INSERT INTO #CREWINFO
 SELECT DISTINCT --TOP 10
		[DateRange] = @From+' - '+ @To 
		,[DateFROM]=CONVERT(DATE,@DATEFROM)
		,[DATETO]= CONVERT(DATE, @DATETO) 
		,[CREWID]= CW.CrewID
		,[POLEGID]= NULL
		,[POLOGID]=NULL
		,[LOGNUM]=NULL
		,[LEGNUM]=NULL
		,[Date] = NULL
		,[Tail Number] = NULL
		,[Type] = 'NO'
		,[FltTyp] = NULL
		,[Flt Cat] = NULL
		,[DepICAO] =NULL
		,[DEPARTDATE] = NULL
		,[ArrICAO] = NULL
		,[ARRIVEDATE] = NULL
		,[BlkOut] = NULL
		,[BlkIn] = NULL
		,[Client] =NULL
		,[Crew Duty Type] = NULL
		,[DutyHoursPS]= NULL
		,[DutyTYPE] = NULL
		,[DUTYTOTAL] = NULL
		,[Block Time] = NULL
		,[Flight Time] = NULL
		,[Nite Time] = NULL
		,[Inst Time] = NULL
		,[T/OFF Dt] = NULL
		,[T/OFF Nt] = NULL
		,[Appr Pr] = NULL
		,[Appr Npr] = NULL
		,[Lnding Dt] = NULL
		,[Lnding Nt] = NULL
		,[SpecificationLong3] = @speclong3
		,[SpecificationLong4] = @speclong4
		,[Specdec3] = @specdec3
		,[Specdec4] = @specdec4
		,[CUSTOM3DESCRIPTION]= (case when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,0)))
									+'.'
									when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,1)))
									when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,2)))
									when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,3)))
									when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,4)))
									when @speclong3 is not null and @speclong3<>'' then convert(varchar(20),cast(0 as numeric(7,5)))
									end ) 
		,[CUSTOM4DESCRIPTION]= (case when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,0)))+'.'
									when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,1)))
									when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,2)))
									when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,3)))
									when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,4)))
									when @speclong4 is not null and @speclong4<>'' then convert(varchar(20),cast('0' as numeric(7,5)))
									end) 
		,[RONs] = NULL
		,[CREWCD]= CW.CrewCD
		,[Associated Crew] = NULL
		,[PILOT] = ISNULL(CW.CrewCD +' '+ CW.LastName +','+ CW.FirstName +''+ CW.MiddleInitial,' ')
		,[PILOTLast] = ISNULL(CW.LastName,'')
		,[PILOTFirst] = ISNULL(CW.FirstName,'')
		,[PILOTMiddle] = ISNULL(CW.MiddleInitial,'')
		,[ON/OFF]= ''
		,[ON] = NULL
		,[OFF] = NULL
		,[BLKON] = NULL
		,[BLKOFF] = NULL
		,[BLK]=NULL
		,[BLK2]=NULL
		,[TenToMin] = @TenToMin
		,[ISDUTYEND] = NULL
		,NextLocal = NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,'H'
		,NULL
		,NULL
		,NULL
FROM Crew CW INNER JOIN #TempCrewID TC ON TC.CrewID=CW.CrewID AND TC.HomeBaseID=CW.HomebaseID
WHERE CW.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
 AND CW.CREWID NOT IN (SELECT DISTINCT ISNULL(T.CREWID,0) FROM #CREWINFO T WHERE ChkVal IN('H','Y'))
END

SELECT PILOT.[DateRange]
	,[DateFROM]
	,[DATETO]
	,[CREWID]
	,[POLEGID]
	,[POLOGID]
	,[LOGNUM]
	,[LEGNUM]
	,[Date]
	,[Tail Number]
	,[Type]
	,[FltTyp]
	,[Flt Cat]
	,[DepICAO]
	,[DEPARTDATE]
	,[ArrICAO]
	,[ARRIVEDATE]
	,[BlkOut]
	,[BlkIn]
	,[Client]
	,[Crew Duty Type]
	,ISNULL([DutyHoursPS],0) [DutyHoursPS]
	,[DutyTYPE]
	,CASE WHEN Rnk=1 THEN  [DUTYTOTAL]  ELSE NULL END [DUTYTOTAL]
	,ISNULL([Block Time],0) [Block Time]
	,ISNULL([Flight Time],0) [Flight Time]
	,ISNULL([Nite Time],0) [Nite Time]
	,ISNULL([Inst Time],0) [Inst Time]
	,[T/OFF Dt]
	,[T/OFF Nt]
	,[Appr Pr]
	,[Appr Npr]
	,[Lnding Dt]
	,[Lnding Nt]
	,[SpecificationLong3]
	,[SpecificationLong4]
	,[Specdec3]
	,[Specdec4]
	,ISNULL([CUSTOM3DESCRIPTION],0) [CUSTOM3DESCRIPTION]
	,ISNULL([CUSTOM4DESCRIPTION],0) [CUSTOM4DESCRIPTION]
	,[RONs]
	,[CREWCD]
	,[Associated Crew]
	,[PILOT]
	,[PILOTLast]
	,[PILOTFirst]
	,[PILOTMiddle]
	,CASE WHEN Rnk=1 THEN  [ON/OFF] ELSE NULL END  [ON/OFF] 
	,[ON]
	,[OFF]
	,CASE WHEN Rnk=1 THEN [BLK] ELSE NULL END [BLK]
	,[BLKON]
	,[BLKOFF]
	,[BLK2]
	,[TenToMin]
	,[ISDUTYEND]
	,NextLocal
   ,[CHECKLISTCODE]
   ,[ChecklistItem]
   ,[ChecklistLast]
   ,[ChecklistDue]
   ,[ChecklistAlert] 
   ,[ChecklistGrace]
   ,[case1] 
   ,ChkVal
   ,Rnk
	
	 FROM (
SELECT DISTINCT 
	[DateRange]
	,[DateFROM]
	,[DATETO]
	,[CREWID]
	,[POLEGID]
	,[POLOGID]
	,[LOGNUM]
	,[LEGNUM]
	,[Date]
	,[Tail Number]
	,[Type]
	,[FltTyp]
	,[Flt Cat]
	,[DepICAO]
	,[DEPARTDATE]
	,[ArrICAO]
	,[ARRIVEDATE]
	,[BlkOut]
	,[BlkIn]
	,[Client]
	,[Crew Duty Type]
	,[DutyHoursPS]
	,[DutyTYPE]
	,[DUTYTOTAL]
	,[Block Time]
	,[Flight Time]
	,[Nite Time]
	,[Inst Time]
	,[T/OFF Dt]
	,[T/OFF Nt]
	,[Appr Pr]
	,[Appr Npr]
	,[Lnding Dt]
	,[Lnding Nt]
	,[SpecificationLong3]
	,[SpecificationLong4]
	,[Specdec3]
	,[Specdec4]
	,CASE 
		WHEN Specdec3=0 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)-1)
		WHEN Specdec3=1 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+1)
		WHEN Specdec3=2 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+2)
		WHEN Specdec3=3 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+3)
		WHEN Specdec3=4 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+4)
		WHEN Specdec3=5 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+5)
	END 
	[CUSTOM3DESCRIPTION]
	,CASE 
		WHEN Specdec4=0 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)-1)
		WHEN Specdec4=1 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+1)
		WHEN Specdec4=2 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+2)
		WHEN Specdec4=3 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+3)
		WHEN Specdec4=4 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+4)
		WHEN Specdec4=5 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+5)
	END 
	[CUSTOM4DESCRIPTION]
	,CASE WHEN ISDUTYEND=1
		THEN
			CASE WHEN 
				(CASE WHEN CONVERT(DATE,NextLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND [Client]<>'R'
				 THEN DATEDIFF(DAY,ARRIVEDATE,NextLocal) 
				 ELSE 
					(CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]<>'R' 
					 THEN DATEDIFF(DAY,ARRIVEDATE,@DATETO)+1 
					 ELSE
						CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]='R' 
						THEN  DATEDIFF(DAY,@DATEFROM,
							(CASE WHEN CONVERT(DATE,@DATETO) =CONVERT(DATE,NextLocal) 
							 THEN @DATETO
							 WHEN CONVERT(DATE,@DATETO)<CONVERT(DATE,NextLocal) 
							 THEN @DATETO+1  
							 ELSE NextLocal 
							 END)
									   )
						ELSE NULL 
						END
					END) 
				END)=0 AND [Client]='R' 
			THEN 1 
			ELSE 
				(CASE WHEN CONVERT(DATE,NextLocal) BETWEEN CONVERT(DATE,@DATEFROM)AND CONVERT(DATE,@DATETO) AND [Client]<>'R'
				 THEN DATEDIFF(DAY,ARRIVEDATE,NextLocal) 
				 ELSE 
					(CASE WHEN CONVERT(DATE,NextLocal) IS NOT NULL AND [Client]<>'R' 
					 THEN DATEDIFF(DAY,ARRIVEDATE,@DATETO)+1 
					 ELSE
						CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]='R' 
						THEN DATEDIFF(DAY,@DATEFROM,
							(CASE WHEN CONVERT(DATE,@DATETO) = CONVERT(DATE,NextLocal) 
							 THEN @DATETO
							 WHEN CONVERT(DATE,@DATETO)<CONVERT(DATE,NextLocal) 
							 THEN @DATETO+1  
							 ELSE NextLocal 
							 END)
									  )
						ELSE NULL 
						END
					END) 
				END) 
			END
		ELSE
		'0'
		END  
	 [RONs]
	,[CREWCD]
	,[Associated Crew]
	,[PILOT]
	,[PILOTLast]
	,[PILOTFirst]
	,[PILOTMiddle]
	,[ON/OFF]
	,[ON]
	,[OFF]
	,[BLK]
	,[BLKON]
	,[BLKOFF]
	,[BLK2]
	,[TenToMin]
	,[ISDUTYEND]
	,NextLocal
   ,[CHECKLISTCODE]
   ,[ChecklistItem]
   ,[ChecklistLast]
   ,[ChecklistDue]
   ,[ChecklistAlert] 
   ,[ChecklistGrace]
   ,[case1] 
   ,ChkVal
   ,1 Rnk
FROM #CREWINFO WHERE ChkVal<>'Y'
UNION ALL

SELECT DISTINCT [DateRange]
	,[DateFROM]
	,[DATETO]
	,[CREWID]
	,[POLEGID]
	,[POLOGID]
	,[LOGNUM]
	,[LEGNUM]
	,[Date]
	,[Tail Number]
	,[Type]
	,[FltTyp]
	,[Flt Cat]
	,[DepICAO]
	,[DEPARTDATE]
	,[ArrICAO]
	,[ARRIVEDATE]
	,[BlkOut]
	,[BlkIn]
	,[Client]
	,[Crew Duty Type]
	,[DutyHoursPS]
	,[DutyTYPE]
	,[DUTYTOTAL]
	,[Block Time]
	,[Flight Time]
	,[Nite Time]
	,[Inst Time]
	,[T/OFF Dt]
	,[T/OFF Nt]
	,[Appr Pr]
	,[Appr Npr]
	,[Lnding Dt]
	,[Lnding Nt]
	,[SpecificationLong3]
	,[SpecificationLong4]
	,[Specdec3]
	,[Specdec4]
	,CASE 
		WHEN Specdec3=0 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)-1)
		WHEN Specdec3=1 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+1)
		WHEN Specdec3=2 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+2)
		WHEN Specdec3=3 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+3)
		WHEN Specdec3=4 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+4)
		WHEN Specdec3=5 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM3DESCRIPTION),1)+5)
	END 
	[CUSTOM3DESCRIPTION]
	,CASE 
		WHEN Specdec4=0 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)-1)
		WHEN Specdec4=1 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+1)
		WHEN Specdec4=2 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+2)
		WHEN Specdec4=3 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+3)
		WHEN Specdec4=4 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+4)
		WHEN Specdec4=5 THEN  SUBSTRING(CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1,CHARINDEX('.',CONVERT(VARCHAR(30),CUSTOM4DESCRIPTION),1)+5)
	END 
	[CUSTOM4DESCRIPTION]
	,CASE WHEN ISDUTYEND=1
		THEN
			CASE WHEN 
				(CASE WHEN CONVERT(DATE,NextLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND [Client]<>'R'
				 THEN DATEDIFF(DAY,ARRIVEDATE,NextLocal) 
				 ELSE 
					(CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]<>'R' 
					 THEN DATEDIFF(DAY,ARRIVEDATE,@DATETO)+1 
					 ELSE
						CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]='R' 
						THEN  DATEDIFF(DAY,@DATEFROM,
							(CASE WHEN CONVERT(DATE,@DATETO) =CONVERT(DATE,NextLocal) 
							 THEN @DATETO
							 WHEN CONVERT(DATE,@DATETO)<CONVERT(DATE,NextLocal) 
							 THEN @DATETO+1  
							 ELSE NextLocal 
							 END)
									   )
						ELSE NULL 
						END
					END) 
				END)=0 AND [Client]='R' 
			THEN 1 
			ELSE 
				(CASE WHEN CONVERT(DATE,NextLocal) BETWEEN CONVERT(DATE,@DATEFROM)AND CONVERT(DATE,@DATETO) AND [Client]<>'R'
				 THEN DATEDIFF(DAY,ARRIVEDATE,NextLocal) 
				 ELSE 
					(CASE WHEN CONVERT(DATE,NextLocal) IS NOT NULL AND [Client]<>'R' 
					 THEN DATEDIFF(DAY,ARRIVEDATE,@DATETO)+1 
					 ELSE
						CASE WHEN CONVERT(DATE,NextLocal) IS  NOT NULL AND [Client]='R' 
						THEN DATEDIFF(DAY,@DATEFROM,
							(CASE WHEN CONVERT(DATE,@DATETO) = CONVERT(DATE,NextLocal) 
							 THEN @DATETO
							 WHEN CONVERT(DATE,@DATETO)<CONVERT(DATE,NextLocal) 
							 THEN @DATETO+1  
							 ELSE NextLocal 
							 END)
									  )
						ELSE NULL 
						END
					END) 
				END) 
			END
		ELSE
		'0'
		END  
	 [RONs]
	,[CREWCD]
	,[Associated Crew]
	,[PILOT]
	,[PILOTLast]
	,[PILOTFirst]
	,[PILOTMiddle]
	,[ON/OFF]
	,[ON]
	,[OFF]
	,[BLK]
	,[BLKON]
	,[BLKOFF]
	,[BLK2]
	,[TenToMin]
	,[ISDUTYEND]
	,NextLocal
   ,[CHECKLISTCODE]
   ,[ChecklistItem]
   ,[ChecklistLast]
   ,[ChecklistDue]
   ,[ChecklistAlert] 
   ,[ChecklistGrace]
   ,[case1] 
   ,ChkVal,ROW_NUMBER()OVER(PARTITION BY CrewID ORDER BY Date DESC) FROM #CREWINFO WHERE ChkVal='Y'
)PILOT

ORDER BY [CREWCD] ,[Date]

IF OBJECT_ID('tempdb..#DUTYTOTAL') IS NOT NULL
DROP TABLE #DUTYTOTAL
IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO
IF OBJECT_ID('tempdb..#EMPTYCREW') IS NOT NULL
DROP TABLE #EMPTYCREW
IF OBJECT_ID('tempdb..#TempCrewID') IS NOT NULL
DROP TABLE #TempCrewID
--EXEC spGetReportPOSTPilotLogIIMainInformation 'supervisor_99','2009/1/1','2009/1/15','','','','','','',0,0,0,'b'



GO


