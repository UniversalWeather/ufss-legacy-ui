IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItineraryMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItineraryMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRECorporateItineraryMhtmlInformation]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		--@TripNUM AS BIGINT -- [Optional INTEGER], Comma delimited string with mutiple values
		@TripNUM AS NVARCHAR(500) = '' -- [Optional INTEGER], Comma delimited string with mutiple values

AS
-- =============================================
-- SPC Name: spGetReportPRECorporateItineraryExportInformation
-- Author: SINDHUJA.K
-- Create date: 20 July 2012
-- Description: Get Preflight CorporateItinerary Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
		SET @SQLSCRIPT = '
			 SELECT DISTINCT
			[orig_nmbr] = PM.TripNUM,
			[tail_nmbr] = F.TailNUM,
			[mindate] = PL.DepartureDTTMLocal,
			[maxdate] = PL.ArrivalDTTMLocal,
			[desc] = PM.TripDescription,
			[leg_num] = PL.LegNUM,
			[legid] = PL.LEGID1,
			[locdep] = PL.DepartureDTTMLocal, 
			[locarr] =PL.ArrivalDTTMLocal,
			[notes] = PL.Notes,
			[etd] = PL.DepartureDTTMLocal,
			[depicao_id] = A.ICAOID,
			[dep_airport_name] = A.AirportName,
			[dep_airport_city] = A.CityName,
			[eta] =PL.ArrivalDTTMLocal,
			[arricao_id] = B.ICAOID,
			[arr_airport_name] = B.AirportName,
			[arr_airport_city] = B.CityName,
			[elp_time] = PL.ElapseTM,
			[fbo_name] = PF.PreflightFBOName,
			[fbo_phone] = PF.PhoneNum1,
			[Title] = ''pending'',
			[Full_Name] = ''pending'',           
			[pax_hotel] = HP.Name,    
			[pax_hotel_phone] = HP.PhoneNum,      
			[crew_hotel] = HC.Name,  
			[crew_hotel_phone] = HC.PhoneNum ,     
			[paxname] = PPL.PassengerFirstName + '', '' + PPL.PassengerMiddleName + '' '' + PPL.PassengerLastName
		
		FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
	    INNER JOIN Airport A ON PL.DepartICAOID = A.AirportID 
	    INNER JOIN Airport B ON PL.ArriveICAOID = B.AirportID		 
	     LEFT JOIN PreflightPassengerList PPL ON PPL.LegID = PL.LegID
		 LEFT JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		 LEFT JOIN PreflightCrewList PCLL ON PCLL.LegID = PL.LegID
		 LEFT JOIN PreflightCrewHotelList PCH ON PCH.PreflightCrewListID = PCLL.PreflightCrewListID 
		 LEFT JOIN Hotel HP ON PPH.HotelID = HP.HotelID
		 LEFT JOIN Hotel HC ON PCH.HotelID = HC.HotelID
	     LEFT JOIN PreflightFBOList PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO = 1
	     LEFT JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
				LEFT OUTER JOIN FleetGroupOrder FGO
				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG 
				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID			
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))		
		---AND (CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)) 
		AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		AND PM.ISDELETED = 0'
	
	--Construct OPTIONAL clauses
				
	IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';						
	END

	IF @TripNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
	END
		
	--IF @TripNUM <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
	--END
	
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END			

	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS NVARCHAR(500)'
    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD 
    
    --EXEC spGetReportPRECorporateItineraryMhtmlInformation 'jwilliams_13','2012/09/01','2012/09/07', '', '', ''
    
--    select * from PreflightMain where TripNUM = 1883
--select notes from PreflightLeg where TripID = 100131783

GO


