IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteLogMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteLogMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQQuoteLogMainInformation] 
( 
      @UserCD VARCHAR(30)    
     ,@UserCustomerID VARCHAR(30)    
     ,@FileNumber VARCHAR(200)='',    
      @QuoteNUM VARCHAR(200)='',    
      @LegNUM VARCHAR(200)='',    
      @BeginDate DATETIME=NULL,    
      @EndDate DATETIME=NULL,    
      @TailNum VARCHAR(200)='',    
      @VendorCD VARCHAR(200)='',    
      @HomeBaseCD VARCHAR(200)='',    
      @AircraftCD VARCHAR(200)='',    
      @CQCustomerCD VARCHAR(30)='',    
      @SalesPersonCD VARCHAR(200)='' 
)
AS
-- =============================================
-- SPC Name:spGetReportCRCorporateAircraftApprovalForm
-- Author: Askar
-- Create date:07th May 2013
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON

 DECLARE @TenToMin NUMERIC(1,0);
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 


IF @BeginDate IS NOT NULL  
    BEGIN
		SELECT ---Group Header 1----
			   File_Number=CQF.FileNUM
			  ,Quote_Number=CM.QuoteNUM
			  ,Trip_Dates= [dbo].[GetShortDateFormatByUserCD](@UserCD,CL.DepartureDTTMLocal)+' - '+[dbo].[GetShortDateFormatByUserCD](@UserCD,CL.ArrivalDTTMLocal)
			  ,Tail_Number=F.TailNum
			  ,[Type]=AT.AircraftCD
			  ,[Description]=CQF.CQFileDescription
			  ,Customer=CQF.CQCustomerName
			  ,Currency=CQF.ExchangeRateID
			  ---Details / Labels----
			  ,DEP=D.IcaoID
			  ,ARR=A.IcaoID
			  ,MILES=CL.Distance
			  ,ETE=CL.ElapseTM
			  ,ACTUAL_MILES='_____________________'
			  ,ACTUAL_HOURS='_____________________'
			  ,TOTALS='_____________________'
		      ,@TenToMin TenToMin
		      ,CL.LegNUM
		 FROM CQFile CQF
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
			  INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0
			  INNER JOIN Fleet F ON F.FleetID=CM.FleetID
			  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=CM.AircraftID
			  LEFT OUTER JOIN Airport D ON D.AirportID=CL.DAirportID
			  LEFT OUTER JOIN Airport A ON A.AirportID=CL.AAirportID
			  LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID    
			  LEFT OUTER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID    
              LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CQF.CQCustomerID  
              INNER JOIN Company C ON CQF.HomebaseID=C.HomebaseID
              LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID
	  WHERE CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
	    AND CM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	    AND(F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )    
		AND (V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )    
		AND (AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )    
		AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )    
		AND (SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )     
		AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
		ORDER BY CQF.FileNUM,CM.QuoteNUM,CL.LegNUM
   END
ELSE
  BEGIN
  	SELECT ---Group Header 1----
			   File_Number=CQF.FileNUM
			  ,Quote_Number=CM.QuoteNUM
			  ,Trip_Dates= [dbo].[GetShortDateFormatByUserCD](@UserCD,CL.DepartureDTTMLocal)+' - '+[dbo].[GetShortDateFormatByUserCD](@UserCD,CL.ArrivalDTTMLocal)
			  ,Tail_Number=F.TailNum
			  ,[Type]=AT.AircraftCD
			  ,[Description]=CQF.CQFileDescription
			  ,Customer=CQF.CQCustomerName
			  ,Currency=CQF.ExchangeRateID
			  ---Details / Labels----
			  ,DEP=D.IcaoID
			  ,ARR=A.IcaoID
			  ,MILES=CL.Distance
			  ,ETE=CL.ElapseTM
			  ,ACTUAL_MILES='_____________________'
			  ,ACTUAL_HOURS='_____________________'
			  ,TOTALS='_____________________'
			  ,@TenToMin TenToMin
			  ,CL.LegNUM
		 FROM CQFile CQF
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
			  INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0
			  INNER JOIN Fleet F ON F.FleetID=CM.FleetID
			  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=CM.AircraftID
			  LEFT OUTER JOIN Airport D ON D.AirportID=CL.DAirportID
			  LEFT OUTER JOIN Airport A ON A.AirportID=CL.AAirportID
		WHERE CM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
		 AND (CQF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )    
         AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )    
         AND (CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' ) 
		ORDER BY CQF.FileNUM,CM.QuoteNUM,CL.LegNUM
  
  END 
END
--EXEC spGetReportCQQuoteLogMainInformation 'SUPERVISOR_99','10001','4'	  

GO


