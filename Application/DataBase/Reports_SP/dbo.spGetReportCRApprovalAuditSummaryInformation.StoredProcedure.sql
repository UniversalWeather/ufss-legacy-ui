IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRApprovalAuditSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRApprovalAuditSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCRApprovalAuditSummaryInformation]
	(
		@UserCD VARCHAR(30) --Mandatory 
		,@UserCustomerID VARCHAR(30) --Mandatory
		,@DATEFROM DATE --Mandatory
		,@DATETO DATE   --Mandatory
		,@CRTripNUM VARCHAR(30) = '' --Optional 
		,@TailNum VARCHAR(30) = ''  --Optional
		,@RequestorCD VARCHAR(1000) = '' --Optional
	)
AS
BEGIN 
-- =====================================================
-- SPC Name:spGetReportCRApprovalAuditSummaryInformation
-- Author: Aishwarya.M
-- Create date:2013 May 07
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ======================================================
SET NOCOUNT ON 
	SELECT 
			RequestNumber = CR.CRTripNUM
			,TripsheetNumber = ISNULL(PM.TripNUM,0)
			,TripRequestor = CASE WHEN ISNULL(P.FirstName,'') <> '' OR ISNULL(PrePax.FirstName,'')<>'' THEN ISNULL(PrePax.LastName,P.LastName) + ', ' + ISNULL(PrePax.FirstName,ISNULL(P.FirstName,'')) ELSE ISNULL(PrePax.LastName,P.LastName) END--CR.RequestorName
			,TripDate = ISNULL(PM.EstDepartureDT,CR.EstDepartureDT)
			,TailNumber = ISNULL(FT.TailNum,F.TailNum)
			,DesignatedApprover = CR.Approver
			,ApprovalStatus = CASE CR.CorporateRequestStatus --CR.AcknowledgementStatus
								WHEN 'W' THEN 'Worksheet'
								WHEN 'T' THEN 'Accepted'
								WHEN 'A' THEN 'Accepted'
								WHEN 'D' THEN '' --Denied
								WHEN 'X' THEN 'Submitted'
								WHEN 'M' THEN 'Modified'
								WHEN 'S' THEN 'Submitted'
								WHEN 'C' THEN 'Canceled'								
								ELSE '' END 
			FROM CRMain CR
			LEFT OUTER JOIN Fleet F ON CR.FleetID = F.FleetID
			LEFT OUTER JOIN PreflightMain PM ON CR.TripID = PM.TripID AND PM.IsDeleted = 0
			LEFT OUTER JOIN Fleet FT ON FT.FleetID=PM.FleetID
			--LEFT OUTER JOIN CRPassenger CP ON CR.PassengerRequestorID = CP.PassengerRequestorID
			LEFT OUTER JOIN Passenger P ON CR.PassengerRequestorID = P.PassengerRequestorID
			LEFT OUTER JOIN Passenger PrePax ON PrePax.PassengerRequestorID = PM.PassengerRequestorID
			LEFT OUTER JOIN Fleet PreF ON PreF.FleetID = F.FleetID
			WHERE CR.CustomerID = CONVERT(BIGINT, @UserCustomerID)
			  AND CR.EstDepartureDT BETWEEN @DATEFROM AND @DATETO
			  AND (CR.CRTripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRTripNUM, ',')) OR @CRTripNUM = '')
			  AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
			  AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM DBO.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
			  AND CR.IsDeleted=0
			  ORDER BY CR.CRTripNUM
END		  

--EXEC spGetReportCRApprovalAuditSummaryInformation 'SUPERVISOR_99','10099','2013-04-01','2013-05-01','','',''

GO


