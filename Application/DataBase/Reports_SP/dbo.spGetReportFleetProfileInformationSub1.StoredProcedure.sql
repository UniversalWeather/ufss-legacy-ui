IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileInformationSub1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileInformationSub1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFleetProfileInformationSub1]
	 @UserCD VARCHAR(30)
	,@TailNum char(6)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileInformationSub1
-- Author: Abhishek.s
-- Create date: 27 Sep 2012
-- Description: Get Fleet Profile Sub Report 2 Information
-- Revision History
-- Date			Name		Ver		Change
-- 27 Sep		Abhishek	1.1		SubRep1 for Fleet Profile
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID   BIGINT,
        @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SELECT DISTINCT
			
         
        [CDesc]      = FC.FleetComponentDescription
		,[LInsp]      = FC.LastInspectionDT
		,[Unts]       = FC.IsHrsDaysCycles
		,[UToNInsp]   = FC.InspectionHrs
		,[UToWarn]    = FC.WarningHrs
				
				
      FROM Fleet F 
INNER JOIN FleetComponent FC
		ON F.CustomerID = FC.CustomerID
		AND F.FleetID = FC.FleetID
		
		WHERE F.IsDeleted = 0
	  AND F.CustomerID = @CUSTOMERID
	  AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	  AND F.TailNum = RTRIM(@TailNum)
	  
 -- EXEC spGetReportFleetProfileInformationSub1 'UC', 'BESVDZ'

GO


