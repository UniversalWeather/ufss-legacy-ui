IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportMins2TenthsConversion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportMins2TenthsConversion]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE procedure [dbo].[spGetReportMins2TenthsConversion]     
(  
  @UserCD AS VARCHAR(30)
)  
AS 
-- =============================================
-- SPC Name: spGetReportMins2TenthsConversion
-- Author: SUDHAKAR J
-- Create date: 03 JAN 2013
-- Description: To display the Mins2Tenths Conversion information in reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================   
BEGIN    
	    
	DECLARE @Info1 varchar(300)  
	DECLARE @Info2 varchar(300)   
	DECLARE @HoursMinutesCONV Int -- 1 standard, 2 flightpak , 3 other  
	SET @HoursMinutesCONV=(Select Company.HoursMinutesCONV From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  
			And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))    

	--SET @HoursMinutesCONV = 2
	IF @HoursMinutesCONV=1   
	BEGIN  
		--SET @Info1 = '0  -  5 = .0# 6  - 11 = .1#12 - 17 = .2#18 - 23 = .3#24 - 29 =.4'   
		--SET @Info2 = '30 - 35 = .5#36  - 41 = .6#42 - 47 = .7#48 - 53 = .8#54 - 59 =.9' 
		--INSERT INTO @Conversion VALUES ('0.0','0','5','0.5','30','35')
		--INSERT INTO @Conversion VALUES ('0.1','6','11','0.6','36','41')
		--INSERT INTO @Conversion VALUES ('0.2','12','17','0.7','42','47')
		--INSERT INTO @Conversion VALUES ('0.3','18','23','0.8','48','53')
		--INSERT INTO @Conversion VALUES ('0.4','24','29','0.9','54','59')
	 
		SET @Info1 = '   0 -  5 =  .0       6 - 11 =  .1      12 - 17 =  .2      18 - 23 =  .3      24 - 29 =  .4#30 - 35 =  .5      36 - 41 =  .6      42 - 47 =  .7      48 - 53 =  .8      54 - 59 =  .9#' 
		SELECT @Info1 AS CONVERSION;
	END  
	ELSE IF @HoursMinutesCONV=2   
	BEGIN  
		--SET @Info1 = '3  -  8 = .1# 9  - 14 = .2#15 - 21 = .3#22 - 27 = .4#28 - 32 =.5'   
		--SET @Info2 = '33 - 39 = .6#40  - 44 = .7#45 - 50 = .8#51 - 57 = .9#58 -  2 =.0'
		--INSERT INTO @Conversion VALUES ('0.1','3','8','0.6','33','39')
		--INSERT INTO @Conversion VALUES ('0.2','9','14','0.7','40','44')
		--INSERT INTO @Conversion VALUES ('0.3','15','21','0.8','45','50')
		--INSERT INTO @Conversion VALUES ('0.4','22','27','0.9','51','57')
		--INSERT INTO @Conversion VALUES ('0.5','28','32','1.0','58','59') 

		SET @Info1 = '   3 -  8 =  .1        9 - 14 =  .2       15 - 21 =  .3       22 - 27 =  .4       28 - 32 =  .5#33 - 39 =  .6       40 - 44 =  .7       45 - 50 =  .8       51 - 57 =  .9       58 - 59 = 1.0#'    
		SELECT @Info1 AS CONVERSION;
	END  
	ELSE IF @HoursMinutesCONV=3   
	BEGIN  
		--SHOULD GET FROM DATABASE'
		SET @Info1 = ''; SET @Info2 = '';
		DECLARE @Tenths VARCHAR(4), @StartMinimum VARCHAR(4), @ENDMinutes VARCHAR(4);
		DECLARE @COUNTER SMALLINT = 1;
		DECLARE curConversion CURSOR FOR
			SELECT CONVERT(VARCHAR,Tenths), CONVERT(VARCHAR,StartMinimum), CONVERT(VARCHAR,ENDMinutes)
			FROM ConversionTable
			WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD);

		OPEN curConversion;
		FETCH NEXT FROM curConversion INTO @Tenths, @StartMinimum, @ENDMinutes;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			--PRINT CONVERT(VARCHAR,@COUNTER) + ' - ' + CONVERT(VARCHAR,@COUNTER % 2)
			--SET @Info1 = @Info1 + @StartMinimum + ' - ' + @ENDMinutes + ' = ' + @Tenths + ' # '

			IF LEN(@StartMinimum) = 1 SET @StartMinimum = ' ' + @StartMinimum;
			IF LEN(@ENDMinutes) = 1 SET @ENDMinutes = ' ' + @ENDMinutes;
			IF CONVERT(FLOAT, @Tenths) < 1 SET @Tenths = '  ' +SUBSTRING(@Tenths,2,LEN(@Tenths));
			
			IF @COUNTER % 2 = 0 BEGIN
				SET @Info2 = @Info2 + @StartMinimum + ' - ' + @ENDMinutes + ' = ' + @Tenths + '#'	
			END ELSE BEGIN
				SET @Info2 = @Info2 + @StartMinimum + ' - ' + @ENDMinutes + ' = ' + @Tenths + '   '	
			END
		
			SET @COUNTER = @COUNTER + 1		
			FETCH NEXT FROM curConversion INTO @Tenths, @StartMinimum, @ENDMinutes;
		END
		CLOSE curConversion;
		DEALLOCATE curConversion;

		SELECT @Info2 AS CONVERSION;
	END  

--SELECT * FROM @Conversion
	--SELECT Tenths, StartMinimum, ENDMinutes
	--FROM ConversionTable 
	--WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
	--                        AND HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)  

END  
  
--exec spGetReportMins2TenthsConversion 'jwilliams_11'
--exec spGetReportMins2TenthsConversion 'jwilliams_13'
--exec spGetReportMins2TenthsConversion 'jk_99'     


GO


