IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFuelPriceConversion]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFuelPriceConversion]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ===============================================================================================
-- Function Name: GetFuelPriceConversion 
-- Author: Mathes
-- Create date: 30 Nov 2012
-- Description: Returns Unit Price in the format set in CompanyProfile for the User Company and Homebase
-- Revision History
-- Date			Name		Ver		Change 
--
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetFuelPriceConversion] (@FromUnit INT, @FromPrice Numeric(10,4), @UserCD VARCHAR(30))
RETURNS NUMERIC (15,4)
WITH EXECUTE AS CALLER
AS
BEGIN
	/*
	*- From/To Units:
		*- 	1=Gallons
		*- 	2=Liters
		*-	3=Imp Gallons
		*-  4=Pounds
		*-  5=Kilos
	*/
	DECLARE @ToUnits NUMERIC(15,2); --Variable to fetch the conversion units
	DECLARE @ToQuantity NUMERIC(15,4); --Variable to return the converted quantity

	--Fetch ToUnits set in CompanyProfile settings for the UserCompany & HomeBase
	SELECT @ToUnits = FuelPurchase 
	FROM COMPANY 
	WHERE CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	  AND HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))


	SELECT 	@ToQuantity =
		
	CASE							
		WHEN @FromUnit = 1 THEN		-- From Gallons
		
			CASE 
				WHEN @ToUnits = 1 THEN				-- To Gallons
					@FromPrice
				WHEN @ToUnits = 2	THEN			-- To Liters
					@FromPrice / 3.7854
				WHEN @ToUnits = 3	THEN			-- To Imp Gallons
					@FromPrice / 0.83267
				WHEN @ToUnits = 4	THEN			-- To Pounds
					@FromPrice / 6.7000
                WHEN @ToUnits = 5    THEN            -- To Kilos
					@FromUnit * 3.0390664
			END
		
		WHEN @FromUnit = 2 THEN		-- From Liters
			
			CASE
				WHEN @ToUnits = 1 THEN				-- To Gallons
					@FromPrice / 0.2641721
				WHEN @ToUnits = 2 THEN				-- To Liters
					@FromPrice
				WHEN @ToUnits = 3	THEN			-- To Imp Gallons
					@FromPrice / 0.2199692
				WHEN @ToUnits = 4	THEN			-- To Pounds
					@FromPrice / 1.8
                WHEN @ToUnits = 5    THEN            -- To Kilos
					@FromUnit * .816465
			END
		WHEN @FromUnit = 3	THEN	-- From Imp Gallons
		
			CASE
				WHEN @ToUnits = 1 THEN				-- To Gallons
					@FromPrice / 1.20095
				WHEN @ToUnits = 2 THEN				-- To Liters
					@FromPrice / 4.546092
				WHEN @ToUnits = 3 THEN				-- To Imp Gallons
					@FromPrice
				WHEN @ToUnits = 4 THEN				-- To Pounds
					 @FromPrice / 8.0467
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromUnit * 3.649918
			END
			
		WHEN @FromUnit = 4	THEN	-- From Pounds
		
			CASE
				WHEN @ToUnits = 1 THEN				-- To Gallons
					@FromPrice / 0.149253
				WHEN @ToUnits = 2	THEN			-- To Liters
					@FromPrice / 0.555555
				WHEN @ToUnits = 3	THEN			-- To Imp Gallons
					@FromPrice / 0.124274
				WHEN @ToUnits = 4	THEN			-- To Pounds
					@FromPrice
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromUnit * .453592 
			END
		        WHEN @FromUnit = 5    THEN    -- From Pounds
       
            CASE
                WHEN @ToUnits = 1 THEN                -- To Gallons
                    @FromUnit * .3290484
                WHEN @ToUnits = 2    THEN            -- To Liters
                    @FromUnit * 1.2247913
                WHEN @ToUnits = 3    THEN            -- To Imp Gallons
                    @FromUnit * .2739787
                WHEN @ToUnits = 4    THEN            -- To Pounds
                    @FromUnit * 2.20462
                WHEN @ToUnits = 5    THEN            -- To Kilos
                    @FromUnit    
            END 				
	ELSE
		@FromPrice
	END

		
    RETURN @ToQuantity;
END;
--Input FromUnit, FromPrice, UserCD
--SELECT DBO.GetFuelPriceConversion(4,200,'UC')



GO


