IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSBestFuelVendorExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSBestFuelVendorExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSBestFuelVendorExportInformation]
@UserCD            AS VARCHAR(30)    
,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'

AS
BEGIN
-- ===========================================================================================
-- SPC Name: spGetReportPRETSBestFuelVendorExportInformation
-- Author: AISHWARYA.M
-- Create date: 15 Sep 2012
-- Description: Get Preflight Tripsheet Report Best Fuel Vendor Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ===========================================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	SELECT PL.LegID
		,[orig_nmbr]= PM.TripNUM
		,[ordernum]= PF.OrderNUM 
		,[depicao_id]= AD.IcaoID
		,[fboname]= PF.FBOName
		,[effdate]= PF.EffectiveDT
		,[bestprice]= PF.BestPrice
		,[gallonfrom]= PF.GallonFrom 
		,[gallonto]= PF.GallonTo
		,[unitprice]= PF.UnitPrice
		,[vendorcode]= FV.VendorCD
		,[lastuser]= UM.UserName
		,[lastupdt]= PF.LastUpdTS
		,[hlrange]= PF.Lrange
		,[hlprice]= PF.Lprice
		,[leastprice]= PF.LeastPrice
		,[updatedttm]= PF.UpdateDTTM
		,[ldeleted]= 'WIP'
		,[vendorname]= FV.VendorCD
		,[ntripno]= 'WIP'
		,[nlegno]= 'WIP'
		,[cset]= 'WIP'
		,[lowdate]= PL.DepartureDTTMLocal
		,[highdate]= PL.ArrivalDTTMLocal
		,[desc]= PM.TripDescription
		,[custdesc]= CASE PM.IsQuote WHEN 1 THEN PM.AuthorizationDescription ELSE '' END
		,[paxname]= P.LastName + ',' + P.FirstName + ' ' + P.MiddleInitial
		,[custphone]= 'CQFILE.DBF/PHONE'
		,[phone]= P.PhoneNum
		,[reqaddlphone]= P.AdditionalPhoneNum
		,[tail_nmbr]= F.TailNum
		,[type_code]= A.AircraftCD
		,[fltphone]= F.FlightPhoneNum
		,[infltphone]= F.FlightPhoneIntlNum
		,[revisnnmbr]= PM.RevisionNUM
		,[dispatchno]= PM.DispatchNUM
		,[dsptname]= UM.FirstName + ' ' + UM.LastName--PM.DsptnName,
		,[dsptphone]= UM.PhoneNum
		,[dsptemail]= UM.EmailID
		,[revisndesc]= PM.RevisionDescriptioin
		,[crewcode]= C.CrewCD
		,[releasedby]= C.LastName + ',' + C.FirstName + ' ' + C.MiddleInitial
		,[verifynmbr]= PM.VerifyNUM
		FROM @tblTripInfo Main
		INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON Main.TripID = PL.TripID
		INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
		LEFT OUTER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
		INNER JOIN Fleet F ON PM.FleetID = F.FleetID
		INNER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID
		LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID= P.PassengerRequestorID
		LEFT OUTER JOIN PreflightFuel PF ON PL.LegID = PF.LegID
		LEFT OUTER JOIN FuelVendor FV ON PF.FuelVendorID = FV.FuelVendorID
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		--AND PM.TripID = @TripID
		ORDER BY PL.LegID

END
	--spGetReportPRETSBestFuelVendorExportInformation 'UC', 10000350 
 
--EXEC spGetReportPRETSBestFuelVendorExportInformation 'UC', '20', '', '', '20120720', '20120725', 'ZZKWGS', 'test', '', '', '', '', ''

GO


