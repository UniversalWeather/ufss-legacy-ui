IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREMonthlyFleetCalendarExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREMonthlyFleetCalendarExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetReportPREMonthlyFleetCalendarExportInformation] 
  @UserCD AS VARCHAR(30),
  @MONTH AS VARCHAR(10),
  @YEAR  AS INT, 
  @AircraftDutyCD	VARCHAR(100)='',
  @IsHomeBase AS BIT = 0
AS  
-- ===============================================================================  
-- SPC Name: spGetReportPREMonthlyFleetCalendarInformation 
-- Author: Askar  
-- Create date: 18 Jul 2012  
-- Description: Get FleetCalendar information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
BEGIN


  DECLARE @date DATETIME

  
   DECLARE @CUSTOMERID BIGINT = (SELECT CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
   Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
										 
--SET @SuppressActivityAircft=1
SELECT @date=CONVERT(DATETIME,CONVERT(VARCHAR(40),CONVERT(VARCHAR(10),@YEAR)+'-'+@MONTH+'-'+'01' ))

DECLARE @DATETO DATE =(SELECT CONVERT(DATE,DATEADD(dd,-(DAY(DATEADD(mm,1,@date))),DATEADD(mm,1,@date)))); 
         DECLARE @TblDates TABLE (AllDates DATE);
          WITH dates(DATEPARAM)
           AS (SELECT @date AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO @TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)

 
 CREATE TABLE #FleetTemp (FleetID BIGINT,  
                   TailNum VARCHAR(60),  
                   ScheduleDate DATE,
                    Aircraft CHAR(4),
                    HomeBase BIGINT,
					AircraftDutyCD CHAR(2),
					TypeCode CHAR(4) )    
       INSERT INTO #FleetTemp                
       SELECT  F.FleetID,F.TailNum,AllDates ScheduleDate,F.AircraftCD,F.HomebaseID,'',F.AircraftCD
            FROM  Fleet F 
            CROSS JOIN @TblDates  
            WHERE  F.CustomerID = @CUSTOMERID
              AND F.IsDeleted=0







DECLARE @FleetInfo TABLE(TripNUM BIGINT,
                        LegNUM BIGINT,
                        TripID BIGINT,
                        LegID BIGINT,
                        Depart DATETIME,
                        Arrive DATETIME,
                        NextLocal DATETIME,
                        TripStatus CHAR(1),
                        DutyTYPE CHAR(2),
                        FleetID BIGINT,
                        TailNum VARCHAR(60),
                        Aircraft CHAR(4),
                        HomeBase BIGINT,
                        AircraftDutyCD CHAR(2),
                        TypeCode CHAR(4)
                       )
    
INSERT INTO @FleetInfo            
SELECT  PM.TripNUM,PL.LegNUM,PM.TripID,PL.LegID,CASE WHEN PL.DepartureDTTMLocal < @DATE THEN @DATE  ELSE PL.DepartureDTTMLocal END  Depart,
                 CASE WHEN PL.ArrivalDTTMLocal > @DATETO THEN @DATETO  ELSE PL.ArrivalDTTMLocal END  Arrive,PL.NextLocalDTTM,
                 PM.TripStatus, PL.DutyTYPE,FT.FleetID,FT.TailNum,FT.Aircraft,F.HomebaseID,AD.AircraftDutyCD,F.AircraftCD 
                      FROM PreflightMain PM 
                            INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
                            INNER JOIN #FleetTemp FT ON PM.FleetID=FT.FleetID
                            INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                            INNER JOIN AircraftDuty AD ON PM.CustomerID=AD.CustomerID
                            
                           

WHERE 
 (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND PM.RecordType <> 'C'
AND PM.TripStatus IN ('T','H')
---AND (PM.HomebaseID = CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR @IsHomeBase = 0 ) 

ORDER BY PL.DutyTYPE,PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,PL.TripID,PL.LegID

 
 

 
 INSERT INTO @FleetInfo
SELECT T.TripNUM,PL.LegNUM,T.TripID,PL.LegID,CASE WHEN PL.DepartureDTTMLocal < @DATE THEN @DATE  ELSE PL.DepartureDTTMLocal END  Depart,
          CASE WHEN PL.ArrivalDTTMLocal > @DATETO THEN @DATETO  ELSE PL.ArrivalDTTMLocal END  Arrive,
          PL.NextLocalDTTM,T.TripStatus,PL.DutyTYPE,T.FleetID,T.TailNum,T.Aircraft,T.HomeBase,T.AircraftDutyCD,T.TypeCode
                                             FROM @FleetInfo T 
                                             INNER JOIN PreflightMain PM ON T.TripID=PM.TripID
                                             INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
                                             WHERE PL.LegNUM=T.LegNUM-1
  

  
 DECLARE @FleetCalendar TABLE(TripNum BIGINT,
                            LegNum BIGINT,
                            TripID BIGINT,
                            LegID BIGINT,
                            ScheduleDate DATE,
                            TripStatus CHAR(1),
                            DutyType CHAR(2),
                            FleetID BIGINT,
                            TailNum VARCHAR(100),
                            Aircraft CHAR(4),
                            HomeBase BIGINT,
                            TypeCode CHAR(4),
                            TransFleet BIGINT) 
  

 INSERT INTO @FleetCalendar        
SELECT DISTINCT TEMP.TripNUM,TEMP.LegNUM,TEMP.TripID,TEMP.LegID,T.ScheduleDate,TEMP.TripStatus,  
                (CASE WHEN ISNULL(TEMP.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(TEMP.TripStatus, '') = 'H' THEN  'H' 
										     WHEN ISNULL(TEMP.TripStatus, '') = 'T' THEN  'F' ELSE 'AA' END
									ELSE TEMP.DutyTYPE END) DutyTYPE,
                T.FleetID,T.TailNum,T.Aircraft,T.HomeBase,T.TypeCode,Fleet FROM #FleetTemp T  
LEFT OUTER JOIN (  
SELECT C.FleetID,CI.Aircraft,CI.HomeBase,CI.TripNUM,CI.TripID,C.TailNum 
,CI.Depart,CI.LegNUM,CI.LegID,C.ScheduleDate
,CI.Arrive,CI.NextLocal
,CASE WHEN 
CONVERT(DATE,C.ScheduleDate) > CONVERT(DATE,CI.Arrive) AND CONVERT(DATE,C.ScheduleDate) < CONVERT(DATE,CI.NextLocal) THEN 'R' 
ELSE CI.DutyTYPE END DutyTYPE,CI.TripStatus,CI.FleetID Fleet 
                               FROM #FleetTemp C LEFT OUTER JOIN @FleetInfo CI ON C.FleetID=CI.FleetID 
                               WHERE CONVERT(DATE, C.ScheduleDate) >= CONVERT(DATE,CI.Depart)
                                 AND  CONVERT(DATE,C.ScheduleDate) <= CONVERT(DATE,ISNULL(CI.NextLocal,CI.Arrive))
                                
                                )TEMP ON T.FleetID=TEMP.FleetID AND T.ScheduleDate=TEMP.ScheduleDate
                       
  
 ORDER BY T.ScheduleDate
  

IF @SuppressActivityAircft=1 
BEGIN
DELETE FROM @FleetCalendar WHERE FleetID NOT IN(SELECT TransFleet FROM @FleetCalendar WHERE TransFleet IS NOT NULL AND DutyType <> '')
                            AND TransFleet IS NULL
END

  IF @AircraftDutyCD <> ''
  
 BEGIN
  UPDATE @FleetCalendar SET DutyType='' WHERE (DutyTYPE NOT IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftDutyCD, ',')))  
 END

 DECLARE @FleetTemp TABLE(
                           ScheduleDate DATE,
                           FleetID BIGINT,
                           TailNum VARCHAR(10),
                           DutyType CHAR(2),
                           TypeCode CHAR(4),
                           Rnk  INT)





INSERT INTO @FleetTemp
SELECT  DISTINCT T.ScheduleDate,T.FleetID,T.TailNum,DutyType,T.TypeCode,
         RANK() OVER (PARTITION BY T.FleetID,T.ScheduleDate ORDER BY T.DutyType ASC) Rnk  FROM @FleetCalendar T
					INNER JOIN #FleetTemp C ON C.FleetID=T.FleetID
					WHERE (T.HomeBase=(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0) 
                 

DELETE FROM @FleetTemp WHERE RNK > 1
                         AND DutyType='AA'


UPDATE @FleetTemp SET DutyType='' WHERE DutyType='AA'

  
 SELECT   DISTINCT TailNum,TypeCode FROM @FleetTemp  
WHERE ScheduleDate BETWEEN CONVERT(DATE,@date) AND CONVERT(DATE,@DATETO) 
ORDER BY TypeCode           


--SELECT   ScheduleDate,T.FleetID,T.TailNum,DutyType,T.Aircraft,T.HomeBase  FROM @FleetCalendar T 
--                   ---  WHERE  (Homebase IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IsHomeBase, ',')) OR @IsHomeBase = 0 ) 
--                     ORDER BY Aircraft
    
IF OBJECT_ID('tempdb..#FleetTemp') IS NOT NULL
DROP TABLE #FleetTemp

 
END
      


 
--- spGetReportPREMonthlyFleetCalendarExportInformation 'JWILLIAMS_13','FEB',2012




GO


