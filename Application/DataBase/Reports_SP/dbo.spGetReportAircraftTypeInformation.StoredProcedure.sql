IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAircraftTypeInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAircraftTypeInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportAircraftTypeInformation]
	@UserCD VARCHAR(30)
AS
-- =============================================
-- SPC Name: spGetReportAircraftTypeInformation
-- Author: SUDHAKAR J
-- Create date: 22 Jun 2012
-- Description: Get the Aircraft Type information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- =============================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

DECLARE @TenToMin INT
SELECT @TenToMin = Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD) 
                    
	SELECT *, [TenToMin] = @TenToMin FROM (
		SELECT [CustomerID],[AircraftCD],[AircraftDescription],[ChargeRate],[ChargeUnit],
			 [PowerDescription] = [PowerSetting] + '  ' + [PowerDescription],[WindAltitude],[IsFixedRotary]
			  , '1  ' + [PowerSettings1Description] AS [PowerSettings]
			  ,[PowerSettings1TrueAirSpeed] AS [TrueAirSpeed]
			  ,[PowerSettings1HourRange] AS [HourRange]
			  ,[PowerSettings1TakeOffBias] AS [TakeOffBias]
			  ,[PowerSettings1LandingBias] AS [LandingBias]
		  FROM [Aircraft] WHERE IsDeleted = 0
		   AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
		   AND [PowerSetting] IS NOT NULL
		UNION ALL
		SELECT [CustomerID],[AircraftCD],[AircraftDescription],[ChargeRate],[ChargeUnit],
			[PowerDescription] = [PowerSetting] + '  ' + [PowerDescription],[WindAltitude],[IsFixedRotary]
			  , '2  ' + [PowerSettings2Description] AS [PowerSettings]
			  ,[PowerSettings2TrueAirSpeed] AS [TrueAirSpeed]
			  ,[PowerSettings2HourRange] AS [HourRange]
			  ,[PowerSettings2TakeOffBias] AS [TakeOffBias]
			  ,[PowerSettings2LandingBias] AS [LandingBias]
		  FROM [Aircraft] WHERE IsDeleted = 0
		   AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
		    AND [PowerSetting] IS NOT NULL
		UNION ALL
		SELECT [CustomerID],[AircraftCD],[AircraftDescription],[ChargeRate],[ChargeUnit],
			[PowerDescription] = [PowerSetting] + '  ' + [PowerDescription],[WindAltitude],[IsFixedRotary]
			  , '3  ' + [PowerSettings3Description] AS [PowerSettings]
			  ,[PowerSettings3TrueAirSpeed] AS [TrueAirSpeed]
			  ,[PowerSettings3HourRange] AS [HourRange]
			  ,[PowerSettings3TakeOffBias] AS [TakeOffBias]
			  ,[PowerSettings3LandingBias] AS [LandingBias]
		  FROM [Aircraft] WHERE IsDeleted = 0
		   AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
		    AND [PowerSetting] IS NOT NULL
		
		UNION ALL -- TO INSERT A BLANK ROW AFTER EACH [AircraftCD]
		SELECT [CustomerID],[AircraftCD],NULL,NULL,NULL,
			[PowerDescription] = NULL,NULL,NULL
			  , '4' AS [PowerSettings]
			  ,NULL AS [TrueAirSpeed]
			  ,NULL AS [HourRange]
			  ,NULL AS [TakeOffBias]
			  ,NULL AS [LandingBias]
		  FROM [Aircraft] WHERE IsDeleted = 0
		   AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
		    AND [PowerSetting] IS NOT NULL
	) A 
    WHERE CustomerID = @CUSTOMERID    
	ORDER BY [AircraftCD],[PowerSettings] 

-- EXEC spGetReportAircraftTypeInformation 'jwilliams_13'

GO


