
/****** Object:  StoredProcedure [dbo].[spGetReportPOSTFlightLogInformationHeader]    Script Date: 10/22/2014 22:54:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogInformationHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogInformationHeader]
GO
/****** Object:  StoredProcedure [dbo].[spGetReportPOSTFlightLogInformationHeader]    Script Date: 10/22/2014 22:54:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFlightLogInformationHeader] 
	  @UserCD AS VARCHAR(30),  
	  @LogID BIGINT  
AS
BEGIN
	DECLARE @DateFormate INT;
	DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @tblTripInfo AS TABLE 
	(  
		POLOGID BIGINT,
		POLEGID BIGINT,
		CustomerID BIGINT 
	)  
SELECT @DateFormate=CONVERT(INT,DayMonthFormat) 
				FROM Company 
				WHERE HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD) AND CustomerID=@CustomerID


INSERT INTO @tblTripInfo (POLOGID, POLEGID,CustomerID)
SELECT PM.POLogID,POLegID,@CustomerID FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
WHERE PM.CustomerID=@CustomerID
AND CONVERT(VARCHAR(30),PM.POLogID)=@LogID
AND PM.IsDeleted = 0

	SELECT PM.LogNum 
	   ,ISNULL(PM.POMainDescription,'') [Description]
	   ,CASE WHEN @DateFormate=1 THEN
			CONVERT(VARCHAR(10),PL.MinDate,103)+ '-' + CONVERT(VARCHAR(10),PL.MaxDate,103)
		ELSE
			CONVERT(VARCHAR(10),PL.MinDate,101)+ '-' + CONVERT(VARCHAR(10),PL.MaxDate,101)
		END AS TripDates
       ,ISNULL(P.LastName+ ', ','') +ISNULL(P.FirstName,'')+ ' ' +ISNULL(P.MiddleInitial,'') Requestor
	   ,F.TailNum TailNumber
	   ,ISNULL(P.AdditionalPhoneNum,'') ContactPhone
	   ,ISNULL(PM.DispatchNUM,'') DispatchNum
       ,ISNULL(A.AircraftCD,'') Type
       ,PM.POLogID
	    FROM (SELECT DISTINCT POLOGID FROM @tblTripInfo)M
		INNER JOIN PostflightMain PM ON M.pologid = PM.POLogID AND PM.IsDeleted = 0
		INNER JOIN (SELECT MIN(ScheduledTM)MinDate,MAX(ScheduledTM)MaxDate,CustomerID,POLogID FROM PostflightLeg WHERE IsDeleted = 0 GROUP BY CustomerID,POLogID)PL ON M.POLogID = PL.POLogID
		INNER JOIN Fleet F ON  PM.FleetID = F.FleetID
		LEFT OUTER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
		LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID				
		WHERE PM.CustomerID = @CustomerID
END



GO

