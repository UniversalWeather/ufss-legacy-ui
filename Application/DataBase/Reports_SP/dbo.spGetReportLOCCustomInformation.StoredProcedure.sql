IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLOCCustomInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLOCCustomInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportLOCCustomInformation]  
	    @UserCustomerID BIGINT
       ,@IcaoID VARCHAR(4)
       ,@CountryCD VARCHAR(2)
       ,@MilesFrom INT 
       ,@CityName VARCHAR(25)
       ,@MetroCD VARCHAR(3)
       ,@StateName VARCHAR(25)
       ,@AirportName VARCHAR(25) 
       ,@IATA VARCHAR(3)
       ,@AirportID VARCHAR(30)
AS

-- ================================================================================  
-- SPC Name: spGetReportLOCCustomInformation  
-- Author: Askar  
-- Create date: 17 Sep 2013  
-- Description:  Get the Locator information of Airport  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- =================================================================================   
BEGIN       
    --1.city,country and state,2.icao 3.iata,city,country,state  
SELECT DISTINCT CustomName=CA.Name
      ,CityName=CA.CustomCity
      ,StateName=CA.CustomState
      ,Country=C.CountryName
      ,ICAO=A.IcaoID
      ,CA.PhoneNum
	  ,LatitudeRad=CASE WHEN UPPER(A.LatitudeNorthSouth) = 'S' THEN (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  ELSE (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRad=CASE WHEN UPPER(A.LongitudeEastWest) = 'W' THEN (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  ELSE (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180 END 
	  ,LatitudeRadCityCenter=CASE WHEN UPPER(CA.LatitudeNorthSouth) = 'S' Then (0-(CA.LatitudeDegree + (CA.LatitudeMinutes / 60))) * PI() / 180  ELSE (CA.LatitudeDegree + (CA.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRadCityCenter =CASE WHEN UPPER(CA.LongitudeEastWest) = 'W' THEN (0-(CA.LongitudeDegrees + (CA.LongitudeMinutes / 60))) * PI() / 180  ELSE (CA.LongitudeDegrees + (CA.LongitudeMinutes / 60)) * PI() / 180 END 			
	  ,CA.Symbol
      ,CA.CustomCity
      ,CA.LatitudeDegree
	  ,CA.LatitudeMinutes
	  ,CA.LatitudeNorthSouth
	  ,CA.LongitudeDegrees
	  ,CA.LongitudeMinutes
	  ,CA.LongitudeEastWest
	  --,Latitude=(CASE WHEN CA.LatitudeNorthSouth='N' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(4),CA.LatitudeDegree)+CONVERT(VARCHAR(10),FLOOR(ISNULL((CA.LatitudeMinutes/60),0)*100)*0.01)
	  --,Longitude=(CASE WHEN CA.LongitudeEastWest='E' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(4),CA.LongitudeDegrees)+CONVERT(VARCHAR(10),FLOOR(ISNULL((CA.LongitudeMinutes/60),0)*100)*0.01)
	  ,Latitude=(CASE WHEN CA.LatitudeNorthSouth='S' THEN '-' ELSE '' END)
	  ,Longitude=(CASE WHEN CA.LongitudeEastWest='W' THEN '-' ELSE '' END)
	  ,LatitudeCalculation = (CA.LatitudeDegree + (FLOOR(ISNULL((CA.LatitudeMinutes/60),0)*100)*0.01))
	  ,LongitudeCalculation = (CA.LongitudeDegrees + (FLOOR(ISNULL((CA.LongitudeMinutes/60),0)*100)*0.01))
	  ,C.CountryCD
	  ,CA.CustomAddressCD
	  INTO  #TempResultTable		
      FROM CustomAddress CA
           LEFT OUTER JOIN Airport A ON A.AirportID=CA.AirportID         
		   LEFT OUTER JOIN Country C ON CA.CountryID = C.CountryID 
      WHERE (((CA .CustomerID = @UserCustomerID)) OR (ISNULL(CONVERT(BIGINT,@AirportID),0))=0 OR A.AirportID = CONVERT(BIGINT,@AirportID))
        AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID='')
        AND (C.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ',')) OR @CountryCD='')
        AND (CA.CustomCity IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CityName, ',')) OR @CityName='')
        AND (CA.MetropolitanArea IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MetroCD, ',')) OR @MetroCD='')
        AND (CA.CustomState IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ',')) OR @StateName='')
        AND (CA.Name IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportName, ',')) OR @AirportName='')
        AND (A.Iata IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IATA , ',')) OR @IATA='')
        AND CA.IsDeleted=0
        AND CA.CustomerID = @UserCustomerID
        
    
 SELECT Custom.* FROM (
 SELECT CustomName
       ,Country
       ,StateName
       ,CityName
       ,ICAO
       ,PhoneNum
       ,Miles=CONVERT(INT,ISNULL((CAST( ACOS(SIN(LatitudeRadCityCenter) * SIN(LatitudeRad) + COS(LatitudeRadCityCenter) * COS(LatitudeRad) * COS(LongitudeRad - (LongitudeRadCityCenter))) * 3959   AS INT)),0),0) 
       ,Symbol
       ,CustomCity
       ,LatitudeDegree
	   ,LatitudeMinutes
	   ,LatitudeNorthSouth
	   ,LongitudeDegrees
	   ,LongitudeMinutes
	   ,LongitudeEastWest
	   ,Latitude
	   ,Longitude
	   ,LatitudeCalculation
	   ,LongitudeCalculation
	   ,CustomAddressCD
	  ,CountryCD
     FROM #TempResultTable    
 )Custom
 WHERE Miles <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 
        
END

	   





GO


