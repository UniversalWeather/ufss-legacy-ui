IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteLogSubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteLogSubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportCQQuoteLogSubInformation] 
( 
      @UserCustomerID AS BIGINT,
      @UserCD AS VARCHAR(30), --Mandatory
      @FileNumber INT,    
      @QuoteNUM INT
)
AS
-- =============================================
-- SPC Name:spGetReportCRCorporateAircraftApprovalForm
-- Author: Askar
-- Create date:07th May 2013
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON


 DECLARE @TenToMin NUMERIC(1,0);
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

--SET @TenToMin=2
DECLARE @QuoteTotal TABLE(Row_Id INT,
                          InfoDesc VARCHAR(60),
                          InfoDescValue NUMERIC(17,3),
                          Quotation NUMERIC(17,2),
                          CharVal CHAR(1),
						  ConcatStr VARCHAR(200),
						  FileNumber BIGINT)

INSERT INTO @QuoteTotal
SELECT Quote.* FROM 
(
SELECT DISTINCT 1 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),QuoteMinimumUse2FeeDepositAdj),'DAILY USEAGE ADJUSTMENT:')
               ,InfoDescValue= ISNULL(CM.AdjAmtTotal,0)
               ,Quotation=ISNULL(CM.UsageAdjTotal,0)
               ,'Y' CharVal
               ,'<B>'+' HRS'+'</B>'  ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
	WHERE CQF.CustomerID=@UserCustomerID
	  AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	  AND CM.QuoteNUM = @QuoteNUM
		

UNION ALL

SELECT DISTINCT 2 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.QuoteLandingFeeDefault),'LANDING FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.LandingFeeTotal,0)
               ,'Y' CharVal
               ,'' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 3 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),QuoteSegment2FeeDeposit),'SEGMENT FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.SegmentFeeTotal,0)
               ,'Y' CharVal
               ,'' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
	
UNION ALL

SELECT DISTINCT 4 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.QouteStdCrew2RONDeposit),'CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.StdCrewRONTotal,0)
               ,'Y' CharVal
               ,'<B>'+' RON'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 5 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.AdditionalCrewCD),'ADDITIONAL CREW CHARGE:')
               ,InfoDescValue=ISNULL(CM.AdditionalCrewRONTotal1,0) 
               ,Quotation=ISNULL(CM.AdditionalCrewTotal,0)
               ,'Y' CharVal
               ,'<B>'+' CREW'+'</B>'  ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 6 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.QuoteAdditionCrewRON),'ADDITIONAL CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.AdditionalCrewRONTotal2,0)
               ,'Y' CharVal
               ,'<B>'+' RON'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 7 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.QuoteWaiting2Charge),'WAIT CHARGE:')
               ,InfoDescValue=ISNULL(CM.WaitTMTotal,0) 
               ,Quotation=ISNULL(CM.WaitChgTotal,0)
               ,'Y' CharVal
               ,'<B>'+' HRS'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 8 Row_Id,InfoDesc=ISNULL(CONVERT(VARCHAR(60),C.Deposit2FlightCharge),'FLIGHT CHARGE:')
               ,InfoDescValue=CONVERT(VARCHAR(30),CM.FlightHoursTotal)
               ,Quotation=ISNULL(CM.FlightChargeTotal,0)
               ,'Y' CharVal
               ,'<B>'+' HRS'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 10 Row_Id,InfoDesc=''
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.DiscountAmtTotal,0)
               ,'Z' CharVal
               ,'<B>'+'DISCOUNT:'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
		
UNION ALL

SELECT DISTINCT 11 Row_Id,InfoDesc=''
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.TaxesTotal,0)
               ,'Z' CharVal
               ,'<B>'+'TAX:'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		
		
UNION ALL

SELECT DISTINCT 12 Row_Id,InfoDesc=''
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.QuoteTotal,0)
               ,'Z' CharVal
               ,'<B>'+'TOTAL:'+'</B>' ConcatStr
               ,CQF.FileNUM FileNumber
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
	    WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND CM.QuoteNUM = @QuoteNUM
		


)Quote
END

SELECT Quote.*,ACTUAL_FEES='_____________________________________________' , CASE WHEN Row_Id=4 THEN 1 
                                                          WHEN Row_Id=5 THEN 1 
                                                          WHEN Row_Id=6 THEN 1 ELSE @TenToMin END TenToMin FROM 
(
SELECT 9 Row_Id,'' InfoDesc,NULL InfoDescValue,SUM(Quotation) Quotation,'<B>'+'SUBTOTAL:'+'</B>'  ConcatStr ,FileNumber FROM @QuoteTotal WHERE CharVal='Y' GROUP BY  CharVal,FileNumber

UNION 

SELECT Row_Id,InfoDesc,InfoDescValue,Quotation,ConcatStr,FileNumber FROM @QuoteTotal
)Quote
ORDER BY Row_Id

--EXEC spGetReportCQQuoteLogSubInformation 10112,'chip_112','22785','1'	  




GO


