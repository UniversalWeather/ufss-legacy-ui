IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQPaxProfileExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQPaxProfileExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCQPaxProfileExportInformation]  
(  
     @UserCD VARCHAR(30),      
              @UserCustomerID VARCHAR(30),      
              @FileNumber VARCHAR(200)='',      
              @QuoteNUM VARCHAR(200)='',      
              @LegNUM VARCHAR(200)='',      
              @BeginDate DATETIME=NULL,      
              @EndDate DATETIME=NULL,      
              @TailNum VARCHAR(200)='',      
              @VendorCD VARCHAR(200)='',      
              @HomeBaseCD VARCHAR(200)='',      
              @AircraftCD VARCHAR(200)='',      
              @CQCustomerCD VARCHAR(30)='',      
              @SalesPersonCD VARCHAR(200)=''   
)  
AS  
-- ==========================================================      
-- SPC Name:spGetReportCQPaxProfileExportInformation       
-- Author: Aishwarya      
-- Create date: 12 June 2013      
-- Description: Get Report Charter Quote Pax Profile      
-- Revision History      
-- Date  Name  Ver  Change      
--       
-- ==========================================================  
BEGIN 
SELECT @QuoteNUM = CASE WHEN @QuoteNUM = '' OR (@BeginDate IS NOT NULL )THEN 1 ELSE @QuoteNUM END


 IF @BeginDate IS NOT NULL 
 BEGIN 
 
  ;With TripInfo AS(
 SELECT FileTemp.* FROM(
  SELECT DISTINCT FileNumber = CF.FileNUM 
  ,QuoteNumber = CM.QuoteNUM 
  ,lowdate=CLL.DepartureDTTMLocal
  ,highdate=CLL.ArrivalDTTMLocal  
  ,TailNumber = F.TailNum 
  ,AircraftType  = A.AircraftCD 
  ,CQDescription = CF.CQFileDescription 
  ,CustomerName = CQC.CQCustomerName 
  ,PaxLastName = ISNULL(P.LastName,'')
  ,PaxFirstName = ISNULL(P.FirstName,'')   
  ,PaxCode = P.PassengerRequestorCD 
  ,PaxNotes = P.Notes
 ,PA.AdditionalINFOCD
 ,PA.AdditionalINFODescription
 ,AdditionalINFOValue=PA.AdditionalINFOValue
 ,QuoteID=CM.CQMainID
 ,PaxName2=''
 ,Notes=CL.Notes
 ,P.PassengerRequestorID
 ,DENSE_RANK()OVER(ORDER BY CLL.DepartureDTTMLocal,CLL.ArrivalDTTMLocal,CF.FileNUM) Rnk
 
  FROM CQFile CF  
   INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID  AND CM.IsDeleted=0
   INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID  AND CL.IsDeleted=0
     INNER JOIN (SELECT  MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CQMainID) CLL ON CLL.CQMainID = CL.CQMainID
   INNER JOIN Fleet F ON CM.FleetID = F.FleetID 
   INNER JOIN CQPassenger CP ON CP.CQLegID = CL.CQLegID 
   INNER JOIN Passenger P ON CP.PassengerRequestorID = P.PassengerRequestorID 
   INNER JOIN Company C ON C.HomebaseID=F.HomebaseID   
   LEFT OUTER JOIN CQCustomer CQC ON CF.CQCustomerID = CQC.CQCustomerID 
   LEFT OUTER JOIN PassengerAdditionalInfo PA ON P.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted = 0
   LEFT OUTER JOIN Aircraft A ON F.AircraftID = A.AircraftID 
   LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID  
   LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID 
   LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID  
   WHERE 
 ( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )     
    AND ( V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )     
    AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )     
    AND ( A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )     
    AND ( SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )     
    AND CF.EstDepartureDT BETWEEN @BeginDate AND @EndDate     
    AND CF.CustomerID = CONVERT(BIGINT,@UserCustomerID)      
    AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')     
    AND CM.QuoteNUM = @QuoteNUM
    )FileTemp
    WHERE Rnk=1
    )
 
SELECT DISTINCT  FileNumber
				 ,QuoteNumber
  				 ,lowdate
				 ,highdate
				 ,TailNumber
				 ,AircraftType
				 ,CQDescription 
				 ,CustomerName 
				 ,PaxLastName
				 ,PaxFirstName
				 ,PaxCode
				 ,PaxNotes
				 ,PA2.AdditionalCD
				 ,PA2.PaxInfoDesc
				 ,PA2.PaxInfoValue
				 ,QuoteID
				 ,PaxName2=''
				 ,Notes FROM TripInfo TI 
							 LEFT OUTER JOIN (SELECT DISTINCT PA1.PassengerRequestorID,AdditionalCD=(
																							SELECT DISTINCT AdditionalINFOCD +' ' FROM TripInfo CP 
																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																							)
																			,PaxInfoDesc=(
																							SELECT DISTINCT AdditionalINFODescription +' ' FROM TripInfo CP 

																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																						  )
																			,PaxInfoValue=(
																							SELECT DISTINCT AdditionalINFOValue +' ' FROM TripInfo CP 
																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																							)FROM TripInfo PA1
								)PA2 ON PA2.PassengerRequestorID=TI.PassengerRequestorID
ORDER BY lowdate,highdate,FileNumber
 
 END
 
 ELSE
 
 BEGIN
 
  ;With TripInfo AS(
  SELECT DISTINCT FileNumber = CF.FileNUM 
				 ,QuoteNumber = CM.QuoteNUM 
  				 ,lowdate=CLL.DepartureDTTMLocal
				 ,highdate=CLL.ArrivalDTTMLocal  
				 ,TailNumber = F.TailNum 
				 ,AircraftType  = A.AircraftCD 
				 ,CQDescription = CF.CQFileDescription 
				 ,CustomerName = CQC.CQCustomerName 
				 ,PaxLastName = ISNULL(P.LastName,'')
				 ,PaxFirstName = ISNULL(P.FirstName,'')
				 ,PaxCode = P.PassengerRequestorCD 
				 ,PaxNotes = P.Notes
				 ,PA.AdditionalINFOCD
				 ,PA.AdditionalINFODescription
				 ,AdditionalINFOValue=PA.AdditionalINFOValue
				 ,QuoteID=CM.CQMainID
				 ,PaxName2=''
				 ,Notes=CL.Notes
				 ,P.PassengerRequestorID
  FROM CQFile CF  
   INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
   INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0
   INNER JOIN (SELECT  MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CQMainID) CLL ON CLL.CQMainID = CL.CQMainID
   INNER JOIN Fleet F ON CM.FleetID = F.FleetID 
   INNER JOIN CQPassenger CP ON CP.CQLegID = CL.CQLegID 
   INNER JOIN Passenger P ON CP.PassengerRequestorID = P.PassengerRequestorID   
   LEFT OUTER JOIN PassengerAdditionalInfo PA ON P.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted = 0
   LEFT OUTER JOIN CQCustomer CQC ON CF.CQCustomerID = CQC.CQCustomerID 
   LEFT OUTER JOIN Aircraft A ON F.AircraftID = A.AircraftID 
   LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID  
   LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID
  
   WHERE(CF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )     
     AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )     
     AND (CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' )       
     AND CF.CustomerID = CONVERT(BIGINT,@UserCustomerID)  

 )   
 

 
SELECT DISTINCT  FileNumber
				 ,QuoteNumber
  				 ,lowdate
				 ,highdate
				 ,TailNumber
				 ,AircraftType
				 ,CQDescription 
				 ,CustomerName 
				 ,PaxLastName
				 ,PaxFirstName
				 ,PaxCode
				 ,PaxNotes
				 ,PA2.AdditionalCD
				 ,PA2.PaxInfoDesc
				 ,PA2.PaxInfoValue
				 ,QuoteID
				 ,PaxName2=''
				 ,Notes FROM TripInfo TI 
							 LEFT OUTER JOIN (SELECT DISTINCT PA1.PassengerRequestorID,AdditionalCD=(
																							SELECT DISTINCT AdditionalINFOCD +' ' FROM TripInfo CP 
																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																							)
																			,PaxInfoDesc=(
																							SELECT DISTINCT AdditionalINFODescription +' ' FROM TripInfo CP 

																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																						  )
																			,PaxInfoValue=(
																							SELECT DISTINCT AdditionalINFOValue +' ' FROM TripInfo CP 
																									WHERE CP.PassengerRequestorID=PA1.PassengerRequestorID
																									FOR XML PATH('')
																							)FROM TripInfo PA1
								)PA2 ON PA2.PassengerRequestorID=TI.PassengerRequestorID
ORDER BY lowdate,highdate,FileNumber
 
 END
  
END  
 
  --SELECT * FROM CQFile WHERE FileNUM=16094
 
  --SELECT * FROM CQMain WHERE CQFileID=1011218741
 
  --SELECT * FROM CQLeg WHERE CQMainID=1011247245
 
  --SELECT * FROM CQPassenger WHERE CQLegID=10112395583
 
 
--EXEC spGetReportCQPaxProfileExportInformation 'chip_112','10112','16094','','','','','','','','','',''
GO


