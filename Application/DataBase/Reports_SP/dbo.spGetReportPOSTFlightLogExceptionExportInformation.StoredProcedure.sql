IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogExceptionExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogExceptionExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
CREATE   procedure  [dbo].[spGetReportPOSTFlightLogExceptionExportInformation] 
(
@UserCD VARCHAR(30)
,@DATEFROM AS DATETIME
,@DATETO AS DATETIME
)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTFlightLogExceptionInformation
-- Author: PremPrakash J
-- Create date: 31 Jul 2012
-- Description: Get Postflight Exception for the Report
-- Revision History
-- Date                 Name        Ver         Change
-- 28-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
SELECT DISTINCT
[tail_nmbr] = F.TailNum
,[lognum] = POM.LogNum
,[estdepdt] = POL.ScheduledTM
,[exception] = POM.IsException
--,[Leg No.] = POL.LegNUM
--,[Exceptions/Dep] = CONVERT (VARCHAR, ADeprt.IcaoID)
--,[Exceptions/Arr] = CONVERT (VARCHAR, AArr.IcaoID)
--,[Exceptions/Scheduled] = POL.ScheduledTM
,[Exceptions/Warning] = EXC.EXCEPTION
--,PFTE.ExceptionDescription
--,pom.EstDepartureDT
FROM Fleet F
left outer JOIN PostflightMain POM ON POM.FleetID = F.FleetID AND POM.IsDeleted=0
INNER JOIN PostflightLeg POL  ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
INNER JOIN PostflightTripException PFTE ON PFTE.POLogID = POL.POLogID  
INNER JOIN Airport ADeprt ON ADeprt.AirportID = POL.DepartICAOID
INNER JOIN Airport AArr ON AArr.AirportID = POL.ArriveICAOID  

LEFT OUTER JOIN (SELECT DISTINCT PFTE2.POLogID, EXCEPTION = 
					(SELECT ISNULL(ExceptionDescription,'') +'$$' 
					 FROM PostflightTripException PFTE1 
						INNER JOIN PostflightMain POM2 ON POM2.POLogID = PFTE1.POLogID AND POM2.IsDeleted=0
					 WHERE PFTE2.POLogID=PFTE1.POLogID
					 ORDER BY PFTE1.ExceptionDescription
                     FOR XML PATH('')
                     )
                     FROM PostflightTripException PFTE2 
				)EXC ON POM.POLogID = EXC.POLogID 

WHERE POM.CustomerID =dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND POL.LegNUM=1
ORDER BY [tail_nmbr], [lognum]
--EXEC spGetReportPOSTFlightLogExceptionExportInformation 'supervisor_99','1/1/2000','1/1/2010'



GO


