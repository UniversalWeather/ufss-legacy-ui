IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAircraftDutyExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAircraftDutyExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportAircraftDutyExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportAircraftDutyExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get the Aircraft Duty information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 

-- =============================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT 
		[code] = AircraftDutyCD
		,[desc] = AircraftDutyDescription
		,[client] = C.ClientCD
		,[graphcolor] = GraphColor
		,[lastuser] = LastUpdUID
	--	,[lastupdt] = CONVERT(varchar(10), LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[lastupdt] =  LastUpdTS
		,[custcolor] = ForeGrndCustomColor
		,[bcustcolor] = BackgroundCustomColor
		,[calentry] = IsCalendarEntry
		,[dfltbegtm] = DefaultStartTM
		,[dfltendtm] = DefualtEndTM
		,[standby] = IsAircraftStandby
	FROM [AircraftDuty] A
		LEFT OUTER JOIN (
			SELECT ClientID, ClientCD FROM Client
		) C ON A.ClientID = C.ClientID
	WHERE IsDeleted = 0
		AND CustomerID = @CUSTOMERID
		AND ( A.ClientID = @CLIENTID OR @CLIENTID IS NULL )
-- EXEC spGetReportAircraftDutyExportInformation 'TIM'
	ORDER BY AircraftDutyCD	
GO


