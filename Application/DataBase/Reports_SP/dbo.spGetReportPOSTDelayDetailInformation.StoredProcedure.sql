IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDelayDetailInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDelayDetailInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE  [dbo].[spGetReportPOSTDelayDetailInformation] 
(
        @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(1000) = '',
		@FleetGroupCD NVARCHAR(1000) = '',
		@LogNum NVARCHAR(1000) = '',
		@IsHomebase BIT = 0
		)
AS
 
 

-- ===============================================================================
-- SPC Name: [spGetReportPOSTDelayDetailInformation]
-- Author: Akhila
-- Create date: 01 August
-- Description: Get POST Delay Detail
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================


DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
														AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
					
BEGIN
DECLARE @TAIL TABLE(
    [DateRange]	VARCHAR(100), 	
	[Tail No]   VARCHAR(9),
	[AIRCRAFT] VARCHAR(10), 
    [Log No] BIGINT,
	[Leg]  VARCHAR(40),
	[LegA]  VARCHAR(40),
	[LegAA] VARCHAR(40),
	[Delay Code] CHAR(2), 
    [Delay Type] VARCHAR(100),
	[No Of Legs] INT,
	[Delay Time] NUMERIC(7,3),
	[Average Delay] NUMERIC(7,3),
	[TenToMin] INT,
	[FlightDate] DATE)
	 

-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN 

INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
END
-------------------------------TailNum and Fleet Group Filteration----------------------
	 
INSERT INTO @TAIL

SELECT DISTINCT 
     [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),	 	
	 [Tail No] = F.TailNum, 
	 [AIRCRAFT] = AC.AircraftCD, 
	 [Log No] = PODET.[Log No],
	 [Leg] = PODET.Leg,
	 [LegA] = PODET.LegA,
	 [LegAA] = PODET.LegAA,
	 [Delay Code] = PODET.[Delay Code], 
	 [Delay Type] = PODET.[Delay Type],
	 [No Of Legs]= PODET.[No Of Legs],
	 [Delay Time] = PODET.[Delay Time],
	 [Average Delay]= PODET.[Average Delay],
	 [TenToMin] = @TenToMin,
	 [FlightDate] = PODET.FlightDate
FROM 
(
	SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger ,F.AircraftID
	FROM Fleet F 
	left outer JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	left outer JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
) F
INNER JOIN
( 
	SELECT 
	POM.FleetID,
	POM.CustomerID,
	[Log No] = POM.LogNum,
	[Leg] = A.IcaoID +'  '+AA.IcaoID,
	[LegA] = A.IcaoID,
	[LegAA] = AA.IcaoID,
	[Delay Code] = DT.DelayTypeCD, 
	[Delay Type] = DT.DelayTypeDescription,
	[No Of Legs]= ISNULL(COUNT(POL.POLegID),0),
	[Delay Time] = ISNULL(POL.DelayTM ,0),
	[Average Delay]=  ISNULL(POL.DelayTM / COUNT(POL.POLegID),0),
	[TenToMin] = @TenToMin,
	[FlightDate] = POM.EstDepartureDT 
FROM [PostflightLeg] POL
	INNER JOIN PostflightMain POM ON POM.POLogID = POL.POLogID AND POM.IsDeleted = 0
	INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
	INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
	INNER JOIN DelayType DT ON DT.DelayTypeID =POL.DelayTypeID
WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	--AND POM.LogNum in(SELECT distinct LogNum  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID  
	--	WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND POL.IsDeleted = 0
GROUP BY POM.FleetID, POM.CustomerID, POM.EstDepartureDT, POL.ScheduledTM,  POM.LogNum, A.IcaoID, AA.IcaoID, DT.DelayTypeCD,
		 DT.DelayTypeDescription, POL.POLegID, POL.DelayTM  
) PODET on PODET.FleetID = F.FleetID AND PODET.CustomerID = F.CustomerID
	LEFT JOIN Company COM ON COM.HomebaseID = F.HomebaseID	
	LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	INNER JOIN #TempFleetID T1 ON F.FleetID=T1.FleetID
 
WHERE  ISNULL([Delay Type],'' )=(case when @TailNum = '' then [Delay Type]  else ISNULL([Delay Type],'' ) end)
ORDER BY PODET.LegA, PODET.LegAA, AC.AircraftCD, F.TailNum, PODET.[Log No], PODET.[Delay Type]	
	
IF @FleetGroupCD <> ''  AND @TailNum <> ''
	BEGIN
		INSERT INTO @TAIL(DateRange,[Tail No],[TenToMin])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,@TenToMin FROM Fleet 
		LEFT OUTER JOIN @TAIL T ON Fleet.TailNum=T.[Tail No]
					   WHERE TailNum NOT IN(SELECT [Tail No]  FROM @TAIL)
						 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
		UNION ALL
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet
					   WHERE TailNum NOT IN(SELECT [Tail No]  FROM @TAIL)
						 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
						AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END 
ELSE IF @FleetGroupCD <> ''  
	BEGIN
		INSERT INTO @TAIL(DateRange,[Tail No],[TenToMin])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [Tail No]  FROM @TAIL)
						 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
	END  
ELSE IF @TailNum <> '' --OR @FleetGroupCD <> ''
	BEGIN
		INSERT INTO @TAIL(DateRange,[Tail No],[TenToMin])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [Tail No]  FROM @TAIL)
						 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
						 AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END
SELECT DISTINCT * FROM @TAIL 
END
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID 
--EXEC spGetReportPOSTDelayDetailInformation 'jwilliams_13','2000-01-01','2012-12-12','','','',0




GO


