IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDelayDetailExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDelayDetailExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure  [dbo].[spGetReportPOSTDelayDetailExportInformation] 
	(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(1000) = '',
		@FleetGroupCD NVARCHAR(1000) = '',
		@LogNum NVARCHAR(1000) = '',
		@IsHomebase BIT = 0
	)
AS
-- ===============================================================================
-- SPC Name: [spGetReportPOSTDelayDetailExportInformation]
-- Author: Avanikanta Pradhan
-- Create date: 01 August
-- Description: Get POST flight Delay Details Export
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
DECLARE @CustomerID BIGINT = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)));
--DROP TABLE #DELAYLOG
--DROP TABLE #TempFleetID
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
(   
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT
)
  
IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CustomerID
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN 

INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CustomerID
END
-------------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE #DELAYLOG
(
	[DateRange] NVARCHAR(100)
	,[tail_nmbr] VARCHAR(9)
	,[lognum] BIGINT
	,[POLegID] BIGINT
	,[ac_code] CHAR(3)
	,[delaytype] CHAR(2)
	,[depicao_id] CHAR(4)
	,[arricao_id] CHAR(4)
	,[descrip] NVARCHAR(100)
	,[leg_cnt] BIGINT
	,[tot_line] NVARCHAR(10)
	,[delaytime] NUMERIC(6,3)
)
INSERT INTO #DELAYLOG
SELECT DISTINCT 
	[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),	       
	[tail_nmbr] = F.TailNUM,
	[lognum] = DEL.lognum,
	[POLegID] = DEL.[POLegID],
	[ac_code] = F.AircraftCD,     
	[delaytype]= DEL.delaytype,
	[depicao_id]  = DEL.depicao_id,
	[arricao_id] = DEL.arricao_id,
	[descrip] = DEL.descrip, 
	[leg_cnt] = DEL.leg_cnt,
	[tot_line] = '',
	[delaytime] = DEL.delaytime 
FROM 
(
	SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger ,F.AircraftID
	FROM Fleet F 
	left outer JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	left outer JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = @CustomerID
) F
INNER JOIN
	( 
	SELECT  
		POM.FleetID,
		POM.CustomerID,		       
		[lognum] = POM.LogNum,
		[POLegID] = POL.POLegID,
		[delaytype]= DT.DelayTypeCD,
		[depicao_id]  = A.IcaoID,
		[arricao_id] = AA.IcaoID,
		[descrip] = DT.DelayTypeDescription, 
		[leg_cnt] = COUNT(POL.POLegID),
		[delaytime] = POL.DelayTM 
	FROM [PostflightLeg] POL
		INNER JOIN PostflightMain POM ON POM.POLogID = POL.POLogID AND POM.IsDeleted=0
		INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
		INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
		INNER JOIN DelayType DT ON DT.DelayTypeID =POL.DelayTypeID
	WHERE POM.CustomerID = @CustomerID
	    AND POL.IsDeleted=0
		AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
		AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
		AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	GROUP BY POM.FleetID, POM.CustomerID, POM.LogNum, DT.DelayTypeCD, A.IcaoID, AA.IcaoID, DT.DelayTypeDescription, POL.POLegID, POL.DelayTM 
	)DEL on DEL.FleetID = F.FleetID AND DEL.CustomerID = F.CustomerID
		LEFT JOIN Company COM ON COM.HomebaseID = F.HomebaseID	
		LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
		INNER JOIN #TempFleetID T1 ON F.FleetID=T1.FleetID
 
WHERE  ISNULL([delaytype],'' )=(case when @TailNum = '' then [delaytype]  else ISNULL([delaytype],'' ) end)
--ORDER BY del.depicao_id, DEL.arricao_id, F.AircraftCD, F.TailNum, DEL.lognum, DEL.delaytype	
	
IF @FleetGroupCD <> ''  AND @TailNum <> ''
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
		LEFT OUTER JOIN #DELAYLOG T ON Fleet.TailNum=T.[tail_nmbr]
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  
																F.CustomerID=@CustomerID
																AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
		UNION ALL
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END 
ELSE IF @FleetGroupCD <> ''  
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  
																F.CustomerID=@CustomerID
																AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
	END  
ELSE IF @TailNum <> '' --OR @FleetGroupCD <> ''
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END

SELECT DISTINCT * FROM #DELAYLOG
ORDER BY tail_nmbr, lognum, POLegID, delaytype, depicao_id, arricao_id

--EXEC spGetReportPOSTDelayDetailExportInformation 'SUPERVISOR_99','2000-01-01','2020-12-12','','','',0




GO


