IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrencyExportInformationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrencyExportInformationSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrencyExportInformationSub]          
(      
 @AsOf DateTime, --Mandatory       
 @CrewCD Varchar(5),--Mandatory           
 @AircraftCD Varchar(10)--Mandatory       
 )        
AS        
  -- EXEC spGetReportPOSTCrewCurrencyExportInformationSub '2009-1-1','',''
-- ===============================================================================        
-- SPC Name: spGetReportPOSTCrewCurrencyInformationSub        
-- Author:  Mathes        
-- Create date: 30 Aug 2012        
-- Description: Get Crew Currency for REPORTS        
-- Revision History        
-- Date   Name  Ver  Change        
--         
-- ================================================================================        
Declare @StartDate DateTime         
Set @StartDate= @AsOf -30        
        
Declare @NightTODate DateTime        
Declare @NightLandDate DateTime        
Declare @InstDate DateTime          
    
SET NOCOUNT ON   BEGIN        
 Set   @NightTODate = (    
           SELECT TOP 1 PostflightLeg.ScheduledTM      
           FROM       PostflightCrew INNER JOIN    
                      PostflightLeg ON PostflightCrew.POLegID = PostflightLeg.POLegID INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      Fleet ON Crew.CrewID = Fleet.CrewID INNER JOIN    
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID        
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightCrew.TakeOffNight>0 And      
                      PostflightLeg.IsDeleted=0 And  
                      PostflightCrew.TakeOffNight Between @StartDate And @AsOf )        
             
   IF @NightTODate is null        
    BEGIN        
    Set    @NightLandDate = (    
           SELECT     TOP (1) PostflightLeg.ScheduledTM    
           FROM       PostflightLeg INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      PostflightSimulatorLog ON Crew.CrewID = PostflightSimulatorLog.CrewID INNER JOIN    
                      Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID    
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightSimulatorLog.TakeOffNight>0 And 
                      PostflightLeg.IsDeleted=0 And       
                      PostflightSimulatorLog.TakeOffNight Between @StartDate And @AsOf )        
   END            
           
   Set    @NightLandDate = (    
           SELECT TOP 1 PostflightLeg.ScheduledTM      
           FROM       PostflightCrew INNER JOIN    
                      PostflightLeg ON PostflightCrew.POLegID = PostflightLeg.POLegID INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      Fleet ON Crew.CrewID = Fleet.CrewID INNER JOIN    
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID        
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightCrew.LandingNight >0 And  
                      PostflightLeg.IsDeleted=0  And      
                      PostflightCrew.LandingNight Between @StartDate And @AsOf )        
             
   IF @NightLandDate is null        
   BEGIN        
    Set    @NightTODate = (    
           SELECT     TOP (1) PostflightLeg.ScheduledTM    
           FROM       PostflightLeg INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      PostflightSimulatorLog ON Crew.CrewID = PostflightSimulatorLog.CrewID INNER JOIN    
                      Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID    
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightSimulatorLog.LandingNight>0 And  
                      PostflightLeg.IsDeleted=0 And      
                      PostflightSimulatorLog.LandingNight Between @StartDate And @AsOf )        
   END            
           
    Set    @InstDate = (    
           SELECT TOP 1 PostflightLeg.ScheduledTM      
           FROM       PostflightCrew INNER JOIN   
                      PostflightLeg ON PostflightCrew.POLegID = PostflightLeg.POLegID INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      Fleet ON Crew.CrewID = Fleet.CrewID INNER JOIN    
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID        
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightCrew.Instrument >0 And 
                      PostflightLeg.IsDeleted=0 And       
                      PostflightCrew.Instrument Between @StartDate And @AsOf )        
             
   IF @InstDate is null        
   BEGIN        
    Set    @InstDate = (    
           SELECT     TOP (1) PostflightLeg.ScheduledTM    
           FROM       PostflightLeg INNER JOIN    
                      Crew ON PostflightLeg.CrewID = Crew.CrewID INNER JOIN    
                      PostflightSimulatorLog ON Crew.CrewID = PostflightSimulatorLog.CrewID INNER JOIN    
                      Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID    
           WHERE      Crew.CrewCD =@CrewCD And Aircraft.AircraftCD=@AircraftCD And        
                      PostflightSimulatorLog.Instrument>0 And    
                      PostflightLeg.IsDeleted=0 And    
                      PostflightSimulatorLog.Instrument Between @StartDate And @AsOf )        
   END            
                
                                
   Select @NightTODate As NightTO,@NightLandDate as NightLnd,@InstDate as Instrument     
                 
        
END

GO


