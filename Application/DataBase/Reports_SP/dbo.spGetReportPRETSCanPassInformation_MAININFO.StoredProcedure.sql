IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCanPassInformation_MAININFO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_MAININFO]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_MAININFO]
	 @UserCD AS VARCHAR(30)    
    ,@LegID AS VARCHAR(30)
AS
BEGIN

	-- =========================================================================
	-- SPC Name: spGetReportPRETSCanPassInformation
	-- Author: SINDHUJA.K
	-- Create date: 28 Sep 2012
	-- Description: Get Preflight CanPass Report Export information for REPORTS
	-- Revision History
	-- Date		Name		Ver		Change
	-- 
	-- ==========================================================================
SET NOCOUNT ON

	SELECT DISTINCT	
		[Name] = CO.CompanyName
		--,PL.LegID
		,[Telephone] = CO.HomebasePhoneNUM
		,[Fax] = CO.BaseFax
		,[AIRCRAFT_REGISTRATION] = F.TailNum
		,[AIRPORT_DESTINATION] = AA.IcaoID
		,[FBO] = FBD.FBOVendor
		,[DATE_OF_ARRIVAL] = PL.ArrivalDTTMLocal--(Date)Reports
		,[ETA_LOCAL_TIME] = PL.ArrivalDTTMLocal--(Time)Reports
		,[DEPARTURE_POINT_STATE] = AD.StateName
		,[MAKE_MODEL_YEAR_OF_AIRCRAFT] = F.TypeDescription + ' / ' + CONVERT(VARCHAR(10),FP.ManufactureYear)
		,[OWNER_OPERATOR] = FP.OwnerLesse
		,PL.LegNUM
		
         from  PreflightMain PM 
		 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		 INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
		 INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
		 INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
		 INNER JOIN Fleet F ON PM.FleetID = F.FleetID 
		 LEFT OUTER JOIN FleetPair FP ON F.FleetID = FP.FleetID 
		 LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID AND PFL.IsArrivalFBO = 1
		 LEFT OUTER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID
		 where PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		 AND PL.LegID = CONVERT(BIGINT, @LegID) 
		 ORDER BY PL.LegNUM
END		 
		 
		  --EXEC spGetReportPRETSCanPassInformation_MAININFO 'JWILLIMAS_11', '100115286'



GO


