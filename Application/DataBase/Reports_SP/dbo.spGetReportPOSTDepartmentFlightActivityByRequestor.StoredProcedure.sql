IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentFlightActivityByRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentFlightActivityByRequestor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentFlightActivityByRequestor]											
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @YEAR AS NVARCHAR(4),
    @TailNum AS VARCHAR(100) = '',
    @UserHomebaseID AS VARCHAR(30) = '',
    @DepartmentCD AS VARCHAR(70)= '',
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @IsHomebase AS BIT = 0
 AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentChargeBackDetailByAircraft
-- Author:  Vikram M
-- Create date: 31 Jul 2012
-- Description: Get FlightHours/BlockHours details based on Department and Tail Number

-- ================================================================================


BEGIN


DECLARE  @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		HomeBaseID BIGINT,
		TailNum VARCHAR(9)
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.HomebaseID,F.TailNum 
	FROM Fleet F
	WHERE F.CustomerID = @UserCustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID, F.HomebaseID,F.TailNum
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @UserCustomerID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.HomebaseID,F.TailNum
	FROM Fleet F 
	WHERE F.CustomerID = @UserCustomerID  
	AND F.IsDeleted=0
END
-------------------------------TailNum and Fleet Group Filteration----------------------	

Declare @SuppressActivityDept BIT  
 	
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

SET NOCOUNT ON;
--DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	   
 declare @AircraftBasis numeric(1,0);
	  SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		   
	   
DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 


-----Retrieving Department details based on @FromDate and @ToDate
DECLARE @DateTable TABLE (SeqID INT IDENTITY, WorkDate DATE,DepartmentCD VARCHAR(10),DepartmentName VARCHAR(60) );

-----Retrieving Fleet details based on @FromDate and @ToDate
DECLARE @FleetDate TABLE
        (FLEETID BIGINT,
        TailNum VARCHAR(9),
        HomebaseID BIGINT,
        WorkDate DATE,
        ScheduledMonth SMALLINT,
        ScheduledYear SMALLINT);
INSERT INTO @DateTable SELECT DISTINCT  DATE,D.DepartmentCD,d.DepartmentName FROM Department D CROSS JOIN  [dbo].[fnDateTable](@FromDate,@ToDate)
							        WHERE CustomerID=@UserCustomerID
									 AND IsDeleted=0
									 AND IsInActive=0
									AND  (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')

--select * from @DateTable
INSERT INTO @FleetDate
       (FLEETID,
        TailNum,
        HomebaseID,
        ScheduledMonth,
        ScheduledYear
       )
       SELECT DISTINCT
         FG1.FleetID,
         FG1.TailNum,
         FG1.HomebaseID,
         MONTH(DT.WorkDate),
         YEAR(DT.WorkDate)
        FROM @DateTable DT
        CROSS JOIN @TempFleetID FG1
        WHERE (FG1.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
        
----Retrieving leg details based on Tail Number and Department
SELECT FC.* INTO #FleetCalender FROM
       (SELECT DISTINCT
        FG.FleetID,
        CASE WHEN TH.OldTailNUM = FG.TailNum THEN TH.NewTailNUM ELSE FG.TailNum END AS [Tail_Number], 
        ISNULL(D.DepartmentCD,'ZZZZ') DepartmentCD,
        D.DepartmentCD + '-' + D.DepartmentName AS DepartmentCD1,
        DepartmentName = ISNULL(D.DepartmentName,'ZZZZ'),
        DATEPART(MM,PL.ScheduledTM) AS SCHEDULEDMONTH,
        DATEPART(yyyy,PL.ScheduledTM)AS SCHEDULEDYEAR,
        P.PassengerRequestorCD,
	    P.PassengerName,
        FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN 'Block Hours' ELSE 'Flight Hours' END,
         CR.ChargeUnit, CR.ChargeRate,PL.Distance,
        CASE WHEN @AircraftBasis= 1 THEN ROUND(PL.BlockHours,1)
             WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1)  END AS FlightBlockHours ,
        CASE WHEN CR.ChargeUnit = 'N' THEN  (PL.Distance*CR.ChargeRate)
			 WHEN CR.ChargeUnit = 'S' THEN ((PL.Distance*CR.ChargeRate)*1.15078)
			 WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN (ROUND(PL.BlockHours,1) *CR.ChargeRate)
			 WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN (ROUND(PL.FlightHours,1) *CR.ChargeRate)
			 END AS ChargeBack ,
       D.IsInActive,
       [TenToMin] = @TenToMin,
       MonthStart = @MonthStart,
	   MonthEnd = @MonthEnd,
	   PM.LogNum,
	   PL.LegNUM
      FROM PostflightMain PM
      JOIN PostflightLeg pl
        ON PM.POLogID = PL.POLogID AND PM.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
      JOIN Fleet FG
        ON FG.FleetID = PM.FleetID
      JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
      LEFT JOIN TailHistory TH 
        ON TH.OldTailNUM = FG.TailNum AND TH.CustomerID = FG.CustomerID
      LEFT JOIN Department D
        ON PL.DepartmentID = D.DepartmentID
      LEFT OUTER JOIN Passenger P
        ON PL.PassengerRequestorID = P.PassengerRequestorID
      --JOIN Company C
      --  ON FG.HomebaseID = C.HomebaseID
      LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
           ) CR ON PL.POLegID = CR.POLegID  
      WHERE (CONVERT(DATE,PL.ScheduledTM) BETWEEN  CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate))
        AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
        AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
        AND PL.CustomerID = @UserCustomerID
        AND PM.IsDeleted = 0
      ) FC



INSERT INTO #FleetCalender(DepartmentCD,DepartmentName,SCHEDULEDMONTH,SCHEDULEDYEAR,PassengerRequestorCD,PassengerName,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,[Tail_Number],FleetID)
----Inserting month for each Fleet
SELECT DISTINCT D.DepartmentCD,D.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),TF.PassengerRequestorCD,TF.PassengerName,@TenToMin,@MonthStart, @MonthEnd ,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END)
               ,TF.[Tail_Number],isnull(TF.FleetID,0)
               FROM Department D CROSS JOIN @DateTable T 
                    LEFT OUTER JOIN #FleetCalender TF ON TF.DepartmentCD=D.DepartmentCD
                   LEFT OUTER JOIN Passenger P ON D.DepartmentID = P.DepartmentID  
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentCD=T.DepartmentCD  AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH )

                           AND D.CustomerID=@UserCustomerID
                           AND D.IsDeleted=0
                           AND D.IsInActive=0

    
INSERT INTO #FleetCalender(DepartmentCD,DepartmentName,SCHEDULEDMONTH,SCHEDULEDYEAR,PassengerRequestorCD,PassengerName,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,[Tail_Number],FleetID)
----Inserting month for each Fleet
SELECT DISTINCT D.DepartmentCD,D.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),TF.PassengerRequestorCD,TF.PassengerName,@TenToMin,@MonthStart, @MonthEnd ,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END)
               ,TF.[Tail_Number],isnull(TF.FleetID,0)
               FROM Department D CROSS JOIN @DateTable T 
                    LEFT OUTER JOIN #FleetCalender TF ON TF.DepartmentCD=D.DepartmentCD
                   LEFT OUTER JOIN Passenger P ON D.DepartmentID = P.DepartmentID  
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentCD=T.DepartmentCD  AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH )

                           AND D.CustomerID=@UserCustomerID
                           AND D.IsDeleted=0
                           AND D.IsInActive=0 
 DECLARE @Count INT
    SELECT @Count=COUNT(*) FROM 
		 ( SELECT  DISTINCT DepartmentCD,Tail_Number CountVal 
		   FROM #FleetCalender
		   WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD=''))TEMP

 
 IF @SuppressActivityDept=0
BEGIN                       
SELECT
        FleetID,
        Tail_Number = ISNULL(Tail_Number, '$$$$'), 
        DepartmentCD,
        DepartmentCD1,
        DepartmentName,
        SCHEDULEDMONTH,
        SCHEDULEDYEAR,
        PassengerRequestorCD,
        PassengerName,
        FlightBlockHoursLabel,
        ChargeUnit, ChargeRate,Distance,
       FlightBlockHours,
       ChargeBack ,
       IsInActive,
       [TenToMin] ,
         MonthStart,
	   MonthEnd,
       @Count NoOfRecords
 FROM #FleetCalender
 WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')

  ORDER BY DepartmentCD,SCHEDULEDMONTH
 END
ELSE

BEGIN                       
SELECT
        FleetID,
        Tail_Number = ISNULL(Tail_Number, '$$$$'), 
        DepartmentCD,
        DepartmentCD1,
        DepartmentName,
        SCHEDULEDMONTH,
        SCHEDULEDYEAR,
        PassengerRequestorCD,
        PassengerName,
        FlightBlockHoursLabel,
        ChargeUnit, ChargeRate,Distance,
       FlightBlockHours,
       ChargeBack ,
       IsInActive,
       [TenToMin] ,
        MonthStart,
	    MonthEnd,
       @Count NoOfRecords
 FROM #FleetCalender
 WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
  AND (DepartmentCD IN (SELECT DepartmentCD FROM #FleetCalender WHERE ChargeUnit IS NOT NULL OR ChargeRate IS NOT NULL OR Distance IS NOT NULL OR FlightBlockHours IS NOT NULL OR ChargeBack IS NOT NULL))

  ORDER BY DepartmentCD,SCHEDULEDMONTH
 END
IF OBJECT_ID('tempdb..#FleetCalender') IS NOT NULL
DROP TABLE #FleetCalender 	

IF OBJECT_ID('tempdb..#DepartmentChargeBack') IS NOT NULL
DROP TABLE #DepartmentChargeBack	

END



--EXEC [spGetReportPOSTDepartmentChargeBackDetailByAircraft] 'jwilliams_13','10099','2010','','',0


GO


