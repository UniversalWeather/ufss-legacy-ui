IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelSummaryTailNumExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelSummaryTailNumExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPOSTFuelSummaryTailNumExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IcaoID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
		
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFuelSummaryTailNumExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Fuel Summary/Tail Number for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)

	SET @SQLSCRIPT = '
			SELECT distinct
			[fuel_loc] = FL.FuelLocatorCD,
			[desc]=FL.FuelLocatorDescription,
			[purchasedt] = PE.PurchaseDT,
			[icao_id] = A.IcaoID,
			[tail_nmbr] = F.TailNum,
		    [dispatchno] = PE.DispatchNUM,
			[pay_type] = PT.PaymentTypeCD,
			[paydesc]=PT.PaymentTypeDescription,
			[ac_code]=F.AircraftCD,
			[vendname] = V.Name,	
			[invnum] = PE.InvoiceNUM,
			[fuelqty] = (dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD)),--PE.FuelQTY,
			[fuelpurch] = pe.FuelPurchase,
			[pay_vendor] = V.VendorCD,	
		    [unitprice] =  Case WHEN C.FuelPurchase <> Pe.FuelPurchase THEN DBO.GetFuelPriceConversion(PE.FuelPurchase,Pe.UnitPrice,@UserCD)
                                               ELSE Pe.UnitPrice End,
			[expamt] = PE.ExpenseAMT
			
			
		FROM PostflightExpense PE 
		
		INNER JOIN  PostflightMain PM ON PM.POLogID = PE.POLogID c
	    LEFT OUTER JOIN  PaymentType PT ON PE.PaymentTypeID = PT.PaymentTypeID 
	    LEFT OUTER JOIN  Vendor V ON PE.PaymentVendorID = V.VendorID 
		LEFT OUTER JOIN  FuelLocator FL ON PE.FuelLocatorID = FL.FuelLocatorID 
		LEFT OUTER JOIN  Airport A ON PE.AirportID = A.AirportID 
		LEFT OUTER JOIN  Company c ON PE.HomeBaseID = C.HomeBaseID 
		INNER JOIN  Fleet F ON F.FleetID = PE.FleetID 
		
        LEFT OUTER JOIN FleetGroupOrder FGO 
					ON PE.FleetID = FGO.FleetID AND PE.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG
                   ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		
		WHERE PE.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				+ ' AND PE.PurchaseDT BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PE.IsDeleted=0	
	'
	
	--Construct OPTIONAL clauses
			
	IF @TailNUM <> '' BEGIN  
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END	
	IF @FleetGroupCD <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	END	
	IF @IcaoID <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@IcaoID, 1) = ',' THEN LEFT(@IcaoID, LEN(@IcaoID) - 1) ELSE @IcaoID END, ',', ''', ''') + ''')';
	END	
  SET @SQLSCRIPT = @SQLSCRIPT + 'order by tail_nmbr,fuel_loc,icao_id,purchasedt'

	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@DATEFROM AS DATETIME, @DATETO AS DATETIME,@UserCD AS VARCHAR(30)'
    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @DATEFROM, @DATETO,@UserCD 
    
    
  -- EXEC spGetReportPOSTFuelSummaryTailNumExportInformation 'jwilliams_13','2011/2/3','2012/12/12', '', '', ''

GO


