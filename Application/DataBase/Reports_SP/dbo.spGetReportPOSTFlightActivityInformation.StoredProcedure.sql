IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightActivityInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightActivityInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spGetReportPOSTFlightActivityInformation]
	@UserCD VARCHAR(30),--Mandatory
	@UserCustomerID VARCHAR(30),--Mandatory
	@DATEFROM DATETIME,--Mandatory
	@DATETO DATETIME,--Mandatory
	@TailNum VARCHAR(500) = '',
	@PassengerRequestorCD  AS VARCHAR(600) = '',
	@FlightPurposeCD AS VARCHAR(300)=''
   
AS

-- ===========================================================
--	SPC Name: spGetReportPOSTFlightActivityInformation
-- Author: Askar
-- Create date: 25 Sep 2013
-- Description: Get Color Legend information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 
--exec spGetReportPOSTFlightActivityInformation 'supervisor_99','10099','2013-01-01','2013-12-31'
-- ============================================================

SET NOCOUNT ON 

BEGIN

DECLARE @AircraftBasis NUMERIC(1,0);
SELECT @AircraftBasis = AircraftBasis FROM Company 
									  WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) 
								        AND HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)
								        
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company
						             WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) 
									   AND HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD)

DECLARE @FltPurpose TABLE(FltPurpose CHAR(2))
INSERT INTO @FltPurpose
SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightPurposeCD, ',')

IF RTRIM(ISNULL(@FlightPurposeCD,''))=''
BEGIN
INSERT INTO @FltPurpose
SELECT 'EB'
UNION ALL
SELECT 'GB'
UNION ALL
SELECT 'NB'
UNION ALL
SELECT 'EN'
UNION ALL
SELECT 'GN'
UNION ALL
SELECT 'EP'
UNION ALL
SELECT 'GP'
END


SELECT DISTINCT Date=ScheduledTM
	  ,TripNum=PM.DispatchNum
	  ,Leg=PL.LegNUM
	  ,Dept_Dest=D.IcaoID+' /'+A.IcaoID+'***'+D.CityName+' /'+A.CityName
	  ,Pilots=SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1)
	  ,Catg=DEP.DepartmentCD
	  ,Purpose=PL.FlightPurpose
	  ,Pax=PL.PassengerTotal
	  ,Business=SUBSTRING(FP.Passenger,1,LEN(FP.Passenger)-1)
	  ,NP_Business=SUBSTRING(FP1.Passenger,1,LEN(FP1.Passenger)-1)
	  ,N_Entertainment=SUBSTRING(FP2.Passenger,1,LEN(FP2.Passenger)-1)
	  ,Entertainment=SUBSTRING(FP3.Passenger,1,LEN(FP3.Passenger)-1)
	  ,flt_hrs=FLOOR((CASE @AircraftBasis WHEN 1 THEN ROUND(PL.BlockHours,1) WHEN 2 THEN ROUND(PL.FlightHours,1) END )*10)*0.1
	  ,NauticalMiles=PL.Distance
	  ,StatuteMiles=FLOOR(ROUND(PL.Distance*1.15078,0))
	  ,AircraftBasis=@AircraftBasis
	  ,DateRange = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	  ,@TenToMin TenToMin
	  ,F.TailNum
	  ,F.AircraftCD
	  ,Department=ISNULL(DEP.DepartmentCD,'-------')
	  ,DepartmentDesc=DEP.DepartmentName
	  ,'' CustomerName
	  ,'' customerAddress
	
 FROM PostflightMain PM
			  INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
			  INNER JOIN Fleet F ON F.FleetID=PM.FleetID
			  LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
			  LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
			  LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
			  LEFT OUTER JOIN (SELECT DISTINCT PC2.POLegID,CrewList = (SELECT C.CrewCD + ' *' FROM PostflightCrew PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.POLegID = PC2.POLegID 
																			  AND PC1.DutyTYPE IN ('P','S') 
																			  ORDER BY PC1.DutyTYPE     
																			 FOR XML PATH('') 
																		 ) FROM PostflightCrew PC2         
	                              ) Crew ON PL.POLegID = Crew.POLegID
	         LEFT OUTER JOIN (SELECT DISTINCT  PP2.POLegID,Passenger=(
										SELECT ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'')+' *' FROM PostflightPassenger PP 
																		 INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID
																		 INNER JOIN FlightPurpose FP ON FP.FlightPurposeID=PP.FlightPurposeID
																		 INNER JOIN @FltPurpose FPE ON FPE.FltPurpose=FP.FlightPurposeCD
												  WHERE FlightPurposeCD IN('EB','GB')
												   AND PP.POLegID=PP2.POLegID
												   ORDER BY PP.OrderNUM
												   FOR XML PATH('') 
									)FROM PostflightPassenger PP2)FP ON FP.POLegID=PL.POLegID
	         LEFT OUTER JOIN (SELECT DISTINCT  PP2.POLegID,Passenger=(
										SELECT ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'')+' *' FROM PostflightPassenger PP 
																		 INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID
																		 INNER JOIN FlightPurpose FP ON FP.FlightPurposeID=PP.FlightPurposeID
																		 INNER JOIN @FltPurpose FPE ON FPE.FltPurpose=FP.FlightPurposeCD
												  WHERE FlightPurposeCD IN('NB')
												   AND PP.POLegID=PP2.POLegID
												   ORDER BY PP.OrderNUM
												   FOR XML PATH('') 
									)FROM PostflightPassenger PP2)FP1 ON FP1.POLegID=PL.POLegID
	         LEFT OUTER JOIN (SELECT DISTINCT  PP2.POLegID,Passenger=(
										SELECT ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'')+' *' FROM PostflightPassenger PP 
																		 INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID
																		 INNER JOIN FlightPurpose FP ON FP.FlightPurposeID=PP.FlightPurposeID
																		 INNER JOIN @FltPurpose FPE ON FPE.FltPurpose=FP.FlightPurposeCD
												  WHERE FlightPurposeCD IN('EN','GN')
												   AND PP.POLegID=PP2.POLegID
												   ORDER BY PP.OrderNUM
												   FOR XML PATH('') 
									)FROM PostflightPassenger PP2)FP2 ON FP2.POLegID=PL.POLegID
	         LEFT OUTER JOIN (SELECT DISTINCT  PP2.POLegID,Passenger=(
										SELECT ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'')+' *' FROM PostflightPassenger PP 
																		 INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID
																		 INNER JOIN FlightPurpose FP ON FP.FlightPurposeID=PP.FlightPurposeID
																		 INNER JOIN @FltPurpose FPE ON FPE.FltPurpose=FP.FlightPurposeCD
												  WHERE FlightPurposeCD IN('EP','GP')
												   AND PP.POLegID=PP2.POLegID
												   ORDER BY PP.OrderNUM
												   FOR XML PATH('') 
									)FROM PostflightPassenger PP2)FP3 ON FP3.POLegID=PL.POLegID
			LEFT OUTER JOIN PostflightPassenger PP ON PP.POLegID=PL.POLegID
			LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID
			LEFT OUTER JOIN FlightPurpose FPP ON FPP.FlightPurposeID=PP.FlightPurposeID
 WHERE PM.CustomerID=@UserCustomerID
   AND PM.IsDeleted=0
   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
   AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum='')
   AND (P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @PassengerRequestorCD='')
   AND (FPP.FlightPurposeCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightPurposeCD, ',')) OR @FlightPurposeCD = '')
   ORDER BY F.AircraftCD,ScheduledTM

	     
END







GO


