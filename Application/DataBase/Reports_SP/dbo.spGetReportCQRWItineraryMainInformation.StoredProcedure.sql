IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWItineraryMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWItineraryMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




    
CREATE PROCEDURE [dbo].[spGetReportCQRWItineraryMainInformation](    
 @UserCustomerID     AS VARCHAR(30)      
 ,@UserHomebaseID    AS VARCHAR(30)      
 ,@FileNumber           AS VARCHAR(30)  = '' --      CQMain.FileNUM    
 ,@QuoteNUM          AS VARCHAR(100) = '' --      CQMain.QuoteNUM     
 ,@LegNUM            AS VARCHAR(100) = '' --      CQLeg.LegNUM    
    
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal    
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal     
 ,@TailNum           AS VARCHAR(30) = '' --     Single Selection, Fleet.TailNUM (Use FleetID of CQMain)    
 ,@AircraftCD        AS VARCHAR(30) = '' --     TypeCD, Single Selection     
 ,@VendorID   AS VARCHAR(30) = '' --     VendorCD, Single Selection    
 ,@CQCustomerID        AS VARCHAR(30) = '' --     CustomerCD, Single Selection    
 ,@HomeBaseCD        AS VARCHAR(30) = '' --     HomeBaseCD, Single Selection    
 ,@SalesPersonID     AS VARCHAR(30) = '' --     SalesPersonCD, Single Selection       
/*    
REPORT CRITERIA    
 Begin Date CQFile.estdept > = [Begin Date] AND [End Date] < = CQFile.estdept    
 End Date     
 Tail Number CQMain.Tail_Nmbr    
 Vendor CQMain.Vendcode    
 Home Base Cqfile.Homebase    
 Type Code CQMain.Type_Code    
 Customer CQFIle.Custcode    
 Sales Person CQFile.saleperid    
*/    
)     
AS    
BEGIN    

DECLARE @USERCD VARCHAR(30);
SET @USERCD = (SELECT TOP 1 USERNAME FROM UserMaster WHERE CustomerID = @UserCustomerID AND HomebaseID = @UserHomebaseID)

DECLARE @CQComanyImage VARBINARY(MAX) = NULL;  
SELECT @CQComanyImage = UWAFilePath FROM FileWarehouse   
   WHERE CustomerID = @UserCustomerID AND RecordID = @UserHomebaseID AND RecordType = 'CQCompany'  
  
 DECLARE @tblInfo AS TABLE (    
  CQFileID VARCHAR(30)     
  ,FileNUM INT    
  ,QuoteNUM INT    
  ,LegNUM INT    
  ,LegID VARCHAR(30)    
  ,TripStartDT VARCHAR(15)    
  ,TripEndDT VARCHAR(15)    
  ,TailNumber VARCHAR(10)    
  ,TypeCD VARCHAR(10)     
  ,FileDescription VARCHAR(100)    
  ,CQCustomerName VARCHAR(50)    
  ,Attention VARCHAR(50)    
  ,Phone VARCHAR(20)    
  ,Fax VARCHAR(20) 
  ,CQCompanyLogPos INT   
 )    
     
    IF @FileNumber = ''     
 BEGIN    
  INSERT INTO @tblInfo (CQFileID, FileNUM, QuoteNUM, LegNUM, LegID, TripStartDT, TripEndDT,     
   TailNumber, TypeCD, FileDescription, CQCustomerName, Attention, Phone, Fax, CQCompanyLogPos)    
  SELECT CQFileID = CONVERT(VARCHAR,QF.CQFileID), QF.FileNUM, QM.QuoteNUM, QL.LegNUM, CONVERT(BIGINT,QL.CQLegID),     
   --CONVERT(DATE,QL.DepartureDTTMLocal), CONVERT(DATE,QL.ArrivalDTTMLocal)
   dbo.GetShortDateFormatByUserCD(@UserCD,PLDate.DepartureDTTMLocal),
    dbo.GetShortDateFormatByUserCD(@UserCD, PLDate.ArrivalDTTMLocal), F.TailNum, AC.AircraftCD,     
   QF.CQFileDescription, QF.CQCustomerName, QF.CQCustomerContactName, ISNULL(QF.PhoneNUM,'')PhoneNUM, 
   ISNULL(QF.FaxNUM,'') FaxNUM,C.ReportWriteLogoPosition
  FROM CQFile QF     
  INNER JOIN CQMain QM ON QF.CQFileID = QM.CQFileID AND QM.IsDeleted=0    
  INNER JOIN CQLeg QL ON QM.CQMainID = QL.CQMainID  AND QL.IsDeleted=0 
	LEFT OUTER JOIN(
					SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CustomerID,CQMainID
					FROM CQLeg 
					GROUP BY CustomerID,CQMainID
		) PLDate ON  PLDate.CustomerID=QL.CustomerID AND PLDate.CQMainID=QL.CQMainID
  INNER JOIN Company C ON QF.CustomerID = C.CustomerID AND QF.HomebaseID = C.HomebaseID  
  LEFT OUTER JOIN ( SELECT FleetID, TailNum FROM Fleet ) F ON QM.FleetID = F.FleetID    
  LEFT OUTER JOIN ( SELECT AircraftID, AircraftCD FROM Aircraft ) AC ON QM.AircraftID = AC.AircraftID    
  
  WHERE QF.CustomerID = CONVERT(BIGINT,@UserCustomerID)    
   AND CONVERT(DATE, QF.EstDepartureDT, 101) BETWEEN CONVERT(DATE, @BeginDate, 101) AND CONVERT(DATE, @EndDate, 101)    
   --AND ( QM.FleetID = CONVERT(BIGINT,@FleetID) OR @FleetID = '' )    
   --AND ( QM.AircraftID = CONVERT(BIGINT,@AircraftID) OR @AircraftID = '' )       
   AND ( F.TailNum = @TailNum OR @TailNum = '' )    
   AND ( AC.AircraftCD = @AircraftCD OR @AircraftCD = '' )    
   AND ( QM.VendorID = CONVERT(BIGINT,@VendorID) OR @VendorID = '' )    
   AND ( QF.CQCustomerID = CONVERT(BIGINT,@CQCustomerID) OR @CQCustomerID = '' )    
   AND ( QM.HomebaseID = CONVERT(BIGINT,@HomeBaseCD) OR @HomeBaseCD = '' )    
   AND ( QF.SalesPersonID = CONVERT(BIGINT,@SalesPersonID) OR @SalesPersonID = '' )    
 END        
 ELSE    
    BEGIN    
  INSERT INTO @tblInfo (CQFileID, FileNUM, QuoteNUM, LegNUM, LegID, TripStartDT, TripEndDT,     
   TailNumber, TypeCD, FileDescription, CQCustomerName, Attention, Phone, Fax, CQCompanyLogPos)    
  SELECT CQFileID = CONVERT(VARCHAR,QF.CQFileID)
		, QF.FileNUM
		, QM.QuoteNUM
		, QL.LegNUM
		, CONVERT(BIGINT,QL.CQLegID)
		,  dbo.GetShortDateFormatByUserCD(@UserCD,PLDate.DepartureDTTMLocal)
		, dbo.GetShortDateFormatByUserCD(@UserCD,PLDate.ArrivalDTTMLocal)
		, F.TailNum
		, AC.AircraftCD
		, QF.CQFileDescription
		, QF.CQCustomerName
		, QF.CQCustomerContactName
		, ISNULL(QF.PhoneNUM,'') PhoneNUM
		, ISNULL(QF.FaxNUM,'') FaxNUM    
	   ,C.ReportWriteLogoPosition
  FROM CQFile QF     
  INNER JOIN CQMain QM ON QF.CQFileID = QM.CQFileID AND QM.IsDeleted=0    
  INNER JOIN CQLeg QL ON QM.CQMainID = QL.CQMainID AND QL.IsDeleted=0
  LEFT OUTER JOIN(
					SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CustomerID,CQMainID
					FROM CQLeg 
					GROUP BY CustomerID,CQMainID
		) PLDate ON  PLDate.CustomerID=QL.CustomerID AND PLDate.CQMainID=QL.CQMainID
  LEFT OUTER JOIN ( SELECT FleetID, TailNum FROM Fleet ) F ON QM.FleetID = F.FleetID    
  LEFT OUTER JOIN ( SELECT AircraftID, AircraftCD FROM Aircraft ) AC ON QM.AircraftID = AC.AircraftID    
  INNER JOIN Company C ON QF.CustomerID = C.CustomerID AND QF.HomebaseID = C.HomebaseID  
     
  WHERE QF.CustomerID = CONVERT(BIGINT,@UserCustomerID)    
   AND QF.FileNUM = CONVERT(INT,@FileNumber)    
   AND (QM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')    
   AND (QL.LegNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')     
 END    
    
 SELECT DISTINCT CQFileID 
			  ,FileNUM     
			  ,QuoteNUM     
			  ,LegNUM     
			  ,LegID 
			  ,TripStartDT 
			  ,TripEndDT 
			  ,TailNumber 
			  ,TypeCD 
			  ,FileDescription 
			  ,CQCustomerName 
			  ,Attention 
			  ,Phone 
			  ,Fax 
			  ,CQCompanyLogPos
			  ,CQComanyImage = @CQComanyImage FROM @tblInfo TI    
			ORDER BY FileNUM, QuoteNUM, LegNUM   
    
END    
        
  --  exec  spGetReportCQRWItineraryMainInformation '10112','0', '23057', ''     
     
     
    
    
    
    
    
    
    



GO


