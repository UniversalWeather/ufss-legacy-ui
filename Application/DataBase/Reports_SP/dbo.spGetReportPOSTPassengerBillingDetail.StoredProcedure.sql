IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerBillingDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerBillingDetail]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerBillingDetail]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID AS VARCHAR(30),
		@DATEFROM AS DATETIME , --Mandatory
		@DATETO AS DATETIME , --Mandatory
		@PassengerRequestorCD AS NVARCHAR(1000) = '', 
		@DepartmentCD AS NVARCHAR(1000) = '',
		@AuthorizationCD AS NVARCHAR(1000) = '',
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0,
		@PassengerGroupCD VARCHAR(500)='' 
        --@TenToMin SMALLINT = 0	
		)
AS
BEGIN
SET NOCOUNT ON
-- ===============================================================================
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

Declare @SuppressActivityPassenger BIT  
 	
SELECT @SuppressActivityPassenger=IsZeroSuppressActivityPassengerRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
										 
 Declare @AircraftBasis numeric(1,0);
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)


-----------------------------Passenger and Passenger Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
	AND P.IsActive = 1
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
	AND P.IsActive = 1
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
	AND P.IsActive = 1
END

-----------------------------Passenger and Passenger Group Filteration----------------------
	--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		
	
----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------
			
SELECT DISTINCT
		DATERANGE =  dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
       [Log_Number] = PM.LogNum
       ,PM.CustomerID  
       ,[Tail_Number] = F.TailNum
       ,[Disp_Number] = PM.DispatchNum
       ,D.DepartmentCD
       ,DA.AuthorizationCD
       ,[DEP_ICAO] = AD.IcaoID
       ,[ARR_ICAO] = AA.IcaoID 
       ,[TripDate] = CAST(PL.ScheduledTM AS DATE) 
       ,P.PassengerRequestorID
       ,[PassengerCode] = P.PassengerRequestorCD
       , ISNULL(LastName,'')+CASE WHEN FirstName<>'' OR FirstName IS NOT NULL THEN  ISNULL(', '+FirstName,'') ELSE '' END AS PassengerName
       ,[StandardBilling] = P.StandardBilling
       ,[Dept] = D.DepartmentCD
       ,[Auth] = DA.AuthorizationCD
       ,[Miles] = PL.Distance
       ,ROUND(PL.FlightHours,1) FlightHours
       ,ROUND(PL.BlockHours,1) BlockHours
       ,[Nbr_Pax] = PL.PassengerTotal
       ,F.MaximumPassenger
       ,[EmpSeats] = F.MaximumPassenger - PL.PassengerTotal 
       ,AC.AircraftTypeCD
       ,F.AircraftCD 
       ,CR.ChargeRate
       ,[Per] = CR.ChargeUnit
       /*,[Total Charges] = CASE WHEN C.AircraftBasis = 1 THEN
          (CASE WHEN FCR.ChargeUnit = 'H' THEN (((PL.BlockHours*FCR.ChargeRate)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
	            WHEN FCR.ChargeUnit = 'N' THEN (((PL.Distance*FCR.ChargeRate)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
	            WHEN FCR.ChargeUnit = 'S' THEN (((PL.Distance*FCR.ChargeRate*1.15078)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
           End)
	   ELSE
           (CASE WHEN FCR.ChargeUnit = 'H' THEN (((PL.FlightHours*FCR.ChargeRate)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
	             WHEN FCR.ChargeUnit = 'N' THEN (((PL.Distance*FCR.ChargeRate)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
	             WHEN FCR.ChargeUnit = 'S' THEN (((PL.Distance*FCR.ChargeRate*1.15078)/PL.PassengerTotal)+ISNULL(SUMExpenseAmt,0))
	        END)
	   END,*/
	   
	   ,/*[Total Charges] = (CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ISNULL((PFE11.SUMExpenseAmt),0)) 
				WHEN CR.ChargeUnit = 'K' THEN (((PL.Distance*CR.ChargeRate)*1.852)+ISNULL((PFE11.SUMExpenseAmt),0)) 
				WHEN (CR.ChargeUnit = 'H'AND C.AircraftBasis = 1) THEN ((PL.BlockHours *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN (CR.ChargeUnit = 'H'AND C.AircraftBasis = 2) THEN ((PL.FlightHours *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				END)*/
		[Total_Charges] = CASE  WHEN ISNULL(PL.PassengerTotal,0)=0 THEN 0 ELSE ((CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
								WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ISNULL((PFE11.SUMExpenseAmt),0)) 
								WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
								WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis= 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
								ELSE ISNULL((PFE11.SUMExpenseAmt),0)
								END)/PL.PassengerTotal)	END	
       ,[TenToMin] = @TenToMin
       ,PL.LegNUM
   INTO #PassTemp  
FROM PostflightMain PM JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
                       LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
                       JOIN Fleet F ON PM.FleetID =F.FleetID AND PM.CustomerID = F.CustomerID  
                       JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
                       LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                                   SELECT ISNULL(SUM(PP2.ExpenseAMT),0) FROM PostflightExpense PP2 
																	    WHERE PP2.POLegID = PP4.POLegID
	                                                                     AND IsBilling = 1)
                                                    FROM PostflightExpense PP4 WHERE IsBilling =1-- AND POLegID = 100997587
                                   )PFE11  ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID										
                       LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID AND AC.CustomerID = F.CustomerID
                       LEFT JOIN Department D ON P.DepartmentID = D.DepartmentID AND P.CustomerID = D.CustomerID 
					   LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = P.AuthorizationID 
					   LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
                       LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
                       LEFT OUTER JOIN (SELECT POLegID, ChargeUnit, ChargeRate FROM PostflightMain PM 
                                             INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                                 INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
                                          ) CR ON PL.POLegID = CR.POLegID    
WHERE CONVERT(DATE,PL.ScheduledTM)  BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID) AND P.PassengerRequestorCD IS NOT NULL
AND P.IsActive = 1
AND PM.IsDeleted = 0




IF @SuppressActivityPassenger=0
BEGIN
INSERT INTO #PassTemp(PassengerCode,PassengerName,TenToMin)
SELECT P.PassengerRequestorCD,ISNULL(LastName,'')+CASE WHEN FirstName<>'' OR FirstName IS NOT NULL THEN  ISNULL(', '+FirstName,'') ELSE '' END ,@TenToMin
       FROM Passenger P 
            INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
       WHERE P.CustomerID=CONVERT(BIGINT,@UserCustomerID)
		-- AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
	     --AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	     --AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
         AND PassengerRequestorCD NOT IN(SELECT PassengerCode FROM #PassTemp WHERE PassengerCode  IS NOT NULL)
         AND P.IsActive = 1
END


SELECT  DATERANGE ,
       [Log_Number] 
       ,CustomerID  
       ,[Tail_Number] 
       ,[Disp_Number]
       ,DepartmentCD
       ,AuthorizationCD
       ,[DEP_ICAO]
       ,[ARR_ICAO]
       ,[TripDate]
       ,PassengerRequestorID
       ,[PassengerCode]
       ,PassengerName
       ,[StandardBilling]
       ,[Dept]
       ,[Auth]
       ,[Miles]
       ,FlightHours
       ,BlockHours
       ,ISNULL([Nbr_Pax],0) [Nbr_Pax]
       ,MaximumPassenger
       ,[EmpSeats]
       ,AircraftTypeCD
       ,AircraftCD 
       ,ChargeRate
       ,[Per]
	    ,[Total_Charges] 		
       ,[TenToMin],LegNUM  FROM #PassTemp
       ORDER BY TripDate,[Log_Number],LegNUM 



IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
DROP TABLE #PassTemp

END

--EXEC spGetReportPOSTPassengerBillingDetail 'JWILLIAMS_13','10013','06/02/2010','06/12/2010','','','','','',0







GO


