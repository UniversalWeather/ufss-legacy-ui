IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE   proc [dbo].[spGetReportPOSTFleetFuelHistory]        
 @MONTH AS VARCHAR(10),        
  @YEAR  AS INT,        
  @UserCD AS VARCHAR(30)   ,    
  @TailNum as nVarchar(1000)='',    
  @FleetGroupCD as  nVarchar(1000)=''    
          
AS             
        
-- ===============================================================================        
-- SPC Name: spGetReportPOSTFleetFuelHistory        
-- Author: PREMPRAKASH.S        
-- Create date: 03 aug 2012        
-- Description: Get Fuel information based on Tail Number Details
-- Revision History        
-- Date                 Name        Ver         Change        
-- spGetReportPOSTFleetFuelHistory 'dec',2010,'JWILLIAMS_11','',''        
-- ================================================================================        
DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1

CREATE TABLE #temp (TailNum varchar(9),AircraftCD varchar(20),POLogID bigint,fuelunits varchar(30),unitsavg varchar(30),units varchar(30),FuelQTY numeric(15,3),
QTY numeric(15,3),ExpenseAMT numeric(17,3),PurchaseDT date,
 Monthval int,months date) ;
CREATE TABLE #temp1 (TailNum varchar(9),AircraftCD varchar(20),POLogID bigint,fuelunits varchar(30),unitsavg varchar(30),units varchar(30),FuelQTY numeric(15,3),
QTY numeric(15,3),ExpenseAMT numeric(17,3),PurchaseDT date,
 Monthval int,months date) ;
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

declare @units varchar(30) 
 set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN '12-Month Avg/Gallons'
                WHEN (cp.FuelPurchase='2') THEN '12-Month Avg/Liters'
				WHEN (cp.FuelPurchase='3') THEN '12-Month Avg/Imp Gallons'
				WHEN (cp.FuelPurchase='4') THEN '12-Month Avg/Pounds'
				ELSE '12-Month Avg/Kilos'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
declare @fuelunits varchar(30) 
 set @fuelunits=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'Fuel Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'Fuel Liters'
				WHEN (cp.FuelPurchase='3') THEN 'Fuel Imp Gallons'
				WHEN (cp.FuelPurchase='4') THEN 'Fuel Pounds'
				ELSE 'Fuel Kilos' 
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   

 declare @unitsavg varchar(30) 
 set @unitsavg=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'Average/Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'Average/Liters'
				WHEN (cp.FuelPurchase='3') THEN 'Average/Imp Gallons'
				WHEN (cp.FuelPurchase='4') THEN 'Average/Pounds'
				ELSE 'Average/Kilos'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------	
DECLARE @EndDate datetime          
DECLARE @StartDate datetime         
DECLARE @StartDateval datetime         
DECLARE @ParameterDefinition AS NVARCHAR(MAX)        
DECLARE @date DATETIME        
DECLARE @A int  
declare @e int 
DECLARE @d DATETIME
------Framing start Date and End Date from the given year and month 
if(@YEAR%4=0)
begin
--print 'Leap Year'
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
--SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
SELECT @EndDate = DATEADD(month, ((@YEAR - 1900) * 12) + @A, -1)
--print @EndDate 
--SET @StartDate=DateAdd(DD,-365, @EndDate) 
SELECT @StartDate = CONVERT(Datetime,DATEADD(dd,-(DAY(DATEADD(M, -11, @EndDate))-1),DATEADD(M, -11, @EndDate)))
--print @EndDate 
--print @StartDate
end
else 
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
--SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
--SET @StartDate=DateAdd(DD,-364, @EndDate) 
SELECT @EndDate = DATEADD(month, ((@YEAR - 1900) * 12) + @A, -1)
SELECT @StartDate = CONVERT(Datetime,DATEADD(dd,-(DAY(DATEADD(M, -11, @EndDate))-1),DATEADD(M, -11, @EndDate)))
end
------------------
--print @StartDate
--print @EndDate
-----------------   
DECLARE @Monthval int          
DECLARE @intFlag INT          
SET @intFlag=1          
DECLARE @Monthdisplay TABLE          
(        
Monthval int ,Monthnameval varchar(20),        
dateFuel datetime,fuel varchar(30) ,average varchar(30) ,totalavg varchar(30) ) 


------Inserting 12 months into @Monthdisplay table  
WHILE (@intFlag <=12)          
BEGIN               
 INSERT INTO @Monthdisplay (Monthval,Monthnameval,dateFuel,fuel,average,totalavg)         
 VALUES(DATEpart(month,@StartDate) , DATENAME(month,@StartDateval) ,@StartDate,@fuelunits ,@unitsavg,@units)           
     SET @StartDate= DateAdd(month,1, @StartDate)        
    SET @intFlag =@intFlag+1           
END 
-------  Leap year calculation    
if(@YEAR%4=0)
begin
SET @StartDate=DateAdd(DD,-365, @EndDate) 
end
else 
begin
SET @StartDate=DateAdd(DD,-364, @EndDate) 
end
----To Retrieve fuel and expense information for each month based on Tail number
INSERT INTO #temp
select distinct TailNum,AircraftCD,POLogID,fuel as fuelunits, average as unitsavg ,totalavg as units,FuelQTY,QTY,ExpenseAMT,PurchaseDT, m.Monthval,dateFuel as months     
from @Monthdisplay m  
left OUTER join           
(            
SELECT DISTINCT 
ft.TailNum,
AircraftCD, 
PFE.POLogID, 
[fuelunits]=@fuelunits,
[unitsavg]=@unitsavg,
[units]=@units,
[FuelQTY]=dbo.GetFuelConversion(pfe.FuelPurchase,pfe.FuelQTY,@UserCD),
[QTY]=  dbo.GetFuelConversion(pfe.FuelPurchase,pfe.FuelQTY,@UserCD),
ExpenseAMT,
PurchaseDT,  

DATEpart(MONTH,PurchaseDT) AS months    
FROM  Fleet ft    
INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = FT.FleetID     
LEFT  JOIN(SELECT PE.POLogID,PE.FuelPurchase,PE.FuelQTY,PE.PurchaseDT,PE.ExpenseAMT,PE.FleetID FROM PostflightExpense PE
                           INNER JOIN PostflightMain PM ON PM.POLogID=PE.POLogID AND PM.IsDeleted=0 
                           INNER JOIN PostflightLeg PL ON PM.PoLogID=PL.PoLogID AND PL.POLegID=PE.POLegID AND PL.IsDeleted=0 AND PE.IsDeleted=0 AND PE.FuelQTY> 0) pfe on ft.FleetID = pfe.FleetID   
LEFT JOIN FleetGroupOrder FGO on FGO.FleetID = FT.FleetID            
LEFT JOIN FleetGroup FG on FG.FleetGroupID = FGO.FleetGroupID
 WHERE ft.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
 and CONVERT(DATE,PurchaseDT) between CONVERT(DATE,@StartDate) AND  CONVERT(DATE,@EndDate) 
 ) a 
    on m.Monthval = a.months      


ORDER BY dateFuel


-----Retrieving Tail Number which are not assigned to the log
INSERT INTO #temp1
SELECT distinct TailNum,AircraftCD,NULL,@fuelunits as fuelunits,@unitsavg as unitsavg,@units as units,NULL,NULL,NULL,NULL,NULL,NULL
FROM Fleet F 
INNER JOIN (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = F.FleetID
left outer JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 

delete from #temp1 where TailNum in(select distinct TailNum from #temp)

IF  @IsZeroSuppressActivityAircftRpt=1 AND (SELECT COUNT(*) FROM #temp WHERE TailNum IS NOT NULL)=0
BEGIN
DELETE FROM #temp WHERE TailNum IS NULL
END


------Applying Zero Suppress Activity for Aircraft based on Company profile setting
IF @IsZeroSuppressActivityAircftRpt=0
BEGIN
	SELECT * FROM #temp
	union all 
	SELECT * FROM #temp1
END
ELSE
BEGIN
	SELECT * FROM #temp
END
IF OBJECT_ID('tempdb..#temp') IS NOT NULL
DROP TABLE #temp
IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1

-- spGetReportPOSTFleetFuelHistory 'dec',2010,'JWILLIAMS_13','',''  





GO


