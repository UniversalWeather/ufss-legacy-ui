IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftPerformanceExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftPerformanceExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetReportPOSTAircraftPerformanceExportInformation]   
(   
  @UserCD AS VARCHAR(30),
  @DATEFROM AS DATETIME,
  @DATETO AS DATETIME,
  @TailNUM AS VARCHAR(5000) = '',  
  @FleetGroupCD AS VARCHAR(5000)='', 
  @SortBy As NVARCHAR(1000) = '' 
  )       
AS   
-- ===============================================================================    
-- SPC Name: spGetReportAircraftPerformanceAnalysis    
-- Author:  P.srinivas    
-- Create date: 
-- Altered by Mathes 30-10-2012    
-- Description: Get Postflight Aircraft Performance Analysis for REPORTS    
-- Revision History    
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================    
SET NOCOUNT ON   
	DECLARE @AppDTFormat VARCHAR(25)
SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';   
  DECLARE @ParameterDefinition AS NVARCHAR(500)  
  Declare @TenToMin Int
  Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))       
 
      
  Create Table #TmpFltTail (TailNum Varchar(9),Aircraft char(20))  
  
  INSERT INTO #TmpFltTail
  SELECT DISTINCT TailNum,AircraftCD Aircraft
  FROM   Fleet F LEFT OUTER JOIN FleetGroupOrder FGO              
         ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID LEFT OUTER JOIN FleetGroup FG               
         ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
  WHERE (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
        (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
        F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                       
 
      
 Create Table #TmpRpt(POLogID BigInt,
                      POLegID BigInt,
					  TailNum Varchar(10),
                      ScheduledTM DateTime,
                      DispatchNum Varchar(12),
                      LogNum BigInt,
                      LegNUM BigInt,
                      DepartICAOID varchar(30),
                      ArrivalICAOID varchar(30),
                      FlightCatagoryCD Varchar(6),
                      SeatLeft Varchar(50) ,
                      SeatRight  Varchar(50), 
                      Distance  Numeric(5,0),
                      StatuteMiles Numeric(5,0),
                      DelayTypeCD Varchar(10),
                      DelayTM Numeric(6,3),
                      BlockHours Numeric(6,3),
                      FlightHours Numeric(6,3), 
                      FuelUsed Numeric(7,0), 
                      PassengerTotal Numeric(5,0),
                      FuelLocatorCD  Varchar(10), 
                      FuelQTY Numeric(15,2),
                      ExpenseAMT Numeric(17,2),
                      AircraftCD Varchar(6),
                      InboundDTTM  DateTime,
                      OutboundDTTM DateTime,
                      EstDepartureDT DateTime,
                      LegID BIGINT,
                      FuelPurchase Numeric(1,0),
                      UnitPrice Numeric(10,4),
                      HrsAway Int) 
                          
 
SET @SQLSCRIPT ='Insert Into #TmpRpt SELECT
DISTINCT 
PFL.POLogID,
PFL.POLegID,
FT.TailNum,
PFL.ScheduledTM,
POM.DispatchNum,
POM.LogNum,
PFL.LegNUM,
CONVERT (VARCHAR, ADeprt.IcaoID) DepartICAOID,
CONVERT (VARCHAR, AArr.IcaoID) ArrivalICAOID,        
FC.FlightCatagoryCD,
SeatLeft=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''L'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),
SeatRight=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''R'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),
PFL.Distance,
PFL.StatuteMiles,
DT.DelayTypeCD,
PFL.DelayTM,
ROUND(PFL.BlockHours,1) BlockHours,
ROUND(PFL.FlightHours,1) FlightHours, 
PFL.FuelUsed, 
PFL.PassengerTotal,
FuelLocator.FuelLocatorCD, 
DBO.GetFuelConversion(PFE.FuelPurchase,PFE.FuelQTY,@UserCD),
Case WHEN CPY.FuelPurchase <> PFE.FuelPurchase  THEN  (PFE.FuelQTY*PFE.UnitPrice)/NULLIF(DBO.GetFuelConversion(PFE.FuelPurchase,PFE.FuelQTY,@UserCD),0) 
                                               ELSE PFE.UnitPrice End,
FT.AircraftCD,
PFL.InboundDTTM,
PFL.OutboundDTTM,
POM.EstDepartureDT,
PFL.POLegID,
PFE.FuelPurchase,
PFE.UnitPrice,
DATEDIFF(d,PFL.OutboundDTTM , PFL.InboundDTTM) HoursAway 
FROM  PostflightMain POM
INNER JOIN PostflightLeg PFL on PFL.POLogID = POM.POLogID and PFL.ISDELETED=0
LEFT JOIN FlightCatagory FC On PFL.FlightCategoryID = FC.FlightCategoryID 
LEFT JOIN PostflightExpense PFE on PFE.POLogID=POM.POLogID and PFE.POLegID = PFL.POLegID AND PFE.ISDELETED=0
LEFT JOIN FuelLocator ON PFE.FuelLocatorID = FuelLocator.FuelLocatorID 
LEFT JOIN
( SELECT DISTINCT F.FleetID,F.AircraftCD,F.HomeBaseID,F.TailNum
  FROM  Fleet F               
  LEFT OUTER JOIN FleetGroupOrder FGO              
  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
  LEFT OUTER JOIN FleetGroup FG               
  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID    
  WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''    
) FT ON FT.FleetID = POM.FleetID   
LEFT JOIN Company CPY on CPY.HomebaseID = FT.HomebaseID 
LEFT JOIN  Airport ADeprt ON ADeprt.AirportID = PFL.DepartICAOID
LEFT JOIN  Airport AArr ON AArr.AirportID = PFL.ArriveICAOID  
LEFT JOIN DelayType DT ON DT.DelayTypeID = PFL.DelayTypeID
LEFT JOIN PostflightCrew PFC on PFC.POLegID = PFL.POLegID 
WHERE PFL.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))     
+ ' AND CONVERT(DATE,PFL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) '  
 
    --Construct OPTIONAL clauses   
IF @TailNUM <> '' BEGIN      
 SET @SQLSCRIPT = @SQLSCRIPT + ' AND FT.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''') '    
END    
   
IF @sortby='LogNum'  
BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY LogNum'  
END  
Else
BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY ScheduledTM'  
END    
  
 SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME,@DATETO AS DATETIME,@TailNUM AS VARCHAR(5000),@FleetGroupCD AS VARCHAR(5000),@sortby As NVARCHAR(1000)'
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD,@DATEFROM,@DATETO,@TailNUM,@FleetGroupCD,@sortby   
 

 Create Table #Tmp(TailNum Varchar(10),Aircraft CHAR(20)) 
 Declare @TailNm Varchar(10)    
 declare @Aircraft varchar(30)
 INSERT INTO #Tmp SELECT DISTINCT TailNum,Aircraft From #TmpFltTail Where TailNum Not In(Select Distinct TailNum from #TmpRpt)
 WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
 BEGIN
  set @Aircraft=''
   Set @TailNm=''
   
   Select Top 1 @TailNm=TailNum,@Aircraft=Aircraft From #Tmp  
   Insert Into #TmpRpt(TailNum,AircraftCD,DelayTM,BlockHours,FlightHours) Values(@TailNm,@Aircraft,0,0,0)
   Delete From #Tmp Where TailNum=@TailNm  
 END           
 IF OBJECT_ID('tempdb..#Tmp') is not null                        
      DROP table #Tmp 
       		   
 Select POLogID POLogID,
		POLegID POLegID,
	    TailNum tail_nmbr,
        AircraftCD ac_code,
        ScheduledTM schedttm,
        DispatchNum dispatchno,
        IsNull(LogNum,0) lognum,
        IsNull(LegNUM,0) leg_num,
        EstDepartureDT estdepdt,
        LegID legid,
        DepartICAOID depicao_id,
        ArrivalICAOID arricao_id,
        FlightCatagoryCD cat_code,
        SeatRight pic,
        SeatLeft sic,
        IsNull(Distance,0) distance,
        OutboundDTTM outdttm,
        InboundDTTM indttm,        
        DelayTypeCD delaytype,
        DelayTM delaytime,
        IsNull(BlockHours,0) blk_hrs,
        IsNull(FlightHours,0) flt_hrs,
        IsNull(Distance,0)/NullIf(FlightHours,0) flt_speed,
        IsNull(FuelUsed,0) fuel_used,
        IsNull(FuelUsed,0)/NullIf(BlockHours,0) fuel_blkhr,         
        IsNull(FuelUsed,0)/NullIf(FlightHours,0) fuel_flthr,
        IsNull(PassengerTotal,0) pax_total,
        FuelLocatorCD fuel_loc,  
        FuelPurchase fuelpurch,
        UnitPrice unitprice, 
        IsNull(FuelQTY,0) fuelqty,  
        IsNull(ExpenseAMT,0) expamt, 
        IsNull(StatuteMiles,0) statemiles,
        HrsAway =Floor((dbo.GetHoursAway(@UserCD,POLegID,POLogID))*10)*0.1,
        @TenToMin TenToMin,
        [DATFORMAT]=@DATEFORMAT 
        From #TmpRpt Order by AircraftCD 
         


  IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
      DROP table #TmpFltTail   
  IF OBJECT_ID('tempdb..#TmpRpt') is not null                        
      DROP table #TmpRpt 
   IF OBJECT_ID('tempdb..#Tmp') is not null                        
      DROP table #Tmp  
 --EXECUTE spGetReportPOSTAircraftPerformanceExportInformation 'supervisor_99','1-jan-2009','15-jan-2009','','',''



GO


