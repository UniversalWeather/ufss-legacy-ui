IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCustomUVMaintainedAirportExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCustomUVMaintainedAirportExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportDBCustomUVMaintainedAirportExportInformation]
	@UserCD VARCHAR(30)
       ,@UserCustomerID AS VARCHAR(30)
       ,@Airports As NVARCHAR(1000) = '' 

AS

BEGIN
 

-- ===============================================================================
-- SPC Name: spGetReportDBCustomUVMaintainedAirportExportInformation
-- Author: Aishwarya.M
-- Create date: 02 Jan 2014
-- Description: Get Customer UV Maintained Airport Information for Reports
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
IF @Airports='UV'

BEGIN

SELECT DISTINCT A.IcaoID  AS [icao_id],
       A.CityName city,
       A.StateName state,
       CR.CountryName crtydesc,
       CR.CountryCD Country,
       A.AirportName airport_nm,
       A.IsInActive inactive,
       'TRUE' fpkflg,
       A.UWAID AS uvid,
       'UV'  customuv
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         AND UWAID IS NOT NULL
 ORDER BY A.IcaoID
END 

ELSE IF  @Airports='CUSTOM'        
BEGIN

SELECT DISTINCT A.IcaoID  AS [icao_id],
       A.CityName city,
       A.StateName state,
       CR.CountryName crtydesc,
       CR.CountryCD country,
       A.AirportName airport_nm,
       A.IsInActive inactive,
       'TRUE' fpkflg,
       A.UWAID AS uvid,
        'Custom'  customuv
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         AND UWAID IS NULL
         ORDER BY A.IcaoID
END 
ELSE
BEGIN 
SELECT DISTINCT A.IcaoID  AS [icao_id],
       A.CityName city,
       A.StateName state,
       CR.CountryName crtydesc,
       CR.CountryCD country,
       A.AirportName airport_nm,
       A.IsInActive inactive,
       'TRUE' fpkflg,
       A.UWAID AS uvid,
       CASE WHEN A.UWAID='' OR  A.UWAID IS NULL THEN 'CUSTOM' ELSE 'UV' END  customuv
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         ORDER BY A.IcaoID
END

END





--EXEC spGetReportDBCustomUVMaintainedAirportExportInformation 'SUPERVISOR_99',10099,'UV'



GO


