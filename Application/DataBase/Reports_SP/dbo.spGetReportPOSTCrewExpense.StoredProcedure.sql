IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewExpense]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewExpense]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spGetReportPOSTCrewExpense] 
(
  @UserCustomerID AS BIGINT,
  @UserCD AS VARCHAR(30), --Mandatory
  @DATEFROM AS DATETIME, --Mandatory
  @DATETO AS DATETIME, --Mandatory
  @CrewCD AS NVARCHAR(1000) = '', 
  @TailNum AS NVARCHAR(1000) = '',
  @LogNum  AS NVARCHAR(1000) = '',
  @AccountNum AS NVARCHAR(1000) = ''	
)
AS	
				
BEGIN

Declare @SuppressActivityCrew BIT  
SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@UserCustomerID
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))


SET NOCOUNT ON;

		
SELECT DISTINCT C.CrewCD 
      ,C.LastName+', '+ C.FirstName+' '+ ISNULL(C.MiddleInitial,'') AS CrewName
      ,F.TailNum
      ,PM.LogNum
      ,CONVERT(DATE,PE.PurchaseDT) AS PurchaseDT
      ,A.IcaoID
      ,AC.AccountNum
      ,FB.FBOVendor
      ,V.Name
      ,PE.InvoiceNUM
      ,PE.SlipNUM
      ,PT.PaymentTypeDescription
      ,PE.ExpenseAMT
      ,C.CrewID
  INTO #PassTemp     
  FROM PostflightLeg PL 
  JOIN PostflightExpense PE ON PL.POLegID=PE.POLegID AND PL.IsDeleted = 0
  JOIN Crew C ON PE.CrewID = C.CrewID
  JOIN Fleet F ON PE.FleetID = F.FleetID AND F.IsDeleted=0 AND F.IsInActive=0
  JOIN PostflightMain PM ON PE.POLogID = PM.POLogID AND PM.IsDeleted = 0
  JOIN Account AC ON PE.AccountID = AC.AccountID
  JOIN FBO FB ON PE.FBOID = FB.FBOID
  JOIN Airport A ON PE.AirportID = A.AirportID
  LEFT JOIN PaymentType PT ON PE.PaymentTypeID = PT.PaymentTypeID
  LEFT JOIN VENDOR V ON PE.PaymentVendorID = V.VendorID
  WHERE CONVERT(DATE,PE.PurchaseDT)  BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)  
    AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD ='')
    AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM ='')
    AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (AC.AccountNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AccountNum, ',')) OR @AccountNum = '')
    AND C.IsStatus = 1 AND PE.CustomerID = @UserCustomerID --AND(V.VendorType = 'C' OR V.VendorType IS NULL)
    AND PE.IsDeleted = 0
  
IF @SuppressActivityCrew=0
BEGIN


INSERT INTO #PassTemp(CrewCD,CrewName,CrewID)
SELECT C.CrewCD,C.LastName+', '+ C.FirstName+' '+ ISNULL(C.MiddleInitial,'') AS CrewName,C.CrewID
       FROM Crew C 
       WHERE C.CustomerID=@UserCustomerID
         AND C.IsDeleted=0
         AND C.IsStatus=1
		 AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
	     AND CrewID NOT IN(SELECT ISNULL(CrewID,0) FROM #PassTemp WHERE CrewCD  IS NOT NULL )
         
END
SELECT * FROM #PassTemp ORDER BY CrewCD, TailNum, LogNum,SlipNUM

IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
DROP TABLE #PassTemp

END







GO


