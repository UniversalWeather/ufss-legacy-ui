IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDestinationInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDestinationInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spGetReportPREDestinationInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@IcaoID AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0
AS
-- ===============================================================================
-- SPC Name: spGetReportPREDestinationInformation
-- Author: SUDHAKAR J
-- Create date: 06 Jul 2012
-- Description: Get Destination information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================
BEGIN
SET NOCOUNT ON



DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

	SET @SQLSCRIPT = '
		SELECT
			[DateRange] = dbo.GetShortDateFormatByUserCD (@UserCD,@DATEFROM) + '' - '' + dbo.GetShortDateFormatByUserCD (@UserCD,@DATETO)
			,[Destination] = CONVERT(VARCHAR, A.ICAOID) + '' '' + A.CityName
			,[TailNUM] = F.TailNUM
			,[DepartureDT] = PL.DepartureDTTMLocal
			,[TripNUM] = PM.TripNUM
			,[TripDescription] = dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
		FROM PreflightMain PM 
			INNER JOIN (
				SELECT TripID, TripNUM, ArriveICAOID, CustomerID, DepartureDTTMLocal FROM PreflightLeg WHERE ISDELETED = 0
			) PL ON PM.TripID = PL.TripID
			INNER  JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID 
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
						OR @FleetGroupCD = '''' ) 
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			INNER JOIN ( 
				SELECT AirportID, IcaoID, CityName, CustomerID FROM Airport
			) A ON PL.ArriveICAOID = A.AirportID
		WHERE PM.IsDeleted = 0 AND PM.TripStatus = ''T'' 
			AND PM.CustomerID = ' + CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			+ ' AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN @DATEFROM AND @DATETO ';

	--Construct OPTIONAL clauses
	IF @IcaoID <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@IcaoID, 1) = ',' THEN LEFT(@IcaoID, LEN(@IcaoID) - 1) ELSE @IcaoID END, ',', ''', ''') + ''')';
	END
	IF @TailNum <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';
	END

	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
	 SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY Destination,CONVERT(DATE,PL.DepartureDTTMLocal),TailNUM'
	
	 
	 
	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS VARCHAR(30)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD
END

-- EXEC spGetReportPREDestinationInformation 'JWILLIAMS_11','2012/09/01','2012/09/07', '', '', '', 1
--select * from PreflightMain where TripNUM =1155




GO


