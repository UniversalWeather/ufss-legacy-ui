IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTLegAnalysisDistanceExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTLegAnalysisDistanceExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


        
CREATE PROCEDURE [dbo].[spGetReportPOSTLegAnalysisDistanceExportInformation]                                      
(                                
  @UserCD AS VARCHAR(30),  --Mandatory                                
  @DATEFROM DATETIME, --Mandatory                                
  @DATETO DATETIME, --Mandatory                                
  @TailNum VARCHAR(5000)='',--Optional                                
  @FleetGroupCD VARCHAR(5000)=''--Optional                                
 )                               
-- =============================================                                      
-- Author: Mathes                 
-- ALtered 05-10-12                                     
-- Description: Leg Analysis/Hours(FC) - Report   
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package                                    
-- =============================================                                      
AS                                    
SET NOCOUNT ON                                      
BEGIN                                      

                                        
 Declare @TenToMin Int
 Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))       
                                                               
  
  DECLARE @CUSTOMERID BIGINT = (SELECT CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))) 
   Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
 
 IF OBJECT_ID('tempdb..#Tmp') is not null                          
   DROP table #Tmp                
                      
 Create Table #Tmp (TailNum Varchar(10),AircraftCD Varchar(10))                  
            
 DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';                                  
 DECLARE @ParameterDefinition AS NVARCHAR(300)                                     
                         
 SET @SQLSCRIPT='INSERT INTO #Tmp 
				SELECT Distinct F.TailNum,F.AircraftCD                
                 FROM  fleet 
                 --INNER JOIN PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID                
                 left outer JOIN               
                 ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.IsInactive                     
                   FROM  Fleet F               
                   LEFT OUTER JOIN FleetGroupOrder FGO              
                   ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
                   LEFT OUTER JOIN FleetGroup FG               
                   ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID    
                   WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''  
                   ) F ON F.FleetID = fleet.FleetID                                         
                  WHERE fleet.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and fleet.isdeleted=0 '                          
                                            
                                   
 IF @TailNum <>''                                   
 BEGIN                                  
 SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';                                  
 END                                
                                          
 SET @SQLSCRIPT=@SQLSCRIPT + ' Order By F.AircraftCD,F.TailNum '
                                   
 SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNum AS VARCHAR(5000), @FleetGroupCD AS VARCHAR(5000)'                                  
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @TailNum, @FleetGroupCD                                  
                
 IF OBJECT_ID('tempdb..#TempDist') is not null                              
 DROP table #TempDist                              
                 
 Create Table #TempDist (DistId Int,DistFrom Int,DistTo Int)                   
 Declare @DistId Int                              
 Declare @DistFrom Int                              
 Declare @DistTo Int                   
                        
 Declare @RowId Int                              
 Declare @NumRows Int                                           
              
                         
   INSERT INTO #TempDist (DistId,DistFrom,DistTo)                              
   Select 1,0,24  union All                               
   Select 2,25,49  union All                               
   Select 3,50,74  union All                               
   Select 4,75,99  union All                               
   Select 5,100,124  union All                               
   Select 6,125,149  union All                               
   Select 7,150,174  union All                               
   Select 8,175,199  union All                   
   Select 9,200,249  union All                               
   Select 10,250,299  union All                               
   Select 11,300,399  union All                               
   Select 12,400,499  union All                               
   Select 13,500,599  union All                               
   Select 14,600,699  union All                               
   Select 15,700,799  union All                               
   Select 16,800,899  union All                               
   Select 17,900,999  union All                               
   Select 18,1000,1199  union All                               
   Select 19,1200,1399  union All                               
   Select 20,1400,1599  union All                               
   Select 21,1600,1799  union All                               
   Select 22,1800,1999  union All                               
   Select 23,2000,2199  union All                               
   Select 24,2200,2399  union All                               
   Select 25,2400,2599  union All                               
   Select 26,2600,2799  union All                               
   Select 27,2800,2999  union All                               
   Select 28,3000,3199  union All                               
   Select 29,3200,3999  union All                               
   Select 30,4000,9999                             
                                  
    IF OBJECT_ID('tempdb..#TempReport') is not null                              
       DROP table #TempReport                              
                                     
    Create Table #TempReport (SNo Int, FromMile Int,ToMile Int,FleetID BIGINT,TailNum Varchar(9),AircraftCD CHAR(6),NoOfLegs Int,                       
                               BlockHours Numeric(10,2),FlightHours Numeric(10,2),  Miles Int, FuelBurn Int, NoOfPax Int,            
                               TotLine Char(1), AvgBleg Numeric(10,2))                           
 Declare @TailNumber Varchar(9)                        
 Declare @AircraftCD CHAR(3)             
 Declare @NoOfLegs INT                 
 Declare @BlockHours Numeric(10,2)                           
 Declare @FlightHours Numeric(10,2)                         
 Declare @Miles Int                          
 Declare @FuelBurn Int                           
 Declare @NoOfPax Int                           
    Declare @SNo Int            
              
    Declare @AvgBleg Numeric(10,2)              
    Declare @TotBlkHrs Numeric(10,2)            
    Declare @TotFltHrs Numeric(10,2)            
                   
    Set @SNo=0            
    
  --Declare @RecCount Int=0
  --Set @RecCount=(SELECT  COUNT(*) FROM #Tmp)
  --IF @RecCount =0
  --Begin
  -- INSERT INTO #Tmp SELECT Distinct TailNum From Fleet Where IsNUll(IsInActive,0)=0 And  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
  --End
                         
              
 WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0                 
 BEGIN            
    Select Top 1 @TailNumber=TailNum From #Tmp   
    Set @AircraftCD =(Select Top 1 AircraftCD From Fleet Where TailNum=@TailNumber)        
    --=========================================              
  SET @RowId = 1                                 
  SET @NumRows =  (SELECT COUNT(*) FROM #TempDist)               
  Set @TotBlkHrs=0            
  Set @TotFltHrs=0            
	  IF @NumRows > 0                               
	   WHILE (@RowId <= (SELECT MAX(DistID) FROM #TempDist))                                
	BEGIN                              
	                               
	   Set @DistId=0                          
	   Set @DistFrom=0                               
	   Set @DistTo=0  
	   Set @BlockHours=0
       Set @FlightHours=0
       Set @Miles=0
       Set @FuelBurn=0
       Set @NoOfLegs=0 
       Set @NoOfPax=0                            
	                            
	   SELECT @DistId=DistId,@DistFrom=DistFrom,@DistTo=DistTo From #TempDist Where DistId=@RowId                
	   -----------------------------------------------------------------------------    
	   IF OBJECT_ID('tempdb..#TmpTble') is not null                              
       DROP table #TmpTble 
       
	   Create Table #TmpTble (AircraftCD Varchar(6), BlockHours Numeric(10,2),FlightHours Numeric(10,2),Miles Int,FuelBurn Int,NoOfLegs Int,NoOfPax Int)
	    
		     Insert Into #TmpTble SELECT                      
			 F.AircraftCD,                       
			 IsNull(Sum(ROUND(PostflightLeg.BlockHours,1)),0) As BlockHours,                        
             IsNull(Sum(ROUND(PostflightLeg.FlightHours,1)),0) As FlightHours,                         
             IsNull(Sum(PostflightLeg.Distance),0) As Distance,                         
             IsNull(Sum(PostflightLeg.FuelUsed),0) As FuelUsed,                         
             ISNULL(COUNT(PostflightLeg.POLegID), 0) AS NoOfLegs,    
             ISNULL(SUM(PostflightLeg.PassengerTotal), 0) AS NoOfPax                       
		FROM  PostflightMain INNER JOIN                    
		   PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID AND PostflightLeg.IsDeleted=0                
		   INNER JOIN               
			( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum               
			  FROM   Fleet F               
			  LEFT OUTER JOIN FleetGroupOrder FGO              
			  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
			  LEFT OUTER JOIN FleetGroup FG               
			  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
			  WHERE (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')           
		   ) F ON F.FleetID = PostflightMain.FleetID 
		  WHERE PostflightMain.IsDeleted=0                  
		GROUP BY  F.FleetID,                     
			F.AircraftCD,                      
			PostflightLeg.BlockHours,                      
			PostflightLeg.FlightHours,                      
			PostflightLeg.Distance,                      
			PostflightLeg.FuelUsed ,                      
			PostflightLeg.CustomerID,                      
			PostflightLeg.ScheduledTM,                      
			F.TailNum              
		 HAVING PostflightLeg.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) And                         
			F.TailNum =@TailNumber And                          
			PostflightLeg.Distance Between @DistFrom And @DistTo And                            
			Convert(Date,PostflightLeg.ScheduledTM) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO) 
	          
	        Set @NoOfLegs = (Select SUM(NoOfLegs) From #TmpTble)             
	        Set @BlockHours = (Select SUM(BlockHours) From #TmpTble)
	        Set @FlightHours = (Select SUM(FlightHours) From #TmpTble)
	        Set @Miles = (Select SUM(Miles) From #TmpTble)
	        Set @FuelBurn = (Select SUM(FuelBurn) From #TmpTble)
	        Set @NoOfPax = (Select SUM(NoOfPax) From #TmpTble) 
	                    
			Set @TotBlkHrs=@TotBlkHrs + @BlockHours             
			Set @TotFltHrs=@TotFltHrs + @FlightHours                       
         
			Set @AvgBleg=0           
	                                
			IF @NoOfLegs>0            
			BEGIN            
			   Set @AvgBleg =@FuelBurn/@NoOfLegs       
			END                
	                        
			Set @SNo = @SNo + 1                
			Insert Into #TempReport(SNo,FromMile,ToMile,TailNum, AircraftCD,NoOfLegs,                    
			   BlockHours,FlightHours,Miles,FuelBurn,NoOfPax,TotLine,AvgBleg)                       
			Values(@SNo,@DistFrom,@DistTo,@TailNumber,@AircraftCD,@NoOfLegs,                      
			   @BlockHours,@FlightHours,@Miles,@FuelBurn,@NoOfPax,'',@AvgBleg)                 
	                                      
		------------------------------------------------------------------------------                            
		Set @RowId=@RowId+1                              
		END                     
                  
   Delete From #Tmp Where TailNum=@TailNumber             
   --=========================================             
   END      
   
     IF @SuppressActivityAircft =1
  
  BEGIN
  
  DELETE FROM #TempReport WHERE TailNum  NOT IN(   SELECT TailNum FROM #TempReport 	WHERE   (NoOfLegs IS NOT NULL
																		OR  BlockHours IS NOT NULL
																		OR  FlightHours IS NOT NULL
																		OR  Miles IS NOT NULL
																		OR  FuelBurn IS NOT NULL 
																		OR NoOfPax IS NOT NULL
																		OR TotLine IS NOT NULL
																	    OR AvgBleg IS NOT NULL)
																        AND NoOfLegs<>0
																		AND BlockHours<>0.00
																		AND FlightHours<>0.00
																		AND Miles<>0
																		AND FuelBurn<>0
																		AND NoOfPax<>0
																	--	AND TotLine<>''
																		AND AvgBleg<>0.00)
END      
            
                           
   IF OBJECT_ID('tempdb..#TempDist') is not null                              
      DROP table #TempDist                   
                  
   Select AircraftCD  ac_code,            
          TailNum tail_nmbr,            
          FromMile beginmile,            
          ToMile endmile,            
          IsNull(NoOfLegs,0) numlegs,  
          IsNull(BlockHours,0) blk_hrs,
          IsNull(FlightHours,0) flt_hrs,   
          IsNull(Miles,0) distance,          
          IsNull(FuelBurn,0) fuel_used,          
          IsNull(AvgBleg,0) avgbleg,          
          IsNull(NoOfPax,0) numpax,
          @TenToMin TenToMin From #TempReport ORDER BY AircraftCD                  
                                
   IF OBJECT_ID('tempdb..#TmpTble') is not null                              
       DROP table #TmpTble                                   
   IF OBJECT_ID('tempdb..#TempReport') is not null                              
      DROP table #TempReport                                  
 END       
       
 --Exec spGetReportPOSTLegAnalysisDistanceExportInformation 'eliza_9','01-Jan-2011','31-Dec-2012' 
                 

GO


