IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetAnnualStatisticsFlightAddlInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightAddlInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightAddlInformation1]              
( 
@UserCD VARCHAR(30), --Mandatory
@DATEFROM AS DATETIME='', --Mandatory
@DATETO AS DATETIME='', --Mandatory
@Year INT, -- Mandatory
@TailNum VARCHAR(5000)='',
@FleetGroupCD VARCHAR(5000)='',
@DepartmentCD VARCHAR(5000)='',
@DepartmentGroupID VARCHAR(5000)='',
@HomeBase VARCHAR(5000)='',
@FlightCatagoryCD VARCHAR(5000)='',
@HomeBaseOnly bit=0
)
AS
SET NOCOUNT ON
BEGIN

Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))

DECLARE @ParameterDefinition AS NVARCHAR(400)
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = ''

Declare @TotalTripDays Int=0
Declare @TotalLegs Int=0
Declare @TotalPassengers Int=0
Declare @TotalMilesFlown Int=0
Declare @TotalLoadFactor Numeric(10,2)=0
Declare @TotalFlightHours Numeric(10,2)=0

IF OBJECT_ID('tempdb..#TmpTble') is not null
DROP table #TmpTble

Create Table #TmpTble(ScheduleDTTMLocal DateTime,DutyType Int, TotalLegs Int,TotalPassengers Int,
TotalMilesFlown Int, TotalFlightHours Numeric(10,2),
ScheduledTM DateTime,POLegID BigInt,POLogID BigInt,LoGNUM BigInt,FleetID BigInt)

SET @SQLSCRIPT ='INSERT INTO #TmpTble SELECT 
PostflightLeg.ScheduleDTTMLocal,
PostflightLeg.DutyTYPE,    
IsNull(Count(PostflightLeg.LegNUM),0) AS TotalLegs,         
IsNull(Sum(PostflightLeg.PassengerTotal),0) As TotalPassengers,
IsNull(Sum(PostflightLeg.Distance),0) As TotalMilesFlown, 
IsNull(Sum(ROUND(PostflightLeg.FlightHours,1)),0) As TotalFlightHours, 
PostflightLeg.ScheduledTM,
PostflightLeg.POLegID,
PostflightMain.POLogID,
PostflightMain.LoGNUM,
F.FleetID
FROM  (SELECT * FROM PostflightLeg WHERE ISDELETED=0)PostflightLeg INNER JOIN                    
PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND  PostflightMain.ISDELETED=0=0   
INNER JOIN                    
( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID 
FROM   Fleet F             
LEFT OUTER JOIN FleetGroupOrder FGO            
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID            
LEFT OUTER JOIN FleetGroup FG             
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''             
) F ON F.FleetID = PostflightMain.FleetID      
LEFT JOIN       
( SELECT Distinct D.DepartmentID,D.DepartmentCD     
FROM   Department D             
LEFT OUTER JOIN DepartmentGroupOrder DGO            
ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID            
LEFT OUTER JOIN DepartmentGroup DG             
ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID 
WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, '','')) OR @DepartmentGroupID = ''''                          
) D ON D.DepartmentID = PostflightMain.DepartmentID                
LEFT JOIN                    
FlightCatagory ON PostflightLeg.FlightCategoryID = FlightCatagory.FlightCategoryID     
LEFT JOIN
Company ON PostflightMain.HomebaseID = Company.HomebaseID 
LEFT JOIN
Airport ON Company.HomebaseAirportID =  Airport.AirportID             
WHERE PostflightMain.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                               
--  + ' AND Year(PostflightLeg.ScheduleDTTMLocal)=@Year ' 

--Construct OPTIONAL clauses

IF @Year <> ''
BEGIN
SET @SQLSCRIPT = @SQLSCRIPT + 'AND Year(PostflightLeg.ScheduleDTTMLocal) = @Year' 
END
ELSE
BEGIN
SET @SQLSCRIPT = @SQLSCRIPT + 'AND CONVERT(DATE,PostflightLeg.ScheduleDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)' 
END

IF @TailNum <> '' BEGIN                                
SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';                              
END                               

IF @DepartmentCD  <> '' BEGIN                                
SET @SQLSCRIPT = @SQLSCRIPT + ' AND D.DepartmentCD IN (''' + REPLACE(CASE WHEN RIGHT(@DepartmentCD , 1) = ',' THEN LEFT(@DepartmentCD, LEN(@DepartmentCD) - 1) ELSE @DepartmentCD END, ',', ''', ''') + ''')';                              
END                               

IF @FlightCatagoryCD  <> '' BEGIN                     
SET @SQLSCRIPT = @SQLSCRIPT + ' AND FlightCatagory.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';               

END                    
IF @HomeBase  <> '' BEGIN                                
SET @SQLSCRIPT = @SQLSCRIPT + ' AND Airport.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBase, 1) = ',' THEN LEFT(@HomeBase, LEN(@HomeBase) - 1) ELSE @HomeBase END, ',', ''', ''') + ''')';                              
END                
IF @HomeBaseOnly = 1 BEGIN            
SET @SQLSCRIPT = @SQLSCRIPT + ' AND PostflightMain.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));            
END               
SET @SQLSCRIPT = @SQLSCRIPT + ' GROUP BY PostflightLeg.ScheduleDTTMLocal, PostflightLeg.LegNUM, 
PostflightLeg.PassengerTotal,PostflightLeg.Distance,PostflightLeg.PassengerTotal,PostflightLeg.FlightHours,PostflightLeg.DutyTYPE,
PostflightLeg.ScheduledTM,PostflightLeg.POLegID,PostflightMain.POLogID,PostflightMain.LoGNUM,F.FleetID '

SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @Year As Int, @TailNum As VARCHAR(5000), @FleetGroupCD As VARCHAR(5000),                
@DepartmentCD As VARCHAR(5000),@DepartmentGroupID As VARCHAR(5000),@HomeBase aS VARCHAR(5000),                
@FlightCatagoryCD As VARCHAR(5000),@HomeBaseOnly Bit'                              

EXECUTE sp_executesql @SQLSCRIPT , @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @Year, @TailNum, @FleetGroupCD,@DepartmentCD,@DepartmentGroupID,@HomeBase,@FlightCatagoryCD,@HomeBaseOnly                     

PRINT @SQLSCRIPT
--select * from #TmpTble where MONTH(ScheduledTM)=1
IF OBJECT_ID('tempdb..#Tmp') is not null                        
DROP table #Tmp              
Declare @ItemDesc Varchar(50)               
Declare @Jan1 Numeric(10,2)=0
Declare @Feb1 Numeric(10,2)=0
Declare @Mar1 Numeric(10,2)=0
Declare @Apr1 Numeric(10,2)=0
Declare @May1 Numeric(10,2)=0
Declare @Jun1 Numeric(10,2)=0
Declare @Jul1 Numeric(10,2)=0
Declare @Aug1 Numeric(10,2)=0
Declare @Sep1 Numeric(10,2)=0
Declare @Oct1 Numeric(10,2)=0
Declare @Nov1 Numeric(10,2)=0
Declare @Dec1 Numeric(10,2)=0
Declare @YTD1 Numeric(10,2)=0

Declare @Jan2 Numeric(10,2)=0
Declare @Feb2 Numeric(10,2)=0
Declare @Mar2 Numeric(10,2)=0
Declare @Apr2 Numeric(10,2)=0
Declare @May2 Numeric(10,2)=0
Declare @Jun2 Numeric(10,2)=0
Declare @Jul2 Numeric(10,2)=0
Declare @Aug2 Numeric(10,2)=0
Declare @Sep2 Numeric(10,2)=0
Declare @Oct2 Numeric(10,2)=0
Declare @Nov2 Numeric(10,2)=0
Declare @Dec2 Numeric(10,2)=0
Declare @YTD2 Numeric(10,2)=0

Declare @TotPass Numeric(10,2)=0
Declare @TotLegs Numeric(10,2)=0
Declare @TotFltHrs Numeric(10,2)=0
Declare @IntlFltHrs Numeric(10,2)=0
Declare @DomFltHrs Numeric(10,2)=0

IF OBJECT_ID('tempdb..#TmpRep') is not null                        
DROP table #TmpRep         
CREATE TABLE #TmpRep (SerNo Int,ItemDesc Varchar(50),Jan1 Numeric(10,2), Feb1 Numeric(10,2), Mar1 Numeric(10,2), Apr1 Numeric(10,2), May1 Numeric(10,2), Jun1 Numeric(10,2),  
 Jul1 Numeric(10,2), Aug1 Numeric(10,2), Sep1 Numeric(10,2), Oct1 Numeric(10,2), Nov1 Numeric(10,2), Dec1 Numeric(10,2),YTD1 Numeric(10,2))


Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (1,'Trip Days',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 

Set @Jan1=0
Set @Feb1=0
Set @Mar1=0
Set @Apr1=0
Set @May1=0
Set @Jun1=0
Set @Jul1=0
Set @Aug1=0
Set @Sep1=0
Set @Oct1=0
Set @Nov1=0
Set @Dec1=0
Set @YTD1=0 

Set @Jan1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
Set @Feb1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
Set @Mar1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
Set @Apr1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
Set @May1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
Set @Jun1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
Set @Jul1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
Set @Aug1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
Set @Sep1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
Set @Oct1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
Set @Nov1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
Set @Dec1=(Select ISNull(SUM(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)
Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
Set @TotLegs=@YTD1 
Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (2,'Segments/Legs',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 


Set @Jan2=(Select ISNull(SUM(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
Set @Feb2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
Set @Mar2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
Set @Apr2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
Set @May2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
Set @Jun2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
Set @Jul2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
Set @Aug2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
Set @Sep2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
Set @Oct2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
Set @Nov2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
Set @Dec2=(Select ISNull(SUM(TotalPassengers) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)
Set @YTD2= @Jan2 + @Feb2 + @Mar2 + @Apr2 + @May2+ @Jun2 + @Jul2 + @Aug2 + @Sep2 + @Oct2 + @Nov2 + @Dec2
Set @TotPass =@YTD2 
Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (3,'Passengers',@Jan2,@Feb2,@Mar2,@Apr2,@May2,@Jun2,@Jul2,@Aug2,@Sep2,@Oct2,@Nov2,@Dec2,@YTD2) 

--Segment Legs LegCount/PaxCount
IF @Jan1 >0
Begin
Set @Jan2=@Jan2/@Jan1
End
IF @Feb1 >0
Begin
Set @Feb2=@Feb2/@Feb1
End
IF @Mar1 >0
Begin
Set @Mar2=@Mar2/@Mar1
End
IF @Apr1 >0
Begin
Set @Apr2=@Apr2/@Apr1
End
IF @May1 >0
Begin
Set @May2=@May2/@May1
End
IF @Jun1 >0
Begin
Set @Jun2=@Jun2/@Jun1
End
IF @Jul1 >0
Begin
Set @Jul2=@Jul2/@Jul1
End
IF @Aug1 >0
Begin
Set @Aug2=@Aug2/@Aug1
End
IF @Sep1 >0
Begin
Set @Sep2=@Sep2/@Sep1
End
IF @Oct1 >0
Begin
Set @Oct2=@Oct2/@OCt1
End
IF @Nov1 >0
Begin
Set @Nov2=@Nov2/@Nov1
End
IF @Dec1 >0
Begin
Set @Dec2=@Dec2/@Dec1
End  

Set @Jan1=0
Set @Feb1=0
Set @Mar1=0
Set @Apr1=0
Set @May1=0
Set @Jun1=0
Set @Jul1=0
Set @Aug1=0
Set @Sep1=0
Set @Oct1=0
Set @Nov1=0
Set @Dec1=0
Set @YTD1=0 

Set @Jan1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
Set @Feb1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
Set @Mar1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
Set @Apr1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
Set @May1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
Set @Jun1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
Set @Jul1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
Set @Aug1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
Set @Sep1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
Set @Oct1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
Set @Nov1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
Set @Dec1=(Select ISNull(SUM(TotalMilesFlown) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)
Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1

Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (4,'Miles Flown',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 

Set @YTD2=0
IF @TotLegs >0
Begin
Set @YTD2=@TotPass/@TotLegs
End

Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (5,'Avg Load Factor',@Jan2,@Feb2,@Mar2,@Apr2,@May2,@Jun2,@Jul2,@Aug2,@Sep2,@Oct2,@Nov2,@Dec2,@YTD2) 

Declare @TotFltPercHours Numeric(10,2)=0    
Declare @LineTotDom Numeric(10,2)=0 
Declare @LineTotInt Numeric(10,2)=0  

Set @Jan1=0
Set @Feb1=0
Set @Mar1=0
Set @Apr1=0
Set @May1=0
Set @Jun1=0
Set @Jul1=0
Set @Aug1=0
Set @Sep1=0
Set @Oct1=0
Set @Nov1=0
Set @Dec1=0 
Set @YTD1=0 

Set @Jan2=0
Set @Feb2=0
Set @Mar2=0
Set @Apr2=0
Set @May2=0
Set @Jun2=0
Set @Jul2=0
Set @Aug2=0
Set @Sep2=0
Set @Oct2=0
Set @Nov2=0
Set @Dec2=0 
Set @YTD2=0 

Set @Jan2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
Set @Feb2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
Set @Mar2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
Set @Apr2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
Set @May2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
Set @Jun2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
Set @Jul2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
Set @Aug2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
Set @Sep2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
Set @Oct2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
Set @Nov2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
Set @Dec2=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)

Set @Jan1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=1)
Set @Feb1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=1)
Set @Mar1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=1)
Set @Apr1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=1)
Set @May1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=1)
Set @Jun1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=1)
Set @Jul1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=1)
Set @Aug1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=1)
Set @Sep1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=1)
Set @Oct1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=1)
Set @Nov1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=1)
Set @Dec1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=1)
Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
Set @DomFltHrs =@YTD1 
Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (6,'Domestic Flt Hours',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 

IF @YTD1>0
BEGIN
IF @Jan1>0
Begin
Set @Jan1 =(@Jan1/@Jan2)*100
Set @LineTotDom=@LineTotDom+@Jan2
End   
IF @Feb1>0
Begin
Set @Feb1 =(@Feb1/@Feb2)*100
Set @LineTotDom=@LineTotDom+@Feb2
End   
IF @Mar1>0
Begin
Set @Mar1 =(@Mar1/@Mar2)*100 
Set @LineTotDom=@LineTotDom+@Mar2
End  
IF @Apr1>0
Begin
Set @Apr1 =(@Apr1/@Apr2)*100
Set @LineTotDom=@LineTotDom+@Apr2
End   
IF @May1>0
Begin
Set @May1 =(@May1/@May2)*100
Set @LineTotDom=@LineTotDom+@May2
End   
IF @Jun1>0
Begin
Set @Jun1 =(@Jun1/@Jun2)*100
Set @LineTotDom=@LineTotDom+@Jun2
End   
IF @Jul1>0
Begin
Set @Jul1 =(@Jul1/@Jul2)*100
Set @LineTotDom=@LineTotDom+@Jul2
End  
IF @Aug1>0
Begin
Set @Aug1 =(@Aug1/@Aug2)*100
Set @LineTotDom=@LineTotDom+@Aug2
End   
IF @Sep1>0
Begin
Set @Sep1 =(@Sep1/@Sep2)*100
Set @LineTotDom=@LineTotDom+@Sep2
End  
IF @Oct1>0
Begin
Set @Oct1 =(@Oct1/@Oct2)*100
Set @LineTotDom=@LineTotDom+@Oct2
End  
IF @Nov1>0
Begin
Set @Nov1 =(@Nov1/@Nov2)*100
Set @LineTotDom=@LineTotDom+@Nov2
End  
IF @Dec1>0
Begin
Set @Dec1 =(@Dec1/@Dec2)*100
Set @LineTotDom=@LineTotDom+@Dec2
End    
END 

Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (7,'Domestic Flt Hours %',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,0) 

Set @Jan1=0
Set @Feb1=0
Set @Mar1=0
Set @Apr1=0
Set @May1=0
Set @Jun1=0
Set @Jul1=0
Set @Aug1=0
Set @Sep1=0
Set @Oct1=0
Set @Nov1=0
Set @Dec1=0
Set @YTD1=0 

Set @Jan1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=2)
Set @Feb1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=2)
Set @Mar1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=2)
Set @Apr1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=2)
Set @May1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=2)
Set @Jun1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=2)
Set @Jul1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=2)
Set @Aug1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=2)
Set @Sep1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=2)
Set @Oct1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=2)
Set @Nov1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=2)
Set @Dec1=(Select ISNull(SUM(TotalFlightHours) ,0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=2)
Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
Set @IntlFltHrs=@YTD1  
Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (8,'Intl. Flt Hours',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 

Set @TotFltHrs= @DomFltHrs + @IntlFltHrs 

IF @YTD1>0
BEGIN
IF @Jan1>0
Begin
Set @Jan1 =(@Jan1/@Jan2)*100
Set @LineTotDom=@LineTotDom+@Jan2
End   
IF @Feb1>0
Begin
Set @Feb1 =(@Feb1/@Feb2)*100
Set @LineTotDom=@LineTotDom+@Feb2
End   
IF @Mar1>0
Begin
Set @Mar1 =(@Mar1/@Mar2)*100 
Set @LineTotDom=@LineTotDom+@Mar2
End  
IF @Apr1>0
Begin
Set @Apr1 =(@Apr1/@Apr2)*100
Set @LineTotDom=@LineTotDom+@Apr2
End   
IF @May1>0
Begin
Set @May1 =(@May1/@May2)*100
Set @LineTotDom=@LineTotDom+@May2
End   
IF @Jun1>0
Begin
Set @Jun1 =(@Jun1/@Jun2)*100
Set @LineTotDom=@LineTotDom+@Jun2
End   
IF @Jul1>0
Begin
Set @Jul1 =(@Jul1/@Jul2)*100
Set @LineTotDom=@LineTotDom+@Jul2
End  
IF @Aug1>0
Begin
Set @Aug1 =(@Aug1/@Aug2)*100
Set @LineTotDom=@LineTotDom+@Aug2
End   
IF @Sep1>0
Begin
Set @Sep1 =(@Sep1/@Sep2)*100
Set @LineTotDom=@LineTotDom+@Sep2
End  
IF @Oct1>0
Begin
Set @Oct1 =(@Oct1/@Oct2)*100
Set @LineTotDom=@LineTotDom+@Oct2
End  
IF @Nov1>0
Begin
Set @Nov1 =(@Nov1/@Nov2)*100
Set @LineTotDom=@LineTotDom+@Nov2
End  
IF @Dec1>0
Begin
Set @Dec1 =(@Dec1/@Dec2)*100
Set @LineTotDom=@LineTotDom+@Dec2
End    
END 

Insert Into #TmpRep(SerNo,ItemDesc ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
Values (9,'Intl. Flt Hours %',@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,0) 


IF @TotFltHrs >0
Begin
Update #TmpRep Set YTD1=(@DomFltHrs/@TotFltHrs)*100 Where SerNo=7
Update #TmpRep Set YTD1=(@IntlFltHrs/@TotFltHrs)*100 Where SerNo=9
End
----Trip Days
Set @Jan1=0
Set @Feb1=0
Set @Mar1=0
Set @Apr1=0
Set @May1=0
Set @Jun1=0
Set @Jul1=0
Set @Aug1=0
Set @Sep1=0
Set @Oct1=0
Set @Nov1=0
Set @Dec1=0 
Set @YTD1=0 

Set @Jan2=0
Set @Feb2=0
Set @Mar2=0
Set @Apr2=0
Set @May2=0
Set @Jun2=0
Set @Jul2=0
Set @Aug2=0
Set @Sep2=0
Set @Oct2=0
Set @Nov2=0
Set @Dec2=0 
Set @YTD2=0 

DECLARE @DepDate DateTIme
DECLARE @ArrDate DateTime
DECLARE @FleetID BigInt
DECLARE @LogNum BigInt
DECLARE @TempLog TABLE(LogNUM BigInt) 
DECLARE @TripDays INT
DECLARE @TotTripDays INT=0
DECLARE @TempDate TABLE(FleetDate varchar(30),TripDate DATETIME) 

DECLARE @TempFLT TABLE(FleetID BigInt) 
Insert Into @TempFLT Select Distinct FleetID From #TmpTble 
WHILE ( SELECT  COUNT(*) FROM @TempFLT ) > 0             
BEGIN        
Select Top 1 @FleetID=FleetID From @TempFLT

Delete From @TempLog 
Insert Into @TempLog Select Distinct LoGNUM From #TmpTble Where FleetID=@FleetID 
---
WHILE ( SELECT  COUNT(*) FROM @TempLog  ) > 0             
BEGIN        
Select Top 1 @LogNum=LogNum From @TempLog  
---

Select  @DepDate=MIN(ScheduledTM),@ArrDate=MAX(ScheduledTM) From #TmpTble Where FleetID=@FleetID And LogNum=@LogNum ; 

WITH date_range (calc_date) AS (
SELECT DATEADD(DAY, DATEDIFF(DAY, 0, @ArrDate) - DATEDIFF(DAY, @DepDate, @ArrDate), 0)
UNION ALL SELECT DATEADD(DAY, 1, calc_date)  FROM date_range WHERE DATEADD(DAY, 1, calc_date) <= @ArrDate)
Insert Into @TempDate Select convert(varchar(25),@FleetID)+convert(varchar(10),calc_date,101),calc_date FROM date_range;

--  select CONVERT(varchar(10),getdate(),101)
--

Delete From @TempLog Where LogNum=@LogNum
END      
-- 
Delete From @TempFLT Where FleetID=@FleetID
END

Set @Jan1= (Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=1)   
Set @Feb1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=2)   
Set @Mar1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=3)   
Set @Apr1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=4)   
Set @May1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=5)   
Set @Jun1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=6)   
Set @Jul1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=7)   
Set @Aug1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=8)   
Set @Sep1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=9)   
Set @Oct1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=10)   
Set @Nov1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=11)   
Set @Dec1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=12)   
Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1

Update #TmpRep Set Jan1=@Jan1,Feb1=@Feb1,Mar1=@Mar1,Apr1=@Apr1,May1=@May1,Jun1=@Jun1,
Jul1=@Jul1,Aug1=@Aug1,Sep1=@Sep1,Oct1=@Oct1,Nov1=@Nov1,Dec1=@Dec1,YTD1=@YTD1 Where SerNo=1


DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
AND IsZeroSuppressActivityAircftRpt IS NOT NULL
AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) 
----Trip Days end
-- SET @SuppressActivityAircraft=1

IF @SuppressActivityAircraft=0
BEGIN
Select SerNo,ItemDesc, Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin From #TmpRep  
END
ELSE
BEGIN
SELECT   SerNo,ItemDesc, Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin 
From #TmpRep 
WHERE (Jan1 >0
OR Feb1 >0
OR Mar1 >0
OR Apr1 >0
OR May1 >0
OR Jun1 >0
OR Jul1 >0
OR Aug1 >0
OR Sep1 >0
OR Oct1 >0
OR Nov1 >0
OR Dec1 >0
OR YTD1 >0)


END

IF OBJECT_ID('tempdb..#Tmp') is not null                        
DROP table #Tmp 
IF OBJECT_ID('tempdb..#TmpRep') is not null                        
DROP table #TmpRep                             
IF OBJECT_ID('tempdb..#TmpTble') is not null                        
DROP table #TmpTble

--Exec spGetReportPOSTFleetAnnualStatisticsFlightAddlInformation1 'SUPERVISOR_99',2009,'','','','','','','' 
--Exec spGetReportPOSTFleetAnnualStatisticsFlightAddlInformation1 'SUPERVISOR_99','2009-1-1','2010-1-1','','','','','','',''                         
                       

END 


GO


