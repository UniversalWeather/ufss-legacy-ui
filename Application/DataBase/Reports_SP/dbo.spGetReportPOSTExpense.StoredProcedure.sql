IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTExpense]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTExpense]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
 --EXEC spGetReportPOSTExpense '10013','','2011-5-1','2011-5-15', '','', '', 0
CREATE PROCEDURE [dbo].[spGetReportPOSTExpense]   
    @UserCustomerID AS VARCHAR(30),  
    @UserHomebaseID AS VARCHAR(30),  
    @DATEFROM DATETIME = NULL,     
    @DATETO DATETIME = NULL, 
    @MultipleAccountNum AS VARCHAR(500)= '',  
    @LogNum AS VARCHAR(30)= '',  
    @SlipNUM AS VARCHAR(30)= '',  
    @IsHomebase AS BIT = 0  
AS  
BEGIN  
--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
SET NOCOUNT ON;  
 IF @LogNum <> ''
	BEGIN      
		SELECT DISTINCT   
			 PE.SlipNUM,  
			 PM.LogNum,  
			 PL.LegNUM,  
			 AC.AccountDescription,  
			 PE.PurchaseDT,  
			 AP.IcaoID,  
			 F.FBOVendor AS [FBOName],  
			 PE.FuelQTY,  
			 unitprice=FLOOR(Pe.UnitPrice *10000)*0.0001 ,  
			 PE.FederalTAX,  
			 PE.SateTAX,  
			 PE.SaleTAX,  
			 PE.ExpenseAMT      
			FROM PostflightMain PM  
			 JOIN PostflightExpense PE   
			   ON PM.POLogID = PE.POLogID AND PE.IsDeleted=0 -- AND PM.CustomerID = PE.CustomerID  
			  JOIN PostflightLeg PL ON PL.POLegID = PE.POLegID AND PL.IsDeleted=0  
			 LEFT JOIN FBO F  
			   ON PE.FBOID = F.FBOID -- AND PE.CustomerID = F.CustomerID  
			 LEFT JOIN Airport AP  
			   ON PE.AirportID = AP.AirportID   
			 LEFT JOIN Account AC  
			   ON PE.AccountID = AC.AccountID --  AND PE.CustomerID = AC.CustomerID  
			 
		   WHERE (AC.AccountNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MultipleAccountNum, ','))OR @MultipleAccountNum='')  
				 AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ','))OR @LogNum='')  
				 AND (PE.SlipNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SlipNUM, ','))OR @SlipNUM='')           
				 AND (PE.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)  
				 --AND (PE.PurchaseDT BETWEEN @DATEFROM And @DATETO)  
				 AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)  
				 AND PM.IsDeleted=0  
		   ORDER BY PE.SlipNUM, PE.PurchaseDT  
    END  
ELSE
	BEGIN
			SELECT DISTINCT   
			 PE.SlipNUM,  
			 PM.LogNum,  
			 PL.LegNUM,  
			 AC.AccountDescription,  
			 PE.PurchaseDT,  
			 AP.IcaoID,  
			 F.FBOVendor AS [FBOName],  
			 PE.FuelQTY,  
			 unitprice=FLOOR(Pe.UnitPrice *10000)*0.0001 ,  
			 PE.FederalTAX,  
			 PE.SateTAX,  
			 PE.SaleTAX,  
			 PE.ExpenseAMT      
			FROM PostflightMain PM  
			 JOIN PostflightExpense PE   
			   ON PM.POLogID = PE.POLogID AND PE.IsDeleted=0 -- AND PM.CustomerID = PE.CustomerID  
			  JOIN PostflightLeg PL ON PL.POLegID = PE.POLegID AND PL.IsDeleted=0  
			 LEFT JOIN FBO F  
			   ON PE.FBOID = F.FBOID -- AND PE.CustomerID = F.CustomerID  
			 LEFT JOIN Airport AP  
			   ON PE.AirportID = AP.AirportID   
			 LEFT JOIN Account AC  
			   ON PE.AccountID = AC.AccountID --  AND PE.CustomerID = AC.CustomerID  
			 
		   WHERE (AC.AccountNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MultipleAccountNum, ','))OR @MultipleAccountNum='')  
				-- AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ','))OR @LogNum='')  
				 AND (PE.SlipNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SlipNUM, ','))OR @SlipNUM='')           
				 AND (PE.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)  
				 AND (PE.PurchaseDT BETWEEN @DATEFROM And @DATETO)  
				 AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)  
				 AND PM.IsDeleted=0  
		   ORDER BY PE.SlipNUM, PE.PurchaseDT  

	
	END

END
GO