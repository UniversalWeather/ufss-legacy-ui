IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor1MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor1MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportPassengerRequestor1MhtmlInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD VARCHAR(500)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor1MhtmlInformation
-- Author: A.Akhila
-- Create date: 28 Jun 2012
-- Description: Get Passenger/CrewPassengerVisa Mhtml Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON

			SELECT
				[paxcode] = P.PassengerRequestorCD
				,[paxname] = P.PassengerName
				,[dept_code] = D.DepartmentCD
				,[dept_desc] = P.PassengerDescription
				,[phone] = P.AdditionalPhoneNum
				,[emp_t_cng] = P.IsEmployeeType
				,[stdbilling] = P.StandardBilling
				,[auth_code] = DA.AuthorizationCD
				,[auth_desc] = P.AuthorizationDescription
				,[PaxNotes] = P.Notes
				,[last_name] = P.LastName
				,[first_name] = P.FirstName
				,[middleinit] = P.MiddleInitial
				,[dob] = P.DateOfBirth
				,[client] = C.ClientCD
				,[assoc_with] = P.AssociatedWithCD
				,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
				,[active] = P.IsActive
				,[fltpurpose] = F.FlightPurposeCD
				,[ssn] = P.SSN
				,[title] = P.Title
				,[salarylvl] = P.SalaryLevel
				,[paxtype] = P.IsPassengerType
				,[lastuser] = P.LastUpdUID
				,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
				,[sscoord] = P.IsScheduledServiceCoord
				,[cmpyname] = P.CompanyName
				,[empld_id] = P.EmployeeID
				,[fax] = P.BusinessFax
				,[email] = P.EmailAddress
				--,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
				,[pin_no] = ISNULL(P.PersonalIDNum,'')
				,[spouse_dep] = P.IsSpouseDependant
				,[acctnum] = A.AccountNum
				,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
				,[siflsec] = P.IsSIFL
				,[gender] = P.Gender
				,[addlphone] = P.AdditionalPhoneNum
				,[requestor] = P.IsRequestor
				,[ctryresid] = CN.CountryCD
				,[tsastat] = P.TSAStatus
				,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
				,[addr1] = P.Addr1
				,[addr2] = P.Addr2
				,[city] = P.City
				,[state] = P.StateName
				,[zipcode] = P.PostalZipCD
				,[isdeleted] = P.IsDeleted
				,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming
				,[adcd] = ''
				,[adcddesc] = ''
				,[adcdvalue] = ''
				,[ppnum] = ''
				,[ppexpire] = ''
				,[choice] = 0
				,[ppnation] = ''
				,[issueplc] = ''
				,[visanum] = ''
				,[country] = ''
				,[expiredt] = ''
				,[NOTES] = ''
				,[name] = CM.CompanyName
				,[section] = 0 
				--[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info
				,[paxalerts]=P.PassengerAlert
				,[paxscandoc]=P.PaxScanDoc
			FROM Passenger P
			LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD FROM Department
			) D ON P.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client 
			) C ON P.ClientID = C.ClientID
			LEFT OUTER JOIN (
				SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose 
			) F ON P.FlightPurposeID = F.FlightPurposeID
			LEFT OUTER JOIN (
				SELECT AccountID, AccountNum FROM Account
			) A ON P.AccountID = A.AccountID
			LEFT OUTER JOIN (
				SELECT CountryID, CountryCD FROM Country
			) CN ON P.CountryOfResidenceID = CN.CountryID
			LEFT OUTER JOIN (
			    SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company
			) CM ON P.HomebaseID = CM.HomebaseAirportID
		WHERE P.IsDeleted = 0
		
			--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
			AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND P.PassengerRequestorCD IN (
				select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')
			)

	UNION ALL

		SELECT
			[paxcode] = P.PassengerRequestorCD
			,[paxname] = P.PassengerName
			,[dept_code] = D.DepartmentCD
			,[dept_desc] = P.PassengerDescription
			,[phone] = P.AdditionalPhoneNum
			,[emp_t_cng] = P.IsEmployeeType
			,[stdbilling] = P.StandardBilling
			,[auth_code] = DA.AuthorizationCD
			,[auth_desc] = P.AuthorizationDescription
			,[PaxNotes] = P.Notes
			,[last_name] = P.LastName
			,[first_name] = P.FirstName
			,[middleinit] = P.MiddleInitial
			,[dob] = P.DateOfBirth
			,[client] = C.ClientCD
			,[assoc_with] = P.AssociatedWithCD
			,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
			,[active] = P.IsActive
			,[fltpurpose] = F.FlightPurposeCD
			,[ssn] = P.SSN
			,[title] = P.Title
			,[salarylvl] = P.SalaryLevel
			,[paxtype] = P.IsPassengerType
			,[lastuser] = P.LastUpdUID
			,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[sscoord] = P.IsScheduledServiceCoord
			,[cmpyname] = P.CompanyName
			,[empld_id] = P.EmployeeID
			,[fax] = P.BusinessFax
			,[email] = P.EmailAddress
			--,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
			,[pin_no] = ISNULL(P.PersonalIDNum,'')
			,[spouse_dep] = P.IsSpouseDependant
			,[acctnum] = A.AccountNum
			,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
			,[siflsec] = P.IsSIFL
			,[gender] = P.Gender
			,[addlphone] = P.AdditionalPhoneNum
			,[requestor] = P.IsRequestor
			,[ctryresid] = CN.CountryCD
			,[tsastat] = P.TSAStatus
			,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[addr1] = P.Addr1
			,[addr2] = P.Addr2
			,[city] = P.City
			,[state] = P.StateName
			,[zipcode] = P.PostalZipCD
			,[isdeleted] = P.IsDeleted
			,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming
			,[adcd] =  PAI.AdditionalINFOCD --Coming from PAXDATA.DBF
			,[adcddesc] = PAI.AdditionalINFODescription --Coming from PAXDATA.DBF
			--,[adcdvalue] = dbo.FlightPakDecrypt(ISNULL(PAI.AdditionalINFOValue,'')) --Coming from PAXDATA.DBF
			,[adcdvalue] = ISNULL(PAI.AdditionalINFOValue,'') --Coming from PAXDATA.DBF
			,[ppnum] = ''
			,[ppexpire] = ''
			,[choice] = 0
			,[ppnation] = ''
			,[issueplc] = ''
			,[visanum] = ''
			,[country] = ''
			,[expiredt] = ''
			,[NOTES] = ''
			,[name] = CM.CompanyName
			,[section] = 1 
			--[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info
			,[paxalerts]=P.PassengerAlert
				,[paxscandoc]=P.PaxScanDoc
		FROM Passenger P
			INNER JOIN PassengerAdditionalInfo PAI
				ON P.CustomerID = PAI.CustomerID
				AND P.PassengerRequestorID = PAI.PassengerRequestorID
			LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD FROM Department
			) D ON P.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client 
			) C ON P.ClientID = C.ClientID
			LEFT OUTER JOIN (
				SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose 
			) F ON P.FlightPurposeID = F.FlightPurposeID
			LEFT OUTER JOIN (
				SELECT AccountID, AccountNum FROM Account
			) A ON P.AccountID = A.AccountID
			LEFT OUTER JOIN (
				SELECT CountryID, CountryCD FROM Country
			) CN ON P.CountryOfResidenceID = CN.CountryID
			LEFT OUTER JOIN (
			    SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company
			) CM ON P.HomebaseID = CM.HomebaseAirportID
		WHERE PAI.IsDeleted = 0
			--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
			AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND P.PassengerRequestorCD IN (
				select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')
			)
			
	UNION ALL

		SELECT
			[paxcode] = P.PassengerRequestorCD
			,[paxname] = P.PassengerName
			,[dept_code] = D.DepartmentCD
			,[dept_desc] = P.PassengerDescription
			,[phone] = P.AdditionalPhoneNum
			,[emp_t_cng] = P.IsEmployeeType
			,[stdbilling] = P.StandardBilling
			,[auth_code] = DA.AuthorizationCD
			,[auth_desc] = P.AuthorizationDescription
			,[PaxNotes] = P.Notes
			,[last_name] = P.LastName
			,[first_name] = P.FirstName
			,[middleinit] = P.MiddleInitial
			,[dob] = P.DateOfBirth
			,[client] = C.ClientCD
			,[assoc_with] = P.AssociatedWithCD
			,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
			,[active] = P.IsActive
			,[fltpurpose] = F.FlightPurposeCD
			,[ssn] = P.SSN
			,[title] = P.Title
			,[salarylvl] = P.SalaryLevel
			,[paxtype] = P.IsPassengerType
			,[lastuser] = P.LastUpdUID
			,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[sscoord] = P.IsScheduledServiceCoord
			,[cmpyname] = P.CompanyName
			,[empld_id] = P.EmployeeID
			,[fax] = P.BusinessFax
			,[email] = P.EmailAddress
			--,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
			,[pin_no] = ISNULL(P.PersonalIDNum,'')
			,[spouse_dep] = P.IsSpouseDependant
			,[acctnum] = A.AccountNum
			,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
			,[siflsec] = P.IsSIFL
			,[gender] = P.Gender
			,[addlphone] = P.AdditionalPhoneNum
			,[requestor] = P.IsRequestor
			,[ctryresid] = CN.CountryCD
			,[tsastat] = P.TSAStatus
			,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[addr1] = P.Addr1
			,[addr2] = P.Addr2
			,[city] = P.City
			,[state] = P.StateName
			,[zipcode] = P.PostalZipCD
			,[isdeleted] = P.IsDeleted
			,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming
			,[adcd] = ''
			,[adcddesc] = ''
			,[adcdvalue] = ''
			--,[ppnum] = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNUM,'')) --Coming from PAXPASS.DBF
			,[ppnum] = ISNULL(CPP.PassportNUM,'') --Coming from PAXPASS.DBF
			,[ppexpire] = CONVERT(varchar(10), CPP.PassportExpiryDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) --Coming from PAXPASS.DBF
			,[choice] = CPP.Choice --Coming from PAXPASS.DBF
			,[ppnation] = dbo.GetCountryCDByCountryID(CPP.CountryID)
			,[issueplc] = CPP.IssueCity --Coming from PAXPASS.DBF
			,[visanum] = ''
			,[country] = ''
			,[expiredt] = ''
			,[NOTES] = ''
			,[name] = CM.CompanyName
			,[section] = 2 
			--[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info
			,[paxalerts]=P.PassengerAlert
				,[paxscandoc]=P.PaxScanDoc
		FROM Passenger P
			INNER JOIN CrewPassengerPassport CPP
					ON P.CustomerID = CPP.CustomerID
					AND P.PassengerRequestoriD = CPP.PassengerRequestoriD
			LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD FROM Department
			) D ON P.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client 
			) C ON P.ClientID = C.ClientID
			LEFT OUTER JOIN (
				SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose 
			) F ON P.FlightPurposeID = F.FlightPurposeID
			LEFT OUTER JOIN (
				SELECT AccountID, AccountNum FROM Account
			) A ON P.AccountID = A.AccountID
			LEFT OUTER JOIN (
				SELECT CountryID, CountryCD FROM Country
			) CN ON P.CountryOfResidenceID = CN.CountryID
			LEFT OUTER JOIN (
			    SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company
			) CM ON P.HomebaseID = CM.HomebaseAirportID
		WHERE CPP.IsDeleted = 0
			--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
			AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND P.PassengerRequestorCD IN (
				select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')
			)
			
	UNION ALL

			SELECT
				[paxcode] = P.PassengerRequestorCD
				,[paxname] = P.PassengerName
				,[dept_code] = D.DepartmentCD
				,[dept_desc] = P.PassengerDescription
				,[phone] = P.AdditionalPhoneNum
				,[emp_t_cng] = P.IsEmployeeType
				,[stdbilling] = P.StandardBilling
				,[auth_code] = DA.AuthorizationCD
				,[auth_desc] = P.AuthorizationDescription
				,[PaxNotes] = P.Notes
				,[last_name] = P.LastName
				,[first_name] = P.FirstName
				,[middleinit] = P.MiddleInitial
				,[dob] = P.DateOfBirth
				,[client] = C.ClientCD
				,[assoc_with] = P.AssociatedWithCD
				,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
				,[active] = P.IsActive
				,[fltpurpose] = F.FlightPurposeCD
				,[ssn] = P.SSN
				,[title] = P.Title
				,[salarylvl] = P.SalaryLevel
				,[paxtype] = P.IsPassengerType
				,[lastuser] = P.LastUpdUID
				,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
				,[sscoord] = P.IsScheduledServiceCoord
				,[cmpyname] = P.CompanyName
				,[empld_id] = P.EmployeeID
				,[fax] = P.BusinessFax
				,[email] = P.EmailAddress
				--,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
				,[pin_no] = ISNULL(P.PersonalIDNum,'')
				,[spouse_dep] = P.IsSpouseDependant
				,[acctnum] = A.AccountNum
				,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
				,[siflsec] = P.IsSIFL
				,[gender] = P.Gender
				,[addlphone] = P.AdditionalPhoneNum
				,[requestor] = P.IsRequestor
				,[ctryresid] = CN.CountryCD
				,[tsastat] = P.TSAStatus
				,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
				,[addr1] = P.Addr1
				,[addr2] = P.Addr2
				,[city] = P.City
				,[state] = P.StateName
				,[zipcode] = P.PostalZipCD
				,[isdeleted] = P.IsDeleted
				,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming
				,[adcd] = ''
				,[adcddesc] = ''
				,[adcdvalue] = ''
				,[ppnum] = ''
				,[ppexpire] = ''
				,[choice] = 0
				,[ppnation] = ''
				,[issueplc] = ''
				,[visanum] = ''
				,[country] = ''
				,[expiredt] = ''
				,[NOTES] = ''
				,[name] = CM.CompanyName
				,[section] = 3 
				--[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info
				,[paxalerts]=P.PassengerAlert
				,[paxscandoc]=P.PaxScanDoc
			FROM Passenger P
			LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD FROM Department
			) D ON P.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client 
			) C ON P.ClientID = C.ClientID
			LEFT OUTER JOIN (
				SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose 
			) F ON P.FlightPurposeID = F.FlightPurposeID
			LEFT OUTER JOIN (
				SELECT AccountID, AccountNum FROM Account
			) A ON P.AccountID = A.AccountID
			LEFT OUTER JOIN (
				SELECT CountryID, CountryCD FROM Country
			) CN ON P.CountryOfResidenceID = CN.CountryID
			LEFT OUTER JOIN (
			    SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company
			) CM ON P.HomebaseID = CM.HomebaseAirportID
		WHERE P.IsDeleted = 0
			--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
			AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND P.PassengerRequestorCD IN (
				select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')
			)
			
	UNION ALL

		SELECT
			[paxcode] = P.PassengerRequestorCD
			,[paxname] = P.PassengerName
			,[dept_code] = D.DepartmentCD
			,[dept_desc] = P.PassengerDescription
			,[phone] = P.AdditionalPhoneNum
			,[emp_t_cng] = P.IsEmployeeType
			,[stdbilling] = P.StandardBilling
			,[auth_code] = DA.AuthorizationCD
			,[auth_desc] = P.AuthorizationDescription
			,[PaxNotes] = P.Notes
			,[last_name] = P.LastName
			,[first_name] = P.FirstName
			,[middleinit] = P.MiddleInitial
			,[dob] = P.DateOfBirth
			,[client] = C.ClientCD
			,[assoc_with] = P.AssociatedWithCD
			,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
			,[active] = P.IsActive
			,[fltpurpose] = F.FlightPurposeCD
			,[ssn] = P.SSN
			,[title] = P.Title
			,[salarylvl] = P.SalaryLevel
			,[paxtype] = P.IsPassengerType
			,[lastuser] = P.LastUpdUID
			,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[sscoord] = P.IsScheduledServiceCoord
			,[cmpyname] = P.CompanyName
			,[empld_id] = P.EmployeeID
			,[fax] = P.BusinessFax
			,[email] = P.EmailAddress
			--,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
			,[pin_no] = ISNULL(P.PersonalIDNum,'')
			,[spouse_dep] = P.IsSpouseDependant
			,[acctnum] = A.AccountNum
			,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
			,[siflsec] = P.IsSIFL
			,[gender] = P.Gender
			,[addlphone] = P.AdditionalPhoneNum
			,[requestor] = P.IsRequestor
			,[ctryresid] = CN.CountryCD
			,[tsastat] = P.TSAStatus
			,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[addr1] = P.Addr1
			,[addr2] = P.Addr2
			,[city] = P.City
			,[state] = P.StateName
			,[zipcode] = P.PostalZipCD
			,[isdeleted] = P.IsDeleted
			,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming
			,[adcd] = ''
			,[adcddesc] = ''
			,[adcdvalue] = ''
			,[ppnum] = ''
			,[ppexpire] = ''
			,[choice] = 0
			,[ppnation] = ''
			,[issueplc] = ''
			--,[visanum] = dbo.FlightPakDecrypt(ISNULL(CPV.VisaNum,''))
			,[visanum] = ISNULL(CPV.VisaNum,'')
			,[country] = dbo.GetCountryCDByCountryID(CPV.CountryID)
			,[expiredt] = CONVERT(varchar(10), CPV.ExpiryDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[NOTES] = CPV.Notes
			,[name] = CM.CompanyName
			,[section] = 4 
			--[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info
			,[paxalerts]=P.PassengerAlert
				,[paxscandoc]=P.PaxScanDoc
		FROM Passenger P
			INNER JOIN CrewPassengerVisa CPV
				ON P.CustomerID = CPV.CustomerID
				AND P.PassengerRequestorID = CPV.PassengerRequestorID
			LEFT OUTER JOIN (
				SELECT DepartmentID, DepartmentCD FROM Department
			) D ON P.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN (
				SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization
			) DA ON P.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN (
				SELECT ClientID, ClientCD FROM Client 
			) C ON P.ClientID = C.ClientID
			LEFT OUTER JOIN (
				SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose 
			) F ON P.FlightPurposeID = F.FlightPurposeID
			LEFT OUTER JOIN (
				SELECT AccountID, AccountNum FROM Account
			) A ON P.AccountID = A.AccountID
			LEFT OUTER JOIN (
				SELECT CountryID, CountryCD FROM Country
			) CN ON P.CountryOfResidenceID = CN.CountryID
			LEFT OUTER JOIN (
			    SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company
			) CM ON P.HomebaseID = CM.HomebaseAirportID
		WHERE CPV.IsDeleted = 0
			--AND P.PassengerRequestorCD = RTRIM(@PassengerCD)
			AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND P.PassengerRequestorCD IN (
				select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')
			)
-- EXEC spGetReportPassengerRequestor1MhtmlInformation 'tim', 'WKOKA,ECJ,MT01,HKIOE,EJM01'
-- EXEC spGetReportPassengerRequestor1MhtmlInformation 'SUPERVISOR_99', 'EXEC2,GUES2'


GO


