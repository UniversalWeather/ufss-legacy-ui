IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDomesticInternationalSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDomesticInternationalSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTDomesticInternationalSummaryInformation]
 (
 @UserCD AS VARCHAR(30),
 @YEAR AS INT,       
 @TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
 @FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
 @IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase 
 @PrintFlightHours as varchar(1)
 
 )
AS         
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTDomesticInternationalSummaryInformation]  
-- Author: Avanikanta Pradhan  
-- Create date: 13 aug 2012  
-- Description: Get Domestic and International summary REPORTS  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================ 
 SET NOCOUNT ON 
 
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
  
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	---LEFT OUTER JOIN FleetGroupOrder FGO
	--ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	--LEFT OUTER JOIN FleetGroup FG 
	--ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	---AND 
	F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration----------------------
	BEGIN                                
	DECLARE @TenToMin Int
	SET @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))                                  
	DECLARE @ParameterDefinition AS NVARCHAR(400) 
	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '' 
	

CREATE TABLE #TmpTble 
(
	Tot Numeric(10,2)
	,TailNum Varchar(9)
	,ScheduledTM DateTime
	,Airport CHAR(1)
	,DomIntDutyType BIGINT
)                             
INSERT INTO #TmpTble
		SELECT 
		CASE (@PrintFlightHours) WHEN 'A' THEN  IsNull(SUM(ROUND(PostflightLeg.FlightHours,1)),0) ELSE IsNull(SUM(ROUND(PostflightLeg.BlockHours,1)),0) END Tot
		,F.TailNum
		,PostflightLeg.ScheduledTM
		,'D' Airport
		,DomIntDutyType = sum(CASE WHEN PostflightLeg.DutyTYPE = 2 THEN 1 ELSE 0 END)
		FROM PostflightLeg
		INNER JOIN PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0
		INNER JOIN
		(
			SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID
			FROM Fleet F
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
			--WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = ''
		) F ON F.FleetID = PostflightMain.FleetID
		LEFT JOIN Company ON PostflightMain.HomebaseID = Company.HomebaseID
		LEFT JOIN Airport ON Company.HomebaseAirportID =  Airport.AirportID
		INNER JOIN
		(
		SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
		WHERE PostflightMain.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		AND Year(PostflightLeg.ScheduledTM)=@Year AND DutyType=2
		AND PostflightLeg.IsDeleted=0
		-- AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
		AND (PostflightMain.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)   
		GROUP BY PostflightLeg.FlightHours, F.TailNum,PostflightLeg.ScheduledTM,PostflightLeg.DutyType           
	UNION ALL
		SELECT CASE (@PrintFlightHours) WHEN 'A' THEN  IsNull(SUM(ROUND(PostflightLeg.FlightHours,1)),0) ELSE IsNull(SUM(ROUND(PostflightLeg.BlockHours,1)),0) END Tot
		,F.TailNum 
		,PostflightLeg.ScheduledTM
		,'I' Airport
		,DomIntDutyType = sum(CASE WHEN PostflightLeg.DutyTYPE = 1 THEN 1 ELSE 0 END)
		FROM PostflightLeg  
		INNER JOIN PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0    
		INNER JOIN
		(
			SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID
			FROM Fleet F
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
			-- WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = ''
		) F ON F.FleetID = PostflightMain.FleetID
		LEFT JOIN Company ON PostflightMain.HomebaseID = Company.HomebaseID
		LEFT JOIN Airport ON Company.HomebaseAirportID =  Airport.AirportID
		INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID                  
		WHERE PostflightMain.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                               
		AND Year(PostflightLeg.ScheduledTM)=@Year AND DutyType=1
		AND PostflightLeg.IsDeleted=0
		-- AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
		AND (PostflightMain.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)    
		GROUP BY  PostflightLeg.BlockHours, F.TailNum,PostflightLeg.ScheduledTM,PostflightLeg.DutyType

CREATE TABLE #DUTY
(
[Department] NVARCHAR(100)
,[DepartmentD] NVARCHAR(100)
,[Authorization] NVARCHAR(100)
,[AuthorizationD] NVARCHAR(100)
,[TailNumber] VARCHAR(9)
,[AIRCRAFTBASIS] NUMERIC(1,0)
,[FlightHoursDom] NUMERIC(8,3)
,[FlightHoursIntl] NUMERIC(8,3)
,[LogNum] BIGINT
,[DutyType] NUMERIC(10,0)
,[ScheduledTM] BIGINT
)
INSERT INTO #DUTY
SELECT DISTINCT
[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
,[DepartmentD] =D.DepartmentName            
,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
,[AuthorizationD] = DA.DeptAuthDescription
,[TailNumber] = ISNULL(F.TailNum,'')
,[AIRCRAFTBASIS] = C.AircraftBasis	
,[FlightHoursDom] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN (CASE WHEN C.AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END) OVER (PARTITION BY POM.LogNum)
,[FlightHoursIntl] = SUM(CASE WHEN POL.DutyTYPE > 1 THEN (CASE WHEN C.AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END) OVER (PARTITION BY POM.LogNum)
,[LogNum] = pom.LogNum
,[DutyType] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN 0 ELSE (CASE WHEN POL.DutyTYPE = 2 THEN 1 ELSE NULL END)END)OVER (PARTITION BY POM.LogNum)
,[ScheduledTM] = MONTH(POL.ScheduledTM)
FROM FLEET F 
LEFT OUTER JOIN PostflightMain POM ON POM.FleetID = F.FleetID AND POM.IsDeleted=0
LEFT OUTER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
LEFT OUTER JOIN Company C ON C.HomebaseID = POM.HomebaseID
LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND Year(POL.ScheduledTM)=@Year
ORDER BY ScheduledTM,lognum

--SELECT * FROM #DUTY  
           
  Declare @TailNumber Varchar(9)               
  Declare @Jan1 Numeric(10,2)=0
  Declare @Feb1 Numeric(10,2)=0
  Declare @Mar1 Numeric(10,2)=0
  Declare @Apr1 Numeric(10,2)=0
  Declare @May1 Numeric(10,2)=0
  Declare @Jun1 Numeric(10,2)=0
  Declare @Jul1 Numeric(10,2)=0
  Declare @Aug1 Numeric(10,2)=0
  Declare @Sep1 Numeric(10,2)=0
  Declare @Oct1 Numeric(10,2)=0
  Declare @Nov1 Numeric(10,2)=0
  Declare @Dec1 Numeric(10,2)=0
  Declare @YTD1 Numeric(10,2)=0
  
  Declare @Jan2 Numeric(10,2)=0
  Declare @Feb2 Numeric(10,2)=0
  Declare @Mar2 Numeric(10,2)=0
  Declare @Apr2 Numeric(10,2)=0
  Declare @May2 Numeric(10,2)=0
  Declare @Jun2 Numeric(10,2)=0
  Declare @Jul2 Numeric(10,2)=0
  Declare @Aug2 Numeric(10,2)=0
  Declare @Sep2 Numeric(10,2)=0
  Declare @Oct2 Numeric(10,2)=0
  Declare @Nov2 Numeric(10,2)=0
  Declare @Dec2 Numeric(10,2)=0
  Declare @YTD2 Numeric(10,2)=0
  
  Declare @JanT1 BIGINT=0
  Declare @FebT1 BIGINT=0
  Declare @MarT1 BIGINT=0
  Declare @AprT1 BIGINT=0
  Declare @MayT1 BIGINT=0
  Declare @JunT1 BIGINT=0
  Declare @JulT1 BIGINT=0
  Declare @AugT1 BIGINT=0
  Declare @SepT1 BIGINT=0
  Declare @OctT1 BIGINT=0
  Declare @NovT1 BIGINT=0
  Declare @DecT1 BIGINT=0
  Declare @YTDT1 BIGINT=0
  
  Declare @JanT2 BIGINT=0
  Declare @FebT2 BIGINT=0
  Declare @MarT2 BIGINT=0
  Declare @AprT2 BIGINT=0
  Declare @MayT2 BIGINT=0
  Declare @JunT2 BIGINT=0
  Declare @JulT2 BIGINT=0
  Declare @AugT2 BIGINT=0
  Declare @SepT2 BIGINT=0
  Declare @OctT2 BIGINT=0
  Declare @NovT2 BIGINT=0
  Declare @DecT2 BIGINT=0
  Declare @YTDT2 BIGINT=0
  

CREATE TABLE #Tmp 
	(
	TailNum Varchar(9)
	)  

INSERT INTO #Tmp 
  SELECT DISTINCT 
  TailNum  
  FROM Fleet F 
  LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID 
  LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
  INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
  WHERE --(TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
        --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
         F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
        
 
       
CREATE TABLE #TmpRep 
		(
		TailNum Varchar(9),
		Jan1 Numeric(10,2),
		Feb1 Numeric(10,2),
		Mar1 Numeric(10,2),
		Apr1 Numeric(10,2),
		May1 Numeric(10,2),
		Jun1 Numeric(10,2),  
		Jul1 Numeric(10,2),
		Aug1 Numeric(10,2),
		Sep1 Numeric(10,2), 
		Oct1 Numeric(10,2),
		Nov1 Numeric(10,2),
		Dec1 Numeric(10,2),
		YTD1 Numeric(10,2),
		
		DomInt nvarchar(1),
		
		Jan2 Numeric(10,2),
		Feb2 Numeric(10,2),
		Mar2 Numeric(10,2),
		Apr2 Numeric(10,2),
		May2 Numeric(10,2),
		Jun2 Numeric(10,2),  
		Jul2 Numeric(10,2),
		Aug2 Numeric(10,2),
		Sep2 Numeric(10,2), 
		Oct2 Numeric(10,2),
		Nov2 Numeric(10,2),
		Dec2 Numeric(10,2),
		YTD2 Numeric(10,2),
		
		JanT1 BIGINT,
	    FebT1 BIGINT,
		MarT1 BIGINT,
		AprT1 BIGINT,
		MayT1 BIGINT,
		JunT1 BIGINT,
		JulT1 BIGINT,
		AugT1 BIGINT,
		SepT1 BIGINT,
		OctT1 BIGINT,
		NovT1 BIGINT,
		DecT1 BIGINT,
		YTDT1 BIGINT,
		
		JanT2 BIGINT,
	    FebT2 BIGINT,
	    MarT2 BIGINT,
		AprT2 BIGINT,
		MayT2 BIGINT,
		JunT2 BIGINT,
		JulT2 BIGINT,
		AugT2 BIGINT,
		SepT2 BIGINT,
		OctT2 BIGINT,
		NovT2 BIGINT,
		DecT2 BIGINT,
		YTDT2 BIGINT,
		)
DECLARE @RowCnt Int=0
WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
BEGIN          
    Select Top 1 @TailNumber=TailNum From #Tmp             
	Set @Jan1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=1 AND Airport='D')
    Set @Feb1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=2 AND Airport='D')
    Set @Mar1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=3 AND Airport='D')
    Set @Apr1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=4 AND Airport='D')
    Set @May1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=5 AND Airport='D')
    Set @Jun1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=6 AND Airport='D')
    Set @Jul1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=7 AND Airport='D')
    Set @Aug1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=8 AND Airport='D')
    Set @Sep1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=9 AND Airport='D')
    Set @Oct1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=10 AND Airport='D')
    Set @Nov1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=11 AND Airport='D')
    Set @Dec1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=12 AND Airport='D')
    Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1

	Set @Jan2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=1 AND Airport='I')
    Set @Feb2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=2 AND Airport='I')
    Set @Mar2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=3 AND Airport='I')
    Set @Apr2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=4 AND Airport='I')
    Set @May2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=5 AND Airport='I')
    Set @Jun2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=6 AND Airport='I')
    Set @Jul2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=7 AND Airport='I')
    Set @Aug2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=8 AND Airport='I')
    Set @Sep2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=9 AND Airport='I')
    Set @Oct2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=10 AND Airport='I')
    Set @Nov2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=11 AND Airport='I')
    Set @Dec2=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=12 AND Airport='I')
    Set @YTD2= @Jan2 + @Feb2 + @Mar2 + @Apr2 + @May2 + @Jun2 + @Jul2 + @Aug2 + @Sep2 + @Oct2 + @Nov2 + @Dec2
    
      Set @JanT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=1)
	  Set @FebT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=2)
	  Set @MarT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=3)
	  Set @AprT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=4)
	  Set @MayT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=5)
	  Set @JunT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=6)
	  Set @JulT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=7)
	  Set @AugT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=8)
	  Set @SepT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=9)
	  Set @OctT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=10)
	  Set @NovT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=11)
	  Set @DecT1 = (Select IsNull(SUM(CASE WHEN DutyType>0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=12)
	  Set @YTDT1 = @JanT1 + @FebT1 + @MarT1 + @AprT1 + @MayT1 + @JunT1 + @JulT1 + @AugT1 + @SepT1 + @OctT1 + @NovT1 + @DecT1
	  
	  Set @JanT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=1)
	  Set @FebT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=2)
	  Set @MarT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=3)
	  Set @AprT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=4)
	  Set @MayT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=5)
	  Set @JunT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=6)
	  Set @JulT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=7)
	  Set @AugT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=8)
	  Set @SepT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=9)
	  Set @OctT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=10)
	  Set @NovT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=11)
	  Set @DecT2 = (Select IsNull(SUM(CASE WHEN DutyType=0 THEN 1 ELSE 0 END),0) From #DUTY Where TailNumber=@TailNumber And (ScheduledTM)=12)
      Set @YTDT2 = @JanT2 + @FebT2 + @MarT2 + @AprT2 + @MayT2 + @JunT2 + @JulT2 + @AugT2 + @SepT2 + @OctT2 + @NovT2 + @DecT2
     
    INSERT INTO #TmpRep(TailNum
    ,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1
    ,DomInt
    ,Jan2,Feb2,Mar2,Apr2,May2,Jun2,Jul2,Aug2,Sep2,Oct2,Nov2,Dec2,YTD2
    ,JanT1,FebT1,MarT1,AprT1,MayT1,JunT1,JulT1,AugT1,SepT1,OctT1,NovT1,DecT1,YTDT1
    ,JanT2,FebT2,MarT2,AprT2,MayT2,JunT2,JulT2,AugT2,SepT2,OctT2,NovT2,DecT2,YTDT2)
    VALUES (@TailNumber
    ,@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1
    ,'D'
    ,@Jan2,@Feb2,@Mar2,@Apr2,@May2,@Jun2,@Jul2,@Aug2,@Sep2,@Oct2,@Nov2,@Dec2,@YTD2
    ,@JanT1,@FebT1,@MarT1,@AprT1,@MayT1,@JunT1,@JulT1,@AugT1,@SepT1,@OctT1,@NovT1,@DecT1,@YTDT1
    ,@JanT2,@FebT2,@MarT2,@AprT2,@MayT2,@JunT2,@JulT2,@AugT2,@SepT2,@OctT2,@NovT2,@DecT2,@YTDT2) 

    --   Insert Into @TailInt(TailNum,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,DomInt)
    --Values (@TailNumber,@Jan2,@Feb2,@Mar2,@Apr2,@May2,@Jun2,@Jul2,@Aug2,@Sep2,@Oct2,@Nov2,@Dec2,@YTD2,'I') 
    IF @FleetGroupCD <> ''
    BEGIN
    DELETE FROM #TmpRep WHERE 
    Jan1=0.00 AND Feb1=0.00 AND Mar1=0.00 AND Apr1=0.00 AND May1=0.00 AND Jun1=0.00 AND Jul1=0.00 AND Aug1=0.00 AND Sep1=0.00 AND Oct1=0.00 AND Nov1=0.00 AND Dec1=0.00 AND YTD1=0.00
    AND Jan2=0.00 AND Feb2=0.00 AND Mar2=0.00 AND Apr2=0.00 AND May2=0.00 AND Jun2=0.00 AND Jul2=0.00 AND Aug2=0.00 AND Sep2=0.00 AND Oct2=0.00 AND Nov2=0.00 AND Dec2=0.00 AND YTD2=0.00
    AND JanT1=0 AND FebT1=0 AND MarT1=0 AND AprT1=0 AND MayT1=0 AND JunT1=0 AND JulT1=0 AND AugT1=0 AND SepT1=0 AND OctT1=0 AND NovT1=0 AND DecT1=0 AND YTDT1=0
    AND JanT2=0 AND FebT2=0 AND MarT2=0 AND AprT2=0 AND MayT2=0 AND JunT2=0 AND JulT2=0 AND AugT2=0 AND SepT2=0 AND OctT2=0 AND NovT2=0 AND DecT2=0 AND YTDT2=0
    END
    
    DELETE FROM #Tmp WHERE TailNum=@TailNumber 
    SET @RowCnt=1
	END  
   
IF @IsZeroSuppressActivityAircftRpt=1 
BEGIN
   DELETE FROM #TmpRep WHERE 
		 (Jan1 IS NULL OR Jan1=0.00)
     AND (Jan2 IS NULL OR Jan2=0.00)
     AND (Feb1 IS NULL OR Feb1=0.00)
     AND (Feb2 IS NULL OR Feb2=0.00) 
     AND (Mar1 IS NULL OR Mar1=0.00)
     AND (Mar2 IS NULL OR Mar2=0.00) 
     AND (Apr1 IS NULL OR Apr1=0.00)
     AND (Apr2 IS NULL OR Apr2=0.00)
     AND (May1 IS NULL OR May1=0.00) 
     AND (May2 IS NULL OR May2=0.00)
     AND (Jun1 IS NULL OR Jun1=0.00)
     AND (Jun2 IS NULL OR Jun2=0.00) 
     AND (Jul1 IS NULL OR Jul1=0.00)
     AND (Jul2 IS NULL OR Jul2=0.00)
     AND (Aug1 IS NULL OR Aug1=0.00) 
     AND (Aug2 IS NULL OR Aug2=0.00)
     AND (Sep1 IS NULL OR Sep1=0.00)
     AND (Sep2 IS NULL OR Sep2=0.00) 
     AND (Oct1 IS NULL OR Oct1=0.00)
     AND (Oct2 IS NULL OR Oct2=0.00)
     AND (Nov1 IS NULL OR Nov1=0.00)
     AND (Nov2 IS NULL OR Nov2=0.00) 
     AND (Dec1 IS NULL OR Dec1=0.00)
     AND (Dec2 IS NULL OR Dec2=0.00) 
     AND (JanT1 IS NULL OR JanT1=0)
     AND (JanT2 IS NULL OR JanT2=0)
     AND (FebT1 IS NULL OR FebT1=0)
     AND (FebT2 IS NULL OR FebT2=0) 
     AND (MarT1 IS NULL OR MarT1=0)
     AND (MarT2 IS NULL OR MarT2=0) 
     AND (AprT1 IS NULL OR AprT1=0)
     AND (AprT2 IS NULL OR AprT2=0)
     AND (MayT1 IS NULL OR MayT1=0) 
     AND (MayT2 IS NULL OR MayT2=0)
     AND (JunT1 IS NULL OR JunT1=0)
     AND (JunT2 IS NULL OR JunT2=0) 
     AND (JulT1 IS NULL OR JulT1=0)
     AND (JulT2 IS NULL OR JulT2=0)
     AND (AugT1 IS NULL OR AugT1=0) 
     AND (AugT2 IS NULL OR AugT2=0)
     AND (SepT1 IS NULL OR SepT1=0)
     AND (SepT2 IS NULL OR SepT2=0) 
     AND (OctT1 IS NULL OR OctT1=0)
     AND (OctT2 IS NULL OR OctT2=0)
     AND (NovT1 IS NULL OR NovT1=0)
     AND (NovT2 IS NULL OR NovT2=0) 
     AND (DecT1 IS NULL OR DecT1=0)
     AND (DecT2 IS NULL OR DecT2=0) 
END

     IF @RowCnt=0 AND @IsZeroSuppressActivityAircftRpt=0
     BEGIN
     SELECT DISTINCT '' As TailNum
     ,0.00 As Jan1,0.00 As Feb1,0.00 As Mar1,0.00 As Apr1,0.00 As May1,0.00 As Jun1,0.00 As Jul1,0.00 As Aug1,0.00 As Sep1,0.00 As Oct1,0.00 As Nov1,0.00 As Dec1,0.00 As YTDDom
     ,''
     ,0.00 As Jan2,0.00 As Feb2,0.00 As Mar2,0.00 As Apr2,0.00 As May2,0.00 As Jun2,0.00 As Jul2,0.00 As Aug2,0.00 As Sep2,0.00 As Oct2,0.00 As Nov2,0.00 As Dec2,0.00 As YTDInt
     ,0 As JanT1,0 As FebT1,0 As MarT1,0 As AprT1,0 As MayT1,0 As JunT1,0 As JulT1,0 As AugT1,0 As SepT1,0 As OctT1,0 As NovT1,0 As DecT1,0 As YTDT1
     ,0 As JanT2,0 As FebT2,0 As MarT2,0 As AprT2,0 As MayT2,0 As JunT2,0 As JulT2,0 As AugT2,0 As SepT2,0 As OctT2,0 As NovT2,0 As DecT2,0 As YTDT2
     ,@TenToMin As TenToMin 
     END
     ELSE
     BEGIN
     SELECT  TailDom.TailNum
     ,TailDom.Jan1 Jan1,TailDom.Feb1 Feb1,TailDom.Mar1 Mar1,TailDom.Apr1 Apr1,TailDom.May1 May1,TailDom.Jun1 Jun1,TailDom.Jul1 Jul1,TailDom.Aug1 Aug1,TailDom.Sep1 Sep1,TailDom.Oct1 Oct1,TailDom.Nov1 Nov1,TailDom.Dec1 Dec1
     ,TailDom.YTD1 YTDDom,TailDom.DomInt FlagDom
     ,TailDom.Jan2 Jan2,TailDom.Feb2 Feb2,TailDom.Mar2 Mar2,TailDom.Apr2 Apr2,TailDom.May2 May2,TailDom.Jun2 Jun2,TailDom.Jul2 Jul2,TailDom.Aug2 Aug2,TailDom.Sep2 Sep2,TailDom.Oct2 Oct2,TailDom.Nov2 Nov2,TailDom.Dec2 Dec2
     ,TailDom.YTD2 YTDInt
     ,TailDom.JanT1 JanT1,TailDom.FebT1 FebT1,TailDom.MarT1 MarT1,TailDom.AprT1 AprT1,TailDom.MayT1 MayT1,TailDom.JunT1 JunT1,TailDom.JulT1 JulT1,TailDom.AugT1 AugT1,TailDom.SepT1 SepT1,TailDom.OctT1 OctT1,TailDom.NovT1 NovT1,TailDom.DecT1 DecT1
     ,TailDom.YTDT1 YTDT1
     ,TailDom.JanT2 JanT2,TailDom.FebT2 FebT2,TailDom.MarT2 MarT2,TailDom.AprT2 AprT2,TailDom.MayT2 MayT2,TailDom.JunT2 JunT2,TailDom.JulT2 JulT2,TailDom.AugT2 AugT2,TailDom.SepT2 SepT2,TailDom.OctT2 OctT2,TailDom.NovT2 NovT2,TailDom.DecT2 DecT2
     ,TailDom.YTDT2 YTDT2
     ,@TenToMin TenToMin
     FROM #TmpRep TailDom  
	 END

   IF OBJECT_ID('tempdb..#Tmp') is not null                        
     DROP table #Tmp 
   IF OBJECT_ID('tempdb..#TmpRep') is not null                        
     DROP table #TmpRep                             
   IF OBJECT_ID('tempdb..#TmpTble') is not null                        
     DROP table #TmpTble
        IF OBJECT_ID('tempdb..#DUTY') is not null                        
     DROP table #DUTY


END 
--EXEC spGetReportPOSTDomesticInternationalSummaryInformation 'supervisor_99',2009,'','',1,'B'


GO


