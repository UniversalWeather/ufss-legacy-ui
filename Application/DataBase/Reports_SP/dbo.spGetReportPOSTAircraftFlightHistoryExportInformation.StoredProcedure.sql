IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftFlightHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftFlightHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





       CREATE Procedure [dbo].[spGetReportPOSTAircraftFlightHistoryExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBaseCD As NVARCHAR(1000) = '',
		@IsHomebase As bit=0
		
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTAircraftFlightHistoryExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Aircraft flight history for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END

DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		
DECLARE @FLEET TABLE (FLEETID BIGINT)
INSERT INTO @FLEET SELECT fleetid FROM Fleet WHERE IsDeleted = 0 AND IsInActive = 0

	-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMER  
	  AND F.IsDeleted=0
	  AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------		
	
			SELECT   distinct
			[tail_nmbr] = F.TailNum,
            [type_desc] = F.TypeDescription,
            [typecode]=f.AircraftCD,
			[lognum] = PM.LogNum,
			[LOGID]=PL.POLOGID,
			[homebase] = A.ICAOID,
			[leg_num] =PL.LegNum,
			[leg_purpose] =PL.FlightPurpose,
			[legid] =PL.POLegID,
			[flt_date] = PL.ScheduledTM,
			[depicao_id] = AD.ICAOID,
			[dep_city_name] = AD.CityName, 
			[arricao_id] = AA.ICAOID,
			[arr_city_name] = AA.CityName,
			[distance] = pl.Distance,
			[flt_hrs] = ROUND(PL.FlightHours,1),
			[blk_hrs] = ROUND(PL.BlockHours,1),
			[crew_codes] = SUBSTRING(ACrew.AssociatedCrew,1,len((ACrew.AssociatedCrew))-1),
			[dept_code] =D.DepartmentCD,
			[auth_code] =DA.AuthorizationCD,
			[desc] = DA.DeptAuthDescription,
			[paxname] =APAx.PassengerName,
			[fltpurpose] = APAx.FlightPurposeCD,
			[paxcode] =APAx.PassengerRequestorCD,
			[pax_num] = IDENTITY(INT,1,1),
			[tot_line] ='',
			[purpose] =APAx.FlightPurposeDescription,
			[dispatchno] =PM.DispatchNum,
			[indttm] =PL.InboundDTTM,
			[dutyend] =PL.IsDutyEnd,
			[legron] =PC.RemainOverNight,
			[ac_code] =F.AircraftCD,
			aPax.LastName
		INTO #FLEETINFO
		FROM PostflightMain PM 
		INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted=0
		INNER JOIN (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
		INNER JOIN Fleet F ON F.FleetID = PM.FleetID 
	    LEFT OUTER JOIN COMPANY CP ON CP.HomebaseID = F.HomebaseID
		LEFT OUTER JOIN  PostflightCrew PC ON PL.POLegID =PC.POLegID 
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PL.PassengerRequestorID
		
		LEFT OUTER JOIN (
                SELECT DISTINCT PassengerID,P.PassengerName,p.LastName,pp.POLegID,P.PassengerRequestorCD,FP.FlightPurposeDescription,
                FP.FlightPurposeCD
				FROM PostflightPassenger PP 
				INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				--left outer JOIN PostflightLeg pl1 ON pl1.POLegID = PP.POLegID 
		                 ) APAx ON Pl.POLegID = APax.POLegID
	   
	
		LEFT OUTER JOIN (
	    SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
		SELECT RTRIM(C.CrewCD) + ','
		FROM PostflightCrew PC INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.POLegID = PC1.POLegID 
		order by C.CrewCD
		FOR XML PATH('')
		
		)
		FROM PostflightCrew PC1 
		) ACrew ON PL.POLegID = ACrew.POLegID
		
		LEFT OUTER JOIN FlightPurpose FP ON P.FlightPurposeID=FP.FlightPurposeID
		LEFT OUTER JOIN  Crew C ON PC.CrewID =C.CrewID 
	    LEFT OUTER JOIN Department D ON PL.DepartmentID =D.DepartmentID 
		LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID =AA.AirportID  
		LEFT OUTER JOIN Airport AD ON PL.DepartICAOID =AD.AirportID 
		LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
		LEFT OUTER JOIN DepartmentAuthorization DA ON PL.AuthorizationID =DA.AuthorizationID
		LEFT OUTER JOIN FleetGroupOrder FGO 
					ON F.FleetID = FGO.FleetID
		LEFT OUTER JOIN FleetGroup FG
                   ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                   
		WHERE PM.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		 AND PM.IsDeleted=0
		 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND (PM.HomebaseID  IN  (CASE(@IsHomebase)  WHEN 1 THEN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  END) OR 		
        (	
		(
		A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))
		And @IsHomebase = 0 
		)		
		OR @HomeBaseCD =''
	)
	)    	   
		   
	 ORDER BY typecode,tail_nmbr,flt_date,LOGNUM,leg_num,LastName
IF @IsZeroSuppressActivityAircftRpt=0
BEGIN 
	SELECT  * FROM #FLEETINFO
UNION ALL 
	SELECT   distinct
			[tail_nmbr] = F.TailNum,
            [type_desc] = F.TypeDescription,[typecode]=f.AircraftCD,
			[lognum] = '0',
			[LOGID]=null,
			[homebase] = NULL,
			[leg_num] ='0',
			[leg_purpose] =NULL,
			[legid] ='0',
			[flt_date] = NULL,
			[depicao_id] = NULL,
			[dep_city_name] = NULL, 
			[arricao_id] = NULL,
			[arr_city_name] = NULL,
			[distance] = '0',
			[flt_hrs] = null,
			[blk_hrs] = null,
			[crew_codes] = NULL,
			[dept_code] =NULL,
			[auth_code] =NULL,
			[desc] = NULL,
			[paxname] =NULL,
			[fltpurpose] = NULL,
			[paxcode] =NULL,
			[pax_num] ='0',
			[tot_line] =null,
			[purpose] =NULL,
			[dispatchno] =NULL,
			[indttm] =NULL,
			[dutyend] ='FALSE',
			[legron] ='0',
			[ac_code] =F.AircraftCD,null
			
		FROM fleet f 
	 INNER JOIN @FLEET Ft ON FT.FLEETID = F.FLEETID AND F.IsDeleted = 0 AND F.IsInActive = 0
	 INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = f.FleetID
	 LEFT OUTER JOIN PostflightMain PM ON F.FleetID = PM.FleetID
     LEFT OUTER JOIN COMPANY CP ON CP.HomebaseID = F.HomebaseID
	 LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
 
  WHERE F.TailNum NOT IN (SELECT DISTINCT tail_nmbr FROM #FLEETINFO)
        AND F.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
        AND F.TailNum IS NOT NULL
		AND (PM.HomebaseID  IN  (CASE(@IsHomebase)  WHEN 1 THEN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  END) OR 
		
	(	
		(
		A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))
		And @IsHomebase = 0 
		)		
		OR @HomeBaseCD =''
	)
	)
    
    ORDER BY typecode,tail_nmbr,flt_date,LOGNUM,leg_num,LastName
    END
ELSE 
BEGIN
SELECT * FROM #FLEETINFO
ORDER BY typecode,tail_nmbr,flt_date,LOGNUM,leg_num,LastName
END
IF OBJECT_ID('tempdb..#FLEETINFO') IS NOT NULL
DROP TABLE #FLEETINFO 

   -- EXEC spGetReportPOSTAircraftFlightHistoryExportInformation 'JWILLIAMS_13','2010/8/20','2010/10/24', '', '','',0




GO


