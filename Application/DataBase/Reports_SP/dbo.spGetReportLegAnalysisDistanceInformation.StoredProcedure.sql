IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLegAnalysisDistanceInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLegAnalysisDistanceInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportLegAnalysisDistanceInformation]          
(    
  @UserCD AS VARCHAR(30),  --Mandatory    
  @DATEFROM DATETIME, --Mandatory    
  @DATETO DATETIME, --Mandatory    
  @TailNumber VARCHAR(5000)=Null,--Optional    
  @FleetGroupCD VARCHAR(5000)=Null--Optional    
 )    
-- =============================================        
-- Author: Mathes        
-- Create date: 01-08-2012        
-- Description: Leg Analysis/Hours(FC) - Report    
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package     
-- =============================================        
AS        
SET NOCOUNT ON        
BEGIN        
          
                    
    DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';    
    DECLARE @ParameterDefinition AS NVARCHAR(300)       
  
     SET @SQLSCRIPT='SELECT DISTINCT Fleet.FleetID, Fleet.TailNum
          FROM PostflightLeg INNER JOIN
          PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID INNER JOIN
          PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID LEFT OUTER JOIN
          Fleet INNER JOIN
          FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN
          FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID ON PostflightMain.FleetID = Fleet.FleetID                     
          WHERE PostflightLeg.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And    
          PostflightLeg.ScheduledTM BETWEEN @DATEFROM AND @DATETO '    
            
      IF @TailNumber <>''     
     BEGIN    
      SET @SQLSCRIPT = @SQLSCRIPT + ' AND Fleet.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNumber, 1) = ',' THEN LEFT(@TailNumber, LEN(@TailNumber) - 1) ELSE @TailNumber END, ',', ''', ''') + ''')';    
     END    
     IF @FleetGroupCD <>''     
     BEGIN    
      SET @SQLSCRIPT = @SQLSCRIPT + ' AND FleetGroup.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';             
     END       
                   
     SET @SQLSCRIPT=@SQLSCRIPT + ' Order By Fleet.TailNum '    
            
     SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNumber AS VARCHAR(5000), @FleetGroupCD AS VARCHAR(5000)'    
     EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @TailNumber, @FleetGroupCD    
              
 END



GO


