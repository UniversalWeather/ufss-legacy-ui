IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSChecklistLegHotelInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSChecklistLegHotelInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSChecklistLegHotelInformation]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
		@LegNum AS VARCHAR(30) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSChecklistLegHotelInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

    	  SELECT [LegId] = CONVERT(VARCHAR,PL.LegID)
    		,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[HDescription] = ISNULL(PPH.PreflightHotelName,'')
			,[HPhone] = ISNULL(PPH.PhoneNum1,'')
			,[HFax] = ISNULL(PPH.FaxNum,'')
			,[HAddress1] = ISNULL(HP.Addr1,'')
			,[HAddress2] = ISNULL(HP.Addr2,'')
			,[HCity] = ISNULL(HP.CityName,'')
			,[HState] = ISNULL(HP.StateName,'')
			,[HZip] = ISNULL(HP.PostalZipCD,'')
			,[HRemarks] = ISNULL(HP.Remarks,'')
			,[HRate] = ISNULL(PPH.Rate,0)
			,[HRooms] = ISNULL(PPL.NoofRooms,0)
			,[HConfirm] = ISNULL(PPH.ConfirmationStatus,'')
			,[HComments] = ISNULL(PPH.Comments,'')
			,[HTYPE] = 'PH'
			,[CrewPaxCD] = P.PassengerRequestorCD
			,[OrderNUM] = 9 --PPL.OrderNUM -- FIELD YET TO BE POPULATED FROM PREFLIGHT
			,[IsHotelEmpty] = CASE WHEN ISNULL(PPH.PreflightHotelName,'') = '' AND
						ISNULL(PPH.PhoneNum1,'') = '' AND
						ISNULL(PPH.FaxNum,'') = '' AND
						--ISNULL(HP.Addr1,'') = '' AND
						--ISNULL(HP.Addr2,'') = '' AND
						--ISNULL(HP.CityName,'') = '' AND
						--ISNULL(HP.StateName,'') = '' AND
						--ISNULL(HP.PostalZipCD,'') = '' AND
						ISNULL(HP.Remarks,'') = '' AND
						ISNULL(PPH.Rate,0) = 0 AND																																																															
						ISNULL(PPL.NoofRooms,0) = 0 AND
						ISNULL(PPH.ConfirmationStatus,'') = '' AND
						ISNULL(PPH.Comments,'') = ''
					THEN 1 ELSE 0 END	
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
			INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')			

			UNION ALL
			--Crew Hotel AND Addnl Crew / Maint Crew - Hotel	
			SELECT  [LegId] = CONVERT(VARCHAR,PL.LegID), [LegNUM] = CONVERT(VARCHAR,PL.LegNUM) 
				,ISNULL(PCH.PreflightHotelName,''),ISNULL(PCH.PhoneNum1,''),ISNULL(PCH.FaxNum,''),ISNULL(HC.Addr1,''),ISNULL(HC.Addr2,''),
				ISNULL(HC.CityName,''),ISNULL(HC.StateName,''),ISNULL(HC.PostalZipCD,''),ISNULL(HC.Remarks,''),ISNULL(PCH.Rate,0),ISNULL(PCLL.NoofRooms,0),
				ISNULL(PCH.ConfirmationStatus,''),ISNULL(PCH.Comments,''), HTYPE = CASE WHEN PCLL.DutyType IN ('P', 'S') THEN 'CH' ELSE 'MH' END
				,[CrewPaxCD] = C.CrewCD, 
				[OrderNUM] =	--CASE PCLL.DutyType WHEN 'P' THEN 1 WHEN 'S' THEN 2 ELSE 3 END  
								CASE PCLL.DutyTYPE WHEN 'P' THEN 1 
								WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
								WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END 
				,[IsHotelEmpty] = CASE WHEN ISNULL(PCH.PreflightHotelName,'') = '' AND
							ISNULL(PCH.PhoneNum1,'') = '' AND
							ISNULL(PCH.FaxNum,'') = '' AND
							ISNULL(HC.Addr1,'') = '' AND
							ISNULL(HC.Addr2,'') = '' AND
							--ISNULL(HC.CityName,'') = '' AND
							--ISNULL(HC.StateName,'') = '' AND
							--ISNULL(HC.PostalZipCD,'') = '' AND
							ISNULL(HC.Remarks,'') = '' AND
							ISNULL(PCH.Rate,0) = 0 AND																																																															
							ISNULL(PCLL.NoofRooms,0) = 0 AND
							ISNULL(PCH.ConfirmationStatus,'') = '' AND
							ISNULL(PCH.Comments,'') = ''
						THEN 1 ELSE 0 END 
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightCrewList PCLL ON PL.LegID = PCLL.LegID
			INNER JOIN Crew C ON PCLL.CrewID = C.CrewID
			INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			--AND PCLL.DutyType IN ('P', 'S')	
	
			ORDER BY LegNUM, [HTYPE], OrderNUM, CrewPaxCD
	END

--EXEC spGetReportPRETSChecklistLegHotelInformation 'SUPERVISOR_99', '10099107564','1'

		
	











GO


