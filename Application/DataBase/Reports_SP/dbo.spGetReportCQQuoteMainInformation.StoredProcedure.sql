IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQQuoteMainInformation]  
 @CQFileID VARCHAR(30),  
 @QuoteNUM INT  
AS   
-- =============================================  
-- SPC Name:spGetReportCQQuoteMainInformation  
-- Author: Aishwarya.M  
-- Create date: 06 Mar 2013  
-- Description: Get Charter Quote Information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- =============================================  
BEGIN  
 SELECT DISTINCT C.IsQuoteInformation2  
         ,[CQCompanyName] = CASE WHEN IsCompanyName2=1  THEN  CQM.QuoteInformation ELSE '' END--C.ChtQuoteCompany---CQM.QuoteInformation  
   ,IsCompanyName2  
  ,CompanyInfo = CASE WHEN C.IsQuoteInformation2=1  THEN  CQM.CompanyInformation ELSE '' END  
  ,IsquoteNumber = C.QuoteIDTYPE  
        ,[QUOTENUMBER] = CQF.FileNUM --CF.QuoteDescription  
        ,[ISTAILNUMBER] = C.IsQuoteTailNum  
  ,[TAILNUMBER] =  F.TailNUM    
  ,[ISAIRCRAFTTYPE] = C.IsQuoteTypeCD  
  ,[AIRCRAFTTYPE] = AC.AircraftCD + ' - ' + AC.AircraftDescription   
  ,[CQheader] = CQM.ReportHeader  
  ,ISLeg = C.IsRptQuoteDetailLegNum   
  ,[Leg] =  CL.LegNUM   
  ,IsDepDate = C.IsRptQuoteDetailDepartureDT   
  ,[DepDate] =  CL.DepartureDTTMLocal   
  ,ISFrom = C.IsRptQuoteDetailFromDescription  
  ,[From] = D.CityName+', '+RTRIM(DC.CountryCD)+'; '+D.IcaoID   
  ,[DepTime] = CL.DepartureDTTMLocal  
  ,IsTo = IsRptQuoteDetailToDescription  
  ,[To] = A.CityName+', '+RTRIM(ACA.CountryCD)+'; '+A.IcaoID   
  ,ISArrTime = C.IsRptQuoteDetailArrivalDate  
  ,[ArrTime] = CL.ArrivalDTTMLocal   
  ,IsFltHrs = C.IsRptQuoteDetailFlightHours  
  ,[FltHrs] = CL.FlightHours   
  ,IsMiles = C.IsRptQuoteDetailMiles  
  ,[Miles] = CL.Distance   
  ,IsPAX = C.IsRptQuoteDetailPassengerCnt  
  ,[PAX] = CL.PassengerTotal   
  ,IsQuotedAmount = C.IsRptQuoteDetailQuotedAmt  
  ,[QuotedAmount] = CL.FlightCharge  
  ,FooterMsg = CQM.ReportFooter  
  ,reporttitle = C.QuoteTitle  
  ,IsLegDetails = CQM. IsPrintDetail  
  ,LogoPosition = CASE WHEN S.LogoPosition IS NOT NULL AND CQF.SalesPersonID IS  NOT NULL AND S.LogoPosition <> 0  
         THEN  S.LogoPosition 
         ELSE C.QuoteLogoPosition END-- 0 : None, 1 : Center, 2 : Left, 3 : Right   
    
  ,ACImagePosition = CQM.ImagePosition -- 1:None, 2:End of report, 3:After Tailnum, 4:SeparatePage  
  ,QImage1 = FW1.UWAFilePath -- Aircraft Image 1  
  ,QImage2 = FW2.UWAFilePath -- Aircraft Image 2  
  ,QImage3 = FW3.UWAFilePath -- Aircraft Image 3  
  ,QImage4 = FW4.UWAFilePath -- Aircraft Image 4  
  ,NoOfImages = C.ImageCnt  
   
   FROM CQFile CQF  
   INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0  
   --INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID  
   INNER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID   
   LEFT JOIN CQQuoteDetail CL ON CQM.CQQuoteMainID = CL.CQQuoteMainID  
   INNER  JOIN Company C ON C.CustomerID = CQF.CustomerID AND C.HomebaseID=CQF.HomebaseID  
   LEFT OUTER JOIN SalesPerson S ON CQF.SalesPersonID = S.SalesPersonID  
   LEFT OUTER JOIN Fleet F ON CM.FleetID = F.FleetID  
   LEFT OUTER JOIN CQFleetCharterRate CF ON CM.CQMainID = CF.CQMainID    
   LEFT OUTER  JOIN Aircraft AC ON CM.AircraftID = AC.AircraftID  
   LEFT OUTER JOIN Airport D ON D.AirportID=CL.DAirportID  
   LEFT OUTER JOIN Airport A ON A.AirportID=CL.AAirportID  
   LEFT OUTER JOIN Country DC ON DC.CountryID=D.CountryID  
   LEFT OUTER JOIN Country ACA ON ACA.CountryID=A.CountryID  
   LEFT OUTER JOIN FileWarehouse FW1 ON FW1.FileWarehouseID = CQM.FileWarehouseID  
   LEFT OUTER JOIN FileWarehouse FW2 ON FW2.FileWarehouseID = CQM.FileWarehouseID2  
   LEFT OUTER JOIN FileWarehouse FW3 ON FW3.FileWarehouseID = CQM.FileWarehouseID3  
   LEFT OUTER JOIN FileWarehouse FW4 ON FW4.FileWarehouseID = CQM.FileWarehouseID4  
   LEFT OUTER JOIN FileWarehouse FW ON FW.RecordID = CQF.HomebaseID AND FW.RecordType = 'CqCompanyQuote'  
      WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)  
     AND CM.QuoteNUM = @QuoteNUM  
  ORDER BY [Leg]   
     
END  
 --EXEC spGetReportCQQuoteMainInformation 100021890,1    
  
  
  
  

GO


