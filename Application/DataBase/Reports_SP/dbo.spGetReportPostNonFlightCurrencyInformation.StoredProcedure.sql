IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostNonFlightCurrencyInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostNonFlightCurrencyInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPostNonFlightCurrencyInformation]
	  @UserCD AS VARCHAR(30)
	 ,@AsOf Datetime
	 ,@CrewCD NVARCHAR(1000)=''
	 ,@CrewGroupCD NVARCHAR(1000)=''
	 ,@AircraftCD NVARCHAR(1000)=''
	 ,@IsHomebase as bit=0
	AS
-- ===============================================================================
-- SPC Name: spGetReportPostNonFlightCurrencyInformation
-- Author:Badrinath/Mullai.D
-- Create date: 06 August 2012
-- Description: Get Non flight Currency Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @CrewCD <> '' 
      BEGIN
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            WHERE c.CustomerID = @CUSTOMER AND C.IsDeleted=0 --AND C.IsStatus=1  
            AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
      END
IF @CrewGroupCD <> '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew C
            LEFT OUTER JOIN CrewGroupOrder cGO
                  ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
            LEFT OUTER JOIN CrewGroup CG 
                  ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
            WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
                  AND C.CustomerID = @CUSTOMER AND C.IsDeleted=0 AND C.IsStatus=1 
      END
ELSE IF @CrewCD = '' AND @CrewGroupCD = '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
            FROM Crew C 
            WHERE C.CustomerID = @CUSTOMER AND C.IsDeleted=0 AND C.IsStatus=1 
      END
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 0
----To Retrieve Crew and Crew Checklist information which are assigned to the log
SELECT DISTINCT
	[AsOf]=dbo.GetShortDateFormatByUserCD(@UserCD, @Asof), 
	[CrewCD]=C.CrewCD,
	[CrewMember] =C.CrewCD+'  '+isnull(C.LastName,''),
	[TypeCode] =AC.AircraftCD,
	[Checklist Item]= isnull(CCD.CheckListCD,'')+' '+isnull(CCL.CrewChecklistDescription,''),
	[Previous]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
	[DueDate]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
	[Grace]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
	[Alert]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
	[Comment] = (CASE WHEN (CCD.IsOneTimeEvent='TRUE') And (CCD.IsCompleted='TRUE') THEN 'COMPLETED'
                              WHEN @Asof='' THEN ' '
							  WHEN (@Asof) < (CASE (CCD.AlertDT) WHEN '' THEN CCD.DueDT ELSE CCD.AlertDT END) THEN ''
							  WHEN @Asof BETWEEN  CCD.AlertDT  AND CCD.DueDT THEN 'ALERT' 
						      WHEN (@Asof> CCD.DUEDT) AND (@Asof<=CCD.GRACEDT) THEN 'GRACE'
						      WHEN (@Asof > CCD.DUEDT) THEN 'PAST DUE' END)
INTO #NONFLIGINFO
FROM Crew c
    INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
	INNER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID AND CCD.IsDeleted=0 AND C.IsDeleted=0
	LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID 
	LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID		
	LEFT OUTER JOIN Airport a on a.AirportID =c.HomebaseID
WHERE c.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND (C.HomebaseID =dbo.GetHomeBaseAirportIDByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
ORDER BY CrewCD	
------Applying Zero Suppress Activity for Crew based on Company profile setting	
IF @IsZeroSuppressActivityCrewRpt=0
BEGIN
SELECT * FROM #NONFLIGINFO
UNION ALL
----To Retrieve Crew and Crew Checklist information which are assigned to the log
SELECT DISTINCT
	[AsOf]=dbo.GetShortDateFormatByUserCD(@UserCD,@AsOF), 
	[CrewCD]=C.CrewCD,
	[CrewMember] =C.CrewCD+'  '+C.LastName,
	[TypeCode] =NULL,
	[Checklist Item]='NO ITEMS',
	[Previous]=NULL,
	[DueDate]=NULL,
	[Grace]=NULL,
	[Alert]=NULL,
	[Comment]=NULL 
FROM Crew C 
    INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
	LEFT OUTER JOIN @CREW CR ON CR.CREWID = C.CrewID
	LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
	LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID 
	LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
	LEFT OUTER JOIN Company cp on cp.HomebaseID=c.HomebaseID
WHERE c.crewcd not in (select DISTINCT CrewCD from #NONFLIGINFO)  
	AND C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
ORDER BY CrewCD
END
ELSE
BEGIN
SELECT * FROM #NONFLIGINFO
ORDER BY CrewCD
END

IF OBJECT_ID('tempdb..#NONFLIGINFO') IS NOT NULL
DROP TABLE #NONFLIGINFO

-- EXEC spGetReportPostNonFlightCurrencyInformation 'JWILLIAMS_13','2009/12/31','','','',0

GO




