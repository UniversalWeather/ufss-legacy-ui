IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotIILogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotIILogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



     
CREATE procedure  [dbo].[spGetReportPOSTPilotIILogInformation]   
  (
	  @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @CrewCD AS NVARCHAR(1000) = '',  
	  @CrewGroupCD AS CHAR(1000) = '',
	  @ChecklistCode AS NVARCHAR(1000) = '',
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @FlightCatagoryCD AS CHAR(1000) = '',
	  @HomeBase AS CHAR(1000) = '', 
	  @HomebaseID BIT = 0,
	  @PrintOnTime BIT = 0,
	  @PrintOffTime BIT = 0,
	  @SortBy as varchar(1)
  )  
AS  
    
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTPilotIILogInformation]  
-- Author: Avanikanta Pradhan  
-- Create date: 06 August  
-- Description: Get Pilot Log II 
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
 
SET NOCOUNT ON
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt =1

--DROP TABLE #CREWINFO

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @CrewCD <> ''
      BEGIN
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            WHERE c.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1  
            AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
      END
IF @CrewGroupCD <> ''
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew C
            LEFT OUTER JOIN CrewGroupOrder cGO
                  ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
            LEFT OUTER JOIN CrewGroup CG 
                  ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
            WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
                  AND C.CustomerID = @CustomerID AND C.IsDeleted=0 AND C.IsStatus=1 
      END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
            FROM Crew C 
            WHERE C.CustomerID = @customerID AND C.IsDeleted=0 AND C.IsStatus=1 
      END
      
IF @IsZeroSuppressActivityCrewRpt=1
BEGIN

DELETE FROM  @TempCrewID WHERE CrewID NOT IN(
SELECT DISTINCT [CREWID]=CW.CrewID
FROM PostflightMain POM 
INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
INNER JOIN PostflightCrew POC ON POC.POLegID = POL.POLegID 
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 --AND CW.IsStatus = 1
LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID

WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POM.IsDeleted=0
AND CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
 AND (Ac.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
 AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = ''))
 END
      
-----------------------------TailNum and Fleet Group Filteration---------------------- 


DECLARE @CREWINFO TABLE 
(
[DateRange] VARCHAR(100), 
[DateFROM] DATE,
[DATETO] DATE,
[CREWID] VARCHAR(50),
[crewcd] VARCHAR(5)
,[PILOTLast] VARCHAR(30)
,[PILOTFirst] VARCHAR(30)
,[PILOTMiddle] VARCHAR(30)
,TenToMin NUMERIC(1,0)
)


DECLARE @TenToMin SMALLINT = 0;
 
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID =@CustomerID
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
 

 
IF (@HomebaseID=1 OR @HomebaseID=0) AND @HomeBase =''
BEGIN


INSERT INTO @CREWINFO
SELECT DISTINCT
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
[DateFROM]=CONVERT(DATE,@DATEFROM),
[DATETO]= CONVERT(DATE, @DATETO) ,
[CREWID]=C.CrewID,
[crewcd]=c.crewcd
,[PILOTLast] = C.LastName
,[PILOTFirst] = C.FirstName
,[PILOTMiddle] = C.MiddleInitial
,@TenToMin TenToMin

FROM PostflightCrew POC
      INNER   JOIN Crew C ON C.CrewID = POC.CrewID
      INNER JOIN @TempCrewID T ON C.CrewID=T.CrewID
      INNER JOIN PostflightLeg POL ON POC.POLegID = POL.POLegID AND POL.IsDeleted=0
      INNER JOIN PostflightMain POM ON POC.POLogID = POM.POLogID AND POM.IsDeleted=0
      INNER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
      INNER JOIN Fleet F ON F.FleetID = POM.FleetID
      LEFT OUTER JOIN Airport AF ON C.HomebaseID=AF.AirportID
      LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
      LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
 
      WHERE C.CustomerID = @CustomerID
      AND CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
      AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
      AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
      AND (C1.HomebaseID=(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID=0)
     
END



IF @HomeBase<> '' AND @HomebaseID=0
BEGIN

INSERT INTO @CREWINFO
SELECT DISTINCT  [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
                 [DateFROM]=CONVERT(DATE,@DATEFROM),
                 [DATETO]= CONVERT(DATE, @DATETO),
                 C.CrewID,
                 C.CrewCD
                ,[PILOTLast] = C.LastName
				,[PILOTFirst] = C.FirstName
				,[PILOTMiddle] = C.MiddleInitial
				,@TenToMin FROM Crew C 
         INNER JOIN @TempCrewID T ON C.CrewID=T.CrewID
         LEFT OUTER JOIN Airport AF ON C.HomebaseID=AF.AirportID
         LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
         WHERE C.CustomerID=@CustomerID
         AND (AF.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBase, ',')) OR @HomeBase = '')
         AND C.CrewCD NOT IN((SELECT CREWCD FROM @CREWINFO))
         
END

ELSE IF @HomeBase= '' AND @HomebaseID=0

BEGIN

INSERT INTO @CREWINFO
SELECT DISTINCT  [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
                 [DateFROM]=CONVERT(DATE,@DATEFROM),
                 [DATETO]= CONVERT(DATE, @DATETO),
                 C.CrewID,
                 C.CrewCD
                ,[PILOTLast] = C.LastName
				,[PILOTFirst] = C.FirstName
				,[PILOTMiddle] = C.MiddleInitial
				,@TenToMin FROM Crew C 
         INNER JOIN @TempCrewID T ON C.CrewID=T.CrewID
         LEFT OUTER JOIN Airport AF ON C.HomebaseID=AF.AirportID
         LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
         WHERE C.CustomerID=@CustomerID
         AND C.CrewCD NOT IN(SELECT CREWCD FROM @CREWINFO)

END
ELSE IF  @HomebaseID=1

BEGIN

INSERT INTO @CREWINFO
SELECT DISTINCT  [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
                 [DateFROM]=CONVERT(DATE,@DATEFROM),
                 [DATETO]= CONVERT(DATE, @DATETO),
                 C.CrewID,
                 C.CrewCD
                ,[PILOTLast] = C.LastName
				,[PILOTFirst] = C.FirstName
				,[PILOTMiddle] = C.MiddleInitial
				,@TenToMin FROM Crew C 
         INNER JOIN @TempCrewID T ON C.CrewID=T.CrewID
         LEFT OUTER JOIN Airport AF ON C.HomebaseID=AF.AirportID
         LEFT OUTER JOIN Company C1 ON AF.AirportID=C1.HomebaseAirportID
         WHERE C.CustomerID=@CustomerID
         AND (C1.HomebaseID=(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID=0)
         AND C.CrewCD NOT IN(SELECT CREWCD FROM @CREWINFO)

END

SELECT * FROM @CREWINFO ORDER BY CREWCD
--EXEC spGetReportPOSTPilotIILogInformation 'supervisor_99','2009/1/1','2009/1/15','','','','','','',0,0,0,'b'


GO


