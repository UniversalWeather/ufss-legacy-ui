IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation]
	@UserCD VARCHAR(50)--MANDATORY
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum VARCHAR(30) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSPassengerNotesAndAlertsInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight Passenger Notes And Alerts information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
SELECT  PassengerName = ISNULL(P.PassengerName,'')
		,Notes = ISNULL(P.Notes,'')
		,Alerts = ISNULL(P.PassengerAlert,'')
		--,Code = PA.AdditionalINFOCD
		--,PDescription = PA.AdditionalINFODescription
		--,AddlInfo = PA.AdditionalINFOValue
		--,PAXCode = P.PassengerRequestorCD
		--,[LegID] = CONVERT(VARCHAR,PAX.LegID)
		--,[LegNum] = CONVERT(VARCHAR,PAX.LegNUM )
	FROM (
		SELECT DISTINCT PPL.PassengerID --, PL.LegID, PL.LegNUM 
		FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			INNER JOIN FlightPurpose FP ON PPL.FlightPurposeID=FP.FlightPurposeID
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	) PAX
	
	INNER JOIN (
		SELECT PassengerRequestorID, PassengerName, Notes, PassengerAlert, PassengerRequestorCD
		FROM Passenger
	) P ON PAX.PassengerID = P.PassengerRequestorID
	--INNER JOIN PassengerAdditionalInfo PA ON PA.PassengerRequestorID = P.PassengerRequestorID
	
END

--EXEC spGetReportPRETSPassengerNotesAndAlertsInformation 'supervisor_99', 10099107564, ''



GO


