IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItinerarySubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation]
         @LegID AS varchar(30)   -- Mandatory

AS

-- =============================================================================
-- SPC Name: spGetReportPRECorporateItinerarySubInformation
-- Author: ABHISHEK.S
-- Create date: 23 August 2012
-- Description: Get Preflight Passenger Itinerary Sub Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==============================================================================

BEGIN
SET NOCOUNT ON;

DECLARE	
	@PIC VARCHAR(50), 
	@SIC VARCHAR(50), 
	@Engineer VARCHAR(50), 
	@Attendent VARCHAR(50),
	@OtherCrew VARCHAR(50),
	@InstrCrew VARCHAR(50)

DECLARE @CrewID BIGINT, @DutyTYPE VARCHAR(1);

	DECLARE curCrewInfo CURSOR FOR
		SELECT  PC.CrewID, PC.DutyTYPE 
		FROM PreflightCrewList PC
		WHERE PC.LegID = convert(Bigint,@LegID)

	OPEN curCrewInfo;
		FETCH NEXT FROM curCrewInfo INTO @CrewID, @DutyTYPE;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- P S E A O I
			IF @DutyTYPE = 'P' 
			BEGIN
				SELECT 	@PIC = FirstName +' '+ LastName						
					FROM CREW WHERE CREWID = @CrewID
			END
			IF @DutyTYPE = 'S' 
			BEGIN
				--@SIC, @SICPhone, @SICPager
				SELECT 	@SIC = FirstName +' '+ LastName						
					FROM CREW WHERE CREWID = @CrewID
			END
			IF @DutyTYPE = 'E' 
			BEGIN
				--@Engineer, @EngineerPhone, @EngineerPager
				SELECT 	@Engineer = FirstName +' '+ LastName						
					FROM CREW WHERE CREWID = @CrewID
			END
			IF @DutyTYPE = 'A' 
			BEGIN
				--@Attendent, @AttendentPhone, @AttendentPager
				SELECT 	@Attendent = FirstName +' '+ LastName						
					FROM CREW WHERE CREWID = @CrewID
			END
			IF @DutyTYPE = 'O' 
			BEGIN
				--@OtherCrew, @OtherCrewPhone, @OtherCrewPager
				SELECT 	@OtherCrew = FirstName +' '+ LastName						
					FROM CREW WHERE CREWID = @CrewID
			END
			IF @DutyTYPE = 'I' 
			BEGIN
				--@InstrCrew, @InstrCrewPhone, @InstrCrewPager
				SELECT 	@InstrCrew = FirstName +' '+ LastName										
					FROM CREW WHERE CREWID = @CrewID
			END			
			FETCH NEXT FROM curCrewInfo INTO @CrewID, @DutyTYPE;
		END
	CLOSE curCrewInfo;
	DEALLOCATE curCrewInfo;

	SELECT 
	PIC = @PIC, 
	SIC = @SIC, 
	Engineer = @Engineer, 
	Attendent = @Attendent,
	OtherCrew = @OtherCrew, 
	InstrCrew = @InstrCrew
--EXEC spGetReportPRECorporateItinerarySubInformation 100115345
END

GO




