IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogMainInformationHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogMainInformationHeader]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTPilotLogMainInformationHeader]    
  (  
  @UserCD AS VARCHAR(30) 
  )  
  As  
    
   BEGIN  

 
      DECLARE @tbl AS TABLE (ROW_ID INT  IDENTITY,
						 CrewLogCustomlabel1 Varchar(30),   
                         CrewLogCustomlabel2 Varchar(30),	
                         CrewLogCustomlabel3 Varchar(30),   
                         CrewLogCustomlabel4 Varchar(30),  
                         ApprNpr Varchar(30),                         
                         ApprPr varchar(30),
                         ArrICAO varchar(30),
                         AssociatedCrew varchar(30),
                         BlkIn varchar(30),    
                         BlkOut Varchar(30),  
                         BlockTime Varchar(30),  
                         Client Varchar(30),  
                         CrewDutyType Varchar(30),  
                         DepICAO Varchar(30),  
                         DutyHours Varchar(30),  
                         FlightTime varchar(30),  
                         FltCat Varchar(30),  
                         FltTyp Varchar(30),  
                         InstTime Varchar(30),  
                         LndingDt Varchar(30),  
                         LndingNt Varchar(30),  
                         NiteTime Varchar(30),  
                         PS Varchar(30),  
                         RONs Varchar(30),  
                         T_OFFDt Varchar(30),  
                         T_OFFNt Varchar(30)  
                       )  

                       

 INSERT INTO @tbl(CrewLogCustomlabel3) VALUES('CrewLog Custom label 3')
 
 Update @tbl Set CrewLogCustomlabel1=(Select CrewLogCustomLBLLong1 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1        
 Update @tbl Set CrewLogCustomlabel2=(Select CrewLogCustomLBLLong2 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1                                
 Update @tbl Set CrewLogCustomlabel3=(Select SpecificationLong3 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1        
 Update @tbl Set CrewLogCustomlabel4=(Select SpecificationLong4 From Company Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) ) WHERE ROW_ID=1        
 
 Update @tbl Set ApprNpr=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Approach Non-Precision','Appr  Npr') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
	Update @tbl Set ApprNpr='Appr  Npr' Where ApprNpr is null or ApprNpr=''
 Update @tbl Set ApprPr=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Approach Precision','Appr  Pr') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
	Update @tbl Set ApprPr='Appr  Pr' Where ApprPr is null or ApprPr=''
 Update @tbl Set ArrICAO=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Arrival ICAO','Arr. ICAO') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1         
	Update @tbl Set ArrICAO='Arr. ICAO' Where ArrICAO is null or ArrICAO=''
 Update @tbl Set AssociatedCrew=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Associated Crew' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1        
	Update @tbl Set AssociatedCrew='Associated Crew' Where AssociatedCrew is null or AssociatedCrew=''
 Update @tbl Set BlkIn=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN ('Block In','Blk In') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1  
	Update @tbl Set BlkIn='Blk In' Where BlkIn is null or BlkIn=''       
 Update @tbl Set BlkOut=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Block Out','Blk Out') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1   
	Update @tbl Set BlkOut='Blk Out' Where BlkOut is null or BlkOut=''        
 Update @tbl Set BlockTime=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Block Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1   
	Update @tbl Set BlockTime='Block Time' Where BlockTime is null or BlockTime=''        
 Update @tbl Set Client=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Client' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1    
	Update @tbl Set Client='Client' Where Client is null or Client=''     
 Update @tbl Set CrewDutyType=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Crew Duty Type' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1  
	Update @tbl Set CrewDutyType='Crew Duty Type' Where CrewDutyType is null or CrewDutyType=''      
 Update @tbl Set DepICAO=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription in('Departure ICAO','Dep. ICAO') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set DepICAO='Dep. ICAO' Where DepICAO is null or DepICAO=''          
 Update @tbl Set DutyHours=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Duty Hours' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1
	Update @tbl Set DutyHours='Duty Hours' Where DutyHours is null or DutyHours=''           
 Update @tbl Set FlightTime=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription='Flight Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG')  WHERE ROW_ID=1  
	Update @tbl Set FlightTime='Flight Time' Where FlightTime is null or FlightTime=''         
 Update @tbl Set FltCat=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Flight Category','Flt Cat') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1
	Update @tbl Set FltCat='Flt Cat' Where FltCat is null or FltCat=''            
 Update @tbl Set FltTyp=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Flight Type','Flt Typ') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set FltTyp='Flt Typ' Where FltTyp is null or FltTyp=''          
 Update @tbl Set InstTime=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Instrument Time','Inst Time') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1
	Update @tbl Set InstTime='Inst Time' Where InstTime is null or InstTime=''          
 Update @tbl Set LndingDt=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription in ('Landing Day','Lnding Dt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set LndingDt='Lnding Dt' Where LndingDt is null or LndingDt=''         
 Update @tbl Set LndingNt=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription in('Landing Night','Lnding Nt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set LndingNt='Lnding Nt' Where LndingNt is null or LndingNt=''          
 Update @tbl Set NiteTime=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN ('Night Time','Nite Time') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1  
	Update @tbl Set NiteTime='Night Time' Where NiteTime is null or NiteTime=''       
 Update @tbl Set PS=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN ('PIC/SIC','P S','P   S') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1   
	Update @tbl Set PS='P S' Where PS is null or PS=''         
 Update @tbl Set RONs=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN ('RON','RONs') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set RONs='RON' Where RONs is null or RONs=''    
 Update @tbl Set T_OFFDt=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN('Takeoff Day','T/OFF Dt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1  
	Update @tbl Set T_OFFDt='T/OFF Dt' Where T_OFFDt is null or T_OFFDt=''         
 Update @tbl Set T_OFFNt=(Select Top 1 CASE
	WHEN (LEN(ISNULL(CustomDescription,''))!=0) 
	THEN CustomDescription 
	ELSE OriginalDescription 
    END  From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
	And OriginalDescription IN ('Takeoff Night','T/OFF Nt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG') WHERE ROW_ID=1 
	Update @tbl Set T_OFFNt='T/OFF Nt' Where T_OFFNt is null or T_OFFNt=''    
 
 INSERT INTO @tbl(CrewLogCustomlabel3) VALUES(1)
 
 Update @tbl Set CrewLogCustomlabel1=ISNULL((Select Top 1 CASE
	WHEN (LEN(ISNULL((Select top 1 CrewLogCustomlabel1 from @tbl),''))!=0) 
	THEN  IsPrint
	ELSE 0 
    END From TsFlightLog Where  OriginalDescription Is Not Null AND CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription IN('Crew Log Custom Label 1','Crew Log Custom Label1','CrewLog Custom label 1') 
	AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))  AND Category = 'PILOT LOG'),0) WHERE ROW_ID=2        
 Update @tbl Set CrewLogCustomlabel2=ISNULL((Select Top 1 CASE
	WHEN (LEN(ISNULL((Select top 1 CrewLogCustomlabel2 from @tbl),''))!=0) 
	THEN  IsPrint
	ELSE 0 
    END From TsFlightLog  Where  OriginalDescription Is Not Null AND CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)  And OriginalDescription IN('Crew Log Custom Label 2','Crew Log Custom Label2','CrewLog Custom label 2')
	AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) 
	AND Category = 'PILOT LOG'),0) WHERE ROW_ID=2        
 Update @tbl Set CrewLogCustomlabel3=ISNULL((Select Top 1 CASE
	WHEN (LEN(ISNULL((Select top 1 CrewLogCustomlabel3 from @tbl),''))!=0) 
	THEN  IsPrint
	ELSE 0 
    END From TsFlightLog Where  OriginalDescription Is Not Null 
 and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)  And OriginalDescription IN('Crew Log Custom Label 3','Crew Log Custom Label3','CrewLog Custom label 3')AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) 
 AND Category = 'PILOT LOG'),0) WHERE ROW_ID=2        
 Update @tbl Set CrewLogCustomlabel4=ISNULL((Select Top 1 CASE
	WHEN (LEN(ISNULL((Select top 1 CrewLogCustomlabel4 from @tbl),''))!=0) 
	THEN  IsPrint
	ELSE 0 
    END From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) And OriginalDescription IN('Crew Log Custom Label 4','Crew Log Custom Label4','CrewLog Custom label 4') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) 
 AND Category = 'PILOT LOG'),0) WHERE ROW_ID=2        
 Update @tbl Set ApprNpr=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Approach Non-Precision','Appr  Npr') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set ApprPr=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Approach Precision','Appr  Pr') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set ArrICAO=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Arrival ICAO','Arr. ICAO') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set AssociatedCrew=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Associated Crew' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set BlkIn=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN ('Block In','Blk In') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set BlkOut=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Block Out','Blk Out')  AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set BlockTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Block Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set Client=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Client' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set CrewDutyType=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Crew Duty Type' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set DepICAO=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription in('Departure ICAO','Dep. ICAO') OR OriginalDescription='Dep. ICAO' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set DutyHours=ISNULL((Select Top 1  IsPrint From TsFlightLog Where OriginalDescription Is Not Null and  CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Duty Hours' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1)  WHERE ROW_ID=2        
 Update @tbl Set FlightTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription='Flight Time' AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set FltCat=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Flight Category','Flt Cat') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set FltTyp=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Flight Type','Flt Typ') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set InstTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Instrument Time','Inst Time') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set LndingDt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription in ('Landing Day','Lnding Dt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set LndingNt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription in('Landing Night','Lnding Nt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set NiteTime=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN ('Night Time','Nite Time') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2        
 Update @tbl Set PS=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN ('PIC/SIC','P S','P   S') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set RONs=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN ('RON','RONs') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2   
 Update @tbl Set T_OFFDt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN('Takeoff Day','T/OFF Dt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2         
 Update @tbl Set T_OFFNt=ISNULL((Select Top 1  IsPrint From TsFlightLog Where  OriginalDescription Is Not Null and CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD) 
 And OriginalDescription IN ('Takeoff Night','T/OFF Nt') AND HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) AND Category = 'PILOT LOG'),1) WHERE ROW_ID=2  
     
 SELECT * FROM @tbl        
         
End

-- exec spGetReportPOSTInformation 'jwilliams_13'





GO


