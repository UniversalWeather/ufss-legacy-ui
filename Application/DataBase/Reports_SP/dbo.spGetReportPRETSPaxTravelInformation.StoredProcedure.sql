IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPaxTravelInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPaxTravelInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSPaxTravelInformation]
	(
		 @UserCD VARCHAR(30) --Mandatory
		,@TripID VARCHAR(30) 
		,@LegNUM VARCHAR(50)
		,@BlankLine INT
		,@BlankForm BIT = 0
		,@PassengerCD VARCHAR(300) = ''
	)
AS
BEGIN
-- =========================================================================
-- SPC Name: spGetReportPRETSPaxTravelInformation
-- Author: AISHWARYA.M
-- Create date: 27 JUNE 2013
-- Description: Get Preflight TripSheet Pax Travel Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ==========================================================================
DECLARE @tblTripInfo TABLE
	(    RowID INT IDENTITY,
		TripNumber BIGINT
		,TripDates VARCHAR(25)
		,TailNumber VARCHAR(9)
		,AircraftType VARCHAR(10)
		,TripDescription VARCHAR(40)
		,TripRequestor VARCHAR(60) 
		,ContactPhone VARCHAR(25)
		,DispatchNumber VARCHAR(15)
		,LegNumber BIGINT
		,DIcao CHAR(4)
		,DAirport VARCHAR(25)
		,DCity VARCHAR(25)
		,DStateName VARCHAR(25)
		,Dcountry VARCHAR(60)
		,DLocalTime DATETIME
		,DUTCTime DATETIME
		,AIcao CHAR(4)
		,AAirport VARCHAR(25) 
		,ACity VARCHAR(25)
		,AStateName VARCHAR(25)
		,Acountry VARCHAR(60)
		,ALocalTime DATETIME
		,AUTCTime DATETIME
		,FlightCost NUMERIC(12,2)
		,PaxName VARCHAR(65)
		,Auth VARCHAR(10)
		,Bus VARCHAR(10)
		,Pers VARCHAR(10)
		,PurposeOfFlight CHAR(2)
		,PaxSignature VARCHAR(10)
		,Code VARCHAR(5)
		,PassengerTotal INT
		,OrderNum INT
) 


IF @BlankForm = 1
BEGIN
 INSERT INTO @tblTripInfo
 SELECT DISTINCT TripNumber = PM.TripNUM
			,TripDates =  dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD)
			,TailNumber = F.TailNUM
			,AircraftType =A.AircraftCD
			,TripDescription = PM.TripDescription
			,TripRequestor = ISNULL(P.LastName,'')+ ', ' + ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.MiddleInitial,'')
			,ContactPhone =P.PhoneNum
			,DispatchNumber = RTRIM(PM.DispatchNUM)
			,LegNumber = PL.LegNUM
			--Departure
			,DIcao = DepA.IcaoID
			,DAirport = DepA.AirportName
			,DCity = DepA.CityName
			,DStateName = DepA.StateName
			,Dcountry = DepA.CountryName
			,DLocalTime = PL.DepartureDTTMLocal
			,DUTCTime = PL.DepartureGreenwichDTTM
			--Arrival
			,AIcao = ArrA.IcaoID
			,AAirport = ArrA.AirportName
			,ACity = ArrA.CityName 
			,AStateName = ArrA.StateName
			,Acountry = ArrA.CountryName
			,ALocalTime = PL.ArrivalDTTMLocal
			,AUTCTime = PL.ArrivalGreenwichDTTM
			,FlightCost = PL.FlightCost
			--Pax Details
			,PaxName = CASE WHEN PPL.FlightPurposeID is null THEN NULL ELSE Pax.PassengerName END
			,Auth = ''
			,Bus = ''
			,Pers = ''
			,PurposeOfFlight = FP.FlightPurposeCD
			,PaxSignature = ''
			,PassengerRequestorCD = CASE WHEN PPL.FlightPurposeID IS NULL THEN NULL ELSE PassengerRequestorCD END
			,PL.PassengerTotal
			,OrderNUM = CASE WHEN PPL.FlightPurposeID IS NULL THEN 9999 ELSE OrderNUM END
			
			
		FROM PreflightMain PM
		INNER JOIN 
			( 
				SELECT LegID,PassengerTotal, LegNUM, TripID, DepartICAOID, ArriveICAOID, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM, ArrivalGreenwichDTTM, FlightCost FROM PreflightLeg
				) PL ON PL.TripID = PM.TripID
		INNER JOIN 
			( SELECT LegID, PassengerID, FlightPurposeID,OrderNUM FROM PreflightPassengerList) PPL ON PL.LegID = PPL.LegID
		INNER JOIN
			( SELECT FleetID, TailNUM, AircraftID FROM Fleet) F ON PM.FleetID = F.FleetID
		LEFT OUTER JOIN
			( SELECT AircraftID, AircraftCD FROM Aircraft) A ON A.AircraftID = F.AircraftID
		LEFT OUTER JOIN 
			( SELECT AirportID, AirportName, CityName, CountryName, IcaoID, StateName FROM Airport) DepA ON DepA.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN 
			( SELECT AirportID, AirportName, CityName, CountryName, IcaoID, StateName FROM Airport) ArrA ON ArrA.AirportID = PL.ArriveICAOID
		LEFT OUTER JOIN 
			( SELECT PassengerRequestorID, LastName, FirstName, MiddleInitial, PhoneNum FROM Passenger
				)P ON  PM.PassengerRequestorID = P.PassengerRequestorID
		INNER JOIN 
			( 
			 SELECT PassengerRequestorCD,PassengerRequestorID, LastName, FirstName, MiddleInitial, PassengerName FROM Passenger
			 ) Pax ON Pax.PassengerRequestorID = PPL.PassengerID
		LEFT OUTER JOIN
			(
			 SELECT FlightPurposeCD, FlightPurposeID FROM FlightPurpose 
			 ) FP ON FP.FlightPurposeID = PPL.FlightPurposeID
				
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND PM.TripID = CONVERT(BIGINT, @TripID)
			AND (Pax.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')
			ORDER BY TripNumber, LegNumber, PaxName

			DELETE FROM @tblTripInfo
			WHERE PaxName IS NULL AND OrderNum=9999 AND PassengerTotal<>0
			
        --Blocked Pax
         IF @PassengerCD = NULL OR @PassengerCD = ''
	     BEGIN 
			DECLARE @LegCt INT=1,@LegCnt INT= (SELECT COUNT(DISTINCT LegNumber) FROM @tblTripInfo)
			DECLARE @RowVal INT,@Row INT;

			WHILE @LegCt <= @LegCnt
			BEGIN
	        
				   SET @RowVal=1;
				   SET @Row=(SELECT PassengerTotal-COUNT(Code) FROM @tblTripInfo WHERE LegNumber=@LegCt GROUP BY PassengerTotal,LegNumber )

				   WHILE @RowVal  <= @Row 
		           
				   BEGIN

		           
				   INSERT INTO @tblTripInfo(TripNumber,LegNumber,TripDates,TailNumber,AircraftType,TripDescription,TripRequestor,DispatchNumber,PaxName,OrderNum)
							  SELECT TOP 1 TripNumber,LegNumber,TripDates,TailNumber,AircraftType,TripDescription,TripRequestor,DispatchNumber,'BLOCKED',9999 FROM @tblTripInfo WHERE LegNumber=@LegCt 
				   SET @RowVal=@RowVal+1
		           
				   END
			   SET @RowVal=1
			   SET @LegCt=@LegCt+1
			 END
         END

END



SELECT  @BlankLine = CASE WHEN @BLANKFORM = 0 THEN 20  ELSE @BlankLine END



DECLARE @COUNTER INT=0
DECLARE @LEGCOUNT INT, @LEGCOUNT1 INT=1
SELECT @LEGCOUNT = COUNT(DISTINCT LegNumber) FROM @tblTripInfo

SELECT @LEGCOUNT=CASE WHEN @LEGCOUNT=0 AND @BlankForm=0 THEN 1 ELSE @LEGCOUNT END




WHILE @LEGCOUNT1<=@LEGCOUNT	 
 BEGIN

	 
	WHILE @COUNTER < @BlankLine BEGIN 
		INSERT INTO @tblTripInfo (PaxName, Auth, Bus, Pers, PurposeOfFlight, PaxSignature,LegNumber)
		VALUES (NULL, NULL, NULL, NULL, NULL, NULL,@LEGCOUNT1)
		
		SET @COUNTER = @COUNTER + 1;
	END	
SET @COUNTER=0
SET @LEGCOUNT1=@LEGCOUNT1+1
	
END				
	SELECT * FROM @tblTripInfo ORDER BY LegNumber,CASE WHEN TripNumber IS NOT NULL THEN 999 ELSE 9999 END,OrderNum
END

-- EXEC spGetReportPRETSPaxTravelInformation 'supervisor_99','10099107564','1',10,0



GO


