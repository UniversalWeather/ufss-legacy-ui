IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSInternationalDataCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSInternationalDataCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSInternationalDataCrewInformation]
	@UserCD VARCHAR(30) 
	 ,@CrewPassengerID VARCHAR(30) 
	 ,@TripID AS VARCHAR(50)
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSInternationalDataCrewInformation
-- Author: AISHWARYA.M
-- Create date:27 SEP 2012
-- Description: Get Preflight Tripsheet International Data information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================


	SELECT --[Crew Information]
	CREWID = C.CREWID,
			[CrewLastName]=C.LastName 
			,[CrewFirstName]=C.FirstName
			,[CDOB]= dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,C.BirthDT) 
			,[CNationality]=ISNULL(CO.CountryCD,CTR.CountryCD)
			,[CStreetAddress]= CONVERT(VARCHAR(MAX),C.Addr1) + ' '+ CONVERT(VARCHAR(MAX),C.Addr2)
			,[CPilotLicNbr]=CONVERT(VARCHAR(MAX),C.PilotLicense1)
			,[CCity]=C.CityName
			,[CPassportNo]= ISNULL(CC.PassportNum,'')
			--,[CPassportNo]= dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')) 
			,[CState]=C.StateName
			,[CExpiration]= dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,CC.PassportExpiryDT) 
			,[CZipCode]=CONVERT(VARCHAR(MAX),C.PostalZipCD)
			FROM Crew C
				LEFT OUTER JOIN 
				(SELECT TOP 1 CR.CrewID,CPP.PassportID, CPP.PassportNum, CPP.PassportExpiryDT,
								CPP.IsDeleted, CPP.CountryID, CPP.Choice FROM 
					PreflightMain PM
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					INNER JOIN PreflightCrewList PCL on PL.LegID = PCL.LegID
					INNER JOIN CREW CR ON PCL.CrewID = CR.CrewID
					INNER JOIN CrewPassengerPassport CPP ON PCL.CrewID = CPP.CrewID 
					AND CPP.PassportID = PCL.PassportID
					AND CPP.IsDeleted = 0--AND CPP.Choice = 1
					WHERE PCL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
					AND CR.CrewID = CONVERT(BIGINT,@CrewPassengerID)
					AND PM.TripID = CONVERT(BIGINT,@TripID)
					)CC ON C.CrewID = CC.CrewID
				LEFT OUTER JOIN Country CO ON CO.CountryID = C.Citizenship
				--LEFT OUTER JOIN Country CO ON CO.CountryID = CC.CountryID	 
				LEFT OUTER JOIN Country CTR ON CTR.CountryID = C.CountryID			
			WHERE C.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND C.CrewID = CONVERT(BIGINT,@CrewPassengerID)	
	END
	

	
--EXEC spGetReportPRETSInternationalDataCrewInformation 'UC',1000182580
--EXEC spGetReportPRETSInternationalDataCrewInformation 'SUPERVISOR_99',1001351543,'10099107564'









GO


