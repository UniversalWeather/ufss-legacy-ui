IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTVendorPaymentAnalysisExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTVendorPaymentAnalysisExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTVendorPaymentAnalysisExportInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@VendorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@PaymentTypeCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
	)
AS

BEGIN
SET NOCOUNT ON
SELECT 
      V.Name,
      POE.PurchaseDT,
      F.TailNum,
      A.AccountNum,
      V.VendorCD,
      PT.PaymentTypeCD,
      A.AccountDescription,
      PT.PaymentTypeDescription,
      POE.ExpenseAMT 
FROM PostflightExpense POE
	LEFT OUTER JOIN PaymentType PT ON PT.PaymentTypeID = POE.PaymentTypeID
	LEFT OUTER JOIN Fleet F ON F.FleetID = POE.FleetID
	LEFT OUTER JOIN Vendor V ON V.VendorID = POE.PaymentVendorID
	INNER JOIN Account A ON A.AccountID = POE.AccountID
WHERE POE.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND POE.CustomerID = CONVERT(BIGINT,@UserCustomerID)
	AND POE.IsDeleted=0
	AND CONVERT(DATE,POE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '')
	AND (PT.PaymentTypeCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaymentTypeCD, ',')) OR @PaymentTypeCD = '')
	AND (V.Name IS NOT NULL OR PT.PaymentTypeCD IS NOT NULL)
	ORDER BY V.Name, POE.PurchaseDT
END



GO


