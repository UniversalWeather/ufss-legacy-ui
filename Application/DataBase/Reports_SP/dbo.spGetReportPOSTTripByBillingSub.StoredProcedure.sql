IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripByBillingSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripByBillingSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[spGetReportPOSTTripByBillingSub]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATE ,--Mandatory
	@DATETO AS DATE , --Mandatory
	@LogNum AS VARCHAR(200) = '',
	@PassengerRequestorCD  AS VARCHAR(200) = '',
	@PassengerGroupCD VARCHAR(500)=''
)
AS
-- ================================================================================   
-- SPC Name: [spGetReportPOSTTripByBillingSub]          
-- Author: A.AKHILA         
-- Create date: 08/03/2013          
-- Description: Get Trip Detail by billing SUB for REPORTS          
-- Revision History          
-- Date   Name  Ver  Change          
-- ================================================================================   
BEGIN
	
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
--ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
--BEGIN  
--INSERT INTO @TempPassengerID
--	SELECT DISTINCT  P.PassengerRequestorID
--	FROM Passenger P 
--	WHERE  P.CustomerID = @CUSTOMER  
--	AND P.IsDeleted=0
--END
-----------------------------Passenger and Passenger Group Filteration----------------------	
	
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 

IF @PassengerRequestorCD <> '' OR @PassengerGroupCD <>''

BEGIN

SELECT TEMP.PassengerRequestorCD,--TEMP.EstDepartureDT,
       TEMP.POLogID,
       TEMP.LogNum,
       TEMP.Billing,
       TEMP.CustomerID,
       FLOOR(SUM(TEMP.StdVAlue)*100)*0.01 StdVAlue FROM 
     (	
     SELECT DISTINCT PL.OutboundDTTM,POM.POLogID,PL.LegNUM,PL.PassengerTotal,P.PassengerRequestorCD,POM.LOGNUM,POP.Billing,POM.CustomerID, ChargeRate,ExpenseAMT,ChargeUnit,
PL.Distance,
CASE WHEN PL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(PL.BlockHours,1),0) ELSE ISNULL(ROUND(PL.FlightHours,1),0) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/PL.PassengerTotal,0)
						)
						END 
						StdVAlue
FROM PostflightMain POM
		LEFT OUTER JOIN PostflightLeg PL ON POM.POLogID = PL.POLogID AND PL.IsDeleted=0
		LEFT OUTER JOIN (
					SELECT PM.LogNum, PL.POLegID,PM.CustomerID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
					FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
										   INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND FC.IsDeleted=0
					WHERE CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM) AND PM.IsDeleted=0
				   ) FCR ON PL.POLegID = FCR.POLegID
		LEFT OUTER JOIN PostflightPassenger POP ON PL.POLegID=POP.POLegID
		LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ISNULL(ExpenseAMT,0))ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted=0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted=0
								WHERE PE.IsBilling=1
								  AND PE.IsDeleted=0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID,LogNum)Temp
								LEFT OUTER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID AND PE.IsDeleted=0))POE ON PL.POLegID = POE.POLegID AND PL.CustomerID=POE.CustomerID AND PL.POLogID=POE.POLogID
        LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
        INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID

		 WHERE POM.CUSTOMERID=DBO.GetCustomerIDbyUserCD(@UserCD)
		 AND POM.IsDeleted=0
		 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND  POM.LogNum=@LogNum
				 )
TEMP
WHERE TEMP.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	--AND CONVERT(DATE,TEMP.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (TEMP.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
   --AND (TEMP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
GROUP BY TEMP.POLogID,TEMP.LogNum,TEMP.Billing,TEMP.CustomerID,TEMP.PassengerRequestorCD
END
ELSE


BEGIN

SELECT TEMP.PassengerRequestorCD,--TEMP.EstDepartureDT,
       TEMP.POLogID,
       TEMP.LogNum,
       TEMP.Billing,
       TEMP.CustomerID,
       FLOOR(SUM(TEMP.StdVAlue)*100)*0.01 StdVAlue FROM 
     (	
     SELECT DISTINCT PL.OutboundDTTM,POM.POLogID,PL.LegNUM,PL.PassengerTotal,P.PassengerRequestorCD,POM.LOGNUM,POP.Billing,POM.CustomerID, ChargeRate,ExpenseAMT,ChargeUnit,
PL.Distance,
CASE WHEN PL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(PL.BlockHours,1),0) ELSE ISNULL(ROUND(PL.FlightHours,1),0) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/PL.PassengerTotal,0)
						)
						END 
						StdVAlue
FROM PostflightMain POM
		LEFT OUTER JOIN PostflightLeg PL ON POM.POLogID = PL.POLogID AND PL.IsDeleted=0
		LEFT OUTER JOIN (
					SELECT PM.LogNum, PL.POLegID,PM.CustomerID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
					FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
										   INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND FC.IsDeleted=0 
					WHERE CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM) AND PM.IsDeleted=0
				   ) FCR ON PL.POLegID = FCR.POLegID
		LEFT OUTER JOIN PostflightPassenger POP ON PL.POLegID=POP.POLegID
		LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ISNULL(ExpenseAMT,0))ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted=0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted=0
								WHERE PE.IsBilling=1
								  AND PE.IsDeleted=0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID,LogNum)Temp
								LEFT OUTER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID AND PE.IsDeleted=0))POE ON PL.POLegID = POE.POLegID AND PL.CustomerID=POE.CustomerID AND PL.POLogID=POE.POLogID
        LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
        --INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID

		 WHERE POM.CUSTOMERID=DBO.GetCustomerIDbyUserCD(@UserCD)
		 AND POM.IsDeleted=0
		 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND  POM.LogNum=@LogNum
				 )
TEMP
WHERE TEMP.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	--AND CONVERT(DATE,TEMP.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (TEMP.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
   --AND (TEMP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
GROUP BY TEMP.POLogID,TEMP.LogNum,TEMP.Billing,TEMP.CustomerID,TEMP.PassengerRequestorCD
END


END
--EXEC spGetReportPOSTTripByBillingSub 'SUPERVISOR_99','10099','2000/1/1','2010/1/1',''


GO


