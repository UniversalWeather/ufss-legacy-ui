IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteLogExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteLogExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportCQQuoteLogExportInformation]
	 @UserCD VARCHAR(30)--='CHIP_112'    
     ,@UserCustomerID VARCHAR(30)---='10112'    
     ,@FileNumber VARCHAR(200)---='23931',    
     ,@QuoteNUM VARCHAR(200)--='1',    
     ,@LegNUM VARCHAR(200)=''   
     ,@BeginDate DATETIME=NULL    
     ,@EndDate DATETIME=NULL    
     ,@TailNum VARCHAR(200)=''    
     ,@VendorCD VARCHAR(200)=''    
     ,@HomeBaseCD VARCHAR(200)=''    
     ,@AircraftCD VARCHAR(200)=''   
     ,@CQCustomerCD VARCHAR(30)=''  
     ,@SalesPersonCD VARCHAR(200)='' 


AS 
-- =============================================
-- SPC Name:spGetReportCQQuoteLogExportInformation
-- Author: Aishwarya.M
-- Create date: 06 Mar 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON

 DECLARE @TenToMin NUMERIC(1,0);
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 


DECLARE @QuoteTotal TABLE(Row_Id INT,
                          description VARCHAR(60),
                          value NUMERIC(17,3),
                          Quotation NUMERIC(17,2),
                          units VARCHAR(10),
						  FileNumber BIGINT,
						  QuoteNum INT)





IF @FileNumber <> ''
BEGIN
INSERT INTO @QuoteTotal
SELECT Quote.* FROM 
(
SELECT DISTINCT 1 Row_Id,InfoDesc=ISNULL(QuoteMinimumUse2FeeDepositAdj,'DAILY USEAGE ADJUSTMENT:')
               ,InfoDescValue= ISNULL(CM.AdjAmtTotal,0)
               ,Quotation=ISNULL(CM.UsageAdjTotal,0)
               ,'HRS'  units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
	WHERE CQF.CustomerID=@UserCustomerID
	  AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	  AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		

UNION ALL

SELECT DISTINCT 2 Row_Id,InfoDesc=ISNULL(C.QuoteLandingFeeDefault,'LANDING FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.LandingFeeTotal,0)
               ,'' units
               ,CQF.FileNUM FileNumber
                ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		
UNION ALL

SELECT DISTINCT 3 Row_Id,InfoDesc=ISNULL(QuoteSegment2FeeDeposit,'SEGMENT FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.SegmentFeeTotal,0)
               ,'' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
	
UNION ALL

SELECT DISTINCT 4 Row_Id,InfoDesc=ISNULL(C.QouteStdCrew2RONDeposit,'CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.StdCrewRONTotal,0)
               ,'RON' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		
UNION ALL

SELECT DISTINCT 5 Row_Id,InfoDesc=ISNULL(C.AdditionalCrewCD,'ADDITIONAL CREW CHARGE:')
               ,InfoDescValue=ISNULL(CM.AdditionalCrewRONTotal1,0) 
               ,Quotation=ISNULL(CM.AdditionalCrewTotal,0)
               ,'CREW' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	     AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		
UNION ALL

SELECT DISTINCT 6 Row_Id,InfoDesc=ISNULL(C.QuoteAdditionCrewRON,'ADDITIONAL CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.AdditionalCrewRONTotal2,0)
               ,'RON' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		
UNION ALL

SELECT DISTINCT 7 Row_Id,InfoDesc=ISNULL(C.QuoteWaiting2Charge,'WAIT CHARGE:')
               ,InfoDescValue=ISNULL(CM.WaitTMTotal,0) 
               ,Quotation=ISNULL(CM.WaitChgTotal,0)
               ,'HRS' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )
		
UNION ALL

SELECT DISTINCT 8 Row_Id,InfoDesc=ISNULL(C.Deposit2FlightCharge,'FLIGHT CHARGE:')
               ,InfoDescValue=CONVERT(VARCHAR(30),CM.FlightHoursTotal)
               ,Quotation=ISNULL(CM.FlightChargeTotal,0)
               ,'HRS' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.FileNUM = CONVERT(BIGINT, @FileNumber)
	      AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )


		
	
)Quote


END

ELSE

BEGIN

INSERT INTO @QuoteTotal
SELECT Quote.* FROM 
(
SELECT DISTINCT 1 Row_Id,InfoDesc=ISNULL(QuoteMinimumUse2FeeDepositAdj,'DAILY USEAGE ADJUSTMENT:')
               ,InfoDescValue= ISNULL(CM.AdjAmtTotal,0)
               ,Quotation=ISNULL(CM.UsageAdjTotal,0)
               ,'HRS'  units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
	WHERE CQF.CustomerID=@UserCustomerID
	 AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		

UNION ALL

SELECT DISTINCT 2 Row_Id,InfoDesc=ISNULL(C.QuoteLandingFeeDefault,'LANDING FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.LandingFeeTotal,0)
               ,'' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		
UNION ALL

SELECT DISTINCT 3 Row_Id,InfoDesc=ISNULL(QuoteSegment2FeeDeposit,'SEGMENT FEES:')
               ,InfoDescValue=NULL
               ,Quotation=ISNULL(CM.SegmentFeeTotal,0)
               ,'' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
	
UNION ALL

SELECT DISTINCT 4 Row_Id,InfoDesc=ISNULL(C.QouteStdCrew2RONDeposit,'CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.StdCrewRONTotal,0)
               ,'RON' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		
UNION ALL

SELECT DISTINCT 5 Row_Id,InfoDesc=ISNULL(C.AdditionalCrewCD,'ADDITIONAL CREW CHARGE:')
               ,InfoDescValue=ISNULL(CM.AdditionalCrewRONTotal1,0) 
               ,Quotation=ISNULL(CM.AdditionalCrewTotal,0)
               ,'CREW' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		
UNION ALL

SELECT DISTINCT 6 Row_Id,InfoDesc=ISNULL(C.QuoteAdditionCrewRON,'ADDITIONAL CREW OVERNIGHT CHARGE:')
               ,InfoDescValue=ISNULL(CM.BillingCrewRONTotal,0)
               ,Quotation=ISNULL(CM.AdditionalCrewRONTotal2,0)
               ,'RON' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		
UNION ALL

SELECT DISTINCT 7 Row_Id,InfoDesc=ISNULL(C.QuoteWaiting2Charge,'WAIT CHARGE:')
               ,InfoDescValue=ISNULL(CM.WaitTMTotal,0) 
               ,Quotation=ISNULL(CM.WaitChgTotal,0)
               ,'HRS' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
	      AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
		
UNION ALL

SELECT DISTINCT 8 Row_Id,InfoDesc=ISNULL(C.Deposit2FlightCharge,'FLIGHT CHARGE:')
               ,InfoDescValue=CM.FlightHoursTotal
               ,Quotation=ISNULL(CM.FlightChargeTotal,0)
               ,'HRS' units
               ,CQF.FileNUM FileNumber
               ,CM.QuoteNUM
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN Company C ON C.HomebaseID=CQF.HomebaseID				
		WHERE CQF.CustomerID=@UserCustomerID
		  AND CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 

		
	
)Quote


END


DECLARE @FileInfo TABLE(flag CHAR(1),
                        cqfilenum BIGINT,
                        quotenum INT,
                        quoteid INT,
                        lowdate DATE,
                        highdate DATE,
                        tail_nmbr VARCHAR(9),
                        type_code VARCHAR(10),
                        [desc] VARCHAR(40),
                        custname VARCHAR(40),
                        depicao_id CHAR(4),
                        arricao_id CHAR(4),
                        distance NUMERIC(5,0),
                        elp_time NUMERIC(7,3),
                        description VARCHAR(60),
                        value NUMERIC(9,3),
                        units VARCHAR(10),
                        quotation NUMERIC(17,2),
                        discount  NUMERIC(17,2),
                        tax NUMERIC(17,2),
                        totquote NUMERIC(17,2),
                        tottaxes NUMERIC(17,2),
                        totdiscamt NUMERIC(17,2),
                        currcode VARCHAR(10),
                        LegNum BIGINT,
                        rnk INT
                        
						)

IF @BeginDate IS NOT NULL  
 BEGIN
INSERT INTO  @FileInfo
  	SELECT   flag='A'
			 ,cqfilenum=CQF.FileNUM
			 ,quotenum=CM.QuoteNUM
			 ,quoteid= CM.CQMainID--CM.QuoteID
			 ,lowdate=CONVERT(DATE,QL.DepartureDTTMLocal)
			 ,highdate=CONVERT(DATE,QL.ArrivalDTTMLocal)
			 ,tail_nmbr=F.TailNum
			 ,type_code=AT.AircraftCD
			 ,[desc]=CQF.CQFileDescription
			 ,custname=CQF.CQCustomerName
			 ,depicao_id=D.IcaoID
			 ,arricao_id=A.IcaoID
			 ,distance=CL.Distance
			 ,elp_time=CL.ElapseTM
			 ,description=NULL
			 ,value=NULL
			 ,units=NULL
			 ,quotation=NULL
			 ,discount=NULL
			 ,tax=NULL
			 ,totquote=CM.QuoteTotal
			 ,tottaxes=TaxesTotal
			 ,totdiscamt=DiscountAmtTotal
			 ,currcode=NULL
			 ,CL.LegNUM
			 ,ROW_NUMBER()OVER(PARTITION BY CM.CQMainID,CM.QuoteNum ORDER BY CM.CQMainID,CM.QuoteNum,CL.CQLegID DESC)
		 FROM CQFile CQF
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
			  INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0
			  INNER JOIN(SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CQMainID) QL ON CL.CQMainID=QL.CQMainID 
			  INNER JOIN Fleet F ON F.FleetID=CM.FleetID
			  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=CM.AircraftID
			  LEFT OUTER JOIN Airport D ON D.AirportID=CL.DAirportID
			  LEFT OUTER JOIN Airport A ON A.AirportID=CL.AAirportID
			  LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID    
			  LEFT OUTER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID    
              LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CQF.CQCustomerID  
              INNER JOIN Company C ON CQF.HomebaseID=C.HomebaseID
              LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID
	  WHERE CQF.EstDepartureDT BETWEEN @BeginDate AND @EndDate 
	    AND CM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	    AND(F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )    
		AND (V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )    
		AND (AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )    
		AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )    
		AND (SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )     
		AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
		ORDER BY CQF.FileNUM,CM.QuoteNUM,CL.LegNUM
END
   
ELSE
 
BEGIN
INSERT INTO  @FileInfo
  	SELECT   flag='A'
			 ,cqfilenum=CQF.FileNUM
			 ,quotenum=CM.QuoteNUM
			 ,quoteid= CM.CQMainID--CM.QuoteID
			 ,lowdate=CONVERT(DATE,QL.DepartureDTTMLocal)
			 ,highdate=CONVERT(DATE,QL.ArrivalDTTMLocal)
			 ,tail_nmbr=F.TailNum
			 ,type_code=AT.AircraftCD
			 ,[desc]=CQF.CQFileDescription
			 ,custname=CQF.CQCustomerName
			 ,depicao_id=D.IcaoID
			 ,arricao_id=A.IcaoID
			 ,distance=CL.Distance
			 ,elp_time=CL.ElapseTM
			 ,description=NULL
			 ,value=NULL
			 ,units=NULL
			 ,quotation=NULL
			 ,discount=NULL
			 ,tax=NULL
			 ,totquote=CM.QuoteTotal
			 ,tottaxes=TaxesTotal
			 ,totdiscamt=DiscountAmtTotal
			 ,currcode=NULL
			 ,CL.LegNUM
			 ,ROW_NUMBER()OVER(PARTITION BY CM.CQMainID,CM.QuoteNum ORDER BY CM.CQMainID,CM.QuoteNum,CL.CQLegID DESC)
		 FROM CQFile CQF
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
			  INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0
			  INNER JOIN(SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CQMainID) QL ON CL.CQMainID=QL.CQMainID 
			  INNER JOIN Fleet F ON F.FleetID=CM.FleetID
			  LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=CM.AircraftID
			  LEFT OUTER JOIN Airport D ON D.AirportID=CL.DAirportID
			  LEFT OUTER JOIN Airport A ON A.AirportID=CL.AAirportID
		WHERE CM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
		 AND (CQF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )    
         AND (CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )    
         AND (CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' ) 
END


SELECT flag,
       cqfilenum,
       quotenum,
       quoteid,
       lowdate,
       highdate,
       tail_nmbr,
       type_code,
       [desc],
       custname,
       depicao_id,
       arricao_id,
       distance,
       elp_time=FLOOR(elp_time*10)*0.1 ,
       description,
       value=FLOOR(value*10)*0.1,
       units,
       quotation=FLOOR(ROUND(ISNULL(quotation,0),0)),
       discount=ISNULL(discount,0),
       tax=ISNULL(tax,0),
       totquote=FLOOR(ROUND(totquote,0)),
       tottaxes=FLOOR(ROUND(ISNULL(tottaxes,0),0)),
       totdiscamt=FLOOR(ROUND(ISNULL(totdiscamt,0),0)),
       currcode FROM (
SELECT flag,
       cqfilenum,
       quotenum,
       quoteid,
       lowdate,
       highdate,
       tail_nmbr,
       type_code,
       [desc],
       custname,
       depicao_id,
       arricao_id,
       distance,
       elp_time,
       FI.description,
       FI.value,
       FI.units,
       FI.quotation,
       discount,
       tax,
       totquote,
       tottaxes,
       totdiscamt,
       currcode,LegNum  FROM @FileInfo FI

UNION ALL

SELECT flag='B',
       cqfilenum,
       FI.quotenum,
       quoteid,
       lowdate,
       highdate,
       tail_nmbr,
       type_code,
       [desc],
       custname,
       depicao_id,
       arricao_id,
       distance,
       elp_time,
       QT.description,
       QT.value,
       QT.units,
       QT.quotation,
       discount,
       tax,
       totquote,
       tottaxes,
       totdiscamt,
       currcode,LegNum  FROM @QuoteTotal QT
                      INNER JOIN @FileInfo FI ON FI.cqfilenum=QT.FileNumber AND FI.quotenum=QT.QuoteNum AND rnk=1


 )TEMP
 ORDER BY cqfilenum,QuoteNUM,LegNum,flag

END

--EXEC spGetReportCQQuoteLogExportInformation 'SUPERVISOR_99','10001','4'	  

GO


