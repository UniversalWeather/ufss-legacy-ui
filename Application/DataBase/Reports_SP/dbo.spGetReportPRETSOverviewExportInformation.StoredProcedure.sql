IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSOverviewExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSOverviewExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSOverviewExportInformation]
  	@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
	
AS
-- ================================================================================
-- SPC Name: spGetReportPRETSOverviewInformation
-- Author: ABHISHEK.S
-- Create date: 10th September 2012
-- Description: Get TripSheet Report Writer Overview Export Information 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ================================================================================


BEGIN

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	SET NOCOUNT ON;
	
	DECLARE @SQLSCRIPT AS NVARCHAR(MAX) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(500);
	
--	SET @SQLSCRIPT ='
	    SELECT 
	    [lowdate]     = CONVERT(VARCHAR(10),PLTrip.Departure,dbo.GetReportDayMonthFormatByUserCD(@UserCD)),
	    [highdate]    = CONVERT(VARCHAR(10),PLTrip.Arrive,dbo.GetReportDayMonthFormatByUserCD(@UserCD)),
        [orig_nmbr]   = PM.TripNUM,
        [dispatchno]  = PM.DispatchNUM,
		[tail_nmbr]   = F.TailNum,
		[type_code]   = AC.AircraftCD,
		[desc]        = PM.TripDescription,
		[reqcode]     = PR.PassengerRequestorCD,	 --PM.PassengerRequestorID,
		[paxname]     =	PR.LastName + ',' + PR.FirstName + ' ' + PR.MiddleInitial, 
		--[paxname]     =	PLI.PassengerLastName+','+PassengerFirstName+' '+ PassengerMiddleName, 
		[phone]       = PR.PhoneNum,
		[leg_num]     =	PL.LegNum,
		[legid]       = PL.LegID,
		[timeapprox]  =	PL.IsApproxTM,
		[depicao_id]  = AD.IcaoID, --PL.DepartICAOID,
		[depcity]     =	AD.CityName,
		[arricao_id]  =	AA.IcaoID, --PL.ArriveICAOID,
		[arrcity]     =	AA.CityName,
		[pax_total]   = PL.PassengerTotal,
		[depzulu]     =	AD.OffsetToGMT, 
		[arrzulu]     = AA.OffsetToGMT, 
		[locdep]      =	PL.DepartureDTTMLocal,
		[locarr]      =	PL.ArrivalDTTMLocal,
		[gmtdep]      =	PL.DepartureGreenwichDTTM,
		[gmtarr]      =	PL.ArrivalGreenwichDTTM,
		[distance]    = PL.Distance,
		[elp_time]    = PL.ElapseTM,
		[code]        = FBOA.FBOCD, --PL.FBOID,
		[purpose]     =	PL.FlightPurpose,
		[arr_name]    = FBOA.FBOVendor,
		[arr_fbophone]= FBOA.PhoneNUM1,
		[arr_fuel_brand] = FBOA.FuelBrand,
		[arr_freq]    = FBOA.Frequency,
		[dep_name]    = FBOD.FBOVendor,
		[dep_fbophone]= FBOD.PhoneNUM1,
		[dep_fuel_brand] = FBOD.FuelBrand,
		[dep_freq]    = FBOD.Frequency,
		--[fuel_burn]   = dbo.FuelBurn(F.AircraftCD,PL.ElapseTM,PL.PowerSetting),
		[fuel_burn]   = dbo.FuelBurn(@UserCD,AC.AircraftCD,PL.ElapseTM,PL.PowerSetting),
		[ac_code]     =	F.AircraftCD,
		[CREWMEMBERS] = RTRIM(Crew.CrewList)
		
			FROM ( SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main
				INNER JOIN PREFLIGHTMAIN PM ON Main.TripID = PM.TripID
				INNER JOIN  PreflightLeg PL ON Main.LegID = PL.LegID
				INNER JOIN (SELECT CustomerID,TripID,TripNUM,MIN(DepartureDTTMLocal) Departure,MAX(ArrivalDTTMLocal) Arrive FROM  PreflightLeg GROUP BY CustomerID,TripID,TripNUM)PLTrip ON PLTrip.TripID=PM.TripID AND PM.CustomerID=PLTrip.CustomerID 
				INNER JOIN ( SELECT DISTINCT TailNum,FleetID,AircraftCD FROM FLEET) F ON PM.FleetID = F.FleetID
				INNER JOIN AIRPORT AD ON PL.DepartICAOID = AD.AirportID
				INNER JOIN AIRPORT AA ON PL.ArriveICAOID = AA.AirportID
				LEFT OUTER JOIN Aircraft AC ON  PM.AircraftID = AC.AircraftID
				--INNER JOIN PreflightPassengerList PLI ON PL.LegID = PLI.LegID
				LEFT JOIN Passenger PR ON PM.PassengerRequestorID = PR.PassengerRequestorID		
			 --   INNER JOIN Company C ON PM.CustomerID = C.CustomerID AND C.HomebaseID = PM.HomebaseID
			    LEFT OUTER JOIN (
					SELECT PF.LegID, FD.FBOVendor, FD.PhoneNUM1, FD.Frequency , FD.FuelBrand, FD.FBOCD
					FROM PreflightFBOList PF
					INNER JOIN FBO FD ON PF.FBOID = FD.FBOID AND PF.IsDepartureFBO = 1
				) FBOD ON PL.LegID = FBOD.LegID

			    LEFT OUTER JOIN (
					SELECT PF.LegID, FA.FBOVendor, FA.PhoneNUM1, FA.Frequency , FA.FuelBrand, FA.FBOCD
					FROM PreflightFBOList PF
					INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1
				) FBOA ON PL.LegID = FBOA.LegID

				 LEFT OUTER JOIN (          
					SELECT DISTINCT PC2.LegID, CrewList = (          
					SELECT C.CrewCD + ' '          
					FROM PreflightCrewList PC1          
					INNER JOIN Crew C ON PC1.CrewID = C.CrewID          
					WHERE PC1.LegID = PC2.LegID 
					ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
                                   WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)         
					FOR XML PATH('')          
					)                   
					FROM PreflightCrewList PC2           
				 ) Crew ON PL.LegID = Crew.LegID  
     
		WHERE PM.IsDeleted = 0
		ORDER BY PM.TripNUM
           	 
   --PRINT @SQLSCRIPT
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@TripNum AS NVARCHAR(100), @Leg AS BIGINT '
 --   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	, @TripNum , @Leg
    
   --EXEC spGetReportPRETSOverviewExportInformation 'UC', 50, ''
 
 -- EXEC spGetReportPRETSOverviewExportInformation 'jwilliams_11', '', '', '', '20121030', '20121031', '', '', '', '', '', '', '','',1,1,1,1,1,1	           
	
END



GO


