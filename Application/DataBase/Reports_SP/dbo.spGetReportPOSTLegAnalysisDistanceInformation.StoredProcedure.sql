IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTLegAnalysisDistanceInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTLegAnalysisDistanceInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





        
CREATE PROCEDURE [dbo].[spGetReportPOSTLegAnalysisDistanceInformation]                                        
(                                  
  @UserCD AS VARCHAR(30),  --Mandatory                                  
  @DATEFROM DATETIME, --Mandatory                                  
  @DATETO DATETIME, --Mandatory                                  
  @TailNum VARCHAR(5000)='',--Optional                                  
  @FleetGroupCD VARCHAR(5000)=''--Optional                                  
 )            
                                     
AS                                    
-- =============================================                                      
-- Author: Mathes                 
-- ALtered 05-10-12                                     
-- Description: Leg Analysis/Distance(FC) - Report        
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package                               
-- =============================================                                      
                                    
SET NOCOUNT ON                                      
BEGIN                                      
 
 Declare @DateRange Varchar(30)
 Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)                                   
 
  DECLARE @CUSTOMERID BIGINT = (SELECT CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))) 
  Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))                                 
                                         
 
----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		AircraftCD VARCHAR(4),
		TailNumber VARCHAR(9)	
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID ,F.AircraftCD,F.TailNum
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.AircraftCD,F.TailNum
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.AircraftCD,F.TailNum
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMERID  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END

-----------------------------TailNum and Fleet Group Filteration---------------------- 
                 
   DECLARE  @TempDist Table(DistId Int,DistFrom Int,DistTo Int)                   
   INSERT INTO @TempDist (DistId,DistFrom,DistTo)                              
   Select 1,0,24  union All                               
   Select 2,25,49  union All                               
   Select 3,50,74  union All                               
   Select 4,75,99  union All                               
   Select 5,100,124  union All                               
   Select 6,125,149  union All                               
   Select 7,150,174  union All                               
   Select 8,175,199  union All                   
   Select 9,200,249  union All                               
   Select 10,250,299  union All                               
   Select 11,300,399  union All                               
   Select 12,400,499  union All                               
   Select 13,500,599  union All                               
   Select 14,600,699  union All                               
   Select 15,700,799  union All                               
   Select 16,800,899  union All                               
   Select 17,900,999  union All                               
   Select 18,1000,1199  union All                               
   Select 19,1200,1399  union All                               
   Select 20,1400,1599  union All                               
   Select 21,1600,1799  union All                               
   Select 22,1800,1999  union All                               
   Select 23,2000,2199  union All                               
   Select 24,2200,2399  union All                               
   Select 25,2400,2599  union All                               
   Select 26,2600,2799  union All                               
   Select 27,2800,2999  union All                               
   Select 28,3000,3199  union All                               
   Select 29,3200,3999  union All                               
   Select 30,4000,9999                                              
    
 Create Table #TempReport (SuppAft INT,DistId Int, FromMile Int,ToMile Int,FleetID BIGINT,TailNum Varchar(9),AircraftCD CHAR(6),NoOfLegs Int,                       
                               BlockHours Numeric(10,2),FlightHours Numeric(10,2),  Miles Int, FuelBurn Int, NoOfPax Int,            
                               TotLine Char(1), AvgBleg Numeric(10,2))                                

                       

INSERT INTO #TempReport(DistId,AircraftCD,FromMile,ToMile,TailNum,FleetID,SuppAft)
SELECT TD.DistId,TF.AircraftCD,TD.DistFrom,TD.DistTo,TF.TailNumber,TF.FleetID,0 FROM @TempFleetID TF CROSS JOIN @TempDist TD ORDER BY AircraftCD



UPDATE #TempReport SET NoOfLegs=Dist.NoOfLegs
					  ,BlockHours=Dist.BlockHours
					  ,FlightHours=Dist.FlightHours
					  ,NoOfPax=Dist.NoOfPax
					  ,FuelBurn=FuelUsed
					  ,Miles=Dist.Miles
					  ,AvgBleg=FuelUsed/Dist.NoOfLegs
					  ,SuppAft=1
					    FROM(
					                           SELECT   AircraftCD,
														TailNumber, 
														FleetID,                      
														IsNull(Sum(ROUND(BlockHours,1)),0) As BlockHours,                        
														IsNull(Sum(ROUND(FlightHours,1)),0) As FlightHours,                         
														IsNull(Sum(Miles),0) As Miles,                         
														IsNull(Sum(FuelUsed),0) As FuelUsed,                         
														ISNULL(COUNT(NoOfLegs), 0) AS NoOfLegs,    
														ISNULL(SUM(NoOfPax), 0) AS NoOfPax ,
														Distance FROM (
					                           SELECT	F.AircraftCD,
														TF.TailNumber, 
														TF.FleetID,                      
														IsNull(ROUND(PostflightLeg.BlockHours,1),0) As BlockHours,                        
														IsNull(ROUND(PostflightLeg.FlightHours,1),0) As FlightHours,                         
														IsNull(PostflightLeg.Distance,0) As Miles,                         
														IsNull(PostflightLeg.FuelUsed,0) As FuelUsed,                         
														ISNULL(PostflightLeg.POLegID, 0) AS NoOfLegs,    
														ISNULL(PostflightLeg.PassengerTotal, 0) AS NoOfPax 
														,CASE WHEN Distance BETWEEN 0 AND 24 THEN 1 
														      WHEN Distance BETWEEN 25 AND 49 THEN 2 
														      WHEN Distance BETWEEN 50 AND 74 THEN 3 
														      WHEN Distance BETWEEN 75 AND 99 THEN 4 
														      WHEN Distance BETWEEN 100 AND 124 THEN 5 
														      WHEN Distance BETWEEN 125 AND 149 THEN 6 
														      WHEN Distance BETWEEN 150 AND 174 THEN 7 
														      WHEN Distance BETWEEN 175 AND 199 THEN 8 
														      WHEN Distance BETWEEN 200 AND 249 THEN 9 
														      WHEN Distance BETWEEN 250 AND 299 THEN 10 
														      WHEN Distance BETWEEN 300 AND 399 THEN 11 
														      WHEN Distance BETWEEN 400 AND 499 THEN 12 
														      WHEN Distance BETWEEN 500 AND 599 THEN 13 
														      WHEN Distance BETWEEN 600 AND 699 THEN 14 
														      WHEN Distance BETWEEN 700 AND 799 THEN 15 
														      WHEN Distance BETWEEN 800 AND 899 THEN 16 
														      WHEN Distance BETWEEN 900 AND 999 THEN 17 
														      WHEN Distance BETWEEN 1000 AND 1199 THEN 18 
														      WHEN Distance BETWEEN 1200 AND 1399 THEN 19 
														      WHEN Distance BETWEEN 1400 AND 1599 THEN 20 
														      WHEN Distance BETWEEN 1600 AND 1799 THEN 21 
														      WHEN Distance BETWEEN 1800 AND 1999 THEN 22 
														      WHEN Distance BETWEEN 2000 AND 2199 THEN 23
														      WHEN Distance BETWEEN 2200 AND 2399 THEN 24 
														      WHEN Distance BETWEEN 2400 AND 2599 THEN 25
														      WHEN Distance BETWEEN 2600 AND 2799 THEN 26 
														      WHEN Distance BETWEEN 2800 AND 2999 THEN 27
														      WHEN Distance BETWEEN 3000 AND 3199 THEN 28  
														      WHEN Distance BETWEEN 3200 AND 3999 THEN 29 
														      WHEN Distance BETWEEN 4000 AND 9999 THEN 30  END Distance              
														FROM  PostflightMain 
															  INNER JOIN PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID and PostflightLeg.IsDeleted = 0               
															  INNER JOIN (SELECT FleetID,AircraftCD,TailNumber FROM @TempFleetID)F ON F.FleetID=PostflightMain.FleetID  
															  INNER JOIN @TempFleetID TF ON TF.FleetID=PostflightMain.FleetID
														WHERE PostflightMain.IsDeleted = 0                
														 AND  PostflightLeg.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                                                                        
														 AND Convert(Date,PostflightLeg.ScheduledTM) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO) 
														)TEMP
														GROUP BY AircraftCD,TailNumber,Distance,FleetID
                                                   )Dist 
                                              WHERE Dist.FleetID=#TempReport.FleetID
                                                AND Distance = #TempReport.DistId


          
 IF @SuppressActivityAircft=1
 BEGIN    
   Select AircraftCD ,            
          TailNum,            
          FromMile,            
          ToMile,             
          IsNull(NoOfLegs,0) NoOfLegs,  
          ISNULL(BlockHours,0) BlockHours,
          ISNULL(FlightHours,0) FlightHours,   
          IsNull(Miles,0) Miles,            
          IsNull(FuelBurn,0) FuelBurn,            
          IsNull(AvgBleg,0) AvgBleg,            
          IsNull(NoOfPax,0) NoOfPax, 
          @TenToMin TenToMin,
          @DateRange DateRange FROM #TempReport 
                               WHERE FleetID IN (SELECT FleetID FROM #TempReport WHERE SuppAft=1)
						       ORDER BY AircraftCD       
END
ELSE
BEGIN
  Select AircraftCD ,            
          TailNum,            
          FromMile,            
          ToMile,             
          IsNull(NoOfLegs,0) NoOfLegs,  
          ISNULL(BlockHours,0) BlockHours,
          ISNULL(FlightHours,0) FlightHours,   
          IsNull(Miles,0) Miles,            
          IsNull(FuelBurn,0) FuelBurn,            
          IsNull(AvgBleg,0) AvgBleg,            
          IsNull(NoOfPax,0) NoOfPax, 
          @TenToMin TenToMin,
          @DateRange DateRange FROM #TempReport  ORDER BY AircraftCD  
          
END

IF OBJECT_ID('tempdb..#TempReport') is not null                              
DROP table #TempReport 
                     
              
                                
                                 
 END       
       
 --Exec spGetReportPOSTLegAnalysisDistanceInformation 'eliza_9','01-Jan-1990','31-Dec-1991',''
 



GO


