
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrency2ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2ExportInformation]                                  
(
   @UserCD AS VARCHAR(30), --Mandatory                         
   @AsOf DateTime, --Mandatory                        
   @CrewCD Varchar(5000)='',                         
   @CrewGroupCD Varchar(5000)='',                         
   @AircraftCD Varchar(5000)='',                
   @IsInactive Bit=0
)                            
AS                                
                                
-- ===============================================================================                                
-- SPC Name: spGetReportPOSTCrewCurrency2ExportInformation                              
-- Author:  Mathes                                
-- Create date: 30 Aug 2012                                
-- Description: Get Crew Currency for REPORTS                                
-- Revision History                                
-- Date   Name  Ver  Change                                
--                                 
-- ================================================================================                                

SET NOCOUNT ON                           
 
BEGIN  
   
   DECLARE @TempCrew TABLE(CrewID BigInt) 
   Insert Into @TempCrew SELECT DISTINCT C.CrewID 
   FROM PostflightLeg inner JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0 INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID left JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID 
                      LEFT JOIN                      
   ( SELECT DISTINCT C.CrewID,C.CrewCD,C.FirstName,C.LastName,C.CustomerID,C.IsStatus,C.MiddleInitial,CG.CrewGroupCD 
     FROM  Crew C                 
     left JOIN CrewGroupOrder CGO                
     ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID                 
     left JOIN CrewGroup CG                 
     ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID   
     WHERE (CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) OR @CrewGroupCD = '')  
   ) C ON C.CrewID = PostflightCrew.CrewID 
   WHERE C.IsStatus =1 AND PostflightLeg.CustomerID =   + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
   AND  (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '') 
   AND PostflightLeg.IsDeleted=0                           
   AND Convert(Date,PostflightLeg.ScheduledTM)<=@AsOf
 

DECLARE @ldQtrBeg DATE      
DECLARE @ldPQtrBeg DATE      
DECLARE @lnQtrSec BIGINT   
DECLARE @lnQtrHrs BIGINT   
      
DECLARE @FIRST_DAY_MONTH DATE      
DECLARE @FIRST_DAY_YEAR DATE      
   
SET @FIRST_DAY_MONTH=(SELECT CONVERT(DATE,DATEADD(MONTH, DATEDIFF(MONTH, 0, @AsOf), 0)))      
SET @FIRST_DAY_YEAR=( SELECT CONVERT(DATE,DATEADD(YEAR, DATEDIFF(YEAR, 0, @AsOf), 0)))      

--DECLARE @TimeDisplayTenMin NUMERIC(1,0) =1 
DECLARE @TimeDisplayTenMin SMALLINT = 0;
SELECT @TimeDisplayTenMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @AircraftBasis NUMERIC(3,0)      
SELECT @AircraftBasis=AircraftBasis FROM  Company,Crew       
                     WHERE Company.homebaseairportid=Crew.HomebaseID       
                     AND  Crew.CrewID In (Select Distinct CrewID From @TempCrew)
 
DECLARE @TempPO TABLE(RowID INT IDENTITY,
	AircraftID BIGINT,  
    AircraftCD VARCHAR(10),
    FleetID BIGINT,      
    POLogID BIGINT,      
    POLegID BIGINT,      
    ScheduledTM DATE,      
    InboundDTTM DATETIME,      
    LandingDay NUMERIC(3,0),      
    LandingNight NUMERIC(3,0),      
    ApproachPrecision NUMERIC(2,0),      
    ApproachNonPrecision NUMERIC(2,0),      
    Instrument NUMERIC(7,3),      
    BlockHours NUMERIC(7,3),      
    FlightHours  NUMERIC(7,3),      
    DutyHours NUMERIC(7,3),      
    DutyEnd CHAR(5),      
    CrewID BIGINT,      
    TakeOffDay NUMERIC(2,0),      
    TakeOffNight NUMERIC(2,0),      
    RType CHAR(5),      
    Actualduty NUMERIC(7,3),      
    CrewName VARCHAR(100),
    CrewCD varchar(10),
    IsInActive Bit)      

DECLARE @TEMP table(CrewID BIGINT,  
     CrewCD VARCHAR(10),    
     DayLanding NUMERIC(3,0),      
     NightLanding NUMERIC(3,0),      
     Approach NUMERIC(3,0),      
     Instrument NUMERIC(3,0),      
     Day7 NUMERIC(3,0),       
     RestDays NUMERIC(3,0),      
     Day90 NUMERIC(3,0),      
     Month6 NUMERIC(3,0),      
     Month12 NUMERIC(3,0),      
     Day365 NUMERIC(3,0),      
     TakeoffDay NUMERIC(3,0),      
     TakeoffNight NUMERIC(3,0),      
     Day30 NUMERIC(3,0))     

INSERT INTO @TEMP(CrewID,CrewCD,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30)    
	SELECT crew.CrewID,crew.CrewCD,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30   
    FROM Company,Crew       
    WHERE Company.HomebaseAirportID = Crew.HomebaseID AND Crew.CrewID In(Select Distinct CrewID From @TempCrew)  AND Crew.IsStatus <> 0  AND Crew.IsDeleted = 0 AND Company.CustomerID = Crew.CustomerID 
                                   
SET @ldQtrBeg=(SELECT CONVERT(DATE,Dateadd(qq, Datediff(qq,0,@AsOf), 0)))      
SET @ldPQtrBeg=(SELECT CONVERT(DATE,DATEADD(qq,DATEDIFF(qq,0,@AsOf)-1,0)))      
      
      
INSERT @TempPO(
	AircraftID,
	AircraftCD, 
	FleetID,      
    POLogID,      
    POLegID,      
    ScheduledTM,      
    InboundDTTM,      
    LandingDay,      
    LandingNight,      
    ApproachPrecision,      
    ApproachNonPrecision,      
    Instrument,      
    BlockHours,      
    FlightHours,      
    DutyHours,      
    DutyEnd,      
    CrewID,      
    TakeOffDay,      
    TakeOffNight,      
    RType,      
    Actualduty,      
    CrewName,
    CrewCD,
    IsInActive)       
(SELECT A.AircraftID,A.AircraftCD,PM.FleetID,PL.POLogID,PL.POLegID,CONVERT(DATE,PL.ScheduledTM) ScheduledTM,PL.InboundDTTM,PC.LandingDay,      
       PC.LandingNight,PC.ApproachPrecision,PC.ApproachNonPrecision,PC.Instrument,ROUND(PC.BlockHours,1) BlockHours,      
       ROUND(PC.FlightHours,1) FlightHours,ROUND(PC.DutyHours,1) DutyHours,PC.DutyEnd,PC.CrewID,PC.TakeOffDay,PC.TakeOffNight,'FL' AS RType,      
       CONVERT(INT,'')  AS Actualduty,IsNull(C.FirstName,'')+' '+ IsNull(C.MiddleInitial,'')+' '+ IsNull(C.LastName,'') AS CrewName, C.CrewCD,IsNull(F.IsInActive,0)       
      FROM  PostflightMain PM 
      INNER JOIN PostflightCrew PC ON PC.POLogID=PM.POLogID
      INNER JOIN PostflightLeg PL ON PC.POLegID=PL.POLegID AND PL.IsDeleted=0 
      INNER JOIN Fleet F ON F.FleetID=PM.FleetID
      LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID
      INNER JOIN Crew C ON PC.CrewID=C.CrewID       
  WHERE CONVERT(DATE,PL.ScheduledTM) BETWEEN @AsOf-400 AND @AsOf
    AND PM.IsDeleted=0
    AND C.CrewID IN (Select Distinct CrewID From @TempCrew) 
UNION        
      
 SELECT A.AircraftID,A.AircraftCD,0 FleetID,0 LogID,0 POLegID,PFSL.SessionDT AS SCHEDTTM,''InboundDTTM,      
          PFSL.LandingDay,PFSL.LandingNight,PFSL.ApproachPrecision,PFSL.ApproachNonPrecision,      
          PFSL.Instrument,ROUND(PFSL.FlightHours,1) AS BLK_HRS,ROUND(PFSL.FlightHours,1) FlightHours,NULL DutyHours,''DutyEnd,      
          PFSL.CrewID,PFSL.TakeOffDay,PFSL.TakeOffNight,'SL' AS RType,CONVERT(INT,'')  AS Actualduty,      
          IsNull(C.FirstName,'')+' '+ IsNull(C.MiddleInitial,'')+' '+ IsNull(C.LastName,'') AS CrewName ,C.CrewCD,1      
    FROM PostflightSimulatorLog PFSL INNER JOIN Crew C ON PFSL.CrewID=C.CrewID
         LEFT OUTER JOIN Aircraft A ON PFSL.AircraftID=A.AircraftID     
   WHERE  PFSL.SessionDT BETWEEN @AsOf-400 AND @AsOf
     AND C.CrewID In (Select Distinct CrewID From @TempCrew) AND PFSL.IsDeleted = 0)   
 
 
SET @lnQtrSec=(SELECT DATEDIFF(hh, @ldQtrBeg,@AsOf) as Hours_Difference)
   
SET @lnQtrHrs= (SELECT DATEDIFF(day, @ldQtrBeg, @AsOf) * 24)
    
    create table #tempmain(
    CrewID bigint,
    CrewCD varchar(50),
    CrewName varchar(100),
    AircraftCD varchar(50),
    IsInActive BIT,
    LandingDay varchar(50),
    L_N90 varchar(50),
    Appr90 varchar(50),
    Instr90 numeric(17,9),
    days7 numeric(17,9),
    dayrest7 numeric(17,9),
    CalMon numeric(17,9),
    days90 numeric(17,9),
    CalQtrRest numeric(17,9),
    CalQtr numeric(17,9),
    Mon6 numeric(17,9),
    Prev2Qtrs numeric(17,9),
    Mon12 numeric(17,9),
    Calyr numeric(17,9),
    Days365 numeric(17,9),
    T_D90 varchar(50),
    T_N90 varchar(50),
    days30 numeric(17,9),
    TenToMin varchar(50),
    d_l numeric(17,9),              
	n_l numeric(17,9),               
	appr numeric(17,9),               
	instr numeric(17,9),                 
	day7 numeric(17,9),             
	dayrst numeric(17,9),                
	day90 numeric(17,9),            
	Month6 numeric(17,9),              
	Month12 numeric(17,9),                
	day365 numeric(17,9),               
	to_d numeric(17,9),                 
	to_n numeric(17,9),            
	day30 numeric(17,9),           
	d_l_min numeric(17,9),               
	n_l_min numeric(17,9),              
	to_dmin numeric(17,9),               
	to_nmin numeric(17,9),             
	apprmin numeric(17,9),              
	instrmin numeric(17,9),              
	day7max numeric(17,9),              
	dayrstmin numeric(17,9),               
	calmonmax numeric(17,9),               
	day90max numeric(17,9),         
	calqtmax numeric(17,9),              
	calqrsmin numeric(17,9),             
	mon6max numeric(17,9),           
	prev2qmax numeric(17,9),               
	mon12max numeric(17,9),              
	calyrmax numeric(17,9),               
	day365max numeric(17,9),             
	day30max numeric(17,9),
	homebase char(4),
	FirstName varchar(50)
	,MiddleInitial varchar(50)
	,LastName varchar(50))
                                                                 
IF @AircraftBasis=1       
 BEGIN    
  
INSERT INTO #tempmain
 SELECT Distinct A.CrewID,A.CrewCD, A.CrewName, A.AircraftCD,A.IsInActive,SUM(A.L_D90) LandingDay,SUM(A.L_N90)L_N90,SUM(A.Appr90)Appr90,SUM(A.Instr90)Instr90,SUM(A.days7)days7,MAX(A.dayrest7) dayrest7,SUM(A.CalMon)CalMon,SUM(A.days90) days90,MAX(A.CalQtrRest) CalQtrRest,SUM(A.CalQtr)CalQtr,SUM(A.Mon6)Mon6,SUM(A.Prev2Qtrs)Prev2Qtrs,SUM(A.Mon12)Mon12,SUM(A.Calyr)Calyr,SUM(A.Days365)Days365,SUM(A.T_D90)T_D90,SUM(A.T_N90)T_N90,SUM(A.days30) days30,@TimeDisplayTenMin TenToMin,
  NULL d_l ,              
	NULL n_l,               
	NULL appr,               
	NULL instr ,                 
	NULL day7,             
	NULL dayrst,                
	NULL day90 ,            
	NULL Month6,              
	NULL Month12,                
	NULL day365,               
	NULL to_d ,                 
	NULL to_n,            
	NULL day30,           
	NULL d_l_min,               
	NULL n_l_min,              
	NULL to_dmin ,               
	NULL to_nmin,             
	NULL apprmin,              
	NULL instrmin,              
	NULL day7max ,              
	NULL dayrstmin ,               
	NULL calmonmax,               
	NULL day90max,         
	NULL calqtmax ,              
	NULL calqrsmin,             
	NULL mon6max ,           
	NULL prev2qmax ,               
	NULL mon12max ,              
	NULL calyrmax ,               
	NULL day365max ,             
	NULL day30max,
	null homebase,
	null FirstName,null MiddleInitial,null LastName
	 FROM
	(SELECT  T1.CrewID,T1.CrewCD,T1.CrewName ,T1.AircraftCD,T1.IsInActive, T1.RType,     
	   (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.DayLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.NightLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingNight),0)ELSE '0.0' END) L_N90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Approach,0)) AND @AsOf THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Instrument,0)) AND @AsOf THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Day7,0)) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) days7,      

		CASE WHEN (T1.RType = 'FL') THEN			
			ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (@AsOf-ISNULL(T.RestDays,0)) AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)      
		ELSE
			ISNULL((T.RestDays*24),0)
		END dayrest7,      
  
	   (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalMon,      
	   (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(Day90,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days90,      
	
		CASE WHEN (T1.RType = 'FL') THEN 			
			ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)  
		ELSE
			ISNULL(@lnQtrHrs,0)
		END CalQtrRest,  

       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalQtr,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month6*30),0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon6,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Prev2Qtrs,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month12*30),0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon12,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Calyr,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day365,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) Days365,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffDay,0) AND @AsOf THEN ISNULL(SUM(T1.TakeOffDay),0) ELSE '0.0' END) T_D90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffNight,0) AND @AsOf THEN ISNULL(SUM(T1.TakeoffNight),0) ELSE '0.0' END) T_N90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day30,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days30,      
       @TimeDisplayTenMin TenToMin 
    
      
   FROM  Crew C
       INNER JOIN @TEMP T ON C.CrewID = T.CrewID
       INNER JOIN  @TempPO T1 ON C.CrewID = T1.CrewID 
       LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE TP.CrewID IN (Select Distinct CrewID From @TempCrew) AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID
	   
   WHERE 
	 C.CrewID In (Select Distinct CrewID From @TempCrew)
     And (AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')  
   And (T1.IsInActive NOT IN (@IsInActive) OR @IsInActive=0)
  -- AND T1.IsInActive=0
   
   GROUP BY T1.CrewID,T1.CrewCD,T1.CrewName,T1.AircraftCD,T1.IsInActive, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM)A   
   GROUP BY A.CrewID,A.CrewCD,A.CrewName,A.AircraftCD,A.IsInActive 
   Order By CrewCD,AircraftCD  
  END       
ELSE      
  BEGIN  
INSERT INTO #tempmain
 SELECT Distinct A.CrewID,A.CrewCD,
        A.CrewName,
        A.AircraftCD,
        A.IsInActive,
        SUM(A.L_D90) curr2,
        SUM(A.L_N90)curr3,
        SUM(A.Appr90)Appr90,
        SUM(A.Instr90)Instr90,
        SUM(A.days7)days7,
        MAX(A.dayrest7) dayrest7,
        SUM(A.CalMon)CalMon,
        SUM(A.days90) days90,
        MAX(A.CalQtrRest) CalQtrRest,
        SUM(A.CalQtr)CalQtr,
        SUM(A.Mon6)Mon6,
        SUM(A.Prev2Qtrs)Prev2Qtrs,
        SUM(A.Mon12)Mon12,
        SUM(A.Calyr)Calyr,
        SUM(A.Days365)Days365,
        SUM(A.T_D90)T_D90,
        SUM(A.T_N90)T_N90,
        SUM(A.days30) days30,
        @TimeDisplayTenMin TenToMin ,
          NULL d_l ,              
	NULL n_l,               
	NULL appr,               
	NULL instr ,                 
	NULL day7,             
	NULL dayrst,                
	NULL day90 ,            
	NULL Month6,              
	NULL Month12,                
	NULL day365,               
	NULL to_d ,                 
	NULL to_n,            
	NULL day30,           
	NULL d_l_min,               
	NULL n_l_min,              
	NULL to_dmin ,               
	NULL to_nmin,             
	NULL apprmin,              
	NULL instrmin,              
	NULL day7max ,              
	NULL dayrstmin ,               
	NULL calmonmax,               
	NULL day90max,         
	NULL calqtmax ,              
	NULL calqrsmin,             
	NULL mon6max ,           
	NULL prev2qmax ,               
	NULL mon12max ,              
	NULL calyrmax ,               
	NULL day365max ,             
	NULL day30max,
	null homebase,
	null FirstName,null MiddleInitial,null LastName
     FROM   
	(SELECT T1.CrewID,T1.CrewCD,T1.CrewName , T1.AircraftCD,T1.IsInActive,T1.RType,
	   (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.DayLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.NightLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingNight),0) ELSE '0.0' END) L_N90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Approach,0)) AND @AsOf THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Instrument,0)) AND @AsOf THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Day7,0)) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0) ELSE '0.0' END) days7,      

		CASE WHEN (T1.RType = 'FL') THEN 			
			ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (@AsOf-ISNULL(T.RestDays,0)) AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0)ELSE 0.0 END),0)
       ELSE
			ISNULL((T.RestDays*24),0)
       END dayrest7,      

       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalMon,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(Day90,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days90,    

		CASE WHEN (T1.RType = 'FL') THEN    
			 ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0)ELSE '0.0' END),0)      			
		ELSE
			ISNULL((@lnQtrHrs),0)
		END CalQtrRest,      

       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalQtr,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month6*30),0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon6,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Prev2Qtrs,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month12*30),0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon12,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Calyr,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day365,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Days365,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffDay,0) AND @AsOf THEN ISNULL(SUM(T1.TakeOffDay),0)ELSE '0.0' END) T_D90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffNight,0) AND @AsOf THEN ISNULL(SUM(T1.TakeoffNight),0)ELSE '0.0' END) T_N90,      
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day30,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days30,      
       @TimeDisplayTenMin TenToMin 
   FROM   Crew C   
       --left outer join Company cp on cp.homebaseairportid=c.HomebaseID  
		INNER JOIN @TEMP T ON C.CrewID = T.CrewID
		INNER JOIN @TempPO T1 ON C.CrewID = T1.CrewID
		  
		LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE TP.CrewID  IN(Select Distinct CrewID From @TempCrew) AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID   
   WHERE 
	 C.CrewID In (Select Distinct CrewID From @TempCrew)
	 And (AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')  
     And (T1.IsInActive NOT IN (@IsInActive) OR @IsInActive=0)
    -- AND T1.IsInActive=0
   GROUP BY T1.CrewID,T1.CrewCD,T1.CrewName,T1.AircraftCD,T1.IsInActive, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM) A
   GROUP BY A.CrewID,A.CrewCD,A.CrewName,A.AircraftCD,A.IsInActive
   Order By  CrewCD, AircraftCD  
   END      
   
UPDATE #TEMPMAIN SET 
					   d_l=TEMP.DayLanding,              
					   n_l =TEMP.NightLanding,                
					   appr=TEMP.Approach,                
					   instr=TEMP.Instrument,                  
					   day7=TEMP.Day7,                
					   dayrst=TEMP.RestDays,                
					   day90=TEMP.Day90,                
					   Month6=TEMP.Month6,                
					   Month12=TEMP.Month12,                
					   day365=TEMP.Day365,                
					   to_d=TEMP.TakeoffDay,                 
					   to_n=TEMP.TakeoffNight,                
					   day30=TEMP.Day30,                
					   d_l_min=TEMP.DayLandingMinimum,                
					   n_l_min=TEMP.NightllandingMinimum,                
					   to_dmin=TEMP.TakeoffDayMinimum ,                
					   to_nmin=TEMP.TakeofNightMinimum,                
					   apprmin=TEMP.ApproachMinimum,                
					   instrmin=TEMP.InstrumentMinimum,                
					   day7max=TEMP.DayMaximum7,                
					   dayrstmin=TEMP.RestDaysMinimum,                
					   calmonmax =TEMP.CalendarMonthMaximum,                
					   day90max =TEMP.DayMaximum90,                
					   calqtmax=TEMP.CalendarQTRMaximum,                
					   calqrsmin=TEMP.RestCalendarQTRMinimum,                
					   mon6max=TEMP.MonthMaximum6,                
					   prev2qmax=TEMP.Previous2QTRMaximum,                
					   mon12max=TEMP.MonthMaximum12,                
					   calyrmax=TEMP.CalendarYearMaximum,                
					   day365max=TEMP.DayMaximum365,                
					   day30max =TEMP.DayMaximum30,
					   homebase=TEMP.IcaoID,
					   FirstName=TEMP.FirstName,
					   MiddleInitial=TEMP.MiddleInitial,
					   LastName=TEMP.LastName
					    FROM 
(SELECT DISTINCT CW.CrewID,C.DayLanding,C.NightLanding,C.Approach,C.Instrument,C.Day7,C.RestDays,C.Day90,C.Month6,C.Month12,C.Day365,
C.TakeoffDay,C.TakeoffNight,C.Day30,C.DayLandingMinimum,C.NightllandingMinimum,C.TakeoffDayMinimum,C.TakeofNightMinimum,C.ApproachMinimum,C.InstrumentMinimum,C.DayMaximum7,C.RestDaysMinimum,C.CalendarMonthMaximum,
C.DayMaximum90,C.CalendarQTRMaximum,C.RestCalendarQTRMinimum,C.MonthMaximum6,C.Previous2QTRMaximum,C.MonthMaximum12,C.CalendarYearMaximum,C.DayMaximum365,C.DayMaximum30,Airport.IcaoID,
Cw.FirstName,Cw.MiddleInitial,Cw.LastName
 FROM Company C 
 INNER JOIN Crew CW ON C.HomebaseAirportID=CW.HomebaseID
 LEFT JOIN  Airport ON Airport.AirportID=C.HomebaseAirportID
 INNER JOIN @TempCrew TC ON TC.CrewID=CW.CrewID
 WHERE C.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))TEMP
 
 WHERE #TEMPMAIN.CREWID=TEMP.CrewID 
 --and  #TEMPMAIN.AircraftCD=TEMP.AircraftCD
 
  SELECT * FROM  #TEMPMAIN  
      IF OBJECT_ID('tempdb..#TEMPMAIN') is not null          
      DROP table #TEMPMAIN                    
 
  --DROP TABLE #TEMPMAIN
   
END      
--spGetReportPOSTCrewCurrency2ExportInformation 'supervisor_99','2012/01/01', ''         

GO


