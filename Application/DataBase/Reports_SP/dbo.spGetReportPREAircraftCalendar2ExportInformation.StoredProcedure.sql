IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar2ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar2ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spGetReportPREAircraftCalendar2ExportInformation]
	    @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(500) = ''  -- [Optional], Comma delimited string with mutiple values
		,@IsHomeBase AS BIT = 0
		,@IsAvailable AS BIT = 0
		,@FlightCatagoryCD AS NVARCHAR(1000) = ''
AS
-- EXEC spGetReportPREAircraftCalendar2ExportInformation 'supervisor_99', '2012-07-20', '2012-10-20', '', '', ''
-- ===============================================================================
-- SPC Name: AircraftCalendar2ExportInformation]
-- Author: ASKAR
-- Create date: 04 Jul 2012
-- Description: Get Aircraft Calendar information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================



BEGIN

SET NOCOUNT ON
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
		CREATE TABLE #TempAircraftCal (
		tail_nmbr VARCHAR(10),activedate DATE, locdep DATETIME,requestor VARCHAR(60),dept_code VARCHAR(8),cat_code CHAR(4),
		duty_type CHAR(2),elp_time NUMERIC(7,3),timeapprox BIT,pax_total INT,ron VARCHAR(6),pic_code VARCHAR(250),sic_code VARCHAR(250),
		orig_nmbr BIGINT,leg_num BIGINT,legid BIGINT,type_code VARCHAR(10),vendcode VARCHAR(5),[desc] VARCHAR(40),rec_type CHAR(1),
		grndtime VARCHAR(6),TrueRon VARCHAR(6),dayfill VARCHAR(6),nextloc DATETIME,ac_code CHAR(6),auth_code VARCHAR(8),resv_avail INT,purpose VARCHAR(40),
		[private] BIT,ordernum VARCHAR(10),aircraft_code VARCHAR(15),maxpax NUMERIC(3,0),dep_time DATETIME,arr_time DATETIME,
		depicao_id CHAR(4),arricao_id CHAR(4),dep_city VARCHAR(25),arr_city VARCHAR(25),dep_airport VARCHAR(25),arr_airport VARCHAR(25),dep_state VARCHAR(25),arr_state VARCHAR(25),
		max_pax INT,Notes VARCHAR(MAX),AddlCrew VARCHAR(300))
	INSERT INTO #TempAircraftCal 
			SELECT
			[tail_nmbr]=F.TailNUM,
			[activedate] = PL.DepartureDTTMLocal,
			[locdep]=PL.DepartureDTTMLocal,
			[requestor]=PL.RequestorName,
			[dept_code]=D.DepartmentCD,
			[cat_code] = FC.FlightCatagoryCD,
			[duty_type]=PL.DutyType,
			[elp_time]=CONVERT(VARCHAR, FLOOR(ISNULL(PL.ElapseTM,0)*10)*0.1),
			[timeapprox]=ISNULL(PL.IsApproxTM,0),
			[pax_total]=ISNULL(PL.PassengerTotal,0),
			[ron]='FALSE',---(CASE WHEN PL.DepartureDTTMLocal < PL.ArrivalDTTMLocal THEN 'FALSE' END),
			[pic_code]=SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),  --TO VERIFY
			[sic_code]=SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1), --TO VERIFY
			[orig_nmbr]= ISNULL(PM.TripNUM,0),
			[leg_num]= ISNULL(PL.LegNUM,0),
			[legid]= ISNULL(PL.LegID,0),
			[type_code]=AT.AircraftCD,
			[vendcode]=V.VendorCD,
			[desc]=PM.TripDescription,
			[rec_type]=PM.RecordType,
			[grndtime]='FALSE',
			[TrueRon]='FALSE', -- to be changed
			[dayfill]='FALSE',
			[nextloc]=PL.NextLocalDTTM,
			[ac_code]=F.AircraftCD,
			[auth_code]=DA.AuthorizationCD,
			[resv_avail]= ISNULL(PL.ReservationAvailable,0),
			[purpose]=PL.FlightPurpose,
			[private]=PL.IsPrivate,
			[ordernum] = '99999',
			[aircraft_code]=F.TypeDescription , --TO VERIFY
			[maxpax]=ISNULL(F.MaximumPassenger,0),
			[dep_time]=PL.DepartureDTTMLocal,
			[arr_time]=PL.ArrivalDTTMLocal,
			[depicao_id]=AD.IcaoID,
			[arricao_id]=AA.IcaoID,			
			[dep_city]=AD.CityName,
			[arr_city]=AA.CityName,
			[dep_airport]=AD.AirportName,
			[arr_airport]=AA.AirportName,
			[dep_state]=AD.StateName,
			[arr_state]=AA.StateName,
			[max_pax]=ISNULL(0,0),
			[Notes]=PL.Notes,
			[AddlCrew]=SUBSTRING(AddlCrew.CrewCodes,1,LEN(AddlCrew.CrewCodes)-1)
			FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,F.AircraftID,F.TypeDescription,
					F.MaximumPassenger,VendorID
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
					OR @FleetGroupCD = ''
                ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
            INNER JOIN (SELECT AirportID, IcaoID,CityName,AirportName,StateName FROM Airport) AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN (SELECT AirportID, IcaoID,CityName,AirportName,StateName FROM Airport) AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN( SELECT AircraftID,AircraftCD FROM Aircraft) AT ON AT.AircraftID=F.AircraftID
			LEFT OUTER JOIN(SELECT VendorID,VendorCD FROM Vendor ) V ON  V.VendorID=F.VendorID
			LEFT OUTER JOIN(SELECT DepartmentID,DepartmentCD FROM Department)D ON PL.DepartmentID=D.DepartmentID
			LEFT OUTER JOIN(SELECT AuthorizationCD,AuthorizationID FROM DepartmentAuthorization)DA ON DA.AuthorizationID=PL.AuthorizationID 
			LEFT OUTER JOIN (SELECT FlightCategoryID,FlightCatagoryCD FROM FlightCatagory) FC 
				ON PL.FlightCategoryID = FC.FlightCategoryID
		     LEFT OUTER JOIN (
				SELECT DISTINCT PC2.LegID, CrewCodes = (
				SELECT ISNULL(RTRIM(C.CrewCD) + ',','') 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
				FOR XML PATH('')
				 )
				FROM PreflightCrewList PC2		
			) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT ISNULL(RTRIM(C1.CrewCD) + ',','')
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT ISNULL(RTRIM(C1.CrewCD) + ',','')
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE NOT IN('P','S')
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) AddlCrew ON PL.LegID = AddlCrew.LegID
		WHERE PM.CustomerID =  CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
		    AND (PM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))	OR @IsHomeBase=0)
	        AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
		    AND (CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO))
		    AND PM.Tripstatus IN ('T','H')	
		    AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD='')
		    AND PM.TripNUM IN (SELECT PM.TripNUM FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
																		  WHERE PM.CustomerID= CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
																		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
																			AND PL.ReservationAvailable > 0
																			AND @IsAvailable=1
								UNION ALL
								
								SELECT PM.TripNUM FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
																		  WHERE PM.CustomerID= CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
																		    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
																			AND @IsAvailable=0
																			)
				
--SELECT * FROM #TempAircraftCal 
	
	DECLARE	 @TEMPTODAY AS DATETIME = CONVERT(DATE, @DATEFROM)
	
	WHILE (@TEMPTODAY <=  CONVERT(DATE, @DATETO))
		BEGIN
			--INSERT INTO #TempAircraftCal (activedate) select (@TEMPTODAY)
			--WHERE NOT EXISTS(SELECT DISTINCT activedate FROM #TempAircraftCal WHERE activedate = @TEMPTODAY)		
		
			IF NOT EXISTS (SELECT DISTINCT activedate FROM #TempAircraftCal WHERE activedate = @TEMPTODAY)
			BEGIN
				INSERT INTO #TempAircraftCal (activedate) VALUES (@TEMPTODAY)
			END 
			SET @TEMPTODAY = @TEMPTODAY + 1
		END
-----------------------------------------------
SELECT DISTINCT [tail_nmbr],[activedate],[locdep],[requestor],[dept_code],[cat_code],[duty_type],[elp_time]=ISNULL([elp_time],0),[timeapprox]=ISNULL(timeapprox,0),
	   [pax_total]=ISNULL(pax_total,0),[ron]='FALSE',[pic_code],[sic_code],[orig_nmbr]= ISNULL([orig_nmbr],0),[leg_num]= ISNULL([leg_num],0),[legid]= ISNULL([legid],0),
	   [type_code],[vendcode],[desc],[rec_type],[grndtime]='FALSE',[TrueRon]='FALSE',[dayfill]='FALSE',[nextloc],[ac_code],[auth_code],[resv_avail]= ISNULL([resv_avail],0),
	   [purpose],ISNULL(private,0)[private],[ordernum] = '99999',[aircraft_code]=[AC_CODE],[maxpax]=ISNULL([maxpax],0),[dep_time],[arr_time],[depicao_id],[arricao_id],[dep_city],[arr_city],[dep_airport],
	   [arr_airport],[dep_state],[arr_state],[max_pax]=ISNULL(0,0),Notes,AddlCrew,[locarr]=[arr_time] 
	  FROM #TempAircraftCal ORDER BY activedate,locdep,ac_code

IF OBJECT_ID('tempdb..#TempAircraftCal') IS NOT NULL
drop table #TempAircraftCal

END







GO


