IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDomesticInternationalSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDomesticInternationalSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



    
    
CREATE   PROC [dbo].[spGetReportPOSTDomesticInternationalSummaryExportInformation]        
(
 @UserCD AS VARCHAR(30),
 @YEAR AS INT,       
 @TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
 @FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
 @IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase 
 @PrintFlightHours as varchar(1)
 
 )
AS      
  
-- ===============================================================================  
-- SPC Name: spGetReportPOSTDomesticInternationalSummaryExportInformation  
-- Author: Avanikanta Pradhan  
-- Create date: 13 aug 2012s
-- Description: Get Domestic and International summary REPORTS  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
BEGIN

DECLARE @StartDate DATE     
SET @StartDate= CAST(RTRIM(Convert(varchar(10),@YEAR)) AS DATETIME) 

SELECT DISTINCT F.TailNum,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN DATEPART(month,@StartDate)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))AND POL.DutyTYPE=1),0))
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN DATEPART(month,@StartDate)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))AND POL.DutyTYPE=1),0))
END  
dflthrs1,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+1)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+1 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+1)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+1 AND POL.DutyTYPE=1),0)) 
END
dflthrs2,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+2)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+2 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+2)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+2 AND POL.DutyTYPE=1),0)) 
END
dflthrs3,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+3)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+3 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+3)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+3 AND POL.DutyTYPE=1),0)) 
END
dflthrs4,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+4)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+4 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+4)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+4 AND POL.DutyTYPE=1),0)) 
END
dflthrs5,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+5)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+5 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+5)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+5 AND POL.DutyTYPE=1),0)) 
END
dflthrs6,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+6)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+6 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+6)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+6 AND POL.DutyTYPE=1),0)) 
END
dflthrs7,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+7)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+7 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+7)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+7 AND POL.DutyTYPE=1),0)) 
END
dflthrs8,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+8)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+8 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+8)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+8 AND POL.DutyTYPE=1),0)) 
END
dflthrs9,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+9)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+9 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+9)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+9 AND POL.DutyTYPE=1),0)) 
END
dflthrs10,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+10) THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+10 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+10) THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+10 AND POL.DutyTYPE=1),0)) 
END
dflthrs11,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+11) THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+11 AND POL.DutyTYPE=1),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+11) THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+11 AND POL.DutyTYPE=1),0)) 
END
dflthrs12,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN  DATEPART(month,@StartDate)     THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate)) AND POL.DutyTYPE=2),0))  
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN  DATEPART(month,@StartDate)     THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate)) AND POL.DutyTYPE=2),0))  
END
iflthrs1,

CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+1)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+1 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+1)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+1 AND POL.DutyTYPE=2),0)) 
END
iflthrs2,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+2)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+2 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+2)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+2 AND POL.DutyTYPE=2),0)) 
END
iflthrs3,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+3)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+3 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+3)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+3 AND POL.DutyTYPE=2),0)) 
END
iflthrs4,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+4)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+4 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+4)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+4 AND POL.DutyTYPE=2),0)) 
END
iflthrs5,



CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+5)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+5 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+5)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+5 AND POL.DutyTYPE=2),0)) 
END
iflthrs6,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+6)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+6 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+6)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+6 AND POL.DutyTYPE=2),0)) 
END
iflthrs7,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+7)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+7 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+7)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+7 AND POL.DutyTYPE=2),0)) 
END
iflthrs8,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+8)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+8 AND POL.DutyTYPE=2),0))
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+8)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+8 AND POL.DutyTYPE=2),0))
END
iflthrs9,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+9)  THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+9 AND POL.DutyTYPE=2),0))
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+9)  THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+9 AND POL.DutyTYPE=2),0))
END
iflthrs10,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+10) THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+10 AND POL.DutyTYPE=2),0)) 
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+10) THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+10 AND POL.DutyTYPE=2),0)) 
END
iflthrs11,


CASE WHEN @PrintFlightHours='a' THEN
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+11) THEN (SUM(ROUND(POL.FlightHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+11 AND POL.DutyTYPE=2),0))
ELSE
(ISNULL((SELECT CASE (DATEPART(month,POL.ScheduledTM)) WHEN (DATEPART(month,@StartDate)+11) THEN (SUM(ROUND(POL.BlockHours,1))) END  WHERE DATEPART(month,POL.ScheduledTM)= (DATEPART(month,@StartDate))+11 AND POL.DutyTYPE=2),0))
END
iflthrs12  


                          
FROM PostflightLeg POL   

        left JOIN PostflightMain POM on POM.POLogID = POL.POLogID AND POM.IsDeleted=0
       LEFT OUTER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger
		FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID WHERE F.IsDeleted = 0
			AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
		
		WHERE   POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	     AND (DATEPART(year,POL.ScheduledTM)between DATEPART(year,@StartDate)AND DATEPART(year,@StartDate)+11)	
	     AND POL.IsDeleted=0
		 AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') 
	     AND (POM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
	
	    GROUP BY F.TailNum, POL.ScheduledTM, POL.DutyTYPE, pol.POLegID


	    END
	    --EXEC spGetReportPOSTDomesticInternationalSummaryExportInformation 'eliza_9',2010,'','',1,'a'




GO


