IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPAXAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPAXAdditionalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSPAXAdditionalInformation]
	@UserCD VARCHAR(50)--Mandatory
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum AS VARCHAR(50) = ''
	,@PassengerCD AS VARCHAR(300) = ''
	
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSPAXNoFlyInformation
-- Author: AISHWARYA.M
-- Create date: 30 Aug 2012
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- =================================================================================
SET NOCOUNT ON
	SELECT --[LegID] = CONVERT(VARCHAR,PL.LegID)
			 PassengerName = ISNULL(P.LastName,'') + ', ' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
			,PaxCode = CONVERT(VARCHAR(MAX),ISNULL(P.PassengerRequestorCD,''))
			,Code = CONVERT(VARCHAR(MAX),ISNULL(PA.AdditionalINFOCD,''))
			,PDescription = CONVERT(VARCHAR(MAX),ISNULL(PA.AdditionalINFODescription,''))
			--,AddlInfo = dbo.FlightPakDecrypt(ISNULL(PA.AdditionalINFOValue,''))
			,AddlInfo = CONVERT(VARCHAR(MAX),ISNULL(PA.AdditionalINFOValue,''))
		   FROM (
				SELECT DISTINCT PPL.PassengerID--, PL.LegID, PL.LegNUM 
				FROM PreflightMain PM
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
				INNER JOIN FlightPurpose FP ON PPL.FlightPurposeID=FP.FlightPurposeID
				WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND PM.TripID = CONVERT(BIGINT,@TripID)
				AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			) PAX
			INNER JOIN 
			(
				SELECT PassengerRequestorID, PassengerRequestorCD, LastName, FirstName, MiddleInitial, CustomerID
				FROM Passenger
			) P ON PAX.PassengerID = P.PassengerRequestorID
			INNER JOIN PassengerAdditionalInfo PA ON PA.PassengerRequestorID = PAX.PassengerID AND P.CustomerID = PA.CustomerID
			INNER JOIN PassengerInformation PINFO ON PA.CustomerID = PINFO.CustomerID AND PA.AdditionalINFOCD = PINFO.PassengerInfoCD
			WHERE
			PA.IsDeleted = 0
			AND PA.AdditionalINFOValue IS NOT NULL
			AND PA.AdditionalINFOValue <> ''
			AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			AND PINFO.IsShowOnTrip = 1
			
END

--EXEC spGetReportPRETSPAXAdditionalInformation 'SUPERVISOR_99', '10099107564','1',''






GO


