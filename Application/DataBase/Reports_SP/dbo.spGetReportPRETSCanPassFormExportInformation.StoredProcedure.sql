IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCanPassFormExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCanPassFormExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSCanPassFormExportInformation]
	  @UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --  PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
    --,@LegID AS VARCHAR(30)
AS
BEGIN

	-- =========================================================================
	-- SPC Name: spGetReportPRETSCanPassFormExportInformation
	-- Author: Askar
	-- Create date: 28 Sep 2012
	-- Description: Get Preflight CanPass Report Export information for REPORTS
	-- Revision History
	-- Date		Name		Ver		Change
	-- 
	-- ==========================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
		TripID BIGINT
		, LegID BIGINT
		, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	----------------Crew Information-------------
	
	SELECT DISTINCT C.CompanyName name,
	                C.HomebasePhoneNUM homephone,
	                C.BaseFax basefax,
	                PM.EstDepartureDT estdepdt,
	                F.TailNum tail_nmbr,
	                F.AircraftCD type_code,
	                F.TypeDescription type_desc,
	                FP.ManufactureYear year,
	                PL.LegID legid,
	                A.IcaoID arricao_id,
	                D.IcaoID depicao_id,
	               -- A.CountryName 
	                'CA' country,
	                PL.ArrivalDTTMLocal arrdate,
	                PL.ArrivalDTTMLocal arrtime,
	                PL.DepartureDTTMLocal depdate,
	                PL.DepartureDTTMLocal deptime,
	                [desc] = FBA.FBOVendor,
	                D.StateName state,
	                FP.OwnerLesse owner_less,
	                CR.CrewCD crewcode,
	                '' paxcode,
	                '' paxname,
	                PCL.CrewLastName last_name,
	                PCL.CrewFirstName first_name,
	                PCL.CrewMiddleName middleinit,
	                CONVERT(DATE,CR.BirthDT) dob,
	                CR.Gender gender,
	                CCC.CountryName nationcode,
	                CCR.CountryName ctryresid,
	                ISNULL(Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER),1) staydays,
	                '' fltpur,
	                'CREW MEMBER' fltpurpose,
	                '' declaratn,
				    C.CanPassNum canpass,
					'UNKONWN' llprint,
					'C' lctype 
	          FROM @tblTripInfo T
				   INNER JOIN PreflightMain PM ON T.TripID=PM.TripID
				   INNER JOIN PreflightLeg  PL ON T.LegID=PL.LegID
				   INNER JOIN Fleet F ON PM.FleetID=F.FleetID
				   INNER JOIN FleetPair FP ON F.FleetID=FP.FleetID
				   INNER JOIN Airport D ON PL.DepartICAOID=D.AirportID
				   INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
				   INNER JOIN Company C ON PM.HomebaseID = C.HomebaseID  
	               INNER JOIN Airport A1 ON C.HomebaseAirportID=A1.AirportID  
				   LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID AND PFL.IsArrivalFBO = 1
				   LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID
				   LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
				   LEFT OUTER JOIN Crew CR ON PCL.CrewID = CR.CrewID
				   LEFT OUTER JOIN Country CCC ON CR.Citizenship = CCC.CountryID
				   LEFT OUTER JOIN Country CCR ON CR.CountryID = CCR.CountryID
				   where (D.CountryName = 'CANADA' or A.CountryName = 'CANADA')
					AND  PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))


		UNION ALL
		----------------Passenger Information-------------
		SELECT DISTINCT C.CompanyName name,
						C.HomebasePhoneNUM homephone,
						C.BaseFax basefax,
						PM.EstDepartureDT estdepdt,
						F.TailNum tail_nmbr,
						F.AircraftCD type_code,
						F.TypeDescription type_desc,
						FP.ManufactureYear year,
						PL.LegID legid,
						A.IcaoID arricao_id,
						D.IcaoID depicao_id,
						'CA' country,
						PL.ArrivalDTTMLocal arrdate,
						PL.ArrivalDTTMLocal arrtime,
						PL.DepartureDTTMLocal depdate,
						PL.DepartureDTTMLocal deptime,
	                [desc] = FBA.FBOVendor,
						D.StateName state,
						FP.OwnerLesse owner_less,
						'' crewcode,
						PP.PassengerRequestorCD paxcode,
						PPL.PassengerLastName+','+PPL.PassengerFirstName+''+PPL.PassengerMiddleName paxname,
						PPL.PassengerLastName last_name,
						PPL.PassengerFirstName first_name,
						PPL.PassengerMiddleName middleinit,
						CONVERT(DATE,PP.DateOfBirth) dob,
						PP.Gender gender,
						CPC.CountryName nationcode,
						CPR.CountryName ctryresid,
						ISNULL(Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER),1) staydays,
						FPP.FlightPurposeCD fltpur,
						FPP.FlightPurposeDescription fltpurpose,
						'' declaratn,
						C.CanPassNum canpass,
						'UNKONWN' llprint,
						'P' lctype 
									
				 FROM   @tblTripInfo T
					    INNER JOIN PreflightMain PM ON T.TripID=PM.TripID
					    INNER JOIN PreflightLeg  PL ON T.LegID=PL.LegID
					    INNER JOIN Fleet F ON PM.FleetID=F.FleetID
					    INNER JOIN FleetPair FP ON F.FleetID=FP.FleetID
					    INNER JOIN Airport D ON PL.DepartICAOID=D.AirportID
					    INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
					    INNER JOIN Company C ON PM.HomebaseID = C.HomebaseID  
	                    INNER JOIN Airport A1 ON C.HomebaseAirportID=A1.AirportID  
					   LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID AND PFL.IsArrivalFBO = 1
					   LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID
					    LEFT OUTER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
						LEFT OUTER JOIN Passenger PP ON PPL.PassengerID = PP.PassengerRequestorID
						LEFT OUTER JOIN Country CPC ON PP.CountryID = CPC.CountryID
						LEFT OUTER JOIN Country CPR ON PP.CountryOfResidenceID = CPR.CountryID
						LEFT OUTER JOIN FlightPurpose FPP ON PP.FlightPurposeID = FPP.FlightPurposeID 
						where (D.CountryName = 'CANADA' or A.CountryName = 'CANADA')
						AND  PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			
		ORDER BY LegID, lctype

END
 --EXEC spGetReportPRETSCanPassFormExportInformation 'UC', '', '', '', '', '', '', '', '', '', '', '', ''
 --EXEC spGetReportPRETSCanPassFormExportInformation 'supervisor_99', '2021'






GO


