IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendarCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendarCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendarCrewInformation]
	@UserCD VARCHAR(50)--Mandatory
	,@DateFrom DATETIME --Mandatory
	,@DateTo DATETIME --Mandatory
	,@TailNum VARCHAR(500) = ''--OPTIONAL
	,@FleetGroupCD VARCHAR(500) = ''--OPTIONAL
AS
-- ===============================================================================
-- SPC Name: spGetReportPREWeeklyCalendarCrewInformation
-- Author: AISHWARYA.M
-- Create date: 28 Aug 2012
-- Description: Get Preflight Weekly CalendarII Legend information for  Report
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
----------------------------TailNum and Fleet Group Filteration----------------------
--DROP TABLE #TempFleetID
CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> '' AND @FleetGroupCD ='' 
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END



DECLARE @DayCount INT
DECLARE @WeekDayCount INT

SET @DayCount=(SELECT DATEPART(weekday,@DATEFROM-1)) 

SET @WeekDayCount=(SELECT DATEPART(weekday,@DATETO)) 



DECLARE @ToDate DATE

SET @DayCount=( 7-((DATEDIFF(DAY,@DateFrom,@DateTo)+1)%7)) 



SET @ToDate=(SELECT CASE  WHEN @DayCount=7 THEN @DateTo ELSE @DateTo+@DayCount END )  


SELECT DISTINCT TEMP.* FROM
(SELECT DISTINCT C.CrewCD, C.LastName, C.FirstName 
FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		INNER JOIN (
			SELECT LegID , CrewID FROM PreflightCrewList
		) PC ON PL.LegID = PC.LegID
		INNER JOIN ( 
			SELECT CrewID, CrewCD, LastName, FirstName,IsStatus,IsDeleted FROM Crew 
		) C ON PC.CrewID = C.CrewID 
		
		INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
   	INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
		WHERE PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		AND C.IsDeleted=0
		AND C.IsStatus=1
		AND PM.TripStatus IN ('T','H')
		AND CONVERT(DATE, PM.EstDepartureDT) BETWEEN CONVERT(DATE, @DATEFROM) AND CONVERT(DATE, @ToDate)
		AND PM.IsDeleted = 0
		)TEMP			
			
--EXEC spGetReportPREWeeklyCalendarCrewInformation 'JWILLIAMS_11', '2012-09-01', '2012-09-07'



GO


