IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostPassengerFlightHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostPassengerFlightHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ===============================================================================
-- SPC Name: spGetReportPostPassengerFlightHistoryExportInformation
-- Author:Badrinath/mullai
-- Create date: 06 August 2012
-- Description: Get Passenger Flight History Information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPostPassengerFlightHistoryExportInformation]
 @UserCD AS VARCHAR(30)
,@DATEFROM Datetime
,@DATETO Datetime
,@IcaoID nvarCHAR(1000)=''
,@PassengerGroupCD as nVARCHAR(1000)=''  
,@TailNum nvarchar(1000)=''
,@FleetGroupCD nvarchar(1000)=''
,@LogNum nvarchar(1000)=''
,@RemovePaxDetail as bit=0 
,@SortBy as varchar(1)
--,@SortByPassengerCode as bit=1 
--,@SortByPassengerName as bit=1
AS

SET NOCOUNT ON

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
-----------------------------Passenger Group Filteration----------------------
DECLARE @TempPassengerGroupCD TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	PASSENGERREQUESTORID BIGINT
	
	)



	BEGIN 
	INSERT INTO @TempPassengerGroupCD
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P 
		LEFT OUTER JOIN PassengerGroupOrder PGO ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
		LEFT OUTER JOIN PassengerGroup PG ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE (PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) OR @PassengerGroupCD = '') 
	--PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ','))
		AND P.CustomerID = @CUSTOMER 
	END
-----------------------------Passenger Group Filteration----------------------
	
	DECLARE @AppDTFormat VARCHAR(25)
SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END
DECLARE @IsZeroSuppressActivityPassengerRpt BIT;
SELECT @IsZeroSuppressActivityPassengerRpt = IsZeroSuppressActivityPassengerRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityPassengerRpt=0


CREATE TABLE #PAXTEXP(PaxCode VARCHAR(10),PaxName varchar(150),DPaxCode VARCHAR(10),DPaxName varchar(150),FlightPurpose varchar(25),FltDate DATE,TailNmbr varchar(9),DispatchNo varchar(12),
LogNum bigint,LegID bigint,passLegID bigint,LegNum bigint,DepICAOID char(4),DepCityName varchar(40),ArrICAOID char(4),ArrCityName varchar(40),Distance numeric(5,0),
Blk_Hrs numeric(6,3),sumBlockHours numeric(6,3),Flt_Hrs numeric(6,3),sumFlightHrs numeric(6,3),Pax_Total numeric(4,0)
,EsTotal numeric(4,0),CatCode char(4),fltcatdesc varchar(60),TotLine varchar(10),FltPurpose varchar(40),
PassengerRequestorCD varchar(5),PassengerFirstName varchar(60),DATFORMAT varchar(25))

CREATE TABLE #PAXTEXP1(PaxCode VARCHAR(10),PaxName varchar(150),DPaxCode VARCHAR(10),DPaxName varchar(150),FlightPurpose varchar(25),FltDate DATE,TailNmbr varchar(9),DispatchNo varchar(12),
LogNum bigint,LegID bigint,passLegID bigint,LegNum bigint,DepICAOID char(4),DepCityName varchar(40),ArrICAOID char(4),ArrCityName varchar(40),Distance numeric(5,0),
Blk_Hrs numeric(6,3),sumBlockHours numeric(6,3),Flt_Hrs numeric(6,3),sumFlightHrs numeric(6,3),Pax_Total numeric(4,0)
,EsTotal numeric(4,0),CatCode char(4),fltcatdesc varchar(60),TotLine varchar(10),FltPurpose varchar(40),
PassengerRequestorCD varchar(5),PassengerFirstName varchar(60),DATFORMAT varchar(25))

INSERT INTO #PAXTEXP
 SELECT A.* FROM (
 SELECT DISTINCT 
 [PaxCode]=pax.PassengerRequestorCD
,[PaxName]= isnull(pax.PassengerFirstName,'')+' '+isnull(pax.PassengerLastName,'')+' '+isnull(pax.PassengerMiddleName,'')
,[DPaxCode]=dpax.PassengerRequestorCD
,[DPaxName]=isnull(dpax.PassengerFirstName,'')+' '+isnull(dpax.PassengerLastName,'')+' '+isnull(dpax.PassengerMiddleName,'')
,[FlightPurpose]=fP.FlightPurposedescription
,[FltDate]=PL.ScheduledTM
,[TailNmbr]=F.TailNum
,[DispatchNo]=(case when convert(varchar,PM.DispatchNum)='' then '0'
                 else PM.DispatchNum End)
,[LogNum]=PM.LogNum
,[LegID]=PL.TripLegID
,[passLegID]=PL.polegID
,[LegNum]=PL.LegNUM
,[DepICAOID]=AD.ICAOID
,[DepCityName]=AD.CityName
,[ArrICAOID]=AA.ICAOID
,[ArrCityName]=AA.CityName
,[Distance]=PL.Distance*1.15078
,[Blk_Hrs]=ROUND(PL.BlockHours,1)
,Hrs.sumBlockHours sumBlockHours
,[Flt_Hrs]=ROUND(PL.FlightHours,1)
, Hrs.SumFlightHours sumFlightHrs
,[Pax_Total]=PL.PassengerTotal
,[EsTotal]=F.MaximumPassenger-PL.PassengerTotal
,[CatCode]=FC.FlightCatagoryCD
,[fltcatdesc]=FC.FlightCatagoryDescription
,[TotLine]=''
,[FltPurpose]=PL.FlightPurpose
 ,dpax.PassengerRequestorCD PassengerRequestorCD
 ,dpax.PassengerFirstName PassengerFirstName
 ,[DATFORMAT]=@DATEFORMAT

 FROM PostflightMain PM   
    INNER JOIN PostflightLeg PL ON PL.Pologid = PM.PoLogID AND PL.IsDeleted=0
    INNER JOIN PostflightPassenger PP ON PL.PoLegID = PP.PoLegID  
    INNER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID   
    INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerGroupCD ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
    INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID   
    INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID  
    left outer JOIN 
(
	select distinct PP.PassengerFirstName,PP.PassengerLastName,PP.PassengerMiddleName,P.PassengerRequestorCD,PP.POLegID,PP.FlightPurposeID
	from PostflightPassenger PP
	INNER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID
	INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerGroupCD ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
) pax on PL.POLegID = pax.POLegID
left outer JOIN 
(
	select distinct PP1.PassengerFirstName,PP1.PassengerLastName,PP1.PassengerMiddleName,DP.PassengerRequestorCD,PP1.POLegID,PP1.FlightPurposeID
	from PostflightPassenger PP1
	INNER JOIN Passenger dP ON dP.PassengerRequestorID = PP1.PassengerID
	INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerGroupCD ) P1 ON P1.PASSENGERREQUESTORID = dP.PassengerRequestorID
) dpax on PL.POLegID = dpax.POLegID     
    INNER JOIN (  
     SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,FG.FleetGroupCD,F.MaximumPassenger   
     FROM Fleet F   
     LEFT OUTER JOIN FleetGroupOrder FGO  
     ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
     LEFT OUTER JOIN FleetGroup FG   
     ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
    WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
   LEFT OUTER JOIN						 
   (				
	SELECT DISTINCT  SUM(ROUND(PL.FlightHours,1)) SumFlightHours , SUM(ROUND(PL.BlockHours,1)) sumBlockHours ,P.PassengerRequestorID, PM.PoLogID			
	FROM PostflightMain PM   
    INNER JOIN PostflightLeg PL ON PL.Pologid = PM.PoLogID AND PL.IsDeleted=0
    INNER JOIN PostflightPassenger PP ON PL.PoLegID = PP.PoLegID  
    INNER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID 
    INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerGroupCD ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
    WHERE  PM.CustomerID= (dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND PM.IsDeleted=0
    GROUP BY 	PM.POLogID,P.PassengerRequestorID 	 ) Hrs
    ON Hrs.PoLogID=pM.PoLogID
   LEFT OUTER JOIN FlightPurpose FDP1 ON DPAX.FlightPurposeID = FDP1.FlightPurposeID
   LEFT OUTER JOIN FlightPurpose FP ON PAX.FlightPurposeID = FP.FlightPurposeID
    LEFT OUTER JOIN FlightCatagory FC  ON PL.FlightCategoryID = FC.FlightCategoryID  
 
   WHERE Pm.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))   
    AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
    AND PM.IsDeleted=0
    AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '' )                   
    AND ( PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '' )          
    AND ( F.TailNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' ))a
 
					
insert into #PAXTExP1
--SELECT A.* FROM (
 SELECT DISTINCT 
              
 [PaxCode]=p.PassengerRequestorCD

,[PaxName]= isnull(p.FirstName,'')+' '+isnull(p.LastName,'')+' '+isnull(p.MiddleInitial,'')
,[DPaxCode]=null
,[DPaxName]=null
,[FlightPurpose]=null
,[FltDate]=NULL
,[TailNmbr]=NULL
,[DispatchNo]=NULL
,[LogNum]=NULL
,[LegID]=NULL
,[passLegID]=NULL
,[LegNum]=NULL
,[DepICAOID]=null
,[DepCityName]=null
,[ArrICAOID]=null
,[ArrCityName]=null
,[Distance]=NULL
,[Blk_Hrs]=NULL
,null sumBlockHours
,[Flt_Hrs]=null
, null sumFlightHrs
,[Pax_Total]=NULL
,[EsTotal]=null
,[CatCode]=null
,[fltcatdesc]=null
,[TotLine]=''
,[FltPurpose]=null
 ,null PassengerRequestorCD
 ,null PassengerFirstName
 ,[DATFORMAT]=@DATEFORMAT
   FROM Passenger P
   INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerGroupCD ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
   WHERE P.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))   


IF @IsZeroSuppressActivityPassengerRpt=0
BEGIN  
delete from #PAXTExP1 where PaxCode in(select distinct PaxCode from #PAXTExP)
SELECT M.* FROM          
(

	SELECT * FROM #PAXTExP1 
		UNION ALL           
	SELECT  * FROM #PAXTExP 
	

)M   
where ( M.ArrICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '' )          
      AND( M.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '' )         
    AND ( M.TailNmbr IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' ) 
    ORDER BY m.PaxCode,m.LogNum,M.LegNum
  
END
ELSE
BEGIN
SELECT M.* FROM (
SELECT DISTINCT T.* FROM #PAXTExP T)M
  
where ( M.ArrICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '' )               
    AND( M.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '' )         
    AND ( M.TailNmbr IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' ) 
	
	 ORDER BY m.PaxCode,m.LogNum,M.LegNum
END           

IF OBJECT_ID('tempdb..#PAXTEXP') IS NOT NULL
DROP TABLE #PAXTEXP

IF OBJECT_ID('tempdb..#PAXTEXP1') IS NOT NULL
DROP TABLE #PAXTEXP1
-- EXEC spGetReportPostPassengerFlightHistoryExportInformation 'jwilliams_13','2012-10-10','2012-12-12','','','','','',1,b





GO


