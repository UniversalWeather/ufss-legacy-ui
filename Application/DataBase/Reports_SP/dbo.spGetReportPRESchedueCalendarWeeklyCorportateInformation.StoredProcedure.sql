IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarWeeklyCorportateInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCorportateInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--[spGetReportPRESchedueCalendarWeeklyCorportateInformation] 'Ajeets',10002,'2014/12/21',1,'2014/12/27','','','','','','',1,0,1,0,0,1,0,0,0,'Local',1,1,0,0,0,0,0,'10002155621,10002155632,10002155633,10002185340,10002188558,10002188573,10002194812,10002194819,10002194910,10002198895,10002208575,10002208970,10002209795,10002209799,10002209803,10002209807,10002209811,10002209815,10002209819,10002209823,10002209827,10002210504','10002191500,10002209372,1000252396,10002209344,10002209844,10002209880,10002209881,10002209882,1000252477,1000252466,10002208844,1000252402,10002208858,10002187496,10002187522,10002187548,10002186215,10002186137,10002186163,10002186189,10002199504,10002209879,10002208551,10002208552,10002208943,10002194841,10002194839,10002208550,10002199514,10002179300,10002208614,1000252375,10002209316,10002209843,10002208949,1000252363,10002208951,1000252484,10002208932,10002177894,10002185266,10002199505,10002185046,10002208310,1000252374,10002208852,1000252367,10002194837,10002210470,10002195263,10002208625,10002208634,10002186267,10002208849,10002210442,10002208571,10002209883,10002186293,10002186319,10002186345,10002208566,10002177733,10002186241,10002199499,10002208568,10002208557,10002194840,10002199506,10002210414,10002199510,10002188817'
*/				   
CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyCorportateInformation]  
	@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID VARCHAR(30)---=10098
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfWeeks INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsShowTripStatus BIT=0
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''


AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarWeeklyCorportateInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
          
            
SET NOCOUNT ON   


		DECLARE @CumTemp TABLE     
		(    
   
		 TripId BIGINT,
		 LegNUM BIGINT,     
		 CumulativeETE numeric(14,3)    
		) 
		DECLARE @TotalEteTemp TABLE     
		(       
		 TripId BIGINT,     
		 TotalETE numeric(14,3)    
		)
		INSERT INTO @TotalEteTemp(TripId, TotalETE)  
		SELECT TripId, ISNULL(SUM(ElapseTM),0) 
		FROM PreflightLeg 
		WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 
		GROUP BY TripID


 SELECT @ECrew=CASE WHEN ISNULL(@EFleet,'')<>'' THEN '' ELSE @ECrew END   
 
SELECT @BlackWhiteClr = CASE WHEN @Color = 0 THEN 1 ELSE @BlackWhiteClr END
	
DECLARE @DateFrom DATETIME=@BeginDate,@DateTo DATETIME=@EndDate

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		


IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END




-----Rest Record and Maintenace Reco
DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9))

 
 DECLARE @TableTrip TABLE(TripNUM BIGINT,
                          CrewList VARCHAR(4000),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          FleetID BIGINT,
                          TailNum VARCHAR(9),
                          TypeCode VARCHAR(10),
                          DutyTYPE CHAR(2),
                          NextLocalDTTM DATETIME,
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(25),
                          ArrivelICAOID VARCHAR(25),
                          LegNum BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          Passenger VARCHAR(4000),
                          TripStatus CHAR(1),
                          Requestor VARCHAR(60),
                          CrewVal VARCHAR(5),
                          FgColor VARCHAR(8),
                          BgColr  VARCHAR(8),
						  ElapseTM NUMERIC(7,3)
                          )
 


   
 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=Crew.CrewList
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PAX.PaxNames
     ,PM.TripStatus
     ,Requestor=ISNULL(PPM.LastName,'') +CASE WHEN ISNULL(PPM.FirstName,'')<>'' THEN  ', ' +PPM.FirstName+' '  ELSE ' ' END+ISNULL(PPM.MiddleInitial,'')
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
	 ,PL.ElapseTM
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN Passenger PPM ON PM.PassengerRequestorID=PPM.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***'  FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	  		 LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END+ISNULL(P1.MiddleInitial,'')+' '+'###'
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID   
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
      AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=Crew.CrewList
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PAX.PaxNames
     ,PM.TripStatus
     ,Requestor=ISNULL(PPM.LastName,'') +CASE WHEN ISNULL(PPM.FirstName,'')<>'' THEN  ', ' +PPM.FirstName+' '  ELSE ' ' END+ISNULL(PPM.MiddleInitial,'')
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
      ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
      ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
	  ,PL.ElapseTM
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN Passenger PPM ON PM.PassengerRequestorID=PPM.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
            LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END +ISNULL(P1.MiddleInitial,'')+' '+'###'
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID   
     
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
	   AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
      AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
       AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


INSERT INTO @CurDate(DateWeek,FleetID,TailNum)
SELECT T.*,F.FleetID,F.TailNum FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                            LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
 



INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,DepICAOID,CrewList,Passenger,TripStatus,Requestor,CrewVal,FgColor,BgColr)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
        CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,000,RecordTye
        ,TT.ArrivelICAOID,TT.CrewList,TT.Passenger,TT.TripStatus,TT.Requestor,TT.CrewVal
        ,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END) 
         FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                             WHERE CONVERT(DATE,CD.DateWeek)>CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,Requestor,CrewVal,FgColor,BgColr,DepICAOID)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.Requestor,TT.CrewVal
                ,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END),TT.DepICAOID
                 FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye IN('C','M')
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                             --  AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


                  
DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)




DECLARE @TempTable TABLE(WeekNumber INT
                        ,FleetID BIGINT
                        ,TailNum VARCHAR(9)
                        ,TripNUM BIGINT
                        ,DepTime VARCHAR(5)
                        ,DepLoc VARCHAR(25)
                        ,ArrTime VARCHAR(5)
                        ,ArrLoc VARCHAR(25) 
                        ,crewlist VARCHAR(4000) 
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,SORTORDER INT
                        ,RecordType VARCHAR(1)
                        ,TypeCode VARCHAR(10)
                        ,Passenger VARCHAR(4000)
                        ,TripStatus CHAR(1)
                        ,Requestor VARCHAR(60)
                        ,CrewVal VARCHAR(5)
                        ,FgColor VARCHAR(8)
                        ,BgColr  VARCHAR(8)
						,ElapseTM NUMERIC(7,3)
						,TotalETE numeric(7,3)
						,CumulativeETE numeric(7,3)
						,TripID BIGINT
                        ) 
 

INSERT INTO @TempTable   
SELECT distinct 0 WeekNumber
     ,TT.FleetID
     ,TT.TailNum
     ,TripNUM
     ,SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)
     ,DepICAOID
     ,SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)
     ,ArrivelICAOID
     ,CrewList
     ,CONVERT(DATE,DepartureDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,TT.Passenger
     ,TT.TripStatus
     ,TT.Requestor
     ,TT.CrewVal
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)
	  ,TT.ElapseTM
	  ,TE.TotalETE
	  ,0 CumulativeETE
	  ,TT.TripID
      FROM @TableTrip TT  
			LEFT OUTER JOIN @TotalEteTemp TE ON TE.TRIPID=TT.TripID  
		    LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
		    LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum

		INSERT INTO @CumTemp(TripId,LegNUM, CumulativeETE)  
			SELECT 
				a.TripId, 	
				a.LegNum,
				ISNULL(SUM(b.ElapseTM),0) ElapseTM
			FROM 
				@TempTable a,
				@TempTable b
			WHERE a.TripId = b.TripId AND
				b.LegNUM <= a.LegNUM
			Group By a.TripId,a.LegNum,a.ElapseTM
			order by a.LegNUM 
			--select * from @TempTable
			--select * from @CumTemp
		UPDATE t 
		SET t.CumulativeETE = c.CumulativeETE
		from  
		 @TempTable as t 
		 inner join @CumTemp as c on c.TripId = t.TripID and c.LegNum = t.LegNum



 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;

SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 
--SELECT  @WEEK= CASE WHEN (COUNT( DISTINCT WeekNumber))=0 THEN ((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) ELSE COUNT( DISTINCT WeekNumber) END  FROM #TempTable

WHILE (@WEEKNUM<=@WEEK)

 BEGIN
  
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)

      WHILE @DateVal < @BeginDate
       
       BEGIN
       
         UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=@DateVal
		
		INSERT INTO @TempTable(WeekNumber)
		   SELECT DISTINCT @WEEKNUM
		                    WHERE NOT EXISTS(SELECT Date FROM @TempTable WHERE WeekNumber=@WEEKNUM) 

    
       SET @DateVal=DATEADD(DAY,1,@DateVal)
      
      END

 
     
 SET @WEEKNUM=@WEEKNUM+1
 
 END


SELECT ROW_NUMBER() OVER(ORDER BY WeekNumber,Date,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER) AS ID
      ,WeekNumber
      ,TripNUM=CASE WHEN TripNUM=0 THEN NULL ELSE TripNUM END
      ,Date SortDate
      ,TailNum
      ,LegNum
      ,DepTime
      ,DepLoc
      ,ArrTime
      ,ArrLoc
      ,crewlist
      ,FleetID
      ,TypeCode
      ,dbo.GetShortDateFormatByUserCD(@UserCD,Date) TripDate
	  ,DATEPART(WEEKDAY, Date) WEEKDAY
	  ,Passenger
	  ,DateRange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)
	  ,TripStatus
	  ,TT.Requestor
	  ,TT.CrewVal
	  ,TT.BgColr
	  ,TT.FgColor
	  ,ElapseTM
	  ,TotalETE
	  ,CumulativeETE
 FROM @TempTable TT
 WHERE DATE BETWEEN @DATEFROM AND @DATETO
 --WHERE DepLoc IS NOT NULL
 ORDER BY WeekNumber,SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER


 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
 DROP TABLE #TempTable
 END
GO


