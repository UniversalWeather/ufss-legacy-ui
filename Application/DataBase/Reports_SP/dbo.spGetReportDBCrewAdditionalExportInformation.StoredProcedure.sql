IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCrewAdditionalExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCrewAdditionalExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportDBCrewAdditionalExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30)
)
AS
 
-- ===============================================================================
-- SPC Name: [spGetReportDBCrewAdditionalExportInformation]
-- Author: 
-- Create date: 4 Nov 2013
-- Description: Get Crew Additional Information
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT 
  [code] = CrewInfoCD
  ,[desc] = CrewInformationDescription
FROM CrewInformation
WHERE CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND CustomerID=CONVERT(BIGINT,@UserCustomerID)
-- And IsInActive=0
 And IsDeleted=0

--EXEC spGetReportDBCrewAdditionalExportInformation 'SUPERVISOR_99',10099





GO


