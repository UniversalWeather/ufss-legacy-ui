IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileInformationSub4]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileInformationSub4]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFleetProfileInformationSub4]
	 @UserCD VARCHAR(30)
	,@TailNum char(6)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileInformationSub4
-- Author: Abhishek.s
-- Create date: 27 Sep 2012
-- Description: Get Fleet Profile Sub Report 3 Information
-- Revision History
-- Date			Name		Ver		Change
-- 16TH Nov		Abhishek	1.1		SubRep4 for Fleet Profile
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID   BIGINT,
        @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

       SELECT [Notes] = F.Notes
         FROM Fleet F 
		WHERE F.IsDeleted = 0
	      AND F.CustomerID = @CUSTOMERID
	      AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	      AND F.TailNum = RTRIM(@TailNum)
	  
 -- EXEC spGetReportFleetProfileInformationSub4 'JWILLIAMS_11', 'XXXX'

GO


