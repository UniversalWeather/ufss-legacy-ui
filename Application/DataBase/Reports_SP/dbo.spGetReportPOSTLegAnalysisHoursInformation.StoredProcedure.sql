IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTLegAnalysisHoursInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTLegAnalysisHoursInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







      
CREATE PROCEDURE [dbo].[spGetReportPOSTLegAnalysisHoursInformation]                                    
(                     
  @UserCD AS VARCHAR(30),  --Mandatory                              
  @DATEFROM DATETIME, --Mandatory                              
  @DATETO DATETIME, --Mandatory                              
  @TailNum VARCHAR(5000)='',--Optional                              
  @FleetGroupCD VARCHAR(5000)=''--Optional                              
 )        
AS                            
 --=============================================                                  
 --Author: Mathes             
 --ALtered 05-10-12                                 
 --Description: Leg Analysis/Hours(FC) - Report       
 -- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package                           
 --=============================================                                  
                                
SET NOCOUNT ON                                  
BEGIN 
--for company profile setting: block hours and flight hours-----------
DECLARE @AircraftBasis numeric(1,0);
      SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
       AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 
--for company profile setting: block hours and flight hours--      
  Declare @DateRange Varchar(30)
 Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)                                   
 
  DECLARE @CUSTOMERID BIGINT = (SELECT CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))) 
  Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))                                 

----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		AircraftCD VARCHAR(4),
		TailNumber VARCHAR(9)	
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID ,F.AircraftCD,F.TailNum
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.AircraftCD,F.TailNum
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.AircraftCD,F.TailNum
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMERID  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END

-----------------------------TailNum and Fleet Group Filteration---------------------- 
             
 DECLARE  @TempDist Table (DistId Int,DistFrom Numeric(10,2),DistTo Numeric(10,2),DispFrom varchar(5),DispTo varchar(5))               
 Declare @DistId Int                          
 Declare @DistFrom Numeric(9,2)                          
 Declare @DistTo Numeric(9,2)                        
                    
 Declare @RowId Int                          
 Declare @NumRows Int    
                   
        INSERT INTO @TempDist (DistId,DistFrom,DistTo,DispFrom,DispTo)                          
        Select 1,0.0,0.2,'0:00','00:12'  union All                     
        Select 2,0.3,0.5,'00:13','00:30'  union All
        Select 3,0.6,0.8,'00:31','00:48'  union All        
        Select 4,0.9,1.1,'00:49','01:06'  union All        
        Select 5,1.2,1.4,'01:07','01:24'  union All        
        Select 6,1.5,1.7,'01:25','01:42'  union All        
        Select 7,1.8,2.0,'01:43','02:00'  union All        
        Select 8,2.1,3.0,'02:01','03:00'  union All  
        Select 9,3.1,4.0,'03:01','04:00'  union All 
        Select 10,4.1,5.0,'04:01','05:00'  union All 
        Select 11,5.1,6.0,'05:01','06:00'  union All 
        Select 12,6.1,7.0,'06:01','07:00'  union All 
        Select 13,7.1,8.0,'07:01','08:00'  union All 
        Select 14,8.1,9.0,'08:01','09:00'  union All 
		select 15,09.1,10.00,'09:01','10:00' union all
        select 16,10.1,11.00,'10:01','11:00' union all
        select 17,11.1,12.00,'11:01','12:00' union all
        select 18,12.1,13.00,'12:01','13:00' union all
        select 19,13.1,14.00,'13:01','14:00' union all
        select 20,14.1,15.00,'14:01','15:00' union all
        select 21,15.1,24.00 ,'15:01','OVER' 
        --Select 15,9.1,24.0,'OVER','09:00'        
         

    Create Table #TempReport (SuppAft INT,DistId Int, FromMile Numeric(9,2),ToMile Numeric(9,2),FleetID BIGINT,TailNum Varchar(9),AircraftCD CHAR(6),NoOfLegs Int,                          
                              BlockHours Numeric(10,2),FlightHours Numeric(10,2),  Miles Int, FuelBurn Int, NoOfPax Int,     
                              TotLine Char(1), AvgBleg Numeric(10,2),DispFrom Varchar(5),DispTo Varchar(5))                       


INSERT INTO #TempReport(DistId,AircraftCD,FromMile,ToMile,TailNum,FleetID,SuppAft,DispFrom,DispTo)
SELECT TD.DistId,TF.AircraftCD,TD.DistFrom,TD.DistTo,TF.TailNumber,TF.FleetID,0,TD.DispFrom,TD.DispTo FROM @TempFleetID TF CROSS JOIN @TempDist TD ORDER BY AircraftCD




UPDATE #TempReport SET NoOfLegs=Dist.NoOfLegs
					  ,BlockHours=Dist.BlockHours
					  ,FlightHours=Dist.FlightHours
					  ,NoOfPax=Dist.NoOfPax
					  ,FuelBurn=FuelUsed
					  ,AvgBleg=FuelUsed/Dist.NoOfLegs
					  ,SuppAft=1
					  ,Miles=Dist.Distance
					    FROM(SELECT AircraftCD,   
									   TailNumber, 
									   FleetID,                   
									   ISNULL(Sum(ROUND(BlockHours,1)),0) As BlockHours,                        
									   ISNULL(Sum(ROUND(FlightHours,1)),0) As FlightHours,                         
									   ISNULL(Sum(Distance),0) As Distance,                         
									   IsNull(Sum(FuelUsed),0) As FuelUsed,                         
									   ISNULL(COUNT(NoOfLegs),0) AS NoOfLegs,    
									   ISNULL(SUM(NoOfPax),0) AS NoOfPax,DistID  FROM (
								 SELECT  F.AircraftCD,                       
										 IsNull(ROUND(PostflightLeg.BlockHours,1),0) As BlockHours,                        
										 IsNull(ROUND(PostflightLeg.FlightHours,1),0) As FlightHours,                         
										 IsNull(PostflightLeg.Distance,0) As Distance,                         
										 IsNull(PostflightLeg.FuelUsed,0) As FuelUsed,                         
										 ISNULL(PostflightLeg.POLegID, 0) AS NoOfLegs,    
										 ISNULL(PostflightLeg.PassengerTotal, 0) AS NoOfPax ,
										 F.TailNumber,
										 F.FleetID,
										 CASE WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 0.00 AND 0.20 THEN 1 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 0.30 AND 0.50 THEN 2 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 0.60 AND 0.80 THEN 3 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 0.90 AND 1.10 THEN 4 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 1.20 AND 1.40 THEN 5 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 1.50 AND 1.70 THEN 6 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 1.80 AND 2.00 THEN 7 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 2.10 AND 3.00 THEN 8 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 3.10 AND 4.00 THEN 9 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 4.10 AND 5.00 THEN 10 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 5.10 AND 6.00 THEN 11 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 6.10 AND 7.00 THEN 12 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 7.10 AND 8.00 THEN 13 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 8.10 AND 9.00 THEN 14 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 9.10 AND 10.00 THEN 15 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 10.10 AND 11.00 THEN 16 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 11.10 AND 12.00 THEN 17 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 12.10 AND 13.00 THEN 18 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 13.10 AND 14.00 THEN 19 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 14.10 AND 15.00 THEN 20 
											  WHEN (CASE WHEN @AircraftBasis=1 THEN PostflightLeg.BlockHours ELSE  PostflightLeg.FlightHours END) BETWEEN 15.10 AND 999999999 THEN 21   END DistID                     
										FROM  PostflightMain 
										   INNER JOIN  PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID  AND PostflightLeg.IsDeleted=0              
										   INNER JOIN @TempFleetID F ON F.FleetID=PostflightMain.FleetID   
										WHERE PostflightMain.IsDeleted=0               
										 AND  PostflightLeg.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))     
										 And  Convert(Date,PostflightLeg.ScheduledTM) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO) 	
								)TEMP
								GROUP BY AircraftCD,TailNumber,DistID,FleetID
							)Dist 
                             WHERE Dist.FleetID=#TempReport.FleetID
                              AND Dist.DistID = #TempReport.DistId
             
 
 IF @SuppressActivityAircft=1
 BEGIN                 
   Select TailNum,         
          AircraftCD,                  
          FromMile BeginHour,        
          ToMile EndHour,        
          IsNull(NoOfLegs,0) NoOfLegs,  
          ISNULL(BlockHours,0) BlockHours,
          ISNULL(FlightHours,0) FlightHours, 
          IsNull(Miles,0) Miles,        
          IsNull(FuelBurn,0) FuelBurn,        
          IsNull(AvgBleg,0) AvgBleg,        
          IsNull(NoOfPax,0) NoOfPax,
          DispFrom,
          DispTo,
          @TenToMin TenToMin,
          @DateRange DateRange,
          @AircraftBasis AircraftBasis  
          From #TempReport  
          WHERE FleetID IN (SELECT FleetID FROM #TempReport WHERE SuppAft=1)
           ORDER BY AircraftCD 
  END
  ELSE
  BEGIN
  
   Select TailNum,         
          AircraftCD,                  
          FromMile BeginHour,        
          ToMile EndHour,        
          IsNull(NoOfLegs,0) NoOfLegs,  
          ISNULL(BlockHours,0) BlockHours,
          ISNULL(FlightHours,0) FlightHours, 
          IsNull(Miles,0) Miles,        
          IsNull(FuelBurn,0) FuelBurn,        
          IsNull(AvgBleg,0) AvgBleg,        
          IsNull(NoOfPax,0) NoOfPax,
          DispFrom,
          DispTo,
          @TenToMin TenToMin,
          @DateRange DateRange,
          @AircraftBasis AircraftBasis   
          From #TempReport  
           ORDER BY AircraftCD 
  
  END                        
                              
   IF OBJECT_ID('tempdb..#TmpTble') is not null                              
       DROP table #TmpTble                                      
   IF OBJECT_ID('tempdb..#TempReport') is not null                          
      DROP table #TempReport                              
 END     
 -- EXEC spGetReportPOSTLegAnalysisHoursInformation 'SUPERVISOR_99','01-JAN-1996','31-DEC-2016',''


GO


