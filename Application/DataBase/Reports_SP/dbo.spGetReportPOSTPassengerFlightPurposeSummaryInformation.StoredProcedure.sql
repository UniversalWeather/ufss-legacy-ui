IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerFlightPurposeSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerFlightPurposeSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerFlightPurposeSummaryInformation]
		(
			@UserCD AS VARCHAR(30), --Mandatory
			@DATEFROM AS DATETIME, --Mandatory
			@DATETO AS DATETIME, --Mandatory
			@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
			@FlightPurposeCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
			@PassengerGroupCD VARCHAR(500)='' 
		)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTPassengerFlightPurposeSummaryInformation
-- Author:  A.Akhila
-- Create date: 2 Aug 2012
-- Description: Get Passenger Flight Purpose Summary for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	SELECT DISTINCT
		[Period] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
		,[PassengerCode] = P.PassengerRequestorCD
	   ,[PassengerLastName] = P.LastName 
	   ,[PassengerFirstName] = P.FirstName
	   ,[PassengerMiddleName] = P.MiddleInitial
	   ,[FlightPurpose] = FP.FlightPurposeDescription +' - '+ CONVERT (VARCHAR, FP.FlightPurposeCD)
	   ,[StatuteMilesFlown] = ISNULL(POL.Distance *1.1508,0)
	   ,[NauticalMilesFlown] = POL.Distance
	   ,[HoursFlown] = ROUND(POL.FlightHours,1)
	   ,[TenToMin] = @TenToMin
	   ,POL.POLegID
  
   FROM PostflightMain POM 
        INNER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
        INNER JOIN PostflightPassenger POP ON POP.POLegID=POL.POLegID 
        INNER JOIN FlightPurpose FP ON FP.FlightPurposeID=POP.FlightPurposeID
        INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID AND P.IsDeleted=0
        INNER JOIN(SELECT DISTINCT PassengerID FROM  @TempPassengerID) TP ON TP.PassengerID=P.PassengerRequestorID
 WHERE POM.IsDeleted=0 
  AND POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND (FP.FlightPurposeCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightPurposeCD, ',')) OR @FlightPurposeCD = '')
ORDER BY [PassengerLastName],[FlightPurpose]
    --EXEC spGetReportPOSTPassengerFlightPurposeSummaryInformation 'jwilliams_13','2000/1/1','2012/12/12', '', ''
    
    
 GO