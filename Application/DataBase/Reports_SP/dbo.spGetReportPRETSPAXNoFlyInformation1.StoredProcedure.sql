IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPAXNoFlyInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation1]  
 @UserCD VARCHAR(50)--Mandatory  
 ,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]  
 ,@LegNum AS VARCHAR(50) = ''  
 ,@ReportHeaderID VARCHAR(30)=''  
 ,@TempSettings VARCHAR(MAX)=''  
 ,@PassengerCD VARCHAR(30) = ''
AS  
BEGIN     
    
--SELECT * FROM PreflightMain WHERE TripNUM=2021 AND CustomerID=10099    
    
--SELECT * FROM TripSheetReportHeader WHERE CustomerID=10099    
-- ================================================================================      
-- SPC Name: spGetReportPRETSPAXNoFlyInformation1      
-- Author: AISHWARYA.M      
-- Create date: 30 Aug 2012      
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS      
-- Revision History      
-- Date   Name  Ver  Change      
--       
-- =================================================================================      
SET NOCOUNT ON      
      
      
--SELECT * FROM PreflightMain WHERE TripNUM=2023 AND CustomerID=10099    
      
DECLARE @TripSheet TABLE(   ONEBITEG BIT      
      , GENNOTES SMALLINT      
      , TRIPALERTS BIT      
      , FULLCREW BIT      
      , DEPTAUTH BIT      
      , LEGNOTES SMALLINT      
      , FUELBURN BIT      
      , NOTES SMALLINT      
      , ALERTS BIT      
      , OUTBOUND BIT      
      , PAXMANIF SMALLINT      
      , PAXCODE BIT      
      , PAXDEPT BIT      
      , PAXPHONE BIT      
      , PAXNBR BIT      
      , PAXCNG BIT      
      , ARRFBP BIT      
      , FBOAPHONE BIT      
      , FBOAFREQ BIT      
      , FBOAFAX BIT      
      , FBOAUVAIR BIT      
      , FBOAFUEL BIT      
      , FBOANPRICE BIT      
      , FBOALPRICE BIT      
      , FBOALDATE BIT      
      , FBOACONT BIT      
      , FBOAADDR BIT      
      , FBOAREM BIT      
      , PAXHOTEL BIT      
      , HOTELPHONE BIT      
      , HOTELFAX BIT      
      , HOTELCOMM SMALLINT      
      , PTANAME BIT      
      , PTAPHONE BIT      
      , PTAFAX BIT      
      , PTAREMARKS BIT      
      , CDNAME BIT      
      , CDPHONE BIT      
      , CDFAX BIT      
      , CDREMARKS BIT      
      , CREWHOTEL BIT      
      , CREWHPHONE BIT      
      , CREWHFAX BIT      
      , CREWHNRATE BIT      
      , CREWHADDR BIT      
      , CREWHCOMM SMALLINT      
      , CTANAME BIT      
      , CTAPHONE BIT      
      , CTAFAX BIT      
      , CTARATE BIT      
      , CTAREMARKS BIT      
      --, MAINTHOTEL BIT      
      --, MAINTPHONE BIT      
      --, MAINTFAX BIT      
      --, MAINTNRATE BIT      
      --, MAINTADDR BIT      
      --, MAINTCOMM SMALLINT      
      , PAXRPTPOS SMALLINT      
      , FBOAPHONE2 BIT      
      , PXLRGEFON BIT      
      , HOTELREM BIT      
      , CANAME BIT      
      , CAPHONE BIT      
      , CAFAX BIT      
      , CAREMARKS BIT      
      , CREWHREM BIT      
      , STATECODE BIT      
      , SUPPAXNOTE BIT      
      , SUPCRWNOTE BIT      
      , DISPATCHNO BIT      
      , PURPOSE BIT      
      , PAXNOTES SMALLINT      
      , ACNOTES SMALLINT      
      , FBOAPAYTYP BIT      
      , FBOAPOSTPR BIT      
      , FBOAFUEBITQT BIT      
      , AIRFLTPHN BIT      
      , CRMBBITPHN SMALLINT      
      , DISPATCHER BIT      
      , TERMTOAFBO SMALLINT      
      , ALLOWSHADE BIT      
      , CREWHROOMS BIT      
      , CREWHCONF BIT      
      , HOTELROOMS BIT      
      , HOTELCONF BIT      
      , PTACONFIRM BIT      
      , PTACOMMENT SMALLINT      
      , PTDNAME BIT      
      , PTDPHONE BIT      
      , PTDFAX BIT      
      , PTDREMARKS BIT      
      , PTDCONFIRM BIT      
      , PTDCOMMENT SMALLINT      
      , CTACONFIRM BIT      
      , CTACOMMENT SMALLINT      
      , CTDNAME BIT      
      , CTDPHONE BIT      
      , CTDFAX BIT      
      , CTDRATE BIT      
      , CTDREMARKS BIT      
      , CTDCONFIRM BIT      
      , CTDCOMMENT SMALLINT      
      , CACONFIRM BIT      
      , CACOMMENT SMALLINT      
      , CDCONFIRM BIT      
      , CDCOMMENT SMALLINT      
      , FBOACONF BIT      
      , FBOACOMM SMALLINT      
      , DEPFBP BIT      
  , FBODPHONE BIT      
      , FBODFREQ BIT      
      , FBODFAX BIT      
      , FBODUVAIR BIT      
      , FBODFUEL BIT      
      , FBODNPRICE BIT      
      , FBODLPRICE BIT      
      , FBODLDATE BIT      
      , FBODCONT BIT      
      , FBODADDR BIT      
      , FBODREM BIT      
      , FBODPHONE2 BIT      
      , FBODPAYTYP BIT      
      , FBODPOSTPR BIT      
      , FBODFUEBITQT BIT      
      , TERMTODFBO SMALLINT      
      , FBODCONF BIT      
      , FBODCOMM SMALLINT      
      , FLTCAT BIT      
      , PASSPORT BIT      
      , CITIZEN BIT      
      , PAXDOB BIT      
      , HOTELRATE BIT      
      , PTARATE BIT      
      , PTDRATE BIT      
      , CARATE BIT      
      , CDRATE BIT      
      , CONDOUTBND BIT      
      , PAXALERTS SMALLINT      
      , FARNUM BIT      
      , CNTRYCODE BIT      
      --, MAINTREM BIT      
      --, MAINTCONF BIT      
      , AIRADDINFO BIT      
      , PAXADDINFO BIT      
      , FUELLOAD BIT      
      , AUTHPHONE BIT      
      , LMESSAGE BIT      
      , [MESSAGE] VARCHAR(MAX)      
      , MESSAGEPOS SMALLINT      
      , FLIGHTNO SMALLINT      
      , LINESEP BIT      
      , ARRAIRPORT BIT      
      , DEPAIRPORT BIT      
      , REQUESTOR BIT      
      , SORTBYDATE BIT      
      , CREWLASTLG BIT      
      , PRINTLOGO BIT      
      , PRNFIRSTPG BIT      
      , INTLFLTPHN BIT      
      , PAXVISA BIT      
      , CREWPILOT1 NVARCHAR(200)      
      , CREWPILOT2 NVARCHAR(200)      
      , CREWADDL NVARCHAR(200)      
      , PAXADDLPHN BIT      
      , REQADDLPHN BIT      
      , WINDS BIT      
      , DSPTEMAIL BIT      
      , DSPTPHONE BIT      
      , TRIPNO BIT      
      , [DESC] BIT      
      , TRIPDATES BIT      
      , TMREQ BIT      
      , TAILNMBR BIT      
      , CONTPHONE BIT      
      , TYPECODE BIT      
      , CQDESC SMALLINT      
      , REVISNNO BIT      
      , REVISNDESC BIT      
      , FUELDTLS SMALLINT      
      , FUELVEND SMALLINT      
      , BOLDNOTES SMALLINT      
      , HOTELADDR BIT      
      , PAXORDER BIT      
      , TSASTATUS BIT      
      , VERIFYNO BIT      
      , ACFTRLSE BIT      
      , CBPXMLS BIT      
      , CHRMONTH BIT      
      , DALERTS BIT      
      , ATISFREQ BIT      
      , DEPATIS  BIT      
      , ARRATIS  BIT      
      , CLRNCE  BIT      
      , CRWADLINFO BIT      
      , CRWDOB  BIT      
      , CRWPNUM  BIT      
      , CRWNATION BIT      
      , CRWPDATE BIT      
      , PAXREQDOC SMALLINT      
      , LEGSEP  BIT      
      , CQNAME  BIT      
      , CQCONTACT BIT      
      , PAXNOTESF   SMALLINT      
      --,   CREWHCOMM   SMALLINT      
      ,   DISPATCHIN SMALLINT      
      ,LGMINFUEL BIT    
      ,LGPOWERSET BIT    
      ,LGAIRSPEED BIT    
      ,FBOARRUNIC BIT    
      ,FBOARRARIN BIT    
      ,FBODEPUNIC BIT    
      ,FBODEPARIN BIT    
	  ,DCREWHTL BIT
	  ,DCREWHADDR BIT
	  ,DCREWHCOMM SMALLINT
	  ,DCREWHCONF BIT
	  ,DCREWHFAX BIT
	  ,DCREWHNRTE BIT
	  ,DCREWHROOM BIT
	  ,DCREWHPHON BIT
	  ,DCREWHREM BIT
   --   ,DMAINHOTEL BIT
	  --,DMAINHADD BIT
	  --,DMAINHCOMM SMALLINT
	  --,DMAINHCON BIT
	  --,DMAINHFAX BIT
	  --,DMAINHRATE BIT
	  --,DMAINHPHON BIT
	  --,DMAINHREM BIT
		,DPAXHOTEL BIT
		,DPAXHPHON BIT
		,DPAXHFAX BIT
		,DPAXHCOMM SMALLINT
		,DPAXHREM BIT
		,DPAXHROOMS BIT
		,DPAXHCONF BIT
		,DPAXHRATE BIT
		,DPAXHADDR BIT       
      )      
      
    
DECLARE @PAXTEMP TABLE(PassengerName VARCHAR(MAX)
					,Code VARCHAR(MAX) 
					,Status VARCHAR(16)
					,Nbr INT
					,Cng CHAR(1)
					,Dept VARCHAR(MAX)
					,Phone VARCHAR(25)
					,AddlPhone VARCHAR(25)
					,PPNum NVARCHAR(120)
					,Country VARCHAR(60)
					,DOB VARCHAR(10)
					,DepartmentDesc VARCHAR(MAX)
					,FlightPurposeCode CHAR(2)
					,LegNum BIGINT
					,Visa VARCHAR(MAX)
					,OrderNUM INT
					,TripID BIGINT
					,PassengerRequestorID BIGINT
					,LegID BIGINT
					,PassengerTotal INT
					,Leg BIGINT
					,RNK INT
					,RkVal INT
)

INSERT INTO @TripSheet      
EXEC spGetReportPRETSFormatSettings @ReportHeaderID ,2 , @TempSettings      
DECLARE @PAXMANIF SMALLINT, @PAXORDER BIT;      
SELECT @PAXMANIF = PAXMANIF, @PAXORDER = PAXORDER FROM @TripSheet;      
      
DECLARE @CUSTOMERID BIGINT;      
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));      
  
IF EXISTS(SELECT 1 FROM PreflightLeg PL INNER JOIN PreflightPassengerList PPL ON PL.LegID=PPL.LegID  
     WHERE PL.TripID = CONVERT(BIGINT,@TripID)      
         AND PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))      
         AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''))  
  
 
 IF @PAXORDER = 0 -- PAXORDER IS TRUE, ORDER BY Last Name      
BEGIN      
   

  
--PRINT 'PAXORDER::TRUE'    
INSERT INTO @PAXTEMP  
 SELECT TEMP.*    
  ,RNK=DENSE_RANK()OVER(PARTITION BY TripID ORDER BY ISNULL(OrderNum,99))  
  ,RkVal=DENSE_RANK()OVER(PARTITION BY TripID ORDER BY LegID)   
 FROM (      
  SELECT DISTINCT [PassengerName] = CONVERT(VARCHAR(MAX),P.LastName) + ',' + CONVERT(VARCHAR(MAX),P.FirstName) + ' ' + CONVERT(VARCHAR(MAX),ISNULL(P.MiddleInitial,''))    
     ,[Code] = CONVERT(VARCHAR(MAX),P.PassengerRequestorCD)    
     ,[Status] = Case PP.TripStatus   WHEN 'R' THEN 'Restricted'      
           WHEN 'C' THEN 'Cleared'       
           WHEN 'S' THEN 'Selected'  ELSE 'N/A'   END      
     ,[Nbr] =  0 --ROW_NUMBER() over (order by PP.PassengerID, PL.LegNum)      
     ,[Cng] = P.IsEmployeeType      
     ,[Dept] = CONVERT(VARCHAR(MAX),ISNULL(D.DepartmentCD,''))     
     ,[Phone] = ISNULL(P.PhoneNum ,'')      
     ,[AddlPhone] = ISNULL(P.AdditionalPhoneNum,'')      
     ,[PPNum] = ISNULL(CPP.PassportNUM,'')      
     --,[PPNum] = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNUM,''))      
     ,[Country] = ISNULL(C.CountryName,'')      
     --,[DOB] = ISNULL(P.DateOfBirth,NULL)      
     ,[DOB] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) --CONVERT(VARCHAR, P.DateOfBirth, 106)      
     ,[DepartmentDesc] = CONVERT(VARCHAR(MAX),D.DepartmentName)           
     ,[FlightPurposeCode] = CASE @PAXMANIF WHEN 2 THEN 'X' WHEN 3 THEN 'XX' ELSE FP.FlightPurposeCD END    
     ,[LegNum] = PL.LegNUM     
     ,Visa.Paxnames Visa      
     ,OrderNUM    
     ,PM.TripID    
     ,P.PassengerRequestorID    
     ,PL.LegID    
     ,PL.PassengerTotal    
     ,Leg=PL.LegNUM    
        FROM PreflightMain PM       
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID      
   LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID      
   LEFT OUTER JOIN CrewPassengerPassport CPP2 ON PP.PassportID = CPP2.PassportID AND PP.PassengerID = CPP2.PassengerRequestorID  AND CPP2.IsDeleted = 0    
      LEFT OUTER JOIN Country C ON CPP2.CountryID = C.CountryID      
      INNER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID AND PP.CustomerID = FP.CustomerID      
      LEFT OUTER JOIN (SELECT DISTINCT PP2.PassengerID,PP2.LegID    
       ,Paxnames = (SELECT 'VISA :'+CONVERT(VARCHAR(5),ROW_NUMBER() OVER(PARTITION BY PL.LEGID ORDER BY PL.LEGID))+':#@#$'+ISNULL(CPV.VisaNum,'')+'@#$###'+CO.CountryName+'###'+CONVERT(VARCHAR(10),CPV.ExpiryDT,101)+'###'      
             FROM PreflightLeg PL      
              INNER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID      
              INNER JOIN Passenger P ON PL.CustomerID = P.CustomerID AND PPL.PassengerID=P.PassengerRequestorID      
              INNER JOIN CrewPassengerVisa CPV ON P.PassengerRequestorID = CPV.PassengerRequestorID AND P.CustomerID = CPV.CustomerID      
              INNER JOIN Country CO ON CPV.CountryID = CO.CountryID      
              INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID      
              INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID       
              WHERE PL.LegID = PP2.LegID      
                AND PP2.PassengerID=P.PassengerRequestorID      
                AND PL.TripID = CONVERT(BIGINT,@TripID)      
                AND PL.CustomerID =@CUSTOMERID      
                AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')      
              FOR XML PATH('')      
            )FROM  PreflightPassengerList PP2 )Visa ON Visa.LegID=PL.LegID AND Visa.PassengerID=PP.PassengerID       
   LEFT OUTER JOIN Department D ON P.DepartmentID = D.DepartmentID        
   LEFT OUTER JOIN CrewPassengerPassport CPP ON PP.PassportID = CPP.PassportID AND CPP.IsDeleted = 0      
   WHERE PM.TripID = CONVERT(BIGINT,@TripID)     
   AND PP.IsDeleted=0 
   AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))      
   AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '') 
   AND P.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN P.PassengerRequestorCD ELSE @PassengerCD END     
 ) TEMP      
   
  
DECLARE @RkVal INT,@LegVal BIGINT=900    
SELECT @RkVal=MAX(RkVal)+1 FROM @PAXTEMP  
 
WHILE @RkVal<=25  
BEGIN  
  
INSERT INTO @PAXTEMP  
SELECT CONVERT(VARCHAR(MAX),''),    
  '@@@@',    
  '',    
  0,    
  'Z',    
  '',    
  '',    
  '',    
  '',    
  '',    
  '' ,    
  '',    
  '',   
  @LegVal,    
  '',    
  0,   
  '',  
  NULL,  
  '',  
  NULL,  
  99,  
  @RkVal,  
  999  

SET @RkVal=@RkVal+1  
SET @LegVal =@LegVal+1
END   
  

 END    
  
     
     
 ELSE    
 BEGIN  -- PAXORDER IS FALSE, ORDER BY PAXCODE      
      
--PRINT 'PAXORDER::FALSE'      
 INSERT INTO @PAXTEMP
 SELECT TEMP.*     
  ,RNK=DENSE_RANK()OVER(PARTITION BY TripID ORDER BY ISNULL(OrderNum,99))  
  ,RkVal=DENSE_RANK()OVER(PARTITION BY TripID ORDER BY LegID) 
 FROM (      
  SELECT DISTINCT [PassengerName] = CONVERT(VARCHAR(MAX),P.LastName) + ',' + CONVERT(VARCHAR(MAX),P.FirstName) + ' ' + CONVERT(VARCHAR(MAX),ISNULL(P.MiddleInitial,''))    
     ,[Code] = CONVERT(VARCHAR(MAX),P.PassengerRequestorCD)    
     ,[Status] = Case PP.TripStatus   WHEN 'R' THEN 'Restricted'      
           WHEN 'C' THEN 'Cleared'       
           WHEN 'S' THEN 'Selected'  ELSE 'N/A'   END      
     ,[Nbr] =  0 --ROW_NUMBER() over (order by PP.PassengerID, PL.LegNum)      
     ,[Cng] = P.IsEmployeeType      
     ,[Dept] = CONVERT(VARCHAR(MAX),ISNULL(D.DepartmentCD,''))     
     ,[Phone] = ISNULL(P.PhoneNum ,'')      
     ,[AddlPhone] = ISNULL(P.AdditionalPhoneNum,'')      
     ,[PPNum] = ISNULL(CPP.PassportNUM,'')      
     --,[PPNum] = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNUM,''))      
     ,[Country] = ISNULL(C.CountryName,'')      
     --,[DOB] = ISNULL(P.DateOfBirth,NULL)      
     ,[DOB] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) --CONVERT(VARCHAR, P.DateOfBirth, 106)      
     ,[DepartmentDesc] = CONVERT(VARCHAR(MAX),D.DepartmentName)           
     ,[FlightPurposeCode] = CASE @PAXMANIF WHEN 2 THEN 'X' WHEN 3 THEN 'XX' ELSE FP.FlightPurposeCD END    
    ,[LegNum] = PL.LegNUM    
  ,Visa.Paxnames Visa      
     ,OrderNUM    
     --,PAXDocs = F.UWAFilePath      
      ,PM.TripID    
     ,P.PassengerRequestorID    
     ,PL.LegID    
     ,PL.PassengerTotal    
     ,Leg=PL.LegNUM    
      
        FROM  PreflightMain PM       
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID      
    LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
    LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID      
    LEFT OUTER JOIN CrewPassengerPassport CPP2 ON PP.PassportID = CPP2.PassportID AND PP.PassengerID = CPP2.PassengerRequestorID  AND CPP2.IsDeleted = 0    
    LEFT OUTER JOIN Country C ON CPP2.CountryID = C.CountryID      
    INNER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID AND PP.CustomerID = FP.CustomerID      
    LEFT OUTER JOIN (SELECT DISTINCT PP2.PassengerID,PP2.LegID    
       ,Paxnames = (SELECT 'VISA :'+CONVERT(VARCHAR(5),ROW_NUMBER() OVER(PARTITION BY PL.LEGID ORDER BY PL.LEGID))+':#@#$'+ISNULL(CPV.VisaNum,'')+'@#$###'+CO.CountryName+'###'+CONVERT(VARCHAR(10),CPV.ExpiryDT,101)+'###'      
             FROM PreflightLeg PL      
              INNER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID      
              INNER JOIN Passenger P ON PL.CustomerID = P.CustomerID AND PPL.PassengerID=P.PassengerRequestorID      
              INNER JOIN CrewPassengerVisa CPV ON P.PassengerRequestorID = CPV.PassengerRequestorID AND P.CustomerID = CPV.CustomerID      
              INNER JOIN Country CO ON CPV.CountryID = CO.CountryID      
              INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID      
              INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID       
              WHERE PL.LegID = PP2.LegID 
                AND PP2.PassengerID=P.PassengerRequestorID      
                AND PL.TripID = CONVERT(BIGINT,@TripID)
				AND PL.CustomerID =@CUSTOMERID      
                AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')      
              FOR XML PATH('')      
            )FROM  PreflightPassengerList PP2 )Visa ON Visa.LegID=PL.LegID AND Visa.PassengerID=PP.PassengerID       
   LEFT OUTER JOIN Department D ON P.DepartmentID = D.DepartmentID        
   LEFT OUTER JOIN CrewPassengerPassport CPP ON PP.PassportID = CPP.PassportID AND CPP.IsDeleted = 0      
   WHERE PM.TripID = CONVERT(BIGINT,@TripID)      
   AND PP.IsDeleted=0
   AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))      
   AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')      
   AND P.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN P.PassengerRequestorCD ELSE @PassengerCD END     
 ) TEMP      
      
 ORDER BY LegNum,TEMP.OrderNUM---TEMP.[PassengerName], TEMP.LegNUM      
   
DECLARE @RkVal1 INT ,@LegVal1 BIGINT=900     
SELECT @RkVal1=MAX(RkVal)+1 FROM @PAXTEMP  
  
  
  
WHILE @RkVal1<=25  
BEGIN  
  
INSERT INTO @PAXTEMP  
SELECT CONVERT(VARCHAR(MAX),''),    
  '@@@@',    
  '',    
  0,    
  '',    
  '',    
  'Z',    
  '',    
  '',    
  '',    
  '' ,    
  '',    
  '',   
  @LegVal1,    
  '',    
  0,   
  '',  
  NULL,  
  '',  
  NULL,  
  99,  
  @RkVal1,  
  999  
  
  
SET @RkVal1=@RkVal1+1
SET @LegVal1=@LegVal1+1  

END   
   
    
END    
  
  
  

  
SELECT DISTINCT [PassengerName]     
      ,[Code]    
      ,[Status]     
       ,[Nbr]       
      ,[Cng]      
      ,[Dept]     
      ,[Phone]      
      ,[AddlPhone]     
      ,[PPNum]    
      ,[Country]       
      ,[DOB]      
       ,[DepartmentDesc]    
      ,[FlightPurposeCode]    
      ,[LegNum]      
      ,Visa      
      ,OrderNUM    
      ,RNK    
    FROM @PAXTEMP     
    --WHERE ISNULL(Code,'') <>''    
     
 UNION ALL    
      
 SELECT DISTINCT     
     
       CONVERT(VARCHAR(MAX),'BLOCKED'),    
  '',    
  '',    
  999,    
  'Z',    
  '',    
  '',    
  '',    
  '',    
  '',    
  '' ,    
  '',    
  CountVal=CONVERT(VARCHAR(6),Leg.CountVal),    
  Leg.LegNum,    
  '',    
  0,    
  RNK = 999    
        FROM @PAXTEMP PM       
           INNER JOIN (SELECT (CASE WHEN (PassengerTotal-ISNULL(COUNT(DISTINCT Code),0)) > 0 THEN LegNum END )LegNum,TripID,PassengerTotal-ISNULL(COUNT(DISTINCT Code),0) CountVal FROM @PAXTEMP PT GROUP BY TripID,LegNum,PassengerTotal)Leg ON Leg.TripID=PM.TripID AND Leg.LegNum=PM.LegNum      
           WHERE PM.TripID = CONVERT(BIGINT,@TripID)    
    
    
 ORDER BY LegNum,OrderNUM     
    
--SET FlightPurposeCode = (SELECT CASE @PAXMANIF WHEN 2 THEN ' X' WHEN 3 THEN 'XX' ELSE FlightPurposeCode END;      

END 
--EXEC spGetReportPRETSPAXNoFlyInformation1 'SUPERVISOR_99', '10099107564','','10099165805',''   
  
--EXEC spGetReportPRETSPAXNoFlyInformation1 'SUPERVISOR_11', '1000158811','','10001281316',''




GO


