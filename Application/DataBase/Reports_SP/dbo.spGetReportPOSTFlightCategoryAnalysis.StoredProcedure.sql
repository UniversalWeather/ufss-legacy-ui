IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightCategoryAnalysis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightCategoryAnalysis]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTFlightCategoryAnalysis]  
	(    
		@DATEFROM AS DATETIME,    
		@DATETO AS DATETIME,     
		@TailNum AS NVARCHAR(4000),    
		@FleetGroupCD AS NVARCHAR(4000),     
		@FlightCatagoryCD AS NVARCHAR(4000),    
		@AircraftCD AS NVARCHAR(4000),     
		@UserCD AS VARCHAR(30)  ,
		@IsHomebase As bit=0 
	)
AS 

-- ===============================================================================      
-- SPC Name: spGetReportFlightCategoryAnalysis      
-- Author:  A.Akhila     
-- Create date: 06 Aug 2012      
-- Description: Get Crew workLoad  information for REPORTS      
--exec spGetReportPOSTFlightCategoryAnalysis '12/12/2010','12/12/2012','N331UV,WING','cvh,test','q33,tdd','DA50,fhj','UC'      
--exec spGetReportPOSTFlightCategoryAnalysis '12/12/2010','12/12/2012','','','','','jwilliams_13'      
-- Revision History      
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================      
SET NOCOUNT ON

DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
													AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1													
 -----------------------------TailNum and Fleet Group Filteration----------------------
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END

-- Fleet group filter end

DECLARE @TailTemp TABLE (FleetID bigint,TailNumber varchar(10), AircraftCD VARCHAR(4));
	  INSERT INTO @TailTemp (FleetID, TailNumber, AircraftCD) 
				SELECT DISTINCT F.FleetID, F.TailNum, f.AircraftCD 
              FROM   Fleet F
                      WHERE  F.FleetID IN (SELECT FleetID FROM @TempFleetID) 

SELECT  
	 [FLEETID] = F.FleetID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)	 	
	,[LogNum] = POM.LogNum
	,[POLegID] = POL.POLegID
	,[AIRCRAFTCODE] = F.AircraftCD
	,[Tail Number] = F.TailNum
	,[Flt Cat] = FC.FlightCatagoryCD
	,[Category Description] = FC.FlightCatagoryDescription
	,[No of Legs] = COUNT(POL.POLegID)
	,[Block Hours] = ROUND(pol.BlockHours,1)
	,[Flight Hours] = ROUND(pol.FlightHours,1)  
	,[Nautical Miles] = POL.Distance
	,[No of PAX] = POL.PassengerTotal
	,[TenToMin] = @TenToMin
INTO #FLEETINFO      
FROM     
       PostflightLeg pol
       LEFT JOIN FlightCatagory FC on FC.FlightCategoryID = POL.FlightCategoryID 
       LEFT JOIN UserMaster UM on UM.ClientID  =POL.ClientID 
       INNER JOIN PostflightMain POM on POM.POLogID = POL.POLogID AND POM.IsDeleted = 0 
       LEFT OUTER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger, F.AircraftID
			FROM Fleet F 
			WHERE F.FleetID  IN (SELECT FleetID FROM @TempFleetID)
			) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
	   LEFT JOIN Aircraft AC on AC.AircraftID = F.AircraftID    
WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
       CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	--AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
	AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
	AND POL.IsDeleted = 0

GROUP BY F.FleetID, F.AircraftCD, F.TailNum, fc.FlightCatagoryCD,  fc.FlightCatagoryDescription, pol.POLegID, pol.BlockHours, pol.FlightHours
	,pol.Distance, pol.PassengerTotal, POM.LogNum, POL.POLegID

IF @IsZeroSuppressActivityAircftRpt=0
BEGIN 
	SELECT TT.FleetID,TailNumber,dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) [DateRange],
	FI.LogNum,FI.POLegID,FI.[Flt Cat],FI.[Category Description],FI.[No of Legs]
	,FI.[Block Hours],FI.[Flight Hours],FI.[Nautical Miles],FI.[No of PAX],TT.AircraftCD,@TenToMin [TenToMin] FROM @TailTemp TT 
																   LEFT OUTER JOIN #FLEETINFO FI ON TT.FleetID=FI.FleetID
	ORDER BY TT.AircraftCD, [Tail Number], [Flt Cat]
END	
ELSE 
BEGIN
	SELECT TT.FleetID,TailNumber,dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) [DateRange],
	FI.LogNum,FI.POLegID,FI.[Flt Cat],FI.[Category Description],FI.[No of Legs]
	,FI.[Block Hours],FI.[Flight Hours],FI.[Nautical Miles],FI.[No of PAX],TT.AircraftCD,@TenToMin [TenToMin] FROM @TailTemp TT 
																   INNER JOIN #FLEETINFO FI ON TT.FleetID=FI.FleetID
	ORDER BY TT.AircraftCD, [Tail Number], [Flt Cat]
END

IF OBJECT_ID('tempdb..#FLEETINFO') IS NOT NULL
DROP TABLE #FLEETINFO
--EXEC spGetReportPOSTFlightCategoryAnalysis '12/12/2010','12/12/2012','','','','','jwilliams_11'      
    
    
    



GO


