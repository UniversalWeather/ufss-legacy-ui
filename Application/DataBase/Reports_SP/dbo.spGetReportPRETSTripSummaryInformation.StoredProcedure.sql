IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[spGetReportPRETSTripSummaryInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(30), --Mandatory
		@LegNum AS VARCHAR(150)    
		
	/*@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
	*/		
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSTripSummaryInformation
-- Author: AISHWARYA.M
-- Create date: 08 OCT 2012
-- Description: Get Preflight Tripsheet Summary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

	---- [Start] Report Criteria --
	--DECLARE @tblTripInfo AS TABLE (
	--TripID BIGINT
	--, LegID BIGINT
	--, PassengerID BIGINT
	--)

	--INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	--EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
	--	@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
	--	@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
	--	@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	---- [End] Report Criteria --

 --	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	--DECLARE @ParameterDefinition AS NVARCHAR(500) = ''
 
	----SET @SQLSCRIPT = '
      SELECT DISTINCT 
		    [TripNumber] = PM.TripNUM,
		    [DispNo] = PM.DispatchNUM,
			[Description] = PM.TripDescription,
			[TripDates] =  dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD),
			[Requestor] = P.PassengerName, --P.LastName+ ',' + P.FirstName+ ' ' + P.MiddleInitial,
			[TailNumber] = F.TailNUM,
			[ContactPhone] =P.PhoneNum,
			[BusPhone] =P.AdditionalPhoneNum,
			[PrimaryMob]=P.PrimaryMobile,
			[Type] =A.AircraftCD,
			[AircraftDomFlitePhone] = F.FlightPhoneNum,
			[AircraftIntlFlitePhone] = F.FlightPhoneIntlNum,
			[RevisionNumber] = PM.RevisionNUM,
			[DispatcherName] = UM.FirstName + ' ' + UM.LastName, --PM.DsptnName,
			[DispatcherPhone] = UM.PhoneNum,
			[Dispatcheremail] = UM.EmailID,
			[AdditionalPhone] = P.AdditionalPhoneNum,
			[ReleasedBy] = C.LastName + ',' + C.FirstName + ' ' + C.MiddleInitial,
			[FlightNumber] = PM.FlightNUM,
			[VerificationNumber] =	PM.VerifyNUM,
			[TSAlert] = PM.Notes 
			,PM.CustomerID
			,F.TailNum
			,TripID = CONVERT(VARCHAR,PM.TripID)
			,[LegNumList] = LNum.LegNumList
			
			FROM PreflightMain PM 
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN  Fleet F ON PM.FleetID = F.FleetID 
			INNER JOIN  Aircraft A ON PM.AircraftID = A.AircraftID
			INNER JOIN (
				SELECT DISTINCT PL2.TripID, LegNumList = ( 
							SELECT CONVERT(VARCHAR,LegNUM) + ','
						FROM PreflightLeg PL1 WHERE PL1.TripID = PL2.TripID AND PL2.CustomerID = 10001
						FOR XML PATH('')
					  )
				FROM  PreflightLeg PL2
			) LNum ON PM.TripID = LNum.TripID
			LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)			
			LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID
			LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
			
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT, @TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '') 
			--AND UM.UserName = @UserCD
			--ORDER BY PM.TripID
	
END			
			
    --EXEC spGetReportPRETSTripSummaryInformation 'TIM', '10001119', ''










GO


