IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar2MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar2MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPREAircraftCalendar2MhtmlInformation]
	    @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(500) = ''  -- [Optional], Comma delimited string with mutiple values
		,@HomeBase AS BIT = 0
AS
-- ===============================================================================
-- SPC Name: AircraftCalendar2ExportInformation]
-- Author: ASKAR
-- Create date: 04 Jul 2012
-- Description: Get Aircraft Calendar information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
		CREATE TABLE #TempAircraftCal (
		tail_nmbr VARCHAR(10),activedate DATE, locdep DATETIME,depicao_id CHAR(4), locarrival DATETIME,arricao_id CHAR(4),requestor VARCHAR(60),dept_code VARCHAR(8),cat_code CHAR(4),
		duty_type CHAR(2),elp_time NUMERIC(7,3),timeapprox BIT,pax_total INT,notes VARCHAR(25),Additional_crew VARCHAR(50), ron VARCHAR(9),pic_code VARCHAR(250),sic_code VARCHAR(250),
		orig_nmbr BIGINT,leg_num BIGINT,legid BIGINT,type_code VARCHAR(10),vendcode VARCHAR(5),[desc] VARCHAR(40),rec_type CHAR(1),
		grndtime VARCHAR(9),TrueRon VARCHAR(9),dayfill VARCHAR(9),nextloc DATETIME,ac_code CHAR(3),auth_code VARCHAR(8),resv_avail INT,purpose VARCHAR(40),
		[private] BIT,ordernum VARCHAR(10),aircraft_code VARCHAR(15),maxpax NUMERIC(3,0),dep_time DATETIME,arr_time DATETIME
		,dep_city VARCHAR(25),arr_city VARCHAR(25),dep_airport VARCHAR(25),arr_airport VARCHAR(25),dep_state VARCHAR(25),arr_state VARCHAR(25),
		max_pax INT)
	INSERT INTO #TempAircraftCal 
			SELECT
			[tail_nmbr]=F.TailNUM,
			[activedate] = PL.DepartureDTTMLocal,
			[locdep]=PL.DepartureDTTMLocal,
			[depicao_id]=AD.IcaoID,
			[locarrival] = '',---yet to be mapped
			[arricao_id]=AA.IcaoID,	
			[requestor]=PL.RequestorName,
			[dept_code]=D.DepartmentCD,
			[cat_code] = FC.FlightCatagoryCD,
			[duty_type]=PL.DutyType,
			[elp_time]=ISNULL(PL.ElapseTM,0),
			[timeapprox]=ISNULL(PL.IsApproxTM,0),
			[pax_total]=ISNULL(PL.PassengerTotal,0),
			[notes] = '',---yet to be mapped
			[Additional_crew] = '',---yet to be mapped
			[ron]='FALSE',---(CASE WHEN PL.DepartureDTTMLocal < PL.ArrivalDTTMLocal THEN 'FALSE' END),
			[pic_code]=SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1),  --TO VERIFY
			[sic_code]=SUBSTRING(SIC.CrewCodes,1,LEN(SIC.CrewCodes)-1), --TO VERIFY
			[orig_nmbr]= ISNULL(PM.TripNUM,0),
			[leg_num]= ISNULL(PL.LegNUM,0),
			[legid]= ISNULL(PL.LegID,0),
			[type_code]=AT.AircraftCD,
			[vendcode]=V.VendorCD,
			[desc]=PM.TripDescription,
			[rec_type]=PM.RecordType,
			[grndtime]='FALSE',
			[TrueRon]='FALSE', -- to be changed
			[dayfill]='FALSE',
			[nextloc]=PL.NextLocalDTTM,
			[ac_code]=F.AircraftCD,
			[auth_code]=DA.AuthorizationCD,
			[resv_avail]= ISNULL(PL.ReservationAvailable,0),
			[purpose]=PL.FlightPurpose,
			[private]=PL.IsPrivate,
			[ordernum] = '99999',
			[aircraft_code]=F.TypeDescription , --TO VERIFY
			[maxpax]=ISNULL(F.MaximumPassenger,0),
			[dep_time]=PL.DepartureDTTMLocal,
			[arr_time]=PL.ArrivalDTTMLocal,		
			[dep_city]=AD.CityName,
			[arr_city]=AA.CityName,
			[dep_airport]=AD.AirportName,
			[arr_airport]=AA.AirportName,
			[dep_state]=AD.StateName,
			[arr_state]=AA.StateName,
			[max_pax]=ISNULL(0,0)
			FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
				INNER JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,F.AircraftID,F.TypeDescription,
					F.MaximumPassenger,VendorID
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
					OR @FleetGroupCD = '')
					AND F.IsDeleted=0
					AND F.IsInActive=0
                ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
            INNER JOIN (SELECT AirportID, IcaoID,CityName,AirportName,StateName FROM Airport) AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN (SELECT AirportID, IcaoID,CityName,AirportName,StateName FROM Airport) AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN( SELECT AircraftID,AircraftCD FROM Aircraft) AT ON AT.AircraftID=F.AircraftID
			LEFT OUTER JOIN(SELECT VendorID,VendorCD FROM Vendor ) V ON  V.VendorID=F.VendorID
			LEFT OUTER JOIN(SELECT DepartmentID,DepartmentCD FROM Department)D ON PL.DepartmentID=D.DepartmentID
			LEFT OUTER JOIN(SELECT AuthorizationCD,AuthorizationID FROM DepartmentAuthorization)DA ON DA.AuthorizationID=PL.AuthorizationID 
			LEFT OUTER JOIN (SELECT FlightCategoryID,FlightCatagoryCD FROM FlightCatagory) FC 
				ON PL.FlightCategoryID = FC.FlightCategoryID
		     LEFT OUTER JOIN (
				SELECT DISTINCT PC2.LegID, CrewCodes = (
				SELECT C.CrewCD + ',' 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
				FOR XML PATH('')
				 )
				FROM PreflightCrewList PC2		
			) PIC ON PL.LEGID = PIC.LEGID 	
			LEFT OUTER JOIN (
				SELECT DISTINCT PC3.LegID, CrewCodes = (
					SELECT C1.CrewCD + ','
					FROM PreflightCrewList PC1
					INNER JOIN Crew C1 ON PC1.CrewID = C1.CrewID AND PC1.CustomerID = C1.CustomerID
					WHERE PC1.LegID = PC3.LEGID AND PC1.DutyTYPE = 'S'
					FOR XML PATH('')
				 )
				FROM PreflightCrewList PC3 	
			) SIC ON PL.LegID = SIC.LegID
		WHERE PM.CustomerID =  CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		    AND PM.IsDeleted=0
			AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')
		    AND (PM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))	OR @HomeBase=0)
	        AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
		    AND (CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO))
				
--SELECT * FROM #TempAircraftCal 
	
	DECLARE	 @TEMPTODAY AS DATETIME = CONVERT(DATE, @DATEFROM)
	
	WHILE (@TEMPTODAY <=  CONVERT(DATE, @DATETO))
		BEGIN
			--INSERT INTO #TempAircraftCal (activedate) select (@TEMPTODAY)
			--WHERE NOT EXISTS(SELECT DISTINCT activedate FROM #TempAircraftCal WHERE activedate = @TEMPTODAY)		
		
			IF NOT EXISTS (SELECT DISTINCT activedate FROM #TempAircraftCal WHERE activedate = @TEMPTODAY)
			BEGIN
				INSERT INTO #TempAircraftCal (activedate) VALUES (@TEMPTODAY)
			END 
			SET @TEMPTODAY = @TEMPTODAY + 1
		END
-----------------------------------------------
SELECT [tail_nmbr],[activedate],[locdep],[depicao_id],[locarrival],[arricao_id],[requestor],[dept_code],[cat_code],[duty_type],[elp_time]=ISNULL([elp_time],0),[timeapprox]=ISNULL(timeapprox,0),
	   [pax_total]=ISNULL(pax_total,0),[notes],[Additional_crew],[ron]='FALSE',[pic_code],[sic_code],[orig_nmbr]= ISNULL([orig_nmbr],0),[leg_num]= ISNULL([leg_num],0),[legid]= ISNULL([legid],0),
	   [type_code],[vendcode],[desc],[rec_type],[grndtime]='FALSE',[TrueRon]='FALSE',[dayfill]='FALSE',[nextloc],[ac_code],[auth_code],[resv_avail]= ISNULL([resv_avail],0),
	   [purpose],[private],[ordernum] = '99999',[aircraft_code],[maxpax]=ISNULL([maxpax],0),[dep_time],[arr_time],[dep_city],[arr_city],[dep_airport],
	   [arr_airport],[dep_state],[arr_state],[max_pax]=ISNULL(0,0)
	  FROM #TempAircraftCal ORDER BY activedate,tail_nmbr
END
-- EXEC spGetReportPREAircraftCalendar2MhtmlInformation 'jwilliams_13', '2012-07-20', '2012-10-26', '', '', ''

GO


