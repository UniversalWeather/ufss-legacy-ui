IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteSubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteSubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[spGetReportCQQuoteSubInformation]
	@CQFileID VARCHAR(30),
	@QuoteNUM INT
AS 
-- =============================================
-- SPC Name:spGetReportCQQuoteSubInformation
-- Author: Aishwarya.M
-- Create date: 06 Mar 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN



SELECT  DISTINCT
		CASE WHEN CQA.IsPrintable=1 THEN  CQA.CQQuoteFeeDescription ELSE '' END StandardChargesAdditionalFees 
		,CQA.ChargeUnit ChargeUnit
		,CQA.Quantity Qunatity	
		,CASE WHEN ISNULL(CQA.Quantity,0) =0 THEN 0 ELSE ISNULL(CQA.QuoteAmount,0)/CQA.Quantity END Rate
		,ISNULL(CQA.QuoteAmount,0) QuotedAmount
		--,CM.QuoteTotal TotalQuote
		--,CM.CustomTotalQuote TotalQuote
		--,CQM.IsPrintFees IsAddlFees
		--,CQM.IsPrintSum IsSummary
		,CQA.CQQuoteAdditionalFeesID

	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID
	LEFT OUTER JOIN CQQuoteAdditionalFees CQA ON CQA.CQQuoteMainID=CQM.CQQuoteMainID
	--INNER JOIN CQQuoteDetail CQD ON CQM.CQQuoteMainID = CQD.CQQuoteMainID								
	WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)
	AND CM.QuoteNUM = @QuoteNUM
	AND CQA.OrderNUM > 3
	AND CQA.IsPrintable=1
	ORDER BY CQA.CQQuoteAdditionalFeesID
	



			
END
	
--EXEC spGetReportCQQuoteSubInformation 10099472,1		








GO


