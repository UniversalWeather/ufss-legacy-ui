IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelPurchaseSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelPurchaseSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



 
 CREATE PROCEDURE [dbo].[spGetReportPOSTFuelPurchaseSummaryInformation]      
 (      
  @UserCD AS VARCHAR(30),         
  @DATEFROM DateTime,      
  @DATETO DateTime,      
  @IcaoID varchar(30)=''    
 )      
-- =============================================      
-- Author: Mathes      
-- Create date: 31-07-2012    
-- Description: Get Postflight Fuel purchase details for Report  
-- Description: Fuel Purchase Summary - Report       
-- =============================================      
AS      
SET NOCOUNT ON      
BEGIN     
Declare @DateRange Varchar(30)
Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
    IF @IcaoID =''      
    BEGIN      
     SELECT FuelLocator.FuelLocatorCD,   
            PostflightExpense.FuelLocatorID,   
            FuelLocator.FuelLocatorDescription,   
            Airport.IcaoID,   
            dbo.GetDateFormatByUserCD(@UserCD,PostflightExpense.PurchaseDT) PurchaseDT,   
            Fleet.TailNum,   
            PostflightExpense.DispatchNUM,  
            CASE CHARINDEX(' ', Airport.AirportName, 1)
                 WHEN 0 THEN Airport.AirportName  
                 ELSE SUBSTRING(Airport.AirportName, 1, CHARINDEX(' ', Airport.AirportName, 1) - 1) End 
            As [AirportName],   
            FBO.FBOVendor,     
            ISNULL(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) FuelQTY, 
            Case Company.FuelPurchase When 1 Then 'GAL' When 2 Then 'LTR' WHEN 3 THEN 'IMP' WHEN 4 THEN 'POUNDS' ELSE 'KILOS' End As Unit, --WHEN 4 THEN 'F.' 
            Case Company.FuelPurchase When 1 Then 'Gallons' When 2 Then 'Liters' WHEN 3 THEN 'Imp Gals' WHEN 4 THEN 'Pounds' ELSE 'Kilos' End As Units,            
            ISNULL((Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End),0) As UnitPrice,
            ISNULL((Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase THEN  (PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.PostFuelPrice End),0) As PostFuelPrice, 
            ISNULL(PostflightExpense.FuelQTY * PostflightExpense.UnitPrice,0) As TotalCost,
            (PostflightExpense.FuelQTY * PostflightExpense.UnitPrice )-((PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0)) CostDiff,
            @DateRange DateRange        
       FROM  PostflightExpense 
			INNER JOIN PostflightLeg ON PostflightExpense.POLegID = PostflightLeg.POLegID AND PostflightLeg.IsDeleted = 0 
            INNER JOIN Fleet ON Fleet.FleetID = PostflightExpense.FleetID
          LEFT JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID 
            LEFT JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID LEFT OUTER JOIN  
            Airport ON PostflightExpense.AirportID = Airport.AirportID 
           -- LEFT OUTER JOIN Company ON PostflightExpense.HomebaseID = Company.HomebaseID
           INNER JOIN Company  ON Fleet.CustomerID = Company.CustomerID  AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)
          INNER JOIN UserMaster UM ON UM.HomebaseID=Company.HomebaseID AND UM.UserName=@UserCD AND UM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
     WHERE  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And  
            Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO) 
            AND PostflightExpense.IsDeleted=0     
            Order  By PostflightExpense.PurchaseDT       
     END      
    ELSE        
     BEGIN      
         SELECT FuelLocator.FuelLocatorCD,   
                PostflightExpense.FuelLocatorID,   
                FuelLocator.FuelLocatorDescription,   
                Airport.IcaoID,   
                dbo.GetDateFormatByUserCD(@UserCD,PostflightExpense.PurchaseDT) PurchaseDT,   
                Fleet.TailNum,   
                PostflightExpense.DispatchNUM,   
                           CASE CHARINDEX(' ', Airport.AirportName, 1)
                 WHEN 0 THEN Airport.AirportName  
                 ELSE SUBSTRING(Airport.AirportName, 1, CHARINDEX(' ', Airport.AirportName, 1) - 1) End 
            As [AirportName],   
                FBO.FBOVendor,                                                    
                DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD) FuelQTY, 
                Case Company.FuelPurchase When 1 Then 'GAL' When 2 Then 'LTR' WHEN 3 THEN 'IMP' WHEN 4 THEN 'F.' End As Unit,  
                Case Company.FuelPurchase When 1 Then 'Gallons' When 2 Then 'Liters' WHEN 3 THEN 'Imp Gals' WHEN 4 THEN ' ' End As Units,            
                Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End As UnitPrice,
                Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase THEN  (PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.PostFuelPrice End As PostFuelPrice,
                PostflightExpense.FuelQTY * PostflightExpense.UnitPrice As TotalCost,
               (PostflightExpense.FuelQTY * PostflightExpense.UnitPrice )-(PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) CostDiff,
                @DateRange DateRange                                          
          FROM  PostflightExpense
			INNER JOIN PostflightLeg ON PostflightExpense.POLegID = PostflightLeg.POLegID AND PostflightLeg.IsDeleted = 0 
            INNER JOIN Fleet ON Fleet.FleetID = PostflightExpense.FleetID
            LEFT JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID 
            LEFT JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID LEFT OUTER JOIN  
            Airport ON PostflightExpense.AirportID = Airport.AirportID 
           --- LEFT OUTER JOIN Company ON PostflightExpense.HomebaseID = Company.HomebaseID
            INNER JOIN Company  ON Fleet.CustomerID = Company.CustomerID  AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)
            INNER JOIN UserMaster UM ON UM.HomebaseID=Company.HomebaseID AND UM.UserName=@UserCD AND UM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
        WHERE   PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And      
                Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO)      
                AND Airport.IcaoID=@IcaoID  AND PostflightExpense.IsDeleted=0        
                Order By PostflightExpense.PurchaseDT       
      END      
                 
END 
--Exec spGetReportPOSTFuelPurchaseSummaryInformation 'jwilliams_13','01-Jan-2000','31-Dec-2012'


GO


