IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vCQPostFlight]'))
DROP VIEW [dbo].[vCQPostFlight]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vCQPostFlight]  AS 
SELECT CF.CQCustomerName
      ,CC.CQCustomerCD
      ,LS.LeadSourceCD
      ,LS.LeadSourceDescription
      ,CF.FileNUM
      ,CM.QuoteNUM
      ,F.TailNum
      ,PL.LegNUM
      ,AA.IcaoID AIcaoID
      ,AD.IcaoID DIcaoID
      ,PL.InboundDTTM
      ,PL.OutboundDTTM
      ,PL.PassengerTotal
      ,LS.LeadSourceID
      ,CC.CQCustomerID
      ,CM.CQMainID 
      ,CF.CustomerID
      ,CF.EstDepartureDT
      ,CL.CQLegID
      ,CM.TripID

         FROM CQFile CF INNER JOIN CQMain CM ON CM.CQFileID = CF.CQFileID
                        INNER JOIN LeadSource LS ON LS.LeadSourceID = CF.LeadSourceID
                        INNER JOIN CQLeg CL ON CL.CQMainID = CM.CQMainID
                        INNER JOIN PostflightMain PM ON PM.TripID=CM.TripID
                        INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
                        INNER JOIN Fleet F ON F.FleetID = CM.FleetID
                        LEFT OUTER  JOIN  Airport AD ON AD.AirportID = PL.DepartICAOID
                        LEFT OUTER  JOIN  Airport AA ON AA.AirportID = PL.ArriveICAOID
                        LEFT OUTER JOIN CQCustomer CC ON CC.CQCustomerID = CF.CQCustomerID
         WHERE CF.IsDeleted=0

GO


