IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFlightLogPreviewInformationHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewInformationHeader]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSFlightLogPreviewInformationHeader] 
	 @UserCD  AS VARCHAR(30)     
    ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
    ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
    ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
    ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
    ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal      
    ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
    ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
    ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
    ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
    ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
    ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
    ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
    ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
    ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
    ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
    ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
    ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
    ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
    ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S' 
AS
BEGIN

	DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

	 -- [Start] Report Criteria --  
    DECLARE @tblTripInfo AS TABLE (  
    TripID BIGINT  
    , LegID BIGINT  
    , PassengerID BIGINT  
    )  
  

    INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
    EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
        @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
        @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
        @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
IF  EXISTS (SELECT FLEETID FROM  PREFLIGHTMAIN WHERE TRIPNUM = @TRIPNUM AND  FLEETID IS NOT NULL )
	SELECT			[TripNumber] = PM.TripNUM 
					,[Description] = PM.TripDescription
					,[TripDates] = dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID,@UserCD)
					,[Requestor] = ISNULL(P.LastName+ ', ','') +ISNULL(P.FirstName,'')+ ' ' +ISNULL(P.MiddleInitial,'')
					,[TailNumber] =F.TailNum
					,[ContactPhone] = P.PhoneNum
					,[DispatchNum] = PM.DispatchNUM 
					,[Type] =  A.AircraftCD
					,PM.TripID
					,DispatcherName = ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'')
					,DispatcherPhone = UM.PhoneNum
					,Dispatcheremail = UM.EmailID
					,PM.FlightNUM

					FROM (SELECT DISTINCT TripID FROM @tblTripInfo)M
					INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
					INNER JOIN Fleet F ON  PM.FleetID = F.FleetID
					INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
					LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
					LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)				
					WHERE PM.CustomerID = @CustomerID
ELSE

SELECT			[TripNumber] = PM.TripNUM 
					,[Description] = PM.TripDescription
					,[TripDates] = dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID,@UserCD)
					,[Requestor] = ISNULL(P.LastName+ ', ','') +ISNULL(P.FirstName,'')+ ' ' +ISNULL(P.MiddleInitial,'')
					,[TailNumber] =''
					,[ContactPhone] = P.PhoneNum
					,[DispatchNum] = PM.DispatchNUM 
					,[Type] =  A.AircraftCD
					,PM.TripID
					,DispatcherName = ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'')
					,DispatcherPhone = UM.PhoneNum
					,Dispatcheremail = UM.EmailID

					FROM (SELECT DISTINCT TripID FROM @tblTripInfo)M
					INNER JOIN PreflightMain PM ON M.TripID = PM.TripID					
					INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
					LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
					LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)				
					WHERE PM.CustomerID = @CustomerID
			
END

GO