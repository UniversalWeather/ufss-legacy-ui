IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportAirportInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =========================================================================
-- SPC Name: spGetReportAirportInformation
-- Author: SUDHAKAR J
-- Create date: 12 Jun 2012
-- Description: Get Airport Information for Report
-- Revision History
-- Date			Name		Ver		Change
-- 27 Jun 2012	Sudhakar.J	1.1		Added Alerts and GeneralNotes
-- =========================================================================

SET NOCOUNT ON

	SELECT TOP 1
		IcaoID
		--Combination of  Lat Degee/Minutes and N/S
		,LatitudeDegree
		,LatitudeMinutes
		,LatitudeNorthSouth
		,OffsetToGMT
		,TowerFreq
		,CityName
		--Combination of Lon Degee/Minutes and E/W
		,LongitudeDegrees
		,LongitudeMinutes
		,LongitudeEastWest
		,IsDayLightSaving
		,ATISFreq
		,StateName
		,[MagneticVariance] = CONVERT(VARCHAR,MagneticVariance) + ' ' + VarianceEastWest
		--See the comments
		,ARINCFreq
		,[CountryCD] = dbo.GetCountryCDByCountryID(CountryID)
		,Elevation
		--See the comments
		,GroundFreq
		,AirportName
		,LongestRunway
		,ApproachFreq
		,IsUSTax
		,IsAirport
		,IsEntryPort
		,DepartFreq
		,IsRural
		,IsHeliport
		,TakeoffBIAS
		,LandingBIAS
		,AirportInfo
		,Alerts 
		,GeneralNotes
		,Label1Freq
		,Freq1
		,Label2Freq
		,Freq2
		,CONVERT(VARCHAR(5), DayLightSavingStartDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) + '  ' + CONVERT(VARCHAR(5),DaylLightSavingStartTM) AS StartUTC
		,CONVERT(VARCHAR(5), DayLightSavingEndDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) + '  ' + CONVERT(VARCHAR(5),DayLightSavingEndTM) AS FinishUTC
	FROM Airport WHERE IsDeleted = 0
	AND ( CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) OR CustomerID = dbo.GetUWACustomerID() )
	AND IcaoID = @IcaoID
	
	ORDER BY CustomerID DESC
	
  -- EXEC spGetReportAirportInformation 'SUPERVISOR_99', '0J0'


GO


