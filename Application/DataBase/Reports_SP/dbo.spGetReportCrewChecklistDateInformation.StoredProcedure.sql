IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewChecklistDateInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewChecklistDateInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportCrewChecklistDateInformation]
	@UserCD varchar(30),
	@CrewCD char(7),
	@IsInActive BIT = 0
AS
-- =============================================
-- SPC Name: spGetReportCrewChecklistDateInformation
-- Author: SUDHAKAR J
-- Create date: 14 Jun 2012
-- Description: Get Crew Checklist Date Information for Report, Crew Roaster
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================

SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
--SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--dbo.GetDateFormatByUserCD(@UserCD,T.[Date]) 

	SELECT DISTINCT
		CCD.CustomerID
		,C.CrewCD
		,CCD.CheckListCD 
		,CC.CrewChecklistDescription --Checklist Item Description
		,A.AircraftCD
		,dbo.GetDateFormatByUserCD(@UserCD, CCD.PreviousCheckDT) AS PreviousCheckDT
		,dbo.GetDateFormatByUserCD(@UserCD, CCD.DueDT) AS DueDT
		,dbo.GetDateFormatByUserCD(@UserCD, CCD.AlertDT) AS AlertDT
		,dbo.GetDateFormatByUserCD(@UserCD, CCD.GraceDT) AS GraceDT
/*
		,CONVERT(varchar(10), CCD.PreviousCheckDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS PreviousCheckDT
		,CONVERT(varchar(10), CCD.DueDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS DueDT
		,CONVERT(varchar(10), CCD.AlertDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS AlertDT
		,CONVERT(varchar(10), CCD.GraceDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS GraceDT
*/
		--Expired: if Current System Date is greater than Due Next and Grace Date 
		, [IsExpired] = CASE 
							  WHEN GETDATE() > CCD.DueDT AND GETDATE() > CCD.GraceDT THEN '1' 
							  ELSE '0' 
						 END
		,[TenToMin] = @TenToMin				  
	FROM CrewCheckListDetail CCD 
		INNER JOIN (
			SELECT CrewCheckCD, CrewChecklistDescription, CustomerID FROM CrewChecklist
		) CC ON CCD.CheckListCD = CC.CrewCheckCD AND CCD.CustomerID = CC.CustomerID
		INNER JOIN (
			SELECT CrewID, CrewCD, ClientID, CustomerID FROM Crew
		) C ON CCD.CrewID = C.CrewID AND CCD.CustomerID = C.CustomerID
		LEFT OUTER JOIN (
		SELECT AircraftID, AircraftCD FROM Aircraft
		) A ON CCD.AircraftID = A.AircraftID
	WHERE CCD.IsDeleted = 0
		AND CCD.CustomerID = @CUSTOMERID
		--AND C.ClientID = ISNULL(@CLIENTID, C.ClientID)
		AND C.CrewCD = RTRIM(@CrewCD)
		--@IsInActive: Flag to include Inactive records
		AND (CCD.IsInActive = 0 OR CCD.IsInActive = @IsInActive)
	ORDER BY CCD.CustomerID,C.CrewCD,CCD.CheckListCD 		
  -- EXEC spGetReportCrewChecklistDateInformation 'jwilliams_11', '1GV'








GO


