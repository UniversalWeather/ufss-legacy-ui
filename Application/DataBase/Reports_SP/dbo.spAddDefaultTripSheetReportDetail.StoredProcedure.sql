IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddDefaultTripSheetReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddDefaultTripSheetReportDetail]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddDefaultTripSheetReportDetail]
(@CustomerID bigint,
@TripSheetReportHeaderID bigint)
AS BEGIN
			DECLARE @TripSheetReportDetailID BIGINT
			DECLARE @TMPTripSheetReportDetailID BIGINT
			DECLARE @tblTripSheetReportDetail TABLE
			(
				TripSheetReportDetailID bigint,
				CustomerID bigint,
				TripSheetReportHeaderID bigint,
				ReportNumber int,
				ReportDescription varchar(35),
				IsSelected bit,
				Copies int,
				ReportProcedure varchar(8),
				ParameterVariable varchar(10),
				ParameterDescription varchar(100),
				ParameterType char(1),
				ParameterWidth int,
				ParameterCValue varchar(60),
				ParameterLValue bit,
				ParameterNValue int,
				ParameterValue varchar(MAX),
				ReportOrder int,
				TripSheetOrder int,
				LastUpdUID varchar(30),
				LastUpdTS datetime,
				IsDeleted bit,
				ParameterSize varchar(10),
				WarningMessage varchar(100)
			)
			
			INSERT INTO @tblTripSheetReportDetail 
			(TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,
				Copies,ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,ParameterCValue,
				ParameterLValue,ParameterNValue,ParameterValue,ReportOrder,TripSheetOrder,LastUpdUID,LastUpdTS,IsDeleted,
				ParameterSize,WarningMessage)
			
			SELECT TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,
					Copies,ReportProcedure,ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,ParameterCValue,
					ParameterLValue,ParameterNValue,ParameterValue,ReportOrder,TripSheetOrder,LastUpdUID,LastUpdTS,IsDeleted,
					ParameterSize,WarningMessage
				FROM TripSheetReportDetail
				WHERE CustomerID = @CustomerID AND TripSheetReportHeaderID = (SELECT TOP 1 TripSheetReportHeaderID FROM TripSheetReportHeader WHERE CustomerID = @CustomerID AND ReportID = 'DEFAULT')

			WHILE (SELECT Count(*) from @tblTripSheetReportDetail)>0
				BEGIN
					
					SELECT TOP 1 @TMPTripSheetReportDetailID = TripSheetReportDetailID FROM @tblTripSheetReportDetail 
							ORDER BY TripSheetReportDetailID ASC
				
				
					EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT
	
						INSERT INTO TripSheetReportDetail
							   ([TripSheetReportDetailID]
							   ,[CustomerID]
							   ,[TripSheetReportHeaderID]
							   ,[ReportNumber]
							   ,[ReportDescription]
							   ,[IsSelected]
							   ,[Copies]
							   ,[ReportProcedure]
							   ,[ParameterVariable]
							   ,[ParameterDescription]
							   ,[ParameterType]
							   ,[ParameterWidth]
							   ,[ParameterCValue]
							   ,[ParameterLValue]
							   ,[ParameterNValue]
							   ,[ParameterValue]
							   ,[ReportOrder]
							   ,[TripSheetOrder]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsDeleted]
							   ,[ParameterSize]
							   ,[WarningMessage])
							
							SELECT @TripSheetReportDetailID
							   ,[CustomerID]
							   ,@TripSheetReportHeaderID
							   ,[ReportNumber]
							   ,[ReportDescription]
							   ,[IsSelected]
							   ,[Copies]
							   ,[ReportProcedure]
							   ,[ParameterVariable]
							   ,[ParameterDescription]
							   ,[ParameterType]
							   ,[ParameterWidth]
							   ,[ParameterCValue]
							   ,[ParameterLValue]
							   ,[ParameterNValue]
							   ,[ParameterValue]
							   ,[ReportOrder]
							   ,[TripSheetOrder]
							   ,[LastUpdUID]
							   ,[LastUpdTS]
							   ,[IsDeleted]							   
							   ,[ParameterSize]
							   ,[WarningMessage]
							   FROM @tblTripSheetReportDetail
							   WHERE TripSheetReportDetailID=@TMPTripSheetReportDetailID
							   
							   DELETE FROM @tblTripSheetReportDetail WHERE TripSheetReportDetailID=@TMPTripSheetReportDetailID
				   				
				END
					

END

GO

