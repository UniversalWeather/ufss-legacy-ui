IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPaxProfileExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPaxProfileExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPaxProfileExport] 
     @UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSPaxProfileExport
-- Author: ABHISHEK.S
-- Create date: 11 September 2012
-- Description: Get TripSheetReportWriter Passenger Profile Export Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
BEGIN

    -- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --

	SET NOCOUNT ON;
	
	DECLARE @SQLSCRIPT AS NVARCHAR(MAX);
	DECLARE @ParameterDefinition AS NVARCHAR(500);

SELECT * FROM (	

	SELECT DISTINCT
	[locdate]   =	dbo.GetShortDateFormatByUserCD(@UserCD,PLTrip.Departure), 
    [highdate]  =   dbo.GetShortDateFormatByUserCD(@UserCD,PLTrip.Arrive),
    [orig_nmbr] =	PM.TripNUM,
    [tail_nmbr] =	F.TailNum,
    [type_code] =	A.AircraftCD,
    [desc]      =	PM.TripDescription,
    [reqcode]   =	PR.PassengerRequestorCD,
    [paxname]   =	PR.LastName + ',' + PR.FirstName + ' ' + PR.MiddleInitial,
    [phone]     =	PR.PhoneNum,
    [paxcode]   =	PR1.PassengerRequestorCD,
    [paxname2]  =	CASE  WHEN (LEN(ISNULL(PR1.PassengerRequestorCD, '')) > 0) THEN '' ELSE (PR1.LastName + ',' + PR1.FirstName + ' ' + PR1.MiddleInitial) END,
    [last_name] =	PR1.LastName,
    [first_name]=	PR1.FirstName,
    [Notes] = PM.Notes,
    [adcd]      =	PA.AdditionalINFOCD,
    [adcddesc]	=	PA.AdditionalINFODescription,
	[adcdvalue]	=	ISNULL(PA.AdditionalINFOValue,''),
	--[adcdvalue] =   dbo.FlightPakDecrypt(ISNULL(PA.AdditionalINFOValue,'')),
    [section]	=   1	-- (CASE WHEN PA.AdditionalINFOCD IS NULL THEN 0 ELSE 1 END)
    
    FROM (SELECT DISTINCT TripID, LegID, PassengerID FROM  @tblTripInfo) Main 
                INNER JOIN PREFLIGHTMAIN PM
                        ON Main.TripID = PM.TripID
				INNER JOIN PreflightLeg PL
				        ON Main.LegID = PL.LegID  
				INNER JOIN (SELECT CustomerID,TripID,TripNUM,MIN(DepartureDTTMLocal) Departure,MAX(ArrivalDTTMLocal) Arrive FROM  PreflightLeg GROUP BY CustomerID,TripID,TripNUM)PLTrip ON PLTrip.TripID=PM.TripID AND PM.CustomerID=PLTrip.CustomerID 
				INNER JOIN (SELECT DISTINCT TailNum,FleetID FROM FLEET) F 
				        ON PM.FleetID = F.FleetID
				INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
				 LEFT JOIN Passenger PR 
				        ON PM.PassengerRequestorID = PR.PassengerRequestorID				
				--INNER JOIN PreflightPassengerList PLI ON PL.LegID = PLI.LegID
				INNER JOIN Passenger PR1 
				        ON Main.PassengerID = PR1.PassengerRequestorID
				 left outer JOIN PassengerAdditionalInfo PA 
				        ON PR1.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted = 0
	WHERE PM.IsDeleted = 0
	 
	UNION ALL

	SELECT DISTINCT
	[locdate]   =	dbo.GetShortDateFormatByUserCD(@UserCD,PLTrip.Departure),
    [highdate]  =   dbo.GetShortDateFormatByUserCD(@UserCD,PLTrip.Arrive),
    [orig_nmbr] =	PM.TripNUM,
    [tail_nmbr] =	F.TailNum,
    [type_code] =	A.AircraftCD,
    [desc]      =	PM.TripDescription,
    [reqcode]   =	PR.PassengerRequestorCD,
    [paxname]   =	PR.LastName + ',' + PR.FirstName + ' ' + PR.MiddleInitial,
    [phone]     =	PR.PhoneNum,
    [paxcode]   =	PR1.PassengerRequestorCD,
    [paxname2]  =	CASE  WHEN (LEN(ISNULL(PR1.PassengerRequestorCD, '')) > 0) THEN '' ELSE (PR1.LastName + ',' + PR1.FirstName + ' ' + PR1.MiddleInitial) END,
    [last_name] =	PR1.LastName,
    [first_name]=	PR1.FirstName,
    [Notes] = '',
    [adcd]      =	'',
    [adcddesc]	=	'',
  --[adcdvalue]	=	PA.AdditionalINFOValue,
	[adcdvalue] =   '',
    [section]	=	0
    
    FROM (SELECT DISTINCT TripID, LegID, PassengerID FROM  @tblTripInfo) Main 
                INNER JOIN PREFLIGHTMAIN PM
                        ON Main.TripID = PM.TripID
				INNER JOIN PreflightLeg PL
				        ON Main.LegID = PL.LegID  
				INNER JOIN (SELECT CustomerID,TripID,TripNUM,MIN(DepartureDTTMLocal) Departure,MAX(ArrivalDTTMLocal) Arrive FROM  PreflightLeg GROUP BY CustomerID,TripID,TripNUM)PLTrip ON PLTrip.TripID=PM.TripID AND PM.CustomerID=PLTrip.CustomerID 
				INNER JOIN (SELECT DISTINCT TailNum,FleetID FROM FLEET) F 
				        ON PM.FleetID = F.FleetID
				INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
				 LEFT JOIN Passenger PR 
				        ON PM.PassengerRequestorID = PR.PassengerRequestorID				
				--INNER JOIN PreflightPassengerList PLI ON PL.LegID = PLI.LegID
				INNER JOIN Passenger PR1 
				        ON Main.PassengerID = PR1.PassengerRequestorID
	WHERE PM.IsDeleted = 0	 
) X	
ORDER BY X.orig_nmbr, X.highdate, X.paxcode, X.section
	  --Construct OPTIONAL clauses
	  
	--IF @Passenger <> '' BEGIN
	--  SET @SQLSCRIPT = @SQLSCRIPT + 'AND PR.PassengerRequestorCD IN (''' + REPLACE(CASE WHEN RIGHT(@Passenger, 1) = ',' THEN LEFT(@Passenger, LEN(@Passenger) - 1) ELSE @Passenger END, ',', ''', ''') + ''')';	
	--END	
		
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@TripNum AS NVARCHAR(100),@Passenger AS NVARCHAR(200)'
 --   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TripNum, @Passenger
    
   --EXEC spGetReportPRETSPaxProfileExport 'SUPERVISOR_99', 1992, ''

  
END






GO


