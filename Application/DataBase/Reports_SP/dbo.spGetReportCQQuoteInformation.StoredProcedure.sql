IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQQuoteInformation](@UserCD VARCHAR(30)
                                              ,@UserCustomerID AS BIGINT
											  ,@DateFrom DATETIME
											  ,@DateTo DATETIME
                                              --,@QuoteStatus VARCHAR(20)
											  ,@SalesPersonCD VARCHAR(200) = ''
                                              ,@CQCustomerCD VARCHAR(200) = ''
                                              )

AS
BEGIN
-- =============================================
-- SPC Name: spGetReportCQQuoteInformation
-- Author: SINDHUJA.K
-- Create date: 28 March 2013
-- Description: Get Charter Quote Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================


SELECT CQF.QuoteDT,
       CQC.CQCustomerCD CustomerCD,
       CQC.CQCustomerName CustomerName,
       SP.SalesPersonCD,
       CM.QuoteNUM,
       CASE WHEN (CM.IsAccepted=1) THEN 'ACCEPTED' 
			WHEN CM.IsSubmitted = 1 THEN 'SUBMITTED' ELSE '' END QuoteStatus,
       CASE WHEN CM.IsAccepted=1 THEN CM.LastAcceptDT ELSE CM.LastSubmitDT END	QuoteStatusDate,
       CQL.CQLostBusinessCD LostBusinessCode,
       CM.QuoteTotal
 FROM CQMain CM 
       INNER JOIN CQFile CQF ON CM.CQFileID=CQF.CQFileID
	   INNER JOIN CQCustomer CQC ON CQC.CQCustomerID=CQF.CQCustomerID
	   LEFT OUTER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID
	   LEFT OUTER JOIN CQLostBusiness CQL ON CQL.CQLostBusinessID=CM.CQLostBusinessID
 WHERE CQF.QuoteDT BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
   AND CM.IsDeleted=0
   --AND CM.IsAccepted=(CASE WHEN @QuoteStatus='Accepted' THEN 1 ELSE 0 END)
   --AND CM.IsSubmitted=(CASE WHEN @QuoteStatus='Submitted' THEN 1 ELSE 0 END)
   AND (SP.SalesPersonCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD='') 
   AND (CQC.CQCustomerCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='') 
   AND CM.TripID IS NULL
   AND CM.CustomerID =@UserCustomerID
   
   ORDER BY CQF.QuoteDT,  CM.QuoteNUM


 
END
 
------EXEC spGetReportCQQuoteInformation 'SUPERVISOR_99',10099,'2013-02-02','2013-05-05','5555','123'

GO


