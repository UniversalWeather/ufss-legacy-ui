IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCharterQuoteCustomerExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCharterQuoteCustomerExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE  [dbo].[spGetReportDBCharterQuoteCustomerExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
       ,@UserCustomerID AS VARCHAR(30)
       ,@CQCustomerCD AS VARCHAR(500)
)
AS
BEGIN
 

-- ===============================================================================
-- SPC Name: spGetReportDBCharterQuoteCustomerExportInformation
-- Author: Askar R
-- Create date: 2 Feb 20134-- Description: Get Customer Additional Information
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT DISTINCT name_a=CQC.CQCustomerName
			   ,vendtype='C'
			   ,vendcode=CQC.CQCustomerCD
			   ,b_name=CQC.BillingName
			   ,b_addr1=CQC.BillingAddr1
			   ,b_addr2=CQC.BillingAddr2
			   ,b_city=CQC.BillingCity
			   ,b_state=CQC.BillingState
			   ,b_zip=CQC.BillingZip
			   ,b_country=CO.CountryCD
			   ,b_phone=CQC.BillingPhoneNum
			   ,b_fax=CQC.BillingFaxNum
			   ,NOTES_A=CQC.Notes
			   ,credit=CQC.Credit
			   ,homebase=A.IcaoID
			   ,icao_id=CT.IcaoID
			   ,appfiled=CQC.IsApplicationFiled
			   ,approve=CQC.IsApproved
			   ,discpct=DiscountPercentage
			   ,active=CASE WHEN CQC.IsInActive=0 THEN 'TRUE' ELSE 'FALSE' END
			   ,contact=CQCC.CQCustomerContactCD
			   ,vcname=ContactName
			   ,choice=CASE WHEN CQCC.IsChoice=1 THEN 'TRUE' ELSE 'FALSE' END
			   ,name_b= ISNULL(CQCC.LastName,'') +CASE WHEN CQCC.LastName<>'' THEN ', '+ISNULL(CQCC.FirstName,'') ELSE ISNULL(CQCC.FirstName,'') END + ' '+ISNULL(MiddleName,'')
			   ,addr1=CQCC.Addr1
			   ,addr2=CQCC.Addr2
			   ,city=CQCC.CityName
			   ,state=CQCC.StateName
			   ,zip=CQCC.PostalZipCD
			   ,country=CR.CountryCD
			   ,phone=CQCC.PhoneNum
			   ,fax=CQCC.FaxNum
			   ,MoreInfo
			   ,crname1=CQCC.CreditName1
			   ,crnum1=CQCC.CreditNum1
			   ,crname2=CQCC.CreditName2
			   ,crnum2=CQCC.CreditNum2
        FROM CQCustomer CQC INNER JOIN CQCustomerContact CQCC ON CQC.CQCustomerID=CQCC.CQCustomerID
                            LEFT OUTER JOIN Company C ON CQC.HomebaseID=C.HomebaseID
                            LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
                            LEFT OUTER JOIN Airport CT ON CQC.AirportID=CT.AirportID
                            LEFT OUTER JOIN Country CO ON CO.CountryID=CQC.CountryID
                            LEFT OUTER JOIN Country CR ON CR.CountryID=CQCC.CountryID
       WHERE CQC.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
         AND CQC.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
         --And CQC.IsInActive=0
         And CQC.IsDeleted=0
         AND CQCC.IsDeleted=0
        ORDER BY CQC.CQCustomerCD, ISNULL(CQCC.LastName,'') +CASE WHEN CQCC.LastName<>'' THEN ', '+ISNULL(CQCC.FirstName,'') ELSE ISNULL(CQCC.FirstName,'') END + ' '+ISNULL(MiddleName,'')
			
		 --AND CQCC.IsChoice=1

END



--EXEC spGetReportDBCharterQuoteCustomerExportInformation 'SUPERVISOR_99',10099,''





GO


