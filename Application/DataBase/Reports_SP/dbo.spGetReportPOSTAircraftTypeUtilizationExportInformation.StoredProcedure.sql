
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftTypeUtilizationExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftTypeUtilizationExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[spGetReportPOSTAircraftTypeUtilizationExportInformation]  
@UserCD AS VARCHAR(30), --Mandatory      
@DATEFROM AS DATETIME, --Mandatory      
@DATETO AS DATETIME, --Mandatory      
@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values      
@HomeBaseCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values      
 @IsHomebase As bit=0     
AS      
      
-- ===============================================================================      
-- SPC Name: spGetReportPOSTAircraftTypeUtilization      
-- Author:  PremPrakash.S      
-- Create date: 6 Jul 2012      
-- Description: Get Aircraft Type Utilization      
-- spGetReportPOSTAircraftTypeUtilizationExportInformation 'UC','12/12/2010','12/12/2012','DA50,ACD,TEST3','fgh,DDF',1      
--spGetReportPOSTAircraftTypeUtilizationExportInformation 'jwilliams_13','8/1/2010','8/10/2012','',''      
-- Revision History      
-- Date   Name  Ver  Change      
--       
-- ================================================================================      
      
SET NOCOUNT ON      
   
   if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END
     
  Declare @TenToMin Int
 Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
       
  declare @units varchar(15)
 set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'LTR'
				WHEN (cp.FuelPurchase='3') THEN 'IMP GAL'
				WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
       
 SELECT A.typecode,A.logid,A.NoOfTrip,A.NoofLogs,SUM(A.NoofLegs) as NoofLegs,SUM(A.BlockHours) as BlockHours,SUM(A.FlightHours) as FlightHours,SUM(A.NautMiles) as NautMiles,
 SUM(A.NoofPax) as NoofPax,SUM(A.FuelBurn) as FuelBurn,SUM(A.Gallons) as Gallons,SUM(A.TotalCost) as TotalCost,a.TenToMin,A.Units,SUM(A.AvgPrice) AS AvgPrice,SUM(a.distance) distance,sum(a.fuelqty) fuelqty
 FROM
(select      
[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
  ,[typecode]=Act.AircraftCD              
  ,[NoOfTrip]=(case when PFM.TripID is null then COUNT(distinct PFM.LogNUM) else COUNT(distinct PFM.TripID) end)
  ,[NoofLogs]=COUNT(distinct PFM.LogNUM)
  ,[logid]=PFM.POLogID                   
  ,[NoofLegs]=COUNT(pfl.legnum)
  ,[BlockHours]=ISNULL(ROUND(PFL.BlockHours,1),0)
  ,[FlightHours]=ISNULL(ROUND(PFL.FlightHours,1),0)             
  ,[NautMiles]=isnull(pfl.Distance,0) 
  ,[NoofPax]=isnull(pfl.PassengerTotal,0)             
  ,[FuelBurn]=isnull(pfl.FuelUsed,0)             
  ,[Gallons]=case when (PFE.PurchaseDT>= @DATEFROM or PFE.PurchaseDT<=@DATETO) then (CAST(ROUND((dbo.GetFuelConversion(PFE.FuelPurchase,PFE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))) end
  ,[TotalCost]=isnull(PFE.ExpenseAMT,0)     
  ,[TenToMin]=@TenToMin
  ,[Units] =@units
   ,[AvgPrice]=  Case WHEN Cp.FuelPurchase <> PFE.FuelPurchase and (ISNULL(pFe.FuelQTY,0))<>0 THEN DBO.GetFuelPriceConversion(PFE.FuelPurchase,ISNULL(pFe.ExpenseAMT,0)/pFe.FuelQTY,@UserCD)
                                               ELSE PFe.UnitPrice End
  ,[distance]=pfl.Distance
  ,[fuelqty]=PFE.FuelQTY
   
   from postflightmain PFM        
              
         
INNER JOIN  FLEET FLT on PFM.FleetID = flt.FleetID              
INNER JOIN PostflightLeg pfl on  pfl.POLogID=PFM.POLogID AND pfl.IsDeleted=0            
INNER JOIN Aircraft Act on Act.AircraftID = PFM.AircraftID
LEFT JOIN PostflightExpense PFE on PFE.POLogID = pfl.POLogID AND PFE.IsDeleted=0   
LEFT OUTER JOIN Company cp ON cp.HomebaseID = PFM.HomebaseID          
LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
left outer join UserMaster um on um.HomebaseID=pfm.HomebaseID and um.CustomerID=PfM.CustomerID
and um.UserName= @UserCD
where  PFM.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  and
 CONVERT(DATE,PFL.ScheduleDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
 AND PFM.IsDeleted=0
 AND (ACt.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
 AND (PfM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
 AND A.IcaoID IN 
	(	
		SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE A.IcaoID END, ',')
	
	) 
		
 --AND (A.IcaoID IN (CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE A.IcaoID END) or @HomeBaseCD='')
	
 
 --AND (cp.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @HomeBaseCDID = 0)
 --AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCDCD, ',')) OR @HomeBaseCDCD = '')
     GROUP BY Act.AircraftCD,pfl.BlockHours,pfl.FlightHours,pfl.Distance,pfl.PassengerTotal,pfl.FuelUsed
              ,PFE.FuelQTY,PFE.ExpenseAMT,cp.FuelPurchase,PFM.POLogID,pfe.ExpenseAMT,pfe.FuelQTY,PFE.FuelPurchase,PFE.PurchaseDT
              ,PFE.UnitPrice,PFM.TripID, pfl.LegNUM,pfl.Distance,PFE.FuelQTY)A

GROUP BY A.typecode,A.logid,A.NoofLogs,A.NoofTrip,a.TenToMin,A.DateRange,A.Units

GO


