IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPAXNoFlyInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation]
	@UserCD VARCHAR(50)--Mandatory
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum AS VARCHAR(150) = ''
	,@PassengerCD AS VARCHAR(300) = ''
	
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSPAXNoFlyInformation
-- Author: AISHWARYA.M
-- Create date: 30 Aug 2012
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- =================================================================================
SET NOCOUNT ON

IF EXISTS(
SELECT PL.TripID FROM PreflightLeg PL 
					  INNER JOIN PreflightPassengerList PPL ON PL.LegID=PPL.LegID
				 WHERE PL.TripID=CONVERT(BIGINT,@TripID) 
				   AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  

)
BEGIN
	SELECT [LegID] = CONVERT(VARCHAR,PL.LegID)
		   ,[PassengerName] = P.LastName + ',' + P.FirstName + ' ' + ISNULL(P.MiddleInitial,'')
		   ,[Code] = CONVERT(VARCHAR(MAX),P.PassengerRequestorCD)
		   ,[Status] = Case PP.TripStatus 
			               WHEN 'R' THEN 'Restricted'
						   WHEN 'C' THEN 'Cleared' 
						   WHEN 'S' THEN 'Selected'
						   ELSE 'N/A' 
						   END
		   ,[Nbr] =  ROW_NUMBER() over (order by PP.PassengerID, PL.LegNum)
		   ,[Cng] = P.IsEmployeeType
		   ,[Dept] = CONVERT(VARCHAR(MAX),D.DepartmentCD)
		   ,[Phone] = P.PhoneNum 
		   ,[AddlPhone] = P.AdditionalPhoneNum
		   ,[PPNum] = ISNULL(CPP.PassportNUM,'')
		   --,[PPNum] = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNUM,''))
		   ,[Country] = C.CountryName
		   ,[DOB] = P.DateOfBirth
		   ,[FlightPurposeCode] = CONVERT(VARCHAR(MAX),FP.FlightPurposeCD) --In Itinerary Report give field name As(Bus/Pers)
		   ,[LegNum] = CONVERT(VARCHAR,PL.LegNum)
		   ,[DepartmentDesc] = CONVERT(VARCHAR(MAX),D.DepartmentName)
		   FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
				LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				LEFT OUTER JOIN Country C ON P.CountryID = C.CountryID
				LEFT OUTER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID
				LEFT OUTER JOIN Department D ON P.DepartmentID = D.DepartmentID		
				LEFT OUTER JOIN CrewPassengerPassport CPP ON PP.PassportID = CPP.PassportID	
			WHERE PM.TripID = CONVERT(BIGINT,@TripID)
			AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			--ORDER BY P.PassengerRequestorCD, PL.LegNUM
			ORDER BY pp.OrderNUM, PL.LegNUM
END
			
END

--EXEC spGetReportPRETSPAXNoFlyInformation 'JWILLIAMS_13', '100131807','1',''








GO


