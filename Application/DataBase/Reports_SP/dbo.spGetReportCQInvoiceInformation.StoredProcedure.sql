IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQInvoiceInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQInvoiceInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQInvoiceInformation]  
 @CQFileID VARCHAR(30),  
 @QuoteNUM VARCHAR(30)  
AS   
-- =============================================  
-- SPC Name:spGetReportCQInvoiceInformation  
-- Author: Aishwarya.M  
-- Create date: 06 Mar 2013  
-- Description: Get Charter Quote Information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- =============================================  
BEGIN  
  
  SELECT DISTINCT    
        IsQuoteInformation2 = C.IsQuoteInformation ,  
        CompanyName = CQI.CharterCompanyInformation---CASE WHEN C.IsQuoteInformation = 1 THEN C.CompanyName ELSE NULL END  
  ,C.IsCompanyName  
  ,CompanyInfo = CQI.CustomerAddressInformation  
  --,C.QuoteIDTYPE  
  ,InvoiceNumber = CASE WHEN C.QuoteIDTYPE = 1 OR C.QuoteIDTYPE = 2 THEN CQI.InvoiceNUM ELSE NULL END  
  ,CQI.InvoiceDT  
  ,InvoiceDate = CQI.InvoiceDT  
  , C.IsQuoteDispatch  
  ,DispatchNumber  = CQI.DispatchNUM   
  ,QuoteNumber = CASE WHEN  IsPrintInvoice= 1 THEN CONVERT(VARCHAR(4),CQF.FileNUM)+' - '+CONVERT(VARCHAR(4),CM.QuoteNUM) ELSE NULL END  --CQM.IsPrintQuoteID --CF.QuoteDescription  
  ,Header =CQI.InvoiceHeader --CQM.ReportHeader,  
  ,IsLeg = C.IsInvoiceRptLegNum  
  ,Leg = CQID.LegNUM --AND CL.IsPositioning = 1  
  ,IsDepDate = C.IsInvoiceRptDetailDT  
  ,DepDate = CQID.DepartureDTTMLocal  --AND CL.IsPositioning = 1   
  ,IsDepFrom = C.IsInvoiceRptFromDescription  
  ,DepFrom = D.CityName+', '+RTRIM(DC.CountryCD)+'; '+D.IcaoID    ---AND CL.IsPositioning = 1 CL.FromDescription  
  ,DepTime = CQID.DepartureDTTMLocal --AND CQID.DepartureDT IS NOT NULL  
  ,IsArrTo = C.IsQuoteDetailRptToDescription   
  ,ArrTo = A.CityName+', '+RTRIM(ACA.CountryCD)+'; '+A.IcaoID --CL.ToDescription  
  --,C.IS  
  ,ArrTime = CQID.ArrivalDTTMLocal  
  ,IsFltHrs = C.IsQuoteDetailFlightHours  
  ,FltHrs =  CQID.FlightHours   
  ,IsMiles = C.IsQuoteDetailMiles  
  ,Miles = CQID.Distance  
  ,IsPax = C.IsQuoteDetailPassengerCnt  
  ,Pax = CQID.PassengerTotal  
  ,IsInvoicedAmount = C.IsQuoteDetailInvoiceAmt  
  ,InvoicedAmount =CQID.FlightCharge --CQA.InvoiceAmt  
  ,FooterMsg = CQI.InvoiceFooter  
  ,ReportTitle = C.InvoiceTitle  
  ,CQID.LegNUM LEGNO  
  ,IsTailNO = C.IsTailNumber  
  ,[TAILNUMBER] = F.TailNUM  
  ,IsArcft = C.IsAircraftTypeCode   
  ,[AIRCRAFTTYPE] = AC.AircraftCD + ' - ' + AC.AircraftDescription  
  ,IsQuoteNUM=CQI.IsPrintQuoteNUM  
  ,IsArrDate=IsInvoiceRptArrivalDate  
  ,LogoPosition = CASE WHEN S.LogoPosition IS NOT NULL AND CQF.SalesPersonID IS  NOT NULL AND S.LogoPosition <> 0
        THEN  S.LogoPosition 
         ELSE C.IlogoPOS END  
      --C.IlogoPOS -- 0 : None, 1 : Center, 2 : Left, 3 : Right  
   FROM CQFile CQF  
   INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0  
   --INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID  
   --INNER JOIN CQInvoiceQuoteDetail CL ON CM.CQMainID = CL.CQInvoiceMainID  
   LEFT OUTER  JOIN Fleet F ON CM.FleetID = F.FleetID  
   LEFT OUTER  JOIN Aircraft AC ON CM.AircraftID = AC.AircraftID  
   LEFT OUTER JOIN SalesPerson S ON CQF.SalesPersonID = S.SalesPersonID  
   INNER JOIN Company C ON CQF.CustomerID = C.CustomerID AND C.HomebaseID=CQF.HomebaseID  
   LEFT OUTER JOIN CQFleetCharterRate CF ON CM.CQMainID = CF.CQMainID  
   LEFT OUTER JOIN CQInvoiceMain CQI ON CM.CQMainID = CQI.CQMainID   
   LEFT OUTER JOIN CQInvoiceQuoteDetail CQID ON CQI.CQInvoiceMainID = CQID.CQInvoiceMainID  AND CQID.LegNum=CQID.LegNUM  
   LEFT OUTER JOIN CQInvoiceAdditionalFees CQA ON CQI.CQInvoiceMainID = CQA.CQInvoiceMainID  
   LEFT OUTER JOIN Airport D ON D.AirportID=CQID.DAirportID  
   LEFT OUTER JOIN Airport A ON A.AirportID=CQID.AAirportID  
   LEFT OUTER JOIN Country DC ON DC.CountryID=D.CountryID  
   LEFT OUTER JOIN Country ACA ON ACA.CountryID=A.CountryID  
   LEFT OUTER JOIN FileWarehouse FW ON FW.RecordID = CQF.HomebaseID AND FW.RecordType = 'CqCompanyInvoice'  
   WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)  
   --AND CM.QuoteNUM = @QuoteNUM  
   AND (CM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')    
   ORDER BY CQID.LegNUM  
   
   
     
END  
   
 --EXEC spGetReportCQInvoiceInformation 100012,1    
  
  
/*  
SELECT  CompanyName =CASE WHEN C.IsQuoteInformation = 1 THEN C.CompanyName ELSE NULL END  
  ,CompanyInfo = CQI.CharterCompanyInformation  
  ,InvoiceNumber = CASE WHEN C.QuoteIDTYPE = 1 OR C.QuoteIDTYPE = 2 THEN CQI.InvoiceNUM ELSE NULL END  
  ,InvoiceDate = CASE WHEN CQI.InvoiceDT IS NOT NULL THEN CQI.InvoiceDT ELSE NULL END  
  ,DispatchNumber  = CASE WHEN C.IsQuoteDispatch = 1 THEN CQI.DispatchNUM ELSE NULL END  
  ,QuoteNumber = CASE WHEN  IsPrintInvoice= 1 THEN CF.QuoteDescription ELSE NULL END  --CQM.IsPrintQuoteID   
  ,Header =CQI.InvoiceHeader --CQM.ReportHeader  
  ,Leg = CASE WHEN C.IsInvoiceRptLegNum = 1 AND CL.IsPositioning = 1 THEN CL.LegNUM ELSE NULL END  
  ,DepDate = CASE WHEN C.IsInvoiceRptDetailDT = 1 AND CQID.DepartureDT IS NOT NULL AND CL.IsPositioning = 1 THEN CL.DepartureDTTMLocal ELSE NULL END  
  ,DepFrom = CASE WHEN C.IsInvoiceRptFromDescription = 1 AND CL.IsPositioning = 1 THEN CL.FromDescription ELSE NULL END  
  ,DepTime = CASE WHEN C.IsInvoiceRptFromDescription = 1 AND CQID.DepartureDT IS NOT NULL THEN CL.DepartureDTTMLocal ELSE NULL END  
  ,ArrTo = CASE WHEN C.IsQuoteDetailRptToDescription = 1 THEN CL.ToDescription ELSE NULL END  
  ,ArrTime = CASE WHEN C.IsQuoteDetailRptToDescription = 1 THEN CL.ArrivalDTTMLocal ELSE NULL END  
  ,FltHrs = CF.ElapseTM  
  ,Miles = CASE WHEN C.IsQuoteDetailFlightHours = 1 THEN CL.Distance ELSE 0 END  
  ,Pax = CASE WHEN C.IsQuoteDetailPassengerCnt = 1 THEN CL.PassengerTotal ELSE 0 END  
  ,InvoicedAmount = CASE WHEN C.IsQuoteDetailInvoiceAmt = 1 THEN CQA.InvoiceAmt ELSE 0 END  
  ,FooterMsg = CQI.InvoiceFooter  
  ,ReportTitle = C.InvoiceTitle  
   FROM CQFile CQF  
   INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID  
   INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID  
   LEFT OUTER JOIN CQFleetCharterRate CF ON CM.CQMainID = CF.CQMainID  
   INNER JOIN Company C ON CQF.CustomerID = C.CustomerID AND C.HomebaseID=CQF.HomebaseID  
   LEFT OUTER JOIN CQInvoiceMain CQI ON CM.CQMainID = CQI.CQMainID   
   LEFT OUTER JOIN CQInvoiceQuoteDetail CQID ON CL.LegID = CQID.LegID  
   LEFT OUTER JOIN CQInvoiceAdditionalFees CQA ON CQI.CQInvoiceMainID = CQA.CQInvoiceMainID  
   WHERE CQF.CQFileID = @CQFileID  
   AND CM.QuoteNUM = @QuoteNUM  
   ORDER BY CL.LegNUM  
*/  
  
  

GO


