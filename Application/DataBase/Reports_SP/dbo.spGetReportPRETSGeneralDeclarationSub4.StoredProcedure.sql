IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub4]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub4]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
  
CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub4]  
 @LegID AS VARCHAR(30)               -- Mandatory    
AS  
-- ==========================================================================================  
-- SPC Name: spGetReportPRETSGeneralDeclarationSub4  
-- Author: ABHISHEK.S  
-- Create date: 20th September 2012  
-- Description: Get TripSheetReportWriter General Declaration Information For Reports  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================================  
  
--- -- EXEC  spGetReportPRETSGeneralDeclaration 'TIM', '', '', '', '2011-07-20', '2012-07-21', '', '', '', '', '', '', '',1,1,1,1,1,1   
   
BEGIN  
  
  
   
SET NOCOUNT ON;   
  
  
  
  
     
SELECT DISTINCT Title=(CASE WHEN PC.DutyTYPE = 'P' THEN 'Capt' ELSE 'Crew' END)   
    ,LastName=CR.LastName  
    ,FirstName=CR.FirstName  
    ,MiddleName=CR.MiddleInitial  
    ,Gender=CR.Gender  
    ,DOB=DATENAME(D,CR.BirthDT)+UPPER(LEFT(DATENAME(M,CR.BirthDT),3))+DATENAME(YEAR,CR.BirthDT)  
    ,Passport=CPP.PassportNum  
    ,ExpDate=DATENAME(D,CPP.PassportExpiryDT)+UPPER(LEFT(DATENAME(M,CPP.PassportExpiryDT),3))+DATENAME(YEAR,CPP.PassportExpiryDT)  
    ,Nationality=CASE WHEN ISNULL(C.CountryCD,'') = ''  THEN CO.CountryCD ELSE C.CountryCD END   
    ,Type='C'  
    ,CrewCD  
    ,PL.LegNUM  
    ,PM.TripNUM

   
 FROM PREFLIGHTMAIN PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0  
   INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID  
   INNER JOIN Crew CR ON PC.CrewID = CR.CrewID --AND CR.IsDeleted=0 AND CR.IsStatus=1  
   LEFT JOIN CrewPassengerPassport CPP ON PC.PassportID = CPP.PassportID AND PC.CrewID = CPP.CrewID AND CPP.IsDeleted = 0  
   LEFT JOIN Country C ON CPP.CountryID = C.CountryID  
   LEFT JOIN Country CO ON CR.CountryID = CO.CountryID  
 WHERE PM.IsDeleted = 0   
   AND PL.LegID=@LegID      
    
 ORDER BY PM.TripNUM,PL.LegNUM,Title,CrewCD  
  
  
END
GO


