IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRTripItinerary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRTripItinerary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






                                                                                           
CREATE PROCEDURE [dbo].[spGetReportCRTripItinerary] 
( 
    @UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30), --Mandatory
	@CRTripNUM AS VARCHAR(500),
	@CRLegNum AS VARCHAR(500)='',
	--@NumberofCopies AS VARCHAR(500)='',
	@PrintOneLegPerPage BIT = 0,
	@PrintLegNotes BIT = 0
	--,@FilterFerryLegs BIT = 0
	,@FilterFerryLegs BIT=0
)
AS
-- =============================================
-- SPC Name:spGetReportCRTripItinerary
-- Author: Akhila
-- Create date:
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON

DECLARE @CRTrip BIGINT;

SELECT DISTINCT @CRTrip=CRM.TripID FROM CRMain CRM INNER JOIN PreflightMain PM ON PM.TripID = CRM.TripID
                            WHERE CRM.CustomerID=@UserCustomerID
                              AND CRM.CRTripNUM=@CRTripNUM

IF @CRTrip IS NULL 

BEGIN

		
SELECT DISTINCT
		[TripNumber] = ISNULL(PM.TripNUM,0)
		,[CRTripNUM] = CRM.CRTripNUM
		,[TripDatesDEPART] = ISNULL(PLDate.DepartureDTTMLocal,'') 
		,[TripDatesARRIVE] = ISNULL(PLDate.ArrivalDTTMLocal ,'')--MIN + MAX
		,[TailNumber] = F.TailNum
		,[Type] = AC.AircraftCD 
		,[Description] = CRM.CRMainDescription
		,[Requestor] = Req.PassengerName
		,[ContactPhone] = Req.PhoneNum
		,[Leg] = CRL.LegNUM
		,[DepartICAO] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName --AD.IcaoID
		,[ArriveICAO] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName --AA.IcaoID
		--,[Zulu1] = Cast(DateDiff(MINUTE,CRL.DepartureGreenwichDTTM,CRL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
		--,[Zulu2] = Cast(DateDiff(MINUTE,CRL.ArrivalGreenwichDTTM,CRL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2))
		,[Timechange] = Cast(DateDiff(MINUTE,CRL.ArrivalGreenwichDTTM,CRL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2)) - Cast(DateDiff(MINUTE,CRL.DepartureGreenwichDTTM,CRL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
		,[LocDep] = CRL.DepartureDTTMLocal
		,[LocArr] = CRL.ArrivalDTTMLocal
		,[Distance] = CRL.Distance
		,[ETE] = CRL.ElapseTM
		,[PAX] = CRL.PassengerTotal
		,[PILOTPIC] = PIC.PilotPI
		,[PILOTSIC] = SIC.PilotSI
		,[ADDLCREW] = ACrew.AssociatedCrew
		,[FBONameFromTerminal] = CRFD.FBOVendor
		,[PhoneFromTerminal] = CRFD.PhoneNUM1
		,[FBONameToTerminal] = CRFA.FBOVendor 
		,[PhoneToTerminal] = CRFA.PhoneNUM1 
		,[HotelName] = CRHP.Name
		,[HotelPhone] = CRHP.PhoneNUM 
		,[TransportName] = CRT.TransportationVendor 
		,[TransportPhone] = CRT.PhoneNUM
		,[CrewHotelName] = CRHC.Name
		,[CrewHotelPhone] = CRHC.PhoneNUM
		,[CrewHotelAddress1] = CRHC.Addr1
		,[CrewHotelAddress2] = CRHC.Addr2
		,[CaterName] = CTRNG.CateringVendor
		,[CaterPhone] = CTRNG.PhoneNum
		,[PAXNotes] = ISNULL(CRL.Notes,'NONE')
		--,[paxname] = CRP.PassengerName
		--,[fltpurpose] = FP.FlightPurposeCD
		,CRM.CustomerID
		FROM CRMain CRM
		LEFT OUTER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.IsDeleted = 0
		LEFT OUTER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID AND CRL.IsDeleted = 0
		LEFT OUTER JOIN Fleet F ON F.FleetID = CRM.FleetID
		LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = CRM.AircraftID
		LEFT OUTER JOIN Passenger Req ON Req.PassengerRequestorID = CRM.PassengerRequestorID
		LEFT OUTER JOIN Airport AA ON AA.AirportID =CRL.AAirportID
		LEFT OUTER JOIN Airport AD ON AD.AirportID =CRL.DAirportID
		LEFT OUTER JOIN PreflightLeg PL ON PL.TripID = PM.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID = PL.LegID
		LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID


		--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'A'
				) CRFA ON CRFA.CRLegID = CRL.CRLegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'D'
				) CRFD ON CRFD.CRLegID = CRL.CRLegID
				
		--HOTEL
		LEFT OUTER JOIN
				(SELECT CH.PhoneNUM PhoneNUM, CH.CRHotelListDescription Name, CH.CRLegID
					FROM CRHotelList CH
					    LEFT OUTER JOIN Hotel H ON CH.HotelID = H.HotelID
					WHERE CH.RecordType = 'P' --PAX HOTEL
				) CRHP ON CRL.CRLegID = CRHP.CRLegID
		LEFT OUTER JOIN
				(SELECT CH.PhoneNUM PhoneNUM, CH.CRHotelListDescription Name, CH.CRLegID, H.Addr1, H.Addr2
					FROM CRHotelList CH
					LEFT OUTER JOIN Hotel H ON CH.HotelID = H.HotelID
					WHERE CH.RecordType = 'C' --CREW HOTEL
				) CRHC ON CRL.CRLegID = CRHC.CRLegID
		LEFT OUTER JOIN
				(SELECT H.PhoneNUM, H.Name, CH.CRLegID
					FROM CRHotelList CH
					INNER JOIN Hotel H ON CH.HotelID = H.HotelID
					WHERE CH.RecordType = 'A' --ADDL CREW / MAINT HOTEL
				) CRHA ON CRL.CRLegID = CRHA.CRLegID
						
		--TRANSPORT 
		LEFT OUTER JOIN 
				(SELECT CT.CRLegID,CT.PhoneNUM PhoneNUM, CT.CRTransportListDescription TransportationVendor
					FROM CRTransportList CT
					LEFT OUTER JOIN  Transport T ON CT.TransportID = T.TransportID
					WHERE CT.RecordType = 'P'
				)CRT ON CRT.CRLegID = CRL.CRLegID
			
		--CATERING
		LEFT OUTER JOIN
				(SELECT CA.CRCateringListDescription CateringVendor,CA.CRLegID,CA.PhoneNUM PhoneNum
					FROM CRCateringList CA
					LEFT OUTER JOIN Catering C ON CA.CateringID = C.CateringID
					WHERE CA.RecordType = 'D'
				)CTRNG ON CTRNG.CRLegID = CRL.CRLegID
					
		--LEFT OUTER JOIN CRPassenger CRP ON CRP.CRLegID = CRL.CRLegID
		--LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID = CRP.FlightPurposeID

		LEFT OUTER JOIN (
				SELECT DISTINCT PC1.LegID, AssociatedCrew = (
				SELECT  ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') 
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE NOT IN ('P','S')
				WHERE PC.LegID = PC1.LegID
				ORDER BY C.CrewCD
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC1
				WHERE PC1.DutyTYPE NOT IN ('P','S')
		) ACrew ON PL.LegID = ACrew.LegID

		LEFT OUTER JOIN (
				SELECT PC.LegID, PilotPI =  ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') 
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE  IN ('P')
		) PIC ON PL.LegID = PIC.LegID

		LEFT OUTER JOIN (
				SELECT PC.LegID, PilotSI = ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') 
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE  IN ('S')
		) SIC ON PL.LegID = SIC.LegID  

		LEFT OUTER JOIN(
					SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,TripNUM,CustomerID,CRMainID
					FROM CRLeg 
					GROUP BY TripNUM,CustomerID,CRMainID
		) PLDate ON  PLDate.CustomerID=CRL.CustomerID AND PLDate.CRMainID=CRL.CRMainID
		LEFT OUTER JOIN FlightCatagory FC ON CRL.FlightCategoryID=FC.FlightCategoryID

		WHERE
		CRM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
		AND (CRM.CRTripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRTripNUM, ',')) OR @CRTripNUM = '')
		AND (CRL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
		AND (FC.IsDeadorFerryHead IN (CASE WHEN @FilterFerryLegs=1 THEN 0 ELSE 1 END) OR FC.IsDeadorFerryHead IS NULL)
		AND CRM.IsDeleted = 0
END

ELSE

BEGIN


SELECT DISTINCT
		[TripNumber] = PM.TripNUM
		,[CRTripNUM] = CRM.CRTripNUM
		,[TripDatesDEPART] = ISNULL(PLDate.DepartureDTTMLocal,'') 
		,[TripDatesARRIVE] = ISNULL(PLDate.ArrivalDTTMLocal ,'')--MIN + MAX
		,[TailNumber] = F.TailNum
		,[Type] = AC.AircraftCD 
		,[Description] = CRM.CRMainDescription
		,[Requestor] = Req.PassengerName
		,[ContactPhone] = Req.PhoneNum
		,[Leg] = PL.LegNUM--CRL.LegNUM
		,[DepartICAO] =  AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[ArriveICAO] =  AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		--,[Zulu1] = Cast(DateDiff(MINUTE,CRL.DepartureGreenwichDTTM,CRL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
		--,[Zulu2] = Cast(DateDiff(MINUTE,CRL.ArrivalGreenwichDTTM,CRL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2))
		,[Timechange] = Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2)) - Cast(DateDiff(MINUTE,PL.DepartureGreenwichDTTM,PL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
		,[LocDep] = PL.DepartureDTTMLocal--CRL.DepartureDTTMLocal
		,[LocArr] =PL.ArrivalDTTMLocal --CRL.ArrivalDTTMLocal
		,[Distance] =PL.Distance --CRL.Distance
		,[ETE] =PL.ElapseTM --CRL.ElapseTM
		,[PAX] =PL.PassengerTotal --CRL.PassengerTotal
		,[PILOTPIC] = PIC.PilotPI
		,[PILOTSIC] = SIC.PilotSI
		,[ADDLCREW] = ACrew.AssociatedCrew
		,[FBONameFromTerminal] = CRFD.FBOVendor
		,[PhoneFromTerminal] = CRFD.PhoneNUM1
		,[FBONameToTerminal] = CRFA.FBOVendor 
		,[PhoneToTerminal] = CRFA.PhoneNUM1 
		,[HotelName] = CRHP.Name
		,[HotelPhone] = CRHP.PhoneNUM 
		,[TransportName] = CRT.TransportationVendor 
		,[TransportPhone] = CRT.PhoneNUM
		,[CrewHotelName] = CRHC.Name
		,[CrewHotelPhone] = CRHC.PhoneNUM
		,[CrewHotelAddress1] = CRHC.Addr1
		,[CrewHotelAddress2] = CRHC.Addr2
		,[CaterName] = CTRNG.CateringVendor
		,[CaterPhone] = CTRNG.ContactPhone
		,[PAXNotes] = ISNULL(PL.Notes,'NONE')
		--,[paxname] = CRP.PassengerName
		--,[fltpurpose] = FP.FlightPurposeCD
		,CRM.CustomerID
		FROM CRMain CRM
		LEFT OUTER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.IsDeleted = 0
		LEFT OUTER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID AND CRL.IsDeleted = 0
		LEFT OUTER JOIN Fleet F ON F.FleetID = CRM.FleetID
		LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = CRM.AircraftID
		LEFT OUTER JOIN Passenger Req ON Req.PassengerRequestorID = CRM.PassengerRequestorID
		LEFT OUTER JOIN PreflightLeg PL ON PL.TripID = PM.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN Airport AA ON AA.AirportID =PL.ArriveICAOID
		LEFT OUTER JOIN Airport AD ON AD.AirportID =PL.DepartICAOID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID = PL.LegID
		LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID


		--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1,PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsArrivalFBO = 1 
				) CRFA ON CRFA.LegID = PL.LegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsDepartureFBO = 1
				) CRFD ON CRFD.LegID = PL.LegID
				
		--HOTEL
		LEFT OUTER JOIN
				(SELECT PPH.PhoneNum1 PhoneNUM, PPH.PreflightHotelName Name, PPL.LegID
					FROM PreflightPassengerList PPL 
			             INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
					--WHERE CH.RecordType = 'P' --PAX HOTEL
				) CRHP ON CRHP.LegID = PL.LegID
		LEFT OUTER JOIN
				(SELECT PCH.PhoneNum1 PhoneNUM, PCH.PreflightHotelName Name, PCLL.LegID, PCH.Address1 Addr1, PCH.Address2 Addr2
					FROM PreflightCrewList PCLL 
				        --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				        INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1
																					AND PCH.crewPassengerType = 'C'
						INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
																		AND PHCP.CrewID = PCLL.CrewID
					WHERE PCLL.DutyType IN ('P', 'S')--CREW HOTEL
				) CRHC ON PL.LegID = CRHC.LegID
		LEFT OUTER JOIN
				(SELECT PCH.PhoneNum1 PhoneNUM, PCH.PreflightHotelName Name, PCLL.LegID
					FROM  PreflightCrewList PCLL 
				        --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				        INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1
																					AND PCH.crewPassengerType = 'C'
						INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
																		AND PHCP.CrewID = PCLL.CrewID
					WHERE PCLL.DutyType NOT IN ('P', 'S') --ADDL CREW / MAINT HOTEL
				) CRHA ON PL.LegID = CRHA.LegID
						
		--TRANSPORT 
		LEFT OUTER JOIN 
				(SELECT PTL.LegID, PTL.PhoneNum1 PhoneNUM, PTL.PreflightTransportName TransportationVendor
					FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
					WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
				)CRT ON CRT.LegID = PL.LegID
			
		--CATERING
		LEFT OUTER JOIN
				(SELECT PCL.ContactName CateringVendor,PCL.LegID,PCL.ContactPhone
					FROM PreflightCateringDetail PCL 
				         LEFT OUTER JOIN Catering C ON PCL.CateringID = C.CateringID 
				     WHERE PCL.ArriveDepart = 'D'
				)CTRNG ON CTRNG.LegID = PL.LegID
					
		--LEFT OUTER JOIN CRPassenger CRP ON CRP.CRLegID = CRL.CRLegID
		--LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID = CRP.FlightPurposeID

		LEFT OUTER JOIN (
				SELECT DISTINCT PC1.LegID, AssociatedCrew = (
				SELECT  ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') +', '
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE NOT IN ('P','S')
				WHERE PC.LegID = PC1.LegID
				ORDER BY C.CrewCD
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC1
				WHERE PC1.DutyTYPE NOT IN ('P','S')
		) ACrew ON PL.LegID = ACrew.LegID

		LEFT OUTER JOIN (
				SELECT PC.LegID, PilotPI =  ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') 
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE  IN ('P')
		) PIC ON PL.LegID = PIC.LegID

		LEFT OUTER JOIN (
				SELECT PC.LegID, PilotSI = ISNULL(LEFT(C.FirstName,1),'')+'. ' + ISNULL(C.LastName,'') 
				FROM PreflightCrewList PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.DutyTYPE  IN ('S')
		) SIC ON PL.LegID = SIC.LegID  

		LEFT OUTER JOIN(
					SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,TripNUM,CustomerID,CRMainID
					FROM CRLeg 
					GROUP BY TripNUM,CustomerID,CRMainID
		) PLDate ON  PLDate.CustomerID=CRL.CustomerID AND PLDate.CRMainID=CRL.CRMainID
		LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID=FC.FlightCategoryID

		WHERE
		CRM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
		AND (CRM.CRTripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRTripNUM, ',')) OR @CRTripNUM = '')
		AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
		AND (FC.IsDeadorFerryHead IN (CASE WHEN @FilterFerryLegs=1 THEN 0 ELSE 1 END) OR FC.IsDeadorFerryHead IS NULL)
		AND CRM.IsDeleted = 0

END 






END
--EXEC spGetReportCRTripItinerary 'erick_1','10001','4','',0,0



GO


