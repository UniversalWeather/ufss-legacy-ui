IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewDutyTypeInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewDutyTypeInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportCrewDutyTypeInformation]
	@UserCD varchar(30)
AS
-- ===============================================================================
-- SPC Name: spGetReportCrewDutyTypeInformation
-- Author: SUDHAKAR J
-- Create date: 14 Jun 2012
-- Description: Get Crew Duty Type information for SSRS REPORT
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

	SELECT 
		[CustomerID]
		,[DutyTypeCD]
		,[DutyTypesDescription]
		,[ValuePTS]
		,[BackgroundCustomColor] AS [GraphColor]		 
		,[WeekendPTS]
		,[IsWorkLoadIndex]
		,[IsOfficeDuty]
	FROM [CrewDutyType]
	WHERE 
	IsDeleted = 0
	AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	ORDER BY [CustomerID],[DutyTypeCD]
-- EXEC spGetReportCrewDutyTypeInformation 'UC'


GO


