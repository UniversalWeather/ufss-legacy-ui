IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREGADeskInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREGADeskInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPREGADeskInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@IsHomeBase BIT = 0,
	@UserHomebaseID AS VARCHAR(30)
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPREGADeskInformation
-- Author:  A.Akhila
-- Create date: 22 Feb 2013
-- Description: Get GA Desk for Reports
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================
BEGIN
-----------------------------TailNum and Fleet Group Filteration----------------------
		DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
		DECLARE @TempFleetID TABLE 
		( 
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(10)
		)
		 
		IF @TailNUM <> ''
		BEGIN
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum 
		FROM Fleet F
		WHERE F.CustomerID = @CUSTOMER
		AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
		END
		IF @FleetGroupCD <> ''
		BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
		FROM Fleet F 
		LEFT OUTER JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
		AND F.CustomerID = @CUSTOMER 
		END
		ELSE IF @TailNUM = '' AND @FleetGroupCD = ''
		BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
		FROM Fleet F 
		WHERE F.CustomerID = @CUSTOMER
		AND F.IsDeleted=0 
		AND IsInActive = 0 
		END
		 
-----------------------------TailNum and Fleet Group Filteration----------------------
 
SET NOCOUNT ON;
SELECT DISTINCT
	[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[Day] = SUBSTRING(DATENAME(dw,PL.DepartureGreenwichDTTM),1,3)--Have doubt in this mapping
	,[DATE] = CONVERT(DATE,PL.DepartureGreenwichDTTM)
	,[TailNo] = F.TailNum
	,[FileNo] = PM.TripNUM
	,[Departure] = SUBSTRING(CONVERT(VARCHAR(10),PL.DepartureGreenwichDTTM,108),1,5)+' '+AD.IcaoID
	,[Arrival] = SUBSTRING(CONVERT(VARCHAR(10),PL.ArrivalGreenwichDTTM,108),1,5)+' '+AA.IcaoID
	,[Pic] = PIC.Crew
	,[CellularPhone] = PIC.CellPhoneNum

FROM PreflightLeg PL
	INNER JOIN PreflightMain PM ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
	INNER JOIN Fleet F ON F.FleetID = PM.FleetID
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
	INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	LEFT OUTER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
	LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
	LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD,C.CellPhoneNum
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=@UserCustomerID)PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
WHERE PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
	AND (PM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomeBase = 0)
	AND CONVERT(DATE,PL.DepartureGreenwichDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)--Have doubt in this mapping
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND (PM.TripStatus='T' OR PM.TripStatus='H') 
	AND PL.IsDeleted = 0
ORDER BY [DATE],[TailNo],[Departure]
--EXEC spGetReportPREGADeskInformation 'SUPERVISOR_99','10099','2010-12-1','2011-1-31','','','',0,'1009951513'
END


GO


