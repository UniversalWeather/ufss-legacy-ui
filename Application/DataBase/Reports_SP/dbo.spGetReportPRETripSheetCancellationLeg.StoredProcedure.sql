IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetCancellationLeg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetCancellationLeg]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETripSheetCancellationLeg]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
		
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETripSheetCancellationLeg
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight TripSheetCancellation Leg information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
       SELECT 
			[LegNum] = CONVERT(VARCHAR,PL.LegNum)
		   ,[DepICAOID] = A.IcaoID
		   ,[ArrICAOID] = AA.IcaoID
		   ,[DepLocal] = PL.DepartureDTTMLocal
		   ,[ArrLocal] = PL.ArrivalDTTMLocal
		   ,[DepZulu] = PL.DepartureGreenwichDTTM
		   ,[ArrZulu] = PL.ArrivalGreenwichDTTM
		   ,[DepAirportName] = A.AirportName
			,[DepCity] = A.CityName
			,[DepState] = A.StateName
			,[ArrAirportName] = AA.AirportName
			,[ArrCity] = AA.CityName
			,[ArrState] = AA.StateName
			,[Distance] = PL.Distance
		   ,[CancelDescription] = PM.CancelDescription
		   ,[LegID] = CONVERT(VARCHAR,PL.LegID)
		   ,[TripID] = CONVERT(VARCHAR,PM.TripID)
		   FROM PreflightMain PM
	       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		   INNER JOIN  Airport A ON PL.DepartICAOID = A.AirportID --AND PM.CustomerID = A.CustomerID
		   INNER JOIN  Airport AA ON PL.ArriveICAOID = AA.AirportID 
		   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			--AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			AND PM.TripStatus = 'X'
			And PM.IsDeleted = 0
			
			ORDER BY PL.DepartureDTTMLocal, PL.ArrivalDTTMLocal, PL.LegNUM
        END	 
        
 ---EXEC spGetReportPRETripSheetCancellationLeg 'supervisor_99',''


GO


