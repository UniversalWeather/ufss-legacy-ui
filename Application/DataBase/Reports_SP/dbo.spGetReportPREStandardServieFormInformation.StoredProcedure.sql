IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREStandardServieFormInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREStandardServieFormInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPREStandardServieFormInformation]
		@UserCD AS VARCHAR(30),--Mandatory
		@UserCustomerID AS VARCHAR(20), --Mandatory
	    @TripNUM VARCHAR(10)--Mandatory
	    ,@LegNUM VARCHAR(100) 
AS
-- ===============================================================================
-- SPC Name: spGetReportPREDestinationInformation
-- Author: Askar 
-- Create date: 27 June 2013
-- Description: Get Standard Service form information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================
BEGIN

SELECT  DISTINCT Date_Time=dbo.GetShortDateFormatByUserCD(@UserCD,GETDATE())+' '+CONVERT(VARCHAR(5),GETDATE(),108)
       ,Flight_Number=PM.TripNUM
       ,Company=CO.CompanyName
       ,TailNo=F.TailNum
       ,Type_Code=F.AircraftCD
       ,Captain=PCC.LastName---SUBSTRING(PIC.CrewCodes,1,LEN(PIC.CrewCodes)-1)
       ,ARRIVAL=DATENAME(DW,PL.ArrivalDTTMLocal) 
       ,Arr_Date=dbo.GetShortDateFormatByUserCD(@UserCD,PL.ArrivalDTTMLocal)---CONVERT(DATE,PL.ArrivalDTTMLocal)
       ,ETA=SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalDTTMLocal,113),13,5) 
       ,ZuleArrTime=SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalGreenwichDTTM,113),13,5)
       ,POD=D.IcaoID
       ,Departure=DATENAME(DW,PL.DepartureDTTMLocal) 
       ,Dep_Date=dbo.GetShortDateFormatByUserCD(@UserCD,PL.DepartureDTTMLocal)
       ,ETD=SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5) 
       ,ZuleDepTime=SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureGreenwichDTTM,113),13,5)
       ,POA=A.IcaoID
       -------------Airport and FBO Information------------------
       ,Arr_City=A.CityName
       ,Airport_Name=A.AirportName
       ,FBO=FBA.FBOVendor
       ,ARINC_UNIC=FBA.Frequency
       ,FBO_Phone=FBA.PhoneNUM1
       ,FBO_Fax=FBA.FaxNum
       ,PIC=PCC.Crew
       ,SIC=SCC.Crew
       ,CrewCount=PCCount.CrewCount
       ,PaxCount=PL.PassengerTotal
       ,DateVal=dbo.GetShortDateFormatByUserCD(@UserCD,GETDATE())
        ,PL.LegNUM

       
       FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                             INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                             LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID=PL.LegID
                             LEFT OUTER JOIN(SELECT COUNT(CrewID) CrewCount,LegID FROM PreflightCrewList WHERE CustomerID=@UserCustomerID GROUP BY LegID)PCCount ON PCCount.LegID=PL.LegID
                             LEFT OUTER JOIN Crew C ON C.CrewID=PC.CrewID
                             LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
                             LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PPL.PassengerID
                             LEFT OUTER JOIN Airport D ON PL.DepartICAOID=D.AirportID
                             LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID

							 LEFT OUTER JOIN (SELECT  PFL.PreflightFBOName FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,
										             F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,
												     F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,
												     PFL.LegID, F.Addr3, F.ContactEmail
								                     FROM PreflightFBOList PFL
								                     INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
							                     ) FBA ON PL.LegID = FBA.LegID
							 LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
							 LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID
							                                  , CrewCodes = (
																				SELECT C.LastName + ',' 
																				FROM PreflightCrewList PC
																				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
																				WHERE PC.LegID = PC2.LEGID AND PC.DutyTYPE = 'P'
																				ORDER BY C.CrewCD 
																				FOR XML PATH('')
				                                                            )FROM PreflightCrewList PC2		
			                                    ) PIC ON PL.LEGID = PIC.LEGID 
			                 LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=@UserCustomerID)PCC ON PCC.LegID=PL.LegID AND Rnk=1
                             LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=@UserCustomerID)SCC ON SCC.LegID=PL.LegID AND SCC.Rnk=1
         WHERE PM.CustomerID=@UserCustomerID
           AND PM.TripNUM=CONVERT(BIGINT,@TripNUM) 
            AND PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ','))
            AND PM.IsDeleted = 0
                                                 


END


--SELECT PM.TripNUM FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.CustomerID=PL.CustomerID
--         WHERE PM.CustomerID=10099

-- EXEC spGetReportPREStandardServieFormInformation 'SUPERVISOR_99','10099','2021','1'







GO


