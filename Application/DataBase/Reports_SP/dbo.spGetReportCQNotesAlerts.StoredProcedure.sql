IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQNotesAlerts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQNotesAlerts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetReportCQNotesAlerts](  
			 @UserCD VARCHAR(30)    
             ,@UserCustomerID VARCHAR(30)    
             ,@FileNumber VARCHAR(200)='',    
              @QuoteNUM VARCHAR(200)='',    
              @LegNUM VARCHAR(200)='',    
              @BeginDate DATETIME=NULL,    
              @EndDate DATETIME=NULL,    
              @TailNum VARCHAR(200)='',    
              @VendorCD VARCHAR(200)='',    
              @HomeBaseCD VARCHAR(200)='',    
              @AircraftCD VARCHAR(200)='',    
              @CQCustomerCD VARCHAR(30)='',    
              @SalesPersonCD VARCHAR(200)='' 
     )   
AS    
    
-- =============================================    
-- SPC Name:spGetReportCQNotes/Alerts      
-- Author: Askar    
-- Create date: 23 Feb 2013    
-- Description: Get Report Charter Quote Notes/Alerts    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
BEGIN    

IF @BeginDate IS NOT NULL  
  
BEGIN  
    
SELECT DISTINCT CF.FileNUM,  
                CM.QuoteNUM,  
       A.AircraftCD Type,  
                CF.CQFileDescription TripDescription,  
                CQC.CQCustomerName,  
                F.TailNum,  
                CF.Notes,  
                TripDates = dbo.GetShortDateFormatByUserCD(@UserCD,MIN(CL.DepartureDTTMLocal))+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,MAX(CL.ArrivalDTTMLocal))     
           
 FROM CQFile CF INNER JOIN CQMain CM ON CF.CQFileID=CM.CQFileID  AND CM.IsDeleted=0   
                        INNER JOIN Fleet F ON F.FleetID=CM.FleetID    
                        LEFT OUTER JOIN FleetPair FP ON FP.FleetID=F.FleetID    
                        LEFT OUTER JOIN FleetProfileDefinition FPD ON FPD.FleetID=F.FleetID    
                        LEFT OUTER JOIN PreflightMain PM ON PM.TripID=CM.TripID    
                        LEFT OUTER JOIN CQLeg CL ON CL.CQMainID=CM.CQMainID AND CL.IsDeleted=0   
                        LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID    
                        INNER JOIN Company C ON C.HomebaseID=F.HomebaseID    
                        LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID    
                        LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID    
                        LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID    
                        LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID    
                            
  WHERE 
    -- ( CF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )    
    --AND ( CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )    
    --AND ( CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' )    
     ( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )    
    AND ( V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )    
    AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )    
    AND ( A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )    
    AND ( SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )    
    AND CF.EstDepartureDT BETWEEN @BeginDate AND @EndDate    
    AND CM.CustomerID = CONVERT(BIGINT,@UserCustomerID)  
    --AND CQC.CustomerID=@UserCustomerID    
    AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')    
    GROUP BY  CF.FileNUM,CM.QuoteNUM,A.AircraftCD,CF.CQFileDescription,CQC.CQCustomerName,F.TailNum,CF.Notes  
   
END  
ELSE  
BEGIN   
SELECT DISTINCT CF.FileNUM,  
                CM.QuoteNUM,  
                A.AircraftCD Type,  
                CF.CQFileDescription TripDescription,  
     CQC.CQCustomerName,  
                F.TailNum,  
                CF.Notes,  
                TripDates = dbo.GetShortDateFormatByUserCD(@UserCD,MIN(CL.DepartureDTTMLocal))+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,MAX(CL.ArrivalDTTMLocal))     
           
 FROM CQFile CF INNER JOIN CQMain CM ON CF.CQFileID=CM.CQFileID AND CM.IsDeleted=0   
                        INNER JOIN Fleet F ON F.FleetID=CM.FleetID    
                        LEFT OUTER JOIN FleetPair FP ON FP.FleetID=F.FleetID    
                        LEFT OUTER JOIN FleetProfileDefinition FPD ON FPD.FleetID=F.FleetID    
                        LEFT OUTER JOIN PreflightMain PM ON PM.TripID=CM.TripID    
                        LEFT OUTER JOIN CQLeg CL ON CL.CQMainID=CM.CQMainID  AND CL.IsDeleted=0  
                        --LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID    
                        --INNER JOIN Company C ON C.HomebaseID=F.HomebaseID    
                        LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID    
                        --LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID    
                        --LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID    
                        LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID    
                            
  WHERE ( CF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )    
    AND ( CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )    
    AND ( CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' )    
    --AND ( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )    
    --AND ( V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )    
    --AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )    
    --AND ( A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )    
    --AND ( SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )    
    AND CM.CustomerID = CONVERT(BIGINT,@UserCustomerID)  
    --AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')  
    GROUP BY  CF.FileNUM,CM.QuoteNUM,A.AircraftCD,CF.CQFileDescription,CQC.CQCustomerName,F.TailNum,CF.Notes  
END   
                           
END                                               
                                                  
--EXEC  spGetReportCQNotesAlerts 'supervisor_16','10016','','','','2007-10-01','2007-10-01','','','','','',''  
--EXEC  spGetReportCQNotesAlerts 'supervisor_16','10016','5','','',NULL,NULL,'','','','','',''     
  
  
  
  
                        
  

GO


