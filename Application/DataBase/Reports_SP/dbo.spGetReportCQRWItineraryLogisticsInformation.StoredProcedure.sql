IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWItineraryLogisticsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWItineraryLogisticsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[spGetReportCQRWItineraryLogisticsInformation]
	@CQLegID AS VARCHAR(30) 
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportCQRWItineraryLogisticsInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Checklist information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
		
	SELECT Distinct 
	     FBDesc = CF.FBOVendor
		,FBAddress1 = CF.Addr1
		,FBAddress2 = CF.Addr2
		,FBAddress3 = CF.Addr3
		,FBPhoneNum = CF.PhoneNUM1
		,FBRemarks = CF.Remarks
		,PHDesc = H.CQHotelListDescription
		,PHPhoneNum = H.PhoneNUM
		,PHREmarks = H.Remarks
		,PTDesc = T.CQTransportListDescription
		,PTPhoneNum = T.PhoneNUM
		,PTRemarks = T.Remarks
		,DepCtrDesc = C.CQCateringListDescription
		,DepCtrPhoneNum = C.PhoneNUM
		,DepCtrRemarks = C.Remarks
	FROM CQLeg CL	
	-- ARRIVAL FBO
	LEFT OUTER JOIN (
				SELECT CFL.CQLegID, CFL.AirportID, CF.FBOVendor, CF.Addr1, CF.Addr2, CF.Addr3, CF.PhoneNUM1, CF.Remarks 
				FROM CQFBOList CFL
				INNER JOIN FBO CF ON CFL.FBOID = CF.FBOID
				)CF ON CL.CQLegID = CF.CQLegID AND CL.AAirportID = CF.AirportID 

	LEFT OUTER JOIN (
				SELECT PH.CQLegID, PH.CQHotelListDescription, PH.PhoneNUM, H.Remarks
				FROM  CQHotelList PH 
				INNER JOIN Hotel H ON PH.HotelID = H.HotelID
				)H ON CL.CQLegID = H.CQLegID

	LEFT OUTER JOIN (
				SELECT CT.CQLegID, CT.AirportID, CT.CQTransportListDescription, CT.PhoneNUM, T.Remarks
				FROM CQTransportList CT 				
				INNER JOIN Transport T ON CT.TransportID = T.TransportID And CT.RecordType = 'TP'
				) T ON CL.CQLegID = T.CQLegID AND CL.AAirportID = T.AirportID  -- ARRIVAL Transport

	LEFT OUTER JOIN (
				SELECT CCL.CQLegID, CCL.AirportID, CCL.CQCateringListDescription, CCL.PhoneNUM, C.Remarks
				FROM CQCateringList CCL 
				INNER JOIN Catering C ON CCL.CateringID = C.CateringID
				)C ON CL.CQLegID = C.CQLegID	AND CL.DAirportID = C.AirportID -- ARRIVAL Catering		
				WHERE CL.CQLegID = CONVERT(BIGINT, @CQLegID)
				  AND CL.IsDeleted=0
END

--EXEC spGetReportCQRWItineraryLogisticsInformation ''




GO


