IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetScheduleDateAircraftInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetScheduleDateAircraftInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







         
CREATE PROCEDURE [dbo].[spGetReportPREFleetScheduleDateAircraftInformation]      
      
  @UserCD AS VARCHAR(30), --Mandatory      
  @DATEFROM AS DATETIME, --Mandatory      
  @DATETO AS DATETIME, --Mandatory      
  @TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values      
  @FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values      
  --,@SortBy AS CHAR(10) = 'TailNum' -- 'CREW' FOR Crew Calendar Crew / 'DATE' for Crew Calendar Date      
AS      
-- =============================================      
-- SPC Name: spGetReportPREFleetScheduleDate/AircraftInformation      
-- Author: SINDHUJA.K      
-- Create date: 10 Junly 2012      
-- Description: Get Preflight Fleet Schedule/Date/Aircraft information for REPORTS      
-- Revision History      
-- Date  Name  Ver  Change      
--       
-- =============================================      
  BEGIN

      SET NOCOUNT ON
      


      DECLARE @TenToMin INT
      DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

      
      SET @TenToMin= (SELECT COM.TimeDisplayTenMin
                      FROM   Company COM
                             JOIN UserMaster UM
                               ON COM.CustomerID = UM.CustomerID
                                  AND COM.HomebaseID = UM.HomebaseID
                      WHERE  UM.UserName = @UserCD)

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	---LEFT OUTER JOIN FleetGroupOrder FGO
	--ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	--LEFT OUTER JOIN FleetGroup FG 
	--ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	---AND 
	F.CustomerID = @CUSTOMERID 
	AND F.IsDeleted=0 
	AND IsInActive = 0
END
-----------------------------TailNum and Fleet Group Filteration----------------------

      --Modified By Bala Ends
      DECLARE @BeginDate DATETIME
      DECLARE @EndDate DATETIME
      DECLARE @FromDate DATETIME
      DECLARE @ToDate DATETIME
      DECLARE @Count INT
      DECLARE @index INT
      DECLARE @NextString NVARCHAR(40)
      DECLARE @Pos INT
      DECLARE @NextPos INT
      DECLARE @String NVARCHAR(40)
      DECLARE @Delimiter NVARCHAR(40)
      DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
      DECLARE @ParameterDefinition AS NVARCHAR(100)

     
 

  
        BEGIN
         SET @FromDate = @DATEFROM
            SET @ToDate = @DATETO
            SET @BeginDate=@FromDate
            SET @EndDate=@ToDate
        END
 
      -- [Start] Storing given Tail Numbers [User Ipnut] in to Temp tables --------------
      CREATE TABLE #TempTailNum
        (
           ID                INT NOT NULL IDENTITY (1, 1),
           activedate        DATE,
           tail_nmbr         VARCHAR(20),
           FleetID           BIGINT,
           DepartArrive      DATETIME,
           icao_id           CHAR(4),
           city              VARCHAR(50),
           airport           VARCHAR(50),
           state             VARCHAR(50),
           country           VARCHAR(80),
           elp_time          VARCHAR(40),
           [desc]            VARCHAR(120),
           pax_total         INT,
           orig_nmbr         BIGINT,
           legid             BIGINT,
           paxno             INT,
           paxname           VARCHAR(60),
           HomeBased          VARCHAR(25),
           DAType            CHAR(1),
           Type_Code         CHAR(4),
           HomebaseAirportID BIGINT,
           AirportID         BIGINT,
           SortDepart DATETIME
        )

  
      DECLARE @TblDates TABLE (AllDates DATE);

      WITH dates(DATEPARAM)
           AS (SELECT @FromDate AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @ToDate)
      INSERT INTO @TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
      
     


      -- This will never return records  	
      --DELETE FROM #TempTailNum
      --WHERE  FleetID NOT IN((SELECT DISTINCT PM.FleetID
      --                       FROM   PreflightMain PM
      --                              INNER JOIN #TempTailNum T
      --                                      ON PM.FleetID = T.FleetID
      --                       WHERE  CONVERT(DATE, T.activedate) BETWEEN
      --                              CONVERT(DATE, @DateFrom) AND
      --                                       CONVERT(DATE, @DateTo)
      --                              AND PM.CustomerID =dbo.Getcustomeridbyusercd(Ltrim(@UserCD))))
                                    


	DECLARE @TempTable TABLE
	(
	ID           INT NOT NULL IDENTITY (1, 1),
	activedate   DATE,
	tail_nmbr    VARCHAR(20),
	FleetID      BIGINT,
	DepartArrive DATETIME,
	icao_id      CHAR(4),
	city         VARCHAR(50),
	airport      VARCHAR(50),
	state        VARCHAR(50),
	country      VARCHAR(80),
	elp_time     VARCHAR(40),
	[desc]       VARCHAR(120),
	pax_total    INT,
	HomeBased    VARCHAR(25),
	DAType       CHAR(1),
	Type_Code    CHAR(4),
	DutyType CHAR(3),
	RecordType CHAR(1),
	SortDepart DATETIME
	)

	INSERT INTO @TempTable
	SELECT TEMPFLEET.*	FROM   
			(SELECT DISTINCT PL.DepartureDTTMLocal         ACTIVEDATE,
			F.TailNum,
			F.FleetID,
			PL.DepartureDTTMLocal         DEPARTARRIVE,
			A.IcaoID                      AS DEPICAO_ID,
			A.CityName,
			A.AirportName,
			A.StateName,
			A.CountryName,
			CONVERT(VARCHAR(40), PL.ElapseTM) ELP_TIME,
			PM.TripDescription,
			PL.PassengerTotal             AS PAX_TOTAL,
			'' HomeBased,
			'D'                           DATYPE,
			F.AircraftCD,
			PL.DutyTYPE,
			PM.RecordType,
			PL.DepartureDTTMLocal
			FROM   PreflightMain PM
			INNER JOIN PreflightLeg PL
			ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
			INNER JOIN (SELECT DISTINCT F.CustomerID,
					 F.FleetID,
					 F.TailNUM,
					 F.AircraftCD,
					 F.HomebaseID
					FROM    ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1
					 INNER JOIN Fleet F ON F1.FleetID=F.FleetID
					LEFT OUTER JOIN FleetGroupOrder FGO
								 ON F.FleetID = FGO.FleetID
									AND	F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG
								 ON FGO.FleetGroupID =
									FG.FleetGroupID
									AND	FGO.CustomerID = FG.CustomerID
					--WHERE  FG.FleetGroupCD IN (SELECT DISTINCT Rtrim(S)FROM	dbo.Splitstring(@FleetGroupCD, ','	))
					-- OR @FleetGroupCD = ''
					 ) F
			ON PM.FleetID = F.FleetID
			AND PM.CustomerID = F.CustomerID
			INNER JOIN AIRPORT A
			ON PL.DepartICAOID = A.AirportID
			WHERE  F.CustomerID = dbo.Getcustomeridbyusercd(Ltrim(@UserCD))
	        AND TripStatus IN( 'T', 'H' )
			AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE, @BeginDate) AND CONVERT(DATE, @EndDate)
			AND PM.IsDeleted = 0
			--AND ( F.TailNum IN (SELECT DISTINCT Rtrim(S)FROM   dbo.Splitstring(@TailNum, ','))
			--	OR @TailNum = '' )
	UNION ALL
	SELECT DISTINCT PL.DepartureDTTMLocal ACTIVEDATE,
			F.TailNum,
			F.FleetID,
			PL.ArrivalDTTMLocal,
			A.IcaoID              AS DEPICAO_ID,
			( CASE( PM.RecordType )
			WHEN 'M' THEN ( CASE PL.DutyTYPE WHEN 'MX' THEN 'MAINTENANCE' ELSE A.CityName END )	
					ELSE A.CityName 	END ),
			A.AirportName,
			A.StateName,
			A.CountryName,
			PM.TripDescription    AS ELP_TIME,
			PM.TripDescription,
			PL.PassengerTotal     AS PAX_TOTAL,
		    '' HomeBased,
			'A'                   DATYPE,
			F.AircraftCD,
			PL.DutyTYPE,
			PM.RecordType,
			PL.DepartureDTTMLocal
	FROM   PreflightMain PM
	INNER JOIN PreflightLeg PL
	ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
	INNER JOIN (SELECT DISTINCT F.CustomerID,
						 F.FleetID,
						 F.TailNUM,
						 F.AircraftCD,
						 F.HomebaseID
				FROM   ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1
					 INNER JOIN Fleet F ON F1.FleetID=F.FleetID
				LEFT OUTER JOIN FleetGroupOrder FGO
							 ON F.FleetID = FGO.FleetID
								AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG
							 ON FGO.FleetGroupID =
								FG.FleetGroupID
								AND	FGO.CustomerID = FG.CustomerID
				--WHERE  FG.FleetGroupCD IN (SELECT DISTINCT Rtrim(S)FROM dbo.Splitstring(@FleetGroupCD, ','))				 
				--OR @FleetGroupCD = ''
				) F
	ON PM.FleetID = F.FleetID
	AND PM.CustomerID = F.CustomerID
	INNER JOIN AIRPORT A
	ON PL.ArriveICAOID = A.AirportID
	WHERE  F.CustomerID = dbo.Getcustomeridbyusercd(Ltrim(@UserCD))
	AND TripStatus IN( 'T', 'H' )
	AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN	CONVERT(DATE, @BeginDate) AND CONVERT(DATE, @EndDate)
	AND PM.IsDeleted = 0
	--AND ( F.TailNum IN (SELECT DISTINCT Rtrim(S) FROM   dbo.Splitstring(@TailNum, ',')) 	OR @TailNum = '' )
	)TEMPFLEET


	------------------------ Maintenance Start--------------------------------------------------

					

				

DELETE FROM @TempTable WHERE RecordType='M'


	DECLARE @DepArrive DATE,@CrDesc VARCHAR(40),@CrDepart DATETIME,@CrArrive DATETIME,@CrDutyType VARCHAR(50),@CrSortDepart DATETIME;
	DECLARE @CrTailNumber   VARCHAR(9),
	@Cractivedate   DATETIME,
	@CrDAType       CHAR(1),
	@CrDepartArrive DATETIME,
	@Cricao_id      CHAR(4),
	@CrHomeBased    VARCHAR(25),
	@Crcity         VARCHAR(50),
	@Crelp_time     VARCHAR(40),
	@PrevDate       DATETIME,
	@DATE           INT,
	@Prevloc        CHAR(4),
	@PrevDAType     CHAR(1),
	@PrevDepart     DATETIME,
	@CrType_Code    CHAR(4),
	@activedate     DATETIME,@CrArrivalDTTMLocal1 DATETIME;
	DECLARE  @CrState VARCHAR(50),@CrCountry VARCHAR(80),@CrAirport VARCHAR(50),@CrPax INT;

	SET @Count=0;
	

	DECLARE c1 CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT F.TailNum,
	       CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) < CONVERT(DATE,@DATEFROM) THEN @DATEFROM  ELSE PL.DepartureDTTMLocal END DepartureDTTMLocal,
		   CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) > CONVERT(DATE,@DATETO) THEN @DATETO  ELSE PL.ArrivalDTTMLocal END ArrivalDTTMLocal,
		   A.IcaoID,
		   A.CityName,
		   PM.TripDescription,
		   F.AircraftCD,
		   PL.DutyType,
		   CASE WHEN PL.DepartureDTTMLocal < @DATEFROM THEN @DATEFROM  ELSE PL.DepartureDTTMLocal END DepartureDTTMLocal,
			A.StateName,
			A.CountryName,
			A.AirportName,
		   PL.PassengerTotal 
	     FROM PreflightMain PM inner join PreflightLeg PL on PM.TripID=PL.TripID AND PL.ISDELETED = 0
             INNER JOIN Fleet F ON F.FleetID=PM.FleetID
             INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID )F1 ON F1.FLEETID=F.FLEETID
             OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
	WHERE PM.RecordType='M'
	  AND PL.DutyType <>'R'
      AND PM.CustomerID=dbo.Getcustomeridbyusercd(Ltrim(@UserCD))
      AND PM.ISDELETED = 0

      AND (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
	--ORDER  BY F.TailNum,PL.DepartureDTTMLocal DESC


	OPEN c1

	FETCH NEXT FROM c1 INTO @CrTailNumber,@CrDepart,@CrArrive,@Cricao_id,@Crcity,@Crelp_time,@CrType_Code,@CrDutyType,@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax;

	WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @DATE=(SELECT DATEDIFF(DAY,@CrDepart,@CrArrive))
	SET @activedate=@CrDepart
     SET @CrDutyType= (SELECT (CASE (@CrDutyType) WHEN 'MX' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='MX'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'F' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='F'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'TR' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='TR'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'BJ' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='BJ'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'G' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='G'  AND CustomerID=@CUSTOMERID) END ))
    
	WHILE(@Count<=@DATE)
	    
		BEGIN
		
	 IF @Count=@DATE AND @Count <> 0
	
	 BEGIN  SET @CrArrivalDTTMLocal1=@CrArrive END
	 ELSE BEGIN SET @CrArrivalDTTMLocal1=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepart, 101) + ' 23:59') END
	 
		   INSERT INTO @TempTable(tail_nmbr,DepartArrive,city,elp_time,activedate,Type_Code,DAType,SortDepart,state,country,airport,pax_total,icao_id) 
			VALUES (@CrTailNumber,@CrDepart,@Crcity,'0.0',@activedate,@CrType_Code,'D',@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax,@Cricao_id)      

			INSERT INTO @TempTable(tail_nmbr,DepartArrive,city,elp_time,activedate,Type_Code,DAType,SortDepart,state,country,airport,pax_total) 
			VALUES (@CrTailNumber,@CrArrivalDTTMLocal1,@CrDutyType,@Crelp_time,@activedate,@CrType_Code,'A',@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax)                 
		    
		    SET @CrDepart=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepart, 101) + ' 00:00')+1
			SET @Count=@Count+1 
			SET @activedate=@activedate+1

		END


	SET @Count=0


	
	FETCH NEXT FROM c1 INTO @CrTailNumber,@CrDepart,@CrArrive,@Cricao_id,@Crcity,@Crelp_time,@CrType_Code,@CrDutyType,@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax;
	END

	CLOSE c1;

	DEALLOCATE c1;




	---- ----------------------Maintenance End-------------------------------------
	


 	


      INSERT INTO #TempTailNum
                  (activedate,tail_nmbr,FleetID,HomebaseAirportID,Type_Code,SortDepart)
      SELECT DISTINCT AllDates AS ACTIVEDATE,
				F.TailNum,F.FleetID,HomebaseAirportID,F.AircraftCD,AllDates
			  FROM   (SELECT DISTINCT F.CustomerID,
                              F.FleetID,
                              F.TailNUM,
                              F.AircraftCD,
                              F.HomebaseID
              FROM   ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1
                     INNER JOIN Fleet F  ON F1.FleetID = F.FleetID
                     LEFT OUTER JOIN FleetGroupOrder FGO
                                  ON F.FleetID = FGO.FleetID
                                     AND F.CustomerID = FGO.CustomerID
                     LEFT OUTER JOIN FleetGroup FG
                                  ON FGO.FleetGroupID = FG.FleetGroupID
                                     AND FGO.CustomerID = FG.CustomerID
                      ) F
             INNER JOIN Company C
                     ON F.HomebaseID = C.HomebaseID
             CROSS JOIN @TblDates
             	WHERE NOT EXISTS(SELECT T.tail_nmbr FROM @TempTable T 
					WHERE T.tail_nmbr = F.TailNum AND T.activedate = AllDates )
            AND  F.CustomerID = dbo.Getcustomeridbyusercd(Ltrim(@UserCD));
             

      WITH PreflightCTE
           AS (SELECT CONVERT(DATE, ArrivalDTTMLocal) AS ARRIVALDTTMLOCAL,
				INTMP.activedate,PL.ArriveICAOID,PM.FleetID,
				ROW_NUMBER()OVER(partition BY PM.FleetID, INTMP.activedate 
									ORDER BY PL.ArrivalDTTMLocal DESC) AS RK
               FROM   PreflightLeg PL
                      INNER JOIN PreflightMain PM
                              ON PL.TripID = PM.TripID AND PL.IsDeleted = 0
                      JOIN #TempTailNum INTMP
                        ON PM.FleetID = INTMP.FleetID
                           AND CONVERT(DATE, ArrivalDTTMLocal) <=
                               CONVERT(DATE, INTMP.activedate)
                           AND PM.TRIPSTATUS IN ('T','H') )
      UPDATE #TempTailNum
      SET    AirportID = S.ArriveICAOID
      FROM   #TempTailNum OUTTMP
             JOIN PreflightCTE S
               ON S.FleetID = OUTTMP.FleetID
                  AND S.activedate = OUTTMP.activedate
      WHERE  S.rk = 1

      UPDATE #TempTailNum
      SET    AirportID = C.HOMEBASEAIRPORTID
      FROM   #TempTailNum TMP
             JOIN FLEET F
               ON F.FleetId = TMP.FleetID
             INNER JOIN COMPANY C
                     ON F.HOMEBASEID = C.HOMEBASEID
      WHERE  AirportID IS NULL

      UPDATE #TempTailNum
      SET    icao_id = A.IcaoID,
             airport = A.AirportName,
             state = A.StateName,
             country = A.CountryName,
             city = A.CityName,
             HomeBased = CASE
                           WHEN TMP.HomebaseAirportID = A.AirportID THEN
                           'AIRCRAFT AT HOME BASE'
                           ELSE 'AIRCRAFT AWAY OVERNIGHT'
                         END,
             DAType = 'D',
             elp_time = '0.0'
      FROM   #TempTailNum TMP
             JOIN AIRPORT A
               ON A.AIRPORTid = TMP.AirportID




	 SELECT  A.*,CASE WHEN @TailNum <> '' THEN 1 ELSE 0 END TailFilter FROM 
	(SELECT tail_nmbr TailNumber,dbo.GetShortDateFormatByUserCD(@UserCD,activedate) [PDate],
			activedate [Date],DAType,DepartArrive,icao_id,'' HomeBased,city,airport,SUBSTRING(datename(dw,activedate),1,3) [DateName],
			state,country,
			elp_time [EstTimeEnroutePurpose],
			pax_total Pax,Type_Code,
			(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange],
			@TenToMin TenToMin,
			SortDepart
			FROM @TempTable T1

	UNION ALL

	SELECT  tail_nmbr TailNumber,dbo.GetShortDateFormatByUserCD(@UserCD,activedate) [PDate],
			activedate [Date],DAType,
			DepartArrive,
			icao_id,HomeBased,city,airport,SUBSTRING(datename(dw,activedate),1,3) [DateName],
			state,country,
			elp_time [EstTimeEnroutePurpose],
			pax_total Pax,Type_Code,
			(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange],
			@TenToMin TenToMin,
			SortDepart
			FROM #TempTailNum T
	)A
	WHERE (CONVERT(DATE,DepartArrive) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR DepartArrive IS NULL)
	AND [Date] BETWEEN  CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	ORDER BY Type_Code,Date,SortDepart,A.DAType DESC--,DepartArrive--,DAType DESC
	
IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE	#TempTailNum

  END    
  
	--[GetAircraftCurrentAirpotID]          
	-- EXEC spGetReportPREFleetScheduleDateAircraftInformation 'JWILLIAMS_11','2012-05-16','2012-10-16','n46e',''      
	-- EXEC spGetReportPREFleetScheduleDateAircraftInformation 'JWILLIAMS_13','2011-07-05','2012-07-20','',''      



GO


