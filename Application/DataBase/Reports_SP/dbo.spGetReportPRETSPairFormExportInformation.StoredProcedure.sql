IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPairFormExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPairFormExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPairFormExportInformation]
     @UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --  PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
    AS
	BEGIN
-- =======================================================================
-- SPC Name: spGetReportPRETSPairFormExportInformation
-- Author: Aishwarya.M
-- Create date: 12 OCT 2012
-- Description: Get Preflight Tripsheet Pair Form information for REPORTS  
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =======================================================================
-- ======================================================================================  
-- REQUIREMENT:
---------------
-- Displays Legs that has Arrival ICAO ID in US and 
-- Departure ICAO ID is International (International Leg - other than US)
-- A Separate Preview and XL report for each Leg in the trip that satisfies above criteria
-- =======================================================================================

SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
    TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	, PICID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	UPDATE @tblTripInfo
	SET PICID = ( 
			SELECT TOP 1 PC.CREWID 
			FROM PreflightCrewList PC
			INNER JOIN @tblTripInfo TI ON PC.LegID = TI.LegID
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND DutyTYPE = 'P'
			ORDER BY C.FirstName ASC
	)
	
SELECT  DISTINCT
	         [dataforms] = 'FALSE'
			,[f1] = F.TailNum 
			,[f2] = FP.BuyAircraftAdditionalFeeDOM
			,[f3] = AA.IcaoID
			,[f4] = ''
			,[f5] = CONVERT(DATE,PL.ArrivalGreenwichDTTM) --'WIP' --(US Arrival Date)
			--,[f5] = CONVERT(varchar(10), PL.ArrivalGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))			
			,[f6] = AD.IcaoID
			,[f7] = ''
			,[f8] = CONVERT(DATE,PL.DepartureGreenwichDTTM) --'WIP' --(Departure Date)
			--,[f8] = CONVERT(varchar(10), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))			
			,[f9] = AD.CityName 
			,[f10] = CD.CountryCD --(Dep)
			,[f11] = REPLACE(dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))),'-','/') --(Foreign Itinerary) Created from tripleg ICAO fields
			,[f12] = PP2.AirportName 
			,[f13] = PP2.StateName 
			,[f14] = PP2.IcaoID 
			,[f15] = PP2.f15 --(Display only Time in 0501 format )
			,[f16] = PP2.f16 --(Display only Date in 082812 format )
			,[f17] = FP.AircraftMake
			,[f18] = FP.AircraftModel
			,[f19] = FP.AircraftColor1 + '/' + FP.AircraftColor2
			,[f20] = FP.AircraftTrimColor
			,[f21] = FP.OwnerLesse
			,[f22] = FP.Addr1
			,[f23] = ''
			,[f24] = FP.CityName
			,[f25] = FP.StateName
			,[f26] = CF.CountryCD --(Use FleetPair.CountryID)
			,[f27] = P.LastName
			,[f28] = P.FirstName
			,[f29] = FP.PostalZipCD
			,[f30] = P.BirthDT
			,[f31] = PP.CountryCD --PILOT NATIONALITY
			,[f32] = CPP.PilotLicenseNum
			,[f33] = P.Addr1
			,[f34] = ''
			,[f35] = P.CityName
			,[f36] = P.StateName
			,[f37] = CP.CountryCD --PILOT COUNTRY
			,[f38] = P.PostalZipCD
			,[f39] = C.LastName
			,[f40] = C.FirstName
			,[f41] = C.BirthDT
			,[f42] = CC.CountryCD --(Use CrewPassengerPassport.CountryID) of Passenger
			,[f43] = 'C'
			,PL.LegID
			FROM (SELECT DISTINCT TripID, LegID,PICID FROM @tblTripInfo) Main  
			INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID
			INNER JOIN Preflightleg PL ON Main.LegID = PL.LegID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Fleet F ON PM.FleetID = F.FleetID
			INNER JOIN FleetPair FP ON PM.CustomerID = FP.CustomerID AND F.FleetID = FP.FleetID
			INNER JOIN Crew P ON P.CrewID = Main.PICID
			INNER JOIN PreflightCrewList PCL ON Main.LegID = PCL.LegID
			INNER JOIN Crew C ON PCL.CrewID=C.CrewID AND C.CrewID <> MAIN.PICID
			INNER JOIN CrewPassengerPassport CPP ON P.CrewID = CPP.CrewID --AND PCL.PassportID = CPP.PassportID
			LEFT OUTER JOIN(SELECT DISTINCT CO.CountryCD,PL.LegID,PL.TripID, PCL.CREWID, PCL.PassportID,RANK() OVER (PARTITION BY   PCL.CrewID ORDER BY PCL.LegID   ASC) Rnk FROM @tblTripInfo PL   
                            INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
                            LEFT OUTER JOIN CrewPassengerPassport CPP ON PCL.CrewID = CPP.CrewID AND PCL.PassportID = CPP.PassportID
                            LEFT OUTER JOIN Country CO ON CPP.CountryID = CO.CountryID 
                            WHERE CPP.PassportID IS NOT NULL)PP ON PP.TripID=Main.TripID AND PP.CrewID=P.CrewID AND Rnk=1		
            LEFT OUTER JOIN(SELECT DISTINCT CPP.CrewID,PC.CrewFirstName,PC.CrewLastName,CO.CountryCD FROM PreflightCrewList PC 
                            JOIN CrewPassengerPassport CPP ON CPP.CrewID = PC.CrewID
                            JOIN Country CO ON CPP.countryid = CO.CountryID)CC ON CC.CrewID = C.CrewID                   
			LEFT OUTER JOIN Country CF ON FP.CountryID = CF.CountryID
			LEFT OUTER JOIN Country CP ON P.CountryID = CP.CountryID	
			LEFT OUTER JOIN (SELECT PL.TripID, AP.AirportName, AP.StateName,AP.IcaoID , [f15]= CONVERT(TIME,PL.DepartureGreenwichDTTM),[f16] = CONVERT(DATE, PL.DepartureGreenwichDTTM)  FROM PreflightLeg PL
	                         INNER JOIN Airport AP on PL.DepartICAOID = AP.AirportID
	                         WHERE PL.LegNUM = 1 and AP.CountryName = 'US'
                              )PP2 on main.tripid = PP2.tripid
                              		
			LEFT OUTER JOIN Country CD ON AD.CountryID = CD.CountryID
			LEFT OUTER JOIN Country CA ON AA.CountryID = CA.CountryID			
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND CA.CountryCD = 'US' AND CD.CountryCD <> 'US'	
		UNION ALL
		
	SELECT DISTINCT
	        [dataforms] = 'FALSE'
			,[f1] = F.TailNum 
			,[f2] = FP.BuyAircraftAdditionalFeeDOM
			,[f3] = AA.IcaoID
			,[f4] = ''
			,[f5] = CONVERT(DATE,PL.ArrivalGreenwichDTTM) --'WIP' --(US Arrival Date)
			--,[f5] = CONVERT(varchar(10), PL.ArrivalGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))			
			,[f6] = AD.IcaoID
			,[f7] = ''
			,[f8] = CONVERT(DATE,PL.DepartureGreenwichDTTM) --'WIP' --(Departure Date)
			--,[f8] = CONVERT(varchar(10), PL.DepartureGreenwichDTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))			
			,[f9] = AD.CityName 
			,[f10] = CD.CountryCD --(Dep)
			,[f11] = REPLACE(dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))),'-','/') --(Foreign Itinerary) Created from tripleg ICAO fields
			,[f12] = PP2.AirportName 
			,[f13] = PP2.StateName 
			,[f14] = PP2.IcaoID 
			,[f15] = PP2.f15 --(Display only Time in 0501 format )
			,[f16] = PP2.f16 --(Display only Date in 082812 format )
			,[f17] = FP.AircraftMake
			,[f18] = FP.AircraftModel
			,[f19] = FP.AircraftColor1 + '/' + FP.AircraftColor2
			,[f20] = FP.AircraftTrimColor
			,[f21] = FP.OwnerLesse
			,[f22] = FP.Addr1
			,[f23] = ''
			,[f24] = FP.CityName
			,[f25] = FP.StateName
			,[f26] = CF.CountryCD --(Use FleetPair.CountryID)
			,[f27] = P.LastName
			,[f28] = P.FirstName
			,[f29] = FP.PostalZipCD
			,[f30] = P.BirthDT
			,[f31] = PP.CountryCD --PILOT NATIONALITY
			,[f32] = CPP.PilotLicenseNum
			,[f33] = P.Addr1
			,[f34] = ''
			,[f35] = P.CityName
			,[f36] = P.StateName
			,[f37] = CP.CountryCD --PILOT COUNTRY
			,[f38] = P.PostalZipCD
			,[f39] = PAX.Lastname
			,[f40] = PAX.FirstName
			,[f41] = PAX.DateOfBirth
			,[f42] = PP1.CountryCD --(Use CrewPassengerPassport.CountryID) of Passenger
			,[f43] = 'P'
			,PL.LegID			
			FROM (SELECT DISTINCT TripID, LegID,PICID FROM @tblTripInfo) Main  
			INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID
			INNER JOIN Preflightleg PL ON Main.LegID = PL.LegID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Fleet F ON PM.FleetID = F.FleetID
			INNER JOIN FleetPair FP ON PM.CustomerID = FP.CustomerID AND F.FleetID = FP.FleetID
			INNER JOIN Crew P ON P.CrewID = MAIN.PICID
			INNER JOIN PreflightCrewList PCL ON Main.LegID = PCL.LegID AND P.CrewID = PCL.CrewID
			INNER JOIN PreflightPassengerList PPL ON Main.LegID = PPL.LegID
			INNER JOIN Passenger PAX ON PPL.PassengerID = PAX.PassengerRequestorID
			INNER JOIN CrewPassengerPassport CPP  ON PCL.CrewID = CPP.CrewID AND PCL.PassportID = CPP.PassportID
			
			LEFT OUTER JOIN ( 
				SELECT PassengerRequestorID, CPP2.PassportID, PPL.LegID, PP1.CountryCD 
				FROM CrewPassengerPassport CPP2
				INNER JOIN PreflightPassengerList PPL ON CPP2.PassengerRequestorID = PPL.PassengerID AND CPP2.PassportID = PPL.PassportID
				INNER JOIN Country PP1 ON CPP2.CountryID = PP1.CountryID 
				--WHERE PPL.LegID = 10099123638              
			) PP1 ON PAX.PassengerRequestorID = PP1.PassengerRequestorID AND PL.LegID = PP1.LegID
	
			LEFT OUTER JOIN Country PP ON CPP.CountryID = PP.CountryID
			LEFT OUTER JOIN Country CF ON FP.CountryID = CF.CountryID
			LEFT OUTER JOIN Country CP ON P.CountryID = CP.CountryID
			
			LEFT OUTER JOIN (SELECT PL.TripID, AP.AirportName, AP.StateName,AP.IcaoID , [f15]= CONVERT(TIME,PL.DepartureGreenwichDTTM),[f16] = CONVERT(DATE,PL.DepartureGreenwichDTTM)  FROM PreflightLeg PL
	                         INNER JOIN Airport AP on PL.DepartICAOID = AP.AirportID
	                         WHERE PL.LegNUM = 1 and AP.CountryName = 'US'
                              )PP2 on main.tripid = PP2.tripid
			LEFT OUTER JOIN Country CD ON AD.CountryID = CD.CountryID
			LEFT OUTER JOIN Country CA ON AA.CountryID = CA.CountryID			
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND CA.CountryCD = 'US' AND CD.CountryCD <> 'US'

	END
	--EXEC spGetReportPRETSPairFormExportInformation 'TIM','100','','','','','','','','','','','',''


GO


