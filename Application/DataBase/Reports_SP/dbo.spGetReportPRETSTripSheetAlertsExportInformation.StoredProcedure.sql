IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSheetAlertsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSheetAlertsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[spGetReportPRETSTripSheetAlertsExportInformation]  
   @UserCD AS VARCHAR(30)  
     ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
  ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
  ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
  ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
  ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal   
  ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
  ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
  ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
  ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
  ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
  ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
  ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
  ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
  ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
  ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
  ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
  ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
  ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
  ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
 AS  
  BEGIN  
-- =============================================  
-- SPC Name: spGetReportPRETSTripSheetAlertsExportInformation  
-- Author: SINDHUJA.K  
-- Create date: 24 July 2012  
-- Description: Get Preflight Tripsheet Alerts Export information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- =============================================  
  
--DECLARE @tblTripID TABLE (  
-- TripID BIGINT   
--)  
-- [Start] Report Criteria --  
 DECLARE @tblTripInfo AS TABLE (  
 TripID BIGINT  
 , LegID BIGINT  
 , PassengerID BIGINT  
 )  
  
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
  
  --get Application date formate
  DECLARE @ApplicationDateFormat VARCHAR(25)
  DECLARE @DateFormat AS INT
  SELECT @ApplicationDateFormat=Upper(ApplicationDateFormat) FROM Company 
WHERE HomeBaseID = dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))

IF @ApplicationDateFormat='AMERICAN'
BEGIN
SET @DateFormat= 1 
END
ELSE IF @ApplicationDateFormat='ANSI'
BEGIN
SET @DateFormat= 2
END
ELSE IF @ApplicationDateFormat='BRITISH' 
BEGIN
SET @DateFormat=3
END
ELSE IF @ApplicationDateFormat='FRENCH'
BEGIN
SET @DateFormat= 3
END
ELSE IF @ApplicationDateFormat='GERMAN'
BEGIN
SET @DateFormat= 4 
END 
ELSE IF @ApplicationDateFormat='ITALIAN'
BEGIN
SET @DateFormat= 5
END 
ELSE IF @ApplicationDateFormat='JAPAN'
	BEGIN
	   SET @DateFormat= 11
	END  
	ELSE IF @ApplicationDateFormat='TAIWAN' 
	BEGIN
	   SET @DateFormat= 11
	END
	ELSE IF @ApplicationDateFormat='DMY'
	BEGIN
	  SET @DateFormat= 3
	END
	ELSE IF @ApplicationDateFormat='MDY'
	BEGIN
	  SET @DateFormat= 1
	END
	ELSE IF @ApplicationDateFormat='YMD'
	BEGIN
	  SET @DateFormat= 11
	END 
	ELSE 
	BEGIN
	    SET @DateFormat= 1
	END   
	
   SELECT DISTINCT --PL.LEGID,  
        [orig_nmbr] = PM.TripNUM  
  ,[lowdate] = CONVERT(DATE,SUBSTRING(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),1,CHARINDEX('-',dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),1)-1),@DateFormat)
  ,[highdate] = CONVERT(DATE,SUBSTRING(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),CHARINDEX('-',dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),1)+1,11),@DateFormat)
  ,[tail_nmbr] = F.TailNUM  
  --,[type_code] = PM.AircraftID  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  --,[paxname] = PM.RequestorLastName+ '' +PM.RequestorFirstName+ '' +PM.RequestorMiddleName  
  --,[paxname] = P.LastName + ',' + P.FirstName + ' ' + P.MiddleInitial  
  ,[paxname] = ISNULL(P.LastName,'') + ',' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
  --,[reqcode] = PM.PassengerRequestorID  
  ,[reqcode] = P.PassengerRequestorCD  
  ,[phone] = P.PhoneNUM  
  ,[crewcode] = C.CrewCD 
  ,[releasedby] = C.LastName + ',' + C.FirstName + ' ' + C.MiddleInitial
  ,[verifynmbr] = PM.VerifyNUM  
  ,[DispatchNo] = PM.DispatchNUM  
  ,[NOTES]=PM.TripSheetNotes
  ,[ALERTS] = PM.Notes
  FROM (SELECT DISTINCT TRIPID FROM @tblTripInfo) M  
   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID  
   --INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID  
   INNER JOIN  Fleet F ON PM.FleetID = F.FleetID 
   INNER JOIN  Aircraft A ON PM.AircraftID = A.AircraftID    
   LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID   
	LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID
   --INNER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID  
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))   
   --AND UM.UserName = @UserCD  
   --AND PM.TripID =   
        END   
          
         --EXEC spGetReportPRETSTripSheetAlertsExportInformation 'UC', '', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''  
         --EXEC spGetReportPRETSTripSheetAlertsExportInformation 'supervisor_99', '1992', '', '', '', '', '', '', '', '', '', '', ''  
  



GO


