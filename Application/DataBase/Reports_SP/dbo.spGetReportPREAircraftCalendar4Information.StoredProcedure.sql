IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar4Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar4Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar4Information] 
	 @UserCD VARCHAR(30) --Mandatory
	,@DATEFROM DATETIME  --Mandatory
	,@DATETO DATETIME    --Mandatory
	,@TailNum VARCHAR(500) = '' --Optional
	,@FleetGroupCD VARCHAR(500) = '' --Optional
AS
--==================================================================
--SPC Name: spGetReportPREAircraftCalendar4Information
--AUTHOR: AISHWARYA.M
--Create Date: 11/07/2012
--DESCRIPTION: Get Aircraft Calendar information for REPORTS
--Revision History
--Date			Name		Ver		Change
--
--==================================================================
SET NOCOUNT ON




 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
 

Declare @SuppressActivityAircft BIT  
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMER
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

 DECLARE @CrDepartureDTTMLocal DATETIME,@CrTailNumber VARCHAR(30),@Cricao_id CHAR(4),@CrDutyTYPE CHAR(2),@crTripDescription VARCHAR(40),@CrArrivalDTTMLocal DATETIME;
DECLARE @DATE1 INT,@Count INT=0,@activedate DATETIME,@CrArrivalDTTMLocal1 DATETIME,@CrAircraftCD CHAR(4),@CrIsApproxTM BIT;
DECLARE @DATElOC DATE;
            
Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)
DECLARE @Loopdate DATETIME


-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		HomeBaseID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.HomebaseID
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.HomebaseID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.HomebaseID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
    WHERE F.CustomerID = @CUSTOMER
    AND F.IsInActive=0
	 AND F.IsDeleted=0   
END
-----------------------------TailNum and Fleet Group Filteration----------------------
CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,TailNum VARCHAR(25)
	,AircraftID BIGINT
	,Ac_Code VARCHAR(4)
	,FleetID BIGINT 
	)

DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM
	  --CREATE TABLE #TblDates  (AllDates DATE);

   --   WITH dates(DATEPARAM)
   --        AS (SELECT @DATEFROM AS DATETIME
   --            UNION ALL
   --            SELECT Dateadd(day, 1, DATEPARAM)
   --            FROM   Dates
   --            WHERE  DATEPARAM < @DATETO)
   --   INSERT INTO #TblDates (AllDates)
   --   SELECT DATEPARAM
   --   FROM   Dates
   --   OPTION (maxrecursion 10000)


	
	
CREATE TABLE #TempTable  (
	activedate DATETIME
	,FlightCrew VARCHAR(500)
	,TailNumber VARCHAR(9)
	,[Route] VARCHAR(50)
	,[ScheduledPassengers] VARCHAR(500)
	,CustomerID BIGINT
	,[NextLocalDTTM] DATETIME
	,DepartureDTTMLocal DATETIME
	,ArrivalDTTMLocal DATETIME
	,DIcaoID CHAR(4)
	,AIcaoID CHAR(4)
	,Duty CHAR(2)
	,FleetID BIGINT
	,TripID BIGINT
	,LegID BIGINT
	,AircraftCD VARCHAR(10)
	,Type_Code VARCHAR(4)
	,LegNum BIGINT
	,TripNum BIGINT
	)
	

	
			
	INSERT INTO #TempTailNum (TailNum, AircraftID,Ac_Code,FleetID)
			SELECT DISTINCT F.TailNum, F.AircraftID,F.AircraftCD,F.FleetID
			FROM Fleet F 
			INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	





		INSERT INTO #TempTable
		(
		 	activedate 
			,[FlightCrew]
			,[TailNumber] 
			,[Route] 
			,[NextLocalDTTM] 
			,DepartureDTTMLocal 
			,ArrivalDTTMLocal
			,DIcaoID
			,AIcaoID
			,FleetID
			,CustomerID
			,Duty
			,TripID
			,LegID
			,AircraftCD
			,Type_Code 
			,LegNum
			,TripNum
		) 
		SELECT CONVERT(DATE,PL.DepartureDTTMLocal)
			,SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1)
			,TEMP.TailNum
			,A.IcaoID + '  ' + AA.IcaoID 
			,PL.NextLocalDTTM 
			,PL.DepartureDTTMLocal
			,PL.ArrivalDTTMLocal
			,A.IcaoID
			,AA.IcaoID
			,F.FleetID
			,F.CustomerID
			,PL.DutyTYPE
			,PM.TripID
			,PL.LegID
			,AR.AircraftCD
			,F.AircraftCD
			,PL.LegNum
			,PM.TripNUM
		FROM PreFlightMain PM
		INNER JOIN PreFlightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN Airport A ON A.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
       INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum,F.AircraftCD,AircraftID
			FROM Fleet F 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
		LEFT OUTER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) AR ON F.AircraftID = AR.AircraftID
		INNER JOIN #TempTailNum TEMP ON TEMP.TailNum = F.TailNum
		INNER JOIN (
			SELECT DISTINCT PC1.LEGID, CrewList = (
				SELECT RTRIM(C.LastName) + ' /' 
				FROM PreflightCrewList PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
				WHERE PC.LEGID = PC1.LEGID 
				ORDER BY C.LastName DESC
				FOR XML PATH('')
			)
			FROM PreflightCrewList PC1
		) Crew ON PL.LEGID = Crew.LEGID 
		WHERE  (	
					(CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.NextLocalDTTM) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.NextLocalDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.NextLocalDTTM) <= CONVERT(DATE,@DATETO))
			 )	
		AND F.CustomerID = @CUSTOMER
		AND PM.TripStatus IN('T','H')
		AND PM.RecordType='T'
		AND PM.IsDeleted = 0


		INSERT INTO #TempTable
		(
		 	activedate 
			,[FlightCrew]
			,[TailNumber] 
			,[Route] 
			,[NextLocalDTTM] 
			,DepartureDTTMLocal 
			,ArrivalDTTMLocal
			,DIcaoID
			,AIcaoID
			,FleetID
			,CustomerID
			,Duty
			,TripID
			,LegID
			,AircraftCD
			,Type_Code 
			,LegNum
			,TripNum
		) 
		SELECT CONVERT(DATE,PL.DepartureDTTMLocal)
			,NULL
			,TEMP.TailNum
			,A.IcaoID + '  ' + AA.IcaoID 
			,PL.NextLocalDTTM 
			,PL.DepartureDTTMLocal
			,PL.ArrivalDTTMLocal
			,A.IcaoID
			,AA.IcaoID
			,F.FleetID
			,F.CustomerID
			,PL.DutyTYPE
			,PM.TripID
			,PL.LegID
			,AR.AircraftCD
			,F.AircraftCD
			,PL.LegNum
			,PM.TripNUM
		FROM PreFlightMain PM
		INNER JOIN PreFlightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN Airport A ON A.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
       INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum,F.AircraftCD,AircraftID
			FROM Fleet F 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
		LEFT OUTER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) AR ON F.AircraftID = AR.AircraftID
		INNER JOIN #TempTailNum TEMP ON TEMP.TailNum = F.TailNum
		WHERE F.CustomerID = @CUSTOMER
		AND PM.RecordType='M'
		AND PL.IsDeleted = 0
		 AND (	
					(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
			 )	
--SELECT DISTINCT TripNum,activedate,FlightCrew,TailNumber,Route,ScheduledPassengers,CustomerID,NextLocalDTTM,
--	       DepartureDTTMLocal,ArrivalDTTMLocal,DIcaoID,AIcaoID,FleetID,TripID,LegID,AircraftCD,Type_Code,LegNum FROM #TempTable 
--	       ORDER BY TripID,legnum	


DECLARE @cractivedate DATETIME
	,@crFlightCrew VARCHAR(500)
	--,@crTailNumber CHAR(6)
	,@crRoute VARCHAR(50)
	,@crScheduledPassengers VARCHAR(500)
	,@crCustomerID BIGINT
	,@crNextLocalDTTM DATETIME
	--,@crDepartureDTTMLocal DATETIME
	--,@crArrivalDTTMLocal DATETIME
	,@crDIcaoID CHAR(4)
	,@crAIcaoID CHAR(4)
	,@crDuty CHAR(2)
	,@crFleetID BIGINT
	,@crTripID BIGINT
	,@crLegID BIGINT
	--,@crAircraftCD CHAR(10)
	,@crType_Code VARCHAR(4)
	,@crLegNum BIGINT,@PrevtTripId BIGINT,@CrTripNum BIGINT;
	

	 
	
DECLARE C1 CURSOR  FAST_FORWARD READ_ONLY FOR SELECT DISTINCT activedate,FlightCrew,TailNumber,Route,ScheduledPassengers,CustomerID,NextLocalDTTM,
	       DepartureDTTMLocal,ArrivalDTTMLocal,DIcaoID,AIcaoID,FleetID,TripID,LegID,AircraftCD,Type_Code,LegNum,TripNum FROM #TempTable ORDER BY TripID,legnum

OPEN C1

FETCH NEXT FROM C1 INTO @cractivedate,@crFlightCrew,@crTailNumber,@crRoute,@crScheduledPassengers,@crCustomerID,@crNextLocalDTTM,@crDepartureDTTMLocal,@crArrivalDTTMLocal,@crDIcaoID,@crAIcaoID,@crFleetID,@crTripID,@crLegID,@crAircraftCD,@crType_Code,@crLegNum,@CrTripNum;
WHILE @@FETCH_STATUS = 0
BEGIN


IF((@PrevtTripId <> @crTripID OR @PrevtTripId IS NULL) AND @crLegNum > 1)

BEGIN
SELECT @CrArrivalDTTMLocal=PL.ArrivalDTTMLocal,@crNextLocalDTTM=PL.NextLocalDTTM 
      FROM #TempTable T INNER JOIN PreflightLeg PL ON T.TripID=PL.TripID AND T.LegNum-1=PL.LegNUM
----------------------------
SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @CrArrivalDTTMLocal, 101) + ' 00:00', 101));

	WHILE(CONVERT(DATE, @Loopdate) < CONVERT(DATE, @crNextLocalDTTM))

	BEGIN

	--print '@Loopdate2: ' + convert(varchar, @Loopdate)
	INSERT INTO #TempTable (activedate, FlightCrew, TailNumber, DepartureDTTMLocal, ArrivalDTTMLocal, [Route], [ScheduledPassengers], CustomerID
				,[NextLocalDTTM] ,Duty, AircraftCD, DIcaoID, AIcaoID, LegID,Type_Code,TripNum)
			VALUES ( @Loopdate ,@crFlightCrew, @crTailNumber, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101)
					,CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101)
					,@crDIcaoID+ '  ' + @crDIcaoID,
					@crScheduledPassengers, @crCustomerID, NULL, 'R', @crAircraftCD, @crDIcaoID, @crDIcaoID, @crLegID ,@crType_Code,@CrTripNum)

		SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
	END

------------------------

INSERT INTO #TempTable(activedate,FlightCrew,TailNumber,Route,ScheduledPassengers,CustomerID,NextLocalDTTM,DepartureDTTMLocal,ArrivalDTTMLocal,DIcaoID,AIcaoID,FleetID,TripID,LegID,AircraftCD,Type_Code,LegNum)
            VALUES(@cractivedate,@crFlightCrew,@crTailNumber,@crDIcaoID+ '  ' + @crDIcaoID,@crScheduledPassengers,@crCustomerID,NULL,CONVERT(DATETIME, CONVERT(VARCHAR(10), @crDepartureDTTMLocal, 101) + ' 00:00', 101),@crDepartureDTTMLocal,@crDIcaoID,@crDIcaoID,@crFleetID,@crTripID,@crLegID,@crAircraftCD,@crType_Code,@crLegNum)

END
SET @PrevtTripId=@crTripID
FETCH NEXT FROM C1  INTO @cractivedate,@crFlightCrew,@crTailNumber,@crRoute,@crScheduledPassengers,@crCustomerID,@crNextLocalDTTM,@crDepartureDTTMLocal,@crArrivalDTTMLocal,@crDIcaoID,@crAIcaoID,@crFleetID,@crTripID,@crLegID,@crAircraftCD,@crType_Code,@crLegNum,@CrTripNum;
END
CLOSE C1;
DEALLOCATE C1;



	DECLARE 
	--@activedate DATETIME
		@FlightCrew VARCHAR(500)
		,@TailNumber CHAR(6)
		,@Route VARCHAR(50)
		,@ScheduledPassengers VARCHAR(500)
		,@CustomerID BIGINT;
	DECLARE @NextLocalDTTM DATETIME
		,@DepartureDTTMLocal DATETIME
		,@ArrivalDTTMLocal DATETIME
		,@DIcaoID CHAR(4)
		,@AIcaoID CHAR(4)
		,@Duty CHAR(2)
		,@AircraftCD VARCHAR(4)
		,@LegID BIGINT,@Type_Code VARCHAR(4);

	DECLARE curAircraftIV CURSOR FAST_FORWARD READ_ONLY
	FOR
	SELECT TT.activedate
		,TT.FlightCrew
		,TT.TailNumber
		,TT.[Route]
		,TT.[ScheduledPassengers]
		,TT.CustomerID
		,TT.[NextLocalDTTM]
		,TT.DepartureDTTMLocal
		,TT.ArrivalDTTMLocal
		,TT.DIcaoID
		,TT.AIcaoID
		,TT.Duty
		,TT.AircraftCD
		,TT.LegID 
		,TT.Type_Code
		,TT.TripNum
	FROM #TempTable TT
	WHERE CONVERT(Date, TT.ArrivalDTTMLocal, 101) < CONVERT(Date, TT.[NextLocalDTTM], 101)
	ORDER BY activedate

	OPEN curAircraftIV
	--print 'Total rows in cursor: ' + convert(varchar,@@CURSOR_ROWS)
	FETCH NEXT
	FROM curAircraftIV
	INTO @activedate
		,@FlightCrew
		,@TailNumber
		,@Route
		,@ScheduledPassengers
		,@CustomerID
		,@NextLocalDTTM
		,@DepartureDTTMLocal
		,@ArrivalDTTMLocal
		,@DIcaoID 
		,@AIcaoID 
		,@Duty
		,@AircraftCD
		,@LegID
		,@Type_Code
		,@CrTripNum

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Loopdate = @ArrivalDTTMLocal;
	
	--print '@Loopdate: ' + convert(varchar, @Loopdate)
	INSERT INTO #TempTable (activedate, FlightCrew, TailNumber, DepartureDTTMLocal, ArrivalDTTMLocal, [Route], 
			[ScheduledPassengers], CustomerID,[NextLocalDTTM] ,Duty, AircraftCD, DIcaoID, AIcaoID, LegID,Type_Code,TripNum ) 
			VALUES (@activedate, @FlightCrew, @TailNumber, @ArrivalDTTMLocal, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 23:59',
			CASE WHEN LEN(@Route) > 4 THEN RIGHT(@Route,4) + ' ' + RIGHT(@Route,4) ELSE @Route END, 
			@ScheduledPassengers, @CustomerID, NULL, 'R', @AircraftCD, @AIcaoID, @AIcaoID, @LegID,@Type_Code,@CrTripNum  )
	SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
	WHILE(CONVERT(DATE, @Loopdate) < CONVERT(DATE, @NextLocalDTTM))
	BEGIN
	
	--print '@Loopdate2: ' + convert(varchar, @Loopdate)
	INSERT INTO #TempTable (activedate, FlightCrew, TailNumber, DepartureDTTMLocal, ArrivalDTTMLocal, [Route], [ScheduledPassengers], CustomerID
				,[NextLocalDTTM] ,Duty, AircraftCD, DIcaoID, AIcaoID, LegID,Type_Code,TripNum)
			VALUES ( @Loopdate ,@FlightCrew, @TailNumber, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101)
					,CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101)
					,CASE WHEN LEN(@Route) > 4 THEN RIGHT(@Route,4) + ' ' + RIGHT(@Route,4) ELSE @Route END ,
					@ScheduledPassengers, @CustomerID, NULL, 'R', @AircraftCD, @AIcaoID, @AIcaoID, @LegID ,@Type_Code,@CrTripNum)

		SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
	END
		
	--print '@Loopdate3: ' + convert(varchar, @Loopdate)
	INSERT INTO #TempTable (
			activedate, FlightCrew, TailNumber, DepartureDTTMLocal, 
			ArrivalDTTMLocal, [Route], [ScheduledPassengers], CustomerID, [NextLocalDTTM] ,Duty, AircraftCD, DIcaoID, AIcaoID, LegID,Type_Code,TripNum)
	VALUES (@Loopdate, @FlightCrew, @TailNumber, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101)
			,@NextLocalDTTM ,CASE WHEN LEN(@Route) > 4 THEN RIGHT(@Route,4) + ' ' + RIGHT(@Route,4) ELSE @Route END,
			 @ScheduledPassengers, @CustomerID, NULL, 'R', @AircraftCD, @AIcaoID, @AIcaoID, @LegID,@Type_Code,@CrTripNum)  			

	FETCH NEXT
	FROM curAircraftIV
	INTO @activedate
		,@FlightCrew
		,@TailNumber
		,@Route
		,@ScheduledPassengers
		,@CustomerID
		,@NextLocalDTTM
		,@DepartureDTTMLocal
		,@ArrivalDTTMLocal
		,@DIcaoID
		,@AIcaoID 
		,@Duty
		,@AircraftCD
		,@LegID
		,@Type_Code
		,@CrTripNum
END

CLOSE curAircraftIV;

DEALLOCATE curAircraftIV;




                   

------------------- to check-----------------------------
	
--------------------------TO CHECK-------------------------
SET @TEMPTODAY = @DATEFROM
	  CREATE TABLE #TblDates  (AllDates DATE);

      WITH dates(DATEPARAM)
           AS (SELECT @TEMPTODAY AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO #TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
      

				
				INSERT INTO #TempTable (TailNumber, activedate, AircraftCD,Type_Code,FleetID) 
				SELECT DISTINCT TempT.TailNum, AllDates, ARC.AircraftCD,Ac_Code,TempT.FleetID
				FROM #TempTailNum TempT
	               INNER JOIN (SELECT AircraftCD, AircraftID FROM Aircraft) ARC ON TempT.AircraftID = ARC.AircraftID 
	               INNER JOIN @TempFleetID TT ON TT.FleetID=TempT.FleetID
			       CROSS JOIN #TblDates
				WHERE NOT EXISTS(SELECT T.TailNumber FROM #TempTable T 
					WHERE T.TailNumber = TempT.TailNum AND T.activedate = AllDates 
				)
--     --------------------------------------------
/*
UPDATE #TempTable SET Route=TEMP.ICAOID FROM(
				SELECT TOP 1 PL.ArriveICAOID ICAOID,PM.FleetID,PL.DepartureDTTMLocal DEP_DATE
					FROM PreflightLeg PL
					INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID
					INNER JOIN #TempTable TT ON TT.FleetID=PM.FleetID
					AND CONVERT(DATE, PL.ArrivalDTTMLocal) <= CONVERT(DATE, activedate)
					AND PM.TripStatus = 'T'
					AND PM.CustomerID=@CUSTOMER
					ORDER BY PL.ArrivalDTTMLocal DESC
			)TEMP
WHERE #TempTable.FleetID=TEMP.FleetID
  AND CONVERT(DATE,#TempTable.activedate)=CONVERT(DATE,TEMP.DEP_DATE)
  AND #TempTable.DepartureDTTMLocal IS NULL

  

  
UPDATE #TempTable SET Route=TEMP.ICAOID FROM(
				SELECT  DISTINCT AA.IcaoID ICAOID,F.FleetID
					FROM  Fleet F INNER JOIN Company C ON F.HomebaseID=C.HomebaseID
					              INNER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID
					WHERE F.CustomerID=@CUSTOMER
			)TEMP
WHERE #TempTable.FleetID=TEMP.FleetID
AND #TempTable.Route IS NULL
 
 */
 
 DECLARE @HomeBaseTable TABLE(FleetID BIGINT,
                             ArrivalDTTMLocal DATE,
                             IcaoID CHAR(4),
                             Rnk INT)
                             
INSERT INTO @HomeBaseTable
SELECT PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ArrivalDTTMLocal,A.IcaoID,
	ROW_NUMBER()OVER(PARTITION BY PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ORDER BY PM.FleetID,ArrivalDTTMLocal DESC) Rnk
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted = 0
		INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
		INNER JOIN @TempFleetID TF ON PM.FleetID=TF.FleetID
		WHERE PM.TRIPSTATUS IN ('T','H') 
		  AND PL.ArrivalDTTMLocal<=@DATETO
		  AND PL.IsDeleted = 0
		ORDER BY ArrivalDTTMLocal DESC


	
	
UPDATE XX SET Route=T.IcaoID
		   FROM #TempTable XX
			INNER JOIN @HomeBaseTable T ON T.FleetID=XX.FleetID AND T.Rnk = 1
           WHERE T.FleetID=XX.FleetID 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.activedate)
            AND ISNULL(Route,'') = ''
             AND XX.DepartureDTTMLocal IS NULL

 UPDATE XX SET Route=IcaoID
		   FROM #TempTable XX
			INNER JOIN (SELECT A.IcaoID,FleetID FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.FleetID=XX.FleetID
							WHERE HB.FleetID=XX.FleetID 
							 AND ISNULL(Route,'') = ''

 IF @SuppressActivityAircft=0
 BEGIN
SELECT distinct AircraftCal4.* FROM
(
SELECT DISTINCT dbo.GetDateFormatByUserCD(@UserCD,T.activedate) activedate
  , T.activedate sortdate
   ,SUBSTRING(datename(dw,activedate),1,3) [DateName]
	,FlightCrew
	,TailNumber AS tail_nmbr
	,dep_time = T.DepartureDTTMLocal  
	,arr_time = T.ArrivalDTTMLocal  
	,[Route]
    ,PaxNames   [ScheduledPassengers]
--	 ,SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) [ScheduledPassengers]
	,Duty
	,aircraft_code = T.AircraftCD
	,DIcaoID AS depicao_id
	,AIcaoID AS arricao_id
	,'' Passengers
	,T.Type_Code
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,@TenToMin TenToMin
	,TripNum
FROM #TempTable T
LEFT OUTER JOIN (
			SELECT DISTINCT PP2.LegID, Paxnames = ( 
				SELECT ISNULL(SUBSTRING(P.FirstName,1,1)+ '. ','') + ISNULL(SUBSTRING(P.MiddleInitial,1,1)+'. ','') + ISNULL(P.LastName,'') + ' '
				FROM PreflightPassengerList PP1 INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID
				WHERE PP1.LegID = PP2.LegID
				ORDER BY P.LastName,P.FirstName
				FOR XML PATH('')
			)
			FROM  PreflightPassengerList PP2
        ) PAX ON T.LegID = PAX.LegID)AircraftCal4
 WHERE  (CONVERT(DATE,sortdate) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR dep_time IS NULL)
ORDER BY sortdate,AircraftCal4.Type_Code,AircraftCal4.dep_time
--ORDER BY AircraftCal4.Type_Code,sortdate,AircraftCal4.tail_nmbr,AircraftCal4.dep_time
END
ELSE
BEGIN
 
SELECT distinct AircraftCal4.* FROM
(
SELECT DISTINCT dbo.GetDateFormatByUserCD(@UserCD,T.activedate) activedate
  , T.activedate sortdate
   ,SUBSTRING(datename(dw,activedate),1,3) [DateName]
	,FlightCrew
	,TailNumber AS tail_nmbr
	,dep_time = T.DepartureDTTMLocal  
	,arr_time = T.ArrivalDTTMLocal  
	,[Route]
    ,PaxNames   [ScheduledPassengers]
--	 ,SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) [ScheduledPassengers]
	,Duty
	,aircraft_code = T.AircraftCD
	,DIcaoID AS depicao_id
	,AIcaoID AS arricao_id
	,'' Passengers
	,T.Type_Code
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,@TenToMin TenToMin
FROM #TempTable T
LEFT OUTER JOIN (
			SELECT DISTINCT PP2.LegID, Paxnames = ( 
				SELECT ISNULL(SUBSTRING(P.FirstName,1,1)+ '. ','') + ISNULL(SUBSTRING(P.MiddleInitial,1,1)+'. ','') + ISNULL(P.LastName,'') + ' '
				FROM PreflightPassengerList PP1 INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID
				WHERE PP1.LegID = PP2.LegID
				ORDER BY P.LastName,P.FirstName
				FOR XML PATH('')
			)
			FROM  PreflightPassengerList PP2
        ) PAX ON T.LegID = PAX.LegID)AircraftCal4
 WHERE CONVERT(DATE,sortdate) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND dep_time IS NOT NULL
 ORDER BY sortdate,AircraftCal4.Type_Code,AircraftCal4.dep_time
---ORDER BY AircraftCal4.Type_Code, sortdate,AircraftCal4.tail_nmbr,AircraftCal4.dep_time
END

IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE #TempTailNum
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable
IF OBJECT_ID('tempdb..#TblDates') IS NOT NULL
DROP TABLE #TblDates

-- EXEC [spGetReportPREAircraftCalendar4Information] 'UC', '2012-07-20', '2012-07-23', 'N331UV,WING', ''  
-- EXEC [spGetReportPREAircraftCalendar4Information] 'UC', '2012-07-20', '2012-07-23', 'N331UV', ''
-- EXEC [spGetReportPREAircraftCalendar4Information] 'JWILLIAMS_13', '2011-08-10', '2011-08-20', '', ''



GO


