IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationAPDXpm1Sub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXpm1Sub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDXpm1Sub]
   @UserCD AS VARCHAR(30),			-- Mandatory
  -- @TripID AS VARCHAR(200),       -- Mandatory
   @LegID AS VARCHAR(30)            -- Mandatory
  ,@PassengerCD VARCHAR(30) = ''	
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSGeneralDeclarationAPDXpm1Sub
-- Author: ABHISHEK.S
-- Create date: 13th September 2012
-- Description: Get TripSheetReportWriter General Declaration Passenger Mainfest Sub Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
BEGIN
	
	SET NOCOUNT ON;	
	
--DECLARE @TEMP1 TABLE (Operator VARCHAR(100),NandR VARCHAR(50),FlightNo BIGINT , Date1 DATE,DepFrm VARCHAR(50),ArrAt VARCHAR(50), TNOFCrew VARCHAR(50), PNUM VARCHAR(100), CNTRY VARCHAR(20) ) 
  DECLARE @TEMP1 TABLE (LegId VARCHAR(30),Operator VARCHAR(100),NandR VARCHAR(9),FlightNo VARCHAR(12) , Date1 VARCHAR(10),PtOfEm VARCHAR(100),PtOfDisem VARCHAR(100), Paxnm VARCHAR(300),ForOprtr VARCHAR(100), Cntry varchar(100), OrderNUM INT) 
INSERT INTO @TEMP1	
 
SELECT DISTINCT
[LegId]     =	PL.LegID,
[Operator]  = 	CONVERT(VARCHAR(MAX),FP.OwnerLesse),
[NandR]     = 	F.TailNum,
[FlightNo]  = 	PM.FlightNUM,
[Date1]     = 	dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,PL.DepartureDTTMLocal),
[PtOfEm]    =	RTRIM(A.CityName)+','+RTRIM(A.CountryName),
[PtOfDisem] =	RTRIM(B.CityName)+','+RTRIM(B.CountryName),
--[Paxnm]    =   PR.FirstName + ', '+ PR.MiddleInitial + ' '+ PR.LastName ,
[Paxnm]    =   CASE WHEN PP.FlightPurposeID Is NULL THEN NULL ELSE ISNULL(PR.LastName,'') + ', '+ ISNULL(PR.FirstName,'') + ' '+ ISNULL(PR.MiddleInitial,'') END,
--[ForOprtr]  =   dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),
[ForOprtr]  =  ISNULL(CPP.PassportNum,''),
[Cntry]     =   C.CountryCD,
[OrderNUM] = CASE WHEN PP.FlightPurposeID Is NULL THEN 9999 ELSE PP.OrderNUM END
 

FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL
				        ON PM.TripID = PL.TripID
				INNER JOIN (SELECT FleetID, TailNum FROM Fleet) F
				        ON PM.FleetID = F.FleetID
				LEFT JOIN FleetPair FP
				        ON F.FleetID = FP.FleetID
				LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS A
			            ON A.AirportID = PL.DepartICAOID
		        LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS B
			            ON B.AirportID = PL.ArriveICAOID		
			     LEFT JOIN PreflightPassengerList PP
				        ON PL.LegID = PP.LegID			 
			    LEFT JOIN Passenger PR
			            ON PP.PassengerID = PR.PassengerRequestorID 
			    LEFT JOIN CrewPassengerPassport CPP				        
						ON PP.PassportID = CPP.PassportID AND CPP.IsDeleted = 0																				
		        LEFT JOIN Country C
			    	    ON CPP.CountryID = C.CountryID			    	    
WHERE PM.IsDeleted = 0
	  AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	  AND PR.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN PR.PassengerRequestorCD ELSE @PassengerCD END	 	             
	  AND CONVERT(VARCHAR,PL.LegID) IN (@LegID)
ORDER BY OrderNUM
DECLARE @COUNT INT 
SELECT @COUNT=COUNT(*) FROM @TEMP1             

WHILE @COUNT<25
BEGIN
SET @COUNT=@COUNT+1
END

     IF(@COUNT%25)=0 
   SET @COUNT=@COUNT
ELSE IF(@COUNT%25)=1
SET @COUNT=@COUNT+24
ELSE IF(@COUNT%25)=2
SET @COUNT=@COUNT+23
ELSE IF(@COUNT%25)=3
SET @COUNT=@COUNT+22
ELSE IF(@COUNT%25)=4
SET @COUNT=@COUNT+21
ELSE IF(@COUNT%25)=5
SET @COUNT=@COUNT+20
ELSE IF(@COUNT%25)=6
SET @COUNT=@COUNT+19
ELSE IF(@COUNT%25)=7
SET @COUNT=@COUNT+18
ELSE IF(@COUNT%25)=8
SET @COUNT=@COUNT+17
ELSE IF(@COUNT%25)=9
SET @COUNT=@COUNT+16
ELSE IF(@COUNT%25)=10
SET @COUNT=@COUNT+15
ELSE IF(@COUNT%25)=11
SET @COUNT=@COUNT+14
ELSE IF(@COUNT%25)=12
SET @COUNT=@COUNT+13
ELSE IF(@COUNT%25)=13
SET @COUNT=@COUNT+12
ELSE IF(@COUNT%25)=14
SET @COUNT=@COUNT+11
ELSE IF(@COUNT%25)=15
SET @COUNT=@COUNT+10
ELSE IF(@COUNT%25)=16
SET @COUNT=@COUNT+9
ELSE IF(@COUNT%25)=17
SET @COUNT=@COUNT+8
ELSE IF(@COUNT%25)=18
SET @COUNT=@COUNT+7
ELSE IF(@COUNT%25)=19
SET @COUNT=@COUNT+6
ELSE IF(@COUNT%25)=20
SET @COUNT=@COUNT+5
ELSE IF(@COUNT%25)=21
SET @COUNT=@COUNT+4
ELSE IF(@COUNT%25)=22
SET @COUNT=@COUNT+3
ELSE IF(@COUNT%25)=23
SET @COUNT=@COUNT+2
ELSE IF(@COUNT%25)=24
SET @COUNT=@COUNT+1

DECLARE @COUNT1 INT;
SELECT @COUNT1=COUNT(*) FROM @TEMP1

WHILE  @COUNT1<@COUNT
BEGIN 
 INSERT INTO @TEMP1(LegId) VALUES(NULL);
 SET @COUNT1 = @COUNT1 +1  
END

	SELECT LegId,Operator,NandR,FlightNo,Date1,PtOfEm,PtOfDisem,Paxnm,ForOprtr,Cntry
	FROM @TEMP1
			    
END
-- EXEC spGetReportPRETSGeneralDeclarationAPDXpm1Sub 'TIM' ,  '100013169'

 









GO


