IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAircraftTypeExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAircraftTypeExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportAircraftTypeExportInformation]    
 @UserCD VARCHAR(30)    
AS    
-- =============================================    
-- SPC Name: spGetReportAircraftTypeExportInformation    
-- Author: SUDHAKAR J    
-- Create date: 27 Jun 2012    
-- Description: Get the Aircraft Type information for REPORT export    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
SET NOCOUNT ON    
    
DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;    
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));    
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));    
    
 SELECT     
  [AircraftCD] = AircraftCD    
  ,[AircraftDescription] = AircraftDescription    
  ,[chrg_rate] = ChargeRate    
  ,[chrg_unit] = ChargeUnit    
  ,[powr_set] = PowerSetting    
  ,[powr_desc] = PowerDescription    
  ,[wind_altd] = WindAltitude    
  ,[ps1desc] = PowerSettings1Description    
  ,[ps1tas] = PowerSettings1TrueAirSpeed    
  ,[ps1hrrng] = PowerSettings1HourRange    
  ,[ps1tobias] = PowerSettings1TakeOffBias    
  ,[ps1lndbias] = PowerSettings1LandingBias    
  ,[ps2desc] = PowerSettings2Description    
  ,[ps2tas] = PowerSettings2TrueAirSpeed    
  ,[ps2hrrng] = PowerSettings2HourRange    
  ,[ps2tobias] = PowerSettings2TakeOffBias    
  ,[ps2lndbias] = PowerSettings2LandingBias    
  ,[ps3desc] = PowerSettings3Description    
  ,[ps3tas] = PowerSettings3TrueAirSpeed    
  ,[ps3hrrng] = PowerSettings3HourRange    
  ,[ps3tobias] = PowerSettings3TakeOffBias    
  ,[ps3lndbias] = PowerSettings3LandingBias    
  ,[rtyfixrf] = IsFixedRotary        
 FROM Aircraft    
 WHERE CustomerID = @CUSTOMERID    
 AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
 AND IsDeleted = 0
 AND PowerSetting IS NOT NULL
 ORDER BY [CustomerID],[AircraftCD]    
    
-- EXEC spGetReportAircraftTypeExportInformation 'TIM'

GO


