IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByCodeTripExportInormation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByCodeTripExportInormation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[spGetReportPOSTBillingByCodeTripExportInormation]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATE ,--Mandatory
	@DATETO AS DATE , --Mandatory
	@LogNum AS VARCHAR(200) = '',
	@PassengerRequestorCD  AS VARCHAR(200) = '',
	@PassengerGroupCD VARCHAR(500)='' 
)
AS
-- ================================================================================   
-- SPC Name: [spGetReportPOSTBillingByCodeTripExportInormation]          
-- Author: Aishwarya.M          
-- Create date: 10/25/2013          
-- Description: Get Trip Detail by billing for REPORTS          
-- Revision History          
-- Date                 Name        Ver         Change
-- 28-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================   
BEGIN            
SET NOCOUNT ON  
 -----------------------------Passenger and Passenger Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
--ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
--BEGIN  
--INSERT INTO @TempPassengerID
--	SELECT DISTINCT  P.PassengerRequestorID
--	FROM Passenger P 
--	WHERE  P.CustomerID = @CUSTOMER  
--	AND P.IsDeleted=0
--END
-----------------------------Passenger and Passenger Group Filteration----------------------  


      
DECLARE @TenToMin SMALLINT = 0;  
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @TenToMin = TimeDisplayTenMin,
@AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 


IF @PassengerRequestorCD <> '' OR @PassengerGroupCD <>''

BEGIN


SELECT DISTINCT
[LogNumber] = POM.LogNum
,[Leg] = POL.LegNUM
,[EstDeptDate] = POM.EstDepartureDT
,[TailNumber] = FL.TailNum
,[Rqstr] = PP.PassengerRequestorCD
,[RqstrDept] = DRQ.DepartmentCD
,[RqstrAuth] = DARQ.AuthorizationCD
,[LegID] = POL.POLegID
,[DepICAO] = AD.IcaoID
,[ArrICAO] = AA.IcaoID
,[Tripdate] = POL.OutboundDTTM
,[Distance] = POL.Distance
,[FlightHours] = FLOOR(ROUND(POL.FlightHours,1)*10)*0.1
,[BlockHours] = FLOOR(ROUND(POL.BlockHours,1)*10)*0.1
,[NbrPax] = POL.PassengerTotal
,[EmptySeats] = FL.MaximumPassenger
,[DeptCD] = DE.DepartmentCD
,[AuthCD] = DA.AuthorizationCD
,[ChargeRate] = CR.ChargeRate
,[Per] = CR.ChargeUnit
,[typecode] = ACR.AircraftCD
,[TotalChargesLEG] = CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
						+ (ISNULL(POE.ExpenseAmt,0))

,[Code] = P.PassengerRequestorCD
,[PassengerName] = P.LastName + ', ' + P.FirstName
,[PaxDept] = DPAX.DepartmentCD
,[PaxAuth] = APAX.AuthorizationCD
--,[PAXTOTAL] = POL.PassengerTotal
,[TotalChargesPax] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
							FLOOR((
							CASE WHEN(
							((CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							NULL
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal
							)=0
							THEN
							NULL
							ELSE
							((CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							0.00
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal
							END
							)*100)*0.01
						END
,[Billing] = POP.Billing
,[StdBilling] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,CR.BeginRateDT) AND CONVERT(DATE,CR.EndRateDT)
						THEN(
							CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/POL.PassengerTotal,0)
						)
						END

,[TenToMin] = @TenToMin
,[exp_account] = EX.AccountDescription
,[exp_amount] = EX.ExpenseAMT
,[IncludeBilling] = POE.IsBilling
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POM.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID AND PE.IsDeleted = 0))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID AND FL.IsDeleted=0 AND FL.IsInActive=0
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
--Leg Level Department and Authorisation
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON POL.AuthorizationID = DA.AuthorizationID  
--Trip Requestor Department and Authorization
LEFT OUTER JOIN Department DRQ ON PP.DepartmentID = DRQ.DepartmentID 
LEFT OUTER JOIN DepartmentAuthorization DARQ ON PP.AuthorizationID = DARQ.AuthorizationID                
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
--Passenger Department and Authorization 
LEFT OUTER JOIN Department DPAX ON P.DepartmentID = DPAX.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization APAX ON APAX.AuthorizationID = P.AuthorizationID
LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
           ) CR ON POL.POLegID = CR.POLegID
LEFT OUTER JOIN Aircraft ACR ON FL.AircraftID = ACR.AircraftID
LEFT OUTER JOIN (
			SELECT AccountDescription, ExpenseAMT, PE.POLegID FROM PostflightExpense PE 
					INNER JOIN Account A ON PE.AccountID = A.AccountID
					WHERE PE.IsDeleted = 0
				)EX ON EX.POLegID = POL.POLegID
INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID

WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	AND POM.IsDeleted = 0
 ---AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')

END

ELSE

BEGIN

SELECT DISTINCT
[LogNumber] = POM.LogNum
,[Leg] = POL.LegNUM
,[EstDeptDate] = POM.EstDepartureDT
,[TailNumber] = FL.TailNum
,[Rqstr] = PP.PassengerRequestorCD
,[RqstrDept] = DRQ.DepartmentCD
,[RqstrAuth] = DARQ.AuthorizationCD
,[LegID] = POL.POLegID
,[DepICAO] = AD.IcaoID
,[ArrICAO] = AA.IcaoID
,[Tripdate] = POL.OutboundDTTM
,[Distance] = POL.Distance
,[FlightHours] = FLOOR(ROUND(POL.FlightHours,1)*10)*0.1
,[BlockHours] = FLOOR(ROUND(POL.BlockHours,1)*10)*0.1
,[NbrPax] = POL.PassengerTotal
,[EmptySeats] = FL.MaximumPassenger
,[DeptCD] = DE.DepartmentCD
,[AuthCD] = DA.AuthorizationCD
,[ChargeRate] = CR.ChargeRate
,[Per] = CR.ChargeUnit
,[typecode] = ACR.AircraftCD
,[TotalChargesLEG] = CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
						+ (ISNULL(POE.ExpenseAmt,0))

,[Code] = P.PassengerRequestorCD
,[PassengerName] = P.LastName + ', ' + P.FirstName
,[PaxDept] = DPAX.DepartmentCD
,[PaxAuth] = APAX.AuthorizationCD
--,[PAXTOTAL] = POL.PassengerTotal
,[TotalChargesPax] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
							FLOOR((
							CASE WHEN(
							((CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							NULL
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal
							)=0
							THEN
							NULL
							ELSE
							((CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							0.00
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal
							END
							)*100)*0.01
						END
,[Billing] = pop.Billing
,[StdBilling] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,CR.BeginRateDT) AND CONVERT(DATE,CR.EndRateDT)
						THEN(
							CASE CR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ISNULL(ROUND(POL.BlockHours,1),0) ELSE ISNULL(ROUND(POL.FlightHours,1),0) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/POL.PassengerTotal,0)
						)
						END

,[TenToMin] = @TenToMin
,[exp_account] = EX.AccountDescription
,[exp_amount] = ex.ExpenseAMT
,[IncludeBilling] = POE.IsBilling
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POM.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID AND PE.IsDeleted = 0))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID AND FL.IsDeleted=0 AND FL.IsInActive=0
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
--Leg Level Department and Authorisation
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON POL.AuthorizationID = DA.AuthorizationID  
--Trip Requestor Department and Authorization
LEFT OUTER JOIN Department DRQ ON PP.DepartmentID = DRQ.DepartmentID 
LEFT OUTER JOIN DepartmentAuthorization DARQ ON PP.AuthorizationID = DARQ.AuthorizationID                
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
--Passenger Department and Authorization 
LEFT OUTER JOIN Department DPAX ON P.DepartmentID = DPAX.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization APAX ON APAX.AuthorizationID = P.AuthorizationID
LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
           ) CR ON POL.POLegID = CR.POLegID
LEFT OUTER JOIN Aircraft ACR ON FL.AircraftID = ACR.AircraftID
LEFT OUTER JOIN (
			SELECT AccountDescription, ExpenseAMT, PE.POLegID FROM PostflightExpense PE 
					INNER JOIN Account A ON PE.AccountID = A.AccountID
					WHERE PE.IsDeleted = 0
				)EX ON EX.POLegID = POL.POLegID
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	AND POM.IsDeleted = 0
 ---AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')


END

END
--EXEC spGetReportPOSTBillingByCodeTripExportInormation 'SUPERVISOR_99','10099','01-01-2009','01-01-2010','','',''






GO


