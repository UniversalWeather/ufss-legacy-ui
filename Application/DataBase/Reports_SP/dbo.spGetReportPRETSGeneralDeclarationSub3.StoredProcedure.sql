IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub3]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub3]  
 @LegID AS VARCHAR(30),               -- Mandatory  
 @PassengerCD VARCHAR(30) = ''
AS  
-- ==========================================================================================  
-- SPC Name: spGetReportPRETSGeneralDeclarationSub4  
-- Author: ABHISHEK.S  
-- Create date: 20th September 2012  
-- Description: Get TripSheetReportWriter General Declaration Information For Reports  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================================  
  
--- -- EXEC  spGetReportPRETSGeneralDeclaration 'TIM', '', '', '', '2011-07-20', '2012-07-21', '', '', '', '', '', '', '',1,1,1,1,1,1   
   
BEGIN  
  
  
   
SET NOCOUNT ON;   
  
  
  

SELECT DISTINCT Title='ZZZ'  
    ,LastName=PR.LastName  
    ,FirstName=PR.FirstName  
    ,MiddleName=PR.MiddleInitial  
    ,Gender=PR.Gender  
    ,DOB=DATENAME(D,PR.DateOfBirth)+UPPER(LEFT(DATENAME(M,PR.DateOfBirth),3))+DATENAME(YEAR,PR.DateOfBirth)  
    ,Passport=CPP.PassportNum  
    ,ExpDate=DATENAME(D,CPP.PassportExpiryDT)+UPPER(LEFT(DATENAME(M,CPP.PassportExpiryDT),3))+DATENAME(YEAR,CPP.PassportExpiryDT)  
    ,Nationality=CASE WHEN ISNULL(C.CountryCD,'') = ''  THEN CO.CountryCD ELSE C.CountryCD END   
    ,Type='P'  
    ,CrewCD=PassengerRequestorCD  
    ,PL.LegNUM  
    ,PM.TripNUM  
   
 FROM PREFLIGHTMAIN PM  
      INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   INNER JOIN PreflightPassengerList PLI ON PL.LegID = PLI.LegID  
   INNER JOIN Passenger PR ON PLI.PassengerID = PR.PassengerRequestorID        
   LEFT JOIN CrewPassengerPassport CPP ON PLI.PassportID = CPP.PassportID AND PLI.PassengerID = CPP.PassengerRequestorID AND CPP.IsDeleted = 0             
   LEFT JOIN Country C ON CPP.CountryID = C.CountryID  
   LEFT JOIN Country CO ON PR.CountryID = CO.CountryID
   INNER JOIN FlightPurpose FP ON PLI.FlightPurposeID=FP.FlightPurposeID   
      
 WHERE PM.IsDeleted = 0    
 AND PL.LegID=@LegID  
 AND PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN PassengerRequestorCD ELSE @PassengerCD END
 ORDER BY PM.TripNUM,PL.LegNUM,Title,PassengerRequestorCD  
  
  
END
GO


