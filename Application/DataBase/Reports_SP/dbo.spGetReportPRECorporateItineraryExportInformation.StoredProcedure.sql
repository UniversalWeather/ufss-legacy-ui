IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItineraryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItineraryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRECorporateItineraryExportInformation]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		--@TripNum AS BIGINT -- [Optional INTEGER], Comma delimited string with mutiple values
		@TripNum AS NVARCHAR(500) = '' -- [Optional INTEGER], Comma delimited string with mutiple values

AS

-- =============================================
-- SPC Name: spGetReportPRECorporateItineraryExportInformation
-- Author: SINDHUJA.K
-- Create date: 20 July 2012
-- Description: Get Preflight CorporateItinerary Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON

 DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));	
 
    


-----------------------------TailNum and Fleet Group Filteration----------------------



CREATE TABLE #TempFleet 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleet
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleet
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO #TempFleet
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END


-----------------------------TailNum and Fleet Group Filteration----------------------	
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
 SELECT DISTINCT
			[orig_nmbr] = PM.TripNUM,
			[tail_nmbr] = F.TailNUM,
			[mindate] =  CONVERT(DATE,PL1.DepartureDTTMLocal),
			[maxdate] =  CONVERT(DATE,PL1.ArrivalDTTMLocal),
			[desc] = PM.TripDescription,
			[leg_num] = PL.LegNUM,
			[legid] = PL.LEGID,
			[locdep] = PL.DepartureDTTMLocal, 
			[locarr] =PL.ArrivalDTTMLocal,
			[etd] = LEFT(CONVERT(VARCHAR, PL.DepartureDTTMLocal, 14),5),
			[depicao_id] = A.ICAOID,
			[dep_airport_name] = A.AirportName,
			[dep_airport_city] = A.CityName,
			[eta] = LEFT(CONVERT(VARCHAR, PL.ArrivalDTTMLocal, 14),5),
			[arricao_id] = B.ICAOID,
			[arr_airport_name] = B.AirportName,
			[arr_airport_city] = B.CityName,
			[elp_time] = CONVERT(NUMERIC(7,3), PL.ElapseTM),
			[fbo_name] = PF.PreflightFBOName,
			[fbo_phone] = PF.PhoneNum1,           
			[pax_hotel] = HP.Name,    
			[pax_hotel_phone] = HP.PhoneNum,      
			[crew_hotel] = HC.Name,  
			[crew_hotel_phone] = HC.PhoneNum ,     
			[paxname] = ISNULL(PPL.PassengerFirstName,'') + ISNULL( ', ' + PPL.PassengerMiddleName ,'') + ' ' + ISNULL(PPL.PassengerLastName,''),
			PL.Notes,
			Full_Name=Crew.CrewList,
			Title=Crew.CrewDuty
		FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		INNER JOIN (SELECT TripNUM,CustomerID,TripID,MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal FROM PreflightLeg WHERE IsDeleted = 0 GROUP BY TripNUM,TripID,CustomerID) PL1 ON PL1.TripID=PL.TripID AND PL.CustomerID=PL1.CustomerID
	    INNER JOIN Airport A ON PL.DepartICAOID = A.AirportID 
	    INNER JOIN Airport B ON PL.ArriveICAOID = B.AirportID		 
	    LEFT JOIN PreflightPassengerList PPL ON PPL.LegID = PL.LegID
		LEFT JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		LEFT JOIN PreflightCrewList PCLL ON PCLL.LegID = PL.LegID
		--LEFT JOIN PreflightCrewHotelList PCH ON PCH.PreflightCrewListID = PCLL.PreflightCrewListID 
		LEFT JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1
																	AND PCH.crewPassengerType = 'C'
		LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PCH.PreflightHotelListID = PHCP.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
		LEFT JOIN Hotel HP ON PPH.HotelID = HP.HotelID
	    LEFT JOIN Hotel HC ON PCH.HotelID = HC.HotelID
	    LEFT JOIN PreflightFBOList PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO = 1
	    INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
		            ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
		LEFT OUTER JOIN (
			SELECT DISTINCT PC1.LEGID,CrewList = (
				SELECT ISNULL(C.FirstName+' '+C.LastName,'')+' ' 
				FROM PreflightCrewList PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
				WHERE PC.LEGID = PC1.LEGID 
				--ORDER BY( CASE PC.DutyTYPE 
				--		WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
				--		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)
				FOR XML PATH('')
			),
			CrewDuty = (
				SELECT CASE(PC.DutyTYPE) WHEN 'P' THEN 'Captain'
				                          WHEN 'S' THEN 'First Officer'
				                          WHEN 'E' THEN 'Engineer'
				                          WHEN 'I' THEN 'Instructor'
				                          WHEN 'A' THEN 'Attendant'
				                          ELSE 'Other' END+' '
				FROM PreflightCrewList PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
				WHERE PC.LEGID = PC1.LEGID 

				FOR XML PATH('')
			)
				
			FROM PreflightCrewList PC1
		) Crew ON PL.LEGID = Crew.LEGID 
	   INNER JOIN #TempFleet TF ON TF.FleetID=F.FleetID	
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))		
		--AND PM.TripStatus='T'
        AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
        AND (PM.TripNUM IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ','))OR @TripNum='')
        AND PM.IsDeleted = 0
	
	--Construct OPTIONAL clauses
				
IF OBJECT_ID('tempdb..#TempFleet') IS NOT NULL 
DROP table #TempFleet


    --EXEC spGetReportPRECorporateItineraryExportInformation 'ERICK_1','2010-05-01','2011-05-01', '', '', ''



GO


