
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftPerformance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftPerformance]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPOSTAircraftPerformance]   
(   
  @UserCD AS VARCHAR(30),
  @DATEFROM AS DATETIME,
  @DATETO AS DATETIME,
  @TailNUM AS VARCHAR(5000) = '',  
  @FleetGroupCD AS VARCHAR(5000)='', 
  @SortBy As NVARCHAR(1000) = '',
  @SeperatePage Bit=0 
  )       
AS   
-- ===============================================================================    
-- SPC Name: spGetReportAircraftPerformanceAnalysis    
-- Author:  P.srinivas    
-- Create date: 
-- Altered by Mathes 30-10-2012    
-- Description: Get Postflight Aircraft Performance Analysis for REPORTS    
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================    
SET NOCOUNT ON   
  Declare @DateRange Varchar(30)
  Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetDateFormatByUserCD(@UserCD,@DATETO)                                   

 DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';   
  DECLARE @ParameterDefinition AS NVARCHAR(500)  
  
  IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
      DROP table #TmpFltTail    
  IF OBJECT_ID('tempdb..#TmpTails') is not null                        
      DROP table #TmpTails      
      
  Create Table #TmpTails (TailNum Varchar(9))      
  Create Table #TmpFltTail (TailNum Varchar(9))  
  Declare @TenToMin Int
  Declare @Units Varchar(20)=''
  Declare @DayMonth int
  Select @TenToMin=Company.TimeDisplayTenMin, 
         @DayMonth=Company.DayMonthFormat,
         @Units=Case Company.FuelPurchase When 1 Then 'Gallons' When 2 Then 'Liters' WHEN 3 THEN 'Imp Gals' WHEN 4 THEN ' ' End 
         From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD) And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)          
  
  
  Declare @Filter Char(1)='F'
  IF @TailNum <>''
  Begin
    INSERT INTO #TmpTails SELECT DISTINCT TailNum FROM Fleet  
    WHERE TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  AND
    CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
    Set @Filter='T'
  End 
   
  IF @FleetGroupCD <>''
  Begin
   INSERT INTO #TmpTails SELECT DISTINCT TailNum 
   FROM  FleetGroupOrder INNER JOIN Fleet ON FleetGroupOrder.FleetID = Fleet.FleetID    
   INNER JOIN FleetGroup ON FleetGroup.FleetGroupID=FleetGroupOrder.FleetGroupID 
   WHERE FleetGroupOrder.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
   FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) AND
   Fleet.TailNum Not IN(SELECT DISTINCT TailNum From #TmpTails)
   Set @Filter='T'
  End     
 
 
 IF OBJECT_ID('tempdb..#TmpRpt') is not null                        
      DROP table #TmpRpt  
      
 Create Table #TmpRpt(POLogID BigInt,
                      POLegID BigInt,
                      TailNum Varchar(10), 
                      ScheduleDTTMLocal DateTime,
                      EstDepartureDT DateTime,
                      DispatchNum Varchar(12),
                      LogNum BigInt,
                      LegNUM BigInt,
                      DepartICAOID varchar(30),
                      ArrivalICAOID varchar(30),
                      FlightCatagoryCD Varchar(9),
                      SeatLeft Varchar(50) ,
                      SeatRight  Varchar(50), 
                      Distance  Numeric(5,0),
                      StatuteMiles Numeric(5,0),
                      DelayTypeCD Varchar(10),
                      DelayTM Numeric(6,3),
                      BlockHours Numeric(6,3),
					  FlightHours Numeric(6,3), 
                      FuelUsed Numeric(7,0), 
                      PassengerTotal Numeric(5,0),
                      FuelLocatorCD  Varchar(10), 
                      FuelQTY Numeric(15,2),
                      ExpenseAMT Numeric(17,2),
                      AircraftCD Varchar(9),
                      InboundDTTM  DateTime,
                      OutboundDTTM DateTime,
                      SLIPNUM BIGINT,
                      HoursAway Numeric(7,1)
                       )   
 
SET @SQLSCRIPT ='Insert Into #TmpRpt SELECT
DISTINCT 
PFL.POLogID,
PFL.POLegID,
FT.TailNum, 
PFL.ScheduleDTTMLocal,
POM.EstDepartureDT,
POM.DispatchNum,
POM.LogNum,
PFL.LegNUM,
CONVERT (VARCHAR, ADeprt.IcaoID) DepartICAOID,
CONVERT (VARCHAR, AArr.IcaoID) ArrivalICAOID,        
FC.FlightCatagoryCD,
SeatLeft=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''L'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),
SeatRight=(SELECT Top 1 Crew.CrewCD FROM  Crew INNER JOIN
          PostflightCrew ON Crew.CrewID = PostflightCrew.CrewID AND Crew.CustomerID = PostflightCrew.CustomerID
          WHERE PostflightCrew.Seat = ''R'' And PostflightCrew.POLegID =PFC.POLegID AND 
          PostflightCrew.DutyType IN(''P'',''S'') Order By PostFlightCrewListID Desc),  
PFL.Distance,
PFL.StatuteMiles,
DT.DelayTypeCD,
PFL.DelayTM,
PFL.BlockHours,
PFL.FlightHours, 
PFL.FuelUsed, 
PFL.PassengerTotal,
					--FuelLocatorCD=(SELECT Top 1   FL.FuelLocatorCD FROM  PostflightExpense PE 
					--LEFT JOIN FuelLocator FL ON PE.FuelLocatorID = FL.FuelLocatorID 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc),
FuelLocatorCD, 
					--(SELECT Top 1 DBO.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD) FROM  PostflightExpense PE 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc), 
DBO.GetFuelConversion(PFE.FuelPurchase,PFE.FuelQTY,@UserCD), 
					--( SELECT Top 1 (Case WHEN CPY.FuelPurchase <> PE.FuelPurchase  THEN  (PFE.FuelQTY*PE.UnitPrice)/NULLIF(DBO.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD),0) 
					--                                             ELSE PE.UnitPrice End) FROM  PostflightExpense PE 
					--WHERE PE.POLegID =PFC.POLegID
					-- Order By PostflightExpenseID Desc), 
PFE.ExpenseAMT,
FT.AircraftCD,
PFL.InboundDTTM,
PFL.OutboundDTTM,
SLIPNUM = SlipNUM,
0

FROM  PostflightMain POM
INNER JOIN PostflightLeg PFL on PFL.POLogID = POM.POLogID
LEFT JOIN FlightCatagory FC On PFL.FlightCategoryID = FC.FlightCategoryID 
LEFT JOIN PostflightExpense PFE on PFE.POLogID=POM.POLogID and PFE.POLegID = PFL.POLegID 
LEFT JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY POLogID,POLEGID ORDER BY SlipNum DESC) Rnk ,FuelLocatorCD,POLogID,POLegID FROM PostflightExpense PFE INNER JOIN FuelLocator  ON PFE.FuelLocatorID = FuelLocator.FuelLocatorID ) PE ON PE.POLogID=POM.POLogID AND RNK=1 AND PE.PoLegid=PFL.PoLegid 
LEFT JOIN Fleet FT ON FT.FleetID = POM.FleetID   
LEFT JOIN Company CPY on CPY.HomebaseID = FT.HomebaseID 
LEFT JOIN  Airport ADeprt ON ADeprt.AirportID = PFL.DepartICAOID
LEFT JOIN  Airport AArr ON AArr.AirportID = PFL.ArriveICAOID  
LEFT JOIN DelayType DT ON DT.DelayTypeID = PFL.DelayTypeID
LEFT JOIN PostflightCrew PFC on PFC.POLegID = PFL.POLegID 
WHERE PFL.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))     
+ ' AND CONVERT(DATE,PFL.ScheduleDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) '  
  
IF @Filter='T' 
BEGIN      
 SET @SQLSCRIPT = @SQLSCRIPT + ' AND FT.TailNUM IN ( Select Distinct TailNum From #TmpTails) '    
END   
 
IF(@sortby='ScheduleDTTMLocal')  
 BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY ScheduleDTTMLocal'  
  END  
IF(@sortby='LogNum')  
 BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT +' ORDER BY LogNum'  
  END  
  
 SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME,@DATETO AS DATETIME,@TailNUM AS VARCHAR(5000),@FleetGroupCD AS VARCHAR(5000),@sortby As NVARCHAR(1000)'
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD,@DATEFROM,@DATETO,@TailNUM,@FleetGroupCD,@sortby   
 
 
 Declare @TailNm Varchar(7)    
 Declare @AircraftCD Varchar(10) 
 IF @TailNum ='' AND @FleetGroupCD =''
 Begin 
	 IF OBJECT_ID('tempdb..#Tmp') is not null                        
		  DROP table #Tmp  
	 Create Table #Tmp(TailNum Varchar(7),AircraftCD Varchar(10))  
	 INSERT INTO #Tmp SELECT DISTINCT TailNum,AircraftCD From Fleet Where CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) And TailNum Not In(Select Distinct TailNum from #TmpRpt)
	 WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
	 BEGIN 
	   Set @TailNm=''
	   Set @AircraftCD=''
	   Select Top 1 @TailNm=TailNum,@AircraftCD=AircraftCD From #Tmp  
	   Insert Into #TmpRpt(TailNum,AircraftCD, DelayTM,BlockHours,FlightHours) Values(@TailNm,@AircraftCD,0,0,0)
	   Delete From #Tmp Where TailNum=@TailNm  
	 END           
	 IF OBJECT_ID('tempdb..#Tmp') is not null                        
		  DROP table #Tmp  	
 End   
 IF OBJECT_ID('tempdb..#TmpTails') is not null                        
      DROP table #TmpTails      
      	   
IF @SuppressActivityAircraft=1
 
 BEGIN
 IF @sortby='ScheduleDTTMLocal'  
 BEGIN  
  Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduleDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        IsNull(Distance,0) Distance,
        IsNull(StatuteMiles,0) StatuteMiles,
        DelayTypeCD,
        DelayTM,
        IsNull(BlockHours,0) BlockHours,
        IsNull(FlightHours,0)FlightHours,   
        IsNull(FuelUsed,0) FuelUsed,
        IsNull(PassengerTotal,0) PassengerTotal,
        FuelLocatorCD,
        IsNull(FuelQTY,0) FuelQTY,
        IsNull(ExpenseAMT,0) ExpenseAMT,  
        InboundDTTM,
        IsNull(Distance,0)/NullIf(FlightHours,0) FltSpeed,
        IsNull(FuelUsed,0) FuelBurn,
        IsNull(FuelUsed,0)/NullIf(BlockHours,0) FuelBHR,         
        IsNull(FuelUsed,0)/NullIf(FlightHours,0) FuelFHR, 
        IsNull(ExpenseAMT,0) FuelPrice, 
        HoursAway=(dbo.GetHoursAway(@UserCD,POLegID,POLogID)),
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt 
        WHERE ScheduleDTTMLocal IS NOT NULL
        order by AircraftCD, LogNum,EstDepartureDT, ScheduleDTTMLocal, LegNUM
  END  
 ELSE
  BEGIN  
    Select Distinct AircraftCD,POLogID,POLegID,TailNum,
        CONVERT(VARCHAR(5), ScheduleDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        IsNull(Distance,0) Distance,
        IsNull(StatuteMiles,0) StatuteMiles,
        DelayTypeCD,
        DelayTM,
        IsNull(BlockHours,0) BlockHours,
        IsNull(FlightHours,0)FlightHours,   
        IsNull(FuelUsed,0) FuelUsed,
        IsNull(PassengerTotal,0) PassengerTotal,
        FuelLocatorCD,
        IsNull(FuelQTY,0) FuelQTY,
        IsNull(ExpenseAMT,0) ExpenseAMT, 
       
        InboundDTTM,
        IsNull(Distance,0)/NullIf(FlightHours,0) FltSpeed,
        IsNull(FuelUsed,0) FuelBurn,
        IsNull(FuelUsed,0)/NullIf(BlockHours,0) FuelBHR,         
        IsNull(FuelUsed,0)/NullIf(FlightHours,0) FuelFHR, 
        IsNull(ExpenseAMT,0) FuelPrice, 
        HoursAway=(dbo.GetHoursAway(@UserCD,POLegID,POLogID)),
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange 
        From #TmpRpt 
        WHERE ScheduleDTTMLocal IS NOT NULL
        order by AircraftCD, LogNum, ScheduleDTTMLocal, LegNUM
        
  END  
      	
 
 END
 
 ELSE
 
 
 BEGIN
 IF @sortby='ScheduleDTTMLocal'  
 BEGIN  
  Select Distinct AircraftCD,POLogID,POLegID,TailNum,EstDepartureDT,
        CONVERT(VARCHAR(5), ScheduleDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        IsNull(Distance,0) Distance,
        IsNull(StatuteMiles,0) StatuteMiles,
        DelayTypeCD,
        DelayTM,
        IsNull(BlockHours,0) BlockHours,
        IsNull(FlightHours,0)FlightHours,   
        IsNull(FuelUsed,0) FuelUsed,
        IsNull(PassengerTotal,0) PassengerTotal,
        FuelLocatorCD,
        IsNull(FuelQTY,0) FuelQTY,
        IsNull(ExpenseAMT,0) ExpenseAMT,  
        InboundDTTM,
        IsNull(Distance,0)/NullIf(FlightHours,0) FltSpeed,
        IsNull(FuelUsed,0) FuelBurn,
        IsNull(FuelUsed,0)/NullIf(BlockHours,0) FuelBHR,         
        IsNull(FuelUsed,0)/NullIf(FlightHours,0) FuelFHR, 
        IsNull(ExpenseAMT,0) FuelPrice, 
        HoursAway=(dbo.GetHoursAway(@UserCD,POLegID,POLogID)),
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt order by AircraftCD, LogNum,EstDepartureDT, ScheduleDTTMLocal, LegNUM
  END  
 ELSE
  BEGIN  
    Select Distinct AircraftCD,POLogID,POLegID,TailNum,
        CONVERT(VARCHAR(5), ScheduleDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS [ScheduleDTTMLocal],
        DispatchNum,
        IsNull(LogNum,0) LogNum,
        IsNull(LegNUM,0) LegNUM,
        DepartICAOID,
        ArrivalICAOID,
        FlightCatagoryCD,
        SeatLeft,
        SeatRight,
        IsNull(Distance,0) Distance,
        IsNull(StatuteMiles,0) StatuteMiles,
        DelayTypeCD,
        DelayTM,
        IsNull(BlockHours,0) BlockHours,
        IsNull(FlightHours,0)FlightHours,   
        IsNull(FuelUsed,0) FuelUsed,
        IsNull(PassengerTotal,0) PassengerTotal,
        FuelLocatorCD,
        IsNull(FuelQTY,0) FuelQTY,
        IsNull(ExpenseAMT,0) ExpenseAMT, 
       
        InboundDTTM,
        IsNull(Distance,0)/NullIf(FlightHours,0) FltSpeed,
        IsNull(FuelUsed,0) FuelBurn,
        IsNull(FuelUsed,0)/NullIf(BlockHours,0) FuelBHR,         
        IsNull(FuelUsed,0)/NullIf(FlightHours,0) FuelFHR, 
        IsNull(ExpenseAMT,0) FuelPrice, 
        HoursAway=(dbo.GetHoursAway(@UserCD,POLegID,POLogID)),
        OutboundDTTM,
        SLIPNUM = SlipNUM,
        @Units Units,
        @TenToMin TenToMin,
        @DayMonth DayMonth,
        @DateRange DateRange From #TmpRpt order by AircraftCD, LogNum, ScheduleDTTMLocal, LegNUM
  END  
      	
 
 END  

 --EXEC spGetReportPOSTAircraftPerformance 'supervisor_99','31-oct-2000','02-nov-2015','N400W' 

GO


