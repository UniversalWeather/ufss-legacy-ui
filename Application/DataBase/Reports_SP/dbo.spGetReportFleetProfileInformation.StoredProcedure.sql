IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetReportFleetProfileInformation]
	@UserCD VARCHAR(30)
	,@TailNum char(6)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileInformation
-- Author: SUDHAKAR J
-- Create date: 21 Jun 2012
-- Description: Get FleetProfile Information for Report 
-- Revision History
-- Date			Name		Ver		Change
-- 29 Aug		Sudhakar	1.1		ClientID filter
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

DECLARE @TenToMin INT
SELECT @TenToMin = Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD) 
--SELECT @TenToMin
	SELECT DISTINCT
		 [AcrftCD]    = F.AircraftCD
		,[AcrftTN]    = F.TailNum
		,[AcrftTCD]   = A.AircraftCD
		,[TypeDes]    = F.TypeDescription
		,[FRWing]     = A.IsFixedRotary
		,[SNum]		  = F.SerialNum
		,[AcrftHBase] = AP.IcaoID
		,[MinRway]	  = F.MimimumRunway
		,[MaxPax]     = F.MaximumPassenger
		,[MaxRes]     = F.MaximumReservation
		,[Client]     = C.ClientCD
    	,[Vendor]     = V.VendorCD
		,[LHInsp]     = dbo.GetShortDateFormatByUserCD(@UserCD, F.LastInspectionDT) --CONVERT(varchar(10), F.LastInspectionDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[HtNInsp]    = F.InspectionHrs
		,[HtWarn]     = F.WarningHrs
		,[MaxFLoad]   = F.MaximumFuel
		,[MinArrFuel] = F.MinimumFuel
		,[BOWgt]      = F.BasicOperatingWeight
		,[Status]     = F.IsInActive
		,[DFPhn]      = F.FlightPhoneNum
		,[IFltPhn]    = F.FlightPhoneIntlNum 
		,[SMControl]  = F.SIFLMultiControlled
		,[SMNControl] = F.SIFLMultiNonControled
		---------------------------------------------------
		,[DeclNum]    = FP.BuyAircraftAdditionalFeeDOM
        ,[AcrftMk]    = FP.AircraftMake
		,[AcrftMdl]   = FP.AircraftModel
		,[AcrftClr1]  = FP.AircraftColor1
		,[AcrftClr2]  = FP.AircraftColor2
		,[AcrftTrm]   = FP.AircraftTrimColor
		,[OwnerLes]   = FP.OwnerLesse
		,[Add1]       = FP.Addr1
		,[Add2]       = FP.Addr2
		,[City]       = FP.CityName
		,[StProv]     = FP.StateName
		,[Postal]     = FP.PostalZipCD 
	    ,[Country]    = CY.CountryName		
		----------------------------------------------------
        ,[AfrmTHrs]   = FI.AirframeHrs
		,[AfrmCycl]   =	FI.AirframeCycle
		,[AfrmAsOf]   =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.AIrframeAsOfDT)
		,[E1TH]		  =	CASE(FI.Engine1Hrs) WHEN 0 THEN NULL ELSE FI.Engine1Hrs END
		,[E1Cy]		  =	FI.Engine1Cycle
		,[E1AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Engine1AsOfDT)
		,[E2TH]       =	CASE(FI.Engine2Hrs) WHEN 0 THEN NULL ELSE FI.Engine2Hrs END
		,[E2Cy]       =	FI.Engine2Cycle
		,[E2AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Engine2AsOfDT)
		,[E3TH]       =	CASE(FI.Engine3Hrs) WHEN 0 THEN NULL ELSE FI.Engine3Hrs END
		,[E3Cy]       =	FI.Engine3Cycle
		,[E3AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Engine3AsOfDT)
		,[E4TH]       =	CASE(FI.Engine4Hrs) WHEN 0 THEN NULL ELSE FI.Engine4Hrs END
		,[E4Cy]       =	FI.Engine4Cycle
		,[E4AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Engine4AsOfDT)
		,[R1TH]       =	CASE(FI.Reverse1Hrs) WHEN 0 THEN NULL ELSE FI.Reverse1Hrs END
		,[R1Cy]       =	FI.Reverser1Cycle
		,[R1AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Reverser1AsOfDT)
		,[R2TH]       =	CASE(FI.Reverse2Hrs) WHEN 0 THEN NULL ELSE FI.Reverse1Hrs END
		,[R2Cy]       =	FI.Reverser2Cycle
		,[R2AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Reverser2AsOfDT)
		,[R3TH]       =	CASE(FI.Reverse3Hrs) WHEN 0 THEN NULL ELSE FI.Reverse3Hrs END
		,[R3Cy]       =	FI.Reverser3Cycle
		,[R3AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Reverser3AsOfDT)
		,[R4TH]       =	CASE(FI.Reverse4Hrs) WHEN 0 THEN NULL ELSE FI.Reverse4Hrs END
		,[R4Cy]       =	FI.Reverser4Cycle
		,[R4AO]       =	dbo.GetShortDateFormatByUserCD(@UserCD, FI.Reverser4AsOfDT)
		,[FBH1]       =	CASE(FI.MACHFuel1Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel1Hrs END
		,[FBH2]       =	CASE(FI.MACHFuel2Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel2Hrs END
		,[FBH3]       =	CASE(FI.MACHFuel3Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel3Hrs END
		,[FBH4]       =	CASE(FI.MACHFuel4Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel4Hrs END
		,[FBH5]       =	CASE(FI.MACHFuel5Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel5Hrs END
		,[FBH6]       =	CASE(FI.MACHFuel6Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel6Hrs END
		,[FBH7]       =	CASE(FI.MACHFuel7Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel7Hrs END
		,[FBH8]       =	CASE(FI.MACHFuel8Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel8Hrs END
		,[FBH9]       =	CASE(FI.MACHFuel9Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel9Hrs END
		,[FBH10]      =	CASE(FI.MACHFuel10Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel10Hrs END
		,[FBH11]      =	CASE(FI.MACHFuel11Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel11Hrs END
		,[FBH12]      =	CASE(FI.MACHFuel12Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel12Hrs END
		,[FBH13]      =	CASE(FI.MACHFuel13Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel13Hrs END
		,[FBH14]      =	CASE(FI.MACHFuel14Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel14Hrs END
		,[FBH15]      =	CASE(FI.MACHFuel15Hrs) WHEN 0 THEN NULL ELSE FI.MACHFuel15Hrs END
		,[TenToMin]	  = @TenToMin     
        
		
	FROM Fleet F 
	     LEFT OUTER JOIN FleetComponent FC
			     ON F.CustomerID = FC.CustomerID
		        AND F.FleetID = FC.FleetID
	     LEFT OUTER JOIN Aircraft A
			     ON F.AircraftID = A.AircraftID
	     LEFT OUTER JOIN Client C
			     ON F.ClientID = C.ClientID
	     LEFT OUTER JOIN FleetInformation FI
	             ON F.FleetID = FI.FleetID 
	     LEFT OUTER JOIN FleetPair FP
	             ON F.FleetID = FP.FleetID
	      LEFT JOIN Vendor V
	             ON F.VendorID = V.VendorID
	      LEFT JOIN Country CY
	             ON FP.CountryID = CY.CountryID	
	      LEFT JOIN Company CM
	             ON F.HomebaseID = CM.HomebaseID
	      LEFT JOIN Airport AP
	             ON CM.HomebaseAirportID = AP.AirportID	 	    
	    
	     
	WHERE F.IsDeleted = 0
	  AND F.CustomerID = @CUSTOMERID
	  AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	  AND F.TailNum = RTRIM(@TailNum)

-- EXEC spGetReportFleetProfileInformation 'jwilliams_11', 'N46E'




GO


