

/****** Object:  StoredProcedure [dbo].[spGetReportPREAdhocInformation]    Script Date: 01/14/2015 13:34:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAdhocInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAdhocInformation]
GO



/****** Object:  StoredProcedure [dbo].[spGetReportPREAdhocInformation]    Script Date: 01/14/2015 13:34:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[spGetReportPREAdhocInformation]
(
	 @UserCustomerID AS VARCHAR(30), --Mandatory
	 @TripIDs AS VARCHAR(MAX),
	 @IsLeg AS BIT=0, 
	 @IsPax AS BIT=0, 
	 @IsCrew AS BIT=0
)      
AS
-- ================================================================================  
-- SPC Name: spGetReportPREAdhocInformation       
-- Author: Jajati Badu
-- Create date: 09/01/2015
-- Description: To Get the PreFlight Details
-- Revision History         
-- Date   Name  Ver  Change         
-- ================================================================================  
BEGIN     
SET NOCOUNT ON  

DECLARE @TripIDTable TABLE ( TripID BIGINT )
INSERT INTO @TripIDTable (TripID) EXEC spSplitText @TripIDs  
--SELECT * FROM @TripIDTable

SELECT DISTINCT --------------PreFlightMain------------------
           TripNUM=PM.TripNUM
		 ,PreviousNUM=PM.PreviousNUM
		  ,MainDescription=PM.TripDescription		  
		  ,DispatchNum=PM.DispatchNUM
		  ,TripStatus=PM.TripStatus
		  ,EstDepartureDT=PM.EstDepartureDT
		  ,EstArrivalDT=PM.EstArrivalDT
		  ,RecordType=PM.RecordType		 
		  ,TripRequestorCD=LReq.PassengerRequestorCD
		  ,TripRequestorName=PM.RequestorFirstName+' '+PM.RequestorMiddleName+' '+PM.RequestorLastName
		  ,RequestorPhoneNUM=PM.RequestorPhoneNUM
		  ,DepartmentDescription=PM.DepartmentDescription
		  ,AuthorizationDescription=PM.AuthorizationDescription
		  ,RequestDT=PM.RequestDT
		  ,IsCrew=PM.IsCrew
		  ,IsPassenger=PM.IsPassenger
		  ,IsQuote=PM.IsQuote
		  ,IsScheduledServices=PM.IsScheduledServices
		  ,IsAlert=PM.IsAlert
		  ,TripNotes=PM.Notes
		  ,LogisticsHistory=PM.LogisticsHistory
		  ,IsAirportAlert=PM.IsAirportAlert
		  ,Islog=PM.IsLog
		  ,TripBeginningGMTDT=PM.BeginningGMTDT
		  ,TripEndingGMTDT=PM.EndingGMTDT
		  ,IsPrivate=PM.IsPrivate
		  ,IsCorpReq=PM.IsCorpReq
		  ,Acknowledge=PM.Acknowledge
		  ,DispatchNotes=PM.DispatchNotes
		  ,TripRequest=PM.TripRequest
		  ,WaitList=PM.WaitList
		  ,FlightNum=PM.FlightNum
		  ,TripFlightCost=PM.FlightCost
		  ,TripException=PM.TripException
		  ,TripSheetNotes=PM.TripSheetNotes
		  ,CancelDescription=PM.CancelDescription
		  ,IsCompleted=PM.IsCompleted
		  ,IsFBOUpdFlag=PM.IsFBOUpdFlag
		  ,RevisionNUM=PM.RevisionNUM
		  ,RevisionDescriptioin=PM.RevisionDescriptioin
		  ,IsPersonal=PM.IsPersonal
		  ,DsptnName=PM.DsptnName
		  ,ScheduleCalendarHistory=PM.ScheduleCalendarHistory
		  ,ScheduleCalendarUpdate=PM.ScheduleCalendarUpdate
		  ,CommentsMessage=PM.CommentsMessage
		  ,TripEstFuelQTY=PM.EstFuelQTY
		  ,TripCrewCD=MCR.CrewCD
		  ,ReleasedBy=PM.ReleasedBy
		  ,VerifyNUM=PM.VerifyNUM
		  ,APISSubmit=PM.APISSubmit
		  ,APISStatus=PM.APISStatus
		  ,APISException=PM.APISException
		  ,IsAPISValid=PM.IsAPISValid
		  ,DispatcherUserName=PM.DispatcherUserName
		  ,EmergencyContactCD=EC.EmergencyContactCD
		  ,FleetCalendarNotes=PM.FleetCalendarNotes
		  ,CrewCalendarNotes=PM.CrewCalendarNotes
		  ,TripFuelUnits=PM.FuelUnits
		  ,TripCQCustomerCD=CQC.CQCustomerCD
		  ,IsTripCopied=PM.IsTripCopied
		  ,AccountNum=MAC.AccountNum
		  ,TripDepartmentCD=MD.DepartmentCD
		  ,TripAuthorizationCD=MDA.AuthorizationCD
		  ,TripClientCD=MC.ClientCD
		  ,TailNum=MF.TailNum
		  ,AircraftCD=MAT.AircraftCD
		  ,HomeBaseCD=MA.IcaoID
		--------------PreFlightLeg----------------------	
		  ,LegNUM=CASE WHEN @IsLeg=1 THEN PL.LegNUM ELSE NULL END
		  ,DepartICAO=CASE WHEN @IsLeg=1 THEN LDEP.IcaoID ELSE NULL END
		  ,ArriveICAO=CASE WHEN @IsLeg=1 THEN LARR.IcaoID ELSE NULL END
		  ,Distance=CASE WHEN @IsLeg=1 THEN PL.Distance ELSE NULL END
		  ,PowerSetting=CASE WHEN @IsLeg=1 THEN PL.PowerSetting ELSE NULL END
		  ,TakeoffBIAS=CASE WHEN @IsLeg=1 THEN PL.TakeoffBIAS ELSE NULL END
		  ,LandingBias=CASE WHEN @IsLeg=1 THEN PL.LandingBias ELSE NULL END
		  ,TrueAirSpeed=CASE WHEN @IsLeg=1 THEN PL.TrueAirSpeed ELSE NULL END
		  ,WindsBoeingTable=CASE WHEN @IsLeg=1 THEN PL.WindsBoeingTable ELSE NULL END
		  ,ElapseTM=CASE WHEN @IsLeg=1 THEN PL.ElapseTM ELSE NULL END
		  ,IsDepartureConfirmed=CASE WHEN @IsLeg=1 THEN PL.IsDepartureConfirmed ELSE NULL END
		  ,IsArrivalConfirmation=CASE WHEN @IsLeg=1 THEN PL.IsArrivalConfirmation ELSE NULL END
		  ,IsApproxTM=CASE WHEN @IsLeg=1 THEN PL.IsApproxTM ELSE NULL END
		  ,IsScheduledServicesLeg=CASE WHEN @IsLeg=1 THEN PL.IsScheduledServices ELSE NULL END
		  ,DepartureDTTMLocal=CASE WHEN @IsLeg=1 THEN PL.DepartureDTTMLocal ELSE NULL END
		  ,ArrivalDTTMLocal=CASE WHEN @IsLeg=1 THEN PL.ArrivalDTTMLocal ELSE NULL END
		  ,DepartureGreenwichDTTM=CASE WHEN @IsLeg=1 THEN PL.DepartureGreenwichDTTM ELSE NULL END
		  ,ArrivalGreenwichDTTM=CASE WHEN @IsLeg=1 THEN PL.ArrivalGreenwichDTTM ELSE NULL END
		  ,HomeDepartureDTTM=CASE WHEN @IsLeg=1 THEN PL.HomeDepartureDTTM ELSE NULL END
		  ,HomeArrivalDTTM=CASE WHEN @IsLeg=1 THEN PL.HomeArrivalDTTM ELSE NULL END
		  ,GoTime=CASE WHEN @IsLeg=1 THEN PL.GoTime ELSE NULL END
		  ,FBOVendor=CASE WHEN @IsLeg=1 THEN LFBO.FBOVendor ELSE NULL END
		  ,LogBreak=CASE WHEN @IsLeg=1 THEN PL.LogBreak ELSE NULL END
		  ,FlightPurpose=CASE WHEN @IsLeg=1 THEN PL.FlightPurpose ELSE NULL END
		  ,LegRequestorName=CASE WHEN @IsLeg=1 THEN PL.RequestorName ELSE NULL END
		  ,DepartmentName=CASE WHEN @IsLeg=1 THEN PL.DepartmentName ELSE NULL END
		  ,AuthorizationName=CASE WHEN @IsLeg=1 THEN PL.AuthorizationName ELSE NULL END
		  ,PassengerTotal=CASE WHEN @IsLeg=1 THEN PL.PassengerTotal ELSE NULL END
		  ,SeatTotal=CASE WHEN @IsLeg=1 THEN PL.SeatTotal ELSE NULL END
		  ,ReservationTotal=CASE WHEN @IsLeg=1 THEN PL.ReservationTotal ELSE NULL END
		  ,ReservationAvailable=CASE WHEN @IsLeg=1 THEN PL.ReservationAvailable ELSE NULL END
		  ,WaitNUM=CASE WHEN @IsLeg=1 THEN PL.WaitNUM ELSE NULL END
		  ,DutyTYPE=CASE WHEN @IsLeg=1 THEN PL.DutyTYPE ELSE NULL END
		  ,DutyHours=CASE WHEN @IsLeg=1 THEN PL.DutyHours ELSE NULL END
		  ,RestHours=CASE WHEN @IsLeg=1 THEN PL.RestHours ELSE NULL END
		  ,FlightHours=CASE WHEN @IsLeg=1 THEN PL.FlightHours ELSE NULL END
		  ,LegNotes=CASE WHEN @IsLeg=1 THEN PL.Notes ELSE NULL END
		  ,FuelLoad=CASE WHEN @IsLeg=1 THEN PL.FuelLoad ELSE NULL END
		  ,OutbountInstruction=CASE WHEN @IsLeg=1 THEN PL.OutbountInstruction ELSE NULL END
		  ,DutyTYPE1=CASE WHEN @IsLeg=1 THEN PL.DutyTYPE1 ELSE NULL END
		  ,IsCrewDiscount=CASE WHEN @IsLeg=1 THEN PL.IsCrewDiscount ELSE NULL END
		  ,CrewDutyRuleCD=CASE WHEN @IsLeg=1 THEN LCD.CrewDutyRuleCD ELSE NULL END
		  ,IsDutyEnd=CASE WHEN @IsLeg=1 THEN PL.IsDutyEnd ELSE NULL END
		  ,FedAviationRegNum=CASE WHEN @IsLeg=1 THEN PL.FedAviationRegNum ELSE NULL END
		  ,WindReliability=CASE WHEN @IsLeg=1 THEN PL.WindReliability ELSE NULL END
		  ,OverrideValue=CASE WHEN @IsLeg=1 THEN PL.OverrideValue ELSE NULL END
		  ,CheckGroup=CASE WHEN @IsLeg=1 THEN PL.CheckGroup ELSE NULL END
		  ,AdditionalCrew=CASE WHEN @IsLeg=1 THEN PL.AdditionalCrew ELSE NULL END
		  ,PilotInCommand=CASE WHEN @IsLeg=1 THEN PL.PilotInCommand ELSE NULL END
		  ,SecondInCommand=CASE WHEN @IsLeg=1 THEN PL.SecondInCommand ELSE NULL END
		  ,NextLocalDTTM=CASE WHEN @IsLeg=1 THEN PL.NextLocalDTTM ELSE NULL END
		  ,NextGMTDTTM=CASE WHEN @IsLeg=1 THEN PL.NextGMTDTTM ELSE NULL END
		  ,NextHomeDTTM=CASE WHEN @IsLeg=1 THEN PL.NextHomeDTTM ELSE NULL END
		  ,CrewNotes=CASE WHEN @IsLeg=1 THEN PL.CrewNotes ELSE NULL END
		  ,IsPrivateLeg=CASE WHEN @IsLeg=1 THEN PL.IsPrivate ELSE NULL END
		  ,LegWaitList=CASE WHEN @IsLeg=1 THEN PL.WaitList ELSE NULL END
		  ,LegFlightNUM=CASE WHEN @IsLeg=1 THEN PL.FlightNUM ELSE NULL END
		  ,LegFlightCost=CASE WHEN @IsLeg=1 THEN PL.FlightCost ELSE NULL END
		  ,CrewFuelLoad=CASE WHEN @IsLeg=1 THEN PL.CrewFuelLoad ELSE NULL END
		  ,UWAID=CASE WHEN @IsLeg=1 THEN PL.UWAID ELSE NULL END
		  ,USCrossing=CASE WHEN @IsLeg=1 THEN PL.USCrossing ELSE NULL END
		  ,Response=CASE WHEN @IsLeg=1 THEN PL.Response ELSE NULL END
		  ,ConfirmID=CASE WHEN @IsLeg=1 THEN PL.ConfirmID ELSE NULL END
		  ,CrewDutyAlert=CASE WHEN @IsLeg=1 THEN PL.CrewDutyAlert ELSE NULL END
		  ,LegEstFuelQTY=CASE WHEN @IsLeg=1 THEN PL.EstFuelQTY ELSE NULL END
		  ,LegFuelUnits=CASE WHEN @IsLeg=1 THEN PL.FuelUnits ELSE NULL END
		  ,DEPHomeBaseCD=CASE WHEN @IsLeg=1 THEN LDEP.IcaoID ELSE NULL END
		  ,ARRHomeBaseCD=CASE WHEN @IsLeg=1 THEN LARR.IcaoID ELSE NULL END
		  ,LegDepartmentCD=CASE WHEN @IsLeg=1 THEN LD.DepartmentCD ELSE NULL END
		  ,LegAuthorizationCD=CASE WHEN @IsLeg=1 THEN LDA.AuthorizationCD ELSE NULL END
		  ,FlightCatagoryCD=CASE WHEN @IsLeg=1 THEN LFC.FlightCatagoryCD ELSE NULL END

        
		  -------------------------------PreFlightPassenger------------------------------
			,PassengerHotelName = CASE WHEN @IsPax=1 THEN PP.HotelName ELSE NULL END 
			,PassengerFirstName = CASE WHEN @IsPax=1 THEN PP.PassengerFirstName ELSE NULL END 
			,PassengerMiddleName = CASE WHEN @IsPax=1 THEN PP.PassengerMiddleName ELSE NULL END 
			,PassengerLastName = CASE WHEN @IsPax=1 THEN PP.PassengerLastName ELSE NULL END			
			,PassengerFlightPurposeCD = CASE WHEN @IsPax=1 THEN FP.FlightPurposeCD ELSE NULL END 
			,FlightPurposeDescription = CASE WHEN @IsPax=1 THEN FP.FlightPurposeDescription ELSE NULL END 
			,PassengerPassportNUM = CASE WHEN @IsPax=1 THEN PP.PassportNUM ELSE NULL END 
			,Billing = CASE WHEN @IsPax=1 THEN PP.Billing ELSE NULL END 
			,IsNonPassenger = CASE WHEN @IsPax=1 THEN PP.IsNonPassenger ELSE NULL END 
			,IsBlocked = CASE WHEN @IsPax=1 THEN PP.IsBlocked ELSE NULL END 
			,PassengerOrderNUM = CASE WHEN @IsPax=1 THEN PP.OrderNUM ELSE NULL END 
			,ReservationNUM = CASE WHEN @IsPax=1 THEN PP.ReservationNUM ELSE NULL END 
			,PassengerWaitList = CASE WHEN @IsPax=1 THEN PP.WaitList ELSE NULL END
			,AssociatedPassenger = CASE WHEN @IsPax=1 THEN PP.AssociatedPassenger ELSE NULL END
			,PassengerTripStatus = CASE WHEN @IsPax=1 THEN PP.TripStatus ELSE NULL END
			,TSADTTM = CASE WHEN @IsPax=1 THEN PP.TSADTTM ELSE NULL END
			,PassengerNoofRooms = CASE WHEN @IsPax=1 THEN PP.NoofRooms ELSE NULL END
			,PassengerClubCard = CASE WHEN @IsPax=1 THEN PP.ClubCard ELSE NULL END
			,IsRateCapPassenger = CASE WHEN @IsPax=1 THEN PP.IsRateCap ELSE NULL END
			,PassengerMaxCapAmount = CASE WHEN @IsPax=1 THEN PP.MaxCapAmount ELSE NULL END
			,PassengerStreet = CASE WHEN @IsPax=1 THEN PP.Street ELSE NULL END
			,PassengerCityName = CASE WHEN @IsPax=1 THEN PP.CityName ELSE NULL END
			,PassengerStateName = CASE WHEN @IsPax=1 THEN PP.StateName ELSE NULL END
			,PassengerPostalZipCD = CASE WHEN @IsPax=1 THEN PP.PostalZipCD ELSE NULL END
			------------------------PreFlightCrew-------------------------
			,CrewHotelName = CASE WHEN @IsCrew=1 THEN PC.HotelName ELSE NULL END
			,CrewFirstName = CASE WHEN @IsCrew=1 THEN PC.CrewFirstName ELSE NULL END
			,CrewMiddleName = CASE WHEN @IsCrew=1 THEN PC.CrewMiddleName ELSE NULL END
			,CrewLastName = CASE WHEN @IsCrew=1 THEN PC.CrewLastName ELSE NULL END
			,CrewPassportNum = CASE WHEN @IsCrew=1 THEN CPP.PassportNum ELSE NULL END
			,CrewDutyTYPE = CASE WHEN @IsCrew=1 THEN PC.DutyTYPE ELSE NULL END
			,CrewOrderNUM = CASE WHEN @IsCrew=1 THEN PC.OrderNUM ELSE NULL END
			,IsDiscount = CASE WHEN @IsCrew=1 THEN PC.IsDiscount ELSE NULL END
			,IsBlackberrMailSent = CASE WHEN @IsCrew=1 THEN PC.IsBlackberrMailSent ELSE NULL END
			,IsNotified = CASE WHEN @IsCrew=1 THEN PC.IsNotified ELSE NULL END
			,IsAdditionalCrew = CASE WHEN @IsCrew=1 THEN PC.IsAdditionalCrew ELSE NULL END
			,CrewStreet = CASE WHEN @IsCrew=1 THEN PC.Street ELSE NULL END
			,CrewCityName = CASE WHEN @IsCrew=1 THEN PC.CityName ELSE NULL END
			,CrewStateName = CASE WHEN @IsCrew=1 THEN PC.StateName ELSE NULL END
			,CrewPostalZipCD = CASE WHEN @IsCrew=1 THEN PC.PostalZipCD ELSE NULL END
			,CrewNoofRooms = CASE WHEN @IsCrew=1 THEN PC.NoofRooms ELSE NULL END
			,CrewClubCard = CASE WHEN @IsCrew=1 THEN PC.ClubCard ELSE NULL END
			,IsRateCapCrew = CASE WHEN @IsCrew=1 THEN PC.IsRateCap ELSE NULL END
			,CrewMaxCapAmount = CASE WHEN @IsCrew=1 THEN PC.MaxCapAmount ELSE NULL END
			


   FROM @TripIDTable PLT 
		  INNER JOIN PreflightMain PM ON PLT.TripID = PM.TripID AND PM.IsDeleted=0
          INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=0
		  LEFT OUTER JOIN Account MAC ON MAC.AccountID=PM.AccountID
		  LEFT OUTER JOIN Department MD ON MD.DepartmentID=PM.DepartmentID
		  LEFT OUTER JOIN DepartmentAuthorization MDA ON MDA.AuthorizationID=PM.AuthorizationID
		  LEFT OUTER JOIN Client MC ON MC.ClientID=PM.ClientID
		  LEFT OUTER JOIN Fleet MF ON MF.FleetID=PM.FleetID
		  LEFT OUTER JOIN Aircraft MAT ON MAT.AircraftID=PM.AircraftID
		  LEFT OUTER JOIN Company MCO ON MCO.HomebaseID=PM.HomebaseID
		  LEFT OUTER JOIN Crew MCR ON MCR.CrewID=PM.CrewID
		  LEFT OUTER JOIN Airport MA ON MA.AirportID=MCO.HomebaseAirportID
		  LEFT OUTER JOIN EmergencyContact EC ON PM.EmergencyContactID=EC.EmergencyContactID
		  LEFT OUTER JOIN CQCustomer CQC ON PM.CQCustomerID=cqc.CQCustomerID

		  LEFT OUTER JOIN Airport LDEP ON LDEP.AirportID=PL.DepartICAOID
		  LEFT OUTER JOIN Airport LARR ON LARR.AirportID=PL.ArriveICAOID
		  LEFT OUTER JOIN Client LC ON PL.ClientID=LC.ClientID
		  LEFT OUTER JOIN Account LAC ON LAC.AccountID=PL.AccountID
		  LEFT OUTER JOIN Department LD ON LD.DepartmentID=PL.DepartmentID
		  LEFT OUTER JOIN DepartmentAuthorization LDA ON LDA.AuthorizationID=PL.AuthorizationID
		  LEFT OUTER JOIN FlightCatagory LFC ON LFC.FlightCategoryID=PL.FlightCategoryID
		  LEFT OUTER JOIN FBO LFBO ON LFBO.FBOID=PL.FBOID
		  LEFT OUTER JOIN CrewDutyRules LCD ON LCD.CrewDutyRulesID=PL.CrewDutyRulesID

		  LEFT OUTER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID AND @IsPax = 1
		  LEFT OUTER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID AND @IsPax = 1
		  LEFT OUTER JOIN Passenger LReq ON LReq.PassengerRequestorID = PM.PassengerRequestorID
		  LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID=PP.FlightPurposeID
		  
		  LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID AND @IsCrew = 1
		  LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID AND @IsCrew = 1
		  LEFT OUTER JOIN CrewPassengerPassport CPP ON CPP.PassportID=PC.PassportID
		  

   WHERE PM.CustomerID=@UserCustomerID
    AND PM.IsDeleted=0
    AND PL.IsDeleted=0
   

END
