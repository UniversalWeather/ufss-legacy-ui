IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCrewDutyOverviewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCrewDutyOverviewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
CREATE PROCEDURE [dbo].[spGetReportPRETSCrewDutyOverviewInformation]  
 @UserCD            AS VARCHAR(30) 
 ,@TripID			AS VARCHAR(30)     
 ,@LegNum			AS VARCHAR(30)
/* ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal   
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S' 
 */ 
AS  
BEGIN  
  
-- =========================================================================  
-- SPC Name: spGetReportPRETSCrewDutyOverviewInformation  
-- Author: Askar 
-- Create date: 13 Sep 2012  
-- Description: Get Preflight Tripsheet Report Export information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================  

SET NOCOUNT ON    
  
DECLARE @CUSTOMER BIGINT=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
  
DECLARE @TenToMin NUMERIC(1,0)  
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
   
  
   
  SELECT  DISTINCT  CONVERT(VARCHAR,PM.TripNUM) TripNumber,  
           PM.DispatchNUM DispatchNo,  
           F.TailNum TailNumber,  
           CONVERT(VARCHAR,PL.LegNUM) LegNum,  
           PL.DepartureDTTMLocal Date,  
           PL.DepartureDTTMLocal Dep,  
           D.IcaoID From_D,  
           PL.ArrivalDTTMLocal Arrive,  
           A.IcaoID TO_A,  
           SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) Crew,  
           CASE(PL.IsDutyEnd) WHEN 0 THEN 'N' ELSE 'Y' END DutyDayEnd,  
            PL.CheckGroup Domestic_IntlLeg,  
           ISNULL(PL.ElapseTM,0.0) LegTime,   
           ISNULL(PL.FlightHours,0.0) TotBlock,  
           ISNULL(PL.DutyHours,0.0) TotDuty,  
           ISNULL(PL.RestHours,0.0) RestPeriod,  
          (CASE WHEN PL.CrewDutyAlert IS NOT NULL THEN (CASE WHEN PL.CrewDutyAlert <>'' THEN 'Y' ELSE 'N' END) ELSE 'N' END)    
           RulesExceeded,  
           CDR.CrewDutyRuleCD CrewRules,PL.FedAviationRegNUM FAR,   
           DATEDIFF(DAY, CONVERT(DATE,Departure), CONVERT(DATE,Arrival))+1 TotalTripDays,  
           ISNULL(MIN(PL1.MinRest),0.0) MinimumRestPeriod,  
           ISNULL(MAX(PL1.MaxRest),0.0) MaximumRestPeriod,  
           PL.IsApproxTM ApproxTM,         
		   @TenToMin TenToMin,  
		   [DispatcherName] = ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,''),  
		   [DispatcherPhone] = UM.PhoneNum,  
		   [Dispatcheremail] = UM.EmailID,
		   TotalTripFlightTime = CASE WHEN PL.IsDutyEnd = 1 OR PL.LegNUM = MAXLeg  THEN PL.FlightHours ELSE 0 END, 
           TotalTripDutyTime = CASE WHEN PL.IsDutyEnd = 1 OR PL.LegNUM = MAXLeg  THEN PL.DutyHours ELSE 0 END  
     FROM PreflightMain PM   
         INNER JOIN PreflightLeg PL ON  PM.TripID = PL.TripID AND PL.IsDeleted = 0      
		 LEFT OUTER JOIN (  
						SELECT DISTINCT PC1.LEGID,CrewList = (  
						SELECT ISNULL(RTRIM(C.LastName)+ ', ' ,'')   
						FROM PreflightCrewList PC INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.IsDeleted = 0  
						WHERE PC.LEGID = PC1.LEGID   
						ORDER BY( CASE PC.DutyTYPE   
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4   
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)  
						FOR XML PATH('')  
						)  
						FROM PreflightCrewList PC1  
			       ) Crew ON PL.LEGID = Crew.LEGID   
		INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
		INNER JOIN Airport D ON PL.DepartICAOID=D.AirportID  
		INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID  
		LEFT OUTER JOIN CrewDutyRules CDR ON CDR.CrewDutyRulesID=PL.CrewDutyRulesID  
		INNER JOIN (SELECT MIN(RestHours) MinRest,MAX(RestHours) MaxRest,TripID,TripNUM FROM PreflightLeg WHERE IsDeleted = 0 GROUP BY TripID,TripNUM) PL1 ON PL.TripID=PL1.TripID  
		INNER JOIN (SELECT TripID,TripNUM,MIN(DepartureDTTMLocal) Departure,MAX(ArrivalDTTMLocal) Arrival, MAX(LegNUM) MAXLeg FROM PreflightLeg WHERE CUSTOMERID=@CUSTOMER AND IsDeleted = 0 GROUP BY TripID,TripNUM) PLDATE ON PLDATE.TripID=PL.TripID  
		INNER JOIN (SELECT TripNUM,TripID,DutyHours,LegNUM Leg FROM PreflightLeg WHERE CUSTOMERID=@CUSTOMER AND IsDeleted=0 AND LegNUM IN(SELECT MAX(LEGNUM) FROM PreflightLeg WHERE TripID=@TripID AND CustomerID=@CUSTOMER AND IsDeleted = 0))PL2 ON PL.TripID=PL2.TripID --AND PL1.LegNUM=PL2.Leg  
		LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)   
     WHERE PM.TripID = CONVERT(BIGINT,@TripID)  
	   AND PM.IsDeleted = 0  
	   AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   GROUP BY PM.TripNUM,PL.LegNUM,PM.DispatchNUM,F.TailNum,PL.DepartureDTTMLocal,PL.DepartureDTTMLocal  
			,D.IcaoID,PL.ArrivalDTTMLocal,A.IcaoID,Crew.CrewList,PL.IsDutyEnd,PL.DutyTYPE,PL.CheckGroup  
			,PL.ElapseTM,PL.FlightHours,PL.DutyHours,PL.RestHours,CDR.MaximumDutyHrs,CDR.MinimumRestHrs
			,CDR.CrewDutyRuleCD,PL.FedAviationRegNUM,PL.IsApproxTM,PL.CrewDutyAlert,PLDATE.Departure,PLDATE.Arrival,UM.FirstName,UM.LastName,UM.PhoneNum,UM.EmailID,PLDATE.MAXLeg   
             
            
END  

		--EXEC spGetReportPRETSCrewDutyOverviewInformation 'jwilliams_11', '100111831', ''  
GO


