IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestor]											
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @Year AS NVARCHAR(4),
    @UserHomebaseID AS VARCHAR(30),
    @RequestorCD AS VARCHAR(100) = '',
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @DepartmentCD AS VARCHAR(70)= '',
    @IsHomebase AS BIT = 0

 --EXEC spGetReportPOSTDepartmentChargeBackDetailByRequestor 'supervisor_99','10099','2011','','','','',0   
 -- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentChargeBackDetailByRequestor
-- Author:
-- Create date:
-- Description: Get FlightHours/BlockHours details based on Department, Tail Number and Account Number
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================ 											
AS 
BEGIN
SET NOCOUNT ON;
DECLARE  @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		HomeBaseID BIGINT,
		FleetGroupCD VARCHAR(6),
		TailNum VARCHAR(9)
  )
  


BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID, F.HomebaseID,FG.FleetGroupCD,F.TailNum
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD='')
	AND F.CustomerID = @UserCustomerID  
	AND F.IsDeleted=0
END




Declare @SuppressActivityDept BIT  
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

--DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @AircraftBasis numeric(1,0);
	  SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	   
  
--DECLARE @FromDate DATE =''-- '2009-01-01';
--DECLARE @ToDate DATE = '' --'2009-12-31;

--SET @FromDate = @Year + '-01-01'
--SET @ToDate = @Year + '-12-31'
--SET @FromDate = CONVERT (DATE, @FromDate)
--SET @ToDate = CONVERT (DATE,@ToDate)
DECLARE @MonthStart INT,@MonthEnd INT;
SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 

-----Retrieving Department details based on @FromDate and @ToDate
DECLARE @DateTable TABLE (SeqID INT IDENTITY, WorkDate DATE,DepartmentCD VARCHAR(10),DepartmentName VARCHAR(60) );
-----Retrieving Fleet details based on @FromDate and @ToDate
DECLARE @FleetDate TABLE
  (FLEETID BIGINT,
   FleetGroupCD CHAR(4),
   TailNum VARCHAR(9),
   HomebaseID BIGINT,
   WorkDate DATE,
   ScheduledMonth SMALLINT,
   ScheduledYear SMALLINT);
INSERT INTO @DateTable SELECT DISTINCT  DATE,D.DepartmentCD,d.DepartmentName FROM Department D CROSS JOIN  [dbo].[fnDateTable](@FromDate,@ToDate)
							        WHERE CustomerID=@UserCustomerID
									 AND IsDeleted=0
									 AND IsInActive=0
									AND  (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')



  INSERT INTO @FleetDate
   (FLEETID,
    FleetGroupCD,
    TailNum,
    HomebaseID,
    ScheduledMonth,
    ScheduledYear
    )
  SELECT DISTINCT
    vF.FleetID,
    vF.FleetGroupCD,
    vF.TailNum,
    vF.HomebaseID,
    MONTH(DT.WorkDate),
    YEAR(DT.WorkDate)
  FROM @DateTable DT
  CROSS JOIN @TempFleetID vF
  WHERE (vF.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
  --AND vF.CustomerID = @UserCustomerID
----Retrieving leg details based on Tail Number, Department and Account Number


SELECT FC.* INTO #FleetCalender FROM
	 (SELECT DISTINCT
	  FG.FleetID,
	  ISNULL(D.DepartmentCD,'ZZZZ') DepartmentCD,
	  D.DepartmentCD + '-' + ISNULL(P.PassengerRequestorCD,'NONE') AS DepartmentCD1,
	  DepartmentName = ISNULL(D.DepartmentName,'ZZZZ'),
	  P.PassengerRequestorCD,
	  P.PassengerName,
	  A.AccountNum,
	  PL.Distance,
	  CR.ChargeUnit,
	  DATEPART(MM,PL.ScheduledTM) AS SCHEDULEDMONTH,
	  DATEPART(yyyy,PL.ScheduledTM) AS ScheduledYear,
	  FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'Block Hours') 
	                                               WHEN 2 THEN CONVERT(VARCHAR(30),'Flight Hours') END,
	  /*CASE WHEN @AircraftBasis= 1 THEN (SUM(PL.BlockHours) OVER (PARTITION BY ISNULL(D.DepartmentCD,'NONE'),DATEPART(MM,PL.ScheduledTM),P.PassengerRequestorCD))
		WHEN @AircraftBasis = 2 THEN SUM(PL.FlightHours) OVER (PARTITION BY ISNULL(D.DepartmentCD,'NONE'),DATEPART(MM,PL.ScheduledTM),P.PassengerRequestorCD) END AS FlightBlockHours,
      CASE WHEN @AircraftBasis = 1 THEN
		(CASE WHEN CR.ChargeUnit = 'N' THEN PL.Distance*CR.ChargeRate
		WHEN CR.ChargeUnit = 'S' THEN ((PL.Distance*CR.ChargeRate)*1.15078)
		WHEN CR.ChargeUnit = 'H' THEN SUM((PL.FlightHours*CR.ChargeRate)) OVER (PARTITION BY ISNULL(D.DepartmentCD,'NONE'),DATEPART(MM,PL.ScheduledTM),P.PassengerRequestorCD)
		End)
	  WHEN @AircraftBasis = 2 THEN
	   (CASE WHEN CR.ChargeUnit = 'N' THEN (PL.Distance*CR.ChargeRate)
     	WHEN CR.ChargeUnit = 'S' THEN ((PL.Distance*CR.ChargeRate)*1.15078)
	    WHEN CR.ChargeUnit = 'H' THEN SUM((PL.BlockHours *CR.ChargeRate)) OVER (PARTITION BY ISNULL(D.DepartmentCD,'NONE'),DATEPART(MM,PL.ScheduledTM),P.PassengerRequestorCD)
	    END)
	END AS ChargeBack,*/
	CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1) END AS FlightBlockHours ,
         CASE WHEN CR.ChargeUnit = 'N' THEN  ISNULL((PL.Distance*CR.ChargeRate),0)
			  WHEN CR.ChargeUnit = 'S' THEN ISNULL(((PL.Distance*CR.ChargeRate)*1.15078),0)
			  WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ISNULL((ROUND(PL.BlockHours,1) *CR.ChargeRate),0)
				WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ISNULL((ROUND(PL.FlightHours,1) *CR.ChargeRate),0)
				END AS ChargeBack ,
	D.IsInActive,
	[TenToMin] = @TenToMin,
	MonthStart = @MonthStart,
	MonthEnd = @MonthEnd,
	D.DepartmentID,
	PM.LogNum,
	PL.LegNUM
	
FROM PostflightMain PM
JOIN PostflightLeg pl
ON PM.POLogID = PL.POLogID AND PM.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
JOIN Fleet FG
ON FG.FleetID = PM.FleetID
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID
INNER JOIN Department D
ON PL.DepartmentID = D.DepartmentID

LEFT OUTER JOIN Passenger P
ON PL.PassengerRequestorID = P.PassengerRequestorID
JOIN Company C
ON FG.HomebaseID = C.HomebaseID
LEFT OUTER JOIN Account A
ON A.AccountID = P.AccountID AND A.CustomerID = P.CustomerID
 LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
           ) CR ON PL.POLegID = CR.POLegID  
WHERE --DATEPART(YEAR,PL.ScheduledTM)= @YEAR AND
(CONVERT(DATE,PL.ScheduledTM) BETWEEN  CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND
(P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ','))OR @RequestorCD='')
--AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))OR @FleetGroupCD='')
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
AND PM.CustomerID = @UserCustomerID 
-- ORDER BY FD.ScheduledMonth
) FC



IF @SuppressActivityDept=0
BEGIN

INSERT INTO #FleetCalender(DepartmentID,DepartmentCD,DepartmentName,SCHEDULEDMONTH,ScheduledYear,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,PassengerName,PassengerRequestorCD,DepartmentCD1,FleetID)
----Inserting month for each Fleet
SELECT DISTINCT D.DepartmentID,D.DepartmentCD,D.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),@TenToMin,@MonthStart, @MonthEnd,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END),
                TF.PassengerName,TF.PassengerRequestorCD,D.DepartmentCD + '-' + ISNULL(TF.PassengerRequestorCD,'NONE') AS DepartmentCD1 ,ISNULL(TF.FleetID,0)
               FROM Department D CROSS JOIN @DateTable T 
                    LEFT OUTER JOIN #FleetCalender TF ON TF.DepartmentID=D.DepartmentID
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentCD=T.DepartmentCD AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH)

                           AND CustomerID=@UserCustomerID
                           AND IsDeleted=0
                           AND D.IsInActive=0

END

ELSE

BEGIN

INSERT INTO #FleetCalender(DepartmentID,DepartmentCD,DepartmentName,SCHEDULEDMONTH,ScheduledYear,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,PassengerName,PassengerRequestorCD,DepartmentCD1,FleetID)
----Inserting month for each Fleet
SELECT DISTINCT D.DepartmentID,D.DepartmentCD,D.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),@TenToMin,@MonthStart, @MonthEnd,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END),
                TF.PassengerName,TF.PassengerRequestorCD,D.DepartmentCD + '-' + ISNULL(TF.PassengerRequestorCD,'NONE') AS DepartmentCD1 ,ISNULL(TF.FleetID,0)
               FROM Department D CROSS JOIN @DateTable T 
                    INNER JOIN #FleetCalender TF ON TF.DepartmentID=D.DepartmentID
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentCD=T.DepartmentCD AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH)

                           AND CustomerID=@UserCustomerID
                           AND IsDeleted=0
                           AND D.IsInActive=0


END
  DECLARE @Count INT
    SELECT @Count=COUNT(*) FROM 
		 ( SELECT  DISTINCT DepartmentCD,PassengerRequestorCD CountVal 
		   FROM #FleetCalender
		   WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD=''))TEMP 
                           

IF @SuppressActivityDept=0
BEGIN
SELECT DISTINCT FleetID,
	  DepartmentCD,
	  DepartmentCD1,
	  DepartmentName,
	  PassengerRequestorCD,
	  PassengerName,
	  AccountNum,
	  Distance,
	  ChargeUnit,
	  SCHEDULEDMONTH,
	  ScheduledYear,
	  FlightBlockHoursLabel ,
	  FlightBlockHours = ISNULL(FlightBlockHours,0),
	  ChargeBack = ISNULL(ChargeBack,0),
	   IsInActive,
	   TenToMin,
	   MonthStart,
	   MonthEnd,
	   @Count NoOfRecords,
	   LogNum,
	   LegNum
 FROM #FleetCalender 
 WHERE (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
 
 ORDER BY DepartmentCD,SCHEDULEDMONTH
END
ELSE

BEGIN
SELECT DISTINCT FleetID,
	  DepartmentCD,
	  DepartmentCD1,
	  DepartmentName,
	  PassengerRequestorCD,
	  PassengerName,
	  AccountNum,
	  Distance,
	  ChargeUnit,
	  SCHEDULEDMONTH,
	  ScheduledYear,
	  FlightBlockHoursLabel ,
	  FlightBlockHours = ISNULL(FlightBlockHours,0),
	  ChargeBack = ISNULL(ChargeBack,0),
	   IsInActive,
	   TenToMin,
	   MonthStart,
	   MonthEnd,
	   @Count NoOfRecords,
	   LogNum,
	   LegNum
 FROM #FleetCalender 
 WHERE (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
 AND (DepartmentCD IN (SELECT DepartmentCD FROM #FleetCalender WHERE AccountNum IS NOT NULL OR Distance IS NOT NULL OR ChargeUnit IS NOT NULL OR FlightBlockHours IS NOT NULL OR ChargeBack IS NOT NULL))
 
 ORDER BY DepartmentCD,SCHEDULEDMONTH
END

IF OBJECT_ID('tempdb..#FleetCalender') IS NOT NULL
DROP TABLE #FleetCalender	

IF OBJECT_ID('tempdb..#DepartmentChargeBack') IS NOT NULL
DROP TABLE #DepartmentChargeBack 	

END



GO


