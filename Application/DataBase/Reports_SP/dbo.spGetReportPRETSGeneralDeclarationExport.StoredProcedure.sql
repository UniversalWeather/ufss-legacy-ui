IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationExport]
    @UserCD            AS VARCHAR(30)      
   ,@TripNUM           AS VARCHAR(300) = ''  -- Mandatory, PreflightMain.TripNUM  
   ,@LegNUM            AS VARCHAR(300) = ''  -- Mandatory, PreflightLeg.LegNUM
   ,@PassengerCD       AS VARCHAR(300) = ''  --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
   ,@BeginDate         AS DATETIME = NULL    --      PreflightLeg.DepartureDTTMLocal  
   ,@EndDate           AS DATETIME = NULL    --      PreflightLeg.ArrivalDTTMLocal   
   ,@TailNUM           AS VARCHAR(300) = ''  --      Fleet.TailNUM (Use FleetID of PreflightMain)  
   ,@DepartmentCD      AS VARCHAR(300) = ''  --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
   ,@AuthorizationCD   AS VARCHAR(300) = ''  --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
   ,@CatagoryCD        AS VARCHAR(300) = ''  --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
   ,@ClientCD          AS VARCHAR(300) = ''  --      Client.ClientCD  
   ,@HomeBaseCD        AS VARCHAR(300) = ''  --      UserMaster.HomeBase  
   ,@RequestorCD       AS VARCHAR(300) = ''  --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
   ,@CrewCD            AS VARCHAR(300) = ''  --      Crew.CrewCD     
   
   ,@IsTrip            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
   ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
   ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
   ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
   ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
   ,@IsScheduledService      AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  	
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSGeneralDeclarationSub4
-- Author: ABHISHEK.S
-- Create date: 20th September 2012
-- Description: Get TripSheetReportWriter General Declaration Export Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
BEGIN
	
	SET NOCOUNT ON;	
	
	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
		
	--	SELECT * FROM @tblTripInfo
	-- [End] Report Criteria --

	--   UNION
	DECLARE @GeneralTemp TABLE(ROW_ID INT IDENTITY,LegID BIGINT,SNONDutyTYPE VARCHAR(10),SNONDutyTYPE2 VARCHAR(10),FIRSTNM VARCHAR(30),LASTNM VARCHAR(30),MIDDLENM VARCHAR(20),PNO NVARCHAR(250),DOB VARCHAR(20),NATIONALITY CHAR(3),TYPE CHAR(1)) 
	INSERT INTO @GeneralTemp
	SELECT DISTINCT
	[LegID]=PL.LEGID,	
	[SNONDutyTYPE]     = (CASE WHEN PC.DutyTYPE = 'P' THEN 'CAPT:'
	                    WHEN PC.DutyTYPE = 'S' THEN 'F/O:'
	                    ELSE 'ADDL:'
	                    END) ,	
    [SNONDutyTYPE2]     = (CASE WHEN PC.DutyTYPE = 'P' THEN '1'
	                    WHEN PC.DutyTYPE = 'S' THEN '2'
	                    ELSE '3'
	                    END),
	[FIRSTNM]     = CR.FirstName ,
	[LASTNM]       = CR.LastName ,
	[MIDDLENM]    = CR.MiddleInitial,
    [PNO]         = ISNULL(CPP.PassportNum,'') ,
	--[PNO]         = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),	
	[DOB]         = dbo.GetDateFormatByUserCD(@UserCD,CR.BirthDT),   
	[NATIONALITY] =	 ISNULL(C.CountryCD,C1.CountryCD),
	[TYPE]        = 'C'
	

	
	FROM ( SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main 
				INNER JOIN PREFLIGHTMAIN PM ON Main.TripID = PM.TripID
				INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID
				LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
		        LEFT  JOIN (SELECT CrewID, CrewCD,FirstName,MiddleInitial,LastName,BirthDT,CountryID FROM Crew) CR ON PC.CrewID = CR.CrewID 
			    LEFT JOIN CrewPassengerPassport CPP ON  PC.CrewID=CPP.CrewID AND PC.PassportID = CPP.PassportID
			    LEFT JOIN Country C ON CPP.CountryID = C.CountryID
				LEFT JOIN Country C1 ON CR.CountryID = C1.CountryID
			   
			
				        
				        
	WHERE PM.IsDeleted = 0			  
	--  AND PL.LegID = CONVERT(BIGINT,@LegID)
      ORDER BY  [SNONDutyTYPE2]  





DECLARE @COUNT INT 
SELECT @COUNT=COUNT(*) FROM @GeneralTemp
WHILE @COUNT <=5
BEGIN
INSERT INTO @GeneralTemp(TYPE)VALUES('C');
SET @COUNT=@COUNT+1
END	
DECLARE @GeneralTemp1 TABLE(LegID BIGINT,SNONDutyTYPE VARCHAR(10),FIRSTNM VARCHAR(30),LASTNM VARCHAR(30),MIDDLENM VARCHAR(20),PNO NVARCHAR(250),DOB VARCHAR(20),NATIONALITY CHAR(3),TYPE CHAR(1))   



IF @COUNT >= 6
BEGIN
INSERT INTO @GeneralTemp1([LEGID],
	[SNONDutyTYPE],
	[FIRSTNM],
	[LASTNM],
	[MIDDLENM],	
	[PNO],
	[DOB], 
	[NATIONALITY],
	[TYPE])
SELECT 
	[LEGID],
	[SNONDutyTYPE],
	[FIRSTNM],
	[LASTNM],
	[MIDDLENM],	
	[PNO],
	[DOB], 
	[NATIONALITY],
	'P'  

	
	FROM @GeneralTemp A
    WHERE  A.ROW_ID >6	
    DELETE FROM @GeneralTemp WHERE ROW_ID >6	
  END
  INSERT INTO @GeneralTemp1
	SELECT DISTINCT A.LEGID,[SNONDutyTYPE] = CONVERT(VARCHAR,ROW_NUMBER()OVER(ORDER BY A.FIRSTNM))+'. ',A.FIRSTNM,A.LASTNM,A.MIDDLENM,A.PNO,A.DOB,A.NATIONALITY,A.TYPE
	 FROM 
	(SELECT DISTINCT
	[LEGID]=PL.LEGID,
	[FIRSTNM]    = PR.FirstName,
	[LASTNM]     = PR.LastName,
	[MIDDLENM]   = PR.MiddleInitial,	
	[PNO]         = ISNULL(CPP.PassportNum,'') ,
	--[PNO]         = dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),
	[DOB]         = dbo.GetDateFormatByUserCD(@UserCD,PR.DateOfBirth),	
	[NATIONALITY] = ISNULL(C.CountryCD,C1.CountryCD),
	[TYPE]        = 'P'

	FROM @tblTripInfo Main 
				INNER JOIN PREFLIGHTMAIN PM ON Main.TripID = PM.TripID
				INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID 
				LEFT JOIN PreflightPassengerList PLI ON Main.PassengerID = PLI.PassengerID and Main.LegID = PLI.LegID 
				LEFT JOIN Passenger PR  ON Main.PassengerID = PR.PassengerRequestorID 					
			    LEFT JOIN CrewPassengerPassport CPP ON PLI.PassportID = CPP.PassportID				  				        
				LEFT JOIN Country C ON CPP.CountryID = C.CountryID
				LEFT JOIN Country C1 ON C1.CountryID = PR.CountryID
				
	WHERE PM.IsDeleted = 0)A
      -- INNER JOIN PreflightPassengerList PLI ON PLI.LegID=A.LEGID 		  
	--  AND PL.LegID = CONVERT(BIGINT,@LegID)
	

 
DECLARE @COUNT1 INT 
SELECT @COUNT1=COUNT(*) FROM @GeneralTemp1
WHILE @COUNT1 <=14
BEGIN
INSERT INTO @GeneralTemp1(TYPE)VALUES('P');
SET @COUNT1=@COUNT1+1
END	



SELECT   SNONDutyTYPE,FIRSTNM,LASTNM,MIDDLENM,PNO,DOB,NATIONALITY,TYPE  FROM @GeneralTemp	
UNION ALL 
SELECT    SNONDutyTYPE,FIRSTNM,LASTNM,MIDDLENM,PNO,DOB,NATIONALITY,TYPE FROM @GeneralTemp1

END

-- EXEC spGetReportPRETSGeneralDeclarationExport 'supervisor_99','1992','1'

--SELECT * FROM PreflightCrewList
--WHERE LegID = 100013169

--UPDATE PreflightCrewList 
--SET CrewFirstName = 'Abhishek', CrewMiddleName = 'K' , CrewLastName = 'Srivastava'
--WHERE LegID = 100013169
GO


