IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarInformation]
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfWeeks INT=1
       ,@EndDate DATETIME--Mandatory
	
AS  

BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarInformation  
-- Author: 
-- Create date: 19 Aug 2015
-- Description: Get Preflight Schedule Calendar Business Week information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
         
 SET NOCOUNT ON  

SET @NoOfWeeks =2*@NoOfWeeks
 --DECLARE @BeginDate DATETIME=DATEADD(mm,(YEAR(@GoToDate) - 1900)* 12 + MONTH(@GoToDate) - 1,1 - 1);
 --DECLARE @EndDate   DATETIME=DATEADD(mm,(YEAR(DATEADD(M,@NoOfWeeks-1,@GoToDate)) - 1900)* 12 + MONTH(DATEADD(M,@NoOfWeeks-1,@GoToDate)) - 1,1 - 1) ;


  ;WITH CTE_DatesTable
  AS 
  (
    SELECT @BeginDate AS [date],1 WeekNo
    UNION ALL
    SELECT DATEADD(HH, 360, [date]),WeekNo+1 WeekNo
    FROM CTE_DatesTable
    WHERE DATEADD(HH, 360, [date]) <= @EndDate
  )
  SELECT date BeginDate
		,DATEADD(HH,336,DATE) EndDate
		,WeekNo
		,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
		
		 FROM CTE_DatesTable
		 WHERE WeekNo <= @NoOfWeeks
		 option (maxrecursion 0)
		 
		 
				
END; 



 --EXEC spGetReportPRESchedueCalendarInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''

















