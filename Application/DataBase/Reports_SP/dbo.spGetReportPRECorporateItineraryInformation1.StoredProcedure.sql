IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItineraryInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItineraryInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRECorporateItineraryInformation1]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TripNUM AS NVARCHAR(500) = '' -- [Optional INTEGER], Comma delimited string with mutiple values

AS
-- ==========================================================================================
-- SPC Name: spGetReportPRECorporateItineraryInformation
-- Author: ABHISHEK.S
-- Create date: 27th August 2012
-- Description: Get Corporate Itinerary Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
SET NOCOUNT ON


		--DECLARE @TenToMin INT
		--SET @TenToMin=(SELECT DISTINCT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
  --                  AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 
                    
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
		 	
		SET @SQLSCRIPT = '
			SELECT
			[TripID] = PM.TripID,
			[FileNo] = PM.TripNUM,
			[LegID] = PL.LegID,
			[TailNumber] = F.TailNUM,
			[Notes] = PL.Notes,
			[TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),
			[Description] = PM.TripDescription,
			[ETD1]=PL.DepartureDTTMLocal,
			[ETD2]=A.AirportName,
			[ETD3]=A.CityName,
			[ETA1]=PL.ArrivalDTTMLocal,
			[ETA2]=AA.AirportName,
			[ETA3]=AA.CityName,		
			[ETE1] = PL.ElapseTM,
			[ETE2] = PF.PreflightFBOName,
			[ETE3] = PF.PhoneNum1,
			[Passengers] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),
			(SELECT DISTINCT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) AS TenToMin

		FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
	    INNER JOIN Airport A ON PL.DepartICAOID = A.AirportID 
	    INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	    INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
				LEFT OUTER JOIN FleetGroupOrder FGO
				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG 
				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	 
		LEFT OUTER JOIN (
			SELECT DISTINCT PP2.LegID, Paxnames = ( 
				SELECT ISNULL(SUBSTRING(P.FirstName,1,1)+ ''.'','''') + ISNULL(SUBSTRING(P.MiddleInitial,1,1)+''.'','''') + ISNULL(P.LastName,'''') + '' ''
				FROM PreflightPassengerList PP1 INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID
				WHERE PP1.LegID = PP2.LegID
				ORDER BY P.LastName,P.FirstName
				FOR XML PATH('''')
			)
			FROM  PreflightPassengerList PP2
        ) PAX ON PL.LegID = PAX.LegID   		    
	    LEFT JOIN PreflightFBOList PF ON PL.LegID = PF.LegID AND PF.IsDepartureFBO = 1
		
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		  AND PM.ISDELETED = 0'

	--Construct OPTIONAL clauses
			
	IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';						
	END
	
	IF @TripNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
	END		
		SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY PM.TripNUM'
--print @SQLSCRIPT
	
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS NVARCHAR(500)'
    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	, @DATEFROM, @DATETO, @FleetGroupCD
   

    
    -- EXEC spGetReportPRECorporateItineraryInformation1 'JWILLIAMS_11','2012/09/01','2012/09/07', '', '', ''

GO


