IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRPassengerItinerarayExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRPassengerItinerarayExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCRPassengerItinerarayExportInformation]
(
	@UserCD VARCHAR(30) --Mandatory 
	,@UserCustomerID VARCHAR(30) --Mandatory
	,@DATEFROM DATE --Mandatory
	,@DATETO DATE   --Mandatory
	,@PaxID VARCHAR(100) --Mandatory
	,@PassengerGroupCD VARCHAR(500)='' --Optional
	,@PrintFlightCrew BIT = 0
	,@PrintLegNotes BIT = 0
	,@PrintPassengerManifest BIT = 0
	,@PrintTerminalInformation BIT = 0
	,@PrintOneLegPerPage BIT = 0
)
AS
BEGIN
-- =====================================================
-- SPC Name:spGetReportCRPassengerItinerarayExportInformation
-- Author: Aishwarya.M
-- Create date:2013 May 08
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ======================================================
DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PaxID <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @UserCustomerID
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaxID, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @UserCustomerID  
END
ELSE IF @PaxID = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @UserCustomerID  
	AND P.IsDeleted=0
END


DECLARE @PassFlag TABLE(ID INT IDENTITY,flag CHAR(1))

INSERT INTO @PassFlag VALUES ('A') 
INSERT INTO @PassFlag VALUES ('B') 
INSERT INTO @PassFlag VALUES ('C')
IF @PrintFlightCrew=1 BEGIN INSERT INTO @PassFlag SELECT 'G' UNION ALL SELECT 'D' END
IF @PrintTerminalInformation=1 BEGIN INSERT INTO @PassFlag SELECT 'E' UNION ALL SELECT 'F' END
IF @PrintPassengerManifest=1 BEGIN INSERT INTO @PassFlag SELECT 'G' UNION ALL SELECT 'H' UNION ALL SELECT 'G' UNION ALL SELECT 'I' END
IF @PrintLegNotes=1 BEGIN INSERT INTO @PassFlag SELECT 'G' UNION ALL SELECT 'J' UNION ALL SELECT 'K' END



SELECT  flag=PF.flag
			   ,orig_nmbr=CRTripNUM
			   ,trip_nmbr=ISNULL(PM.TripNUM,0)
			   ,leg_num=ISNULL(PL.LegNUM,CRL.LegNUM)
			   ,legid= ISNULL(PL.LegID,CRL.CRLegID)
			   ,paxcode=ISNULL(PP.PassengerRequestorCD,CP.PassengerRequestorCD)
			   ,prepared_for=ISNULL(PP.FirstName,ISNULL(CP.FirstName,'')) + ' ' + ISNULL(PP.LastName,ISNULL(CP.LastName,''))
		       ,lowdate=ISNULL(PLM.DepartureDTTMLocal,CLL.DepartureDTTMLocal)
		       ,highdate=ISNULL(PLM.ArrivalDTTMLocal,CLL.ArrivalDTTMLocal)
		       ,tripdate=ISNULL(PL.DepartureDTTMLocal,CRL.DepartureDTTMLocal)
			   ,locdep=ISNULL(PL.DepartureDTTMLocal,CRL.DepartureDTTMLocal)
			   ,locarr=ISNULL(PL.ArrivalDTTMLocal,CRL.ArrivalDTTMLocal)
			   ,tail_nmbr=ISNULL(F.TailNum,CF.TailNum)
			   ,type_code=ISNULL(A.AircraftCD,CA.AircraftCD)
			   ,type_desc=ISNULL(A.AircraftDescription,CA.AircraftDescription)
			   ,depicao_id=ISNULL(AD.IcaoID,CD.IcaoID)
			   ,arricao_id=ISNULL(AA.IcaoID,CAR.IcaoID)
			   ,depcity=ISNULL(AD.CityName,CD.CityName)
			   ,arrcity=ISNULL(AA.CityName,CAR.CityName)
			   ,from_city=CASE WHEN ISNULL(AD.CityName,'')<>'' THEN AD.CityName+', '+AD.StateName ELSE CD.CityName+', '+CD.StateName END
			   ,from_airport=ISNULL(AD.AirportName,CD.AirportName)
			   ,to_city=CASE WHEN ISNULL(AA.CityName,'')<>'' THEN AA.CityName+', '+AA.StateName ELSE CAR.CityName+', '+CAR.StateName END
			   ,to_airport=ISNULL(AA.AirportName,CAR.AirportName)
			   ,flightcrew = ''
			   ,dep_terminal=ISNULL(PFD.FBOVendor,CRFD.FBOVendor) + ' ' + ISNULL(PFD.PhoneNUM1,CRFD.PhoneNUM1)
			   ,arr_terminal=ISNULL(PFA.FBOVendor,CRFA.FBOVendor) + ' ' + ISNULL(PFA.PhoneNUM1, CRFA.PhoneNUM1)
			   ,blank_line=NULL
			   ,elp_time=ISNULL(PL.ElapseTM,CRL.ElapseTM)
			   ,numpax=ISNULL(PL.PassengerTotal,CRL.PassengerTotal)
			   ,pax1 = ''
			   ,pax2 = ''
			   ,leg_notes = ISNULL(PL.Notes,CRL.Notes)
		FROM CRMain CRM
		INNER JOIN CRLeg CRL ON CRM.CRMainID = CRL.CRMainID AND CRL.IsDeleted = 0
		INNER JOIN (SELECT  MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,CRMainID FROM CRLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CRMainID) CLL ON CLL.CRMainID = CRL.CRMainID
		INNER JOIN Fleet CF ON CRM.FleetID=CF.FleetID
		INNER JOIN CRPassenger CRP ON CRL.CRLegID = CRP.CRLegID
		INNER JOIN Passenger CP ON CRP.PassengerRequestorID = CP.PassengerRequestorID
		LEFT OUTER JOIN Aircraft CA ON CA.AircraftID=CF.AircraftID
		LEFT OUTER JOIN Airport CD ON CRL.DAirportID = CD.AirportID
		LEFT OUTER JOIN Airport CAR ON CRL.AAirportID = CAR.AirportID
				--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'A'
				) CRFA ON CRFA.CRLegID = CRL.CRLegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'D'
				) CRFD ON CRFD.CRLegID = CRL.CRLegID
		
		
		
		LEFT OUTER JOIN PreflightMain PM ON PM.TripID=CRM.TripID AND PM.IsDeleted=0
		LEFT OUTER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=0	
		LEFT OUTER JOIN (SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,TripID FROM PreflightLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY TripID)PLM ON PM.TripID=PLM.TripID 
		LEFT OUTER JOIN Fleet F ON F.FleetID=PM.FleetID	
	    LEFT OUTER JOIN PreflightPassengerList PPL  ON PPL.LegID = PL.LegID
		LEFT OUTER JOIN Passenger PP ON CRP.PassengerRequestorID = CP.PassengerRequestorID
		LEFT OUTER JOIN Aircraft A ON A.AircraftID=F.AircraftID
		LEFT OUTER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
		LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID 
				--PREFLIGHT	TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1,PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsArrivalFBO = 1 
				) PFA ON PFA.LegID = PL.LegID 
		--PREFLIGHT FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsDepartureFBO = 1
				) PFD ON PFD.LegID = PL.LegID
		
		INNER JOIN @TempPassengerID TP ON TP.PassengerID = ISNULL(PP.PassengerRequestorID, CP.PassengerRequestorID)
		CROSS JOIN @PassFlag P
		INNER JOIN @PassFlag PF ON PF.ID=P.ID
		
		
		WHERE CRM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
			AND ISNULL(PM.EstDepartureDT,CRM.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
			AND CRM.IsDeleted = 0
			ORDER BY locdep,Paxcode,leg_num,PF.ID

END		
	--EXEC spGetReportCRPassengerItinerarayExportInformation 'SUPERVISOR_99','10099','2010-05-01','2013-05-01',''
	


GO


