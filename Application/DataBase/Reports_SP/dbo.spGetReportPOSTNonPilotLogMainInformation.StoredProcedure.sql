IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotLogMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotLogMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE Procedure [dbo].[spGetReportPOSTNonPilotLogMainInformation]  
  @UserCD AS VARCHAR(30), --Mandatory  
  @DATEFROM AS DATETIME, --Mandatory  
  @DATETO AS DATETIME, --Mandatory  
  @CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @CrewGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
  @ChecklistCode AS NVARCHAR(1000) = '',  
  @AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
  @PrintOnTime AS bit=0,   
  @PrintOffTime AS bit=0    
AS  


--SELECT * FROM PostflightMain WHERE LogNum=81 AND CustomerID=10098
--SELECT * FROM  PostflightLeg WHERE POLogID=1009880
--===============================================================================
-- SPC Name: spGetReportPOSTSubNonPilotLogInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight NonPilot Log for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON

DECLARE @Custom SMALLINT = 0;
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

SELECT @Custom = TimeDisplayTenMin
FROM Company
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID ---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID 
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
	  AND C.IsDeleted=0
	  AND C.IsStatus=1
END
-----------------------------Crew and Crew Group Filteration----------------------

--DROP TABLE #DUTYTOTAL
CREATE TABLE #DUTYTOTAL (
	POLogID BIGINT
	,LOGNUM INT
	,LegID BIGINT
	,IsStatus NVARCHAR(1)
	,DutyBegin VARCHAR(20)
	,DutyEnd VARCHAR(20)
	,CrewID BIGINT
	)

DECLARE @DUTY TABLE (
	DutyBegin VARCHAR(20)
	,DutyEnd VARCHAR(20)
	,POLogID BIGINT
	,LegID BIGINT
	,CrewID BIGINT
	) 

INSERT INTO #DUTYTOTAL
SELECT PM.POLogID
	,PM.LogNum
	,PL.POLegID
	,PL.IsDutyEnd
	,PC.BeginningDuty
	,PC.DutyEnd
	,PC.CrewID
FROM PostflightMain PM
INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
INNER JOIN PostflightCrew PC ON PL.POLegID = PC.POLegID
WHERE (PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
	AND PM.LogNum IN (
		SELECT DISTINCT LogNum
		FROM PostflightMain PM
		INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
		WHERE CONVERT(DATE, PL.OutboundDTTM) BETWEEN CONVERT(DATE, @DATEFROM)
				AND CONVERT(DATE, @DATETO)
			AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND PM.IsDeleted = 0
		)
	AND PM.IsDeleted = 0

INSERT INTO @DUTY
SELECT DISTINCT CUR.DutyBegin
	,NXT.DutyEnd
	,CUR.POLogID
	,NXT.LegID
	,CUR.CrewID
FROM (
	SELECT RANK() OVER (
			PARTITION BY LOGNUM
			,CREWID ORDER BY LEGID
			) RNK
		,LOGNUM
		,DutyBegin
		,DutyEnd
		,POLogID
		,LEGID
		,CrewID
	FROM #DUTYTOTAL
	WHERE IsStatus = 0
		AND DutyEnd IS NULL
	) CUR
JOIN (
	SELECT RANK() OVER (
			PARTITION BY LOGNUM
			,CREWID ORDER BY LEGID
			) RNK
		,LOGNUM
		,DutyBegin
		,DutyEnd
		,POLogID
		,LEGID
		,CrewID
	FROM #DUTYTOTAL
	WHERE IsStatus = 1
		AND DutyBegin IS NULL
	) NXT ON CUR.LOGNUM = NXT.LOGNUM
	AND CUR.RNK = NXT.RNK
	AND CUR.CrewID = NXT.CrewID

UPDATE #DUTYTOTAL
SET DutyBegin = TEST1.DutyBegin
FROM (
	SELECT DISTINCT DutyBegin
		,CrewID
		,LegID
	FROM @DUTY
	) TEST1
WHERE #DUTYTOTAL.LegID = TEST1.LegID
	AND #DUTYTOTAL.DutyEnd IS NOT NULL

UPDATE #DUTYTOTAL
SET DutyBegin = NULL
FROM (
	SELECT DISTINCT DutyBegin
		,CrewID
		,LegID
	FROM @DUTY
	) TEST1
WHERE #DUTYTOTAL.LegID <> TEST1.LegID
	AND #DUTYTOTAL.DutyEnd IS NULL

DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin
FROM Company
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @Spec3 INT;  
DECLARE @SpecLong3 VARCHAR(25);  
DECLARE @Spec4 INT;  
DECLARE @SpecLong4 VARCHAR(25); 

SELECT @Spec3 = Specdec3
	,@SpecLong3 = SpecificationLong3
	,@Spec4 = Specdec4
	,@SpecLong4 = SpecificationLong4
FROM Company
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

CREATE TABLE #temp (
[DateRange] VARCHAR(100),
	DateFROM VARCHAR(10)
	,DATETO VARCHAR(10)
	,shdate DATE
	,TailNumber VARCHAR(9)
	,aType VARCHAR(MAX)
	,FltTyp CHAR(3)
	,DepICAO CHAR(4)
	,ArrICAO CHAR(4)
	,BlkOut VARCHAR(5)
	,BlkIn VARCHAR(5)
	,DutyHours NUMERIC(7, 3)
	,bduty NUMERIC(7, 3)
	,BlockTime NUMERIC(7, 3)
	,FlightTime NUMERIC(7, 3)
	,specification3 VARCHAR(25)
	,specification4 VARCHAR(25)
	,CUSTOM3DESCRIPTION VARCHAR(20)
	,CUSTOM4DESCRIPTION VARCHAR(20)
	,AssociatedCrew VARCHAR(max)
	,TenToMin INT
	,crewid VARCHAR(200)
	,Crew VARCHAR(max)
	,maincrew VARCHAR(5)
	,chk VARCHAR(15)
	,BLK VARCHAR(15)
    ,[CHECKLISTCODE] CHAR(3)
   ,[ChecklistItem] VARCHAR(25)
   ,[ChecklistLast] VARCHAR(30)
   ,[ChecklistDue] VARCHAR(30)
   ,[ChecklistAlert] VARCHAR(30)
   ,[ChecklistGrace] VARCHAR(30)
   ,[case1] VARCHAR(15)
   ,ChkVal CHAR(1)
   ,DutyEnd BIT
   ,Rnk INT
   
	
	)




INSERT INTO #temp
SELECT NPL.*,ROW_NUMBER()OVER(PARTITION BY CONVERT(DATE,shDate) ORDER BY shDate DESC) rnk FROM
(
SELECT DISTINCT 
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
[DateFROM] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)
	,[DATETO] =  dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
	,[shDate] = NULL
	,[TailNumber] = ''
	,[aType] = 'NO ACTIVIT'
	,[FltTyp] = ''
	,[DepICAO] = ''
	,[ArrICAO] = ''
	,[BlkOut] = NULL
	,[BlkIn] = NULL
	,[DutyHours] = NULL
	,[bduty] = NULL
	,[BlockTime] = NULL
	,[FlightTime] = NULL
	,[specification3] = NULL
	,[specification4] = NULL
	,[CUSTOM3DESCRIPTION] = (
		CASE 
			WHEN CM.Specdec3 = 0
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 0)))
			WHEN CM.Specdec3 = 1
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 1)))
			WHEN CM.Specdec3 = 2
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 2)))
			WHEN CM.Specdec3 = 3
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 3)))
			WHEN CM.Specdec3 = 4
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 4)))
			WHEN CM.Specdec3 = 5
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 5)))
			END
		)
	,[CUSTOM4DESCRIPTION] = (
		CASE 
			WHEN CM.Specdec4 = 0
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 0)))
			WHEN CM.Specdec4 = 1
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 1)))
			WHEN CM.Specdec4 = 2
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 2)))
			WHEN CM.Specdec4 = 3
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 3)))
			WHEN CM.Specdec4 = 4
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 4)))
			WHEN CM.Specdec4 = 5
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 5)))
			END
		)
	,[AssociatedCrew] = ''
	,[TenToMin] = @TenToMin
	,[crewid] = c.crewid
	,[Crew] = c.crewcd + ' ' + C.LastName + ',' + C.FirstName + ' ' + ISNULL(c.MiddleInitial, '')
	,
	--[DUTY] =PC.Dutytype, 
	[maincrew] = c.crewcd
	,[chk] = (
		CASE 
			WHEN (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 1)
					)
				THEN 'On/Off'
			WHEN (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 0)
					)
				THEN ''
			WHEN (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 0)
					)
				THEN 'On'
			WHEN (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 1)
					)
				THEN 'Off'
			END
		)
	,[BLK] = NULL
	,[CHECKLISTCODE]=NULL
	,[ChecklistItem]=NULL
	,[ChecklistLast]=NULL
	,[ChecklistDue] =NULL
	,[ChecklistAlert] =NULL
	,[ChecklistGrace]=NULL
	,[case1]=NULL
	,'H' ChkVal
	,PL.IsDutyEnd
FROM PostflightLeg PL
INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0
INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
INNER JOIN Crew C ON C.CrewID = PC.CrewID
INNER JOIN Company CM ON CM.CustomerID = PM.CustomerID
INNER JOIN UserMaster UM ON CM.HomebaseID=UM.HomebaseID AND UM.UserName=LTRIM(RTRIM(@UserCD))
INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND CONVERT(DATE, PL.ScheduledTM) BETWEEN CONVERT(DATE, @DATEFROM)
		AND CONVERT(DATE, @DATETO)
	AND PC.Dutytype IN (
		'P'
		,'S'
		)
	AND PC.DutyTYPE IS NULL
	AND c.IsDeleted = 0
	AND c.IsStatus = 1
	AND PL.IsDeleted = 0

UNION

SELECT DISTINCT 
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
[DateFROM] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)
	,[DATETO] = dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
	,[shDate] = PL.ScheduledTM
	,[TailNumber] = F.TailNum
	,[aType] = AC.AircraftCD
	,[FltTyp] = PL.FedAviationRegNum
	,[DepICAO] = AD.ICAOID
	,[ArrICAO] = AA.ICAOID
	,[BlkOut] = PL.BlockOut
	,[BlkIn] = PL.BlockIN
	,[DutyHours] = ROUND(PC.DutyHours,1)
	,[bduty] = (
		CASE 
			WHEN (PC.DUTYEND IS NOT NULL)
				THEN ROUND(PC.DUTYHOURS,1)
			END
		)
	,[BlockTime] = ROUND(PC.BlockHours,1)
	,[FlightTime] = ROUND(PC.FlightHours,1)
	,[specification3] = CM.SpecificationLong3
	,[specification4] = CM.SpecificationLong4
	,[CUSTOM3DESCRIPTION] = (
		CASE 
			WHEN CM.Specdec3 = 0
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 0)))
			WHEN CM.Specdec3 = 1
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 1)))
			WHEN CM.Specdec3 = 2
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 2)))
			WHEN CM.Specdec3 = 3
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 3)))
			WHEN CM.Specdec3 = 4
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 4)))
			WHEN CM.Specdec3 = 5
				AND CM.SpecificationLong3 IS NOT NULL
				AND CM.SpecificationLong3 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification3 AS NUMERIC(7, 5)))
			END
		)
	,[CUSTOM4DESCRIPTION] = (
		CASE 
			WHEN CM.Specdec4 = 0
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 0)))
			WHEN CM.Specdec4 = 1
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 1)))
			WHEN CM.Specdec4 = 2
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 2)))
			WHEN CM.Specdec4 = 3
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 3)))
			WHEN CM.Specdec4 = 4
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 4)))
			WHEN CM.Specdec4 = 5
				AND CM.SpecificationLong4 IS NOT NULL
				AND CM.SpecificationLong4 <> ''
				THEN convert(VARCHAR(20), cast(PC.Specification4 AS NUMERIC(7, 5)))
			END
		)
	,[AssociatedCrew] = ACrew.AssociatedCrew
	,[TenToMin] = @TenToMin
	,[crewid] = c.crewid
	,[Crew] = c.crewcd + ' ' + C.LastName + ',' + C.FirstName + ' ' + ISNULL(c.MiddleInitial, '')
	,
	--[DUTY] =PC.Dutytype,  
	[maincrew] = c.crewcd
	,[chk] = (
		CASE 
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 1)
					)
				THEN 'On/Off'
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 0)
					)
				THEN ''
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 0)
					)
				THEN 'On'
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 1)
					)
				THEN 'Off'
			END
		)
	,[BLK] = (
		CASE 
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 0)
					)
				THEN DT.DutyBegin
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 1)
					)
				THEN DT.DutyEnd
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 1)
					)
				THEN DT.DutyBegin + '/' + DT.DutyEnd
			WHEN (PL.IsDutyEnd = 1)
				AND (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 0)
					)
				THEN ''
			END
		)
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,'H'
	,PL.IsDutyEnd
FROM PostflightLeg PL
INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0
INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
INNER JOIN Crew C ON C.CrewID = PC.CrewID
INNER JOIN (
	SELECT DISTINCT PC1.POLegID
		,AssociatedCrew = (
			SELECT RTRIM(C.CrewCD) + ','
			FROM PostflightCrew PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.POLegID = PC1.POLegID
			FOR XML PATH('')
			)
	FROM PostflightCrew PC1
	) ACrew ON PL.POLegID = ACrew.POLegID
INNER JOIN Fleet F ON F.FleetID = PM.FleetID
INNER JOIN Company CM ON CM.CustomerID = PM.CustomerID
INNER JOIN UserMaster UM ON CM.HomebaseID=UM.HomebaseID AND UM.UserName=LTRIM(RTRIM(@UserCD))
INNER JOIN Aircraft AC ON AC.AircraftID = PM.AircraftID
LEFT OUTER JOIN CrewCheckListDetail CCD ON CCD.CrewID = PC.CrewID
LEFT OUTER JOIN CrewCheckList ccl ON ccd.CheckListCD = ccl.CrewCheckCD
INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
LEFT OUTER JOIN (
	SELECT DISTINCT *
	FROM #DUTYTOTAL
	) DT ON DT.POLogID = PM.POLogID
	AND DT.LegID = PL.POLegID
WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND CONVERT(DATE, PL.ScheduledTM) BETWEEN CONVERT(DATE, @DATEFROM)
		AND CONVERT(DATE, @DATETO)
	AND PC.Dutytype NOT IN (
		'P'
		,'S'
		)
	AND c.IsDeleted = 0
	AND c.IsStatus = 1
	AND (
		AC.AircraftCD IN (
			SELECT DISTINCT RTRIM(S)
			FROM dbo.SplitString(@AircraftCD, ',')
			)
		OR @AircraftCD = ''
		)
	AND PL.IsDeleted = 0
)NPL
ORDER BY maincrew



/*


UPDATE #TEMP SET [CHECKLISTCODE]=CHK.[CHECKLISTCODE],[ChecklistItem]=CHK.ChecklistItem,[ChecklistLast]=CHK.ChecklistLast,
				 [ChecklistDue]=CHK.ChecklistDue,[ChecklistAlert]=CHK.ChecklistAlert,[ChecklistGrace]=CHK.ChecklistGrace,[case1]=CHK.case1
				FROM  (
				   SELECT DISTINCT [CHECKLISTCODE] = CCL.CrewCheckCD, 
									[ChecklistItem]=ccl.CrewChecklistDescription,
									[ChecklistLast]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
									[ChecklistDue]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
									[ChecklistAlert]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
									[ChecklistGrace]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
									[crewid]=c.crewid,[CREW]=C.CREWCD,
									[case1]=  (CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
												   WHEN (@DATETO > CCD.DUEDT) THEN 'PASSED DUE' END)
			  							FROM PostflightLeg PL 
											INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID
											INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
											INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID
											INNER JOIN Crew C ON C.CrewID = PC.CrewID
											INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and CCD.CustomerID=ccl.CustomerID
											LEFT OUTER JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  
											INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
								       WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
										 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
										 --AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
										 AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
										 AND PC.Dutytype NOT IN('P','S') 
										 AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '') 
				 )CHK
		 WHERE CHK.crewid=#temp.crewid

*/

IF @IsZeroSuppressActivityCrewRpt = 0
BEGIN
INSERT INTO #temp
SELECT DISTINCT 
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO), 
[DateFROM] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM)
	,[DATETO] = dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
	,
	--[DateRange]=CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101),   
	[shDate] = NULL
	,[TailNumber] = ''
	,[aType] = 'NO ACTIVIT'
	,[FltTyp] = ''
	,[DepICAO] = ''
	,[ArrICAO] = ''
	,[BlkOut] = NULL
	,[BlkIn] = NULL
	,[DutyHours] = NULL
	,[bduty] = NULL
	,[BlockTime] = NULL
	,[FlightTime] = NULL
	,[specification3] = NULL
	,[specification4] = NULL
	,[[CUSTOM3DESCRIPTION] = (
		CASE 
			WHEN @Spec3 = 0
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0')
			WHEN @Spec3 = 1
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0.0')
			WHEN @Spec3 = 2
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0.00')
			WHEN @Spec3 = 3
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0.000')
			WHEN @Spec3 = 4
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0.0000')
			WHEN @Spec3 = 5
				AND @SpecLong3 IS NOT NULL
				AND @SpecLong3 <> ''
				THEN CONVERT(VARCHAR(20), '0.00000')
			END
		)
	,[CUSTOM4DESCRIPTION] = (
		CASE 
			WHEN @Spec4 = 0
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0')
			WHEN @Spec4 = 1
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0.0')
			WHEN @Spec4 = 2
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0.00')
			WHEN @Spec4 = 3
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0.000')
			WHEN @Spec4 = 4
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0.0000')
			WHEN @Spec4 = 5
				AND @SpecLong4 IS NOT NULL
				AND @SpecLong4 <> ''
				THEN CONVERT(VARCHAR(20), '0.00000')
			END
		)
	,[AssociatedCrew] = ''
	,[TenToMin] = @TenToMin
	,[crewid] = c.crewid
	,[Crew] = c.crewcd + ' ' + C.LastName + ',' + C.FirstName + ' ' + ISNULL(c.MiddleInitial, '')
	,
	--[DUTY]=PC.Dutytype,
	[maincrew] = c.crewcd
	,[chk] = (
		CASE 
			WHEN (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 1)
					)
				THEN 'On/Off'
			WHEN (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 0)
					)
				THEN ''
			WHEN (
					(@PrintOnTime = 1)
					AND (@PrintOffTime = 0)
					)
				THEN 'On'
			WHEN (
					(@PrintOnTime = 0)
					AND (@PrintOffTime = 1)
					)
				THEN 'Off'
			END
		)
	,[BLK] = NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,'H'
	,0
	,0
FROM Crew C
--LEFT OUTER JOIN  PostflightCrew PC ON PC.CrewID = C.CrewID 
INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
WHERE C.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND c.IsDeleted = 0
	AND c.IsStatus = 1
	AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(crewid,0) FROM #temp)
ORDER BY maincrew
END

INSERT INTO #temp(DateRange,DateFROM,DATETO,CREW,maincrew,CHECKLISTCODE,ChecklistItem,ChecklistLast,ChecklistDue,ChecklistAlert,ChecklistGrace,crewid,case1,ChkVal,DutyEnd,TenToMin)
SELECT DISTINCT T.DateRange,T.DateFROM,T.DATETO,T.Crew,T.maincrew,[CHECKLISTCODE] = CCL.CrewCheckCD, 
									[ChecklistItem]=ccl.CrewChecklistDescription,
									[ChecklistLast]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
									[ChecklistDue]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
									[ChecklistAlert]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
									[ChecklistGrace]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
									[crewid]=c.crewid,
									[case1]=  (CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
												   WHEN (@DATETO > CCD.DUEDT) THEN 'PASSED DUE' END),
												   'Z',
												   0,@TenToMin
			  							FROM PostflightLeg PL 
											INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0
											INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
											INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID
											INNER JOIN Crew C ON C.CrewID = PC.CrewID
											INNER JOIN #temp T ON T.CREWID=C.CrewID
											INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and CCD.CustomerID=ccl.CustomerID
											LEFT OUTER JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  
											INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID
								       WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
										 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
										 --AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
										 AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
										 AND PC.Dutytype NOT IN('P','S') 
										 AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '') 
										 AND PL.IsDeleted = 0



SELECT 
	[DateRange] 
	,DateFROM 
	,DATETO 
	,shdate 
	,TailNumber
	,aType
	,FltTyp
	,DepICAO 
	,ArrICAO 
	,BlkOut
	,BlkIn 
	,ISNULL(DutyHours ,0) DutyHours
	,bduty 
	,ISNULL(BlockTime ,0) BlockTime
	,ISNULL(FlightTime,0) FlightTime
	,specification3 
	,specification4 
	,ISNULL(CUSTOM3DESCRIPTION ,0) CUSTOM3DESCRIPTION
	,ISNULL(CUSTOM4DESCRIPTION ,0) CUSTOM4DESCRIPTION
	,AssociatedCrew 
	,TenToMin 
	,crewid 
	,Crew 
	,maincrew 
	,chk 
	,BLK 
    ,[CHECKLISTCODE] 
   ,[ChecklistItem] 
   ,[ChecklistLast] 
   ,[ChecklistDue] 
   ,[ChecklistAlert] 
   ,[ChecklistGrace] 
   ,[case1] 
   ,ChkVal 
   ,DutyEnd 
   ,Rnk 
   ,CASE WHEN DutyEnd=1  THEN  DutyHours ELSE 0 END DutyVal FROM #TEMP

ORDER BY MAINCREW,shdate,Rnk DESC




IF OBJECT_ID('tempdb..#DUTYTOTAL') IS NOT NULL
	DROP TABLE #DUTYTOTAL

IF OBJECT_ID('tempdb..#temp') IS NOT NULL
	DROP TABLE #temp


IF OBJECT_ID('tempdb..#TempCrewID') IS NOT NULL
	DROP TABLE #TempCrewID
--  EXEC spGetReportPOSTNonPilotLogMainInformation 'JWILLIAMS_13','2011/1/1','2011/12/31','','','','',0,0





GO


