IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItineraryMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItineraryMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRECorporateItineraryMainInformation]  
    
  @UserCD AS VARCHAR(30), --Mandatory  
  @DATEFROM AS DATETIME, --Mandatory  
  @DATETO AS DATETIME, --Mandatory  
  @TailNum AS VARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @FleetGroupCD AS VARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @TripNum AS VARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values  
  @PassengerCD VARCHAR(500)=''   
  
AS  
-- ==========================================================================================  
-- SPC Name: spGetReportPRECorporateItineraryInformation  
-- Author: ABHISHEK.S  
-- Create date: 27th August 2012  
-- Description: Get Corporate Itinerary Information For Reports  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================================  
SET NOCOUNT ON  
    
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
  DECLARE @ParameterDefinition AS NVARCHAR(100)  
  
  
   SELECT distinct  
   [TripID] = PM.TripID,  
   [FileNo] = PM.TripNUM,  
   --[LegID] = PL.LegID,   
   [TailNumber] = F.TailNUM,  
   [TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),  
   [DateRange] = [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATEFROM) + '-' + [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATETO),  
   [Description] = PM.TripDescription  
   FROM PreflightMain PM   
     INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
     INNER JOIN (  
      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD  
      FROM Fleet F   
      LEFT OUTER JOIN FleetGroupOrder FGO  
      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
      LEFT OUTER JOIN FleetGroup FG   
      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
       WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
         OR @FleetGroupCD = '' )   
    ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
          INNER JOIN  PreflightPassengerList PP ON  PP.LegID=PL.LegID  
          LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID    
    --LEFT OUTER JOIN PassengerGroupOrder PGO ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID  
    --LEFT OUTER JOIN PassengerGroup PG ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID  
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))   
     AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD='')   
      AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum='')  
       AND (PM.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum='')  
     AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
     AND PM.IsDeleted = 0
   
  
  
----PRINT @SQLSCRIPT   
-- SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS NVARCHAR(500),@PassengerCD AS VARCHAR(500)'  
--    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD , @DATEFROM, @DATETO, @FleetGroupCD ,@PassengerCD  
     
  
      
    -- EXEC spGetReportPRECorporateItineraryMainInformation 'supervisor_99','09/01/2012','09/07/2012', '', '', '',''  
  --  SELECT distinct  
  -- [TripID] = PM.TripID,  
  -- [FileNo] = PM.TripNUM,  
  -- --[LegID] = PL.LegID,   
  -- [TailNumber] = F.TailNUM,  
  -- [TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),  
  -- [DateRange] = [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATEFROM) + '-' + [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATETO),  
  -- [Description] = PM.TripDescription  
  -- FROM PreflightMain PM   
  -- INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
  -- INNER JOIN (  
  --  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD  
  --  FROM Fleet F   
  --  LEFT OUTER JOIN FleetGroupOrder FGO  
  --  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
  --  LEFT OUTER JOIN FleetGroup FG   
  --  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
  --   WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
  --     OR @FleetGroupCD = '' )   
  --) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
  -- WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))   
  --    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
  

GO


