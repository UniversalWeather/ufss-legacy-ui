IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








										
CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation]											
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @YEAR AS NVARCHAR(4),
    @TailNum AS VARCHAR(100) = '',
    @UserHomebaseID AS VARCHAR(30) = '',
    @DepartmentCD AS VARCHAR(70)= '',
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @IsHomebase AS BIT = 0
    --@TenToMin SMALLINT = 0												
 --EXEC spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation 'supervisor_99','10099','2009','','','','',0											

AS 
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation
-- Author:  A.Akhila
-- Create date: 31 Jul 2012
-- Description: Get FlightHours/BlockHours details based on Department and Tail Number
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================


SET NOCOUNT ON
BEGIN
Declare @SuppressActivityDept BIT  
	
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
DECLARE @TenToMin SMALLINT = 0;
DECLARE @AircraftBasis SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin,@AircraftBasis = AircraftBasis FROM Company 
																	WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
																	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
				
DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID

DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 


-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempFleetID TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT,
	TailNum VARCHAR(10)
	)

IF @TailNUM <> ''
	BEGIN
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum 
		FROM Fleet F
		WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
	END
IF @FleetGroupCD <> ''
	BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum
		FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
		 AND F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		 AND F.IsDeleted=0
			
	END
ELSE IF @TailNUM = '' AND @FleetGroupCD = ''
	BEGIN 
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum
	FROM Fleet F 
	WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	  AND F.IsDeleted=0
	  AND F.IsInActive=0
	END

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TEMPTODAY DATETIME=@FromDate,
        @DATETO DATETIME= @ToDate;
DECLARE @MonthNo TAble(MonthNo INT,Year INT);


WITH dates(DATEPARAM)
           AS (SELECT @TEMPTODAY AS DATETIME
               UNION ALL
               SELECT Dateadd(DAY, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO @MonthNo  (MonthNo,Year)
      SELECT DISTINCT MONTH(DATEPARAM),YEAR(DATEPARAM)
      FROM   Dates
      OPTION (maxrecursion 10000)
      
    

 

DECLARE @DeptRequestor TABLE(DATE INT
                            ,SCHYEAR INT
                            ,TailNum VARCHAR(9)
                            ,FleetID BIGINT
                            ,DepartmentID BIGINT
                            ,dept_code VARCHAR(8)
                            ,deptdesc VARCHAR(25)
                            ,ac_code VARCHAR(4)
                            ,acctnum VARChAR(60)
                            ,totcol NUMERIC(7,3)
                            ,flthrs NUMERIC(10,3)
                            ,chrgrate NUMERIC(17,2)
                            ,chrgunit CHAR(1)
                            ,nmiles NUMERIC(5,0)
                            ,fltcost NUMERIC(17,0)
                            ,LogNum BIGINT
                            ,Category CHAR(1)
                            ,LegNum BIGINT
                            ,Rnk INT
                            )


/*
  ,cmon=LEFT(DATENAME(M,POL.ScheduledTM),3)
  ,lmon1='TRUE'
  ,activecol=
*/



INSERT INTO @DeptRequestor
SELECT Dept.* 
     ,ROW_NUMBER()OVER(PARTITION BY DepartmentID,FleetID,Date ORDER BY FleetID,DepartmentID,Date,flthrs DESC) Rnk
       FROM (
SELECT DISTINCT 
	[DATE] = MONTH(POL.ScheduledTM)
	,SCHYEAR = YEAR(POL.ScheduledTM)
	,F.TailNum
	,F.FleetID
	,DepartmentID=ISNULL(D.DepartmentID,999)
    ,dept_code=ISNULL(D.DepartmentCD,'ZZZ@#')
    ,deptdesc=ISNULL(D.DepartmentName,'UNALLOCATED DEPT')
    ,ac_code=F.AircraftCD
    ,acctnum=NULL
    ,totcol=NULL
    ,flthrs= CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END
    ,chrgrate=FCR.ChargeRate
    ,chrgunit=FCR.ChargeUnit
    ,nmiles=POL.Distance
    ,fltcost=CONVERT(NUMERIC(17,0),(CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(	
							(CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END,0)) 
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END)
						)ELSE
						NULL
						END
				))
  ,POM.LogNum
  ,CASE WHEN POL.DepartmentID IS NOT NULL THEN 1 ELSE 2 END   Category
  ,POL.LegNUM
FROM PostflightMain POM
	INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted = 0
	INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.IsDeleted=0 AND F.IsInActive=0
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
	LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POL.PassengerRequestorID
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
	LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate,BeginRateDT,EndRateDT
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) FCR ON POL.POLegID = FCR.POLegID  
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND CONVERT(DATE, POL.ScheduledTM) BETWEEN   CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)
  AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
  AND POM.IsDeleted=0

 )Dept



IF @SuppressActivityDept=0
BEGIN


INSERT INTO @DeptRequestor
SELECT DATE=D.MonthNo
      ,SCHYEAR=D.Year
      ,TailNum=NULL
      ,FleetID=99
      ,DepartmentID
      ,DepartmentCD 
      ,DepartmentName 
	  ,ac_code=NULL
	  ,acctnum=NULL 
	  ,totcol=NULL 
	  ,flthrs=0 
      ,chrgrate=0 
	  ,chrgunit=NULL
	  ,nmiles=0
	  ,fltcost=0 
	  ,LogNum=0 
	  ,Category=1
	  ,LegNum=NULL
	  ,Rnk=2 FROM Department CROSS JOIN (SELECT MONTH(@FromDate)MonthNo,YEAR(@FromDate) YEAR)D
	                 WHERE CustomerID=@UserCustomerID
	                 AND DepartmentID NOT IN (SELECT ISNULL(DepartmentID,0) FROM @DeptRequestor)
	                 

END

	              


ELSE

BEGIN


INSERT INTO @DeptRequestor
SELECT DATE=D.MonthNo
      ,SCHYEAR=D.Year
      ,TailNum=NULL
      ,FleetID=99
      ,DepartmentID
      ,dept_code 
      ,deptdesc 
	  ,ac_code=NULL
	  ,acctnum=NULL 
	  ,totcol=NULL 
	  ,flthrs=0 
      ,chrgrate=0 
	  ,chrgunit=NULL
	  ,nmiles=0
	  ,fltcost=0 
	  ,LogNum=0 
	  ,Category
	  ,LegNum=NULL
	  ,Rnk=2 FROM @DeptRequestor CROSS JOIN (SELECT MONTH(@FromDate)MonthNo,YEAR(@FromDate) YEAR)D
	                WHERE MonthNo NOT IN (SELECT DISTINCT MonthNo FROM @DeptRequestor)

END


DECLARE @Month TAble(ac_code VARCHAR(4),TailNum VARCHAR(9),FleetID BIGINT,DepartmentID  BIGINT,MonthNo INT,Year INT,dept_code VARCHAR(8),deptdesc VARCHAR(25),Category CHAR(1));
 
INSERT INTO @Month 
SELECT DISTINCT ac_code,TailNum,FleetID,DepartmentID,MonthNo,YEAR,dept_code,deptdesc,Category FROM @DeptRequestor CROSS JOIN @MonthNo

SELECT DISTINCT DATE
      ,SCHYEAR
      ,DepartmentID
      ,dept_code 
      ,deptdesc
	  ,ac_code
	  ,acctnum 
	  ,totcol 
	  ,flthrs 
      ,chrgrate
	  ,chrgunit
	  ,nmiles
	  ,fltcost
	  ,LogNum 
	  ,cmon
	  ,lmon
	  ,Category 
	  ,Rnk
	  ,tail_nmbr=TailNum
	  ,LegNum
	  FROM (
SELECT DATE
      ,SCHYEAR
      ,DepartmentID
      ,dept_code 
      ,deptdesc
	  ,ac_code
	  ,acctnum 
	  ,totcol 
	  ,flthrs=FLOOR(flthrs*10)*0.1 
      ,chrgrate=ISNULL(chrgrate,0)
	  ,chrgunit
	  ,nmiles
	  ,fltcost=ISNULL(fltcost,0) 
	  ,LogNum 
	  ,cmon=LEFT(DateName( month , DateAdd( month , DATE , -1 ) ),3)
	  ,lmon='TRUE'
	  ,Category 
	  ,Rnk
	  ,TailNum
	  ,FleetID
	  ,LegNum
	  FROM @DeptRequestor
	  WHERE (dept_code IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
UNION ALL

SELECT M.MonthNo
      ,M.Year
      ,M.DepartmentID
      ,dept_code 
      ,deptdesc
	  ,ac_code
	  ,acctnum=NULL 
	  ,totcol=NULL 
	  ,flthrs=0
      ,chrgrate=0
	  ,chrgunit=NULL
	  ,nmiles=0
	  ,fltcost=0 
	  ,LogNum=NULL 
	  ,cmon=LEFT(DateName( month , DateAdd( month , M.MonthNo , -1 ) ),3)
	  ,lmon='TRUE'
	  ,M.Category 
	  ,Rnk=2
	  ,TailNum
	  ,FleetID
	  ,LegNum=NULL
	  FROM @Month M
	  WHERE NOT EXISTS(SELECT DR.DepartmentID,DR.FleetID FROM @DeptRequestor DR WHERE DR.DepartmentID=M.DepartmentID AND DR.FleetID=M.FleetID AND DR.DATE=M.MonthNo AND DR.SCHYEAR=M.Year)
	    AND (dept_code IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	   
)TEMP	  
ORDER BY Category,TailNum


END


         


--SELECT * FROM #REQUESTOR  WHERE (BlockHours<>0 OR ChargeBack<>0)

--EXEC spGetReportPOSTRequestorChargeBackDetailByDepartmentExportInformation 'SUPERVISOR_99', '10099', '2009', '', '', '', 0,''



--EXEC [spGetReportPOSTDepartmentChargeBackDetailByAircraftExportInformation] 'jwilliams_13','10099','2010','','',0


GO


