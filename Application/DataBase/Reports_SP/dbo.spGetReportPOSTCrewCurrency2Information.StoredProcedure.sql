IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrency2Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



         
CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2Information]                        
( 
 @UserCD Varchar(30),
 @AsOf DateTime, 
 @CrewCD Varchar(5000)='',
 @CrewGroupCD Varchar(5000)='',
 @AircraftCD Varchar(5000)='',
 @IsInactive Bit=0
)                      
AS                                          
-- ===============================================================================                        
-- SPC Name: spGetReportPOSTCrewCurrency2Information                         
-- Author:  Mathes                        
-- Create date: 30 Aug 2012                        
-- Description: Get Crew Currency for REPORTS                        
-- Revision History                        
-- Date   Name  Ver  Change                        
--                         
-- ================================================================================                        
     
SET NOCOUNT ON                             
   
BEGIN    
  
-----------------------------Crew and Crew Group Filteration----------------------  
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
DECLARE @TempCrewID  TABLE       
 (     
  ID INT NOT NULL IDENTITY (1,1),   
  CrewID BIGINT  
  )  
    
  
IF @CrewCD <> ''  
BEGIN  
 INSERT INTO @TempCrewID  
 SELECT DISTINCT C.CrewID   
 FROM Crew C  
 WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))   
 AND C.CustomerID = @CUSTOMERID    
END  
  
IF @CrewGroupCD <> ''  
BEGIN    
INSERT INTO @TempCrewID  
 SELECT DISTINCT  C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
 FROM Crew C   
  LEFT OUTER JOIN CrewGroupOrder CGO   
     ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID  
  LEFT OUTER JOIN CrewGroup CG   
     ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
 WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ','))   
 AND C.CustomerID = @CUSTOMERID   
 AND CG.IsDeleted=0   
END  
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''  
BEGIN    
INSERT INTO @TempCrewID  
 SELECT DISTINCT  C.CrewID  
 FROM Crew C   
 WHERE C.CustomerID = @CUSTOMERID    
 AND C.IsDeleted=0  
 --AND C.IsStatus=1  
END  


-----------------------------Crew and Crew Group Filteration----------------------  
     
   DECLARE @TempCrew TABLE(CrewID BigInt)   
   Insert Into @TempCrew SELECT DISTINCT TC.CrewID   
   FROM PostflightLeg inner JOIN  
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID and PostflightMain.IsDeleted=0   
                      INNER JOIN PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID   
                      INNER JOIN Fleet ON Fleet.FleetID = PostflightMain.FleetID   
                      INNER JOIN @TempCrewID TC ON TC.CrewID=PostflightCrew.CrewID  
                      left JOIN Aircraft ON Fleet.AircraftID = Aircraft.AircraftID   
  
   WHERE PostflightLeg.CustomerID =   + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                              
   AND Convert(Date,PostflightLeg.ScheduledTM)<=@AsOf   
   AND PostflightLeg.IsDeleted=0  
  
DECLARE @ldQtrBeg DATE        
DECLARE @ldPQtrBeg DATE        
DECLARE @lnQtrSec BIGINT     
DECLARE @lnQtrHrs BIGINT     
        
DECLARE @FIRST_DAY_MONTH DATE        
DECLARE @FIRST_DAY_YEAR DATE        
     
SET @FIRST_DAY_MONTH=(SELECT CONVERT(DATE,DATEADD(MONTH, DATEDIFF(MONTH, 0, @AsOf), 0)))        
SET @FIRST_DAY_YEAR=( SELECT CONVERT(DATE,DATEADD(YEAR, DATEDIFF(YEAR, 0, @AsOf), 0)))        
  
--DECLARE @TimeDisplayTenMin NUMERIC(1,0) =1   
DECLARE @TimeDisplayTenMin SMALLINT = 0;  
SELECT @TimeDisplayTenMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
    AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)   
  
DECLARE @AircraftBasis NUMERIC(3,0)        
SELECT @AircraftBasis=AircraftBasis FROM  Company,Crew         
                     WHERE Company.homebaseairportid=Crew.HomebaseID         
                     AND  Crew.CrewID In (Select Distinct CrewID From @TempCrew)  
   
DECLARE @TempPO TABLE(RowID INT IDENTITY,        
    AircraftCD VARCHAR(10),        
    FleetID BIGINT,        
    POLogID BIGINT,        
    POLegID BIGINT,        
    ScheduledTM DATE,        
    InboundDTTM DATETIME,        
    LandingDay NUMERIC(3,0),        
    LandingNight NUMERIC(3,0),        
    ApproachPrecision NUMERIC(2,0),        
    ApproachNonPrecision NUMERIC(2,0),        
    Instrument NUMERIC(7,3),        
    BlockHours NUMERIC(7,3),        
    FlightHours  NUMERIC(7,3),        
    DutyHours NUMERIC(7,3),        
    DutyEnd CHAR(5),        
    CrewID BIGINT,        
    TakeOffDay NUMERIC(2,0),        
    TakeOffNight NUMERIC(2,0),        
    RType CHAR(5),        
    Actualduty NUMERIC(7,3),        
    CrewName VARCHAR(100),  
    CrewCD varchar(10),  
    IsInActive Bit)        
            
   
DECLARE @TEMP table(CrewID BIGINT,    
     CrewCD VARCHAR(10),      
     DayLanding NUMERIC(3,0),        
     NightLanding NUMERIC(3,0),        
     Approach NUMERIC(3,0),        
     Instrument NUMERIC(3,0),        
     Day7 NUMERIC(3,0),         
     RestDays NUMERIC(3,0),        
     Day90 NUMERIC(3,0),        
     Month6 NUMERIC(3,0),        
     Month12 NUMERIC(3,0),        
     Day365 NUMERIC(3,0),        
     TakeoffDay NUMERIC(3,0),        
     TakeoffNight NUMERIC(3,0),        
     Day30 NUMERIC(3,0),  
     HomeBase BIGINT)       
       
INSERT INTO @TEMP(CrewID,CrewCD,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30,HomeBase)      
 SELECT C.CrewID,C.CrewCD,DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30,CO.HomebaseAirportID     
    FROM Crew C LEFT JOIN Company CO ON CO.HomebaseAirportID = C.HomebaseID  AND Co.CustomerID = C.CustomerID      
    WHERE C.CrewID In(Select Distinct CrewID From @TempCrew)    
     AND (C.IsStatus IN (CASE WHEN @IsInactive=1 THEN 1  ELSE 0 END) OR @IsInactive =0)
     AND C.IsDeleted = 0   
 --Start Newly Add for other homebase crew  
 --Get LoggedIn User HomebaseId and CustomerId  
DECLARE @UserHomeBaseId BIGINT  
DECLARE @UserCustomerId BIGINT  
SELECT @UserHomeBaseId = HomebaseID, @UserCustomerId=CustomerID FROM UserMaster WHERE UserName = @UserCD  
DECLARE @UserHomeBaseAirportId BIGINT  
SELECT @UserHomeBaseAirportId = HomeBaseAirportId FROM Company WHERE HomebaseID = @UserHomeBaseId   
  
     
 UPDATE @TEMP SET DayLanding=C.DayLanding,  
     NightLanding=C.NightLanding,  
     Approach=C.Approach,  
     Instrument=C.Instrument,  
     Day7=C.Day7,  
     RestDays=C.RestDays,  
     Day90=C.Day90,  
     Month6=C.Month6,  
     Month12=C.Month12,  
     Day365=C.Day365,  
     TakeoffDay=C.TakeoffDay,  
     TakeoffNight=C.TakeoffNight,  
     Day30=C.Day30 FROM (SELECT DayLanding,NightLanding,Approach,Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,TakeoffDay,TakeoffNight,Day30 FROM Company C WHERE C.HomebaseAirportID=@UserHomeBaseAirportId  ) C     
             WHERE HomeBase IS NULL   
  
  
   
 --End Newly Add for other homebase crew                                     
SET @ldQtrBeg=(SELECT CONVERT(DATE,Dateadd(qq, Datediff(qq,0,@AsOf), 0)))        
SET @ldPQtrBeg=(SELECT CONVERT(DATE,DATEADD(qq,DATEDIFF(qq,0,@AsOf)-1,0)))        
        
        
INSERT @TempPO(AircraftCD,        
    FleetID,        
    POLogID,        
    POLegID,        
    ScheduledTM,        
    InboundDTTM,        
    LandingDay,        
    LandingNight,        
    ApproachPrecision,        
    ApproachNonPrecision,        
    Instrument,        
    BlockHours,        
    FlightHours,        
    DutyHours,        
    DutyEnd,        
    CrewID,        
    TakeOffDay,        
    TakeOffNight,        
    RType,        
    Actualduty,        
    CrewName,  
    CrewCD,  
    IsInActive)         
(SELECT A.AircraftCD,PM.FleetID,PL.POLogID,PL.POLegID,CONVERT(DATE,PL.ScheduledTM) ScheduledTM,PL.InboundDTTM,PC.LandingDay,        
       PC.LandingNight,PC.ApproachPrecision,PC.ApproachNonPrecision,PC.Instrument,ROUND(PC.BlockHours,1) BlockHours,   
       ROUND(PC.FlightHours,1) FlightHours,ROUND(PC.DutyHours,1) DutyHours,PC.DutyEnd,PC.CrewID,PC.TakeOffDay,PC.TakeOffNight,'FL' AS RType,        
       CONVERT(INT,'')  AS Actualduty,IsNull(C.FirstName,'')+' '+ IsNull(C.MiddleInitial,'')+' '+ IsNull(C.LastName,'') AS CrewName, C.CrewCD,IsNull(F.IsInActive,0)         
      FROM  PostflightMain PM   
      INNER JOIN PostflightCrew PC ON PC.POLogID=PM.POLogID  
      INNER JOIN PostflightLeg PL ON PC.POLegID=PL.POLegID AND PL.IsDeleted=0   
      INNER JOIN Fleet F ON F.FleetID=PM.FleetID  
      LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID  
      INNER JOIN Crew C ON PC.CrewID=C.CrewID         
  WHERE    CONVERT(DATE,PL.ScheduledTM) BETWEEN @AsOf-400 AND @AsOf    
    AND C.CrewID IN (Select Distinct CrewID From @TempCrew) AND PM.IsDeleted = 0  
UNION          
        
 SELECT  A.AircraftCD,0 FleetID,0 LogID,0 POLegID,PFSL.SessionDT AS SCHEDTTM,''InboundDTTM,        
          PFSL.LandingDay,PFSL.LandingNight,PFSL.ApproachPrecision,PFSL.ApproachNonPrecision,        
          PFSL.Instrument,PFSL.FlightHours AS BLK_HRS,PFSL.FlightHours,NULL DutyHours,''DutyEnd,        
          PFSL.CrewID,PFSL.TakeOffDay,PFSL.TakeOffNight,'SL' AS RType,CONVERT(INT,'')  AS Actualduty,        
          IsNull(C.FirstName,'')+' '+ IsNull(C.MiddleInitial,'')+' '+ IsNull(C.LastName,'') AS CrewName ,C.CrewCD,1        
    FROM PostflightSimulatorLog PFSL INNER JOIN Crew C ON PFSL.CrewID=C.CrewID  
         LEFT OUTER JOIN Aircraft A ON PFSL.AircraftID=A.AircraftID       
   WHERE  PFSL.SessionDT BETWEEN @AsOf-400 AND @AsOf  
     AND C.CrewID In (Select Distinct CrewID From @TempCrew) AND PFSL.IsDeleted = 0)     
   
   
SET @lnQtrSec=(SELECT DATEDIFF(hh, @ldQtrBeg,@AsOf) as Hours_Difference)  
     
SET @lnQtrHrs= (SELECT DATEDIFF(day, @ldQtrBeg, @AsOf) * 24)  
                                                                   
IF @AircraftBasis=1         
 BEGIN      
     
 SELECT Distinct A.CrewID,A.CrewCD, A.CrewName, A.AircraftCD,SUM(A.L_D90) LandingDay,SUM(A.L_N90)L_N90,SUM(A.Appr90)Appr90,SUM(A.Instr90)Instr90,SUM(A.days7)days7,MAX(A.dayrest7) dayrest7,SUM(A.CalMon)CalMon,SUM(A.days90) days90,MAX(A.CalQtrRest) CalQtrRest,SUM(A.CalQtr)CalQtr,SUM(A.Mon6)Mon6,SUM(A.Prev2Qtrs)Prev2Qtrs,SUM(A.Mon12)Mon12,SUM(A.Calyr)Calyr,SUM(A.Days365)Days365,SUM(A.T_D90)T_D90,SUM(A.T_N90)T_N90,SUM(A.days30) days30,@TimeDisplayTenMin TenToMin FROM  
 (SELECT  T1.CrewID,T1.CrewCD,T1.CrewName ,T1.AircraftCD, T1.RType,       
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.DayLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.NightLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingNight),0)ELSE '0.0' END) L_N90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Approach,0)) AND @AsOf THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Instrument,0)) AND @AsOf THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Day7,0)) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) days7,        
  
  CASE WHEN (T1.RType = 'FL') THEN     
   ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (@AsOf-ISNULL(T.RestDays,0)) AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)        
  ELSE  
   ISNULL((T.RestDays*24),0)  
  END dayrest7,        
    
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalMon,        
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(Day90,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days90,        
   
  CASE WHEN (T1.RType = 'FL') THEN      
   ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0) ELSE '0.0' END),0)    
  ELSE  
   ISNULL(@lnQtrHrs,0)  
  END CalQtrRest,    
  
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) CalQtr,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month6*30),0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon6,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Prev2Qtrs,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month12*30),0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Mon12,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) Calyr,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day365,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0)ELSE '0.0' END) Days365,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffDay,0) AND @AsOf THEN ISNULL(SUM(T1.TakeOffDay),0) ELSE '0.0' END) T_D90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffNight,0) AND @AsOf THEN ISNULL(SUM(T1.TakeoffNight),0) ELSE '0.0' END) T_N90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day30,0) AND @AsOf THEN ISNULL(SUM(T1.BlockHours),0) ELSE '0.0' END) days30,        
       @TimeDisplayTenMin TenToMin       
   FROM  Crew C  
       INNER JOIN @TEMP T ON C.CrewID = T.CrewID  
       INNER JOIN  @TempPO T1 ON C.CrewID = T1.CrewID   
       LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE TP.CrewID IN (Select Distinct CrewID From @TempCrew) AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID  
   WHERE   
  C.CrewID In (Select Distinct CrewID From @TempCrew)  
     And (AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')    
     And (C.IsStatus IN (CASE WHEN @IsInactive=1 THEN 1  ELSE 0 END) OR @IsInactive =0)
   GROUP BY T1.CrewID,T1.CrewCD,T1.CrewName,T1.AircraftCD, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM)A     
   GROUP BY A.CrewID,A.CrewCD,A.CrewName,A.AircraftCD   
   Order By CrewCD,AircraftCD    
  END         
ELSE        
  BEGIN    
    
 SELECT Distinct A.CrewID,A.CrewCD,  
        A.CrewName,  
        A.AircraftCD,  
        SUM(A.L_D90) LandingDay,  
        SUM(A.L_N90)L_N90,  
        SUM(A.Appr90)Appr90,  
        SUM(A.Instr90)Instr90,  
        SUM(A.days7)days7,  
        MAX(A.dayrest7) dayrest7,  
        SUM(A.CalMon)CalMon,  
        SUM(A.days90) days90,  
        MAX(A.CalQtrRest) CalQtrRest,  
        SUM(A.CalQtr)CalQtr,  
        SUM(A.Mon6)Mon6,  
        SUM(A.Prev2Qtrs)Prev2Qtrs,  
        SUM(A.Mon12)Mon12,  
        SUM(A.Calyr)Calyr,  
        SUM(A.Days365)Days365,  
        SUM(A.T_D90)T_D90,  
        SUM(A.T_N90)T_N90,  
        SUM(A.days30) days30,  
        @TimeDisplayTenMin TenToMin   
     FROM     
 (SELECT T1.CrewID,T1.CrewCD,T1.CrewName , T1.AircraftCD, T1.RType,  
    (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.DayLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingDay),0) ELSE '0.0' END) L_D90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.NightLanding,0)) AND @AsOf THEN ISNULL(SUM(T1.LandingNight),0) ELSE '0.0' END) L_N90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Approach,0)) AND @AsOf THEN ISNULL(SUM(T1.ApproachPrecision) + SUM(T1.ApproachNonPrecision),0) ELSE '0.0' END) Appr90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Instrument,0)) AND @AsOf THEN ISNULL(SUM(T1.Instrument),0) ELSE '0.0' END) Instr90,  
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN (@AsOf-ISNULL(T.Day7,0)) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0) ELSE '0.0' END) days7,        
  
  CASE WHEN (T1.RType = 'FL') THEN      
   ISNULL((T.RestDays*24)-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN (@AsOf-ISNULL(T.RestDays,0)) AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0)ELSE 0.0 END),0)  
       ELSE  
   ISNULL((T.RestDays*24),0)  
       END dayrest7,        
  
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_MONTH AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalMon,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(Day90,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days90,      
  
  CASE WHEN (T1.RType = 'FL') THEN      
    ISNULL(@lnQtrHrs-(CASE WHEN (CONVERT(DATE,TM.MaxScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(TM.DutyHours),0)ELSE '0.0' END),0)           
  ELSE  
   ISNULL((@lnQtrHrs),0)  
  END CalQtrRest,        
  
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldQtrBeg AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) CalQtr,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month6*30),0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon6,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @ldPQtrBeg AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Prev2Qtrs,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL((T.Month12*30),0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Mon12,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @FIRST_DAY_YEAR AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Calyr,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day365,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) Days365,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffDay,0) AND @AsOf THEN ISNULL(SUM(T1.TakeOffDay),0)ELSE '0.0' END) T_D90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.TakeoffNight,0) AND @AsOf THEN ISNULL(SUM(T1.TakeoffNight),0)ELSE '0.0' END) T_N90,        
       (CASE WHEN (CONVERT(DATE,T1.ScheduledTM)) BETWEEN  @AsOf-ISNULL(T.Day30,0) AND @AsOf THEN ISNULL(SUM(T1.FlightHours),0)ELSE '0.0' END) days30,        
       @TimeDisplayTenMin TenToMin   
   FROM   Crew C       
  INNER JOIN @TEMP T ON C.CrewID = T.CrewID  
  INNER JOIN @TempPO T1 ON C.CrewID = T1.CrewID  
  LEFT OUTER JOIN (SELECT TP.POLOGID, MAX(TP.ScheduledTM) MaxScheduledTM, SUM(DutyHours) DutyHours FROM @TempPO TP WHERE TP.CrewID  IN(Select Distinct CrewID From @TempCrew) AND TP.RType = 'FL' GROUP BY TP.POLogID) TM ON T1.POLOGID=TM.POLogID     
   WHERE   
  C.CrewID In (Select Distinct CrewID From @TempCrew)  
  And (AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')    
  And (C.IsStatus IN (CASE WHEN @IsInactive=1 THEN 1  ELSE 0 END) OR @IsInactive =0) 
   GROUP BY T1.CrewID,T1.CrewCD,T1.CrewName,T1.AircraftCD, T1.RType, T1.CrewID,T1.CrewName,ScheduledTM,DayLanding,NightLanding,Approach,T.Instrument,Day7,RestDays,Day90,Month6,Month12,Day365,T.TakeoffDay,T.TakeoffNight,Day30,TM.MaxScheduledTM) A  
   GROUP BY A.CrewID,A.CrewCD,A.CrewName,A.AircraftCD   
   Order By  CrewCD, AircraftCD    
   END        
END          
--spGetReportPOSTCrewCurrency2Information 'JWILLIAMS_13','2012/01/01', 10013515401351540     
  
GO


