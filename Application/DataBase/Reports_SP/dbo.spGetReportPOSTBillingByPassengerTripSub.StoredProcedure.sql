
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByPassengerTripSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripSub]         
( 
@UserCD AS VARCHAR(30), --Mandatory
@UserCustomerID AS VARCHAR(30),           
@DATEFROM AS DATE , --Mandatory          
@DATETO AS DATE , --Mandatory           
@LogNum NVARCHAR(1000) = '',
@PassengerRequestorCD  AS VARCHAR(1000) = '',
@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values   
@FleetGroupCD AS NVARCHAR(4000) = '', 
@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase   
@UserHomebaseID AS VARCHAR(30) ,
@PassengerGroupCD VARCHAR(500)=''   
)   
AS
--================================================================================              
-- SPC Name:[spGetReportPOSTBillingByPassengerTripSub]        
-- Author: HARIHARAN S          
-- Create date: 02/18/2013          
-- Description: Get Trip Detail by billing for REPORTS          
-- Revision History          
-- Date   Name  Ver  Change          
--================================================================================              
BEGIN

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
--ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
--BEGIN  
--INSERT INTO @TempPassengerID
--	SELECT DISTINCT  P.PassengerRequestorID
--	FROM Passenger P 
--	WHERE  P.CustomerID = @CUSTOMER  
--	AND P.IsDeleted=0
--END
-----------------------------Passenger and Passenger Group Filteration----------------------	
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 

IF (@PassengerRequestorCD <> '' OR  @PassengerGroupCD <> '')

BEGIN 


SELECT TEMP1.DepartmentCD,TEMP1.AuthorizationCD,TEMP1.PassengerRequestorCD,TEMP1.PASSENGERNAME,TEMP1.LogNum,TEMP1.Billing, SUM(TEMP1.StdVAlue) StdVAlue FROM(
SELECT 
TEMP.DepartmentCD
,TEMP.AuthorizationCD
,ISNULL(TEMP.LastName+', ','')+ISNULL(TEMP.FirstName+' ','')+ISNULL(TEMP.MiddleInitial,'') PASSENGERNAME 
,TEMP.PassengerRequestorCD
--,TEMP.ScheduledTM
,TEMP.POLogID
,TEMP.LogNum
,TEMP.Billing
,TEMP.CustomerID
,FLOOR(SUM(TEMP.StdVAlue)*100)*0.01 StdVAlue 
FROM 
(	
SELECT DISTINCT D.DepartmentCD,DA.AuthorizationCD,PL.OutboundDTTM,POM.POLogID,PL.LegNUM,PL.PassengerTotal,P.PassengerRequestorCD,P.LastName
,P.FirstName,P.MiddleInitial, POM.LOGNUM,Pop.Billing,POM.CustomerID, ChargeRate,ExpenseAMT,ChargeUnit,PL.DepartmentID,PL.AuthorizationID,
PL.Distance,
CASE WHEN PL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(PL.BlockHours,1) ELSE ROUND(PL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/PL.PassengerTotal,0)
						)
						END 
						StdVAlue
FROM PostflightMain POM
		LEFT OUTER JOIN PostflightLeg PL ON POM.POLogID = PL.POLogID AND PL.IsDeleted=0
		LEFT OUTER JOIN (
					SELECT PM.LogNum, PL.POLegID,PM.CustomerID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
					FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
										   INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
					AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM) AND PM.IsDeleted=0
				   ) FCR ON PL.POLegID = FCR.POLegID
		LEFT OUTER JOIN PostflightPassenger POP ON PL.POLegID=POP.POLegID
		LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ISNULL(ExpenseAMT,0))ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted=0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted=0
								WHERE PE.IsBilling=1
								  AND PE.IsDeleted=0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID,PM.LogNum)Temp
								LEFT OUTER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON PL.POLegID = POE.POLegID 
								AND PL.CustomerID=POE.CustomerID AND PL.POLogID=POE.POLogID
        INNER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
        INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
        --LEFT OUTER JOIN vDeptAuthGroup DE ON P.DepartmentID = DE.DepartmentID AND P.AuthorizationID=DE.AuthorizationID AND P.CustomerID = DE.CustomerID
		LEFT OUTER JOIN Department D ON D.DepartmentID = P.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0   
						--AND D.DepartmentID = DA.DepartmentID 
						AND P.AuthorizationID=DA.AuthorizationID  
		 WHERE POM.CUSTOMERID=DBO.GetCustomerIDbyUserCD(@UserCD)
		 AND POM.IsDeleted=0
		 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND  POM.LogNum=@LogNum 
				 )
TEMP
LEFT OUTER JOIN Department D ON D.DepartmentID = TEMP.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = TEMP.AuthorizationID
WHERE TEMP.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	--AND CONVERT(DATE,TEMP.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (TEMP.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	---AND (TEMP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
GROUP BY TEMP.POLogID,TEMP.LogNum,TEMP.Billing,TEMP.CustomerID,TEMP.PassengerRequestorCD
,TEMP.MiddleInitial,TEMP.FirstName,TEMP.LastName,TEMP.DepartmentCD,TEMP.AuthorizationCD,D.DepartmentCD,DA.AuthorizationCD)TEMP1
GROUP BY TEMP1.DepartmentCD,TEMP1.AuthorizationCD,TEMP1.PASSENGERNAME,TEMP1.LogNum,TEMP1.Billing,TEMP1.PassengerRequestorCD

END

ELSE 

BEGIN

SELECT TEMP1.DepartmentCD,TEMP1.AuthorizationCD,TEMP1.PassengerRequestorCD,TEMP1.PASSENGERNAME,TEMP1.LogNum,TEMP1.Billing, SUM(TEMP1.StdVAlue) StdVAlue FROM(
SELECT 
TEMP.DepartmentCD
,TEMP.AuthorizationCD
,ISNULL(TEMP.LastName+', ','')+ISNULL(TEMP.FirstName+' ','')+ISNULL(TEMP.MiddleInitial,'') PASSENGERNAME 
,TEMP.PassengerRequestorCD
--,TEMP.ScheduledTM
,TEMP.POLogID
,TEMP.LogNum
,TEMP.Billing
,TEMP.CustomerID
,FLOOR(SUM(TEMP.StdVAlue)*100)*0.01 StdVAlue 
FROM 
(	
SELECT DISTINCT D.DepartmentCD,DA.AuthorizationCD,PL.OutboundDTTM,POM.POLogID,PL.LegNUM,PL.PassengerTotal,P.PassengerRequestorCD,P.LastName
,P.FirstName,P.MiddleInitial, POM.LOGNUM,Pop.Billing,POM.CustomerID, ChargeRate,ExpenseAMT,ChargeUnit,PL.DepartmentID,PL.AuthorizationID,
PL.Distance,
CASE WHEN PL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(PL.BlockHours,1) ELSE ROUND(PL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/PL.PassengerTotal,0)
						)
						END 
						StdVAlue
FROM PostflightMain POM
		LEFT OUTER JOIN PostflightLeg PL ON POM.POLogID = PL.POLogID AND PL.IsDeleted=0
		LEFT OUTER JOIN (
					SELECT PM.LogNum, PL.POLegID,PM.CustomerID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
					FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
										   INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
					AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM) AND PM.IsDeleted=0
				   ) FCR ON PL.POLegID = FCR.POLegID
		LEFT OUTER JOIN PostflightPassenger POP ON PL.POLegID=POP.POLegID
		LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ISNULL(ExpenseAMT,0))ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted=0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted=0
								WHERE PE.IsBilling=1
								  AND PE.IsDeleted=0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID,PM.LogNum)Temp
								LEFT OUTER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON PL.POLegID = POE.POLegID 
								AND PL.CustomerID=POE.CustomerID AND PL.POLogID=POE.POLogID
        INNER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
       -- INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
        --LEFT OUTER JOIN vDeptAuthGroup DE ON P.DepartmentID = DE.DepartmentID AND P.AuthorizationID=DE.AuthorizationID AND P.CustomerID = DE.CustomerID
		LEFT OUTER JOIN Department D ON D.DepartmentID = P.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0   
						--AND D.DepartmentID = DA.DepartmentID 
						AND P.AuthorizationID=DA.AuthorizationID  
		 WHERE POM.CUSTOMERID=DBO.GetCustomerIDbyUserCD(@UserCD)
		 AND POM.IsDeleted=0
		 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND  POM.LogNum=@LogNum 
				 )
TEMP
LEFT OUTER JOIN Department D ON D.DepartmentID = TEMP.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = TEMP.AuthorizationID
WHERE TEMP.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	--AND CONVERT(DATE,TEMP.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (TEMP.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	---AND (TEMP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
GROUP BY TEMP.POLogID,TEMP.LogNum,TEMP.Billing,TEMP.CustomerID,TEMP.PassengerRequestorCD
,TEMP.MiddleInitial,TEMP.FirstName,TEMP.LastName,TEMP.DepartmentCD,TEMP.AuthorizationCD,D.DepartmentCD,DA.AuthorizationCD)TEMP1
GROUP BY TEMP1.DepartmentCD,TEMP1.AuthorizationCD,TEMP1.PASSENGERNAME,TEMP1.LogNum,TEMP1.Billing,TEMP1.PassengerRequestorCD



END

END
--EXEC spGetReportPOSTBillingByPassengerTripSub 'supervisor_99','10099','01-01-2009','01-01-2010','2','','','','','',0 ,''         


GO


