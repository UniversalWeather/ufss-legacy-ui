IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegHotelInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegHotelInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSLegHotelInformation]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
		@LegNum AS VARCHAR(150) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSLegHotelInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==============================================


DECLARE @PaxCrew TABLE(PassengerRequestorCD VARCHAR(5),
                       LegID BIGINT,
                       HotelID BIGINT,
                       Type CHAR(2))
                       
INSERT INTO @PaxCrew
SELECT P1.PassengerRequestorCD ,PPL.LegID,HP.HotelID,'PH'
						    FROM PreflightMain PM 
								INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
								INNER JOIN  PreflightPassengerList PPL ON PPL.LegID=PL.LegID 
								INNER JOIN Passenger P1 ON PPL.PassengerID=P1.PassengerRequestorID 
								INNER JOIN PreflightHotelList PHL ON PHL.LegID = PL.LegID AND PHL.crewPassengerType = 'P'
								INNER JOIN PreflightHotelCrewPassengerList PPH ON PHL.PreflightHotelListID = PPH.PreflightHotelListID
															AND PPH.PassengerRequestorID = PPL.PassengerID															
								LEFT OUTER JOIN Hotel HP ON PHL.HotelID = HP.HotelID
							WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			                 AND PM.TripID = CONVERT(BIGINT,@TripID)
			                 AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			                 
	
INSERT INTO @PaxCrew
SELECT C.CrewCD ,PCLL.LegID,HC.HotelID,'CH' FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			LEFT JOIN PreflightCrewList PCLL ON PL.LegID = PCLL.LegID
			INNER JOIN Crew C ON PCLL.CrewID = C.CrewID
			INNER JOIN PreflightHotelList PHL ON PHL.LegID = PL.LegID AND PHL.crewPassengerType = 'C'
			INNER JOIN PreflightHotelCrewPassengerList PCH ON PHL.PreflightHotelListID = PCH.PreflightHotelListID
															AND PCH.CrewID = PCLL.CREWID															
			LEFT OUTER JOIN Hotel HC ON PHL.HotelID = HC.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			--AND PCLL.DutyTYPE IN ('P','S')
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			ORDER BY(CASE PCLL.DutyTYPE WHEN 'P' THEN 1 
								WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
								WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)

 /*
 INSERT INTO @PaxCrew
SELECT C.CrewCD ,PCLL.LegID,HC.HotelID,'MH' FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			LEFT JOIN PreflightCrewList PCLL ON PL.LegID = PCLL.LegID
			INNER JOIN Crew C ON PCLL.CrewID = C.CrewID
			INNER JOIN PreflightHotelList PHL ON PHL.LegID = PL.LegID AND PHL.crewPassengerType = 'C'
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelCrewPassengerList PCH ON PCH.PreflightHotelListID = PHL.PreflightHotelListID
															AND PCH.CrewID = C.CrewID
			LEFT OUTER JOIN Hotel HC ON PHL.HotelID = HC.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND PCLL.DutyTYPE NOT IN ('P','S')
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			ORDER BY(CASE PCLL.DutyTYPE WHEN 'P' THEN 1 
								WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
								WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)
			
*/

 
 SELECT DISTINCT  [LegId] = CONVERT(VARCHAR,PL.LegID)
    		,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[HDescription] = ISNULL(PPH.PreflightHotelName,'')
			,[HPhone] = ISNULL(PPH.PhoneNum1,'')
			,[HFax] = ISNULL(PPH.FaxNum,'')
			,[HAddress1] = ISNULL(PPH.Address1,'')
			,[HAddress2] = ISNULL(PPH.Address2,'')
			,[HCity] = ISNULL(PPH.CityName,'')
			,[HState] = ISNULL(PPH.StateName,'')
			,[HZip] = ISNULL(PPH.PostalZipCD,'')
			,[HRemarks] = ISNULL(HP.Remarks,'')
			,[HRate] = ISNULL(PPH.Rate,0)
			,[HRooms] = ISNULL(PPH.NoofRooms,0)
			,[HConfirm] = ISNULL(PPH.ConfirmationStatus,'')
			,[HComments] = ISNULL(PPH.Comments,'')
			,[HTYPE] = 'PH'
			,[CrewPaxCD] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1)
			,[IsHotelEmpty] = CASE WHEN ISNULL(PPH.PreflightHotelName,'') = '' AND
						ISNULL(PPH.PhoneNum1,'') = '' AND
						ISNULL(PPH.FaxNum,'') = '' AND
						ISNULL(PPH.Address1,'') = '' AND
						ISNULL(PPH.Address2,'') = '' AND
						ISNULL(PPH.CityName,'') = '' AND
						ISNULL(PPH.StateName,'') = '' AND
						ISNULL(PPH.PostalZipCD,'') = '' AND
						ISNULL(HP.Remarks,'') = '' AND
						ISNULL(PPH.Rate,0) = 0 AND																																																															
						ISNULL(PPH.NoofRooms,0) = 0 AND
						ISNULL(PPH.ConfirmationStatus,'') = '' AND
						ISNULL(PPH.Comments,'') = ''
					THEN 1 ELSE 0 END
			,[Flag]	 = CASE WHEN PPH.isArrivalHotel = 1 THEN 'A'
								 WHEN PPH.isDepartureHotel = 1 THEN 'D' ELSE '' END
			,[OrderCrewPaxCD] = ISNULL(SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),'Z')
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
			
			INNER JOIN PreflightHotelList PPH ON PPH.LegID = PL.LegID AND PPH.crewPassengerType = 'P'
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			LEFT JOIN (SELECT P.PassengerRequestorCD, LegID, P.PassengerRequestorID, NoofRooms FROM PreflightPassengerList PPL 
														   INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
														   --AND PCL.DutyTYPE  IN ('P','S')
														   ) PPLL ON PL.LegID = PPLL.LegID
			
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PPH.PreflightHotelListID --CREW ASSIGNED
															AND PHCP.PassengerRequestorID = PPLL.PassengerRequestorID															
			LEFT OUTER JOIN (SELECT DISTINCT PP2.LegID,PP2.HotelID, PaxNames = (
                       SELECT RTRIM(UPPER(PassengerRequestorCD)) +', ' FROM @PaxCrew PC 
                               WHERE PC.Type='PH'
                                 AND PC.LegID = PP2.LegID
                                 AND PC.HotelID=PP2.HotelID
                             FOR XML PATH('')
                             )
                              FROM @PaxCrew PP2
				     ) PAX ON PL.LegID = PAX.LegID AND PAX.HotelID=HP.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')			

	
UNION ALL

	--Crew Hotel 
SELECT DISTINCT [LegId] = CONVERT(VARCHAR,PL.LegID)
				,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM) 
				,ISNULL(PCH.PreflightHotelName,'')
				,ISNULL(PCH.PhoneNum1,'')
				,ISNULL(PCH.FaxNum,'')
				,ISNULL(PCH.Address1,'')
				,ISNULL(PCH.Address2,'')
				,ISNULL(PCH.CityName,'')
				,ISNULL(PCH.StateName,'')
				,ISNULL(PCH.PostalZipCD,'')
				,ISNULL(HC.Remarks,'')
				,ISNULL(PCH.Rate,0)
				,ISNULL(PCH.NoofRooms,0)
				,ISNULL(PCH.ConfirmationStatus,'')
				,ISNULL(PCH.Comments,'') 
				,HTYPE = 'CH'
				,[CrewPaxCD] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1)
				,[IsHotelEmpty] = CASE WHEN ISNULL(PCH.PreflightHotelName,'') = '' AND
							ISNULL(PCH.PhoneNum1,'') = '' AND
							ISNULL(PCH.FaxNum,'') = '' AND
							ISNULL(PCH.Address1,'') = '' AND
							ISNULL(PCH.Address2,'') = '' AND
							ISNULL(PCH.CityName,'') = '' AND
							ISNULL(PCH.StateName,'') = '' AND
							ISNULL(PCH.PostalZipCD,'') = '' AND
							ISNULL(HC.Remarks,'') = '' AND
							ISNULL(PCH.Rate,0) = 0 AND																																																															
							ISNULL(PCH.NoofRooms,0) = 0 AND
							ISNULL(PCH.ConfirmationStatus,'') = '' AND
							ISNULL(PCH.Comments,'') = ''
						THEN 1 ELSE 0 END 
				,[Flag] = CASE WHEN PCH.isArrivalHotel = 1 THEN 'A'
								 WHEN PCH.isDepartureHotel = 1 THEN 'D' ELSE '' END
				,[OrderCrewPaxCD] = ISNULL(SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),'Z')
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PL.LegID AND PCH.crewPassengerType = 'C'
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			LEFT JOIN (SELECT CrewCD, LegID, C.CrewID, NoofRooms, DutyTYPE FROM PreflightCrewList PCL 
														   INNER JOIN Crew C ON PCL.CrewID = C.CrewID 
														   --AND PCL.DutyTYPE  IN ('P','S')
														   ) PCLL ON PL.LegID = PCLL.LegID
			
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID --CREW ASSIGNED
															AND PHCP.CrewID = PCLL.CREWID															
			LEFT OUTER JOIN (SELECT DISTINCT PP2.LegID,PP2.HotelID, PaxNames = (
                       SELECT RTRIM(UPPER(PassengerRequestorCD)) +', ' FROM @PaxCrew PC 
                               WHERE PC.Type='CH'
                                 AND PC.LegID = PP2.LegID
                                 AND PC.HotelID=PP2.HotelID
                             FOR XML PATH('')
                             )
                              FROM @PaxCrew PP2
				     ) PAX ON PL.LegID = PAX.LegID AND PAX.HotelID=HC.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
 /*
 UNION ALL

	-- Addnl Crew / Maint Crew - Hotel	
SELECT DISTINCT [LegId] = CONVERT(VARCHAR,PL.LegID), [LegNUM] = CONVERT(VARCHAR,PL.LegNUM) 
				,ISNULL(PCH.PreflightHotelName,''),ISNULL(PCH.PhoneNum1,''),ISNULL(PCH.FaxNum,''),ISNULL(PCH.Address1,''),ISNULL(PCH.Address2,''),
				ISNULL(PCH.CityName,''),ISNULL(PCH.StateName,''),ISNULL(PCH.PostalZipCD,''),ISNULL(HC.Remarks,''),ISNULL(PCH.Rate,0),ISNULL(PCLL.NoofRooms,0),
				ISNULL(PCH.ConfirmationStatus,''),ISNULL(PCH.Comments,''), 
				 HTYPE = 'MH'
				,[CrewPaxCD] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) 
				,[IsHotelEmpty] = CASE WHEN ISNULL(PCH.PreflightHotelName,'') = '' AND
							ISNULL(PCH.PhoneNum1,'') = '' AND
							ISNULL(PCH.FaxNum,'') = '' AND
							ISNULL(PCH.Address1,'') = '' AND
							ISNULL(PCH.Address2,'') = '' AND
							ISNULL(PCH.CityName,'') = '' AND
							ISNULL(PCH.StateName,'') = '' AND
							ISNULL(PCH.PostalZipCD,'') = '' AND
							ISNULL(HC.Remarks,'') = '' AND
							ISNULL(PCH.Rate,0) = 0 AND																																																															
							ISNULL(PCLL.NoofRooms,0) = 0 AND
							ISNULL(PCH.ConfirmationStatus,'') = '' AND
							ISNULL(PCH.Comments,'') = ''
						THEN 1 ELSE 0 END 
				,[Flag] = CASE WHEN PCH.isArrivalHotel = 1 THEN 'A'
								WHEN PCH.isDepartureHotel = 1 THEN 'D' ELSE '' END 
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PL.LegID AND PCH.crewPassengerType = 'C'
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			LEFT JOIN (SELECT CrewCD, LegID, C.CrewID, NoofRooms, DutyTYPE FROM PreflightCrewList PCL 
														   INNER JOIN Crew C ON PCL.CrewID = C.CrewID AND PCL.DutyTYPE NOT IN ('P','S')) PCLL ON PL.LegID = PCLL.LegID
			
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID --CREW ASSIGNED
															AND PHCP.CrewID = PCLL.CREWID
			LEFT OUTER JOIN (SELECT DISTINCT PP2.LegID,PP2.HotelID, PaxNames = (
                       SELECT PassengerRequestorCD +', ' FROM @PaxCrew PC 
                               WHERE PC.Type='MH'
                                 AND PC.LegID = PP2.LegID
                                 AND PC.HotelID=PP2.HotelID
                             FOR XML PATH('')
                             )
                              FROM @PaxCrew PP2
				     ) PAX ON PL.LegID = PAX.LegID AND PAX.HotelID=HC.HotelID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND PCLL.DutyTYPE NOT IN ('P','S')
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
  */
   --ORDER BY LegNUM, [HTYPE], CrewPaxCD
   ORDER BY LegNUM, [HTYPE],[OrderCrewPaxCD]



	END

--EXEC spGetReportPRETSLegHotelInformation 'ERICK_1', '1000158811','2'


GO


