IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity1ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity1ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================    
-- SPC Name: spGetReportPREDailybaseActivity1ExportInformation   
-- Author: Abhishek S    
-- Create date: 27 Jul 2012    
-- Description: Get DailybaseActivity1 Export Information for REPORTS    
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================   
CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity1ExportInformation]
        @UserCD AS VARCHAR(30), -- Mandatory
		@DATEFROM AS DATETIME,  -- Mandatory
		@DATETO AS DATETIME,    -- Mandatory
		@IcaoID AS NVARCHAR(500) = '', -- Mandatory
		@TripNum AS NVARCHAR(500) = '', -- [Optional]
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0
	
AS
BEGIN	
	SET NOCOUNT ON;
	
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------

	SELECT DISTINCT	locdep =PL.DepartureDTTMLocal
				   ,orig_nmbr=PM.TripNUM
                   ,legid=PL.LegID    
                   ,tail_nmbr=F.TailNum
				   ,ac_code=F.AircraftCD
                   ,activity=CASE WHEN (D.IcaoID = @IcaoID) THEN 'DEPARTS' ELSE 'ARRIVES' END 
                   ,icao=CASE WHEN (D.IcaoID = @IcaoID)  THEN D.IcaoID ELSE CONVERT(VARCHAR(10),A.IcaoID) END
    	           ,timeout=CASE WHEN (D.IcaoID = @IcaoID) THEN CONVERT(VARCHAR(5), PL.DepartureDTTMLocal , 108)      
                                                                                     ELSE CONVERT(VARCHAR(5), PL.ArrivalDTTMLocal , 108) END
                   ,pax_total=PL.PassengerTotal
                   ,activity2=CASE WHEN (A.IcaoID=@IcaoID) THEN 'DEPARTS'  ELSE 'ARRIVES' END 
                   ,icao2=CASE WHEN A.IcaoID=@IcaoID THEN CONVERT(VARCHAR(10),D.IcaoID)
                                                                                    ELSE CONVERT(VARCHAR(10),A.IcaoID) END
                   ,timeout2=CASE WHEN (A.IcaoID=@IcaoID) THEN CONVERT(VARCHAR(5),   PL.DepartureDTTMLocal , 108)      
                                                                                    ELSE CONVERT(varchar(5),  PL.ArrivalDTTMLocal , 108) END
				   ,paxname=CASE WHEN PL.PassengerTotal = 0 THEN ' ' ELSE ISNULL(P.LastName+ ',','') + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') END 
                   ,blocked=CASE WHEN PL.PassengerTotal = 0 THEN NULL ELSE PP.IsBlocked END
                   ,sort=(CASE(PP.IsBlocked)WHEN 'TRUE' THEN '1' ELSE 0 END)        
                   ,fltpurpose=CASE WHEN PL.PassengerTotal = 0 THEN ' ' ELSE FP.FlightPurposeCD END
                   INTO #TEMP
    
   FROM PreflightMain PM INNER JOIN PreflightLeg PL	ON PM.TripID = PL.TripID AND PL.IsDeleted = 0		  
		               	 LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS D ON D.AirportID = PL.DepartICAOID
		                 LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A ON A.AirportID = PL.ArriveICAOID		 
			             INNER JOIN (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID FROM Fleet F 
			                        ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			             INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			             LEFT OUTER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
			             LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PP.PassengerID
			             LEFT OUTER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID    
			      
    WHERE PM.IsDeleted = 0
	  AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	  AND PM.TripStatus = 'T' 
	  AND PM.RecordType <> 'M'
	  AND (D.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')))OR 
	       A.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ','))))
	  AND (PM.TripNUM IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ','))OR @TripNum='')
	  AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
	  --AND PL.LegNUM=1
	  
	  ORDER BY locdep,timeout,timeout2



 
			      


INSERT INTO #Temp
SELECT DISTINCT	locdep =PL.DepartureDTTMLocal
				   ,orig_nmbr=PM.TripNUM
                   ,legid=PL.LegID    
                   ,tail_nmbr=F.TailNum
				   ,ac_code=F.AircraftCD
                   ,activity=CASE WHEN (D.IcaoID = @IcaoID) THEN 'DEPARTS' ELSE 'ARRIVES' END 
                   ,icao=CASE WHEN (D.IcaoID = @IcaoID)  THEN D.IcaoID ELSE CONVERT(VARCHAR(10),A.IcaoID) END
    	          ,timeout=CASE WHEN (D.IcaoID = @IcaoID) THEN CONVERT(VARCHAR(5), PL.DepartureDTTMLocal , 108)      
                                                                                     ELSE CONVERT(VARCHAR(5), PL.ArrivalDTTMLocal , 108) END
                  ,pax_total=PL.PassengerTotal
                   ,activity2=CASE WHEN (A.IcaoID=@IcaoID) THEN 'DEPARTS'  ELSE 'ARRIVES' END 
                   ,icao2=CASE WHEN A.IcaoID=@IcaoID THEN CONVERT(VARCHAR(10),D.IcaoID)
                                                                                    ELSE CONVERT(VARCHAR(10),A.IcaoID) END
                   ,timeout2=CASE WHEN (A.IcaoID=@IcaoID) THEN CONVERT(VARCHAR(5),   PL.DepartureDTTMLocal , 108)      
                                                                                    ELSE CONVERT(varchar(5),  PL.ArrivalDTTMLocal , 108) END
				  ,paxname=''
                  ,blocked=NULL
                  ,sort=0--(CASE(PP.IsBlocked)WHEN 'TRUE' THEN '1' ELSE 0 END)        
                  ,fltpurpose=''
       
   FROM PreflightMain PM INNER JOIN PreflightLeg PL	ON PM.TripID = PL.TripID AND PL.IsDeleted = 0		  
		               	 LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS D ON D.AirportID = PL.DepartICAOID
		                 LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A ON A.AirportID = PL.ArriveICAOID		 
			             INNER JOIN (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID FROM Fleet F 
			                        ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			             INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			             INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
			             INNER JOIN Passenger P ON P.PassengerRequestorID=PP.PassengerID
			             INNER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID    
			      
    WHERE PM.IsDeleted = 0
	  AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	  AND PM.TripStatus = 'T' 
	  AND PM.RecordType <> 'M'
	  AND (D.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')))OR 
	       A.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ','))))
	  AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
	  AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase='')
ORDER BY locdep,timeout,timeout2


--DELETE FROM #Temp WHERE paxname='AA' HAVING COUNT( 	  

SELECT DISTINCT * FROM #TEMP  ORDER BY locdep,timeout,timeout2

IF OBJECT_ID('TEMPDB..#TempFleetID') IS NOT NULL DROP TABLE #TempFleetID
IF OBJECT_ID('TEMPDB..#Temp') IS NOT NULL DROP TABLE #Temp
-- -- EXEC spGetReportPREDailybaseActivity1ExportInformation 'ELIZA_9','2012-10-23','2012-11-29 ','KHOU',', ', ', 0
    
 END



GO


