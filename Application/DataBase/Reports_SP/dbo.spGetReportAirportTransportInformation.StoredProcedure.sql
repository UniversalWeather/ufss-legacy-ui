IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportTransportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportTransportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportAirportTransportInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =============================================
-- SPC Name: spGetReportAirportTransportInformation
-- Author: SUDHAKAR J
-- Create date: 12 Jun 2012
-- Description: Get Airport Transport Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

		SELECT
			T.IsChoice, T.TransportationVendor, T.PhoneNum
		FROM Transport T
			INNER JOIN Airport A ON T.AirportID = A.AirportID
		WHERE T.IsDeleted = 0
			AND T.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
			AND A.IcaoID = @IcaoID 
			
	    ORDER BY T.TransportationVendor

  -- EXEC spGetReportAirportTransportInformation 'tim', 'KHOU'


GO


