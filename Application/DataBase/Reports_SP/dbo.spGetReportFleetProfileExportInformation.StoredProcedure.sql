IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportFleetProfileExportInformation]  
 @UserCD VARCHAR(30)  
 ,@TailNum char(6)  
AS  
BEGIN
-- ===============================================================================================  
-- SPC Name: spGetReportFleetProfileExportInformation  
-- Author: SUDHAKAR J  
-- Create date: 21 Jun 2012  
-- Description: Get Passenger/CrewPassengerVisa Information for Report Passenger/Requestor I  
-- Revision History  
-- Date      Name     Ver   Change  
-- 29 Aug  Sudhakar   1.1   ClientID filter  
-- 28 Sep  Abhishek   1.2   Join new tables and changed the fields with alias
-- ===============================================================================================  
SET NOCOUNT ON  
  
 DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
  
 SELECT TEMP.*,IDENTITY( int )  AS idcol
  INTO #FleetTemp FROM  
 ( SELECT DISTINCT
        [accode]    = F.AircraftCD,
		[tailnmbr]   = F.TailNum,
		[serialnum]  = F.SerialNum,
		[typecode]   = A.AircraftCD,
		[typedesc]  = F.TypeDescription,
		[maxreserv]  = F.MaximumReservation,
		[lastinspdt] = CONVERT(varchar(10), F.LastInspectionDT), --dbo.GetReportDayMonthFormatByUserCD(@UserCD), 
		[insphrs]    = F.InspectionHrs,
		[warnhrs]    = F.WarningHrs, 
		[maxpax]    = F.MaximumPassenger, 
		[homebase]   = AH.IcaoID, 
		[maxfuel]    = F.MaximumFuel,
		[minfuel]    = F.MinimumFuel,
		[bow]        = F.BasicOperatingWeight, 
        [minrunway] = F.MimimumRunway,  
        [inactive]   = F.IsInActive,
		[display31]  = F.IsDisplay31,
        [multctrl]  = F.SIFLMultiControlled,
		[multnctrl] = F.SIFLMultiNonControled, 
		[compcode]  = F.ComponentCD,
        [Client]     = C.ClientCD,
		[FleetType]  = F.FleetType, 
		[fltphone]   = F.FlightPhoneNum, 
		[Class]      = F.Class,
		[vendcode]   =  V.VendorCD,
		[cqyrmade]   = F.YearMade, 
		[cqextcolor] = F.ExteriorColor,
		[cqintcolor] = F.InteriorColor, 
		[cqafis]     = F.IsAFIS,
		[cquvdata]   = F.IsUWAData,		
		[lastuser]   = F.LastUpdUID,
        [LastUpdTS]  = F.LastUpdTS,
		[fpcruzsp]   = F.FlightPlanCruiseSpeed, 
		[fpmxfltlvl] = F.FlightPlanMaxFlightLevel,
		[maxtkoff]   = F.MaximumTakeOffWeight,
		[maxlnding]  = F.MaximumLandingWeight ,
		[maxzero]    = F.MaximumWeightZeroFuel,
		[TaxiFuel]   = F.TaxiFuel ,
		[multisec]  = F.MultiSec ,
		[custcolor]  = F.ForeGrndCustomColor ,
		[bcustcolor] = F.BackgroundCustomColor,
		[infltphone] = F.FlightPhoneIntlNum,
		[FleetSize]  = F.FleetSize , 
		[minday]     = F.MinimumDay,
		[istdcrw]    = F.StandardCrewIntl,          
        [dstdcrw]    = F.StandardCrewDOM,
		[far91]      = F.IsFAR91,
		[far135]     = F.IsFAR135,
		[mxcrew]     =  CW.CrewCD, 
		[econcode]   =  EC.EmergencyContactCD ,  
		[taxdlyadj]  = F.IsTaxDailyAdj,
		[isdeleted]  = F.IsDeleted,
		[taxlndfee]  = F.IsTaxLandingFee,
		[decalnmbr] = FP.BuyAircraftAdditionalFeeDOM, 
		[acmake]    = FP.AircraftMake, 
		[acmodel]   = FP.AircraftModel,
		[colors1]   = FP.AircraftColor1,
		[colors2]   = FP.AircraftColor2,
		[trim]       = FP.AircraftTrimColor,
		[ownerless] = FP.OwnerLesse,
		[Addr1]      = FP.Addr1,
        [Addr2]      = FP.Addr2, 
		[CityName]   = ISNULL(FP.CityName,' '),
		[StateName]  = FP.StateName,
		[zipcode]    = FP.PostalZipCD,
		[Country]    = CY.CountryCD, 
		[afhrs]     = FI.AirframeHrs,
		[afcycle]   = FI.AirframeCycle,
		[afasof]    = FI. AIrframeAsOfDT, 
		[en1hrs]    = FI.Engine1Hrs,
		[en1cycle]  = FI.Engine1Cycle,
		[en1asof]   = FI.Engine1AsOfDT,
		[en2hrs]    = FI.Engine2Hrs,
		[en2cycle]  = FI.Engine2Cycle,
		[en2asof]   = FI.Engine2AsOfDT,
		[en3hrs]    = FI.Engine3Hrs,
		[en3cycle]  = FI.Engine3Cycle,
		[en3asof]   = FI.Engine3AsOfDT,
		[en4hrs]    = FI.Engine4Hrs,
		[en4cycle]  = FI.Engine4Cycle, 
		[en4asof]   = FI.Engine4AsOfDT,
		[re1hrs]    = FI.Reverse1Hrs,
		[re1cycle]  = FI.Reverser1Cycle,
		[re1asof]   = FI.Reverser1AsOfDT,  
		[re2hrs]    = FI.Reverse2Hrs,
		[re2cycle]  = FI.Reverser2Cycle, 
		[re2asof]   = FI.Reverser2AsOfDT, 
		[re3hrs]    = FI.Reverse3Hrs,  
		[re3cycle]  = FI.Reverser3Cycle,
		[re3asof]   = FI.Reverser3AsOfDT,
		[re4hrs]    = FI.Reverse4Hrs,
		[re4cycle]  = FI.Reverser4Cycle,
		[re4asof]   = FI.Reverser4AsOfDT,
		[machfuel1hrs]  = FI.MACHFuel1Hrs,
		[machfuel2hrs]  = FI.MACHFuel2Hrs,
		[machfuel3hrs]  = FI.MACHFuel3Hrs,	
		[machfuel4hrs]  = FI.MACHFuel4Hrs,
		[machfuel5hrs]  = FI.MACHFuel5Hrs,
        [machfuel6hrs]  = FI.MACHFuel6Hrs,
		[machfuel7hrs]  = FI.MACHFuel7Hrs, 
		[machfuel8hrs]  = FI.MACHFuel8Hrs,
		[machfuel9hrs]  = FI.MACHFuel9Hrs,
		[machfuel10hrs] = FI.MACHFuel10Hrs,
		[machfuel11hrs] = FI.MACHFuel11Hrs,
		[machfuel12hrs] = FI.MACHFuel12Hrs,
		[machfuel13hrs] = FI.MACHFuel13Hrs,
		[machfuel14hrs] = FI.MACHFuel14Hrs,
		[machfuel15hrs] = FI.MACHFuel15Hrs,
		[lrcfuel1hrs]   = FI.LongRangeFuel1Hrs,  
		[lrcfuel2hrs]   = FI.LongRangeFuel2Hrs,
		[lrcfuel3hrs]   = FI.LongRangeFuel3Hrs,
		[lrcfuel4hrs]   = FI.LongRangeFuel4Hrs, 
		[lrcfuel5hrs]   = FI.LongRangeFuel5Hrs,
		[lrcfuel6hrs]   = FI.LongRangeFuel6Hrs, 
		[lrcfuel7hrs]   = FI.LongRangeFuel7Hrs,
		[lrcfuel8hrs]   = FI.LongRangeFuel8Hrs, 
		[lrcfuel9hrs]   = FI.LongRangeFuel9Hrs,
		[lrcfuel10hrs]  = FI.LongRangeFuel10Hrs, 
		[lrcfuel11hrs]  = FI.LongRangeFuel11Hrs, 
		[lrcfuel12hrs]  = FI.LongRangeFuel12Hrs,
		[lrcfuel13hrs]  = FI.LongRangeFuel13Hrs,
		[lrcfuel14hrs]  = FI.LongRangeFuel14Hrs,
		[lrcfuel15hrs]  = FI.LongRangeFuel15Hrs, 
		[hrcfuel1hrs]   = FI.HighSpeedFuel1Hrs,
		[hrcfuel2hrs]   = FI.HighSpeedFuel2Hrs,
		[hrcfuel3hrs]   = FI.HighSpeedFuel3Hrs,
		[hrcfuel4hrs]   = FI.HighSpeedFuel4Hrs,
		[hrcfuel5hrs]   = FI.HighSpeedFuel5Hrs,
		[hrcfuel6hrs]   = FI.HighSpeedFuel6Hrs,
		[hrcfuel7hrs]   = FI.HighSpeedFuel7Hrs,
		[hrcfuel8hrs]   = FI.HighSpeedFuel8Hrs,  
		[hrcfuel9hrs]   = FI.HighSpeedFuel9Hrs, 
		[hrcfuel10hrs]  = FI.HighSpeedFuel10Hrs,
		[hrcfuel11hrs]  = FI.HighSpeedFuel11Hrs,
		[hrcfuel12hrs]  = FI.HighSpeedFuel12Hrs,
		[hrcfuel13hrs]  = FI.HighSpeedFuel13Hrs,
		[hrcfuel14hrs]  = FI.HighSpeedFuel14Hrs,
		[hrcfuel15hrs]  = FI.HighSpeedFuel51Hrs,
		[powrset]       = A.PowerSetting,
		[compdesc]      = FC.FleetComponentDescription,
		[cmplastinspdt] = FC.LastInspectionDT,
		--[hdcflag]       = FC.IsHrsDaysCycles,
		[cmpinsphrs]    = ISNULL(FC .InspectionHrs,'0'),
		[cmpwarnhrs]    = ISNULL(FC .WarningHrs,'0'), 
		[infocode]     = FPI.FleetInfoCD, 
		[infodesc]     = FPI.FleetProfileAddInfDescription,
		--[infovalue]    = dbo.FlightPakDecrypt(IsNull(FPD.InformationValue,'')),	
		[infovalue]      = IsNull(FPD.InformationValue,''), -- This is no more an encrypted value, cahnge in requirements		 	 
		[ratebeg]        = FCR.BeginRateDT,
		[rateend]        = FCR.EndRateDT, 
		[chrgrate]      = ISNULL(FCR.ChargeRate,'0'),
		[chrgunit]      = FCR.ChargeUnit,
		--[compdesc]        ='',
		--[cmplastinspdt]   =CONVERT(date, NULL),
		[hdcflag]=(CASE WHEN FC.IsHrsDaysCycles = 'H' THEN 'HOURS'
                        WHEN FC.IsHrsDaysCycles = 'D' THEN 'DAYS'
                        WHEN FC.IsHrsDaysCycles= 'C' THEN 'CYCLES'
                        END),
		--[cmpinsphrs]    =  0,
		--[cmpwarnhrs]    = 0, 
		[rtyfixrf]       = A.IsFixedRotary,	
		--[infocode]       = '', 
		--[infodesc]       = '',
		--[infovalue]      = '',
		--[ratebeg]        = '',
		--[ratebeg]       = CONVERT(date, NULL), 
		--[rateend]        = CONVERT(date, NULL), 
		--[chrgrate]       = '0',
		--[chrgunit]       = '',
		[section]        = '0',
		[notes]= F.Notes	--Added for Mhtml
		--[infltphone] = F.FlightPhoneIntlNum--Added for Mhtml
    
		 FROM  Aircraft A
		   INNER JOIN Fleet F
				   ON A.AircraftID = F.AircraftID
		   LEFT OUTER JOIN FleetChargeRate FCR
				   ON F.FleetID = FCR.FleetID
		   LEFT OUTER JOIN FleetComponent FC
				   ON F.FleetID = FC.FleetID
		   LEFT OUTER JOIN FleetInformation FI
				   ON F.FleetID = FI.FleetID
		   LEFT OUTER JOIN FleetPair FP
				   ON F.FleetID = FP.FleetID
		   LEFT OUTER JOIN FleetProfileDefinition FPD 
				   ON F.FleetID = FPD.FleetID  
		   LEFT OUTER JOIN FleetProfileInformation FPI
				   ON FPD.FleetProfileInformationID = FPI.FleetProfileInformationID
		   LEFT OUTER JOIN Client C
				   ON F.ClientID = C.ClientID
		   LEFT JOIN EmergencyContact EC
				   ON EC.EmergencyContactID = F.EmergencyContactID
		   LEFT JOIN Country CY
				   ON FP.CountryID = CY.CountryID
		   LEFT JOIN Vendor V
				   ON F.VendorID = V.VendorID	
		   LEFT JOIN Crew CW
		           ON F.CrewID = CW.CrewID
		   LEFT OUTER JOIN Company CO ON F.HomebaseID = CO.HomebaseID
		   LEFT OUTER JOIN Airport AH ON CO.HomebaseAirportID = AH.AirportID	   
		 WHERE F.IsDeleted = 0 
		 AND F.CustomerID = @CUSTOMERID  
		 --AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )  
		 AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='' ))  TEMP


SELECT  * FROM #FleetTemp
 
      
IF OBJECT_ID('tempdb..#FleetTemp') IS NOT NULL
DROP TABLE #FleetTemp


END
  
-- EXEC spGetReportFleetProfileExportInformation 'supervisor_99', 'N46E'
 -- EXEC spGetReportFleetProfileExportInformation 'GGJ_2','N110UV'







GO


