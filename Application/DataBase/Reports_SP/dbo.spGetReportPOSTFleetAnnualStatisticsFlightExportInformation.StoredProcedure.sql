IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetAnnualStatisticsFlightExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightExportInformation]                
( @UserCD VARCHAR(30), --Mandatory                               
  @YEAR INT, -- Mandatory                    
  @TailNum VARCHAR(5000)='',                    
  @FleetGroupCD VARCHAR(5000)='',                    
  @DepartmentCD VARCHAR(5000)='',                    
  @DepartmentGroupCD VARCHAR(5000)='',                    
  @HomeBaseCD VARCHAR(5000)='',                    
  @FlightCatagoryCD VARCHAR(5000)='',                    
  @IsHomebase Bit=0,              
  @PrintType Char(1)='B'                 
 )             
 AS 
   SET NOCOUNT ON    
   BEGIN             
     
   DECLARE @DispName Varchar(30)='Aircraft Block Hours'
   Declare @PrintFlightHours INT  
   SET @PrintFlightHours=0   
   IF @PrintType ='H'  
   BEGIN  
     SET @PrintFlightHours=1  
     SET @DispName='Aircraft Flight Hours'
   END   
             
   DECLARE @ParameterDefinition AS NVARCHAR(400)                  
                
   Declare @WhereSql varchar(Max)                
   Declare @FromSql  Nvarchar(Max)              
   Declare @GrpBy varchar(Max)              
                 
   SET @GrpBy =' Group by ScheduleDTTMLocal, FlightCatagory.FlightCatagoryCD , FlightCatagory.FlightCatagoryDescription,F.TailNum,  
                  PostflightLeg.FlightHours , PostflightLeg.LegNUM, PostflightLeg.PassengerTotal , PostflightLeg.Distance ,F.MaximumPassenger  '            
   SET @WhereSql =' WHERE PostflightMain.CustomerID=' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
   + ' AND Year(PostflightLeg.ScheduleDTTMLocal)=@Year '                  
               
 IF @TailNum <> '' BEGIN             
 SET @WhereSql = @WhereSql + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';           
 END            
             
 IF @DepartmentCD  <> '' BEGIN             
 SET @WhereSql = @WhereSql + ' AND D.DepartmentCD IN (''' + REPLACE(CASE WHEN RIGHT(@DepartmentCD , 1) = ',' THEN LEFT(@DepartmentCD, LEN(@DepartmentCD) - 1) ELSE @DepartmentCD END, ',', ''', ''') + ''')';           
 END            

 IF @FlightCatagoryCD  <> '' BEGIN             
 SET @WhereSql = @WhereSql + ' AND FlightCatagory.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';                  
 END                  
 IF @HomeBaseCD  <> '' BEGIN             
 SET @WhereSql = @WhereSql + ' AND Airport.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBaseCD, 1) = ',' THEN LEFT(@HomeBaseCD, LEN(@HomeBaseCD) - 1) ELSE @HomeBaseCD END, ',', ''', ''') + ''')';           
 END              
 IF @IsHomebase = 1 BEGIN          
 SET @WhereSql = @WhereSql + ' AND PostflightMain.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));          
 END             
            
DECLARE @TempAFS TABLE              
(      
section INT,  
subsection INT DEFAULT 0,         
sectiondesc  VARCHAR(50),            
detldesc  VARCHAR(50),            
tail_nmbr VARCHAR(50) DEFAULT  '',            
cat_code VARCHAR(50) DEFAULT '',            
cat_desc VARCHAR(50) DEFAULT '',            
totcol VARCHAR(50) DEFAULT '',            
            
flthrs1 NUMERIC(9,2) DEFAULT 0,            
flthrs2 NUMERIC(9,2) DEFAULT 0,            
flthrs3 NUMERIC(9,2) DEFAULT 0,      
flthrs4 NUMERIC(9,2) DEFAULT 0,             
flthrs5 NUMERIC(9,2) DEFAULT 0,             
flthrs6 NUMERIC(9,2) DEFAULT 0,             
flthrs7 NUMERIC(9,2) DEFAULT 0,             
flthrs8 NUMERIC(9,2) DEFAULT 0,             
flthrs9 NUMERIC(9,2) DEFAULT 0,             
flthrs10 NUMERIC(9,2) DEFAULT 0,            
flthrs11 NUMERIC(9,2) DEFAULT 0,            
flthrs12 NUMERIC(9,2) DEFAULT 0,            
            
fltpct1 NUMERIC(9,2) DEFAULT 0,             
fltpct2 NUMERIC(9,2) DEFAULT 0,             
fltpct3 NUMERIC(9,2) DEFAULT 0,             
fltpct4 NUMERIC(9,2) DEFAULT 0,             
fltpct5 NUMERIC(9,2) DEFAULT 0,             
fltpct6 NUMERIC(9,2) DEFAULT 0,             
fltpct7 NUMERIC(9,2) DEFAULT 0,             
fltpct8 NUMERIC(9,2) DEFAULT 0,             
fltpct9 NUMERIC(9,2) DEFAULT 0,             
fltpct10 NUMERIC(9,2) DEFAULT 0,            
fltpct11 NUMERIC(9,2) DEFAULT 0,            
fltpct12 NUMERIC(9,2) DEFAULT 0,            
            
tripdays1 INT DEFAULT 0,            
tripdays2 INT DEFAULT 0,            
tripdays3 INT DEFAULT 0,            
tripdays4 INT DEFAULT 0,            
tripdays5 INT DEFAULT 0,             
tripdays6 INT DEFAULT 0,            
tripdays7 INT DEFAULT 0,            
tripdays8 INT DEFAULT 0,            
tripdays9 INT DEFAULT 0,            
tripdays10 INT DEFAULT 0,            
tripdays11 INT DEFAULT 0,            
tripdays12 INT DEFAULT 0,            
             
legcnt1 INT DEFAULT 0,            
legcnt2 INT DEFAULT 0,             
legcnt3 INT DEFAULT 0,            
legcnt4 INT DEFAULT 0,            
legcnt5 INT DEFAULT 0,            
legcnt6 INT DEFAULT 0,            
legcnt7 INT DEFAULT 0,            
legcnt8 INT DEFAULT 0,            
legcnt9 INT DEFAULT 0,            
legcnt10 INT DEFAULT 0,            
legcnt11 INT DEFAULT 0,            
legcnt12 INT DEFAULT 0,            
            
paxcnt1 INT DEFAULT 0,            
paxcnt2 INT DEFAULT 0,             
paxcnt3 INT DEFAULT 0,            
paxcnt4 INT DEFAULT 0,            
paxcnt5 INT DEFAULT 0,            
paxcnt6 INT DEFAULT 0,            
paxcnt7 INT DEFAULT 0,            
paxcnt8 INT DEFAULT 0,            
paxcnt9 INT DEFAULT 0,            
paxcnt10 INT DEFAULT 0,            
paxcnt11 INT DEFAULT 0,             
paxcnt12 INT DEFAULT 0,            
            
nmiles1 INT DEFAULT 0,            
nmiles2 INT DEFAULT 0,            
nmiles3 INT DEFAULT 0,            
nmiles4 INT DEFAULT 0,             
nmiles5 INT DEFAULT 0,            
nmiles6 INT DEFAULT 0,            
nmiles7 INT DEFAULT 0,            
nmiles8 INT DEFAULT 0,            
nmiles9 INT DEFAULT 0,             
nmiles10 INT DEFAULT 0,            
nmiles11 INT DEFAULT 0,            
nmiles12 INT DEFAULT 0,            
            
load1 NUMERIC(9,2) DEFAULT 0,            
load2 NUMERIC(9,2) DEFAULT 0,             
load3 NUMERIC(9,2) DEFAULT 0,            
load4 NUMERIC(9,2) DEFAULT 0,            
load5 NUMERIC(9,2) DEFAULT 0,            
load6 NUMERIC(9,2) DEFAULT 0,            
load7 NUMERIC(9,2) DEFAULT 0,            
load8 NUMERIC(9,2) DEFAULT 0,             
load9 NUMERIC(9,2) DEFAULT 0,            
load10 NUMERIC(9,2) DEFAULT 0,             
load11 NUMERIC(9,2) DEFAULT 0,            
load12 NUMERIC(9,2) DEFAULT 0,            
            
dflthrs1 NUMERIC(9,2) DEFAULT 0,            
dflthrs2 NUMERIC(9,2) DEFAULT 0,            
dflthrs3 NUMERIC(9,2) DEFAULT 0,            
dflthrs4 NUMERIC(9,2) DEFAULT 0,            
dflthrs5 NUMERIC(9,2) DEFAULT 0,            
dflthrs6 NUMERIC(9,2) DEFAULT 0,             
dflthrs7 NUMERIC(9,2) DEFAULT 0,            
dflthrs8 NUMERIC(9,2) DEFAULT 0,            
dflthrs9 NUMERIC(9,2) DEFAULT 0,            
dflthrs10 NUMERIC(9,2) DEFAULT 0,            
dflthrs11 NUMERIC(9,2) DEFAULT 0,            
dflthrs12 NUMERIC(9,2) DEFAULT 0,            
            
iflthrs1 NUMERIC(9,2) DEFAULT 0,            
iflthrs2 NUMERIC(9,2) DEFAULT 0,            
iflthrs3 NUMERIC(9,2) DEFAULT 0,            
iflthrs4 NUMERIC(9,2) DEFAULT 0,            
iflthrs5 NUMERIC(9,2) DEFAULT 0,            
iflthrs6 NUMERIC(9,2) DEFAULT 0,            
iflthrs7 NUMERIC(9,2) DEFAULT 0,             
iflthrs8 NUMERIC(9,2) DEFAULT 0,            
iflthrs9 NUMERIC(9,2) DEFAULT 0,            
iflthrs10 NUMERIC(9,2) DEFAULT 0,            
iflthrs11 NUMERIC(9,2) DEFAULT 0,            
iflthrs12 NUMERIC(9,2) DEFAULT 0,             
            
dfltpct1 NUMERIC(9,2) DEFAULT 0,            
dfltpct2 NUMERIC(9,2) DEFAULT 0,            
dfltpct3 NUMERIC(9,2) DEFAULT 0,            
dfltpct4 NUMERIC(9,2) DEFAULT 0,            
dfltpct5 NUMERIC(9,2) DEFAULT 0,            
dfltpct6 NUMERIC(9,2) DEFAULT 0,            
dfltpct7 NUMERIC(9,2) DEFAULT 0,            
dfltpct8 NUMERIC(9,2) DEFAULT 0,            
dfltpct9 NUMERIC(9,2) DEFAULT 0,            
dfltpct10 NUMERIC(9,2) DEFAULT 0,            
dfltpct11 NUMERIC(9,2) DEFAULT 0,            
dfltpct12 NUMERIC(9,2) DEFAULT 0,            
            
ifltpct1 NUMERIC(9,2) DEFAULT 0,            
ifltpct2 NUMERIC(9,2) DEFAULT 0,             
ifltpct3 NUMERIC(9,2) DEFAULT 0,             
ifltpct4 NUMERIC(9,2) DEFAULT 0,            
ifltpct5 NUMERIC(9,2) DEFAULT 0,            
ifltpct6 NUMERIC(9,2) DEFAULT 0,            
ifltpct7 NUMERIC(9,2) DEFAULT 0,            
ifltpct8 NUMERIC(9,2) DEFAULT 0,            
ifltpct9 NUMERIC(9,2) DEFAULT 0,            
ifltpct10 NUMERIC(9,2) DEFAULT 0,            
ifltpct11 NUMERIC(9,2) DEFAULT 0,            
ifltpct12 NUMERIC(9,2) DEFAULT 0,            
            
cmon1 Varchar(15) DEFAULT 'Jan',            
cmon2 Varchar(15) DEFAULT 'Feb',            
cmon3 Varchar(15) DEFAULT 'Mar',             
cmon4 Varchar(15) DEFAULT 'Apr',            
cmon5 Varchar(15) DEFAULT 'May',            
cmon6 Varchar(15) DEFAULT 'Jun',            
cmon7 Varchar(15) DEFAULT 'Jul',            
cmon8 Varchar(15) DEFAULT 'Aug',            
cmon9 Varchar(15) DEFAULT 'Sep',            
cmon10 Varchar(15) DEFAULT 'Oct',            
cmon11 Varchar(15) DEFAULT 'Nov',            
cmon12 Varchar(15) DEFAULT 'Dec',            
            
lmon1 Bit DEFAULT 1,            
lmon2 Bit DEFAULT 1,            
lmon3 Bit DEFAULT 1,            
lmon4 Bit DEFAULT 1,            
lmon5 Bit DEFAULT 1,            
lmon6 Bit DEFAULT 1,            
lmon7 Bit DEFAULT 1,            
lmon8 Bit DEFAULT 1,            
lmon9 Bit DEFAULT 1,            
lmon10 Bit DEFAULT 1,            
lmon11 Bit DEFAULT 1,            
lmon12 Bit DEFAULT 1,            
            
flthrs13 NUMERIC(9,2) DEFAULT 0,            
fltpct13 NUMERIC(9,2) DEFAULT 0,            
tripdays13 NUMERIC(9,2) DEFAULT 0,            
legcnt13 NUMERIC(9,2) DEFAULT 0,            
paxcnt13 NUMERIC(9,2) DEFAULT 0,             
nmiles13 NUMERIC(9,2) DEFAULT 0,            
load13 NUMERIC(9,2) DEFAULT 0,            
dflthrs13 NUMERIC(9,2) DEFAULT 0,            
iflthrs13 NUMERIC(9,2) DEFAULT 0,            
dfltpct13 NUMERIC(9,2) DEFAULT 0,            
ifltpct13 NUMERIC(9,2) DEFAULT 0,            
            
activecol NUMERIC(9,2) DEFAULT 0            
)             
       IF OBJECT_ID('tempdb..#Tmp') is not null                
          DROP table #Tmp    
       IF OBJECT_ID('tempdb..#TmpCat') is not null                
          DROP table #TmpCat              
        IF OBJECT_ID('tempdb..#TmpTail') is not null                
          DROP table #TmpTail    
       Create Table #TmpTail(TailNum Varchar(10))        
       Create Table #TmpCat(FlightCatagoryCD Varchar(100))             
         
       Create Table #Tmp (MonthNo Int, TailNum Varchar(10),FlightCatagoryCD  Varchar(10),FlightCatagoryDescription Varchar(25), FlightHours  NUMERIC(9,2),            
         BlockHours  NUMERIC(9,2),TotDays  NUMERIC(9,2), TotLegs  NUMERIC(9,2), TotMiles  NUMERIC(9,2),       
         TotPassengers  NUMERIC(9,2), TotLoad  NUMERIC(9,2),Total Numeric(10,2))    
      
       SET @FromSql=' INSERT INTO #Tmp             
       (MonthNo,TailNum ,FlightCatagoryCD ,FlightCatagoryDescription,            
       FlightHours ,BlockHours,TotDays, TotLegs , TotMiles , TotPassengers , TotLoad)            
       SELECT MONTH(ScheduleDTTMLocal) As MonthNo,             
       F.TailNum ,               
       FlightCatagory.FlightCatagoryCD ,              
       FlightCatagory.FlightCatagoryDescription,             
       SUM(ROUND(PostflightLeg.FlightHours,1)) AS FlightHours,           
       SUM(ROUND(PostflightLeg.BlockHours,1)) AS BlockHours,            
       COUNT(PostflightLeg.ScheduleDTTMLocal) As TotDays,          
       COUNT(PostflightLeg.LegNUM) As TotLegs,                 
       sum(PostflightLeg.Distance) As TotMiles,  
       SUM(PostflightLeg.PassengerTotal) As TotPassengers,         
       SUM(PostflightLeg.PassengerTotal)/NullIF(F.MaximumPassenger,0) As TotLoad             
       FROM   (SELECT * FROM PostflightLeg WHERE IsDeleted = 0)PostflightLeg INNER JOIN   
              PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted = 0    
              INNER JOIN   
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID,F.MaximumPassenger        
                FROM Fleet F             
                LEFT OUTER JOIN FleetGroupOrder FGO            
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID            
                LEFT OUTER JOIN FleetGroup FG             
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
                WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''             
              ) F ON F.FleetID = PostflightMain.FleetID      
              LEFT JOIN       
              ( SELECT Distinct D.DepartmentID,D.DepartmentCD     
                FROM   Department D             
                LEFT OUTER JOIN DepartmentGroupOrder DGO            
                ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID            
                LEFT OUTER JOIN DepartmentGroup DG             
                ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID 
                WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupCD, '','')) OR @DepartmentGroupCD = ''''         
              ) D ON D.DepartmentID = PostflightMain.DepartmentID                
              LEFT OUTER JOIN   
              FlightCatagory ON PostflightLeg.FlightCategoryID = FlightCatagory.FlightCategoryID     
              LEFT JOIN
              Company ON PostflightMain.HomebaseID = Company.HomebaseID 
              LEFT JOIN
              Airport ON Company.HomebaseAirportID =  Airport.AirportID ' + @WhereSql + @GrpBy             
  
        SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @Year As Int, @TailNum As Varchar(5000), @FleetGroupCD As Varchar(5000),@DepartmentCD As Varchar(5000),@DepartmentGroupCD As Varchar(5000),          
        @HomeBaseCD As Varchar(5000),@FlightCatagoryCD As Varchar(5000), @HomeBaseCDOnly Bit'             
        EXECUTE sp_executesql @FromSql, @ParameterDefinition, @UserCD, @Year, @TailNum, @FleetGroupCD,@DepartmentCD,@DepartmentGroupCD,@HomeBaseCD,@FlightCatagoryCD,@IsHomebase                
        
        INSERT INTO #TmpTail
        SELECT DISTINCT TailNum  
        FROM   Fleet F LEFT OUTER JOIN FleetGroupOrder FGO              
               ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID LEFT OUTER JOIN FleetGroup FG               
               ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
        WHERE  (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
               (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
               F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
         
   
       Declare @MonSql nvarchar(Max)             
       Declare @RowIdStr char(1)              
       DECLARE @TailNm Varchar(20)
       DECLARE @RepResultCount Int=0   
       
       Declare @PartIRowCount Int=0 
       Set @PartIRowCount=(SELECT  COUNT(*) FROM #TmpTail)
       IF @PartIRowCount=0
       Begin
        INSERT INTO @TempAFS (section,sectiondesc,detldesc) Values(1,@DispName,'')
       End
       
       WHILE ( SELECT  COUNT(*) FROM #TmpTail ) > 0            
       BEGIN            
             Select  Top 1 @TailNm =TailNum From #TmpTail     
             
             SET @RepResultCount=0
             SET @RepResultCount=(Select Count(*) From #Tmp Where TailNum=@TailNm)
             INSERT INTO @TempAFS (section,sectiondesc,detldesc,tail_nmbr) Values(1,@DispName,@TailNm,@TailNm)
             
             BEGIN            
              
              IF @RepResultCount>0
              BEGIN
                  Update @TempAFS Set flthrs1=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                     From #Tmp Where MonthNo=1 and TailNum=@TailNm),         
                 legcnt1=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=1 and TailNum=@TailNm),            
                 paxcnt1=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=1 and TailNum=@TailNm),           
                 nmiles1=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=1 and TailNum=@TailNm)           
                 Where tail_nmbr=@TailNm  
                           
                Update @TempAFS Set flthrs2=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=2 and TailNum=@TailNm),          
                 legcnt2=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=2 and TailNum=@TailNm),            
                 paxcnt2=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=2 and TailNum=@TailNm),           
                 nmiles2=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=2 and TailNum=@TailNm)       
                 Where tail_nmbr=@TailNm  
                           
                 Update @TempAFS Set flthrs3=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=3 and TailNum=@TailNm),           
                 legcnt3=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=3 and TailNum=@TailNm),            
                 paxcnt3=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=3 and TailNum=@TailNm),            
                 nmiles3=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=3 and TailNum=@TailNm)           
                 Where tail_nmbr=@TailNm        
                     
                 Update @TempAFS Set flthrs4=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=4 and TailNum=@TailNm),      
                 legcnt4=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=4 and TailNum=@TailNm),            
                 paxcnt4=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=4 and TailNum=@TailNm),            
                 nmiles4=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=4 and TailNum=@TailNm)          
                 Where tail_nmbr=@TailNm            
                 
                 Update @TempAFS Set flthrs5=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=5 and TailNum=@TailNm),    
                 legcnt5=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=5 and TailNum=@TailNm),            
                 paxcnt5=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=5 and TailNum=@TailNm),            
                 nmiles5=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=5 and TailNum=@TailNm)       
                 Where tail_nmbr=@TailNm            
                  Update @TempAFS Set flthrs6=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=6 and TailNum=@TailNm),           
                 legcnt6=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=6 and TailNum=@TailNm),            
                 paxcnt6=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=6 and TailNum=@TailNm),            
                 nmiles6=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=6 and TailNum=@TailNm)            
                 Where tail_nmbr=@TailNm            
                 
                 Update @TempAFS Set flthrs7=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                    From #Tmp Where MonthNo=7 and TailNum=@TailNm),                            
                 legcnt7=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=7 and TailNum=@TailNm),       
                 paxcnt7=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=7 and TailNum=@TailNm),            
                 nmiles7=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=7 and TailNum=@TailNm)            
                 Where tail_nmbr=@TailNm    
                         
                 Update @TempAFS Set flthrs8=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=8 and TailNum=@TailNm),                      
                 legcnt8=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=8 and TailNum=@TailNm),            
                 paxcnt8=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=8 and TailNum=@TailNm),            
                 nmiles8=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=8 and TailNum=@TailNm)           
                 Where tail_nmbr=@TailNm    
                         
                 Update @TempAFS Set flthrs9=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=9 and TailNum=@TailNm),                        
                 legcnt9=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=9 and TailNum=@TailNm),            
                 paxcnt9=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=9 and TailNum=@TailNm),            
                 nmiles9=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=9 and TailNum=@TailNm)           
                 Where tail_nmbr=@TailNm    
                         
                 Update @TempAFS Set flthrs10=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=10 and TailNum=@TailNm),                      
                 legcnt10=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=10 and TailNum=@TailNm),            
                 paxcnt10=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=10 and TailNum=@TailNm),            
                 nmiles10=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=10 and TailNum=@TailNm)           
                 Where tail_nmbr=@TailNm            
                 
                 Update @TempAFS Set flthrs11=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End    
                      From #Tmp Where MonthNo=11 and TailNum=@TailNm),                       
                 legcnt11=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=11 and TailNum=@TailNm),            
                 paxcnt11=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=11 and TailNum=@TailNm),            
                 nmiles11=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=11 and TailNum=@TailNm)             
                 Where tail_nmbr=@TailNm            
                
                 Update @TempAFS Set flthrs12=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                      Else  IsNull(Sum(BlockHours),0) End      
                      From #Tmp Where MonthNo=12 and TailNum=@TailNm),                      
                 legcnt12=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=12 and TailNum=@TailNm),            
                 paxcnt12=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=12 and TailNum=@TailNm),            
                 nmiles12=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=12 and TailNum=@TailNm)             
                 Where tail_nmbr=@TailNm         
                 
                 Update @TempAFS Set flthrs13=(Select Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0)      
                 Else  IsNull(Sum(BlockHours),0) End      
                 From #Tmp Where TailNum=@TailNm),                     
                 legcnt13=(Select IsNull(Sum(TotLegs),0) From #Tmp Where TailNum=@TailNm),            
                 paxcnt13=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where TailNum=@TailNm),            
                 nmiles13=(Select IsNull(Sum(TotMiles),0) From #Tmp Where TailNum=@TailNm)            
                 Where tail_nmbr=@TailNm    
               END    
            END            
            Delete From #TmpTail Where TailNum = @TailNm             
       END            
       Delete From #TmpTail           
       -- Part I over         
       Declare @TotFound Int=0
       Declare @NetTotFound Int=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs1>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs2>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs3>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs4>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs5>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs6>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs7>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs8>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs9>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs10>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs11>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       Set @TotFound=0
       Set @TotFound=(Select Count(*) From @TempAFS Where flthrs12>0)
       IF @TotFound>0
       Begin
         Set @NetTotFound=@NetTotFound+1
       End
       
       Declare @FlightCatCD Varchar(100)
       INSERT INTO #TmpCat SELECT Distinct FlightCatagoryCD FROM #Tmp  Where FlightCatagoryCD is not null  
        
       Declare @PartIIRowCount Int=0 
       Set @PartIIRowCount=(SELECT COUNT(*) FROM #TmpCat)
       IF @PartIIRowCount=0
       Begin
        INSERT INTO @TempAFS (section,sectiondesc,detldesc) Values(2,'Flight Category Hours:','NO CATEGORY ACTIVITY')
        INSERT INTO @TempAFS (section,sectiondesc,detldesc) Values(3,'Flight Category Percentages:','NO CATEGORY ACTIVITY')
       End        
             
       WHILE ( SELECT  COUNT(*) FROM #TmpCat ) > 0            
       BEGIN            
             Select  Top 1 @FlightCatCD =FlightCatagoryCD From #TmpCat              
             
              INSERT INTO @TempAFS (section,sectiondesc,detldesc,cat_code)            
              SELECT TOP 1 2,'Flight Category Hours:',FlightCatagoryDescription,FlightCatagoryCD 
              FROM #Tmp            
              Where FlightCatagoryCD =@FlightCatCD      
           Delete From #TmpCat Where FlightCatagoryCD=@FlightCatCD             
       END   
       Delete From #TmpCat                         
     
       INSERT INTO #TmpCat SELECT Distinct FlightCatagoryCD FROM #Tmp  Where FlightCatagoryCD is not null 
       WHILE ( SELECT  COUNT(*) FROM #TmpCat ) > 0            
       BEGIN            
              Select  Top 1 @FlightCatCD=FlightCatagoryCD From #TmpCat                 
              BEGIN             
         
              Update @TempAFS Set
                -- flthrs1=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=1 and FlightCatagoryCD=@FlightCatCD And Section=2), 
                flthrs1=(Select 
                (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=1 and FlightCatagoryCD=@FlightCatCD And Section=2), 
                 legcnt1=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=1 and FlightCatagoryCD=@FlightCatCD And Section=2),                  
                 paxcnt1=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=1 and FlightCatagoryCD=@FlightCatCD And Section=2),                 
                 nmiles1=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=1 and FlightCatagoryCD=@FlightCatCD And Section=2)                
                 Where cat_code =@FlightCatCD            
              Update @TempAFS Set
                -- flthrs2=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=2 and FlightCatagoryCD=@FlightCatCD And Section=2),    
                 flthrs2=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=2 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt2=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=2 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt2=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=2 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles2=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=2 and FlightCatagoryCD=@FlightCatCD And Section=2)          
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set
              -- flthrs3=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=3 and FlightCatagoryCD=@FlightCatCD And Section=2),      
                 flthrs3=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=3 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt3=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=3 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt3=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=3 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles3=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=3 and FlightCatagoryCD=@FlightCatCD And Section=2)         
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set  
                 --flthrs4=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=4 and FlightCatagoryCD=@FlightCatCD And Section=2),
                 flthrs4=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=4 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt4=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=4 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt4=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=4 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles4=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=4 and FlightCatagoryCD=@FlightCatCD And Section=2)         
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set 
                 --flthrs5=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=5 and FlightCatagoryCD=@FlightCatCD And Section=2),     
                 flthrs5=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=5 and FlightCatagoryCD=@FlightCatCD And Section=2),          
                 legcnt5=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=5 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt5=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=5 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles5=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=5 and FlightCatagoryCD=@FlightCatCD And Section=2)          
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set
                 --flthrs6=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=6 and FlightCatagoryCD=@FlightCatCD And Section=2),        
                 flthrs6=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=6 and FlightCatagoryCD=@FlightCatCD And Section=2),          
                 legcnt6=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=6 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt6=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=6 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles6=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=6 and FlightCatagoryCD=@FlightCatCD And Section=2)        
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set 
                 --flthrs7=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=7 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs7=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=7 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt7=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=7 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt7=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=7 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles7=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=7 and FlightCatagoryCD=@FlightCatCD And Section=2)     
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set 
                 --flthrs8=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=8 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs8=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=8 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt8=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=8 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt8=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=8 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles8=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=8 and FlightCatagoryCD=@FlightCatCD And Section=2)             
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set  
                 --flthrs9=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=9 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs9=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=9 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt9=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=9 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt9=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=9 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles9=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=9 and FlightCatagoryCD=@FlightCatCD And Section=2)       
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set  
                 --flthrs10=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=10 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs10=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=10 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt10=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=10 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt10=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=10 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles10=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=10 and FlightCatagoryCD=@FlightCatCD And Section=2)     
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set 
                 --flthrs11=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=11 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs11=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=11 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt11=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=11 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt11=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=11 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles11=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=11 and FlightCatagoryCD=@FlightCatCD And Section=2)            
                 Where cat_code=@FlightCatCD            
              Update @TempAFS Set  
                 --flthrs12=(Select IsNull(Sum(FlightHours),0) From #Tmp Where MonthNo=12 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs12=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where MonthNo=12 and FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt12=(Select IsNull(Count(TotLegs),0) From #Tmp Where MonthNo=12 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt12=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where MonthNo=12 and FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles12=(Select IsNull(Sum(TotMiles),0) From #Tmp Where MonthNo=12 and FlightCatagoryCD=@FlightCatCD And Section=2)            
                 Where cat_code=@FlightCatCD         
              Update @TempAFS Set
                 --flthrs13=(Select IsNull(Sum(FlightHours),0) From #Tmp Where FlightCatagoryCD=@FlightCatCD And Section=2),           
                 flthrs13=(Select 
                 (Case @PrintFlightHours When 1 Then IsNull(Sum(FlightHours),0) Else  IsNull(Sum(BlockHours),0) End) 
							From #Tmp Where FlightCatagoryCD=@FlightCatCD And Section=2),           
                 legcnt13=(Select IsNull(Count(TotLegs),0) From #Tmp Where FlightCatagoryCD=@FlightCatCD And Section=2),            
                 paxcnt13=(Select IsNull(Sum(TotPassengers),0) From #Tmp Where FlightCatagoryCD=@FlightCatCD And Section=2),            
                 nmiles13=(Select IsNull(Sum(TotMiles),0) From #Tmp Where FlightCatagoryCD=@FlightCatCD And Section=2)     
                 Where cat_code=@FlightCatCD     
            END                
            Delete From #TmpCat Where FlightCatagoryCD=@FlightCatCD                  
       END            
       Delete From #TmpCat            
       --Part II over                 
        
       INSERT INTO #TmpCat SELECT Distinct FlightCatagoryCD FROM #Tmp Where FlightCatagoryCD is not null         
       WHILE ( SELECT  COUNT(*) FROM #TmpCat ) > 0            
       BEGIN            
             Select  Top 1 @FlightCatCD =FlightCatagoryCD From #TmpCat              
             
              INSERT INTO @TempAFS (section,sectiondesc,detldesc,cat_code)--,cat_desc)            
              SELECT TOP 1 3,'Flight Category Percentages:',FlightCatagoryDescription,FlightCatagoryCD--,FlightCatagoryDescription 
              FROM #Tmp Where FlightCatagoryCD =@FlightCatCD                    
              
             Delete From #TmpCat Where FlightCatagoryCD=@FlightCatCD             
       END            
       Delete From #TmpCat    
       
       Declare @FPct1 Numeric(7,2)=0
       Declare @FPct2 Numeric(7,2)=0
       Declare @FPct3 Numeric(7,2)=0
       Declare @FPct4 Numeric(7,2)=0
       Declare @FPct5 Numeric(7,2)=0
       Declare @FPct6 Numeric(7,2)=0
       Declare @FPct7 Numeric(7,2)=0
       Declare @FPct8 Numeric(7,2)=0
       Declare @FPct9 Numeric(7,2)=0
       Declare @FPct10 Numeric(7,2)=0
       Declare @FPct11 Numeric(7,2)=0
       Declare @FPct12 Numeric(7,2)=0
       Declare @FPct13 Numeric(7,2)=0
       Declare @FPctNet Numeric(7,2)=0
       
       Declare @Pct1 Numeric(7,2)=0
       Declare @Pct2 Numeric(7,2)=0
       Declare @Pct3 Numeric(7,2)=0
       Declare @Pct4 Numeric(7,2)=0
       Declare @Pct5 Numeric(7,2)=0
       Declare @Pct6 Numeric(7,2)=0
       Declare @Pct7 Numeric(7,2)=0
       Declare @Pct8 Numeric(7,2)=0
       Declare @Pct9 Numeric(7,2)=0
       Declare @Pct10 Numeric(7,2)=0
       Declare @Pct11 Numeric(7,2)=0
       Declare @Pct12 Numeric(7,2)=0
       Declare @Pct13 Numeric(7,2)=0
       
             Set @FPct1=0
			 Set @FPct2=0
			 Set @FPct3=0
			 Set @FPct4=0
			 Set @FPct5=0
			 Set @FPct6=0
			 Set @FPct7=0
			 Set @FPct8=0
			 Set @FPct9=0
			 Set @FPct10=0
			 Set @FPct11=0
			 Set @FPct12=0
			 
		     Select @FPct1=IsNull(Sum(flthrs1),0) From @TempAFS Where Section=2 
             Select @FPct2=IsNull(Sum(flthrs2),0) From @TempAFS Where Section=2 
             Select @FPct3=IsNull(Sum(flthrs3),0) From @TempAFS Where Section=2 
             Select @FPct4=IsNull(Sum(flthrs4),0) From @TempAFS Where Section=2 
             Select @FPct5=IsNull(Sum(flthrs5),0) From @TempAFS Where Section=2 
             Select @FPct6=IsNull(Sum(flthrs6),0) From @TempAFS Where Section=2 
             Select @FPct7=IsNull(Sum(flthrs7),0) From @TempAFS Where Section=2 
             Select @FPct8=IsNull(Sum(flthrs8),0) From @TempAFS Where Section=2 
             Select @FPct9=IsNull(Sum(flthrs9),0) From @TempAFS Where Section=2 
             Select @FPct10=IsNull(Sum(flthrs10),0) From @TempAFS Where Section=2 
             Select @FPct11=IsNull(Sum(flthrs11),0) From @TempAFS Where Section=2 
             Select @FPct12=IsNull(Sum(flthrs12),0) From @TempAFS Where Section=2  
       INSERT INTO #TmpCat SELECT Distinct FlightCatagoryCD FROM #Tmp Where FlightCatagoryCD is not null         
       WHILE ( SELECT  COUNT(*) FROM #TmpCat ) > 0            
       BEGIN            
             Select  Top 1 @FlightCatCD =FlightCatagoryCD From #TmpCat 
               
			 Set @Pct1=0
			 Set @Pct2=0
			 Set @Pct3=0
			 Set @Pct4=0
			 Set @Pct5=0
			 Set @Pct6=0
			 Set @Pct7=0
			 Set @Pct8=0
			 Set @Pct9=0
			 Set @Pct10=0
			 Set @Pct11=0
			 Set @Pct12=0
			 Set @Pct13=0 
             
             Select @Pct1=IsNull(flthrs1,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct2=IsNull(flthrs2,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct3=IsNull(flthrs3,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct4=IsNull(flthrs4,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct5=IsNull(flthrs5,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct6=IsNull(flthrs6,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct7=IsNull(flthrs7,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct8=IsNull(flthrs8,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct9=IsNull(flthrs9,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct10=IsNull(flthrs10,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct11=IsNull(flthrs11,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             Select @Pct12=IsNull(flthrs12,0) From @TempAFS Where Section=2 and cat_code=@FlightCatCD
             
             Set @FPctNet=0
             Set @FPctNet=@FPct1+@FPct2+@FPct3+@FPct4+@FPct5+@FPct6+@FPct7+@FPct8+@FPct9+@FPct10+@FPct11+@FPct12
             
               
             IF @FPctNet>0
             Begin
                  IF @FPct1>0
				  Begin
				    Update @TempAFS Set fltpct1=(@Pct1/@FPct1)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End   
				  IF @FPct2>0
				  Begin
					 Update @TempAFS Set fltpct2=(@Pct2/@FPct2)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End   
				  IF @FPct3>0
				  Begin  
					 Update @TempAFS Set fltpct3=(@Pct3/@FPct3)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End  
				  IF @FPct4>0
				  Begin
					Update @TempAFS Set fltpct4=(@Pct4/@FPct4)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End   
				  IF @FPct5>0
				  Begin
					Update @TempAFS Set fltpct5=(@Pct5/@FPct5)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End   
				  IF @FPct6>0
				  Begin
					 Update @TempAFS Set fltpct6=(@Pct6/@FPct6)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End   
				  IF @FPct7>0
				  Begin
					 Update @TempAFS Set fltpct7=(@Pct7/@FPct7)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End  
				  IF @FPct8>0
				  Begin
					 Update @TempAFS Set fltpct8=(@Pct8/@FPct8)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End  
				  IF @FPct9>0
				  Begin
					 Update @TempAFS Set fltpct9=(@Pct9/@FPct9)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End  
				  IF @FPct10>0
				  Begin
					 Update @TempAFS Set fltpct10=(@Pct10/@FPct10)*100 Where Section=3 And cat_code=@FlightCatCD  
				  End  
				  IF @FPct11>0
				  Begin
					 Update @TempAFS Set fltpct11=(@Pct11/@FPct11)*100 Where Section=3 And cat_code=@FlightCatCD 
				  End  
				  IF @FPct12>0
				  Begin
					 Update @TempAFS Set fltpct12=(@Pct12/@FPct12)*100 Where Section=3 And cat_code=@FlightCatCD 
				  End  
				End  
				
				Set @FPct13=0
			    Set @FPct13=(Select IsNull(fltpct1,0)+IsNull(fltpct2,0)+IsNull(fltpct3,0)+IsNull(fltpct4,0)+IsNull(fltpct5,0)+IsNull(fltpct6,0)+IsNull(fltpct7,0)+IsNull(fltpct8,0)+IsNull(fltpct9,0)+IsNull(fltpct10,0)+IsNull(fltpct11,0)+IsNull(fltpct12,0)From @TempAFS Where Section=3 And cat_code=@FlightCatCD )                
				IF @NetTotFound>0 
                Begin
                  Update @TempAFS Set fltpct13=@FPct13/@NetTotFound Where Section=3 And cat_code=@FlightCatCD 
                End				  
				
             Delete From #TmpCat Where FlightCatagoryCD=@FlightCatCD             
       END            
       Delete From #TmpCat   
       --Part III over   
      
       Declare @Leg1 Int=0
       Declare @Leg2 Int=0
       Declare @Leg3 Int=0
       Declare @Leg4 Int=0
       Declare @Leg5 Int=0
       Declare @Leg6 Int=0
       Declare @Leg7 Int=0
       Declare @Leg8 Int=0
       Declare @Leg9 Int=0
       Declare @Leg10 Int=0
       Declare @Leg11 Int=0
       Declare @Leg12 Int=0
       Declare @Leg13 Int=0
       
       Declare @Pax1 Int=0
       Declare @Pax2 Int=0
       Declare @Pax3 Int=0
       Declare @Pax4 Int=0
       Declare @Pax5 Int=0
       Declare @Pax6 Int=0
       Declare @Pax7 Int=0
       Declare @Pax8 Int=0
       Declare @Pax9 Int=0
       Declare @Pax10 Int=0
       Declare @Pax11 Int=0
       Declare @Pax12 Int=0
       Declare @Pax13 Int=0
       
       Declare @Load1 Numeric(10,2)=0
       Declare @Load2 Numeric(10,2)=0
       Declare @Load3 Numeric(10,2)=0
       Declare @Load4 Numeric(10,2)=0
       Declare @Load5 Numeric(10,2)=0
       Declare @Load6 Numeric(10,2)=0
       Declare @Load7 Numeric(10,2)=0
       Declare @Load8 Numeric(10,2)=0
       Declare @Load9 Numeric(10,2)=0
       Declare @Load10 Numeric(10,2)=0
       Declare @Load11 Numeric(10,2)=0
       Declare @Load12 Numeric(10,2)=0
       Declare @Load13 Numeric(10,2)=0
       
       
       IF OBJECT_ID('tempdb..#TmpTble') is not null                        
         DROP table #TmpTble 
       Create Table #TmpTble(ScheduleDTTMLocal DateTime,DutyType Int,TotalTripDays Int,TotalLegs Int,TotalPassengers Int,
                 TotalMilesFlown Int,TotalLoadFactor Numeric(10,2),TotalFlightHours Numeric(10,2),ScheduledTM DateTime,POLegID BigInt,POLogID BigInt,LoGNUM BigInt,FleetID BigInt)   
                              
       Declare @SQLSCRIPT NVarchar(4000)         
       SET @SQLSCRIPT ='INSERT INTO #TmpTble 
			  SELECT 
			  
              PostflightLeg.ScheduleDTTMLocal,
              PostflightLeg.DutyTYPE,   
              IsNull(Count(PostflightLeg.ScheduleDTTMLocal),0) As TotalTripDays,
              IsNull(Count(PostflightLeg.LegNUM),0) AS TotalLegs,         
              IsNull(Sum(PostflightLeg.PassengerTotal),0) As TotalPassengers,
              IsNull(Sum(PostflightLeg.Distance),0) As TotalMilesFlown,
              IsNull(Sum(PostflightLeg.PassengerTotal)/NULLIF(Sum(F.MaximumPassenger),0),0) As TotalLoadFactor,
              --IsNull(Sum(PostflightLeg.FlightHours),0) As TotalFlightHours
              Case @PrintFlightHours When 1 Then IsNull(Sum(ROUND(PostflightLeg.FlightHours,1)),0) Else  IsNull(Sum(ROUND(PostflightLeg.BlockHours,1)),0) End As TotalFlightHours,
                PostflightLeg.ScheduledTM,
                PostflightLeg.POLegID,
              PostflightMain.POLogID,
              PostflightMain.LoGNUM,
              F.FleetID
              FROM   PostflightLeg INNER JOIN   
              PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted = 0    
              INNER JOIN   
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID,F.MaximumPassenger            
                FROM   Fleet F             
                LEFT OUTER JOIN FleetGroupOrder FGO            
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID            
                LEFT OUTER JOIN FleetGroup FG             
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
                WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''             
              ) F ON F.FleetID = PostflightMain.FleetID      
              LEFT JOIN       
              ( SELECT Distinct D.DepartmentID,D.DepartmentCD     
                FROM   Department D             
                LEFT OUTER JOIN DepartmentGroupOrder DGO            
                ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID            
                LEFT OUTER JOIN DepartmentGroup DG             
                ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID 
                WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupCD, '','')) OR @DepartmentGroupCD = ''''         
              ) D ON D.DepartmentID = PostflightMain.DepartmentID                
              LEFT JOIN   
              FlightCatagory ON PostflightLeg.FlightCategoryID = FlightCatagory.FlightCategoryID     
              LEFT JOIN
              Company ON PostflightMain.HomebaseID = Company.HomebaseID 
              LEFT JOIN
              Airport ON Company.HomebaseAirportID =  Airport.AirportID             
              WHERE PostflightMain.CustomerID= ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
              + ' AND Year(PostflightLeg.ScheduleDTTMLocal)=@Year  AND PostflightLeg.ISDELETED = 0'   
         
  IF @TailNum <> '' BEGIN               
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''') '         
  END              
             
  IF @DepartmentCD  <> '' BEGIN               
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND D.DepartmentCD IN (''' + REPLACE(CASE WHEN RIGHT(@DepartmentCD , 1) = ',' THEN LEFT(@DepartmentCD, LEN(@DepartmentCD) - 1) ELSE @DepartmentCD END, ',', ''', ''') + ''') '         
  END              
                 
  IF @FlightCatagoryCD  <> '' BEGIN    
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND FlightCatagory.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''') '              
   
  END   
  IF @HomeBaseCD  <> '' BEGIN               
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND Airport.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBaseCD, 1) = ',' THEN LEFT(@HomeBaseCD, LEN(@HomeBaseCD) - 1) ELSE @HomeBaseCD END, ',', ''', ''') + ''') '        
  END                
  IF @IsHomebase = 1 BEGIN            
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PostflightMain.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))           
  END               
  SET @SQLSCRIPT = @SQLSCRIPT + ' GROUP BY PostflightLeg.ScheduleDTTMLocal, PostflightLeg.LegNUM, 
  PostflightLeg.PassengerTotal,PostflightLeg.Distance,PostflightLeg.PassengerTotal,PostflightLeg.FlightHours,PostflightLeg.DutyTYPE,PostflightLeg.ScheduledTM,
                PostflightLeg.POLegID,PostflightMain.POLogID,PostflightMain.LoGNUM,F.FleetID '
  
  SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @Year As Int, @TailNum As VARCHAR(5000), @FleetGroupCD As VARCHAR(5000),                
  @DepartmentCD As VARCHAR(5000),@DepartmentGroupCD As VARCHAR(5000),@HomeBaseCD aS VARCHAR(5000),                
  @FlightCatagoryCD As VARCHAR(5000),@HomeBaseCDOnly Bit,@PrintFlightHours INT'             
              
  EXECUTE sp_executesql @SQLSCRIPT , @ParameterDefinition, @UserCD, @Year, @TailNum, @FleetGroupCD,@DepartmentCD,@DepartmentGroupCD,@HomeBaseCD,@FlightCatagoryCD,@IsHomebase,@PrintFlightHours    
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,1, 'Additional Statistics:','Trip Days')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,2, 'Additional Statistics:','Segments/Legs')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,3, 'Additional Statistics:','Passengers')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,4, 'Additional Statistics:','Miles Flown')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,5, 'Additional Statistics:','Avg Load Factor')
  
  IF @PartIRowCount>0 OR @PartIIRowCount>0
  Begin
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,6, 'Additional Statistics:','Domestic Flt Hrs')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,7, 'Additional Statistics:','Domestic Flt Hrs %')

  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,8, 'Additional Statistics:','Intl Flt Hrs')
  INSERT INTO @TempAFS (section,subsection,sectiondesc,detldesc )Values(4,9, 'Additional Statistics:','Intl Flt Hrs %')
  End
  
---------------------------------------Trip Days------------------------------------------------------------------
    --Set @Jan1=0
    --Set @Feb1=0
    --Set @Mar1=0
    --Set @Apr1=0
    --Set @May1=0
    --Set @Jun1=0
    --Set @Jul1=0
    --Set @Aug1=0
    --Set @Sep1=0
    --Set @Oct1=0
    --Set @Nov1=0
    --Set @Dec1=0 
    --Set @YTD1=0 
    
    --Set @Jan2=0
    --Set @Feb2=0
    --Set @Mar2=0
    --Set @Apr2=0
    --Set @May2=0
    --Set @Jun2=0
    --Set @Jul2=0
    --Set @Aug2=0
    --Set @Sep2=0
    --Set @Oct2=0
    --Set @Nov2=0
    --Set @Dec2=0 
    --Set @YTD2=0 
    
    DECLARE @DepDate DateTIme
    DECLARE @ArrDate DateTime
    DECLARE @FleetID BigInt
    DECLARE @LogNum BigInt
    DECLARE @TempLog TABLE(LogNUM BigInt) 
    DECLARE @TripDays INT
    DECLARE @TotTripDays INT=0
    DECLARE @TempDate TABLE(FleetDate varchar(30),TripDate DATETIME) 
    
    DECLARE @TempFLT TABLE(FleetID BigInt) 
    Insert Into @TempFLT Select Distinct FleetID From #TmpTble 
    WHILE ( SELECT  COUNT(*) FROM @TempFLT ) > 0             
    BEGIN        
    Select Top 1 @FleetID=FleetID From @TempFLT
    
    Delete From @TempLog 
    Insert Into @TempLog Select Distinct LoGNUM From #TmpTble Where FleetID=@FleetID 
    ---
      WHILE ( SELECT  COUNT(*) FROM @TempLog  ) > 0             
      BEGIN        
      Select Top 1 @LogNum=LogNum From @TempLog  
      ---
      
      Select  @DepDate=MIN(ScheduledTM),@ArrDate=MAX(ScheduledTM) From #TmpTble Where FleetID=@FleetID And LogNum=@LogNum ; 
      
      WITH date_range (calc_date) AS (
      SELECT DATEADD(DAY, DATEDIFF(DAY, 0, @ArrDate) - DATEDIFF(DAY, @DepDate, @ArrDate), 0)
      UNION ALL SELECT DATEADD(DAY, 1, calc_date)  FROM date_range WHERE DATEADD(DAY, 1, calc_date) <= @ArrDate)
      Insert Into @TempDate Select convert(varchar(25),@FleetID)+convert(varchar(10),calc_date,101),calc_date FROM date_range;
    
  --  select CONVERT(varchar(10),getdate(),101)
       --
       
       Delete From @TempLog Where LogNum=@LogNum
      END      
    -- 
    Delete From @TempFLT Where FleetID=@FleetID
    END
    
    --Set @Jan1= (Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=1)   
    --Set @Feb1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=2)   
    --Set @Mar1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=3)   
    --Set @Apr1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=4)   
    --Set @May1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=5)   
    --Set @Jun1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=6)   
    --Set @Jul1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=7)   
    --Set @Aug1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=8)   
    --Set @Sep1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=9)   
    --Set @Oct1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=10)   
    --Set @Nov1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=11)   
    --Set @Dec1 =(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=12)   
    --Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
    
    --Update #TmpRep Set Jan1=@Jan1,Feb1=@Feb1,Mar1=@Mar1,Apr1=@Apr1,May1=@May1,Jun1=@Jun1,
    --                   Jul1=@Jul1,Aug1=@Aug1,Sep1=@Sep1,Oct1=@Oct1,Nov1=@Nov1,Dec1=@Dec1,YTD1=@YTD1 Where SerNo=1

----------------------------------Trip Days end--------------------------------------------------------------------
    
    
    
                           
  Update @TempAFS Set tripdays1=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=1) Where Section=4 And detldesc='Trip Days'          
  Update @TempAFS Set tripdays2=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=2) Where Section=4 And detldesc='Trip Days'      
  Update @TempAFS Set tripdays3=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=3) Where Section=4 And detldesc='Trip Days'     
  Update @TempAFS Set tripdays4=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=4) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays5=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=5) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays6=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=6) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays7=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=7) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays8=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=8) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays9=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=9) Where Section=4 And detldesc='Trip Days' 
  Update @TempAFS Set tripdays10=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=10) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays11=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=11) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays12=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate Where MONTH(TripDate)=12) Where Section=4 And detldesc='Trip Days'
  Update @TempAFS Set tripdays13=(Select IsNull(Count(Distinct (FleetDate)),0) From @TempDate) Where Section=4 And detldesc='Trip Days'
   
  Set @Leg1=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
  Set @Leg2=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
  Set @Leg3=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
  Set @Leg4=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
  Set @Leg5=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
  Set @Leg6=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
  Set @Leg7=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
  Set @Leg8=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
  Set @Leg9=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
  Set @Leg10=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
  Set @Leg11=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
  Set @Leg12=(Select IsNull(Sum(TotalLegs),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)
  Set @Leg13=(Select IsNull(Sum(TotalLegs),0) From #TmpTble)
   
  Update @TempAFS Set legcnt1=@Leg1,legcnt2=@Leg2,legcnt3=@Leg3,legcnt4=@Leg4,legcnt5=@Leg5,legcnt6=@Leg6, legcnt7=@Leg7,
                      legcnt8=@Leg8,legcnt9=@Leg9,legcnt10=@Leg10,legcnt11=@Leg11,legcnt12=@Leg12,legcnt13=@Leg13  Where Section=4 And detldesc='Segments/Legs'            
   
  Set @Pax1=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1)
  Set @Pax2=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2)
  Set @Pax3=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3)
  Set @Pax4=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4)
  Set @Pax5=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5)
  Set @Pax6=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6)
  Set @Pax7=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7)
  Set @Pax8=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8)
  Set @Pax9=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9)
  Set @Pax10=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10)
  Set @Pax11=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11)
  Set @Pax12=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12)
  Set @Pax13=(Select IsNull(Sum(TotalPassengers),0) From #TmpTble)
   
  Update @TempAFS Set paxcnt1=@Pax1,paxcnt2=@Pax2,paxcnt3=@Pax3,paxcnt4=@Pax4,paxcnt5=@Pax5,paxcnt6=@Pax6, paxcnt7=@Pax7,
                      paxcnt8=@Pax8,paxcnt9=@Pax9,paxcnt10=@Pax10,paxcnt11=@Pax11,paxcnt12=@Pax12,paxcnt13=@Pax13  Where Section=4 And detldesc='Passengers'     
       
  Update @TempAFS Set nmiles1=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1) Where Section=4 And detldesc='Miles Flown'            
  Update @TempAFS Set nmiles2=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2) Where Section=4 And detldesc='Miles Flown'       
  Update @TempAFS Set nmiles3=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles4=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles5=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles6=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles7=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles8=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles9=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles10=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles11=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11) Where Section=4 And detldesc='Miles Flown'      
  Update @TempAFS Set nmiles12=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12) Where Section=4 And detldesc='Miles Flown'        
  Update @TempAFS Set nmiles13=(Select IsNull(Sum(TotalMilesFlown),0) From #TmpTble) Where Section=4 And detldesc='Miles Flown'   
  
  --IF @Pax1 >0
  --Begin
  --   Set @Load1=@Leg1/@Pax1 
  --End  
  --IF @Pax2 >0
  --Begin
  --   Set @Load2=@Leg2/@Pax2
  --End     
  --IF @Pax3 >0
  --Begin
  --   Set @Load3=@Pax3/@Leg3
  --End     
  --IF @Pax4 >0
  --Begin
  --   Set @Load4=@Leg4/@Pax4 
  --End    
  --IF @Pax1 >0
  --Begin
  --   Set @Load1=@Leg1/@Pax1 
  --End     
  --IF @Pax5 >0
  --Begin
  --   Set @Load5=@Leg5/@Pax5
  --End     
  --IF @Pax6 >0
  --Begin
  --   Set @Load6=@Leg6/@Pax6
  --End     
  --IF @Pax7 >0
  --Begin
  --   Set @Load7=@Leg7/@Pax7 
  --End     
  --IF @Pax8 >0
  --Begin
  --   Set @Load8=@Leg8/@Pax8
  --End     
  --IF @Pax9 >0
  --Begin
  --   Set @Load9=@Leg9/@Pax9 
  --End     
  --IF @Pax1 >0
  --Begin
  --   Set @Load1=@Leg1/@Pax1 
  --End     
  --IF @Pax10 >0
  --Begin
  --   Set @Load10=@Leg10/@Pax10 
  --End     
  --IF @Pax11 >0
  --Begin
  --   Set @Load11=@Leg11/@Pax11 
  --End                                 
  --IF @Pax12 >0
  --Begin
  --   Set @Load12=@Leg12/@Pax12
  --End  
  --IF @Pax11 >0
  --Begin
  --   Set @Load11=@Leg11/@Pax11 
  --End
  --IF @Pax12 >0
  --Begin
  --   Set @Load12=@Leg12/@Pax12 
  --End
  --  IF @Pax13 >0
  --Begin
  --   Set @Load13=@Leg13/@Pax13
  --End
IF @Leg1 >0
Begin
Set @Load1=(CONVERT(NUMERIC(9,2),@Pax1)/CONVERT(NUMERIC(9,2),@Leg1))
End
IF @Leg2 >0
Begin
Set @Load2=(CONVERT(NUMERIC(9,2),@Pax2)/CONVERT(NUMERIC(9,2),@Leg2))
End
IF @Leg3 >0
Begin
Set @Load3=(CONVERT(NUMERIC(9,2),@Pax3)/CONVERT(NUMERIC(9,2),@Leg3))
End
IF @Leg4 >0
Begin
Set @Load4=(CONVERT(NUMERIC(9,2),@Pax4)/CONVERT(NUMERIC(9,2),@Leg4))
End
IF @Leg5 >0
Begin
Set @Load5=(CONVERT(NUMERIC(9,2),@Pax5)/CONVERT(NUMERIC(9,2),@Leg5))
End
IF @Leg6 >0
Begin
Set @Load6=(CONVERT(NUMERIC(9,2),@Pax6)/CONVERT(NUMERIC(9,2),@Leg6))
End
IF @Leg7 >0
Begin
Set @Load7=(CONVERT(NUMERIC(9,2),@Pax7)/CONVERT(NUMERIC(9,2),@Leg7))
End
IF @Leg8 >0
Begin
Set @Load8=(CONVERT(NUMERIC(9,2),@Pax8)/CONVERT(NUMERIC(9,2),@Leg8))
End
IF @Leg9 >0
Begin
Set @Load9=(CONVERT(NUMERIC(9,2),@Pax9)/CONVERT(NUMERIC(9,2),@Leg9))
End
IF @Leg10 >0
Begin
Set @Load10=(CONVERT(NUMERIC(9,2),@Pax10)/CONVERT(NUMERIC(9,2),@Leg10))
End
IF @Leg11 >0
Begin
Set @Load11=(CONVERT(NUMERIC(9,2),@Pax11)/CONVERT(NUMERIC(9,2),@Leg11))
End
IF @Leg12 >0
Begin
Set @Load12=(CONVERT(NUMERIC(9,2),@Pax12)/CONVERT(NUMERIC(9,2),@Leg12))
End
IF @Leg13 >0
Begin
Set @Load13=(CONVERT(NUMERIC(9,2),@Pax13)/CONVERT(NUMERIC(9,2),@Leg13))
End
 
  
  Update @TempAFS Set load1=@Load1,load2=@Load2,load3=@Load3,load4=@Load4,load5=@Load5,load6=@Load6, load7=@Load7,
                      load8=@Load8,load9=@Load9,load10=@Load10,load11=@Load11,load12=@Load12,load13=@Load13 Where Section=4 And detldesc='Avg Load Factor'     
  
  IF @PartIRowCount>0 OR @PartIIRowCount>0
  Begin     
  Update @TempAFS Set dflthrs1=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'            
  Update @TempAFS Set dflthrs2=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs3=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs4=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs5=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs6=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'                
  Update @TempAFS Set dflthrs7=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs8=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'                
  Update @TempAFS Set dflthrs9=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'                
  Update @TempAFS Set dflthrs10=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'                
  Update @TempAFS Set dflthrs11=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'                
  Update @TempAFS Set dflthrs12=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'          
  Update @TempAFS Set dflthrs13=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where DutyType=1) Where Section=4 And detldesc='Domestic Flt Hrs'     
  
  Declare @TotDomFltHrs Numeric(10,2)=0
  Set @TotDomFltHrs=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where DutyType=1)  
    
  Declare @Totdfltpct1 Numeric(10,2)=0
  Declare @Totdfltpct2 Numeric(10,2)=0
  Declare @Totdfltpct3 Numeric(10,2)=0
  Declare @Totdfltpct4 Numeric(10,2)=0
  Declare @Totdfltpct5 Numeric(10,2)=0
  Declare @Totdfltpct6 Numeric(10,2)=0
  Declare @Totdfltpct7 Numeric(10,2)=0
  Declare @Totdfltpct8 Numeric(10,2)=0
  Declare @Totdfltpct9 Numeric(10,2)=0
  Declare @Totdfltpct10 Numeric(10,2)=0
  Declare @Totdfltpct11 Numeric(10,2)=0
  Declare @Totdfltpct12 Numeric(10,2)=0
  Declare @Totdfltpct13 Numeric(10,2)=0
  
  Declare @Totifltpct1 Numeric(10,2)=0
  Declare @Totifltpct2 Numeric(10,2)=0
  Declare @Totifltpct3 Numeric(10,2)=0
  Declare @Totifltpct4 Numeric(10,2)=0
  Declare @Totifltpct5 Numeric(10,2)=0
  Declare @Totifltpct6 Numeric(10,2)=0
  Declare @Totifltpct7 Numeric(10,2)=0
  Declare @Totifltpct8 Numeric(10,2)=0
  Declare @Totifltpct9 Numeric(10,2)=0
  Declare @Totifltpct10 Numeric(10,2)=0
  Declare @Totifltpct11 Numeric(10,2)=0
  Declare @Totifltpct12 Numeric(10,2)=0
  Declare @Totifltpct13 Numeric(10,2)=0
  
  Set @Totdfltpct1=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=1)
  Set @Totdfltpct2=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=1)
  Set @Totdfltpct3=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=1)
  Set @Totdfltpct4=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=1)
  Set @Totdfltpct5=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=1)
  Set @Totdfltpct6=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=1)
  Set @Totdfltpct7=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=1)
  Set @Totdfltpct8=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=1)
  Set @Totdfltpct9=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=1)
  Set @Totdfltpct10=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=1)
  Set @Totdfltpct11=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=1)
  Set @Totdfltpct12=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=1)
  Set @Totdfltpct13=@Totdfltpct1+@Totdfltpct2+@Totdfltpct3+@Totdfltpct4+@Totdfltpct5+@Totdfltpct6+@Totdfltpct7+@Totdfltpct8+@Totdfltpct9+@Totdfltpct10
					+@Totdfltpct11+@Totdfltpct12
  
  Set @Totifltpct1=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=2)
  Set @Totifltpct2=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=2)
  Set @Totifltpct3=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=2)
  Set @Totifltpct4=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=2)
  Set @Totifltpct5=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=2)
  Set @Totifltpct6=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=2)
  Set @Totifltpct7=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=2)
  Set @Totifltpct8=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=2)
  Set @Totifltpct9=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=2)
  Set @Totifltpct10=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=2)
  Set @Totifltpct11=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=2)
  Set @Totifltpct12=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=2)
  Set @Totifltpct13=@Totifltpct1+@Totifltpct2+@Totifltpct3+@Totifltpct4+@Totifltpct5+@Totifltpct6+@Totifltpct7+@Totifltpct8+@Totifltpct9+@Totifltpct10
					+@Totifltpct11+@Totifltpct12
  
  IF (@Totdfltpct1+@Totifltpct1) >0
  Begin
  Update @TempAFS Set dfltpct1=((@Totdfltpct1/(@Totdfltpct1+@Totifltpct1)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %'     
  end
  IF (@Totdfltpct2+@Totifltpct2) >0
  Begin
  Update @TempAFS Set dfltpct2=((@Totdfltpct2/(@Totdfltpct2+@Totifltpct2)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct3+@Totifltpct3) >0
  Begin
  Update @TempAFS Set dfltpct3=((@Totdfltpct3/(@Totdfltpct3+@Totifltpct3)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct4+@Totifltpct4) >0
  Begin
  Update @TempAFS Set dfltpct4=((@Totdfltpct4/(@Totdfltpct4+@Totifltpct4)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct5+@Totifltpct5) >0
  Begin
  Update @TempAFS Set dfltpct5=((@Totdfltpct5/(@Totdfltpct5+@Totifltpct5)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct6+@Totifltpct6) >0
  Begin
  Update @TempAFS Set dfltpct6=((@Totdfltpct6/(@Totdfltpct6+@Totifltpct6)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct7+@Totifltpct7) >0
  Begin
  Update @TempAFS Set dfltpct7=((@Totdfltpct7/(@Totdfltpct7+@Totifltpct7)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %'    
  end
  IF (@Totdfltpct8+@Totifltpct8) >0
  Begin
  Update @TempAFS Set dfltpct8=((@Totdfltpct8/(@Totdfltpct8+@Totifltpct8)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct9+@Totifltpct9) >0
  Begin
  Update @TempAFS Set dfltpct9=((@Totdfltpct9/(@Totdfltpct9+@Totifltpct9)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct10+@Totifltpct10) >0
  Begin
  Update @TempAFS Set dfltpct10=((@Totdfltpct10/(@Totdfltpct10+@Totifltpct10)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct11+@Totifltpct11) >0
  Begin
  Update @TempAFS Set dfltpct11=((@Totdfltpct11/(@Totdfltpct11+@Totifltpct11)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct12+@Totifltpct12) >0
  Begin
  Update @TempAFS Set dfltpct12=((@Totdfltpct12/(@Totdfltpct12+@Totifltpct12)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %' 
  end
  IF (@Totdfltpct13+@Totifltpct13) >0
  Begin
  Update @TempAFS Set dfltpct13=((@Totdfltpct13/(@Totdfltpct13+@Totifltpct13)) * 100) Where Section=4 And detldesc='Domestic Flt Hrs %'   
  End         
 
  Update @TempAFS Set iflthrs1=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=1 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs2=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=2 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs3=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=3 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs4=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=4 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs5=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=5 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'         
  Update @TempAFS Set iflthrs6=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=6 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs7=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=7 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs8=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=8 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs9=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=9 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'       
  Update @TempAFS Set iflthrs10=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=10 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'          
  Update @TempAFS Set iflthrs11=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=11 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'         
  Update @TempAFS Set iflthrs12=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where MONTH(ScheduleDTTMLocal)=12 And DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'           
  Update @TempAFS Set iflthrs13=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where DutyType=2) Where Section=4 And detldesc='Intl Flt Hrs'      

  Set @TotDomFltHrs=0
  Set @TotDomFltHrs=(Select IsNull(Sum(TotalFlightHours),0) From #TmpTble Where DutyType=2)

  IF (@Totdfltpct1+@Totifltpct1) >0
  Begin
  Update @TempAFS Set ifltpct1=((@Totifltpct1/(@Totdfltpct1+@Totifltpct1)) * 100)  Where Section=4 And detldesc='Intl Flt Hrs %'
    end
  IF (@Totdfltpct2+@Totifltpct2) >0
  Begin
  Update @TempAFS Set ifltpct2=((@Totifltpct2/(@Totdfltpct2+@Totifltpct2)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct3+@Totifltpct3) >0
  Begin
  Update @TempAFS Set ifltpct3=((@Totifltpct3/(@Totdfltpct3+@Totifltpct3)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct4+@Totifltpct4) >0
  Begin
  Update @TempAFS Set ifltpct4=((@Totifltpct4/(@Totdfltpct4+@Totifltpct4)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct5+@Totifltpct5) >0
  Begin
  Update @TempAFS Set ifltpct5=((@Totifltpct5/(@Totdfltpct5+@Totifltpct5)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct6+@Totifltpct6) >0
  Begin
  Update @TempAFS Set ifltpct6=((@Totifltpct6/(@Totdfltpct6+@Totifltpct6)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct7+@Totifltpct7) >0
  Begin
  Update @TempAFS Set ifltpct7=((@Totifltpct7/(@Totdfltpct7+@Totifltpct7)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct8+@Totifltpct8) >0
  Begin
  Update @TempAFS Set ifltpct8=((@Totifltpct8/(@Totdfltpct8+@Totifltpct8)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct9+@Totifltpct9) >0
  Begin
  Update @TempAFS Set ifltpct9=((@Totifltpct9/(@Totdfltpct9+@Totifltpct9)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct10+@Totifltpct10) >0
  Begin
  Update @TempAFS Set ifltpct10=((@Totifltpct10/(@Totdfltpct10+@Totifltpct10)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct11+@Totifltpct11) >0
  Begin
  Update @TempAFS Set ifltpct11=((@Totifltpct11/(@Totdfltpct11+@Totifltpct11)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  end
  IF (@Totdfltpct12+@Totifltpct12) >0
  Begin
  Update @TempAFS Set ifltpct12=((@Totifltpct12/(@Totdfltpct12+@Totifltpct12)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %'  
  end
  IF (@Totdfltpct13+@Totifltpct13) >0
  Begin
  Update @TempAFS Set ifltpct13=((@Totifltpct13/(@Totdfltpct13+@Totifltpct13)) * 100) Where Section=4 And detldesc='Intl Flt Hrs %' 
  End  

  --Declare @NetFltPercHrs Numeric(10,2)=0 
  --Declare @NetFltDomPercHrs Numeric(10,2)=0 
  --Declare @NetFltIntPercHrs Numeric(10,2)=0           
  --Set @NetFltDomPercHrs=(Select IsNull(Sum(dfltpct1),0)+IsNull(Sum(dfltpct2),0)+IsNull(Sum(dfltpct3),0)+IsNull(Sum(dfltpct4),0)+IsNull(Sum(dfltpct5),0)+IsNull(Sum(dfltpct6),0)+
  --       IsNull(Sum(dfltpct7),0)+IsNull(Sum(dfltpct8),0)+IsNull(Sum(dfltpct9),0)+IsNull(Sum(dfltpct10),0)+IsNull(Sum(dfltpct11),0)+IsNull(Sum(dfltpct12),0)
  --       From @TempAFS Where Section=4 And detldesc In ('Domestic Flt Hrs %'))
  --Set @NetFltIntPercHrs=(Select IsNull(Sum(ifltpct1),0)+IsNull(Sum(ifltpct2),0)+IsNull(Sum(ifltpct3),0)+IsNull(Sum(ifltpct4),0)+IsNull(Sum(ifltpct5),0)+IsNull(Sum(ifltpct6),0)+
  --       IsNull(Sum(ifltpct7),0)+IsNull(Sum(ifltpct8),0)+IsNull(Sum(ifltpct9),0)+IsNull(Sum(ifltpct10),0)+IsNull(Sum(ifltpct11),0)+IsNull(Sum(ifltpct12),0) 
  --       From @TempAFS Where Section=4 And detldesc In ('Intl Flt Hrs %'))  
  --Set @NetFltPercHrs=@NetFltDomPercHrs+@NetFltIntPercHrs
  --IF @NetFltPercHrs >0
  --Begin
  --Update @TempAFS Set dfltpct13=(@NetFltDomPercHrs/@NetFltPercHrs * 100) Where Section=4 And detldesc='Domestic Flt Hrs %'              
  --Update @TempAFS Set ifltpct13=(@NetFltIntPercHrs/@NetFltPercHrs * 100) Where Section=4 And detldesc='Intl Flt Hrs %'
  --End
  End -- IF @PartIRowCount>0 OR @PartIIRowCount>0
   
  Update @TempAFS Set cat_code =TEMP.FlightCatagoryCD,cat_desc=TEMP.CategoryDesc FROM(
	SELECT F.TailNum,FC.FlightCatagoryCD,FC.FlightCatagoryDescription CategoryDesc,RANK() OVER(PARTITION BY F.TailNum ORDER BY PM.LogNum DESC, PL.LegNUM DESC) RNK
					 FROM PostflightMain PM
					 INNER JOIN Fleet F ON PM.FleetID = F.FleetID
					 INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
					 INNER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
					 WHERE PM.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
						AND Year(pl.ScheduleDTTMLocal)=@Year   
					-- ORDER BY PM.LogNum DESC, PL.LegNUM DESC
                 ) TEMP           
                 Where tail_nmbr=TEMP.TailNum  
                 AND TEMP.RNK=1 
                  
Update @TempAFS Set tail_nmbr = (
									SELECT  TOP 1 F.TailNum FROM PostflightMain PM
																 INNER JOIN Fleet F ON PM.FleetID = F.FleetID
																 INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
																 INNER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
															WHERE PM.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
															  AND Year(pl.ScheduleDTTMLocal)=@Year 
															 ORDER BY PM.LogNum DESC,PL.LegNUM DESC  
                                 ) 
			   WHERE section=2 
			   
                 
       Delete From #TmpTail             
       Delete From #Tmp  
       IF OBJECT_ID('tempdb..#TmpTble') is not null                
          DROP table #TmpTble                 
        -- final Out put            
        IF OBJECT_ID('tempdb..#TmpTail') is not null                
          DROP table #TmpTail                 
        IF OBJECT_ID('tempdb..#Tmp') is not null                
          DROP table #Tmp       
           IF OBJECT_ID('tempdb..#Tmp1') is not null                
          DROP table #Tmp1                 
       Select * From @TempAFS   
--Exec spGetReportPOSTFleetAnnualStatisticsFlightExportInformation 'SUPERVISOR_99',2009,'','','','','','',1,'B'     
END 

GO


