IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportCrewInformation]    
 @UserCD varchar(30),    
 @CrewCD char(7)    
AS    
-- =============================================    
-- SPC Name: spGetReportCrewInformation    
-- Author: SUDHAKAR J    
-- Create date: 14 Jun 2012    
-- Description: Get Crew Information for Report, Cre Roaster    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
    
SET NOCOUNT ON    
    
 SELECT    
  Crew.CustomerID    
  ,Crew.CrewCD    
  ,Crew.Addr1    
  ,Crew.LastName    
  ,Crew.Addr2    
  ,Crew.FirstName    
  ,Crew.CityName    
  ,Crew.MiddleInitial    
  ,Crew.StateName    
  ,Crew.PhoneNum    
  ,Crew.PostalZipCD    
  --,dbo.FlightPakDecrypt(ISNULL(Crew.SSN,'')) AS SSN    
  ,ISNULL(Crew.SSN,'') AS SSN    
  ,Crew.PagerNum    
  ,[HomeBase] = dbo.GetHomeBaseCDByHomeBaseID(Crew.HomeBaseID)    
  ,Crew.CellPhoneNum    
  ,IsStatus=(CASE WHEN Crew.IsStatus=1 THEN 'ACTIVE' ELSE 'INACTIVE' END)    
  ,Crew.FaxNum    
  ,Crew.EmailAddress    
  ,dbo.GetDateFormatByUserCD(@UserCD,Crew.BirthDT) AS BirthDT    
  ,Crew.PilotLicense1    
  ,dbo.GetDateFormatByUserCD(@UserCD,Crew.HireDT) AS HireDT    
  ,CONVERT(VARCHAR(MAX),Crew.LicenseType1) AS LicenseType1    
  ,dbo.GetDateFormatByUserCD(@UserCD,Crew.TerminationDT) AS TerminationDT    
  ,Crew.PilotLicense2    
  ,[CountryCD] = dbo.GetCountryCDByCountryID(Crew.CountryID)    
  ,CrewTypeDescription = Crew.CrewTypeCD    
  ,CONVERT(VARCHAR(MAX),Crew.LicenseType2) AS LicenseType2    
  ,D.DepartmentCD    
  ,CO.CountryCD AS Citizenship    
  ,Crew.Notes    
  ,Crew.Notes2    
  ,CL.ClientCD    
  ,LicenseCountry1 = Crew.LicenseCountry1    
  ,LicenseCountry2 = Crew.LicenseCountry2    
  ,[Addr3] = Crew.Addr3    
    
 FROM Crew     
 LEFT OUTER JOIN (    
   SELECT DepartmentID, CONVERT(VARCHAR(MAX),DepartmentCD) AS DepartmentCD FROM Department    
  ) D ON Crew.DepartmentID = D.DepartmentID    
 LEFT OUTER JOIN Country CO ON Crew.Citizenship = CO.CountryID    
 LEFT OUTER JOIN Client CL ON Crew.ClientID = CL.ClientID    
 WHERE Crew.IsDeleted = 0    
 AND Crew.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND Crew.CrewCD = RTRIM(@CrewCD)  
   
  -- EXEC spGetReportCrewInformation 'jwilliams_11', '1GV'  
  
GO


