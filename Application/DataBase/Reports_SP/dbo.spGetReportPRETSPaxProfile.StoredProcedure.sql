IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPaxProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPaxProfile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPaxProfile]
	@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
/*
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
*/	
AS
-- ================================================================================
-- SPC Name: spGetReportPRETSPaxProfile
-- Author: ABHISHEK.S
-- Create date: 10th September 2012
-- Description: Get TripSheetReportWriter Passenger Profile Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ================================================================================
BEGIN

DECLARE @BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD
		,@BeginDate,@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
		
	SET NOCOUNT ON;
	
	--SET @SQLSCRIPT = '
			SELECT	DISTINCT
			[TripN] = PM.TripNUM,
			[Desc] = PM.TripDescription,
			[TripDts] = dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD),
			[Requestor] = ISNULL(PR.LastName, '') + ', ' + ISNULL(PR.FirstName, '')+ ' ' + ISNULL(PR.MiddleInitial, ''),
			[TailN] = F.TailNum,
			[PhnNm] = PR.PhoneNum,
			[Type] = A.AircraftCD,
			[DispatchN] = PM.DispatchNUM,
			[TripID] = CONVERT(VARCHAR,PM.TripID),
			[PaxID] = CONVERT(VARCHAR,Main.PassengerID),
			[PaxCD] = PR1.PassengerRequestorCD,
			[PaxName] = PR1.LastName + ',' + PR1.FirstName,
			[AddlInfo] = PA.AddlInfo,				
			[Notes] = LEN(ISNULL(PR1.Notes,'')),
			[DispatcherName] = UM.FirstName + ' ' + UM.LastName, --PM.DsptnName,
			[DispatcherPhone] = UM.PhoneNum,
			[Dispatcheremail] = UM.EmailID
			
			FROM (SELECT DISTINCT TripID, LegID, PassengerID FROM  @tblTripInfo) Main 
				INNER JOIN PREFLIGHTMAIN PM ON Main.TripID = PM.TripID
				INNER JOIN  PreflightLeg PL ON Main.LegID = PL.LegID  
				INNER JOIN ( SELECT DISTINCT TailNum,FleetID FROM FLEET) F ON PM.FleetID = F.FleetID						
				INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
				INNER JOIN Passenger PR1 ON Main.PassengerID = PR1.PassengerRequestorID
				LEFT OUTER JOIN Passenger PR ON PM.PassengerRequestorID = PR.PassengerRequestorID	
				LEFT JOIN (SELECT PassengerRequestorID, AddlInfo = COUNT(1) FROM PassengerAdditionalInfo GROUP BY PassengerRequestorID) PA
				          ON PR.PassengerRequestorID = PA.PassengerRequestorID
				LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		    WHERE PM.IsDeleted = 0
	
			ORDER BY PR1.PassengerRequestorCD
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@TripNum AS NVARCHAR(100),@Passenger AS NVARCHAR(200)'
 --   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TripNum, @Passenger
   
-- EXEC spGetReportPRETSPaxProfile 'supervisor_99', '1992', '', ''
    
END






GO


