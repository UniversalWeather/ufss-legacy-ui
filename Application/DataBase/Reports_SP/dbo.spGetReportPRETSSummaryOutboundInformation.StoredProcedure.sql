IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSSummaryOutboundInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSSummaryOutboundInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSSummaryOutboundInformation]
	@UserCD VARCHAR(50)
	,@TripID VARCHAR(30)
	,@LegNum VARCHAR(150)	
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSSummaryOutboundInformation
-- Author: AISHWARYA.M
-- Create date: 12 Sep 2012
-- Description: Get Preflight TripSheet Summary Outbound information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SELECT DISTINCT 0 AS LegNUM, C.RptTabOutbdInstLab1,C.RptTabOutbdInstLab2,C.RptTabOutbdInstLab3,C.RptTabOutbdInstLab4
      ,C.RptTabOutbdInstLab5,C.RptTabOutbdInstLab6,C.RptTabOutbdInstLab7, '' AS FuelLoad
      FROM PreflightMain PM
      INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
      INNER JOIN PreflightTripOutbound PTO ON PL.LegID = PTO.LegID
      INNER JOIN Company C ON PM.HomebaseID = C.HomebaseID AND PM.CustomerID = C.CustomerID
      WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
      AND PM.TripID = @TripID
      AND PTO.IsDeleted = 0
     
UNION
 
SELECT PL.LegNUM, PIV.[1], PIV.[2], PIV.[3], PIV.[4], PIV.[5], PIV.[6], PIV.[7], [FuelLoad] = CONVERT(VARCHAR(10),PL.FuelLoad)
FROM PreflightMain PM
INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
INNER JOIN
(
      SELECT * FROM (
              SELECT LegID, OutboundInstructionNUM, OutboundDescription
              FROM PreflightTripOutbound
      ) up
      PIVOT (MAX(OutboundDescription) FOR OutboundInstructionNUM IN ([1],[2],[3],[4],[5],[6],[7])) AS pvt
) PIV ON PL.LegID = PIV.LegID
WHERE PM.TripID = @TripID
AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')

END

--EXEC spGetReportPRETSSummaryOutboundInformation 'TIM',100006941,''
--EXEC spGetReportPRETSSummaryOutboundInformation 'TIM', 10000350,''
--EXEC spGetReportPRETSSummaryOutboundInformation 'TIM', 10001119,''


GO


