IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vDeptAuthGroup]'))
DROP VIEW [dbo].[vDeptAuthGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vDeptAuthGroup]  AS 
SELECT D.DepartmentID,D.CustomerID,D.DepartmentCD,D.DepartmentName
,DG.DepartmentGroupID, DG.HomebaseID,DG.DepartmentGroupCD, DG.DepartmentGroupDescription
,DA.AuthorizationID,DA.AuthorizationCD,DA.DeptAuthDescription
FROM Department D 
LEFT OUTER JOIN DepartmentGroupOrder DGO
	 ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID=DGO.CustomerID
left JOIN Departmentgroup DG 
	ON DGO.DepartmentGroupID=DG.DepartmentGroupID AND DGO.CustomerID=DG.CustomerID
LEFT JOIN DepartmentAuthorization DA 
	ON D.DepartmentID = DA. DepartmentID AND D.CustomerID = DA.CustomerID and DA.isDeleted =0 
WHERE D.isDeleted = 0 --and D.CustomerID=10013
GO


