IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileInformationSub3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileInformationSub3]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFleetProfileInformationSub3]
	 @UserCD VARCHAR(30)
	,@TailNum char(6)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileInformationSub3
-- Author: Abhishek.s
-- Create date: 27 Sep 2012
-- Description: Get Fleet Profile Sub Report 3 Information
-- Revision History
-- Date			Name		Ver		Change
-- 27 Sep		Abhishek	1.1		SubRep3 for Fleet Profile
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID   BIGINT,
        @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SELECT DISTINCT	
         
         [Begin]      = FCR.BeginRateDT
        ,[End]        = FCR.EndRateDT
        ,[CRate]      = FCR.ChargeRate
        ,[Units]      = FCR.ChargeUnit
        
				
				
      FROM Fleet F 
INNER JOIN FleetChargeRate FCR
        ON F.FleetID = FCR.FleetID
	             
		WHERE F.IsDeleted = 0
	  AND F.CustomerID = @CUSTOMERID
	  AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	  AND F.TailNum = RTRIM(@TailNum)
	  
 -- EXEC spGetReportFleetProfileInformationSub3 'UC', 'BESVDZ'

GO


