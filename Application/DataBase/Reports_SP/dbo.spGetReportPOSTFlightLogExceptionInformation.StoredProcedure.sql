IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogExceptionInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogExceptionInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportPOSTFlightLogExceptionInformation] (@UserCD VARCHAR(30),@DATEFROM AS DATETIME,@DATETO AS DATETIME)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFlightLogExceptionInformation
-- Author: PremPrakash J
-- Create date: 31 Jul 2012
-- Description: Get FlightLog Exception Information REPORTS
--spGetReportPOSTFlightLogExceptionInformation 'MATT','10/1/2012','10/31/2012'
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
 
--SET @UserCD='UC'
--SET @DATEFROM ='08/01/2012'
--SET @DATETO = '08/31/2012'

BEGIN
/*
SELECT EXCEP.Date
       ,POLogID 
       ,EstDepartureDT
       ,[Log No.]
       ,[Tail No.]
       ,[Leg No.]
       ,[Exceptions/Dep]
       ,[Exceptions/Arr]
       ,[Exceptions/Scheduled]
       ,[Exceptions/Warning] 
               FROM(   
					SELECT   [Date]=dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
							 PM.POLogID,
							 PM.EstDepartureDT,
							 [Log No.]=PM.LogNum, 
							 [Tail No.]=F.TailNum,
							 [Leg No.]=PL.LegNUM,
							 [Exceptions/Dep]=D.IcaoID,
							 [Exceptions/Arr]=A.IcaoID,
							 [Exceptions/Scheduled]=PL.ScheduledTM,
							 [Exceptions/Warning]=ExceptionDescription,
							 ROW_NUMBER()OVER(PARTITION BY PM.POLogID ORDER BY PL.ScheduledTM,PM.POLogID,PL.LegNum)Rnk   
					  FROM   PostflightMain PM
							 INNER JOIN PostflightLeg PL  ON PM.POLogID=PL.POLogID
							 INNER JOIN  Fleet F ON F.FleetID=PM.FleetID
							 INNER JOIN Airport D ON D.AirportID = PL.DepartICAOID
							 INNER JOIN Airport A ON A.AirportID = PL.ArriveICAOID
							 INNER JOIN PostflightTripException PTE ON PTE.POLogID=PM.POLogID   
							 --INNER JOIN (SELECT DISTINCT PTE1.POLogID,Exception = (SELECT DISTINCT ExceptionDescription +'***'FROM PostflightTripException PTE 
								--															 WHERE PTE.POLogID=PTE1.POLogID 
								--															 FOR XML PATH(''))
								--													FROM PostflightTripException PTE1
								--										   )PTT ON PTT.POLogID=PM.POLogID       
					   WHERE PM.CustomerID =dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
						AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
                    )EXCEP
    ORDER BY [Exceptions/Scheduled],[Leg No.]
*/

SELECT EXCEP.[Date]
       ,POLogID
       ,EstDepartureDT
       ,[Log No.]
       ,[Tail No.]
       ,[Leg No.]
       ,[Exceptions/Dep]
       ,[Exceptions/Arr]
       ,[Exceptions/Scheduled]
       ,[Exceptions/Warning]
       ,PaxCrewException=CASE WHEN Rnk=1 THEN PaxCrewException ELSE '' END
       
  FROM 
 (
	 SELECT  [Date]=dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			 PM.POLogID,
			 PM.EstDepartureDT,
			 [Log No.]=PM.LogNum, 
			 [Tail No.]=F.TailNum,
			 [Leg No.]=PL.LegNUM,
			 [Exceptions/Dep]=D.IcaoID,
			 [Exceptions/Arr]=A.IcaoID,
			 [Exceptions/Scheduled]=PL.ScheduledTM,
			 [Exceptions/Warning]=Exception,
			 PaxCrewException,
			 ROW_NUMBER()OVER(PARTITION BY PM.POLogID ORDER BY PM.POLogID,PL.LegNUM DESC)Rnk
	  FROM   PostflightMain PM
			 INNER JOIN PostflightLeg PL  ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
			 INNER JOIN  Fleet F ON F.FleetID=PM.FleetID
			 INNER JOIN Airport D ON D.AirportID = PL.DepartICAOID
			 INNER JOIN Airport A ON A.AirportID = PL.ArriveICAOID 
			 INNER JOIN (SELECT DISTINCT PL1.POLogID,CONVERT(VARCHAR(2),LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(SUBSTRING(PL1.ExceptionDescription,4,3))),1,2)))) LegNUM, Exception = (SELECT 'Warning - '+ExceptionDescription +'***'
																	   FROM PostflightTripException PTE INNER JOIN PostflightLeg PL ON PTE.POLogID=PL.POLogID AND PL.IsDeleted=0
																	   WHERE PTE.POLogID=PL1.POLogID
																		 AND CONVERT(VARCHAR(2),PL.LegNUM)=CONVERT(VARCHAR(2),LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(SUBSTRING(PL1.ExceptionDescription,4,3))),1,2))))
																		 AND CONVERT(VARCHAR(2),LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(SUBSTRING(PTE.ExceptionDescription,4,3))),1,2))))=CONVERT(VARCHAR(2),PL.LegNUM)
																		 ORDER BY PL.LegNUM
																	 FOR XML PATH('')
																  )
													  FROM PostflightTripException PL1)PTT ON PTT.POLogID=PM.POLogID AND PTT.LegNUM =CONVERT(VARCHAR(2),PL.LegNUM)        
			LEFT OUTER JOIN (SELECT DISTINCT PTE1.POLogID,PaxCrewException = (SELECT DISTINCT ExceptionDescription +'***'FROM PostflightTripException PTE 
																								 WHERE PTE.POLogID=PTE1.POLogID 
																								   AND CONVERT(VARCHAR(3),LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(SUBSTRING(PTE.ExceptionDescription,1,3))),1,3))))<>'Leg'
																								 FOR XML PATH(''))
																						FROM PostflightTripException PTE1)PAXCWEXP ON PAXCWEXP.POLogID=PM.POLogID
	   WHERE PM.CustomerID =dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
	    AND PM.IsDeleted=0
		AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	
)Excep
ORDER BY  [Log No.],[Leg No.]

										


   

     --SELECT DISTINCT POLogID ,EstDepartureDT ,[Log No.] , [Tail No.] ,[Exceptions/Dep],[Exceptions/Arr], [Leg No.],[Exceptions/Scheduled],[Exceptions/Warning]   from  #tempException   
   
END
--EXEC spGetReportPOSTFlightLogExceptionInformation 'jwilliams_13','2000-01-01','2012-12-12'

GO


