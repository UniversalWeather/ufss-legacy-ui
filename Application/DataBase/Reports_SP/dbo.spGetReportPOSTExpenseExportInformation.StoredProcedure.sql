IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTExpenseExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTExpenseExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPOSTExpenseExportInformation]
    @UserCustomerID AS VARCHAR(30),
    @UserHomebaseID AS VARCHAR(30),
	@DATEFROM DATETIME, --MANDATORY  
    @DATETO DATETIME, --MANDATORY 
    @AccountNum AS VARCHAR(30)= '',
    @LogNum AS VARCHAR(30)= '',
    @SlipNUM AS VARCHAR(30)= '',
    @IsHomebase AS BIT = 0
AS

--EXEC spGetReportPOSTExpenseExportInformation '10099','','2017-12-09','2017-12-12','','','',0
BEGIN

SET NOCOUNT ON;
SELECT DISTINCT
       PE.SlipNUM AS slipnum,
       PM.LogNum AS lognum,
       PE.POLegID AS legid,
       PE.LegNUM AS leg_num,
       AC.AccountNum AS acctnum,
       PE.AccountPeriod AS acctperiod,
       CA.IcaoID AS Homebase,
       FL.TailNum AS tail_nmbr,
       PE.PurchaseDT AS purchasedt,
       AP.IcaoID AS icao_id,
       F.FBOCD AS fbo_code,
       FLC.FuelLocatorCD AS fuel_loc,
       PT.PaymentTypeCD AS pay_type,
       V.VendorCD AS pay_vendor,
       CR.CrewCD AS crewcode,
       FC.FlightCatagoryCD AS cat_code,
       PE.DispatchNUM AS dispatchno,
       PE.InvoiceNUM AS invnum,
       PE.ExpenseAMT AS expamt,
       PE.FuelPurchase AS fuelpurch,
       PE.FuelQTY AS fuelqty,
       PE.UnitPrice AS unitprice,
       PE.FederalTAX AS fedtax,
       PE.SateTAX AS statetax,
       PE.SaleTAX AS salestax,
       PE.IsAutomaticCalculation AS autocalc,
       PE.IsBilling AS billing,
       PE.LastUpdUID AS lastuser,
       PE.LastUpdTS AS lastupdt,
       PE.PostEDPR AS postedpr,
       PE.PostFuelPrice AS postfuelprice,
       PE.IsDeleted AS isdeleted,
       AC.AccountDescription AS acctdesc,
       F.FBOVendor AS fboname,
       PN.Notes AS Notes    

FROM PostflightMain PM JOIN PostflightExpense PE ON PM.POLogID = PE.POLogID AND PE.IsDeleted=0 
					   JOIN PostflightLeg PL ON PL.POLegID = PE.POLegID AND PL.IsDeleted=0
				       LEFT JOIN Fleet FL ON FL.FleetID = PE.FleetID  
					   LEFT JOIN Account AC ON PE.AccountID = AC.AccountID
				       LEFT JOIN FBO F ON PE.FBOID = F.FBOID 
                       LEFT JOIN Company C ON PE.HomebaseID = C.HomebaseID AND PE.CustomerID = C.CustomerID
                       LEFT JOIN Airport CA ON CA.AirportID=C.HomebaseAirportID
                       LEFT JOIN Airport AP ON PE.AirportID = AP.AirportID
                       LEFT JOIN FuelLocator FLC ON PE.FuelLocatorID = FLC.FuelLocatorID
                       LEFT JOIN Crew CR ON CR.CrewID = PE.CrewID
                       LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = PE.FlightCategoryID   
                       LEFT JOIN Vendor V ON PE.PaymentVendorID = V.VendorID
                       LEFT JOIN PaymentType PT ON PT.PaymentTypeID = PE.PaymentTypeID 
                       LEFT JOIN PostflightNote PN ON PN.PostflightExpenseID = PE.PostflightExpenseID        
   WHERE (AC.AccountNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AccountNum, ','))OR @AccountNum='')
         AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ','))OR @LogNum='')
         AND (PE.SlipNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SlipNUM, ','))OR @SlipNUM='')
         AND (PE.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
         AND (PE.PurchaseDT BETWEEN @DATEFROM And @DATETO)
         AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
         AND PM.IsDeleted=0
   ORDER BY PE.PurchaseDT
    END  





GO


