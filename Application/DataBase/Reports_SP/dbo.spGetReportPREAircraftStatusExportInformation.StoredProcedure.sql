IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftStatusExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftStatusExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[spGetReportPREAircraftStatusExportInformation]      
  @UserCD AS VARCHAR(30), --Mandatory          
  @AsOf AS DATETIME, --Mandatory        
  @TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @IsHomeBase AS BIT = 0      
AS        
-- ===============================================================================        
-- SPC Name: spGetReportAircraftStatusExportInformation       
-- Author: ASKAR         
-- Create date: 16 Jul 2012        
-- Description: Get Aircraft Status information for REPORTS        
-- Revision History        
-- Date   Name  Ver  Change        
--         
-- ================================================================================        
BEGIN  
SET NOCOUNT ON
 
DECLARE @EstDepartureDT DATETIME=@AsOf
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
    
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))   
    

 DECLARE @TenToMin INT

-----------------------------TailNum and Fleet Group Filteration----------------------

     

      
      SET @TenToMin= (SELECT COM.TimeDisplayTenMin
                      FROM   Company COM
                             JOIN UserMaster UM
                               ON COM.CustomerID = UM.CustomerID
                                  AND COM.HomebaseID = UM.HomebaseID
                      WHERE  UM.UserName = @UserCD)
DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE (F.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
	AND F.IsDeleted=0
	AND F.IsInActive =0
	AND F.CustomerID = @CUSTOMERID  
END


-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @SQLSCRIPT AS NVARCHAR(MAX) = '';      
DECLARE @ParameterDefinition AS NVARCHAR(500)  
      

 
 CREATE TABLE #TempFleetInfo (TailNum VARCHAR(9),
                     type_code VARCHAR(10),
                     ac_code CHAR(4),
                     af_hrs NUMERIC(11,3),
                     af_asof DATE,
                     af_landings NUMERIC(6,0),
                     en1_hrs NUMERIC(11,3),
                     en1_asof DATE,
                     en2_hrs NUMERIC(11,3),
                     en2_asof DATE,
                     en3_hrs NUMERIC(11,3),
                     en3_asof DATE,
                     en4_hrs NUMERIC(11,3),
                     en4_asof DATE,
                     en1_cycle NUMERIC(6,0),
                     en2_cycle NUMERIC(6,0),
                     en3_cycle NUMERIC(6,0),
                     en4_cycle NUMERIC(6,0),
                     re1_cycle NUMERIC(6,0),
                     re2_cycle NUMERIC(6,0),
                     re3_cycle NUMERIC(6,0),
                     re4_cycle NUMERIC(6,0),
                     day1 NUMERIC(7,3),
                     day2 NUMERIC(7,3),
                     day3 NUMERIC(7,3),
                     day4 NUMERIC(7,3),
                     day5 NUMERIC(7,3),
                     day6 NUMERIC(7,3),
                     day7 NUMERIC(7,3),
                     day8 NUMERIC(7,3),
                     day9 NUMERIC(7,3),
                     day10 NUMERIC(7,3),
                     day11 NUMERIC(7,3),
                     day12 NUMERIC(7,3),
                     day13 NUMERIC(7,3),
                     day14 NUMERIC(7,3),
                     day15 NUMERIC(7,3),
                     inactive BIT,
                     FleetID BIGINT
                     )

    
  INSERT INTO #TempFleetInfo(TailNum,
							 type_code,
							 ac_code,
							 af_hrs,
							 af_asof,
							 af_landings,
							 en1_hrs,
							 en1_asof,
							 en2_hrs,
							 en2_asof,
							 en3_hrs,
							 en3_asof,
							 en4_hrs,
							 en4_asof,
							 en1_cycle,
							 en2_cycle,
							 en3_cycle,
							 en4_cycle,
							 re1_cycle,
							 re2_cycle,
							 re3_cycle,
							 re4_cycle,
							 inactive,
							 FleetID )

SELECT F.TailNum,AT.AircraftCD,F.AircraftCD,FI.AirframeHrs,FI.AIrframeAsOfDT,FI.AirframeCycle,FI.Engine1Hrs,FI.Engine1AsOfDT,
       FI.Engine2Hrs,FI.Engine2AsOfDT,FI.Engine3Hrs,FI.Engine3AsOfDT,FI.Engine4Hrs,FI.Engine4AsOfDT,FI.Engine1Cycle,FI.Engine2Cycle,
       FI.Engine3Cycle,FI.Engine4Cycle,FI.Reverser1Cycle,FI.Reverser2Cycle,FI.Reverser3Cycle,FI.Reverser4Cycle,F.IsInActive,F.FleetID



 FROM  (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.IsDeleted,F.AircraftID,F.IsInActive
													FROM Fleet F ) F    
                    INNER JOIN @TempFleetID  T ON F.FleetID = T.FleetID  
                    LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
                    LEFT OUTER JOIN FleetInformation FI ON F.FleetID=FI.FleetID
             WHERE   F.CUSTOMERID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
             --  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN   CONVERT(DATE,@EstDepartureDT) AND  CONVERT(DATE,@EstDepartureDT+14)
              --- AND PM.TripStatus='T'  


UPDATE #TempFleetInfo SET day1=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


UPDATE #TempFleetInfo SET day2=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+1)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                           AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                           AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum
  
  
 UPDATE #TempFleetInfo SET day3=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=  0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+2)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day4=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+3)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day5=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+4)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
                          
UPDATE #TempFleetInfo SET day6=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+5)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

 
UPDATE #TempFleetInfo SET day7=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+6)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


 
                          
                          
UPDATE #TempFleetInfo SET day8=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+7)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          

UPDATE #TempFleetInfo SET day9=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+8)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day10=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+9)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0 
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day11=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+10)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

    
    
UPDATE #TempFleetInfo SET day12=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+11)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


UPDATE #TempFleetInfo SET day13=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+12)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day14=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+13)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day15=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+14)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
                          AND PM.Tripstatus IN ('T','H')	
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          

UPDATE #TempFleetInfo SET af_hrs = ISNULL(af_hrs,0)+POST2.POHOURS2 FROM (
	SELECT isnull(POST.[POHOURS],0) AS POHOURS2, #TempFleetInfo.TailNUM 
		FROM #TempFleetInfo
		  INNER JOIN
		 (
			 SELECT F.TailNUM,
			 [POHOURS] = CASE WHEN C.AircraftBasis = 1 THEN SUM(POL.BlockHours) ELSE SUM(POL.FlightHours) END
			 FROM
			 PostflightMain POM 
			 INNER JOIN PostflightLeg POL ON POM.POLOGID = POL.POLOGID
			 INNER JOIN Fleet F ON F.FleetID=POM.FleetID
			 LEFT OUTER JOIN FleetInformation FI ON F.FleetID=FI.FleetID 
			 INNER JOIN Company C ON POM.CustomerID = C.CustomerID AND POM.HomebaseID = C.HomebaseID
	     	 WHERE CONVERT(DATE,ScheduleDTTMLocal) BETWEEN
	     	                                         (CASE WHEN CONVERT(DATE,FI.AIrframeAsOfDT) IS NULL  THEN CONVERT(DATE,ScheduleDTTMLocal) ELSE CONVERT(DATE,FI.AIrframeAsOfDT) End)
	     	                                         AND CONVERT(DATE,@EstDepartureDT)
			 AND POM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			 GROUP BY  F.TailNUM, C.AircraftBasis 
		) POST ON #TempFleetInfo.TailNum = POST.TailNum
	) POST2	 
WHERE #TempFleetInfo.TailNum = POST2.TailNum



UPDATE #TempFleetInfo SET af_landings =ISNULL(af_landings,0)+POST.LegID FROM (SELECT F.TailNUM,COUNT(POL.POLegID) LegID
			 FROM
			 PostflightMain POM 
			 INNER JOIN PostflightLeg POL ON POM.POLOGID = POL.POLOGID
			 INNER JOIN Fleet F ON F.FleetID=POM.FleetID
			 LEFT OUTER JOIN FleetInformation FI ON F.FleetID=FI.FleetID 
			 INNER JOIN Company C ON POM.CustomerID = C.CustomerID AND POM.HomebaseID = C.HomebaseID
		   	 WHERE CONVERT(DATE,ScheduleDTTMLocal) BETWEEN
	     	                                         (CASE WHEN CONVERT(DATE,FI.AIrframeAsOfDT) IS NULL  THEN CONVERT(DATE,ScheduleDTTMLocal) ELSE CONVERT(DATE,FI.AIrframeAsOfDT) End)
	     	                                         AND CONVERT(DATE,@EstDepartureDT)
			   AND POM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			 GROUP BY  F.TailNUM)POST
WHERE #TempFleetInfo.TailNum = POST.TailNum                                 


                                                                           
-----------------Suppere Activity--------------------


 IF @SuppressActivityAircft=0 

BEGIN
INSERT INTO #TempFleetInfo(TailNum,type_code,FleetID,ac_code)
SELECT A.* FROM 
(SELECT  DISTINCT F.TailNum, B.AircraftCD,F.FleetID,F.AircraftCD Aircraft
      FROM Fleet F LEFT OUTER JOIN Aircraft B ON F.AircraftID = B.AircraftID  
      WHERE  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
      AND RTRIM(TailNum) NOT IN (SELECT RTRIM(TailNum) FROM #TempFleetInfo) 
     )A
ORDER BY A.Aircraft
END

-----------------End Suppress Activity---------------



SELECT DISTINCT TI.TailNum tail_nmbr,type_code,ac_code,FLOOR(ISNULL(af_hrs,0)*10)*0.1 af_hrs,af_asof,FLOOR(ISNULL(af_landings,0)*10)*0.1af_landings,
                FLOOR(ISNULL(en1_hrs,0)*10)*0.1en1_hrs,en1_asof,FLOOR(ISNULL(en2_hrs,0)*10)*0.1en2_hrs,en2_asof,FLOOR(ISNULL(en3_hrs,0)*10)*0.1en3_hrs,en3_asof,FLOOR(ISNULL(en4_hrs,0)*10)*0.1en4_hrs,
                en4_asof,FLOOR(ISNULL(en1_cycle,0)*10)*0.1 en1_cycle,FLOOR(ISNULL(en2_cycle,0)*10)*0.1 en2_cycle,FLOOR(ISNULL(en3_cycle,0)*10)*0.1 en3_cycle,FLOOR(ISNULL(en4_cycle,0)*10)*0.1 en4_cycle,FLOOR(ISNULL(re1_cycle,0)*10)*0.1 re1_cycle,FLOOR(ISNULL(re2_cycle,0)*10)*0.1 re2_cycle,FLOOR(ISNULL(re3_cycle,0)*10)*0.1re3_cycle,FLOOR(ISNULL(re4_cycle,0)*10)*0.1	re4_cycle,
                FLOOR(ISNULL(day1,0)*10)*0.1 day1,FLOOR(ISNULL(day2,0)*10)*0.1day2,FLOOR(ISNULL(day3,0)*10)*0.1day3,FLOOR(ISNULL(day4,0)*10)*0.1day4,FLOOR(ISNULL(day5,0)*10)*0.1day5,FLOOR(ISNULL(day6,0)*10)*0.1day6,FLOOR(ISNULL(day7,0)*10)*0.1day7,FLOOR(ISNULL(day8,0)*10)*0.1day8,FLOOR(ISNULL(day9,0)*10)*0.1day9,
                FLOOR(ISNULL(day10,0)*10)*0.1day10,FLOOR(ISNULL(day11,0)*10)*0.1day11,FLOOR(ISNULL(day12,0)*10)*0.1day12,FLOOR(ISNULL(day13,0)*10)*0.1day13,FLOOR(ISNULL(day14,0)*10)*0.1day14,FLOOR(ISNULL(day15,0)*10)*0.1day15,ISNULL(inactive,0)inactive


       FROM #TempFleetInfo TI  
       INNER JOIN @TempFleetID TF ON TF.FleetID=TI.FleetID
       ORDER BY ac_code
      
IF OBJECT_ID('tempdb..#TempFleetInfo') IS NOT NULL
DROP TABLE #TempFleetInfo

IF OBJECT_ID('tempdb..#TempFleet') IS NOT NULL
DROP TABLE  #TempFleet 

END    


-- EXEC spGetReportPREAircraftStatusExportInformation 'JWILLIAMS_11', '2012-08-01',''









GO


