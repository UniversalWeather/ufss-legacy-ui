IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar1Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--TEST

CREATE Procedure [dbo].[spGetReportPREAircraftCalendar1Information]  
  @DATEFROM AS DATETIME, --Mandatory  
  @UserCD AS VARCHAR(30),
  @TailNUM AS VARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values  
AS  
-- ===============================================================================  
-- SPC Name: spGetReportPREAircraftCalender1Information  
-- Author: ASKAR  
-- Create date: 04 Jul 2012  
-- Description: Get Crew Calender information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
-- 20 Aug 2012 Sudhakar 1.1  Fix in date filter  
-- ================================================================================  
BEGIN  
SET NOCOUNT ON  
  
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
DECLARE @ParameterDefinition AS NVARCHAR(100);
DECLARE @NoOfRows INT
  
-- Populate the complete Crew List for each Date in the given range  
 CREATE TABLE #TempFleetDate (  
     RowID INT identity(1, 1) NOT NULL,  
  DeptDateOrg DATETIME,  
  TailNum CHAR(10),  
  AircraftID BIGINT,  
  FleetID BIGINT,  
  TripID BIGINT,  
  LegID BIGINT,  
  AT_Type CHAR(10),  
  DepartICAOID CHAR(4),  
  ArriveICAOID CHAR(4),  
  DepartureDTTMLocal DATETIME,  
  ArrivalDTTMLocal DATETIME,  
  NextLocalDTTM DATETIME,  
  DutyType CHAR(2),  
  CrewMember VARCHAR(250),  
  HomeBaseCD CHAR(8),  
  TripNUM BIGINT)   
INSERT INTO #TempFleetDate(DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,AT_Type,
		DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,
		DutyType,CrewMember,HomeBaseCD,TripNUM)  
SELECT @DATEFROM,F.TailNum,A.AircraftID,F.FleetID,PL.TripID,PL.LegID,A.AircraftCD,
		AT.IcaoID DepartICAOID,APT.IcaoID ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,
		PL.NextLocalDTTM,DutyType,  
       C1.CrewList, dbo.GetHomeBaseCDByHomeBaseID(C.HomebaseAirportID) HomeBaseCD,PM.TripNUM    
       FROM PreflightMain PM INNER JOIN (SELECT TripID,LegID,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,  
       NextLocalDTTM,TripNUM,DutyTYPE,AdditionalCrew FROM PreflightLeg WHERE IsDeleted = 0) PL ON PL.TripID=PM.TripID  
       INNER JOIN (SELECT FleetID,HomebaseID,AircraftID,TailNum, CustomerID FROM Fleet)F 
		ON PM.FleetID=F.FleetID AND PM.CustomerID = F.CustomerID
	 LEFT OUTER JOIN (  
	   SELECT DISTINCT PC2.tripid, CrewList = (  
		 SELECT distinct C.CrewCD + ','  
		 FROM PreflightCrewList PC1 
		 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
		 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
		 WHERE Pl.tripid = PC2.tripid  
		 FOR XML PATH('')  
		 )           
		FROM PreflightLeg PC2  
   ) C1 ON PM.tripid = C1.tripid  
	   INNER JOIN Company C ON C.HomebaseID=F.HomebaseID
       INNER JOIN (SELECT AircraftID,AircraftCD FROM Aircraft)A ON A.AircraftID=F.AircraftID  
       INNER JOIN(SELECT  AirportID,IcaoID FROM Airport)AT ON AT.AirportID=PL.DepartICAOID  
       INNER JOIN(SELECT  AirportID,IcaoID FROM Airport)APT ON APT.AirportID=PL.ArriveICAOID   
       WHERE   
     (CONVERT(DATE,PL.DepartureDTTMLocal) = CONVERT(DATE,@DATEFROM))  
    -- (End) v1.1 --  
         AND  F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) 
         AND  ISNULL(PM.RecordType,'') <> 'M'  
         AND ISNULL(PL.DutyTYPE,'') <> 'D'
		AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
		AND PM.IsDeleted = 0       
 ---  SELECT * FROM #TempFleetDate  
-------------------  
DECLARE @crTailNum CHAR(6), @crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT, @crTripID BIGINT,   
@crLegID BIGINT, @crDepartICAOID CHAR(4),@crArriveICAOID CHAR(4),@crDepartureDTTMLocaL DATETIME,@crArrivalDTTMLocal DATETIME,  
@crNextLocalDTTM DATETIME,@crDutyType CHAR(2),@crCrewCD CHAR(5),@crHomebaseCD CHAR(4),@crTripNUM BIGINT  
DECLARE @Loopdate DATETIME  
DECLARE curFleetCal CURSOR FOR SELECT TFD.TailNum,TFD.DeptDateOrg,TFD.AircraftID,TFD.FleetID,TFD.TripID,TFD.LegID,TFD.DepartICAOID,TFD.ArriveICAOID,  
       TFD.DepartureDTTMLocal,TFD.ArrivalDTTMLocal,TFD.NextLocalDTTM,TFD.DutyTYPE,TFD.HomebaseCD,TFD.TripNUM   
       FROM #TempFleetDate TFD  
 WHERE CONVERT(VARCHAR, TFD.ArrivalDTTMLocal, 101) < CONVERT(VARCHAR, TFD.NextLocalDTTM, 101)   
  ORDER BY DeptDateOrg,TFD.TailNum  
OPEN curFleetCal;  
 FETCH NEXT FROM curFleetCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crArriveICAOID,  
               @crDepartureDTTMLocal,@crArrivalDTTMLocal,@crNextLocalDTTM,@crDutyType,@crHomebaseCD,@crTripNUM;  
 WHILE @@FETCH_STATUS = 0  
 BEGIN  
 SET @Loopdate = '';  
  --STEP 1:  
     INSERT INTO #TempFleetDate(DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,DutyType,HomeBaseCD,TripNUM)  
   VALUES (@crDeptDateOrg,@crTailNum,@crAircraftID,@crFleetID,@crTripID,@crLegID,@crDepartICAOID,@crArriveICAOID,@crArrivalDTTMLocal,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrivalDTTMLocal, 101) + ' 23:59', 101),NULL, 'R',@crHomeBaseCD,@crTripNUM)   
    
  --STEP 2:  
  SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrivalDTTMLocal, 101) + ' 00:00', 101));  
  WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @crNextLocalDTTM, 101))  
  BEGIN  
   --PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)  
   INSERT INTO #TempFleetDate (DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,DutyType,HomeBaseCD,TripNUM)  
   VALUES (@Loopdate,@crTailNum,@crAircraftID,@crFleetID,@crTripID,@crLegID,@crArriveICAOID,@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:01', 101), CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),NULL
,'R',@crHomeBaseCD,@crTripNUM)   
   SET @Loopdate = DATEADD(DAY, 1, @Loopdate);  
   FETCH NEXT FROM curFleetCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crArriveICAOID,  
               @crDepartureDTTMLocal,@crArrivalDTTMLocal,@crNextLocalDTTM,@crDutyType,@crHomebaseCD,@crTripNUM;  
  END  
  END   
 CLOSE curFleetCal;  
 DEALLOCATE curFleetCal; 
 SET @NoOfRows =(select COUNT(*) from #TempFleetDate); 
 
 IF @NoOfRows > 0

BEGIN
 SELECT DeptDateOrg,TailNum,AT_Type,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,DutyType,  
       SUBSTRING(CrewMember,1,LEN(CrewMember)-1) AS CrewMember,(len(CrewMember) - len(replace(CrewMember, ',', '*'))+1) AS Crew_Count,TripNUM 
       FROM #TempFleetDate 
 END 
 ELSE BEGIN
 		SELECT NULL DeptDateOrg, NULL TailNum, NULL AT_Type,A.ICAOID DepartICAOID, 
			NULL ArriveICAOID,NULL DepartureDTTMLocal, NULL ArrivalDTTMLocal, NULL DutyType, 
			NULL CrewMember,NULL Crew_Count,NULL TripNUM
		FROM Fleet F 
		INNER JOIN Company C ON F.HomebaseID = C.HomebaseID
		INNER JOIN Airport A ON C.HomebaseAirportID = A.AirportID
		WHERE F.TailNum = @TailNUM
		AND F.CustomerID =  dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))  
    END       
END   
---EXEC spGetReportPREAircraftCalendar1Information '2012-10-21', 'tim', 'N4753'
---EXEC spGetReportPREAircraftCalendar1Information '2012-07-20', 'UC', ''


GO


