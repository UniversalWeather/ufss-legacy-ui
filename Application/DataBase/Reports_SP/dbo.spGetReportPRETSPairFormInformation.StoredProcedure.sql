IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPairFormInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPairFormInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPairFormInformation]
	@UserCD VARCHAR(30)
	,@TripID VARCHAR(30)
	,@LegNum VARCHAR(30)
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSPairFormInformation
-- Author: AISHWARYA.M
-- Create date:27 SEP 2012
-- Description: Get Preflight Tripsheet Pair Form information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================
DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
		SELECT TOP 1--PART I: ARRIVAL INFORMATION
				[USAirportofArrival] = AA.IcaoID
				,[ArrivalTimeZulu] = AA.OffsetToGMT
				,[ArrivalTimeLocal] = PL.ArrivalDTTMLocal --(Only Time)
				,[ForeignAirportofDeparture] = AD.IcaoID + ' ' + AD.CityName + ' ' + CD.CountryCD
				,[DepartureTimeZulu] = AD.OffsetToGMT
				,[DepartureTimeLocal] = PL.DepartureDTTMLocal --(Only Time)
				--,[DateofArrival] = PL.ArrivalDTTMLocal
				--,[DateofDeparture] = PL.DepartureDTTMLocal
				,[DateofArrival] = PL.ArrivalGreenwichDTTM
				,[DateofDeparture] = PL.DepartureGreenwichDTTM			
				--PART II: AIRCRAFT INFORMATION
				,[AircraftTailNo] = F.TailNum
				,[DecalNo] = FP.BuyAircraftAdditionalFeeDOM
				,[Make] = FP.AircraftMake
				,[Model] = FP.AircraftModel
				,[Colors] = FP.AircraftColor1 + '/' + FP.AircraftColor2
				,[Trim] = FP.AircraftTrimColor
				,[AircraftOwnerLesseeName] = FP.OwnerLesse
				,[AStreetAddress] = FP.Addr1 + ' ' + FP.Addr2
				,[ACity] = FP.CityName
				,[AState] = FP.StateName
				,[ACountry] = CF.CountryCD
				,[AZip] = FP.PostalZipCD
				----PART III: PILOT INFORMATION
				,[FullLegalName] = ISNULL(C.LastName, '') + ',' + ISNULL(C.FirstName, '') + ' ' + ISNULL(C.MiddleInitial, '')
				,[StreetAddress] = C.Addr1 + '' + C.Addr2
				,[DateofBirth] = C.BirthDT
				,[Nationality] = CC.CountryCD
				,[City] = C.CityName
				,[State] = C.StateName
				,[PilotLicenseNumber] = C.PilotLicense1
				,[Country] = CP.CountryCD
				,[Zip] = C.PostalZipCD
				FROM PreflightMain PM
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
				INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID				
				INNER JOIN Fleet F ON PM.FleetID = F.FleetID
				INNER JOIN FleetPair FP ON PM.CustomerID = FP.CustomerID AND F.FleetID = FP.FleetID
				INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID AND PCL.DutyTYPE = 'P'
				INNER JOIN Crew C ON PCL.CrewID = C.CrewID
				LEFT OUTER JOIN CrewPassengerPassport CPP 
					ON CPP.CrewID = PCL.CrewID AND PCL.PassportID = CPP.PassportID AND CPP.IsDeleted = 0
				LEFT OUTER JOIN Country CD ON AD.CountryID = CD.CountryID
				LEFT OUTER JOIN Country CC ON CPP.CountryID = CC.CountryID
				LEFT OUTER JOIN Country CP ON C.CountryID = CP.CountryID
				LEFT OUTER JOIN Country CF ON FP.CountryID = CF.CountryID
				WHERE PM.CustomerID = @CustomerID
				AND PM.TripID = CONVERT(BIGINT,@TripID)
				AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
				
				ORDER BY C.CrewCD 
END

--EXEC spGetReportPRETSPairFormInformation 'TIM', 1000142, '1' 









GO


