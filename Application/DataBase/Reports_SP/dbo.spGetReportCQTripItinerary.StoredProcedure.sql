IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQTripItinerary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQTripItinerary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

                                                                                           
CREATE PROCEDURE [dbo].[spGetReportCQTripItinerary] 
( 
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30), --Mandatory
	@TripNUM AS NVARCHAR(500) = '',
	@LEG AS BIGINT,
	--@NumberofCopies AS BIGINT,
	@PrintOneLegPerPage BIT = 0,
	@PrintLegNotes BIT = 0
	--,@FilterFerryLegs BIT = 0
)
AS
-- =============================================
-- SPC Name:spGetReportCQTripItinerary
-- Author: Akhila
-- Create date:
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON
SELECT DISTINCT
[TripNumber] = PM.TripNUM
,[TripDates] = ISNULL(CRL.DepartureDTTMLocal,'') --+ ISNULL(CRL.ArrivalDTTMLocal ,'')--DOUBT
,[TailNumber] = F.TailNum
,[Type] = AC.AircraftCD
,[Description] = NULL --DOUBT
,[Requestor] = P.PassengerName
,[ContactPhone] = NULL --DOUBT
,[Leg] = CRL.LegNUM
,[DepartICAO] = NULL
,[ArriveICAO] = NULL
,[Timechange] = NULL
,[LocalTime] = NULL
,[LocDep] = NULL
,[LocArr] = NULL
,[Distance] = CRL.Distance
,[ETE] = CRL.ElapseTM
,[PAX] = CRL.PassengerTotal
,[PILOTPIC] = CASE WHEN PC.DutyTYPE='P' THEN ISNULL(C.CrewCD,'') + ISNULL(C.LastName,'') + ISNULL(C.FirstName,'') ELSE '' END--DOUBT
,[PILOTSIC] = CASE WHEN PC.DutyTYPE='S' THEN ISNULL(C.CrewCD,'') + ISNULL(C.LastName,'') + ISNULL(C.FirstName,'') ELSE '' END--DOUBT
,[ADDLCREW] = ACrew.AssociatedCrew
,[FBONameFromTerminal] = CASE WHEN CRFBO.RecordType='FD' THEN CRFBO.CRFBOListDescription ELSE '' END
,[PhoneFromTerminal] = CASE WHEN CRFBO.RecordType='FD' THEN CRFBO.PhoneNUM ELSE '' END
,[FBONameToTerminal] = CASE WHEN CRFBO.RecordType='FB' THEN CRFBO.CRFBOListDescription ELSE '' END
,[PhoneToTerminal] = CASE WHEN CRFBO.RecordType='FB' THEN CRFBO.PhoneNUM ELSE '' END
,[HotelName] = CASE WHEN CRFBO.RecordType='PH' THEN CRH.CRHotelListDescription ELSE '' END
,[HotelPhone] = CASE WHEN CRFBO.RecordType='PH' THEN CRH.PhoneNUM ELSE '' END
,[TransportName] = CASE WHEN CRFBO.RecordType='PT' THEN CRT.CRTransportListDescription ELSE '' END
,[TransportPhone] = CASE WHEN CRFBO.RecordType='PT' THEN CRT.PhoneNUM ELSE '' END
,[CrewHotelName] = CASE WHEN CRFBO.RecordType='CH' THEN CRH.CRHotelListDescription ELSE '' END
,[CrewHotelPhone] = CASE WHEN CRFBO.RecordType='CH' THEN CRH.PhoneNUM ELSE '' END
,[CrewHotelAddress1] = H.Addr1
,[CrewHotelAddress2] = H.Addr2
,[CaterName] = CASE WHEN CRFBO.RecordType='CA' THEN CRC.CRCateringListDescription ELSE '' END
,[PAXNotes] = ISNULL(CRL.Notes,'NONE')
,[paxname] = CRP.PassengerName
,[fltpurpose] = FP.FlightPurposeCD
--SEE LINE 71
FROM PreflightMain PM
LEFT OUTER JOIN CRMain CRM ON CRM.TripID = PM.TripID
LEFT OUTER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID
LEFT OUTER JOIN Fleet F ON F.FleetID = CRM.FleetID
LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID
--LEFT OUTER JOIN Airport A ON A.AirportID =
LEFT OUTER JOIN PreflightLeg PL ON PL.TripID = PM.TripID
LEFT OUTER JOIN PreflightCrewList PC ON PC.LegID = PL.LegID
LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID
LEFT OUTER JOIN CRFBOList CRFBO ON CRFBO.CRLegID = CRL.CRLegID
LEFT OUTER JOIN CRHotelList CRH ON CRH.CRLegID = CRL.CRLegID
LEFT OUTER JOIN CRTransportList CRT ON CRT.CRLegID = CRL.CRLegID
LEFT OUTER JOIN  Hotel H ON H.HotelID = CRH.HotelID
LEFT OUTER JOIN CRCateringList CRC ON CRC.LegID = CRL.LegID
LEFT OUTER JOIN CRPassenger CRP ON CRP.LegID = CRL.LegID
LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID = CRP.FlightPurposeID
LEFT OUTER JOIN (
		SELECT DISTINCT PC1.LegID, AssociatedCrew = (
		SELECT RTRIM(C.CrewCD) + ','
		FROM PreflightCrewList PC 
		INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.LegID = PC1.LegID
		order by C.CrewCD
		FOR XML PATH('')
		)
		FROM PreflightCrewList PC1
					) ACrew ON PL.LegID = ACrew.LegID
WHERE
(PM.TripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '')
END
--EXEC spGetReportCQTripItinerary 'SUPERVISOR_99','10099','2','','',0,0,0

GO


