IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationSummaryExportInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
	)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationSummaryExportInformation
-- Author:  A.Akhila
-- Create date: 23 AUG 2012
-- Description: Get Department Authorization Summary for EXPORT REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =1
CREATE TABLE #DEPARTINFO
(
[DEPARTMENTID] BIGINT
,[DateRange] NVARCHAR(100)
,[lognum] NVARCHAR(10)
,[reqdept] NVARCHAR(10)
,[reqauth] NVARCHAR(10)
,[legid] NVARCHAR(10)
,[dept_code] NVARCHAR(8)
,[deptdesc] NVARCHAR(25)
,[auth_code] NVARCHAR(8)
,[authdesc] NVARCHAR(25)
,[cat_code] CHAR(4)
,[fltcatdesc] NVARCHAR(25)
,[blk_hrs] NUMERIC(6,3)
,[flt_hrs] NUMERIC(6,3)
,[tot_line] NVARCHAR(10)
,[leg_cnt] BIGINT
,[Distance] NUMERIC(5,0)
,[pax] BIGINT
)

INSERT INTO #DEPARTINFO
SELECT
[DEPARTMENTID] = D.DEPARTMENTID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[lognum] = '0'
,[reqdept] = ''
,[reqauth] = ''
,[legid] = '0'
,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END
,[deptdesc] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END
,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END
,[authdesc] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END
,[cat_code] = FC.FlightCatagoryCD
,[fltcatdesc] = FC.FlightCatagoryDescription
,[blk_hrs] = ISNULL(ROUND(POL.BlockHours,1),0)
,[flt_hrs] = ISNULL(ROUND(POL.FlightHours,1),0)
,[tot_line] = ''
,[leg_cnt] = ISNULL(COUNT(POL.POLegID),0)
,[Distance] = ISNULL(POL.Distance,0)
,[pax] = POL.PassengerTotal
FROM PostflightLeg POL
LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA
ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 AND 
POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND POL.IsDeleted=0
AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
GROUP BY D.DEPARTMENTID, POL.POLegID, POL.LEGNUM, DepartmentCD,D.DepartmentName,DA.DeptAuthDescription ,DA.AuthorizationCD, 
FC.FlightCatagoryCD, FC.FlightCatagoryDescription, TripLegID, BlockHours, FlightHours, Distance, PassengerTotal, POM.LogNum

CREATE TABLE #DEPARTEMPTY
(
[DEPARTMENTID] BIGINT
,[DateRange] NVARCHAR(100)
,[lognum] NVARCHAR(10)
,[reqdept] NVARCHAR(10)
,[reqauth] NVARCHAR(10)
,[legid] NVARCHAR(10)
,[dept_code] NVARCHAR(8)
,[deptdesc] NVARCHAR(25)
,[auth_code] NVARCHAR(8)
,[authdesc] NVARCHAR(25)
,[cat_code] CHAR(4)
,[fltcatdesc] NVARCHAR(25)
,[blk_hrs] NUMERIC(6,3)
,[flt_hrs] NUMERIC(6,3)
,[tot_line] NVARCHAR(10)
,[leg_cnt] BIGINT
,[Distance] NUMERIC(5,0)
,[pax] BIGINT
)

INSERT INTO #DEPARTEMPTY
SELECT
[DEPARTMENTID] = D.DEPARTMENTID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[lognum] = '0'
,[reqdept] = ''
,[reqauth] = ''
,[legid] = '0'
,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END
,[deptdesc] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END
,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END
,[authdesc] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END
,[cat_code] = NULL
,[fltcatdesc] = 'NO ACTIVITY'
,[blk_hrs] = NULL
,[flt_hrs] = NULL
,[tot_line] = ''
,[leg_cnt] = NULL
,[Distance] = NULL
,[pax] = NULL
FROM DEPARTMENT D
LEFT OUTER JOIN DepartmentAuthorization DA
ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND DA.IsInActive=0
AND DA.IsInActive = 0 AND D.DepartmentID = DA.DepartmentID
WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsInActive=0
 AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
IF @IsZeroSuppressActivityDeptRpt = 0
BEGIN
SELECT * FROM #DEPARTINFO
UNION ALL
SELECT * FROM #DEPARTEMPTY
ORDER BY [dept_code], [auth_code], [cat_code]
END
ELSE
BEGIN
SELECT * FROM #DEPARTINFO
ORDER BY [dept_code], [auth_code], [cat_code]
END


IF OBJECT_ID('tempdb..#DEPARTINFO') IS NOT NULL
DROP TABLE #DEPARTINFO 	

IF OBJECT_ID('tempdb..#DEPARTEMPTY') IS NOT NULL
DROP TABLE #DEPARTEMPTY 	

--EXEC spGetReportPOSTDepartmentAuthorizationSummaryExportInformation 'jwilliams_13','2009-01-1','2010-1-1', '', '', 0






GO


