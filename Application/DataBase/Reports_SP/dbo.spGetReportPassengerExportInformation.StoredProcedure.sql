IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPassengerExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportPassengerExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get Passenger Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		[code] = PassengerInfoCD
		,[desc] = PassengerDescription 
	FROM PassengerInformation WHERE IsDeleted = 0
	AND CustomerID = @CUSTOMERID
	--AND ClientID = ISNULL(@CLIENTID, ClientID)
	AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
	ORDER BY PassengerInfoCD
  -- EXEC spGetReportPassengerExportInformation 'UC'



GO


