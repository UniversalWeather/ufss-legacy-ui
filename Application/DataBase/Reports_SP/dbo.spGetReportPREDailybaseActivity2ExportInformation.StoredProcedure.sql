IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity2ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity2ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ===============================================================================      
-- SPC Name: spGetReportPREDailybaseActivity2ExportInformation     
-- Author: Abhishek S      
-- Create date: 27 Jul 2012      
-- Description: Get DailybaseActivity2 Export Information for REPORTS      
-- Revision History      
-- Date   Name  Ver  Change      
--       
-- ================================================================================     
CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity2ExportInformation]  
  @UserCD AS VARCHAR(30), -- Mandatory  
  @DATEFROM AS DATETIME,  -- Mandatory  
  @DATETO AS DATETIME,    -- Mandatory  
  @IcaoID AS NVARCHAR(500) = '', -- Mandatory  
  @TripNum AS NVARCHAR(500) = '', -- [Optional]  
  @TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @IsHomeBase AS BIT = 0  
   
AS  
BEGIN   
 SET NOCOUNT ON;  
   
 DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
    DECLARE @ParameterDefinition AS NVARCHAR(500)   
  
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------
     SELECT DISTINCT [locdep]=PL.DepartureDTTMLocal 
					,[orig_nmbr]=PM.TripNUM  
					,[legid]=PL.LegID  
					,[fuel_load]=ISNULL(PL.FuelLoad,0)
					,[tail_nmbr]=F.TailNum  
					,[ac_code]=F.AircraftCD  
                ,activity=CASE WHEN (D.IcaoID = @IcaoID) THEN 'DEPARTS' ELSE 'ARRIVES' END 
                   ,icao=CASE WHEN (D.IcaoID = @IcaoID)  THEN D.IcaoID ELSE CONVERT(VARCHAR(10),A.IcaoID) END
    	           ,timeout=CASE WHEN (D.IcaoID = @IcaoID) THEN CONVERT(VARCHAR(5), PL.DepartureDTTMLocal , 108)      
                                                                                     ELSE CONVERT(VARCHAR(5), PL.ArrivalDTTMLocal , 108) END
                   ,pax_total=PL.PassengerTotal
                   ,activity2=CASE WHEN (A.IcaoID=@IcaoID) THEN 'DEPARTS'  ELSE 'ARRIVES' END 
                   ,icao2=CASE WHEN A.IcaoID=@IcaoID THEN CONVERT(VARCHAR(10),D.IcaoID)
                                                                                    ELSE CONVERT(VARCHAR(10),A.IcaoID) END
                   ,timeout2=CASE WHEN (A.IcaoID=@IcaoID) THEN CONVERT(VARCHAR(5),   PL.DepartureDTTMLocal , 108)      
                                                                                    ELSE CONVERT(varchar(5),  PL.ArrivalDTTMLocal , 108) END
				   ,paxname=CASE WHEN PL.PassengerTotal = 0 THEN ' ' ELSE ISNULL(P.LastName+ ',','') + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') END 
        
					,[blocked] = CASE WHEN PL.PassengerTotal = 0 THEN NULL ELSE PP.IsBlocked END  
					,[sort] = CASE(PP.IsBlocked)WHEN 'TRUE' THEN '1' ELSE 0 END   
					,[fltpurpose]= CASE WHEN PL.PassengerTotal = 0 THEN ' ' ELSE FP.FlightPurposeCD END 
					,PL.OutbountInstruction
					INTO #TEMP
   FROM PreflightMain PM   
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
	     LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS D ON D.AirportID = PL.DepartICAOID
	   LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A ON A.AirportID = PL.ArriveICAOID
		INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD 
			FROM Fleet F 					
		   ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID 
		INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
		LEFT OUTER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PP.PassengerID
	    LEFT OUTER JOIN FlightPurpose FP
				ON PP.FlightPurposeID = FP.FlightPurposeID 		            
    WHERE PM.IsDeleted = 0  
		AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
		AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)   
		AND PM.TripStatus = 'T'  
		AND PM.RecordType <> 'M'
		AND (D.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')))OR 
	         A.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ','))))
	    AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
	    AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase='')

INSERT INTO #Temp 
SELECT DISTINCT [locdep]=PL.DepartureDTTMLocal 
					,[orig_nmbr]=PM.TripNUM  
					,[legid]=PL.LegID  
					,[fuel_load]=ISNULL(PL.FuelLoad,0)
					,[tail_nmbr]=F.TailNum  
					,[ac_code]=F.AircraftCD  
					,activity=CASE WHEN (D.IcaoID = @IcaoID) THEN 'DEPARTS' ELSE 'ARRIVES' END 
                   ,icao=CASE WHEN (D.IcaoID = @IcaoID)  THEN D.IcaoID ELSE CONVERT(VARCHAR(10),A.IcaoID) END
    	          ,timeout=CASE WHEN (D.IcaoID = @IcaoID) THEN CONVERT(VARCHAR(5), PL.DepartureDTTMLocal , 108)      
                                                                                     ELSE CONVERT(VARCHAR(5), PL.ArrivalDTTMLocal , 108) END
                  ,pax_total=PL.PassengerTotal
                   ,activity2=CASE WHEN (A.IcaoID=@IcaoID) THEN 'DEPARTS'  ELSE 'ARRIVES' END 
                   ,icao2=CASE WHEN A.IcaoID=@IcaoID THEN CONVERT(VARCHAR(10),D.IcaoID)
                                                                                    ELSE CONVERT(VARCHAR(10),A.IcaoID) END
                   ,timeout2=CASE WHEN (A.IcaoID=@IcaoID) THEN CONVERT(VARCHAR(5),   PL.DepartureDTTMLocal , 108)      
                                                                                    ELSE CONVERT(varchar(5),  PL.ArrivalDTTMLocal , 108) END
				  ,paxname=''
             ,[blocked] = NULL
					,[sort] = 0
					,[fltpurpose]=''
					,PL.OutbountInstruction
   FROM PreflightMain PM   
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS D ON D.AirportID = PL.DepartICAOID
		                 LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A ON A.AirportID = PL.ArriveICAOID	
		INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD 
			FROM Fleet F 					
		   ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID 
		INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
		LEFT OUTER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PP.PassengerID
	    LEFT OUTER JOIN FlightPurpose FP
				ON PP.FlightPurposeID = FP.FlightPurposeID 		            
    WHERE PM.IsDeleted = 0  
		AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
		AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)   
		AND PM.TripStatus = 'T'  
		AND PM.RecordType <> 'M'
		AND (D.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')))OR 
	         A.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ','))))
	    AND (PM.TripNUM IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripNum, ',')))OR @TripNum='')
	    AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase='')     
	         
 SELECT DISTINCT * FROM #TEMP  ORDER BY locdep,timeout,timeout2
 -- EXEC spGetReportPREDailybaseActivity2ExportInformation 'TIM','2012-10-01','2012-10-22','KHOU','', '', 'TEST,123', 0  
 -- EXEC spGetReportPREDailybaseActivity2ExportInformation 'eliza_9','2008-07-20','2012-07-22 ','','', '', '', 0  
 -- EXEC spGetReportPREDailybaseActivity2ExportInformation 'JWILLIAMS_11','2011-01-01','2012-01-01','1936','', '', '', 0 
      
 
 IF OBJECT_ID('TEMPDB..#TempFleetID') IS NOT NULL DROP TABLE #TempFleetID
 IF OBJECT_ID('TEMPDB..#Temp') IS NOT NULL DROP TABLE #Temp

 END  






GO


