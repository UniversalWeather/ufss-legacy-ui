IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAnnualCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAnnualCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--EXEC spGetReportPOSTAnnualCrewInformation 'BRUCE_98',2013,'JAN',FEB

  
CREATE PROCEDURE [dbo].[spGetReportPOSTAnnualCrewInformation]              
( 
@UserCD VARCHAR(30), --Mandatory       --='BRUCE_98'                          
 @YEAR INT, -- Mandatory  
 @MONTHFROM AS VARCHAR(3), --Mandatory  --='JAN'
 @MONTHTO AS  VARCHAR(3), --Mandatory --='DEC'                   
 @CrewCD VARCHAR(5000)='',                
 @CrewGroupCD VARCHAR(5000)='',                
 @DepartmentCD VARCHAR(5000)='',                
 @DepartmentGroupID VARCHAR(5000)='',                
 @HomeBaseCD VARCHAR(5000)='',                
 @FlightCatagoryCD VARCHAR(5000)='',                
 @IsHomebase bit=1,
 @PrintType Char(1)='B'           
 )                  
 AS    
 
 -- ===============================================================================        
-- SPC Name: spGetReportPOSTAnnualCrewInformation      
-- Author: ASKAR         
-- Create date: 13 Jan 2014        
-- Description: Get Annual Crew Statistics Information       
-- Revision History        
-- Date   Name  Ver  Change        
--         
-- ================================================================================       
SET NOCOUNT ON
BEGIN

DECLARE @CustomerID BIGINT=(dbo.GetCustomerIDbyUserCD(@UserCD))
DECLARE @MonthStart INT =(DATEPART(mm,CAST(@MONTHFROM+ ' 1900' AS DATETIME)))
DECLARE @MonthEnd INT =(DATEPART(mm,CAST(@MONTHTO+ ' 1900' AS DATETIME)))

DECLARE  @DATEFROM  DATE =(SELECT DATEADD(month,(@MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @DATETO DATE =(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0))))  

-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE  @TempCrewDepartmentID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		DepartmentID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO @TempCrewDepartmentID(CrewID)
	SELECT DISTINCT C.CrewID 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  

END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO @TempCrewDepartmentID(CrewID)
	SELECT DISTINCT  C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO @TempCrewDepartmentID(CrewID) 
	SELECT DISTINCT  C.CrewID
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
		AND C.IsDeleted=0
	AND C.IsStatus=1
END

-----------------------------TailNum and Fleet Group Filteration----------------------


-----------------------------Department and Department Group Filteration----------------------
IF @DepartmentCD <> ''
BEGIN
	INSERT INTO @TempCrewDepartmentID(DepartmentID)
	SELECT DISTINCT D.DepartmentID 
	FROM Department D
	WHERE D.CustomerID = @CUSTOMERID
	AND D.DepartmentCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))
END

IF @DepartmentGroupID <> ''
BEGIN  
INSERT INTO @TempCrewDepartmentID(DepartmentID)
	SELECT DISTINCT  D.DepartmentID
	FROM Department D 
	LEFT OUTER JOIN DepartmentGroupOrder DGO
	ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID
	LEFT OUTER JOIN DepartmentGroup DG 
	ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID
	WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, ',')) 
	AND D.CustomerID = @CUSTOMERID  
END
ELSE IF @DepartmentCD = '' AND  @DepartmentGroupID = '' 
BEGIN  
INSERT INTO @TempCrewDepartmentID(DepartmentID)
	SELECT DISTINCT D.DepartmentID
	FROM Department D 
	WHERE D.IsDeleted=0
	  AND D.IsInActive =0
	  AND D.CustomerID = @CUSTOMERID  
	UNION ALL
	SELECT 999
END


-----------------------------Department and Department Group Filteration----------------------

 Declare @SuppressActivityCrew BIT  

SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@CUSTOMERID 
																		 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))


DECLARE @CrewStat TABLE(CrewCD VARCHAR(5)
                        ,Crew VARCHAR(70)
                        ,CrewID BIGINT
					    ,HoursVal NUMERIC(10,3)
					    ,MonthNum INT
					    ,YearNum INT
					    ,FlightCategory VARCHAR(25)
					    ,PassengerTotal INT
					    ,Miles NUMERIC(5,0)
					    ,DutyType INT
					    ,Rnk INT
					    ,DeptTime DATETIME
					    ,ArrTime DATETIME
					    ,RnkTD INT)
					    

																						

INSERT INTO @CrewStat
SELECT C.CrewCD
	  ,Crew=C.LastName+(CASE WHEN ISNULL(C.FirstName,'')<>'' THEN ', '+C.FirstName ELSE '' END)+' '+ISNULL(C.MiddleInitial,'')
	  ,C.CrewID
	  ,HoursVal=CASE WHEN @PrintType='B' THEN PC.BlockHours ELSE PC.FlightHours END 
	  ,MonthNum=MONTH(PL.ScheduledTM)
	  ,YearNum=YEAR(PL.ScheduledTM)
	  ,FC.FlightCatagoryDescription
	  ,PL.PassengerTotal
	  ,PL.Distance
	  ,PL.DutyTYPE
	  ,Rnk=ROW_NUMBER()OVER(PARTITION BY PL.PoLegID ORDER BY PL.PoLegID)
	  ,DeptTime=PL.OutboundDTTM
	  ,ArrTime=PL.InboundDTTM
      ,RnkTD=ROW_NUMBER()OVER(PARTITION BY CONVERT(DATE,PL.OutboundDTTM) ORDER BY CONVERT(DATE,PL.OutboundDTTM))
	  FROM (SELECT POLogID,CustomerID,FleetID,DepartmentID,LogNum,HomebaseID FROM PostflightMain WHERE IsDeleted=0) PM 
		   INNER JOIN (SELECT ScheduledTM,POLogID,POLegID,FlightCategoryID,Distance,LegNUM,PassengerTotal,DutyTYPE,OutboundDTTM,InboundDTTM FROM PostflightLeg WHERE IsDeleted=0) PL ON PM.POLogID=PL.POLogID 
		   INNER JOIN (SELECT POLegID,CrewID,FlightHours,BlockHours,DutyTYPE FROM PostflightCrew) PC ON PC.POLegID=PL.POLegID
		   INNER JOIN (SELECT CrewID,CrewCD,LastName,FirstName,MiddleInitial FROM Crew WHERE IsDeleted=0 AND IsStatus=1) C ON PC.CrewID=C.CrewID 
		   INNER JOIN (SELECT CrewID FROM @TempCrewDepartmentID WHERE DepartmentID IS NULL) TF ON TF.CrewID = C.CrewID
		   INNER JOIN (SELECT DepartmentID FROM @TempCrewDepartmentID WHERE CrewID IS NULL) TD ON TD.DepartmentID = ISNULL(PM.DepartmentID,999)
		   LEFT JOIN  (SELECT FlightCategoryID,FlightCatagoryDescription,FlightCatagoryCD FROM FlightCatagory)FC ON FC.FlightCategoryID=PL.FlightCategoryID
		   LEFT JOIN (SELECT HomebaseID,HomebaseAirportID FROM Company) CO ON CO.HomebaseID=PM.HomebaseID
		   LEFT JOIN (SELECT AirportID,IcaoID FROM Airport) A ON A.AirportID=CO.HomebaseAirportID
		  WHERE PM.CustomerID=@CustomerID
		   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		   AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')


IF @SuppressActivityCrew=0 AND @IsHomebase=1
BEGIN

INSERT INTO @CrewStat(CrewCD,Crew,MonthNum,YearNum)
SELECT CrewCD,Crew=C.LastName+(CASE WHEN ISNULL(C.FirstName,'')<>'' THEN ', '+C.FirstName ELSE '' END)+' '+ISNULL(C.MiddleInitial,''),MonthNum=MONTH(@DateFrom),YearNum=YEAR(@DateFrom) FROM Crew C		
              INNER JOIN  Company CO ON CO.HomebaseAirportID=C.HomebaseID
              INNER JOIN UserMaster UM ON UM.HomebaseID=CO.HomebaseID AND LTRIM(RTRIM(UM.UserName))=@UserCD
              INNER JOIN (SELECT CrewID FROM @TempCrewDepartmentID WHERE DepartmentID IS NULL) TC ON TC.CrewID=C.CrewID
         WHERE C.IsDeleted=0
           AND C.IsStatus=1
           AND C.CustomerID=@CustomerID
           AND C.CrewID NOT IN (SELECT ISNULL(CrewID,0) FROM @CrewStat)
END
ELSE IF @SuppressActivityCrew=0 AND @IsHomebase=0
BEGIN
INSERT INTO @CrewStat(CrewCD,CREW,MonthNum,YearNum)
SELECT CrewCD,Crew=C.LastName+(CASE WHEN ISNULL(C.FirstName,'')<>'' THEN ', '+C.FirstName ELSE '' END)+' '+ISNULL(C.MiddleInitial,''),MonthNum=MONTH(@DateFrom),YearNum=YEAR(@DateFrom)
    FROM Crew C
         INNER JOIN (SELECT CrewID FROM @TempCrewDepartmentID WHERE DepartmentID IS NULL) TC ON TC.CrewID=C.CrewID
         WHERE C.IsDeleted=0
           AND C.IsStatus=1
           AND C.CustomerID=@CustomerID
           AND C.CrewID NOT IN (SELECT ISNULL(CrewID,0) FROM @CrewStat)
END



DECLARE @CountVal INT=1,@Count INT=12,@Crew VARCHAR(5)=NULL,@FlightCategory VARCHAR(25),
		@MonthNum INT=MONTH(@DATEFROM),@YearNum INT=YEAR(@DATEFROM),@CrewName VARCHAR(70)


WHILE @CountVal <= @Count

BEGIN


	SELECT TOP 1 @CrewName=Crew,@Crew=CREWCD,@FlightCategory=FlightCategory FROM @CrewStat

	INSERT INTO @CrewStat(Crew,CrewCD,MonthNum,YearNum,HoursVal,FlightCategory)
	SELECT  @CrewName,@Crew, @MonthNum,@YearNum,0,@FlightCategory
	SET @MonthNum =MONTH(DATEADD(MONTH,@CountVal, @DATEFROM))
	SET @YearNum  =YEAR(DATEADD(MONTH,@CountVal, @DATEFROM))
    SET @CountVal=@CountVal+1

END

SELECT DISTINCT CrewCD
	  ,Crew
	  ,HoursVal=ISNULL(HoursVal,0)
	  ,FlightCategory=ISNULL(FlightCategory,'ZZZZ')
	  ,TripDays=(CASE WHEN RnkTD=1 THEN DATEDIFF(DAY,DeptTime,ArrTime)+1 ELSE 0 END)
	  ,SegLeg=(CASE WHEN Rnk=1 THEN 1 ELSE 0 END) 
	  ,PassengerTotal=(CASE WHEN Rnk=1 THEN PassengerTotal ELSE 0 END) 
	  ,Miles=(CASE WHEN Rnk=1 THEN Miles ELSE 0 END) 
	  ,DutyType=ISNULL(DutyType,1)
	  ,MonthNum
	  ,YearNum
	  ,GroupVal=1
	  ,Domestic=CASE WHEN DutyType=1 THEN HoursVal ELSE 0 END
	  ,Inertnational=CASE WHEN DutyType=2 THEN HoursVal ELSE 0 END
FROM @CrewStat
ORDER BY YEARNUM,MonthNum

END
         



GO


