IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportTripsheetCheckListInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportTripsheetCheckListInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportTripsheetCheckListInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportTripsheetCheckListInformation
-- Author: SUDHAKAR J
-- Create date: 18 Jun 2012
-- Description: Get Trip Manager Checklist Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT
		TC.CustomerID
		,TC.CheckListCD
		,TCG.CheckGroupCD
		,TC.CheckListDescription
	FROM 
		TripManagerCheckList TC INNER JOIN 
		TripManagerCheckListGroup TCG ON TC.CheckGroupID = TCG.CheckGroupID
	WHERE TC.IsDeleted = 0
	AND TC.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	ORDER BY TC.CustomerID,TC.CheckListCD,TCG.CheckGroupCD
  -- EXEC spGetReportTripsheetCheckListInformation 'UC'

GO


