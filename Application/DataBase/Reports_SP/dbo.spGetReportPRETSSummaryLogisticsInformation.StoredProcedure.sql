IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSSummaryLogisticsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSSummaryLogisticsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
  
CREATE PROCEDURE [dbo].[spGetReportPRETSSummaryLogisticsInformation]  
 @UserCD VARCHAR(30)  
 ,@TripID VARCHAR(30)  
 ,@LegNum VARCHAR(150)  
 ,@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table  
 ,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'  
AS  
BEGIN  
-- =====================================================================  
-- SPC Name: spGetReportPRETSSummaryLogisticsInformation  
-- Author: SINDHUJA.K  
-- Create date: 24 July 2012  
-- Description: Get Preflight Tripsheet Summary information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ======================================================================  
SET NOCOUNT ON  
  
DECLARE @Logistics TABLE (  
 Row_Id INT IDENTITY  
 ,LegNUM INT  
 ,[Description] VARCHAR(100)  
 ,[Phone1] VARCHAR(30)  
 ,[Fax] VARCHAR(30)  
 ,[Freq] VARCHAR(30)  
 ,[UVAir] BIT  
 ,[Fuel] VARCHAR(30)  
 ,[LastPrice] NUMERIC  
 ,[Date] DATE  
 ,[NegPrice] VARCHAR(30)  
 ,[Comments] VARCHAR(MAX)  
 ,[Type] VARCHAR(2)  --Departure FBO  
 ,[IsSecEmpty] BIT  
)  
  
    
INSERT INTO @Logistics (LegNUM,[Description],[Phone1],[Fax],[Freq],[UVAir],[Fuel],[LastPrice],[Date],[NegPrice],[Comments],[Type],[IsSecEmpty])     
 SELECT  PL.LegNUM  
   ,[Description] = ISNULL(FBD.FBOVendor,'')  
   ,[Phone1] = ISNULL(FBD.PhoneNUM1,'')  
   ,[Fax] = ISNULL(FBD.FaxNum,'')  
   ,[Freq] = CASE
         WHEN (FBD.Frequency = '' or  FBD.Frequency is null) then ''
         WHEN (FBD.Frequency not like '%.%' and (FBD.Frequency is not null or FBD.Frequency <> '')) THEN FBD.Frequency + '.0'
         ELSE FBD.Frequency
         END   
   ,[UVAir] = FBD.IsUWAAirPartner  
   ,[Fuel] = ISNULL(FBD.FuelBrand,'')  
   ,[LastPrice] = ISNULL(FBD.LastFuelPrice,0)  
   ,[Date] = ISNULL(FBD.LastFuelDT,NULL)  
   ,[NegPrice] = ISNULL(CONVERT(VARCHAR,FBD.NegotiatedFuelPrice),'')  
   ,[Comments] = ISNULL(FBD.Comments,'')  
   ,[Type] = 'DF'  --Departure FBO  
   ,[IsSecEmpty] = CASE WHEN ISNULL(FBD.FBOVendor,'') = '' AND  
         ISNULL(FBD.PhoneNUM1,'') = '' AND  
         ISNULL(FBD.FaxNum,'') = '' AND  
         ISNULL(FBD.Frequency,'') = '' AND  
         --ISNULL(FBD.IsUWAAirPartner,'') = '' AND  
         ISNULL(FBD.FuelBrand,'') = '' AND  
         ISNULL(FBD.LastFuelPrice,0) = 0 AND  
         ISNULL(FBD.LastFuelDT,NULL) = '' AND  
         ISNULL(FBD.NegotiatedFuelPrice,0) = 0 AND  
         ISNULL(FBD.Comments,'') = ''   
         THEN 1 ELSE 0 END  
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   LEFT OUTER JOIN (  
    SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.*  
    FROM PreflightFBOList PFL  
    INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1  
   ) FBD ON PL.LegID = FBD.LegID  
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   AND PM.TripID = CONVERT(BIGINT,@TripID)  
   AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
  UNION ALL  
 SELECT  PL.LegNUM  
   ,[Description] = ISNULL(FBA.FBOVendor,'')  
   ,[Phone1] = ISNULL(FBA.PhoneNUM1,'')  
   ,[Fax] = ISNULL(FBA.FaxNum,'')  
   ,[Freq] = CASE
         WHEN (FBA.Frequency = '' or  FBA.Frequency is null) then ''
         WHEN (FBA.Frequency not like '%.%' and (FBA.Frequency is not null or FBA.Frequency <> '')) THEN FBA.Frequency + '.0'
         ELSE FBA.Frequency
         END    
   ,[UVAir] = FBA.IsUWAAirPartner  
   ,[Fuel] = ISNULL(FBA.FuelBrand,'')  
   ,[LastPrice] = ISNULL(FBA.LastFuelPrice,0)  
   ,[Date] = ISNULL(FBA.LastFuelDT,NULL)  
   ,[NegPrice] = ISNULL(CONVERT(VARCHAR,FBA.NegotiatedFuelPrice),'')  
   ,[Comments] = ISNULL(FBA.Comments,'')  
   ,[Type] = 'AF'  
   ,[IsSecEmpty] = CASE WHEN ISNULL(FBA.FBOVendor,'') = '' AND  
         ISNULL(FBA.PhoneNUM1,'') = '' AND  
         ISNULL(FBA.FaxNum,'') = '' AND  
         ISNULL(FBA.Frequency,'') = '' AND  
         --ISNULL(FBD.IsUWAAirPartner,'') = '' AND  
         ISNULL(FBA.FuelBrand,'') = '' AND  
         ISNULL(FBA.LastFuelPrice,0) = 0 AND  
         ISNULL(FBA.LastFuelDT,NULL) = '' AND  
         ISNULL(FBA.NegotiatedFuelPrice,0) = 0 AND  
         ISNULL(FBA.Comments,'') = ''   
         THEN 1 ELSE 0 END  
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   LEFT OUTER JOIN (  
    SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.*   
    FROM PreflightFBOList PFL  
    INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1  
   ) FBA ON PL.LegID = FBA.LegID   
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   AND PM.TripID = CONVERT(BIGINT,@TripID)  
   AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
 -- UNION ALL  
 --SELECT PL.LegNUM  
 --  ,[Description] = PH.PreflightHotelName  
 --  ,[Phone1] = PH.PhoneNum1  
 --  ,[Fax] = PH.FaxNum  
 --  ,[Freq] = NULL  
 --  ,[UVAir] = NULL  
 --  ,[Fuel] = NULL  
 --  ,[LastPrice] = NULL  
 --  ,[Date] = NULL  
 --  ,[NegPrice] = ISNULL(CONVERT(VARCHAR,PH.Rate),'')  
 --  ,[Comments] = PH.Comments  
 --  ,[Type] = 'PH'  
 --  ,[IsSecEmpty] = CASE WHEN ISNULL(PH.PreflightHotelName,'') = '' AND  
 --        ISNULL(PH.PhoneNum1,'') = '' AND  
 --        ISNULL(PH.FaxNum,'') = '' AND  
 --        ISNULL(PH.Rate,0) = 0 AND  
 --        ISNULL(PH.Comments,'') = ''   
 --        THEN 1 ELSE 0 END  
 --  FROM PreflightMain PM  
 --  INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
 --  LEFT OUTER JOIN ( SELECT   
 --   PPL.LegID,PPL.PreflightPassengerListID,PPH.PreflightHotelName,PPH.PhoneNum1,PPH.FaxNUM,PPH.Rate,  
 --   PPH.Comments   
 --  FROM PreflightPassengerList PPL   
 --  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID  
 --  LEFT OUTER JOIN Hotel HP ON PPH.HotelID = HP.HotelID  
 --  ) PH ON PL.legID = PH.LegID  
 --  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
 --  AND PM.TripID = CONVERT(BIGINT,@TripID)  
 --  AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
 UNION ALL  
  
 SELECT PL.LegNUM  
   ,[Description] = PT.PreflightTransportName  
   ,[Phone1] = PT.PhoneNum1  
   ,[Fax] = PT.FaxNUMPT  
   ,[Freq] = NULL  
   ,[UVAir] = NULL  
   ,[Fuel] = NULL  
   ,[LastPrice] = NULL  
   ,[Date] = NULL  
   ,[NegPrice] = CASE WHEN ISNULL(PT.NegPrice,'') = '0.00' THEN '' ELSE ISNULL(PT.NegPrice,'') END  
   ,[Comments] = PT.Comments  
   ,[Type] = 'PT'  
   ,[IsSecEmpty] = CASE WHEN ISNULL(PT.PreflightTransportName,'') = '' AND  
         ISNULL(PT.PhoneNum1,'') = '' AND  
         ISNULL(PT.FaxNUMPT,'') = '' AND  
         --ISNULL(PT.NegPrice,'') = ''   
         ( ISNULL(PT.NegPrice,'') = '' OR ISNULL(PT.NegPrice,'') = '0.00' ) AND  
         ISNULL(PT.Comments,'') = ''   
         THEN 1 ELSE 0 END  
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   LEFT OUTER JOIN (  
    SELECT PTL.Comments, PTL.LegID, PTL.PreflightTransportName, PTL.PhoneNum1, [FaxNUMPT] = PTL.FaxNUM,[NegPrice] = PTL.PhoneNum4, T.*     
    FROM PreflightTransportList PTL   
    LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID   
    WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'  
   ) PT ON PL.LegID = PT.LegID  
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   AND PM.TripID = CONVERT(BIGINT,@TripID)  
   AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
 -- UNION ALL  
    
 --SELECT PL.LegNUM  
 --  ,[Description] = CH.PreflightHotelName  
 --  ,[Phone1] = CH.PhoneNum1  
 --  ,[Fax] = CH.FaxNum  
 --  ,[Freq] = NULL  
 --  ,[UVAir] = NULL  
 --  ,[Fuel] = NULL  
 --  ,[LastPrice] = NULL  
 --  ,[Date] = NULL  
 --  ,[NegPrice] = CASE WHEN ISNULL(CONVERT(VARCHAR(25),CH.Rate),'') = '0.00' THEN '' ELSE ISNULL(CONVERT(VARCHAR(25),CH.Rate),'') END  
 --  ,[Comments] = CH.Comments  
 --  ,[Type] = 'CH'  
 --  ,[IsSecEmpty] = CASE WHEN ISNULL(CH.PreflightHotelName,'') = '' AND  
 --        ISNULL(CH.PhoneNum1,'') = '' AND  
 --        ISNULL(CH.FaxNum,'') = '' AND  
 --        ISNULL(CH.Rate,0) = 0 AND  
 --     ISNULL(CH.Comments,'') = ''   
 --        THEN 1 ELSE 0 END  
 --  FROM PreflightMain PM  
 --  INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
 --  LEFT OUTER JOIN (     
 --   SELECT PCLL.LegID,PCH.PreflightHotelName,PCH.PhoneNum1,PCH.FaxNUM,PCH.Rate,PCH.Comments    
 --   FROM PreflightCrewList PCLL   
 --   INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
 --   LEFT OUTER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
 --   WHERE PCLL.DutyType IN ('P', 'S')  
 --  ) CH ON PL.legID = CH.LegID  
 --  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
 --  AND PM.TripID = CONVERT(BIGINT,@TripID)  
 --  AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
  UNION ALL  
 SELECT PL.LegNUM  
   ,[Description] = CT.PreflightTransportName  
   ,[Phone1] = CT.PhoneNum1  
   ,[Fax] = CT.CrewFax  
   ,[Freq] = NULL  
   ,[UVAir] = NULL  
   ,[Fuel] = NULL  
   ,[LastPrice] = NULL  
   ,[Date] = NULL  
   ,[NegPrice] = CT.Rate  
   ,[Comments] = CT.Comments  
   ,[Type] = 'CT'  
   ,[IsSecEmpty] = CASE WHEN ISNULL(CT.PreflightTransportName,'') = '' AND  
         ISNULL(CT.PhoneNum1,'') = '' AND  
         ISNULL(CT.CrewFax,'') = '' AND  
         ( ISNULL(CT.Rate,'') = '' OR ISNULL(CT.Rate,'') = '0.00' ) AND  
         --ISNULL(CT.Rate,'') = '' AND  
         ISNULL(CT.Comments,'') = ''   
         THEN 1 ELSE 0 END  
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   LEFT OUTER JOIN (  
    SELECT  PTL.Comments, PTL.LegID, PTL.PreflightTransportName, PTL.PhoneNum1, [CrewFax] = PTL.FaxNUM, [Rate]=PTL.PhoneNum4, T.*   
    FROM PreflightTransportList PTL   
    LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID   
    WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'  
   ) CT ON PL.LegID = CT.LegID  
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   AND PM.TripID = CONVERT(BIGINT,@TripID)  
   AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
 -- UNION ALL  
 --SELECT PL.LegNUM  
 --  ,[Description] = MH.PreflightHotelName  
 --  ,[Phone1] = MH.PhoneNum1  
 --  ,[Fax] = MH.FaxNum  
 --  ,[Freq] = NULL  
 --  ,[UVAir] = NULL  
 --  ,[Fuel] = NULL  
 --  ,[LastPrice] = NULL  
 --  ,[Date] = NULL  
 --  ,[NegPrice] = ISNULL(CONVERT(VARCHAR,MH.Rate),'')  
 --  ,[Comments] = MH.Comments  
 --  ,[Type] = 'MH'  
 --  ,[IsSecEmpty] = CASE WHEN ISNULL(MH.PreflightHotelName,'') = '' AND  
 --        ISNULL(MH.PhoneNum1,'') = '' AND  
 --        ISNULL(MH.FaxNum,'') = '' AND  
 --        ISNULL(MH.Rate,0) = 0 AND  
 --        ISNULL(MH.Comments,'') = ''   
 --        THEN 1 ELSE 0 END  
 --  FROM PreflightMain PM  
 --  INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
 --  LEFT OUTER JOIN (     
 --   SELECT PCLL.LegID,PCH.PreflightHotelName,PCH.PhoneNum1,PCH.FaxNUM,PCH.Rate,PCH.Comments     
 --   FROM PreflightCrewList PCLL   
 --   INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
 --   LEFT OUTER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
 --   WHERE PCLL.DutyType NOT IN ('P', 'S')  
 --  ) MH ON PL.legID = MH.LegID     
 --  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
 --  AND PM.TripID = CONVERT(BIGINT,@TripID)  
 --  AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
  UNION ALL  
 SELECT PL.LegNUM  
   ,[Description] = DC.CateringName  
   ,[Phone1] = DC.ContactPhone  
   ,[Fax] = DC.ContactFax  
   ,[Freq] = NULL  
   ,[UVAir] = NULL  
   ,[Fuel] = NULL  
   ,[LastPrice] = NULL  
   ,[Date] = NULL  
   ,[NegPrice] = ISNULL(CONVERT(VARCHAR,DC.Cost),'')  
   ,[Comments] = DC.CateringComments  
   ,[Type] = 'DC'  
   ,[IsSecEmpty] = CASE WHEN ISNULL(DC.CateringName,'') = '' AND  
         ISNULL(DC.ContactPhone,'') = '' AND  
         ISNULL(DC.ContactFax,'') = '' AND  
         ISNULL(DC.Cost,0) = 0 AND  
         ISNULL(DC.CateringComments,'') = ''   
         THEN 1 ELSE 0 END  
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   LEFT OUTER JOIN (  
    SELECT PCL.CateringComments, [CateringName] = PCL.ContactName, PCL.ContactPhone, PCL.ContactFax, PCL.Cost, PCL.LegID, C.*   
    FROM PreflightCateringDetail PCL   
    LEFT OUTER JOIN Catering C ON PCL.CateringID = C.CateringID   
    WHERE PCL.ArriveDepart = 'D'  
   ) DC ON PL.LegID = DC.LegID  
     
   WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
   AND PM.TripID = CONVERT(BIGINT,@TripID)  
   AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')  
     
   ORDER BY PL.LegNUM  
--SELECT * FROM @Logistics  
    
  
     
     
     
DECLARE @FormatSettings TABLE (  
 NOGO BIT  
, PASSENG BIT  
, PASSPHONE BIT  
, PASSCNG BIT  
, PASSPURP SMALLINT  
, CREW BIT  
, LEGNOTES SMALLINT  
, TRIPALERTS BIT  
, FBO BIT  
, FBODEP BIT  
, AIRARRIV BIT  
, ANOTES BIT  
, PAXHOTEL BIT  
, PAXTRANS BIT  
, CREWHOTEL BIT  
, CREWTRANS BIT  
, MAINHOTEL BIT  
, DEPCATER BIT  
, OUTINST BIT  
, PAXRPTPOS SMALLINT  
, PASSDEPT BIT  
, DISPATCHNO BIT  
, PURPOSE BIT  
, ZULUTIME BIT  
, CHRMONTH BIT  
--,   NOTESF      SMALLINT  
, LEGNOTESF SMALLINT  
,   DISPATCHIN SMALLINT  
,   DSPTEMAIL BIT  
,   DISPATCHER BIT  
,   DSPTPHONE BIT  
,	DCREWHOTEL BIT
,	PRIMARYMOB BIT
,	BUSPHONE BIT
--,	DMAINHOTEL BIT
)  
INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 13, @TempSettings  
--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161677, 13, 'FBODEP::FALSE'   
  
--SELECT * FROM @FormatSettings  
  
  
DECLARE @FBODEP BIT  
SELECT @FBODEP=FBODEP FROM @FormatSettings  
--SELECT @FBODEP  
  
--SELECT MAINTHOTEL=@MAINTHOTEL, CDNAME=@CDNAME, CREWHOTEL=@CREWHOTEL   
--,CTANAME=@CTANAME,PTANAME=@PTANAME  
  
SELECT * FROM @Logistics   
WHERE [Type] <> CASE WHEN @FBODEP = '1' THEN 'DF' ELSE '' END  
ORDER BY LegNUM  
  
END  
  
--EXEC spGetReportPRETSSummaryLogisticsInformation 'JWILLIAMS_13', 100131807, '1', 10013161677, 'FBODEP::FALSE'  
--EXEC spGetReportPRETSSummaryLogisticsInformation 'JWILLIAMS_13', 100131807, '1', 10013161677, 'FBODEP::TRUE'  
  

GO


