IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotFlightTypeExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotFlightTypeExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  Procedure  [dbo].[spGetReportPOSTPilotFlightTypeExportInformation]         
(     
	@DATEFROM Datetime,        
	@DATETO Datetime ,        
	@CrewCD NVARCHAR(1000)='',        
	@CrewGroupCD  NVARCHAR(1000)='',        
	@AircraftCD NVARCHAR(1000)='' ,         
	@UserCD AS VARCHAR(30),        
	@HomeBaseCD AS NVARCHAR(1000)='' 
)        
AS          
-- ===============================================================================           
-- SPC Name: [spGetReportPOSTPilotFlightTypeExportInformation           
-- Author:  PREMPRAKASH.S           
-- Create date: 01 Aug 2012           
-- Description: Get FlightType information for REPORTS           
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @ShortDT DATETIME = dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO);
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT,
CrewCD nvarchar(10),
LastName nvarchar(30)
)
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
,c.CrewCD
,C.LastName
FROM Crew c
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE c.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1 
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
,c.CrewCD
,C.LastName
FROM Crew c
LEFT OUTER JOIN CrewGroupOrder cGO
ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CustomerID AND C.IsDeleted=0 
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )--AND C.IsStatus=1 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID
,c.CrewCD ---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
,C.LastName
FROM Crew C 
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE C.CustomerID = @customerID AND C.IsDeleted=0 AND C.IsStatus=1 
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )
END
 
 
--SELECT * FROM @TempCrewID
 
 
-----------------------------Crew and Crew Group Filteration---------------------- 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND isStatus=1
DECLARE @TenToMin SMALLINT = 0;

DECLARE @CrewTemp TABLE (CrewID bigint,
CrewCD varchar(10)
,LastName varchar(30));
INSERT INTO @CrewTemp (CrewID, CrewCD, LastName) 
SELECT DISTINCT C.CrewID, C.CrewCD, C.LastName
FROM Crew C
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
WHERE C.CrewID IN (SELECT CrewID FROM @TempCrewID)
--Temp Fleet
 
DECLARE @TempFleet TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
AircraftID BIGINT,
FleetID BIGINT,
TailNum NVARCHAR(10)
--AircraftCD NVARCHAR(10)
);
IF @AircraftCD <> ''
BEGIN
INSERT INTO @TempFleet
SELECT F.AircraftID, F.FleetID ,F.TAILNUM
FROM Fleet F
WHERE F.CustomerID = @CustomerID
AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
(SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@CustomerID AND
AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))))
END
IF @AircraftCD = ''
BEGIN 
INSERT INTO @TempFleet
SELECT F.AircraftID, F.FleetID ,F.TAILNUM
FROM Fleet F
WHERE F.CustomerID = @CustomerID
END
 
--SELECT * FROM @TempFleet
--End Temp Fleet
--DROP TABLE #CREWINFO
SELECT DISTINCT
POC.CREWID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[crewcode] = C.CrewCD 
,[last_name] = C.LastName 
,[type_code] = AC.AircraftCD
,[dutyend] = '0'
,[end] = ''
,[dutytype] = ''
,[total_line] = 'R'
,sum(CASE WHEN POL.IsDutyEnd=1 THEN ISNULL(ROUND(POC.DutyHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID)dutyhrs
,sum(ISNULL(ROUND(POC.BlockHours,1),0)) over (Partition by POC.CREWID, AC.AircraftID)blk_hrs
,sum(ISNULL(ROUND(POC.FlightHours,1),0)) over (Partition by POC.CREWID, AC.AircraftID)flt_hrs
,sum(CASE WHEN POC.DutyTYPE='P' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID)pic_hours
,sum(CASE WHEN POC.DutyTYPE='S' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID)sic_hours
,sum(ISNULL(POC.Night,0)) over (partition by POC.CREWID, AC.AircraftID)night
,SUM(ISNULL(POC.Instrument,0)) over (partition by POC.CREWID, AC.AircraftID)instr
,SUM(ISNULL(POC.TakeOffDay,0)) over (partition by POC.CREWID, AC.AircraftID)tkoff_d
,SUM(ISNULL(POC.TakeOffNight,0)) over (partition by POC.CREWID, AC.AircraftID)tkoff_n
,SUM(ISNULL(POC.LandingDay,0)) over (partition by POC.CREWID, AC.AircraftID)lnding_d
,SUM(ISNULL(POC.LandingNight,0)) over (partition by POC.CREWID, AC.AircraftID)lnding_n
,SUM(ISNULL(POC.ApproachPrecision,0)) over (partition by POC.CREWID, AC.AircraftID)app_p
,SUM(ISNULL(POC.ApproachNonPrecision,0)) over (partition by POC.CREWID, AC.AircraftID)app_n
INTO #CREWINFO
FROM PostflightCrew POC
-- INNER JOIN ( SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = POC.CrewID
INNER JOIN Crew C ON C.CrewID = POC.CrewID
INNER JOIN PostflightLeg POL ON POC.POLegID = POL.POLegID AND POL.IsDeleted=0
INNER JOIN PostflightMain POM ON POC.POLogID = POM.POLogID AND POM.IsDeleted=0
INNER JOIN Fleet F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE C.IsDeleted=0 
--AND C.IsStatus=1 
AND C.CustomerID = @CustomerID
--AND (AA.AirportID IN(SELECT DISTINCT TOP 1 HomebaseAirportID FROM COMPANY WHERE HOMEBASEID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))))OR @HomeBaseCD=0)
AND 
CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
AND pom.FleetID IN (SELECT FleetID from @TempFleet) AND POC.CUSTOMERID=@CustomerID
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )

IF @CrewCD<>'' OR @CrewGroupCD <>''
BEGIN
SELECT 
CT.CREWID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[crewcode] = CT.CrewCD 
,[last_name] = CT.LastName 
,[type_code] = ISNULL(CI.[type_code],'')
,[dutyend] = CI.[dutyend]
,[end] = CI.[end]
,[dutytype] = CI.[dutytype]
,[total_line] = CI.[total_line]
,ISNULL(CI.dutyhrs,0)dutyhrs
,ISNULL(CI.blk_hrs,0)blk_hrs
,ISNULL(CI.flt_hrs,0)flt_hrs
,ISNULL(CI.pic_hours,0)pic_hours
,ISNULL(CI.sic_hours,0)sic_hours
,ISNULL(CI.night,0)night
,ISNULL(CI.instr,0)instr
,ISNULL(CI.tkoff_d,0)tkoff_d
,ISNULL(CI.tkoff_n,0)tkoff_n
,ISNULL(CI.lnding_d,0)lnding_d
,ISNULL(CI.lnding_n,0)lnding_n
,ISNULL(CI.app_p,0)app_p
,ISNULL(CI.app_n,0)app_n
FROM @TempCrewID CT 
LEFT JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID --AND CT.CrewID IN (SELECT CREWID FROM #CREWINFO)
ORDER BY CrewCD
END 
IF @CrewCD='' AND @CrewGroupCD=''
BEGIN
SELECT 
CT.CREWID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[crewcode] = CT.CrewCD 
,[last_name] = CT.LastName 
,[type_code] = ISNULL(CI.[type_code],'')
,[dutyend] = CI.[dutyend]
,[end] = CI.[end]
,[dutytype] = CI.[dutytype]
,[total_line] = CI.[total_line]
,ISNULL(CI.dutyhrs,0)dutyhrs
,ISNULL(CI.blk_hrs,0)blk_hrs
,ISNULL(CI.flt_hrs,0)flt_hrs
,ISNULL(CI.pic_hours,0)pic_hours
,ISNULL(CI.sic_hours,0)sic_hours
,ISNULL(CI.night,0)night
,ISNULL(CI.instr,0)instr
,ISNULL(CI.tkoff_d,0)tkoff_d
,ISNULL(CI.tkoff_n,0)tkoff_n
,ISNULL(CI.lnding_d,0)lnding_d
,ISNULL(CI.lnding_n,0)lnding_n
,ISNULL(CI.app_p,0)app_p
,ISNULL(CI.app_n,0)app_n
FROM @TempCrewID CT 
INNER JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID --AND CT.CrewID IN (SELECT CREWID FROM #CREWINFO)
ORDER BY CrewCD
END 

IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO 	

--SELECT * FROM #CREWINFO         
--EXEC spGetReportPOSTPilotFlightTypeExportInformation '2009/12/1','2010/1/1','','','','supervisor_99',''


GO


