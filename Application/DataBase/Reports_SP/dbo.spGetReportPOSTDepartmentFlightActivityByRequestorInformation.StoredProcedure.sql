IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentFlightActivityByRequestorInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentFlightActivityByRequestorInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



 CREATE Procedure [dbo].[spGetReportPOSTDepartmentFlightActivityByRequestorInformation]
        @YEAR  AS INT,  
         @UserCD AS VARCHAR(30),
         @DepartmentCD AS VARCHAR(30)='',
		 @TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		 @FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		 @IsHomebase As bit=0

				
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentFlightActivityByRequestorInformation
-- Author:  D.Mullai
-- Create date: 13-08-2012
-- Description: Get Postflight DepartmentFlight Activity By Requestor for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- spGetReportPOSTDepartmentFlightActivityByRequestorInformation 2009,'jwilliams_11','','','',0  
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================


DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt=1
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

create table #deptemp(TAILNUM varchar(9),POLogID BIGINT,POLEgID BIGINT,DepartmentCD varchar(8),DepartmentName varchar(25),PassengerName varchar(63),PassengerRequestorCD varchar(5),
tenth int,DEPT VARCHAR(60),jan numeric(6,3),feb numeric(6,3),mar numeric(6,3),apr numeric(6,3),may numeric(6,3),jun numeric(6,3),jul numeric(6,3),aug numeric(6,3)
,sep numeric(6,3),oct numeric(6,3),nov numeric(6,3),dece numeric(6,3))


create table #deptemp1(TAILNUM varchar(9),POLogID BIGINT,POLEgID BIGINT,DepartmentCD varchar(8),DepartmentName varchar(25),PassengerName varchar(63),PassengerRequestorCD varchar(5),
tenth int,DEPT VARCHAR(60),jan numeric(6,3),feb numeric(6,3),mar numeric(6,3),apr numeric(6,3),may numeric(6,3),jun numeric(6,3),jul numeric(6,3),aug numeric(6,3)
,sep numeric(6,3),oct numeric(6,3),nov numeric(6,3),dece numeric(6,3))

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------	

DECLARE @StartDate DATE     
SET @StartDate= CAST(RTRIM(Convert(varchar(10),@YEAR)) AS DATETIME) 
insert into #deptemp

SELECT DISTINCT F.TAILNUM,PM.POLogID,PL.POLegID,D.DepartmentCD,D.DepartmentName,P.PassengerName,P.PassengerRequestorCD,@TenToMin tenth,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM))= DATEPART(month,@StartDate) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate)) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate)) jan,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) =(DATEPART(month,@StartDate)+1) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+1) feb,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+2) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+2) mar,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+3) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+3) apr,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+4) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+4) may,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+5) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+5) jun,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+6) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+6) jul,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+7) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+7) aug,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+8) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+8) sep,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+9) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+9) oct,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+10) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+10) nov,
       (SELECT CASE WHEN (DATEPART(month,PL.ScheduledTM)) = (DATEPART(month,@StartDate)+11) AND (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) THEN (SUM(ROUND(PL.FlightHours,1))) END  WHERE (DATEPART(YEAR,PL.ScheduledTM))= DATEPART(YEAR,@StartDate) AND DATEPART(month,pl.ScheduledTM)= (DATEPART(month,@StartDate))+11) dece

FROM PostflightLeg PL 

         INNER JOIN PostflightMain PM on PL.POLogID = PM.POLogID  AND PM.IsDeleted=0 
         INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
        INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.HomeBaseID
		FROM Fleet F 
		left outer JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		left outer JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		left outer JOIN Department D on PL.DepartmentID = D.DepartmentID  
		LEFT OUTER JOIN Passenger P on PL.PassengerRequestorID = P.PassengerRequestorID
		left outer join Company c on c.HomebaseID=PM.HomebaseID  
 

WHERE Pl.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
  AND PL.IsDeleted=0
				 --AND Pl.ScheduledTM BETWEEN CONVERT(DATE,@StartDate) AND CONVERT(DATE,@EndDate)	 
				 AND (DATEPART(YEAR,Pl.ScheduledTM) = DATEPART(YEAR,@StartDate)) 
				 AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
                -- AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
                 AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
                 and (PL.PassengerRequestorID is not null or PL.DepartmentID is Not null)

   GROUP BY F.TAILNUM,PM.POLogID,PL.POLegID,D.DepartmentCD,D.DepartmentName,P.PassengerName,P.PassengerRequestorCD,pl.ScheduledTM



IF @TailNUM <> '' OR @FleetGroupCD <> ''

BEGIN
INSERT INTO #DEPTEMP1
SELECT  null,null,null,D.DepartmentCD,D.DepartmentName,null,null,@TenToMin tenth,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
null,null,null,null,null,null,null,null,null,null,null,null
FROM Department d
LEFT OUTER JOIN PostflightMain PM on D.DepartmentID = PM.DepartmentID AND PM.IsDeleted=0 
INNER JOIN  (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
END
-------------------------uncommented
ELSE
BEGIN
INSERT INTO #DEPTEMP1
SELECT DISTINCT null,null,null,D.DepartmentCD,D.DepartmentName,null,null,@TenToMin tenth,isnull(D.DepartmentCD,'-UNALLOCATED DEPT') DEPT,
null,null,null,null,null,null,null,null,null,null,null,null
FROM Department d
LEFT OUTER JOIN PostflightMain PM on D.DepartmentID = PM.DepartmentID AND PM.IsDeleted=0
WHERE d.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and d.IsDeleted=0 and d.IsInActive=0
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
END
-------------------------uncommented
DELETE FROM #DEPTEMP1 WHERE DepartmentCD in(SELECT DISTINCT DepartmentCD FROM #DEPTEMP)

IF @IsZeroSuppressActivityDeptRpt = 0
BEGIN        
SELECT * FROM #DEPTEMP1
UNION ALL           
SELECT * FROM #DEPTEMP
ORDER BY PassengerName
END
ELSE
BEGIN
SELECT * FROM #DEPTEMP
ORDER BY PassengerName
END

IF OBJECT_ID('tempdb..#deptemp') IS NOT NULL
DROP TABLE #deptemp 	

IF OBJECT_ID('tempdb..#deptemp1') IS NOT NULL
DROP TABLE #deptemp1	


 --WHERE (Q.total IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
 --                AND (Q.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
 --                AND (Q.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
--order by dept
-- spGetReportPOSTDepartmentFlightActivityByRequestorInformation 2009,'jwilliams_11','','','',0  



GO


