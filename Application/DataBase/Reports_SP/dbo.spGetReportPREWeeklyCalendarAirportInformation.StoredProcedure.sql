IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendarAirportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendarAirportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendarAirportInformation]
 @UserCD VARCHAR(50)-- MANDATORY
	,@DATEFROM DATETIME --MANDATORY
	,@DATETO DATETIME --MANDATORY
	,@TailNum VARCHAR(500) = ''--OPTIONAL
	,@FleetGroupCD VARCHAR(500) = ''--OPTIONAL
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPREWeeklyCalendarAirportInformation
-- Author: AISHWARYA.M
-- Create date: 28 Aug 2012
-- Description: Get Preflight Weekly CalendarII Legend information for  Report
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

DECLARE @DayCount INT
DECLARE @WeekDayCount INT
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

SET @DayCount=(SELECT DATEPART(WEEKDAY,@DATEFROM-1)) 

SET @WeekDayCount=(SELECT DATEPART(WEEKDAY,@DATETO)) 

----------------------------TailNum and Fleet Group Filteration----------------------
--DROP TABLE #TempFleetID
CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> '' AND @FleetGroupCD ='' 
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END


DECLARE @ToDate DATE

SET @DayCount=( 7-((DATEDIFF(DAY,@DateFrom,@DateTo)+1)%7)) 



SET @ToDate=(SELECT CASE  WHEN @DayCount=7 THEN @DateTo ELSE @DateTo+@DayCount END )  

	SELECT DISTINCT A.ICAOID, A.AIRPORTNAME
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PM.TRIPID = PL.TRIPID AND PM.IsDeleted = 0
		INNER JOIN Airport A ON A.AirportID = PL.DepartICAOID
		INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
	 INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			WHERE (A.AirportID = PL.DepartICAOID OR A.AirportID = PL.ArriveICAOID)
			AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')
			AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			AND PM.TripStatus IN ('T','H')
			AND CONVERT(DATE, PM.EstDepartureDT, 101) BETWEEN 
			CONVERT(DATE, @DATEFROM, 101) AND CONVERT(DATE, @ToDate, 101)
			AND PL.IsDeleted = 0
				
END
	--EXEC spGetReportPREWeeklyCalendarAirportInformation 'JWILLIAMS_11', '2012-09-01', '2012-09-07','N46F',''



GO


