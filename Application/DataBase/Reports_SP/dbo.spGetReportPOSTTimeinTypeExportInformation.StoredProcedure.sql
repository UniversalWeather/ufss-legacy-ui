IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTimeinTypeExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTimeinTypeExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTTimeinTypeExportInformation]
@UserCD AS VARCHAR(30),  
@AsOf Datetime,--Mandatory  
@CrewCD AS NVARCHAR(1000) = '',  
@CrewGroupCD AS NVARCHAR(1000) = '',  
@AircraftCD AS NVARCHAR(1000) = ''  
-- ===============================================================================  
-- SPC Name: spGetReportPOSTTimeinTypeExportInformation  
-- Author:Badrinath  
-- Create date: 03 August 2012  
-- Description: Get Time in Type Information for REPORTS  
-- Revision History  
-- Date Name Ver Change  
--  
-- ================================================================================  
  
AS  
SET NOCOUNT ON  
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=0

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END


-----------------------------TailNum and Fleet Group Filteration---------------------- 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 0

Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @Flighthrs varchar(30);
SELECT @Flighthrs = (case when (cp.AircraftBasis=1) then 'Block Hours'
when (cp.AircraftBasis=2) then 'Flight Hours' end) FROM Company cp
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @TempFleet TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
AircraftID BIGINT,
FleetID BIGINT,
TailNum NVARCHAR(10)
--AircraftCD NVARCHAR(10)
);
IF @AircraftCD <> ''
      BEGIN
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @Customer
            AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
            (SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@Customer AND
                                                                  AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))))
      END
IF @AircraftCD = ''
      BEGIN 
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @Customer

      END

DECLARE @Flighthrs1 varchar(30);
SELECT @Flighthrs1 = cp.AircraftBasis FROM Company cp
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

CREATE TABLE #TYPEEXPINFO(CrewCode VARCHAR(5),LastName VARCHAR(50),TypeCode VARCHAR(40),TypeDesc VARCHAR(15),PicFlag VARCHAR(10),SicFlag VARCHAR(15),AsOfDt VARCHAR(100),FlightHours VARCHAR(30),
TimeInTypeHrs NUMERIC(9,1),Simulator NUMERIC(7,3),totalhrs NUMERIC(17,10),TenToMin int,Status1 VARCHAR(20),ISSTATUS BIT,ISDELETED BIT)

CREATE TABLE #TYPEEXPINFO1(CrewCode VARCHAR(5),LastName VARCHAR(50),TypeCode VARCHAR(40),TypeDesc VARCHAR(15),PicFlag VARCHAR(10),SicFlag VARCHAR(15),AsOfDt VARCHAR(100),FlightHours VARCHAR(30),
TimeInTypeHrs NUMERIC(9,1),Simulator NUMERIC(7,3),totalhrs NUMERIC(17,10),TenToMin int,Status1 VARCHAR(20),ISSTATUS BIT,ISDELETED BIT)

INSERT INTO #TYPEEXPINFO

SELECT TEMP.CrewCode,TEMP.LastName,TEMP.TypeCode,TEMP.TypeDesc,TEMP.PicFlag,TEMP.SicFlag,TEMP.AsOfDt,temp.FlightHours,
SUM(TEMP.TimeInTypeHrs) TimeInTypeHrs,(TEMP.Simulator) Simulator,(TEMP.TotalHrs) TotalHrs,TEMP.TenToMin,TEMP.Status1,TEMP.IsStatus,TEMP.IsDeleted
FROM
(
select
[CrewCode]			=	C.CREWCD,
[LastName]			=	C.LastName,--PL.ScheduledTM,CR.ASOFDT,
[TypeCode]			=	isnull(AC.AircraftCD,''), 
[TypeDesc]			=	isnull(CR.CrewRatingDescription,''), 
[PicFlag]			= (CASE WHEN (CR.IsPilotinCommand= 1) THEN 'TRUE'
								   WHEN (CR.IsPilotinCommand= 0) THEN 'FALSE' END) , 
[SicFlag]			= (CASE WHEN (CR.IsSecondInCommand= 1) THEN 'TRUE'
								   WHEN (CR.IsSecondInCommand= 0) THEN 'FALSE' END),
[AsOfDt]			= dbo.GetShortDateFormatByUserCD(@UserCD,@asof), 
[FlightHours]		= @Flighthrs,
[TimeInTypeHrs]		= (CASE WHEN (@Flighthrs1=1) AND(CONVERT(DATE,pl.ScheduledTM) BETWEEN ISNULL(CONVERT(DATE,cr.AsOfDT),'') AND CONVERT(DATE,@AsOF)) then ISNULL(ROUND(PC.BlockHours,1),0)
							WHEN (@Flighthrs1=2) AND (CONVERT(DATE,pl.ScheduledTM) BETWEEN ISNULL(CONVERT(DATE,cr.AsOfDT),'') AND CONVERT(DATE,@AsOF)) then ISNULL(ROUND(PC.FlightHours,1),0) else 0 end), 
[Simulator]			= (CASE WHEN (CONVERT(DATE,pl.ScheduledTM) BETWEEN ISNULL(CONVERT(DATE,cr.AsOfDT),'') AND CONVERT(DATE,@AsOF)) then ISNULL(ROUND(PS.FlightHours,1),0) else 0 end), 
[TotalHrs]			= (CASE WHEN (CONVERT(DATE,pl.ScheduledTM) BETWEEN ISNULL(CONVERT(DATE,cr.AsOfDT),'') AND CONVERT(DATE,@AsOF)) then ISNULL(CR.TotalTimeInTypeHrs,0) else 0 end)
--(case when (@Flighthrs1=1) AND(CONVERT(DATE,pl.ScheduledTM) between ISNULL(CONVERT(DATE,cr.AsOfDT),'') and CONVERT(DATE,@AsOF)) then ISNULL(CR.TotalTimeInTypeHrs,0)
-- when (@Flighthrs1=2) AND (CONVERT(DATE,pl.ScheduledTM) between ISNULL(CONVERT(DATE,cr.AsOfDT),'') and CONVERT(DATE,@AsOF)) then ISNULL(CR.TotalTimeInTypeHrs,0) ELSE 0 end) 
,[TenToMin]			= @TenToMin 
 ,[Status1]			=(CASE WHEN (C.IsStatus= 1) THEN 'TRUE'
						   WHEN (C.IsStatus= 0) THEN 'FALSE' END)
,C.IsStatus
,C.IsDeleted
FROM PostflightCrew PC

INNER JOIN ( SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = PC.CrewID
INNER JOIN PostflightMain PM ON PM.POLOgID=PC.POLogID AND PM.IsDeleted=0
INNER JOIN PostflightLeg PL ON PL.POLegID=PC.POLegID AND PL.IsDeleted=0
INNER JOIN CREW C ON C.CrewID=pc.CrewID
LEFT OUTER JOIN PostflightSimulatorLog PS ON PS.CrewID=PL.CrewID
inner JOIN CrewRating CR ON PC.CrewID=CR.CrewID and CR.IsDeleted=0
LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = CR.AircraftTypeID --PM.AircraftID--CR.AircraftTypeID 
LEFT OUTER JOIN Company CP ON CP.HomebaseID=PM.HOMEBASEID 
WHERE C.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))

--AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
GROUP BY C.CrewCD,AC.AircraftCD,CP.AircraftBasis,PL.ScheduledTM,PC.BlockHours,PS.FlightHours,CP.AircraftBasis,CR.AsOfDT,PC.FlightHours,c.LastName,CR.IsSecondInCommand,
CR.IsPilotinCommand,Cr.CrewRatingDescription,CR.TotalTimeInTypeHrs,PS.FlightHours,PM.AircraftID,CR.AircraftTypeID,C.IsDeleted,C.IsStatus)TEMP
GROUP BY TEMP.CrewCode,TEMP.LastName,TEMP.TypeCode,TEMP.TypeDesc,TEMP.PicFlag,TEMP.SicFlag,TEMP.AsOfDt,TEMP.FlightHours,TEMP.TenToMin,TEMP.Simulator,TEMP.TotalHrs,TEMP.IsStatus,TEMP.IsDeleted,TEMP.Status1
 --and c.IsDeleted = 0 
--AND C.IsStatus=1 

INSERT INTO #TYPEEXPINFO1
SELECT distinct
[CrewCode]			= C.CREWCD,
[LastName]			= C.LastName,
[TypeCode]			= NULL, 
[TypeDesc]			= 'NO ITEMS', 
[PicFlag]			= 'FALSE', 
[SicFlag]			= 'FALSE',
[AsOfDt]			= NULL, 
[FlightHours]		= @Flighthrs,
[TimeInTypeHrs]		= NULL, 
[Simulator]			= NULL, 
[totalhrs]			= NULL,
[TenToMin]			= @TenToMin,
[Status1]			='FALSE',
C.IsStatus ,
C.IsDeleted

FROM Crew C 

INNER JOIN ( SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
WHERE C.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))

DELETE FROM #TYPEEXPINFO1 WHERE CrewCode IN(SELECT DISTINCT CrewCode from #TYPEEXPINFO WHERE TypeCode IS NOT NULL)
AND TypeCode IS NULL
DELETE FROM #TYPEEXPINFO1 WHERE ISDELETED=0 AND ISSTATUS=0 AND CrewCode NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))

IF @IsZeroSuppressActivityCrewRpt=0
BEGIN
	SELECT * FROM #TYPEEXPINFO
	UNION ALL
	SELECT * FROM #TYPEEXPINFO1
END
ELSE
BEGIN
	SELECT * FROM #TYPEEXPINFO
END
 
IF OBJECT_ID('tempdb..#TYPEEXPINFO') IS NOT NULL
DROP TABLE #TYPEEXPINFO

IF OBJECT_ID('tempdb..#TYPEEXPINFO1') IS NOT NULL
DROP TABLE #TYPEEXPINFO1 
 
-- EXEC spGetReportPOSTTimeinTypeInformation 'jwilliams_11','2009-5-5','','','' 

-- EXEC spGetReportPOSTTimeinTypeExportInformation 'jwilliams_13','2019-07-01','','',''  

GO


