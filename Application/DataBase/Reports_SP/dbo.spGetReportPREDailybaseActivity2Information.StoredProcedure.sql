IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity2Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity2Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ===============================================================================
-- SPC Name: spGetReportPREDailybaseActivity2Information
-- Author: Abhishek S
-- Create date: 31 Jul 2012
-- Description: Get Daily base Activity2 Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 07/26/2012  ABHISHEK             Logic
-- ================================================================================
CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity2Information]

        @UserCD AS VARCHAR(30), -- Mandatory
		@DATEFROM AS DATETIME,  -- Mandatory
		@DATETO AS DATETIME,    -- Mandatory
		@IcaoID AS VARCHAR(4),  -- Mandatory
		@TripNum AS NVARCHAR(500) = '', -- [Optional ]
		@TailNum AS NVARCHAR(30) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(30) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0
AS

BEGIN
SET NOCOUNT ON

  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
  DECLARE @ParameterDefinition AS NVARCHAR(1000)
        
	--DECLARE @IcaoID_N BIGINT
	--SET @IcaoID_N=(SELECT AirportID FROM Airport WHERE ICAOID=@IcaoID AND IsDeleted = 0)
			
 SET @SQLSCRIPT = '
         SELECT
         [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ ''-'' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) 
		 --[DepartureDT] = CONVERT(varchar(10), PL.DepartureDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[DepartureDT] = dbo.GetShortDateFormatByUserCD(@UserCD, PL.DepartureDTTMLocal)
		,[TripNUM] = PM.TripNUM
		,[LegID] = PL.LegID
		,[TailNUM] = F.TailNUM	
		,[Activity] = CASE WHEN (PL.DepartICAOID = A.AirportID AND A.IcaoID = @IcaoID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END		
		,[PlaceAndTime1] = CASE WHEN (PL.DepartICAOID = A.AirportID AND A.IcaoID = @IcaoID) 
		                        THEN CONVERT(varchar(10),A.IcaoID) + ''  '' + CONVERT(varchar(5), PL.DepartureDTTMLocal , 108)
					            ELSE CONVERT(varchar(10),B.IcaoID) + ''  '' + CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108)END 
		,[Pax] = PL.PassengerTotal
		,[Activity2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND B.IcaoID = @IcaoID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END		
		,[PlaceAndTime2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND A.IcaoID = @IcaoID) 		                   
						        THEN CONVERT(varchar(10),B.IcaoID) + ''  '' + CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108)
					            ELSE CONVERT(varchar(10),A.IcaoID) + ''  '' + CONVERT(varchar(5), PL.DepartureDTTMLocal , 108)END 			  
    	,[Passengers] = PaxNames
		,[OutBound] = PL.OutbountInstruction		
		,[Fuel Load] = CASE WHEN PL.FuelLoad IS NULL THEN 0 ELSE PL.FuelLoad END
		 FROM PreflightMain PM 
			INNER JOIN PreflightLeg PL				 
				  ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
		  	INNER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A
			      ON A.AirportID = PL.DepartICAOID
		    INNER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS B
			      ON B.AirportID = PL.ArriveICAOID								      
			LEFT OUTER JOIN (
					SELECT DISTINCT LegID, PaxNames
						FROM PreflightPassengerList p1
						CROSS APPLY ( 
							SELECT IsNull(P.LastName,'''') + '','' + IsNull(P.FirstName,'''') + '' '' + IsNull(P.MiddleInitial,'''') + ''; '' 
							FROM PreflightPassengerList p2
							INNER JOIN Passenger P ON p2.PassengerID = P.PassengerRequestorID
							WHERE p2.LegID = p1.LegID AND p2.FlightPurposeID is Not NULL
							ORDER BY PassengerFirstName 
							FOR XML PATH('''')  
						) D ( PaxNames )
				) PAX ON PL.LegID = PAX.LegID  
	        INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD 
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
							WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 					
					   ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			WHERE
			PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))	
			AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)							
			AND (PM.TripStatus = ''T'' OR PM.TripStatus = ''S'') 
			AND PM.RecordType <> ''M''
            AND (A.AirportID IN(SELECT AirportID FROM Airport WHERE ICAOID=@IcaoID AND IsDeleted = 0)
            AND PM.ISDELETED = 0
			   OR B.AirportID IN (SELECT AirportID FROM Airport WHERE ICAOID=@IcaoID AND IsDeleted = 0))'	
		 	
   --Construct OPTIONAL clauses
   
    IF @TripNum <> '' BEGIN  
		 SET @SQLSCRIPT = @SQLSCRIPT + ' AND CONVERT(VARCHAR,PM.TripNUM) IN (''' +REPLACE(CASE WHEN RIGHT(@TripNum, 1) = ',' THEN LEFT(@TripNum, LEN(@TripNum) - 1) ELSE @TripNum END, ',', ''', ''') + ''')';
	END  
    IF @TailNum <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';
	END	
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));		
	END	
	SET @SQLSCRIPT=@SQLSCRIPT+'ORDER BY PL.DepartureDTTMLocal'
--  UPDATE PreflightPassengerList SET PassengerFirstName = 'Kennedy', PassengerMiddleName = 'John', PassengerLastName = 'C' WHERE PassengerID = 1 
 -- UPDATE PreflightPassengerList SET PassengerFirstName = 'Shubio', PassengerMiddleName = 'Steve', PassengerLastName = 'M' WHERE PassengerID = 2
 -- UPDATE PreflightPassengerList SET PassengerFirstName = 'Srivastava', PassengerMiddleName = 'Abhishek', PassengerLastName = 'K' WHERE  PassengerID = 3	


END

    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME,@IcaoID AS VARCHAR(4) ,@FleetGroupCD AS NVARCHAR(30)'
	
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO,@IcaoID,@FleetGroupCD
--PRINT @SQLSCRIPT

 -- EXEC spGetReportPREDailybaseActivity2Information 'TIM','2012-10-20','2012-10-22','KHOU','', '', '', 0
 -- EXEC spGetReportPREDailybaseActivity2Information 'JWILLIAMS_11','2012-08-31','2012-09-07','KDAL',''



GO


