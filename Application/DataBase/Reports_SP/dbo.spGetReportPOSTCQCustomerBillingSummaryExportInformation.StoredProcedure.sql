IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCQCustomerBillingSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCQCustomerBillingSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTCQCustomerBillingSummaryExportInformation](@UserCD VARCHAR(30)
										,@UserCustomerID BIGINT
										,@DateFrom DATETIME
										,@DateTo DATETIME
										,@CQCustomerCD VARCHAR(200)='')
AS
-- =============================================
-- SPC Name:spGetReportPOSTCQCustomerBillingSummaryExportInformation
-- Author: Askar
-- Create date: 23 Feb 2013
-- Description: Get Charter Quote Details based on CustomerName
-- Revision History
-- Date		Name		Ver		Change
-- =============================================
BEGIN
SELECT DISTINCT custcode=CC.CQCustomerCD
			   ,custname=CF.CQCustomerName
			   ,estdepdt=CONVERT(DATE,Post.EstDepartureDT)
			   ,quotedt=CF.QuoteDT
			   ,cqfilenum=CF.FileNUM
			   ,quotenum=CM.QuoteNUM
			   ,totquote=CM.QuoteTotal
			   ,livetrip='T'
			   ,orig_nmbr=PM.TripNUM
			   ,leadsrcid=LS.LeadSourceCD
			   ,leadsrcnm=LS.LeadSourceDescription
			   ,tripdate=Post.EstDepartureDT
			   ,type_code=AT.AircraftCD
			   ,tripdesc=PM.TripDescription
			   ,lognum=Post.LogNum
			   ,tail_nmbr=F.TailNum
			   ,pax_total=ISNULL(Post.Cnt,0)
			   ,distance=ISNULL(Post.Distance,0)
		
         FROM CQFile CF INNER JOIN CQMain CM ON CM.CQFileID = CF.CQFileID
                        INNER JOIN LeadSource LS ON LS.LeadSourceID = CF.LeadSourceID
                        INNER JOIN CQLeg CL ON CL.CQMainID = CM.CQMainID
                        LEFT OUTER JOIN Fleet F ON F.FleetID = CM.FleetID
                        LEFT OUTER JOIN CQCustomer CC ON CC.CQCustomerID = CF.CQCustomerID
                        LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
                        INNER JOIN PreflightMain PM ON PM.TripID=CM.TripID AND PM.IsDeleted=0 
                        INNER JOIN (SELECT PM.POLogID,PM.LogNum,PL1.Distance,TripID,EstDepartureDT,COUNT(DISTINCT PassengerID) Cnt FROM PostflightMain PM 
																																			INNER JOIN(SELECT SUM(Distance)Distance,POLogID FROM  PostflightLeg WHERE CustomerID=@UserCustomerID GROUP BY POLogID) PL1 ON PM.POLogID=PL1.POLogID
																																			INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
																																			LEFT OUTER JOIN  PostflightPassenger PP ON PP.POLegID=PL.POLegID
																																		WHERE PM.CustomerID=@UserCustomerID
																																		GROUP BY PM.POLogID,PM.LogNum,PM.TripID,PL1.Distance,EstDepartureDT
                                         )Post ON Post.TripID=PM.TripID 
                         INNER JOIN PostflightMain POM ON POM.TripID=PM.TripID
	

WHERE CONVERT(DATE,POM.EstDepartureDT)BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
	AND CF.CustomerID=@UserCustomerID
	AND (CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
	ORDER BY CF.CQCustomerName

END

-------EXEC spGetReportPOSTCQCustomerBillingSummaryExportInformation 'SUPERVISOR_16',10016,'2007-10-01','2007-10-01',''





GO


