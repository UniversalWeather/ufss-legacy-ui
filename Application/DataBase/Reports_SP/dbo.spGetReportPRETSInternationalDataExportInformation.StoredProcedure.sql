IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSInternationalDataExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSInternationalDataExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spGetReportPRETSInternationalDataExportInformation]  
  @UserCD            AS VARCHAR(30)      
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
 ,@BeginDate         AS DATETIME = NULL --      PreflightLeg.DepartureDTTMLocal  
 ,@EndDate           AS DATETIME = NULL --      PreflightLeg.ArrivalDTTMLocal   
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
AS  
BEGIN  
-- =====================================================================================  
-- SPC Name: spGetReportPRETSInternationalDataExportInformation  
-- Author: AISHWARYA.M  
-- Create date:03 OCT 2012  
-- Description: Get Preflight Tripsheet International Data Export information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ======================================================================================  
  
 -- [Start] Report Criteria --  
 DECLARE @tblTripInfo AS TABLE (  
 TripID BIGINT  
 , LegID BIGINT  
 , PassengerID BIGINT  
 )  
  
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
 -- [End] Report Criteria --  
SELECT TEMP.* FROM 
  ( SELECT DISTINCT [orig_nmbr]= PM.TripNUM  
    ,[dispatchno]= PM.DispatchNUM  
    ,[creworpax]= 'C'  
    ,[crewcode]= C.CrewCD  
    ,[last_name]= C.LastName  
    ,[first_name]= C.FirstName  
    ,[dob]= dbo.GetShortDateFormatByUserCD(@UserCD,C.BirthDT) 
    ,[nation]= PP.CountryCD  
    ,[lic_num]= C.PilotLicense1  
    ,[addr1]= C.Addr1  
    ,[addr2]= C.Addr2  
    ,[city]= C.CityName  
    ,[state]= C.StateName  
    ,[zipcode]= C.PostalZipCD  
    ,[ppnum]= ISNULL(PP.PassportNum, '')
    --,[ppnum]= dbo.FlightPakDecrypt (ISNULL(PP.PassportNum, ''))  
    ,[ppexpire]= dbo.GetShortDateFormatByUserCD(@UserCD,PP.PassportExpiryDT) 
    --[Passenger Information]  
    ,[paxcode]= NULL  
    ,[ppaxname]= NULL  
    ,[plast_name]= NULL  
    ,[pfirst_name]= NULL  
    ,[pmiddleinit]= NULL  
    ,[pdob]= NULL  
    ,[p_ppnum]= NULL  
    ,[p_ppexpire]= NULL 
    ,[p_nation]= NULL  
    ,[C_VISA_INFO] = CV.VISAList
    ,[P_VISA_INFO]=''
    FROM (SELECT DISTINCT TripID FROM @tblTripInfo) Main  
    INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
    INNER JOIN (  
     SELECT DISTINCT PL.TRIPID, PCL.CREWID, PCL.PassportID FROM @tblTripInfo PL   
     INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
    ) CL ON PM.TRIPID = CL.TRIPID  
	INNER JOIN Crew C ON CL.CrewID = C.CrewID  
    LEFT OUTER JOIN(SELECT DISTINCT CO.CountryCD,PassportExpiryDT,PassportNum, PL.TRIPID,PL.LegID, PCL.CREWID, PCL.PassportID,RANK() OVER (PARTITION BY   PCL.CrewID ORDER BY PCL.LegID   ASC) Rnk FROM @tblTripInfo PL   
    INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
    LEFT OUTER JOIN CrewPassengerPassport CPP ON PCL.CrewID = CPP.CrewID AND PCL.PassportID = CPP.PassportID
    LEFT OUTER JOIN Country CO ON CPP.CountryID = CO.CountryID 
                      WHERE CPP.PassportID IS NOT NULL)PP ON PP.TripID=CL.TripID AND PP.CrewID=C.CrewID AND Rnk=1
    
    LEFT OUTER JOIN (
		--Visa 1: IN , #7879, 01/01/24
    	SELECT DISTINCT PL2.TripID, PC2.CrewID, VISAList = (
			--SELECT 'Visa ' + CONVERT(VARCHAR,RANK() OVER (PARTITION BY CPV.CrewID ORDER BY CPV.CrewID)) + ':' 
			--	+ CO.CountryCD + ', #' +  dbo.FlightPakDecrypt(CPV.VisaNum) + ', ' 
			--	+ dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT) + '###'
				--CONVERT(VARCHAR(10),CPV.ExpiryDT,101)
					SELECT 'Visa ' + CONVERT(VARCHAR,RANK() OVER (PARTITION BY CPV.CrewID ORDER BY CPV.CrewID)) + ':' 
				+ CO.CountryCD + ', #' +  ISNULL(CPV.VisaNum,'') + ', ' 
				+ dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT) + '###'
			FROM PreflightLeg PL
			INNER JOIN Crew C ON PL.CustomerID = C.CustomerID
			INNER JOIN CrewPassengerVisa CPV ON C.CrewID = CPV.CrewID
			INNER JOIN Country CO ON CPV.CountryID = CO.CountryID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
			WHERE PL.LegID = PL2.LegID AND C.CrewID = PC2.CrewID
			AND ISNULL(CPV.IsDeleted, 0) = 0
			AND (CPV.CountryID = AA.CountryID OR CPV.CountryID = AD.CountryID)
			FOR XML PATH('')
		)
		FROM PreflightLeg PL2
		INNER JOIN PreflightCrewList PC2 ON PL2.LegID = PC2.LegID
		WHERE PL2.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		
    ) CV ON Main.TripID = CV.TripID AND C.CrewID = CV.CrewID
	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM( @UserCD))  
  
UNION ALL  

  SELECT DISTINCT  [orig_nmbr]= PM.TripNUM  
    ,[dispatchno]= PM.DispatchNUM  
    ,[creworpax]= 'P'  
    ,[crewcode]= NULL  
    ,[last_name]= NULL  
    ,[first_name]= NULL  
    ,[dob]= NULL  
    ,[nation]= NULL  
    ,[lic_num]= NULL  
    ,[addr1]= NULL  
    ,[addr2]= NULL  
    ,[city]= NULL  
    ,[state]= NULL  
    ,[zipcode]= NULL  
    ,[ppnum]= NULL  
    ,[ppexpire]= NULL  
    --[Passenger Information]  
    ,[paxcode]= P.PassengerRequestorCD  
    ,[ppaxname]= P.LastName + ',' + P.FirstName  
    ,[plast_name]= P.LastName  
    ,[pfirst_name]= P.FirstName  
    ,[pmiddleinit]= P.MiddleInitial  
    ,[pdob]= dbo.GetShortDateFormatByUserCD(@UserCD,P.DateOfBirth)   
    ,[p_ppnum]= ISNULL(PP.PassportNUM,'')
   --,[p_ppnum]= dbo.FlightPakDecrypt(ISNULL(PP.PassportNUM,''))   
    ,[p_ppexpire]= dbo.GetShortDateFormatByUserCD(@UserCD,PP.PassportExpiryDT)     
    ,[p_nation]= PP.CountryCD  
    ,[C_VISA_INFO]=''
    ,[P_VISA_INFO]=PV.VISAList
    FROM (SELECT DISTINCT TRIPID, PassengerID FROM @tblTripInfo WHERE PassengerID IS NOT NULL) Main  
    INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
    LEFT OUTER JOIN Passenger P ON Main.PassengerID = P.PassengerRequestorID 
     LEFT OUTER JOIN(SELECT DISTINCT  CO.CountryCD,PassportExpiryDT,CPP.PassportNum, PL.TRIPID,PL.LegID, PCL.PassengerID, PCL.PassportID,RANK() OVER (PARTITION BY  PCL.PassengerID ORDER BY PCL.LegID   ASC) Rnk FROM @tblTripInfo PL   
    LEFT OUTER JOIN PreflightPassengerList PCL ON PL.LegID = PCL.LegID  
    LEFT OUTER JOIN CrewPassengerPassport CPP ON PCL.PassengerID = CPP.PassengerRequestorID AND PCL.PassportID = CPP.PassportID
    LEFT OUTER JOIN Country CO ON CPP.CountryID = CO.CountryID 
                      WHERE CPP.PassportID IS NOT NULL)PP ON PP.PassengerID=MAIN.PassengerID AND PP.TripID=Main.TripID AND Rnk=1
    LEFT OUTER JOIN (
		--Visa 1: IN , #7879, 01/01/24
    	SELECT DISTINCT PL2.TripID, PP2.PassengerID, VISAList = (
			--SELECT 'Visa ' + CONVERT(VARCHAR,RANK() OVER (PARTITION BY CPV.PassengerRequestorID ORDER BY CPV.PassengerRequestorID)) + ':' 
			--	+ CO.CountryCD + ', #' +  dbo.FlightPakDecrypt(CPV.VisaNum) + ', ' 
			--	+ dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT) + '###'
				--CONVERT(VARCHAR(10),CPV.ExpiryDT,101)
					SELECT 'Visa ' + CONVERT(VARCHAR,RANK() OVER (PARTITION BY CPV.PassengerRequestorID ORDER BY CPV.PassengerRequestorID)) + ':' 
				+ CO.CountryCD + ', #' +  ISNULL(CPV.VisaNum,'') + ', ' 
				+ dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT) + '###'
			FROM PreflightLeg PL
			INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
			INNER JOIN CrewPassengerVisa CPV ON PP.PassengerID = CPV.PassengerRequestorID
			INNER JOIN Country CO ON CPV.CountryID = CO.CountryID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
			WHERE PL.LegID = PL2.LegID AND PP.PassengerID = PP2.PassengerID
			AND ISNULL(CPV.IsDeleted, 0) = 0
			AND (CPV.CountryID = AA.CountryID OR CPV.CountryID = AD.CountryID)
			FOR XML PATH('')
		)
		FROM PreflightLeg PL2
		INNER JOIN PreflightPassengerList PP2 ON PL2.LegID = PP2.LegID
		WHERE PL2.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		
    ) PV ON Main.TripID = PV.TripID AND P.PassengerRequestorID = PV.PassengerID     
    WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) )TEMP
   ORDER BY dispatchno,creworpax    
  
END  
--EXEC spGetReportPRETSInternationalDataExportInformation 'supervisor_99','1992','','','','','','','','','','','','','','','','','',''  






 --SELECT DISTINCT TOP 1 dbo.FlightPakDecrypt (ISNULL(CPP.PassportNum, '')), ,PL.TRIPID, PCL.CREWID, PCL.PassportID,RANK() OVER (PARTITION BY  PCL.LegID ORDER BY PCL.LegID   ASC) Rnk FROM @tblTripInfo PL   
 --        INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID  
 --        INNER JOIN PreflightLeg PL1 ON PCL.LegID=PL1.LegID
 --       LEFT OUTER JOIN CrewPassengerPassport CPP ON PCL.CrewID = CPP.CrewID AND PCL.PassportID = CPP.PassportID
 --        LEFT OUTER JOIN Country CO ON CPP.CountryID = CO.CountryID 
 --        WHERE CPP.PassportID IS NOT NULL

GO


