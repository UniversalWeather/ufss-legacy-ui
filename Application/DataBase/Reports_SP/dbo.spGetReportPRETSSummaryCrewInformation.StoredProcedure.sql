IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSSummaryCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSSummaryCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSSummaryCrewInformation]  
 @UserCD VARCHAR(50),  
 @TripID VARCHAR(30),
 @LegNum VARCHAR(150) = '',
 @CrewCD VARCHAR(300) = '' 
 
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRETSSummaryCrewInformation  
-- Author: AISHWARYA.M  
-- Create date: 12 SEP 2012  
-- Description: Get Preflight TripSheet Summary Crew information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================ 


CREATE TABLE #TEMP1(LegID BIGINT,LegNum BIGINT,CrewNames VARCHAR(2000));  
--DECLARE @TripID AS BIGINT = 10000350;  
DECLARE @LEGID AS BIGINT;  
SELECT DISTINCT LEGID INTO #TEMP FROM  PreflightLeg PL WHERE PL.TripID=@TripID 
	AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
SET @LEGID=(SELECT DISTINCT TOP 1 T.LEGID FROM #TEMP T)  

WHILE @LEGID IS NOT NULL   
BEGIN  
INSERT INTO #TEMP1(LegID,LegNum,CrewNames)  
       (
/*       
       SELECT DISTINCT PC2.LegID,PL.LegNUM,CrewNames = (   
        --SELECT DISTINCT LEFT(C.FirstName,1) + '.' + C.LastName + '(' + PC.DutyTYPE + ') '  
        SELECT DISTINCT LEFT(C.FirstName,1) + '.' + C.LastName + '(' +   
		CASE PC.DutyTYPE WHEN 'P' THEN 'PIC'  
		 WHEN 'S' THEN 'SIC' WHEN 'E' THEN 'ENGR' WHEN 'I' THEN 'INST' 
		 WHEN 'A' THEN 'ATND' WHEN 'O' THEN 'OTHER' ELSE '' END  
	   + ') '  
            FROM PreflightCrewList PC  
            INNER JOIN Crew C ON PC.CrewID = C.CrewID  
            INNER JOIN PreflightLeg PL ON PC.LegID = PL.LegID  
            WHERE PL.TripID = @TripID   
            AND PL.LegID IN (@LEGID) 
        FOR XML PATH('')  
      )  
 */
	 SELECT DISTINCT PL.LegID,PL.LegNUM,CrewNames = (   
		SELECT LEFT(C.FirstName,1) + '.' + C.LastName + '(' +   
		CASE PC.DutyTYPE WHEN 'P' THEN 'PIC'  
		 WHEN 'S' THEN 'SIC' WHEN 'E' THEN 'ENGR' WHEN 'I' THEN 'INST' 
		 WHEN 'A' THEN 'ATND' WHEN 'O' THEN 'OTHER' ELSE '' END  
		+ ') '  
			FROM (SELECT PC.LEGID, PC.CrewID, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
					WHEN 'P' THEN 1 WHEN 'S' THEN 2 ELSE 3 END
			  FROM PreflightCrewList PC) PC  
			INNER JOIN Crew C ON PC.CrewID = C.CrewID  
			INNER JOIN PreflightLeg PL ON PC.LegID = PL.LegID  
			WHERE PL.TripID = @TripID 
			AND ((C.CrewCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD , ',')) AND PC.DutyType IN('P','S'))  OR @CrewCD = '') 	   
			AND PL.LegID IN (@LEGID) 
		   order by PC.RowID, PC.DutyTYPE
		FOR XML PATH('')  
	  )
      FROM  PreflightCrewList PC2  
      RIGHT OUTER JOIN PreflightLeg PL ON PC2.LegID = PL.LegID  
      WHERE PL.TripID = CONVERT(BIGINT,@TripID)  
      AND PL.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
      AND (PL.LegID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LEGID , ','))OR @LEGID='')
      )
 
      DELETE FROM #TEMP WHERE LegID IN (@LEGID)  
      SET @LEGID=(SELECT DISTINCT TOP 1 T.LEGID FROM #TEMP T)  
 END  
   SELECT DISTINCT LegID,LegNum,CrewNames FROM #TEMP1  
   
  
  
IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL 
DROP table #TEMP1 

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL 
DROP table #TEMP 
END  
  
--EXEC spGetReportPRETSSummaryCrewInformation 'UC', 10001125, '', '' 
---EXEC spGetReportPRETSSummaryCrewInformation 'SUPERVISOR_99',10099127279, '',''



GO


