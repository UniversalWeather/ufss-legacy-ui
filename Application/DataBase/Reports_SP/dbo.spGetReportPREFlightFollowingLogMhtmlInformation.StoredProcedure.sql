IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFlightFollowingLogMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFlightFollowingLogMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================    
-- SPC Name: spGetReportPREFlightFollowingLogExportInformation   
-- Author: Abhishek S    
-- Create date: 6th Aug 2012    
-- Description: Get Flight Following Export Information     
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================ 
CREATE PROCEDURE [dbo].[spGetReportPREFlightFollowingLogMhtmlInformation]

        @UserCD AS VARCHAR(30), -- Mandatory
        @Date AS DATETIME, --Mandatory
        @TailNum AS NVARCHAR(300) = '', -- [Optional], Comma Delimited String with multiple values
        @FleetGroupCD AS NVARCHAR(300) = '' -- [Optional], Comma Delimited String with multiple values

AS
BEGIN	
	SET NOCOUNT ON;
	
	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
    DECLARE @ParameterDefinition AS NVARCHAR(100)
    
    SET @SQLSCRIPT = '
    SELECT
    [tail_nmbr] = F.TailNum
   ,[orig_nmbr] = PM.TripNUM 
   ,[leg_num] = PL.LegNUM
   ,[legid] = PL.LegID1
   ,[crew] = (SELECT SUBSTRING(C.CrewList,1,LEN(C.CrewList)-1)) 
   ,[dtlocdep] = PL.DepartureDTTMLocal
   ,[locdep] = PL.DepartureDTTMLocal
   ,[dtgmtdep] = PL.DepartureGreenwichDTTM
   ,[gmtdep] = PL.DepartureGreenwichDTTM
   ,[dtlocarr] = PL.ArrivalDTTMLocal
   ,[locarr] = PL.ArrivalDTTMLocal
   ,[dtgmtarr] = PL.ArrivalGreenwichDTTM
   ,[gmtarr] =PL.ArrivalGreenwichDTTM
   ,[depicao_id] = (SELECT IcaoID FROM Airport WHERE AirportID=PL.DepartICAOID)
   ,[arricao_id] = (SELECT IcaoID FROM Airport WHERE AirportID=PL.ArriveICAOID)
   ,[ac_code] = F.AircraftCD

	
	   FROM PreflightMain PM
       INNER JOIN PreflightLeg PL
       ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
			INNER JOIN ( SELECT DISTINCT PC2.LEGID, CrewList = (
                 SELECT C.CrewCD + '', ''  
				 FROM PreflightCrewList PC1 INNER JOIN Crew C ON PC1.CrewID = C.CrewID 
				   WHERE PC1.LEGID = PC2.LEGID 
				   ORDER BY C.CrewCD 
                   FOR XML PATH('''')    
            )
            FROM PreflightCrewList PC2
		) C ON PL.LEGID = C.LEGID 
       INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID	
			WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 					
	   ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN(SELECT AircraftID, AircraftCD FROM Aircraft)AT
       ON AT.AircraftID = F.AircraftID 
	   
	   WHERE PM.IsDeleted = 0
	     AND PM.TripStatus = ''T''
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	     AND CONVERT(DATE,PL.DepartureDTTMLocal) = convert(DATE,@Date)
	 '    
	     
-- Optional Clauses

    IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END		  
	
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATE AS DATETIME, @FleetGroupCD AS VARCHAR(500)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATE, @FleetGroupCD 
    
END

-- EXEC spGetReportPREFlightFollowingLogMhtmlInformation 'JWILLIAMS_13','2012-06-4','',''


GO


