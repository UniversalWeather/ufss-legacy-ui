IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostCostTrackingBudgetSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostCostTrackingBudgetSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPostCostTrackingBudgetSummary]
    ( 
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
	@TailNUM AS NVARCHAR(1000) = '',
	@FiscalYear AS NVARCHAR(4)
	)

AS
BEGIN

SET NOCOUNT ON;

DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID


IF @MonthStart=1
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       JAN=ISNULL(BU.Month1Budget,0.00),
       FEB=ISNULL(BU.Month2Budget,0.00),
       MAR=ISNULL(BU.Month3Budget,0.00),
       APR=ISNULL(BU.Month4Budget,0.00),
       MAY=ISNULL(BU.Month5Budget,0.00),
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       YTD= ISNULL(BU.Month1Budget,0.00)+ ISNULL(BU.Month2Budget,0.00)+ ISNULL(BU.Month3Budget,0.00)+ ISNULL(BU.Month4Budget,0.00)+  ISNULL(BU.Month5Budget,0.00)+ ISNULL(BU.Month6Budget,0.00)+ 
			 ISNULL(BU.Month7Budget,0.00)+ISNULL(BU.Month8Budget,0.00)+  ISNULL(BU.Month9Budget,0.00)+  ISNULL(BU.Month10Budget,0.00)+  ISNULL(BU.Month11Budget,0.00)+ ISNULL(BU.Month12Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear   
 FROM Account AC INNER JOIN Budget BU ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=2
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       FEB=ISNULL(BU.Month2Budget,0.00),
       MAR=ISNULL(BU.Month3Budget,0.00),
       APR=ISNULL(BU.Month4Budget,0.00),
       MAY=ISNULL(BU.Month5Budget,0.00),
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       YTD =ISNULL(BU.Month2Budget,0.00)+  ISNULL(BU.Month3Budget,0.00)+  ISNULL(BU.Month4Budget,0.00)+  ISNULL(BU.Month5Budget,0.00)+
             ISNULL(BU.Month6Budget,0.00)+  ISNULL(BU.Month7Budget,0.00)+  ISNULL(BU.Month8Budget,0.00)+  ISNULL(BU.Month9Budget,0.00)+
             ISNULL(BU.Month10Budget,0.00)+ ISNULL(BU.Month11Budget,0.00)+ ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00),          
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1                 
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0)B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=3
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       MAR=ISNULL(BU.Month3Budget,0.00),
       APR=ISNULL(BU.Month4Budget,0.00),
       MAY=ISNULL(BU.Month5Budget,0.00),
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       YTD = ISNULL(BU.Month3Budget,0.00)+ ISNULL(BU.Month4Budget,0.00)+ ISNULL(BU.Month5Budget,0.00)+ ISNULL(BU.Month6Budget,0.00)+
			 ISNULL(BU.Month7Budget,0.00)+ ISNULL(BU.Month8Budget,0.00)+ ISNULL(BU.Month9Budget,0.00)+ ISNULL(BU.Month10Budget,0.00)+
			 ISNULL(BU.Month11Budget,0.00)+ISNULL(BU.Month12Budget,0.00)+ISNULL(B.Month1Budget,0.00)+   ISNULL(B.Month2Budget,0.00),
	   MonthStart = @MonthStart,
	   MonthEnd = @MonthEnd,  
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1                 
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=4
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       APR=ISNULL(BU.Month4Budget,0.00),
       MAY=ISNULL(BU.Month5Budget,0.00),
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       YTD =  ISNULL(BU.Month4Budget,0.00)+ ISNULL(BU.Month5Budget,0.00)+ ISNULL(BU.Month6Budget,0.00)+ ISNULL(BU.Month7Budget,0.00)+
			  ISNULL(BU.Month8Budget,0.00)+ ISNULL(BU.Month9Budget,0.00)+ ISNULL(BU.Month10Budget,0.00)+ ISNULL(BU.Month11Budget,0.00)+
			  ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00)+ ISNULL(B.Month2Budget,0.00)+   ISNULL(B.Month3Budget,0.00),    
       MonthStart = @MonthStart,   
       MonthEnd = @MonthEnd,   
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1           
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=5
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       MAY=ISNULL(BU.Month5Budget,0.00),
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       YTD = ISNULL(BU.Month5Budget,0.00)+ ISNULL(BU.Month6Budget,0.00)+  ISNULL(BU.Month7Budget,0.00)+ISNULL(BU.Month8Budget,0.00)+
			 ISNULL(BU.Month9Budget,0.00)+ ISNULL(BU.Month10Budget,0.00)+ ISNULL(BU.Month11Budget,0.00)+ ISNULL(BU.Month12Budget,0.00)+
			 ISNULL(B.Month1Budget,0.00)+  ISNULL(B.Month2Budget,0.00)+   ISNULL(B.Month3Budget,0.00)+ ISNULL(B.Month4Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1             
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=6
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       JUN=ISNULL(BU.Month6Budget,0.00),
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       YTD = ISNULL(BU.Month6Budget,0.00)+  ISNULL(BU.Month7Budget,0.00)+   ISNULL(BU.Month8Budget,0.00)+  ISNULL(BU.Month9Budget,0.00)+ 
			 ISNULL(BU.Month10Budget,0.00)+ ISNULL(BU.Month11Budget,0.00)+  ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00)+
			 ISNULL(B.Month2Budget,0.00)+   ISNULL(B.Month3Budget,0.00)+    ISNULL(B.Month4Budget,0.00)+   ISNULL(B.Month5Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=7
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       JUL=ISNULL(BU.Month7Budget,0.00),
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       YTD = ISNULL(BU.Month7Budget,0.00)+  ISNULL(BU.Month8Budget,0.00)+ ISNULL(BU.Month9Budget,0.00)+  ISNULL(BU.Month10Budget,0.00)+
			 ISNULL(BU.Month11Budget,0.00)+ ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00)+   ISNULL(B.Month2Budget,0.00)+
			 ISNULL(B.Month3Budget,0.00)+   ISNULL(B.Month4Budget,0.00)+   ISNULL(B.Month5Budget,0.00)+   ISNULL(B.Month6Budget,0.00),
       MonthStart = @MonthStart,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=8
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       AUG=ISNULL(BU.Month8Budget,0.00),
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       JUL=ISNULL(B.Month7Budget,0.00),
       YTD= ISNULL(BU.Month8Budget,0.00)+  ISNULL(BU.Month9Budget,0.00)+ISNULL(BU.Month10Budget,0.00)+ ISNULL(BU.Month11Budget,0.00)+
			ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00)+ ISNULL(B.Month2Budget,0.00)+   ISNULL(B.Month3Budget,0.00)+
			ISNULL(B.Month4Budget,0.00)+   ISNULL(B.Month5Budget,0.00)+ ISNULL(B.Month6Budget,0.00)+   ISNULL(B.Month7Budget,0.00),
       MonthStart = @MonthStart ,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1             
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=9
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       SEP=ISNULL(BU.Month9Budget,0.00),
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       JUL=ISNULL(B.Month7Budget,0.00),
       AUG=ISNULL(B.Month8Budget,0.00),
       YTD =  ISNULL(BU.Month9Budget,0.00)+  ISNULL(BU.Month10Budget,0.00)+  ISNULL(BU.Month11Budget,0.00)+   ISNULL(BU.Month12Budget,0.00)+
			  ISNULL(B.Month1Budget,0.00)+    ISNULL(B.Month2Budget,0.00)+    ISNULL(B.Month3Budget,0.00)+    ISNULL(B.Month4Budget,0.00)+
			  ISNULL(B.Month5Budget,0.00)+    ISNULL(B.Month6Budget,0.00)+  ISNULL(B.Month7Budget,0.00)+    ISNULL(B.Month8Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=10
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       OCT=ISNULL(BU.Month10Budget,0.00),
       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       JUL=ISNULL(B.Month7Budget,0.00),
       AUG=ISNULL(B.Month8Budget,0.00),  
       SEP=ISNULL(B.Month9Budget,0.00),
       YTD =  ISNULL(BU.Month10Budget,0.00)+  ISNULL(BU.Month11Budget,0.00)+  ISNULL(BU.Month12Budget,0.00)+  ISNULL(B.Month1Budget,0.00)+
			    ISNULL(B.Month2Budget,0.00)+    ISNULL(B.Month3Budget,0.00)+    ISNULL(B.Month4Budget,0.00)+    ISNULL(B.Month5Budget,0.00)+
			    ISNULL(B.Month6Budget,0.00)+    ISNULL(B.Month7Budget,0.00)+   ISNULL(B.Month8Budget,0.00)+   ISNULL(B.Month9Budget,0.00),
        MonthStart = @MonthStart,
        MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1               
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=11
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 

       NOV=ISNULL(BU.Month11Budget,0.00),
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       JUL=ISNULL(B.Month7Budget,0.00),
       AUG=ISNULL(B.Month8Budget,0.00),
       SEP=ISNULL(B.Month9Budget,0.00),
       OCT=ISNULL(B.Month10Budget,0.00),
       YTD = ISNULL(BU.Month11Budget,0.00)+ ISNULL(BU.Month12Budget,0.00)+ ISNULL(B.Month1Budget,0.00)+  ISNULL(B.Month2Budget,0.00)+
			   ISNULL(B.Month3Budget,0.00)+  ISNULL(B.Month4Budget,0.00)+    ISNULL(B.Month5Budget,0.00)+  ISNULL(B.Month6Budget,0.00)+
			   ISNULL(B.Month7Budget,0.00)+  ISNULL(B.Month8Budget,0.00)+    ISNULL(B.Month9Budget,0.00)+  ISNULL(B.Month10Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,Month10Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE ---IF @MonthStart=11
BEGIN
SELECT AC.AccountNum + '$$' + AC.AccountDescription AS AccountNumber$$AccountDescription, 
       DEC=ISNULL(BU.Month12Budget,0.00),
       JAN=ISNULL(B.Month1Budget,0.00),
       FEB=ISNULL(B.Month2Budget,0.00),
       MAR=ISNULL(B.Month3Budget,0.00),
       APR=ISNULL(B.Month4Budget,0.00),
       MAY=ISNULL(B.Month5Budget,0.00),
       JUN=ISNULL(B.Month6Budget,0.00),
       JUL=ISNULL(B.Month7Budget,0.00),
       AUG=ISNULL(B.Month8Budget,0.00),
       SEP=ISNULL(B.Month9Budget,0.00),
       OCT=ISNULL(B.Month10Budget,0.00),
       NOV=ISNULL(B.Month11Budget,0.00),
       YTD=  ISNULL(BU.Month12Budget,0.00)+  ISNULL(B.Month1Budget,0.00)+  ISNULL(B.Month2Budget,0.00)+    ISNULL(B.Month3Budget,0.00)+
			   ISNULL(B.Month4Budget,0.00)+   ISNULL(B.Month5Budget,0.00)+   ISNULL(B.Month6Budget,0.00)+   ISNULL(B.Month7Budget,0.00)+   
			   ISNULL(B.Month8Budget,0.00)+    ISNULL(B.Month9Budget,0.00)+   ISNULL(B.Month10Budget,0.00)+    ISNULL(B.Month11Budget,0.00),
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,Month10Budget,AccountID,Month11Budget FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END

END

--SELECT  B.AccountID,BG.Month10Budget Month10Budget,B.Month11Budget FROM Budget B 
--                                           LEFT OUTER JOIN (SELECT Month10Budget,Month11Budget,AccountID FROM BudgeT WHERE CustomerID=10099 AND FiscalYear=2018)BG ON BG.AccountID=B.AccountID
--                                           WHERE CustomerID=10099 AND FiscalYear=2017


GO




