IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostCostTrackingBudgetSummaryExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostCostTrackingBudgetSummaryExport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPostCostTrackingBudgetSummaryExport]
    ( 
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
	@TailNUM AS NVARCHAR(1000) = '',
	@FiscalYear AS NVARCHAR(4)
	)

AS
BEGIN

SET NOCOUNT ON;

DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID


IF @MonthStart=1
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month1Budget,0),
       cmon1 = 'Jan',
       lmon1 = 'TRUE',
       mon2=ISNULL(BU.Month2Budget,0),
       cmon2 = 'Feb',
       lmon2 = 'TRUE',
       mon3=ISNULL(BU.Month3Budget,0),
       cmon3 = 'Mar',
       lmon3 = 'TRUE',
       mon4=ISNULL(BU.Month4Budget,0),
       cmon4 = 'Apr',
       lmon4 = 'TRUE',
       mon5=ISNULL(BU.Month5Budget,0),
       cmon5 = 'May',
       lmon5 = 'TRUE',
       mon6=ISNULL(BU.Month6Budget,0),
       cmon6 = 'Jun',
       lmon6 = 'TRUE',
       mon7=ISNULL(BU.Month7Budget,0),
       cmon7 = 'Jul',
       lmon7 = 'TRUE',
       mon8=ISNULL(BU.Month8Budget,0),
       cmon8 = 'Aug',
       lmon8 = 'TRUE',
       mon9=ISNULL(BU.Month9Budget,0),
       cmon9 = 'Sep',
       lmon9 = 'TRUE',
       mon10=ISNULL(BU.Month10Budget,0),
       cmon10 = 'Oct',
       lmon10 = 'TRUE',
       mon11=ISNULL(BU.Month11Budget,0),
       cmon11 = 'Nov',
       lmon11 = 'TRUE',
       mon12=ISNULL(BU.Month12Budget,0),
       cmon12 = 'Dec',
       lmon12 = 'TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear   
 FROM Account AC INNER JOIN Budget BU ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=2
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month2Budget,0),
       cmon1='Feb',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month3Budget,0),
       cmon2='Mar',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month4Budget,0),
       cmon3='Apr',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month5Budget,0),
       cmon4='May',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month6Budget,0),
       cmon5='Jun',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month7Budget,0),
       cmon6='Jul',
       lmon6='TRUE',
       mon7=ISNULL(BU.Month8Budget,0),
       cmon7='Aug',
       lmon7='TRUE',
       mon8=ISNULL(BU.Month9Budget,0),
       cmon8='Sep',
       lmon8='TRUE',
       mon9=ISNULL(BU.Month10Budget,0),
       cmon9='Oct',
       lmon9='TRUE',
       mon10=ISNULL(BU.Month11Budget,0),
       cmon10='Nov',
       lmon10='TRUE',
       mon11=ISNULL(BU.Month12Budget,0),
       cmon11='Dec',
       lmon11='TRUE',
       mon12=ISNULL(B.Month1Budget,0),
       cmon12='Jan',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1                 
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0)B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=3
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month3Budget,0),
       cmon1 = 'Mar',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month4Budget,0),
       cmon2 = 'Apr',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month5Budget,0),
       cmon3 = 'May',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month6Budget,0),
       cmon4 = 'Jun',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month7Budget,0),
       cmon5 = 'Jul',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month8Budget,0),
       cmon6 = 'Aug',
       lmon6='TRUE',
       mon7=ISNULL(BU.Month9Budget,0),
       cmon7 = 'Sep',
       lmon7='TRUE',
       mon8=ISNULL(BU.Month10Budget,0),
       cmon8 = 'Oct',
       lmon8='TRUE',
       mon9=ISNULL(BU.Month11Budget,0),
       cmon9 = 'Nov',
       lmon9='TRUE',
       mon10=ISNULL(BU.Month12Budget,0),
       cmon10 = 'Dec',
       lmon10='TRUE',
       mon11=ISNULL(B.Month1Budget,0),
       cmon11 = 'Jan',
       lmon11='TRUE',
       mon12=ISNULL(B.Month2Budget,0),
       cmon12 = 'Feb',
       lmon12='TRUE',
	   MonthEnd = @MonthEnd,  
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1                 
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=4
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month4Budget,0),
       cmon1 = 'Apr',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month5Budget,0),
       cmon2 = 'May',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month6Budget,0),
       cmon3 = 'Jun',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month7Budget,0),
       cmon4 = 'Jul',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month8Budget,0),
       cmon5 = 'Aug',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month9Budget,0),
       cmon6 = 'Sep',
       lmon6='TRUE',
       mon7=ISNULL(BU.Month10Budget,0),
       cmon7 = 'Oct',
       lmon7='TRUE',
       mon8=ISNULL(BU.Month11Budget,0),
       cmon8 = 'Nov',
       lmon8='TRUE',
       mon9=ISNULL(BU.Month12Budget,0),
       cmon9 = 'Dec',
       lmon9='TRUE',
       mon10=ISNULL(B.Month1Budget,0),
       cmon10 = 'Jan',
       lmon10='TRUE',
       mon11=ISNULL(B.Month2Budget,0),
       cmon11 = 'Feb',
       lmon11='TRUE',
       mon12=ISNULL(B.Month3Budget,0), 
       cmon12 = 'Mar',
       lmon12='TRUE',
       MonthStart = @MonthStart,   
       MonthEnd = @MonthEnd,   
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1           
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=5
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month5Budget,0),
       cmon1 = 'May',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month6Budget,0),
       cmon2 = 'Jun',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month7Budget,0),
       cmon3 = 'Jul',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month8Budget,0),
       cmon4 = 'Aug',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month9Budget,0),
       cmon5 = 'Sep',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month10Budget,0),
       cmon6 = 'Oct',
       lmon6='TRUE',
       mon7=ISNULL(BU.Month11Budget,0),
       cmon7 = 'Nov',
       lmon7='TRUE',
       mon8=ISNULL(BU.Month12Budget,0),
       cmon8 = 'Dec',
       lmon8='TRUE',
       mon9=ISNULL(B.Month1Budget,0),
       cmon9 = 'Jan',
       lmon9='TRUE',
       mon10=ISNULL(B.Month2Budget,0),
       cmon10 = 'Feb',
       lmon10='TRUE',
       mon11=ISNULL(B.Month3Budget,0),
       cmon11 = 'Mar',
       lmon11='TRUE',
       mon12=ISNULL(B.Month4Budget,0), 
       cmon12 = 'Apr',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1             
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER  JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=6
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month6Budget,0),
       cmon1 = 'Jun',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month7Budget,0),
       cmon2 = 'Jul',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month8Budget,0),
       cmon3 = 'Aug',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month9Budget,0),
       cmon4 = 'Sep',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month10Budget,0),
       cmon5 = 'Oct',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month11Budget,0),
       cmon6 = 'Nov',
       lmon6='TRUE',
       mon7=ISNULL(BU.Month12Budget,0),
       cmon7 = 'Dec',
       lmon7='TRUE',
       mon8=ISNULL(B.Month1Budget,0),
       cmon8 = 'Jan',
       lmon8='TRUE',
       mon9=ISNULL(B.Month2Budget,0),
       cmon9 = 'Feb',
       lmon9='TRUE',
       mon10=ISNULL(B.Month3Budget,0),
       cmon10 = 'Mar',
       lmon10='TRUE',
       mon11=ISNULL(B.Month4Budget,0),
       cmon11 = 'Apr',
       lmon11='TRUE',
       mon12=ISNULL(B.Month5Budget,0),
       cmon12 = 'May',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=7
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month7Budget,0),
       cmon1 = 'Jul',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month8Budget,0),
       cmon2 = 'Aug',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month9Budget,0),
       cmon3 = 'Sep',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month10Budget,0),
       cmon4 = 'Oct',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month11Budget,0),
       cmon5 = 'Nov',
       lmon5='TRUE',
       mon6=ISNULL(BU.Month12Budget,0),
       cmon6 = 'Dec',
       lmon6='TRUE',
       mon7=ISNULL(B.Month1Budget,0),
       cmon7 = 'Jan',
       lmon7='TRUE',
       mon8=ISNULL(B.Month2Budget,0),
       cmon8 = 'Feb',
       lmon8='TRUE',
       mon9=ISNULL(B.Month3Budget,0),
       cmon9 = 'Mar',
       lmon9='TRUE',
       mon10=ISNULL(B.Month4Budget,0),
       cmon10 = 'Apr',
       lmon10='TRUE',
       mon11=ISNULL(B.Month5Budget,0),
       cmon11 = 'May',
       lmon11='TRUE',
       mon12=ISNULL(B.Month6Budget,0),
       cmon12 = 'Jun',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=8
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month8Budget,0),
       cmon1 = 'Aug',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month9Budget,0),
       cmon2 = 'Sep',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month10Budget,0),
       cmon3 = 'Oct',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month11Budget,0),
       cmon4 = 'Nov',
       lmon4='TRUE',
       mon5=ISNULL(BU.Month12Budget,0),
       cmon5 = 'Dec',
       lmon5='TRUE',
       mon6=ISNULL(B.Month1Budget,0),
       cmon6 = 'Jan',
       lmon6='TRUE',
       mon7=ISNULL(B.Month2Budget,0),
       cmon7 = 'Feb',
       lmon7='TRUE',
       mon8=ISNULL(B.Month3Budget,0),
       cmon8 = 'Mar',
       lmon8='TRUE',
       mon9=ISNULL(B.Month4Budget,0),
       cmon9 = 'Apr',
       lmon9='TRUE',
       mon10=ISNULL(B.Month5Budget,0),
       cmon10 = 'May',
       lmon10='TRUE',
       mon11=ISNULL(B.Month6Budget,0),
       cmon11 = 'Jun',
       lmon11='TRUE',
       mon12=ISNULL(B.Month7Budget,0),
       cmon12 = 'Jul',
       lmon12='TRUE',
       MonthStart = @MonthStart ,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1             
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=9
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month9Budget,0),
       cmon1 = 'Sep',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month10Budget,0),
       cmon2 = 'Oct',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month11Budget,0),
       cmon3 = 'Nov',
       lmon3='TRUE',
       mon4=ISNULL(BU.Month12Budget,0),
       cmon4 = 'Dec',
       lmon4='TRUE',
       mon5=ISNULL(B.Month1Budget,0),
       cmon5 = 'Jan',
       lmon5='TRUE',
       mon6=ISNULL(B.Month2Budget,0),
       cmon6 = 'Feb',
       lmon6='TRUE',
       mon7=ISNULL(B.Month3Budget,0),
       cmon7 = 'Mar',
       lmon7='TRUE',
       mon8=ISNULL(B.Month4Budget,0),
       cmon8 = 'Apr',
       lmon7='TRUE',
       mon9=ISNULL(B.Month5Budget,0),
       cmon9 = 'May',
       lmon9='TRUE',
       mon10=ISNULL(B.Month6Budget,0),
       cmon10 = 'Jun',
       lmon10='TRUE',
       mon11=ISNULL(B.Month7Budget,0),
       cmon11 = 'Jul',
       lmon11='TRUE',
       mon12=ISNULL(B.Month8Budget,0),
       cmon12 = 'Aug',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=10
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month10Budget,0),
       cmon1 = 'Oct',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month11Budget,0),
       cmon2 = 'Nov',
       lmon2='TRUE',
       mon3=ISNULL(BU.Month12Budget,0),
       cmon3 = 'Dec',
       lmon3='TRUE',
       mon4=ISNULL(B.Month1Budget,0),
       cmon4 = 'Jan',
       lmon4='TRUE',
       mon5=ISNULL(B.Month2Budget,0),
       cmon5 = 'Feb',
       lmon5='TRUE',
       mon6=ISNULL(B.Month3Budget,0),
       cmon6 = 'Mar',
       lmon6='TRUE',
       mon7=ISNULL(B.Month4Budget,0),
       cmon7 = 'Apr',
       lmon7='TRUE',
       mon8=ISNULL(B.Month5Budget,0),
       cmon8 = 'May',
       lmon8='TRUE',
       mon9=ISNULL(B.Month6Budget,0),
       cmon9 = 'Jun',
       lmon9='TRUE',
       mon10=ISNULL(B.Month7Budget,0),
       cmon10 = 'Jul',
       lmon10='TRUE',
       mon11=ISNULL(B.Month8Budget,0),  
       cmon11 = 'Aug',
       lmon11='TRUE',
       mon12=ISNULL(B.Month9Budget,0),
       cmon12 = 'Sep',
       lmon12='TRUE',
        MonthStart = @MonthStart,
        MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1               
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE IF @MonthStart=11
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month11Budget,0),
       cmon1 = 'Nov',
       lmon1='TRUE',
       mon2=ISNULL(BU.Month12Budget,0),
       cmon2 = 'Dec',
       lmon2='TRUE',
       mon3=ISNULL(B.Month1Budget,0),
       cmon3 = 'Jan',
       lmon3='TRUE',
       mon4=ISNULL(B.Month2Budget,0),
       cmon4 = 'Feb',
       lmon4='TRUE',
       mon5=ISNULL(B.Month3Budget,0),
       cmon5 = 'Mar',
       lmon5='TRUE',
       mon6=ISNULL(B.Month4Budget,0),
       cmon6 = 'Apr',
       lmon6='TRUE',
       mon7=ISNULL(B.Month5Budget,0),
       cmon7 = 'May',
       lmon7='TRUE',
       mon8=ISNULL(B.Month6Budget,0),
       cmon8 = 'Jun',
       lmon8='TRUE',
       mon9=ISNULL(B.Month7Budget,0),
       cmon9 = 'Jul',
       lmon9='TRUE',
       mon10=ISNULL(B.Month8Budget,0),
       cmon10 = 'Aug',
       lmon10='TRUE',
       mon11=ISNULL(B.Month9Budget,0),
       cmon11 = 'Sep',
       lmon11='TRUE',
       mon12=ISNULL(B.Month10Budget,0),
       cmon12 = 'Oct',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,Month10Budget,AccountID FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END
ELSE ---IF @MonthStart=12
BEGIN
SELECT AC.AccountNum AS acctnum,  AC.AccountDescription AS acctdesc,
       mon1=ISNULL(BU.Month12Budget,0),
       cmon1= 'Dec',
       lmon1='TRUE',
       mon2=ISNULL(B.Month1Budget,0),
       cmon2= 'Jan',
       lmon2='TRUE',
       mon3=ISNULL(B.Month2Budget,0),
       cmon3= 'Feb',
       lmon3='TRUE',
       mon4=ISNULL(B.Month3Budget,0),
       cmon4= 'Mar',
       lmon4='TRUE',
       mon5=ISNULL(B.Month4Budget,0),
       cmon5= 'Apr',
       lmon5='TRUE',
       mon6=ISNULL(B.Month5Budget,0),
       cmon6= 'May',
       lmon6='TRUE',
       mon7=ISNULL(B.Month6Budget,0),
       cmon7= 'Jun',
       lmon7='TRUE',
       mon8=ISNULL(B.Month7Budget,0),
       cmon8= 'Jul',
       lmon8='TRUE',
       mon9=ISNULL(B.Month8Budget,0),
       cmon9= 'Aug',
       lmon9='TRUE',
       mon10=ISNULL(B.Month9Budget,0),
       cmon10= 'Sep',
       lmon10='TRUE',
       mon11=ISNULL(B.Month10Budget,0),
       cmon11= 'Oct',
       lmon11='TRUE',
       mon12=ISNULL(B.Month11Budget,0),
       cmon12= 'Nov',
       lmon12='TRUE',
       MonthStart = @MonthStart,
       MonthEnd = @MonthEnd,
       FiscalYearstart = @FiscalYear,
       FiscalYearend   = @FiscalYear+1              
 FROM Account AC INNER JOIN Budget BU  ON AC.AccountID = BU.AccountID
                  JOIN Fleet F ON F.FleetID = BU.FleetID
                 LEFT OUTER JOIN (SELECT Month1Budget,Month2Budget,Month3Budget,Month4Budget,Month5Budget,Month6Budget,Month7Budget,Month8Budget,Month9Budget,Month10Budget,AccountID,Month11Budget FROM BudgeT WHERE CustomerID=@UserCustomerID AND FiscalYear=@FiscalYear+1 AND IsDeleted = 0 )B ON BU.AccountID=B.AccountID
 WHERE (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
   AND BU.FiscalYear = @FiscalYear AND BU.IsDeleted =0
   AND BU.CustomerID = @UserCustomerID
END

END

--EXEC spGetReportPostCostTrackingBudgetSummaryExport 'supervisor_99',10099,'','2013'



GO


