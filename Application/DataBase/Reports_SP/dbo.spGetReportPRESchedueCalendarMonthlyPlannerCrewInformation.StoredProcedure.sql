GO
/****** Object:  StoredProcedure [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation]    Script Date: 02/20/2014 12:22:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation]
GO
/****** Object:  StoredProcedure [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation]    Script Date: 02/20/2014 12:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation]
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)--=10098
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfMonths INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	     ---Display Options---
	  ,@IsBoth INT=1  ---1-Both,2-Fleet,3-Crew
	  ,@IsActivityOnly BIT=0
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@ECrew VARCHAR(MAX)=''
	  ,@NonReversedColor BIT = 0
	  ,@DisplayDays INT=1	
	  ,@FDate DATETIME
	  ,@TDate DATETIME
	  ,@ShowTails INT=1	---1-Show Trip Last Tail,2-Show Day Last Tail
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
          
SET NOCOUNT ON     
 
SELECT @BlackWhiteClr = CASE WHEN @Color = 0 THEN 1 ELSE @BlackWhiteClr END
               
DECLARE @FROMDATE DATETIME=@BEGINDATE,@DATETO DATETIME=@ENDDATE   

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		
----------------------
IF @IsActivityOnly = 1
BEGIN
DECLARE @Crew TABLE(CrewID BIGINT)
  
 INSERT INTO @Crew 
 SELECT DISTINCT PCL.CrewID FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              INNER JOIN(SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)) CW ON CW.CrewID=PCL.CrewID
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND PM.RecordType IN('T','C')
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@TDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@TDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@TDate))
			    )
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
 
UNION

 SELECT DISTINCT PCL.CrewID FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              INNER JOIN(SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)) CW ON CW.CrewID=PCL.CrewID
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND PM.RecordType IN('T','C')
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@TDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@TDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@FDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@TDate))
			    )
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL) 
 
END
------ 

IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END

DECLARE @DisplayInterval INT=(SELECT CASE WHEN @DisplayDays=2 THEN 12
								  WHEN @DisplayDays=3 THEN 8
								  WHEN @DisplayDays=4 THEN 6
								  WHEN @DisplayDays=5 THEN 1 ELSE 24 END);

DECLARE @DateRangeStart1 DATETIME
DECLARE @DateRangeStart2 DATETIME
DECLARE @DateRangeStart3 DATETIME
DECLARE @DateRangeStart4 DATETIME
DECLARE @DateRangeStart5 DATETIME
DECLARE @DateRangeStart6 DATETIME
DECLARE @DateRangeStart7 DATETIME
DECLARE @DateRangeStart8 DATETIME
DECLARE @DateRangeStart9 DATETIME
DECLARE @DateRangeStart10 DATETIME
DECLARE @DateRangeStart11 DATETIME
DECLARE @DateRangeStart12 DATETIME
DECLARE @DateRangeStart13 DATETIME
DECLARE @DateRangeStart14 DATETIME
DECLARE @DateRangeStart15 DATETIME
DECLARE @DateRangeStart16 DATETIME
DECLARE @DateRangeStart17 DATETIME
DECLARE @DateRangeStart18 DATETIME
DECLARE @DateRangeStart19 DATETIME
DECLARE @DateRangeStart20 DATETIME
DECLARE @DateRangeStart21 DATETIME
DECLARE @DateRangeStart22 DATETIME
DECLARE @DateRangeStart23 DATETIME
DECLARE @DateRangeStart24 DATETIME
DECLARE @DateRangeStart25 DATETIME
DECLARE @DateRangeStart26 DATETIME
DECLARE @DateRangeStart27 DATETIME
DECLARE @DateRangeStart28 DATETIME
DECLARE @DateRangeStart29 DATETIME
DECLARE @DateRangeStart30 DATETIME
-- Find the 30 date ranges that are required using the start date and the display interval
SELECT @DateRangeStart1  = @BeginDate , 
@DateRangeStart2 = DATEADD(HOUR,@DisplayInterval , @BeginDate), 
@DateRangeStart3 = DATEADD(HOUR,@DisplayInterval * 2, @BeginDate), 
@DateRangeStart4 = DATEADD(HOUR,@DisplayInterval * 3, @BeginDate), 
@DateRangeStart5 = DATEADD(HOUR,@DisplayInterval * 4, @BeginDate), 
@DateRangeStart6 = DATEADD(HOUR,@DisplayInterval * 5, @BeginDate), 
@DateRangeStart7 = DATEADD(HOUR,@DisplayInterval * 6, @BeginDate), 
@DateRangeStart8 = DATEADD(HOUR,@DisplayInterval * 7, @BeginDate), 
@DateRangeStart9 = DATEADD(HOUR,@DisplayInterval * 8, @BeginDate), 
@DateRangeStart10 = DATEADD(HOUR,@DisplayInterval * 9, @BeginDate), 
@DateRangeStart11 = DATEADD(HOUR,@DisplayInterval * 10, @BeginDate), 
@DateRangeStart12 = DATEADD(HOUR,@DisplayInterval * 11, @BeginDate), 
@DateRangeStart13 = DATEADD(HOUR,@DisplayInterval * 12, @BeginDate), 
@DateRangeStart14 = DATEADD(HOUR,@DisplayInterval * 13, @BeginDate), 
@DateRangeStart15 = DATEADD(HOUR,@DisplayInterval * 14, @BeginDate), 
@DateRangeStart16 = DATEADD(HOUR,@DisplayInterval * 15, @BeginDate), 
@DateRangeStart17 = DATEADD(HOUR,@DisplayInterval * 16, @BeginDate), 
@DateRangeStart18 = DATEADD(HOUR,@DisplayInterval * 17, @BeginDate), 
@DateRangeStart19 = DATEADD(HOUR,@DisplayInterval * 18, @BeginDate), 
@DateRangeStart20 = DATEADD(HOUR,@DisplayInterval * 19, @BeginDate), 
@DateRangeStart21 = DATEADD(HOUR,@DisplayInterval * 20, @BeginDate), 
@DateRangeStart22 = DATEADD(HOUR,@DisplayInterval * 21, @BeginDate), 
@DateRangeStart23 = DATEADD(HOUR,@DisplayInterval * 22, @BeginDate), 
@DateRangeStart24 = DATEADD(HOUR,@DisplayInterval * 23, @BeginDate), 
@DateRangeStart25 = DATEADD(HOUR,@DisplayInterval * 24, @BeginDate), 
@DateRangeStart26 = DATEADD(HOUR,@DisplayInterval * 25, @BeginDate), 
@DateRangeStart27 = DATEADD(HOUR,@DisplayInterval * 26, @BeginDate), 
@DateRangeStart28 = DATEADD(HOUR,@DisplayInterval * 27, @BeginDate), 
@DateRangeStart29 = DATEADD(HOUR,@DisplayInterval * 28, @BeginDate), 
@DateRangeStart30 = DATEADD(HOUR,@DisplayInterval * 29, @BeginDate)



	 SELECT 		
		F.TailNum TailNum, 
		PC.CrewID CrewID,
		CW.CrewCD,
		CW.FirstName,
		CW.MiddleInitial,
		CW.LastName,
		PC.DutyTYPE As CrewDuty,
		PM.RecordType RecordType,
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM, 
		PM.TripNUM TripNUM, 		
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		CONVERT(VARCHAR(15), PL.DutyTYPE) DutyTYPE,
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal, 
		PL.DepartureDTTMLocal as DepartureDisplayTime, 
		PL.ArrivalDTTMLocal as ArrivalDisplayTime,
		F.ForeGrndCustomColor as FgColor,
		F.BackgroundCustomColor as BgColr,
		PL.LastUpdTS LegLastUpdTS, 
		PM.LastUpdTS TripLastUpdTS
		
     
	INTO #CrewPlannerData
	FROM PreflightLeg PL
	INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
	LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
	--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
	INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
	INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
	LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
	LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
	LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
	LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @UserCustomerID AND AD.IsDeleted = 0
	LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
	LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
	LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
	LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
	LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @UserCustomerID  AND CD.IsDeleted = 0
	INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
	--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
	WHERE 1=2 AND PM.CustomerID=@UserCustomerID
	AND PL.LegNUM IS NOT NULL

	
	-- Based on the Display Time Base option use the correct columns in the WHERE clause.
	IF(@TimeBase = 'UTC')

	BEGIN
		INSERT INTO #CrewPlannerData

		SELECT 
		F.TailNum TailNum, 
		PC.CrewID CrewID,
		CW.CrewCD,
		CW.FirstName,
		CW.MiddleInitial,
		CW.LastName,
		PC.DutyTYPE CrewDuty,
		PM.RecordType RecordType,
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM, 
		PM.TripNUM TripNUM, 		
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		CONVERT(VARCHAR(15), PL.DutyTYPE) DutyTYPE,
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureGreenwichDTTM DepartureDTTMLocal, 
		PL.ArrivalGreenwichDTTM ArrivalDTTMLocal,
		PL.DepartureGreenwichDTTM as DepartureDisplayTime, 
		PL.ArrivalGreenwichDTTM as ArrivalDisplayTime
		,FgColor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor
				   WHEN PM.RecordType='C' THEN CD.ForeGrndCustomColor  ELSE NULL END
		,BgColr=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor
				   WHEN PM.RecordType='C' THEN CD.BackgroundCustomColor  ELSE NULL END
		,PL.LastUpdTS LegLastUpdTS
		,PM.LastUpdTS TripLastUpdTS

		--INTO #CrewPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown

		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID	
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID	
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @UserCustomerID AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @UserCustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
		--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
		WHERE PL.DepartureGreenwichDTTM is not null AND PL.ArrivalGreenwichDTTM is not null
        --AND PL.DepartureGreenwichDTTM <= PL.ArrivalGreenwichDTTM 
        AND ((PL.DepartureGreenwichDTTM >= @BeginDate AND PL.DepartureGreenwichDTTM <=@EndDate)  OR (PL.ArrivalGreenwichDTTM >= @BeginDate AND PL.ArrivalGreenwichDTTM <=@EndDate)  
        OR  (PL.DepartureGreenwichDTTM <=@EndDate  AND PL.ArrivalGreenwichDTTM >= @BeginDate))
        AND	PL.DepartureGreenwichDTTM >= DATEADD(day,-31,@BeginDate) AND PL.DepartureGreenwichDTTM <= @EndDate
		AND PM.RecordType <> 'M' AND PM.CustomerID=@UserCustomerID  
		AND PL.LegNUM IS NOT NULL
		and CW.IsStatus = 1

		AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		AND (PA.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		AND (HB.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (((VR.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(VR.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

	END

	ELSE IF (@TimeBase = 'Home')
	
	BEGIN
		INSERT INTO #CrewPlannerData

		SELECT 
		F.TailNum TailNum, 
		PC.CrewID CrewID,
		CW.CrewCD,
		CW.FirstName,
		CW.MiddleInitial,
		CW.LastName,
		PC.DutyTYPE CrewDuty,
		PM.RecordType RecordType,
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM, 
		PM.TripNUM TripNUM, 		
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		CONVERT(VARCHAR(15), PL.DutyTYPE) DutyTYPE,
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.HomeDepartureDTTM DepartureDTTMLocal, 
		PL.HomeArrivalDTTM ArrivalDTTMLocal,
		PL.HomeDepartureDTTM as DepartureDisplayTime, 
		PL.HomeArrivalDTTM as ArrivalDisplayTime
		,FgColor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor
				   WHEN PM.RecordType='C' THEN CD.ForeGrndCustomColor  ELSE NULL END
		,BgColr=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor
				   WHEN PM.RecordType='C' THEN CD.BackgroundCustomColor  ELSE NULL END
		,PL.LastUpdTS LegLastUpdTS
		,PM.LastUpdTS TripLastUpdTS
	
		--INTO #CrewPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID			
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID 
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @UserCustomerID  AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID		
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @UserCustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change 
		--LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID	-- Home Base Airport ID change
		WHERE PL.HomeDepartureDTTM is not null AND PL.HomeArrivalDTTM is not null
        --AND PL.HomeDepartureDTTM <= PL.HomeArrivalDTTM
        AND ((PL.HomeDepartureDTTM >= @BeginDate AND PL.HomeDepartureDTTM <=@EndDate)  OR (PL.HomeArrivalDTTM >= @BeginDate AND PL.HomeArrivalDTTM <=@EndDate)  
        OR  (PL.HomeDepartureDTTM <=@EndDate  AND PL.HomeArrivalDTTM >= @BeginDate))
		AND PM.CustomerID=@UserCustomerID  
		AND PL.LegNUM IS NOT NULL AND
		 PL.HomeDepartureDTTM >= DATEADD(day,-31,@BeginDate) AND PL.HomeDepartureDTTM <= @EndDate
		AND PM.RecordType <> 'M'
		and CW.IsStatus = 1

		AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		AND (PA.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		AND (HB.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (((VR.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(VR.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

	END	

	ELSE	
	BEGIN 
		INSERT INTO #CrewPlannerData
		SELECT 
		F.TailNum TailNum, 
		PC.CrewID CrewID,
		CW.CrewCD,
		CW.FirstName,
		CW.MiddleInitial,
		CW.LastName,
		PC.DutyTYPE CrewDuty,
		PM.RecordType RecordType,
		PL.LegID LegID, 
		PL.TripID TripID, 
		PL.LegNUM LegNUM, 
		PM.TripNUM TripNUM, 		
		PL.ArriveICAOID ArriveICAOID,
		PL.DepartICAOID DepartICAOID,
		CONVERT(VARCHAR(15), PL.DutyTYPE) DutyTYPE,
		A.IcaoID ArriveICAO,
		DT.IcaoID DepartICAO,
		PL.DepartureDTTMLocal DepartureDTTMLocal, 
		PL.ArrivalDTTMLocal ArrivalDTTMLocal,
		PL.DepartureDTTMLocal as DepartureDisplayTime, 
		PL.ArrivalDTTMLocal as ArrivalDisplayTime
		,FgColor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor
				   WHEN PM.RecordType='C' THEN CD.ForeGrndCustomColor  ELSE NULL END
		,BgColr=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor
				   WHEN PM.RecordType='C' THEN CD.BackgroundCustomColor  ELSE NULL END
		,PL.LastUpdTS LegLastUpdTS
		,PM.LastUpdTS TripLastUpdTS
		
		--INTO #FleetPlannerData 
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted !=1 AND PL.IsDeleted != 1
		LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
		--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
		INNER JOIN Airport A ON A.AirportID  = PL.ArriveICAOID
		INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
		LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
		LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID
		LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN AircraftDuty AD ON AD.AircraftDutyCD=PL.DutyTYPE and AD.CustomerID = @UserCustomerID  AND AD.IsDeleted = 0
		LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID 
		LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID
		LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
		LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID
		LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD = PL.DutyTYPE and CD.CustomerID = @UserCustomerID  AND CD.IsDeleted = 0
		INNER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change
		WHERE PL.DepartureDTTMLocal is not null AND PL.ArrivalDTTMLocal is not null 
        --AND PL.DepartureDTTMLocal <= PL.ArrivalDTTMLocal 
        AND ((PL.DepartureDTTMLocal >= @BeginDate AND PL.DepartureDTTMLocal <=@EndDate)  OR (PL.ArrivalDTTMLocal >= @BeginDate AND PL.ArrivalDTTMLocal <=@EndDate)  
        OR  (PL.DepartureDTTMLocal <=@EndDate  AND PL.ArrivalDTTMLocal >= @BeginDate))
		AND PM.CustomerID=@UserCustomerID  
		AND PL.LegNUM IS NOT NULL AND PL.DepartureDTTMLocal >= DATEADD(day,-31,@BeginDate) AND PL.DepartureDTTMLocal <= @EndDate
		AND PM.RecordType <> 'M'
		and CW.IsStatus = 1

		AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		AND (PA.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		AND (HB.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (((VR.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(VR.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


		Update #CrewPlannerData
		Set ArrivalDTTMLocal=DepartureDTTMLocal,DepartureDTTMLocal=ArrivalDTTMLocal
		Where DATEDIFF(day,DepartureDTTMLocal,ArrivalDTTMLocal)<0 

	END



-- Insert  Rest Over Night(RON) Records.
--if (@TripStatus = 1)
--Begin

SELECT F.TailNum,PM.FleetID,PL.ArrivalDTTMLocal,PL.DepartureDTTMLocal,PL.DepartureGreenwichDTTM,PL.ArrivalGreenwichDTTM
					,PL.HomeDepartureDTTM,PL.HomeArrivalDTTM,DT.IcaoID As DepartICAO,AA.IcaoID As ArriveICAO,PM.TripID,PL.LegNUM
					,PM.TripNUM,PM.RecordType,AFT.AircraftCD TypeCode,PC.CrewID CrewID,CW.CrewCD,CW.FirstName,CW.MiddleInitial
					,CW.LastName,PC.DutyTYPE CrewDuty,PL.ArriveICAOID,PL.DepartICAOID
					,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END AS DepartureDisplayTime
					,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END AS ArrivalDisplayTime
					,PL.LastUpdTS As LegLastUpdTS,PM.LastUpdTS As TripLastUpdTS
INTO #CrewRONRecords
				FROM PreflightLeg PL
			INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID  AND PM.IsDeleted !=1 AND PL.IsDeleted != 1 AND PM.CustomerID=@UserCustomerID
			INNER JOIN Fleet F ON PM.FleetID = F.FleetID AND F.IsInActive != 1 -- inactive aircraft trips should not be shown
			--INNER JOIN PreflightCrewList C ON C.LegID = L.LegID
			INNER JOIN Airport AA ON AA.AirportID  = PL.ArriveICAOID
			INNER JOIN Airport DT ON DT.AirportId = PL.DepartICAOID
			LEFT OUTER JOIN Vendor VR ON VR.VendorID=F.VendorID
			LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID		
			LEFT OUTER JOIN Department D ON D.DepartmentID=PL.DepartmentID
			LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
			LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
			LEFT OUTER JOIN Passenger PA ON PA.PassengerRequestorID = PL.PassengerRequestorID
			LEFT OUTER JOIN PreflightCrewList PC ON PC.LegId = PL.LegId
			LEFT OUTER JOIN Crew CW ON CW.CrewID = PC.CrewID 
			LEFT OUTER JOIN Company HM ON HM.HomebaseID=PM.HomebaseID	
			LEFT OUTER JOIN Airport HB ON HB.AirportId = HM.HomebaseAirportID -- Home Base Airport ID change	
WHERE PM.CustomerID=@UserCustomerID
AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
		AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
		AND (PA.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
		AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
		AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
		AND (HB.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (((VR.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(VR.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


INSERT INTO #CrewPlannerData

SELECT 			
		A.TailNum TailNum, 
		A.CrewID,
		A.CrewCD,
		A.FirstName,
		A.MiddleInitial,
		A.LastName,
		A.CrewDuty,
		'T' RecordType, 
		9999 LegID, 
		A.TripID TripID, 
		9999 LegNUM,
		A.TripNUM TripNUM,
		A.ArriveICAOID ArriveICAOID,
		A.DepartICAOID DepartICAOID,
		'R' DutyTYPE, 
		--CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.TailNum ELSE A.TailNum END DutyTYPE,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArriveICAO ELSE A.ArriveICAO END ArriveICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartICAO ELSE A.DepartICAO END DepartICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE A.ArrivalDTTMLocal END AS DepartureDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureDTTMLocal ELSE (CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) END ArrivalDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartureDisplayTime ELSE A.DepartureDisplayTime END DepartureDisplayTime,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalDisplayTime ELSE A.ArrivalDisplayTime END ArrivalDisplayTime,
		FgColor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'
                   WHEN A.RecordType='T' THEN CD.ForeGrndCustomColor  ELSE NULL END,
		BgColr=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN A.RecordType='T' THEN CD.BackgroundCustomColor  ELSE NULL END,
		B.LegLastUpdTS LegLastUpdTS, 
		B.TripLastUpdTS TripLastUpdTS
		
	
	FROM #CrewPlannerData A 
	--INNER JOIN  #CrewPlannerData B	
	--	ON A.TripID = B.TripID AND (A.LegNUM = B.LegNUM -1
	--	 OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTMLocal,A.ArrivalDTTMLocal)<0)
	--	) And A.CrewID=B.CrewID
	INNER JOIN #CrewRONRecords B
	ON A.TripID = B.TripID AND A.CrewID=B.CrewID AND (A.LegNUM = B.LegNUM -1
	 OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTMLocal,A.ArrivalDTTMLocal)<0)
	 OR (B.LegNUM=A.LegNUM-1 
			AND ((CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) <= @BeginDate AND (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) <=@BeginDate)
			)
	)
	LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD='R' AND CD.CustomerID=@UserCustomerID AND CD.IsDeleted=0 AND CD.IsInActive=0
	WHERE A.RecordType = 'T'

-- Insert Rest Over Night(RON) Records for the Trips where a Leg Starts and Ends before StartDate & another Leg Starts and Ends after EndDate
INSERT INTO #CrewPlannerData
SELECT 	TailNum,CrewID,CrewCD,FirstName,MiddleInitial,LastName,CrewDuty,RecordType,LegID,TripID,LegNUM,TripNUM,ArriveICAOID,DepartICAOID
 ,DutyTYPE,ArriveICAO,DepartICAO,DepartureDTTMLocal,ArrivalDTTMLocal,DepartureDisplayTime,ArrivalDisplayTime
 ,FgColor,BgColr,LegLastUpdTS,TripLastUpdTS
FROM
(SELECT
		A.TailNum TailNum, 
		A.CrewID,
		A.CrewCD,
		A.FirstName,
		A.MiddleInitial,
		A.LastName,
		A.CrewDuty,
		'T' RecordType, 
		9999 LegID, 
		A.TripID TripID, 
		9999 LegNUM,
		A.TripNUM TripNUM,
		A.ArriveICAOID ArriveICAOID,
		A.DepartICAOID DepartICAOID,
		'R' DutyTYPE, 
		--CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.TailNum ELSE A.TailNum END DutyTYPE,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArriveICAO ELSE A.ArriveICAO END ArriveICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartICAO ELSE A.DepartICAO END DepartICAO,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE A.ArrivalDTTMLocal END AS DepartureDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN A.DepartureDTTMLocal ELSE (CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) END ArrivalDTTMLocal,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.DepartureDisplayTime ELSE A.DepartureDisplayTime END DepartureDisplayTime,
		CASE WHEN  B.LegNUM=A.LegNUM-1 THEN B.ArrivalDisplayTime ELSE A.ArrivalDisplayTime END ArrivalDisplayTime,
		FgColor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'
                   WHEN A.RecordType='T' THEN CD.ForeGrndCustomColor  ELSE NULL END,
		BgColr=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN A.RecordType='T' THEN CD.BackgroundCustomColor  ELSE NULL END,
		B.LegLastUpdTS LegLastUpdTS, 
		B.TripLastUpdTS TripLastUpdTS,
		ROW_NUMBER() OVER (PARTITION BY A.FleetID,A.TripID,A.CrewID ORDER BY (CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE A.ArrivalDTTMLocal END) desc) AS ROW_NUM
		
	FROM #CrewRONRecords A
	INNER JOIN #CrewRONRecords B
	ON A.TripID = B.TripID AND A.CrewID=B.CrewID AND (A.LegNUM = B.LegNUM -1
	 OR (A.LegNUM=1 And A.LegNUM=B.LegNUM And DATEDIFF(day,A.DepartureDTTMLocal,A.ArrivalDTTMLocal)<0)
	 OR (B.LegNUM=A.LegNUM-1 
			AND ((CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) <= @BeginDate AND (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) <=@BeginDate)
				-- OR
				-- ((CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) >= @EndDate AND (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) <=@EndDate)
				--)
		)
	)
	LEFT OUTER JOIN CrewDutyType CD ON CD.DutyTypeCD='R' AND CD.CustomerID=@UserCustomerID AND CD.IsDeleted=0 AND CD.IsInActive=0
	WHERE A.RecordType='T' AND
	(CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @TimeBase='Local' THEN B.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN B.ArrivalGreenwichDTTM ELSE B.HomeArrivalDTTM END) ELSE (CASE WHEN @TimeBase='Local' THEN A.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN A.ArrivalGreenwichDTTM ELSE A.HomeArrivalDTTM END) END)<@BeginDate
	AND
	(CASE WHEN  B.LegNUM=A.LegNUM-1 THEN (CASE WHEN @TimeBase='Local' THEN A.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN A.DepartureGreenwichDTTM ELSE A.HomeDepartureDTTM END) ELSE (CASE WHEN @TimeBase='Local' THEN B.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN B.DepartureGreenwichDTTM ELSE B.HomeDepartureDTTM END) END)>@EndDate
	) C
	WHERE ROW_NUM=1

	DROP table #CrewRONRecords
--------------------------------

--End
	
	
-- Split the data for the required 30 date ranges 
SELECT 
	PlannerColumnPeriodStart,
	TailNum, 
	CrewID,
	CrewCD,
	FirstName,
	MiddleInitial,
	LastName,
	CrewDuty,
	RecordType,
	LegID, 
	TripID,
	LegNUM,  
	TripNUM,
	ArriveICAOID,
	DepartICAOID,
	DutyTYPE,
	ArriveICAO,
	DepartICAO,
	DepartureDTTMLocal, 
	ArrivalDTTMLocal, 
	DepartureDisplayTime,
	ArrivalDisplayTime,
	FgColor,
	BgColr,
	LegLastUpdTS, 
	TripLastUpdTS,
	CONVERT(BIT, 0) ConflictFlag,
	NULL MonthNo	
	
INTO #CrewPlannerDetail

FROM 
(	

	--:?? Performance
	--:?? Also check if ArrivalDTTMLocal >= @DateRangeStart should be ArrivalDTTMLocal > @DateRangeStart
	
SELECT @DateRangeStart1  PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart2 AND ArrivalDTTMLocal >= @DateRangeStart1 

UNION ALL

 SELECT @DateRangeStart2 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart3 AND ArrivalDTTMLocal >= @DateRangeStart2

UNION ALL

 SELECT @DateRangeStart3 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart4 AND ArrivalDTTMLocal >= @DateRangeStart3

UNION ALL

 SELECT @DateRangeStart4 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart5 AND ArrivalDTTMLocal >= @DateRangeStart4

UNION ALL

 SELECT @DateRangeStart5 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart6 AND ArrivalDTTMLocal >= @DateRangeStart5

UNION ALL

 SELECT @DateRangeStart6 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart7 AND ArrivalDTTMLocal >= @DateRangeStart6

UNION ALL

 SELECT @DateRangeStart7 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart8 AND ArrivalDTTMLocal >= @DateRangeStart7

UNION ALL

 SELECT @DateRangeStart8 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart9 AND ArrivalDTTMLocal >= @DateRangeStart8

UNION ALL
 SELECT @DateRangeStart9 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart10 AND ArrivalDTTMLocal >= @DateRangeStart9

UNION ALL

 SELECT @DateRangeStart10 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart11 AND ArrivalDTTMLocal >= @DateRangeStart10

UNION ALL
 SELECT @DateRangeStart11 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart12 AND ArrivalDTTMLocal >= @DateRangeStart11

UNION ALL

 SELECT @DateRangeStart12 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart13 AND ArrivalDTTMLocal >= @DateRangeStart12

UNION ALL
 SELECT @DateRangeStart13 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart14 AND ArrivalDTTMLocal >= @DateRangeStart13

UNION ALL

 SELECT @DateRangeStart14 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart15 AND ArrivalDTTMLocal >= @DateRangeStart14

UNION ALL
 SELECT @DateRangeStart15 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart16 AND ArrivalDTTMLocal >= @DateRangeStart15

UNION ALL

 SELECT @DateRangeStart16 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart17 AND ArrivalDTTMLocal >= @DateRangeStart16

UNION ALL

 SELECT @DateRangeStart17 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart18 AND ArrivalDTTMLocal >= @DateRangeStart17

UNION ALL
 SELECT @DateRangeStart18 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart19 AND ArrivalDTTMLocal >= @DateRangeStart18

UNION ALL

 SELECT @DateRangeStart19 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart20 AND ArrivalDTTMLocal >= @DateRangeStart19

UNION ALL
 SELECT @DateRangeStart20 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart21 AND ArrivalDTTMLocal >= @DateRangeStart20

UNION ALL

 SELECT @DateRangeStart21 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart22 AND ArrivalDTTMLocal >= @DateRangeStart21

UNION ALL
 SELECT @DateRangeStart22 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart23 AND ArrivalDTTMLocal >= @DateRangeStart22

UNION ALL

 SELECT @DateRangeStart23 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart24 AND ArrivalDTTMLocal >= @DateRangeStart23

UNION ALL

 SELECT @DateRangeStart24 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart25 AND ArrivalDTTMLocal >= @DateRangeStart24

UNION ALL

 SELECT @DateRangeStart25 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart26 AND ArrivalDTTMLocal >= @DateRangeStart25

UNION ALL

 SELECT @DateRangeStart26 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart27 AND ArrivalDTTMLocal >= @DateRangeStart26

UNION ALL

 SELECT @DateRangeStart27 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart28 AND ArrivalDTTMLocal >= @DateRangeStart27

UNION ALL

 SELECT @DateRangeStart28 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart29 AND ArrivalDTTMLocal >= @DateRangeStart28

UNION ALL

 SELECT @DateRangeStart29 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @DateRangeStart30 AND ArrivalDTTMLocal >= @DateRangeStart29

UNION ALL

 SELECT @DateRangeStart30 PlannerColumnPeriodStart, TailNum, CrewID, CrewCD, FirstName, MiddleInitial, LastName, CrewDuty, RecordType, LegID, TripID, LegNUM, TripNUM
	, ArriveICAOID, DepartICAOID, DutyTYPE, ArriveICAO, DepartICAO, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureDisplayTime, ArrivalDisplayTime
	,FgColor, BgColr, LegLastUpdTS, TripLastUpdTS
 FROM #CrewPlannerData
 WHERE DepartureDTTMLocal < @EndDate AND ArrivalDTTMLocal >= @DateRangeStart30
 
) CrewPlannerData 

DROP TABLE #CrewPlannerData

--Insert NULL ICAOs
DECLARE  @FirstDate	DATETIME= @BeginDate,
		 @LastDate	DATETIME= @EndDate;
	   
  Declare @Date TABLE (Date DATETIME,Date1 DATETIME)

SELECT @FirstDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @FirstDate));  
  SELECT @LastDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @LastDate));
  
  SET @LastDate=CONVERT(DATETIME, CONVERT(VARCHAR(10),@LastDate, 101) + ' 23:59', 101);

  DECLARE @Value INT=(SELECT CASE WHEN @DisplayDays=2 THEN 12
								  WHEN @DisplayDays=3 THEN 8
								  WHEN @DisplayDays=4 THEN 6
								  WHEN @DisplayDays=5 THEN 1 ELSE 24 END);

WITH CTE_DatesTable
  AS 
  (
    SELECT @FirstDate AS [date],DATEADD(MINUTE,-1,DATEADD(HH, @Value, @FirstDate))  AS [date1],DATEADD(HH, @Value, @FirstDate)  AS [date2]
    UNION ALL
    SELECT DATEADD(HH, @Value, [date]), DATEADD(MINUTE,-1,DATEADD(HH, @Value, [date2])),DATEADD(HH, @Value, [date2])
    FROM CTE_DatesTable
    WHERE DATEADD(HH, @Value, [date]) < @LastDate
  )
  INSERT INTO @Date 
  SELECT date,date1 FROM CTE_DatesTable
  OPTION (MAXRECURSION 0)

DECLARE @CurDate TABLE(DateWeek DATETIME,DateWeek1 DATETIME,CrewID BIGINT)

INSERT INTO @CurDate(DateWeek,DateWeek1,CrewID)
SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  ((SELECT * FROM  @Date)) T 
                                 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND CC.IsDeleted=0 AND CC.IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)
                                   AND (CC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')	

INSERT INTO #CrewPlannerDetail(CrewID,CrewCD,FirstName,MiddleInitial,LastName,PlannerColumnPeriodStart,LegID)
SELECT DISTINCT C.CrewID,C.CrewCD,C.FirstName,C.MiddleInitial,C.LastName,CD.DateWeek,0
			FROM @CurDate CD INNER JOIN  Crew C ON CD.CrewID=C.CrewID
			WHERE C.IsDeleted=0
			AND C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
			AND C.IsStatus=1
			AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing=@RotaryWingCrew) OR @RotaryWingCrew = 0)
			AND NOT EXISTS(SELECT ISNULL(CrewID,0) FROM #CrewPlannerDetail T WHERE T.CrewID=CD.CrewID AND T.PlannerColumnPeriodStart=CD.DateWeek)

--*****

	-- Identify the (PlannerColumnPeriodStart + TailNumber) which have more than one record -- for display purpose on the UI -- not used in query
	UPDATE #CrewPlannerDetail 
		SET ConflictFlag = 1
		FROM #CrewPlannerDetail A INNER JOIN 
		(
		
			SELECT 
					PlannerColumnPeriodStart, 
					CrewID
			FROM #CrewPlannerDetail
			--GROUP BY PlannerColumnPeriodStart, CrewID
			--HAVING COUNT(1) > 1
			GROUP BY PlannerColumnPeriodStart, CrewID, LegID
			HAVING LegID<>9999
		) B
		ON A.PlannerColumnPeriodStart = B.PlannerColumnPeriodStart AND 
		A.CrewID = B.CrewID

--MonthNo
DECLARE @MONTHNUM INT=1,@DateVal DATETIME=@BeginDate,@DateFrom DATETIME;
 DECLARE @MonthVal INT=15
 
 SET @MonthVal=(SELECT CASE WHEN @DisplayDays=1 THEN 360 
							WHEN @DisplayDays=2 THEN 180
						    WHEN @DisplayDays=3 THEN 120
							WHEN @DisplayDays=4 THEN 90
							WHEN @DisplayDays=5 THEN 2 END)


SET @NoOfMonths = 2*@NoOfMonths
IF @DisplayDays=5
BEGIN

WHILE (@MONTHNUM<=@NoOfMonths)
                                  
 BEGIN
    UPDATE #CrewPlannerDetail SET MonthNo=@MONTHNUM WHERE PlannerColumnPeriodStart BETWEEN @DateVal AND DATEADD(HH,15,@DateVal)
    SET @DateVal=DATEADD(HH,15,@DateVal)
    SET @MONTHNUM=@MONTHNUM+1        
 END

END

ELSE

BEGIN

WHILE (@MONTHNUM<=@NoOfMonths)

 BEGIN
	UPDATE #CrewPlannerDetail SET MonthNo=@MONTHNUM 
					WHERE PlannerColumnPeriodStart BETWEEN @DateVal AND DATEADD(HH,@MonthVal,@DateVal)
	SET @DateVal=DATEADD(HH,@MonthVal,@DateVal)

 SET @MONTHNUM=@MONTHNUM+1
		 
 END

END
-----------

DECLARE @Final TABLE(ID INT IDENTITY
					,TailNum VARCHAR(30)
					,CrewID BIGINT
					,CrewCD VARCHAR(5)
					,CrewMember VARCHAR(90)
					,TripNUM BIGINT
					,Date DATETIME
					,RecordType VARCHAR(1)
					,CrewVal VARCHAR(20)
					,Rnk INT
					,MonthNo INT
					,SortMonth INT
					,DateVal VARCHAR(20)
					,BgColr VARCHAR(10)
					,FgColor VARCHAR(10)
					,DepartureDisplayTime DATETIME
					,ArrivalDisplayTime DATETIME)


IF @ShowTails=1
BEGIN
IF @IsBoth=1 OR @IsBoth=3
BEGIN
INSERT INTO @Final
Select A.TailNum
	,A.CrewID
	,A.CrewCD
	,A.CrewMember
	,A.TripNUM
	,Date=A.PlannerColumnPeriodStart
	,A.RecordType
	,CrewVal=ISNULL(CONVERT(VARCHAR, A.DutyTYPE), A.CrewVal+(CASE WHEN ISNULL(CrewDuty,'')<>'' THEN '-'+CrewDuty ELSE '' END))
	,A.Rnk
	,A.MonthNo
	,DENSE_RANK() OVER (PARTITION BY CrewID,MonthNo ORDER BY CrewID,MonthNo,PlannerColumnPeriodStart) AS SortMonth
	,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
	,A.BgColr
	,A.FgColor
	,A.DepartureDisplayTime
	,A.ArrivalDisplayTime
	
FROM (
SELECT PlannerColumnPeriodStart
	  ,TailNum
      ,CrewID
	  ,CrewCD
	  ,CrewMember=CrewCD+' - '+ISNULL(LastName,'')+', '+ISNULL(FirstName,'')+' '+ISNULL(MiddleInitial,'')
	  ,CrewDuty
	  ,TripNUM
	  ,RecordType
	  ,DutyTYPE
	  ,TailNum CrewVal
	  ,ROW_NUMBER()OVER(PARTITION BY CrewID,PlannerColumnPeriodStart,TripNUM ORDER BY ArrivalDisplayTime DESC) Rnk
	  ,MonthNo
	  ,FgColor
	  ,BgColr
	  ,LegLastUpdTS
	  ,TripLastUpdTS
	  ,ConflictFlag
	  ,DepartureDisplayTime
	  ,ArrivalDisplayTime
FROM #CrewPlannerDetail CP
WHERE CP.LegID Not in (Select B.LegID From #CrewPlannerDetail As B Where B.LegNUM='9999' And B.ConflictFlag=1 And B.PlannerColumnPeriodStart = CP.PlannerColumnPeriodStart AND B.CrewID = CP.CrewID)
)A
WHERE Rnk=1

END

END

ELSE IF @ShowTails=2
BEGIN
IF @IsBoth=1 OR @IsBoth=3
BEGIN
INSERT INTO @Final
Select A.TailNum
	,A.CrewID
	,A.CrewCD
	,A.CrewMember
	,A.TripNUM
	,Date=A.PlannerColumnPeriodStart
	,A.RecordType
	,CrewVal=ISNULL(CONVERT(VARCHAR, A.DutyTYPE), A.CrewVal+(CASE WHEN ISNULL(CrewDuty,'')<>'' THEN '-'+CrewDuty ELSE '' END))
	,A.Rnk
	,A.MonthNo
	,DENSE_RANK() OVER (PARTITION BY CrewID,MonthNo ORDER BY CrewID,MonthNo,PlannerColumnPeriodStart) AS SortMonth
	,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
	,A.BgColr
	,A.FgColor
	,A.DepartureDisplayTime
	,A.ArrivalDisplayTime
	
FROM (
SELECT PlannerColumnPeriodStart
	  ,TailNum
      ,CrewID
	  ,CrewCD
	  ,CrewMember=CrewCD+' - '+ISNULL(LastName,'')+', '+ISNULL(FirstName,'')+' '+ISNULL(MiddleInitial,'')
	  ,CrewDuty
	  ,TripNUM
	  ,RecordType
	  ,DutyTYPE
	  ,TailNum CrewVal
	  ,ROW_NUMBER()OVER(PARTITION BY CrewID,PlannerColumnPeriodStart ORDER BY ArrivalDisplayTime DESC) Rnk
	  ,MonthNo
	  ,FgColor
	  ,BgColr
	  ,LegLastUpdTS
	  ,TripLastUpdTS
	  ,ConflictFlag
	  ,DepartureDisplayTime
	  ,ArrivalDisplayTime
FROM #CrewPlannerDetail CP
WHERE CP.RecordType='C' AND
CP.LegID Not in (Select B.LegID From #CrewPlannerDetail As B Where B.LegNUM='9999' And B.ConflictFlag=1 And B.PlannerColumnPeriodStart = CP.PlannerColumnPeriodStart AND B.CrewID = CP.CrewID)

UNION

SELECT PlannerColumnPeriodStart
	  ,TailNum
      ,CrewID
	  ,CrewCD
	  ,CrewMember=CrewCD+' - '+ISNULL(LastName,'')+', '+ISNULL(FirstName,'')+' '+ISNULL(MiddleInitial,'')
	  ,CrewDuty
	  ,TripNUM
	  ,RecordType
	  ,DutyTYPE
	  ,TailNum CrewVal
	  ,ROW_NUMBER()OVER(PARTITION BY CrewID,PlannerColumnPeriodStart ORDER BY ArrivalDisplayTime DESC) Rnk
	  ,MonthNo
	  ,FgColor
	  ,BgColr
	  ,LegLastUpdTS
	  ,TripLastUpdTS
	  ,ConflictFlag
	  ,DepartureDisplayTime
	  ,ArrivalDisplayTime
FROM #CrewPlannerDetail CP1
WHERE (CP1.RecordType='T' OR CP1.RecordType is NULL) AND 
CP1.LegID Not in (Select B.LegID From #CrewPlannerDetail As B Where B.LegNUM='9999' And B.ConflictFlag=1 And B.PlannerColumnPeriodStart = CP1.PlannerColumnPeriodStart AND B.CrewID = CP1.CrewID)
)A
WHERE Rnk=1

END

END

SELECT  TailNum
	,CrewID
	,CrewCD
	,CrewMember
	,TripNUM
	,Date
	,RecordType
	,CONVERT(VARCHAR(6),CrewVal) AS CrewVal
	,Rnk
	,MonthNo
	,SortMonth
	,DateVal
	,BgColr
	,FgColor
FROM (
SELECT *,
	1 As Sort
FROM @Final
WHERE RecordType='C'

UNION 

SELECT *,
	2 As Sort
FROM @Final
WHERE RecordType='T' OR RecordType is NULL
) F
WHERE MonthNo =1
ORDER BY MonthNo,CrewCD,Date,Sort,ArrivalDisplayTime

 DROP TABLE #CrewPlannerDetail
				
END




 --EXEC spGetReportPRESchedueCalendarMonthlyPlannerCrewInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''