IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCreCurrency2Hdr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCreCurrency2Hdr]
GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

        
CREATE PROCEDURE [dbo].[spGetReportPOSTCreCurrency2Hdr]                                        
(                                  
  @UserCD AS VARCHAR(30)                               
 )            
                                
-- =============================================                                      
-- Author: Mathes                 
-- ALtered 28-12-2012                                 
-- Description: Crew Currency II Header                                    
-- =============================================                                      
AS                                    
SET NOCOUNT ON                                      
BEGIN         

	Select
	DayLanding,  
	NightLanding,  
	TakeoffDay,   
	TakeoffNight,  
	Approach,      
	Instrument,   
	Day7,   
	RestDays,
	Day90,
	Month6,
	Month12,
	Day365,
	Day30 
	From Company 
	Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
	And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)   

END

GO