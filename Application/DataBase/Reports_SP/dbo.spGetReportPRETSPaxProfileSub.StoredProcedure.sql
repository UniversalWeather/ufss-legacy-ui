IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPaxProfileSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPaxProfileSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRETSPaxProfileSub] 
 --  @UserCD AS VARCHAR(30),       -- Mandatory
   @PaxID AS VARCHAR(30),               -- Mandatory
   @TripID AS VARCHAR(30)         -- Mandatory
AS
-- =======================================================================
-- SPC Name: spGetReportPRETSPaxProfileSub
-- Author: ABHISHEK.S
-- Create date: 11 September 2012
-- Description: Sub Report for TripSheet Passenger Profile
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =======================================================================
BEGIN

	SET NOCOUNT ON;
	
     
	SELECT DISTINCT
	        [TripID] = PM.TripID,
			[PaxID] = PLI.PassengerID,
	        [PaxCD] = CONVERT(VARCHAR(MAX),PR.PassengerRequestorCD),
			[PaxName] = CONVERT(VARCHAR(MAX),ISNULL(PR.PassengerRequestorCD,'')) + '  ' + ISNULL(PR.LastName,'') + ', ' + ISNULL(PR.FirstName,''),			
			[Notes] = CONVERT(VARCHAR(MAX),PR.Notes),
			[Code] = CONVERT(VARCHAR(MAX),PA.AdditionalINFOCD),
			[Description] = CONVERT(VARCHAR(MAX),PA.AdditionalINFODescription),
			[Value] = CONVERT(VARCHAR(MAX),ISNULL(PA.AdditionalINFOValue,'')),
			--[Value] = dbo.FlightPakDecrypt(ISNULL(PA.AdditionalINFOValue,''))
			[HomeAddress] = PR.Addr1 + (CASE WHEN ISNULL(PR.Addr2,'') <> '' AND ISNULL(PR.Addr1,'') <> '' THEN '$$' + PR.Addr2 ELSE ISNULL(PR.Addr2,'') END)
								     + (CASE WHEN ISNULL(PR.Addr3,'') <> '' AND (ISNULL(PR.Addr2,'') <> '' OR ISNULL(PR.Addr1,'') <> '') THEN  '$$' + PR.Addr3 ELSE ISNULL(PR.Addr3,'') END) 
			
			FROM  PREFLIGHTMAIN PM
				  INNER JOIN PreflightLeg PL
				          ON PM.TripID = PL.TripID  
			      INNER JOIN PreflightPassengerList PLI
			              ON PL.LegID = PLI.LegID 
				  INNER JOIN Passenger PR 
				          ON PLI.PassengerID = PR.PassengerRequestorID
				         AND CONVERT(VARCHAR,PLI.PassengerID) = @PaxID
				   LEFT JOIN PassengerAdditionalInfo PA
				          ON PR.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted=0
				          
			WHERE PM.IsDeleted = 0			  
			  AND PM.TripID  = CONVERT(bigint,@TripID)
			 
 
	


END

--EXEC spGetReportPRETSPaxProfileSub '10001199926', '10001119'

--select * from Preflightmain where TripID = '100111807'
--select PassengerID from PreflightPassengerList where LegID = 100115319
--select notes from Passenger where PassengerRequestorID =10011159871 



GO


