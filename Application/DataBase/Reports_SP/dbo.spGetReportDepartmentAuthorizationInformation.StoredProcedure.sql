IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDepartmentAuthorizationInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDepartmentAuthorizationInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportDepartmentAuthorizationInformation]
	@UserCD VARCHAR(30)
AS
-- =============================================
-- SPC Name: spGetReportDepartmentAuthorizationInformation
-- Author: SUDHAKAR J
-- Create date: 22 Jun 2012
-- Description: Get the Department Authorization Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
--
-- =============================================
SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		D.CustomerID
		,D.DepartmentCD
		,D.DepartmentName
		,C.ClientCD		
		,DA.AuthorizationCD		
		,DA.DeptAuthDescription
	FROM
		Department D 
		LEFT OUTER JOIN DepartmentAuthorization DA
			ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
			AND DA.IsInActive = 0 AND D.DepartmentID = DA.DepartmentID
		LEFT OUTER JOIN Client C ON D.ClientID = C.ClientID
	WHERE ISNULL(D.IsDeleted, 0) = 0
		AND D.IsInActive = 0
		AND D.CustomerID = @CUSTOMERID
		AND ( D.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	ORDER BY CustomerID,DepartmentCD

-- EXEC spGetReportDepartmentAuthorizationInformation 'TIM'

GO


