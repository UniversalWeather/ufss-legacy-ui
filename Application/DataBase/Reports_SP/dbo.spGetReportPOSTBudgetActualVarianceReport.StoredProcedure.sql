IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBudgetActualVarianceReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBudgetActualVarianceReport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTBudgetActualVarianceReport]
    @UserCD AS VARCHAR(30),
    @UserCustomerID  VARCHAR(15),
	@YEAR VARCHAR(15),
    @HomeBaseCD AS VARCHAR(6) =''


AS
BEGIN


-- ===============================================================================
-- SPC Name: spGetReportPOSTBudgetActualVarianceReport
-- Author:Askar
-- Create date: 
-- Description: Get Budget/Actual Variance Detail for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON;
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@CUSTOMERID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@CUSTOMERID
DECLARE  @DATEFROM  DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @DATETO DATE =(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0))))  

--SELECT * FROM Account WHERE AccountID = 10024143398
--SELECT FederalTax,* FROM Company WHERE CustomerID=10024 AND HomebaseID=1002442378
DECLARE @AccountNum TABLE(RowID INT IDENTITY,
				          AccountNum VARCHAR(32))


INSERT INTO @AccountNum
SELECT DISTINCT FederalTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                   WHERE C.CustomerID=@CUSTOMERID 
                   AND FederalTax IS NOT NULL 
                   AND FederalTax <> ''
                   
UNION ALL
SELECT DISTINCT SaleTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
               WHERE C.CustomerID=@CUSTOMERID 
               AND SaleTax IS NOT NULL 
               AND SaleTax <> ''
UNION ALL
SELECT DISTINCT StateTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID 
				   WHERE C.CustomerID=@CUSTOMERID 
				   AND StateTax IS NOT NULL 
				   AND StateTax <> ''
	


---Retrieving User Homebase
IF @HomeBaseCD=''
BEGIN

SELECT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=@CUSTOMERID


END


---To get the Budget Details for each account 
DECLARE @Budget TABLE(AccountNo VARCHAR(32),
                      Account VARCHAR(40),
                      Budget  NUMERIC(17,2),
                      Actual  NUMERIC(17,2),
                      Variance  NUMERIC(17,2),
                      VariancePercent  NUMERIC(17,2),
                      HomeBase VARCHAR(100)
                       )


------------------
---Retrieving Actual Details for each Account with-in the give date range
INSERT INTO @Budget
SELECT Budget.AccountNum,Budget.Account,Budget,SUM(Actual) Actual,Variance,VariancePercent,HomeBase FROM 
(
SELECT DISTINCT  AC.AccountNum,AC.AccountDescription Account,NULL Budget,
				 Actual = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX --AND AC.AccountDescription <> 'FEDERAL TAX' 
					  WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX --AND AC.AccountDescription <> 'SALE TAX' 
					  WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX --AND AC.AccountDescription <> 'STAET TAX' 
					  ELSE PE.ExpenseAMT END,
				0 Variance,0 VariancePercent,A.IcaoID+' - '+A.AirportName HomeBase
																					            
			 FROM Account AC  LEFT OUTER  JOIN Budget BG ON BG.AccountID=AC.AccountID
							  LEFT OUTER  JOIN PostflightExpense PE ON PE.AccountID=AC.AccountID AND PE.IsDeleted = 0
							  INNER JOIN PostflightMain PM on PM.POLogID=PE.POLogID  AND PM.IsDeleted=0
							  INNER JOIN PostflightLeg PL ON  PE.POLegID=PL.POLegID AND PL.IsDeleted = 0
							  LEFT OUTER  JOIN Company C ON C.HomebaseID=AC.HomebaseID
							  LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
							  LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
			                  
			 WHERE  PE.PurchaseDT BETWEEN @DATEFROM AND @DATETO
			   AND (A.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))OR @HomeBaseCD='')
			   AND AC.CustomerID = @CUSTOMERID 

)Budget
GROUP BY Budget.AccountNum,Budget.Account,Budget,Variance,VariancePercent,HomeBase


---Insert Empty Accountnum Details
INSERT INTO @Budget
SELECT DISTINCT AC.AccountNum,AC.AccountDescription,0.0,NULL,0.0,0.0,A.IcaoID+' - '+A.AirportName FROM Account AC 
                         LEFT OUTER JOIN Company C ON AC.HomebaseID=C.HomebaseID
                         LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=C.HomebaseID
                         LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
                         LEFT OUTER JOIN Budget BG ON BG.AccountNum=AC.AccountNum
         WHERE  (A.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))OR @HomeBaseCD='')
           AND C.CustomerID=@CUSTOMERID
           AND AC.AccountNum NOT IN(SELECT DISTINCT AccountNo FROM @Budget)


----Updating Budget Values for each Account Num
UPDATE @Budget SET Budget=BG.Budget FROM  
                           (
                          SELECT BG1.AccountNum,SUM(BG1.Budget) Budget FROM
                              (SELECT FiscalYear,BG.AccountNum,ISNULL(BG.Month1Budget,0)+ISNULL(BG.Month2Budget,0)+ISNULL(BG.Month3Budget,0)+ISNULL(BG.Month4Budget,0)+ISNULL(BG.Month5Budget,0)+ISNULL(BG.Month6Budget,0)+ISNULL(BG.Month7Budget,0)+ISNULL(BG.Month8Budget,0)+ISNULL(BG.Month9Budget,0)+ISNULL(BG.Month10Budget,0)+ISNULL(BG.Month11Budget,0)+ISNULL(BG.Month12Budget,0) Budget FROM Budget BG WHERE FiscalYear IN (@YEAR,CASE WHEN @MonthStart >1 THEN @YEAR+1 ELSE @YEAR END) AND CustomerID=@CUSTOMERID AND IsDeleted=0)BG1
                            GROUP BY BG1.AccountNum
                            )BG
               WHERE BG.AccountNum=AccountNo


----Updating Actual Values for Federal Tax
UPDATE @Budget SET Actual=ActualVal.FederalTAX  FROM 
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc FROM
( 
SELECT DISTINCT AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum FROM PostflightMain PM 
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.POLegID = PL.POLEGID AND PE.IsDeleted = 0
                                LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=PM.HomebaseID
                                LEFT OUTER JOIN Company C ON UM.HomebaseID=C.HomebaseID 
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
                                LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
       	 WHERE PurchaseDT BETWEEN @DATEFROM AND @DATETO 
       	 AND PM.CustomerID = @CUSTOMERID
       	 AND PM.IsDeleted=0	
       	 AND (A.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))OR @HomeBaseCD='')
		  -- AND AC.AccountNum IN(SELECT AccountNum FROM @AccountNum WHERE RowID=1)
		)Actual  
GROUP BY Actual.FederalTaxAc  
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNo



UPDATE @Budget SET Actual=ActualVal.SaleTAX  FROM 
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc FROM
( 
SELECT DISTINCT AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum FROM PostflightMain PM 
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PL.POLegID = PE.POLegID AND PE.IsDeleted = 0
                                LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=PM.HomebaseID
                                LEFT OUTER JOIN Company C ON UM.HomebaseID=C.HomebaseID 
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2)  AC ON AC.AccountNum=C.SaleTax
                                LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
       	 WHERE PurchaseDT BETWEEN @DATEFROM AND @DATETO 
       	 AND PM.CustomerID = @CUSTOMERID	
       	 AND PM.IsDeleted=0
       	  AND (A.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))OR @HomeBaseCD='')
		  -- AND AC.AccountNum IN(SELECT AccountNum FROM @AccountNum WHERE RowID=1)
		)Actual  
GROUP BY Actual.SalesTaxAc  
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNo




 
UPDATE @Budget SET Actual=ActualVal.SaleTAX  FROM 
(
SELECT SUM(Actual.SateTAX)SaleTAX,Actual.SateTaxAc FROM
( 
SELECT DISTINCT AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum FROM PostflightMain PM 
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.POLegID = PL.POLegID AND PE.IsDeleted = 0
                                LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=PM.HomebaseID
                                LEFT OUTER JOIN Company C ON UM.HomebaseID=C.HomebaseID 
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3)  AC ON AC.AccountNum=C.StateTax
                                LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
       	 WHERE PurchaseDT BETWEEN @DATEFROM AND @DATETO 
       	 AND PM.CustomerID = @CUSTOMERID	
       	 AND PM.IsDeleted=0
       	  AND (A.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))OR @HomeBaseCD='')
		  -- AND AC.AccountNum IN(SELECT AccountNum FROM @AccountNum WHERE RowID=1)
		)Actual  
GROUP BY Actual.SateTaxAc  
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNo



----Displaying Budget,Acutal and Variance Details for each Account Num.

SELECT LEFT(DateName( month , DateAdd( month , @MonthStart , 0 ) - 1 ),3)+' '+LEFT(@YEAR,4) +' - '
       +LEFT(DateName( month , DateAdd( month , @MonthEnd , 0 ) - 1 ),3)+' '+RIGHT(CASE WHEN @MonthStart > 1 THEN @YEAR+1 ELSE @YEAR END,4) DateRange,
       BudgetData.Account,
       ISNULL(Budget,0) Budget,
       ISNULL(Actual,0)Actual,ISNULL(ISNULL(Budget,0)-ISNULL(Actual,0),0) Variance,
       CASE WHEN Budget=0 OR Budget IS NULL  THEN 0 ELSE  ISNULL(((ISNULL(Budget,0)-ISNULL(Actual,0))/ISNULL(Budget,0))*100,0) END  VariancePercent,
       HomeBase,AccountNo FROM 
(
SELECT Account,Budget,Actual,VariancePercent,HomeBase,AccountNo,Variance FROM @Budget

)BudgetData
ORDER BY AccountNo
  



  
  
END   


-----------EXEC spGetReportPOSTBudgetActualVarianceReport 'SUPERVISOR_16',10016,2012

GO