IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetUtilizationInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetUtilizationInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
CREATE PROCEDURE  [dbo].[spGetReportPOSTFleetUtilizationInformation]   
  (
	  @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @TailNum AS NVARCHAR(1000) = '',
	  @FleetGroupCD AS NVARCHAR(4000) = '',
	  @HomeBaseCD AS VARCHAR(1000) = '', 
	  @IsHomebase BIT = 0
  )  
AS  
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTFleetUtilizationInformation]  
-- Author: Avanikanta Pradhan  
-- Create date: 03 August  
-- Description: Get POST flight Delay Details  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
-----------------------------TailNum and Fleet Group Filteration----------------------
if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1),
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
	INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
	INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------
   DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

declare @units varchar(15) 
 set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'Liters'
				WHEN (cp.FuelPurchase='3') THEN 'Imp. Gallons'
				WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
       
       
       --SELECT * FROM PostflightMain WHERE LogNum=464 AND CustomerID=10099
       --SELECT * FROM PostflightLeg WHERE POLogID=1009910542
       --SELECT * FROM PostflightExpense WHERE POLogID=1009910542
       
  SELECT FleetUtil.DateRange
        ,[Aircraft Type]
        ,FLEETAIRCRAFT
        ,[Tail Number]
        ,POLogID
        ,POLegID
        ,lognum
        ,[No of Trips]
        ,[No of Logs]
        ,[No of Legs]
        ,[Block Hours]=CASE WHEN Rnk=1 THEN  [Block Hours] ELSE 0 END
        ,[Flight Hours]=CASE WHEN Rnk=1 THEN [Flight Hours] ELSE 0 END 
        ,[Avg Blk Hrs/Leg]
        ,[Avg Flt Hrs/Leg]
        ,[Naut Miles]=CASE WHEN Rnk=1 THEN  [Naut Miles] ELSE 0 END
        ,[Avg MI/Leg]
        ,[No of Pax]=CASE WHEN Rnk=1 THEN  [No of Pax] ELSE 0 END
        ,[Fuel Burn]=CASE WHEN Rnk=1 THEN  [Fuel Burn] ELSE 0 END
        ,[Avg FB/Flt Hr]
        ,[FUELQTY]
        ,[FUELPURCHASE]
        ,CFuelPurchase
        ,[Units]
        ,[Gallons]
        ,[FUEL PURCHASE/Total Cost]
        ,[TenToMin]
        ,Rnk

   FROM (
    SELECT DISTINCT 
          [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
		 ,[Aircraft Type] = AC.AircraftCD 
		 ,[FLEETAIRCRAFT] = F.AircraftCD 
		 ,[Tail Number] = F.TailNum 
		 ,[Tripid]= POM.TripID
		 ,[POLogID] = POM.POLogID
		 ,[POLegID] = POL.POLegID
		 ,[lognum]=pom.lognum
		 ,[No of Trips]	= COUNT(CASE WHEN POM.TripID IS NULL THEN POM.POLogID ELSE POM.TripID END)	  
         ,[No of Logs] = COUNT(POM.POLogID)
         ,[No of Legs] = COUNT(POL.POLegID)
         ,[Block Hours] = ISNULL(ROUND(POL.BlockHours,1),0)
		 ,[Flight Hours] = ISNULL(ROUND(POL.FlightHours,1),0)
         ,[Avg Blk Hrs/Leg] = ' ' 
         ,[Avg Flt Hrs/Leg] = ' ' 
         ,[Naut Miles] = ISNULL(POL.Distance,0)
         ,[Avg MI/Leg] = ' '
         ,[No of Pax] = ISNULL(POL.PassengerTotal,0) 
         ,[Fuel Burn] = ISNULL(POL.FuelUsed,0)        
         ,[Avg FB/Flt Hr] = ' '
         ,[FUELQTY] = DBO.GetFuelConversion(poe.FuelPurchase,poe.FuelQTY,@UserCD)
         ,[FUELPURCHASE] = POE.FuelPurchase
         ,CFuelPurchase=C.FuelPurchase
         ,[Units] = @units
         ,[Gallons]= DBO.GetFuelConversion(poe.FuelPurchase,poe.FuelQTY,@UserCD)
         ,[FUEL PURCHASE/Total Cost] = ISNULL(POE.ExpenseAMT,0)
		 ,[TenToMin] = @TenToMin
		 ,ROW_NUMBER()OVER(PARTITION BY POL.POLegID ORDER BY AC.AircraftCD, F.TailNum)Rnk
     FROM PostflightMain POM
	 LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
	 LEFT OUTER JOIN PostflightExpense POE ON POE.POLegID = POL.POLegID AND POE.IsDeleted=0 AND POE.FuelQTY > 0
	 LEFT OUTER JOIN (
	 SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID ,F.AircraftID
	 FROM Fleet F 
	 LEFT OUTER JOIN FleetGroupOrder FGO
	 ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	 LEFT OUTER JOIN FleetGroup FG 
	 ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	 ) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
	INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID
	  LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
      LEFT JOIN Company C ON C.HomebaseID = F.HomebaseID
	  LEFT JOIN Airport AF ON AF.AirportID = C.HomebaseAirportID   
		WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
		AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
		--AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
		AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
				AND AF.IcaoID IN 
				(			SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE AF.IcaoID END, ',')
				) 
		AND POM.IsDeleted = 0
		 
     GROUP BY POL.ScheduledTM, AC.AircraftCD, F.TailNum, POL.BlockHours, POL.FlightHours, POL.Distance, POL.PassengerTotal,
	 POL.FuelUsed, POE.FuelQTY, POE.ExpenseAMT, pom.lognum, pom.tripid, POE.FuelPurchase, POE.PurchaseDT, C.FuelPurchase, POE.UnitPrice, F.AircraftCD,
	POM.POLogID,POL.POLegID, POM.HomebaseID,POL.LegNUM
)FleetUtil
ORDER BY [Aircraft Type],[Tail Number]
--EXEC spGetReportPOSTFleetUtilizationInformation 'supervisor_99','2000-01-01','2020-12-12','','','','',1

GO