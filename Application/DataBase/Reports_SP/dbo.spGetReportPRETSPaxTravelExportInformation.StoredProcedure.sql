IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPaxTravelExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPaxTravelExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPRETSPaxTravelExportInformation]
	(
		@UserCD            AS VARCHAR(30)--='SUPERVISOR_99'    
		,@TripNUM           AS VARCHAR(300)='' --= '2021' --      PreflightMain.TripNUM
		,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
		,@PassengerCD       AS VARCHAR(300)='' --= 'KABUH' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
		,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
		,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
		,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
		,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
		,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
		,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
		,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
		,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
		,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
		,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
		,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
		,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
		,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
		,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
		,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
		,@IsScheduledService      AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
	    ,@ReportHeaderID VARCHAR(30)
	    ,@ReportNumber VARCHAR(5)
	    ,@TempSettings VARCHAR(100)=''
	)
AS

--exec spGetReportPRETSPaxTravelExportInformation @UserCD='SUPERVISOR_99',@TripNUM='2021',@ReportHeaderID='10099165805',@ReportNumber='7'
-- =========================================================================
-- SPC Name: spGetReportPRETSPaxTravelExportInformation
-- Author: AISHWARYA.M
-- Create date: 27 JUNE 2013
-- Description: Get Preflight TripSheet Pax Travel Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ==========================================================================
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsScheduledService = 1
		SET @TripStatus = @TripStatus + 'S,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		
----------------------------

  DECLARE @PaxTSSettings TABLE (LMESSAGE BIT,
								MESSAGEPOS SMALLINT,
								MESSAGE VARCHAR(MAX),
								COLAUTH BIT,
								BUSPERS BIT,
								PURPOSE BIT,
								SIGNATURE BIT,
								DEPCOUNTRY BIT,
								DEPAIRPORT BIT,
								ARRCOUNTRY BIT,
								ARRAIRPORT BIT,
								BLANK BIT,
								ONELEG BIT,
								BLANKLINES SMALLINT,
								ARRCITY BIT,
								ARRICAO BIT,
								ARRSTATE BIT,
								ARRUTCTIME BIT,
								DEPCITY BIT,
								DEPICAO BIT,
								DEPSTATE BIT,
								DEPUTCTIME BIT,
								DISPATCHNO BIT,
								FLTCOST BIT,
								FLTPURPOSE BIT)

INSERT INTO @PaxTSSettings  
EXEC spGetReportPRETSFormatSettings @ReportHeaderID,@ReportNumber,@TempSettings 
DECLARE @BlankLine INT,@BlankForm BIT = 0,@CustomerID BIGINT=dbo.GetCustomerIDbyUserCD(@UserCD)
SELECT @BlankForm=BLANK  FROM @PaxTSSettings



    DECLARE @tblInfo AS TABLE (
		TripID BIGINT
	   ,LegID BIGINT
	   ,LegNUM BIGINT
	   ,PassengerID BIGINT
	   )
    
    BEGIN
    IF @TripNUM = '' 
	BEGIN
	
	INSERT INTO @tblInfo
	SELECT DISTINCT PM.TripID,PL.LegID,PL.LegNUM,ISNULL(REQ.PassengerRequestorID,999)
		FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
				INNER JOIN  Fleet F ON PM.FleetID = F.FleetID 
				INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
				INNER JOIN Airport HB ON CO.HomebaseAirportID = HB.AirportID
				LEFT OUTER JOIN (SELECT LegID, CrewID,DutyType FROM PreflightCrewList WHERE IsDeleted=0) PC ON PL.LegID = PC.LegID 
				LEFT OUTER JOIN (SELECT CrewID, CrewCD FROM Crew) C ON PC.CrewID = C.CrewID
				INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID AND PPL.IsDeleted=0
				INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID 
				LEFT OUTER JOIN Passenger REQ ON PM.PassengerRequestorID = REQ.PassengerRequestorID	 	
				LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
				LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID
				LEFT OUTER JOIN (SELECT AuthorizationID, AuthorizationCD FROM DepartmentAuthorization) DA ON PM.AuthorizationID = DA.AuthorizationID	
		  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND PM.RecordType = 'T' 
			AND PM.IsDeleted=0 
			AND CONVERT(DATE, PM.EstDepartureDT, 101) BETWEEN CONVERT(DATE, @BeginDate, 101) AND CONVERT(DATE, @EndDate, 101)
			AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')   
			AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')   
			AND (DA.AuthorizationCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')    
			AND (FC.FlightCatagoryCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CatagoryCD , ',')) OR @CatagoryCD = '')  
			AND ( PM.ClientID IN (SELECT ClientID FROM Client WHERE ClientCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD  , ','))) OR @ClientCD = '')
			AND (REQ.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD , ',')) OR @RequestorCD = '')  
			AND ((C.CrewCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD , ',')) AND PC.DutyType IN('P','S'))  OR @CrewCD = '') 	
			AND (HB.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD , ',')) OR @HomeBaseCD = '') 
		 	AND PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ','))
	END
	ELSE    
    BEGIN
    INSERT INTO @tblInfo
    SELECT DISTINCT PM.TripID,PL.LegID,PL.LegNUM,P.PassengerRequestorID
			FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
				INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  AND PPL.IsDeleted=0
				INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID 
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			  AND (PM.TripNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '')
			  AND (PL.LegNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')  
			  AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			  AND PM.RecordType = 'T' 
			  AND PM.IsDeleted=0
	END




DECLARE @tblTripInfo TABLE
	(    RowID INT IDENTITY,
		TripNumber BIGINT
		,lowdate DATETIME
		,highdate DATETIME
		,TailNumber VARCHAR(9)
		,AircraftType VARCHAR(10)
		,TripDescription VARCHAR(40)
		,TripRequestor VARCHAR(60) 
		,reqcode VARCHAR(5)
		,ContactPhone VARCHAR(25)
		,DispatchNumber VARCHAR(15)
		,LegNumber BIGINT
		,DIcao CHAR(4)
		,DAirport VARCHAR(25)
		,DCity VARCHAR(25)
		,DStateName VARCHAR(25)
		,Dcountry VARCHAR(60)
		,DLocalTime DATETIME
		,DUTCTime DATETIME
		,AIcao CHAR(4)
		,AAirport VARCHAR(25) 
		,ACity VARCHAR(25)
		,AStateName VARCHAR(25)
		,Acountry VARCHAR(60)
		,ALocalTime DATETIME
		,AUTCTime DATETIME
		,FlightCost NUMERIC(12,2)
		,PaxName VARCHAR(65)
		,PurposeOfFlight CHAR(2)
		,Code VARCHAR(5)
		,PassengerTotal INT
		,OrderNum INT
		,legid BIGINT
		,FlightPurposeDescription VARCHAR(25)
		,Rnk INT
		,CodeVal CHAR(1)
		,PassCD VARCHAR(7)
) 





IF @BlankForm = 1

BEGIN	

DECLARE @BlockPax TABLE(Num INT)
DECLARE @BlockLeg TABLE(LegID BIGINT,Num INT)
DECLARE @Num INT=1
WHILE @Num <= 999
BEGIN

INSERT INTO @BlockPax
SELECT @Num
SET @Num=@Num+1

END	



INSERT INTO @tblTripInfo
 SELECT   TripNumber = PM.TripNUM
			,TD.Dep
			,TD.Arr
			,TailNumber = F.TailNUM
			,AircraftType =A.AircraftCD
			,TripDescription = PM.TripDescription
			,TripRequestor = ISNULL(P.LastName,'')+ ', ' + ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.MiddleInitial,'')
			,reqcode=P.PassengerRequestorCD
			,ContactPhone =P.PhoneNum
			,DispatchNumber = RTRIM(PM.DispatchNUM)
			,LegNumber = PL.LegNUM
			--Departure
			,DIcao = DepA.IcaoID
			,DAirport = DepA.AirportName
			,DCity = DepA.CityName
			,DStateName = DepA.StateName
			,Dcountry = DepA.CountryName
			,DLocalTime = PL.DepartureDTTMLocal
			,DUTCTime = PL.DepartureGreenwichDTTM
			--Arrival
			,AIcao = ArrA.IcaoID
			,AAirport = ArrA.AirportName
			,ACity = ArrA.CityName 
			,AStateName = ArrA.StateName
			,Acountry = ArrA.CountryName
			,ALocalTime = PL.ArrivalDTTMLocal
			,AUTCTime = PL.ArrivalGreenwichDTTM
			,FlightCost = PL.FlightCost
			--Pax Details
			,PaxName = Pax.PassengerName
			,PurposeOfFlight = FP.FlightPurposeCD
			,Code=Pax.PassengerRequestorCD
			,PL.PassengerTotal
			,OrderNUM	
			,PL.LegID
			,FlightPurposeDescription
			,Rnk=ROW_NUMBER()OVER(PARTITION BY PL.LegID ORDER BY PM.TripNum)
			,CodeVal=''
			,CASE WHEN TripNUM <>'' THEN PAX.PassengerRequestorCD ELSE P.PassengerRequestorCD END
		FROM PreflightMain PM
		INNER JOIN 
			( 
				SELECT LegID,PassengerTotal, LegNUM, TripID, DepartICAOID, ArriveICAOID, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM, ArrivalGreenwichDTTM, FlightCost FROM PreflightLeg
				) PL ON PL.TripID = PM.TripID
		INNER JOIN (SELECT MIN(DepartureDTTMLocal)Dep,MAX(ArrivalDTTMLocal) Arr,TripID FROM PreflightLeg WHERE CustomerID=@CustomerID GROUP BY TripID) TD ON TD.TripID=PM.TripID
		INNER JOIN 
			( SELECT LegID, PassengerID, FlightPurposeID,OrderNUM FROM PreflightPassengerList) PPL ON PL.LegID = PPL.LegID
		INNER JOIN
			( SELECT FleetID, TailNUM, AircraftID FROM Fleet) F ON PM.FleetID = F.FleetID
	   INNER JOIN 
			( SELECT PassengerRequestorCD,PassengerRequestorID, LastName, FirstName, MiddleInitial, PassengerName FROM Passenger
			) Pax ON Pax.PassengerRequestorID = PPL.PassengerID
		INNER JOIN(SELECT DISTINCT TripID,LegID FROM  @tblInfo) TI ON TI.TripID=PL.TripID AND TI.LegID=PL.LegID --AND TI.PassengerID=Pax.PassengerRequestorID
		LEFT OUTER JOIN
			( SELECT AircraftID, AircraftCD FROM Aircraft) A ON A.AircraftID = F.AircraftID
		LEFT OUTER JOIN 
			( SELECT AirportID, AirportName, CityName, CountryName, IcaoID, StateName FROM Airport) DepA ON DepA.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN 
			( SELECT AirportID, AirportName, CityName, CountryName, IcaoID, StateName FROM Airport) ArrA ON ArrA.AirportID = PL.ArriveICAOID
		LEFT OUTER JOIN 
			( SELECT PassengerRequestorID, LastName, FirstName, MiddleInitial, PhoneNum,PassengerRequestorCD FROM Passenger
				)P ON  PM.PassengerRequestorID = P.PassengerRequestorID
		LEFT OUTER JOIN
			(SELECT FlightPurposeCD, FlightPurposeID,FlightPurposeDescription FROM FlightPurpose ) FP ON FP.FlightPurposeID = PPL.FlightPurposeID
				
			WHERE PM.CustomerID =@CustomerID 
			ORDER BY PL.TripID, PL.LegNUM, PAX.PassengerName

INSERT INTO @BlockLeg
SELECT DISTINCT legid,Num FROM @tblTripInfo CROSS JOIN @BlockPax


INSERT INTO @tblTripInfo
 SELECT TripNumber
			   ,lowdate
			   ,highdate
			   ,TailNumber
			   ,AircraftType
			   ,TripDescription
			   ,TripRequestor
			   ,reqcode
			   ,ContactPhone
			   ,DispatchNumber
			   ,LegNumber
			   ,DIcao
			   ,DAirport
			   ,DCity
			   ,DStateName
			   ,Dcountry
			   ,DLocalTime
			   ,DUTCTime
			   ,AIcao
			   ,AAirport
			   ,ACity
			   ,AStateName
			   ,Acountry
			   ,ALocalTime
			   ,AUTCTime
			   ,FlightCost
			   ,PaxName='BLOCKED'
			   ,PurposeOfFlight=TC.CountVal
			   ,Code='BLK'
			   ,PassengerTotal=0
			   ,ordernum=2
			   ,TI.legid
			   ,FlightPurposeDescription=''
			   ,Rnk=1
			   ,CodeVal='Z'
			   ,PassCD='BLk'
	                  FROM @tblTripInfo TI
						   INNER JOIN @BlockLeg BL ON BL.LegID=TI.legid
						   INNER JOIN (SELECT legid,PassengerTotal-COUNT(Code) CountVal FROM @tblTripInfo  GROUP BY PassengerTotal,legid)TC ON TC.legid=TI.legid AND CountVal > 0
	                  WHERE Rnk=1
	                    AND BL.Num <= TC.CountVal
	                 



END
ELSE

BEGIN

DECLARE @COUNTER INT=1
DECLARE @LEGCOUNT INT
SET @LEGCOUNT = 21


	WHILE @COUNTER <= @LEGCOUNT 
	BEGIN 

		INSERT INTO @tblTripInfo (PaxName, PurposeOfFlight,LegNumber)
		VALUES (NULL,NULL,0)
		
		SET @COUNTER = @COUNTER + 1;
	END	


END



IF @BlankForm=1
BEGIN

SELECT lowdate
	  ,highdate
	  ,orig_nmbr=ISNULL(TripNumber,0)
	  ,dispatchno=DispatchNumber
	  ,tail_nmbr=TailNumber
	  ,type_code=AircraftType
	  ,trip_desc=TripDescription
	  ,reqcode
	  ,paxname=TripRequestor
	  ,phone=ContactPhone
	  ,fltcost=ISNULL(FlightCost,0)
	  ,leg_num=ISNULL(LegNumber,0)
	  ,legid=ISNULL(TI.legid,0)
	  ,depicao_id=DIcao
	  ,arricao_id=AIcao
	  ,locdep=DLocalTime
	  ,locarr=ALocalTime
	  ,gmtdep=DUTCTime
	  ,gmtarr=AUTCTime
	  ,depcity=DCity
	  ,arrcity=ACity
	  ,depcountry=Dcountry
	  ,arrcountry=Acountry
	  ,depairport=DAirport
	  ,arrairport=AAirport
	  ,sortfield=CASE WHEN PaxName ='BLOCKED' THEN 1 ELSE 0 END
	  ,paxcode=CASE WHEN PaxName ='BLOCKED' THEN '' ELSE ISNULL(Code,'0') END  
	  ,ordernum
	  ,paxname_p=PaxName
	  ,fltpurpose=PurposeOfFlight
	  ,purpose=FlightPurposeDescription
	  ,CodeVal
FROM @tblTripInfo TI
WHERE (PassCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(CASE WHEN @TripNUM <> '' THEN @PassengerCD ELSE @RequestorCD END , ',')) OR @PassengerCD = '') 

ORDER BY TripNumber,leg_num,CodeVal,OrderNum
END

ELSE

BEGIN

SELECT lowdate
	  ,highdate
	  ,orig_nmbr=ISNULL(TripNumber,0)
	  ,dispatchno=DispatchNumber
	  ,tail_nmbr=TailNumber
	  ,type_code=AircraftType
	  ,trip_desc=TripDescription
	  ,reqcode
	  ,paxname=TripRequestor
	  ,phone=ContactPhone
	  ,fltcost=ISNULL(FlightCost,0)
	  ,leg_num=ISNULL(LegNumber,0)
	  ,legid=ISNULL(TI.legid,0)
	  ,depicao_id=DIcao
	  ,arricao_id=AIcao
	  ,locdep=DLocalTime
	  ,locarr=ALocalTime
	  ,gmtdep=DUTCTime
	  ,gmtarr=AUTCTime
	  ,depcity=DCity
	  ,arrcity=ACity
	  ,depcountry=Dcountry
	  ,arrcountry=Acountry
	  ,depairport=DAirport
	  ,arrairport=AAirport
	  ,sortfield=CASE WHEN PaxName ='BLOCKED' THEN 1 ELSE 0 END
	  ,paxcode=CASE WHEN PaxName ='BLOCKED' THEN '' ELSE ISNULL(Code,'0') END  
	  ,ordernum
	  ,paxname_p=PaxName
	  ,fltpurpose=PurposeOfFlight
	  ,purpose=FlightPurposeDescription
	  ,CodeVal
FROM @tblTripInfo TI

END

END


GO


