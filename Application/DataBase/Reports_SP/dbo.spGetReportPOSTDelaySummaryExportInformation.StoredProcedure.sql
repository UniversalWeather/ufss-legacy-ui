IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDelaySummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDelaySummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure  [dbo].[spGetReportPOSTDelaySummaryExportInformation] 
 (
        @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(1000) = '',
		@LogNum AS NVARCHAR(1000) = '',
		@FleetGroupCD NVARCHAR(1000) = '',
		@IsHomebase BIT = 0,
		@SortByTailNo BIT = 0
	)
 AS
-- ===============================================================================
-- SPC Name: [spGetReportPOSTDelaySummaryExportInformation]
-- Author: A.Akhila
-- Create date: 01 August
-- Description: Get POST Delay Summary Export
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
--DROP TABLE #DELAYLOG
--DROP TABLE #TempFleetID
DECLARE @CustomerID BIGINT = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)));
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
(   
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT
)
  
IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CustomerID
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN 

INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CustomerID
END
-------------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE #DELAYLOG
(
	[DateRange] NVARCHAR(100)
	,[tail_nmbr] VARCHAR(9)
	,[lognum] BIGINT
	,[ac_code] CHAR(3)
	,[delaytype] CHAR(2)
	,[descrip] NVARCHAR(100)
	,[tot_line] NVARCHAR(10)
	,[leg_cnt] BIGINT
	,[delaytime] NUMERIC(6,3)
)
INSERT INTO #DELAYLOG
SELECT DISTINCT 
	[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),	       
	[tail_nmbr] = F.TailNum,
	[lognum] = DEL.lognum, 
	[ac_code] = F.AircraftCD,   
	[delaytype] = DEL.delaytype,   
	[descrip] = DEL.descrip,
	[tot_line] = '',   
	[leg_cnt] = DEL.leg_cnt,
	[delaytime] = DEL.delaytime 
FROM 
(
	SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger ,F.AircraftID
	FROM Fleet F 
	left outer JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	left outer JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = @CustomerID
) F
INNER JOIN
	( 
	SELECT  
		POM.FleetID,
		POM.CustomerID,		       
		[lognum] = POM.LogNum, 		       
		[delaytype] = DT.DelayTypeCD,   
		[descrip] = DT.DelayTypeDescription,
		[tot_line] = '',   
		[leg_cnt] = COUNT(POL.POLegID),
		[delaytime] = POL.DelayTM 
	FROM [PostflightLeg] POL
		INNER JOIN PostflightMain POM ON POM.POLogID = POL.POLogID AND POM.IsDeleted=0
		INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
		INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
		INNER JOIN DelayType DT ON DT.DelayTypeID =POL.DelayTypeID
	WHERE POM.CustomerID = @CustomerID
	    AND POL.IsDeleted=0
		AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
		AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
		AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	GROUP BY POM.FleetID, POM.CustomerID, POM.LogNum, DT.DelayTypeCD, DT.DelayTypeDescription, POL.POLegID, POL.DelayTM 
	)DEL on DEL.FleetID = F.FleetID AND DEL.CustomerID = F.CustomerID
		LEFT JOIN Company COM ON COM.HomebaseID = F.HomebaseID	
		LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
		INNER JOIN #TempFleetID T1 ON F.FleetID=T1.FleetID
 
WHERE  ISNULL([delaytype],'' )=(case when @TailNum = '' then [delaytype]  else ISNULL([delaytype],'' ) end)
ORDER BY DEL.lognum, F.AircraftCD, F.TailNum, DEL.delaytype
	
IF @FleetGroupCD <> ''  AND @TailNum <> ''
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
		LEFT OUTER JOIN #DELAYLOG T ON Fleet.TailNum=T.[tail_nmbr]
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  
																F.CustomerID=@CustomerID
																AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
		UNION ALL
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END 
ELSE IF @FleetGroupCD <> ''  
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
																ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
																LEFT OUTER JOIN FleetGroup FG 
																ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
																WHERE  
																F.CustomerID=@CustomerID
																AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
	END  
ELSE IF @TailNum <> '' --OR @FleetGroupCD <> ''
	BEGIN
		INSERT INTO #DELAYLOG(DateRange,[tail_nmbr],[ac_code])
		SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,AircraftCD FROM Fleet 
					   WHERE TailNum NOT IN(SELECT [tail_nmbr]  FROM #DELAYLOG)
						 AND CustomerID=@CustomerID
						 AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	END
	
	--SELECT DISTINCT * FROM #DELAYLOG


IF @SortByTailNo=1
BEGIN
SELECT DISTINCT * FROM #DELAYLOG
ORDER BY tail_nmbr,lognum,delaytype
END 
ELSE
BEGIN
SELECT DISTINCT * FROM #DELAYLOG
ORDER BY lognum,delaytype
END 



--EXEC spGetReportPOSTDelaySummaryExportInformation 'JWILLIAMS_13','2000-01-01','2012-12-12','','','',0,0



GO


