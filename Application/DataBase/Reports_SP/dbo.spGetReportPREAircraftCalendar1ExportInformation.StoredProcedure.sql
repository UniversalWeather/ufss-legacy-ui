IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar1ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar1ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar1ExportInformation]    
    @UserCD AS VARCHAR(30) --Mandatory      
    ,@DATEFROM AS DATETIME --Mandatory      
    ,@DATETO AS DATETIME --Mandatory      
    ,@TailNum AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values      
    ,@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values  
    ,@IsPDF bit = 0       
    --,@HomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase      
AS    
BEGIN    
-- ===============================================================================    
-- SPC Name: spGetReportPREAircraftCalendar1ExportInformation    
-- Author: Askar    
-- Create date: 23 Jul 2012     
-- Description: Get Preflight Aircraft Calendar1 information for REPORTS     
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================    
SET NOCOUNT ON    



----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END


-------------------------------TailNum and Fleet Group Filteration----------------------
    
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';    
DECLARE @ParameterDefinition AS NVARCHAR(100)    
    

 CREATE TABLE #TempFleetCalendar (    
  RowID INT identity(1, 1) NOT NULL    
  ,TailNum VARCHAR(20),DeptDateOrg DATETIME,AircraftID BIGINT,FleetID BIGINT,TripID BIGINT    
  ,LegID BIGINT,DepartICAOID CHAR(4),Depart DATETIME,ArriveICAOID CHAR(4),Arrive DATETIME    
  ,NextLocalDTTM DATETIME, DutyType CHAR(2), PassTotal INT,    
  TripNUM BIGINT,CrewCD VARCHAR(500),Aircraft_Type VARCHAR(10),Ac_Code CHAR(4))    
     


 DECLARE @CurDate TABLE(DateWeek DATE,TailNum VARCHAR(9),FleetID BIGINT,HomeBaseID BIGINT,Ac_Code CHAR(4),Aircraft_Type VARCHAR(10))
 INSERT INTO @CurDate(TailNum,DateWeek,FleetID,HomeBaseID,Ac_Code,Aircraft_Type)
SELECT DISTINCT F.TailNum,T.date,F.FleetID,C.HomebaseAirportID,F.AircraftCD,AT.AircraftCD
    FROM Fleet F  
    INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
    CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@DateFrom,@DateTo)) T
    INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID 
    LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
    WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
      AND F.ISDELETED=0 
      

     
   ---------------------------------------------------------------------  
    
    INSERT INTO #TempFleetCalendar ( TailNum,DeptDateOrg,AircraftID,FleetID,TripID,LegID,DepartICAOID,    
    Depart,ArriveICAOID,Arrive,NextLocalDTTM,DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code)    
        
    SELECT DISTINCT F.TailNum,CONVERT(DATE,PL.DepartureDTTMLocal),A.AircraftID,F.FleetID,PM.TripID,PL.LegID,AD.IcaoID,PL.DepartureDTTMLocal,AA.IcaoID,PL.ArrivalDTTMLocal,    
           PL.NextLocalDTTM,CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END
									ELSE PL.DutyTYPE END,PL.PassengerTotal,PM.TripNUM,SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) CrewList,A.AircraftCD,F.AircraftCD   
    FROM PreflightMain PM      
        INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  AND PL.IsDeleted=0         
         INNER JOIN (          
       SELECT DISTINCT PC2.LegID, CrewList = (          
        SELECT C.CrewCD + ','          
        FROM PreflightCrewList PC1          
        INNER JOIN Crew C ON PC1.CrewID = C.CrewID          
        WHERE PC1.LegID = PC2.LegID 
        ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                	WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)         
        FOR XML PATH('')          
        )                   
       FROM PreflightCrewList PC2           
     ) Crew ON PL.LegID = Crew.LegID      
        INNER JOIN (SELECT AirportID,IcaoID FROM Airport)AD ON AD.AirportID = PL.DepartICAOID      
        INNER JOIN (SELECT AirportID,IcaoID FROM Airport)AA  ON AA.AirportID = PL.ArriveICAOID         
        INNER JOIN (    
       SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.AircraftID    
       FROM Fleet F        
       ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID 
       INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID    
     LEFT OUTER JOIN Aircraft A  ON A.AircraftID=F.AircraftID     
    WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND PM.IsDeleted=0   
    AND PM.TRIPSTATUS IN ('T','H')  
    AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE, @DateFrom) AND CONVERT(DATE, @DateTo)  
    AND PM.RecordType <>'C' 
    
     
    

   
    

 DECLARE @crTailNum  VARCHAR(20),@crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT;    
 DECLARE @crTripID BIGINT, @crLegID BIGINT,@crDepart DATETIME;    
 DECLARE @crDepartICAOID CHAR(4), @crArrive DATETIME, @crArriveICAOID CHAR(4);    
 DECLARE @crNextLocalDTTM DATETIME, @crDutyType CHAR(2),@crPassTotal INT,@crCrewCD VARCHAR(250),@crAircraft_Type VARCHAR(10),@crTripNUM BIGINT;    
 DECLARE @Loopdate DATETIME,@Ac_Code CHAR(4);     
  
  
 DECLARE curCrewCal CURSOR FAST_FORWARD READ_ONLY FOR    
  SELECT TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code    
  FROM #TempFleetCalendar     
  WHERE CONVERT(VARCHAR, Arrive, 101) < CONVERT(VARCHAR, NextLocalDTTM, 101)     
  ORDER BY TailNum, DeptDateOrg;    
    
 OPEN curCrewCal;    
 FETCH NEXT FROM curCrewCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crDepart,     
                              @crArriveICAOID,@crArrive, @crNextLocalDTTM, @crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_Code;    
 WHILE @@FETCH_STATUS = 0    
 BEGIN    
  SET @Loopdate = '';    
  --STEP 1:    
     INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code)    
   VALUES (@crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,@crArrive,       
     @crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 23:59', 101),  NULL, 'R',    
     @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_Code)    
      
  --STEP 2:    
  SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:00', 101));    
  WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @crNextLocalDTTM, 101))    
  BEGIN    
   --PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)    
   INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code)    
   VALUES (@crTailNum, @Loopdate, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:01', 101),       
     --@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),  NULL, 'R',    
     'RON',CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),  NULL, NULL,    
     @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_Code)    
   SET @Loopdate = DATEADD(DAY, 1, @Loopdate);    
  END    
  ----STEP 3:    
  --INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
  -- DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type)    
  -- VALUES (@crTailNum, @Loopdate, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:01', 101),       
  --   @crArriveICAOID, @crNextLocalDTTM,  NULL, 'R',    
  --   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type)    
         
   FETCH NEXT FROM curCrewCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crDepart,     
                              @crArriveICAOID,@crArrive, @crNextLocalDTTM, @crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_Code;    
 END    
 CLOSE curCrewCal;    
 DEALLOCATE curCrewCal;   
 
 -----------------Maintenace----------------
-- DECLARE @CurDate TABLE(DateWeek DATE,TailNum VARCHAR(9),FleetID BIGINT)
-- INSERT INTO @CurDate(TailNum,DateWeek,FleetID)
--SELECT F.TailNum,T.*,PM.FleetID from PreflightMain PM INNER JOIN PreflightLeg PL  ON PM.TripID=PL.TripID
--                                            INNER JOIN Fleet F ON F.FleetID=PM.FleetID
--                                            INNER JOIN #TempFleetID TF ON F.FleetID=TF.FleetID
-- CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@DateFrom,@DateTo)) T
--         WHERE PM.CustomerID=@CUSTOMERID
--          AND (	
--					(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
--				  OR
--					(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
--				  OR
--					(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO)))
--       AND PL.DutyTYPE='MX'
       
      
  
 INSERT INTO #TempFleetCalendar ( TailNum,DeptDateOrg,AircraftID,FleetID,TripID,LegID,DepartICAOID,    
    Depart,ArriveICAOID,Arrive,NextLocalDTTM,DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code)   
SELECT DISTINCT F.TailNum,CONVERT(DATE,PL.DepartureDTTMLocal),F.AircraftID,F.FleetID,PM.TripID,PL.LegID,AA.IcaoID,PL.DepartureDTTMLocal,'KKR',PL.DepartureDTTMLocal,
      PL.ArrivalDTTMLocal,PL.DutyTYPE,PL.PassengerTotal,PM.TripNUM,PM.TripDescription,AT.AircraftCD,F.AircraftCD
                         FROM PreflightMain PM 
                              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=0
                              INNER JOIN Fleet F ON PM.FleetID=F.FleetID
                              LEFT OUTER JOIN Aircraft AT ON F.AircraftID=AT.AircraftID
                              INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON PM.FleetID = F1.FleetID
                              OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, PM.FleetID) AS AA 
WHERE 
 (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
AND PM.CustomerID=@CUSTOMERID
AND PL.DutyTYPE='MX'
AND PM.IsDeleted=0


INSERT INTO #TempFleetCalendar ( TailNum,DeptDateOrg,AircraftID,FleetID,TripID,LegID,DepartICAOID,    
    Depart,ArriveICAOID,Arrive,NextLocalDTTM,DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type,Ac_Code)
 SELECT DISTINCT PM.TailNum,DateWeek,PM.AircraftID,PM.FleetID,PM.TripID,PM.LegID,PM.DepartICAOID,
                CASE WHEN DateWeek=CONVERT(DATE,PM.Arrive) THEN PM.Arrive ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00') END,NULL,
                CASE WHEN DateWeek=CONVERT(DATE,PM.NextLocalDTTM) THEN PM.NextLocalDTTM ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59') END,PM.NextLocalDTTM,PM.DutyType,
                PM.PassTotal,NULL,PM.CrewCD,PM.Aircraft_Type,PM.Ac_Code
            FROM #TempFleetCalendar PM
                  INNER JOIN @CurDate CD ON CD.FleetID=PM.FleetID
         WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,PM.Arrive)
           AND CONVERT(DATE,CD.DateWeek) <= CONVERT(DATE,PM.NextLocalDTTM)
           AND DutyType='MX'

           
     DELETE FROM #TempFleetCalendar WHERE ArriveICAOID='KKR'
 
 --------------End Maintenace-------------------
 
 
 --DELETE FROM #TEMPFLEETCALENDAR WHERE 
 

-----------------------------------------------------------  
--Populate NO trips for rest of the Fleet [Just insert FleetID and Date]  
 INSERT INTO #TempFleetCalendar (FleetID,TailNum, DeptDateOrg,DepartICAOID,Ac_Code,Aircraft_Type)  
  SELECT DISTINCT TD.FleetID,TD.TailNum, TD.DateWeek,A.IcaoID,TD.Ac_Code,TD.Aircraft_Type FROM @CurDate TD 
  INNER JOIN Airport A ON A.AirportID=TD.HomeBaseID
  --OUTER APPLY dbo.GetFleetCurrentAirpotCD(TD.DateWeek, TD.FleetID) AS A  
  WHERE NOT EXISTS(SELECT CC.FleetID,CC.TailNum,CC.DeptDateOrg FROM #TempFleetCalendar CC WHERE CC.FleetID = TD.FleetID AND CC.DeptDateOrg = TD.DateWeek)  
-----------------------------------------------------------   

 

--------- MAIN CALENDAR -------------    
CREATE TABLE #TailTable (  
   SetNumber VARCHAR(10),       
   Tail1 VARCHAR(20)   
  ,Tail2 VARCHAR(20)    
  ,Tail3 VARCHAR(20)    
  ,Tail4 VARCHAR(20)    
  ,Tail5 VARCHAR(20)   
 )     
  
   --SELECT DISTINCT TailNum,Ac_Code,Aircraft_Type,A.AircraftCD FROM #TempFleetCalendar TF LEFT OUTER JOIN Aircraft A ON TF.AircraftID=A.AircraftID ORDER BY Ac_Code  
  
 CREATE TABLE #TEMP(RowID INT IDENTITY,TailNum VARCHAR(20),Ac_Code VARCHAR(4),Aircraft_Type VARCHAR(10))  
 INSERT INTO #TEMP  
 SELECT DISTINCT TailNum,Ac_Code,Aircraft_Type FROM #TempFleetCalendar ORDER BY Ac_Code  

  DECLARE @SetNumber INT=1,    
--@COUNTER INT=0,    
@i INT=1,@j INT=6     
DECLARE @COUNT INT=1                                            
WHILE (@COUNT<=(SELECT  COUNT(distinct(tailnum)) FROM #TempFleetCalendar) )       
          BEGIN        
           INSERT INTO #TailTable(SetNumber,Tail1,Tail2,Tail3,Tail4,Tail5)   
           VALUES(@SetNumber,  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+1),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+2),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+3),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+4))  
         --  SET @COUNTER=@COUNTER+1    
           SET @i=@j    
           SET @j=@j+5    
           SET @SetNumber=@SetNumber+1    
           SET @COUNT=@COUNT+5    
         END    

-------------------------------------    
CREATE  TABLE #TempTable(    
   ID INT identity(1, 1) NOT NULL    
  ,activedate DATE    
  ,tail_nmbr1 VARCHAR(20)     
  ,depicao_id1 VARCHAR(50)    
  ,deptime1 DATETIME    
  ,arricao_id1 CHAR(4)    
  ,arrtime1 DATETIME    
  ,flight1 CHAR(2)    
  ,pax_total1 INT    
  ,orig_nmbr1 BIGINT    
  ,crewlist1 VARCHAR(250)    
  ,tail_nmbr2 VARCHAR(20)    
  ,depicao_id2 VARCHAR(50)  
  ,deptime2 DATETIME    
  ,arricao_id2 CHAR(4)    
  ,arrtime2 DATETIME    
  ,flight2 CHAR(2)    
  ,pax_total2 INT    
  ,orig_nmbr2 BIGINT    
  ,crewlist2 VARCHAR(250)    
  ,tail_nmbr3 VARCHAR(20)    
  ,depicao_id3 VARCHAR(50)     
  ,deptime3 DATETIME    
  ,arricao_id3 CHAR(4)    
  ,arrtime3 DATETIME    
  ,flight3 CHAR(2)    
  ,pax_total3 INT    
  ,orig_nmbr3 BIGINT    
  ,crewlist3 VARCHAR(250)     
  ,tail_nmbr4 VARCHAR(20)    
  ,depicao_id4 VARCHAR(50)   
  ,deptime4 DATETIME    
  ,arricao_id4 CHAR(4)    
  ,arrtime4 DATETIME    
  ,flight4 CHAR(2)    
  ,pax_total4 INT    
  ,orig_nmbr4 BIGINT    
  ,crewlist4 VARCHAR(250)    
  ,tail_nmbr5 VARCHAR(20)    
  ,depicao_id5 VARCHAR(50)    
  ,deptime5 DATETIME    
  ,arricao_id5 CHAR(4)    
  ,arrtime5 DATETIME    
  ,flight5 CHAR(2)    
  ,pax_total5 INT    
  ,orig_nmbr5 BIGINT    
  ,crewlist5 VARCHAR(250)    
  ,tail_1 VARCHAR(20)    
  ,tail_2 VARCHAR(20)    
  ,tail_3 VARCHAR(20)    
  ,tail_4 VARCHAR(20)    
  ,tail_5 VARCHAR(20)    
  ,type_1 VARCHAR(10)    
  ,type_2 VARCHAR(10)     
  ,type_3 VARCHAR(10)     
  ,type_4 VARCHAR(10)     
  ,type_5 VARCHAR(10)   
  ,rptgroup INT)    
 DECLARE @TailVal VARCHAR(20),@ID1 INT,@ID2 INT,@ID3 INT,@ID4 INT,@ID5 INT;          
DECLARE @PrevDt DATE=NULL,@ID INT,@Tail VARCHAR(20),@COUNT1 INT;  
DECLARE @Prevset INT=1,@PrevDeptICAOID CHAR(4),@PrevAType VARCHAR(9);   


  --SET @IsPDF=1          
DECLARE @Set1 INT,@Tail1 VARCHAR(20),@Set2 INT,@Tail2 VARCHAR(20),@Set3 INT,@Tail3 VARCHAR(20),@Set4 INT,@Tail4 VARCHAR(20),@Set5 INT,@Tail5 VARCHAR(20);   
DECLARE CalendarInfo CURSOR FAST_FORWARD READ_ONLY FOR   
SELECT DISTINCT DeptDateOrg,TailNum,DepartICAOID,Depart,ArriveICAOID,Arrive,    
                DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type, ac_code FROM #TempFleetCalendar    
  ORDER BY  ac_code,DeptDateOrg,depart   
          
 OPEN CalendarInfo;              
           
   FETCH NEXT FROM CalendarInfo INTO @crDeptDateOrg,@crTailNum,@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,  
                                   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_code;        
  WHILE @@FETCH_STATUS = 0              
  BEGIN 
    
  SELECT @SetNumber=SetNumber FROM #TailTable WHERE Tail1=@crTailNum OR Tail2=@crTailNum OR Tail3=@crTailNum OR Tail4=@crTailNum OR Tail5=@crTailNum 
  SELECT @TailVal=(CASE(@crTailNum) WHEN Tail1 THEN 1  
						   WHEN Tail2 THEN 2
						   WHEN Tail3 THEN 3
						   WHEN Tail4 THEN 4
						   WHEN Tail5 THEN 5 END) FROM #TailTable WHERE SetNumber=@SetNumber

IF @IsPDF=0
BEGIN
       SELECT DISTINCT @COUNT1=COUNT(*) FROM #TempTable WHERE activedate=@PrevDt AND rptgroup=@SetNumber 

		   IF @COUNT1=2 
			  BEGIN  
			  INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  
			   INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  
			   INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  
			   INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  
			   INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  

			  END  
			ELSE IF (@COUNT1 >1 AND @COUNT1 < 8 AND (SELECT TOP 1 ID FROM #TempTable WHERE (deptime1 IS NOT NULL OR deptime2 IS NOT NULL OR deptime3 IS NOT NULL OR deptime4 IS NOT NULL OR deptime5 IS NOT NULL ) AND rptgroup=@SetNumber AND activedate=@PrevDt) IS NOT NULL)  
			 BEGIN  
				WHILE(@COUNT1<8)  
				 BEGIN  
				  INSERT INTO #TempTable(activedate,rptgroup)  
					VALUES(@PrevDt,@Prevset)  
				  SET @COUNT1=@COUNT1+1    
				 END  
			 END  
  
     

IF(@TailVal=1)  
  
  BEGIN  

IF @PrevDt IS NULL
 
  BEGIN
  
   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)  
     
   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)  

 END
 
 ELSE IF((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr1 IS NULL AND crewlist1 IS NULL  AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL)  
       
       BEGIN  
          SET @ID1=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr1 is NULL  AND crewlist1 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)

           
           UPDATE #TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id1=@crDepartICAOID,deptime1=@crDepart,arricao_id1=@crArriveICAOID,arrtime1=@crArrive,flight1=@crDutyType,pax_total1=@crPassTotal,orig_nmbr1=@crTripNUM,tail_1=@crTailNum,type_1=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND @ID=@ID1
           UPDATE #TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist1=@crCrewCD,tail_1=@crTailNum,type_1=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID1+1 
       
       END  
  
ELSE
   BEGIN  


   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)  
     
   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)  
   END 
END   
   ELSE IF (@TailVal=2) 
BEGIN

IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr2 IS NULL AND crewlist2 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL
       BEGIN  
       
       SET @ID2=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr2 is NULL  AND crewlist2 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)



           UPDATE #TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id2=@crDepartICAOID,deptime2=@crDepart,arricao_id2=@crArriveICAOID,arrtime2=@crArrive,flight2=@crDutyType,pax_total2=@crPassTotal,orig_nmbr2=@crTripNUM,tail_2=@crTailNum,type_2=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID2            
            UPDATE #TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist2=@crCrewCD,tail_2=@crTailNum,type_2=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID2+1 

       END  
ELSE
       BEGIN  

        INSERT INTO #TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO #TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  

   END
  ---
  ELSE IF (@TailVal=3) 
BEGIN

 
IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr3 IS NULL AND crewlist3 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID3=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr3 IS NULL  AND crewlist3 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
          
           UPDATE #TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id3=@crDepartICAOID,deptime3=@crDepart,arricao_id3=@crArriveICAOID,arrtime3=@crArrive,flight3=@crDutyType,pax_total3=@crPassTotal,orig_nmbr3=@crTripNUM,tail_3=@crTailNum,type_3=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID3 
           UPDATE #TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist3=@crCrewCD,tail_3=@crTailNum,type_3=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID3+1

       END
   ELSE
       BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO #TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  
   
         
   END
   
 ELSE IF (@TailVal=4) 
BEGIN


   IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr4 IS NULL AND crewlist4 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID4=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr4 IS NULL  AND crewlist4 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
    
           UPDATE #TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id4=@crDepartICAOID,deptime4=@crDepart,arricao_id4=@crArriveICAOID,arrtime4=@crArrive,flight4=@crDutyType,pax_total4=@crPassTotal,orig_nmbr4=@crTripNUM,tail_4=@crTailNum,type_4=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID4 
           UPDATE #TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist4=@crCrewCD,tail_4=@crTailNum,type_4=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID4+1

       END 
     ELSE
      BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO #TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END   
   END
   
 ELSE IF (@TailVal=5) 
BEGIN


 IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr5 IS NULL AND crewlist5 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID5=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr5 IS NULL  AND crewlist5 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
           
           UPDATE #TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id5=@crDepartICAOID,deptime5=@crDepart,arricao_id5=@crArriveICAOID,arrtime5=@crArrive,flight5=@crDutyType,pax_total5=@crPassTotal,orig_nmbr5=@crTripNUM,tail_5=@crTailNum,type_5=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID5 
           UPDATE #TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist5=@crCrewCD,tail_5=@crTailNum,type_5=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID5+1

       END
  ELSE
       BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO #TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END    
 END
  
  --- 
   

 SET @ID=SCOPE_IDENTITY()
SET @PrevDt=@crDeptDateOrg  
SET @Tail=@crTailNum  
SET @Prevset=@SetNumber  
SET @PrevDeptICAOID=@crDepartICAOID  
SET @PrevAType=@crAircraft_Type 

END

ELSE

BEGIN

IF(@TailVal=1)  
  
  BEGIN  

IF @PrevDt IS NULL
 
  BEGIN
  
   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber) 
  
   --INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
   --             VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber)    
     
 

 END
 
 ELSE IF((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr1 IS NULL AND crewlist1 IS NULL  AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL)  
       
       BEGIN  
          SET @ID1=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr1 is NULL  AND crewlist1 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)

           
           UPDATE #TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id1=@crDepartICAOID,deptime1=@crDepart,arricao_id1=@crArriveICAOID,arrtime1=@crArrive,flight1=@crDutyType,pax_total1=@crPassTotal,orig_nmbr1=@crTripNUM,tail_1=@crTailNum,type_1=@crAircraft_Type,crewlist1=@crCrewCD WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND @ID=@ID1
         --  UPDATE #TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist1=NULL,tail_1=@crTailNum,type_1=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID1+1
       
       END  
  
ELSE
   BEGIN  


   INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
    --INSERT INTO #TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
    --            VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber)    
     
 
   END 
END   
   ELSE IF (@TailVal=2) 
BEGIN

IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr2 IS NULL AND crewlist2 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL
       BEGIN  
       
       SET @ID2=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr2 is NULL  AND crewlist2 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)



           UPDATE #TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id2=@crDepartICAOID,deptime2=@crDepart,arricao_id2=@crArriveICAOID,arrtime2=@crArrive,flight2=@crDutyType,pax_total2=@crPassTotal,orig_nmbr2=@crTripNUM,tail_2=@crTailNum,type_2=@crAircraft_Type,crewlist2=@crCrewCD WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID2
          -- UPDATE #TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist2=NULL,tail_2=@crTailNum,type_2=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID2+1            

       END  
ELSE
       BEGIN  

        INSERT INTO #TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       --INSERT INTO #TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
       --         VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber)

       END  

   END
  ---
  ELSE IF (@TailVal=3) 
BEGIN

 
IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr3 IS NULL AND crewlist3 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID3=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr3 IS NULL  AND crewlist3 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
          
           UPDATE #TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id3=@crDepartICAOID,deptime3=@crDepart,arricao_id3=@crArriveICAOID,arrtime3=@crArrive,flight3=@crDutyType,pax_total3=@crPassTotal,orig_nmbr3=@crTripNUM,tail_3=@crTailNum,type_3=@crAircraft_Type,crewlist3=@crCrewCD WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID3
         --  UPDATE #TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist3=NULL,tail_3=@crTailNum,type_3=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID3+1 

       END
   ELSE
       BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       --INSERT INTO #TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
       --         VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber) 

       END  
   
         
   END
   
 ELSE IF (@TailVal=4) 
BEGIN


   IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr4 IS NULL AND crewlist4 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID4=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr4 IS NULL  AND crewlist4 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
    
           UPDATE #TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id4=@crDepartICAOID,deptime4=@crDepart,arricao_id4=@crArriveICAOID,arrtime4=@crArrive,flight4=@crDutyType,pax_total4=@crPassTotal,orig_nmbr4=@crTripNUM,tail_4=@crTailNum,type_4=@crAircraft_Type,crewlist4=@crCrewCD WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID4 
         --  UPDATE #TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist4=NULL,tail_4=@crTailNum,type_4=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID4+1

       END 
     ELSE
      BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)

      --INSERT INTO #TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
      --          VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
       END   
   END
   
 ELSE IF (@TailVal=5) 
BEGIN


 IF(SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr5 IS NULL AND crewlist5 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg) )  IS NOT NULL 
       BEGIN  
          
          SET @ID5=ISNULL((SELECT TOP 1 ID FROM #TempTable WHERE  rptgroup=@SetNumber AND tail_nmbr5 IS NULL  AND crewlist5 IS NULL AND activedate=CONVERT(DATE,@crDeptDateOrg)),@ID)
           
           UPDATE #TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id5=@crDepartICAOID,deptime5=@crDepart,arricao_id5=@crArriveICAOID,arrtime5=@crArrive,flight5=@crDutyType,pax_total5=@crPassTotal,orig_nmbr5=@crTripNUM,tail_5=@crTailNum,type_5=@crAircraft_Type,crewlist5=@crCrewCD WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID5 
        --   UPDATE #TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist5=NULL,tail_5=@crTailNum,type_5=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID5+1

       END
  ELSE
       BEGIN  
        INSERT INTO #TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)

              --INSERT INTO #TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
              --  VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
       END    
 END
  
  --- 
   

 SET @ID=SCOPE_IDENTITY()
SET @PrevDt=@crDeptDateOrg  
SET @Tail=@crTailNum  
SET @Prevset=@SetNumber  
SET @PrevDeptICAOID=@crDepartICAOID  
SET @PrevAType=@crAircraft_Type 

END  
  
FETCH NEXT FROM CalendarInfo INTO @crDeptDateOrg,@crTailNum,@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,  
                                   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type,@Ac_code;    
END  
CLOSE CalendarInfo;              
DEALLOCATE CalendarInfo; 



 SET @SetNumber=1;

 WHILE (@SetNumber<=(SELECT DISTINCT COUNT(*) FROM #TailTable))
 BEGIN
 UPDATE #TempTable SET tail_1=(SELECT DISTINCT tail_1 FROM #TempTable WHERE rptgroup=@SetNumber AND tail_1 IS NOT NULL),
					   tail_2=(SELECT DISTINCT tail_2 FROM #TempTable WHERE rptgroup=@SetNumber AND tail_2 IS NOT NULL),
					   tail_3=(SELECT DISTINCT tail_3 FROM #TempTable WHERE rptgroup=@SetNumber AND tail_3 IS NOT NULL),
					   tail_4=(SELECT DISTINCT tail_4 FROM #TempTable WHERE rptgroup=@SetNumber AND tail_4 IS NOT NULL),
					   tail_5=(SELECT DISTINCT tail_5 FROM #TempTable WHERE rptgroup=@SetNumber AND tail_5 IS NOT NULL),
					   type_1=(SELECT DISTINCT type_1 FROM #TempTable WHERE rptgroup=@SetNumber AND type_1 IS NOT NULL),
					   type_2=(SELECT DISTINCT type_2 FROM #TempTable WHERE rptgroup=@SetNumber AND type_2 IS NOT NULL),
					   type_3=(SELECT DISTINCT type_3 FROM #TempTable WHERE rptgroup=@SetNumber AND type_3 IS NOT NULL),
					   type_4=(SELECT DISTINCT type_4 FROM #TempTable WHERE rptgroup=@SetNumber AND type_4 IS NOT NULL),
					   type_5=(SELECT DISTINCT type_5 FROM #TempTable WHERE rptgroup=@SetNumber AND type_5 IS NOT NULL)
				WHERE rptgroup=@SetNumber
 SET @SetNumber=@SetNumber+1
 END



 --UPDATE #TempTable SET tail_nmbr1=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE #TempTable SET tail_nmbr2=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE #TempTable SET tail_nmbr3=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE #TempTable SET tail_nmbr4=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE #TempTable SET tail_nmbr5=NULL WHERE orig_nmbr1 IS NULL				  


 SELECT DISTINCT T.ID,T.activedate,T.tail_nmbr1,depicao_id1,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.deptime1,113),13,5),':','')  deptime1,arricao_id1,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.arrtime1,113),13,5),':','')  arrtime1,flight1,
        CASE WHEN flight1='MX' THEN NULL ELSE  pax_total1 END paxtotal1,orig_nmbr1,crewlist1,
        T.tail_nmbr2,T.depicao_id2, REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.deptime2,113),13,5),':','') deptime2,arricao_id2,
        REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.arrtime2,113),13,5),':','') arrtime2,flight2,CASE WHEN flight2='MX' THEN NULL ELSE  pax_total2 END pax_total2,T.orig_nmbr2,T.crewlist2,
        T.tail_nmbr3,T.depicao_id3,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.deptime3,113),13,5),':','')  deptime3,arricao_id3,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.arrtime3,113),13,5),':','') arrtime3,flight3,CASE WHEN flight3='MX' THEN NULL ELSE  pax_total3 END pax_total3,T.orig_nmbr3,T.crewlist3, 
        T.tail_nmbr4,T.depicao_id4,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.deptime4,113),13,5),':','')  deptime4,arricao_id4,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.arrtime4,113),13,5),':','') arrtime4,flight4,CASE WHEN flight4='MX' THEN NULL ELSE  pax_total4 END pax_total4,T.orig_nmbr4,T.crewlist4,
        T.tail_nmbr5,T.depicao_id5,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.deptime5,113),13,5),':','')  deptime5,arricao_id5,REPLACE(SUBSTRING (CONVERT(VARCHAR(30),T.arrtime5,113),13,5),':','') arrtime5,flight5,CASE WHEN flight5='MX' THEN NULL ELSE  pax_total5 END pax_total5,T.orig_nmbr5,T.crewlist5,
        T.tail_1,T.tail_2,T.tail_3,T.tail_4,T.tail_5,T.type_1,T.type_2,T.type_3,T.type_4,T.type_5,T.rptgroup ,
        CONVERT(VARCHAR(5), T.activedate, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS DisplayDT 
        FROM #TempTable T    
        WHERE CONVERT(DATE,activedate) BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)  
        ORDER BY T.rptgroup,activedate                       







IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
DROP TABLE #TEMP

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable

IF OBJECT_ID('tempdb..#TailTable') IS NOT NULL
DROP TABLE #TailTable

IF OBJECT_ID('tempdb..#TempFleetCalendar') IS NOT NULL
DROP TABLE #TempFleetCalendar

END
  
 -- EXEC spGetReportPREAircraftCalendar1ExportInformation 'supervisor_99', '2010-08-01 ', '2010-08-30', '', '' 
 
 --select * from Fleet
 
 --select * from PreflightLeg

GO
