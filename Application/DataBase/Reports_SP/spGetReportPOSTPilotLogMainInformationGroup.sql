IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogMainInformationGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogMainInformationGroup]
/****** Object:  StoredProcedure [dbo].[spGetReportPOSTPilotLogMainInformationGroup]    Script Date: 04/07/2015 11:54:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTPilotLogMainInformationGroup]
(
	@UserCD AS VARCHAR(30), --Mandatory  
	@DATEFROM AS DATETIME, --Mandatory  
	@DATETO AS DATETIME, --Mandatory  
	@CrewCD AS NVARCHAR(1000) = '',  
	@CrewGroupCD AS CHAR(1000) = '',
	@ChecklistCode AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@AircraftCD AS NVARCHAR(1000) = '',
	@FlightCatagoryCD AS CHAR(1000) = '',
	@HomeBaseCD AS CHAR(1000) = '', 
	@IsHomebase BIT = 0
)
AS
declare @defaultcrewrulecd as VARCHAR(20)
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
DECLARE @dutybasis tinyint;
DECLARE @customfieldheader1 as VARCHAR(25)
DECLARE @customfieldheader2 as VARCHAR(25)
DECLARE @customfieldheader3 as VARCHAR(25)
DECLARE @customfieldheader4 as VARCHAR(25)
DECLARE @Specdec3Field as INT
DECLARE @Specdec4Field as INT
DECLARE @DATETILL AS DATETIME
SET @DATETILL = DateAdd(Day, Datediff(Day,0, @DATETO), 1)
set @DATETILL = DateAdd(SECOND, -1, @DATETILL)


DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt, @dutybasis=DutyBasis 
,@customfieldheader1=CrewLogCustomLBLLong1,@customfieldheader2=CrewLogCustomLBLLong2, 
@customfieldheader3=SpecificationLong3, @customfieldheader4=SpecificationLong4,
@Specdec3Field=Specdec3,@Specdec4Field=Specdec4
FROM Company 
WHERE CustomerID = @CUSTOMERID
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

SELECT @defaultcrewrulecd = CrewDutyRules.CrewDutyRuleCD 
FROM Company INNER JOIN CrewDutyRules ON CrewDutyRules.CrewDutyRulesID = Company.CrewDutyID  
WHERE Company.CustomerID = @CUSTOMERID and CrewDutyRules.CustomerID=@CUSTOMERID
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)


--print 'duty basis:'
--PRINT @dutybasis

CREATE TABLE  #TempCrewList( CrewID BIGINT )
CREATE TABLE  #TempCrewGroupList( CrewID BIGINT )
CREATE TABLE  #TempCrewHomeBasesList( CrewID BIGINT )
CREATE TABLE  #TempCrewIsHomebaseList( CrewID BIGINT )

IF @CrewCD IS NOT NULL AND @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewList
	SELECT DISTINCT C.CrewID
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END
ELSE
BEGIN
	INSERT INTO #TempCrewList
	SELECT DISTINCT C.CrewID
	FROM Crew C
	WHERE C.CustomerID = @CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END

IF @CrewGroupCD IS NOT NULL AND @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewGroupList
SELECT DISTINCT  C.CrewID FROM Crew C 
	LEFT OUTER JOIN CrewGroupOrder CGO ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
	LEFT OUTER JOIN CrewGroup CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 AND C.IsDeleted=0 AND C.IsStatus=1
END
ELSE
BEGIN
	INSERT INTO #TempCrewGroupList
	SELECT DISTINCT C.CrewID
	FROM Crew C
	WHERE C.CustomerID = @CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END


IF @IsHomebase=1
BEGIN
INSERT INTO #TempCrewIsHomebaseList
	SELECT DISTINCT  C.CrewID  FROM Crew C 
	INNER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
	INNER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
	INNER JOIN Airport A ON C.HomebaseID=A.AirportID
	WHERE C.CustomerID=@CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END
ELSE
BEGIN
	INSERT INTO #TempCrewIsHomebaseList
	SELECT DISTINCT C.CrewID
	FROM Crew C
	WHERE C.CustomerID = @CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END

IF @HomeBaseCD IS NOT NULL AND @HomeBaseCD <> '' AND @IsHomebase=0
BEGIN
INSERT INTO #TempCrewHomeBasesList
	SELECT DISTINCT  C.CrewID  FROM Crew C 
	LEFT OUTER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
	LEFT OUTER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
	INNER JOIN Airport A ON C.HomebaseID=A.AirportID
	WHERE C.CustomerID=@CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
	AND A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))
END
ELSE
BEGIN
	INSERT INTO #TempCrewHomeBasesList
	SELECT DISTINCT C.CrewID
	FROM Crew C
	WHERE C.CustomerID = @CUSTOMERID AND C.IsDeleted=0 AND C.IsStatus=1
END

SELECT * into #TEMPCREWSFILTERED FROM (
	SELECT CrewID FROM #TempCrewList
	INTERSECT
	SELECT CrewID FROM #TempCrewGroupList
	INTERSECT
	SELECT CrewID FROM #TempCrewHomeBasesList
	INTERSECT
	SELECT CrewID FROM #TempCrewIsHomebaseList
) as crewsintersected

--we get all data into the dataset and let the front end filter out whatever is not needed
select * INTO #TEMPPILOTLOGRESULTWITHDATA  from (select ISNULL(CW.CrewCD,'') + ' ' + ISNULL(CW.LastName,'') + CASE WHEN CW.FirstName IS NOT NULL OR CW.FirstName <> '' THEN ', ' ELSE '' END + ISNULL(CW.FirstName,'') + ' ' + ISNULL(CW.MiddleInitial, '') AS [PILOT] 
,ISNULL(CW.LastName,'') as LastName, 
CrewCD,
POM.CustomerID, POM.HomebaseID, CW.CrewID
from POSTFLIGHTMAIN POM 
inner join PostFLightLeg POL on POL.POLogID = POM.POLogID
INNER JOIN FLeet FL on POM.FLeetID = FL.FLeetID
INNER JOIN Aircraft AC ON AC.AircraftID = FL.AircraftID
LEFT JOIN FLightCatagory FC ON FC.FLightCategoryID = POL.FLightCategoryID
INNER JOIN Airport AP1 ON AP1.AirportID = POL.DepartICAOID
INNER JOIN Airport AP2 ON AP2.AirportID = POL.ArriveICAOID
INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID and POC.POLegID = POL.POLegID
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 AND CW.IsStatus=1
INNER JOIN #TEMPCREWSFILTERED TC ON TC.CrewID = CW.CrewID 
INNER JOIN CrewDutyRules CDR ON CDR.CrewDutyRuleCD = COALESCE(NULLIF(rtrim(POL.CrewCurrency),''), @defaultcrewrulecd)
LEFT OUTER JOIN (SELECT DISTINCT PC1.POLegID, (' ' + LTRIM(RTRIM((
									SELECT RTRIM(C.CrewCD) + ' , '
									FROM PostflightCrew PC
									INNER JOIN Crew C ON PC.CrewID = C.CrewID
									WHERE PC.POLegID = PC1.POLegID
									ORDER BY C.CrewCD
									FOR XML PATH('')
								))) + ' , ' )AS AssociatedCrew
    FROM PostflightCrew PC1 ) ACrew ON POL.POLegID = ACrew.POLegID
LEFT OUTER JOIN CrewDutyType CD ON POL.DutyType=CD.DutyTypeID 
LEFT OUTER  JOIN Client CL ON CL.ClientID=POM.ClientID	
where POM.IsDeleted=0 and POL.IsDeleted=0 and POM.CustomerID= @CUSTOMERID
and CDR.CustomerId = @CUSTOMERID
and POL.POLegId in (select polegid from postflightleg where pologid in 
(select distinct pologid from postflightleg where OutboundDTTM > @DATEFROM and OutboundDTTM < @DATETILL and CustomerId = @CUSTOMERID and isdeleted=0) )
AND (
	Ac.AircraftCD IN (
		SELECT DISTINCT RTRIM(S)
		FROM dbo.SplitString(@AircraftCD, ',')
		)
	OR @AircraftCD IS NULL OR @AircraftCD = ''
	)
AND (
	FC.FlightCatagoryCD IN (
		SELECT DISTINCT RTRIM(S)
		FROM dbo.SplitString(@FlightCatagoryCD, ',')
		)
	OR @FlightCatagoryCD IS NULL OR @FlightCatagoryCD = ''
	)
AND CW.CrewID IN (
	select * from #TEMPCREWSFILTERED
	) 
union all
SELECT ISNULL(CRW.CrewCD,'') + ' ' + ISNULL(CRW.LastName,'') + CASE WHEN CRW.FirstName IS NOT NULL OR CRW.FirstName <> '' THEN ', ' ELSE '' END + ISNULL(CRW.FirstName,'') + ' ' + ISNULL(CRW.MiddleInitial, '') AS [PILOT], 
 ISNULL(CRW.LastName,'') as LastName,
 CRW.CrewCD,
 PSL.CustomerID, PSL.HomebaseID, CRW.CrewID
 FROM PostflightSimulatorLog  PSL
        INNER JOIN Crew CRW ON PSL.CrewID=CRW.CrewID
        LEFT OUTER JOIN CrewDutyType CD ON PSL.DutyTypeID=CD.DutyTypeID 
        LEFT OUTER  JOIN Client CL ON CL.ClientID=PSL.ClientID
WHERE PSL.CustomerID=@CUSTOMERID
AND (PSL.DepartureDTTMLocal BETWEEN @DATEFROM AND @DATETILL OR
        PSL.ArrivalDTTMLocal BETWEEN @DATEFROM AND  @DATETILL)
AND CRW.CrewID IN (
	select * from #TEMPCREWSFILTERED
	) 
) as FULL_SIMULATOR_TRIP 

select *, 1 AS ONOFFGROUPNUM INTO #TEMPPILOTLOGWITHOUTGROUP from (select ISNULL(CRW.CrewCD,'') + ' ' + ISNULL(CRW.LastName,'') + CASE WHEN CRW.FirstName IS NOT NULL OR CRW.FirstName <> '' THEN ', ' ELSE '' END + ISNULL(CRW.FirstName,'') + ' ' + ISNULL(CRW.MiddleInitial, '') AS [PILOT], 
ISNULL(CRW.LastName,'') as LastName,
CRW.CrewCD,
NULL AS CustomerID, NULL AS HomebaseID, TC.CrewID
from #TEMPCREWSFILTERED TC 
INNER JOIN Crew CRW ON TC.CrewID=CRW.CrewID
WHERE TC.CrewID not in (SELECT CrewId from #TEMPPILOTLOGRESULTWITHDATA)
AND (@IsZeroSuppressActivityCrewRpt is null or @IsZeroSuppressActivityCrewRpt = 0)
union
SELECT TPLR.PILOT, TPLR.LastName, TPLR.CrewCD, 
TPLR.CustomerID, TPLR.HomebaseID, TPLR.CrewID
 FROM #TEMPPILOTLOGRESULTWITHDATA TPLR
 ) as PILOTLOGDATA
 
--SELECT * FROM #TEMPPILOTLOGWITHOUTGROUP ORDER BY [PILOT], POLogID, legnum, [DATE]
DECLARE @pilotname varchar(500)
DECLARE @pologid bigint
DECLARE @polegnum int
DECLARE @polegid bigint
DECLARE @pilotlogdate DATE
DECLARE @isdutyend bit
DECLARE @groupnumber integer
set @groupnumber = 1

select tp.PILOT,tp.LastName,tp.CrewCD,tp.CrewID from #TEMPPILOTLOGWITHOUTGROUP tp 
Where tp.CrewCD Is Not NULL
ORDER BY tp.[PILOT]

IF OBJECT_ID('tempdb..#TempCrewList') IS NOT NULL
DROP TABLE #TempCrewList

IF OBJECT_ID('tempdb..#TempCrewGroupList') IS NOT NULL
DROP TABLE #TempCrewGroupList

IF OBJECT_ID('tempdb..#TempCrewHomeBasesList') IS NOT NULL
DROP TABLE #TempCrewHomeBasesList

IF OBJECT_ID('tempdb..#TempCrewIsHomebaseList') IS NOT NULL
DROP TABLE #TempCrewIsHomebaseList

IF OBJECT_ID('tempdb..#TEMPCREWSFILTERED') IS NOT NULL
DROP TABLE #TEMPCREWSFILTERED

IF OBJECT_ID('tempdb..#TEMPPILOTLOGRESULTWITHDATA') IS NOT NULL
DROP TABLE #TEMPPILOTLOGRESULTWITHDATA

IF OBJECT_ID('tempdb..#TEMPPILOTLOGWITHOUTGROUP') IS NOT NULL
DROP TABLE #TEMPPILOTLOGWITHOUTGROUP

-- exec [dbo].[spGetReportPOSTPilotLogMainInformationGroup] 'muthu', '2014-10-11', '2014-10-18'
