IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentChargeBackSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackSummary]											
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @Year AS NVARCHAR(4),
    @UserHomebaseID AS VARCHAR(30),
    @DepartmentCD AS VARCHAR(70)= '',
    @IsHomebase AS BIT = 0
AS 
 -- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentChargeBackSummary
-- Author:
-- Create date:
-- Description: Get FlightHours/BlockHours,Charge Back details based on Department
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================ 
BEGIN

SET NOCOUNT ON;
Declare @SuppressActivityDept BIT  
 	
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	   
 declare @AircraftBasis numeric(1,0);
	  select @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	   
	   
	   
--DECLARE @FromDate DATE =''-- '2009-01-01';
--DECLARE @ToDate DATE = '' --'2009-12-31;

--SET @FromDate = @Year + '-01-01'
--SET @ToDate = @Year + '-12-31'
--SET @FromDate = CONVERT (DATE, @FromDate)
--SET @ToDate = CONVERT (DATE,@ToDate)

DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 

-----Retrieving Department details based on @FromDate and @ToDate
DECLARE @DateTable TABLE (SeqID INT IDENTITY, WorkDate DATE,DepartmentCD VARCHAR(10),DepartmentName VARCHAR(60) );
-----Retrieving Fleet details based on @FromDate and @ToDate	
DECLARE @FleetDate TABLE
  (FLEETID BIGINT,
   TailNum VARCHAR(9),
   HomebaseID BIGINT,
   WorkDate DATE,
   ScheduledMonth SMALLINT,
   ScheduledYear SMALLINT);
INSERT INTO @DateTable SELECT DISTINCT  DATE,D.DepartmentCD,d.DepartmentName FROM Department D CROSS JOIN  [dbo].[fnDateTable](@FromDate,@ToDate)
							        WHERE CustomerID=@UserCustomerID
									 AND IsDeleted=0
									 AND IsInActive=0
									AND  (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')


INSERT INTO @FleetDate
       (FLEETID,
        TailNum,
        HomebaseID,
        -- WorkDate,
        ScheduledMonth,
        ScheduledYear
       )
       SELECT DISTINCT
         FG1.FleetID,
         FG1.TailNum,
         FG1.HomebaseID,
         --DT.WorkDate,
         MONTH(DT.WorkDate),
         YEAR(DT.WorkDate)
        FROM @DateTable DT
        CROSS JOIN Fleet FG1
        --JOIN TailHistory TH1 ON FG1.TailNum = TH1.OldTailNUM
        WHERE (FG1.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
        --(FG1.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID = 0)
        AND FG1.CustomerID = @UserCustomerID
       --SELECT * FROM @FleetDate
----Retrieving leg details based on Department
SELECT FC.* INTO #FleetCalender FROM
       (SELECT DISTINCT
        FG.FleetID,
        --FG.TailNum,
         FG.TailNum  AS [Tail Number], 
        ISNULL(D.DepartmentCD,'ZZZZ') DepartmentCD,
        D.DepartmentCD + '-' + D.DepartmentName AS DepartmentCD1,
        DepartmentName = ISNULL(D.DepartmentName,'ZZZZ'),
        --ISNULL(D.DepartmentName,'NONE') DepartmentDescription ,
        DATEPART(MM,PL.ScheduledTM) AS SCHEDULEDMONTH,
        DATEPART(yyyy,PL.ScheduledTM)  AS ScheduledYear,
        FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END,
        CR.ChargeUnit,
        CR.ChargeRate,
        PL.Distance,
        CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1) END AS FlightBlockHours ,
        CASE WHEN CR.ChargeUnit = 'N' THEN  ISNULL((PL.Distance*CR.ChargeRate),0)
	         WHEN CR.ChargeUnit = 'S' THEN ISNULL(((PL.Distance*CR.ChargeRate)*1.15078),0)
		     WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ISNULL((ROUND(PL.BlockHours,1) *CR.ChargeRate),0)
		     WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ISNULL((ROUND(PL.FlightHours,1) *CR.ChargeRate),0)
				--ELSE ISNULL(CR.ChargeRate,0)
			 END AS ChargeBack ,
       D.IsInActive,
       [TenToMin] = @TenToMin,
       MonthStart = @MonthStart,
	   MonthEnd = @MonthEnd,
	   PM.LogNum,
	   PL.LegNUM
 FROM PostflightMain PM
      JOIN PostflightLeg pl
        ON PM.POLogID = PL.POLogID AND PM.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
      JOIN Fleet FG
       ON FG.FleetID = PM.FleetID AND FG.IsDeleted=0 AND FG.IsInActive=0
      LEFT JOIN Department D
      ON PL.DepartmentID = D.DepartmentID
     -- LEFT JOIN FleetChargeRate FCR
      --ON FG.FleetID = FCR.FleetID
       LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON PL.POLegID = CR.POLegID  
      WHERE --DATEPART(YEAR,PL.ScheduledTM)= DATEPART(YEAR,@YEAR)
      (CONVERT(DATE,PL.ScheduledTM) BETWEEN  CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate))
      AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
      --AND (FG.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID = 0)
      AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
      AND PL.CustomerID = @UserCustomerID
      AND PM.IsDeleted = 0
      ) FC


--SELECT * FROM @DateTable



INSERT INTO #FleetCalender(DepartmentCD,DepartmentName,SCHEDULEDMONTH,ScheduledYear,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,FleetID)
----Inserting month for each Department
   
SELECT DISTINCT D.DepartmentCD,D.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),@TenToMin,@MonthStart, @MonthEnd ,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'Block Hours' WHEN 2 THEN 'Flight Hours' END),ISNULL(TF.FleetID,0)
               FROM Department D CROSS JOIN @DateTable T 
                    LEFT OUTER JOIN #FleetCalender TF ON TF.DepartmentCD=D.DepartmentCD
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentCD=T.DepartmentCD AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH)

                           AND CustomerID=@UserCustomerID
                           AND IsDeleted=0
                           AND D.IsInActive=0
                        -- and DepartmentCD='100'



IF @SuppressActivityDept=0
BEGIN
                    
SELECT DISTINCT FleetID,
      [Tail Number], 
      DepartmentCD,
      DepartmentCD1,
      DepartmentName,
      SCHEDULEDMONTH,
      ScheduledYear,
      FlightBlockHoursLabel,
      ChargeUnit,
      ChargeRate,
      Distance,
      FlightBlockHours ,
      ChargeBack ,
       TenToMin,
      MonthStart,
	   MonthEnd,
	   LogNum,
	   LegNum
 FROM #FleetCalender 
 WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
 --AND (DepartmentCD NOT IN (SELECT ISNULL(DepartmentCD,'DDDD') FROM #FleetCalender WHERE ChargeUnit IS NULL AND ChargeRate IS NULL AND Distance IS NULL AND FlightBlockHours IS NULL AND ChargeBack IS NULL))
 ORDER BY DepartmentCD,SCHEDULEDMONTH
 
END

ELSE
BEGIN

SELECT DISTINCT FleetID,
      [Tail Number], 
      DepartmentCD,
      DepartmentCD1,
      DepartmentName,
      SCHEDULEDMONTH,
      ScheduledYear,
      FlightBlockHoursLabel,
      ChargeUnit,
      ChargeRate,
      Distance,
      FlightBlockHours ,
      ChargeBack ,
      TenToMin,
      MonthStart,
	  MonthEnd,
	  LogNum,
	   LegNum
 FROM #FleetCalender 
 WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')
  AND (DepartmentCD IN (SELECT DepartmentCD FROM #FleetCalender WHERE ChargeUnit IS NOT NULL OR ChargeRate IS NOT NULL OR Distance IS NOT NULL OR FlightBlockHours IS NOT NULL OR ChargeBack IS NOT NULL))
 ORDER BY DepartmentCD,SCHEDULEDMONTH

END 
 
IF OBJECT_ID('tempdb..#FleetCalender') IS NOT NULL
DROP TABLE #FleetCalender 	

IF OBJECT_ID('tempdb..#DepartmentChargeBack') IS NOT NULL
DROP TABLE #DepartmentChargeBack	



END
--	EXEC spGetReportPOSTDepartmentChargeBackSummary 'VIKRAM_99','2010','',0

--EXEC [spGetReportPOSTDepartmentChargeBackDetailByRequestor] 'JWILLIAMS_13',10013,'2011','','',0









GO


