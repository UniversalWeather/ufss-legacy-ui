IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSInternationalDataPaxInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSInternationalDataPaxInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSInternationalDataPaxInformation]
	 @UserCD VARCHAR(30)
	,@TripID VARCHAR(30)
	,@CrewPassengerID VARCHAR(30)
	,@PassengerCD VARCHAR(30) = ''
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSInternationalDataPaxInformation
-- Author: AISHWARYA.M
-- Create date:27 SEP 2012
-- Description: Get Preflight Tripsheet International Data information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================
	SELECT	[PaxCode]=CONVERT(VARCHAR(MAX),P.PassengerRequestorCD)
			,[PaxName]= P.LastName + ', ' + P.FirstName
			,[PDOB]= dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,PP.DateOfBirth) 
			,[PNationality]= ISNULL(CPC.CountryCD,PPC.CountryCD)
			,[PLastName]=P.LastName
			,[PPassportNo]=ISNULL(PP.PassportNUM,'')
			--,[PPassportNo]=dbo.FlightPakDecrypt(ISNULL(CPP.PassportNUM,''))
			,[PFirstName]=P.FirstName
			,[PExpiration]= dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,PP.PassportExpiryDT) 
			,[PInitial]=P.MiddleInitial
			
			FROM Passenger P LEFT OUTER JOIN 
			(SELECT TOP 1 P.PassengerRequestorID,DateOfBirth,CPP.PassportID,CPP.PassportNum,PassportExpiryDT,CPP.IsDeleted,CPP.CountryID,CPP.Choice
				  -- ROW_NUMBER()OVER(PARTITION BY CPP.PassengerRequestorID ORDER BY LEGID) Rnk
                 FROM
                 PreflightMain PM
                 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
                 INNER JOIN PreflightPassengerList PPL  ON PL.LegID = PPL.LegID
                 INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
				 INNER JOIN CrewPassengerPassport CPP ON CPP.PassportID = PPL.PassportID 
															AND CPP.PassengerRequestorID = PPL.PassengerID
															AND CPP.IsDeleted = 0
                 WHERE PPL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			      AND ISNULL(PPL.IsDeleted,0) = 0
			      AND P.PassengerRequestorID = CONVERT(BIGINT,@CrewPassengerID)
			      AND PM.TripID = CONVERT(BIGINT,@TRIPID)
			      )PP ON P.PassengerRequestorID=PP.PassengerRequestorID --AND Rnk=1 
			LEFT OUTER JOIN (SELECT CountryID, CountryCD FROM Country) CPC ON PP.CountryID = CPC.CountryID --AND Rnk=1		
			LEFT OUTER JOIN (SELECT CountryID, CountryCD FROM Country) PPC ON P.CountryID = PPC.CountryID 	
			WHERE P.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			  AND P.PassengerRequestorID = CONVERT(BIGINT,@CrewPassengerID)
			  AND P.PassengerRequestorCD = CASE WHEN ISNULL(@PassengerCD,'') = '' THEN P.PassengerRequestorCD ELSE @PassengerCD END	 	             
			  
	END
	
	--EXEC spGetReportPRETSInternationalDataPaxInformation 'TIM', 10001216915,'10099123637'
--	10001216913
--10001216914
--10001216915

	
	
	
	
	





GO


