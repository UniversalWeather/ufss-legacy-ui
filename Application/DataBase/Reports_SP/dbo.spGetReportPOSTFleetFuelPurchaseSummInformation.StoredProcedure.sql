
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelPurchaseSummInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummInformation]
GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummInformation]            
 (            
  @UserCD AS VARCHAR(30),  --Mandatory            
  @DATEFROM DATETIME, --Mandatory            
  @DATETO DATETIME, --Mandatory            
  @TailNum Varchar(5000)='',--Optional            
  @FleetGroupCD Varchar(5000)='' --Optional            
 )          
-- =============================================          
-- Author: Mathes          
-- Create date: 17-10-2012          
-- Description: Fleet Fuel Purchase Summary Sub- Report           
-- =============================================          
AS          
SET NOCOUNT ON               
BEGIN                
  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';   
  DECLARE @SQLSCRIPTSUB AS NVARCHAR(4000) = '';                
  DECLARE @ParameterDefinition AS NVARCHAR(300)                
  Declare @TailNumber Varchar(10)                  
  Declare @ItmDesc Varchar(2000)
  Declare @ItmAmount Varchar(2000)
  Declare @Itm1 Varchar(150)
  Declare @Itm2 Varchar(100)
  Declare @Itm1All Varchar(150)
  Declare @Itm2All Varchar(2000)
 
  
     IF OBJECT_ID('tempdb..#TmpFPS') is not null                        
        DROP table #TmpFPS    
     IF OBJECT_ID('tempdb..#TmpTail') is not null                
        DROP table #TmpTail      
     Create Table #TmpTail(TailNum Varchar(10))  
    
     Create Table #TmpFPS (
            tail_nmbr Varchar(7) ,     
            ac_code Char(4),                 
            fuel_loc Varchar(25),               
            [desc] Varchar(25),                
            icao_id Varchar(25),      
            fbo_code Varchar(25),         
            purchasedt DATETIME,                
            dispatchno VARCHAR(12) ,   
            city Varchar(25),     
            name Varchar(25),          
            fuelqty Numeric(15,2),      
            fuelpurch Numeric(1,0),        
            fedtax Numeric(17,2),                    
            statetax Numeric(17,2),                     
            salestax Numeric(17,2),      
            autocalc BIT,       
            unitprice Numeric(10,4),    
            expamt Numeric(17,2),                               
            ACCTLABEL Varchar(2000),
            ACCTAMT Varchar(2000))
             
     SET @SQLSCRIPT='Insert Into #TmpFPS 
          SELECT DISTINCT 
            F.TailNum,     
            F.AircraftCD,                 
            FuelLocator.FuelLocatorCD,              
            FuelLocator.FuelLocatorDescription,                
            Airport.IcaoID,        
            FBO.FBOCD ,          
            PostflightExpense.PurchaseDT,                
            PostflightExpense.DispatchNUM,   
            Airport.CityName,  
            Airport.AirportName,       
            PostflightExpense.FuelQTY,      
            PostflightExpense.FuelPurchase,    
            PostflightExpense.FederalTAX,                   
            PostflightExpense.SateTAX,                 
            PostflightExpense.SaleTAX,   
            PostflightExpense.IsAutomaticCalculation,       
            PostflightExpense.UnitPrice,     
            PostflightExpense.ExpenseAMT,
            '''' , '''' 
    FROM   PostflightExpense      
            LEFT OUTER JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID        
            inner JOIN  
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum               
                FROM   Fleet F  
                LEFT OUTER JOIN FleetGroupOrder FGO              
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
                LEFT OUTER JOIN FleetGroup FG               
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
                Where ( F.IsInActive is null OR f.IsInActive=0)  
                AND FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''  
             ) F ON F.FleetID = PostflightExpense.FleetID   
            LEFT OUTER JOIN Airport ON PostflightExpense.AirportID = Airport.AirportID       
            LEFT OUTER JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID       
            LEFT OUTER JOIN Account ON PostflightExpense.AccountID = Account.AccountID      
     WHERE  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) AND PostflightExpense.IsDeleted=0 And            
            CONVERT(DATE,PostflightExpense.PurchaseDT) BETWEEN CONVERT(DATE,''' + convert(varchar(20),@DATEFROM) +''' )AND CONVERT(DATE,'''+convert(varchar(20),@DATETO)  +''') ' 
                             
     IF @TailNum <>''                 
        BEGIN                
         SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';                
        END                
        
     SET @SQLSCRIPT=@SQLSCRIPT + '  Group By  F.TailNum ,  F.AircraftCD,             
            PostflightExpense.FuelLocatorID,              
            FuelLocator.FuelLocatorDescription,   
            FBO.FBOCD,             
            PostflightExpense.AirportID,                 
            PostflightExpense.PurchaseDT,                
            PostflightExpense.DispatchNUM,                
            Airport.AirportName,                
            FBO.FBOVendor,             
            Airport.IcaoID,                
            PostflightExpense.FuelQTY,              
            PostflightExpense.UnitPrice,                    
            PostflightExpense.FederalTAX,                   
            PostflightExpense.SateTAX,                 
            PostflightExpense.SaleTAX,                   
            PostflightExpense.AccountID,              
            Account.AccountDescription,               
            PostflightExpense.ExpenseAMT,            
            FuelLocator.FuelLocatorCD,Airport.CityName,PostflightExpense.FuelPurchase,PostflightExpense.IsAutomaticCalculation'               
                     
     SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNum AS VARCHAR(5000), @FleetGroupCD AS VARCHAR(5000)'                
     EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @TailNum, @FleetGroupCD                
     --Exec spGetReportPOSTFleetFuelPurchaseSummaryExportInformation 'MATT','01-Jan-2000','31-dec-2012','N247RG','f' 
     
    
     INSERT INTO #TmpTail SELECT Distinct tail_nmbr FROM #TmpFPS         
    
     WHILE ( SELECT  COUNT(*) FROM #TmpTail ) > 0             
     BEGIN        
       Select Top 1 @TailNumber=TailNum From #TmpTail  
       
        IF OBJECT_ID('tempdb..#TmpRpt') is not null                
          DROP table #TmpRpt       
        Create Table #TmpRpt(ItmDesc Varchar(2000),ItmAmt Numeric(10,2),ItmDate DateTime)            
                    
       --========================================= 
	SET @SQLSCRIPTSUB='Insert Into #TmpRpt Select DISTINCT Account.AccountDescription ,Sum(PostflightExpense.ExpenseAMT) As Total,           
		 PostflightExpense.PurchaseDT FROM  PostflightExpense        
		 INNER JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID    
		 INNER JOIN Account ON PostflightExpense.AccountID = Account.AccountID          
		 INNER JOIN FLEET  ON Fleet.FleetID = PostflightExpense.FleetID     
		 LEFT OUTER JOIN Airport ON PostflightExpense.AirportID = Airport.AirportID         
		 LEFT OUTER JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID      
		 GROUP By Account.AccountDescription, PostflightExpense.CustomerID,Fleet.TailNum ,PostflightExpense.PurchaseDT		 
		 HAVING PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And          
		 CONVERT(DATE,PostflightExpense.PurchaseDT) BETWEEN CONVERT(DATE,''' + convert(varchar(20),@DATEFROM) +''' )AND CONVERT(DATE,'''+convert(varchar(20),@DATETO)  +''')             
		 AND Fleet.TailNum=@TailNumber Order By PostflightExpense.PurchaseDT '
	    
	    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNumber AS VARCHAR(10)'                
        EXECUTE sp_executesql @SQLSCRIPTSUB, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @TailNumber
	    SET @Itm1All ='' 
	    SET @Itm2All =''  
	     WHILE ( SELECT  COUNT(*) FROM #TmpRpt  ) > 0             
         BEGIN        
          Select Top 1 @Itm1=ItmDesc,@Itm2=ItmAmt From #TmpRpt  
	      
	      IF @Itm1All =''
	         BEGIN
	          SET @Itm1All = @Itm1
	         END
	      ELSE
	         BEGIN
	          SET @Itm1All = @Itm1All + '</BR> ' + @Itm1
	         END
	      
	      IF @Itm2All =''
	         BEGIN
	          SET @Itm2All = @Itm2
	         END
	      ELSE
	         BEGIN
	          SET @Itm2All = @Itm2All + '</BR>' + @Itm2
	         END
	      Delete From #TmpRpt Where ItmDesc=@Itm1
	     END
	     
	    --==============================================
	    Update #TmpFPS Set ACCTLABEL=@Itm1All Where tail_nmbr=@TailNumber 
	    Update #TmpFPS Set ACCTAMT=@Itm2All Where tail_nmbr=@TailNumber 
	    
	    Delete From #TmpTail Where TailNum=@TailNumber 
	  END 	   
     
     Select * From #TmpFPS order by ac_code
     IF OBJECT_ID('tempdb..#TmpFPS') is not null                        
        DROP table #TmpFPS 
       
     IF OBJECT_ID('tempdb..#TmpTail') is not null                
        DROP table #TmpTail     
        
     --EXEC spGetReportPOSTFleetFuelPurchaseSummInformation 'eliza_9','01-Jan-2000','31-Dec-2012'       
END 