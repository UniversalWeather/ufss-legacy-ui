
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCanPassInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCanPassInformation]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSCanPassInformation]
	 @UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
    ,@LegID AS VARCHAR(30)
AS
BEGIN

	-- =========================================================================
	-- SPC Name: spGetReportPRETSCanPassInformation
	-- Author: SINDHUJA.K
	-- Create date: 28 Sep 2012
	-- Description: Get Preflight CanPass Report Export information for REPORTS
	-- Revision History
	-- Date		Name		Ver		Change
	-- 
	-- ==========================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	SELECT DISTINCT
		 [Name] = CO.CompanyName
		,[Telephone] = CO.HomebasePhoneNUM
		,[Fax] = CO.BaseFax
		,[AIRCRAFT_REGISTRATION] = F.TailNum
		,[AIRPORT_DESTINATION] = PL.ArriveICAOID
		,[FBO] = FBD.FBOVendor
		,[DATE_OF_ARRIVAL] = PL.ArrivalDTTMLocal--(Date)Reports
		,[ETA_LOCAL_TIME] = PL.ArrivalDTTMLocal--(Time)Reports
		,[DEPARTURE_POINT_STATE] = AD.StateName
		,[MAKE_MODEL_YEAR_OF_AIRCRAFT] = F.TypeDescription
		,[OWNER_OPERATOR] = FP.OwnerLesse

		------Crew Heading-------
		,[SURNAME] = PCL.CrewLastName
		,[FIRST_NAME] = PCL.CrewFirstName
		,[INI] = PCL.CrewMiddleName
		,[DATE_OF_BIRTH] = CR.BirthDT
		,[SEX] = CR.Gender
		,[CITIZENSHIP] = CCC.CountryName
		,[COUNTRY_OF_RESIDENCE] =CCR.CountryName
		,[LENGTH_OF_STAY_ABSENCE] = Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER)
		,[TRIP_REASON] = ''-- If Crew Member display Trip Reason as "CREW MEMBER"
		
		--------------Passenger Heading-------------
		
		,[P_SURNAME] = PPL.PassengerLastName
		,[P_FIRST_NAME] = PPL.PassengerFirstName
		,[P_INI] = PPL.PassengerMiddleName
		,[P_DATE_OF_BIRTH] = PP.DateOfBirth
		,[P_SEX] = PP.Gender
		,[P_CITIZENSHIP] = CPC.CountryName
		,[P_COUNTRY_OF_RESIDENCE] = CPR.CountryName
		,[P_LENGTH_OF_STAY_ABSENCE] = Cast(DateDiff(DAY,PL.ArrivalDTTMLocal,PL.NextLocalDTTM)AS INTEGER)
		,[P_TRIP_REASON] = FPP.FlightPurposeDescription
		,[P_Declaration] = ''--Remains Empty
		,[P_canpass] = CO.CanPassNum
		
		 FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
		 INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		 INNER JOIN Company CO ON PM.HomebaseID = CO.HomebaseID
		 INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
		 INNER JOIN Fleet F ON PM.FleetID = F.FleetID 
		 INNER JOIN FleetPair FP ON F.FleetID = FP.FleetID 
		 INNER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID
		 INNER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID
		 
		 LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
		 LEFT OUTER JOIN Crew CR ON PCL.CrewID = CR.CrewID
		 LEFT OUTER JOIN Country CCC ON CR.Citizenship = CCC.CountryID
		 LEFT OUTER JOIN Country CCR ON CR.CountryID = CCR.CountryID
		 
		 LEFT OUTER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
		 LEFT OUTER JOIN Passenger PP ON PPL.PassengerID = PP.PassengerRequestorID
		 LEFT OUTER JOIN Country CPC ON PP.CountryID = CPC.CountryID
		 LEFT OUTER JOIN Country CPR ON PP.CountryOfResidenceID = CPR.CountryID
		 LEFT OUTER JOIN FlightPurpose FPP ON PP.FlightPurposeID = FPP.FlightPurposeID 
		 
		 WHERE  M.LegID = CONVERT(BIGINT, @LegID) 
		  AND  PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		 
END

 --EXEC spGetReportPRETSCanPassInformation 'UC', '', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''
 --EXEC spGetReportPRETSCanPassInformation 'UC', '20', '', '', '20120720', '20120725', '', '', '', '', '', '', '','',1,1,1,1,1,1,''	

GO


