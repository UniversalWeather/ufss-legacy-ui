IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightCategoryAnalysisExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightCategoryAnalysisExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTFlightCategoryAnalysisExportInformation] 
		@UserCD AS VARCHAR(30),
		@DATEFROM AS DATETIME,
		@DATETO AS DATETIME,
		@TailNum AS NVARCHAR(4000),
		@FleetGroupCD AS NVARCHAR(4000),
		@FlightCatagoryCD AS NVARCHAR(4000),
		@AircraftCD AS NVARCHAR(4000),
		@IsHomebase  bit=0
AS 
-- ===============================================================================    
-- SPC Name: spGetReportFlightCategoryAnalysis    
-- Author:  A.AKHILA
-- Create date: 01 Aug 2012    
-- Description: Get Crew workLoad  information for REPORTS    
-- Revision History    
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================ 
SET NOCOUNT ON 
-----------------------------TailNum and Fleet Group Filteration----------------------
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------
 
CREATE TABLE #TAILNUMBERLOG
(
	[FLEETID] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[ac_code] CHAR(10)
	,[lognum] BIGINT
	,[legid] BIGINT
	,[cat_code] CHAR(4)
	,[tot_line] NVARCHAR(10)
	,[leg_cnt] BIGINT
	,[taillegs] NVARCHAR(10)
	,[fltcatdesc] NVARCHAR(25)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[pax] NUMERIC(4,0)
)
INSERT INTO #TAILNUMBERLOG
SELECT DISTINCT
	[FLEETID] = F.FleetID 
	,[tail_nmbr] = F.TailNum
	,[ac_code] = F.AircraftCD
	,[lognum] = POM.LogNum
	,[legid] = POL.POLegID
	,[cat_code] = FC.FlightCatagoryCD
	,[tot_line] =''
	,[leg_cnt] = Count(POL.POLegID)
	,[taillegs] =''
	,[fltcatdesc] = FC.FlightCatagoryDescription
	,[blk_hrs] = ROUND(POL.BlockHours,1)
	,[flt_hrs] = ROUND(POL.FlightHours,1)
	,[distance] = POL.Distance
	,[pax] = POL.PassengerTotal
FROM  PostflightLeg POL
	LEFT OUTER JOIN PostflightMain POM ON POM.POLogID = POL.POLogID AND POM.IsDeleted=0
	LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
	LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
	LEFT OUTER JOIN Aircraft AC on AC.AircraftID = F.AircraftID
	INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID
WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
	CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	AND POL.IsDeleted=0
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
	AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
GROUP BY F.FleetID, F.TailNum ,F.AircraftCD ,POM.LogNum ,POL.POLegID ,FC.FlightCatagoryCD ,POL.POLegID, FC.FlightCatagoryDescription
	,POL.BlockHours ,POL.FlightHours ,POL.Distance ,POL.PassengerTotal

CREATE TABLE #TAILNUMBER
(
	[FLEETID] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[ac_code] CHAR(10)
	,[lognum] BIGINT
	,[legid] BIGINT
	,[cat_code] CHAR(4)
	,[tot_line] NVARCHAR(10)
	,[leg_cnt] BIGINT
	,[taillegs] NVARCHAR(10)
	,[fltcatdesc] NVARCHAR(25)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[pax] NUMERIC(4,0)
)
INSERT INTO #TAILNUMBER
SELECT DISTINCT
	[FLEETID] = F.FleetID  
	,[tail_nmbr] = F.TailNum
	,[ac_code] = F.AircraftCD
	,[lognum] = NULL
	,[legid] = NULL
	,[cat_code] = NULL
	,[tot_line] =''
	,[leg_cnt] = NULL
	,[taillegs] =''
	,[fltcatdesc] = NULL
	,[blk_hrs] = NULL
	,[flt_hrs] = NULL
	,[distance] = NULL
	,[pax] = NULL
FROM Fleet F
INNER JOIN @TempFleetID T ON F.FleetID=T.FleetID
WHERE F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
DELETE FROM #TAILNUMBER WHERE FLEETID IN (SELECT DISTINCT FLEETID FROM #TAILNUMBERLOG)

SELECT * FROM #TAILNUMBERLOG
	UNION ALL
SELECT  * FROM #TAILNUMBER
ORDER BY ac_code, tail_nmbr, cat_code



IF OBJECT_ID('tempdb..#TAILNUMBERLOG') IS NOT NULL
DROP TABLE #TAILNUMBERLOG

IF OBJECT_ID('tempdb..#TAILNUMBER') IS NOT NULL
DROP TABLE #TAILNUMBER

--EXEC spGetReportPOSTFlightCategoryAnalysisExportInformation 'SUPERVISOR_99','12/12/2010','12/12/2012','','','','',0



GO


