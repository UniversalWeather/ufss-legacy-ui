IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBBudgetExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBBudgetExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[spGetReportDBBudgetExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
)
AS
 
-- ===============================================================================
-- SPC Name: [spGetReportDBBudgetExportInformation]
-- Author: 
-- Create date: 4 Nov 2013
-- Description: Get Budget Details
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
SELECT
[acctnum] = B.AccountNum
,[desc] = A.AccountDescription
,[fiscalyr] = B.FiscalYear
,[tailnmbr] = F.TailNum
,[mon1] = B.Month1Budget
,[mon2] = B.Month2Budget
,[mon3] = B.Month3Budget
,[mon4] = B.Month4Budget
,[mon5] = B.Month5Budget
,[mon6] = B.Month6Budget
,[mon7] = B.Month7Budget
,[mon8] = B.Month8Budget
,[mon9] = B.Month9Budget
,[mon10] = B.Month10Budget
,[mon11] = B.Month11Budget
,[mon12] = B.Month12Budget
FROM Account A
LEFT OUTER JOIN BUDGET B ON A.AccountID = B.AccountID
LEFT OUTER JOIN Fleet F ON F.FleetID = B.FleetID
WHERE B.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
-- And IsInActive=0

--EXEC spGetReportDBBudgetExportInformation 'SUPERVISOR_99'


GO


