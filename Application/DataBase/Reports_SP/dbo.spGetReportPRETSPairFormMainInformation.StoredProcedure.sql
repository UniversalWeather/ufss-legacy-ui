IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPairFormMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPairFormMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPRETSPairFormMainInformation]
  @UserCD            AS VARCHAR(30)      
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal   
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
 ,@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table
 ,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'
AS  
  
BEGIN 
-- ===================================================================================== 
-- SPC Name: spGetReportPRETSPairFormMainInformation 
-- Author: AISHWARYA.M 
-- Create date:03 OCT 2012 
-- Description: Get Preflight Tripsheet Pair Form information for REPORTS 
-- Revision History 
-- Date Name Ver Change 
-- 
-- ====================================================================================== 

-- [Start] Report Criteria -- 
DECLARE @tblTripInfo AS TABLE ( 
TripID BIGINT 
, LegID BIGINT 
, PassengerID BIGINT 
) 

INSERT INTO @tblTripInfo (TripId, LegID, PassengerID) 
EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate, 
@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD, 
@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
@IsWorkSheet, @IsUnFulFilled, @IsScheduledService 
-- [End] Report Criteria -- 


---- ====================================================================================== 
-- REQUIREMENT:
---------------
-- Displays Legs that has Arrival ICAO ID in US and 
-- Departure ICAO ID is International (International Leg - other than US)
-- A Separate Preview and XL report for each Leg in the trip that satisfies above criteria
-- =======================================================================================

DECLARE @TEMP AS TABLE( [TripID] VARCHAR(30), [LegNum] VARCHAR(30), [SetNum] INT )
DECLARE @COUNT INT,@COUNT1 INT=1;
DECLARE @crTripID BIGINT,@crLegID BIGINT;
--SELECT DISTINCT TripID,LegID FROM  @tblTripInfo

DECLARE C1 CURSOR FOR SELECT DISTINCT TripID,LegID FROM  @tblTripInfo
OPEN C1
FETCH NEXT FROM C1 INTO @crTripID,@crLegID
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @COUNT1 = 1;
	SET @COUNT = 1;	
	


		SET @COUNT=(SELECT CEILING((SELECT ISNULL(PCL.CREWCOUNT,0)+ISNULL(PPL.PAXCOUNT,0) COUNT
	FROM PreflightMain PM 
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
	INNER JOIN Country CD ON AD.CountryID = CD.CountryID
	INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	INNER JOIN Country CA ON AA.CountryID = CA.CountryID
	LEFT OUTER JOIN ( SELECT LEGID, CREWCOUNT = COUNT(*) FROM PreflightCrewList GROUP BY LEGID) PCL ON PL.LegID = PCL.LegID
	LEFT OUTER JOIN ( SELECT LEGID, PAXCOUNT = COUNT(*) FROM PreflightPassengerList GROUP BY LEGID) PPL ON PL.LegID = PPL.LegID
	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
	AND PM.TripID=@crTripID AND PL.LegID=@crLegID
	AND CA.CountryCD = 'US' AND CD.CountryCD <> 'US'
	)))



	-- Excluding 1 Crew [First Pilot]
	SET @COUNT =CEILING((@COUNT-1)/CONVERT(decimal(4,2), 10))

	WHILE @COUNT1 <= @COUNT

		BEGIN

		

		

			INSERT INTO @TEMP
			SELECT [TripID] = CONVERT(VARCHAR, PM.TripID)
			,[LegNum] = CONVERT(VARCHAR, PL.LegNum)
			,[SetNum] = @COUNT1
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
			INNER JOIN Country CD ON AD.CountryID = CD.CountryID
			INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
			INNER JOIN Country CA ON AA.CountryID = CA.CountryID
			LEFT OUTER JOIN ( SELECT LEGID, CREWCOUNT = COUNT(1) FROM PreflightCrewList GROUP BY LEGID) PCL ON PL.LegID = PCL.LegID
			LEFT OUTER JOIN ( SELECT LEGID, PAXCOUNT = COUNT(1) FROM PreflightPassengerList GROUP BY LEGID) PPL ON PL.LegID = PPL.LegID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND PM.TripID=@crTripID AND PL.LegID=@crLegID
			AND CA.CountryCD = 'US' AND CD.CountryCD <> 'US'

			SET @COUNT1 = @COUNT1 + 1
		END
	--SELECT * FROM @TEMP ORDER BY TripID, LegNum, SetNum
	FETCH NEXT FROM C1 INTO @crTripID,@crLegID
END
CLOSE C1;
DEALLOCATE C1;
--START LOOP
	-- LOCAL: @curTRIPID, @curLEGID


	
--END LOOP

-- TO ADD A DUMMY RECORD TO DISPLAY BLANK FORM BASED IN THE FORMAT SETTINGS
----------------------------------------------------------------------------
	DECLARE @FormatSettings TABLE (
		BLANKFORM BIT
		,PAXMNORD BIT
		,BLANKDTTM BIT
	)
	INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 9, @TempSettings
	--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161675, 9, 'BLANKFORM::FALSE' 
	--SELECT * FROM @FormatSettings


	DECLARE	@BLANKFORM BIT, @PAXMNORD BIT, @BLANKDTTM BIT;
	SELECT @BLANKFORM = BLANKFORM, @PAXMNORD = PAXMNORD, @BLANKDTTM = BLANKDTTM FROM @FormatSettings
	--SELECT @BLANKFORM, @PAXMNORD, @BLANKDTTM
	
	DECLARE @ROWCOUNT INT = (SELECT COUNT(1) FROM @TEMP);
	--SELECT @ROWCOUNT
	--IF 	@BLANKFORM  = '0' 
	IF 	@ROWCOUNT > 0 AND @BLANKFORM  = '0' 
	BEGIN
		SELECT TripID, LegNum, SetNum FROM @TEMP --ORDER BY TripID, LegNum, SetNum
		UNION ALL
		SELECT '0','0',0
	END ELSE BEGIN
		SELECT * FROM @TEMP ORDER BY TripID, LegNum, SetNum
	END
	
--SELECT * FROM @TEMP ORDER BY TripID, LegNum, SetNum

END


--EXEC spGetReportPRETSPairFormMainInformation 'JWILLIAMS_13','2005','','','','','','','','','','','','','',1,1,1,1,1, '10013161677', 'BLANKFORM::TRUE'


GO


