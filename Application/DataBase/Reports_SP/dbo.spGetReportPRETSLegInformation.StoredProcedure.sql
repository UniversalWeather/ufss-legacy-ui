IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSLegInformation]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
		,@LegNum AS VARCHAR(150) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSLegInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

 Declare @TenToMin Int
  Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

      SELECT
		    --Tripsheet Message	"tsrptdtl.paramval [Condition - reptid= reptid='DEFAULT' AND rptnum=2 AND rptdesc = 'TRIPSHEET' AND paramdesc='MESSAGE BODY'], 
			--this is memo/text datatype in tsrptdtl table"
			-- [Itinerary Message]	tsrptdtl.paramval [Condition - RPTNUM=3 AND paraMvar='MESSAGE'], this is memo/text datatype in tsrptdtl table
			[LegId] = CONVERT(VARCHAR,PL.LegID)
			,[LegNum] = CONVERT(VARCHAR,PL.LegNum)
			--,[TimePlusMinus] = Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.DepartureGreenwichDTTM)*1.0/60 as numeric(18,2))
			--,[TimePlusMinus] =  Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2)) - (Cast(DateDiff(MINUTE,PL.DepartureGreenwichDTTM,PL.DepartureDTTMLocal)*1.0/60 as numeric(18,2)))
			,[TimePlusMinus] =  (CASE WHEN AD.IsDayLightSaving = 1 
					AND PL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT 
					THEN AD.OffsetToGMT + 1
					ELSE AD.OffsetToGMT END)
					-
					(CASE WHEN AA.IsDayLightSaving = 1 
					AND PL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT 
					THEN AA.OffsetToGMT + 1
					ELSE AA.OffsetToGMT END)
			--,[DepZuluDiff] =  Cast(DateDiff(MINUTE,PL.DepartureGreenwichDTTM,PL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
			--,[ArrZuluDiff] = Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2))
			,[DepZuluDiff] =  CASE WHEN AD.IsDayLightSaving = 1 
								AND PL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT 
								THEN AD.OffsetToGMT + 1
								ELSE AD.OffsetToGMT END
			,[ArrZuluDiff] = CASE WHEN AA.IsDayLightSaving = 1 
								AND PL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT 
								THEN AA.OffsetToGMT + 1
								ELSE AA.OffsetToGMT END
			,[DepLocal] = PL.DepartureDTTMLocal
			,[ArrLocal] = PL.ArrivalDTTMLocal
			,[DepZulu] = PL.DepartureGreenwichDTTM
			,[ArrZulu] = PL.ArrivalGreenwichDTTM
			,[Distance] = PL.Distance
			,[ETE] = PL.ElapseTM
			,[Pax] = PL.PassengerTotal
			,[DepICAOID] = AD.IcaoID
			,[DepAirportName] = AD.AirportName
			,[DepCity] = AD.CityName
			,[DepState] = AD.StateName
			,[DepCountry] = DC.CountryCD
			,[ArrICAOID] = AA.IcaoID
			,[ArrAirportName] = AA.AirportName
			,[ArrCity] = AA.CityName
			,[ArrState] = AA.StateName
			,[ArrCountry] = AC.CountryCD
			,[LegPurpose] = PL.FlightPurpose
			,[LegRequestor] = P.PassengerName --P.LastName + ',' + P.FirstName + ' ' + P.MiddleInitial
			,[Dept] = D.DepartmentCD + ' ' + D.DepartmentName
			,[AuthorizationDescription] = DA.AuthorizationCD + ' ' + DA.DeptAuthDescription
			,[AuthPhone] = DA.AuthorizerPhoneNum
			,[FuelBurn] = ISNULL(dbo.FuelBurn(@UserCD,AR.AircraftCD,PL.ElapseTM,PL.PowerSetting),0)
			,[FuelLoad] = ISNULL(PL.FuelLoad,0)
			,[FltCat] = FC.FlightCatagoryCD
			,[FlightNumber] = PM.FlightNUM
			,[Winds] = PL.WindsBoeingTable
			,[Far] = PL.FedAviationRegNUM
			,[DepDate] = CONVERT(VARCHAR, PL.DepartureDTTMLocal, 6)
			,DepFboVendorName = FBD.FBOVendor
			,DepFboPhone = FBD.PhoneNUM1 
			,ArrFboVendorName = FBA.FBOVendor
			,ArrFboPhone = FBA.PhoneNUM1
			,FboFreq = FBA.Frequency
			,CrewList = SUBSTRING(CR.Crewnames,1,LEN(CR.Crewnames)-1)
			,[IsApproxTM] = PL.IsApproxTM
			,@TenToMin TenToMin
			,[NoGo] = PL.GoTime
			,[FltCatDesc] = FC.FlightCatagoryDescription
			,[DepATISFreq] = ISNULL(AD.ATISFreq,'')
			,[ArrATISFreq] = ISNULL(AA.ATISFreq,'')
            ,[DepAtisPhoneNum] = ISNULL(AD.AtisPhoneNum,'')
            ,[ArrAtisPhoneNum] = ISNULL(AA.AtisPhoneNum,'')
            ,[DepClearence1DEL] = ISNULL(AD.Clearence1DEL,'')
            ,[DepClearence2DEL] = ISNULL(AD.Clearence2DEL,'')
            ,[ArrClearence1DEL] = ISNULL(AA.Clearence1DEL,'')
            ,[ArrClearence2DEL] = ISNULL(AA.Clearence2DEL,'')
			,[CancelDescription] = PM.CancelDescription
			,MinFuel = ISNULL(F.MinimumFuel,0)
			,Mach = CASE PL.PowerSetting WHEN '1' THEN AR.PowerSettings1Description
						WHEN '2' THEN AR.PowerSettings2Description
						WHEN '3' THEN AR.PowerSettings3Description
						ELSE '' END
			,Airspeed = PL.TrueAirSpeed 
				
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN  Airport AD ON PL.DepartICAOID = AD.AirportID --AND PM.CustomerID = A.CustomerID
			INNER JOIN  Airport AA ON PL.ArriveICAOID = AA.AirportID --AND PM.CustomerID = AA.CustomerID
			LEFT OUTER JOIN Aircraft AR ON PM.AircraftID = AR.AircraftID
			LEFT OUTER JOIN Fleet F ON PM.FleetID = F.FleetID
			
			-- ADDED FOR ZULU CALCULATION
			LEFT JOIN DSTRegion DDST ON AD.DSTRegionID = DDST.DSTRegionID
			LEFT JOIN DSTRegion ADST ON AA.DSTRegionID = ADST.DSTRegionID
			
			LEFT OUTER JOIN (
				SELECT CountryCD, CountryID FROM Country
			) DC ON AD.CountryID = DC.CountryID 
			
			LEFT OUTER JOIN (
				SELECT CountryCD, CountryID FROM Country
			) AC ON AA.CountryID = AC.CountryID
			
			LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID = P.PassengerRequestorID
			LEFT OUTER JOIN Department D ON PL.DepartmentID = D.DepartmentID
			LEFT OUTER JOIN DepartmentAuthorization DA ON PL.AuthorizationID = DA.AuthorizationID
			LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
			
			--LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID
			--LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID AND PFL.IsArrivalFBO = 1
			--LEFT OUTER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID AND PFL.IsDepartureFBO = 1
			
			LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
			
			LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
			
			LEFT OUTER JOIN (
                  SELECT DISTINCT PC2.LegID, Crewnames = (
                        SELECT ISNULL(PC1.CrewLastName + ', ','')
                        FROM (
                              SELECT PC.LEGID, [CrewLastName] = C.LastName, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
                                    WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
									WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
                              FROM PreflightCrewList PC
                              INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
                        ) PC1 WHERE PC1.LegID = PC2.LegID
                        ORDER BY [RowID]
                        FOR XML PATH('')
                        )
                  FROM PreflightCrewList PC2
            ) CR ON PL.LegID = CR.LegID

			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''  OR @LegNum is NULL )
			ORDER BY PL.LegNUM
        END	 

GO		