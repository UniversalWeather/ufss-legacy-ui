IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSOverviewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSOverviewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSOverviewInformation]
	@UserCD            AS VARCHAR(30)   
	,@TripID			AS VARCHAR(30)   
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	
AS

-- ================================================================================
-- SPC Name: spGetReportPRETSOverviewInformation
-- Author: ABHISHEK.S
-- Create date: 29th August 2012
-- Description: Get TripSheet Report Writer Overview Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ================================================================================

BEGIN	

SET NOCOUNT ON
		
	 Declare @TenToMin Int
	 Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 
	
	DECLARE @TEMP TABLE(RowID INT,
                    CrewID BIGINT,
                    TripID BIGINT,
                    CREWCD VARCHAR(5),
                    LegNUM BIGINT,
                    Legid BIGINT,
                    DutyType CHAR(1)) 



INSERT INTO @TEMP 
SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY OrderNUM),CW.CrewID,PM.TripID,CREWCD ,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
                                                 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
                                                 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID WHERE PCL.DutyTYPE IN('P') AND PM.TripID IN(@TripID)
                                                 ORDER BY PL.LegID,OrderNUM 
  



INSERT INTO @TEMP 
SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY OrderNUM),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
                                                 INNER  JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
                                                 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
                                                 WHERE PCL.DutyTYPE IN('S') 
                                                 AND PM.TripID IN(@TripID)
                                                 ORDER BY PL.LegID ,OrderNUM 


		--SET @SQLSCRIPT = '
			SELECT	DISTINCT
			[TripN] = PM.TripNUM,
			[Desc] = PM.TripDescription,
			[TripDts] = dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID, @UserCD),
			--[Requestor] = PM.RequestorLastName + ',' + PM.RequestorFirstName + ' ' + PM.RequestorMiddleName,
			[Requestor] = ISNULL(PR.LastName+', ','') + ISNULL(PR.FirstName,'') + ' ' + ISNULL(PR.MiddleInitial,''),
			[TailN] = F.TailNum,
			[PhnNm] = PR.PhoneNum,
			[Type] = AC.AircraftCD,
			[DispatchN] = PM.DispatchNUM,
			[LegN] = PL.LegNUM,
		--	[CrewID] = PCL.CrewID,
			--[CrewCD] = ISNULL(T.CREWCD,'') + ' ' + ISNULL(T1.CREWCD,'') + ' ' + ISNULL(Crew.CrewList,''),
			[CrewCD] = CASE WHEN T.DutyType = 'P' THEN T.CREWCD + ' (PIC)' ELSE '' END 
						+ ' ' + CASE WHEN T1.DutyType = 'S' THEN T1.CREWCD + ' (SIC)' ELSE '' END
						+ ' ' + ISNULL(Crew.CrewList,''),
			--[CrewCD] = PL.AdditionalCrew,
			[Pax] = PL.PassengerTotal,
			--[DepartID] = PL.DepartICAOID,
			--[DepartSt] = AD.StateName,
			[DepartID] = AD.IcaoID,
			[DepartSt] = AD.CityName,			
			--[Zulu1] = AD.OffsetToGMT,
			[Zulu1] = Cast(DateDiff(MINUTE,PL.DepartureGreenwichDTTM,PL.DepartureDTTMLocal)*1.0/60 as numeric(18,2)),
			[DepartLocal] = PL.DepartureDTTMLocal,
			[DepartZulu] = PL.DepartureGreenwichDTTM,
			[ArriveID] = AA.IcaoID,
			[ArriveSt] = AA.CityName,
			--[Zulu2] = AA.OffsetToGMT,
			[Zulu2] = Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2)),
			[ArriveLocal] = PL.ArrivalDTTMLocal,
			[ArriveZulu] = PL.ArrivalGreenwichDTTM,
			[Miles] = PL.Distance,
			[ETE] = PL.ElapseTM,
			[FuelBurn] = dbo.FuelBurn(@UserCD,AC.AircraftCD,PL.ElapseTM,PL.PowerSetting),
			[DFBOdesc] = FBOD.FBOVendor,
			[DFBOphone] = FBOD.PhoneNUM1,
			[DFBOfreq] = FBOD.Frequency,
			[DFBOfuel] = FBOD.FuelBrand,
			[AFBOdesc] = FBOA.FBOVendor,
			[AFBOphone] = FBOA.PhoneNUM1, 
			[AFBOfreq] = FBOA.Frequency,
			[AFBOfuel] = FBOA.FuelBrand,
			[LegPur] = PL.FlightPurpose,
			[Notes] = C.ApplicationMessage,
			[IsApproxTM] = ISNULL(PL.IsApproxTM,0),
			[TenToMin] = @TenToMin,
			[DispatcherName] = UM.FirstName + ' ' + UM.LastName, --PM.DsptnName,
			[DispatcherPhone] = UM.PhoneNum,
			[Dispatcheremail] = UM.EmailID
			
			FROM PREFLIGHTMAIN PM 
				INNER JOIN  PreflightLeg PL ON PM.TripID = PL.TripID  
				INNER JOIN ( SELECT DISTINCT TailNum,FleetID,AircraftCD FROM FLEET) F ON PM.FleetID = F.FleetID
				INNER JOIN AIRPORT AD ON PL.DepartICAOID = AD.AirportID
				INNER JOIN AIRPORT AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN Aircraft AC ON PM.AircraftID = AC.AircraftID
			    INNER JOIN Company C ON PM.CustomerID = C.CustomerID --AND C.HomebaseID = PM.HomebaseID
			    LEFT JOIN Passenger PR ON PM.PassengerRequestorID = PR.PassengerRequestorID
			    
			    LEFT OUTER JOIN (
					SELECT PF.LegID, FD.FBOVendor, FD.PhoneNUM1, FD.Frequency , FD.FuelBrand
					FROM PreflightFBOList PF
					INNER JOIN FBO FD ON PF.FBOID = FD.FBOID AND PF.IsDepartureFBO = 1
				) FBOD ON PL.LegID = FBOD.LegID

			    LEFT OUTER JOIN (
					SELECT PF.LegID, FA.FBOVendor, FA.PhoneNUM1, FA.Frequency , FA.FuelBrand
					FROM PreflightFBOList PF
					INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1
				) FBOA ON PL.LegID = FBOA.LegID
				
				LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
				
				LEFT OUTER JOIN (
					SELECT DISTINCT PC2.LegID, CrewList = (
						SELECT C.CrewCD + ' (' + CASE PC1.DutyTYPE WHEN 'E' THEN 'ENGR' WHEN 'I' THEN 'INST' 
								 WHEN 'A' THEN 'ATND' WHEN 'O' THEN 'OTHER' ELSE '' END + ')' + ' '
						FROM PreflightCrewList PC1
						INNER JOIN Crew C ON PC1.CrewID = C.CrewID
						WHERE PC1.LegID = PC2.LegID
						AND PC1.DutyTYPE NOT IN('P','S')
						ORDER BY C.CrewCD
						FOR XML PATH('')
					 )									
					FROM PreflightCrewList PC2
				) Crew ON PL.LegID = Crew.LegID
				
				LEFT OUTER JOIN (SELECT CREWCD,LegNUM,TripID,DutyType FROM @TEMP  WHERE RowID=1 AND DutyType='P' AND (LegNUM IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
				) T ON T.TripID=PM.TripID AND PL.LegNUM=T.LegNUM
				LEFT OUTER JOIN (SELECT CREWCD,LegNUM,TripID,dutytype FROM @TEMP  WHERE RowID=1 AND DutyType='S'AND (LegNUM IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''))T1 ON T1.TripID=PM.TripID AND T1.LegNUM=PL.LegNUM
				
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		AND PM.TripID = CONVERT(BIGINT,@TripID)
		AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
        AND PM.IsDeleted = 0
        AND C.HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	
	          
   --PRINT @SQLSCRIPT
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@TripNum AS NVARCHAR(100), @Leg AS BIGINT '
 --   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	, @TripNum , @Leg
    

-- EXEC spGetReportPRETSOverviewInformation 'JWILLIAMS_13', '100131835', ''
--EXEC spGetReportPRETSOverviewInformation 'JWILLIAMS_11', '100111827', '1'
 END







GO


