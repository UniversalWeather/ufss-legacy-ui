IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelSlipTransactionsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelSlipTransactionsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTFuelSlipTransactionsInformation]  
(
@UserCD AS VARCHAR(30), --Mandatory  
@DATEFROM AS DATETIME, --Mandatory  
@DATETO AS DATETIME, --Mandatory  
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
@FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values  
)
AS  

-- ===============================================================================  
-- SPC Name: spGetReportPOSTFuelSlipTransactionsInformation  
-- Author:  A.Akhila  
-- Create date: 1 Aug 2012  
-- Description: Get Fuel details from Postflight Report
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
--DROP TABLE  #TEMP

SET NOCOUNT ON  
declare @units varchar(15) 
set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'GAL'
WHEN (cp.FuelPurchase='2') THEN 'LTR'
WHEN (cp.FuelPurchase='3') THEN 'IMP'
WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
ELSE 'KILOS'
END)) from Company cp where 
CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	---LEFT OUTER JOIN FleetGroupOrder FGO
	--ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	--LEFT OUTER JOIN FleetGroup FG 
	--ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	---AND 
	F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration----------------------
SELECT Temp.DateRange ,TEMP.SlipNum, TEMP.PurchaseDate, TEMP.TailNum, TEMP.DispNum, TEMP.ICAOID, TEMP.AirportName,
		TEMP.FuelLoc, TEMP.FBO, TEMP.InvoiceNumber, TEMP.Unit, TEMP.PurchQty, TEMP.UnitPrice, TEMP.ExtndCost, TEMP.PostedPrice,
        ISNULL(TEMP.ExtndCost,0)-(ISNULL(TEMP.PurchQty,0)*ISNULL(TEMP.PostedPrice,0)) CostDiff, CASE WHEN TEMP.ExtndCost <>0 THEN  ((ISNULL(TEMP.ExtndCost,0)-(ISNULL(TEMP.PurchQty,0)* ISNULL(TEMP.PostedPrice,0)))/ISNULL(TEMP.ExtndCost,0))*100 ELSE 0 END PercentDiff
        INTO #TEMP FROM (
SELECT  
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[SlipNum] = SlipNUM  
,[PurchaseDate] = PurchaseDT  
,[TailNum] = F.TailNum  
,[DispNum] = POE.DispatchNUM  
,[ICAOID] = A.IcaoID  
,[AirportName] = A.AirportName   
,[FuelLoc] = FL.FuelLocatorCD  
,[FBO] = FB.FBOVendor  
,[InvoiceNumber] = InvoiceNUM  
,[Unit] = @units
,[PurchQty] =CAST(ROUND((dbo.GetFuelConversion(POE.FuelPurchase,POE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))
,[UnitPrice] = Case WHEN C.FuelPurchase <> POE.FuelPurchase THEN DBO.GetFuelPriceConversion(POE.FuelPurchase,POE.UnitPrice,@UserCD)
                                               ELSE POE.UnitPrice End
,[ExtndCost] = ISNULL(ExpenseAMT, 0)

,[PostedPrice] =Case WHEN C.FuelPurchase <> POE.FuelPurchase THEN DBO.GetFuelPriceConversion(POE.FuelPurchase,POE.PostFuelPrice,@UserCD)
                                               ELSE POE.PostFuelPrice End
,[CostDiff] = ''--ISNULL(CONVERT(BIGINT,ExpenseAMT - ((POE.FuelQTY)*PostFuelPrice)),0)
,[PercentDiff] = ''--ISNULL(CASE WHEN ExpenseAMT = 0 THEN NULL ELSE ((ExpenseAMT - ((POE.FuelQTY)*PostFuelPrice))/ExpenseAMT)*100 END,0)

FROM [PostflightExpense]POE 
INNER JOIN PostflightLeg PL ON POE.POLegID = PL.POLegID AND PL.IsDeleted = 0
INNER JOIN Company C ON C.HomebaseID = POE.HomebaseID 
LEFT JOIN FuelLocator FL ON POE.FuelLocatorID = FL.FuelLocatorID  
LEFT JOIN FBO FB ON POE.FBOID = FB.FBOID  
LEFT JOIN Airport A ON POE.AirportID = A.AirportID  
LEFT JOIN PostflightMain POM ON POE.POLogID = POM.POLogID  

INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger
		FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID WHERE F.IsDeleted = 0
			---AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
		 INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F.FleetID=F1.FleetID


WHERE    POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
CONVERT(DATE,POE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND POE.IsDeleted=0 AND POE.FuelQTY > 0
GROUP BY SlipNUM, PurchaseDT, F.TailNum, POE.DispatchNUM, A.IcaoID, A.AirportName, FL.FuelLocatorCD, 
FB.FBOVendor, FB.FBOVendor, InvoiceNUM, C.FuelPurchase, POE.FuelQTY, POE.FuelPurchase, UnitPrice, ExpenseAMT, PostFuelPrice)Temp

------Retrieve only if Rail Number and Fleet Group filter is empty
IF @TailNum <> '' OR @FleetGroupCD <> '' 
BEGIN
SELECT * FROM #TEMP
END
ELSE 
BEGIN
SELECT * FROM #TEMP
END

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
DROP TABLE  #Temp

--EXEC spGetReportPOSTFuelSlipTransactionsInformation 'BRUCE_98','2013-01-01','2013-01-31', '', ''






GO


