IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuelBurn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FuelBurn]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FuelBurn] (@UserCD AS VARCHAR(30),@AircraftCD VARCHAR(10),@ElapseTM NUMERIC(7,3),@PowerSetting CHAR(1))
RETURNS DECIMAL(10,3)
AS
BEGIN

DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 



DECLARE @AircraftCDVal CHAR(3)=@AircraftCD;



DECLARE @ElapseTMVal NUMERIC(7,3);



SELECT @ElapseTMVal=(CASE(@TenToMin) WHEN 1 THEN FLOOR(@ElapseTM/0.1)*0.1 ELSE FLOOR(@ElapseTM/0.001)*0.001 END) 

DECLARE @PowerSettingVal CHAR(1)=@PowerSetting;
DECLARE @FuelBurnVal DECIMAL(10,3);
IF @PowerSettingVal='1'
BEGIN

SELECT DISTINCT @FuelBurnVal=CASE WHEN  @ElapseTMVal BETWEEN 0 AND 1 THEN ISNULL(MACHFuel1Hrs*@ElapseTMVal,0)
        WHEN  @ElapseTMVal BETWEEN 1.1 AND 2 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0) *ISNULL((@ElapseTMVal-1),0)
        WHEN  @ElapseTMVal BETWEEN 2.1 AND 3 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0) *ISNULL((@ElapseTMVal-2),0)
        WHEN  @ElapseTMVal BETWEEN 3.1 AND 4 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0) *ISNULL((@ElapseTMVal-3),0)
        WHEN  @ElapseTMVal BETWEEN 4.1 AND 5 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+isnull(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+(ISNULL(MACHFuel5Hrs,0) *(@ElapseTMVal-4))
        WHEN  @ElapseTMVal BETWEEN 5.1 AND 6 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+(ISNULL(MACHFuel6Hrs,0) *(@ElapseTMVal-5))
        WHEN  @ElapseTMVal BETWEEN 6.1 AND 7 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+isnull(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+(ISNULL(MACHFuel7Hrs,0) *(@ElapseTMVal-6))
        WHEN  @ElapseTMVal BETWEEN 7.1 AND 8 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+(ISNULL(MACHFuel8Hrs,0) *(@ElapseTMVal-7))
        WHEN  @ElapseTMVal BETWEEN 8.1 AND 9 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+(ISNULL(MACHFuel9Hrs,0) *(@ElapseTMVal-8))
        WHEN  @ElapseTMVal BETWEEN 9.1 AND 10 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+(ISNULL(MACHFuel10Hrs,0) *(@ElapseTMVal-9))
        WHEN  @ElapseTMVal BETWEEN 10.1 AND 11 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL((MACHFuel11Hrs *(@ElapseTMVal-10)),0)
        WHEN  @ElapseTMVal BETWEEN 11.1 AND 12 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL(MACHFuel11Hrs,0)+ISNULL((MACHFuel12Hrs *(@ElapseTMVal-11)),0)
        WHEN  @ElapseTMVal BETWEEN 12.1 AND 13 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL(MACHFuel11Hrs,0)+ISNULL(MACHFuel12Hrs,0)+ISNULL((MACHFuel13Hrs *(@ElapseTMVal-12)),0)
        WHEN  @ElapseTMVal BETWEEN 13.1 AND 14 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL(MACHFuel11Hrs,0)+ISNULL(MACHFuel12Hrs,0)+ISNULL(MACHFuel13Hrs,0)+ISNULL((MACHFuel14Hrs *(@ElapseTMVal-13)),0)
        WHEN  @ElapseTMVal BETWEEN 14.1 AND 15 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL(MACHFuel11Hrs,0)+ISNULL(MACHFuel12Hrs,0)+ISNULL(MACHFuel13Hrs,0)+ISNULL(MACHFuel14Hrs,0)+ISNULL((MACHFuel15Hrs *(@ElapseTMVal-14)),0)
        WHEN  @ElapseTMVal > 15 THEN ISNULL(MACHFuel1Hrs,0)+ISNULL(MACHFuel2Hrs,0)+ISNULL(MACHFuel3Hrs,0)+ISNULL(MACHFuel4Hrs,0)+ISNULL(MACHFuel5Hrs,0)+ISNULL(MACHFuel6Hrs,0)+ISNULL(MACHFuel7Hrs,0)+ISNULL(MACHFuel8Hrs,0)+ISNULL(MACHFuel9Hrs,0)+ISNULL(MACHFuel10Hrs,0)+ISNULL(MACHFuel11Hrs,0)+ISNULL(MACHFuel12Hrs,0)+ISNULL(MACHFuel13Hrs,0)+ISNULL(MACHFuel14Hrs,0)+ISNULL(MACHFuel15Hrs,0)+ISNULL(ROUND(((@ElapseTMVal-15)*MACHFuel15Hrs),0),0)
             END  
            FROM  PreflightLeg PL INNER JOIN PreflightMain PM ON PM.TripID=PL.TripID
							      INNER JOIN Fleet F ON PM.FleetID=F.FleetID
							      INNER JOIN FleetInformation FI ON FI.FleetID=F.FleetID
							      LEFT OUTER JOIN Aircraft A ON A.AircraftID=PM.AircraftID
			WHERE A.AircraftCD=@AircraftCD
			  AND PL.ElapseTM=@ElapseTM
			  AND PL.PowerSetting=@PowerSetting
			  AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
END
ELSE IF @PowerSettingVal='2'
BEGIN
SELECT DISTINCT @FuelBurnVal=CASE WHEN  @ElapseTMVal BETWEEN 0 AND 1 THEN LongRangeFuel1Hrs*@ElapseTMVal
            WHEN  @ElapseTMVal BETWEEN 1.1 AND 2 THEN ISNULL(LongRangeFuel1Hrs,0)+(LongRangeFuel2Hrs *(@ElapseTMVal-1))
            WHEN  @ElapseTMVal BETWEEN 2.1 AND 3 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0) *ISNULL((@ElapseTMVal-2),0)
            WHEN  @ElapseTMVal BETWEEN 3.1 AND 4 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0) *ISNULL((@ElapseTMVal-3),0)
            WHEN  @ElapseTMVal BETWEEN 4.1 AND 5 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0) *ISNULL((@ElapseTMVal-4),0)
            WHEN  @ElapseTMVal BETWEEN 5.1 AND 6 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0) *ISNULL((@ElapseTMVal-5),0)
            WHEN  @ElapseTMVal BETWEEN 6.1 AND 7 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0) *ISNULL((@ElapseTMVal-6),0)
            WHEN  @ElapseTMVal BETWEEN 7.1 AND 8 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0) *ISNULL((@ElapseTMVal-7),0)
            WHEN  @ElapseTMVal BETWEEN 8.1 AND 9 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0) *ISNULL((@ElapseTMVal-8),0)
            WHEN  @ElapseTMVal BETWEEN 9.1 AND 10 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0) *ISNULL((@ElapseTMVal-9),0)
            WHEN  @ElapseTMVal BETWEEN 10.1 AND 11 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0) *ISNULL((@ElapseTMVal-10),0)
            WHEN  @ElapseTMVal BETWEEN 11.1 AND 12 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0)+ISNULL(LongRangeFuel12Hrs,0) *ISNULL((@ElapseTMVal-11),0)
            WHEN  @ElapseTMVal BETWEEN 12.1 AND 13 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0)+ISNULL(LongRangeFuel12Hrs,0)+ISNULL(LongRangeFuel13Hrs,0) *ISNULL((@ElapseTMVal-12),0)
            WHEN  @ElapseTMVal BETWEEN 13.1 AND 14 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0)+ISNULL(LongRangeFuel12Hrs,0)+ISNULL(LongRangeFuel13Hrs,0)+ISNULL(LongRangeFuel14Hrs,0) *ISNULL((@ElapseTMVal-13),0)
            WHEN  @ElapseTMVal BETWEEN 14.1 AND 15 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0)+ISNULL(LongRangeFuel12Hrs,0)+ISNULL(LongRangeFuel13Hrs,0)+ISNULL(LongRangeFuel14Hrs,0)+ISNULL(LongRangeFuel15Hrs,0) *ISNULL((@ElapseTMVal-14),0)
            WHEN  @ElapseTMVal > 15 THEN ISNULL(LongRangeFuel1Hrs,0)+ISNULL(LongRangeFuel2Hrs,0)+ISNULL(LongRangeFuel3Hrs,0)+ISNULL(LongRangeFuel4Hrs,0)+ISNULL(LongRangeFuel5Hrs,0)+ISNULL(LongRangeFuel6Hrs,0)+ISNULL(LongRangeFuel7Hrs,0)+ISNULL(LongRangeFuel8Hrs,0)+ISNULL(LongRangeFuel9Hrs,0)+ISNULL(LongRangeFuel10Hrs,0)+ISNULL(LongRangeFuel11Hrs,0)+ISNULL(LongRangeFuel12Hrs,0)+ISNULL(LongRangeFuel13Hrs,0)+ISNULL(LongRangeFuel14Hrs,0)+ISNULL(LongRangeFuel15Hrs,0)+ROUND(((@ElapseTMVal-15)*ISNULL(LongRangeFuel15Hrs,0)),0) 
            END 
            FROM  PreflightLeg PL INNER JOIN PreflightMain PM ON PM.TripID=PL.TripID
							      INNER JOIN Fleet F ON PM.FleetID=F.FleetID
							      INNER JOIN FleetInformation FI ON FI.FleetID=F.FleetID
							      LEFT OUTER JOIN Aircraft A ON A.AircraftID=PM.AircraftID
			WHERE A.AircraftCD=@AircraftCD
			  AND PL.ElapseTM=@ElapseTM
			  AND PL.PowerSetting=@PowerSetting
			  AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
END
			  
ELSE IF @PowerSettingVal='3'
BEGIN
SELECT DISTINCT @FuelBurnVal=CASE WHEN  @ElapseTMVal BETWEEN 0 AND 1 THEN ISNULL(HighSpeedFuel1Hrs,0)*@ElapseTMVal
            WHEN  @ElapseTMVal BETWEEN 1.1 AND 2 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0) *(@ElapseTMVal-1)
            WHEN  @ElapseTMVal BETWEEN 2.1 AND 3 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+(ISNULL(HighSpeedFuel3Hrs,0) *(@ElapseTMVal-2))
            WHEN  @ElapseTMVal BETWEEN 3.1 AND 4 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+(ISNULL(HighSpeedFuel4Hrs,0) *(@ElapseTMVal-3))
            WHEN  @ElapseTMVal BETWEEN 4.1 AND 5 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+(ISNULL(HighSpeedFuel5Hrs,0) *(@ElapseTMVal-4))
            WHEN  @ElapseTMVal BETWEEN 5.1 AND 6 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+(ISNULL(HighSpeedFuel6Hrs,0) *(@ElapseTMVal-5))
            WHEN  @ElapseTMVal BETWEEN 6.1 AND 7 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+(ISNULL(HighSpeedFuel7Hrs,0) *(@ElapseTMVal-6))
            WHEN  @ElapseTMVal BETWEEN 7.1 AND 8 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+(ISNULL(HighSpeedFuel8Hrs,0) *(@ElapseTMVal-7))
            WHEN  @ElapseTMVal BETWEEN 8.1 AND 9 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+(ISNULL(HighSpeedFuel9Hrs,0) *(@ElapseTMVal-8))
            WHEN  @ElapseTMVal BETWEEN 9.1 AND 10 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+(ISNULL(HighSpeedFuel10Hrs,0) *(@ElapseTMVal-9))
            WHEN  @ElapseTMVal BETWEEN 10.1 AND 11 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+(ISNULL(HighSpeedFuel11Hrs,0) *(@ElapseTMVal-10))
            WHEN  @ElapseTMVal BETWEEN 11.1 AND 12 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+ISNULL(HighSpeedFuel11Hrs,0)+(ISNULL(HighSpeedFuel12Hrs,0) *(@ElapseTMVal-11))
            WHEN  @ElapseTMVal BETWEEN 12.1 AND 13 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+ISNULL(HighSpeedFuel11Hrs,0)+ISNULL(HighSpeedFuel12Hrs,0)+(ISNULL(HighSpeedFuel13Hrs,0) *(@ElapseTMVal-12))
            WHEN  @ElapseTMVal BETWEEN 13.1 AND 14 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+ISNULL(HighSpeedFuel11Hrs,0)+ISNULL(HighSpeedFuel12Hrs,0)+ISNULL(HighSpeedFuel13Hrs,0)+(ISNULL(HighSpeedFuel14Hrs,0) *(@ElapseTMVal-13))
            WHEN  @ElapseTMVal BETWEEN 14.1 AND 15 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+ISNULL(HighSpeedFuel11Hrs,0)+ISNULL(HighSpeedFuel12Hrs,0)+ISNULL(HighSpeedFuel13Hrs,0)+ISNULL(HighSpeedFuel14Hrs,0)+(ISNULL(HighSpeedFuel51Hrs,0) *(@ElapseTMVal-14))
            WHEN  @ElapseTMVal > 15 THEN ISNULL(HighSpeedFuel1Hrs,0)+ISNULL(HighSpeedFuel2Hrs,0)+ISNULL(HighSpeedFuel3Hrs,0)+ISNULL(HighSpeedFuel4Hrs,0)+ISNULL(HighSpeedFuel5Hrs,0)+ISNULL(HighSpeedFuel6Hrs,0)+ISNULL(HighSpeedFuel7Hrs,0)+ISNULL(HighSpeedFuel8Hrs,0)+ISNULL(HighSpeedFuel9Hrs,0)+ISNULL(HighSpeedFuel10Hrs,0)+ISNULL(HighSpeedFuel11Hrs,0)+ISNULL(HighSpeedFuel12Hrs,0)+ISNULL(HighSpeedFuel13Hrs,0)+ISNULL(HighSpeedFuel14Hrs,0)+ISNULL(HighSpeedFuel51Hrs,0)+ROUND(((@ElapseTMVal-15)*ISNULL(HighSpeedFuel51Hrs,0)),0) END 
            FROM  PreflightLeg PL INNER JOIN PreflightMain PM ON PM.TripID=PL.TripID
							      INNER JOIN Fleet F ON PM.FleetID=F.FleetID
							      INNER JOIN FleetInformation FI ON FI.FleetID=F.FleetID
							      LEFT OUTER JOIN Aircraft A ON A.AircraftID=PM.AircraftID
			WHERE A.AircraftCD=@AircraftCD
			  AND PL.ElapseTM=@ElapseTM
			  AND PL.PowerSetting=@PowerSetting
			  AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))

END	
RETURN ROUND(@FuelBurnVal,0)	    
END

--select DBO.FuelBurn('JWILLIAMS_13','900',2.300,'1')

GO


