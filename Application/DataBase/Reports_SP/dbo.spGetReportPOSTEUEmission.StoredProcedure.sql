IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTEUEmission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTEUEmission]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE  [dbo].[spGetReportPOSTEUEmission]   
  (
	  @UserCD AS VARCHAR(30),  
	  @UserCustomerID AS BIGINT,
	  @DATEFROM AS DATETIME,  
	  @DATETO AS DATETIME,
	  @TailNum AS VARCHAR(100) = '',  
	  @AircraftCD AS NVARCHAR(1000) = ''
  )
  
AS
BEGIN  

---exec  [dbo].[spGetReportPOSTEUEmission]   'supervisor_99',10099,'2012-01-01','2012-12-31'
	
DECLARE @DepICAOID VARCHAR(10), @ArrICAOID VARCHAR(10)

Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company 
                                     WHERE CustomerID=@UserCustomerID 
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
	
DECLARE  @TempFleetID TABLE     
 (   
  ID INT NOT NULL IDENTITY (1,1), 
  FleetID BIGINT,
  TailNum VARCHAR(9),
  AircraftCD VARCHAR(20),
  AircraftDescription VARCHAR(100)
 )
  
  
  
INSERT INTO @TempFleetID  
SELECT DISTINCT 
       F.FleetID,
       F.TailNum,
       AC.AircraftCD,
       AC.AircraftDescription
  FROM Aircraft AC 
  JOIN Fleet F ON F.AircraftID = AC.AircraftID
  WHERE AC.CustomerID = @UserCustomerID
  AND F.IsDeleted = 0
  AND F.IsInActive = 0
  AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='')
  AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')   
  

		
SELECT * INTO #TEMP1 FROM	
(  
SELECT DISTINCT
      
      CONVERT(DATE,PL.ScheduledTM) AS ScheduledTM
      ,[Tail Number]=FE.TailNum--CASE WHEN TH.OldTailNUM = FE.TailNum THEN TH.NewTailNUM ELSE FE.TailNum END AS [Tail Number]
      ,AC.AircraftCD
      ,AD.IcaoID AS DEPART
      ,AA.IcaoID AS ARRIVE
      ,AD.CityName AS DEPARTCITY
      ,AA.CityName AS ARRIVECITY
      ,ISNULL(PL.PassengerTotal,0) AS PassengerTotal
      ,(ISNULL(PL.PassengerTotal,0)*100) AS PayloadKG
      ,(ISNULL(PL.PassengerTotal,0)/10) AS PAYLOADT
      ,GCD = CASE WHEN AD.IcaoID = AA.IcaoID THEN '0' ELSE  ROUND([dbo].[GCD](AD.IcaoID,AA.IcaoID),0) END
      ,GCD1 = CASE WHEN AD.IcaoID = AA.IcaoID THEN '0' ELSE  (ROUND([dbo].[GCD](AD.IcaoID,AA.IcaoID),0)+95) END
      ,(ISNULL(PL.FuelOn,0)*2204.62) AS FuelBurnlbs
      ,ISNULL(PL.FuelOn,0) AS FUELBURNT
      ,(ISNULL(PL.FuelOn,0)*3.15) AS CO2
      ,TONNE = (ISNULL(PL.PassengerTotal,0)/10) *(CASE WHEN AD.IcaoID = AA.IcaoID THEN '0' ELSE  (ROUND([dbo].[GCD](AD.IcaoID,AA.IcaoID),0)+95) END)
      ,PM.FleetID
      ,PM.LogNum
      ,PL.LegNUM
      ,PL.ScheduledTM DepartureDTTMLocal
FROM PostflightMain PM  
     JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
     JOIN Fleet FE  ON PM.FleetID = FE.FleetID
     JOIN @TempFleetID TF ON TF.FleetID=FE.FleetID 
   --  LEFT JOIN TailHistory TH ON TH.OldTailNUM = FE.TailNum AND TH.CustomerID = FE.CustomerID
    -- LEFT JOIN Company C ON PM.HomebaseID = C.HomebaseID
    -- LEFT JOIN Airport AP ON C.HomebaseAirportID = AP.AirportID 
     LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID        
     LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
     LEFT JOIN Aircraft AC ON AC.AircraftID = FE.AircraftID 
 WHERE  CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND (AD.IsEUETS = 1 OR AA.IsEUETS =1) 
	AND PM.CustomerID = @UserCustomerID
	AND PM.IsDeleted = 0
	)T1
	
--	SELECT * FROM #TEMP1

IF @SuppressActivityAircft=0
BEGIN
	
INSERT INTO #TEMP1(
      ScheduledTM
      ,[Tail Number]
      ,AircraftCD
      ,DEPART
      ,ARRIVE
      ,DEPARTCITY
      ,ARRIVECITY
      ,PassengerTotal
      ,PayloadKG
      ,PAYLOADT
      ,GCD
      ,GCD1
      ,FuelBurnlbs
      ,FUELBURNT
      ,CO2
      ,TONNE
      )
SELECT DISTINCT 
      NULL AS ScheduledTM
      ,T.TailNum 
      ,T.AircraftCD
      ,'' AS DEPART
      ,'' AS ARRIVE
      ,'' AS DEPARTCITY
      ,'' AS ARRIVECITY
      ,0 AS PassengerTotal
      ,0 AS PayloadKG
      ,0 AS PAYLOADT
      ,0 AS GCD
      ,0 AS GCD1
      ,0 AS  FuelBurnlbs
      ,0 AS FUELBURNT
      ,0 AS CO2
      ,0 AS TONNE
       FROM Aircraft AD CROSS JOIN @TempFleetID T
       JOIN Fleet F ON AD.AircraftID = F.AircraftID
       JOIN @TempFleetID TF ON TF.FleetID=F.FleetID 
        
       WHERE NOT EXISTS (SELECT  T1.FleetID FROM  #TEMP1 T1 WHERE T1.FleetID=T.FleetID )
                           AND AD.CustomerID=@UserCustomerID
  
END                         
  SELECT DISTINCT
	   ScheduledTM
      ,[Tail Number]
      ,AircraftCD
      ,DEPART
      ,ARRIVE
      ,DEPARTCITY
      ,ARRIVECITY
      ,PassengerTotal
      ,PayloadKG
      ,PAYLOADT
      ,GCD
      ,GCD1
      ,FuelBurnlbs
      ,FUELBURNT
      ,CO2
      ,TONNE
      ,DepartureDTTMLocal
      ,LogNum
      ,LegNUM
	FROM #TEMP1
	ORDER BY DepartureDTTMLocal,LogNum,LegNUM
	
IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL
DROP TABLE #TEMP1 		
	
	
 END    
 



GO


