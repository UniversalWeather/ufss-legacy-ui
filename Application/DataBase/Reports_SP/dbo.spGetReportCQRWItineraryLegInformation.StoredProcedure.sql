IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWItineraryLegInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWItineraryLegInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQRWItineraryLegInformation](
	--@UserCustomerID     AS VARCHAR(30),  
	--@UserHomebaseID    AS VARCHAR(30),  
	@CQLegID				AS VARCHAR(30) 
) 
AS
BEGIN
	SELECT 
	--airport.airport_nm, airport.city, airport.state (icao)
	Dep = DA.AirportName + ', ' + DA.CityName + ', ' + DA.StateName 
	,DepICAO = DA.IcaoID
	, ArrICAO =   AA.IcaoID
	, Arr = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
	, TimePlusMinus =  Cast(DateDiff(MINUTE,QL.ArrivalGreenwichDTTM,QL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2)) - (Cast(DateDiff(MINUTE,QL.DepartureGreenwichDTTM,QL.DepartureDTTMLocal)*1.0/60 as numeric(18,2)))	
	, QL.DepartureDTTMLocal
	, QL.ArrivalDTTMLocal
	, QL.Distance
	, QL.ElapseTM
	, QL.PassengerTotal
	, QL.Notes
	, QL.LegNUM
	FROM CQFile QF 
	INNER JOIN CQMain QM ON QF.CQFileID = QM.CQFileID AND QM.IsDeleted=0
	INNER JOIN CQLeg QL ON QM.CQMainID = QL.CQMainID AND QL.IsDeleted=0
	LEFT OUTER JOIN Airport DA ON QL.DAirportID = DA.AirportID
	LEFT OUTER JOIN Airport AA ON QL.AAirportID = AA.AirportID
	WHERE --QF.CustomerID = CONVERT(BIGINT,@UserCustomerID) AND 
	QL.CQLegID =CONVERT(bigint, @CQLegID)
END    
--  exec  spGetReportCQRWItineraryLegInformation ''
 
 






GO


