IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByPassengerTrip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTrip]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTrip]         
( 
@UserCD AS VARCHAR(30), --Mandatory
@UserCustomerID AS VARCHAR(30),           
@DATEFROM AS DATE , --Mandatory          
@DATETO AS DATE , --Mandatory           
@LogNum NVARCHAR(1000) = '',
@PassengerRequestorCD  AS VARCHAR(1000) = '',
@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@FleetGroupCD AS NVARCHAR(4000) = '',
@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase   
@UserHomebaseID AS VARCHAR(30)  ,
@PassengerGroupCD VARCHAR(500)='' 
)   
AS
--================================================================================              
-- SPC Name: [spGetReportPOSTBillingByPassengerTrip]          
-- Author: HARIHARAN S          
-- Create date: 02/18/2013          
-- Description: Get Trip Detail by billing for REPORTS          
-- Revision History          
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
--================================================================================              
BEGIN            
SET NOCOUNT ON  


-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration---------------------- 

 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------    
DECLARE @TenToMin SMALLINT = 0;  
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @TenToMin = TimeDisplayTenMin,
@AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 


SELECT DISTINCT
[LogNumber] = POM.LogNum
,[Leg] = POL.LegNUM
,[Tripdate] = CONVERT(DATE,POL.OutboundDTTM)
,[TailNumber] = FL.TailNum
,[Rqstr] = PP.PassengerRequestorCD
,[DepICAO] = AD.IcaoID
,[ArrICAO] = AA.IcaoID
,[Miles] = POL.Distance
,[FlightHours] = ROUND(POL.FlightHours,1)
,[BlockHours] = ROUND(POL.BlockHours,1)
,[NbrPax] = POL.PassengerTotal
,[EmptySeats] = ISNULL(FL.MaximumPassenger,0) - ISNULL(POL.PassengerTotal,0)
,[DeptCD] = DE.DepartmentCD
,[AuthCD] = DA.AuthorizationCD
,[ChargeRate] = CR.ChargeRate
,[Per] = CR.ChargeUnit
,[TotalChargesLEG] = --(CASE WHEN CONVERT(DATE,POM.EstDepartureDT) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						--THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							--)ELSE
						--NULL
						--END) 
						+ (ISNULL(POE.ExpenseAmt,0))

,[Code] = CONVERT(VARCHAR(MAX),'')
,[PassengerName] = CONVERT(VARCHAR(MAX),'')
,[PassengerBilling] = CONVERT(VARCHAR(MAX),'')
,[Dept] = CONVERT(VARCHAR(MAX),'')
,[Auth] = CONVERT(VARCHAR(MAX),'')
,[PAXTOTAL] = POL.PassengerTotal
,[TotalChargesPax] = (REPLACE(Leg, LegNUM,
						(SELECT TOP 1 ISNULL(c.CurrencySymbol,'$') AS CurrencySymbol
FROM UserMaster u INNER JOIN Company c ON u.CustomerID = c.CustomerID AND u.HomeBaseID = c.HomeBaseID WHERE RTRIM(u.UserName) =RTRIM(@UserCD)) + CONVERT(VARCHAR(20),CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
							FLOOR((
							((CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							'0.00'
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal)*100)*0.01
						END)
						))
,[Account] = EXC.ACCOUNT
,[ExpenseAmount] = EXC.EXPENSEAMOUNT
--,[BillingCode] = CASE WHEN POL.LegNUM=1 THEN Bill.StandardBilling ELSE NULL END
--,[TotalChargesBILLING] = FLOOR(ISNULL(Bill.StdVal,0)*100)*0.01

,[TenToMin] = @TenToMin
,pol.POLegID
INTO #PaxTemp1
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POL.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
--LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = POL.AuthorizationID
LEFT OUTER JOIN FleetChargeRate FCR ON FL.FleetID = FCR.FleetID AND FL.CustomerID = FCR.CustomerID AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,POL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,POL.ScheduledTM) AND FCR.IsDeleted=0
--LEFT OUTER JOIN FleetChargeRate FCR ON FL.FleetID = FCR.FleetID AND FL.CustomerID = FCR.CustomerID AND POM.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT
INNER JOIN @TempFleetID T ON T.FleetID = FL.FleetID
LEFT OUTER JOIN ( SELECT DISTINCT PP2.ExpenseAMT,PP2.POLegID,PP2.AccountID,
					  ACCOUNT = (
                      SELECT ISNULL(P1.AccountDescription,' ')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH('')),
                      EXPENSEAMOUNT = (
                      SELECT ISNULL(CONVERT(VARCHAR,PP1.ExpenseAMT),' ')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH(''))
                      FROM PostflightExpense PP2
                     
				 ) EXC ON POL.POLegID = EXC.POLegID 
 
LEFT OUTER JOIN Account A ON POE.AccountID = A.AccountID AND POE.CustomerID = A.CustomerID
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
INNER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND FC.IsDeleted=0
			WHERE CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON POL.POLegID = CR.POLegID   
       
LEFT OUTER JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,Leg = (

                      SELECT ISNULL(CONVERT(VARCHAR(2),PL.LegNUM),'')+'$$'
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID
												   INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
                                                   INNER JOIN PostflightLeg PL ON PP1.POLegID=PL.POLegID AND PL.IsDeleted = 0
                               WHERE PP1.POLegID = PP2.POLegID
                              -- AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
                               
                               FOR XML PATH(''))

					  FROM PostflightPassenger PP2
					  
			    ) PAX ON POL.POLegID = PAX.POLegID  
INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID

WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
   -- AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
  -- AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
   -- AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
    --AND (FL.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0

SELECT TEMP1.* INTO #PaxTemp FROM #PaxTemp1 TEMP1 
WHERE (TEMP1.DEPTCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))  OR @DepartmentCD = '')
AND (TEMP1.AuthCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')	

IF OBJECT_ID('tempdb..#PaxTemp1') IS NOT NULL
DROP TABLE  #PaxTemp1	

SELECT DISTINCT [LogNumber]
,[Leg]
,[Tripdate]
,[TailNumber]
,[Rqstr] 
,[DepICAO] 
,[ArrICAO]
,[Miles] 
,[FlightHours]
,[BlockHours] 
,[NbrPax] 
,[EmptySeats] 
,[DeptCD]
,[AuthCD]
,[ChargeRate] 
,[Per] 
,[TotalChargesLEG]
,[Code]=PAX.CODE
,[PassengerName]=PAX.PASSENGERNAME 
,[PassengerBilling]=PAX.PASSENGERBILLING 
,[Dept]=PAX.DEPT
,[Auth]=PAX.AUTH
,[PAXTOTAL]
,[TotalChargesPax] 
,[Account] 
,[ExpenseAmount]
,[TenToMin]
  FROM #PaxTemp PT
		LEFT OUTER JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,
					  PASSENGERNAME = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$' 
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                                                   INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                              --- AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
                               ORDER BY P1.PassengerRequestorCD
                               FOR XML PATH('')),
                      CODE = (
                      SELECT ISNULL(P1.PassengerRequestorCD,'')+'$$'
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
													INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                              --- AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
                               ORDER BY P1.PassengerRequestorCD
                               FOR XML PATH('')),
                               
                  	  PASSENGERBILLING = (
						  SELECT ISNULL(PP1.Billing,'')+'$$'
						  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
													   INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
								   WHERE PP1.POLegID = PP2.POLegID
								 ---  AND (P1.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
								   ORDER BY P1.PassengerRequestorCD
								   FOR XML PATH('')),
				      DEPT = (
		                  SELECT ISNULL(D.DepartmentCD,'')+'$$'
		                  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
		                                               INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
													   LEFT OUTER JOIN Department D ON D.DepartmentID = P1.DepartmentID
				                   WHERE PP1.POLegID = PP2.POLegID
				                   ORDER BY P1.PassengerRequestorCD
				                   FOR XML PATH('')),
					  AUTH = (
		                  SELECT ISNULL(A.AuthorizationCD,'')+'$$'
		                  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
													   INNER JOIN @TempPassengerID TP ON TP.PassengerID=P1.PassengerRequestorID 
													   LEFT OUTER JOIN DepartmentAuthorization A ON A.AuthorizationID = P1.AuthorizationID
				                   WHERE PP1.POLegID = PP2.POLegID
				                   ORDER BY P1.PassengerRequestorCD
				                   FOR XML PATH(''))
					  FROM PostflightPassenger PP2
					  
			    ) PAX ON PT.POLegID = PAX.POLegID  


IF OBJECT_ID('tempdb..#PaxTemp') IS NOT NULL
DROP TABLE  #PaxTemp

END
--EXEC spGetReportPOSTBillingByPassengerTrip 'supervisor_99','10099','01-01-2009','03-03-2013','2','','','','','',0 ,''         






GO


