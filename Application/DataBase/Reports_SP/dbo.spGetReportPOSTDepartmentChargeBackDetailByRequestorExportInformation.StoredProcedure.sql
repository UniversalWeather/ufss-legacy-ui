IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestorExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestorExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentChargeBackDetailByRequestorExportInformation]											
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @Year AS NVARCHAR(4),
    @UserHomebaseID AS VARCHAR(30),
    @RequestorCD AS VARCHAR(100) = '',
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @DepartmentCD AS VARCHAR(70)= '',
    @IsHomebase AS BIT = 0

 											
AS 

--EXEC spGetReportPOSTDepartmentChargeBackDetailByRequestorExportInformation 'SUPERVISOR_99',10099,'2013','','','','',0

BEGIN  
SET NOCOUNT ON;  
DECLARE  @TempFleetID TABLE       
 (     
  ID INT NOT NULL IDENTITY (1,1),   
  FleetID BIGINT,  
  HomeBaseID BIGINT,  
  FleetGroupCD VARCHAR(6),  
  TailNum VARCHAR(9)  
  )  
    
  
  
BEGIN    
INSERT INTO @TempFleetID  
 SELECT DISTINCT  F.FleetID, F.HomebaseID,FG.FleetGroupCD,F.TailNum  
 FROM Fleet F   
 LEFT OUTER JOIN FleetGroupOrder FGO  
 ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
 LEFT OUTER JOIN FleetGroup FG   
 ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
 WHERE (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD='')  
 AND F.CustomerID = @UserCustomerID    
 AND F.IsDeleted=0  
END  
  
  
  
  
Declare @SuppressActivityDept BIT    
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID   
           AND IsZeroSuppressActivityAircftRpt IS NOT NULL  
           AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
  
--DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
DECLARE @TenToMin SMALLINT = 0;  
  
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
    AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
DECLARE @AircraftBasis numeric(1,0);  
   SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
    AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)      
    
  
DECLARE @MonthStart INT,@MonthEnd INT;  
SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID  
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID  
DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))  
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0))))   
  
-----Retrieving Department details based on @FromDate and @ToDate  
DECLARE @DateTable TABLE (SeqID INT IDENTITY, WorkDate DATE,DepartmentCD VARCHAR(10),DepartmentName VARCHAR(60),DepartmentID  BIGINT);  
-----Retrieving Fleet details based on @FromDate and @ToDate  
DECLARE @FleetDate TABLE  
  (FLEETID BIGINT,  
   FleetGroupCD CHAR(4),  
   TailNum VARCHAR(9),  
   HomebaseID BIGINT,  
   WorkDate DATE,  
   ScheduledMonth SMALLINT,  
   ScheduledYear SMALLINT);  
INSERT INTO @DateTable SELECT DISTINCT  DATE,D.DepartmentCD,d.DepartmentName,D.DepartmentID FROM Department D CROSS JOIN  [dbo].[fnDateTable](@FromDate,@ToDate)  
               WHERE CustomerID=@UserCustomerID  
          AND IsDeleted=0  
          AND IsInActive=0  
         AND  (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
  
  
  
  INSERT INTO @FleetDate  
   (FLEETID,  
    FleetGroupCD,  
    TailNum,  
    HomebaseID,  
    ScheduledMonth,  
    ScheduledYear  
    )  
  SELECT DISTINCT  
    vF.FleetID,  
    vF.FleetGroupCD,  
    vF.TailNum,  
    vF.HomebaseID,  
    MONTH(DT.WorkDate),  
    YEAR(DT.WorkDate)  
  FROM @DateTable DT  
  CROSS JOIN @TempFleetID vF  
  WHERE (vF.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)   
  ORDER BY YEAR(DT.WorkDate)   
    
  
  
----Retrieving leg details based on Tail Number, Department and Account Number  
  
  
SELECT FC.* INTO #FleetCalender FROM  
  (SELECT DISTINCT  
   FG.FleetID,  
   ISNULL(D.DepartmentCD,'ZZZZ') DepartmentCD,  
   D.DepartmentCD + '-' + ISNULL(P.PassengerRequestorCD,'NONE') AS DepartmentCD1,  
   DepartmentName = ISNULL(D.DepartmentName,'ZZZZ'),  
   P.PassengerRequestorCD,  
   P.PassengerName,  
   FG.AircraftCD AS ac_code,  
   A.AccountNum,  
   PL.Distance,  
   CR.ChargeUnit,  
   CR.ChargeRate,  
   DATEPART(MM,PL.ScheduledTM) AS SCHEDULEDMONTH,  
   DATEPART(yyyy,PL.ScheduledTM) AS ScheduledYear,  
   FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blkhrs')   
                                                WHEN 2 THEN CONVERT(VARCHAR(30),'flthrs') END,  
    
 CASE WHEN @AircraftBasis = 1 THEN ROUND(PL.BlockHours,1) WHEN @AircraftBasis = 2 THEN ROUND(PL.FlightHours,1) END AS FlightBlockHours ,  
         CASE WHEN CR.ChargeUnit = 'N' THEN  ISNULL((PL.Distance*CR.ChargeRate),0)  
     WHEN CR.ChargeUnit = 'S' THEN ISNULL(((PL.Distance*CR.ChargeRate)*1.15078),0)  
     WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ISNULL((ROUND(PL.BlockHours,1) *CR.ChargeRate),0)  
    WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ISNULL((ROUND(PL.FlightHours,1) *CR.ChargeRate),0)  
    END AS ChargeBack ,  
 D.IsInActive,  
 [TenToMin] = @TenToMin,  
 MonthStart = @MonthStart,  
 MonthEnd = @MonthEnd,  
    DepartmentID=ISNULL(D.DepartmentID,99),  
 PM.LogNum,  
 PL.LegNUM,  
 PassengerRequestorID=ISNULL(P.PassengerRequestorID,999)  
   
FROM PostflightMain PM  
JOIN PostflightLeg pl  
ON PM.POLogID = PL.POLogID AND PM.CustomerID = PL.CustomerID AND PL.IsDeleted = 0  
JOIN Fleet FG  
ON FG.FleetID = PM.FleetID  
JOIN @TempFleetID TF ON TF.FleetID=FG.FleetID  
INNER JOIN Department D  
ON PL.DepartmentID = D.DepartmentID  
LEFT OUTER JOIN Passenger P  
ON PL.PassengerRequestorID = P.PassengerRequestorID  
JOIN Company C  
ON FG.HomebaseID = C.HomebaseID  
LEFT OUTER JOIN Account A  
ON A.AccountID = P.AccountID AND A.CustomerID = P.CustomerID  
 LEFT OUTER JOIN (  
   SELECT POLegID, ChargeUnit, ChargeRate  
   FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID  
                          INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID   
   AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)  
           ) CR ON PL.POLegID = CR.POLegID    
WHERE (CONVERT(DATE,PL.ScheduledTM) BETWEEN  CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND  
(P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ','))OR @RequestorCD='')  
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)  
AND PM.CustomerID = @UserCustomerID    
) FC  
  
  
IF @SuppressActivityDept=0  
BEGIN  
  
INSERT INTO #FleetCalender(DepartmentID,DepartmentCD,DepartmentName,SCHEDULEDMONTH,ScheduledYear,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,PassengerName,PassengerRequestorCD,DepartmentCD1,FleetID,PassengerRequestorID)  
----Inserting month for each Fleet  
SELECT DISTINCT ISNULL(T.DepartmentID,99),T.DepartmentCD,T.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),@TenToMin,@MonthStart, @MonthEnd,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'blkhrs' WHEN 2 THEN 'flthrs' END),  
                TF.PassengerName,TF.PassengerRequestorCD,T.DepartmentCD + '-' + ISNULL(TF.PassengerRequestorCD,'NONE') AS DepartmentCD1 ,ISNULL(TF.FleetID,0),ISNULL(TF.PassengerRequestorID,999)  
               FROM @DateTable T   
                    LEFT OUTER JOIN #FleetCalender TF ON TF.DepartmentID=T.DepartmentID  
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentID=T.DepartmentID AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH)  
                                     ORDER BY YEAR(T.WorkDate)  
  
END  
  
ELSE  
  
BEGIN  
  
INSERT INTO #FleetCalender(DepartmentID,DepartmentCD,DepartmentName,SCHEDULEDMONTH,ScheduledYear,TenToMin,MonthStart,MonthEnd,FlightBlockHoursLabel,PassengerName,PassengerRequestorCD,DepartmentCD1,FleetID,PassengerRequestorID)  
----Inserting month for each Fleet  
SELECT DISTINCT ISNULL(T.DepartmentID,99),T.DepartmentCD,T.DepartmentName ,MONTH(T.WorkDate),YEAR(T.WorkDate),@TenToMin,@MonthStart, @MonthEnd,  FlightBlockHoursLabel = (CASE @AircraftBasis WHEN 1 THEN 'blkhrs' WHEN 2 THEN 'flthrs' END),  
                TF.PassengerName,TF.PassengerRequestorCD,T.DepartmentCD + '-' + ISNULL(TF.PassengerRequestorCD,'NONE') AS DepartmentCD1 ,ISNULL(TF.FleetID,0),ISNULL(TF.PassengerRequestorID,999)  
               FROM @DateTable T  
                    INNER JOIN #FleetCalender TF ON TF.DepartmentID=T.DepartmentID  
                  WHERE NOT EXISTS (SELECT  T1.DepartmentCD FROM  #FleetCalender T1 WHERE T1.DepartmentID=T.DepartmentID AND MONTH(T.WorkDate)=T1.SCHEDULEDMONTH)  
                                     ORDER BY YEAR(T.WorkDate)  
  
  
END  
  
DECLARE @Month TABLE (DeptID BIGINT,ReqID BIGINT,DepartmentCD VARCHAR(10),PassengerCD VARCHAR(5),PassName VARCHAR(63),MonthNo INT,Year INT,DepartmentName VARCHAR(25))  
INSERT INTO @Month  
SELECT DepartmentID,PassengerRequestorID,DepartmentCD,PassengerRequestorCD,PassengerName,MonthNo,Year,DepartmentName FROM #FleetCalender FC  
                                       CROSS JOIN (SELECT DISTINCT MONTH(WorkDate)MonthNo,YEAR(WorkDate) Year FROM @DateTable)TT  
  
  DECLARE @Count INT  
    SELECT @Count=COUNT(*) FROM   
   ( SELECT  DISTINCT DepartmentCD,PassengerRequestorCD CountVal   
     FROM #FleetCalender  
     WHERE  (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD=''))TEMP   
                             

IF @SuppressActivityDept=0  
BEGIN  

SELECT DISTINCT FleetID,  
   DepartmentCD,  
   DepartmentCD1,  
   DepartmentName,  
   PassengerRequestorCD,  
   PassengerName,  
   ac_code,  
   AccountNum,  
   Distance,  
   ChargeUnit,  
   ChargeRate = Floor(ISNULL(ChargeRate,0)*100)*0.01,  
   SCHEDULEDMONTH,  
   ScheduledYear,  
   CONVERT(INT,SCHEDULEDMONTH) AS ORDEREXP,  
   FlightBlockHoursLabel ,  
   FlightBlockHours = Floor(ISNULL(FlightBlockHours,0)*10)*0.1,  
   ChargeBack = FLOOR(ROUND(ISNULL(ChargeBack,0),0)*10)*0.1,  
    IsInActive,  
    TenToMin,  
    MonthStart,  
    MonthEnd,  
    @Count NoOfRecords,  
    LogNum,  
    LegNum,  
    CASE WHEN LegNum = 1 THEN COUNT(DISTINCT FlightBlockHours) ELSE 0 END  act  ,
    '' totcol
 FROM #FleetCalender   
 WHERE (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
 GROUP BY FleetID,DepartmentCD,DepartmentCD1,DepartmentName,PassengerRequestorCD,PassengerName,ac_code,AccountNum,Distance,ChargeUnit,ChargeRate,SCHEDULEDMONTH,ScheduledYear,  
SCHEDULEDMONTH,FlightBlockHoursLabel,FlightBlockHours,ChargeBack,IsInActive,TenToMin,MonthStart,MonthEnd,LogNum,LegNum  
   
 UNION ALL  
   
 SELECT DISTINCT FleetID=0,  
   DepartmentCD,  
   DepartmentCD=DepartmentCD + '-' + ISNULL(PassengerCD,'NONE') ,  
   DepartmentName,  
   PassengerCD,  
   PassName,  
   ac_code=NULL,  
   AccountNum=NULL,  
   Distance=NULL,  
   ChargeUnit=NULL,  
   ChargeRate = 0,  
   MonthNo,  
   YEAR,  
   CONVERT(INT,MonthNo) AS ORDEREXP,  
   FlightBlockHoursLabel= CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blkhrs')   
                                                WHEN 2 THEN CONVERT(VARCHAR(30),'flthrs') END ,  
   FlightBlockHours=0,  
   ChargeBack=0,  
   IsInActive=NULL,  
   @TenToMin,  
   @MonthStart,  
   @MonthEnd,  
   @Count NoOfRecords,  
   LogNum=0,  
   LegNum=0,  
   0  act  ,
    '' totcol
 FROM @Month M  
 WHERE (M.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
   AND NOT EXISTS (SELECT DepartmentID,PassengerRequestorID FROM #FleetCalender FC WHERE FC.DepartmentID=M.DeptID AND FC.PassengerRequestorID=M.ReqID AND FC.SCHEDULEDMONTH=M.MonthNo AND FC.ScheduledYear=M.Year)  
  
   
 ORDER BY DepartmentCD,SCHEDULEDMONTH,ScheduledYear   
END  
ELSE  
  
BEGIN  
SELECT DISTINCT FleetID,  
   DepartmentCD,  
   DepartmentCD1,  
   DepartmentName,  
   PassengerRequestorCD,  
   PassengerName,  
   ac_code,  
   AccountNum,    
   Distance,  
   ChargeUnit,  
   ChargeRate = Floor(ISNULL(ChargeRate,0)*100)*0.01,  
   SCHEDULEDMONTH,  
   ScheduledYear,  
   CONVERT(INT,SCHEDULEDMONTH) AS ORDEREXP,  
   FlightBlockHoursLabel ,  
   FlightBlockHours = Floor(ISNULL(FlightBlockHours,0)*10)*0.1,  
   ChargeBack = FLOOR(ROUND(ISNULL(ChargeBack,0),0)*10)*0.1,  
    IsInActive,  
    TenToMin,  
    MonthStart,  
    MonthEnd,  
    @Count NoOfRecords,  
    LogNum,  
    LegNum,  
    CASE WHEN LegNum = 1 THEN COUNT(DISTINCT FlightBlockHours) ELSE 0 END  act,  
    0 AS activecol ,
    '' totcol
 FROM #FleetCalender   
 WHERE (DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
 AND (DepartmentCD IN (SELECT DepartmentCD FROM #FleetCalender WHERE AccountNum IS NOT NULL OR Distance IS NOT NULL OR ChargeUnit IS NOT NULL OR FlightBlockHours IS NOT NULL OR ChargeBack IS NOT NULL))  
 GROUP BY   
 FleetID,DepartmentCD,DepartmentCD1,DepartmentName,PassengerRequestorCD,PassengerName,ac_code,AccountNum,Distance,ChargeUnit,ChargeRate,SCHEDULEDMONTH,ScheduledYear,  
SCHEDULEDMONTH,FlightBlockHoursLabel,FlightBlockHours,ChargeBack,IsInActive,TenToMin,MonthStart,MonthEnd,LogNum,LegNum  
   
UNION ALL  
   
 SELECT DISTINCT FleetID=0,  
   DepartmentCD,  
   DepartmentCD=DepartmentCD + '-' + ISNULL(PassengerCD,'NONE') ,  
   DepartmentName,  
   PassengerCD,  
   PassName,  
   ac_code=NULL,  
   AccountNum=NULL,  
   Distance=NULL,  
   ChargeUnit=NULL,  
   ChargeRate = 0,  
   MonthNo,  
   YEAR,  
   CONVERT(INT,MonthNo) AS ORDEREXP,  
   FlightBlockHoursLabel= CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blkhrs')   
                                                WHEN 2 THEN CONVERT(VARCHAR(30),'flthrs') END ,  
   FlightBlockHours=0,  
   ChargeBack=0,  
   IsInActive=NULL,  
   @TenToMin,  
   @MonthStart,  
   @MonthEnd,  
   @Count NoOfRecords,  
   LogNum=0,  
   LegNum=0,  
   0  act  ,
   0 AS activecol, 
    '' totcol 
 FROM @Month M  
 WHERE (M.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ','))OR @DepartmentCD='')  
   AND NOT EXISTS (SELECT DepartmentID,PassengerRequestorID FROM #FleetCalender FC WHERE FC.DepartmentID=M.DeptID AND FC.PassengerRequestorID=M.ReqID AND FC.SCHEDULEDMONTH=M.MonthNo AND FC.ScheduledYear=M.Year)  
ORDER BY DepartmentCD,SCHEDULEDMONTH,ScheduledYear    
END  
  
IF OBJECT_ID('tempdb..#FleetCalender') IS NOT NULL  
DROP TABLE #FleetCalender   
  
IF OBJECT_ID('tempdb..#DepartmentChargeBack') IS NOT NULL  
DROP TABLE #DepartmentChargeBack    
  
END
GO


