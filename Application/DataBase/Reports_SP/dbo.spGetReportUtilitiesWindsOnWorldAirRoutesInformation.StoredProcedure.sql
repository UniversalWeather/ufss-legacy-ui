IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportUtilitiesWindsOnWorldAirRoutesInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportUtilitiesWindsOnWorldAirRoutesInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportUtilitiesWindsOnWorldAirRoutesInformation]
(@UserCD VARCHAR(30)
,@UserCustomerID BIGINT
,@Departure VARCHAR(4)=''
,@Arrival VARCHAR(4)='')

AS 
-- ==============================================================================
-- SPC Name: spGetReportUtilitiesWindsOnWorldAirRoutesInformation
-- Author: Askar
-- Create date: 
-- Description: Get Utilitiest Winds on World Air Routes for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

BEGIN
---- spGetReportUtilitiesWindsOnWorldAirRoutesInformation 'SUPERVISOR_99',10099,'KDAL','KHOU'

DECLARE @DIcaoID VARCHAR(4),@DCityName VARCHAR(25),@DZone NUMERIC(5,0);
DECLARE @AIcaoID VARCHAR(4),@ACityName VARCHAR(25),@AZone NUMERIC(5,0)

	DECLARE @DepZone BIGINT
	DECLARE @ArrZone BIGINT
	DECLARE @TempZone BIGINT
	
	----------------Airport Information-----------------
SELECT @DIcaoID=D.IcaoID,@DCityName=D.CityName,@DZone=D.WindZone FROM Airport D WHERE D.IcaoID=@Departure AND CustomerID=@UserCustomerID
              


SELECT @AIcaoID=A.IcaoID,@ACityName=A.CityName,@AZone=A.WindZone FROM Airport A WHERE A.IcaoID=@Arrival AND CustomerID=@UserCustomerID
	
	SELECT @DepZone = WindZone FROM Airport WHERE CustomerID = @UserCustomerID AND IcaoID=@Departure
	SELECT @ArrZone = WindZone FROM Airport WHERE CustomerID = @UserCustomerID AND IcaoID=@Arrival
	
	DECLARE @IsExist INT
	SET @IsExist = 0
	
	DECLARE @tmpWorldWind TABLE 
	(
		DepartureZoneAirport bigint,
		ArrivalZoneAirport bigint,
		DepartureCity varchar(25),
		ArrivalCity varchar(25),
		WorldWindQuarter char(1),
		Altitude char(1),
		Direction int,
		ReturnDirection int,
		WorldWindStandard int,
		Rnk INT
	)


--SELECT @DepZone
--SELECT @ArrZone



IF @Departure<>'' AND @Arrival<>''
	BEGIN
	 
	 IF EXISTS(SELECT WorldWindID FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @DepZone AND ArrivalZoneAirport = @ArrZone)
			
		BEGIN   


			
			INSERT INTO @tmpWorldWind
				(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
			SELECT DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter) 
			FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @DepZone AND ArrivalZoneAirport = @ArrZone
			ORDER BY WorldWindQuarter ASC	
		END
   ELSE BEGIN


   	       INSERT INTO @tmpWorldWind
				(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
			SELECT DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
				,Altitude,Direction,ReturnDirection,WorldWindStandard,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter) 
			FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @ArrZone AND ArrivalZoneAirport = @DepZone
			ORDER BY WorldWindQuarter ASC
	      
	    END
			
	
END
ELSE IF @Departure <>''AND @Arrival=''
	BEGIN

	INSERT INTO @tmpWorldWind
		(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
		,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
	SELECT DEP.*,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter)  FROM 
	(
	
	SELECT DISTINCT @DepZone DepartureZoneAirport,NULL ArrivalZoneAirport,@DCityName Dity,NULL ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @DepZone
	
	UNION ALL
	
	SELECT DISTINCT @DepZone DepartureZoneAirport,NULL ArrivalZoneAirport,@DCityName Dity,NULL ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @DepZone
	
	UNION ALL
	
	SELECT DISTINCT @DepZone DepartureZoneAirport,NULL ArrivalZoneAirport,@DCityName Dity,NULL ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND DepartureZoneAirport = @DepZone
	
	)DEP
	ORDER BY WorldWindQuarter ASC
	
	END		
ELSE IF @Departure =''AND @Arrival<>''
	BEGIN
	
	INSERT INTO @tmpWorldWind
		(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
		,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
	
	SELECT Arr.*,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter)  FROM 
	(
	
	SELECT DISTINCT  NULL DepartureZoneAirport,@ArrZone ArrivalZoneAirport,NULL DepCity,@ACityName ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND ArrivalZoneAirport = @ArrZone
	
	UNION ALL
	
	SELECT DISTINCT  NULL DepartureZoneAirport,@ArrZone ArrivalZoneAirport,NULL DepCity,@ACityName ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND ArrivalZoneAirport = @ArrZone
	
	UNION ALL
	
	SELECT DISTINCT  NULL DepartureZoneAirport,@ArrZone ArrivalZoneAirport,NULL DepCity,@ACityName ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard
	FROM WorldWind WHERE CustomerID = @UserCustomerID AND ArrivalZoneAirport = @ArrZone
	
	)Arr
	ORDER BY WorldWindQuarter ASC
	
	END	

ELSE

  BEGIN
  
  INSERT INTO @tmpWorldWind(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
		                     ,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
  SELECT 0 DepartureZoneAirport,0 ArrivalZoneAirport,'' DepCity,'' ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter) FROM (
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  UNION ALL
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  UNION ALL
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  )TEMP
  
  END
  
  
 IF NOT EXISTS(SELECT * FROM @tmpWorldWind) 
 BEGIN
  INSERT INTO @tmpWorldWind(DepartureZoneAirport,ArrivalZoneAirport,DepartureCity,ArrivalCity,WorldWindQuarter
		                     ,Altitude,Direction,ReturnDirection,WorldWindStandard,Rnk)
  SELECT 0 DepartureZoneAirport,0 ArrivalZoneAirport,'' DepCity,'' ArrivalCity,WorldWindQuarter
		,0 Altitude,0 Direction,0 ReturnDirection,0 WorldWindStandard,ROW_NUMBER()OVER(PARTITION BY WorldWindQuarter ORDER BY WorldWindQuarter) FROM (
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  UNION ALL
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  UNION ALL
  SELECT DISTINCT WorldWindQuarter FROM WorldWind WHERE CustomerID = @UserCustomerID
  )TEMP
END


SELECT  CASE WHEN Rnk=1 THEN  '20,000' 
	             WHEN Rnk=2 THEN  '30,000' ELSE '40,000' END FlightLevel
	         ,@DIcaoID  Dep
	         ,@AIcaoID Arr
	         ,@DCityName DCity
	         ,@ACityName ArrCity
	         ,@DepZone DepZone
	         ,@ArrZone Arrzone
	         ,DepartureZoneAirport
	         ,ArrivalZoneAirport
	         ,DepartureCity DepartureCity
	         ,ArrivalCity ArrivalCity
	         ,WorldWindQuarter
			 ,Altitude
			 ,Direction
			 ,ReturnDirection
			 ,WorldWindStandard FROM @tmpWorldWind order by WorldWindQuarter asc,Altitude desc,FlightLevel desc
     
			
			


END

GO


