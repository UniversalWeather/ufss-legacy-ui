IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostPassengerFlightHistoryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostPassengerFlightHistoryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================
-- SPC Name: spGetReportPostPassengerFlightHistoryInformation
-- Author:Badrinath/mullai
-- Create date: 06 August 2012
-- Description: Get Passenger Flight History Information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- 20-04-2015		Rajesh			2.4			Added sorting on column fltDate as requested in ticket FSS-60
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPostPassengerFlightHistoryInformation]
   @UserCD AS VARCHAR(30)  
  ,@DATEFROM as Datetime  
  ,@DATETO as Datetime  
  ,@IcaoID as nvarCHAR(1000)='' 
  ,@PassengerRequestorCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values  
  ,@PassengerGroupCD as nVARCHAR(1000)=''  
  ,@TailNum as nvarchar(1000)=''  
  ,@FleetGroupCD as nvarchar(1000)=''  
  ,@LogNum as nvarchar(1000)=''
  ,@RemovePaxDetail as bit=0 
  ,@SortBy as varchar(1)
  --,@SortbyPassengerCode as bit=0 
  --,@SortbyPassengerName as bit=0
 
 AS  
SET NOCOUNT ON  

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PASSENGERREQUESTORID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration---------------------
--DROP TABLE #PAXTEMP
--DROP TABLE #PAXTEMP1

DECLARE @IsZeroSuppressActivityPassengerRpt BIT;
SELECT @IsZeroSuppressActivityPassengerRpt = IsZeroSuppressActivityPassengerRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityPassengerRpt=0

DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		
CREATE TABLE #PAXTEMP(DateRange varchar(100),PaxCode bigint,PaxName varchar(150),FltDate varchar(100),TailNmbr varchar(9),DispatchNo varchar(12),
LogNum bigint,LegID bigint,LegNum bigint,DepICAOID char(4),DepCityName varchar(40),ArrICAOID char(4),ArrCityName varchar(40),Distance numeric(5,0),
Blk_Hrs numeric(12,3),Flt_Hrs numeric(12,3),TenToMin smallint,Pax_Total numeric(4,0)
,EsTotal numeric(4,0),fltcatdesc varchar(60),TotLine varchar(10),FltPurpose varchar(40),assflightcat varchar(max),
PassengerRequestorCD varchar(5),PassengerFirstName varchar(60),POLegID BIGINT,icaoid char(4),Fleet BIGINT)

CREATE TABLE #PAXTEMP1(DateRange varchar(100),PaxCode bigint,PaxName varchar(150),FltDate varchar(100),TailNmbr varchar(9),DispatchNo varchar(12),
LogNum bigint,LegID bigint,LegNum bigint,DepICAOID char(4),DepCityName varchar(40),ArrICAOID char(4),ArrCityName varchar(40),Distance numeric(5,0),
Blk_Hrs numeric(12,3),Flt_Hrs numeric(12,3),TenToMin smallint,Pax_Total numeric(4,0)
,EsTotal numeric(4,0),fltcatdesc varchar(60),TotLine varchar(10),FltPurpose varchar(40),assflightcat varchar(max),
PassengerRequestorCD varchar(5),PassengerFirstName varchar(60),POLegID BIGINT,icaoid char(4),Fleet BIGINT)

IF @RemovePaxDetail = 0
 BEGIN
INSERT INTO #PAXTEMP
           SELECT A.* FROM (SELECT DISTINCT 
             [DateRange]=dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
             [PaxCode]=P.PassengerRequestorID --'*******'+PPAX2.AssociatedFPURP
            ,[PaxName]= P.PassengerRequestorCD +' '+P.PassengerName--PP.PassengerLastName+' '+PP.PassengerFirstName+','+isnull(PP.PassengerMiddleName,'')
            --,[FlightPurpose]=PL.FlightPurpose  
            ,[FltDate]=dbo.GetDateFormatByUserCD(@UserCD,PL.ScheduledTM)
            ,[TailNmbr]=F.TailNum  
            ,[DispatchNo]=PM.DispatchNum  
            ,[LogNum]=PM.LogNum  
            ,[LegID]=PL.TripLegID  
            ,[LegNum]=PL.LegNUM  
            ,[DepICAOID]=AD.IcaoID  
            ,[DepCityName]=AD.IcaoID+' '+AD.CityName  
            ,[ArrICAOID]=AA.IcaoID  
            ,[ArrCityName]=AA.IcaoID +' '+AA.CityName  
            ,[Distance]=PL.Distance*1.150  
            ,[Blk_Hrs]=isnull(ROUND(PL.BlockHours,1),0)  
            ,[Flt_Hrs]=isnull(ROUND(PL.FlightHours,1),0)  
            ,[TenToMin]=@TenToMin
            ,[Pax_Total]=PL.PassengerTotal  
            ,[EsTotal]=F.MaximumPassenger-PL.PassengerTotal  
            --,[CatCode]=FC.FlightCatagoryCD  
            ,[fltcatdesc]=FC.FlightCatagoryCD +' '+FC.FlightCatagoryDescription  
            --,[DPaxCode]=P.PassengerRequestorCD  
            --,[DPaxName]=PP.PassengerFirstName+''+PP.PassengerLastName+''+PP.PassengerMiddleName  
            ,[TotLine]='********'  
            ,[FltPurpose]= PL.FlightPurpose
            ,[assflightcat]=ppax1.AssociatedFPURP1
            ,P.PassengerRequestorCD PassengerRequestorCD
            ,Pp.PassengerFirstName PassengerFirstName
            ,PL.POLegID
    ,[icaoid]=A.IcaoID
   , F.FleetID 
    
    FROM PostflightMain PM   
    
    INNER JOIN PostflightLeg PL ON PL.Pologid = PM.PoLogID AND PL.IsDeleted = 0  
    INNER JOIN PostflightPassenger PP ON PL.PoLegID = PP.PoLegID  
    INNER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID
    INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID   
    INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID       
    INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerID  ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
    INNER JOIN (  
     SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,FG.FleetGroupCD,F.MaximumPassenger   
     FROM Fleet F   
     LEFT OUTER JOIN FleetGroupOrder FGO  
     ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
     LEFT OUTER JOIN FleetGroup FG   
     ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
    WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
   	
   	INNER JOIN 
    (SELECT DISTINCT PP2.POLegID,pp2.FlightPurposeID,
    AssociatedFPURP = (SELECT RTRIM(P.PassengerName)+'&' FROM PostflightPassenger PP 
                INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
				WHERE PP.POLegID = PP2.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP2) PPax2 ON PL.POLegID = PPax2.POLegID   
	
	INNER JOIN 
    (SELECT DISTINCT PP1.POLegID,pp1.FlightPurposeID,
    AssociatedFPURP1 = (SELECT RTRIM(FP.FlightPurposeDescription)+'&' FROM PostflightPassenger PP 
              	INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP1.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP1) PPax1 ON PL.POLegID = PPax1.POLegID  
								
	LEFT OUTER JOIN Company Cp  ON PM.HomebaseID = Cp.HomebaseID  
	LEFT OUTER JOIN Airport A ON A.AirportID = Cp.HomebaseAirportID  						
    LEFT OUTER JOIN FlightCatagory FC  ON PL.FlightCategoryID = FC.FlightCategoryID  
    LEFT OUTER JOIN  FlightPurpose FP  ON PPax2.FlightPurposeID = FP.FlightPurposeID
    
   WHERE Pm.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))   
   
    AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
    AND PM.IsDeleted = 0
   )a
     
  
         
     --Construct OPTIONAL clauses  
 
 END 
Else BEGIN
INSERT INTO #PAXTEMP
          SELECT A.* FROM (SELECT DISTINCT 
          [DateRange]=dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
             [PaxCode]=''
            ,[PaxName]= P.PassengerRequestorCD +' '+P.PassengerName
            --,[FlightPurpose]=PL.FlightPurpose  
            ,[FltDate]=dbo.GetDateFormatByUserCD(@UserCD,PL.ScheduledTM)
            ,[TailNmbr]=F.TailNum  
            ,[DispatchNo]=PM.DispatchNum  
            ,[LogNum]=PM.LogNum  
            ,[LegID]=PL.TripLegID  
            ,[LegNum]=PL.LegNUM  
            ,[DepICAOID]=AD.IcaoID  
            ,[DepCityName]=AD.IcaoID+' '+AD.CityName  
            ,[ArrICAOID]=AA.IcaoID  
            ,[ArrCityName]=AA.IcaoID +' '+AA.CityName  
            ,[Distance]=PL.Distance*1.150  
            ,[Blk_Hrs]=isnull(ROUND(PL.BlockHours,1),0)  
            ,[Flt_Hrs]=isnull(ROUND(PL.FlightHours,1),0)  
            ,[TenToMin]=@TenToMin
            ,[Pax_Total]=PL.PassengerTotal  
            ,[EsTotal]=F.MaximumPassenger-PL.PassengerTotal  
            --,[CatCode]=FC.FlightCatagoryCD  
            ,[fltcatdesc]=FC.FlightCatagoryCD +' '+FC.FlightCatagoryDescription  
           -- ,[DPaxCode]=P.PassengerRequestorCD  
            --,[DPaxName]=PP.PassengerFirstName+''+PP.PassengerLastName+''+PP.PassengerMiddleName  
            ,[TotLine]=''  
            ,[FltPurpose]= pl.FlightPurpose 
             ,[assflightcat]=''
            ,P.PassengerRequestorCD PassengerRequestorCD
            ,Pp.PassengerFirstName PassengerFirstName
            ,PL.POLegID
            ,[icaoid]=A.IcaoID
            ,F.FleetID 
    FROM PostflightMain PM   
    INNER JOIN PostflightLeg PL ON PL.Pologid = PM.PoLogID AND PL.IsDeleted = 0   
    INNER JOIN PostflightPassenger PP ON PL.PoLegID = PP.PoLegID  
    INNER JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID   
    INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID   
    INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID       
    INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerID  ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
    INNER JOIN (  
     SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,FG.FleetGroupCD,F.MaximumPassenger   
     FROM Fleet F   
     LEFT OUTER JOIN FleetGroupOrder FGO  
     ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
     LEFT OUTER JOIN FleetGroup FG   
     ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
     WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
  	
  	inner JOIN 
    (SELECT DISTINCT PP2.POLegID,pp2.FlightPurposeID,
    AssociatedFPURP = (SELECT RTRIM(PP.PassengerLastName)+' '+RTRIM(PP.PassengerFirstName)+' '+RTRIM(PP.PassengerMiddleName)+' '+RTRIM(FP.FlightPurposeDescription)+'&' FROM PostflightPassenger PP 
                --INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
				INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP2.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP2) PPax2 ON PL.POLegID = PPax2.POLegID   
			
   inner JOIN 
    (SELECT DISTINCT PP1.POLegID,pp1.FlightPurposeID,
    AssociatedFPURP1 = (SELECT RTRIM(FP.FlightPurposeDescription)+'&' FROM PostflightPassenger PP 
                --INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
				INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP1.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP1) PPax1 ON PL.POLegID = PPax1.POLegID 
    LEFT OUTER JOIN Company Cp  ON PM.HomebaseID = Cp.HomebaseID  
    LEFT OUTER JOIN Airport A ON A.AirportID = Cp.HomebaseAirportID  			
      LEFT OUTER JOIN FlightCatagory FC  ON PL.FlightCategoryID = FC.FlightCategoryID  
      LEFT OUTER JOIN  FlightPurpose FP  ON PPax2.FlightPurposeID = FP.FlightPurposeID
   WHERE Pm.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))   
   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
   AND PM.IsDeleted = 0
     )A       
  
     --Construct OPTIONAL clauses  
 
 END 
insert into #PAXTEMP1
  SELECT A.* FROM (SELECT DISTINCT 
               [DateRange]=dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
             [PaxCode]=NULL
            ,[PaxName]= P.PassengerRequestorCD +' '+P.PassengerName
            --,[FlightPurpose]=PL.FlightPurpose  
            ,[FltDate]=NULL
            ,[TailNmbr]=NULL
            ,[DispatchNo]=NULL
            ,[LogNum]=NULL
            ,[LegID]=NULL
            ,[LegNum]=NULL
            ,[DepICAOID]=NULL
            ,[DepCityName]=NULL
            ,[ArrICAOID]=NULL
            ,[ArrCityName]=NULL
            ,[Distance]=NULL
            ,[Blk_Hrs]=NULL
            ,[Flt_Hrs]=NULL
            ,[TenToMin]=@TenToMin
            ,[Pax_Total]=NULL
            ,[EsTotal]=NULL
            --,[CatCode]=FC.FlightCatagoryCD  
            ,[fltcatdesc]=NULL
           -- ,[DPaxCode]=P.PassengerRequestorCD  
            --,[DPaxName]=PP.PassengerFirstName+''+PP.PassengerLastName+''+PP.PassengerMiddleName  
            ,[TotLine]=NULL  
            ,[FltPurpose]= NULL
             ,[assflightcat]=NULL
            ,P.PassengerRequestorCD PassengerRequestorCD
            ,NULL PassengerFirstName,null polegid
            ,[icaoid] =null
            ,Fleet =null
            FROM Passenger P
    left outer JOIN PostflightPassenger PP ON P.PassengerRequestorID = PP.PassengerID  
    left outer JOIN PostflightLeg PL ON PL.Polegid = Pp.PoLegID AND PL.IsDeleted = 0  
    left outer JOIN Postflightmain Pm ON Pm.Pologid = Pl.PoLogID   
    INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerID  ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
    INNER JOIN (  
     SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,FG.FleetGroupCD,F.MaximumPassenger   
     FROM Fleet F   
     LEFT OUTER JOIN FleetGroupOrder FGO  
     ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
     LEFT OUTER JOIN FleetGroup FG   
     ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
     WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
  	LEFT OUTER JOIN Company Cp  ON PM.HomebaseID = Cp.HomebaseID  
    LEFT OUTER JOIN Airport A ON A.AirportID = Cp.HomebaseAirportID  			
  
              
     WHERE P.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) and P.IsDeleted=0                  
     
     )A    
  
       
delete from #PAXTEMP1 where PassengerRequestorCD in(select distinct PassengerRequestorCD from #PAXTEMP)


IF @IsZeroSuppressActivityPassengerRpt=0
BEGIN  
SELECT M.* FROM          
(

	SELECT *,AssociatedFPURP=null FROM #PAXTEMP1 
		UNION ALL           
	SELECT DISTINCT T.*,(CASE(@RemovePaxDetail) WHEN 0 THEN PPax2.AssociatedFPURP ELSE '' END)AssociatedFPURP 
	FROM #PAXTEMP T
		INNER JOIN 
		(SELECT DISTINCT PP2.POLegID,pp2.FlightPurposeID,
				AssociatedFPURP = (SELECT RTRIM(P.PassengerName)+'&' FROM PostflightPassenger PP 
                INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
              --INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerID  ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
				--INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP2.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP2) PPax2 ON T.POLegID = PPax2.POLegID
)M   
where ( M.ArrICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '' )          
    --and ( M.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PAX, ',')) OR @PAX = '' )      
    and( M.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '' )         
    and ( M.TailNmbr IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' ) 
   -- and (M.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') 
		        ORDER BY (CASE (@Sortby) WHEN 'a' THEN  PassengerRequestorCD
							             WHEN 'b' THEN PassengerFirstName  END), fltDate, LegNum
END
ELSE
BEGIN
SELECT M.* FROM          
(
SELECT DISTINCT T.*,(CASE(@RemovePaxDetail) WHEN 0 THEN PPax2.AssociatedFPURP ELSE '' END)AssociatedFPURP 
	FROM #PAXTEMP T
		INNER JOIN 
		(SELECT DISTINCT PP2.POLegID,pp2.FlightPurposeID,
				AssociatedFPURP = (SELECT RTRIM(P.PassengerName)+'&' FROM PostflightPassenger PP 
                INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                --INNER JOIN ( SELECT DISTINCT PASSENGERREQUESTORID FROM @TempPassengerID  ) P1 ON P1.PASSENGERREQUESTORID = P.PassengerRequestorID
				--INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP2.POLegID FOR XML PATH('' ))
				FROM PostflightPassenger PP2) PPax2 ON T.POLegID = PPax2.POLegID	
)M   
where ( M.ArrICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '' )          
    --and ( M.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PAX, ',')) OR @PAX = '' )      
    and( M.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '' )         
    and ( M.TailNmbr IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' ) 
   -- and (M.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') 
		        ORDER BY (CASE (@Sortby) WHEN 'a' THEN  PassengerRequestorCD
							             WHEN 'b' THEN PassengerFirstName  END), fltDate, LegNum
END   

IF OBJECT_ID('tempdb..#PAXTEMP') IS NOT NULL
DROP TABLE #PAXTEMP
IF OBJECT_ID('tempdb..#PAXTEMP1') IS NOT NULL
DROP TABLE #PAXTEMP1        
 --EXEC spGetReportPostPassengerFlightHistoryInformation 'SUPERVISOR_99','2009-01-01','2009-12-12','','','','','' ,0,a

GO


