IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar1Information]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPREAircraftCalendar1Information]  
  @DATEFROM AS DATETIME, --Mandatory
  @UserCD AS VARCHAR(30),
  @TailNUM AS VARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values  
AS  
-- ===============================================================================  
-- SPC Name: spGetReportPREAircraftCalender1Information  
-- Author: ASKAR  
-- Create date: 04 Jul 2012  
-- Description: Get Crew Calender information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
-- 20 Aug 2012 Sudhakar 1.1  Fix in date filter  
-- ================================================================================  

BEGIN 
SET NOCOUNT ON 
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = ''; 
DECLARE @ParameterDefinition AS NVARCHAR(100);
DECLARE @NoOfRows INT,@CrCrewMember VARCHAR(250)
-- Populate the complete Crew List for each Date in the given range 

CREATE TABLE #TempFleetDate ( 
RowID INT identity(1, 1) NOT NULL, 
DeptDateOrg DATETIME, 
TailNum CHAR(10), 
AircraftID BIGINT, 
FleetID BIGINT, 
TripID BIGINT, 
LegID BIGINT, 
AT_Type CHAR(10), 
DepartICAOID CHAR(4), 
ArriveICAOID CHAR(4), 
DepartureDTTMLocal DATETIME, 
ArrivalDTTMLocal DATETIME, 
NextLocalDTTM DATETIME, 
DutyType CHAR(2), 
CrewMember VARCHAR(250), 
HomeBaseCD CHAR(8), 
TripNUM BIGINT,
RecordType  CHAR(1),
CrewCount INT,
Descript VARCHAR(40)) 


   
          
INSERT INTO #TempFleetDate(DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,AT_Type,
DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,
DutyType,CrewMember,HomeBaseCD,TripNUM,RecordType,CrewCount,Descript) 

SELECT @DATEFROM,F.TailNum,A.AircraftID,F.FleetID,PL.TripID,PL.LegID,A.AircraftCD,AT.IcaoID DepartICAOID,
       APT.IcaoID ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,PL.NextLocalDTTM,
       CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END
									ELSE PL.DutyTYPE END DutyTYPE, C1.CrewList, 
       dbo.GetHomeBaseCDByHomeBaseID(C.HomebaseAirportID) HomeBaseCD,PM.TripNUM,PM.RecordType,PL.PassengerTotal CrewCount,PM.TripDescription  
       FROM PreflightMain PM 
           INNER JOIN  PreflightLeg PL ON PL.TripID=PM.TripID  AND PL.IsDeleted=0
           INNER JOIN (SELECT FleetID,HomebaseID,AircraftID,TailNum, CustomerID FROM Fleet)F ON PM.FleetID=F.FleetID AND PM.CustomerID = F.CustomerID
           LEFT OUTER JOIN ( SELECT DISTINCT PC2.tripid, CrewList = ( SELECT DISTINCT C.CrewCD + ','   FROM PreflightCrewList PC1 
																										INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted=0
																										INNER JOIN Crew C ON PC1.CrewID = C.CrewID 
																										WHERE Pl.tripid = PC2.tripid 
																										FOR XML PATH('') ) FROM PreflightLeg PC2 WHERE PC2.IsDeleted=0) C1 ON PM.tripid = C1.tripid 
			INNER JOIN Company C ON C.HomebaseID=F.HomebaseID
			LEFT OUTER JOIN (SELECT AircraftID,AircraftCD FROM Aircraft)A ON A.AircraftID=F.AircraftID 
			INNER JOIN(SELECT AirportID,IcaoID FROM Airport)AT ON AT.AirportID=PL.DepartICAOID 
			INNER JOIN(SELECT AirportID,IcaoID FROM Airport)APT ON APT.AirportID=PL.ArriveICAOID 
			WHERE   (CONVERT(DATE,@DATEFROM) BETWEEN CONVERT(DATE, PL.DepartureDTTMLocal) AND  CONVERT(DATE, PL.ArrivalDTTMLocal) 
			 OR   CONVERT(DATE,@DATEFROM) BETWEEN CONVERT(DATE, PL.ArrivalDTTMLocal) AND  CONVERT(DATE, PL.NextLocalDTTM)) 
              AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) 
              AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
              AND PM.TripStatus NOT IN('X','U','W')
              AND PM.IsDeleted=0
              
 --DECLARE @DutyType CHAR(2)         
 --SET @DutyType=(SELECT TOP 1 DutyType FROM #TempFleetDate WHERE DutyType='MX')   
 
 UPDATE #TempFleetDate SET DepartureDTTMLocal=CASE WHEN CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,@DATEFROM) THEN   CONVERT(DATETIME, CONVERT(VARCHAR(10),@DATEFROM, 101) + ' 00:00') ELSE DepartureDTTMLocal END,
                           ArrivalDTTMLocal=CASE WHEN CONVERT(DATE,ArrivalDTTMLocal) <> CONVERT(DATE,@DATEFROM) THEN  CONVERT(DATETIME, CONVERT(VARCHAR(10),@DATEFROM, 101) + ' 23:59') ELSE ArrivalDTTMLocal END
                        WHERE RecordType='M'
                        AND DutyType <> 'R'
                  
                               
          

DECLARE @crTailNum CHAR(6), @crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT, @crTripID BIGINT, 
@crLegID BIGINT, @crDepartICAOID CHAR(4),@crArriveICAOID CHAR(4),@crDepartureDTTMLocaL DATETIME,@crArrivalDTTMLocal DATETIME, 
@crNextLocalDTTM DATETIME,@crDutyType CHAR(2),@crCrewCD CHAR(5),@crHomebaseCD CHAR(4),@crTripNUM BIGINT 
DECLARE @Loopdate DATETIME,@crCrewCount INT,@crTripDescription  VARCHAR(40)



DECLARE curFleetCal CURSOR FAST_FORWARD READ_ONLY FOR 
 
 SELECT TFD.TailNum,TFD.DeptDateOrg,TFD.AircraftID,TFD.FleetID,TFD.TripID,TFD.LegID,TFD.DepartICAOID,TFD.ArriveICAOID, 
		TFD.DepartureDTTMLocal,TFD.ArrivalDTTMLocal,TFD.NextLocalDTTM,TFD.DutyTYPE,TFD.HomebaseCD,TFD.TripNUM,TFD.CrewMember,TFD.CrewCount,TFD.Descript 
		FROM #TempFleetDate TFD 
			WHERE CONVERT(DATE, TFD.ArrivalDTTMLocal) < CONVERT(DATE, TFD.NextLocalDTTM) 
			AND RecordType <> 'M'
			ORDER BY DeptDateOrg,TFD.TailNum 
OPEN curFleetCal; 

FETCH NEXT FROM curFleetCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crArriveICAOID, 
@crDepartureDTTMLocal,@crArrivalDTTMLocal,@crNextLocalDTTM,@crDutyType,@crHomebaseCD,@crTripNUM,@CrCrewMember,@crCrewCount,@crTripDescription; 


WHILE @@FETCH_STATUS = 0 

BEGIN 

SET @Loopdate = ''; 
--STEP 1: 
BEGIN

INSERT INTO #TempFleetDate(DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,DutyType,HomeBaseCD,TripNUM,CrewMember,CrewCount,Descript) 
VALUES (@crDeptDateOrg,@crTailNum,@crAircraftID,@crFleetID,@crTripID,@crLegID,@crArriveICAOID,@crArriveICAOID,@crArrivalDTTMLocal,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrivalDTTMLocal, 101) + ' 23:59', 101),NULL, 'R',@crHomeBaseCD,@crTripNUM,@CrCrewMember,@crCrewCount,@crTripDescription) 

END
--STEP 2: 

SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrivalDTTMLocal, 101) + ' 00:00', 101)); 

WHILE(CONVERT(DATE, @Loopdate) < CONVERT(DATE, @crNextLocalDTTM)) 

    BEGIN 
--PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101) 
		INSERT INTO #TempFleetDate (DeptDateOrg,TailNum,AircraftID,FleetID,TripID,LegID,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,NextLocalDTTM,DutyType,HomeBaseCD,TripNUM,CrewMember,CrewCount,Descript) 
		VALUES (@Loopdate,@crTailNum,@crAircraftID,@crFleetID,@crTripID,@crLegID,@crArriveICAOID,'RON',CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:00', 101), CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),NULL,NULL,@crHomeBaseCD,@crTripNUM,@CrCrewMember,NULL,@crTripDescription) 
  
        SET @Loopdate = DATEADD(DAY, 1, @Loopdate); 

   END

FETCH NEXT FROM curFleetCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crArriveICAOID, 
@crDepartureDTTMLocal,@crArrivalDTTMLocal,@crNextLocalDTTM,@crDutyType,@crHomebaseCD,@crTripNUM,@CrCrewMember,@crCrewCount,@crTripDescription; 

END 

 CLOSE curFleetCal; 
DEALLOCATE curFleetCal; 

SET @NoOfRows =(SELECT COUNT(*) from #TempFleetDate); 

IF @NoOfRows > 0

BEGIN

SELECT DeptDateOrg,TailNum,AT_Type,DepartICAOID,ArriveICAOID,DepartureDTTMLocal,ArrivalDTTMLocal,DutyType, 
SUBSTRING(CrewMember,1,LEN(CrewMember)-1) AS CrewMember, CrewCount Crew_Count,TripNUM,CASE(RecordType) WHEN 'M' THEN Descript ELSE '' END Descript,
RecordType
FROM #TempFleetDate 
WHERE CONVERT(DATE,DepartureDTTMLocal)=CONVERT(DATE,@DATEFROM)

END 

ELSE 

BEGIN

SELECT NULL DeptDateOrg, NULL TailNum, NULL AT_Type,A1.ICAOID DepartICAOID, 
NULL ArriveICAOID,NULL DepartureDTTMLocal, NULL ArrivalDTTMLocal, NULL DutyType, 
NULL CrewMember,NULL Crew_Count,NULL TripNUM,NULL Descript,NULL RecordType
FROM Fleet F 
INNER JOIN Company C ON F.HomebaseID = C.HomebaseID
INNER JOIN Airport A ON C.HomebaseAirportID = A.AirportID
OUTER APPLY dbo.GetFleetCurrentAirpotCD(@DATEFROM, F.FleetID) AS A1 
WHERE F.TailNum = @TailNUM
AND F.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 

END


IF OBJECT_ID('tempdb..#TempFleetDate') IS NOT NULL
DROP TABLE #TempFleetDate 

END 


---EXEC spGetReportPREAircraftCalendar1Information '12-10-09', 'tim', 'N4753'

---EXEC spGetReportPREAircraftCalendar1Information '2012-07-20', 'UC', ''

GO


