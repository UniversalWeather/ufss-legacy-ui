IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBSalesPersonsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBSalesPersonsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportDBSalesPersonsExportInformation]   
(  
        @UserCD AS VARCHAR(30) --Mandatory  
       ,@UserCustomerID AS VARCHAR(30)  
)  
AS  
BEGIN  
   
  
-- ===============================================================================  
-- SPC Name: spGetReportDBSalesPersonsExportInformation  
-- Author: 
-- Create date: 04 Nov 2013
-- Description: Get SalesPerson Information  
-- Revision History  
-- Date        Name        Ver         Change  
--   
-- ================================================================================  
  
SELECT [code] = SP.SalesPersonCD,
	   [desc] = CASE WHEN SP.LastName<>'' THEN SP.LastName +', ' ELSE '' END+ISNULL(FirstName,'')+' '+ISNULL(MiddleName,'') 
       FROM SalesPerson SP
       WHERE SP.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
         AND SP.IsDeleted=0
         --AND SP.IsInActive=0  
-- And IsInActive=0  
 --And V.IsDeleted=0 
 ORDER BY SalesPersonCD 
  
END  
  
--EXEC spGetReportDBSalesPersonsExportInformation 'SUPERVISOR_16',10016



GO


