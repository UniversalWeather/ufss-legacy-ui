IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPreferencesInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPreferencesInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPreferencesInformation]  
 @UserCD VARCHAR(30)    
AS    
-- =============================================    
-- SPC Name: spGetReportPreferencesInformation    
-- Author: SUDHAKAR J    
-- Create date: 12 Jun 2012    
-- Description: Get Report Preferences Information for Header, Footer etc..    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
    
SET NOCOUNT ON    
    
  SELECT TOP 1    
    [UserCD] = u.UserName    
   ,u.CustomerID    
  --,[HomeBase] = c.HomeBaseCD    
  ,[HomeBase] = A.IcaoID    
  ,c.CompanyName  -- Header 1   
  ,ISNULL(c.CurrencySymbol,'$') AS CurrencySymbol  
  ,c.DayMonthFormat -- Day Month format in the report content    
  ,c.HeardTextRpt -- ??    
  ,c.FooterTextRpt  -- ??    
  ,c.CustomRptMessage  -- Footer message    
  ,c.IsTimeStampRpt -- whether to dispplay timestamp in the report footer  
  ,[ShortDateString] = DBO.GetDateFormatStringByUserCD(@UserCD, 0)    
  ,[LongDateString] = DBO.GetDateFormatStringByUserCD(@UserCD, 1)  
  ,[DayMonthString] = DBO.GetDateFormatStringByUserCD(@UserCD, 2)  
  ,[FooterLogo] = FW.UWAFilePath  
  ,[TSLogoPosition] = C.TripS -- 0 : None, 1 : Center, 2 : Left, 3 : Right   
  ,[TenToMin] = c.TimeDisplayTenMin  
  ,[FooterVisibility] = C.IsPrintUWAReports  
  ,[ShortDateStringForTripSheet] = DBO.GetDateFormatStringByUserCDForTripSheet(@UserCD, 0)    
  ,[LongDateStringForTripSheet] = DBO.GetDateFormatStringByUserCDForTripSheet(@UserCD, 1)  
  ,[DayMonthStringForTripSheet] = DBO.GetDateFormatStringByUserCDForTripSheet(@UserCD, 2) 
  ,CASE
		WHEN A.IsDayLightSaving = 1 AND	GETDATE() >= A.DaylightSavingStartDT AND
		GETDATE() <= A.DayLightSavingEndDT THEN DATEADD(MI, (A.OffsetTOGMT + 1) * 60, GETUTCDATE())
		ELSE DATEADD(MI, ISNULL(A.OffsetTOGMT,0) * 60, GETUTCDATE())
	END As LocalDateTime 
  
  FROM UserMaster u   
 INNER JOIN Company c ON u.CustomerID = c.CustomerID AND u.HomeBaseID = c.HomeBaseID    
 INNER JOIN Airport A ON C.HomebaseAirportID = A.AirportID    
 LEFT OUTER JOIN (SELECT TOP 1 F.UWAFilePath, F.CustomerID  
  FROM FileWarehouse F  
  WHERE UWAWebpageName = 'CompanyProfileCatalog.aspx'  
  AND RTRIM(RecordType)='Company'  
  AND F.IsDeleted = 0  
  AND F.CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)  
  AND F.RecordID = DBO.GetHomeBaseByUserCD(@UserCD)  
  ORDER BY LastUpdTS DESC  
 ) FW ON U.CustomerID = FW.CustomerID  
  WHERE RTRIM(u.UserName) = RTRIM(@UserCD)    
    
  -- EXEC spGetReportPreferencesInformation 'JWILLIAMS_13'  
GO


