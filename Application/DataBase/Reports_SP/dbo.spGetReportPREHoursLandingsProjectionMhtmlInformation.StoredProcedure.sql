IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREHoursLandingsProjectionMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================    
-- SPC Name: spGetReportPREHoursLandingsProjectionExportInformation   
-- Author: Abhishek S    
-- Create date: 1st Aug 2012    
-- Description: Get estimated hours and handlings excel report based on scheduled
--              trips   
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================ 
create PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionMhtmlInformation]
        @UserCD AS VARCHAR(30), -- Mandatory
		@DATEFROM AS DATETIME,  -- Mandatory
		@DATETO AS DATETIME,    -- Mandatory
		@TailNUM AS NVARCHAR(300) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(300) = '', -- [Optional], Comma delimited string with mutiple values
	    @TripNUM As NVARCHAR(500)= '', -- [Optional]
	    @HomeBase AS BIT = 0
   
AS
BEGIN
	
	SET NOCOUNT ON;
	
	  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
      DECLARE @ParameterDefinition AS NVARCHAR(100)
      
      SET @SQLSCRIPT = '
      
      SELECT
      [tail_nmbr] = F.TailNum
     ,[ac_code] = CONVERT(VARCHAR(30),F.AircraftCD)
     ,[type_code] = AT.AircraftCD 
	 ,[tot_line] = ''R''
	 ,[elp_time] = PL.ElapseTM
	 ,[flt_hrs] = PL.FlightHours
	 ,[landings] = COUNT(PL.LegID)
	 
	  FROM PreflightMain PM
       INNER JOIN PreflightLeg PL
       ON PM.TripID = PL.TripID AND PL.ISDELETED = 0        
	   INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID	
				WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 						
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN(SELECT AircraftID, AircraftCD, AircraftTypeCD FROM Aircraft)AT
       ON AT.AircraftID = F.AircraftID 
	   
      WHERE PM.IsDeleted = 0
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND PM.TripStatus = ''T''
         AND PM.RecordType = ''T''
         AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
 ' 
	 
	IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FGO.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @TripNUM <> '' BEGIN  
		-- SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
	      SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM  = ' + @TripNUM
	END
	IF @HomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	

    SET @SQLSCRIPT= @SQLSCRIPT+' GROUP BY F.TailNum, AT.AircraftCD, F.AircraftCD, PL.ElapseTM,PL.FlightHours'

    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS VARCHAR(50)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO,@FleetGroupCD
 
END

--EXEC spGetReportPREHoursLandingsProjectionExportInformation 'uc','2000-07-20','2012-08-01','', '', '', 0


GO


