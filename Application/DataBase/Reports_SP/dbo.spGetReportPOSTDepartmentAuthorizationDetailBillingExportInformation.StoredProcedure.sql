IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailBillingExportInformation]
		(
		@UserCD AS VARCHAR(30),
		@UserCustomerID AS BIGINT,
		@DATEFROM AS DATETIME , --Mandatory
		@DATETO AS DATETIME , --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@DepartmentGroupID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0,
		@HomebaseCD AS NVARCHAR(1000) = ''
		)

AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailBillingExportInformation
-- Author:  Vikram M
-- Create date: 31 Jul 2012
-- Description: Get FlightHours/BlockHours details based on Department and Authorization
---EXEC spGetReportPOSTDepartmentAuthorizationDetailBillingExportInformation 'SUPERVISOR_99','10099','2015-05-05,'2015-05-07'

-- ================================================================================
BEGIN

SET NOCOUNT ON
DECLARE @AircraftBasis NUMERIC(1,0);
		SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
		AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 
			
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMERID 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------




SELECT DISTINCT dept_code=DEL.DepartmentCD,
			    auth_code=DAL.AuthorizationCD,
			    tail_nmbr=FL.TailNum,
			    estdepdt=PO.EstDepartureDT,
			    dispatchno=PO.DispatchNum,
			    lognum=PO.LogNum,
			    reqdept=DEO.DepartmentCD,
			    reqauth=DAO.AuthorizationCD,
			    legid=PL.POLegID,
			    schedttm=PL.ScheduledTM,
			    depicao_id=AD.IcaoID,
			    arricao_id=AA.IcaoID,
			    distance=PL.Distance,
			    flt_hrs=ROUND(PL.FlightHours,1), 
			    blk_hrs=ROUND(PL.BlockHours,1),
			    pax_total=PL.PassengerTotal,
			    max_pax=FL.MaximumPassenger,
			    type_code=FL.AircraftCD,
			    es_total = ISNULL((FL.MaximumPassenger - PL.PassengerTotal),0),
			    chrg_rate=CR.ChargeRate,
                chrg_unit=CR.ChargeUnit,
                tot_line='',
                total_charges =(CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ ISNULL((PFE11.SUMExpenseAmt),0))
							         WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ ISNULL((PFE11.SUMExpenseAmt),0)) 
									 WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
									 WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0)) 
							     END),
				paxname=P.LastName+(CASE WHEN ISNULL(P.FirstName,'')<>'' THEN  ', '+P.FirstName ELSE '' END)+' '+ISNULL(P.MiddleInitial,''),
                fltpurpose=FP.FlightPurposeCD,
                PL.DepartmentID,
                AuthorizationID=ISNULL(PL.AuthorizationID,999),
                PassengerRequestorCD,
                99 Tot,
                RnkDept=ROW_NUMBER()OVER(PARTITION BY PL.DepartmentID ORDER BY PL.ScheduledTM DESC,PassengerRequestorCD DESC),
                Rnk=ROW_NUMBER()OVER(PARTITION BY PL.DepartmentID,PL.AuthorizationID ORDER BY PL.ScheduledTM DESC,PassengerRequestorCD DESC),--,PP.OrderNum
                RnkLeg=ROW_NUMBER()OVER(PARTITION BY PL.POLegID ORDER BY PL.ScheduledTM DESC,PassengerRequestorCD DESC),
                RnkTot=ROW_NUMBER()OVER(PARTITION BY 1 ORDER BY PL.ScheduledTM DESC,PassengerRequestorCD DESC)
                ,[expaccount] = EX.AccountDescription
				,[exp_amount] = EX.ExpenseAMT
	            INTO #DeptAuth
	FROM PostflightMain PO 
		 INNER JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
		 INNER JOIN Fleet FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID 
		 INNER  JOIN Department DEL ON PL.DepartmentID = DEL.DepartmentID AND DEL.CustomerID = PL.CustomerID
		 INNER JOIN @TempFleetID TF ON TF.FleetID=FL.FleetID
		 LEFT OUTER JOIN DepartmentAuthorization DAL ON PL.AuthorizationID = DAL.AuthorizationID
		 LEFT OUTER JOIN Department DEO ON PO.DepartmentID = DEO.DepartmentID AND DEO.CustomerID = PO.CustomerID
		 LEFT OUTER JOIN DepartmentAuthorization DAO ON PO.AuthorizationID = DAO.AuthorizationID
	     LEFT OUTER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID         
	     LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID       
	     LEFT OUTER JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                      SELECT ISNULL(SUM(PP2.ExpenseAMT),0) 
                               FROM PostflightExpense PP2 
									    WHERE PP2.POLegID = PP4.POLegID AND IsBilling = 1)
                       FROM PostflightExpense PP4 WHERE IsBilling =1 
                       )PFE11
                      ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID			  
         LEFT OUTER JOIN (SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON PL.POLegID = CR.POLegID 
           
          LEFT OUTER JOIN PostflightPassenger PP ON PP.POLegID=PL.POLegID
          LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PP.PassengerID
          LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID=P.FlightPurposeID
          LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
																		  INNER JOIN Account A ON PE.AccountID = A.AccountID
																	  WHERE PE.IsDeleted = 0
																	  AND PE.POLegID=PE1.POLegID
																	  FOR XML PATH('')
														)
								,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(200),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
																		  INNER JOIN Account A ON PE.AccountID = A.AccountID
																	  WHERE PE.IsDeleted = 0
																	  AND PE.POLegID=PE1.POLegID
																	  FOR XML PATH('')
														)
											FROM PostflightExpense PE1
									
				)EX ON EX.POLegID = PL.POLegID    
	
	WHERE  CONVERT(DATE,PL.ScheduledTM) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
		   AND (DEL.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		   AND (DAL.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')
	       AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
		   AND PO.CustomerID = @UserCustomerID
		   AND PO.IsDeleted = 0
ORDER BY DEL.DepartmentCD DESC,schedttm DESC,PassengerRequestorCD DESC




SELECT DISTINCT dept_code,
	    auth_code,
	    tail_nmbr,
	    estdepdt,
	    dispatchno,
	    lognum,
	    reqdept,
	    reqauth,
	    legid,
	    schedttm,
	    depicao_id,
	    arricao_id,
	    distance,
	    flt_hrs=FLOOR(flt_hrs*10)*0.1, 
	    blk_hrs=FLOOR(blk_hrs*10)*0.1,
	    pax_total,
	    max_pax,
	    type_code,
	    es_total,
	    chrg_rate,
        chrg_unit,
        tot_line,
        total_charges,
		paxname,
        fltpurpose,
        expaccount,
		exp_amount FROM (

SELECT DISTINCT  dept_code,
	    auth_code,
	    tail_nmbr,
	    estdepdt,
	    dispatchno,
	    lognum,
	    reqdept,
	    reqauth,
	    legid,
	    schedttm,
	    depicao_id,
	    arricao_id,
	    distance,
	    flt_hrs, 
	    blk_hrs,
	    pax_total,
	    max_pax,
	    type_code,
	    es_total,
	    chrg_rate,
        chrg_unit,
        tot_line='',
        total_charges,
		paxname,
        fltpurpose,
        expaccount,
		exp_amount FROM #DeptAuth DA 
UNION ALL

SELECT DISTINCT  dept_code,
	    auth_code,
	    tail_nmbr,
	    estdepdt=NULL,
	    dispatchno=NULL,
	    lognum,
	    reqdept=NULL,
	    reqauth=NULL,
	    legid=0,
	    schedttm,
	    depicao_id=NULL,
	    arricao_id=NULL	,
	    distance=0,
	    flt_hrs=D.flt_hrs,
	    blk_hrs=D.blk_hrs,
	    pax_total=0,
	    max_pax=0,
	    type_code=NULL,
	    es_total=0,
	    chrg_rate=0,
        chrg_unit=NULL,
        tot_line='R',
        total_charges=0,
		paxname,
        fltpurpose=NULL,
        expaccount,
		exp_amount FROM #DeptAuth DA
                        INNER JOIN (SELECT  DepartmentID,AuthorizationID,flt_hrs=SUM(flt_hrs), blk_hrs=SUM(blk_hrs) FROM #DeptAuth DA WHERE RnkLeg=1 GROUP  BY DepartmentID,AuthorizationID)D ON D.DepartmentID=DA.DepartmentID AND D.AuthorizationID=DA.AuthorizationID 
						WHERE Rnk=1

UNION ALL

SELECT DISTINCT  dept_code,
	    auth_code='Z*SubTot',
	    tail_nmbr,
	    estdepdt=NULL,
	    dispatchno=NULL,
	    lognum,
	    reqdept=NULL,
	    reqauth=NULL,
	    legid=0,
	    schedttm,
	    depicao_id=NULL,
	    arricao_id=NULL	,
	    distance=0,
	    flt_hrs=D.flt_hrs,
	    blk_hrs=D.blk_hrs,
	    pax_total=0,
	    max_pax=0,
	    type_code=NULL,
	    es_total=0,
	    chrg_rate=0,
        chrg_unit=NULL,
        tot_line='S',
        total_charges=0,
		paxname,
        fltpurpose=NULL,
        expaccount,
		exp_amount FROM #DeptAuth DA
                        INNER JOIN (SELECT  DepartmentID,flt_hrs=SUM(flt_hrs), blk_hrs=SUM(blk_hrs) FROM #DeptAuth DA WHERE RnkLeg=1 GROUP  BY DepartmentID)D ON D.DepartmentID=DA.DepartmentID 
						WHERE RnkdEPT=1

UNION ALL

SELECT DISTINCT  TOP 1 dept_code='ZZZZZZZZ',
	    auth_code='Z*Totals',
	    tail_nmbr,
	    estdepdt=NULL,
	    dispatchno=NULL,
	    lognum,
	    reqdept=NULL,
	    reqauth=NULL,
	    legid=0,
	    schedttm,
	    depicao_id=NULL,
	    arricao_id=NULL	,
	    distance=0,
	    flt_hrs=D.flt_hrs,
	    blk_hrs=D.blk_hrs,
	    pax_total=0,
	    max_pax=0,
	    type_code=NULL,
	    es_total=0,
	    chrg_rate=0,
        chrg_unit=NULL,
        tot_line='T',
        total_charges=0,
		paxname,
        fltpurpose=NULL,
        expaccount,
		exp_amount FROM #DeptAuth DA
                        INNER JOIN (SELECT  Tot,flt_hrs=SUM(flt_hrs), blk_hrs=SUM(blk_hrs) FROM #DeptAuth DA WHERE RnkLeg=1 GROUP BY Tot)D ON D.Tot=DA.Tot 
 )DeptAuth
 ORDER BY dept_code,auth_code,tot_line

IF OBJECT_ID('tempdb..#DeptAuth') IS NOT NULL
DROP TABLE  #DeptAuth 

END

GO


