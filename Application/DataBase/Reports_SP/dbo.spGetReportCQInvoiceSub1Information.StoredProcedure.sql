IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQInvoiceSub1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQInvoiceSub1Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportCQInvoiceSub1Information] @CQFileID VARCHAR(30)
	,@QuoteNUM INT
AS
-- =============================================
-- SPC Name:spGetReportCQInvoiceSubInformation
-- Author: Aishwarya.M
-- Create date: 06 Mar 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN



--SELECT * FROM CQInvoiceSummary WHERE CQInvoiceMainID IN (SELECT CQInvoiceMainID FROM CQInvoiceMain 
--           WHERE CQMainID IN(SELECT CQMainID FROM CQMain WHERE CQFileID=100023449 AND QuoteNUM=1) )
         

		SELECT DISTINCT INVOICE.*
	FROM (
		SELECT DISTINCT 8 RowID, InfoDesc = CASE 
				WHEN CQIS.FlightChgDescription IS NOT NULL
					AND CQIS.FlightChgDescription <> ''
					THEN CASE 
							WHEN CQIS.IsPrintFlightChg = 1 /* AND CL.IsPositioning = 1*/
								THEN CQIS.FlightChgDescription + ' (' + ISNULL(CONVERT(VARCHAR(30),CMM.FlightHours),'') + ' HRS' + ')'
							ELSE NULL
							END
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintFlightChg = 1 --AND CL.IsPositioning = 1 
					THEN CQIS.FlightChargeTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		 INNER JOIN (SELECT CM.CQFileID,CM.QuoteNUM,SUM(CL.FlightHours) FlightHours FROM CQMain CM 
			                   INNER JOIN CQInvoiceMain CQI ON CM.CQMainID = CQI.CQMainID
			                   INNER JOIN CQInvoiceQuoteDetail CL ON CQI.CQInvoiceMainID = CL.CQInvoiceMainID	 
			             WHERE CM.CQFileID = CONVERT(BIGINT,@CQFileID)
	                       AND CM.QuoteNUM = @QuoteNUM	
	                      GROUP BY CM.CQFileID,CM.QuoteNUM
                  )CMM ON CMM.CQFileID=CM.CQFileID	
		
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT  4,
		InfoDesc = CASE WHEN  CQIS.IsPrintUsage = 1 THEN CQIS.UsageAdjDescription ELSE 'uncheck' END
		/*InfoDesc = CASE 
				WHEN CQIS.UsageAdjDescription IS NOT NULL
					AND CQIS.UsageAdjDescription <> ''
					THEN CASE 
							WHEN CQIS.IsPrintUsage = 1
								THEN CQIS.UsageAdjDescription + ' (' + CONVERT(VARCHAR(30), CASE 
											WHEN CQIS.IsPrintUsage = 1
												THEN CQIS.UsageAdjTotal
											ELSE 0
											END) + ' HRS' + ')'
							ELSE NULL
							END
				ELSE NULL
				END */
			,InfoDescvalue = CASE 
				WHEN CQIS.IsPrintUsage = 1
					THEN CQIS.UsageAdjTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 5,InfoDesc = CASE 
				WHEN CQIS.IsPrintAdditonalFees = 1
					THEN CQIS.DescriptionAdditionalFee
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintAdditonalFees = 1
					THEN CQIS.AdditionalFeeTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT 7,InfoDesc =CASE WHEN CQIS.StdCrewRONDescription  IS NOT NULL AND CQIS.StdCrewRONDescription <>'' THEN CASE 
				WHEN CQIS.IsPrintStdCrew = 1
					THEN --CM.StdCrewROMDescription 
						CQIS.StdCrewRONDescription + ' (' + ISNULL(CONVERT(VARCHAR(30), CASE 
								WHEN CQIS.IsPrintStdCrew = 1
									THEN CQIS.StdCrewRONTotal
								ELSE 0
								END),'') + ' RON' + ')'
				ELSE NULL
				END  ELSE 'uncheck' END 
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintStdCrew = 1
					THEN CQIS.StdCrewRONTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		--UNION ALL
		
		--SELECT InfoDesc = CASE 
		--		WHEN CQIS.IsPrintAdditionalCrew = 1
		--			THEN CQIS.AdditionalCrewDescription
		--		ELSE NULL
		--		END
		--	,InfoDescValue = CASE 
		--		WHEN CQIS.IsPrintAdditionalCrew = 1
		--			THEN CQIS.AdditionalCrewTotal
		--		ELSE 0
		--		END
		--FROM CQInvoiceMain CQIM
		--INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID
		--LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
		--	AND CM.QuoteNUM = @QuoteNUM
		
		--UNION ALL
		
		--SELECT InfoDesc = CASE 
		--		WHEN CQIS.IsAdditionalCrewRON = 1
		--			THEN CQIS.AdditionalCrewRON
		--		ELSE NULL
		--		END
		--	,InfoDescValue = CASE 
		--		WHEN CQIS.IsAdditionalCrewRON = 1
		--			THEN /*CM.AdditionalCrewRONTotal2*/ CQIS.AdditionalCrewRONTotal
		--		ELSE 0
		--		END
		--FROM CQInvoiceMain CQIM
		--INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID
		--LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
		--	AND CM.QuoteNUM = @QuoteNUM
		
		--UNION ALL
		
		--SELECT InfoDesc = CQIS.WaitChgdescription --CASE WHEN CQIM.IsPrintWaitingChg = 1 THEN CQIS.WaitChgdescription ELSE NULL END
		--	,InfoDescValue = ISNULL(CQIS.WaitChgTotal, 0) --CASE WHEN CQIS.IsPrintWaitingChg = 1 THEN CQIS.WaitChgTotal ELSE 0 END
		--FROM CQInvoiceMain CQIM
		--INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID
		--LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
		--	AND CM.QuoteNUM = @QuoteNUM
		
		--UNION ALL
		
		--SELECT InfoDesc = CASE 
		--		WHEN CQIS.IsPrintLandingFees = 1
		--			THEN CQIS.LandingFeeDescription
		--		ELSE NULL
		--		END
		--	,InfoDescValue = CASE 
		--		WHEN CQIS.IsPrintLandingFees = 1
		--			THEN CQIS.LandingFeeTotal
		--		ELSE 0
		--		END
		--FROM CQInvoiceMain CQIM
		--INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID
		--LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
		--	AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 6,InfoDesc = CASE 
				WHEN CQIS.IsPrintSegmentFee = 1
					THEN CQIS.SegmentFeeDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintSegmentFee = 1
					THEN CQIS.SegmentFeeTotal
				ELSE 0
				END --CQFleetChargeTotal.SegementFeeTotal(CQMain.totsegfee),
				,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 9,InfoDesc = CASE 
				WHEN CQIS.IsPrintSubtotal = 1
					THEN CQIS.SubtotalDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintSubtotal = 1
					THEN CQIS.SubtotalTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 10,
		InfoDesc =CASE 
				WHEN  ISNULL(CQIS.DescriptionDiscountAmt,'') <> ''
					THEN CASE 
							WHEN CQIS.IsPrintDiscountAmt = 1
								THEN  CQIS.DescriptionDiscountAmt+ ' (' + CONVERT(VARCHAR(30),CQIS.DiscountAmtTotal) + ' %' + ')'
							ELSE NULL
							END
				ELSE 'uncheck' END 
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintDiscountAmt = 1
					THEN CQIS.DiscountAmtTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 11,InfoDesc = CASE 
				WHEN CQIS.IsPrintTaxes = 1
					THEN CQIS.TaxesDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintTaxes = 1
					THEN CQIS.TaxesTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 12,InfoDesc = CASE 
				WHEN CQIS.IsInvoiceTax = 1
					THEN CQIS.InvoiceDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintInvoice = 1
					THEN CQIS.InvoiceTotal
				ELSE 0
				END --CM.InvoiceTotal
				,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 13,InfoDesc = CASE 
				WHEN CQIS.IsPrintPrepay = 1
				--	AND CQIS.PrepayTotal <> 0
					THEN CQIS.PrepayDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintPrepay = 1
					--AND CQIS.PrepayTotal <> 0
					THEN CQIS.PrepayTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
		
		UNION ALL
		
		SELECT DISTINCT 14, InfoDesc = CASE 
				WHEN CQIS.IsPrintRemaingAMT = 1
					--AND CQIS.PrepayTotal <> 0
					THEN CQIS.RemainingAmtDescription
				ELSE 'uncheck'
				END
			,InfoDescValue = CASE 
				WHEN CQIS.IsPrintRemaingAMT = 1
					--AND CQIS.PrepayTotal <> 0
					THEN CQIS.RemainingAmtTotal
				ELSE 0
				END,InvoiceTotal =CQIS.InvoiceTotal
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER  JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		--LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
			AND CM.QuoteNUM = @QuoteNUM
UNION ALL
SELECT DISTINCT 1,StandardChargesAdditonalFees =CASE WHEN CQIA.IsPrintable=1 THEN  CQIA.CQInvoiceFeeDescription  ELSE 'uncheck' END
		,InvoicedAmount =CASE WHEN CQIA.IsPrintable=1 THEN CQIA.InvoiceAmt ELSE NULL END
		,InvoiceTotal =CQIS.InvoiceTotal
		 FROM CQInvoiceMain CQIM 
		 INNER JOIN CQMain CM ON CQIM.CQMainID=CM.CQMainID AND CM.IsDeleted=0
		 INNER JOIN CQFile CQF ON CQF.CQFileID = CM.CQFileID
         LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
         INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
         WHERE CM.CQFileID=CONVERT(BIGINT,@CQFileID)
           AND  CM.QuoteNUM=@QuoteNUM
           AND OrderNUM =1
           AND CQIA.IsPrintable=1	

UNION ALL

SELECT DISTINCT  2,StandardChargesAdditonalFees =CASE WHEN CQIA.IsPrintable=1 THEN CQIA.CQInvoiceFeeDescription ELSE 'uncheck' END
		,InvoicedAmount =CASE WHEN CQIA.IsPrintable=1 THEN CQIA.InvoiceAmt ELSE NULL END
		,InvoiceTotal =CQIS.InvoiceTotal
		 FROM CQInvoiceMain CQIM 
		 INNER JOIN CQMain CM ON CQIM.CQMainID=CM.CQMainID AND CM.IsDeleted=0
		 INNER JOIN CQFile CQF ON CQF.CQFileID = CM.CQFileID
		 INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
         LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
         WHERE CM.CQFileID=CONVERT(BIGINT,@CQFileID)
           AND  CM.QuoteNUM=@QuoteNUM
           AND OrderNUM =2
           AND CQIA.IsPrintable=1	
           
UNION ALL

SELECT DISTINCT 3
         ,StandardChargesAdditonalFees =(CASE 
				WHEN  ISNULL(CQIA.CQInvoiceFeeDescription,'') <> ''
					THEN CASE 
							WHEN CQIA.IsPrintable=1 
								THEN CQIA.CQInvoiceFeeDescription + ' (' + ISNULL(CONVERT(VARCHAR(30), CASE 
											WHEN CQIA.IsPrintable=1 
												THEN CQIA.Quantity
											ELSE 0
											END),'') + ' HRS' + ')'
							ELSE 'uncheck'
							END
				ELSE NULL END)
        --- ,StandardChargesAdditonalFees =CQIA.CQInvoiceFeeDescription 
		,InvoicedAmount =CASE WHEN CQIA.IsPrintable=1 THEN CQIA.InvoiceAmt ELSE NULL END
		,InvoiceTotal =CQIS.InvoiceTotal
		 FROM CQInvoiceMain CQIM 
		 INNER JOIN CQMain CM ON CQIM.CQMainID=CM.CQMainID AND CM.IsDeleted=0
		 INNER JOIN CQFile CQF ON CQF.CQFileID = CM.CQFileID
		 INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
         LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
         WHERE CM.CQFileID=CONVERT(BIGINT,@CQFileID)
           AND  CM.QuoteNUM=@QuoteNUM
           AND OrderNUM =3	
           AND CQIA.IsPrintable=1	
		) INVOICE
		
WHERE InfoDesc<>'uncheck'
ORDER BY INVOICE.RowID
END
	--EXEC spGetReportCQInvoiceSub1Information 10099472,1	
	--EXEC spGetReportCQInvoiceSub1Information '100023847',1	




GO


