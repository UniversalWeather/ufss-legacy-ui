IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogInformationHdr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogInformationHdr]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetReportPOSTFlightLogInformationHdr]     
  (  
  @UserCD AS VARCHAR(30),  
  @LogID BIGINT  
  )  
  As  
   
Begin    
     
-- ===============================================================================
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
     
  Declare @Info1 varchar(100)  
  Declare @Info2 varchar(100)   
  Declare @HoursMinutesCONV Int -- 1 standard, 2 flightpak , 3 other  
  Set @HoursMinutesCONV=(Select Company.HoursMinutesCONV From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  
                        And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))    
  IF @HoursMinutesCONV=1   
  Begin  
     Set @Info1 = '0  -  5 = .0# 6  - 11 = .1#12 - 17 = .2#18 - 23 = .3#24 - 29 =.4'   
     Set @Info2 = '30 - 35 = .5#36  - 41 = .6#42 - 47 = .7#48 - 53 = .8#54 - 59 =.9'   
  End  
  Else IF @HoursMinutesCONV=2   
  Begin  
     Set @Info1 = '3  -  8 = .1# 9  - 14 = .2#15 - 21 = .3#22 - 27 = .4#28 - 32 =.5'   
     Set @Info2 = '33 - 39 = .6#40  - 44 = .7#45 - 50 = .8#51 - 57 = .9#58 -  2 =.0'    
  End  
  Else IF @HoursMinutesCONV=3   
  Begin  
     Set @Info1 = ' 0 -  0 = .0# 0  -  0 = .0# 0 -  0 = .0# 0 -  0 = .0#28 -  0 =.0'   
     Set @Info2 = ' 0 -  0 = .0# 0  -  0 = .0# 0 -  0 = .0# 0 -  0 = .0#28 -  0 =.0'   
  End  
           
 Create Table #Tmp (LogNum BigInt,TailNum Varchar(10),AircraftCD Varchar(30),MDesc Varchar(100),Name Varchar(200),PhoneNum Varchar(100),SchDate DateTime, Dates Varchar(500))  
   
 Insert Into #Tmp SELECT Distinct PostflightMain.LogNum,  
        Fleet.TailNum,  
        Aircraft.AircraftCD,  
        PostflightMain.POMainDescription,  
        PostflightMain.RequestorName As Name,  
        Passenger.PhoneNum,  
        Convert(Date,PostflightLeg.ScheduledTM) As Dt,  
        '' as Dates   
        From PostflightMain INNER JOIN       
                PostflightLeg ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightLeg.IsDeleted=0 INNER JOIN      
                Fleet ON Fleet.FleetID=PostflightmAIN.FleetID INNER JOIN      
                PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN  
                Crew ON PostflightCrew.CrewID = Crew.CrewID Left Outer Join       
                PostflightPassenger ON PostflightPassenger.POLegID = PostflightLeg.POLegID LEFT OUTER JOIN       
                Passenger ON PostflightMain.PassengerRequestorID = Passenger.PassengerRequestorID LEFT OUTER JOIN     
                Aircraft ON PostflightMain.AircraftID=Aircraft.AircraftID    
         Where  PostflightMain.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)  
                AND PostflightLeg.POLogID=@LogID  
                AND PostflightMain.IsDeleted=0 
                  
       Declare @RowID Int           
       Declare @Dts Date      
       Declare @Dats varchar(500)=''     
       Select Distinct SchDate Into #Tmp2 from #Tmp  
       
       Select Distinct SchDate, ROW_NUMBER() OVER (ORDER BY SchDate) AS num Into #Tmp1 from #Tmp2     
     
       WHILE ( SELECT  COUNT(*) FROM #Tmp1 ) > 0               
       BEGIN     
          Select Top 1 @RowID=num,@Dts=Convert(Date,SchDate) From #Tmp1      
             
          IF @Dats=''  
           Begin  
            Set @Dats=Convert(Varchar,@Dts)  
           End   
          Else    
           Begin  
            Set @Dats=@Dats + ' ' + Convert(Varchar,@Dts)   
           End  
             
          Delete From #Tmp1 Where num=@RowID
       END   
          
       Update #Tmp Set Dates=@Dats             
         
       Select Top 1 LogNum,AircraftCD,TailNum,MDesc,PhoneNum,Name,Dates,@Info1 Info1,@Info2 Info2 From #Tmp      
    

    IF OBJECT_ID('tempdb..#Tmp') is not null                        
     DROP table #Tmp              
    IF OBJECT_ID('tempdb..#Tmp1') is not null                        
     DROP table #Tmp1     
    IF OBJECT_ID('tempdb..#Tmp2') is not null                        
     DROP table #Tmp2       
   
END  
  
--EXEC spGetReportPOSTFlightLogInformationHdr 'supervisor_99',10099470

GO


