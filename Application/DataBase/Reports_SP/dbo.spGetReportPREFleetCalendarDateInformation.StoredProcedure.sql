IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetCalendarDateInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetCalendarDateInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREFleetCalendarDateInformation]
	@UserCD VARCHAR(30)
	,--Mandatory
	@DATEFROM DATETIME
	,--Mandatory
	@DATETO DATETIME
	,--Mandatory
	@TailNum VARCHAR(500) = ''
	,--[Optional], Comma delimited string with mutiple values
	@FleetGroupCD VARCHAR(500) = ''
	,--[Optional], Comma delimited string with mutiple values
	@AircraftCD VARCHAR(500) = ''
	,--[Optional], Comma delimited string with mutiple values
	@IsHomeBase BIT = 0,
	@HomeBaseCD VARCHAR(500) = ''
	
	--@SortBy CHAR(10) = 'TAILNUMBER'
AS

-- ===========================================================
--	SPC Name: spGetReportPREFleetCalendarDateInformation
-- Author: AISHWARYA.M
-- Create date: 10 Jul 2012
-- Description: Get Fleet Calender Date information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 
-- ============================================================
SET NOCOUNT ON 



DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@Customer 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))


DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)
DECLARE @Loopdate DATETIME

 DECLARE @TenToMin INT
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------
---To Get Tail for each date
DECLARE @HomeBase VARCHAR(4);
SELECT @HomeBase=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=@CUSTOMER

CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,FleetID BIGINT
	,TailNum VARCHAR(25)
	,AircraftID BIGINT
	,[AC_CODE] CHAR(4)
	)
	
	
----To get the Fleet Calendar Information for each date
	CREATE TABLE #TempTable
		(
		ID INT identity(1, 1) NOT NULL
		,[CustomerID] BIGINT
		,[TailNum] VARCHAR(9)
		,[FileNum] VARCHAR(30)
		,DepartureDTTMLocal DATETIME
		,ArrivalDTTMLocal DATETIME
		,DepartICAOID CHAR(4)
		,ArriveICAOID CHAR(4)
		,[Duty] CHAR(2)
		,[Description] VARCHAR(40)
		,[cat] CHAR(4)
		,[Rqstr] VARCHAR(500)
		,[Pax] INT
		,[Pic] VARCHAR(500)
		,[Sic] VARCHAR(500)
		,[Crew] VARCHAR(500)
		,[Ete] NUMERIC(7, 3)
		,[Date] DATE
		,[NextLocalDTTM] DATETIME
		,FleetID BIGINT
		,TripID BIGINT
		,LegID BIGINT
		,[AC_CODE] CHAR(4)
		,IsApproxTM BIT
		,RecordType CHAR(1)
		,LegNum BIGINT
		)



		

	
BEGIN
SET @SQLSCRIPT =' 
			 INSERT INTO #TempTailNum (FleetID,TailNum, AircraftID, [AC_CODE])
			SELECT DISTINCT F.FleetID,F.TailNum, F.AircraftID, F.AircraftCD
			FROM Fleet F 
			LEFT OUTER JOIN Company C ON C.HomebaseID=F.HomebaseID
			LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
			LEFT OUTER JOIN Aircraft  AT ON AT.AircraftID=F.AircraftID
			INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=F.HomebaseID AND UM.CustomerID=F.CustomerID AND UM.UserName= @UserCD
			WHERE  F.CustomerID = @CUSTOMER'
			
				
		IF @IsHomeBase = 0 BEGIN
			IF @HomeBaseCD <> '' BEGIN
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBaseCD, 1) = ',' THEN LEFT(@HomeBaseCD, LEN(@HomeBaseCD) - 1) ELSE @HomeBaseCD END, ',', ''', ''') + ''')';
			END	
		END
		IF @AircraftCD <> ''
		BEGIN
		    SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
		 END
		
		SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY F.AircraftCD '
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500),@CUSTOMER AS BIGINT'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD,@CUSTOMER 
	END
	


INSERT INTO #TempTable
			(
			 [Date]
			,[TailNum]
			,[FileNum]
			,DepartureDTTMLocal
			,ArrivalDTTMLocal
			,DepartICAOID 
			,ArriveICAOID 
			,[Duty]
			,[Description]
			,[cat] 
			,[Rqstr] 
			,[Pax]  
			,[Ete]
			,[NextLocalDTTM] 
			,CustomerID
			,FleetID 
			,TripID 
			,LegID 
			,[AC_CODE]
			,IsApproxTM 
			,RecordType
			,Pic
			,Sic
			,Crew
			,LegNum
			)
				
		SELECT DISTINCT CONVERT(DATE,PT.DepartureDTTMLocal)
			,PT.TailNum 
			,CONVERT(VARCHAR,PT.TripNUM)
			,PT.DepartureDTTMLocal
			,CASE WHEN CONVERT(DATE,PT.DepartureDTTMLocal)=CONVERT(DATE,PT.ArrivalDTTMLocal)  THEN PT.ArrivalDTTMLocal ELSE 
			(CASE WHEN RecordType='M' THEN  (CONVERT(DATETIME, CONVERT(VARCHAR(20),PT.DepartureDTTMLocal, 101) + ' 23:59')) ELSE  PT.ArrivalDTTMLocal END) END
			,PT.DepartICAOID 
			,PT.ArriveICAOID
			,CASE WHEN RecordType='T' AND TripStatus = 'T' THEN 'F'
				  WHEN RecordType='T' AND TripStatus = 'H' THEN 'H' ELSE PT.DutyType END
			,PT.TripDescription 
			,PT.FlightCatagoryCD 
			,PT.RequestorName 
			,CASE WHEN RecordType='M' THEN NULL ELSE PT.PassengerTotal END 
			,PT.ElapseTM 
			,PT.NextLocalDTTM 
			,PT.CustomerID 
			,PT.FleetID
			,PT.TripID
			,PT.LegID
			,PT.Ac_Code
			,PT.IsApproxTM
			,PT.RecordType
			,PIC.CrewCD
			,SIC.CrewCD
			,SUBSTRING(ADDL.CrewList,1,LEN(ADDL.CrewList)-1)
			,LegNUM
		FROM (SELECT * FROM [dbo].[vPreflightTrans]) PT
		INNER JOIN #TempFleetID TF ON TF.FleetID=PT.FleetID
		--INNER JOIN #TempTailNum TT ON TT.FleetID=PT.FleetID
		LEFT OUTER JOIN (SELECT  PCL.LegID,CrewCD,ROW_NUMBER()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID) Rank
		                                  FROM PreflightCrewList PCL INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID AND PCL.DutyTYPE = 'P')PIC ON PIC.LegID=PT.LegID AND Rank=1
		LEFT OUTER JOIN (SELECT  PCL.LegID,CrewCD,ROW_NUMBER()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID) Rank
		                                  FROM PreflightCrewList PCL INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID AND PCL.DutyTYPE = 'S')SIC ON SIC.LegID=PT.LegID AND SIC.Rank=1
		LEFT OUTER JOIN (SELECT DISTINCT PC2.tripid, CrewList = (SELECT DISTINCT ISNULL(RTRIM(C.CrewCD) + ',','') 
													 FROM PreflightCrewList PC1 
													 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
													 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
													 WHERE Pl.TRIPID = PC2.tripid 
													  AND PC1.DutyTYPE NOT IN ('P','S')
													 FOR XML PATH('')  
			                                        )           
			      FROM PreflightLeg PC2 )ADDL ON ADDL.TripID=PT.TripID
		WHERE PT.CustomerID=@CUSTOMER
		  AND PT.TripStatus IN ('T','H')
          AND CONVERT(DATE,PT.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
		  AND PT.RecordType='T'
		  AND (PT.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)


INSERT INTO #TempTable
			(
			 [Date]
			,[TailNum]
			,[FileNum]
			,DepartureDTTMLocal
			,ArrivalDTTMLocal
			,DepartICAOID 
			,ArriveICAOID 
			,[Duty]
			,[Description]
			,[cat] 
			,[Rqstr] 
			,[Pax]  
			,[Ete]
			,[NextLocalDTTM] 
			,CustomerID
			,FleetID 
			,TripID 
			,LegID 
			,[AC_CODE]
			,IsApproxTM 
			,RecordType
			,Pic
			,Sic
			,Crew
			,LegNum
			)
				
		SELECT DISTINCT CONVERT(DATE,PT.DepartureDTTMLocal)
			,PT.TailNum 
			,NULL
			,PT.DepartureDTTMLocal
			,CASE WHEN CONVERT(DATE,PT.DepartureDTTMLocal)=CONVERT(DATE,PT.ArrivalDTTMLocal)  THEN PT.ArrivalDTTMLocal ELSE 
			(CASE WHEN RecordType='M' THEN  (CONVERT(DATETIME, CONVERT(VARCHAR(20),PT.DepartureDTTMLocal, 101) + ' 23:59')) ELSE  PT.ArrivalDTTMLocal END) END
			,PT.DepartICAOID 
			,PT.ArriveICAOID
			,CASE WHEN RecordType='T' AND TripStatus = 'T' THEN 'F'
				  WHEN RecordType='T' AND TripStatus = 'H' THEN 'H' ELSE PT.DutyType END
			,PT.TripDescription 
			,PT.FlightCatagoryCD 
			,PT.RequestorName 
			,CASE WHEN RecordType='M' THEN NULL ELSE PT.PassengerTotal END 
			,PT.ElapseTM 
			,PT.NextLocalDTTM 
			,PT.CustomerID 
			,PT.FleetID
			,PT.TripID
			,PT.LegID
			,PT.Ac_Code
			,PT.IsApproxTM
			,PT.RecordType
			,PIC.CrewCD
			,SIC.CrewCD
			,SUBSTRING(ADDL.CrewList,1,LEN(ADDL.CrewList)-1)
			,LegNUM
		FROM (SELECT * FROM [dbo].[vPreflightTrans]) PT
		INNER JOIN #TempFleetID TF ON TF.FleetID=PT.FleetID
		--INNER JOIN #TempTailNum TT ON TT.FleetID=PT.FleetID
		LEFT OUTER JOIN (SELECT  PCL.LegID,CrewCD,ROW_NUMBER()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID) Rank
		                                  FROM PreflightCrewList PCL INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID AND PCL.DutyTYPE = 'P')PIC ON PIC.LegID=PT.LegID AND Rank=1
		LEFT OUTER JOIN (SELECT  PCL.LegID,CrewCD,ROW_NUMBER()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID) Rank
		                                  FROM PreflightCrewList PCL INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID AND PCL.DutyTYPE = 'S')SIC ON SIC.LegID=PT.LegID AND SIC.Rank=1
		LEFT OUTER JOIN (SELECT DISTINCT PC2.tripid, CrewList = (SELECT DISTINCT ISNULL(RTRIM(C.CrewCD) + ',','') 
													 FROM PreflightCrewList PC1 
													 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
													 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
													 WHERE Pl.TRIPID = PC2.tripid 
													  AND PC1.DutyTYPE NOT IN ('P','S')
													 FOR XML PATH('')  
			                                        )           
			      FROM PreflightLeg PC2 )ADDL ON ADDL.TripID=PT.TripID
		WHERE PT.CustomerID=@CUSTOMER
          AND (	
					(CONVERT(DATE,PT.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PT.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PT.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PT.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PT.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PT.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
			 )	
			 AND PT.RecordType='M'
			 AND (PT.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)



DECLARE @CurDate TABLE(DateWeek DATE,TailNum VARCHAR(9),FleetID BIGINT,AC_CODE VARCHAR(4))
INSERT INTO #TempTailNum (FleetID,TailNum, [AC_CODE])
SELECT FleetID,TailNum,AC_CODE FROM #TempTable WHERE FleetID NOT IN (SELECT ISNULL(FleetID,0) FROM #TempTailNum)

INSERT INTO @CurDate(TailNum,DateWeek,FleetID,AC_CODE)
SELECT TT.TailNum,T.date,TT.FleetID,TT.AC_CODE FROM #TempTailNum TT CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@DATEFROM,@DATETO)) T
	
INSERT INTO #TempTable( [Date]
					,[TailNum]
					,[FileNum]
					,DepartureDTTMLocal
					,ArrivalDTTMLocal
					,DepartICAOID 
					,ArriveICAOID 
					,[Duty]
					,[Description]
					,[cat] 
					,[Rqstr] 
					,[Pax]  
					,[Ete]
					,[NextLocalDTTM] 
					,CustomerID
					,FleetID 
					,TripID 
					,LegID 
					,[AC_CODE]
					,IsApproxTM 
					,RecordType
					,Pic
					,Sic
					,Crew
					,LegNum)
SELECT CONVERT(DATETIME,CONVERT(DATE,PL.DepartureDTTMLocal)),T.TailNum,T.FileNum,PL.DepartureDTTMLocal,
       PL.ArrivalDTTMLocal,AD.IcaoID,AA.IcaoID,T.Duty,T.Description,T.cat,T.Rqstr,PL.PassengerTotal,
       PL.ElapseTM,PL.NextLocalDTTM,T.CustomerID,T.FleetID,T.TripID,PL.LegID,T.AC_CODE,PL.IsApproxTM,T.RecordType,
       T.Pic,T.Sic,T.Crew,T.LegNum
                                             FROM #TempTable T 
                                             INNER JOIN PreflightMain PM ON T.TripID=PM.TripID AND PM.IsDeleted = 0
                                             INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                             LEFT OUTER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID
                                             LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID=AA.AirportID
                                             WHERE PL.LegNUM=T.LegNum-1
                                               AND T.RecordType='T'
 
 INSERT INTO #TempTable(Date
                       ,TailNum
                       ,FleetID
                       ,FileNum
                       ,DepartureDTTMLocal
                       ,ArrivalDTTMLocal
                       ,DepartICAOID
                       ,ArriveICAOID
                       ,Duty
                       ,Description
                       ,Rqstr
                       ,NextLocalDTTM
                       ,AC_CODE
                       ,IsApproxTM
                       ,RecordType
                       )
SELECT DISTINCT CD.DateWeek
					,TT.TailNum
					,TT.FleetID
					,TT.FileNum
					,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00')) END
					,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END
					,TT.ArriveICAOID
					,TT.ArriveICAOID
					,'R' 
					,TT.Description
					,TT.Rqstr
				   ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END
				   ,TT.AC_CODE
				   ,TT.IsApproxTM
				   ,TT.RecordType
                    FROM #TempTable TT
                    LEFT OUTER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
         -- Removed on 31 May 2013 by JAY(Bringing only Full 'REST' records)
         --WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,TT.ArrivalDTTMLocal)
         WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,TT.ArrivalDTTMLocal)
           AND CONVERT(DATE,CD.DateWeek) <  CONVERT(DATE,TT.NextLocalDTTM) 
           AND RecordType='T'

 INSERT INTO #TempTable(Date
                       ,TailNum
                       ,FleetID
                       ,FileNum
                       ,DepartureDTTMLocal
                       ,ArrivalDTTMLocal
                       ,DepartICAOID
                       ,ArriveICAOID
                       ,Duty
                       ,Description
                       ,Rqstr
                       ,NextLocalDTTM
                       ,AC_CODE
                       ,IsApproxTM
                       ,RecordType)
SELECT DISTINCT CD.DateWeek
					,TT.TailNum
					,TT.FleetID
					,TT.FileNum
					,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 00:00')) END
					,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END
					,TT.DepartICAOID
					,TT.ArriveICAOID
					,Duty 
					,TT.Description
					,TT.Rqstr
				    ,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END
				    ,TT.AC_CODE
				    ,TT.IsApproxTM
				    ,TT.RecordType
                    FROM #TempTable TT
                    LEFT OUTER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
         WHERE  CONVERT(DATE,CD.DateWeek) >(CASE WHEN RecordType='M' THEN  CONVERT(DATE,TT.DepartureDTTMLocal) ELSE  CONVERT(DATE,TT.ArrivalDTTMLocal) END)
           AND CONVERT(DATE,CD.DateWeek) <  CONVERT(DATE,TT.NextLocalDTTM) 
           AND RecordType='M'





IF @SuppressActivityAircft=0

BEGIN
			
INSERT INTO #TempTable(TailNum,Date,AC_CODE,FleetID,FileNum)
SELECT T1.TailNum,T1.DateWeek,T1.AC_CODE,T1.FleetID,'' FROM @CurDate T1
                     -- OUTER APPLY dbo.GetFleetCurrentAirpotCD(T1.DateWeek, T1.FleetID) AS A
                      WHERE NOT EXISTS(SELECT T.TailNum FROM #TempTable T WHERE T.FleetID = T1.FleetID AND CONVERT(DATE,T.[Date]) = CONVERT(DATE,T1.DateWeek))

END

DECLARE @HomeBaseTable TABLE(FleetID BIGINT,
                             ArrivalDTTMLocal DATE,
                             IcaoID CHAR(4),
                             Rnk INT)

INSERT INTO @HomeBaseTable
SELECT PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ArrivalDTTMLocal,A.IcaoID,
	ROW_NUMBER()OVER(PARTITION BY PM.FleetID,CONVERT(DATE,ArrivalDTTMLocal) ORDER BY PM.FleetID,ArrivalDTTMLocal DESC) Rnk
		FROM PreflightLeg PL
		INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted = 0
		INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
		INNER JOIN #TempFleetID TF ON PM.FleetID=TF.FleetID
		WHERE PM.TRIPSTATUS IN ('T','H')  --ISNULL(PM.TripStatus,'') <> 'X'
		  AND PL.ArrivalDTTMLocal<=@DATETO
		  AND PL.IsDeleted = 0
		ORDER BY ArrivalDTTMLocal DESC


				
UPDATE XX SET FileNum=T.IcaoID
		   FROM #TempTable XX
			INNER JOIN @HomeBaseTable T ON T.FleetID=XX.FleetID AND Rnk = 1
           WHERE T.FleetID=XX.FleetID 
            AND  T.ArrivalDTTMLocal <=CONVERT(DATE,XX.Date)
            AND ISNULL(FileNum,'') = ''

 UPDATE XX SET FileNum=IcaoID
		   FROM #TempTable XX
			INNER JOIN (SELECT A.IcaoID,FleetID FROM FLEET F 
		                                 INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		                                 INNER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
		                                 WHERE F.CustomerID = @CUSTOMER)HB ON HB.FleetID=XX.FleetID
							WHERE HB.FleetID=XX.FleetID 
							 AND ISNULL(FileNum,'') = ''

DECLARE  @TempTable TABLE   (
		 ID INT IDENTITY(1, 1) NOT NULL
		,TailNum VARCHAR(9)
		,[Date] DATE
		,[FileNum] VARCHAR(30)
		,DepartureDTTMLocal DATETIME
		,ArrivalDTTMLocal DATETIME
		,DepartICAOID CHAR(4)
		,ArriveICAOID CHAR(4)
		,[Duty] CHAR(2)
		,[Description] VARCHAR(40)
		,[cat] CHAR(4)
		,[Rqstr] VARCHAR(500)
		,[Pax] INT
		,[Pic] VARCHAR(500)
		,[Sic] VARCHAR(500)
		,[Crew] VARCHAR(500)
		,[Ete] NUMERIC(7, 3)
		,AC_CODE VARCHAR(4)
		,FleetID BIGINT
		
		)   						

INSERT INTO @TempTable
SELECT TailNum = T.TailNum
			,[Date] 
			,FileNum
			,DepartureDTTMLocal
			,ArrivalDTTMLocal 
			,DepartICAOID 
			,ArriveICAOID 
			,Duty
			,[Description]
			,cat 
			,Rqstr 
			,Pax 
			,Pic 
			,Sic 
			,Crew 
			,Ete
			,[AC_CODE]
			,T.FleetID FROM #TempTable T
         WHERE Date BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
        --AND  TailNum='N794MH' 
         ORDER BY [AC_CODE],[Date],[TailNum],DepartureDTTMLocal 



		
SELECT DISTINCT TailNum = T.TailNum
			,[Date] = dbo.GetShortDateFormatByUserCD(@UserCD,T.[Date])
			,[SortDate] = T.[Date]
			--,[DateName] = SUBSTRING(datename(dw,T.[Date]),1,3)
			,FileNum
			,DepartureDTTMLocal
			,ArrivalDTTMLocal 
			,DepartICAOID 
			,ArriveICAOID 
			,Duty
			,[Description]
			,cat 
			,Rqstr 
			,Pax 
			,Pic 
			,Sic 
			,Crew 
			,Ete
			,[AC_CODE] 
			,[DateName] = SUBSTRING(datename(dw,T.[Date]),1,3)
			,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
			, @TenToMin TenToMin
           FROM @TempTable T
           WHERE Date BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
        --AND  TailNum='N794MH' 
         ORDER BY [AC_CODE],[SortDate],[TailNum],DepartureDTTMLocal 


IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID
IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE #TempTailNum
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable
IF OBJECT_ID('tempdb..#TblDates') IS NOT NULL
DROP TABLE #TblDates
	
	--EXEC spGetReportPREFleetCalendarDateInformation 'jwilliams_11', '2012/09/01', '2012/09/07', ', ', ', 0, '
	--EXEC spGetReportPREFleetCalendarDateInformation 'jwilliams_11', '2012-07-15', '2012-08-15', ', ', ', 0, '
	--EXEC spGetReportPREFleetCalendarDateInformation 'JWILLIAMS_11', '2012-07-31', '2012-11-22', 'n46f', ', 'cl604'
	--EXEC spGetReportPREFleetCalendarDateInformation 'JWILLIAMS_13', '2012-07-31', '2012-11-22', 'N46f', ', 'cl604'




GO


