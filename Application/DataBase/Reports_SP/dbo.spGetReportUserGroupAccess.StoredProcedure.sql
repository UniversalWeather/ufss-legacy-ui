IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportUserGroupAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportUserGroupAccess]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetReportUserGroupAccess]
   @UserCD AS VARCHAR(30),
   @UserGroupCD VARCHAR(100)
AS
BEGIN
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SET NOCOUNT ON;

SELECT UG.UserGroupCD UsergroupCode, 
       UG.UserGroupDescription GroupDescription,
       M.ModuleName AS [Module], 
       UM.UMPermissionName as [Form],
       UP.CanView AS [View],
       UP.CanAdd AS [Add],
       UP.CanEdit AS [Edit] ,
       UP.CanDelete AS [Delete],
	   CASE M.ModuleName          
 WHEN 'Database' THEN 1          
 WHEN 'Preflight' THEN 2          
 WHEN 'PostFlight' THEN 3     
 WHEN 'Charter Quote' THEN 4     
 WHEN 'Corporate Request' THEN 5           
 WHEN 'Database Reports' THEN 6           
 WHEN 'Preflight Reports' THEN 7           
 WHEN 'PostFlight Reports' THEN 8   
 WHEN 'Postflight Custom Reports' THEN 9
 WHEN 'Charter Quote Reports' THEN 10     
 WHEN 'Corporate Request Reports' THEN 11
 WHEN 'Tripsheet Reports' THEN 12
 WHEN 'Tripsheet RW Reports' THEN 13
 WHEN 'Report Writer' THEN 14
 WHEN 'Menu' THEN 15
 WHEN 'Favorites' THEN 16
 WHEN 'Scheduled Service' THEN 17
 WHEN 'Security Administration' THEN 18
 WHEN 'System Administration' THEN 19      
 WHEN 'Utility' THEN 20
 WHEN 'Utility Reports' THEN 21
 WHEN 'Universal' THEN 22 END AS SortOrder                         
FROM UserGroupPermission UP 
     INNER JOIN UserGroup UG ON UP.UserGroupID = UG.UserGroupID AND UP.CustomerID = UG.CustomerID
     INNER JOIN UMPermission UM ON UP.UMPermissionID = UM.UMPermissionID
     INNER JOIN Module M ON UM.ModuleID = M.ModuleID
    WHERE UP.CustomerID = @CUSTOMERID 
    AND UG.UserGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@UserGroupCD, ','))
ORDER BY UG.UserGroupCD, SortOrder, UG.UserGroupDescription, M.ModuleName
END
     
GO


