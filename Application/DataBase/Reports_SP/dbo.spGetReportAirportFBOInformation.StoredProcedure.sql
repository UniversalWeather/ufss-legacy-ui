IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportFBOInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportFBOInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportAirportFBOInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =============================================
-- SPC Name: spGetReportAirportFBOInformation
-- Author: SUDHAKAR J
-- Create date: 12 Jun 2012
-- Description: Get Airport FBO Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT
		F.CustomerID, F.IsChoice, F.FBOVendor, F.PhoneNUM1, F.Frequency, F.IsUWAAirPartner
		,F.FuelBrand, F.IsCrewCar
	FROM FBO F
	INNER JOIN Airport A ON F.AirportID = A.AirportID
	WHERE F.IsDeleted = 0
	    AND F.IsInActive = 0 
		AND F.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
		AND A.IcaoID = @IcaoID 
		
	ORDER BY F.FBOVendor

  -- EXEC spGetReportAirportFBOInformation 'jwilliams_11', 'KDAL'


GO


