IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTripSheetReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTripSheetReportDetail]
GO


SET ANSI_NULLS ON
GO


SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetTripSheetReportDetail]
(@CustomerID bigint)
AS BEGIN

	SELECT TripSheetReportDetailID,
			CustomerID,
			TripSheetReportHeaderID,
			ReportNumber,
			ReportDescription,
			IsSelected,
			Copies,
			ReportProcedure,
			ParameterVariable,
			ParameterDescription,
			ParameterType,
			ParameterWidth,
			ParameterCValue,
			ParameterLValue,
			ParameterNValue,
			ParameterValue,
			ReportOrder,
			TripSheetOrder,
			LastUpdUID,
			LastUpdTS,
			IsDeleted,
			ParameterSize,
			WarningMessage
		FROM TripSheetReportDetail
	WHERE CustomerID = @CustomerID AND ISNULL(IsDeleted,0)=0

END

GO


