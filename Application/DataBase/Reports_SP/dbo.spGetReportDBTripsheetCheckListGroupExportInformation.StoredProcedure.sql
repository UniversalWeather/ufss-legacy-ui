IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBTripsheetCheckListGroupExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBTripsheetCheckListGroupExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE  [dbo].[spGetReportDBTripsheetCheckListGroupExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30) --Mandatory
)
AS

-- ===============================================================================
-- SPC Name: [spGetReportDBTripsheetCheckListGroupExportInformation]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get Payment Types
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT GroupCode = CheckGroupCD
	 ,Groupdesc = CheckGroupDescription
FROM TripManagerCheckListGroup
WHERE CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
AND CustomerID = CONVERT(BIGINT, @UserCustomerID)
-- And IsInActive=0
And IsDeleted=0

--EXEC spGetReportDBTripsheetCheckListGroupExportInformation 'SUPERVISOR_99','10099'





GO


