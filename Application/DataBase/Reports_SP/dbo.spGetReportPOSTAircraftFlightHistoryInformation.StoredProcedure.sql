IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftFlightHistoryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftFlightHistoryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spGetReportPOSTAircraftFlightHistoryInformation]
	    @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBaseCD As NVARCHAR(1000) = '',
		@IsHomebase As bit=0,
		@RequestorCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
		
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTAircraftFlightHistoryInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Aircraft flight history for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END

DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=0

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		
DECLARE @FLEET TABLE (FLEETID BIGINT)
INSERT INTO @FLEET SELECT fleetid FROM Fleet WHERE IsDeleted = 0 AND IsInActive = 0


-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	  AND F.IsDeleted=0
	  AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------	
	
	SELECT DISTINCT
			[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
			[TailNum]=F.TailNum,
			[LOGID]=PL.POLOGID,
			[INBO]=PL.InboundDTTM,
			[DutyTYPE]=PL.DutyTYPE,
			[Aircraft] =F.TailNum+' '+'- '+isnull(F.TypeDescription,''),
			[typecode]=f.AircraftCD,
			[Flt] = PL.LegNUM,
			[LOGNUM]=PM.LogNUM,
			[FltDate] = PL.ScheduledTM,
			[DispatchNum] = PM.DispatchNUM,
			[Passengers] = P.PassengerName,
			[Crew]=SUBSTRING(ACrew.AssociatedCrew,1,len((ACrew.AssociatedCrew))-1),
			[Itinerary] = AD.CityName ,
			[ArrItinerary]= AA.CityName,
			[RON] = FP.FlightPurposeDescription,[homebase]=pm.HomebaseID,
			[StatueMiles] = ((PL.Distance)*1.150),
			[FlightHours] = ISNULL(ROUND(PL.FlightHours,1),0),
			[BlockHours] = ISNULL(ROUND(PL.BlockHours,1),0),
			[TenToMin]=@TenToMin,
			[Pax] = APAx.AssociatedPax,--SUBSTRING(APAx.AssociatedPax,1,len((APAx.AssociatedPax))-1),
			[Charge] = D.DepartmentCD+' '+ISNULL(DA.DeptAuthDescription,''),
			[Purpose]='Purpose: '+PL.FlightPurpose,
			[RON1]=PL1.Ron1,
				[RON2]= CASE WHEN PL.IsDutyEnd=1  THEN 
			       (CASE WHEN CONVERT(DATE,PL.InboundDTTM) < CONVERT(DATE,@DATETO) THEN 
			       (CASE WHEN (PL1.Ron1>PL.LegNUM) THEN  
			       (CASE WHEN  CONVERT(DATE,PLNEXT.ScheduledTM)<= CONVERT(DATE,@DATETO) THEN 
			       (CASE WHEN  (CONVERT(DATE,PLNEXT.ScheduledTM)> CONVERT(DATE,PL.InboundDTTM)) THEN 
			       DATEDIFF(DAY,PL.InboundDTTM,PLNEXT.ScheduledTM) ---ELSE DATEDIFF(DAY,PL.InboundDTTM,@DATETO+1)
			        END)
			       ELSE (CASE WHEN CONVERT(DATE,@DATETO) > CONVERT(DATE,PL.InboundDTTM) THEN  DATEDIFF(DAY,PL.InboundDTTM,@DATETO+1) END) END)END)END) ELSE '' END, 
			
             
			[purposedesc]= PPax1.AssociatedFPURP1 --SUBSTRING(PPax1.AssociatedFPURP1,1,len((PPax1.AssociatedFPURP1))-1)
			,[ISREQUESTOR] = P.IsRequestor
			,P.PassengerRequestorCD
		INTO #FLEETINFO
		FROM PostflightMain pm 
		INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
		--LEFT OUTER JOIN PostflightMain PM ON F.FleetID = PM.FleetID
		LEFT OUTER JOIN (SELECT POLOGID,max(legnum) Ron1 FROM PostflightLeg WHERE IsDeleted=0 GROUP BY POLogID)PL1 ON PM.POLogID=PL1.POLOGID
		LEFT OUTER JOIN  Postflightleg Pl ON PM.POLogID = PL.POLogID AND Pl.IsDeleted=0
		LEFT OUTER JOIN Postflightcrew PC ON PC.POLegID = PL.POLEgID
		LEFT OUTER JOIN PostflightPassenger Pp ON Pp.POLogID = Pm.POLogID
		--LEFT OUTER JOIN PostflightMain PM ON F.FleetID = PM.FleetID
		LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID 
		LEFT OUTER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,f.TypeDescription, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      left outer JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      left outer JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                      --WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
                                      --OR @FleetGroupCD = ''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		LEFT OUTER JOIN COMPANY CP ON CP.HomebaseID = F.HomebaseID
	    LEFT OUTER JOIN Department D ON D.DepartmentID = PL.DepartmentID
	    LEFT OUTER JOIN DepartmentAuthorization DA ON PL.AuthorizationID =DA.AuthorizationID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID 
		LEFT OUTER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
		LEFT OUTER JOIN (
SELECT PL.POLogID, PL.POLegID, PL.LegNUM, PL.ScheduledTM
FROM POSTflightLeg PL WHERE PL.IsDeleted=0
) PLNEXT ON PLNEXT.POLogID = PM.POLogID AND PL.LegNUM + 1 = PLNEXT.LegNUM
		LEFT OUTER JOIN (
	    SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
		       SELECT RTRIM(C.CrewCD) + ','
				FROM PostflightCrew PC 
				INNER JOIN Crew C ON PC.CrewID = C.CrewID
				WHERE PC.POLegID = PC1.POLegID
				--order by C.CrewCD
				ORDER BY (CASE PC.DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
						)
				FOR XML PATH('')
				)
				FROM PostflightCrew PC1
		) ACrew ON PL.POLegID = ACrew.POLegID 
         LEFT OUTER JOIN (
         SELECT DISTINCT PP1.POLegID, AssociatedPax = (
		       SELECT  RTRIM(P.PassengerName) + '&'
				FROM PostflightPassenger PP 
				INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				WHERE PP.POLegID = PP1.POLegID ORDER BY OrderNUM FOR XML PATH(''))
				FROM PostflightPassenger PP1
				
         ) APAx ON Pl.POLegID = APax.POLegID
         LEFT OUTER JOIN 
         (SELECT DISTINCT PP2.POLegID,pp2.FlightPurposeID,
          AssociatedFPURP1 = (SELECT RTRIM(FP.FlightPurposeDescription)+'&' FROM PostflightPassenger PP 
                --INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
				INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				WHERE PP.POLegID = PP2.POLegID   ORDER BY OrderNUM FOR XML PATH('' ))
				FROM PostflightPassenger PP2) PPax1 ON PL.POLegID = PPax1.POLegID  
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=Pl.PassengerRequestorID
		LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID=P.FlightPurposeID
		--LEFT OUTER JOIN Passenger PPP ON PPP.PassengerRequestorID = Pl.PassengerRequestorID AND PPP.IsRequestor=1
		WHERE F.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	    AND pm.IsDeleted=0
	    AND PL.IsDeleted=0
				 --AND F.TailNum IS NOT NULL
---AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
--AND A.IcaoID IN (CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE A.IcaoID END)
AND 
	A.IcaoID IN 
	(
		SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE A.IcaoID END, ',')
	)

AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
	

IF @IsZeroSuppressActivityAircftRpt=0
BEGIN 
SELECT * FROM #FLEETINFO
UNION ALL 
		SELECT DISTINCT
			[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
			[TailNum]=F.TailNum,
			[LOGID]=NULL,
			[INBO]=NULL,
			[DutyTYPE]=NULL,
			[Aircraft] =F.TailNum+' '+'- '+isnull(F.TypeDescription,''),
			[typecode]=f.AircraftCD,
			[Flt] = NULL,
			[LOGNUM]=NULL,
			[FltDate] = NULL,
			[DispatchNum] = 'NO ACTIVITY',
			[Passengers] = null,
			[Crew]=NULL,
			[Itinerary] =NULL,[ArrItinerary]= NULL,
			[RON] = NULL,
			[homebase]=pm.HomebaseID,
			[StatueMiles] =NULL,
			[FlightHours] = NULL,
			[BlockHours] = NULL,[TenToMin]=@TenToMin,
			[Pax] = NULL,
			[Charge] = NULL,
			[Purpose]=NULL,
			[RON1]= NULL ,[RON2]= NULL ,
			[purposedesc]= NULL
			,[ISREQUESTOR] = NULL
			,NULL
		FROM fleet f 
		INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = f.FleetID
		
		INNER JOIN @FLEET Ft ON FT.FLEETID = F.FLEETID AND F.IsDeleted = 0 AND F.IsInActive = 0
		LEFT OUTER JOIN PostflightMain PM ON F.FleetID = PM.FleetID AND PM.IsDeleted=0
		LEFT OUTER JOIN PostflightLeg POL ON POL.POLogID = PM.POLogID AND POL.IsDeleted=0
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POL.PassengerRequestorID
		LEFT OUTER JOIN COMPANY CP ON CP.HomebaseID = F.HomebaseID
	  	LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
		LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID
		LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		
	  
		WHERE F.TailNum NOT IN (SELECT DISTINCT TailNum FROM #FLEETINFO)
        AND F.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
        AND F.TailNum IS NOT NULL
        
	--AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
   AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
   AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
   AND 
	A.IcaoID IN 
	(
		SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE A.IcaoID END, ',')
	)
  -- AND A.IcaoID IN (CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE A.IcaoID END) 
  --AND A.IcaoID IN(CASE(@HomeBaseCDOnly) WHEN 0 THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))ELSE @HomeBaseCD END)
  AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
  ORDER BY typecode,Aircraft,FltDate,LOGNUM,Flt
END
ELSE 
BEGIN
SELECT * FROM #FLEETINFO
ORDER BY typecode,Aircraft,FltDate,LOGNUM,Flt
END
IF OBJECT_ID('tempdb..#FLEETINFO') IS NOT NULL
DROP TABLE #FLEETINFO 	
	  -- EXEC spGetReportPOSTAircraftFlightHistoryInformation 'jwilliams_11','2011/1/1','2012/12/12', '', '', '', 0


/*

	
	SELECT DISTINCT
			[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
			[TailNum]=F.TailNum,
			[LOGID]=PL.POLOGID,
			[INBO]=PL.InboundDTTM,
			[DutyTYPE]=PL.DutyTYPE,
			[Aircraft] =F.TailNum+' '+'- '+isnull(F.TypeDescription,''),
			[typecode]=f.AircraftCD,
			[Flt] = PL.LegNUM,
			[LOGNUM]=PM.LogNUM,
			[FltDate] = PL.ScheduledTM,
			[DispatchNum] = PM.DispatchNUM,
			[Passengers] = P.PassengerName,
			[Crew]=SUBSTRING(ACrew.AssociatedCrew,1,len((ACrew.AssociatedCrew))-1),
			[Itinerary] = AD.CityName ,
			[ArrItinerary]= AA.CityName,
			[RON] = FP.FlightPurposeDescription,[homebase]=pm.HomebaseID,
			[StatueMiles] = ((PL.Distance)*1.150),
			[FlightHours] = ISNULL(PL.FlightHours,0),
			[BlockHours] = ISNULL(PL.BlockHours,0),
			[TenToMin]=@TenToMin,
			[Pax] = APAx.AssociatedPax,--SUBSTRING(APAx.AssociatedPax,1,len((APAx.AssociatedPax))-1),
			[Charge] = D.DepartmentCD+' '+ISNULL(DA.DeptAuthDescription,''),
			[Purpose]='Purpose: '+PL.FlightPurpose,
			[RON1]=PL1.Ron1,
			[RON2]= CASE WHEN PL.IsDutyEnd=1  THEN 
			       (CASE WHEN CONVERT(DATE,PL.InboundDTTM) < CONVERT(DATE,@DATETO) THEN 
			       (CASE WHEN (PL1.Ron1>PL.LegNUM) THEN  
			       (CASE WHEN  CONVERT(DATE,PLNEXT.ScheduledTM)<= CONVERT(DATE,@DATETO) THEN 
			       (CASE WHEN  (CONVERT(DATE,PLNEXT.ScheduledTM)> CONVERT(DATE,PL.InboundDTTM)) THEN 
			       DATEDIFF(DAY,PL.InboundDTTM,PLNEXT.ScheduledTM) ---ELSE DATEDIFF(DAY,PL.InboundDTTM,@DATETO+1)
			        END)
			       ELSE (CASE WHEN CONVERT(DATE,@DATETO) > CONVERT(DATE,PL.InboundDTTM) THEN  DATEDIFF(DAY,PL.InboundDTTM,@DATETO+1) END) END)END)END) ELSE '' END 
			
             
		  ,[purposedesc]= APAx.AssociatedFPURP1 --SUBSTRING(PPax1.AssociatedFPURP1,1,len((PPax1.AssociatedFPURP1))-1)
		  ,[ISREQUESTOR] = P.IsRequestor
		  ,P.PassengerRequestorCD

		FROM PostflightMain PM 
		INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
		INNER JOIN (SELECT POLOGID,MAX(legnum) Ron1 FROM PostflightLeg GROUP BY POLogID)PL1 ON PM.POLogID=PL1.POLOGID
		INNER JOIN  Postflightleg Pl ON PM.POLogID = PL.POLogID
		LEFT OUTER JOIN Postflightcrew PC ON PC.POLegID = PL.POLEgID
		LEFT OUTER JOIN PostflightPassenger PP ON Pp.POLogID = Pm.POLogID
		LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID 
		LEFT OUTER JOIN ( SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,f.TypeDescription, F.HomeBaseID,F.MaximumPassenger FROM Fleet F
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		LEFT OUTER JOIN COMPANY CP ON CP.HomebaseID = F.HomebaseID
	    LEFT OUTER JOIN Department D ON D.DepartmentID = PL.DepartmentID
	    LEFT OUTER JOIN DepartmentAuthorization DA ON PL.AuthorizationID =DA.AuthorizationID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID 
		LEFT OUTER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
		--LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
		LEFT OUTER JOIN (SELECT PL.POLogID, PL.POLegID, PL.LegNUM, PL.ScheduledTM FROM POSTflightLeg PL) PLNEXT ON PLNEXT.POLogID = PM.POLogID AND PL.LegNUM + 1 = PLNEXT.LegNUM
		LEFT OUTER JOIN ( SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
		       SELECT RTRIM(C.CrewCD) + ',' FROM PostflightCrew PC INNER JOIN Crew C ON PC.CrewID = C.CrewID
											WHERE PC.POLegID = PC1.POLegID
				                            ORDER BY C.CrewCD
				                            FOR XML PATH('')
				                                       )
			                           FROM PostflightCrew PC1
		                  ) ACrew ON PL.POLegID = ACrew.POLegID 
       LEFT OUTER JOIN (SELECT DISTINCT PP1.POLegID, AssociatedPax = (
		                     SELECT DISTINCT RTRIM(P.PassengerName) + '&' FROM PostflightPassenger PP 
									INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
									INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				          WHERE PP.POLegID = PP1.POLegID FOR XML PATH('')),AssociatedFPURP1=(SELECT DISTINCT RTRIM(FP.FlightPurposeDescription)+'&' FROM PostflightPassenger PP 
				                                                           INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = PP.FlightPurposeID
				                                                            WHERE PP.POLegID = PP1.POLegID FOR XML PATH('' )
				           )
				          FROM PostflightPassenger PP1
				
                       ) APAx ON Pl.POLegID = APax.POLegID
	 LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=Pl.PassengerRequestorID
	 LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID=P.FlightPurposeID
	 WHERE F.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	   AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @HomeBaseCDOnly = 0)
      -- AND AD.IcaoID IN (CASE WHEN @HomeBaseCDOnly = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE AD.IcaoID END)
	   AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')

*/




GO


