IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREComponentStatusExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREComponentStatusExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




									
CREATE PROCEDURE [dbo].[spGetReportPREComponentStatusExportInformation]	
    @UserCD AS VARCHAR(30) ,
    @UserCustomerID AS BIGINT,
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @TailNum AS VARCHAR(100) = ''
    
AS    
BEGIN
SET NOCOUNT ON;

--EXEC spGetReportPREComponentStatusExportInformation 'SUPERVISOR_99',10099
Declare @SuppressActivityAircft BIT  
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@UserCustomerID
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))


--SET @SuppressActivityAircft=1
DECLARE  @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		HomeBaseID BIGINT,
		TailNum VARCHAR(9)
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.HomebaseID,F.TailNum 
	FROM Fleet F
	WHERE F.CustomerID = @UserCustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID, F.HomebaseID,F.TailNum
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @UserCustomerID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.HomebaseID,F.TailNum
	FROM Fleet F 
	WHERE F.CustomerID = @UserCustomerID  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END


DECLARE @TenToMin SMALLINT = 0;

SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @AircraftBasis numeric(1,0);
	  SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	   

SELECT IDENTITY (INT, 1,1) AS RowId,T.* INTO #TEMP FROM 
(SELECT DISTINCT 
        F.TailNum TailNum,
        FC.WarningHrs,
        FC.FleetComponentDescription,
        FC.AircraftCD,
        FC.ComponentCD,
        FC.LastInspectionDT LastInspectionDT, 
        CONVERT(VARCHAR(10),FC.InspectionHrs)  AS NextDueAfter, 
CASE WHEN FC.IsHrsDaysCycles = 'H' THEN 'H'
     WHEN FC.IsHrsDaysCycles = 'D' THEN 'D'
     WHEN FC.IsHrsDaysCycles = 'C' THEN 'C' END hoursdayscycles,
CASE WHEN FC.IsHrsDaysCycles = 'H' THEN (
CASE WHEN CONVERT(DATE,PL.ScheduledTM) >  CONVERT(DATE,ISNULL(FC.LastInspectionDT,GETDATE())) AND @AircraftBasis = 1 THEN SUM(PL.BlockHours) OVER (PARTITION BY F.TailNum,fc.FleetComponentDescription)   
     WHEN CONVERT(DATE,PL.ScheduledTM) >  CONVERT(DATE,ISNULL(FC.LastInspectionDT,GETDATE())) AND @AircraftBasis = 2 THEN SUM(PL.FlightHours) OVER (PARTITION BY F.TailNum,fc.FleetComponentDescription) END)
     WHEN FC.IsHrsDaysCycles = 'D' THEN   DATEDIFF(DAY,CONVERT(DATE,FC.LastInspectionDT),Convert(DATE, GetDate())) 
     WHEN FC.IsHrsDaysCycles = 'C' then  (SL.COUNTLEGID)   End SinceLastInsp,
CASE WHEN FC.IsHrsDaysCycles = 'H' THEN (
CASE WHEN CONVERT(DATE,PL.ScheduledTM) > CONVERT(DATE,ISNULL(FC.LastInspectionDT,GETDATE()))  AND @AircraftBasis = 1 THEN ISNULL(FC.InspectionHrs,0)-ISNULL((SUM(PL.BlockHours) OVER (PARTITION BY F.TailNum,fc.FleetComponentDescription) ) ,0)  
     WHEN CONVERT(DATE,PL.ScheduledTM) >  CONVERT(DATE,ISNULL(FC.LastInspectionDT,GETDATE())) AND @AircraftBasis = 2 THEN ISNULL(FC.InspectionHrs,0)-ISNULL((SUM(PL.FlightHours) OVER (PARTITION BY F.TailNum,fc.FleetComponentDescription)),0) END)
     WHEN FC.IsHrsDaysCycles = 'D' THEN   DATEDIFF(DAY,DATEDIFF(DAY,CONVERT(DATE,FC.LastInspectionDT),CONVERT(DATE, GetDate())),FC.InspectionHrs )
     WHEN FC.IsHrsDaysCycles = 'C' THEN  ISNULL(FC.InspectionHrs,0) - ISNULL(SL.COUNTLEGID,0)  End Remaining,                                   
CASE WHEN FC.IsHrsDaysCycles = 'D' THEN DATEADD(D,FC.InspectionHrs,FC.LastInspectionDT) END AS Projection  
,PM.FleetID
,FleetComponentID

  FROM PostflightMain PM 
  JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
  LEFT JOIN Fleet F  ON PM.FleetID = F.FleetID
  JOIN (SELECT DISTINCT PM.FleetID,COUNT(PL.POLegID) AS COUNTLEGID  FROM PostflightMain PM JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
                                       JOIN Fleet F ON PM.FleetID = F.FleetID  
                                       JOIN FleetComponent FC ON FC.FleetID = F.FleetID
                                   WHERE CONVERT(DATE,PL.ScheduledTM) > FC.LastInspectionDT AND PM.CustomerID = @UserCustomerID GROUP BY PM.FleetID)SL ON F.FleetID = SL.FleetID
  JOIN FleetComponent FC ON F.FleetID = FC.FleetID
  JOIN @TempFleetID TF ON F.FleetID = TF.FleetID
  JOIN Company C ON F.HomebaseID = C.HomebaseID
  WHERE PM.CustomerID = @UserCustomerID AND PM.IsDeleted = 0--F.IsInActive = 0 
  AND CONVERT(DATE,PL.ScheduledTM) >  CONVERT(DATE,ISNULL(FC.LastInspectionDT,GETDATE()))
  )T 
  ORDER BY FleetID,FleetComponentID



DECLARE @RemHours TABLE(HoursVal NUMERIC(6,3),
                        OutboundDTTM DATE)

DECLARE @TempVal TABLE(RowID INT IDENTITY,
                       TotalHoursVal NUMERIC(6,3),
                       FleetID BIGINT,
                       FleetComponentID BIGINT,
                       DateVal DATETIME,
                       HoursCycles VARCHAR(10)
                       )

INSERT INTO @TempVal
SELECT CASE WHEN  @AircraftBasis = 1 THEN PL.BlockHours ELSE PL.FlightHours END TotalHoursVal
         ,PM.FleetID
         ,FC.FleetComponentID
         ,PL.InboundDTTM
         ,'H'
          FROM PostflightMain PM 
               INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
               INNER JOIN #TEMP T ON PM.FleetID=T.FleetID
               INNER JOIN FleetComponent FC ON FC.FleetComponentID=T.FleetComponentID
		 WHERE PM.CustomerID=@UserCustomerID
		  AND CONVERT(DATE,PL.OutboundDTTM) >  CONVERT(DATE,T.LastInspectionDT)	
		  AND T.Remaining < 0
		  AND LTRIM(RTRIM(hoursdayscycles))='H'
		  AND PM.IsDeleted = 0
		 ORDER BY PL.OutboundDTTM
		 
INSERT INTO @TempVal
SELECT CASE WHEN  @AircraftBasis = 1 THEN PL.BlockHours ELSE PL.FlightHours END TotalHoursVal
         ,PM.FleetID
         ,FC.FleetComponentID
         ,PL.InboundDTTM
         ,'H'
          FROM PostflightMain PM 
               INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
               INNER JOIN #TEMP T ON PM.FleetID=T.FleetID
               INNER JOIN FleetComponent FC ON FC.FleetComponentID=T.FleetComponentID
		 WHERE PM.CustomerID=@UserCustomerID
		  AND T.NextDueAfter = 0
		  AND LTRIM(RTRIM(hoursdayscycles))='H'
		  AND PM.IsDeleted = 0
		 ORDER BY PL.OutboundDTTM
		  
		  



INSERT INTO @TempVal
SELECT CASE WHEN PM.LogNum=0 THEN 0 ELSE  1 END TotalHoursVal
         ,PM.FleetID
         ,FC.FleetComponentID
         ,MAX(PL.InboundDTTM)
         ,'C'
          FROM PostflightMain PM 
               INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
               INNER JOIN #TEMP T ON PM.FleetID=T.FleetID
               INNER JOIN FleetComponent FC ON FC.FleetComponentID=T.FleetComponentID
		 WHERE PM.CustomerID=@UserCustomerID
		  AND CONVERT(DATE,PL.OutboundDTTM) >  CONVERT(DATE,T.LastInspectionDT)	
		  AND T.Remaining < 0
		  AND LTRIM(RTRIM(hoursdayscycles))='C'
		  AND PM.IsDeleted = 0
		  GROUP BY PM.FleetID,FC.FleetComponentID,PM.LogNum
		  
INSERT INTO @TempVal
SELECT CASE WHEN PM.LogNum=0 THEN 0 ELSE  1 END TotalHoursVal
         ,PM.FleetID
         ,FC.FleetComponentID
         ,MAX(PL.InboundDTTM)
         ,'C'
          FROM PostflightMain PM 
               INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
               INNER JOIN #TEMP T ON PM.FleetID=T.FleetID
               INNER JOIN FleetComponent FC ON FC.FleetComponentID=T.FleetComponentID
		 WHERE PM.CustomerID=@UserCustomerID
		  AND CONVERT(DATE,PL.OutboundDTTM) >  CONVERT(DATE,T.LastInspectionDT)	
		  AND T.NextDueAfter = 0
		  AND LTRIM(RTRIM(hoursdayscycles))='C'
		  AND PM.IsDeleted = 0
		  GROUP BY PM.FleetID,FC.FleetComponentID,PM.LogNum







UPDATE #TEMP SET Projection=TT.DateVal FROM( 
SELECT t1.RowID, t1.TotalHoursVal, SUM(t2.TotalHoursVal) as TotalVal,t1.FleetID,t1.FleetComponentID
      ,ROW_NUMBER()OVER(PARTITION BY t1.FleetID,t1.FleetComponentID ORDER BY t1.FleetID,t1.FleetComponentID,T1.RowID)Rnk
      ,t1.DateVal
		FROM @TempVal t1
				INNER JOIN @TempVal t2 on t1.RowID >= t2.RowID AND t1.FleetID=t2.FleetID AND t1.FleetComponentID=t2.FleetComponentID
				INNER JOIN #TEMP T ON t1.FleetID=T.FleetID AND t1.FleetComponentID=T.FleetComponentID
				GROUP BY t1.RowID, t1.TotalHoursVal,t1.FleetID,t1.FleetComponentID,T.NextDueAfter,t1.DateVal
				HAVING SUM(t2.TotalHoursVal) >= T.NextDueAfter
       )TT
        WHERE TT.FleetComponentID=#TEMP.FleetComponentID
          AND TT.FleetID=#TEMP.FleetID
          AND TT.Rnk=1
          AND #TEMP.Remaining <= 0
          AND LTRIM(RTRIM(hoursdayscycles))='H' 


UPDATE #TEMP SET Projection=TT.DateVal FROM( 
SELECT t1.RowID, t1.TotalHoursVal, SUM(t2.TotalHoursVal) as TotalVal,t1.FleetID,t1.FleetComponentID
      ,ROW_NUMBER()OVER(PARTITION BY t1.FleetID,t1.FleetComponentID ORDER BY t1.FleetID,t1.FleetComponentID,T1.RowID)Rnk
      ,t1.DateVal
		FROM @TempVal t1
				INNER JOIN @TempVal t2 on t1.RowID >= t2.RowID AND t1.FleetID=t2.FleetID AND t1.FleetComponentID=t2.FleetComponentID
				INNER JOIN #TEMP T ON t1.FleetID=T.FleetID AND t1.FleetComponentID=T.FleetComponentID
				GROUP BY t1.RowID, t1.TotalHoursVal,t1.FleetID,t1.FleetComponentID,T.NextDueAfter,t1.DateVal
				HAVING SUM(t2.TotalHoursVal) >= T.NextDueAfter
       )TT
        WHERE TT.FleetComponentID=#TEMP.FleetComponentID
          AND TT.FleetID=#TEMP.FleetID
          AND TT.Rnk=1
          AND #TEMP.Remaining <= 0
          AND LTRIM(RTRIM(hoursdayscycles))='C' 


          




   --  SELECT DepartureDTTMLocal FROM PreflightLeg WHERE CustomerID=@UserCustomerID GROUP BY DepartureDTTMLocal HAVING COUNT(LEGID)=2
  
IF @SuppressActivityAircft=0
BEGIN  
     
SELECT DISTINCT tail_nmbr=TailNum
			   ,warn_hrs=WarningHrs
			   ,comp_desc=FleetComponentDescription
			   ,ac_code=AircraftCD
			   ,comp_code=ComponentCD
			   ,lastinspdt=LastInspectionDT
			   ,insp_hrs=NextDueAfter
			   ,hdc_flag=hoursdayscycles
			   ,since_hrs=FLOOR(ISNULL(SinceLastInsp,0)*10)*0.1 
			   ,remain_hrs=FLOOR(ISNULL(Remaining,0)*10)*0.1
			   ,duedate=(CASE  WHEN Remaining >0 THEN (DATEADD(DAY,(CASE WHEN LTRIM(RTRIM(hoursdayscycles))='H' THEN ((DATEDIFF(DAY,LastInspectionDT,GETDATE())*NextDueAfter) / SinceLastInsp)
                                                     WHEN LTRIM(RTRIM(hoursdayscycles))='C' THEN ((DATEDIFF(DAY,LastInspectionDT,GETDATE())*NextDueAfter) / SinceLastInsp) END),LastInspectionDT)) ELSE Projection END)     
			   ,status=''
    FROM #TEMP
    UNION 
    SELECT CASE WHEN TH.OldTailNUM = F.TailNum THEN TH.NewTailNUM ELSE F.TailNum END AS TailNum,0,'NO ACTIVITY',NULL,NULL,NULL,NULL,'',0,0,NULL,status='' FROM Fleet F 
         LEFT JOIN TailHistory TH ON TH.OldTailNUM = F.TailNum AND TH.CustomerID = F.CustomerID
         LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
         LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	   WHERE(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))OR @FleetGroupCD='') 
	       AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))OR @TailNum='') 
           AND F.CustomerID = @UserCustomerID AND  F.IsInActive=0
           AND F.FleetID NOT IN (SELECT ISNULL(FleetID,0) FROM #TEMP)
    
END
ELSE
BEGIN

SELECT DISTINCT tail_nmbr= TailNum
			  ,warn_hrs=WarningHrs
			  ,comp_desc=FleetComponentDescription
			  ,ac_code=AircraftCD
			  ,comp_code=ComponentCD
			  ,lastinspdt=LastInspectionDT
			  ,insp_hrs=NextDueAfter
			  ,hdc_flag=hoursdayscycles
			  ,since_hrs=FLOOR(ISNULL(SinceLastInsp,0)*10)*0.1
			  ,remain_hrs=FLOOR(ISNULL(Remaining,0)*10)*0.1 
              ,duedate=(CASE  WHEN Remaining >0 THEN (DATEADD(DAY,(CASE WHEN LTRIM(RTRIM(hoursdayscycles))='H' THEN ((DATEDIFF(DAY,LastInspectionDT,GETDATE())*NextDueAfter) / SinceLastInsp)
                                                     WHEN LTRIM(RTRIM(hoursdayscycles))='C' THEN ((DATEDIFF(DAY,LastInspectionDT,GETDATE())*NextDueAfter) / SinceLastInsp) END),LastInspectionDT)) ELSE Projection END)    
             ,status=''
    FROM #TEMP

END
 
IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
DROP TABLE #TEMP	

END




GO


