IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBVendorListing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBVendorListing]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE  [dbo].[spGetReportDBVendorListing] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30) --Mandatory
)
AS
 
-- ===============================================================================
-- SPC Name: [spGetReportDBVendorListing]
-- Author: Ravi R
-- Create date: 01 August
-- Description: Get Vendor Listing
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
SELECT
[Code] = V.VendorCD
,[Vendor Name] = V.Name
,[Contact Name] = CASE WHEN VC.LastName<>'' THEN VC.LastName +', ' ELSE '' END+ISNULL(VC.FirstName,'')+' '+ISNULL(LEFT(VC.MiddleName,1),'') ---V.VendorContactName
,[Active] = CASE WHEN V.IsInActive=1 THEN 'Y' ELSE 'N' END
,[Homebase] = A.IcaoID

FROM  Vendor V
LEFT OUTER JOIN (SELECT * FROM VendorContact WHERE IsChoice=1) VC  ON V.VendorID=VC.VendorID
LEFT OUTER JOIN Airport A ON V.HomebaseID=A.AirportID
WHERE V.CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND V.CustomerID = CONVERT(BIGINT, @UserCustomerID)
 And V.IsDeleted=0
 AND V.VendorType='V'
 ORDER BY V.VendorID

--EXEC spGetReportDBVendorListing 'SUPERVISOR_99','10099'


GO


