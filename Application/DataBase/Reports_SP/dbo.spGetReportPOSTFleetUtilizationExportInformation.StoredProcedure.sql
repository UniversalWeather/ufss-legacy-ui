IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetUtilizationExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetUtilizationExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


   
 CREATE procedure  [dbo].[spGetReportPOSTFleetUtilizationExportInformation]   
	(
	  @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @TailNum AS NVARCHAR(1000) = '',
	  @FleetGroupCD AS NVARCHAR(4000) = '',
	  @HomeBaseCD AS VARCHAR(1000) = '', 
	  @IsHomebase BIT = 0
	)  
 AS  
-- ===============================================================================  
-- SPC Name: [[spGetReportPOSTFleetUtilizationExportInformation]]  
-- Author: A.AKHILA  
-- Create date: 03 August  
-- Description: Get POST flight Delay Details  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================  
SET NOCOUNT ON

-----------------------------TailNum and Fleet Group Filteration----------------------
 if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END 
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1),
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
	INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
	INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------

SELECT
	[LOG] = POM.LOGNUM
	,[type_code] = AC.AircraftCD
	,[tail_nmbr] = F.TailNum
	,[trip_count] = CASE WHEN POM.TripID IS NULL THEN POM.POLogID ELSE POM.TripID END
	,[log_count] = POM.POLogID
	,[leg_count] = COUNT(POL.POLegID)
	,[blk_hrs] = ROUND(POL.BlockHours,1)
	,[flt_hrs] = ROUND(POL.FlightHours,1)
	,[avg_blk_leg] =''
	,[avg_flt_leg] =''
	,[distance] = POL.Distance
	,[avg_dist_leg] =''
	,[pax_total] = POL.PassengerTotal 
	,[fuel_used] = POL.FuelUsed             
	,[fuel_flt_hr] =''
	,[fuelqty] = DBO.GetFuelConversion(poe.FuelPurchase,poe.FuelQTY,@UserCD)
	,[expamt] = POE.ExpenseAMT
	,[tot_line] = ''
	,[avg_price] = ''
FROM PostflightMain POM
	LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted=0
	LEFT OUTER JOIN PostflightExpense POE ON POE.POLegID = POL.POLegID
	LEFT OUTER JOIN Fleet F ON POM.FleetID = F.FleetID 
	LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	LEFT JOIN Company C ON C.HomebaseID = F.HomebaseID
	LEFT JOIN Airport AF ON AF.AirportID = C.HomebaseAirportID  
	INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID 
WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
    AND POM.IsDeleted=0 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	--AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
	AND AF.IcaoID IN 
	(	
		SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE AF.IcaoID END, ',')
	
	)

GROUP BY POM.LOGNUM, AC.AircraftCD, F.TailNum, POM.TripID, POM.POLogID, POL.POLegID, POL.BlockHours, 
			POL.FlightHours, POL.Distance, POL.PassengerTotal, POL.FuelUsed, POE.FuelPurchase, POE.FuelQTY, POE.ExpenseAMT 
ORDER BY AC.AircraftCD, F.TailNum 
 --EXEC spGetReportPOSTFleetUtilizationExportInformation 'supervisor_99','2009-01-01','2010-1-1','','','','',0

GO


