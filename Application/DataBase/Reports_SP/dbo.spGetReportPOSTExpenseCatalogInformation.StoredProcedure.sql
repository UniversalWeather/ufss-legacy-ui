IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTExpenseCatalogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTExpenseCatalogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTExpenseCatalogInformation]
	 @UserCD AS VARCHAR(30)
	,@SlipNum AS VARCHAR(30)  
	,@LogNum AS VARCHAR(30)
	,@LegNum AS VARCHAR(30) 
AS
-- ===============================================================================        
-- SPC Name: spGetReportPOSTExpenseCatalogInformation        
-- Author: AISHWARYA.M        
-- Create date: 21 JULY 2014        
-- Description: GET EXPENSE CATALOGUE REPORT DETAILS
-- Revision History        
-- Date                 Name        Ver         Change        
-- ================================================================================ 		
DECLARE @IcaoID VARCHAR(4)=''    
SELECT @IcaoID=A.IcaoID FROM Company C  
              INNER JOIN UserMaster UM ON UM.HomeBaseID=C.HomeBaseID   
                    INNER JOIN Airport A ON C.HomebaseAirportID = A.AirportID  
                  WHERE LTRIM(RTRIM(UserName)) = @UserCD  
  
SELECT DISTINCT TI.LogNum LogNumber  
  ,TI.LegNUM LegNumber  
  ,AC.AccountNum AccountNumber  
  ,AC.AccountDescription AccDesc  
  ,PE.AccountPeriod AcctPeriod  
  ,TI.TailNum TailNumber  
  ,@IcaoID [HomeBase]  
  ,PE.IsBilling IncludeOnBilling  
  ,PE.PurchaseDT PurchaseDate  
  ,AP.IcaoID [ICAO]  
  ,LEFT(FB.FBOVendor,CHARINDEX(' ',FB.FBOVendor,1)) FBOName  
  ,CR.CrewCD CrewCode  
  ,PE.DispatchNUM DispatchNumber  
  ,PE.InvoiceNUM InvoiceNumber  
  ,PT.PaymentTypeCD PaymentType  
  ,V.VendorCD PayableVendor  
  ,V.Name VendorDesc  
  ,FC.FlightCatagoryCD FlightCatagory  
  ,PE.FuelQTY Quantity  
  ,PE.UnitPrice UnitPrice  
  ,CASE PE.FuelPurchase   
   WHEN 1 THEN 'Gallons'   
   WHEN 2 THEN 'Liters'   
   WHEN 3 THEN 'Imp Gals'   
   WHEN 4 THEN 'Pounds'   
   ELSE 'Kilos'   
   END  FuelUnits  
  ,FL.FuelLocatorCD FuelLocator  
  ,PE.FederalTAX  
  ,PE.SateTAX StateTax  
  ,PE.SaleTAX SalesTax  
  ,PE.ExpenseAMT TotalExpense
  ,PE.SlipNUM SlipNumber
  ,TI.POLegID [LegID]
  ,FB.FBOCD FBOCode
  ,PE.IsAutomaticCalculation autocalc
  ,PE.LastUpdUID lastuser
  ,PE.LastUpdTS lastupdt
  ,PE.PostEDPR postedpr
  ,PE.PostFuelPrice postfuelprice
  ,PE.IsDeleted [Isdeleted]
  ,PE.FuelPurchase [FuelPurchase]
  ,'' FixedBaseOperatorsCode
  ,'' Notes
FROM PostflightExpense PE  
  LEFT JOIN (SELECT PM.LogNum,PL.LegNUM,F.TailNum,PL.POLegID 
						,PM.CustomerID ,PM.IsDeleted FROM PostflightMain PM 
					       JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID
					       JOIN Fleet F ON F.FleetID = PM.FleetID
			)TI ON PE.POLegID = TI.POLegID AND TI.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  LEFT JOIN (SELECT AccountID, AccountNum, AccountDescription FROM Account)  
      AC ON AC.AccountID = PE.AccountID  
  LEFT JOIN (SELECT IcaoID, AirportID FROM Airport) AP ON AP.AirportID = PE.AirportID  
  LEFT JOIN (SELECT FlightCategoryID, FlightCatagoryCD FROM FlightCatagory)  
      FC ON FC.FlightCategoryID = PE.FlightCategoryID  
  LEFT JOIN (SELECT PaymentTypeID, PaymentTypeCD FROM PaymentType)  
       PT ON PE.PaymentTypeID = PT.PaymentTypeID  
  LEFT JOIN (SELECT VendorID, VendorCD, Name FROM Vendor)   
      V ON V.VendorID = PE.PaymentVendorID  
  LEFT JOIN (SELECT FBOID, FBOVendor, FBOCD FROM FBO)  
      FB ON FB.FBOID = PE.FBOID  
  LEFT JOIN (SELECT FL.FuelLocatorID, FuelLocatorCD FROM PostflightExpense PE  
						 INNER JOIN FuelLocator FL ON FL.FuelLocatorID = PE.FuelLocatorID  
    )FL ON FL.FuelLocatorID = PE.FuelLocatorID  
  LEFT JOIN (SELECT C.CrewCD,C.CrewID,PE.POLegID   
    FROM (SELECT  POLegID,CrewID FROM PostflightExpense) PE   
    INNER JOIN (SELECT CrewCD,CrewID FROM Crew) C ON PE.CrewID = C.CrewID  
    )CR ON CR.CrewID = PE.CrewID AND CR.POLegID = PE.POLegID  
 WHERE 
  PE.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  AND PE.SlipNUM = @SlipNum  
  --AND PM.LogNum = @LogNum  
  --AND PL.LegNUM = @LegNum  
  AND PE.IsDeleted = 0  
  
  
  --exec spGetReportPOSTExpenseCatalogInformation 'SUPERVISOR_99','467','1'
GO


