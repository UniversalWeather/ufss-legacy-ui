IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDelaySummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDelaySummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE Procedure  [dbo].[spGetReportPOSTDelaySummaryInformation] 
      (
        @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(1000) = '',
		@LogNum AS NVARCHAR(1000) = '',
		@FleetGroupCD NVARCHAR(1000) = '',
		@IsHomebase BIT = 0,
		@SortByTailNo BIT = 0
	   )
AS

-- ===============================================================================
-- SPC Name: [[spGetReportPOSTDelaySummaryInformation]]
-- Author: Avanikanta Pradhan
-- Create date: 01 August
-- Description: Get POST Delay Summary
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
					
begin
DECLARE @TAIL TABLE(
    [DateRange]	VARCHAR(100), 	
	[Tail Number]   VARCHAR(9),
	[AIRCRAFT] VARCHAR(10), 
    [Log Number] BIGINT,
	[Delay Code] CHAR(2), 
    [Delay Type] VARCHAR(100),
	[No.Of Legs] INT,
	[Delay Time] NUMERIC(7,3),
	[Avg.Delay] NUMERIC(7,3),
	[TenToMin] INT,
	[FlightDate] DATE)
	 
	-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN 

INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
END
-------------------------------TailNum and Fleet Group Filteration---------------------- 
INSERT INTO @TAIL
select distinct
 
	 
	 	  [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),	      
      [Tail Number] = F.TailNum,
      [AIRCRAFT] = AC.AircraftCD,
      [Log Number] = podet.[Log Number],
      [Delay Code] = PODET.[Delay Code],
      [Delay Type] = PODET.[Delay Type],
      [No.Of Legs] = PODET.[No.Of Legs],
      [Delay Time] = PODET.[Delay Time],     
      [Avg.Delay] = PODET.[Avg.Delay] ,
       [TenToMin] = @TenToMin,
       [FlightDate] = PODET.[FlightDate]

 from 

(
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger ,F.AircraftID
		FROM Fleet F 
		left outer JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		left outer JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID

		
		
		WHERE F.IsDeleted = 0
		

		and F.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		
		) F
inner join 


  ( select 
	POM.FleetID,
	POM.CustomerID,

      [Log Number] = POM.LogNum,
      [Delay Code] = DT.DelayTypeCD,
      [Delay Type] = DT.DelayTypeDescription,
      [No.Of Legs] = ISNULL(COUNT(POL.POLegID),0),
      [Delay Time] = ISNULL(POL.DelayTM, 0),     
      [Avg.Delay] = ISNULL(POL.DelayTM/COUNT(POL.POLegID),0) ,
     [TenToMin] = @TenToMin,
     [FlightDate] = POM.EstDepartureDT 
	    from 

 [PostflightLeg] POL
	 INNER JOIN PostflightMain POM ON POM.POLogID = POL.POLogID AND POM.IsDeleted = 0

	 INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
	 INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
	 INNER JOIN DelayType DT ON DT.DelayTypeID =POL.DelayTypeID
	 
 WHERE 
 POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
 AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)
 AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
 AND POL.IsDeleted = 0
AND POM.LogNum in(SELECT distinct LogNum  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0 
	WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND PM.IsDeleted = 0)
	
 --AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
 	GROUP BY POM.FleetID, POM.CustomerID, POL.ScheduledTM,  POM.LogNum, A.IcaoID, AA.IcaoID, DT.DelayTypeCD,
	DT.DelayTypeDescription, POL.POLegID, POL.DelayTM, POM.EstDepartureDT  
) PODET on PODET.FleetID = F.FleetID AND PODET.CustomerID = F.CustomerID
 LEFT JOIN Company COM ON COM.HomebaseID = F.HomebaseID	
 LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
  INNER JOIN #TempFleetID T1 ON F.FleetID=T1.FleetID
where 
	
	
	  ISNULL([Delay Type],'' )=(case when @TailNum = '' then [Delay Type]  else ISNULL([Delay Type],'' ) end)
	ORDER BY PODET.[Log Number], AC.AircraftCD, F.TailNum,  PODET.[Delay Type]

IF @FleetGroupCD <> ''  AND @TailNum <> ''
BEGIN

INSERT INTO @TAIL(DateRange,[Tail Number],[TenToMin])
SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum,@TenToMin FROM Fleet 
LEFT OUTER JOIN @TAIL T ON Fleet.TailNum=T.[Tail Number]
               WHERE TailNum NOT IN(SELECT [Tail Number]  FROM @TAIL)
                 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
														ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
														LEFT OUTER JOIN FleetGroup FG 
														ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID

										WHERE  (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))
UNION ALL

SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet
               WHERE TailNum NOT IN(SELECT [Tail Number]  FROM @TAIL)
                 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')

										
END 
ELSE IF @FleetGroupCD <> ''  
BEGIN
INSERT INTO @TAIL(DateRange,[Tail Number],[TenToMin])
SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet 
               WHERE TailNum NOT IN(SELECT [Tail Number]  FROM @TAIL)
                 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                 AND (TailNum IN(SELECT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO
														ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
														LEFT OUTER JOIN FleetGroup FG 
														ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID

										WHERE  (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')))

										
END  
ELSE IF @TailNum <> '' --OR @FleetGroupCD <> ''
BEGIN

INSERT INTO @TAIL(DateRange,[Tail Number],[TenToMin])
SELECT dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),TailNum, @TenToMin FROM Fleet 
               WHERE TailNum NOT IN(SELECT [Tail Number]  FROM @TAIL)
                 AND CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                 AND (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')

END
IF @SortByTailNo=1
BEGIN
SELECT DISTINCT * FROM @TAIL
ORDER BY [Tail Number],[Log Number],[Delay Code]
END
ELSE 
BEGIN
SELECT DISTINCT * FROM @TAIL
ORDER BY [Log Number],[Delay Code]
END
END		

IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID 

--EXEC spGetReportPOSTDelaySummaryInformation 'jwilliams_13','2000-01-01','2012-12-12','N46E','','',0,''






GO