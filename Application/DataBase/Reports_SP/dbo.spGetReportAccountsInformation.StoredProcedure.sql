IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAccountsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAccountsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportAccountsInformation]
		@UserCD VARCHAR(30)
AS
-- ===============================================================================
-- SPC Name: spGetReportAccountsInformation
-- Author: SUDHAKAR J
-- Create date: 31 May 2012
-- Description: Get Accounts catalog information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 13 Jun 2012	Sudhakar J	1.1		Added parameters @CustomerID and @HomeBase
-- ================================================================================
SET NOCOUNT ON

DECLARE @CUSTOMERID BIGINT;
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

  SELECT 
	   [AccountNum]
      ,[AccountDescription]
      ,[CorpAccountNum]
      ,[HomeBase] = AA.IcaoID
      ,[IsBilling]
      ,A.CustomerID
      ,A.IsInactive
  FROM Account A
  LEFT OUTER JOIN Company C ON A.HomebaseID = C.HomebaseID
  LEFT OUTER JOIN Airport AA ON C.HomebaseAirportID = AA.AirportID
    
  WHERE 
    ISNULL(A.IsDeleted,0) = 0
	AND A.CustomerID = @CUSTOMERID
	---AND C.HomebaseAirportID = dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
  ORDER BY [AccountNum]
-- EXEC spGetReportAccountsInformation 'ELIZA_9'

GO


