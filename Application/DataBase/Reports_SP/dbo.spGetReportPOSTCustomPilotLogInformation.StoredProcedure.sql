IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCustomPilotLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCustomPilotLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportPOSTCustomPilotLogInformation] 
(
	@UserCD AS VARCHAR(30)
	,@IsPilot BIT = 1  --Mandatory
)

-- ===============================================================================
-- SPC Name: [spGetReportPOSTCustomPilotLogInformation]
-- Author: Akhila
-- Create date: 01 August
-- Description: Get POST Delay Detail
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

AS

DECLARE @Category VARCHAR(10)
IF @IsPilot = 1 
BEGIN
	SET @Category = 'PILOT LOG'
END ELSE BEGIN
	SET @Category = 'FLIGHT LOG'
END

SELECT 
[OriginalDescription] = TSF.OriginalDescription
,[CustomDescription] = TSF.CustomDescription
,[IsPrint] = IsPrint
,[Category] = TSF.Category 
FROM TSFlightLog TSF 
WHERE TSF.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND TSF.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))
AND TSF.Category = @Category




--EXEC spGetReportPOSTCustomPilotLogInformation 'JWILLIAMS_13', 1
GO


