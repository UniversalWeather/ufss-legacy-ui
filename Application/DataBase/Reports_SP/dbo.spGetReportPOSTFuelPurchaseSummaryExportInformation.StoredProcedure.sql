IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelPurchaseSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelPurchaseSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


    
CREATE PROCEDURE [dbo].[spGetReportPOSTFuelPurchaseSummaryExportInformation]          
 (          
  @UserCD AS VARCHAR(30),             
  @DATEFROM Date,          
  @DATETO Date,          
  @IcaoID CHAR(10)=''          
 )          
-- =============================================          
-- Author: Mathes          
-- Create date: 31-07-2012          
-- Description: Fuel Purchase Summary - Report           
-- =============================================          
AS          
SET NOCOUNT ON          
DECLARE @AppDTFormat VARCHAR(25)
SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END      
BEGIN          
              
    IF @IcaoID =''          
    BEGIN          
        SELECT  FuelLocator.FuelLocatorCD fuel_loc ,   
                  FuelLocator.FuelLocatorDescription [desc],    
                  Airport.IcaoID icao_id,   
                  PostflightExpense.PurchaseDT purchasedt,   
                  PostflightExpense.DispatchNUM dispatchno,   
                  Fleet.TailNum  tail_nmbr,   
                  Airport.CityName city,  
				  FBO.FBOCD fbo_code,   
                  FBO.FBOVendor name,       
                  PostflightExpense.FuelQTY fuelqty,   
                  PostflightExpense.FuelPurchase fuelpurch,
                  Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End As unitprice,    
                  PostflightExpense.FuelQTY * PostflightExpense.UnitPrice expamt,   
                  Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase THEN  (PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.PostFuelPrice End As postfuelprice,  
                  (PostflightExpense.FuelQTY * PostflightExpense.UnitPrice )-((PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0)) costdiff 
                  ,@DATEFORMAT DATFORMAT
           FROM  PostflightExpense 
            INNER JOIN Fleet ON Fleet.FleetID = PostflightExpense.FleetID
            left outer JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID 
            left outer JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID LEFT JOIN  
            Airport ON PostflightExpense.AirportID = Airport.AirportID LEFT OUTER JOIN
            Company ON PostflightExpense.HomebaseID = Company.HomebaseID   
        WHERE  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
         AND PostflightExpense.IsDeleted=0
         And Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO)  
               Order  By PostflightExpense.PurchaseDT       
     END          
    ELSE            
     BEGIN          
       SELECT  FuelLocator.FuelLocatorCD fuel_loc ,   
                  FuelLocator.FuelLocatorDescription [desc],    
                  Airport.IcaoID icao_id,   
                  PostflightExpense.PurchaseDT purchasedt,   
                  PostflightExpense.DispatchNUM dispatchno,   
                  Fleet.TailNum  tail_nmbr,   
                  Airport.CityName city,  
                  FBO.FBOVendor fbo_code,   
                   FBO.FBOVendor name,    
                  PostflightExpense.FuelQTY fuelqty,   
                  PostflightExpense.FuelPurchase fuelpurch,   
                  Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End As unitprice, 
                  PostflightExpense.FuelQTY * PostflightExpense.UnitPrice expamt,   
                   Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase THEN  (PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.PostFuelPrice End As postfuelprice,  
                  (PostflightExpense.FuelQTY * PostflightExpense.UnitPrice )-((PostflightExpense.FuelQTY*PostflightExpense.PostFuelPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0)) costdiff  
                  ,@DATEFORMAT DATFORMAT
          FROM  PostflightExpense 
            INNER JOIN Fleet ON Fleet.FleetID = PostflightExpense.FleetID
            left outer JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID 
            left outer JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID LEFT JOIN  
            Airport ON PostflightExpense.AirportID = Airport.AirportID LEFT OUTER JOIN 
            Company ON PostflightExpense.HomebaseID = Company.HomebaseID   
        WHERE  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
         AND   PostflightExpense.IsDeleted=0 
         And   Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO)  
         AND Airport.IcaoID=@IcaoID        
               Order By PostflightExpense.PurchaseDT       
      END          
                     
END   
--Exec spGetReportPOSTFuelPurchaseSummaryExportInformation 'supervisor_99','01-Jan-2011','31-Dec-2012'

GO


