IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQInvoiceSubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQInvoiceSubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportCQInvoiceSubInformation]
	@CQFileID VARCHAR(30),
	@QuoteNUM INT
AS 
-- =============================================
-- SPC Name:spGetReportCQInvoiceSubInformation
-- Author: Aishwarya.M
-- Create date: 06 Mar 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN



SELECT DISTINCT  StandardChargesAdditonalFees =CASE WHEN CQIA.IsPrintable=1 THEN CQIA.CQInvoiceFeeDescription ELSE NULL END--CASE WHEN C.IsQuoteFeelColumnDescription = 1 THEN CQIA.CQInvoiceFeeDescription ELSE NULL END
		,ChargeUnit = CQIA.ChargeUnit---CASE WHEN C.IsCOLFee3 = 1 THEN CQIA.ChargeUnit ELSE '' END
		,Quantity = CQIA.Quantity
		,Rate =CASE WHEN ISNULL(CQIA.Quantity,0)=0 THEN 0 ELSE ISNULL(CQIA.InvoiceAmt,0)/CQIA.Quantity END 
		,InvoicedAmount =CQIA.InvoiceAmt
		,CQIA.CQInvoiceAdditionalFeesID
		 FROM CQInvoiceMain CQIM 
		 INNER JOIN CQMain CM ON CQIM.CQMainID=CM.CQMainID AND CM.IsDeleted=0
		 INNER JOIN CQFile CQF ON CQF.CQFileID = CM.CQFileID
         LEFT OUTER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
         --INNER JOIN Company C ON CQIM.CustomerID = C.CustomerID
         WHERE CM.CQFileID=CONVERT(BIGINT,@CQFileID)
           AND  CM.QuoteNUM=@QuoteNUM
           AND OrderNUM > 3
           AND CQIA.IsPrintable=1
           ORDER BY CQIA.CQInvoiceAdditionalFeesID


			
END
	
	--SELECT * FROM CQInvoiceAdditionalFees
	--EXEC spGetReportCQInvoiceSub1Information 10099472,1		

/*
SELECT DISTINCT  StandardChargesAdditonalFees = CASE WHEN C.IsQuoteFeelColumnDescription = 1 THEN CQID.FromDescription ELSE NULL END
		,ChargeUnit = CASE WHEN C.IsCOLFee3 = 1 THEN CQIA.ChargeUnit ELSE 0 END
		,Quantity = CQIA.Quantity
		,Rate =CASE WHEN ISNULL(CQIA.Quantity,0)=0 THEN 0 ELSE ISNULL(CQIA.InvoiceAmt,0)/CQIA.Quantity END 
		,InvoicedAmount =CQIA.InvoiceAmt
		/*
		,FlightChargeDesc = CASE WHEN CQIS.IsPrintFlightChg = 1/* AND CL.IsPositioning = 1*/ THEN CQIS.FlightChgDescription ELSE NULL END
		,FlightCharge = CASE WHEN CQIS.IsPrintFlightChg = 1 --AND CL.IsPositioning = 1 
		                   THEN CQIS.FlightChargeTotal ELSE 0 END
		,UsageDesc = CASE WHEN CQIS.IsPrintUsage = 1 THEN CQIS.UsageAdjDescription ELSE NULL END
		,Usagevalue = CASE WHEN CQIS.IsPrintUsage = 1 THEN CQIS.UsageAdjTotal ELSE 0 END
		,AdditonalFeesDesc = CASE WHEN CQIS.IsPrintAdditonalFees = 1 THEN CQIS.DescriptionAdditionalFee ELSE NULL END
		,AdditonalFees = CASE WHEN CQIS.IsPrintAdditonalFees = 1 THEN CQIS.AdditionalFeeTotal ELSE 0 END
		,StdCrewDesc = CASE WHEN CQIS.IsPrintStdCrew = 1 THEN --CM.StdCrewROMDescription 
		                   CQIS.StdCrewRONDescription ELSE NULL END
		,StdCrew = CASE WHEN CQIS.IsPrintStdCrew = 1 THEN CQIS.StdCrewRONTotal ELSE 0 END
		,AdditionalCrewDesc = CASE WHEN CQIS.IsPrintAdditionalCrew = 1 THEN CQIS.AdditionalCrewDescription ELSE NULL END
		,AdditionalCrew = CASE WHEN CQIS.IsPrintAdditionalCrew = 1 THEN CQIS.AdditionalCrewTotal ELSE 0 END
		,AdditionalCrewRONDesc = CASE WHEN CQIS.IsAdditionalCrewRON = 1 THEN CQIS.AdditionalCrewRON ELSE NULL END
		,AdditionalCrewRON = CASE WHEN CQIS.IsAdditionalCrewRON = 1 THEN /*CM.AdditionalCrewRONTotal2*/ CQIS.AdditionalCrewRONTotal ELSE 0 END
		,WaitingChgDesc = CQIS.WaitChgdescription --CASE WHEN CQIM.IsPrintWaitingChg = 1 THEN CQIS.WaitChgdescription ELSE NULL END
		,WaitingChg = ISNULL(CQIS.WaitChgTotal,0)--CASE WHEN CQIS.IsPrintWaitingChg = 1 THEN CQIS.WaitChgTotal ELSE 0 END
		,LandingFeesDesc = CASE WHEN CQIS.IsPrintLandingFees = 1 THEN CQIS.LandingFeeDescription ELSE NULL END
		,LandingFees = CASE WHEN CQIS.IsPrintLandingFees = 1 THEN CQIS.LandingFeeTotal ELSE 0 END
		,SegmentFeeDesc = CASE WHEN CQIS.IsPrintSegmentFee = 1 THEN CQIS.SegmentFeeDescription ELSE NULL END
		,SegmentFee = CASE WHEN CQIS.IsPrintSegmentFee = 1 THEN CQIS.SegmentFeeTotal ELSE 0 END --CQFleetChargeTotal.SegementFeeTotal(CQMain.totsegfee)
		,SubtotalDesc = CASE WHEN CQIS.IsPrintSubtotal = 1 THEN CQIS.SubtotalDescription ELSE NULL END
		,Subtotal = CASE WHEN CQIS.IsPrintSubtotal = 1 THEN CQIS.SubtotalTotal ELSE 0 END
		,DiscountDesc = CASE WHEN CQIS.IsPrintDiscountAmt = 1 THEN CQIS.DescriptionDiscountAmt ELSE NULL END
		,Discount = CASE WHEN CQIS.IsPrintDiscountAmt = 1 THEN CQIS.DiscountAmtTotal ELSE 0 END
		,TaxesDesc = CASE WHEN CQIS.IsPrintTaxes = 1 THEN CQIS.TaxesDescription ELSE NULL END
		,Taxes = CASE WHEN CQIS.IsPrintTaxes = 1 THEN CQIS.TaxesTotal ELSE 0 END
		,InvoiceDesc = CASE WHEN CQIS.IsPrintInvoice = 1 THEN CQIS.InvoiceDescription ELSE NULL END
		,Invoice = CASE WHEN CQIS.IsPrintInvoice = 1 THEN CQIS.InvoiceTotal ELSE 0 END --CM.InvoiceTotal
		,PayDesc = CASE WHEN CQIS.IsPrintPrepay = 1 AND CQIS.PrepayTotal <> 0 THEN CQIS.PrepayDescription ELSE NULL END
		,Pay = CASE WHEN CQIS.IsPrintPrepay = 1 AND CQIS.PrepayTotal <> 0 THEN CQIS.PrepayTotal ELSE 0 END
		,RemaingAMTDesc = CASE WHEN CQIS.IsPrintRemaingAMT = 1 AND CQIS.PrepayTotal <> 0 THEN CQIS.RemainingAmtDescription ELSE NULL END
		,RemaingAMT = CASE WHEN CQIS.IsPrintRemaingAMT = 1 AND CQIS.PrepayTotal <> 0 THEN CQIS.RemainingAmtTotal ELSE 0 END
		*/
		 FROM CQInvoiceMain CQIM 
		 INNER JOIN CQMain CM ON CQIM.CQMainID=CM.CQMainID
         INNER JOIN CQInvoiceAdditionalFees CQIA ON CQIM.CQInvoiceMainID=CQIA.CQInvoiceMainID
         INNER JOIN CQFile CQF ON CQF.CQFileID = CM.CQFileID
         INNER JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID=CQIM.CQInvoiceMainID
         --INNER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID=CQID.CQInvoiceMainID
         INNER JOIN Company C ON CQIM.CustomerID = C.CustomerID
         WHERE CM.CQFileID=CONVERT(BIGINT,@CQFileID)
           AND  CM.QuoteNUM=@QuoteNUM
*/




GO


