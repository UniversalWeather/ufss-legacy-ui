IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewworkLoadIndex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewworkLoadIndex]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTCrewworkLoadIndex] (
@UserCD AS VARCHAR(30),
@Year VARCHAR(4),
@CrewCD VARCHAR(500)='',-- Optional
@CrewGroupCD VARCHAR(500)='' -- Optional
) AS


-- ===============================================================================             
-- SPC Name: spGetReportPOSTCrewWorkLoadIndex             
-- Author:  Pukal K           
-- Create date: 05 Jan 2013             
-- Description: Get Calendar information form Preflight and Tripinformation from Postflight. WorkLoadIndex calculatioin is below
/* 
Workload Index = Duty Hrs + Blk Hrs + Legs + Office Hrs +
RON (Occurrences x Value Points of Duty type �R�) +
Duty Types (Occurrences x Value Points of the specific duty type)            

�	Duty Hours, Block Hours, Legs and RON values are pulled from Postflight -> Flight Logs and Other Crew Duty Log.
�	Office Hours are pulled from Preflight, Crew Calendar. This should be a standard calculation
�	Duty Type value get from CrewDutyType

*/
-- Revision History             
-- Date   Name  Ver  Change             
-- EXEC spGetReportPOSTCrewWorkLoadIndex '2010','JWILLIAMS_13','',''
-- ================================================================================             



BEGIN
 -----------------------------Crew and Crew Group Filteration----------------------
DECLARE @UserCustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID

SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
DECLARE  @DATEFROM DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @DATETO  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0))))  
DECLARE @AftBasis NUMERIC(1,0); 
SELECT @AftBasis=AircraftBasis FROM Company WHERE CustomerID=@UserCustomerID 
                                           AND HomebaseID=(CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
 

                        
DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 


Declare @SuppressActivityCrew BIT  

SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

DECLARE  @TempCrewID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		CrewCD VARCHAR(5),
		CrewMember VARCHAR(60),
		CustomerID BIGINT,
		HomeBaseID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO @TempCrewID
	SELECT DISTINCT C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
	FROM Crew C 
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @UserCustomerID  
	AND C.IsDeleted=0
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID--F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @UserCustomerID 
	AND C.IsDeleted=0 
	AND C.IsStatus=1

END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = '' 
BEGIN 

INSERT INTO @TempCrewID
	SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
	FROM Crew C 
	WHERE C.CustomerID = @UserCustomerID
	AND C.IsDeleted=0
END




-----------------------------Crew and Crew Group Filteration----------------------

								   

---------------------------------Ron Calculation----------------------------

  CREATE  TABLE #CrewInfoTable(
                               CrewMember VARCHAR(60),
                               CrewID BIGINT,
                               DutyType CHAR(2),
                               PoLogID BIGINT,
                               LogNum BIGINT,
                               LegNum BIGINT,
                               Depart DATETIME,
                               Arrive DATETIME,
                               NextLocal DATETIME,
                               RecordType CHAR(1),
                               IsDutyEnd BIT,
                               PoLegID BIGINT)

 INSERT INTO #CrewInfoTable
   SELECT  DISTINCT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  NULL DutyTYPE,
				  PM.POLogID,
				  PM.LogNum,
				  PL.LegNUM,
				  PL.OutboundDTTM,
				  PL.InboundDTTM,
				  NULL NextLocal,--PL.NextDTTM
				  PM.RecordType,
				  PL.IsDutyEnd,PL.POLegID
   FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                          LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                          LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                          
         WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND PL.LegNUM=1
             AND PM.IsDeleted = 0




   
   INSERT INTO #CrewInfoTable
   SELECT DISTINCT  C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,
				  C.CrewID,
				  NULL,
				  PM.POLogID,
				  PM.LogNum,
				  PL1.LegNUM,
				  PL1.OutboundDTTM,
				  PL1.InboundDTTM,
				  NULL,--PL.NextDTTM
				  PM.RecordType,
				  PL1.IsDutyEnd,PL1.POLegID
   FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                          LEFT OUTER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                          INNER JOIN Crew C ON C.CrewID=PC.CrewID 
                          LEFT OUTER JOIN #CrewInfoTable CI ON CI.CrewID=C.CrewID AND CI.PoLegID=PL.POLegID
                          INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                          LEFT OUTER JOIN Aircraft A ON PM.AircraftID=A.AircraftID
                          INNER JOIN PostflightLeg PL1 ON PL.POLogID=PL1.POLogID AND PL.LegNUM+1=PL1.LegNUM
                          
         WHERE   (	
					(CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL.InboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.InboundDTTM) <= CONVERT(DATE,@DATETO))
				  OR
					(CONVERT(DATE,PL1.OutboundDTTM) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL1.OutboundDTTM) <= CONVERT(DATE,@DATETO))
				 ) 
                                        
             AND C.IsDeleted=0
             AND C.IsStatus=1
             AND PM.CustomerID=@UserCustomerID
             AND PM.IsDeleted = 0
           


INSERT INTO #CrewInfoTable
SELECT C.CrewCD+' '+C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID,PL.DutyTYPE,PL.POLogID,CI.LogNum,PL.LegNUM,PL.OutboundDTTM
      ,PL.InboundDTTM ,NULL,CI.RecordType,CI.IsDutyEnd,PL.POLegID
   
 FROM PostflightLeg PL 
	INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
	INNER JOIN Crew C ON C.CrewID=PC.CrewID
	INNER JOIN #CrewInfoTable CI ON PL.POLogID=CI.PoLogID AND CI.LegNum-1=PL.LegNUM
	INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
 WHERE PL.POLegID NOT IN (SELECT DISTINCT ISNULL(POLegID,0) FROM #CrewInfoTable)

 UPDATE #CrewInfoTable SET NextLocal=TEMP.OutboundDTTM FROM
	(SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.OutboundDTTM,PL.LegNUM FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
                                                                                        WHERE PM.CustomerID= @UserCustomerID)TEMP
                 WHERE #CrewInfoTable.LEGNUM+1=TEMP.LegNUM
                   AND #CrewInfoTable.POLOGID=TEMP.POLogID 
                   AND #CrewInfoTable.LOGNUM=TEMP.LogNum 
 

 
DECLARE @CurDate TABLE(DateWeek DATE,CrewCD VARCHAR(5),CrewMember VARCHAR(60),CrewID BIGINT)

INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID FROM Crew C  
                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@DateFrom,@DateTo) ) T
                WHERE C.CustomerID=@UserCustomerID 
                  AND C.IsDeleted=0
                  AND C.IsStatus=1

 
 CREATE TABLE  #tblCrew(CrewCD VARCHAR(5),
                      CrewMember VARCHAR(60),
                      DateWeek DATETIME,
                      Arrive DATETIME,
                      DutyType CHAR(2),
                      CrewID BIGINT,
                      RecordType CHAR(1),
                      Office_Hrs NUMERIC(7,3)
                      )



INSERT INTO #tblCrew                
SELECT DISTINCT CD.CrewCD,CD.CrewMember, 
               CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END, 
               CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE DateWeek END,
				CASE WHEN CI.RecordType='C' THEN CI.DutyType  ELSE  'R' END  DutyType,CD.CrewID,CI.RecordType,
				ROUND(DATEDIFF(MINUTE,(CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END),
				            (CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59') END))/ 60.0,1)
				FROM @CurDate CD LEFT JOIN #CrewInfoTable CI ON CD.CrewID=CI.CrewID
				                 INNER JOIN Crew C ON C.CrewID=CI.CrewID
				                 --INNER JOIN CrewDutyType CDT ON CDT.CustomerID=C.CustomerID
			    WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,CASE WHEN CI.RecordType='C' THEN CI.Depart  ELSE CI.Arrive END)
				 AND CONVERT(DATE,CD.DateWeek) <= CONVERT(DATE,CASE WHEN CI.RecordType='C' THEN CI.Arrive ELSE CI.NextLocal END )
				 AND RecordType='C'

INSERT INTO #tblCrew  
SELECT DISTINCT CD.CrewCD,CD.CrewMember,CASE WHEN CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END,
                             CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE DateWeek END,'R' DutyType,CD.CrewID,CI.RecordType,
                 ROUND(DATEDIFF(MINUTE,(CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Depart) THEN CI.Depart ELSE DateWeek END),
				            (CASE   WHEN  CONVERT(DATE,DateWeek)=CONVERT(DATE,CI.Arrive) THEN CI.Arrive ELSE CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59') END))/ 60.0,1)
				FROM @CurDate CD LEFT JOIN #CrewInfoTable CI ON CD.CrewID=CI.CrewID
			    WHERE  CONVERT(DATE,CD.DateWeek) >= CONVERT(DATE,CI.Arrive)
				 AND CONVERT(DATE,CD.DateWeek) < CONVERT(DATE, CI.NextLocal )
				 AND IsDutyEnd=1
				
			  -- AND RecordType='L'

---------------------------------------Main Query--------------------------

  DECLARE  @InfoTable TABLE (
                               CrewID BIGINT,
                               CrewCD VARCHAR(5),
                               CrewMember VARCHAR(60),
                               LegNum BIGINT,
                               FlightHours NUMERIC(7,3),
                               BlocHours NUMERIC(7,3),
                               DutyHours NUMERIC(7,3),
                               HomeBase CHAR(4),
                               DaysAway INT,
                               FlightDays INT,
                               RonWeekDayCount INT,
                               RonWeekEndCount INT,
                               DutyType CHAR(2),
                               Office_Hrs NUMERIC(7,3),
                               WeekEndPts NUMERIC(7,3),
                               ValuePTS   NUMERIC(7,3),
                               WkLoad NUMERIC(9,3),
                               Ron INT,
                               Rnk INT,
                               DutyTypeCount INT,
                               MonthVal INT,
                               CrewTypeDesc VARCHAR(25),
                               YearVal INT
                               ) 
  
  


INSERT INTO @InfoTable 
SELECT TEMP.CrewID,TEMP.CrewCD,TEMP.CrewMember,TEMP.Legs,TEMP.FlightHours,TEMP.BlockHours,TEMP.DutyHrs,TEMP.HomeBase,TEMP.DaysAway,TEMP.FlightDays
      ,TEMP.RonWeekDayCount,TEMP.RonWeekEndCount,TEMP.DutyType,TEMP.OfficeHrs,TEMP.WeekEndPts,TEMP.ValuePTS,TEMP.WkLoad,TEMP.Ron
      ,ROW_NUMBER() OVER (PARTITION BY TEMP.CrewID,MonthVal ORDER BY TEMP.CrewID,MonthVal) Rnk,NULL DutyTypeCount,MonthVal,CrewTypeDescription,YearVal
      
      FROM (                           
SELECT DISTINCT  ISNULL(CREW1.CrewID,TC.CrewID) CrewID,ISNULL(Crew1.CrewCD,TC.CrewCD) CrewCD,ISNULL(Crew1.CrewMember,TC.CrewMember) CrewMember,Crew1.Legs,Crew1.FlightHours,BlockHours,Crew1.DutyHrs,ISNULL(Crew1.HomeBase,A.IcaoID) HomeBase,ISNULL(Crew1.DaysAway,DATEDIFF(DAY,T.Arrive,T.DateWeek)+1 ) DaysAway,Crew1.FlightDays,
       RonWeekDayCount.CountVal RonWeekDayCount,RonWeekEndCount.CountVal RonWeekEndCount,ISNULL(RonWeekDayCount.DutyType,RonWeekEndCount.DutyType) DutyType,0 OfficeHrs,NULL WeekEndPts,NULL ValuePTS,NULL WkLoad,NULL Ron,NULL DutyTypeCount,ISNULL(MonthVal,MONTH(T.DateWeek))MonthVal,CrewTypeDescription,ISNULL(Crew1.YearVal,YEAR(T.DateWeek)) YearVal
       
       FROM @TempCrewID TC 
       INNER JOIN Airport A ON TC.HomeBaseID=A.AirportID
       LEFT OUTER JOIN #tblCrew T ON TC.CrewID=T.CrewID
       LEFT OUTER JOIN 
					(
					SELECT CREW.CrewID,Crew.CrewCD,CREW.CrewMember,COUNT(Legs)Legs,SUM(FlightHours)FlightHours,SUM(DutyHrs)DutyHrs,HomeBase,SUM(DaysAway)DaysAway,COUNT(DISTINCT CONVERT(DATE,FlightDays)) FlightDays,MONTH(DateVal)MonthVal,CrewTypeDescription,SUM(BlockHours) BlockHours,YEAR(DateVal)YearVal
					 FROM
								(
										SELECT C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,PC.CrewID,COUNT(PL.LegNUM)Legs,SUM(ROUND(PL.FlightHours,1))FlightHours,SUM(ROUND(PL.FlightHours,1))BlockHours,DATEDIFF(DAY, PL.OutboundDTTM,PL.InboundDTTM)+1 DaysAway,
											   CASE WHEN PL.IsDutyEnd=1 THEN  SUM(ROUND(PC.DutyHours,1)) ELSE 0 END DutyHrs,A.IcaoID HomeBase,PL.OutboundDTTM FlightDays,PL.OutboundDTTM DateVal,CrewTypeDescription
																FROM PostflightMain PM 
																	 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
																	 INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID 
																	 INNER JOIN @TempCrewID TC ON TC.CrewID=PC.CrewID
																	 INNER JOIN Crew C ON PC.CrewID=C.CrewID
																	 INNER JOIN Airport A ON TC.HomebaseID=A.AirportID
																	 LEFT OUTER JOIN Aircraft AT ON PM.AircraftID=AT.AircraftID
																WHERE PM.CustomerID=@UserCustomerID 
                                          						  AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
                                          						  AND PM.IsDeleted = 0
																GROUP BY C.CrewCD,C.LastName,C.FirstName,PC.CrewID,PL.IsDutyEnd,A.IcaoID,PC.DaysAway,PL.OutboundDTTM,PL.InboundDTTM,CrewTypeDescription
								 )Crew 
								 GROUP BY CREW.CrewID,CREW.HomeBase,CREW.CrewMember,Crew.CrewCD,MONTH(DateVal),CrewTypeDescription,YEAR(DateVal)
							)Crew1 ON TC.CrewID=Crew1.CrewID
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal,MONTH(TC.DateWeek) Mon,YEAR(TC.DateWeek) YearVal FROM #tblCrew TC
																			   WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) NOT IN(7,1) 
																			   GROUP BY CrewMember,DutyType,TC.CrewID,MONTH(TC.DateWeek),YEAR(TC.DateWeek))RonWeekDayCount ON RonWeekDayCount.CrewID=TC.CrewID AND (Crew1.MonthVal=RonWeekDayCount.Mon )
LEFT OUTER JOIN (SELECT TC.CrewMember,TC.CrewID,TC.DutyType,COUNT(TC.DateWeek) CountVal,MONTH(TC.DateWeek) Mon,YEAR(TC.DateWeek) YearVal FROM #tblCrew TC
																					WHERE DutyType='R'  AND DATEPART (Dw, DateWeek) IN(7,1) 
																					 GROUP BY CrewMember,DutyType,TC.CrewID,MONTH(TC.DateWeek),YEAR(TC.DateWeek))RonWeekEndCount ON RonWeekEndCount.CrewID=TC.CrewID AND (Crew1.MonthVal=RonWeekEndCount.Mon)


		

UNION ALL

SELECT TC.CrewID,TC.CrewCD,TC.CrewMember,0 Legs, 0 FlightHours,0 BlockHours,0 DutyHrs,A.IcaoID,0 DaysAway,0 FlightDays,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =6 OR DATEPART(Dw, DateWeek) =5 OR DATEPART(Dw, DateWeek) =4 OR DATEPART(Dw, DateWeek) =3 OR DATEPART(Dw, DateWeek) =2 THEN 1 ELSE 0 END)  WeekdayCount,
       SUM(CASE WHEN DATEPART(Dw, DateWeek) =7 OR DATEPART(Dw, DateWeek) =1 THEN 1 ELSE 0 END )WeekEndCount,TC.DutyType,
       SUM(TC.Office_Hrs),NULL,NULL,NULL,NULL,NULL,MONTH(TC.DateWeek),C.CrewTypeDescription, YEAR(DateWeek)
                            FROM Crew C 
                             INNER JOIN #tblCrew TC  ON TC.CrewID=C.CrewID
                             INNER JOIN Airport A ON A.AirportID=C.HomebaseID
                             WHERE DutyType <>'R'
							GROUP BY CrewMember,DutyType,A.IcaoID,TC.DutyType,TC.CrewID,TC.CrewCD,MONTH(TC.DateWeek),CrewTypeDescription, YEAR(DateWeek)

 
)TEMP

DELETE FROM @InfoTable WHERE MonthVal IS NULL AND YearVal IS NULL

UPDATE @InfoTable SET WeekEndPts=ISNULL(CONVERT(NUMERIC(2,0),ISNULL(RonWeekEndCount,0)),0)*ISNULL(CONVERT(NUMERIC(2,0),ISNULL(Pts.WeekendPTS,0)),0),ValuePTS=ISNULL(CONVERT(NUMERIC(2,0),ISNULL(RonWeekDayCount,0)),0)*ISNULL(CONVERT(NUMERIC(2,0),ISNULL(Pts.ValuePTS,0)),0)
                FROM 
                   (SELECT DISTINCT DutyTypeCD,CDT.WeekendPTS,CDT.ValuePTS,T.MonthVal Mon FROM CrewDutyType CDT INNER JOIN @InfoTable T ON T.DutyType=CDT.DutyTypeCD WHERE CustomerID=@UserCustomerID AND IsOfficeDuty=0 AND IsWorkLoadIndex=1)Pts
                 WHERE Pts.DutyTypeCD=DutyType
                   AND Pts.Mon=MonthVal
    

UPDATE @InfoTable SET Office_Hrs=WK.Office_Hrs,WkLoad=WK.WkLoad,FlightHours=WK.FlightHours,DutyHours=WK.DutyHours,DaysAway=WK.DaysAway,
                     FlightDays=WK.FlightDays,LegNum=WK.LegNum
                  FROM (
                  SELECT MonthNo,WK1.Crew,SUM(WK1.Office_Hrs)Office_Hrs,SUM(WK1.WkLoad)WkLoad,SUM(WK1.FlightHours)FlightHours,SUM(WK1.DutyHours) DutyHours,SUM(WK1.FlightDays)FlightDays,SUM(WK1.DaysAway)DaysAway,SUM(WK1.LegNum)LegNum
                     FROM
                    (SELECT MonthVal MonthNo,CrewID Crew,CrewMember,
                    CONVERT(NUMERIC(7,3),SUM(ISNULL(CONVERT(NUMERIC(7,3),CASE WHEN @AftBasis=2 THEN  FlightHours ELSE BlocHours END),0)))
                     +CONVERT(NUMERIC(7,3),SUM(ISNULL(CONVERT(NUMERIC(7,3),DutyHours),0)))
                    +CONVERT(NUMERIC(7,3),SUM(ISNULL(CONVERT(NUMERIC(7,3),T.WeekEndPts),0)))
                    +CONVERT(NUMERIC(7,3),SUM(ISNULL(CONVERT(NUMERIC(7,3),T.ValuePTS),0)))
                    +CONVERT(NUMERIC(7,3),SUM(ISNULL( CASE WHEN CD.IsOfficeDuty=1 THEN  ISNULL(CONVERT(NUMERIC(7,3),T.Office_Hrs),0) ELSE 0 END ,0)))
                    +CONVERT(NUMERIC(7,3),SUM(ISNULL(CONVERT(NUMERIC(7,3),LegNum),0)))
                     WkLoad,
							   CASE WHEN CD.IsOfficeDuty=1 THEN  T.Office_Hrs ELSE 0 END  Office_Hrs,SUM(ISNULL(FlightHours,0)) FlightHours,SUM(ISNULL(DutyHours,0)) DutyHours,SUM(ISNULL(LegNum,0)) LegNum,
							   SUM(T.DaysAway)DaysAway,SUM(T.FlightDays)FlightDays
							  FROM @InfoTable T
								   LEFT OUTER JOIN(SELECT DISTINCT * FROM  CrewDutyType  WHERE CustomerID=@UserCustomerID) CD ON T.DutyType=CD.DutyTypeCD
							  GROUP BY CrewMember,CrewID,CD.IsOfficeDuty,T.Office_Hrs,MonthVal
					)WK1 
				GROUP BY Crew,MonthNo
				  )WK
				 WHERE WK.Crew=CrewID
				   AND WK.MonthNo=MonthVal



/*
IF @SuppressActivityCrew=0

BEGIN

INSERT INTO @InfoTable(CrewID,CrewCD,CrewMember,HomeBase,MonthVal,CrewTypeDesc,YearVal)
SELECT C.CrewID,
	   C.CrewCD,LastName+', '+ISNULL(FirstName,'') CrewMember,
	   A.IcaoID,MonthNo,C.CrewTypeDescription,ISNULL(CI.YearVal,@Year)
	  FROM Crew C
		   INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
		   LEFT OUTER JOIN @InfoTable CI ON CI.CrewID=C.CrewID
		   INNER JOIN Airport A ON C.HomebaseID=A.AirportID
		   CROSS JOIN @MonthVal
      WHERE C.CustomerID=@UserCustomerID 
		AND C.IsDeleted=0
		AND IsStatus=1
		--AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(CrewID,0) FROM @InfoTable)
		--AND MonthNo NOT IN (SELECT DISTINCT ISNULL(MonthVal,0) FROM @InfoTable)


END
*/

DECLARE @MonthVal TAble(MonthNo INT,Year INT);

WITH dates(DATEPARAM)
           AS (SELECT @DATEFROM AS DATETIME
               UNION ALL
               SELECT Dateadd(MONTH, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO @MonthVal (MonthNo,Year)
      SELECT MONTH(DATEPARAM),YEAR(DATEPARAM)
      FROM   Dates
      OPTION (maxrecursion 10000)



IF @SuppressActivityCrew=0

BEGIN

INSERT INTO @InfoTable(CrewID,CrewCD,CrewMember,HomeBase,MonthVal,CrewTypeDesc,YearVal)
SELECT C.CrewID,
	   C.CrewCD,LastName+', '+ISNULL(FirstName,'') CrewMember,
	   A.IcaoID,M.MonthNo,C.CrewTypeDescription,M.Year
	  FROM Crew C
		   INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
		   LEFT OUTER JOIN @InfoTable CI ON CI.CrewID=C.CrewID
		   INNER JOIN Airport A ON C.HomebaseID=A.AirportID
		   CROSS JOIN @MonthVal M
      WHERE C.CustomerID=@UserCustomerID 
		AND C.IsDeleted=0
		AND IsStatus=1
		--AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(CrewID,0) FROM @InfoTable)
		AND MonthNo NOT IN (SELECT DISTINCT ISNULL(MonthVal,0) FROM @InfoTable)
		
		
INSERT INTO @InfoTable(CrewID,CrewCD,CrewMember,HomeBase,MonthVal,CrewTypeDesc,YearVal)
SELECT C.CrewID,
	   C.CrewCD,LastName+', '+ISNULL(FirstName,'') CrewMember,
	   A.IcaoID,@MonthStart MonthNo,C.CrewTypeDescription,@Year
	  FROM Crew C
		   INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
		   INNER JOIN Airport A ON C.HomebaseID=A.AirportID
		   
      WHERE C.CustomerID=@UserCustomerID 
		AND C.IsDeleted=0
		AND IsStatus=1
		AND C.CrewID NOT IN (SELECT DISTINCT ISNULL(CrewID,0) FROM @InfoTable)


END





SELECT DISTINCT YearVal, [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
			   CrewID=T.CrewID,
			   T.CrewCD,
			   CrewMember=T.CrewMember ,
			   HomeBase=T.HomeBase,
			   WkLoadIndex=ISNULL(CASE WHEN Rnk=1 THEN FLOOR(ISNULL(T.WkLoad,0)*10)*0.1  ELSE 0 END ,0),
			   @TenToMin TenToMin,
			   MonthVal MonthNo,
			   MonthVal=UPPER(LEFT(DateName( MONTH , DateAdd( month , MonthVal , 0 ) - 1 ),3)),
			  ISNULL(CASE WHEN CrewTypeDesc='' THEN 'NO CREW TYPE ASSIGNED' ELSE  CrewTypeDesc END,'NO CREW TYPE ASSIGNED') CrewTypeDesc,
			  CASE WHEN ISNULL(CrewTypeDesc,'')='' THEN 1 ELSE 2 END Sorting
		  FROM @InfoTable T 
          ORDER BY YearVal,MonthNo,CASE WHEN ISNULL(CrewTypeDesc,'')='' THEN 1 ELSE 2 END,CrewTypeDesc,CrewCD
		  


END


IF OBJECT_ID('TEMPDB..#CrewInfoTable') IS NOT NULL 
DROP TABLE #CrewInfoTable

IF OBJECT_ID('TEMPDB..#tblCrew') IS NOT NULL 
DROP TABLE #tblCrew




 --DATEDIFF(HH,CD.WorkDate,CT.ActualDutyStartDTTM)
     
     
--select DutyTypeCD,WeekendPTS,ValuePTS,IsWorkLoadIndex,IsOfficeDuty from CrewDutyType where DutyTypeCD='va' and CustomerID=10099                                          







--EXEC spGetReportPOSTWorkLoadIndexInformation 'supervisor_99','10099','2009-1-1','2010-1-1','','','',0

GO