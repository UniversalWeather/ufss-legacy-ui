IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerVISAInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerVISAInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportPassengerVISAInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD char(5)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerVISAInformation
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Get Passenger Passport Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON

	SELECT 
		CPV.CustomerID
		,P.PassengerRequestorCD
		--,dbo.FlightPakDecrypt(IsNull(CPV.VisaNum,'')) AS VisaNum
		,IsNull(CPV.VisaNum,'') AS VisaNum
		--,[ExpiryDT] = CONVERT(varchar(10), CPV.ExpiryDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[ExpiryDT] = dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT)
		,[Nationality] = dbo.GetCountryCDByCountryID(CPV.CountryID)
		,CPV.Notes
		
	FROM CrewPassengerVisa CPV
	INNER JOIN (
			SELECT PassengerRequestorID, PassengerRequestorCD,Notes FROM Passenger 
		) P
		ON CPV.PassengerRequestorID = P.PassengerRequestorID
	WHERE CPV.IsDeleted = 0
	AND CPV.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND P.PassengerRequestorCD = RTRIM(@PassengerCD)

-- EXEC spGetReportPassengerVISAInformation 'TIM', 'CMAL'


GO


