IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCrewDutyOverviewExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCrewDutyOverviewExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



    
CREATE PROCEDURE [dbo].[spGetReportPRETSCrewDutyOverviewExportInformation]    
 @UserCD            AS VARCHAR(30)        
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM    
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM    
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)    
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal    
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal     
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)    
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)    
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)    
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)    
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD    
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase    
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)    
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD       
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'    
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'    
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'    
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'    
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'    
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'    
AS    
BEGIN  
  
-- =========================================================================  
-- SPC Name: spGetReportPRETSCrewDutyOverviewInformation  
-- Author: Askar 
-- Create date: 13 Sep 2012  
-- Description: Get Preflight Tripsheet Report Export information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================  

SET NOCOUNT ON  

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --


DECLARE @CUSTOMER BIGINT=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))

DECLARE @TenToMin NUMERIC(1,0)
SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 
 

 SELECT TEMP.* FROM 
  (SELECT  DISTINCT  CONVERT(VARCHAR,PM.TripNUM) orig_nmbr ,
           PM.DispatchNUM dispatchno,
           CONVERT(DATE,PLDATE.Departure) lowdate,
           CONVERT(DATE,PLDATE.Arrival) highdate,
           F.TailNum tail_nmbr,
           CONVERT(VARCHAR,PL.LegNUM) leg_num,
           PL.LegID legid,
           CONVERT(DATE,PL.DepartureDTTMLocal) locdep,
           D.IcaoID depicao_id,
           REPLACE(SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5),':','') dep_time,
           A.IcaoID arricao_id,
           REPLACE(SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalDTTMLocal,113),13,5),':','')  arr_time,
           ISNULL(PL.IsApproxTM,0) timeapprox,
           SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) crewlist,
           CASE(PL.IsDutyEnd) WHEN 0 THEN 'FALSE' ELSE 'TRUE' END dutyend,
           PL.DutyTYPE1 dutytype,
           ISNULL(PL.ElapseTM,0.0) elp_time, 
           ISNULL(PL.FlightHours,0.0) flt_hrs,
           ISNULL(PL.DutyHours,0.0) duty_hrs,
           ISNULL(PL.RestHours,0.0) rest_hrs,
           CDR.CrewDutyRuleCD crewcurr,
          (CASE WHEN PL.CrewDutyAlert IS NOT NULL THEN (CASE WHEN PL.CrewDutyAlert <>'' THEN 'TRUE' ELSE 'FALSE' END) ELSE 'FALSE' END) rulesexceed,
           PL.FedAviationRegNUM farnum,
           DATEDIFF(DAY, CONVERT(DATE,Departure), CONVERT(DATE,Arrival))+1 tot_trip_days, 
          CASE WHEN PL2.Leg=PL.LegNUM THEN  SUM(PL2.DutyHours) +ISNULL(PL3.DutyHours,0)+ISNULL(PL4.DutyHours,0) ELSE 0 END  duty_total,
           '' tot_line,  
           ISNULL(MIN(PL1.MinRest),0.0) min_rest,
           ISNULL(MAX(PL1.MaxRest),0.0) max_rest,
           @TenToMin TenToMin
		   FROM PreflightMain PM INNER JOIN PreflightLeg PL ON  PM.TripID = PL.TripID
		   
		   INNER JOIN(SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M ON M.TripID=PM.TripID AND M.TripID=PL.TripID AND M.LegID=PL.LegID
		   
		   	LEFT OUTER JOIN (
			SELECT DISTINCT PC1.LEGID,CrewList = (
				SELECT ISNULL(RTRIM(C.LastName)+ ', ' ,'') 
				FROM PreflightCrewList PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
				WHERE PC.LEGID = PC1.LEGID 
				ORDER BY( CASE PC.DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)
				FOR XML PATH('')
			)
			FROM PreflightCrewList PC1
		) Crew ON PL.LEGID = Crew.LEGID 
		  
		   INNER JOIN Fleet F ON PM.FleetID=F.FleetID
		  
		   INNER JOIN Airport D ON PL.DepartICAOID=D.AirportID
		  
		   INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
		  
		   LEFT OUTER JOIN CrewDutyRules CDR ON CDR.CrewDutyRulesID=PL.CrewDutyRulesID
		  
           LEFT OUTER JOIN (SELECT MIN(RestHours) MinRest,MAX(RestHours) MaxRest,TripID,TripNUM FROM PreflightLeg GROUP BY TripID,TripNUM) PL1 ON PL.TripID=PL1.TripID
		   
		   
		   INNER JOIN (SELECT TripID,TripNUM,MIN(DepartureDTTMLocal) Departure,MAX(ArrivalDTTMLocal) Arrival,CustomerID FROM PreflightLeg WHERE CUSTOMERID=@CUSTOMER GROUP BY TripID,TripNUM,CustomerID) PLDATE ON PLDATE.TripID=PL.TripID AND PLDATE.CustomerID=PL.CustomerID
		  
		   LEFT OUTER JOIN (SELECT TripNUM,TripID,DutyHours,LegNUM Leg FROM PreflightLeg WHERE CUSTOMERID=@CUSTOMER AND LegNUM IN(SELECT MAX(LEGNUM) FROM PreflightLeg WHERE  CustomerID=@CUSTOMER GROUP BY TripID,TripNUM,CustomerID))PL2 ON PL.TripID=PL2.TripID --AND PL1.LegNUM=PL2.Leg
		  
		   LEFT OUTER JOIN (SELECT TripNUM,TripID,DutyHours,FlightHours,LegNUM Leg FROM PreflightLeg
		    WHERE CUSTOMERID=@CUSTOMER AND  IsDutyEnd=1  AND LegNUM NOT IN(SELECT MAX(LEGNUM) FROM PreflightLeg WHERE CustomerID=@CUSTOMER GROUP BY TripID,TripNUM,CustomerID))PL3 ON PL3.TripID=PL.TripID --AND PL1.LegNUM=PL2.Leg
		    
		   LEFT OUTER JOIN(SELECT DutyHours,TripID FROM PreflightLeg WHERE CUSTOMERID=@CUSTOMER AND  IsDutyEnd=1  AND LegNUM  IN(SELECT MIN(LEGNUM) FROM PreflightLeg WHERE CustomerID=@CUSTOMER GROUP BY TripID,TripNUM,CustomerID))PL4 ON PL4.TripID=PL.TripID  
		 

		  
		   INNER JOIN(SELECT SUM(TEMP.FlightTime)FlightTime,TEMP.TripID,TEMP.TripNUM FROM
									(SELECT DISTINCT SUM(PL.FlightHours) FlightTime,SUM(PL.DutyHours)DutyTime,PM.TripID,PM.TripNUM
											  FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
																   INNER JOIN (SELECT TRIPID,TripNUM,MAX(LegNUM) MaxLeg FROM PreflightLeg GROUP BY TRIPID,TripNUM)PLMAX ON PL.TripID=PLMAX.TripID  AND PL.LegNUM=PLMAX.MaxLeg 
									                          
									           WHERE PM.CustomerID=@CUSTOMER                                                                                                         
									                                                                                                                   
									                                                                     
											   GROUP BY PM.TripID,PM.TripNUM

									UNION ALL

									SELECT DISTINCT SUM(PL.FlightHours) FlightTime,SUM(PL.DutyHours)DutyTime,PM.TripID,PM.TripNUM
											  FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
																   INNER JOIN (SELECT TRIPID,TripNUM,MIN(LegNUM) MinLeg FROM PreflightLeg GROUP BY TRIPID,TripNUM)PLMIN ON PL.TripID=PLMIN.TripID AND PL.LegNUM=PLMIN.MinLeg                                                                               
									                                                                                                                   
									                                                                                                                   
									           WHERE PM.CUSTOMERID=@CUSTOMER                                                          
											  GROUP BY PM.TripID,PM.TripNUM)TEMP
									GROUP BY TEMP.TripID,TEMP.TripNUM
									)FlightTime ON PM.TripID=FlightTime.TripID
	
		   
		   WHERE  PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			-- AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
		 GROUP BY PM.TripNUM,PL.LegNUM,PM.DispatchNUM,F.TailNum,PL.DepartureDTTMLocal,PL.DepartureDTTMLocal,
				  D.IcaoID,PL.ArrivalDTTMLocal,A.IcaoID,Crew.CrewList,PL.IsDutyEnd,PL.DutyTYPE,PL.CheckGroup,
			      PL.ElapseTM,PL.FlightHours,PL.DutyHours,PL.RestHours,CDR.MaximumDutyHrs,CDR.MinimumRestHrs,CDR.CrewDutyRuleCD,PL.FedAviationRegNUM,PL.IsApproxTM,PL.CrewDutyAlert,FlightTime.FlightTime,PLDATE.Departure,PLDATE.Arrival,PL3.DutyHours,PL3.FlightHours,PL4.DutyHours,PL.LegID,PL.DutyTYPE1,PL2.Leg)TEMP
WHERE duty_total<>0
           
           
     
 
 
           
END

  
--EXEC spGetReportPRETSCrewDutyOverviewExportInformation 'TIM', '', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''    
--EXEC spGetReportPRETSCrewDutyOverviewExportInformation 'TIM', '20', '', '', '20120919', '20120924', '', '', '', '', '', '', '','',1,1,1,1,1,1     

GO


