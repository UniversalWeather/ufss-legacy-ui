IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerBillingSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerBillingSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerBillingSummaryExportInformation] (
	 @UserCustomerID VARCHAR(30),
	@UserCD AS VARCHAR(30),
	@DATEFROM AS DATETIME,
	@DATETO AS DATETIME,
	@PassengerRequestorCD AS NVARCHAR(1000) = '',
	@TailNum AS NVARCHAR(1000) = '',
	@FleetGroupCD AS NVARCHAR(1000) = '',
	@UserHomebaseID AS VARCHAR(30),
	@IsHomebase AS BIT = 0,
	@PassengerGroupCD VARCHAR(500)='' 

)
AS
BEGIN
	
	SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
Declare @SuppressActivityPassenger BIT  
 	
SELECT @SuppressActivityPassenger=IsZeroSuppressActivityPassengerRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
	AND P.IsActive = 1
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER 
	AND P.IsActive = 1 
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
	AND P.IsActive = 1
END
-----------------------------Passenger and Passenger Group Filteration----------------------

	DECLARE @TenToMin SMALLINT = 0;

	SELECT @TenToMin = TimeDisplayTenMin
	FROM Company
	WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
		AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	SELECT DISTINCT 
	     CASE WHEN TH.OldTailNUM = FL.TailNum THEN TH.NewTailNUM ELSE FL.TailNum END AS [tail_nmbr]
	     ,PO.LogNum AS lognum
	     ,CONVERT(DATE, PO.EstDepartureDT) AS estdepdt
	     ,DE.DepartmentCD AS reqdept
		 ,DA.AuthorizationCD AS reqauth
	     ,ISNULL(PL.POLegID,0) AS legid
		 ,P.PassengerRequestorCD AS paxcode
		 ,PP.Billing AS  billing
		 ,p.StandardBilling AS stdbilling
		 ,DP.DepartmentCD AS [dept_code]
         ,DPA.AuthorizationCD AS [auth_code]
		 ,ISNULL(LastName, '') + CASE WHEN FirstName <> '' OR FirstName IS NOT NULL THEN ISNULL(', ' + FirstName, '') ELSE '' END AS paxname
		 ,PL.ScheduledTM AS schedttm
		 ,PL.Distance AS distance
		 ,PL.FlightHours AS [flt_hrs]
		 ,PL.BlockHours AS [blk_hrs]
		 ,CR.ChargeRate AS [chrg_rate]
		 ,AC.AircraftTypeCD AS [type_code]
		 ,[total_charges] = (
			(
				CASE 
					WHEN CR.ChargeUnit = 'N'
						THEN ((PL.Distance * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0))
					WHEN CR.ChargeUnit = 'S'
						THEN (((PL.Distance * CR.ChargeRate) * 1.15078) + ISNULL((PFE11.SUMExpenseAmt), 0))
					WHEN (
							CR.ChargeUnit = 'H'
							AND CO.AircraftBasis = 1
							)
						THEN ((PL.BlockHours * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0))
					WHEN (
							CR.ChargeUnit = 'H'
							AND CO.AircraftBasis = 2
							)
						THEN ((PL.FlightHours * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0))
					ELSE ISNULL((PFE11.SUMExpenseAmt), 0)
					END
				) / PL.PassengerTotal
			)
		,[TenToMin] = @TenToMin
		,[EXP_ACCOUNT] = EX.AccountDescription
		,[EXP_AMOUNT] = EX.ExpenseAMT
	INTO #PassTemp
	FROM PostflightMain PO
	JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PL.IsDeleted = 0 AND PO.CustomerID = PL.CustomerID
	LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID
	LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
	LEFT JOIN Department DP ON DP.DepartmentID = P.DepartmentID
    LEFT JOIN DepartmentAuthorization DPA ON DPA.AuthorizationID = P.AuthorizationID
    INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	LEFT JOIN Department DE ON DE.DepartmentID = P.DepartmentID
	LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = P.AuthorizationID
	JOIN vFleetGroup FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID 
	LEFT JOIN TailHistory TH ON FL.TailNum = TH.OldTailNUM AND FL.CustomerID = TH.CustomerID
	LEFT JOIN Company CO ON FL.HomebaseID = CO.HomebaseID AND FL.CustomerID = CO.CustomerID
	LEFT JOIN Aircraft AC ON AC.AircraftID = FL.AircraftID AND AC.CustomerID = FL.CustomerID
	LEFT JOIN Airport AI ON CO.HomebaseAirportID = AI.AIRPORTID AND AI.CustomerID = CO.CustomerID
	LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
	LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	LEFT JOIN (
		SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling,PP4.CustomerID
			,SUMExpenseAmt = (SELECT ISNULL(SUM(PP2.ExpenseAMT), 0) FROM PostflightExpense PP2
				WHERE PP2.POLegID = PP4.POLegID AND IsBilling = 1 AND PP2.IsDeleted = 0)
		FROM PostflightExpense PP4
		WHERE IsBilling = 1 
		AND PP4.IsDeleted = 0
		) PFE11 ON PL.POLegID = PFE11.POLegID
		AND PL.POLogID = PFE11.POLogID
		LEFT OUTER JOIN (
		SELECT POLegID,ChargeUnit,ChargeRate FROM PostflightMain PM
		INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
		INNER JOIN FleetChargeRate FC ON PM.FleetID = FC.FleetID AND CONVERT(DATE, BeginRateDT) <= CONVERT(DATE, PL.ScheduledTM) AND EndRateDT >= CONVERT(DATE, PL.ScheduledTM)
		) CR ON PL.POLegID = CR.POLegID
		LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
											  INNER JOIN Account A ON PE.AccountID = A.AccountID
										  WHERE PE.IsDeleted = 0
										  AND PE.POLegID=PE1.POLegID
										  FOR XML PATH('')
							)
		,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(30),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
											  INNER JOIN Account A ON PE.AccountID = A.AccountID
										  WHERE PE.IsDeleted = 0
										  AND PE.POLegID=PE1.POLegID
										  FOR XML PATH('')
							)
				FROM PostflightExpense PE1
							)EX ON EX.POLegID = PL.POLegID  
	WHERE CONVERT(DATE, PL.ScheduledTM) BETWEEN @DateFrom AND @DateTo
	 AND (FL.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	 AND (FL.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')
     AND (PO.HomebaseID IN (CONVERT(BIGINT, @UserHomebaseID)) OR @IsHomebase = 0)
	 AND PO.CustomerID = CONVERT(BIGINT, @UserCustomerID)
     AND P.PassengerRequestorCD IS NOT NULL
	 AND P.IsActive = 1
     AND PO.IsDeleted = 0
     
 


IF @SuppressActivityPassenger=0
BEGIN
	INSERT INTO #PassTemp (paxcode,paxname,TenToMin,legid)
	SELECT P.PassengerRequestorCD,ISNULL(LastName, '') + CASE WHEN FirstName <> '' OR FirstName IS NOT NULL THEN ISNULL(', ' + FirstName, '') ELSE '' END ,@TenToMin,0
	  FROM Passenger P
      INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	WHERE P.CustomerID = CONVERT(BIGINT, @UserCustomerID) AND PassengerRequestorCD NOT IN (SELECT PassengerRequestorCD FROM #PassTemp WHERE PassengerRequestorCD IS NOT NULL) AND P.IsActive = 1

END
	SELECT tail_nmbr
          ,lognum
          ,estdepdt
          ,reqdept
          ,reqauth
          ,legid
          ,paxcode
          ,billing
          ,stdbilling
          ,[dept_code]
          ,[auth_code]
          ,paxname
          ,schedttm
          ,distance
          ,[flt_hrs]
          ,[blk_hrs]
          ,[chrg_rate]
          ,[type_code]
          ,[tot_line] = ''
          ,[total_charges]
		  ,[TenToMin]
		  ,EXP_ACCOUNT
		  ,EXP_AMOUNT
	FROM #PassTemp

	IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
		DROP TABLE #PassTemp
END




GO


