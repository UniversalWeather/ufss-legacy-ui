IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewAlretsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewAlretsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




       CREATE Procedure [dbo].[spGetReportPOSTCrewAlretsInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@AsOf AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBaseCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@ChecklistCode AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IncludeAllAlerts as bit
						
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTCrewAlretsInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Crew Alerts for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================


SET NOCOUNT ON

DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=0

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END

--select * from @TempCrewID
-----------------------------Crew and Crew Group Filteration---------------------- 
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 0

--IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
--DROP TABLE #CREWINFO 	

CREATE TABLE #CREWINFO(As_of varchar(100),Crew_Member VARCHAR(60),CrewCD VARCHAR(9),Aircraft_Type varchar(10),Checklist_Item varchar(40),Previous varchar(100),
Due_Date varchar(100),Grace varchar(100),Alert varchar(100),Comment varchar(25),
Freq int,Base_Date varchar(100),TenToMin numeric(1,0),Total_Req_Hrs numeric(7,1),TOTFLTHOURS numeric(7,3),IsDeleted BIT,ISSTATUS BIT)

CREATE TABLE #CREWINFO1(As_of varchar(100),Crew_Member VARCHAR(60),CrewCD VARCHAR(9),Aircraft_Type varchar(10),Checklist_Item varchar(40),Previous varchar(100),
Due_Date varchar(100),Grace varchar(100),Alert varchar(100),Comment varchar(25),
Freq int,Base_Date varchar(100),TenToMin numeric(1,0),Total_Req_Hrs numeric(7,1),TOTFLTHOURS numeric(7,3),IsDeleted BIT,ISSTATUS BIT)
 
IF(@IncludeAllAlerts=0)
BEGIN
INSERT INTO #CREWINFO
select As_of,Crew_Member,CrewCD,Aircraft_Type,Checklist_Item,Previous,
Due_Date ,Grace ,Alert ,Comment,
Freq ,Base_Date ,TenToMin ,Total_Req_Hrs,sum(TOTFLTHOURS),IsDeleted ,ISSTATUS
from(
			SELECT distinct
			[As_of] = dbo.GetShortDateFormatByUserCD(@UserCD,@Asof),
			[Crew_Member] = ISNULL(C.FirstName,'') +'  '+ISNULL(C.LastName,''),
			[CrewCD]= C.CrewCD,
			[Aircraft_Type] = AC.AircraftCD,
			[Checklist_Item] =CCD.CheckListCD+' '+ CCL.CrewChecklistDescription,
                          
	        [Previous] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
			[Due_Date] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
			[Grace] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
			[Alert] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
			[Comment] = (CASE WHEN (CCD.IsOneTimeEvent='TRUE') And (CCD.IsCompleted='TRUE') THEN 'COMPLETED'
                              WHEN @Asof='' THEN ' '
							  WHEN (@Asof) < (CASE (CCD.AlertDT) WHEN '' THEN CCD.DueDT ELSE CCD.AlertDT END) THEN ''
							  WHEN @Asof BETWEEN  CCD.AlertDT  AND CCD.DueDT THEN 'ALERT' 
						      WHEN (@Asof> CCD.DUEDT) AND (@Asof<=CCD.GRACEDT) THEN 'GRACE'
						      --WHEN (@Asof > CCD.DUEDT) THEN 'PASSED DUE' END),
						      WHEN (@Asof > CCD.DUEDT) THEN 'PAST DUE' END),
			[Freq] = CCD.FrequencyMonth,
			[Base_Date] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.BaseMonthDT),
			[TenToMin]=@TenToMin,
			[Total_Req_Hrs] =CCD.TotalREQFlightHrs,
			[TOTFLTHOURS]=CASE WHEN PM.AircraftID=CCD.AircraftID THEN  ROUND(PC.FlightHours,1) ELSE NULL END,
			C.ISDELETED,C.IsStatus
		FROM Crew c
			INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
			LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID AND CCD.IsDeleted=0 
			LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
			LEFT OUTER JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
			inner JOIN POSTFLIGHTCREW PC ON C.CREWID=PC.CREWID
			left outer JOIN POSTFLIGHTMAIN PM ON PM.POLogID=PC.POLogID AND PM.IsDeleted=0
			LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
			LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID    
			LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
			LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID
		WHERE C.CustomerID =CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			AND (CONVERT(DATE,@Asof) >= CONVERT(DATE,CCD.AlertDT))
			AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
			AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
			AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = ''))a
		GROUP BY As_of,Crew_Member,CrewCD,Aircraft_Type,Checklist_Item,Previous,
Due_Date ,Grace ,Alert ,Comment,
Freq ,Base_Date ,TenToMin ,Total_Req_Hrs,IsDeleted ,ISSTATUS
END
ELSE
BEGIN
INSERT INTO #CREWINFO
select As_of,Crew_Member,CrewCD,Aircraft_Type,Checklist_Item,Previous,
Due_Date ,Grace ,Alert ,Comment,
Freq ,Base_Date ,TenToMin ,Total_Req_Hrs,sum(TOTFLTHOURS),IsDeleted ,ISSTATUS
from(
	SELECT DISTINCT
			[As_of] = dbo.GetShortDateFormatByUserCD(@UserCD,@Asof),
			[Crew_Member] =  ISNULL(C.FirstName,'') +'  '+ISNULL(C.LastName,''),
			[CrewCD]= C.CrewCD,
			[Aircraft_Type] = AC.AircraftCD,
			[Checklist_Item] = CCD.CheckListCD+' '+ CCL.CrewChecklistDescription,
			[Previous] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
			[Due_Date] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
			[Grace] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
			[Alert] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
			[Comment] = (CASE WHEN (CCD.IsOneTimeEvent='TRUE') And (CCD.IsCompleted='TRUE') THEN 'COMPLETED'
                              WHEN @Asof='' THEN ' '
							  WHEN (@Asof) < (CASE (CCD.AlertDT) WHEN '' THEN CCD.DueDT ELSE CCD.AlertDT END) THEN ''
							  WHEN @Asof BETWEEN  CCD.AlertDT  AND CCD.DueDT THEN 'ALERT' 
						      WHEN (@Asof> CCD.DUEDT) AND (@Asof<=CCD.GRACEDT) THEN 'GRACE'
						      --WHEN (@Asof > CCD.DUEDT) THEN 'PASSED DUE' END),
						      WHEN (@Asof > CCD.DUEDT) THEN 'PAST DUE' END),
			[Freq] = CCD.FrequencyMonth,
			[Base_Date] = dbo.GetShortDateFormatByUserCD(@UserCD,CCD.BaseMonthDT),
			[TenToMin]=@TenToMin,
			[Total_Req_Hrs] = CCD.TotalREQFlightHrs,
			[TOTFLTHOURS]=CASE WHEN PM.AircraftID=CCD.AircraftID THEN  ROUND(PC.FlightHours,1) ELSE NULL END,
			C.ISDELETED,C.IsStatus
		FROM Crew c
			INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
			inner JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
			LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
			LEFT OUTER JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
			inner JOIN POSTFLIGHTCREW PC ON C.CREWID=PC.CREWID
			left outer JOIN POSTFLIGHTMAIN PM ON PM.POLogID=PC.POLogID AND PM.IsDeleted=0
			LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
			LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID    
			LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
			LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
		WHERE C.CustomerID =CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
			AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
			AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = ''))a
		GROUP BY As_of,Crew_Member,CrewCD,Aircraft_Type,Checklist_Item,Previous,
Due_Date ,Grace ,Alert ,Comment,
Freq ,Base_Date ,TenToMin ,Total_Req_Hrs,IsDeleted ,ISSTATUS
END

INSERT INTO #CREWINFO1
SELECT DISTINCT
			[As_of] = dbo.GetShortDateFormatByUserCD(@UserCD,@Asof),
			[Crew_Member] = ISNULL(C.FirstName,'') +'  '+ISNULL(C.LastName,''),
			[CrewCD]= CrewCD,
			[Aircraft_Type] = NULL,
			[Checklist_Item] = 'NO ITEMS',
	        [Previous] = NULL,
			[Due_Date] = NULL,
			[Grace] = NULL,
			[Alert] = NULL,
			[Comment] = NULL,
			[Freq] = NULL,
			[Base_Date] = NULL,[TenToMin]=@TenToMin,
			[Total_Req_Hrs] = NULL,
			[TOTFLTHOURS]=NULL,C.ISDELETED,C.IsStatus
           from Crew C 
				INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
				LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
				LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
				LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID
				LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
				LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
				WHERE  C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
				AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
DELETE FROM #CREWINFO1 WHERE CrewCD IN(SELECT DISTINCT CrewCD from #CREWINFO)
			DELETE FROM #CREWINFO1 WHERE ISDELETED=0 AND ISSTATUS=0 AND crewcd NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))
			--ORDER BY CREWCD

  
				
IF @IsZeroSuppressActivityCrewRpt=0
BEGIN 
SELECT DISTINCT * FROM #CREWINFO
UNION ALL
SELECT DISTINCT * FROM #CREWINFO1
ORDER BY CREWCD
END
ELSE 
BEGIN
--DELETE FROM #CREWINFO1 WHERE ISDELETED=0 AND ISSTATUS=0 AND crewcd NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))
--DELETE FROM #CREWINFO WHERE Checklist_Item='NO ITEMS'
SELECT DISTINCT * FROM #CREWINFO
ORDER BY CREWCD	
END


IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO 	

IF OBJECT_ID('tempdb..#CREWINFO1') IS NOT NULL
DROP TABLE #CREWINFO1	


--EXEC spGetReportPOSTCrewAlretsInformation 'supervisor_99','2012/12/31','','','','','',0




GO


