IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLOCVendorInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLOCVendorInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportLOCVendorInformation]  
	    @UserCustomerID BIGINT
       ,@IcaoID VARCHAR(4)
       ,@CountryCD VARCHAR(2)
       ,@MilesFrom INT 
       ,@CityName VARCHAR(25)
       ,@MetroCD VARCHAR(3)
       ,@AircraftType VARCHAR(10)
       ,@StateName VARCHAR(25)
       ,@AirportName VARCHAR(25) 
       ,@IATA VARCHAR(3)
       ,@AirportID VARCHAR(30)
AS

-- ================================================================================  
-- SPC Name: spGetReportLOCVendorInformation  
-- Author: Askar  
-- Create date: 17 Sep 2013  
-- Description:  Get the Locator information of Airport  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- =================================================================================   


BEGIN       
    --1.city,country and state,2.icao 3.iata,city,country,state  
SELECT DISTINCT VendorName=ISNULL(V.Name,'')
      ,StateName=V.BillingState
      ,CityName=V.BillingCity
      ,Country=C.CountryName
      ,ICAO=A.IcaoID
      ,AT.AircraftCD
      ,V.BusinessPhone
	  ,LatitudeRad=CASE WHEN UPPER(A.LatitudeNorthSouth) = 'S' THEN (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  ELSE (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRad=CASE WHEN UPPER(A.LongitudeEastWest) = 'W' THEN (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  ELSE (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180 END 
	  ,LatitudeRadCityCenter=CASE WHEN UPPER(V.LatitudeNorthSouth) = 'S' Then (0-(V.LatitudeDegree + (V.LatitudeMinutes / 60))) * PI() / 180  ELSE (V.LatitudeDegree + (V.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRadCityCenter =CASE WHEN UPPER(V.LongitudeEastWest) = 'W' THEN (0-(V.LongitudeDegree + (V.LongitudeMinutes / 60))) * PI() / 180  ELSE (V.LongitudeDegree + (V.LongitudeMinutes / 60)) * PI() / 180 END 			
	  ,V.VendorCD
	  ,C.CountryCD
	  ,V.LatitudeDegree
	  ,V.LatitudeMinutes
	  ,V.LatitudeNorthSouth
	  ,V.LongitudeDegree
	  ,V.LongitudeMinutes
	  ,V.LongitudeEastWest
	  --,Latitude=(CASE WHEN V.LatitudeNorthSouth='N' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(3),V.LatitudeDegree)+CONVERT(VARCHAR(10),FLOOR(ISNULL((V.LatitudeMinutes/60),0)*100)*0.01)
	  --,Longitude=(CASE WHEN V.LongitudeEastWest='E' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(3),V.LongitudeDegree)+CONVERT(VARCHAR(10),FLOOR(ISNULL((V.LongitudeMinutes/60),0)*100)*0.01)
	  ,Latitude=(CASE WHEN V.LatitudeNorthSouth='S' THEN '-' ELSE '' END)
	  ,Longitude=(CASE WHEN V.LongitudeEastWest='W' THEN '-' ELSE '' END)
	  ,LatitudeCalculation = (V.LatitudeDegree + FLOOR(ISNULL((V.LatitudeMinutes/60),0)*100)*0.01)
	  ,LongitudeCalculation = (V.LongitudeDegree + FLOOR(ISNULL((V.LongitudeMinutes/60),0)*100)*0.01)
	  ,V.CustomerID
	  INTO  #TempResultTable		
      FROM Vendor V
           LEFT OUTER JOIN Airport A ON A.AirportID=V.AirportID   
		   LEFT OUTER JOIN Metro M ON V.MetroID = M.MetroID        
		   LEFT OUTER JOIN Country C ON V.CountryID = C.CountryID 
		   LEFT OUTER JOIN Fleet F ON V.VendorID=F.VendorID AND F.IsDeleted=0 AND F.IsInActive=0
		   LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID AND AT.CustomerID=V.CustomerID
      WHERE (((V.CustomerID = @UserCustomerID)) OR (A.AirportID=CONVERT(BIGINT,@AirportID) OR V.CustomerID = @UserCustomerID)) 
        AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID='')
        AND (C.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ',')) OR @CountryCD='')
        AND (V.BillingCity IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CityName, ',')) OR @CityName='')
        AND (M.MetroCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MetroCD, ',')) OR @MetroCD='')
        AND (V.BillingState IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ',')) OR @StateName='')
        AND (V.Name IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportName, ',')) OR @AirportName='')
        AND (A.Iata IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IATA , ',')) OR @IATA='')
        AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftType , ',')) OR @AircraftType='')
        AND V.IsDeleted=0
        AND V.VendorType='V'
         --AND V.CustomerID=@UserCustomerID
        
        
    
SELECT Vendor.* FROM (
 SELECT VendorName
       ,Country
       ,CityName
       ,StateName
       ,ICAO
       ,AircraftCD
       ,BusinessPhone
       ,Miles=CONVERT(INT,ISNULL((CAST( ACOS(SIN(LatitudeRadCityCenter) * SIN(LatitudeRad) + COS(LatitudeRadCityCenter) * COS(LatitudeRad) * COS(LongitudeRad - (LongitudeRadCityCenter))) * 3959   AS INT)),0),0)
       ,LatitudeDegree
	   ,LatitudeMinutes
	   ,LatitudeNorthSouth
	   ,LongitudeDegree
	   ,LongitudeMinutes
	   ,LongitudeEastWest
	   ,Latitude
	   ,Longitude
	   ,LatitudeCalculation
	   ,LongitudeCalculation
	   ,CountryCD
	   ,VendorCD
     FROM #TempResultTable  
   )Vendor
  WHERE Miles <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END
 ORDER BY VendorName  
 
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 
        
END




GO


