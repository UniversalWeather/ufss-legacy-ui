IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDestinationMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDestinationMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create Procedure [dbo].[spGetReportPREDestinationMhtmlInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@ICAOID AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBase AS BIT = 0
AS
-- ===============================================================================
-- SPC Name: spGetReportPREDestinationInformation
-- Author: Aishwarya.M
-- Create date: 10 Aug 2012
-- Description: Get Destination Export information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON


DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

	SET @SQLSCRIPT = '
		SELECT
			 arricao_id = A.ICAOID   
			,city = A.CityName
			,tail_nmbr = F.TailNum
			,estdepdt = PM.EstDepartureDT
			,locarr = PL.ArrivalDTTMLocal
			,locdep = PL.DepartureDTTMLocal
			,orig_nmbr = PM.TripNUM
			,trip_desc = dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
			,ac_code = F.AircraftCD
		FROM PreflightMain PM 
			INNER JOIN (
				SELECT TripID, TripNUM, ArriveICAOID, CustomerID,ArrivalDTTMLocal, DepartureDTTMLocal FROM PreflightLeg
			) PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
			INNER  JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID 
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
						OR @FleetGroupCD = '''' ) 
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			INNER JOIN ( 
				SELECT AirportID, IcaoID, CityName, CustomerID FROM Airport
			) A ON PL.ArriveICAOID = A.AirportID
		WHERE PM.IsDeleted = 0 AND PM.TripStatus = ''T'' 
			AND PM.CustomerID = ' + CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			+ ' AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN @DATEFROM AND @DATETO  ';

	--Construct OPTIONAL clauses
	IF @ICAOID <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@ICAOID, 1) = ',' THEN LEFT(@ICAOID, LEN(@ICAOID) - 1) ELSE @ICAOID END, ',', ''', ''') + ''')';
	END
	IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @HomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
        SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY arricao_id, city, locarr,locdep, tail_nmbr '
	
	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS VARCHAR(30)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD
END

	
-- EXEC spGetReportPREDestinationExportInformation 'JWILLIAMS_13','2012/09/01','2012/09/07', '', '', '', 1

--SELECT
--			 arricao_id = A.ICAOID   
--			,city = A.CityName
--			,tail_nmbr = F.TailNum
--			,estdepdt = PM.EstDepartureDT
--			,locarr = PL.ArrivalDTTMLocal
--			,locdep = PL.DepartureDTTMLocal
--			,orig_nmbr = PM.TripNUM
--			,trip_desc = dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
--			,ac_code = F.AircraftCD
--		FROM PreflightMain PM 
--			INNER JOIN (
--				SELECT TripID, TripNUM, ArriveICAOID, CustomerID, ArrivalDTTMLocal, DepartureDTTMLocal  FROM PreflightLeg
--			) PL ON PM.TripID = PL.TripID
--			INNER JOIN (
--				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID 
--				FROM Fleet F 
--				LEFT OUTER JOIN FleetGroupOrder FGO
--				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
--				LEFT OUTER JOIN FleetGroup FG 
--				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
--				WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
--					OR @FleetGroupCD = '' ) 
--			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
--			INNER JOIN ( 
--				SELECT AirportID, IcaoID, CityName, CustomerID FROM Airport
--			) A ON PL.ArriveICAOID = A.AirportID
--			LEFT OUTER JOIN FleetGroupOrder FG 
--				ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
--		WHERE PM.IsDeleted = 0 AND PM.TripStatus = 'T' 
--			AND PM.CustomerID =  + CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
--			 AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN @DATEFROM AND @DATETO




GO


