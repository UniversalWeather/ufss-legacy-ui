IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftStatusInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftStatusInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spGetReportPREAircraftStatusInformation]      
  @UserCD AS VARCHAR(30), --Mandatory          
  @EstDepartureDT AS DATETIME, --Mandatory        
  @TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @IsHomeBase AS BIT = 0      
AS        
-- ===============================================================================  
-- SPC Name: spGetReportAircraftStatusInformation       
-- Author: ASKAR         
-- Create date: 16 Jul 2012        
-- Description: Get Aircraft Status information for REPORTS        
-- Revision History        
-- Date			Name		Ver		Change        
-- 20 Aug 2012	Sudhakar.J	1.1		Added alias names like day1, day2 ...
--
-- ================================================================================        
BEGIN  
SET NOCOUNT ON 

DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
    
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))   
    

 DECLARE @TenToMin INT

-----------------------------TailNum and Fleet Group Filteration----------------------

     

      
      SET @TenToMin= (SELECT COM.TimeDisplayTenMin
                      FROM   Company COM
                             JOIN UserMaster UM
                               ON COM.CustomerID = UM.CustomerID
                                  AND COM.HomebaseID = UM.HomebaseID
                      WHERE  UM.UserName = @UserCD)
DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	--AND F.IsDeleted=0
	--AND F.IsInActive = 1 
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	--AND F.IsInActive = 1 
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE (F.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
	AND F.CustomerID = @CUSTOMERID  
	AND F.IsDeleted=0
	AND F.IsInActive =0
END


-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @SQLSCRIPT AS NVARCHAR(MAX) = '';      
DECLARE @ParameterDefinition AS NVARCHAR(500)  
      


 
 CREATE TABLE #TempFleetInfo (TailNum VARCHAR(9),
                     AircraftCD VARCHAR(10),
                     AIR_FRAME_HRS_LDS NUMERIC(11,1),
                     Fleet BIGINT,
                     STEP VARCHAR(4),
                     LoggedHours NUMERIC(11,1),
                     Cycles NUMERIC(6,0),
                     REVERSERSCycles NUMERIC(6,0),
                     day1 NUMERIC(7,3),
                     day2 NUMERIC(7,3),
                     day3 NUMERIC(7,3),
                     day4 NUMERIC(7,3),
                     day5 NUMERIC(7,3),
                     day6 NUMERIC(7,3),
                     day7 NUMERIC(7,3),
                     day8 NUMERIC(7,3),
                     day9 NUMERIC(7,3),
                     day10 NUMERIC(7,3),
                     day11 NUMERIC(7,3),
                     day12 NUMERIC(7,3),
                     day13 NUMERIC(7,3),
                     day14 NUMERIC(7,3),
                     day15 NUMERIC(7,3)
                     ,Type_Code CHAR(4))
                     
 CREATE TABLE #TempFleet (TailNum VARCHAR(9),
                     AircraftCD VARCHAR(10),
                     Fleet BIGINT,
                     AIR_FRAME_HRS_LDS NUMERIC(11,1),
                     FleetID BIGINT,
                     STEP VARCHAR(4),
                     LoggedHours NUMERIC(11,1),
                     Cycles NUMERIC(6,0),
                     REVERSERSCycles NUMERIC(6,0)
                     ,Type_Code CHAR(4)
                     )
    




INSERT INTO #TempFleet
 SELECT DISTINCT F.TailNum, B.AircraftCD,F.FleetID Fleet,-- A.*     
A.AIR_FRAME_HRS_LDS AS AIR_FRAME_HRS_LDS, A.FleetID, A.STEP, A.LoggedHours AS LoggedHours,
 A.Cycles,A.REVERSERSCycles,f.AircraftCD
  
 FROM  Fleet F      
  LEFT OUTER JOIN Aircraft B ON F.AircraftID = B.AircraftID  
 
LEFT OUTER JOIN (      
  SELECT  AirframeHrs AS AIR_FRAME_HRS_LDS,FleetID, '#1' AS STEP,CONVERT(NUMERIC(19,1),Engine1Hrs) LoggedHours,Engine1Cycle Cycles,Reverser1Cycle REVERSERSCycles  
   FROM FleetInformation      
  UNION      
  SELECT AirframeCycle AS AIR_FRAME_HRS_LDS,FleetID, '#2' AS STEP,CONVERT(NUMERIC(19,1),Engine2Hrs) LoggedHours,Engine2Cycle Cycles,Reverser2Cycle REVERSERSCycles  
  FROM FleetInformation         
 ) AS A ON F.FleetID = A.FleetID   
 WHERE  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                
  
  
 
INSERT INTO #TempFleet(TailNum,AircraftCD,Fleet,FleetID,STEP,LoggedHours,Cycles,REVERSERSCycles,Type_Code)
SELECT DISTINCT F.TailNum, B.AircraftCD,F.FleetID Fleet, A.*,F.AircraftCD 
FROM Fleet F      
INNER JOIN Aircraft B ON F.AircraftID = B.AircraftID 
	LEFT OUTER JOIN
	  (      
	  SELECT FleetID, '#3' AS STEP,Engine3Hrs LoggedHours,Engine3Cycle Cycles,Reverser3Cycle REVERSERSCycles   FROM FleetInformation           
	 ) AS A ON F.FleetID = A.FleetID       
	 WHERE  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	   AND A.LoggedHours >0
	   AND A.Cycles > 0
	   AND REVERSERSCycles >0
	  
INSERT INTO #TempFleet(TailNum,AircraftCD,Fleet,FleetID,STEP,LoggedHours,Cycles,REVERSERSCycles,Type_Code)
SELECT DISTINCT F.TailNum, B.AircraftCD,F.FleetID Fleet, A.*,F.AircraftCD 
FROM  Fleet F      
     LEFT OUTER JOIN Aircraft B ON F.AircraftID = B.AircraftID 
	LEFT OUTER  JOIN
	  (           
	  SELECT FleetID, '#4' AS STEP,Engine4Hrs LoggedHours,Engine4Cycle Cycles,Reverser4Cycle REVERSERSCycles   FROM FleetInformation      
	 ) AS A ON F.FleetID = A.FleetID       
	 WHERE  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	  AND A.LoggedHours >0
	  AND A.Cycles > 0
	  AND REVERSERSCycles >0
	  

INSERT INTO #TempFleetInfo(TailNum,AircraftCD,AIR_FRAME_HRS_LDS,Fleet,STEP,LoggedHours,Cycles,REVERSERSCycles,Type_Code)
  SELECT DISTINCT
		 AtStatus.TailNum, 
		 AtStatus.AircraftCD
		,AtStatus.AIR_FRAME_HRS_LDS
		,AtStatus.Fleet
		,AtStatus.STEP
		,AtStatus.LoggedHours
		,AtStatus.Cycles
		,AtStatus.REVERSERSCycles
	    ,AtStatus.Type_Code 
	FROM (SELECT   T.TailNum
                  ,T.AircraftCD
                  ,T.Fleet
                  ,T.STEP
                  ,T.LoggedHours
                  ,T.Cycles
                  ,T.REVERSERSCycles
                  ,T.AIR_FRAME_HRS_LDS
                  ,T.Type_Code
                        
             FROM  (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD Aircraft, F.HomeBaseID,F.IsDeleted 
													FROM Fleet F 
												 ) F
                    INNER JOIN #TempFleet  T ON F.FleetID = T.Fleet  
             WHERE  F.CUSTOMERID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                )AtStatus

UPDATE #TempFleetInfo SET day1=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


UPDATE #TempFleetInfo SET day2=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+1)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                           AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                           AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum
  
  
 UPDATE #TempFleetInfo SET day3=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+2)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day4=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+3)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day5=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+4)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
                          
UPDATE #TempFleetInfo SET day6=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+5)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

 
UPDATE #TempFleetInfo SET day7=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+6)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


 
                          
                          
UPDATE #TempFleetInfo SET day8=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+7)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          

UPDATE #TempFleetInfo SET day9=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+8)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day10=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+9)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day11=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+10)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

    
    
UPDATE #TempFleetInfo SET day12=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+11)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum


UPDATE #TempFleetInfo SET day13=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+12)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
UPDATE #TempFleetInfo SET day14=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+13)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum




UPDATE #TempFleetInfo SET day15=TEMP.ElapseTM FROM 
(SELECT SUM(PL.ElapseTM) ElapseTM,TailNum FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                              INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                          WHERE CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,@EstDepartureDT+14)
                          AND F.CustomerID=dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
                          AND PM.TripStatus='T'
                          AND ( PM.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
                          AND PM.IsDeleted = 0
                          GROUP BY TailNum
                          )TEMP
  WHERE #TempFleetInfo.TailNum=TEMP.TailNum

                          
                          

                          
                             
DECLARE @TailNum1 VARCHAR(9),@AircraftCD VARCHAR(10),@AIR_FRAME_HRS_LDS NUMERIC(11,1),@LoggedHours NUMERIC(11,1),@Cycles NUMERIC(6,0),@REVERSERSCycles NUMERIC(6,0),
@CrDAY1 NUMERIC(11,9),@CrDAY2 NUMERIC(11,9),@CrDAY3 NUMERIC(11,9),@CrDAY4 NUMERIC(11,9),@CrDAY5 NUMERIC(11,9),@CrDAY6 NUMERIC(11,9),@CrDAY7 NUMERIC(11,9),@CrDAY8 NUMERIC(11,9),@CrDAY9 NUMERIC(11,9),@CrDAY10 NUMERIC(11,9),
@CrDAY11 NUMERIC(11,9),@CrDAY12 NUMERIC(11,9),@CrDAY13 NUMERIC(11,9),@CrDAY14 NUMERIC(11,9),@CrDAY15 NUMERIC(11,9),@crType_Code CHAR(3),@Fleet BIGINT;



                                                                              
DECLARE C1 CURSOR FOR 
SELECT TailNum,AircraftCD,AIR_FRAME_HRS_LDS,LoggedHours,Cycles,REVERSERSCycles,DAY1,DAY2,DAY3,DAY4,DAY5,DAY6,DAY7,DAY8,DAY9,DAY10,DAY11,DAY12,DAY13,DAY14,DAY15,Type_Code,Fleet 
																			  FROM #TempFleetInfo
                                                                               WHERE (LoggedHours IS NULL OR LoggedHours=0.0) 
                                                                              --  AND (AIR_FRAME_HRS_LDS IS NULL OR AIR_FRAME_HRS_LDS=0.0)
                                                                               AND (Cycles IS NULL OR Cycles=0.0)
                                                                               AND (REVERSERSCycles IS NULL OR 	REVERSERSCycles=0.0) 
                                                                               AND STEP IS NULL
                                                                               ORDER BY TailNum
OPEN C1;
	FETCH NEXT FROM C1 INTO @TailNum1, @AircraftCD,@AIR_FRAME_HRS_LDS, @LoggedHours, @Cycles, @REVERSERSCycles,@CrDAY1,@CrDAY2,@CrDAY3,@CrDAY4,@CrDAY5,@CrDAY6,@CrDAY7,@CrDAY8,@CrDAY9,@CrDAY10,@CrDAY11,@CrDAY12,@CrDAY13,@CrDAY14,@CrDAY15,@crType_Code,@Fleet;
	WHILE @@FETCH_STATUS = 0
	BEGIN
	BEGIN
	UPDATE #TempFleetInfo SET STEP='#1' WHERE TailNum=@TailNum1
	INSERT INTO #TempFleetInfo(Fleet,TailNum,AircraftCD,AIR_FRAME_HRS_LDS,LoggedHours,Cycles,REVERSERSCycles,STEP,day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,Type_Code)
	VALUES(@Fleet,@TailNum1,@AircraftCD,@AIR_FRAME_HRS_LDS,@LoggedHours,@Cycles,@REVERSERSCycles,'#2',@CrDAY1,@CrDAY2,@CrDAY3,@CrDAY4,@CrDAY5,@CrDAY6,@CrDAY7,@CrDAY8,@CrDAY9,@CrDAY10,@CrDAY11,@CrDAY12,@CrDAY13,@CrDAY14,@CrDAY15,@crType_Code)
	
	  FETCH NEXT FROM C1 INTO @TailNum1, @AircraftCD,@AIR_FRAME_HRS_LDS, @LoggedHours, @Cycles, @REVERSERSCycles,@CrDAY1,@CrDAY2,@CrDAY3,@CrDAY4,@CrDAY5,@CrDAY6,@CrDAY7,@CrDAY8,@CrDAY9,@CrDAY10,@CrDAY11,@CrDAY12,@CrDAY13,@CrDAY14,@CrDAY15,@crType_Code,@Fleet;
	END
	END
	CLOSE C1;
	DEALLOCATE C1; 
	
	
DELETE FROM #TempFleetInfo WHERE Fleet IN (SELECT Fleet FROM #TempFleet) 
                                 AND day1=0.0 AND day2=0.0 AND day3=0.0 AND day4=0.0 AND day5=0.0 AND day6=0.0 
                                 AND day7=0.0 AND day8=0.0 AND day9=0.0 AND day10=0.0 AND day11=0.0 AND day12=0.0 
                                 AND day13=0.0 AND day14=0.0 AND day15=0.0 
                                 
                              



UPDATE #TempFleetInfo SET AIR_FRAME_HRS_LDS = ISNULL(AIR_FRAME_HRS_LDS,0)+POST2.POHOURS2 FROM (
	SELECT isnull(POST.[POHOURS],0) AS POHOURS2, #TempFleetInfo.TailNUM 
		FROM #TempFleetInfo
		  INNER JOIN
		 (
			 SELECT F.TailNUM,
			 [POHOURS] = CASE WHEN C.AircraftBasis = 1 THEN SUM(POL.BlockHours) ELSE SUM(POL.FlightHours) END
			 FROM
			 PostflightMain POM 
			 INNER JOIN PostflightLeg POL ON POM.POLOGID = POL.POLOGID
			 INNER JOIN Fleet F ON F.FleetID=POM.FleetID
			 LEFT OUTER JOIN FleetInformation FI ON F.FleetID=FI.FleetID 
			 INNER JOIN Company C ON POM.CustomerID = C.CustomerID AND POM.HomebaseID = C.HomebaseID
	     	 WHERE CONVERT(DATE,ScheduleDTTMLocal) BETWEEN
	     	                                         (CASE WHEN CONVERT(DATE,FI.AIrframeAsOfDT) IS NULL  THEN CONVERT(DATE,ScheduleDTTMLocal) ELSE CONVERT(DATE,FI.AIrframeAsOfDT) End)
	     	                                         AND CONVERT(DATE,@EstDepartureDT)
			 AND POM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			 GROUP BY  F.TailNUM, C.AircraftBasis 
		) POST ON #TempFleetInfo.TailNum = POST.TailNum
	) POST2	 
WHERE #TempFleetInfo.TailNum = POST2.TailNum
AND (#TempFleetInfo.STEP='#1')


UPDATE #TempFleetInfo SET AIR_FRAME_HRS_LDS =ISNULL(AIR_FRAME_HRS_LDS,0)+POST.LegID FROM (SELECT F.TailNUM,COUNT(POL.POLegID) LegID
			 FROM
			 PostflightMain POM 
			 INNER JOIN PostflightLeg POL ON POM.POLOGID = POL.POLOGID
			 INNER JOIN Fleet F ON F.FleetID=POM.FleetID
			 LEFT OUTER JOIN FleetInformation FI ON F.FleetID=FI.FleetID 
			 INNER JOIN Company C ON POM.CustomerID = C.CustomerID AND POM.HomebaseID = C.HomebaseID
		   	 WHERE CONVERT(DATE,ScheduleDTTMLocal) BETWEEN
	     	                                         (CASE WHEN CONVERT(DATE,FI.AIrframeAsOfDT) IS NULL  THEN CONVERT(DATE,ScheduleDTTMLocal) ELSE CONVERT(DATE,FI.AIrframeAsOfDT) End)
	     	                                         AND CONVERT(DATE,@EstDepartureDT)
			   AND POM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			 GROUP BY  F.TailNUM)POST
WHERE #TempFleetInfo.TailNum = POST.TailNum                                 
AND (#TempFleetInfo.STEP='#2')

                                                                           
---------------Suppere Activity--------------------

--SET @SuppressActivityAircft=1
 IF @SuppressActivityAircft=1 
BEGIN
DELETE FROM #TempFleetInfo WHERE (day1 IS NULL OR day1=0.000) 
                              AND (day2 IS NULL OR day2=0.000)
                              AND (day3 IS NULL OR day3=0.000)
                              AND (day4 IS NULL OR day4=0.000) 
                              AND (day5 IS NULL OR day5=0.000) 
                              AND (day6 IS NULL OR day6=0.000)
                              AND (day7 IS NULL OR day7=0.000)
                              AND (day8 IS NULL OR day8=0.000)
                              AND (day9 IS NULL OR day9=0.000)
                              AND (day10 IS NULL OR day10=0.000)
                              AND (day11 IS NULL OR day11=0.000)
                              AND (day12 IS NULL OR day12=0.000)
                              AND (day13 IS NULL OR day13=0.000) 
                              AND (day14 IS NULL OR day14=0.000)
                              AND (day15 IS NULL OR day15=0.000)
                              AND (AIR_FRAME_HRS_LDS IS NULL OR AIR_FRAME_HRS_LDS=0.0)
                              AND (Cycles IS NULL OR Cycles=0)
                              AND (REVERSERSCycles IS NULL OR  REVERSERSCycles=0)
                              AND (LoggedHours IS NULL OR LoggedHours=0.0)
END

ELSE
BEGIN


INSERT INTO #TempFleetInfo(TailNum,AircraftCD,Fleet,STEP,LoggedHours,AIR_FRAME_HRS_LDS,Type_Code)
SELECT A.* FROM 
(SELECT  DISTINCT F.TailNum, B.AircraftCD,F.FleetID,'#1' AS STEP,0.0 LoggedHours,0.0 AIR_FRAME_HRS_LDS,F.AircraftCD Aircraft
      FROM Fleet F LEFT OUTER JOIN Aircraft B ON F.AircraftID = B.AircraftID  
      WHERE  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
      AND F.IsDeleted=0  
      AND RTRIM(TailNum) NOT IN (SELECT RTRIM(TailNum) FROM #TempFleet) 
      
UNION 

SELECT DISTINCT F.TailNum, B.AircraftCD,F.FleetID Fleet,'#2' AS STEP,0.0 LoggedHours,0.0 AIR_FRAME_HRS_LDS,F.AircraftCD  Aircraft
	  FROM Fleet F  LEFT OUTER JOIN Aircraft B ON F.AircraftID = B.AircraftID
	  WHERE F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	  AND F.IsDeleted=0  
	  AND RTRIM(TailNum) NOT IN (SELECT RTRIM(TailNum) FROM #TempFleet))A
ORDER BY A.Aircraft
END


-----------------End Suppress Activity---------------



UPDATE #TempFleetInfo SET day1=NULL,day2=NULL,day3=NULL,day4=NULL,day5=NULL,day6=NULL,day7=NULL,day8=NULL,day9=NULL,day10=NULL,day11=NULL,
                           day12=NULL,day13=NULL,day14=NULL,day15=NULL
                       WHERE STEP IN('#2','#3','#4')

 
UPDATE #TempFleetInfo SET day1=ISNULL(day1,0),day2=ISNULL(day2,0),day3=ISNULL(day3,0),day4=ISNULL(day4,0),day5=ISNULL(day5,0),day6=ISNULL(day6,0),day7=ISNULL(day7,0),day8=ISNULL(day8,0),day9=ISNULL(day9,0),day10=ISNULL(day10,0),day11=ISNULL(day11,0),
                           day12=ISNULL(day12,0),day13=ISNULL(day13,0),day14=ISNULL(day14,0),day15=ISNULL(day15,0)
                       WHERE STEP IN('#1')







SELECT DISTINCT TailNum,AircraftCD,AIR_FRAME_HRS_LDS,STEP,LoggedHours,Cycles = ISNULL(Cycles,0),
       REVERSERSCycles = ISNULL(REVERSERSCycles,0),Type_Code, day1, day2, day3, day4,day5,
        day6,day7, day8, day9, day10,
        day11, day12, day13,day14,day15
       ,@TenToMin TenToMin, 1 Flag
       FROM #TempFleetInfo TI  
       INNER JOIN @TempFleetID TF ON TF.FleetID=TI.Fleet
       ORDER BY Type_Code,STEP ASC
      
IF OBJECT_ID('tempdb..#TempFleetInfo') IS NOT NULL
DROP TABLE #TempFleetInfo

IF OBJECT_ID('tempdb..#TempFleet') IS NOT NULL
DROP TABLE  #TempFleet 

END    


-- EXEC spGetReportPREAircraftStatusInformation 'JWILLIAMS_11', '2012-08-01','N46E'












GO


