	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor2ExportInformation]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spGetReportPassengerRequestor2ExportInformation]
	GO

	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO


	CREATE Procedure [dbo].[spGetReportPassengerRequestor2ExportInformation]
		@UserCD VARCHAR(30),
		@PassengerCD varchar(500)
	AS
	-- ===============================================================================================
	-- SPC Name: spGetReportPassengerRequestor2ExportInformation
	-- Author: SUDHAKAR J
	-- Create date: 28 Jun 2012
	-- Description: Get Passenger export Information for Report Passenger/Requestor II
	-- Revision History
	-- Date			Name		Ver		Change
	-- 29 Aug		Sudhakar	1.1		ClientID filter
	--
	-- ===============================================================================================
	SET NOCOUNT ON

	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(100);

	DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
	SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

		SELECT
			[paxname] = P.PassengerName
			,[paxcode] = P.PassengerRequestorCD
			--,[phone] = P.PhoneNum
			,[phone] = P.AdditionalPhoneNum --Business Phone
			,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
			,[emp_t_cng] = P.IsEmployeeType
			,[active] = P.IsActive
			,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
			,[dob] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[dlastleg] = CONVERT(VARCHAR,dbo.GetShortDateFormatByUserCD(@UserCD,POLOG.ScheduleDTTMLocal)) --Not sure from which table they are coming
			,[lognum] = CONVERT(VARCHAR,POLOG.LogNum) --lognum is there in POPAX & POLEGS
			,[ltop] = 'Unknown' --Not sure from which table they are coming
			,[norder] = 'Unknown' --norder is coming from POPAX.DBF
			--,[Passport] = ''                                           --Added for Mhtml
			--,[VISA] = ''                                               --Added for Mhtml

		FROM Passenger P
		LEFT OUTER JOIN (
			SELECT A.* FROM (
				SELECT RANK() OVER(PARTITION BY POP.PassengerID ORDER BY POL.ScheduleDTTMLocal DESC ) RNK
				 ,POP.PassengerID, POM.LogNum, POL.ScheduleDTTMLocal, POM.CustomerID
				FROM PostflightPassenger POP
				INNER JOIN PostflightLeg POL ON POP.POLegID = POL.POLegID AND POP.CustomerID = POL.CustomerID AND POL.IsDeleted=0
				INNER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID AND POL.CustomerID = POM.CustomerID AND POM.IsDeleted=0
			) A
			WHERE A.RNK = 1
		) POLOG ON P.CustomerID = POLOG.CustomerID AND P.PassengerRequestorID = POLOG.PassengerID  
		WHERE P.IsDeleted = 0
		AND P.CustomerID = @CUSTOMERID

	/*
	SET @SQLSCRIPT = '
		SELECT
			[paxname] = P.PassengerName
			,[paxcode] = P.PassengerRequestorCD
			,[phone] = P.PhoneNum
			,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
			,[emp_t_cng] = P.IsEmployeeType
			,[active] = P.IsActive
			,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
			,[dob] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
			,[dlastleg] = ''Unknown'' --Not sure from which table they are coming
			,[lognum] = ''Unknown'' --lognum is there in POPAX & POLEGS
			,[ltop] = ''Unknown'' --Not sure from which table they are coming
			,[norder] = ''Unknown'' --norder is coming from POPAX.DBF
			--,[Passport] = ''                                           --Added for Mhtml
			--,[VISA] = ''                                               --Added for Mhtml

		FROM Passenger P 
		WHERE P.IsDeleted = 0
		AND P.CustomerID = @CUSTOMERID
		'
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND P.PassengerRequestorCD IN (''' + REPLACE(CASE WHEN RIGHT(@PassengerCD, 1) = ',' THEN LEFT(@PassengerCD, LEN(@PassengerCD) - 1) ELSE @PassengerCD END, ',', ''', ''') + ''')';
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @CUSTOMERID BIGINT'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @CUSTOMERID
	*/

	-- EXEC spGetReportPassengerRequestor2ExportInformation 'tim', 'MAL01,HKIOE,EJM01,MT01,ASJ01,Cake,OPAC'


	GO


