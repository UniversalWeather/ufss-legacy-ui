IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQInvoiceMainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQInvoiceMainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQInvoiceMainInformation]
	@CQFileID VARCHAR(30),
	@QuoteNUM VARCHAR(30)
AS 
-- =============================================
-- SPC Name:spGetReportCQInvoiceMainInformation
-- Author: Aishwarya.M
-- Create date: 20 Apr 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN

  SELECT DISTINCT  
		InvoiceTotal =CQIS.InvoiceTotal,
		QuoteNumber = CM.QuoteNUM,
		CM.CQFileID
		
		FROM CQInvoiceMain CQIM
		INNER JOIN CQMain CM ON CQIM.CQMainID = CM.CQMainID AND CM.IsDeleted=0
		LEFT OUTER JOIN CQInvoiceQuoteDetail CQID ON CQID.CQInvoiceMainID = CQIM.CQInvoiceMainID
		LEFT OUTER JOIN CQInvoiceSummary CQIS ON CQIS.CQInvoiceMainID = CQIM.CQInvoiceMainID
		WHERE CM.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND (CM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')  
		ORDER BY CM.QuoteNUM	
END
	
	--EXEC spGetReportCQInvoiceMainInformation 100012,1		



GO


