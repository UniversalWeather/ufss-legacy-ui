IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightOperationsSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightOperationsSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- ===============================================================================
-- SPC Name: spGetReportPOSTFlightOperationsSummaryExportInformation
-- Author:Badrinath/mullai
-- Create date: 01 August 2012
-- Description: Get Flight Operation summary Export Information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPOSTFlightOperationsSummaryExportInformation]
@UserCD AS VARCHAR(30)-- Mandatory  
,@DATEFROM datetime-- Mandatory  
,@DATETO datetime-- Mandatory  
,@TailNumber nvarchar(1000)=''  
,@FleetGroupCD nvarchar(1000)=''  
,@IsHomebase As bit=0
,@SortBy as varchar(1)

AS  
 
SET NOCOUNT ON  
  
  
DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1

--SET ANSI_WARNINGS OFF  
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
  Declare @TenToMin Int
 Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
declare @units varchar(15) 
 set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'LTR'
				WHEN (cp.FuelPurchase='3') THEN 'IMP GAL'
				WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNumber <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNumber, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNumber = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER
	AND F.IsInActive = 0
	AND F.IsDeleted = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------	

CREATE TABLE #fleetexp (DateRange varchar(30),TailNumber varchar(9),typecode varchar(26),
POLogID bigint,NoofTrips bigint,NoofLogs bigint,NoofLegs bigint,NoofDays bigint,BlockHours numeric(6,3),
FlightHours numeric(6,3),TenToMin numeric(1,0),AvgBlkHrsLeg numeric(16,9),AvgFltHrs numeric(16,9),distance numeric(5,0),
AvgMILeg numeric(16,9),NoofPax numeric(5,0),FuelBurn numeric(7,0),AvgFBHr numeric(15,9),units varchar(15),
Gallons numeric(6,0),TotalCost numeric(17,5),AvgPrice numeric(15,5));

CREATE TABLE #fleetexp1 (DateRange varchar(30),TailNumber varchar(9),typecode varchar(26),
POLogID bigint,NoofTrips bigint,NoofLogs bigint,NoofLegs bigint,NoofDays bigint,BlockHours numeric(6,3),
FlightHours numeric(6,3),TenToMin numeric(1,0),AvgBlkHrsLeg numeric(16,9),AvgFltHrs numeric(16,9),distance numeric(5,0),
AvgMILeg numeric(16,9),NoofPax numeric(5,0),FuelBurn numeric(7,0),AvgFBHr numeric(15,9),units varchar(15),
Gallons numeric(6,0),TotalCost numeric(17,2),AvgPrice numeric(15,2));

CREATE TABLE #FLEETDAYS(TailNumber varchar(9),POLogID bigint,
--leg varchar(20),SCHEDULETM DATE,
minsche date,maxsche date);

CREATE TABLE #FLEETDAYS1(TailNumber varchar(9),POLogID bigint,
--leg varchar(20),SCHEDULETM DATE,
minsche date,maxsche date,NoofDays int);

INSERT INTO #FLEETDAYS
SELECT DISTINCT
[TailNumber] = F.TailNUM,  
[POLogID]=pl.pologid,
--leg=pl.polegid,
--ScheduleTM= PL.ScheduledTM,
minsche=min(pl.ScheduledTM),
maxsche=max(pl.ScheduledTM)


FROM PostflightLeg PL
INNER JOIN PostFlightMain PM ON PM.POLogID=PL.POLogID AND PM.IsDeleted = 0  
INNER JOIN (SELECT POLOGID,(datediff(day,min(ScheduledTM),MAX(ScheduledTM))+1) Ron1 FROM PostflightLeg GROUP BY POLogID)PL1 ON PM.POLogID=PL1.POLOGID

 INNER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      left outer JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      left outer JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                      WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
                                      OR @FleetGroupCD = ''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
                      WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND PL.IsDeleted = 0 
group by f.TailNum,PL.POLogID--,PL.POLegID,PL.ScheduledTM
  order by TailNumber                    
 


;WITH device_recording_row_num AS
(
    SELECT 
	ROW_NUMBER() OVER(ORDER BY A.TailNumber,A.minsche ) AS row_num,A.POLogID,A.minsche,A.maxsche,A.TailNumber
 FROM #FLEETDAYS A      
)


insert into #FLEETDAYS1 
SELECT
    [current].TailNumber,
    [current].POLogID,
    [current].minsche,
    [current].maxsche,
    (Case When [current].minsche=previous.maxsche then DateDiff(day,[current].minsche,[current].maxsche) 
          Else   DateDiff(day,[current].minsche,[current].maxsche)+1 End) as Noofdays
FROM 
    device_recording_row_num AS [current]
LEFT OUTER JOIN
    device_recording_row_num AS previous ON [current].TailNumber = previous.TailNumber AND [current].row_num = previous.row_num + 1

ORDER BY
    [current].TailNumber, [current].minsche

 --select * from #FLEETDAYS
 -- Select * from #FLEETDAYS1

INSERT INTO #fleetexp
SELECT a.DateRange,A.TailNumber,A.ac_code,A.POLogID,A.tripcount,A.logcount,sum(A.legcount) as NoofLegs,A.NoofDays,SUM(A.blk_hours) AS BlockHours,
SUM(A.FlightHours) AS FlightHours,a.TenToMin,SUM(A.AvgBlkHrsLeg) AS AvgBlkHrsLeg,SUM(A.AvgFltHrs) AS AvgFltHrs,SUM(A.distance) AS distance,
SUM(A.AvgMILeg) AS AvgMILeg,SUM(A.NoofPax) AS NoofPax,SUM(A.FuelBurn) AS FuelBurn,SUM(A.AvgFBHr) AS AvgFBHr,A.Units,SUM(A.Gallons) AS Gallons,SUM(A.TotalCost) AS TotalCost,
SUM(A.AvgPrice) AS AvgPrice FROM 
(SELECT  distinct
[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
[TailNumber] = F.TailNUM,  
[ac_code]=f.AircraftCD,
[POLogID]=pl.pologid, 
[tripcount]= (case when PM.TripID is null then COUNT(PM.LogNUM) else COUNT(PM.TripID) end),
[logcount] = COUNT(Pm.LogNUM),  
[legcount] = COUNT(PL.legnum),
[NoofDays] =PL1.Ron1,  
[blk_hours] = ISNULL(ROUND(PL.BlockHours,1),0),  
[FlightHours] = ISNULL(ROUND(PL.FlightHours,1),0),  
[TenToMin]=@TenToMin,
[AvgBlkHrsLeg] =  case(ISNULL(COUNT(PL.LegNUM),0)) when 0 then 0 ELSE ISNULL(ROUND(PL.BlockHours,1),0)/COUNT(PL.LegNUM) END ,	 
[AvgFltHrs] = case(ISNULL(COUNT(PL.LegNUM),0)) when 0 then 0 ELSE ISNULL(ROUND(PL.FlightHours,1),0)/COUNT(PL.LegNUM) END ,
[distance]=(CASE WHEN (@sortby='a') THEN PL.Distance
                 WHEN (@sortby='b') THEN PL.Distance*1.15078
                 WHEN (@sortby is null) THEN PL.Distance end),
[AvgMILeg]= case(ISNULL(COUNT(PL.TripLEgID),0)) when 0 then 0 ELSE ISNULL(PL.Distance,0)/COUNT(PL.TripLEgID) END ,  --IsNULL(PL.Distance/COUNT(PL.TripLEgID),0),  
[NoofPax] =PL.PassengerTotal,  
[FuelBurn] = PL.FuelUsed,  
[AvgFBHr] = case(ISNULL(ROUND(PL.FlightHours,1),0)) when 0 then 0 ELSE ISNULL(pl.FuelUsed,0)/ROUND(PL.FlightHours,1) END ,  --IsNULL(PL.FuelUsed/PL.FlightHours,0),  
[Units] = @units,
[Gallons]= case when (pe.PurchaseDT>= @DATEFROM or pe.PurchaseDT<=@DATETO) then (CAST(ROUND((dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))) end,  
[TotalCost]=PE.ExpenseAMT,

[AvgPrice]= Case WHEN Cp.FuelPurchase <> Pe.FuelPurchase and (ISNULL(pe.FuelQTY,0))<>0 THEN DBO.GetFuelPriceConversion(PE.FuelPurchase,ISNULL(pe.ExpenseAMT,0)/pe.FuelQTY,@UserCD)
                                               ELSE Pe.UnitPrice End

  
FROM PostFlightLeg PL  
INNER JOIN PostFlightMain PM ON PM.POLogID=PL.POLogID AND PM.IsDeleted = 0  
INNER JOIN (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
INNER JOIN (SELECT POLogID,NoofDays Ron1 FROM #FLEETDAYS1 GROUP BY POLogID,NoofDays)PL1 ON PM.POLogID=PL1.POLOGID 
LEFT OUTER JOIN PostFlightExpense PE ON PL.POLEgID=PE.POLEgID  

 INNER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      left outer JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      left outer JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                      --WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
                                      --OR @FleetGroupCD = ''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID

LEFT OUTER JOIN Company cp ON cp.HomebaseID = f.HomebaseID 
LEFT OUTER JOIN Airport a ON a.AirportID = cp.HomebaseAirportID 
left outer join UserMaster um on um.HomebaseID=pm.HomebaseID and um.CustomerID=PM.CustomerID
and um.UserName= @UserCD
WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
--AND (F.TailNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNumber, ',')) OR @TailNumber = '')
AND (PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
AND PM.IsDeleted = 0
group by F.TailNUM,pl.pologid,PL.PassengerTotal,PL.FlightHours,cp.TimeDisplayTenMin,PL.FuelUsed,cp.FuelPurchase,PL.StatuteMiles,PM.LogNum,PL.ScheduledTM,PL.NextDTTM,
PL.FuelIn,PL.FlightCost,PE.ExpenseAMT,PL.Distance,PE.FuelQTY,PL.BlockHours,PL.FlightHours,PL.InboundDTTM,PL.OutboundDTTM,PE.PurchaseDT,PL1.Ron1,PE.FuelPurchase,f.AircraftCD,
PE.UnitPrice,PM.TripID)A
GROUP BY A.TailNumber,A.POLogID,A.logcount,A.tripcount,a.TenToMin,A.DateRange,A.Units, A.Noofdays,A.ac_code
order by ac_code

insert into #fleetexp1
SELECT DISTINCT 
[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO), 
[TailNumber] = F.TailNUM, 
[ac_code]=f.AircraftCD, 
[POLogID]=NULL,
[tripcount]= NULL,  
[logcount] = NULL,  
[legcount] = 0,
[NoofDays] = NULL,  
[blk_hours] = 0,  
[FlightHours] = 0,
[TenToMin]=@TenToMin,  
[AvgBlkHrsLeg] =  0 ,	 
[AvgFltHrs] = 0,
[distance]=null,
[AvgMILeg]= NULL,  
[NoofPax] =NULL,  
[FuelBurn] =NULL,  
[AvgFBHr] = NULL, 
[Units] =@units,

[Gallons]= NULL,  
[TotalCost]=NULL,

[AvgPrice]= NULL

FROM Fleet f
INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = f.FleetID
LEFT OUTER JOIN Company cp ON cp.HomebaseID = f.HomebaseID 

        WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
       
order by ac_code
delete from #fleetexp1 where TailNumber in(select distinct TailNumber from #fleetexp)

IF @IsZeroSuppressActivityAircftRpt = 0
BEGIN         
SELECT * FROM #fleetexp1
UNION ALL
SELECT * FROM #fleetexp
END
ELSE
BEGIN
SELECT * FROM #fleetexp
END
   
IF OBJECT_ID('tempdb..#fleetexp') IS NOT NULL
DROP TABLE #fleetexp 	


IF OBJECT_ID('tempdb..#fleetexp1') IS NOT NULL
DROP TABLE #fleetexp1	


IF OBJECT_ID('tempdb..#FLEETDAYS') IS NOT NULL
DROP TABLE #FLEETDAYS 	

IF OBJECT_ID('tempdb..#FLEETDAYS1') IS NOT NULL
DROP TABLE #FLEETDAYS1	


 
 
-- EXEC spGetReportPOSTFlightOperationsSummaryExportInformation 'jwilliams_11','2011-01-01','2011-12-12','','',1,'b'


GO


