IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendar1MainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1MainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1MainInformation]
	 @UserCD VARCHAR(50)-- MANDATORY
	,@DateFrom DATETIME --MANDATORY
	,@DateTo DATETIME --MANDATORY
	,@TailNum VARCHAR(500) = ''--OPTIONAL
	,@FleetGroupCD VARCHAR(500) = ''--OPTIONAL
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPREWeeklyCalendar1MainInformation
-- Author: AISHWARYA.M
-- Create date: 23 Jul 2012
-- Description: Get Preflight Weekly Calendar1 information for Main Report
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================


SET NOCOUNT ON
--DROP TABLE #TempWeeklyInfo
--DROP TABLE #TempFleetID

	DECLARE @Count INT, @index INT;
	DECLARE @SQLSCRIPT NVARCHAR(MAX);
	Declare @SuppressActivityAircft BIT  
	DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @ParameterDefinition AS NVARCHAR(200);
	SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@Customer 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
		
	DECLARE @TempTailNum TABLE (
		ID int identity(1,1) Not Null,
		TailNum VARCHAR(25)
	)	
	CREATE TABLE #TempWeeklyInfo (
		WeekNumber int
		,[Date1] DATETIME
		,[Date2] DATETIME
		,[Date3] DATETIME
		,[Date4] DATETIME
		,[Date5] DATETIME
		,[Date6] DATETIME
		,[Date7] DATETIME
	)
	--SET @SuppressActivityAircft=1
	----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> '' AND @FleetGroupCD ='' 
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	  AND F.IsDeleted=0
END


IF @SuppressActivityAircft=1
BEGIN
DELETE FROM #TempFleetID WHERE FleetID NOT IN(
SELECT  PM.FleetID FROM  PreflightMain PM   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0  
											INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID  
											INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID     
											INNER JOIN (SELECT CustomerID, FleetID, TailNum, HomebaseID FROM Fleet)F ON PM.FleetID = F.FleetID  
										    INNER JOIN #TempFleetID TF ON PM.FleetID=TF.FleetID
											INNER JOIN Company CO ON CO.HomebaseID = F.HomebaseID  
											WHERE PM.CustomerID = F.CustomerID  
											  AND F.FleetID = PM.FleetID
											  AND PM.TripStatus IN('T','H')
											  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
											  AND PM.IsDeleted = 0
											UNION ALL 
														     
SELECT   PM.FleetID FROM PreflightMain PM  
					  INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
					   INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID  
					   INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID     
					   INNER JOIN (SELECT CustomerID, FleetID, TailNum, HomebaseID FROM Fleet)F ON PM.FleetID = F.FleetID  
					    INNER JOIN #TempFleetID TF ON PM.FleetID=TF.FleetID
					   INNER JOIN Company CO ON CO.HomebaseID = F.HomebaseID  
					   WHERE PM.CustomerID = F.CustomerID  
						AND F.FleetID = PM.FleetID
						AND PM.TripStatus='MX'
						AND PM.IsDeleted = 0
						AND (	
								(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
							  OR
								(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
							  OR
								(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
						))

END

-----------------------------TailNum and Fleet Group Filteration----------------------


SET @Count = DATEDIFF(DAY, @DateFrom, @DateTo) + 1
SET @index = 0
WHILE (@index < @Count)
	BEGIN
		INSERT INTO #TempWeeklyInfo (
			WeekNumber
			,[Date1]
			,[Date2]
			,[Date3]
			,[Date4]
			,[Date5]
			,[Date6]
			,[Date7]
		)
		VALUES ( 
			Cast((DATEDIFF(DAY,@DateFrom,DATEADD(DAY, @index, @DateFrom))/7)+1 as int)
			,DateAdd(DAY,0+@index, @DateFrom)
			,DateAdd(DAY,1+@index, @DateFrom)
			,DateAdd(DAY,2+@index, @DateFrom)
			,DateAdd(DAY,3+@index, @DateFrom)
			,DateAdd(DAY,4+@index, @DateFrom)
			,DateAdd(DAY,5+@index, @DateFrom)
			,DateAdd(DAY,6+@index, @DateFrom)
		)
			
		SET @index = @index + 7  
	END
	--SELECT * FROM #TempWeeklyInfo
				 		
		SET @SQLSCRIPT = '
				SELECT DISTINCT TWI.WeekNumber, F.TailNum, TWI.Date1,TWI.Date2,TWI.Date3,
				TWI.Date4,TWI.Date5,TWI.Date6,TWI.Date7, CONVERT(VARCHAR(30),F.FleetID) AS FleetID,F.AircraftCD 
				FROM (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
						--WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
						--OR @FleetGroupCD = ''''
				  ) F 
				  INNER JOIN  ( SELECT DISTINCT FLEETID FROM #TempFleetID ) F1  ON F1.FleetID = F.FleetID
				 CROSS JOIN #TempWeeklyInfo TWI
				 WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
				 --AND F.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD) 
			'
		--IF @TailNUM <> '' BEGIN  
		--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
		--END
		
		SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY TWI.WeekNumber, F.AircraftCD  '
		
		--PRINT @SQLSCRIPT
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD
		
 END	
 --EXEC spGetReportPREWeeklyCalendar1MainInformation 'JWILLIAMS_11', '2012-10-01', '2012-10-01', '', ''




IF OBJECT_ID('tempdb..#TempWeeklyInfo') IS NOT NULL
DROP TABLE #TempWeeklyInfo

IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE  #TempFleetID 





GO


