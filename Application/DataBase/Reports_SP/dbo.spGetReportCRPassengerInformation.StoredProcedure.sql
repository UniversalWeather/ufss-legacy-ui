IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRPassengerInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRPassengerInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCRPassengerInformation] (
	@UserCD AS VARCHAR(30),
	@CRTripNUM AS VARCHAR(50),
	@CRLegNum AS VARCHAR(50)
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportCRPassengerInformation
-- Author: Askar
-- Create date: 25 Apr 2013
-- Description: Get Passenger Information
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @CrPassenger VARCHAR(63),@ID INT=0;

DECLARE @PassTemp TABLE(    ID INT IDENTITY,
							Passenger1 VARCHAR(63),
							Passenger2 VARCHAR(63),
							Passenger3 VARCHAR(63)
                        )





DECLARE @CRTrip BIGINT;

SELECT DISTINCT @CRTrip=CRM.TripID FROM CRMain CRM INNER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.IsDeleted = 0
                            WHERE CRM.CustomerID=@CUSTOMERID
                              AND CRM.CRTripNUM=@CRTripNUM
                              AND CRM.IsDeleted = 0

IF @CRTrip IS NULL

BEGIN

DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR SELECT CASE WHEN P.LastName <>'' THEN  P.LastName+', ' ELSE '' END+ISNULL(P.FirstName,'')+' '+ISNULL(P.MiddleInitial,'') FROM CRMain CRM 
                                                     LEFT OUTER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.ISDELETED = 0
							                         INNER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID AND CRL.ISDELETED = 0
							                         INNER JOIN CRPassenger CRP ON CRP.CRLegID = CRL.CRLegID
							                         INNER JOIN Passenger P ON CRP.PassengerRequestorID=P.PassengerRequestorID
	                                           WHERE CRP.CustomerID=@CUSTOMERID
	                                             AND CRM.CRTripNUM=@CRTripNUM
	                                             AND CRL.LegNUM=@CRLegNum
	                                             AND CRM.ISDELETED = 0

OPEN C1


FETCH NEXT FROM C1 INTO @CrPassenger

WHILE @@FETCH_STATUS = 0
BEGIN

IF ((SELECT ID FROM @PassTemp WHERE ID=@ID AND Passenger3 IS NOT NULL) IS NOT NULL OR @ID=0)

	BEGIN
	 INSERT INTO @PassTemp(Passenger1) VALUES(@CrPassenger)
	END
ELSE IF (SELECT ID FROM @PassTemp WHERE ID=@ID AND Passenger2 IS NULL) IS NOT NULL
    BEGIN
     UPDATE @PassTemp SET Passenger2=@CrPassenger WHERE ID=@ID
    END
ELSE
   BEGIN
    UPDATE @PassTemp SET Passenger3=@CrPassenger WHERE ID=@ID
   END

SET @ID=SCOPE_IDENTITY()


FETCH NEXT FROM C1 INTO @CrPassenger
END
CLOSE C1;
DEALLOCATE C1;

END

ELSE

BEGIN



DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR SELECT CASE WHEN P.LastName <>'' THEN  P.LastName+', ' ELSE '' END+ISNULL(P.FirstName,'')+' '+ISNULL(P.MiddleInitial,'') FROM CRMain CRM 
                                                     INNER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.ISDELETED = 0 
							                         INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
							                         INNER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
							                         INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
	                                           WHERE PM.CustomerID=@CUSTOMERID
	                                             AND CRM.CRTripNUM=@CRTripNUM
	                                             AND PL.LegNUM=@CRLegNum
	                                             AND CRM.ISDELETED = 0

OPEN C1


FETCH NEXT FROM C1 INTO @CrPassenger

WHILE @@FETCH_STATUS = 0
BEGIN

IF ((SELECT ID FROM @PassTemp WHERE ID=@ID AND Passenger3 IS NOT NULL) IS NOT NULL OR @ID=0)

	BEGIN
	 INSERT INTO @PassTemp(Passenger1) VALUES(@CrPassenger)
	END
ELSE IF (SELECT ID FROM @PassTemp WHERE ID=@ID AND Passenger2 IS NULL) IS NOT NULL
    BEGIN
     UPDATE @PassTemp SET Passenger2=@CrPassenger WHERE ID=@ID
    END
ELSE
   BEGIN
    UPDATE @PassTemp SET Passenger3=@CrPassenger WHERE ID=@ID
   END

SET @ID=SCOPE_IDENTITY()


FETCH NEXT FROM C1 INTO @CrPassenger
END
CLOSE C1;
DEALLOCATE C1;


END

SELECT * FROM @PassTemp



END 
	--EXEC spGetReportCRPassengerInformation 'SUPERVISOR_99','3','1'



GO


