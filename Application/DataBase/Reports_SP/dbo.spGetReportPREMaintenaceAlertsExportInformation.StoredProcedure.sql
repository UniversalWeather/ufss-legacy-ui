IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREMaintenaceAlertsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREMaintenaceAlertsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPREMaintenaceAlertsExportInformation]  
 @UserCD AS VARCHAR(30)  
 ,@UserCustomerID AS VARCHAR(30)  
 ,@TailNum AS VARCHAR(100)   
 ,@FleetGroupCD AS VARCHAR(1000)  
  
AS   
-- ===============================================================================  
-- SPC Name: spGetReportPREMaintenaceAlertsExportInformation  
-- Author:  Aishwarya.M  
-- Create date: 27 Dec 2013  
-- Description: Get Maintenance Alerts Information For Exports  
-- Revision History  
-- Date   Name  Ver  Change  
-- ================================================================================  
SET NOCOUNT ON;  
BEGIN  
  
Declare @SuppressActivityAircft BIT    
    
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@UserCustomerID   
           AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
  
 declare @AircraftBasis numeric(1,0);  
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
  
----------------------------TailNum and Fleet Group Filteration------------------------  
DECLARE  @TempFleetID TABLE       
(     
ID INT NOT NULL IDENTITY (1,1),   
FleetID BIGINT,  
TailNum VARCHAR(9),  
AircraftCD VARCHAR(4)  
  )  
    
  
IF @TailNum <> ''  
BEGIN  
INSERT INTO @TempFleetID  
SELECT DISTINCT F.FleetID,F.TailNum ,F.AircraftCD  
FROM Fleet F  
WHERE F.CustomerID = @UserCustomerID  
AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  
END  
  
IF @FleetGroupCD <> ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID,F.TailNum,F.AircraftCD  
FROM Fleet F   
LEFT OUTER JOIN FleetGroupOrder FGO  
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
LEFT OUTER JOIN FleetGroup FG   
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
AND F.CustomerID = @UserCustomerID    
AND FG.IsDeleted=0  
END  
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID,F.TailNum,F.AircraftCD  
FROM Fleet F   
WHERE F.CustomerID = @UserCustomerID    
AND F.IsDeleted=0  
AND F.IsInActive=0  

END  
 
-------------------------------TailNum and Fleet Group Filteration----------------------  
  

SELECT DISTINCT  
 [flag] = 'H'  
 ,[tail_nmbr] = F.TailNum  
 ,[ac_code] = AC.AircraftCD  
 ,[lastinspdt] = F.LastInspectionDT  
 ,[insp_hrs] = F.InspectionHrs  
 ,[warn_hrs] = F.WarningHrs  
 ,[modyyy] = PL.ScheduledTM  
 ,[dispatchno] = PM.DispatchNum  
 ,[lognum] = PM.LogNum  
 ,[leg_num] = PL.LegNUM  
 ,[depicao_id] = AD.IcaoID  
 ,[arricao_id] = AA.IcaoID  
 ,[distance] = PL.Distance  
 ,[blk_hrs] = PL.BlockHours  
 ,[flt_hrs] = PL.FlightHours  
 ,[blk_hrs_i] = PL.BlockHours  
 ,[flt_hrs_i] = PL.FlightHours  
 ,[hrs_inspect] = F.InspectionHrs   
 ,[activity] = ''  
 ,[tot_line] = ''  
 ,[comment] = ''  
 ,F.FleetID  
 ,HoursVal = CONVERT(NUMERIC(10,3),NULL)
 ,InspectionHrs = CONVERT(NUMERIC(10,3),NULL)
 INTO #TripMaintenance
 FROM PostflightMain PM  
 INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0  
 INNER JOIN Fleet F ON PM.FleetID = F.FleetID  
 INNER JOIN @TempFleetID TF ON TF.FleetID = F.FleetID  
 LEFT OUTER JOIN Airport AD ON PL.ArriveICAOID = AD.AirportID  
 LEFT OUTER JOIN Airport AA ON PL.DepartICAOID = AA.AirportID  
 LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID  
 WHERE PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)  
 AND PM.IsDeleted = 0  
 AND F.LastInspectionDT IS NOT NULL  
 AND CONVERT(DATE,PL.ScheduledTM) >   CONVERT(DATE,F.LastInspectionDT) 
 

INSERT INTO  #TripMaintenance
SELECT DISTINCT  
 [flag] = 'P'  
 ,[tail_nmbr] = F.TailNum  
 ,[ac_code] = AC.AircraftCD  
 ,[lastinspdt] = F.LastInspectionDT  
 ,[insp_hrs] = F.InspectionHrs  
 ,[warn_hrs] = F.WarningHrs  
 ,[modyyy] = PL.DepartureDTTMLocal   
 ,[dispatchno] = PM.DispatchNum  
 ,[lognum] = PM.TripNUM  
 ,[leg_num] = PL.LegNUM  
 ,[depicao_id] = D.IcaoID  
 ,[arricao_id] = A.IcaoID  
 ,[distance] = PL.Distance  
 ,[blk_hrs] =PL.ElapseTM --FlightHours  
 ,[flt_hrs] =PL.ElapseTM --FlightHours  
 ,[blk_hrs_i] = PL.ElapseTM --FlightHours 
 ,[flt_hrs_i] = PL.ElapseTM --FlightHours  
 ,[hrs_inspect] = F.InspectionHrs   
 ,[activity] = ''  
 ,[tot_line] = ''  
 ,[comment] = ''  
 ,F.FleetID 
 ,POSTFLEET.HoursVal  
 ,InspectionHrs = POSTFLEET.InspectionHrs - POSTFLEET.HoursVal 
 FROM PreflightMain PM   
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
               INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
               INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID 
               LEFT OUTER JOIN Aircraft AC ON AC.AircraftID=F.AircraftID
               LEFT OUTER JOIN Airport D ON PL.DepartICAOID=D.AirportID  
               LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID  
               LEFT OUTER JOIN (    
        SELECT SUM(CASE WHEN @AircraftBasis=2 THEN FlightHours ELSE BlockHours END) HoursVal, InspectionHrs,F.FleetID   
          FROM PostflightMain PM   
             INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0  
             INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
          WHERE PM.CustomerID=@UserCustomerID  
           AND PM.IsDeleted=0  
            AND CONVERT(DATE,PL.ScheduledTM) >   CONVERT(DATE,F.LastInspectionDT)  
           GROUP BY F.FleetID, F.InspectionHrs)POSTFLEET ON POSTFLEET.FleetID=F.FleetID  
 WHERE  PM.IsDeleted = 0  
   AND CONVERT(DATE,PL.DepartureDTTMLocal)  > CONVERT(DATE,GETDATE())   
   AND F.LastInspectionDT IS NOT NULL  
   AND PM.TripStatus = 'T'
  
SELECT DISTINCT flag  
  ,tail_nmbr  
  ,ac_code  
  ,lastinspdt  
  ,insp_hrs  
  ,warn_hrs  
  ,modyyy  
  ,dispatchno  
  ,lognum  
  ,leg_num  
  ,depicao_id  
  ,arricao_id  
  ,distance  
  ,blk_hrs  
  ,flt_hrs  
  ,blk_hrs_i  
  ,flt_hrs_i  
  ,hrs_inspect  
  ,activity  
  ,tot_line  
  ,comment  
  ,AircraftBasis = @AircraftBasis 
  ,HoursVal  
  ,InspectionHrs
 FROM #TripMaintenance  
 UNION ALL  
SELECT DISTINCT 'H' AS flag  
  ,TailNum AS tail_nmbr  
  ,AircraftCD AS ac_code  
  ,NULL AS lastinspdt  
  ,0 AS insp_hrs  
  ,0 AS warn_hrs  
  ,NULL AS modyyy  
  ,NULL AS dispatchno  
  ,0 AS lognum  
  ,0 AS leg_num  
  ,NULL AS depicao_id  
  ,NULL AS arricao_id  
  ,0 AS distance  
  ,0 AS blk_hrs  
  ,0 AS flt_hrs  
  ,0 AS blk_hrs_i  
  ,0 AS flt_hrs_i  
  ,0 AS hrs_inspect  
  ,NULL AS activity  
  ,NULL AS tot_line  
  ,NULL AS comment  
  ,AircraftBasis = @AircraftBasis 
  ,HoursVal = 0  
  ,InspectionHrs = 0
  FROM @TempFleetID TF  
  WHERE @SuppressActivityAircft = 0  
   AND TF.FleetID NOT IN (SELECT ISNULL(FleetID,0) FROM #TripMaintenance) 
   ORDER BY flag,tail_nmbr,modyyy 
   
IF OBJECT_ID('tempdb..#TripMaintenance') IS NOT NULL
DROP TABLE  #TripMaintenance 
   
 --EXEC spGetReportPREMaintenaceAlertsExportInformation 'SUPERVISOR_99','10099','',''  
  
   
END  
  
  




GO


