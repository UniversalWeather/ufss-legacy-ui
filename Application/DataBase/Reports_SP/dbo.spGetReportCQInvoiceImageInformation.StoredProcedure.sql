IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQInvoiceImageInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQInvoiceImageInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportCQInvoiceImageInformation]
 @UserCD VARCHAR(30),
 @CQFileID VARCHAR(30),  
 @QuoteNUM VARCHAR(30) 
  
AS  
-- =============================================    
-- SPC Name: spGetReportCQInvoiceImageInformation    
-- Author: Aishwarya.M   
-- Create date: 20 Feb 2014    
-- Description: Get Report Image Information for Invoice    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- =============================================    
    
SET NOCOUNT ON    



--SELECT FW.UWAFilePath, FW.CustomerID,SP.SalesPersonCD  
--			FROM CQFile CQF  
--			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
--			  INNER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID 
--			  INNER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID
--			  INNER  JOIN Company C ON C.CustomerID = CQF.CustomerID AND C.HomebaseID=CQF.HomebaseID  
--			  INNER JOIN FileWarehouse FW ON FW.CustomerID=C.CustomerID
--			  INNER JOIN UserMaster UM ON UM.CustomerID=C.CustomerID AND UM.HomebaseID=C.HomebaseID AND RTRIM(UM.UserName)=RTRIM(@UserCD)
--			 WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)  
--			   AND CM.QuoteNUM = @QuoteNUM
--			   AND CQF.CustomerID=DBO.GetCustomerIDbyUserCD(@UserCD) 
--			   AND UWAWebpageName = 'SalesPersonCatalog.aspx' 
--			   AND FW.IsDeleted=0 
--				ORDER BY FW.LastUpdTS DESC  
  
IF NOT EXISTS(SELECT 1 	FROM CQFile CQF  
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
			  --INNER JOIN CQInvoiceMain CQI ON CM.CQMainID = CQI.CQMainID 
			  INNER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID AND SP.LogoPosition IN (1,2,3)
			  INNER  JOIN Company C ON C.CustomerID = CQF.CustomerID AND C.HomebaseID=CQF.HomebaseID  
			  INNER JOIN FileWarehouse FW ON FW.CustomerID=C.CustomerID AND SP.SalesPersonID = FW.RecordID
			  INNER JOIN UserMaster UM ON UM.CustomerID=C.CustomerID AND UM.HomebaseID=C.HomebaseID AND RTRIM(UM.UserName)=RTRIM(@UserCD)
			 WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)  
			   --AND CM.QuoteNUM = @QuoteNUM
			   AND (CM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')      
			   AND CQF.CustomerID=DBO.GetCustomerIDbyUserCD(@UserCD) 
			   AND UWAWebpageName = 'SalesPersonCatalog.aspx' 
			   AND FW.IsDeleted=0  )    
  
BEGIN  

    
  SELECT TOP 1    
  [Logo] = FW.UWAFilePath  
  FROM UserMaster u   
 INNER JOIN Company c ON u.CustomerID = c.CustomerID AND u.HomeBaseID = c.HomeBaseID      
 LEFT OUTER JOIN (SELECT TOP 1 F.UWAFilePath, F.CustomerID  
  FROM FileWarehouse F  
  WHERE UWAWebpageName = 'CompanyProfileCatalog.aspx'  
  AND F.RecordType = 'CqCompanyInvoice'
  AND F.IsDeleted = 0  
  AND F.CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)  
  AND F.RecordID = DBO.GetHomeBaseByUserCD(@UserCD)  
  ORDER BY LastUpdTS DESC  
 ) FW ON U.CustomerID = FW.CustomerID  
  
  WHERE RTRIM(u.UserName) = RTRIM(@UserCD)    
END  
 ELSE    
   
BEGIN  
    
  SELECT TOP 1    
  [Logo] = SP.UWAFilePath  
  FROM UserMaster u   
 INNER JOIN Company c ON u.CustomerID = c.CustomerID AND u.HomeBaseID = c.HomeBaseID      
 INNER JOIN (SELECT TOP 1 FW.UWAFilePath, FW.CustomerID 
			FROM CQFile CQF  
			  INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0 
			  --INNER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID 
			  INNER JOIN SalesPerson SP ON CQF.SalesPersonID=SP.SalesPersonID AND SP.LogoPosition IN (1,2,3)
			  INNER  JOIN Company C ON C.CustomerID = CQF.CustomerID AND C.HomebaseID=CQF.HomebaseID  
			  INNER JOIN FileWarehouse FW ON FW.CustomerID=C.CustomerID AND SP.SalesPersonID = FW.RecordID
			  INNER JOIN UserMaster UM ON UM.CustomerID=C.CustomerID AND UM.HomebaseID=C.HomebaseID AND RTRIM(UM.UserName)=RTRIM(@UserCD)
			 WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)  
			   --AND CM.QuoteNUM = @QuoteNUM
			   AND (CM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')      
			   AND CQF.CustomerID=DBO.GetCustomerIDbyUserCD(@UserCD) 
			   AND UWAWebpageName = 'SalesPersonCatalog.aspx' 
			   AND FW.IsDeleted=0 
			 ORDER BY FW.LastUpdTS DESC  
			) SP ON U.CustomerID = SP.CustomerID  
  WHERE RTRIM(u.UserName) = RTRIM(@UserCD)    
END  
  -- EXEC spGetReportCQInvoiceImageInformation 'SUPERVISOR_99'  
  



GO


