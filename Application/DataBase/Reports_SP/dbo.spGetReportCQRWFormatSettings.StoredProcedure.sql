IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWFormatSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWFormatSettings]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--  EXEC  spGetReportCQRWFormatSettings 1009965, 1, 'FBOPHONE::True||FBOCOMM::False' --ITINERARY
CREATE PROCEDURE [dbo].[spGetReportCQRWFormatSettings](
	@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table
	,@ReportNumber SMALLINT -- 1 = 'OVERVIEW', 2 = 'TRIPSHEET' etc
	,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'
) 
AS
BEGIN

IF OBJECT_ID('TEMPDB..#tblCQSettings') IS NOT NULL 
DROP TABLE #tblCQSettings

	--DECLARE @tblTSSettings AS TABLE  (
	CREATE TABLE #tblCQSettings (
		RecID INT NOT NULL IDENTITY (1,1)
		,CQReportHeaderID BIGINT
		,CQReportDetailID BIGINT
		,ReportNumber SMALLINT
		,ReportDescription VARCHAR(100)
		,ParameterCD VARCHAR(100) 
		,ParameterType VARCHAR(20)
		,ParameterCValue VARCHAR(20)
		,ParameterLValue BIT
		,ParameterNValue INT
		,ParameterValue VARCHAR(MAX)
	)

	INSERT INTO #tblCQSettings (
		CQReportHeaderID
		,CQReportDetailID
		,ReportNumber
		,ReportDescription
		,ParameterCD
		,ParameterType
		,ParameterCValue
		,ParameterLValue
		,ParameterNValue
		,ParameterValue
	) 
	SELECT CQReportHeaderID, CQReportDetailID, 
	ReportNumber, ReportDescription, ParameterCD, [ParamterType] = ParamterType,
	ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	FROM CQReportDetail
	WHERE CQReportHeaderID = CONVERT(BIGINT,@ReportHeaderID)
	AND ReportNumber = @ReportNumber

		IF OBJECT_ID('TEMPDB..#tblCQ') IS NOT NULL 
		DROP TABLE #tblCQ
		CREATE  TABLE #tblCQ(ParameterCD VARCHAR(100),ParamValue VARCHAR(MAX)
			--, ParameterType CHAR(1)
		)
		INSERT INTO #tblCQ
		SELECT ParameterCD, [ParamValue] = CASE ParameterType 
				WHEN 'L' THEN CONVERT(VARCHAR,ISNULL(ParameterLValue,0))
				WHEN 'N' THEN CONVERT(VARCHAR,ISNULL(ParameterNValue,0))
				WHEN 'M' THEN CONVERT(VARCHAR(MAX),ISNULL(ParameterValue,''))
				ELSE CONVERT(VARCHAR,ISNULL(ParameterCValue,'')) END
			--, ParameterType	
		FROM #tblCQSettings

		--[START TEMPSETTINGS UPDATE]-------------
		IF @TempSettings <> '' 
		BEGIN
			DECLARE @separator CHAR(2), @keyValueSeperator CHAR(2)
			SET @separator = '||'
			SET @keyValueSeperator = '::'

			DECLARE @separator_position INT , @keyValueSeperatorPosition INT
			DECLARE @match VARCHAR(MAX) 
			
			SET @TempSettings = @TempSettings + @separator
			
			WHILE PATINDEX('%' + @separator + '%' , @TempSettings) <> 0 
			BEGIN
				SELECT @separator_position =  PATINDEX('%' + @separator + '%' , @TempSettings)
				SELECT @match = LEFT(@TempSettings, @separator_position - 1)
				IF @match <> '' 
				BEGIN
					SELECT @keyValueSeperatorPosition = PATINDEX('%' + @keyValueSeperator + '%' , @match)
					IF @keyValueSeperatorPosition <> -1 
					BEGIN
						--INSERT @OutTable VALUES (LEFT(@match,@keyValueSeperatorPosition -1),RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition))
						--print LEFT(@match,@keyValueSeperatorPosition -1) + ' - ' + RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1)
						--UPDATE #tblCQ SET ParamValue = RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) WHERE ParameterCD = LEFT(@match,@keyValueSeperatorPosition -1)
						UPDATE #tblCQ SET ParamValue = CASE 
											WHEN RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) = 'True' THEN '1'
											WHEN RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) = 'False' THEN '0'
											ELSE RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) END
											WHERE ParameterCD = LEFT(@match,@keyValueSeperatorPosition -1)
					END
				END		
				SELECT @TempSettings = STUFF(@TempSettings, 1, @separator_position + 1, '')
			END
		END	
		--[END TEMPSETTINGS UPDATE]-------------

	-- Corrective meassure, reports are designed in a this way: 1 for HIDE and 0 fro DISPLAY
	--UPDATE #tblCQ SET ParamValue = CASE ParamValue WHEN '1' THEN '0' WHEN '0' THEN '1' ELSE ParamValue END
	--WHERE ParameterType = 'L'
	--ALTER TABLE #tblCQ DROP COLUMN ParameterType

--select * from #tblCQ

	DECLARE @columns VARCHAR(MAX)
	DECLARE @query VARCHAR(MAX)
	SELECT @columns = COALESCE(@columns + ',[' + cast(ParameterCD as varchar) + ']',
	'[' + cast(ParameterCD as varchar)+ ']')
	FROM #tblCQ

	SET @query = 'SELECT * FROM #tblCQ
	PIVOT ( MAX(ParamValue) FOR [ParameterCD] IN (' + @columns + ') ) AS P'

	EXECUTE(@query)
END
--  EXEC  spGetReportCQRWFormatSettings 1009965, 1, '' --ITINERARY
--  EXEC  spGetReportCQRWFormatSettings 1009965, 1, 'FBOPHONE::True||FBOCOMM::False' --ITINERARY


GO


