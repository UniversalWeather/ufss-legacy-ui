IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFleetCurrentAirpotCD]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFleetCurrentAirpotCD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Function Name: GetFleetCurrentAirpotCD
-- Author: SUDHAKAR J
-- Create date: 20 Aug 2012
-- Description: Returns AirportCD (ICAOID) in where the given Fleet is located on the given date
-- Else will return the HomebaseCD of that Fleet
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetFleetCurrentAirpotCD] (@ActiveDate datetime, @FleetID BIGINT)
RETURNS @returnAirport TABLE (
	[FleetID] [bigint] NOT NULL,
	[AirportID] [bigint] NOT NULL,
	[IcaoID] [char](4) NULL,
	[CustomerID] [bigint] NULL,
	[CityName] [varchar](25) NULL,
	[StateName] [varchar](25) NULL,
	[CountryName] [varchar](60) NULL,
	[CountryID] [bigint] NULL,
	[AirportName] [varchar](25) NULL
	)
AS
BEGIN

	DECLARE @AirportID BIGINT;

	IF EXISTS (	SELECT TOP 1 PL.ArriveICAOID
			FROM PreflightLeg PL
			INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID
			WHERE PM.FleetID = @FleetID
			AND CONVERT(DATE, ArrivalDTTMLocal) <= CONVERT(DATE, @ActiveDate)
			AND PM.TripStatus = 'T'
			ORDER BY ArrivalDTTMLocal DESC
		)
	BEGIN
		SET @AirportID = (
				SELECT TOP 1 PL.ArriveICAOID
					FROM PreflightLeg PL
					INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID
					WHERE PM.FleetID = @FleetID
					AND CONVERT(DATE, ArrivalDTTMLocal) <= CONVERT(DATE, @ActiveDate)
					AND PM.TripStatus = 'T'
					ORDER BY ArrivalDTTMLocal DESC
			)
		
		--SELECT @AirportCD
	END	
	ELSE
	BEGIN
		SELECT @AirportID = C.HOMEBASEAIRPORTID 
		FROM FLEET F INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
		WHERE F.FleetId = @FleetID
	END
	--RETURN @AirportID
	
	INSERT @returnAirport
	SELECT 
		@FleetID, AirportID, IcaoID, CustomerID, CityName, StateName, 
		CountryName, CountryID, AirportName 
		FROM AIRPORT WHERE AIRPORTid = @AirportID
	
	RETURN
END;

GO


