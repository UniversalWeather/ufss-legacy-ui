IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclaration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclaration]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
Create PROCEDURE [dbo].[spGetReportPRETSGeneralDeclaration] 
 @LegID AS VARCHAR(30 ) 
AS 
-- ========================================================================================== 
-- SPC Name: spGetReportPRETSGeneralDeclaration 
-- Author: ABHISHEK.S 
-- Create date: 13th September 2012 
-- Description: Get TripSheetReportWriter General Declaration Information For Reports 
-- Revision History 
-- Date     Name           Ver  Change 
-- 8 Sept   Dhruva Singh   N/A  Added NationalityCountryID for fleet table
-- ========================================================================================== 
BEGIN 
  
SET NOCOUNT ON;  
  
 

  
 
SELECT DISTINCT   F.TailNum 
   ,PL .FlightNUM 
   ,Dep =D. IcaoID 
   ,DepCity =D. CityName 
   ,DepDate= DATENAME(D ,PL. DepartureDTTMLocal)+UPPER (LEFT(DATENAME( M,PL .DepartureDTTMLocal), 3))+DATENAME (YEAR, PL.DepartureDTTMLocal ) 
   ,DepUTC =REPLACE( SUBSTRING (CONVERT( VARCHAR(30 ),PL. DepartureGreenwichDTTM,113 ),13, 5),':' ,'')  
   ,DepLocal =REPLACE( SUBSTRING (CONVERT( VARCHAR(30 ),PL. DepartureDTTMLocal,113 ),13, 5),':' ,'')   
   ,Arr =A. IcaoID 
   ,ArrCity =A. CityName 
   ,ArrDate= DATENAME(D ,PL. DepartureDTTMLocal)+UPPER (LEFT(DATENAME( M,PL .DepartureDTTMLocal), 3))+DATENAME (YEAR, PL.DepartureDTTMLocal ) 
   ,ArrUTC =REPLACE( SUBSTRING (CONVERT( VARCHAR(30 ),PL. ArrivalGreenwichDTTM,113 ),13, 5),':' ,'')
   ,ArrLocal =REPLACE( SUBSTRING (CONVERT( VARCHAR(30 ),PL. ArrivalDTTMLocal,113 ),13, 5),':' ,'')   
   ,PL .LegNUM 
   ,PM .TripNUM
   ,PL .LegID
   ,C .CountryName [Nationality]
    FROM PreflightMain PM  
     INNER JOIN PreflightLeg PL ON PM .TripID= PL.TripID 
     INNER JOIN Fleet F ON F .FleetID= PM.FleetID 
     LEFT JOIN Airport D ON D .AirportID= PL.DepartICAOID 
     LEFT JOIN Airport A ON A .AirportID= PL.ArriveICAOID 
        LEFT JOIN Country C ON C .CountryId= F.NationalityCountryID
      WHERE PL. LegID= CONVERT(BIGINT ,@LegID)
    
  
        
END 
-- EXEC spGetReportPRETSGeneralDeclaration 'TIM' , '20' , '' 
 
 -- EXEC  spGetReportPRETSGeneralDeclaration 'TIM', '', '', '', '2011-07-20', '2012-07-21', '', '', '', '', '', '', '',1,1,1,1,1,1   
