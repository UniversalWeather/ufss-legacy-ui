IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTripDatesByTripIDUserCD]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetTripDatesByTripIDUserCD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Function Name: GetTripDatesByTripIDUserCD
-- Author: SUDHAKAR J
-- Create date: 24 Jul 2012
-- Description: 
-- Returns Trip Dates ['Dept of First Leg' - 'Arrv of Last Leg'] for a given TRIPID
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetTripDatesByTripIDUserCD] 
(
	@TripID BIGINT,
	@UserCD char(30)
)
RETURNS VARCHAR(23)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @DTFirstLegDept AS DATETIME;
	DECLARE @DTLastLegArrv AS DATETIME;
	DECLARE @DateFormat AS INT = dbo.GetReportDayMonthFormatByUserCD(@UserCD)
	
	SELECT TOP 1 @DTFirstLegDept =  DepartureDTTMLocal FROM PreflightLeg
								WHERE TripID = @TripID ORDER BY LegNum ASC
	--DepartureDTTMLocal FROM PreflightLeg WHERE LegNum = (SELECT MIN(LegNum) FROM PreflightLeg WHERE TripID = @TripID) AND TripID = @TripID
	
	SELECT TOP 1 @DTLastLegArrv = ArrivalDTTMLocal FROM PreflightLeg
								WHERE TripID = @TripID ORDER BY LegNum DESC
	--ArrivalDTTMLocal FROM PreflightLeg WHERE LegNum = (SELECT MIN(LegNum) FROM PreflightLeg WHERE TripID = @TripID) AND TripID = @TripID
	
    RETURN ISNULL([dbo].[GetShortDateFormatByUserCD](@UserCD,@DTFirstLegDept), '') + ' - ' 
				+ ISNULL([dbo].[GetShortDateFormatByUserCD](@UserCD,@DTLastLegArrv), '');
	
   -- RETURN ISNULL(CONVERT(varchar(10), @DTFirstLegDept, @DateFormat), '') + ' - ' 
			--+ ISNULL(CONVERT(varchar(10), @DTLastLegArrv, @DateFormat), '');
END;


---SELECT [dbo].[GetTripDatesByTripIDUserCD] (10013147,'jwilliams_13')


---SELECT * FROM PreflightLeg WHERE CUSTOMERID=dbo.GetCustomerIDbyUserCD('ELIZA_3') AND TripNUM < 4000

GO


