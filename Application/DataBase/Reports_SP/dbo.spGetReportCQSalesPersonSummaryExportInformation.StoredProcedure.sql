IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQSalesPersonSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQSalesPersonSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCQSalesPersonSummaryExportInformation]
     
    @UserCD AS VARCHAR(30),
    @UserCustomerID BIGINT,
	@CQCustomerCD VARCHAR(1000)='',
	@DateFrom DATETIME, --MANDATORY  
    @DateTo DATETIME, --MANDATORY 
    @SalesPersonCD VARCHAR(1000)=''

AS

BEGIN

SET NOCOUNT ON;


 SELECT
       SP.SalesPersonCD,
       CONVERT(DATE,CF.EstDepartureDT) AS estdepdt,
       CONVERT(DATE,CF.QuoteDT) AS quotedt,
       CF.FileNUM  AS cqfilenum,
       CQC.CQCustomerCD,
       CQC.CQCustomerName,
       CM.CQMainID AS [QuoteID],
       CM.QuoteNUM AS quotenum,
       CASE WHEN CM.TripID IS NOT NULL THEN CM.QuoteTotal ELSE 0 END AS totquote,
       CM.LiveTrip,
       PM.TripNUM AS orig_nmbr,
       CASE WHEN LastName<>'' AND LastName IS NOT NULL THEN LastName+', ' ELSE '' END + ISNULL(FirstName,'') AS salepernm,
       CASE WHEN PM.IsLog IS NULL THEN 'FALSE' ELSE 'TRUE' END fulltrip,
       1 AS nquote, 
       ISNULL(PM.IsLog,0) AS nbooked
        
       FROM CQMain CM INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID
			          INNER JOIN SalesPerson SP ON SP.SalesPersonID = CF.SalesPersonID
			          INNER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID
			          LEFT OUTER JOIN PreflightMain PM  ON PM.TripID = CM.TripID
			WHERE CF.QuoteDT BETWEEN @DateFrom And @DateTo 
		   AND CF.CustomerID = @UserCustomerID
		   AND (SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ','))OR @SalesPersonCD='')
           AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')  
           AND SP.IsDeleted=0      
           ORDER BY SalesPersonCD,CQCustomerCD,CQCustomerName
            
END




GO


