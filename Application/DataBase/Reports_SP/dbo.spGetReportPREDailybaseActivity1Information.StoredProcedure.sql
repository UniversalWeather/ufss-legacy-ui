IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity1Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- ===============================================================================
-- SPC Name: spGetReportPREDailybaseActivity1Information
-- Author: Abhishek S
-- Create date: 25 Jul 2012
-- Description: Get Daily base Activity1 Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 07/26/2012  ABHISHEK             Logic
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity1Information]
		@UserCD AS VARCHAR(30), -- Mandatory
		@DATEFROM AS DATETIME,  -- Mandatory
		@DATETO AS DATETIME,    -- Mandatory
		@IcaoID AS VARCHAR(4),  -- Mandatory
		@TripNum AS NVARCHAR(500) = '', -- [Optional]
		@TailNum AS NVARCHAR(30) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(30) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0
AS

BEGIN
SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------
IF OBJECT_ID('TEMPDB..#TempFleetID') IS NOT NULL DROP TABLE #TempFleetID

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted = 0
	AND F.IsInActive = 0
END
-----------------------------TailNum and Fleet Group Filteration----------------------
  DECLARE @SQLSCRIPT AS  NVARCHAR(4000) = '';
  DECLARE @ParameterDefinition AS NVARCHAR(500)
        
	--DECLARE @IcaoID_N BIGINT
	--SET @IcaoID_N=(SELECT AirportID FROM Airport,PreflightLeg PL WHERE ICAOID=@IcaoID
	--															   AND PL.DepartICAOID=Airport.AirportID
	--															   AND PL.ArriveICAOID=Airport.AirportID
	--															   AND AIRPORT.IsDeleted = 0)
			
 SET @SQLSCRIPT = '
         SELECT
         [DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ ''-'' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) 
		--,[DepartureDT] = CONVERT(varchar(10), PL.DepartureDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[DepartureDT] = dbo.GetShortDateFormatByUserCD(@UserCD,PL.DepartureDTTMLocal)
		,[TripNUM] = PM.TripNUM
		,[TS] = PM.TripStatus
		,[LegID] = PL.LegID
		,[TailNUM] = FS.TailNUM	
		,[Activity] = CASE WHEN (A.AirportID = PL.DepartICAOID AND A.IcaoID = @IcaoID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END
		,[Departure] = CASE WHEN (A.AirportID = PL.DepartICAOID AND A.IcaoID = @IcaoID)  THEN
		                     CONVERT(varchar(10),A.IcaoID) + ''  '' + CONVERT(varchar(5), PL.DepartureDTTMLocal , 108)
					   ELSE
					         CONVERT(varchar(10),B.IcaoID) + ''  '' + CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108) 
				       END	
	    		       				
		,[Pax] = PL.PassengerTotal
		
		,[Activity2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND B.IcaoID = @IcaoID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END
		,[Arrival] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND A.IcaoID = @IcaoID) THEN		                   
						   CONVERT(varchar(10),B.IcaoID) + ''  '' + CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108)
					 ELSE
					       CONVERT(varchar(10),A.IcaoID) + ''  '' + CONVERT(varchar(5), PL.DepartureDTTMLocal , 108) 
				     END			
		,[Passengers] =  PAX.PaxNames
		
		 FROM PreflightMain PM 
			INNER JOIN PreflightLeg PL				 
				  ON PM.TripID = PL.TripID AND PL.ISDELETED = 0		  
			LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A
			      ON A.AirportID = PL.DepartICAOID
		    LEFT OUTER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS B
			      ON B.AirportID = PL.ArriveICAOID					      
	--		INNER JOIN PreflightPassengerList PP
	--		      ON PL.LegID = PP.LegID
			LEFT OUTER JOIN (
					SELECT DISTINCT LegID, PaxNames
						FROM PreflightPassengerList p1
						CROSS APPLY ( 
							SELECT IsNull(P.LastName,'''') + '','' + IsNull(P.FirstName,'''') + '' '' + IsNull(P.MiddleInitial,'''') + ''; '' 
							FROM PreflightPassengerList p2
							left JOIN Passenger P ON p2.PassengerID = P.PassengerRequestorID
							WHERE p2.LegID = p1.LegID AND p2.FlightPurposeID is Not NULL
							ORDER BY P.LastName
							FOR XML PATH('''') 
						) D ( PaxNames )
				) PAX ON PL.LegID = PAX.LegID 
		 INNER JOIN(
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
		) FS ON PM.FleetID = FS.FleetID AND PM.CustomerID = FS.CustomerID
		INNER JOIN #TempFleetID TF ON TF.FleetID=FS.FleetID
	    WHERE PM.IsDeleted = 0
			AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		--	AND (PL.DepartICAOID = A.AirportID OR PL.ArriveICAOID = B.AirportID)
		--	AND PM.EstDepartureDT BETWEEN @DATEFROM AND @DATETO	
			AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)						
			AND PM.TripStatus = ''T'' 
			AND PM.RecordType <> ''M''
			AND (A.AirportID IN(SELECT AirportID FROM Airport WHERE ICAOID=@IcaoID AND IsDeleted = 0)
			 OR B.AirportID IN (SELECT AirportID FROM Airport WHERE ICAOID=@IcaoID AND IsDeleted = 0))
			 
		 	'	
		 	
		 	 
		 	
   --Construct OPTIONAL clauses
   
    IF @TripNum <> '' BEGIN  
		   SET @SQLSCRIPT = @SQLSCRIPT + ' AND CONVERT(VARCHAR,PM.TripNUM) IN (''' +REPLACE(CASE WHEN RIGHT(@TripNum, 1) = ',' THEN LEFT(@TripNum, LEN(@TripNum) - 1) ELSE @TripNum END, ',', ''', ''') + ''')';
	     --  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM  = @TripNum'
	END  
    IF @TailNum <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND FS.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
	SET @SQLSCRIPT = @SQLSCRIPT +'ORDER BY PL.DepartureDTTMLocal,Departure'
END
	--PRINT @SQLSCRIPT
    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME,@IcaoID AS VARCHAR(4) ,@FleetGroupCD AS NVARCHAR(30) '
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO,@IcaoID, @FleetGroupCD

 -- EXEC spGetReportPREDailybaseActivity1Information 'uc','2001-07-20','2012-07-22','KHOU',''
 -- EXEC spGetReportPREDailybaseActivity1Information 'JWILLIAMS_13','2012-08-31','2012-09-04','KDAL',''










GO


