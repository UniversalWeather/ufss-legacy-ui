IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAdhocInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAdhocInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[spGetReportPOSTAdhocInformation]    
(
	 --@UserCD AS VARCHAR(30), --Mandatory
	 @UserCustomerID AS VARCHAR(30), --Mandatory
	 @PoLogIDs AS VARCHAR(MAX),
	 @IsLeg AS BIT=0, 
	 @IsPax AS BIT=0, 
	 @IsCrew AS BIT=0,
	 @IsExpense AS BIT=0  
	 
)      
AS
-- ================================================================================  
-- SPC Name: spGetReportPOSTAdhocInformation       
-- Author: Askar/Aishwarya          
-- Create date: 17/05/2013         
-- Description: To Get the Post Flight Details
-- Revision History         
-- Date   Name  Ver  Change         
-- ================================================================================  
BEGIN     
SET NOCOUNT ON  

DECLARE @PoLogIDTable TABLE ( PoLogID BIGINT )
INSERT INTO @PoLogIDTable (PoLogID) EXEC spSplitText @PoLogIDs  
--SELECT * FROM @PoLogIDTable
  
SELECT DISTINCT --------------PostFlightMain------------------
          --LogID=PM.POLogID
          --CustomerID = PM.CustomerID
			LogNum=PM.LogNum
		  --,TripID=PM.TripID
		  ,MainDescription=PM.POMainDescription
		  ,DispatchNum=PM.DispatchNum
		  ,EstDepartureDT=PM.EstDepartureDT
		  ,RecordType=PM.RecordType
		  --,FleetID=PM.FleetID
		  ,LogRequestorCD=LReq.PassengerRequestorCD
		  ,LogRequestorName=PM.RequestorName
		  ,DepartmentDescription=PM.DepartmentDescription
		  ,AuthorizationDescription=PM.AuthorizationDescription
		  --,ClientID=PM.ClientID
		  --,HomebaseID=PM.HomebaseID
		  ,IsPersonal=PM.IsPersonal
		  ,IsException=PM.IsException
		  ,Notes=PM.Notes
		  ,FlightNum=PM.FlightNum
		  ,IsCompleted=PM.IsCompleted
		  ,History=PM.History
		  ,TechLog=PM.TechLog
		  --,AircraftID=PM.AircraftID
		  ,IsLastAdj=PM.IsLastAdj
		  --,AccountID=PM.AccountID
		  --,DepartmentID=PM.DepartmentID
		  --,AuthorizationID=PM.AuthorizationID
		  ,AccountNum=MAC.AccountNum
		  ,LogDepartmentCD=MD.DepartmentCD
		  ,LogAuthorizationCD=MDA.AuthorizationCD
		  ,ClientCD=MC.ClientCD
		  ,TripNUM=PRM.TripNUM
		  ,TailNum=MF.TailNum
		  ,AircraftCD=MAT.AircraftCD
		  ,HomeBaseCD=MA.IcaoID
  --------------PostFlightLeg----------------------
      --  ,LegID=CASE WHEN @IsLeg=1 THEN PL.POLegID 
						--WHEN @IsExpense=1 THEN PE.POLegID 
						--WHEN @IsPax=1 THEN PP.POLegID 
						--WHEN @IsCrew = 1 THEN PC.POLegID
						--ELSE NULL END
		  --,LogID=CASE WHEN @IsLeg=1 THEN PL.POLogID ELSE NULL END
		  ,LegNUM=CASE WHEN @IsLeg=1 THEN PL.LegNUM ELSE NULL END
		  ,TripLegID=CASE WHEN @IsLeg=1 THEN PL.TripLegID ELSE NULL END
		  ,DepartICAO=CASE WHEN @IsLeg=1 THEN LDEP.IcaoID ELSE NULL END
		  ,ArriveICAO=CASE WHEN @IsLeg=1 THEN LARR.IcaoID ELSE NULL END
		  ,Distance=CASE WHEN @IsLeg=1 THEN PL.Distance ELSE NULL END
		  ,ScheduledTM=CASE WHEN @IsLeg=1 THEN PL.ScheduledTM ELSE NULL END
		  ,BlockOut=CASE WHEN @IsLeg=1 THEN PL.BlockOut ELSE NULL END
		  ,BlockIN=CASE WHEN @IsLeg=1 THEN PL.BlockIN ELSE NULL END
		  ,TimeOff=CASE WHEN @IsLeg=1 THEN PL.TimeOff ELSE NULL END
		  ,TimeOn=CASE WHEN @IsLeg=1 THEN PL.TimeOn ELSE NULL END
		  ,BlockHours=CASE WHEN @IsLeg=1 THEN ROUND(PL.BlockHours,1) ELSE NULL END
		  ,FlightHours=CASE WHEN @IsLeg=1 THEN ROUND(PL.FlightHours,1) ELSE NULL END
		  ,FedAviationRegNum=CASE WHEN @IsLeg=1 THEN PL.FedAviationRegNum ELSE NULL END
		  --,ClientID=CASE WHEN @IsLeg=1 THEN PL.ClientID ELSE NULL END
		  ,FlightPurpose=CASE WHEN @IsLeg=1 THEN PL.FlightPurpose ELSE NULL END
		  ,DutyTYPE=CASE WHEN @IsLeg=1 THEN PL.DutyTYPE ELSE NULL END
		  ,CrewCurrency=CASE WHEN @IsLeg=1 THEN PL.CrewCurrency ELSE NULL END
		  ,IsDutyEnd=CASE WHEN @IsLeg=1 THEN PL.IsDutyEnd ELSE NULL END
		  ,FuelOut=CASE WHEN @IsLeg=1 THEN PL.FuelOut ELSE NULL END
		  ,FuelIn=CASE WHEN @IsLeg=1 THEN PL.FuelIn ELSE NULL END
		  ,FuelUsed=CASE WHEN @IsLeg=1 THEN PL.FuelUsed ELSE NULL END
		  ,FuelBurn=CASE WHEN @IsLeg=1 THEN PL.FuelBurn ELSE NULL END
		  ,DelayTM=CASE WHEN @IsLeg=1 THEN PL.DelayTM ELSE NULL END
		  ,DutyHrs=CASE WHEN @IsLeg=1 THEN (CASE WHEN PL.IsDutyEnd=1 THEN ROUND(PC.DutyHours,1) ELSE 0 END) ELSE NULL END
		  ,RestHrs=CASE WHEN @IsLeg=1 THEN PL.RestHrs ELSE NULL END
		  ,NextDTTM=CASE WHEN @IsLeg=1 THEN PL.NextDTTM ELSE NULL END
		  ,DepatureFBO=CASE WHEN @IsLeg=1 THEN PL.DepatureFBO ELSE NULL END
		  ,ArrivalFBO=CASE WHEN @IsLeg=1 THEN PL.ArrivalFBO ELSE NULL END
		  --,CrewID=CASE WHEN @IsLeg=1 THEN PL.CrewID ELSE NULL END
		  --,IsException=CASE WHEN @IsLeg=1 THEN PL.IsException ELSE NULL END
		  ,IsAugmentCrew=CASE WHEN @IsLeg=1 THEN PL.IsAugmentCrew ELSE NULL END
		  ,BeginningDutyHours=CASE WHEN @IsLeg=1 THEN PL.BeginningDutyHours ELSE NULL END
		  ,EndDutyHours=CASE WHEN @IsLeg=1 THEN PL.EndDutyHours ELSE NULL END
		  ,PassengerTotal=CASE WHEN @IsLeg=1 THEN PL.PassengerTotal ELSE NULL END
		  ,ScheduleDTTMLocal=CASE WHEN @IsLeg=1 THEN PL.ScheduleDTTMLocal ELSE NULL END
		  ,BlockOutLocal=CASE WHEN @IsLeg=1 THEN PL.BlockOutLocal ELSE NULL END
		  ,BlockInLocal=CASE WHEN @IsLeg=1 THEN PL.BlockInLocal ELSE NULL END
		  ,TimeOffLocal=CASE WHEN @IsLeg=1 THEN PL.TimeOffLocal ELSE NULL END
		  ,TimeOnLocal=CASE WHEN @IsLeg=1 THEN PL.TimeOnLocal ELSE NULL END
		  ,OutboundDTTM=CASE WHEN @IsLeg=1 THEN PL.OutboundDTTM ELSE NULL END
		  ,InboundDTTM=CASE WHEN @IsLeg=1 THEN PL.InboundDTTM ELSE NULL END
		  ,AdditionalDist=CASE WHEN @IsLeg=1 THEN PL.AdditionalDist ELSE NULL END
		  ,AVTrak=CASE WHEN @IsLeg=1 THEN PL.AVTrak ELSE NULL END
		  --,FlightNum=CASE WHEN @IsLeg=1 THEN PL.FlightNum ELSE NULL END
		  ,CargoOut=CASE WHEN @IsLeg=1 THEN PL.CargoOut ELSE NULL END
		  ,CargoIn=CASE WHEN @IsLeg=1 THEN PL.CargoIn ELSE NULL END
		  ,CargoThru=CASE WHEN @IsLeg=1 THEN PL.CargoThru ELSE NULL END
		  ,StatuteMiles=CASE WHEN @IsLeg=1 THEN PL.StatuteMiles ELSE NULL END
		  ,FlightCost=CASE WHEN @IsLeg=1 THEN PL.FlightCost ELSE NULL END
		  --,TechLog=CASE WHEN @IsLeg=1 THEN PL.TechLog ELSE NULL END
		  ,AirFrameHours=CASE WHEN @IsLeg=1 THEN PL.AirFrameHours ELSE NULL END
		  ,AirframeCycle=CASE WHEN @IsLeg=1 THEN PL.AirframeCycle ELSE NULL END
		  ,Engine1Hours=CASE WHEN @IsLeg=1 THEN PL.Engine1Hours ELSE NULL END
		  ,Engine2Hours=CASE WHEN @IsLeg=1 THEN PL.Engine2Hours ELSE NULL END
		  ,Engine3Hours=CASE WHEN @IsLeg=1 THEN PL.Engine3Hours ELSE NULL END
		  ,Engine4Hours=CASE WHEN @IsLeg=1 THEN PL.Engine4Hours ELSE NULL END
		  ,Engine1Cycle=CASE WHEN @IsLeg=1 THEN PL.Engine1Cycle ELSE NULL END
		  ,Engine2Cycle=CASE WHEN @IsLeg=1 THEN PL.Engine2Cycle ELSE NULL END
		  ,Engine3Cycle=CASE WHEN @IsLeg=1 THEN PL.Engine3Cycle ELSE NULL END
		  ,Engine4Cycle=CASE WHEN @IsLeg=1 THEN PL.Engine4Cycle ELSE NULL END
		  ,Re1Hours=CASE WHEN @IsLeg=1 THEN PL.Re1Hours ELSE NULL END
		  ,Re2Hours=CASE WHEN @IsLeg=1 THEN PL.Re2Hours ELSE NULL END
		  ,Re3Hours=CASE WHEN @IsLeg=1 THEN PL.Re3Hours ELSE NULL END
		  ,Re4Hours=CASE WHEN @IsLeg=1 THEN PL.Re4Hours ELSE NULL END
		  ,Re1Cycle=CASE WHEN @IsLeg=1 THEN PL.Re1Cycle ELSE NULL END
		  ,Re2Cycle=CASE WHEN @IsLeg=1 THEN PL.Re2Cycle ELSE NULL END
		  ,Re3Cycle=CASE WHEN @IsLeg=1 THEN PL.Re3Cycle ELSE NULL END
		  ,Re4Cycle=CASE WHEN @IsLeg=1 THEN PL.Re4Cycle ELSE NULL END
		  ,FuelOn=CASE WHEN @IsLeg=1 THEN PL.FuelOn ELSE NULL END
		  --,AccountID=CASE WHEN @IsLeg=1 THEN PL.AccountID ELSE NULL END
		  --,DelayTypeID=CASE WHEN @IsLeg=1 THEN PL.DelayTypeID ELSE NULL END
		  --,PassengerRequestorID=CASE WHEN @IsLeg=1 THEN PL.PassengerRequestorID ELSE NULL END
		  --,DepartmentID=CASE WHEN @IsLeg=1 THEN PL.DepartmentID ELSE NULL END
		  --,AuthorizationID=CASE WHEN @IsLeg=1 THEN PL.AuthorizationID ELSE NULL END
		  --,FlightCategoryID=CASE WHEN @IsLeg=1 THEN PL.FlightCategoryID ELSE NULL END
		  ,DEPHomeBaseCD=CASE WHEN @IsLeg=1 THEN LDEP.IcaoID ELSE NULL END
		  ,ARRHomeBaseCD=CASE WHEN @IsLeg=1 THEN LARR.IcaoID ELSE NULL END
		  --,ClientCD=CASE WHEN @IsLeg=1 THEN LC.ClientCD ELSE NULL END
		  ,LegCrewCD=CASE WHEN @IsLeg=1 THEN LCR.CrewCD ELSE NULL END
		  --,AccountNum=CASE WHEN @IsLeg=1 THEN LAC.AccountNum ELSE NULL END
		  ,DelayTypeCD=CASE WHEN @IsLeg=1 THEN LDT.DelayTypeCD ELSE NULL END
		  ,LegDepartmentCD=CASE WHEN @IsLeg=1 THEN LD.DepartmentCD ELSE NULL END
		  ,LegAuthorizationCD=CASE WHEN @IsLeg=1 THEN LDA.AuthorizationCD ELSE NULL END
		  ,FlightCatagoryCD=CASE WHEN @IsLeg=1 THEN LFC.FlightCatagoryCD ELSE NULL END
        ---------------------PostFlightExpense----------
         
          --,PostflightExpenseID=CASE WHEN @IsExpense=1 THEN PE.PostflightExpenseID ELSE NULL END
		  --,LogID=CASE WHEN @IsExpense=1 AND @IsLeg=0 THEN PE.POLogID ELSE NULL END
		  --,LegID=CASE WHEN @IsExpense=1 THEN PE.POLegID ELSE NULL END
		  --,HomebaseID=CASE WHEN @IsExpense=1 THEN PE.HomebaseID ELSE NULL END
		  ,SlipNUM=CASE WHEN @IsExpense=1 THEN PE.SlipNUM ELSE NULL END
		  --,LegNUM=CASE WHEN @IsLeg=1 THEN PL.LegNUM WHEN @IsExpense=1 THEN PE.LegNUM ELSE NULL END
		  --,AccountID=CASE WHEN @IsExpense=1 THEN PE.AccountID ELSE NULL END
		  ,AccountPeriod=CASE WHEN @IsExpense=1 THEN PE.AccountPeriod ELSE NULL END
		  --,FleetID=CASE WHEN @IsExpense=1 THEN PE.FleetID ELSE NULL END
		  ,PurchaseDT=CASE WHEN @IsExpense=1 THEN PE.PurchaseDT ELSE NULL END
		  ,AirportID=CASE WHEN @IsExpense=1 THEN PE.AirportID ELSE NULL END
		  --,FBOID=CASE WHEN @IsExpense=1 THEN PE.FBOID ELSE NULL END
		  --,FuelLocatorID=CASE WHEN @IsExpense=1 THEN PE.FuelLocatorID ELSE NULL END
		  --,PaymentTypeID=CASE WHEN @IsExpense=1 THEN PE.PaymentTypeID ELSE NULL END
		  ,PaymentVendorID=CASE WHEN @IsExpense=1 THEN PE.PaymentVendorID ELSE NULL END
		  --,CrewID=CASE WHEN @IsExpense=1 THEN PE.CrewID ELSE NULL END
		  --,FlightCategoryID=CASE WHEN @IsExpense=1 THEN PE.FlightCategoryID ELSE NULL END
		  --,DispatchNUM=CASE WHEN @IsExpense=1 THEN PE.DispatchNUM ELSE NULL END
		  ,InvoiceNUM=CASE WHEN @IsExpense=1 THEN PE.InvoiceNUM ELSE NULL END
		  ,ExpenseAMT=CASE WHEN @IsExpense=1 THEN PE.ExpenseAMT ELSE NULL END
		  ,FuelPurchase=CASE WHEN @IsExpense=1 THEN PE.FuelPurchase ELSE NULL END
		  ,FuelQTY=CASE WHEN @IsExpense=1 THEN PE.FuelQTY ELSE NULL END
		  ,UnitPrice=CASE WHEN @IsExpense=1 THEN PE.UnitPrice ELSE NULL END
		  ,FederalTAX=CASE WHEN @IsExpense=1 THEN PE.FederalTAX ELSE NULL END
		  ,SateTAX=CASE WHEN @IsExpense=1 THEN PE.SateTAX ELSE NULL END
		  ,SaleTAX=CASE WHEN @IsExpense=1 THEN PE.SaleTAX ELSE NULL END
		  ,IsAutomaticCalculation=CASE WHEN @IsExpense=1 THEN PE.IsAutomaticCalculation ELSE NULL END
		  ,IsBilling=CASE WHEN @IsExpense=1 THEN PE.IsBilling ELSE NULL END
		  ,PostEDPR=CASE WHEN @IsExpense=1 THEN PE.PostEDPR ELSE NULL END
		  ,PostFuelPrice=CASE WHEN @IsExpense=1 THEN PE.PostFuelPrice ELSE NULL END 
		  --,LogNum=PM.LogNum
		  --,HomeBaseCD=EA.IcaoID
		  --,AccountNum=EAC.AccountNum
		  --,TailNum=EF.TailNum
		  ,AirportCD=CASE WHEN @IsExpense=1 THEN EAT.IcaoID ELSE NULL END 
		  ,FBOCD=CASE WHEN @IsExpense=1 THEN EFBO.FBOCD ELSE NULL END 
		  ,FuelLocatorCD=CASE WHEN @IsExpense=1 THEN EFL.FuelLocatorCD ELSE NULL END 
		  ,PaymentTypeCD=CASE WHEN @IsExpense=1 THEN EPT.PaymentTypeCD ELSE NULL END 
		  ,ExpenseCrewCD=CASE WHEN @IsExpense=1 THEN EC.CrewCD ELSE NULL END 
		  --,FlightCatagoryCD=EFC.FlightCatagoryCD
		  
		  -------------------------------PostFlightPassenger------------------------------
		  
		/*  --,PostflightPassengerListID = PP.PostflightPassengerListID
			--,POLogID = PP.POLogID
			--,POLegID = PP.POLegID
			,CustomerID = PP.CustomerID
			--,PassengerID = PP.PassengerID
			,PassengerFirstName = PP.PassengerFirstName
			,PassengerMiddleName = PP.PassengerMiddleName
			,PassengerLastName = PP.PassengerLastName
			,FlightPurposeID = PP.FlightPurposeID
			,IsNonPassenger = PP.IsNonPassenger
			,PassportNUM = PP.PassportNUM
			,Billing = PP.Billing
			,OrderNUM = PP.OrderNUM
			--,LastUpdUID = PP.LastUpdUID
			--,LastUpdTS = PP.LastUpdTS
			,WaitList = PP.WaitList
			--,IsDeleted = PP.IsDeleted
			,DepartPercentage = PP.DepartPercentage
			
		*/
			
			----------------------------Passenger---------------------------
			
			,PassengerCD = P.PassengerRequestorCD
			,PassengerName = P.PassengerName
			,PaxLastName = P.LastName
			,PaxFirstName = P.FirstName
			,PaxMiddleInitial = P.MiddleInitial
			,DateOfBirth = P.DateOfBirth
			,PaxTitle = P.PaxTitle
			,PaxGender = P.Gender
			
			------------------------PostFlightCrew-------------------------
			
			/*
			,PostflightCrewListID = PC.PostflightCrewListID
			--,POLegID = PC.POLegID
			,CustomerID = PC.CustomerID
			,CrewID = PC.CrewID
			,CrewFirstName = PC.CrewFirstName
			,CrewMiddleName = PC.CrewMiddleName
			,CrewLastName = PC.CrewLastName
			,DutyTYPE = PC.DutyTYPE
			,TakeOffDay = PC.TakeOffDay
			,TakeOffNight = PC.TakeOffNight
			,LandingDay = PC.LandingDay
			,LandingNight = PC.LandingNight
			,ApproachPrecision = PC.ApproachPrecision
			,ApproachNonPrecision = PC.ApproachNonPrecision
			,Instrument = PC.Instrument
			,Night = PC.Night
			,RemainOverNight = PC.RemainOverNight
			,BeginningDuty = PC.BeginningDuty
			,DutyEnd = PC.DutyEnd
			,DutyHours = ROUND(PC.DutyHours,1)
			,Specification1 = PC.Specification1
			,Sepcification2 = PC.Sepcification2
			,IsOverride = PC.IsOverride
			,Discount = PC.Discount
			,BlockHours = PC.BlockHours
			,FlightHours = PC.FlightHours
			,IsAugmentCrew = PC.IsAugmentCrew
			,IsRemainOverNightOverride = PC.IsRemainOverNightOverride
			,Seat = PC.Seat
			--,LastUpdUID = PC.LastUpdUID
			--,LastUpdTS = PC.LastUpdTS
			,OutboundDTTM = PC.OutboundDTTM
			,InboundDTTM = PC.InboundDTTM
			,CrewDutyStartTM = PC.CrewDutyStartTM
			,CrewDutyEndTM = PC.CrewDutyEndTM
			,ItsNew = PC.ItsNew
			,Specification3 = PC.Specification3
			,Specification4 = PC.Specification4
			,DaysAway = PC.DaysAway
			--,IsDeleted = PC.IsDeleted
			,OrderNUM = PC.OrderNUM
			*/
			
			-------------------------Crew----------------------
			
			,CrewCD = C.CrewCD
			,CrewLastName = C.LastName
			,CrewFirstName = C.FirstName
			,CrewMiddleInitial = C.MiddleInitial
			,CrewGender = C.Gender		

   FROM @PoLogIDTable PLT 
		  INNER JOIN PostflightMain PM ON PLT.PoLogID = PM.POLogID AND PM.IsDeleted=0
          INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
		  LEFT OUTER JOIN PostflightExpense PE ON PE.POLegID=PL.POLegID
		  LEFT OUTER JOIN Account MAC ON MAC.AccountID=PM.AccountID
		  LEFT OUTER JOIN Department MD ON MD.DepartmentID=PM.DepartmentID
		  LEFT OUTER JOIN DepartmentAuthorization MDA ON MDA.AuthorizationID=PM.AuthorizationID
		  LEFT OUTER JOIN Client MC ON MC.ClientID=PM.ClientID
		  LEFT OUTER JOIN PreflightMain PRM ON PRM.TripID=PM.TripID
		  LEFT OUTER JOIN Fleet MF ON MF.FleetID=PM.FleetID
		  LEFT OUTER JOIN Aircraft MAT ON MAT.AircraftID=PM.AircraftID
		  LEFT OUTER JOIN Company MCO ON MCO.HomebaseID=PM.HomebaseID
		  LEFT OUTER JOIN Airport MA ON MA.AirportID=MCO.HomebaseAirportID
		  LEFT OUTER JOIN Airport LDEP ON LDEP.AirportID=PL.DepartICAOID
		  LEFT OUTER JOIN Airport LARR ON LARR.AirportID=PL.ArriveICAOID
		  LEFT OUTER JOIN Client LC ON PL.ClientID=LC.ClientID
		  LEFT OUTER JOIN Crew LCR ON LCR.CrewID=PL.CrewID
		  LEFT OUTER JOIN Account LAC ON LAC.AccountID=PL.AccountID
		  LEFT OUTER JOIN DelayType LDT ON LDT.DelayTypeID=PL.DelayTypeID
		  LEFT OUTER JOIN Department LD ON LD.DepartmentID=PL.DepartmentID
		  LEFT OUTER JOIN DepartmentAuthorization LDA ON LDA.AuthorizationID=PL.AuthorizationID
		  LEFT OUTER JOIN FlightCatagory LFC ON LFC.FlightCategoryID=PL.FlightCategoryID
		  LEFT OUTER JOIN PostflightExpense MPE ON MPE.POLogID=PM.POLogID
		  LEFT OUTER JOIN Company ECO ON ECO.HomebaseID=PE.HomebaseID
		  LEFT OUTER JOIN Airport EA ON EA.AirportID=ECO.HomebaseAirportID
		  LEFT OUTER JOIN Account EAC ON EAC.AccountID=PE.AccountID
		  LEFT OUTER JOIN Fleet EF ON EF.FleetID=PE.FleetID
		  LEFT OUTER JOIN Airport EAT ON EAT.AirportID=PE.AirportID
		  LEFT OUTER JOIN FBO EFBO ON EFBO.FBOID=PE.FBOID
		  LEFT OUTER JOIN FuelLocator EFL ON EFL.FuelLocatorID=PE.FuelLocatorID
		  LEFT OUTER JOIN PaymentType EPT ON EPT.PaymentTypeID=PE.PaymentTypeID
		  LEFT OUTER JOIN Crew EC ON EC.CrewID=PE.CrewID
		  LEFT OUTER JOIN FlightCatagory EFC ON EFC.FlightCategoryID=PE.FlightCategoryID
	      
		  LEFT OUTER JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID AND @IsPax = 1
		  LEFT OUTER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID AND @IsPax = 1
		  LEFT OUTER JOIN Passenger LReq ON LReq.PassengerRequestorID = PM.PassengerRequestorID
	      
		  LEFT OUTER JOIN PostflightCrew PC ON PL.POLegID = PC.POLegID AND @IsCrew = 1
		  LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID AND @IsCrew = 1
              
   WHERE PM.CustomerID=@UserCustomerID
    AND PM.IsDeleted=0
    AND PL.IsDeleted=0
   --AND (PM.POLogID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PoLogIDs, ',')))
   
END

	--EXEC spGetReportPOSTAdhocInformation '10099','100999797',0,0,0,0
	--EXEC spGetReportPOSTAdhocInformation '10099','10099376,10099377,10099378,10099379,10099380,10099381,10099382,10099383,10099384,10099385,10099386,10099387,10099388,10099389,10099390,10099391,10099392,10099393,10099394,10099395,10099396,10099397,10099398,10099399,10099400,10099401,10099402,10099403,10099404,10099405,10099406,10099407,10099408,10099409,10099410,10099411,10099412,10099413,10099414,10099415,10099416,10099417,10099418,10099419,10099420,10099421,10099422,10099423,10099424,10099425,10099426,10099427,10099428,10099429,10099430,10099431,10099432,10099433,10099434,10099435,10099436,10099437,10099438,10099439,10099440,10099441,10099442,10099443,10099444,10099445,10099446,10099447,10099448,10099449,10099450,10099451,10099452,10099453,10099454,10099455,10099456,10099457,10099458,10099459,10099460,10099461,10099462,10099463,10099464,10099465,10099466,10099467,10099468,10099469,10099470,10099471,10099472,10099473,10099474,10099475,10099476,10099477,10099478,10099479,10099480,10099481,10099482,10099483,10099484,10099485,10099486,10099487,10099488,10099489,10099490,10099491,10099492,10099493,10099494,10099495,10099496,10099497,10099498,10099499,10099500,10099501,10099502,10099503,10099504,10099505,10099506,10099507,10099508,10099509,10099510,10099511,10099512,10099513,10099514,10099515,10099516,10099517,10099518,10099519,10099520,10099521,10099522,10099523,10099524,10099525,10099526,10099527,10099528,10099529,10099530,10099531,10099532,10099533,10099534,10099535,10099536,10099537,10099538,10099539,10099540,10099541,10099542,10099543,10099544,10099545,10099546,10099547,10099548,10099549,10099550,10099551,10099552,10099553,10099554,10099555,10099556,10099557,10099558,10099559,10099560,10099561,10099562,10099563,10099564,10099565,10099566,10099567,10099568,10099569,10099570,10099571,10099572,10099573,10099574,10099575,10099576,10099577,10099578,10099579,10099580,10099581,10099582,10099583,10099584,10099585,10099586,10099587,10099588,10099589,10099590,10099591,10099592,10099593,10099594,10099595,10099596,10099597,10099598,10099599,10099600,10099601,10099602,10099603,10099604,10099605,10099606,10099607,10099608,10099609,10099610,10099611,10099612,10099613,10099614,10099615,10099616,10099617,10099618,10099619,10099620,10099621,10099622,10099623,10099624,10099625,10099626,10099627,10099628,10099629,10099630,10099631,10099632,10099633,10099634,10099635,10099636,10099637,10099638,10099639,10099640,10099641,10099642,10099643,10099644,10099645,10099646,10099647,10099648,10099649,10099650,10099651,10099652,10099653,10099654,10099655,10099656,10099657,10099658,10099659,10099660,10099661,10099662,10099663,10099664,10099665,10099666,10099667,10099668,10099669,10099670,10099671,10099672,10099673,10099674,10099675,10099676,10099677,10099678,10099679,10099680,10099681,10099682,10099683,10099684,10099685,10099686,10099687,10099688,10099689,10099690,10099691,10099692,10099693,10099694,10099695,10099696,10099697,10099698,10099699,10099700,10099701,10099702,10099703,10099704,10099705,10099706,10099707,10099708,10099709,10099710,10099711,10099712,10099713,10099714,10099715,10099716,10099717,10099718,10099719,10099720,10099721,10099722,10099723,10099724,10099725,10099726,10099727,10099728,10099729,10099730,10099731,10099732,10099733,10099734,10099735,10099736,10099737,10099738,10099739,10099740,10099741,10099742,10099743,10099744,10099745,10099746,10099747',0,0,0,0

GO


