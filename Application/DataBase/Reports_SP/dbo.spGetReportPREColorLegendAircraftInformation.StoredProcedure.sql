IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREColorLegendAircraftInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREColorLegendAircraftInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREColorLegendAircraftInformation]
   @UserCustomerID BIGINT
  ,@NonReversedColor BIT = 0
AS

-- ===========================================================
--	SPC Name: spGetReportPREColorLegendAircraftInformation
-- Author: Askar
-- Create date: 25 Sep 2013
-- Description: Get Color Legend information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 
-- ============================================================

SET NOCOUNT ON 

BEGIN

SELECT TypeOfDuty='Aircraft'
	  ,Code=AircraftDutyCD
	  ,Description=AircraftDutyDescription
	   ,ForeColor=CASE WHEN @NonReversedColor=1 THEN ForeGrndCustomColor  ELSE BackgroundCustomColor END 
	   ,BackGroudColor=CASE WHEN @NonReversedColor=1 THEN BackgroundCustomColor  ELSE ForeGrndCustomColor END  
	FROM AircraftDuty 
	WHERE CustomerID=@UserCustomerID 
	AND IsInActive=0 
	AND IsDeleted=0
ORDER BY AircraftDutyCD
	     
END


GO


