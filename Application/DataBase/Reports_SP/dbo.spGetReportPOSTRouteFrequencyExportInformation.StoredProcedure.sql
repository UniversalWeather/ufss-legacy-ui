IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTRouteFrequencyExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTRouteFrequencyExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ===============================================================================
-- SPC Name: spGetReportPOSTRouteFrequencyExportInformation
-- Author:Badrinath/mullai
-- Create date: 01 August 2012
-- Description: Get Post Route Frequency Information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 28-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPOSTRouteFrequencyExportInformation]
  @UserCD AS VARCHAR(30)  
 ,@DATEFROM datetime-- Mandatory  
 ,@DATETO datetime-- Mandatory  
 ,@TailNumber NVARCHAR(1000)=''  
 ,@FleetGroupCD NVARCHAR(1000)=''  
 ,@CategoryCD NVARCHAR(1000)=''  

 AS  
  
BEGIN  
SET NOCOUNT ON  
  
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
FleetID BIGINT
)

IF @TailNumber <> ''
BEGIN
INSERT INTO @TempFleetID
SELECT DISTINCT F.FleetID 
FROM Fleet F
WHERE F.CustomerID = @CUSTOMER
AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNumber, ','))
END
IF @FleetGroupCD <> ''
BEGIN 
INSERT INTO @TempFleetID
SELECT DISTINCT F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Fleet F 
LEFT OUTER JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
LEFT OUTER JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
AND F.CustomerID = @CUSTOMER 
END
ELSE IF @TailNumber = '' AND @FleetGroupCD = ''
BEGIN 
INSERT INTO @TempFleetID
SELECT DISTINCT F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Fleet F 
WHERE F.CustomerID = @CUSTOMER 
END
-----------------------------TailNum and Fleet Group Filteration---------------------- 
  
  
  
 
          
 SELECT  
    [lognum]=PM.LogNum   
    ,[tailnmbr]=F.TAILNUM
    ,[DepIcao] =AD.IcaoID  
    ,[DepartureCity] = AD.CityName  
    ,[ArrIcao] = AA.IcaoID  
    ,[ArrCityName] =AA.CityName  
    ,[Distance]=PL.Distance  
    ,[paxtotal]=PL.PassengerTotal
    ,[Frequency]=COUNT (pl.POLogID)
       FROM PostflightMain PM 
					  INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = PM.FleetID	
                      INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
                      INNER JOIN Airport AD on AD.AirportID=PL.DepartICAOID
			          INNER JOIN Airport AA on AA.AirportID=PL.ArriveICAOID
                      INNER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      left outer JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      left outer JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                     -- WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '',''))
                                     --     OR @FleetGroupCD = ''''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
                      LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
            
         WHERE Pm.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))   
                  AND CONVERT(DATE,Pl.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
                  AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CategoryCD, ',')) OR @CategoryCD = '')
                  AND PM.IsDeleted = 0
       group by PM.LogNum,F.TAILNUM,AA.IcaoID,AD.CityName,AD.IcaoID,AA.CityName,PL.Distance,PL.PassengerTotal
      
 
 ORDER BY DepIcao
   
END  
 
 
  
   
  
-- EXEC spGetReportPOSTRouteFrequencyExportInformation 'JWILLIAMS_13','2009-01-01','2009-1-31','N46E','',''

GO


