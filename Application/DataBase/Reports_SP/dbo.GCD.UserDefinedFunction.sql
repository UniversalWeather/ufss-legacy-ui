IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GCD]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GCD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GCD]
(
@DepICAO VARCHAR(4),
@ArrICAO VARCHAR(4)
)
RETURNS NUMERIC(11,4)
AS
BEGIN
DECLARE @lnDepLatDeg NUMERIC(11,4),@lnDepLatMin NUMERIC(11,4),@lnDepLatSec NUMERIC(11,4)=0,@lcDepLatDir CHAR(1),@lnDepLngDeg NUMERIC(11,4),@lnDepLngMin NUMERIC(11,4),@lnDepLngSec NUMERIC(11,4)=0,@lcDepLngDir CHAR(1);
DECLARE @lnArrLatDeg NUMERIC(11,4),@lnArrLatMin NUMERIC(11,4),@lnArrLatSec NUMERIC(11,4)=0,@lcArrLatDir CHAR(1),@lnArrLngDeg NUMERIC(11,4),@lnArrLngMin NUMERIC(11,4),@lnArrLngSec NUMERIC(11,4)=0,@lcArrLngDir CHAR(1),@lcDistMeasure VARCHAR(2)='KM';

SELECT DISTINCT @lnDepLatDeg=LatitudeDegree,@lnDepLatMin=LatitudeMinutes,@lcDepLatDir=LatitudeNorthSouth
     ,@lnDepLngDeg=LongitudeDegrees,@lnDepLngMin=LongitudeMinutes,@lcDepLngDir=LongitudeEastWest FROM Airport WHERE IcaoID=@DepICAO

         
SELECT DISTINCT @lnArrLatDeg=LatitudeDegree,@lnArrLatMin=LatitudeMinutes,@lcArrLatDir=LatitudeNorthSouth
     ,@lnArrLngDeg=LongitudeDegrees,@lnArrLngMin=LongitudeMinutes,@lcArrLngDir=LongitudeEastWest FROM Airport WHERE IcaoID=@ArrICAO 



DECLARE @lnFromLatDeg NUMERIC(11,4),@lnFromLatMin NUMERIC(11,4),@lnFromLongDeg NUMERIC(11,4),@lnFromLongMin NUMERIC(11,4);
DECLARE @lnToLatDeg NUMERIC(11,4),@lnToLatMin NUMERIC(11,4),@lnToLongDeg NUMERIC(11,4),@lnToLongMin NUMERIC(11,4) ;

SET @lnFromLatDeg=@lnDepLatDeg
SET @lnFromLatMin=(@lnDepLatMin/60)+(@lnDepLatSec/3600)		
SET @lnFromLongDeg= @lnDepLngDeg
SET @lnFromLongMin=(@lnDepLngMin/60)+(@lnDepLngSec/3600)

SET @lnToLatDeg=@lnArrLatDeg
SET @lnToLatMin=(@lnArrLatMin/60)+(@lnDepLatSec/3600)		
SET @lnToLongDeg= @lnArrLngDeg
SET @lnToLongMin=(@lnArrLngMin/60)+(@lnArrLngSec/3600)

DECLARE @lnLat1 NUMERIC(11,4),@lnLon1 NUMERIC(11,4),@lnLat2 NUMERIC(11,4),@lnLon2 NUMERIC(11,4);


SET @lnLat1 = (@lnFromLatDeg+@lnFromLatMin)*(CASE(@lcDepLatDir) WHEN 'N' THEN 1 ELSE -1 END)
SET @lnLon1 = (@lnFromLongDeg+@lnFromLongMin)*(CASE(@lcDeplngDir) WHEN 'W' THEN -1 ELSE 1 END)
SET @lnLat2 = (@lnToLatDeg+@lnToLatMin)*(CASE(@lcArrLatDir) WHEN 'N' THEN 1 ELSE -1 END)
SET @lnLon2 = (@lnToLongDeg+@lnToLongMin)*(CASE(@lcArrLngDir) WHEN 'W' THEN -1 ELSE 1 END)


DECLARE @lnMajAxis BIGINT=6378137,
        @lnMinAxis NUMERIC(11,4)=6356752.3142,
        @lnPi NUMERIC(3,2)=3.14;
DECLARE @lnFlaten NUMERIC(11,4)=(@lnMajAxis-@lnMinAxis)/@lnMajAxis  ,
        @lnDifLon NUMERIC(11,4)=((@lnLon2-@lnLon1)*(@lnPi/180));
        

 

DECLARE @lnReducedDepLat NUMERIC(11,4),@lnReducedArrLat NUMERIC(11,4)
SET @lnReducedDepLat = ATAN((1 - @lnFlaten) * TAN((@lnLat1 * (@lnPi / 180))))
SET @lnReducedArrLat = ATAN((1 - @lnFlaten) * TAN((@lnLat2 * (@lnPi / 180))))

DECLARE @sinlnReducedDepLat NUMERIC(11,4)= SIN(@lnReducedDepLat),
		@coslnReducedDepLat NUMERIC(11,4)= COS(@lnReducedDepLat),
		@sinlnReducedArrLat NUMERIC(11,4)= SIN(@lnReducedArrLat),
		@coslnReducedArrLat NUMERIC(11,4)= COS(@lnReducedArrLat);



IF ABS(@lnPi/2-ABS(@lnLat1)) < (1e-12)
BEGIN
	SET @lnLat1 = SIGN(@lnLat1)*(@lnPi/2-(1e-12))
END
IF ABS(@lnPi/2-ABS(@lnLat2)) < (1e-12)
BEGIN
	SET @lnLat2 = SIGN(@lnLat2)*(@lnPi/2-(1e-12))
END

DECLARE @lnApprox NUMERIC(11,4)=@lnDifLon,
        @lnPiApprox NUMERIC(3,2)= 2*@lnPi,
        @lnLimit INT= 50
    
--select @lnLat1
--select @lnLon1
--select @lnLat2
--select @lnLon2       
--SELECT  @lnDifLon



WHILE ABS(@lnApprox - @lnPiApprox) > (1e-12) And @lnLimit > 0
BEGIN
	SET @lnLimit = @lnLimit - 1
    DECLARE @sinlnApprox NUMERIC(11,4),@coslnApprox NUMERIC(11,4),@lnSinSigma NUMERIC(11,4),@lnDistance NUMERIC(11,4),@lnCosSigma NUMERIC(11,4);
	DECLARE @sigma NUMERIC(11,4),@lnSinAlpha NUMERIC(11,4),@cosSqAlpha NUMERIC(11,4),@lnCosSigma2 NUMERIC(11,4),@lnCalc NUMERIC(11,4);
	
	SET @sinlnApprox = SIN(@lnApprox)
	SET @coslnApprox = COS(@lnApprox)
	SET @lnSinSigma  = SQRT((@coslnReducedArrLat * @sinlnApprox) * (@coslnReducedArrLat * @sinlnApprox) + (@coslnReducedDepLat * @sinlnReducedArrLat - @sinlnReducedDepLat * @coslnReducedArrLat * @coslnApprox) * (@coslnReducedDepLat * @sinlnReducedArrLat - @sinlnReducedDepLat * @coslnReducedArrLat * @coslnApprox))

	IF (@lnSinSigma = 0)
	BEGIN
		SET @lnDistance = 0
	END


--SELECT (@sinlnReducedDepLat * @sinlnReducedArrLat) +( @coslnReducedDepLat * @coslnReducedArrLat * @coslnApprox)
	SET @lnCosSigma = (@sinlnReducedDepLat * @sinlnReducedArrLat) +( @coslnReducedDepLat * @coslnReducedArrLat * @coslnApprox)
    SET @sigma = (ATN2(@lnSinSigma,@lnCosSigma))
    SET @lnSinAlpha = @coslnReducedDepLat * @coslnReducedArrLat * @sinlnApprox / @lnSinSigma
	SET @cosSqAlpha = (1 - @lnSinAlpha * @lnSinAlpha)
	SET @lnCosSigma2 = @lnCosSigma - 2 * @sinlnReducedDepLat * @sinlnReducedArrLat / @cosSqAlpha

	SET @lnCalc=@lnFlaten / 16 * @cosSqAlpha * (4 + @lnFlaten * (4 - 3 * @cosSqAlpha))
	SET @lnPiApprox =@lnApprox
	SET @lnApprox = @lnDifLon + (1 - @lnCalc) * @lnFlaten * @lnSinAlpha * (@sigma + @lnCalc * @lnSinSigma * (@lnCosSigma2 + @lnCalc * @lnCosSigma * (-1 + 2 * @lnCosSigma2 * @lnCosSigma2)))
--SELECT @lnCosSigma

END

--SELECT @lnCosSigma2
--SELECT @lnSinAlpha	



IF (@lnLimit = 0)
BEGIN
	SET @lnDistance = 0
END

DECLARE @uSq NUMERIC(11,4),@VinA NUMERIC(11,4),@VinB NUMERIC(11,4),@deltaSigma NUMERIC(11,4)
	
SET @uSq =@cosSqAlpha * (@lnMajAxis * @lnMajAxis - @lnMinAxis * @lnMinAxis) / (@lnMinAxis * @lnMinAxis)
SET @VinA = 1 + @uSq / 16384 * (4096 + @uSq * (-768 + @uSq * (320 - 175 * @uSq)))
SET @VinB = @uSq / 1024 * (256 + @uSq * (-128 + @uSq * (74 - 47 * @uSq)))

SET @deltaSigma = @VinB * @lnSinSigma * (@lnCosSigma2 + @VinB / 4 * (@lnCosSigma * (-1 + 2 * @lnCosSigma2 * @lnCosSigma2) - @VinB / 6 * @lnCosSigma2 * (-3 + 4 * @lnSinSigma * @lnSinSigma) * (-3 + 4 * @lnCosSigma2 * @lnCosSigma2)))




SELECT  @lnDistance = CASE  WHEN  @lcDistMeasure = 'KM' THEN  ROUND((ROUND(@lnMinAxis * @VinA * (@sigma - @deltaSigma),3) ) * 0.001,0)   	--&& return discance In kilometers
						    WHEN  @lcDistMeasure = 'SM' THEN  ROUND(ROUND((ROUND(@lnMinAxis * @VinA * (@sigma - @deltaSigma),3) ) * 0.001,3)*0.6213711922,0)   	---&& first convert discance In kilometers && convert Kilometers In Statute Miles
                            WHEN  @lcDistMeasure = 'NM' THEN  ROUND(ROUND((ROUND(@lnMinAxis * @VinA * (@sigma - @deltaSigma),3) ) * 0.001,3)*0.5399568035,0)   	---&& first convert discance In kilometers && convert Kilometers in Nautical Miles
                            WHEN  @lcDistMeasure = 'MI' THEN  ROUND(ROUND((ROUND(@lnMinAxis * @VinA * (@sigma - @deltaSigma),3) ) * 0.001,3)* 0.6213711922 ,0) 
                            ELSE ROUND(@lnDistance,3)  END  	--&& first convert discance In kilometers && convert Kilometers In Miles




RETURN FLOOR(ISNULL(@lnDistance,0)*10)*0.1



END

--------SELECT [dbo].[GCD]('KHOU','KLAX')



GO