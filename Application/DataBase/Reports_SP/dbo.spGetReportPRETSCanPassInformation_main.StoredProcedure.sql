IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCanPassInformation_main]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_main]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSCanPassInformation_main]
     @UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --  PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
    --,@LegID AS VARCHAR(30)
    AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSCanPassInformation_main
-- Author: Sindhuja.k
-- Create date: 03 OCT 2012
-- Description: Get LegID BY TripID
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
	SELECT DISTINCT
	[LegID] = CONVERT(VARCHAR,PL.LegID)
	, PM.TripNUM, PL.LegNUM
	FROM (SELECT DISTINCT LEGID FROM @tblTripInfo) M
	INNER JOIN PreflightLeg PL ON M.LEGID = PL.LegID
	INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID
	INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
	INNER JOIN ( SELECT TRIPID, TRIPNUM FROM PreflightMain) PM ON PL.TripID = PM.TripID
	
	-- Filter out the LEG when there is NO pax or crew associated
	INNER JOIN ( 
		SELECT LegID FROM PreflightCrewList UNION ALL
	SELECT LegID FROM PreflightPassengerList ) PCL ON PL.LegID = PCL.LegID
	
	WHERE (AD.CountryName = 'CANADA' or AA.CountryName = 'CANADA')
	AND  PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	ORDER BY PM.TripNUM, PL.LegNUM
		 
END

 --EXEC spGetReportPRETSCanPassInformation_main 'TIM', '20', '', '', '', '', '', '', '', '', '', '', '','',1,1,1,1,1,1



GO


