IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrency2Hdr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2Hdr]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



        
CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2Hdr]                                        
(                                  
  @UserCD AS VARCHAR(30),
  @AsOf DateTime                       
 )            
                                
-- =============================================                                      
-- Author: Mathes                 
-- ALtered 28-12-2012                                 
-- Description: Crew Currency II Header                                    
-- =============================================                                      
AS                                    
SET NOCOUNT ON  
/*                                    
BEGIN         
   Declare @DateRange Varchar(30)
   Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@AsOf)  
    
	Select
	IsNull(DayLanding,0) DayLanding,  
	IsNull(NightLanding,0) NightLanding,  
	IsNull(TakeoffDay,0) TakeoffDay,   
	IsNull(TakeoffNight,0) TakeoffNight,  
	IsNull(Approach,0) Approach,      
	IsNull(Instrument,0) Instrument,   
	IsNull(Day7,0) Day7,   
	IsNull(RestDays,0) RestDays,
	IsNull(Day90,0) Day90,
	IsNull(Month6,0) Month6,
	IsNull(Month12,0) Month12,
	IsNull(Day365,0) Day365,
	IsNull(Day30,0) Day30,
	@DateRange DateRange 
	From Company 
	Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
	And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)   

END
*/
BEGIN         
   Declare @DateRange Varchar(30)
   Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@AsOf)  
    
	Select
	IsNull(DayLanding,0) DayLanding,  
	IsNull(NightLanding,0) NightLanding,  
	IsNull(TakeoffDay,0) TakeoffDay,   
	IsNull(TakeoffNight,0) TakeoffNight,  
	IsNull(Approach,0) Approach,      
	IsNull(Instrument,0) Instrument,   
	IsNull(Day7,0) Day7,   
	IsNull(RestDays,0) RestDays,
	IsNull(Day90,0) Day90,
	IsNull(Month6,0) Month6,
	IsNull(Month12,0) Month12,
	IsNull(Day365,0) Day365,
	IsNull(Day30,0) Day30, 
    IsNull(DayLandingMinimum,0) DayLandingMinimum,
    IsNull(NightllandingMinimum,0) NightllandingMinimum,
    IsNull(TakeoffDayMinimum,0) TakeoffDayMinimum,
    IsNull(TakeofNightMinimum,0) TakeofNightMinimum,
    IsNull(ApproachMinimum,0) ApproachMinimum,
    IsNull(InstrumentMinimum,0) InstrumentMinimum,
    IsNull(DayMaximum7,0) DayMaximum7,
    IsNull(RestDaysMinimum,0) RestDaysMinimum,
    IsNull(CalendarMonthMaximum,0) CalendarMonthMaximum,
    IsNull(DayMaximum90,0) DayMaximum90,
    IsNull(CalendarQTRMaximum,0) CalendarQTRMaximum,
    IsNull(RestCalendarQTRMinimum,0) RestCalendarQTRMinimum,
    IsNull(MonthMaximum6,0)MonthMaximum6,
    IsNull(Previous2QTRMaximum,0) Previous2QTRMaximum,
    IsNull(MonthMaximum12,0) MonthMaximum12,
    IsNull(CalendarYearMaximum,0) CalendarYearMaximum,
    IsNull(DayMaximum30,0) DayMaximum30,
    @DateRange DateRange ,
    ISNULL(DayMaximum365,0) DayMaximum365
    --ISNULL(RestCalendarQTRMinimum,0) RestCalendarQTRMinimum
	From Company 
	Where CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
	And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)   

END


GO




