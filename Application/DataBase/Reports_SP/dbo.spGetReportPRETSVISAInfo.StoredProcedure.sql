IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSVISAInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSVISAInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetReportPRETSVISAInfo]
		@UserCD AS VARCHAR(30),
		@TripID AS VARCHAR(50),
		@CrewPassengerID AS BIGINT, -- CrewID or PassengerID
		@CrewPaxType CHAR(1) -- 'C' for Crew and 'P' for Passenger
	AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSVISAInfo
-- Author: Sudhakar J
-- Create date: 28 Sep 2012
-- Description: Get VISA information for Crew/Passenger for a given TRIP
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

	DECLARE @VISAINFO TABLE (
		RowID SMALLINT IDENTITY(1,1),
		CountryCD VARCHAR(100), 
		VISANUM NVARCHAR(150),
		--VISAExpDt DATE
		VISAExpDt VARCHAR(10)
	)
	
	--SELECT * FROM CrewPassengerVisa

	IF @CrewPaxType = 'C' BEGIN -- CREW

		INSERT INTO @VISAINFO (CountryCD, VISANUM, VISAExpDt)
		SELECT DISTINCT CO.CountryCD, --[VisaNum] = DBO.FlightPakDecrypt(CPV.VisaNum),
		[VisaNum] = ISNULL(CPV.VisaNum,''),
		[ExpiryDT] = dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT)
		FROM PreflightLeg PL
		INNER JOIN Crew C ON PL.CustomerID = C.CustomerID
		INNER JOIN CrewPassengerVisa CPV ON C.CrewID = CPV.CrewID AND C.CustomerID = CPV.CustomerID
		INNER JOIN Country CO ON CPV.CountryID = CO.CountryID
		INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
		INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
		
		WHERE C.CrewID = @CrewPassengerID
		AND PL.TripID = @TripID
		AND PL.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		AND ISNULL(CPV.IsDeleted, 0) = 0
		AND (CPV.CountryID = AA.CountryID OR CPV.CountryID = AD.CountryID)
		
	END ELSE BEGIN -- PASSENGER
	
		INSERT INTO @VISAINFO (CountryCD, VISANUM, VISAExpDt)
		SELECT DISTINCT CO.CountryCD, --[VisaNum] = DBO.FlightPakDecrypt(CPV.VisaNum),
		[VisaNum] = ISNULL(CPV.VisaNum,''),
		[ExpiryDT] = dbo.GetShortDateFormatByUserCD(@UserCD,CPV.ExpiryDT)
		FROM PreflightLeg PL
		INNER JOIN Passenger P ON PL.CustomerID = P.CustomerID
		INNER JOIN CrewPassengerVisa CPV ON P.PassengerRequestorID = CPV.PassengerRequestorID
				AND P.CustomerID = CPV.CustomerID
		INNER JOIN Country CO ON CPV.CountryID = CO.CountryID
		INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
		INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID		
		
		WHERE P.PassengerRequestorID = @CrewPassengerID
		AND PL.TripID = @TripID
		AND PL.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		AND ISNULL(CPV.IsDeleted, 0) = 0
		AND (CPV.CountryID = AA.CountryID OR CPV.CountryID = AD.CountryID)
	END
	
	SELECT * FROM @VISAINFO			
END

	--EXEC spGetReportPRETSVISAInfo 'UC', 10001325204, 1000182567, 'C'






GO


