IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQLeadSourceHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQLeadSourceHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[spGetReportCQLeadSourceHistoryExportInformation]
	(
	@UserCD AS VARCHAR(30),
	@UserCustomerID AS VARCHAR(30),
	@DATEFROM DATETIME, --MANDATORY  
	@DATETO DATETIME, --MANDATORY 
	@CQCustomerCD VARCHAR(500)='',
	@CustomerName VARCHAR(500)=''
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportCQLeadSourceHistoryExportInformation
-- Author: Akhila
-- Create date: 
-- Description: Charter Quote Customer Information based on lead source
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON

SELECT DISTINCT custcode=CC.CQCustomerCD
	  ,custname=CF.CQCustomerName
	  ,estdepdt=CF.EstDepartureDT
	  ,quotedt=CF.QuoteDT
	  ,cqfilenum=CF.FileNUM
	  ,quoteid=CM.QuoteID
	  ,quotenum=CM.QuoteNUM
	  ,totquote=CM.QuoteTotal
	  ,orig_nmbr=ISNULL(Pre.TripNUM,0)
	  ,leadsrcid=LS.LeadSourceCD
	  ,leadsrcnm=LS.LeadSourceDescription
	  ,tripdate=CF.EstDepartureDT
	  ,lognum=ISNULL(Post.LogNum,0)
	  ,pax_total=ISNULL(Post.Cnt,ISNULL(Pre.Cnt,0))
	  ,fulltrip=CASE WHEN Post.TripID IS NOT NULL THEN 'TRUE' ELSE 'FALSE' END
	  ,nquote=1
	  ,nbooked=CASE WHEN Post.TripID IS NOT NULL THEN 1 ELSE 0 END
         FROM CQFile CF INNER JOIN CQMain CM ON CM.CQFileID = CF.CQFileID
                        INNER JOIN LeadSource LS ON LS.LeadSourceID = CF.LeadSourceID
                        INNER JOIN CQLeg CL ON CL.CQMainID = CM.CQMainID
                        LEFT OUTER JOIN Fleet F ON F.FleetID = CM.FleetID
                        LEFT OUTER JOIN CQCustomer CC ON CC.CQCustomerID = CF.CQCustomerID
                        LEFT OUTER JOIN (SELECT PM.CQMainID,COUNT(DISTINCT CQPassengerID) Cnt FROM CQMain PM 
																									INNER JOIN CQLeg PL ON PM.CQMainID=PL.CQMainID AND PL.IsDeleted=0
																									LEFT OUTER JOIN  CQPassenger PP ON PP.CQLegID=PL.CQLegID
																								WHERE PM.CustomerID=@UserCustomerID AND PM.IsDeleted=0
																								GROUP BY PM.CQMainID
                                         )CQ ON CQ.CQMainID=CM.CQMainID
                       LEFT OUTER JOIN (SELECT PM.TripID,PM.TripNUM,COUNT(DISTINCT PassengerID) Cnt FROM PreflightMain PM 
																												INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted=0
																												LEFT OUTER JOIN  PreflightPassengerList PP ON PP.LegID=PL.LegID
																											WHERE PM.CustomerID=@UserCustomerID AND PM.IsDeleted=0
																											GROUP BY PM.TripID,PM.TripNUM
                                         )Pre ON Pre.TripID=CM.TripID
                        
                        LEFT OUTER JOIN (SELECT PM.POLogID,PM.LogNum,TripID,COUNT(DISTINCT PassengerID) Cnt FROM PostflightMain PM 
																												INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
																												LEFT OUTER JOIN  PostflightPassenger PP ON PP.POLegID=PL.POLegID
																											WHERE PM.CustomerID=@UserCustomerID AND PM.IsDeleted=0
																											GROUP BY PM.POLogID,PM.LogNum,PM.TripID
                                         )Post ON Post.TripID=CM.TripID

                        
         WHERE CF.IsDeleted=0
           AND CF.CustomerID=@UserCustomerID
           AND CF.EstDepartureDT BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
         ORDER BY CF.CQCustomerName,LeadSourceDescription,CF.EstDepartureDT
END 

--EXEC spGetReportCQLeadSourceHistoryExportInformation 'SUPERVISOR_16','10016','01-01-2000','01-01-2010','',''





GO


