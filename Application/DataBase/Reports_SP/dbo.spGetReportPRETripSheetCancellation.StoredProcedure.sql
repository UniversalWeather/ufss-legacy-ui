IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetCancellation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetCancellation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETripSheetCancellation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory		
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
AS
-- ===============================================================================
-- SPC Name: spGetReportPRETripSheetCancellation
-- Author: sindhuja.k
-- Create date: 27 Mar 2013
-- Description: Get TripSheet Cancellation information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------
IF OBJECT_ID('TEMPDB..#TempFleetID') IS NOT NULL DROP TABLE #TempFleetID

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	  AND F.IsDeleted=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------

	SELECT distinct
			[TripNumber] = PM.TripNUM
		   ,[DispNo] = RTRIM(PM.DispatchNUM)
		   ,[Description] = PM.TripDescription
		   ,[TripDates] =  dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD)
		   ,[Requestor] = P.PassengerName --P.LastName+ ',' + P.FirstName+ ' ' + P.MiddleInitial,
		   ,[TailNumber] = F.TailNUM
		   ,[ContactPhone] =P.PhoneNum
		   ,[Type] = AC.AircraftCD
		--   ,[LegID] = CONVERT(VARCHAR,PL.LegID)
		   ,[TripID] = CONVERT(VARCHAR,PM.TripID)
		   ,[CancelDescription] = PM.CancelDescription
	       FROM PreflightMain PM
	       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		   INNER JOIN Fleet F ON PM.FleetID = F.FleetID
		   INNER JOIN  Aircraft AC ON PM.AircraftID = AC.AircraftID
		   LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
		   INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
		   WHERE PM.CustomerID =@CUSTOMER
		   AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		   AND PM.TripStatus = 'X'
		   And PM.IsDeleted = 0
		   -- ORDER BY F.AircraftCD
 END
 
 --EXEC spGetReportPRETripSheetCancellation 'supervisor_99', '2009-12-01', '2010-01-01', '', ''

GO


