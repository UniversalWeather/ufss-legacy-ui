IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationBillingSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationBillingSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationBillingSummaryExportInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID AS BIGINT,
		@DATEFROM AS DATE, --Mandatory
		@DATETO AS DATE, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0
		)

AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationBillingSummaryExportInformation
-- Author:  Vikram M
-- Create date: 31 Jul 2012
-- Description: Get FlightHours/BlockHours details based on Department and Authorization
--EXEC spGetReportPOSTDepartmentAuthorizationBillingSummaryExportInformation 'SUPERVSIOR_99',10099,'2013-06-16','2013-07-16','','','','','',0
-- ================================================================================
BEGIN

SET NOCOUNT ON
DECLARE @AircraftBasis NUMERIC(1,0);
		SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
		AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @UserCustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @UserCustomerID  
	AND F.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @UserCustomerID 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------

;With DeptAuth AS  (
SELECT DISTINCT 
               PO.LogNum,
               PO.EstDepartureDT AS estdepdt,
               FL.TailNum AS tail_nmbr,
               P.PassengerRequestorCD AS reqcode,
               PO.DispatchNum AS dispatchno,
               DEO.DepartmentCD AS reqdept,
               DAO.AuthorizationCD AS reqauth,
               PL.POLegID AS legid,
               PL.LegNUM AS legnum,
               AD.IcaoID AS depicao_id,
               PL.ScheduledTM AS schedttm,
               AA.IcaoID AS arricao_id,
               PL.Distance,
               ROUND(PL.FlightHours,1) flt_hrs, 
			   ROUND(PL.BlockHours,1) blk_hrs,
			   DEL.DepartmentCD AS dept_code,
			   DAL.AuthorizationCD AS auth_code,
			   CR.ChargeRate,
			   CR.ChargeUnit,
			   FL.MaximumPassenger,
			   FL.AircraftCD,
			   PL.PassengerTotal,
			   TOTAL_CHARGES = CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ ISNULL((PFE11.SUMExpenseAmt),0))
			   WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ ISNULL((PFE11.SUMExpenseAmt),0)) 
			   WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ((ISNULL(ROUND(PL.BlockHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
			   WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 2) THEN ((ISNULL(ROUND(PL.FlightHours,1),0) *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0)) 
		       END,
		       [EXP_ACCOUNT] = EXC.ACCOUNT,
		       [EXP_AMOUNT] = EXC.EXPENSEAMOUNT
	FROM PostflightMain PO 
		 INNER JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID AND PL.IsDeleted = 0
		 LEFT OUTER JOIN Department DEL ON PL.DepartmentID = DEL.DepartmentID AND DEL.CustomerID = PL.CustomerID
		 LEFT OUTER JOIN DepartmentAuthorization DAL ON PL.AuthorizationID = DAL.AuthorizationID
		 LEFT OUTER JOIN Department DEO ON PO.DepartmentID = DEO.DepartmentID AND DEO.CustomerID = PO.CustomerID
		 LEFT OUTER JOIN DepartmentAuthorization DAO ON PO.AuthorizationID = DAO.AuthorizationID
		 LEFT OUTER JOIN Passenger P ON PO.PassengerRequestorID=P.PassengerRequestorID
	     INNER JOIN Fleet FL ON FL.FleetID = PO.FleetID AND FL.CustomerID = PO.CustomerID  
	     INNER JOIN @TempFleetID TF ON TF.FleetID=FL.FleetID
	     LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID         
	     LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID        
	     LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                      SELECT ISNULL(SUM(PP2.ExpenseAMT),0) 
                               FROM PostflightExpense PP2 
									    WHERE PP2.POLegID = PP4.POLegID AND IsBilling = 1)
                       FROM PostflightExpense PP4 WHERE IsBilling =1 
                       )PFE11
                      ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID			  
               LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON PL.POLegID = CR.POLegID 
        LEFT OUTER JOIN ( SELECT DISTINCT PP2.ExpenseAMT,PP2.POLegID,PP2.AccountID,
					  ACCOUNT = (
                      SELECT ISNULL(P1.AccountDescription,' ')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH('')),
                      EXPENSEAMOUNT = (
                      SELECT ISNULL(CONVERT(VARCHAR,PP1.ExpenseAMT),' ')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH(''))
                      FROM PostflightExpense PP2
                     
				 ) EXC ON PL.POLegID = EXC.POLegID    
	
	WHERE  CONVERT(DATE,PL.ScheduledTM) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
		   AND (DEL.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		   AND (DAL.AuthorizationCD in (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ','))OR @AuthorizationCD='')
	       AND (PO.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
		   AND PO.CustomerID = @UserCustomerID
		   AND PO.IsDeleted = 0
)


SELECT lognum
	  ,estdepdt
	  ,tail_nmbr
	  ,reqcode
	  ,dispatchno
	  ,reqdept
	  ,reqauth
	  ,legid
	  ,leg_num
	  ,depicao_id
	  ,schedttm
	  ,arricao_id
	  ,distance
	  ,flt_hrs=FLOOR(flt_hrs*10)*0.1
	  ,blk_hrs=FLOOR(blk_hrs*10)*0.1
	  ,dept_code
	  ,auth_code
	  ,chrg_rate
	  ,chrg_unit
	  ,tot_line
	  ,max_pax
	  ,type_code
	  ,pax_total
	  ,total_charges=ISNULL(total_charges,0)
	  ,exp_account
	  ,exp_amount FROM (
SELECT lognum=DA.LogNum
	  ,estdepdt=DA.estdepdt
	  ,tail_nmbr=DA.tail_nmbr
	  ,reqcode=DA.reqcode
	  ,dispatchno=DA.dispatchno
	  ,reqdept=DA.reqdept
	  ,reqauth=DA.reqauth
	  ,legid=DA.legid
	  ,leg_num=DA.legnum
	  ,depicao_id=DA.depicao_id
	  ,schedttm=DA.schedttm
	  ,arricao_id=DA.arricao_id
	  ,distance=DA.Distance
	  ,flt_hrs=DA.flt_hrs
	  ,blk_hrs=DA.blk_hrs
	  ,dept_code=DA.dept_code
	  ,auth_code=DA.auth_code
	  ,chrg_rate=DA.ChargeRate
	  ,chrg_unit=DA.ChargeUnit
	  ,tot_line='' 
	  ,max_pax=DA.MaximumPassenger
	  ,type_code=DA.AircraftCD
	  ,pax_total=DA.PassengerTotal
	  ,total_charges=DA.TOTAL_CHARGES
	  ,exp_account = DA.EXP_ACCOUNT
	  ,exp_amount = DA.EXP_AMOUNT
 FROM DeptAuth DA	
 UNION ALL
 SELECT lognum=999999999
	  ,estdepdt=NULL
	  ,tail_nmbr=NULL
	  ,reqcode=NULL
	  ,dispatchno=NULL
	  ,reqdept=NULL
	  ,reqauth=NULL
	  ,legid=0
	  ,leg_num=999999999
	  ,depicao_id=NULL
	  ,schedttm=NULL
	  ,arricao_id=NULL
	  ,distance=SUM(Distance)
	  ,flt_hrs=SUM(flt_hrs) 
	  ,blk_hrs=SUM(blk_hrs)
	  ,dept_code=dept_code
	  ,auth_code=auth_code
	  ,chrg_rate=0
	  ,chrg_unit=NULL
	  ,tot_line='R' 
	  ,max_pax=0
	  ,type_code=NULL
	  ,pax_total=0
	  ,total_charges=0
	  ,EXP_ACCOUNT = NULL
	  ,EXP_AMOUNT = NULL
	  FROM DeptAuth GROUP  BY dept_code,auth_code
UNION ALL
 SELECT lognum=999999999
	  ,estdepdt=NULL
	  ,tail_nmbr=NULL
	  ,reqcode=NULL
	  ,dispatchno=NULL
	  ,reqdept=NULL
	  ,reqauth=NULL
	  ,legid=0
	  ,leg_num=999999999
	  ,depicao_id=NULL
	  ,schedttm=NULL
	  ,arricao_id=NULL
	  ,distance=SUM(Distance)
	  ,flt_hrs=SUM(flt_hrs) 
	  ,blk_hrs=SUM(blk_hrs)
	  ,dept_code=dept_code
	  ,auth_code='ZZZZ'
	  ,chrg_rate=0
	  ,chrg_unit=NULL
	  ,tot_line='S' 
	  ,max_pax=0
	  ,type_code=NULL
	  ,pax_total=0
	  ,total_charges=0
	  ,EXP_ACCOUNT = NULL
	  ,EXP_AMOUNT = NULL
	  FROM DeptAuth GROUP  BY dept_code	  
)DeptSummary
ORDER BY dept_code,auth_code,lognum,leg_num,tot_line

		
END
	





GO


