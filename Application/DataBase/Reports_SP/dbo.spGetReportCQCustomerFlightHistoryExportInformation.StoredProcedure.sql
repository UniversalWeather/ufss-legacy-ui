IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQCustomerFlightHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQCustomerFlightHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQCustomerFlightHistoryExportInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@UserCustomerID BIGINT,
		@DATEFROM AS DATETIME , --Mandatory
		@DATETO AS DATETIME , --Mandatory
		@IcaoID AS CHAR(4) = '', -- [Optional]
		@LogNum AS BIGINT = '', -- [Optional]
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CQCustomerCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@SortBy as varchar(1)
		)
AS	

--EXEC spGetReportCQCustomerFlightHistoryExportInformation 'SUPERVISOR_99',10099,'2013-01-10','2013-10-20','',0,'','','','' 	
BEGIN
SET NOCOUNT ON

DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
			
DECLARE @AircraftBasis numeric(1,0);
	  SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	   AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	   
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));	
	   
	   DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = '' 
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.IsDeleted=0
	AND F.IsInActive =0
	AND F.CustomerID = @CUSTOMERID  
END


-----------------------------TailNum and Fleet Group Filteration----------------------		
			
SELECT custcode
	  ,custname
	  ,flight_purpose
	  ,flt_date
	  ,TailNum
	  ,DispatchNum
	  ,LogNum
	  ,legid
	  ,legnum
	  ,DepartICAOCD
	  ,Depcity
	  ,ArriveICAOCD
	  ,Arrcity
	  ,Distance
	  ,FlightBlockHoursLabel
	  ,FlightBlockHours
	  ,pax_total
	  ,es_total
	  ,cat_code
	  ,fltcatdesc
	  ,dpaxcode
	  ,dpaxname
	  ,tot_line
	  ,FlightPurpose
	  ,TenToMin
	  ,Rnk
	  ,FlightBlockHoursVal=CASE WHEN Rnk=1 THEN FlightBlockHoursVal ELSE 0 END
 FROM 
(
	SELECT DISTINCT
           CC.CQCustomerCD AS custcode,
           CC.CQCustomerName  AS custname,
           PL.FlightPurpose AS [flight_purpose],
           PL.ScheduledTM AS [flt_date],
           F.TailNum,
           PO.DispatchNum, 
           PO.LogNum,
           PL.POLegID AS legid,
           PL.LegNUM AS legnum,
           AD.IcaoID AS DepartICAOCD ,
           AD.CityName AS Depcity,
		   AA.IcaoID AS ArriveICAOCD,
		   AA.CityName AS Arrcity,
		  (PL.Distance * 1.15078) AS Distance,
		   FlightBlockHoursLabel = CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'blk_hrs') 
	                                               WHEN 2 THEN CONVERT(VARCHAR(30),'flt_hrs') END,
		   CASE WHEN @AircraftBasis = 1 THEN FLOOR(ROUND(PL.BlockHours,1)*10)*0.1 WHEN @AircraftBasis = 2 THEN FLOOR(ROUND(PL.FlightHours,1)*10)*0.1 END AS FlightBlockHours ,
		   PL.PassengerTotal AS [pax_total] ,
		   ISNULL(F.MaximumPassenger - PL.PassengerTotal,0) AS [es_total],
           FC.FlightCatagoryCD AS [cat_code],
           FC.FlightCatagoryDescription AS fltcatdesc,
           P.PassengerRequestorCD AS dpaxcode,
           P.PassengerName AS dpaxname,
           '' tot_line,
           FP.FlightPurposeCD AS [FlightPurpose],
		   [TenToMin] = @TenToMin,
		   Rnk= ROW_NUMBER()OVER(PARTITION BY PL.POLegID ORDER BY PO.LogNum,PL.LegNum),
		   CASE WHEN @AircraftBasis = 1 THEN FLOOR(ROUND(PL.BlockHours,1)*10)*0.1 WHEN @AircraftBasis = 2 THEN FLOOR(ROUND(PL.FlightHours,1)*10)*0.1 END AS FlightBlockHoursVal 
		   
	FROM PostflightMain PO JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PO.CustomerID = PL.CustomerID
	     LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID
	     LEFT JOIN Passenger P ON P.PassengerRequestorID = PP.PassengerID
	     INNER JOIN Fleet F ON PO.FleetID = F.FleetID
	     INNER JOIN @TempFleetID  T ON F.FleetID = T.FleetID 
		 LEFT JOIN Company C ON F.HomebaseID = C.HomebaseID				
         LEFT JOIN Airport AP ON C.HomebaseAirportID = AP.AirportID				
         LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID        				
         LEFT JOIN Airport AA ON pl.ArriveICAOID = AA.AirportID    
		 INNER JOIN CQMain CM ON CM.TripID = PO.TripID
         INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID    
	     INNER JOIN CQCustomer CC ON CC.CQCustomerID=CF.CQCustomerID
	     LEFT JOIN FlightCatagory FC ON FC.CustomerID = PL.CustomerID AND FC.FlightCategoryID = PL.FlightCategoryID 
	     LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID = P.FlightPurposeID
	WHERE CONVERT(DATE,PL.ScheduledTM) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
			  AND (AP.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ICAOId, ',')) OR @ICAOId ='')
			  AND (CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD ='')
			  AND (PO.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum ='')
			  AND PO.CustomerID = @UserCustomerID
    )A
	 ORDER BY 
	 CASE  @SortBy WHEN 'a' THEN A.custcode
	               WHEN 'b' THEN A.custname  END

END


GO


