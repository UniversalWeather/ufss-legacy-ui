IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDepartmentAuthorizationExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDepartmentAuthorizationExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportDepartmentAuthorizationExportInformation]
	@UserCD VARCHAR(30)
AS
-- =============================================
-- SPC Name: spGetReportDepartmentAuthorizationExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get the Department Authorization Information for REPORT export
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================
SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		[code] = D.DepartmentCD
		,[desc] = D.DepartmentName
		,[client] = C.ClientCD
		,[authcode] = DA.AuthorizationCD	
		,[authdesc] = DA.DeptAuthDescription
	FROM
		Department D 
		LEFT OUTER JOIN DepartmentAuthorization DA
			ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
			AND DA.IsInActive = 0 AND D.DepartmentID = DA.DepartmentID
		LEFT OUTER JOIN Client C ON D.ClientID = C.ClientID
	WHERE ISNULL(D.IsDeleted, 0) = 0
		AND D.IsInActive = 0
		AND D.CustomerID = @CUSTOMERID
		AND ( D.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	ORDER BY D.DepartmentCD
	
-- EXEC spGetReportDepartmentAuthorizationExportInformation 'TIM'

GO


