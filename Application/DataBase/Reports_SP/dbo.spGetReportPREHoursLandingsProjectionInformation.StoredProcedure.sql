IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREHoursLandingsProjectionInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================    
-- SPC Name: spGetReportPREHoursLandingsProjectionInformation   
-- Author: Abhishek S    
-- Create date: 1st Aug 2012    
-- Description: Get estimated hours and handlings based on scheduled trips   
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================  
CREATE PROCEDURE [dbo].[spGetReportPREHoursLandingsProjectionInformation]
    @UserCD AS VARCHAR(30), -- Mandatory
	@DATEFROM AS DATETIME,  -- Mandatory
	@DATETO AS DATETIME,    -- Mandatory
	@TailNum AS NVARCHAR(30) = '', -- [Optional], Comma delimited string with mutiple values
	@FleetGroupCD AS NVARCHAR(200) = '', -- [Optional], Comma delimited string with mutiple values
    @TripNum As NVARCHAR(500)= '', -- [Optional]
    @IsHomeBase AS BIT = 0
AS
BEGIN	
	SET NOCOUNT ON;
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))   
    


CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID
	AND F.IsDeleted=0  
	AND F.IsInActive = 0
END
-------------------------------TailNum and Fleet Group Filteration----------------------	

	
	CREATE TABLE #TempHours (
		 FleetId BIGINT,
         TailNo VARCHAR(9),
         TypeCode VARCHAR(10),
         [Hours] NUMERIC(11,3),
         [Landings] BIGINT,
         [ProjectedTotalHours] VARCHAR(100),
         [ProjectedTotalLandings] VARCHAR(100)
     )
	
	  DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
      DECLARE @ParameterDefinition AS NVARCHAR(500)
      
      DECLARE @TenToMin INT
				SET @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 
      --drop table #TempHours
      SET @SQLSCRIPT = '
      INSERT INTO #TempHours
      
      SELECT  
      [FleetId]=F.FleetID    
     ,[TailNo] =F.TailNum
     ,[TypeCode] = AT.AircraftCD
     ,[Hours] = SUM(PL.ElapseTM)
     --,[Landings] = SUM(LAND.LANDINGS)
     ,[Landings] = COUNT(*)
     ,[ProjectedTotalHours] = ''_____________________''
     ,[ProjectedTotalLandings] = ''_____________________''
       
       FROM PreflightMain PM
       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
        INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
				FROM Fleet F 				
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN(SELECT AircraftID, AircraftCD FROM Aircraft)AT
       ON AT.AircraftID = F.AircraftID 
       INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
   
       WHERE PM.IsDeleted = 0
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND PM.TripStatus = ''T''
         AND PM.RecordType = ''T''
         AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	'
		 

	IF @TripNum <> '' BEGIN  
		-- SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNum, 1) = ',' THEN LEFT(@TripNum, LEN(@TripNum) - 1) ELSE @TripNum END + ')';
	      SET @SQLSCRIPT = @SQLSCRIPT + ' AND CONVERT(VARCHAR,PM.TripNUM)  = @TripNum'
	END
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	

    SET @SQLSCRIPT= @SQLSCRIPT+' GROUP BY F.FleetID, F.TailNum , AT.AircraftCD '
--PRINT @SQLSCRIPT
    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS VARCHAR(50),@TripNum As NVARCHAR(500)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD,@TripNum


IF @SuppressActivityAircft = 0       
SELECT DISTINCT A.*,
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
 FROM 
(SELECT   T.TailNo,T.TypeCode,FLOOR(T.Hours * 10) * 0.1 HOURS,T.Landings Landings,T.ProjectedTotalHours,T.ProjectedTotalLandings, @TenToMin TenToMin 
	FROM #TempHours T	
       GROUP BY T.TailNo,T.TypeCode,T.ProjectedTotalHours,T.ProjectedTotalLandings,T.FleetId ,T.Hours,T.Landings
       
 UNION ALL
 
SELECT  F.TailNum ,A.AircraftCD,0.0 HOURS,0 Landings,'_____________________' ProjectedTotalHours,'_____________________' ProjectedTotalLandings, @TenToMin TenToMin
        FROM (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
				FROM Fleet F			
		) F 
        INNER JOIN Aircraft A ON F.AircraftID=A.AircraftID 
        INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
        WHERE  F.CustomerID= CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
        AND  TailNum NOT IN (SELECT  TailNo FROM #TempHours)
      --  AND (TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
        )A
ORDER BY A.TypeCode,A.TailNo


ELSE 
BEGIN
SELECT DISTINCT A.*,
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) FROM 
(SELECT   T.TailNo,T.TypeCode,FLOOR(T.Hours * 10) * 0.1 HOURS,T.Landings Landings,T.ProjectedTotalHours,T.ProjectedTotalLandings, @TenToMin TenToMin 
	FROM #TempHours T	
       GROUP BY T.TailNo,T.TypeCode,T.ProjectedTotalHours,T.ProjectedTotalLandings,T.FleetId ,T.Hours,T.Landings
        )A
ORDER BY A.TypeCode,A.TailNo
END
--SELECT * FROM #TempHours WHERE TailNo = 'N46E'

IF OBJECT_ID('tempdb..#TempHours') IS NOT NULL
DROP TABLE #TempHours 

IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID

END

--EXEC spGetReportPREHoursLandingsProjectionInformation 'JWILLIAMS_13','2011-07-01','2011-07-16','N46F', '', '', 0

GO


