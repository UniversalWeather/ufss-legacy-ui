IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTimeinTypeInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTimeinTypeInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTTimeinTypeInformation]
@UserCD AS VARCHAR(30),  
@AsOf Datetime,--Mandatory  
@CrewCD AS NVARCHAR(1000) = '',  
@CrewGroupCD AS NVARCHAR(1000) = '',  
@AircraftCD AS NVARCHAR(1000) = ''  
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTTimeinTypeInformation]  
-- Author:Badrinath/Mullai.D  
-- Create date: 03 August 2012  
-- Description: Get Time in Type Information for REPORT
-- Revision History  
-- Date Name Ver Change  
--  
-- ================================================================================  
 AS
  
SET NOCOUNT ON 
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=0
--DROP TABLE #TYPEINFO
--DROP TABLE #TYPEINFO1
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END


-----------------------------TailNum and Fleet Group Filteration---------------------- 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 0

Declare @TenToMin Int
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @Flighthrs varchar(30);
SELECT @Flighthrs = (case when (cp.AircraftBasis=1) then 'Block Hours'
when (cp.AircraftBasis=2) then 'Flight Hours' end) FROM Company cp
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @TempFleet TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
AircraftID BIGINT,
FleetID BIGINT,
TailNum NVARCHAR(10)
--AircraftCD NVARCHAR(10)
);
IF @AircraftCD <> ''
      BEGIN
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @Customer
            AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
            (SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@Customer AND
                                                                  AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))))
      END
IF @AircraftCD = ''
      BEGIN 
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @Customer

      END

DECLARE @Flighthrs1 varchar(30);
SELECT @Flighthrs1 = cp.AircraftBasis FROM Company cp
WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)


CREATE TABLE #TYPEINFO(Crew VARCHAR(5),CrewMember VARCHAR(50),TypeCode VARCHAR(40),Descriptions VARCHAR(15),PIC VARCHAR(10),SIC VARCHAR(15),AsOf VARCHAR(100),FlightHours VARCHAR(30),
TimeInTypeHrs NUMERIC(9,1),Simulator NUMERIC(7,3),totalhrs NUMERIC(17,10),TenToMin int,ISSTATUS BIT,ISDELETED BIT,StartDate DATE,EndDate DATE,Date DATE)

CREATE TABLE #TYPEINFO1(Crew VARCHAR(5),CrewMember VARCHAR(50),TypeCode VARCHAR(40),Descriptions VARCHAR(15),PIC VARCHAR(10),SIC VARCHAR(15),AsOf VARCHAR(100),FlightHours VARCHAR(30),
TimeInTypeHrs NUMERIC(9,1),Simulator NUMERIC(7,3),totalhrs NUMERIC(17,10),TenToMin int,ISSTATUS BIT,ISDELETED BIT,StartDate DATE,EndDate DATE,Date DATE)
-----To Retrieve empty Crew and Type Code details




INSERT INTO #TYPEINFO

SELECT TEMP.Crew,TEMP.CrewMember,TEMP.TypeCode,TEMP.Descriptions,TEMP.PIC,TEMP.SIC,TEMP.AsOf,temp.FlightHours,
SUM(TEMP.TimeInTypeHrs) TimeInTypeHrs,(TEMP.Simulator) Simulator,(TEMP.TotalHrs) TotalHrs,TEMP.TenToMin,TEMP.IsStatus,TEMP.IsDeleted,TEMP.StartDate,TEMP.EndDate,TEMP.Date
FROM
(
SELECT [Crew]		=	C.CREWCD,
       [CrewMember]	=	C.CrewCD+' - '+C.LastName,--PL.ScheduledTM,CR.ASOFDT,
       [TypeCode]		=	ISNULL(AT.AircraftCD,''), 
       [Descriptions]	=	ISNULL(CR.CrewRatingDescription,''), 
       [PIC]			= (CASE WHEN (CR.IsPilotinCommand= 1) THEN 'Y' WHEN (CR.IsPilotinCommand= 0) THEN 'N' END),
       [SIC]			= (CASE WHEN (CR.IsSecondInCommand= 1) THEN 'Y' WHEN (CR.IsSecondInCommand= 0) THEN 'N' END),
       [AsOf]			= dbo.GetShortDateFormatByUserCD(@UserCD,@asof), 
       [FlightHours]	= @Flighthrs,
       [TimeInTypeHrs] =  CASE  WHEN CR.AsOfDT IS NOT NULL THEN(CASE WHEN (@Flighthrs1=1) AND(CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,CR.AsOfDT)  AND CONVERT(DATE,@AsOF)) THEN ISNULL(ROUND(PC.BlockHours,1),0)
                                                                      WHEN (@Flighthrs1=2) AND (CONVERT(DATE,pl.ScheduledTM) BETWEEN CONVERT(DATE,CR.AsOfDT) AND CONVERT(DATE,@AsOF)) THEN ISNULL(ROUND(PC.FlightHours,1),0) ELSE 0 END)
                                                             ELSE (CASE WHEN (@Flighthrs1=1) AND(CONVERT(DATE,PL.ScheduledTM)<=CONVERT(DATE,@AsOF)) THEN ISNULL(ROUND(PC.BlockHours,1),0)
                                                                         WHEN (@Flighthrs1=2) AND (CONVERT(DATE,PL.ScheduledTM)<=CONVERT(DATE,@AsOF)) THEN ISNULL(ROUND(PC.FlightHours,1),0) ELSE 0 END) END, 
       
       [Simulator]=0,
       [TotalHrs]		= 0,---(CASE WHEN (CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,cr.AsOfDT) AND CONVERT(DATE,@AsOF)) THEN ISNULL(CR.TotalTimeInTypeHrs,0) ELSE 0 END),
       [TenToMin]		= @TenToMin, 
       C.IsStatus,
       C.IsDeleted,
       NULL StartDate,
       NULL EndDate,
       NULL Date
    FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightCrew PC ON PC.POLegID=PL.POLegID
                                INNER JOIN Crew C ON C.CrewID=PC.CrewID
                                INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                                INNER JOIN CrewRating CR ON CR.CrewID=PC.CrewID AND CR.AircraftTypeID=F.AircraftID
                                INNER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID 
                                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
    WHERE PM.CustomerID=@CUSTOMER
      AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
      AND PM.IsDeleted = 0
)TEMP
GROUP BY TEMP.Crew,TEMP.CrewMember,TEMP.TypeCode,TEMP.Descriptions,TEMP.PIC,TEMP.SIC,TEMP.AsOf,TEMP.FlightHours,TEMP.TenToMin,TEMP.Simulator,TEMP.TotalHrs,TEMP.IsStatus,TEMP.IsDeleted,TEMP.StartDate,TEMP.EndDate,TEMP.Date



INSERT INTO #TYPEINFO1
SELECT DISTINCT
[Crew]			= C.CREWCD,
[CrewMember]	= C.CrewCD +' - ' +C.LastName,
[TypeCode]		= NULL, 
[Description]	= 'NO ITEMS', 
[PIC]			= NULL, 
[SIC]			= NULL,
[AsOf]			= NULL, 
[FlightHours]	= @Flighthrs,
[TimeInTypeHrs] = NULL, 
[Simulator]		= NULL, 
[totalhrs]		= NULL,
[TenToMin]		= @TenToMin,
C.IsStatus,
C.IsDeleted,
NULL,
NULL,
NULL

FROM Crew C 

INNER JOIN ( SELECT DISTINCT CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
WHERE C.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))

DELETE FROM #TYPEINFO1 WHERE Crew IN(SELECT DISTINCT Crew from #TYPEINFO WHERE TypeCode IS NOT NULL)
AND TypeCode IS NULL
DELETE FROM #TYPEINFO1 WHERE ISDELETED=0 AND ISSTATUS=0 AND Crew NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))
------Applying Zero Suppress Activity for Crew based on Company profile setting



DECLARE @CurDate TABLE(DateWeek DATE,CrewCD VARCHAR(5),CrewMember VARCHAR(60),CrewID BIGINT)
DECLARE @CustomerID BIGINT=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))),@DATEFROM DATETIME

SELECT @DATEFROM='1990-01-01' FROM PostflightSimulatorLog PSL INNER JOIN CrewRating CR ON CR.CrewID=PSL.CrewID
                                                                INNER JOIN @TempCrewID TC ON TC.CrewID=CR.CrewID
          WHERE PSL.CustomerID=@CUSTOMERID
            --AND (CONVERT(DATE,PSL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@AsOf) AND CONVERT(DATE,CR.AsOfDT) OR
            --      CONVERT(DATE,PSL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@AsOf) AND CONVERT(DATE,CR.AsOfDT))
  
   

INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.CrewCD+' - '+C.LastName CrewMember,C.CrewID FROM Crew C  
                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@DATEFROM,@AsOf) ) T
                WHERE C.CustomerID= @CUSTOMER
                  AND C.IsDeleted=0
                  AND C.IsStatus=1




INSERT INTO #TYPEINFO(Crew,CrewMember,TypeCode,Descriptions,PIC,SIC,AsOf,totalhrs,TenToMin,FlightHours)
SELECT C.CrewCD,C.CrewCD +' - ' +C.LastName,AT.AircraftCD,AT.AircraftDescription,
       (CASE WHEN (CR.IsPilotinCommand= 1) THEN 'Y' WHEN (CR.IsPilotinCommand= 0) THEN 'N' END),
       (CASE WHEN (CR.IsSecondInCommand= 1) THEN 'Y' WHEN (CR.IsSecondInCommand= 0) THEN 'N' END),
       dbo.GetShortDateFormatByUserCD(@UserCD,@asof),
       CR.TotalTimeInTypeHrs,@TenToMin,@Flighthrs FROM Crew C INNER JOIN CrewRating CR ON C.CrewID=CR.CrewID
                     INNER JOIN @TempCrewID TC ON TC.CrewID=CR.CrewID
                     INNER JOIN Aircraft AT ON AT.AircraftID=CR.AircraftTypeID
         WHERE C.CustomerID=@CUSTOMER
         AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
         



INSERT INTO #TYPEINFO(Crew,CrewMember,FlightHours,Simulator,StartDate,EndDate,TenToMin,TypeCode,Descriptions,Date,PIC,SIC,AsOf,totalhrs) 
SELECT C.CrewCD
      ,C.CrewCD +' - ' +C.LastName
      ,@Flighthrs
      ,PSL.FlightHours
      ,CASE WHEN  PSL.SessionDT <= @DATEFROM THEN @DATEFROM
                              WHEN  PSL.SessionDT >= @AsOf THEN @AsOf ELSE PSL.SessionDT END
       ,DATEADD(DAY,(DATEDIFF(DAY,PSL.DepartureDTTMLocal,PSL.ArrivalDTTMLocal)+1),PSL.SessionDT)
       ,@TenToMin
       ,A.AircraftCD
       ,A.AircraftDescription
       ,PSL.DepartureDTTMLocal
       ,(CASE WHEN (CR.IsPilotinCommand= 1) THEN 'Y' WHEN (CR.IsPilotinCommand= 0) THEN 'N' END)
        ,(CASE WHEN (CR.IsSecondInCommand= 1) THEN 'Y' WHEN (CR.IsSecondInCommand= 0) THEN 'N' END)
        ,dbo.GetShortDateFormatByUserCD(@UserCD,@asof)
        ,ISNULL(CR.TimeInType,0)
      FROM PostflightSimulatorLog  PSL
           INNER JOIN Crew C ON PSL.CrewID=C.CrewID
           INNER JOIN @TempCrewID TC ON C.CrewID = TC.CREWID
           INNER JOIN CrewRating CR ON C.CrewID=CR.CrewID
           INNER JOIN Aircraft A ON CR.AircraftTypeID=A.AircraftID
    WHERE PSL.CustomerID=@CUSTOMERID
    AND (A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='')
    AND ((CONVERT(DATE,PSL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,ISNULL(CR.AsOfDT,@DATEFROM))  AND  CONVERT(DATE,@AsOf)) OR
         (CONVERT(DATE,PSL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@AsOf) AND CONVERT(DATE,ISNULL(CR.AsOfDT,@DATEFROM))))


INSERT INTO #TYPEINFO(Crew,CrewMember,FlightHours,Date,Simulator,PIC,SIC,AsOf,TenToMin,TypeCode,Descriptions,totalhrs)
SELECT DISTINCT CD.CrewCD,CD.CrewMember,CI.FlightHours,CD.DateWeek,CI.Simulator,CI.PIC,CI.SIC,CI.AsOf,@TenToMin,CI.TypeCode,CI.Descriptions,0
				FROM Crew C INNER  JOIN  @CurDate CD ON CD.CrewID=C.CrewID
				            INNER JOIN @TempCrewID TC ON C.CrewID = TC.CREWID
				            LEFT JOIN #TYPEINFO CI ON CD.CrewCD=CI.Crew
			    WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,CI.StartDate)
				  AND  CONVERT(DATE,CD.DateWeek) < CONVERT(DATE, CI.EndDate )

IF @IsZeroSuppressActivityCrewRpt=0
BEGIN



	
SELECT TYPEINFO.Crew
      ,TYPEINFO.CrewMember
      ,TYPEINFO.TypeCode
      ,TYPEINFO.Descriptions
      ,TYPEINFO.PIC
      ,TYPEINFO.SIC
      ,TYPEINFO.AsOf
      ,TYPEINFO.FlightHours
      ,SUM(TYPEINFO.TimeInTypeHrs) TimeInTypeHrs
      ,SUM(TYPEINFO.Simulator) Simulator
      ,SUM(TYPEINFO.totalhrs) totalhrs
      ,TYPEINFO.TenToMin

 FROM  
   (	
	SELECT DISTINCT Crew,CrewMember,TypeCode,Descriptions,PIC,SIC,AsOf,FlightHours ,
                    ISNULL(TimeInTypeHrs,0) TimeInTypeHrs,ISNULL(Simulator,0)Simulator,ISNULL(totalhrs,0) totalhrs,TenToMin,Date FROM #TYPEINFO
	UNION ALL
	SELECT DISTINCT Crew,CrewMember,TypeCode ,Descriptions,PIC,SIC,AsOf,FlightHours ,
                    ISNULL(TimeInTypeHrs,0) TimeInTypeHrs,ISNULL(Simulator,0) Simulator,ISNULL(totalhrs,0) totalhrs,TenToMin,Date  FROM #TYPEINFO1
    )TYPEINFO
    GROUP BY  TYPEINFO.Crew,TYPEINFO.CrewMember,TYPEINFO.TypeCode,TYPEINFO.Descriptions,TYPEINFO.PIC,TYPEINFO.SIC,TYPEINFO.AsOf,TYPEINFO.FlightHours,TYPEINFO.TenToMin
    ORDER BY Crew,TypeCode
END
ELSE
BEGIN
	SELECT TYPEINFO.Crew
      ,TYPEINFO.CrewMember
      ,TYPEINFO.TypeCode
      ,TYPEINFO.Descriptions
      ,TYPEINFO.PIC
      ,TYPEINFO.SIC
      ,TYPEINFO.AsOf
      ,TYPEINFO.FlightHours
      ,SUM(TYPEINFO.TimeInTypeHrs) TimeInTypeHrs
      ,SUM(TYPEINFO.Simulator) Simulator
      ,SUM(TYPEINFO.totalhrs) totalhrs
      ,TYPEINFO.TenToMin

 FROM  
   (	
	SELECT DISTINCT Crew,CrewMember,TypeCode,Descriptions,PIC,SIC,AsOf,FlightHours ,
                    ISNULL(TimeInTypeHrs,0) TimeInTypeHrs,ISNULL(Simulator,0)Simulator,ISNULL(totalhrs,0) totalhrs,TenToMin,Date FROM #TYPEINFO
    )TYPEINFO
    GROUP BY  TYPEINFO.Crew,TYPEINFO.CrewMember,TYPEINFO.TypeCode,TYPEINFO.Descriptions,TYPEINFO.PIC,TYPEINFO.SIC,TYPEINFO.AsOf,TYPEINFO.FlightHours,TYPEINFO.TenToMin
     ORDER BY Crew,TypeCode
END
  
IF OBJECT_ID('tempdb..#TYPEINFO') IS NOT NULL
DROP TABLE  #TYPEINFO  
  
IF OBJECT_ID('tempdb..#TYPEINFO1') IS NOT NULL
DROP TABLE  #TYPEINFO1    
-- EXEC spGetReportPOSTTimeinTypeInformation 'jwilliams_11','2009-5-5','','','' 

GO


