IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBPayableVendors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBPayableVendors]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
  
CREATE PROCEDURE  [dbo].[spGetReportDBPayableVendors]   
(  
        @UserCD AS VARCHAR(30) --Mandatory  
       ,@UserCustomerID AS VARCHAR(30)  
       ,@VendorCD AS VARCHAR(500)  
)  
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportDBPayableVendors  
-- Author: Askar R  
-- Create date: 23 Feb 2013  
-- Description: Get Vendor  Information  
-- Revision History  
-- Date        Name        Ver         Change  
--   
-- ================================================================================  
SELECT TEMP.* FROM 
(
SELECT DISTINCT --Vendor General Infromation----  
       V.VendorCD VENDORCode,  
       VendorContactCD,
       V.Name CompanyName,  
       ISNULL(V.BillingPhoneNum,'') Phone,  
       ISNULL(V.BillingFaxNum,'') Fax,  
       ISNULL(V.BillingAddr1,'') Addr1,  
       ISNULL(V.BillingAddr2,'') Addr2,  
       ISNULL(V.BillingAddr3,'') Addr3,  
       ISNULL(V.BillingCity,'') City,  
       ISNULL(V.BillingState,'') StateProv,  
       ISNULL(V.BillingZip,'') Postal,  
       ISNULL(CR.CountryCD,'') Country,
       ISNULL(M.MetroCD,'') METRO , 
       ISNULL(A.IcaoID,'') HOMEBASE,  
       ISNULL(CT.IcaoID,'') CLOSESTICAO,  
       ISNULL(V.TaxID,'') TAXID,  
       ISNULL(V.Terms,'') TERMS,  
       ISNULL(V.Notes,'') Notes,  
       --Vendor Contact Infromation----  
       ISNULL(VC.IsChoice,0) MAINCHOICE,  
       VC.FirstName +' '+VC.LastName ConName,  
       ISNULL(VC.PhoneNum,'') ConPHONE,  
       ISNULL(VC.FaxNum,'') ConFAX,  
       ISNULL(VC.Addr1,'') ConAddr1,  
       ISNULL(VC.Addr2,'') ConAddr2, 
       ISNULL(VC.Addr3,'') ConAddr3,  
       ISNULL(VC.CityName,'') ConCity,  
       ISNULL(VC.StateName,'') ConStateProv,  
       ISNULL(VC.PostalZipCD,'') ConPostal,  
       ISNULL(CRC.CountryCD,'') ConCountry,  
       ISNULL(VC.MoreInfo,'') CREAddInfo ,
       ISNULL(VC.Notes,'') ConNotes,
       V.HomebaseID
       FROM Vendor V  INNER JOIN VendorContact VC ON V.VendorID=VC.VendorID 
                            LEFT JOIN Company C ON V.HomebaseID=C.HomebaseAirportID  
                            LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID  
                            LEFT JOIN Airport CT ON V.AirportID=CT.AirportID  
                            LEFT JOIN Country CR ON CR.CountryID=V.CountryID 
                            LEFT JOIN Country CRC ON CRC.CountryID=VC.CountryID  
                            LEFT JOIN Metro M ON M.MetroID=V.MetroID
       WHERE V.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
         AND V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ','))  
         AND V.CustomerID=CONVERT(BIGINT,@UserCustomerID)   
         --AND IsChoice=1
       And V.IsInActive=1 
       And V.IsDeleted=0  
       AND VC.IsDeleted=0
       )TEMP
  ORDER BY MAINCHOICE DESC,VendorContactCD
END  
--SELECT * FROM VendorContact WHERE CustomerID=10099
--SELECT * FROM Vendor WHERE CustomerID=10099
 --EXEC spGetReportDBPayableVendors 'SUPERVISOR_99',10099,'P1000'


GO


