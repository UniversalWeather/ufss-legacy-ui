IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportUtilitiesAirportPairs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportUtilitiesAirportPairs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportUtilitiesAirportPairs]
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT,
    @AirportPairNumber AS BIGINT
    
AS
BEGIN

SET NOCOUNT ON;

SELECT DISTINCT 
        AP.AirportPairNumber
      ,AP.AirportPairDescription
      ,AC.AircraftCD
      ,AD.QTR
      ,D.IcaoID AS DEPART
      ,A.IcaoID AS ARRIVE 
      ,AD.Distance
      ,AD.PowerSetting
      ,AD.TakeoffBIAS
      ,AD.LandingBIAS
      ,AD.TrueAirSpeed
      ,AD.Winds
      ,AD.ElapseTM
      ,AD.Cost
      ,AD.LegID
      ,AD.WindReliability
      ,AP.LastUpdTS
      ,AP.LastUpdUID
 FROM AirportPair AP
      JOIN AirportPairDetail AD ON AP.AirportPairID = AD.AirportPairID
      JOIN Aircraft AC ON AD.AircraftID = AC.AircraftID
      JOIN Airport D ON AD.DAirportID = D.AirportID        
      JOIN Airport A ON AD.AAirportID = A.AirportID
 WHERE (AP.AirportPairNumber IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportPairNumber, ','))OR @AirportPairNumber='')
    AND AP.CustomerID = @UserCustomerID 
    ORDER BY AD.LegID
   
 --EXEC spGetReportUtilitiesAirportPairs 'SUPERVISOR_99',10099,2
 
 END

GO


