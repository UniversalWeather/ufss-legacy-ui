IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFlightCatagoryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFlightCatagoryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFlightCatagoryInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportFlightCatagoryInformation
-- Author: SUDHAKAR J
-- Create date: 18 Jun 2012
-- Description: Get Flight Catagory Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================

SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		F.CustomerID
		,F.FlightCatagoryCD
		,FlightCatagoryDescription
		,C.ClientCD
	FROM FlightCatagory F
	LEFT OUTER JOIN Client C ON F.ClientID = C.ClientID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = @CUSTOMERID
	AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	-- What about 'IsInactive', need to be considered in filter??
	ORDER BY F.CustomerID, F.FlightCatagoryCD
  -- EXEC spGetReportFlightCatagoryInformation 'UC'

GO


