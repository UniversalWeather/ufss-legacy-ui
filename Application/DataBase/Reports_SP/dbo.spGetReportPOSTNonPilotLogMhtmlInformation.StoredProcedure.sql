IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotLogMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotLogMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







        CREATE Procedure [dbo].[spGetReportPOSTNonPilotLogMhtmlInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupcd AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@PrintOnTime AS bit=0, 
		@PrintOffTime AS bit=0  
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTNonPilotLogMhtmlInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight NonPilot Log for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
	DECLARE @AppDTFormat VARCHAR(25)
    SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END
	       
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=1

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT,
crewcd varchar(30),
LASTNAME VARCHAR(80),
FIRSTNAME VARCHAR(80),
MiddleInitial VARCHAR(80)
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID ,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID ,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END

--select * from @TempCrewID
-----------------------------Crew and Crew Group Filteration---------------------- 
CREATE TABLE #DUTYTOTAL
(
POLogID BIGINT
,LOGNUM INT
,LegID BIGINT
,IsStatus nvarchar(1)
,DutyBegin VARCHAR(20)
,DutyEnd VARCHAR(20)
,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
DutyBegin VARCHAR(20),
DutyEnd VARCHAR(20),
POLogID BIGINT,
LegID BIGINT
,CrewID BIGINT
)
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE PM.LogNum in(SELECT DISTINCT LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0 
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted=0)
AND PM.IsDeleted=0
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID FROM 
(SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID


UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NULL

SELECT 
     A.LOGNUM,A.CREWID,A.LEGID,A.FARNUM,A.CREWCODE,A.TYPE_CODE,A.DUTYEND,A.LAST_NAME,A.FIRST_NAME,A.MIDDLEINIT
	,A.SCHEDTTM,A.TAIL_NMBR,A.CREWCURR,A.DEPICAO_ID,A.ARRICAO_ID,A.DUTYCOUNTER,A.DC,A.FLTDATE,A.DUTYTOTAL
	,A.BLKOUT,A.BLKIN,A.DUTYHRS,A.BLK_HRS,A.FLT_HRS,A.ASSOC_CREW,A.TOT_LINE,A.OVERRIDE1,A.DUTYBEG,A.END1
	,A.ONTIME,A.OFFTIME,a.item,a.last_accepted,a.duedate,a.alertdate,a.msgcode,A.SPEC3,A.SPEC4,A.SPECLONG3,A.SPECLONG4,A.SPECDEC3,A.SPECDEC4,A.DATFORMAT 
	INTO #TEMP
	FROM(
	Select Distinct
			[lognum]=PM.LOGNUM,
			[legid]=PL.POLegID,
			[CREWID]=C.CREWID,
			[farnum]=PL.FedAviationRegNum,
			[crewcode]=C.CrewCD,
			[type_code]=ac.AircraftCD,
			[dutyend]=(CASE WHEN (PL.IsDutyEnd=1) THEN 'TRUE'
			                WHEN (PL.IsDutyEnd=0) THEN 'FALSE'
			                END),
			[last_name]=C.LastName,
			[first_name]=C.FirstName,
			[middleinit]=C.MiddleInitial,
			[schedttm]=PL.ScheduledTM,
			[tail_nmbr]=F.TAILNUM,
			[crewcurr]=PL.CrewCurrency,
			[depicao_id]=AD.IcaoID,
			[arricao_id]=AA.IcaoID,
			[dutycounter]=NULL,--case (ISNULL(PC.dutyend,'')) when '' then ''  ELSE CONVERT(VARCHAR(5),@dutycounter12+1)  END ,
			[dc]=ISNULL(PC.dutyend,''),
			[fltdate]=pl.ScheduledTM,                
			[dutytotal]= (CASE WHEN (PC.dutyend is not null) THEN ROUND(PC.dutyhours,1)
			               END),
			
			[blkout]=PL.BlockOut,
			[blkin]=PL.BlockIN,
			[dutyhrs]=ROUND(PC.DutyHours,1),
			[blk_hrs]=ROUND(PC.BlockHours,1),
			[flt_hrs]=ROUND(PC.FlightHours,1),
			[assoc_crew]=ACrew.AssociatedCrew, 
			[tot_line]=' ',
			[override1]=(CASE WHEN(PC.IsOverride=1) THEN 'TRUE'
			                WHEN (PC.IsOverride=0) THEN 'FALSE'
			                END),
			[dutybeg]=PC.BeginningDuty,
			[end1]=PC.DutyEnd,
			[item]=ACHKLIST.AssociatedCHKLIST,
			[last_accepted]=ACHKLISTDT.ACHKLTPREVIEWDT,
			[duedate]= ACHKLISTDT.ACHKLTDUEDT,
			[alertdate]= ACHKLISTDT.ACHKLTALERTDT ,
			[msgcode]=ACHKLISTDT.MsgCd ,
			[ontime]=CASE WHEN PL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE (PC.BeginningDuty) END,
			[offtime]=PC.DutyEnd,
			[spec3]=PC.Specification3,
			[spec4]=PC.Specification4,
			[speclong3]=CM.SpecificationLong3,
			[speclong4]=CM.SpecificationLong4,
			[specdec3]=CM.Specdec3,
			[specdec4]=CM.Specdec4
			,[DATFORMAT]=@DATEFORMAT
			  	FROM PostflightLeg PL 
		INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0
		INNER JOIN  PostflightCrew PC ON PC.POLegID = PL.POLegID		
		INNER JOIN  Fleet F ON F.FleetID = PM.FleetID
	    LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=PM.POLogID AND DT.LegID=PL.POLegID
		INNER JOIN (  
        SELECT DISTINCT C.CustomerID, C.CREWID,CG.CrewGroupCD,C.LastName,C.CrewCD, CG.HomebaseID,c.FirstName,c.MiddleInitial
        FROM CREW C  
        LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
		left outer JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
        WHERE c.IsDeleted = 0 
		--AND (CG.CREWGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CREWGroupCD, ',')) OR @CREWGroupCD = '')
		)C ON Pc.CREWID = C.CREWID AND Pc.CustomerID = C.CustomerID 
		
		INNER JOIN (SELECT DISTINCT PC1.POLegID, AssociatedCrew = (SELECT RTRIM(C.CrewCD) + ',' 
		FROM PostflightCrew PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
				WHERE PC.POLegID = PC1.POLegID 
				order by crewcd FOR XML PATH(''))
				FROM PostflightCrew PC1) ACrew ON PL.POLegID = ACrew.POLegID  
		
		LEFT OUTER JOIN 
		(SELECT DISTINCT PC1.CrewID, AssociatedCHKLIST = 
		(SELECT DISTINCT RTRIM(CCL.CrewChecklistDescription)  + ','
		FROM PostflightCrew PC 
		left outer JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		left outer join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID FOR XML PATH(''))
			FROM PostflightCrew PC1) ACHKLIST
			ON PC.CrewID = ACHKLIST.CrewID  
		
	    LEFT OUTER JOIN 
		(SELECT DISTINCT PC1.CrewID,PC1.CustomerID,ACHKLTPREVIEWDT = 
		(SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.PreviousCheckDT),101) +'QQ'
		FROM PostflightCrew PC 
		LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
			FOR XML PATH('')),
		ACHKLTDUEDT=(SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.DueDT),101) +'QQ'
		FROM PostflightCrew PC 
		LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID
			FOR XML PATH('')),
			ACHKLTALERTDT=(SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.AlertDT),101) +'QQ'
		FROM PostflightCrew PC 
		LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID
			FOR XML PATH('')),
			ACHKLTGRACEDT=(SELECT DISTINCT CONVERT(VARCHAR(10),CONVERT(DATE,CCD.GraceDT),101) +'QQ'
		FROM PostflightCrew PC 
		LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID AND PC.CustomerID=PC1.CustomerID 
			FOR XML PATH('')),
			MsgCd= (SELECT DISTINCT RTRIM((CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
						   WHEN (@DATETO > CCD.DUEDT) THEN 'PAST DUE' END))  + 'QQ'
		FROM PostflightCrew PC 
		left outer JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID and CCD.IsDeleted=0
		left outer join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID and CCl.IsDeleted=0
			WHERE PC.CrewID = PC1.CrewID FOR XML PATH(''))
			FROM PostflightCrew PC1) ACHKLISTDT
			ON PC.CrewID = ACHKLISTDT.CrewID AND PC.CustomerID=ACHKLISTDT.CustomerID
			

			
        INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID 
		INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID	
		left outer JOIN  Company CM ON CM.HomebaseID = PM.HomebaseID
		left outer JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID AND CCD.CustomerID=PC.CustomerID	
		left outer join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND CCL.CustomerID=PC.CustomerID
		left outer JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID        
		
	 
		WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		  AND PL.IsDeleted=0
				 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
				 AND (PC.Dutytype not IN('P','S') or PC.Dutytype is null)
				 --AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
                 AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = ''))A
                 

 declare @lognum bigint,@CREWID BIGINT,@legid bigint,@farnum as char(3),@crewcode varchar(5),@type_code as char(3),@dutyend varchar(20),@last_name varchar(30),@first_name varchar(20),@middleinit varchar(20)
	,@schedttm  datetime,@tail_nmbr varchar(9),@crewcurr as char(3),@depicao_id as char(4),@arricao_id as char(4),@dutycounter varchar(20),@dc varchar(5),@fltdate datetime,@dutytotal varchar(20)
	,@blkout varchar(5),@blkin varchar(5),@dutyhrs numeric(7,3),@blk_hrs numeric(7,3),@flt_hrs numeric(7,3),@assoc_crew varchar(20),@tot_line varchar(20),@override1 varchar(20),@dutybeg varchar(5),@end1 varchar(5)
	,@ontime varchar(5),@offtime varchar(5),@item varchar(125),@last_accepted VARCHAR(160),@duedate VARCHAR(160),@alertdate VARCHAR(160),@msgcode VARCHAR(160),@spec3 numeric(7,3),@spec4 numeric(7,3),@speclong3 varchar(25),@speclong4 varchar(25),@specdec3 int,@specdec4 int,@DATFORMAT varchar(25);--@dutycounter varchar(20),;
 DECLARE @PrevCount INT,@DUTYCOUNT VARCHAR(20);
 DECLARE @DUTYCOUNT1 INT=0;
 DECLARE curTailInfo CURSOR FAST_FORWARD READ_ONLY FOR  SELECT * FROM  #TEMP ORDER BY CREWCODE
 
OPEN curTailInfo;              
 -- PRINT @@CURSOR_ROWS              
  FETCH NEXT FROM curTailInfo INTO 
     @lognum,@CREWID,@legid,@farnum,@crewcode,@type_code,@dutyend,@last_name,@first_name,@middleinit
	,@schedttm,@tail_nmbr,@crewcurr,@depicao_id,@arricao_id,@dutycounter,@dc,@fltdate,@dutytotal
	,@blkout,@blkin,@dutyhrs,@blk_hrs,@flt_hrs,@assoc_crew,@tot_line,@override1,@dutybeg,@end1
	,@ontime,@offtime,@item,@last_accepted,@duedate,@alertdate,@msgcode,@spec3,@spec4,@speclong3,@speclong4,@specdec3,@specdec4,@DATFORMAT
      
 -- PRINT @DUTYCOUNT1     
WHILE @@FETCH_STATUS = 0              
BEGIN
 IF @dc='0'
 BEGIN 
  UPDATE #TEMP SET DUTYCOUNTER=0 WHERE LEGID=@LEGID AND CREWID=@CREWID;
 END
ELSE IF @dc<>'0' AND @dc<>''
 BEGIN
    SET @DUTYCOUNT1=@DUTYCOUNT1+1;
    UPDATE #TEMP SET DUTYCOUNTER=@DUTYCOUNT1 WHERE LEGID=@LEGID AND CREWID=@CREWID;
 END
 ELSE IF @dc=''
 BEGIN
   UPDATE #TEMP SET DUTYCOUNTER=@PrevCount WHERE LEGID=@LEGID AND CREWID=@CREWID;
 END
 
  SET @PrevCount=@DUTYCOUNT1
    FETCH NEXT FROM curTailInfo INTO 
     @lognum,@CREWID,@legid,@farnum,@crewcode,@type_code,@dutyend,@last_name,@first_name,@middleinit
	,@schedttm,@tail_nmbr,@crewcurr,@depicao_id,@arricao_id,@dutycounter,@dc,@fltdate,@dutytotal,@blkout
	,@blkin,@dutyhrs,@blk_hrs,@flt_hrs,@assoc_crew,@tot_line,@override1,@dutybeg,@end1,@ontime,
	@offtime,@item,@last_accepted,@duedate,@alertdate,@msgcode,@spec3,@spec4,@speclong3,@speclong4,
	@specdec3,@specdec4,@DATFORMAT;                      
END            
 CLOSE curTailInfo;              
 DEALLOCATE curTailInfo;
   
     
--SELECT * FROM  #TEMP ORDER BY CREWCODE 
IF @IsZeroSuppressActivityCrewRpt = 0
BEGIN     
SELECT * FROM  #TEMP  --WHERE DUTYCOUNTER <> 0
     UNION ALL

SELECT DISTINCT null,null,crewid,null,crewcd,'NO ACTIVITY','FALSE',LASTNAME,FIRSTNAME,MiddleInitial,
	            null,null,null,null,null,null,null,null,null,null,
			    null,null,null,null,null,null,'FALSE',null,null,null,
			    null,null,null,null,null,null,null,null,null,null
			    ,null,null,@DATFORMAT
			    FROM @TempCrewID WHERE CrewID NOT IN(SELECT CrewID FROM  #TEMP)
     ORDER BY crewcode
END
ELSE
BEGIN
SELECT * FROM  #TEMP
 ORDER BY crewcode
END	 


IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
drop table #TEMP

IF OBJECT_ID('tempdb..#DUTYTOTAL') IS NOT NULL
drop table #DUTYTOTAL

   -- EXEC spGetReportPOSTNonPilotLogMhtmlInformation 'eliza_9','2010/8/3','2012/9/5','','',''
   --EXEC spGetReportPOSTNonPilotLogInformation 'TIM','2010/8/3','2012/9/5','','',''



GO


