IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSubPilotLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSubPilotLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  CREATE Procedure [dbo].[spGetReportPOSTSubPilotLogInformation]
(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewID AS NVARCHAR(1000), -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(1000) = '',
		@FlightCatagoryCD AS CHAR(1000) = '',
		@PrintOnTime BIT = 0,
		@PrintOffTime BIT = 0
		)
  AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTSubPilotLogInformation
-- Author: A.AKHILA
-- Create date: 
-- Description: Get Postflight SUB Pilot Log for REPORTS
-- Revision History
-- Date Name Ver Change
-- 
-- ================================================================================
SET NOCOUNT ON
--DROP TABLE #DUTYTOTAL
CREATE TABLE #DUTYTOTAL
(
POLogID BIGINT
,LOGNUM INT
,LegID BIGINT
,IsStatus nvarchar(1)
,DutyBegin VARCHAR(20)
,DutyEnd VARCHAR(20)
,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
DutyBegin VARCHAR(20),
DutyEnd VARCHAR(20),
POLogID BIGINT,
LegID BIGINT
,CrewID BIGINT
)
 
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0 
INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE PM.IsDeleted=0 AND
PM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND PM.IsDeleted=0)
 
 
 
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID FROM 
(SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID
 
 
 
UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NULL
 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 --AND IsStatus = 1
IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO 
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
 
SELECT DISTINCT --TOP 1
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) 
,[DateFROM]=CONVERT(DATE,@DATEFROM)
,[DATETO]= CONVERT(DATE, @DATETO) 
,[CREWID]= CW.CrewID
,[LEGID]=POL.POLegID
,[LOGID]=POM.LogNum
,[Date] = CONVERT(DATE,POL.OutboundDTTM)
,[Tail Number] = F.TailNum
,[Type] = AC.AircraftCD
,[FltTyp] = POL.FedAviationRegNum
,[Flt Cat] = ISNULL(FC.FlightCatagoryCD,' ')
,[DepICAO] = A.IcaoID
,[ArrICAO] = AA.IcaoID
,[BlkOut] = ISNULL(REPLACE(POL.BlockOut,':',''),0000)
,[BlkIn] = ISNULL(REPLACE(POL.BlockIN,':',''),0000) 
,[Client] = ISNULL(CT.ClientCD,' ')
,[Crew Duty Type] = ISNULL(CDT.DutyTypeCD,' ') 
,[Duty Hours P S]= ISNULL(ROUND(POC.DutyHours,1),0)
,[DutyTYPE] = POC.DutyTYPE
,[DUTYTOTAL] = (CASE WHEN (POL.IsDutyEnd=1) then ROUND(POC.DutyHours,1)
                      END)
--,[DUTYTOTAL] = ISNULL(CASE WHEN(CASE WHEN CONVERT(DATE,POL.InboundDTTM) <= CONVERT(DATE,@DATETO) THEN 'True' ELSE 'False' END) = 'True' 
--OR(((CASE WHEN CONVERT(DATE,POC.CrewDutyStartTM) >= CONVERT(DATE,@DATEFROM) THEN POC.BeginningDuty END) <> '')AND(
-- (CASE WHEN CONVERT(DATE,POC.CrewDutyStartTM) <= CONVERT(DATE,@DATETO) THEN POC.DutyEnd END) <> ''))THEN POC.DutyHours END ,0)
 
,[Block Time] = ISNULL(ROUND(POC.BlockHours,1),0)
,[Flight Time] = ISNULL(ROUND(POC.FlightHours,1),0)
,[Nite Time] = ISNULL(POC.Night,0)
,[Inst Time] = ISNULL(POC.Instrument,0)
,[T/OFF Dt] = ISNULL(POC.TakeOffDay,0)
,[T/OFF Nt] = ISNULL(POC.TakeOffNight,0)
,[Appr Pr] = ISNULL(POC.ApproachPrecision,0)
,[Appr Npr] = ISNULL(POC.ApproachNonPrecision,0)
,[Lnding Dt] = ISNULL(POC.LandingDay,0)
,[Lnding Nt] = ISNULL(POC.LandingNight,0)
,[SpecificationLong3] = isnull(c.SpecificationLong3,'')
,[SpecificationLong4] = isnull(c.SpecificationLong4,'')
,[Specdec3] = C.Specdec3
,[Specdec4] = C.Specdec4
,[CUSTOM3DESCRIPTION]= (case when  c.Specdec3=0 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,0)))+'.'
						when c.Specdec3=1 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,1)))
						when c.Specdec3=2 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,2)))
						when c.Specdec3=3 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,3)))
						when c.Specdec3=4 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,4)))
						when c.Specdec3=5 and c.SpecificationLong3 is not null and c.SpecificationLong3<>'' then convert(varchar(20),cast(POC.Specification3 as numeric(7,5)))
						end ) 
,[CUSTOM4DESCRIPTION]= (case when  C.Specdec4=0 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,0)))+'.'
						when C.Specdec4=1 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,1)))
						when C.Specdec4=2 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,2)))
						when C.Specdec4=3 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,3)))
						when C.Specdec4=4 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,4)))
						when C.Specdec4=5 and C.SpecificationLong4 is not null and C.SpecificationLong4<>'' then convert(varchar(20),cast(POC.Specification4 as numeric(7,5)))
						end) 
,[RONs] = ISNULL(POC.RemainOverNight,0)
-- ,[RON1]=POL1.Ron1
-- ,[RON2]= 
--(CASE WHEN POL.IsDutyEnd=1 THEN 
-- (CASE WHEN CONVERT(DATE,POL.InboundDTTM) < CONVERT(DATE,@DATETO) THEN 
-- (CASE WHEN (POL1.Ron1>POL.LegNUM) THEN 
-- (CASE WHEN CONVERT(DATE,PLNEXT.ScheduledTM)<= CONVERT(DATE,@DATETO) THEN 
-- (CASE WHEN CONVERT(DATE,PLNEXT.ScheduledTM)> CONVERT(DATE,POL.InboundDTTM) THEN DATEDIFF(DAY,POL.InboundDTTM,PLNEXT.ScheduledTM) END)
-- ELSE (CASE WHEN CONVERT(DATE,@DATETO) > CONVERT(DATE,POL.InboundDTTM) THEN DATEDIFF(DAY,POL.InboundDTTM,@DATETO+1)END) 
-- END) END) END) ELSE '' END)
,[CREWCD]= CW.CrewCD
,[Associated Crew] = ACrew.AssociatedCrew
,[PILOT] = ISNULL(CW.CrewCD +' '+ CW.LastName +','+ CW.FirstName +''+ CW.MiddleInitial,' ')
,[PILOTLast] = ISNULL(CW.LastName,'')
,[PILOTFirst] = ISNULL(CW.FirstName,'')
,[PILOTMiddle] = ISNULL(CW.MiddleInitial,'')
,[ON/OFF]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off' 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On' 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END)
,[ON] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN 'On' ELSE '' END
,[OFF] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN 'Off'  ELSE '' END 
,[BLK]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') 
WHEN (POL.IsDutyEnd=1 and(@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','')+'/'+REPLACE(ISNULL(DT.DutyEnd,''),':','')
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
 ,[BLKON] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOnTime=1) THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE '' END
,[BLKOFF] = CASE WHEN (POL.IsDutyEnd=1 and @PrintOffTime=1) THEN REPLACE(ISNULL(DT.DutyEnd,''),':','') ELSE '' END
,[BLK2]= (CASE WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=0)) THEN REPLACE(POC.BeginningDuty,':','') 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=1)) THEN REPLACE(POC.DutyEnd,':','') 
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=1) AND (@PrintOffTime=1)) THEN REPLACE(ISNULL(POC.BeginningDuty,''),':','')+'/'+REPLACE(ISNULL(POC.DutyEnd,''),':','')
WHEN (POL.IsDutyEnd=1 and (@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
 
,[TenToMin] = @TenToMin
INTO #CREWINFO
FROM PostflightMain POM 
LEFT OUTER JOIN (SELECT POLOGID,max(legnum) Ron1 FROM PostflightLeg WHERE IsDeleted=0 GROUP BY POLogID)POL1 ON POM.POLogID=POL1.POLOGID
INNER JOIN PostflightCrew POC ON POC.POLogID = POM.POLogID 
INNER JOIN Crew CW ON CW.CrewID = POC.CrewID AND CW.IsDeleted = 0 --AND CW.IsStatus = 1
INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted=0
LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
LEFT JOIN PostflightSimulatorLog POSL ON POSL.CrewID = POC.CrewID
LEFT JOIN Client CT ON CT.ClientID = POSL.ClientID
LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeID = POSL.DutyTypeID
INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
INNER JOIN Company c on c.HomebaseID = POM.HomebaseID
LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=POM.POLogID AND DT.LegID=POL.POLegID
-- LEFT OUTER JOIN (
--SELECT PL.POLogID, PL.POLegID, PL.LegNUM, PL.ScheduledTM
--FROM POSTflightLeg PL 
--) PLNEXT ON PLNEXT.POLogID = POM.POLogID AND POL.LegNUM + 1 = PLNEXT.LegNUM
LEFT OUTER JOIN (
SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
SELECT RTRIM(C.CrewCD) + ','
FROM PostflightCrew PC 
INNER JOIN Crew C ON PC.CrewID = C.CrewID
WHERE PC.POLegID = PC1.POLegID
order by C.CrewCD
FOR XML PATH('')
)
FROM PostflightCrew PC1
) ACrew ON POL.POLegID = ACrew.POLegID 
 
WHERE POM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
AND POM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)AND PM.IsDeleted=0  AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
AND (poc.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')
AND (Ac.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
AND  POM.IsDeleted=0
AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
 
SELECT * FROM #CREWINFO
UNION ALL
SELECT DISTINCT --TOP 10
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO) 
,[DateFROM]=CONVERT(DATE,@DATEFROM)
,[DATETO]= CONVERT(DATE, @DATETO) 
,[CREWID]= CW.CrewID
,[LEGID]=NULL
,[LOGID]=NULL
,[Date] = NULL
,[Tail Number] = NULL
,[Type] = 'NO'
,[FltTyp] = NULL
,[Flt Cat] = NULL
,[DepICAO] =NULL
,[ArrICAO] = NULL
,[BlkOut] = NULL
,[BlkIn] = NULL
,[Client] =NULL
,[Crew Duty Type] = NULL
,[Duty Hours P S]= '0'
,[DutyTYPE] = NULL
,[DUTYTOTAL] = NULL
 
 
,[Block Time] = '0'
,[Flight Time] = '0'
,[Nite Time] = '0'
,[Inst Time] = '0'
,[T/OFF Dt] = NULL
,[T/OFF Nt] = NULL
,[Appr Pr] = NULL
,[Appr Npr] = NULL
,[Lnding Dt] = NULL
,[Lnding Nt] = NULL
,[SpecificationLong3] = NULL
,[SpecificationLong4] = NULL
,[Specdec3] = NULL
,[Specdec4] = NULL
,[CUSTOM3DESCRIPTION]= NULL
,[CUSTOM4DESCRIPTION]= NULL
,[RONs] = NULL
 
,[CREWCD]= CW.CrewCD
,[Associated Crew] = NULL
,[PILOT] = ISNULL(CW.CrewCD +' '+ CW.LastName +','+ CW.FirstName +''+ CW.MiddleInitial,' ')
,[PILOTLast] = ISNULL(CW.LastName,'')
,[PILOTFirst] = ISNULL(CW.FirstName,'')
,[PILOTMiddle] = ISNULL(CW.MiddleInitial,'')
,[ON/OFF]= NULL
 ,[ON] = NULL
,[OFF] = NULL
,[BLK]=NULL
 ,[BLKON] = NULL
,[BLKOFF] = NULL
,[BLK2]=NULL
 
,[TenToMin] = @TenToMin
FROM Crew CW
INNER JOIN @CREW CT ON CW.CrewID = CT.CREWID
-- left outer JOIN PostflightCrew POC ON POC.CrewID = CW.CrewID AND poc.CustomerID = CW.CustomerID 
WHERE 
CW.IsDeleted=0 and --CW.IsStatus=1 
 CW.CREWID NOT IN (SELECT DISTINCT T.CREWID FROM #CREWINFO T)
AND CW.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
 AND (cw.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ',')) OR @CrewID = '')

ORDER BY [CREWCD]
--EXEC spGetReportPOSTSubPilotLogInformation 'jwilliams_13','2009/01/01','2009/01/15','',1,1




GO


