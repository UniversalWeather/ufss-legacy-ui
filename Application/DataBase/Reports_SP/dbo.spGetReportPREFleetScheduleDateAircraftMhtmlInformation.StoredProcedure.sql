IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetScheduleDateAircraftMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetScheduleDateAircraftMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPREFleetScheduleDateAircraftMhtmlInformation]

		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPREFleetScheduleDateAircraftExportInformation
-- Author: SINDHUJA.K
-- Create date: 18 July 2012
-- Description: Get Preflight Fleet Schedule/DateAircraft Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON  
	    Declare @FromDate datetime  
		Declare @ToDate datetime  
		Declare @Count int  
		Declare @index int  
		DECLARE @NextString NVARCHAR(40)
		DECLARE @Pos INT
		DECLARE @NextPos INT
		DECLARE @String NVARCHAR(40)
		DECLARE @Delimiter NVARCHAR(40)
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100) 
		
		Set @FromDate = @DATEFROM  
		Set @ToDate = @DATETO  

		-- [Start] Storing given Tail Numbers [User Ipnut] in to Temp tables --------------
		DECLARE  @TempTailNum  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		activedate DATE,
		tail_nmbr char(6),
		FleetID BIGINT,
		deptime datetime,
		arrtime datetime,
		depicao_id CHAR(4),
		arricao_id CHAR(4),
		dep_city varchar(25),
		arr_city varchar(25),
		dep_airport varchar(25),
		arr_airport varchar(25),
		dep_state varchar(25),
		arr_state varchar(25),
		dep_country varchar(60),
		arr_country varchar(60),
		elp_time numeric(7,3),
		[desc]  varchar(40),
		pax_total int
		--,orig_nmbr bigint,
		--legid bigint,
		--paxno INT  , 
		--paxname varchar(60)	     
	)
	
		
	Set @Count=DATEDIFF(DAY,@FromDate,@ToDate)  
	Set @index=0  

	while(@index<=@Count)  
	Begin  
	--print 'Index:' + convert(varchar,@index)
			insert into @TempTailNum 
			(
				activedate,
				tail_nmbr,
				FleetID,
				depicao_id,
				dep_airport,
				dep_state,
				dep_country,
				dep_city ,
				arr_city 
			)   
			Select  DISTINCT @FromDate,
			        F.TailNum, 
			      F.FleetID,
			        A.IcaoID,  
                    A.StateName,
                    A.AirportName,
                    A.CountryName,
                    A.CityName, 
				    Case When C.HomebaseAirportID = A.AirportID  
								Then 'AIRCRAFT AT HOME BASE' ELSE  'AIRCRAFT AWAY OVERNIGHT' END 
			FROM  PreflightMain PM         
			  Inner Join PreflightLeg PL On PM.TripID = PL.TripID AND PL.ISDELETED = 0        
			  INNER JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomebaseID
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
					OR @FleetGroupCD = ''
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			  INNER JOIN Company C ON F.HomebaseID=C.HomebaseID
			  OUTER APPLY dbo.GetFleetCurrentAirpotCD(DATEADD(DAY,@index,@FromDate), F.FleetID) AS A
			  WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
			  AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')  

		Set @index=@index+1  
		SET @FromDate=@FromDate+1 
	END
			
		-- [End] Storing given Tail Numbers in to Temp tables --------------

		DECLARE  @TempTable  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1),
		activedate DATE,
		tail_nmbr char(6),
		FleetID BIGINT,
		deptime datetime,
		arrtime datetime,
		depicao_id CHAR(4),
		arricao_id CHAR(4),
		dep_city varchar(25),
		arr_city varchar(25),
		dep_airport varchar(25),
		arr_airport varchar(25),
		dep_state varchar(25),
		arr_state varchar(25),
		dep_country varchar(60),
		arr_country varchar(60),
		elp_time numeric(7,3),
		[desc]  varchar(40),
		pax_total int     
	)  

           INSERT INTO @TempTable
                Select DISTINCT PL.DepartureDTTMLocal,
                         F.TailNum, 
                         F.FleetID,
					     PL.DepartureDTTMLocal,
					     PL.ArrivalDTTMLocal,
						 A.IcaoID as depicao_id,
						 A1.IcaoID as arricao_id,
						 A.CityName,
						 A1.CityName,
						 A.AirportName,
						 A1.AirportName,
						 A.StateName,
						 A1.StateName,
						 A.CountryName,
						 A1.CountryName, 
						 PL.ElapseTM as elp_time, 
						 PM.TripDescription as [desc], 
						 PL.PassengerTotal as pax_total
			FROM   
			PreflightMain PM         
			 Inner Join PreflightLeg PL On PM.TripID = PL.TripID AND PL.ISDELETED = 0        
			  INNER JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomebaseID
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
					OR @FleetGroupCD = ''
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID      
			 Inner Join AIRPORT A On PL.DepartICAOID=A.AirportID   
			 Inner Join AIRPORT A1 On PL.ArriveICAOID=A1.AirportID   
			 --Inner Join Country C On A.CountryID=C.CountryID  
			 ---Inner Join Country C1 On A1.CountryID=C1.CountryID  
		--	 INNER JOIN PreflightPassengerList PP1  ON PP1.LegID=PL.LegID
		---	 INNER JOIN Passenger P ON PP1.PassengerID=P.PassengerRequestorID
		       WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		       AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		       AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')  
		       AND PM.ISDELETED = 0

DELETE T1 FROM @TempTailNum T1
          INNER JOIN @TempTable T2 ON T1.FleetID = T2.FleetID
                 AND T1.activedate = T2.activedate



SELECT  A.* FROM 
(SELECT activedate,tail_nmbr,FleetID,deptime,arrtime,depicao_id,arricao_id,dep_city,arr_city,dep_airport,arr_airport,
	   dep_state,arr_state,dep_country,arr_country,ISNULL(elp_time,0)elp_time,[desc],ISNULL(pax_total,0)pax_total
	   FROM @TempTable 

UNION ALL

SELECT  activedate,tail_nmbr,FleetID,deptime,arrtime,depicao_id,arricao_id,dep_city,arr_city,dep_airport,arr_airport,
	   dep_state,arr_state,dep_country,arr_country,ISNULL(elp_time,0)elp_time,[desc],ISNULL(pax_total,0)pax_total
	    FROM @TempTailNum)A
ORDER BY A.activedate,tail_nmbr


END 
	 --ORDER BY PDate, tail_nmbr 
	
	-- EXEC spGetReportPREFleetScheduleDateAircraftEXPORTInformation 'JWILLIAMS_13','2012/08/01','2012/8/30','',''
  -- EXEC spGetReportPREFleetScheduleDateAircraftExportInformation 'UC','2012/7/20','2012/7/22','',''

GO


