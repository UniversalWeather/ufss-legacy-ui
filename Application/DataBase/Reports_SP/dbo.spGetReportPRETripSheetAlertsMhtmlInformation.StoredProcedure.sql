IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetAlertsMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetAlertsMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETripSheetAlertsMhtmlInformation]
	    @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TripNUM AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS
-- ===============================================================================
-- SPC Name: spGetReportPRETripSheetAlertsExportInformation
-- Author: Abhishek S
-- Create date: 18 Jul 2012
-- Description: Get Trip Sheet Alerts Information in excel format
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
	
	SET NOCOUNT ON;
	
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

Declare @SuppressActivityAircft BIT  
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMER
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(9)
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNUM
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNUM
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER
	  AND F.IsDeleted=0
	  AND F.IsInActive=0  
END
-----------------------------TailNum and Fleet Group Filteration----------------------

/*	
	-- DECLARE @ParameterDefinition AS NVARCHAR(100)
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(500)	
SET @SQLSCRIPT='SELECT 
	  [tail_nmbr] = F.TailNum
	 ,[orig_nmbr] = PM.TripNUM
	 ,[desc] = PM.TripDescription
	 ,[trip_dates]= [dbo].[GetTripDatesByTripIDUserCD](PM.TripID,@UserCD)
     ,[notes]=PM.Notes   --Added for Mhtml
	 FROM PreflightMain PM 
	 INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
		FROM Fleet F 
		LEFT OUTER JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
			OR @FleetGroupCD = '''' ) 
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	  
	WHERE PM.IsDeleted = 0
		  AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
		  AND PM.EstDepartureDT BETWEEN @DATEFROM AND @DATETO'

IF @TripNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND CONVERT(VARCHAR,PM.TripNUM) IN (''' +REPLACE( CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END ,',', ''', ''') + ''')';
	END       
    IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupID IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @HomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
	
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD VARCHAR(500),@TripNUM NVARCHAR(500)'
	
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD,@TripNUM 
		  
		--  EXECUTE sp_executesql @ParameterDefinition, @UserCD, @DATEFROM, @DATETO
*/
SELECT DISTINCT
	  [tail_nmbr] = F.TailNum
	 ,[orig_nmbr] = PM.TripNUM
	 ,[desc] = PM.TripDescription
	 ,[trip_dates]= [dbo].[GetTripDatesByTripIDUserCD](PM.TripID,@UserCD)
    ,[notes]=PM.Notes   
	 FROM PreflightMain PM
	 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0 
	 INNER JOIN (SELECT FleetID,TailNum FROM @TempFleetID) F ON F.FleetID=PM.FleetID 
	WHERE PM.IsDeleted = 0
	  AND ISNULL(PM.Notes,'') <> ''
	  AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	  AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN @DATEFROM AND @DATETO
	  AND ( PM.TripNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNUM, ',')) OR @TripNUM = '' ) 
	  AND ( PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @HomeBase = 0 ) 
		  
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'JWILLIAMS_11', '2012-01-01', '2012-12-09', '', '', '', 0
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'JWILLIAMS_11', '2012-01-01', '2012-12-09', '', 'N46E', '', 0
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'JWILLIAMS_11', '2012-01-01', '2012-12-09', '', '', '', 1


GO


