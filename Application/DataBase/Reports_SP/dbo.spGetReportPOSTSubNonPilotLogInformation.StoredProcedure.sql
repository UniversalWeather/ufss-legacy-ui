IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSubNonPilotLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSubNonPilotLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  CREATE Procedure [dbo].[spGetReportPOSTSubNonPilotLogInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TypeCode AS NVARCHAR(1000) = '',
		@PrintOnTime AS bit=0,   
        @PrintOffTime AS bit=0    
		
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTSubNonPilotLogInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight NonPilot Log for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 28-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

DECLARE @Custom SMALLINT = 0;
SELECT @Custom = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	

--DROP TABLE #DUTYTOTAL
CREATE TABLE #DUTYTOTAL
(
POLogID BIGINT
,LOGNUM INT
,LegID BIGINT
,IsStatus nvarchar(1)
,DutyBegin VARCHAR(20)
,DutyEnd VARCHAR(20)
,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
DutyBegin VARCHAR(20),
DutyEnd VARCHAR(20),
POLogID BIGINT,
LegID BIGINT
,CrewID BIGINT
)
 
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID 
INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE (PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
AND PM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID 
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
 
 
 
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID FROM 
(SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID
 
 
 
UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NULL

	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	
	DECLARE @Spec3 INT;
	DECLARE @SpecLong3 INT;
	DECLARE @Spec4 INT;
	DECLARE @SpecLong4 INT;
	SELECT @Spec3 = Specdec3,@SpecLong3=SpecificationLong3,@Spec4 = Specdec4,@SpecLong4=SpecificationLong4	
	 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)	
	
		create table #temp(DateFROM date,DATETO date,shdate date,TailNumber varchar(9),aType varchar(max),
		FltTyp char(3),DepICAO char(4),ArrICAO char(4),BlkOut varchar(5),BlkIn varchar(5),DutyHours numeric(7,3),bduty numeric(7,3),
		BlockTime numeric(7,3),FlightTime numeric(7,3),specification3 varchar(25),specification4 varchar(25),
		CUSTOM3DESCRIPTION varchar(20),CUSTOM4DESCRIPTION varchar(20),AssociatedCrew varchar(max),TenToMin int,crewid VARCHAR(200),
		Crew varchar(max),maincrew varchar(5),chk varchar(15),BLK varchar(15))
		
		create table #temp1(DateFROM date,DATETO date,shdate date,TailNumber varchar(9),aType varchar(max),
		FltTyp char(3),DepICAO char(4),ArrICAO char(4),BlkOut varchar(5),BlkIn varchar(5),DutyHours numeric(7,3),bduty numeric(7,3),
		BlockTime numeric(7,3),FlightTime numeric(7,3),specification3 varchar(25),specification4 varchar(25),
		CUSTOM3DESCRIPTION varchar(20),CUSTOM4DESCRIPTION varchar(20),AssociatedCrew varchar(max),TenToMin int,crewid VARCHAR(200),
		Crew varchar(max),maincrew varchar(5),chk varchar(15),BLK varchar(15))

INSERT INTO #temp

	 SELECT distinct
	    [DateFROM]=CONVERT(DATE,@DATEFROM),[DATETO]= CONVERT(DATE, @DATETO),   
		[shDate]=NULL, 
		[TailNumber]='', 
		[aType]='NO ACTIVIT',  
		[FltTyp]='', 
		[DepICAO]='', 
		[ArrICAO]='',  
		[BlkOut]=NULL, 
		[BlkIn]=NULL,  
		[DutyHours]=NULL, 
		[bduty]=null, 
		[BlockTime]=NULL,	
		[FlightTime]=NULL,  
		[specification3]=null,
		[specification4]=null, 
		[CUSTOM3DESCRIPTION]= (case when  CM.Specdec3=0 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,0)))+'.'
			                            when CM.Specdec3=1 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,1)))
			                            when CM.Specdec3=2 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,2)))
			                            when CM.Specdec3=3 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,3)))
			                            when CM.Specdec3=4 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,4)))
			                             when CM.Specdec3=5 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,5)))
			                            end )  ,  
			[CUSTOM4DESCRIPTION]= (case when  CM.Specdec4=0 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,0)))+'.'
			                            when CM.Specdec4=1 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,1)))
			                            when CM.Specdec4=2 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,2)))
			                            when CM.Specdec4=3 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,3)))
			                            when CM.Specdec4=4 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,4)))
			                            when CM.Specdec4=5 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,5)))
			                            end) ,     
		[AssociatedCrew]='',  
		[TenToMin]=@TenToMin,
		[crewid]=c.crewid,                 
		[Crew]=c.crewcd+' '+C.LastName+','+C.FirstName+' '+ISNULL(c.MiddleInitial,''),  
		--[DUTY] =PC.Dutytype, 
		[maincrew]=c.crewcd, 
		[chk]=(CASE WHEN ((@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off'  
					     WHEN ((@PrintOnTime=0) AND (@PrintOffTime=0)) THEN ''  
					     WHEN ((@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On'  
					     WHEN ((@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END),  	
		[BLK]= NULL
		 FROM PostflightLeg PL   
	INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0  
	INNER JOIN  PostflightCrew PC ON PC.POLegID = PL.POLegID 
	INNER JOIN Crew C ON C.CrewID = PC.CrewID    
	LEFT OUTER JOIN  Company CM ON CM.HomebaseID = PM.HomebaseID
	
 WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
          AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
           AND PC.Dutytype IN('P','S') and PC.DutyTYPE is null 
           --and c.IsDeleted = 0 AND c.IsStatus = 1
          AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
          AND PL.IsDeleted = 0
   UNION  
   SELECT distinct  
			[DateFROM]=CONVERT(DATE,@DATEFROM),[DATETO]= CONVERT(DATE, @DATETO),   
			[shDate] = PL.ScheduledTM, 
			[TailNumber]=F.TailNum, 
			[aType]=AC.AircraftCD,  
			[FltTyp]=PL.FedAviationRegNum,  
			[DepICAO]=AD.ICAOID, 
			[ArrICAO]=AA.ICAOID,  
			[BlkOut]=PL.BlockOut, 
			[BlkIn]=PL.BlockIN,  
			[DutyHours]=ROUND(PC.DutyHours,1), 
			[bduty]=(CASE WHEN (PC.DUTYEND IS NOT NULL) THEN ROUND(PC.DUTYHOURS,1) END), 
			[BlockTime]=ROUND(PC.BlockHours,1),  
			[FlightTime]=ROUND(PC.FlightHours,1), 
			[specification3]=CM.SpecificationLong3,
			[specification4]=CM.SpecificationLong4,
			[CUSTOM3DESCRIPTION]= (case when  CM.Specdec3=0 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,0)))+'.'
			                            when CM.Specdec3=1 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,1)))
			                            when CM.Specdec3=2 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,2)))
			                            when CM.Specdec3=3 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,3)))
			                            when CM.Specdec3=4 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,4)))
			                             when CM.Specdec3=5 and CM.SpecificationLong3 is not null and CM.SpecificationLong3<>'' then convert(varchar(20),cast(PC.Specification3 as numeric(7,5)))
			                            end )  ,  
			[CUSTOM4DESCRIPTION]= (case when  CM.Specdec4=0 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,0)))+'.'
			                            when CM.Specdec4=1 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,1)))
			                            when CM.Specdec4=2 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,2)))
			                            when CM.Specdec4=3 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,3)))
			                            when CM.Specdec4=4 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,4)))
			                            when CM.Specdec4=5 and CM.SpecificationLong4 is not null and CM.SpecificationLong4<>'' then convert(varchar(20),cast(PC.Specification4 as numeric(7,5)))
			                            end) ,   
			[AssociatedCrew] = ACrew.AssociatedCrew, 
			[TenToMin]=@TenToMin,
			[crewid]=c.crewid,                     
			[Crew]=c.crewcd+' '+C.LastName+','+C.FirstName+' '+ISNULL(c.MiddleInitial,''),  
			--[DUTY] =PC.Dutytype,  
			[maincrew]=c.crewcd,  
			[chk]= (CASE WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off'  
					     WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=0) AND (@PrintOffTime=0)) THEN ''  
					     WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On'  
					     WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END),  
            [BLK]= (CASE WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=1) AND (@PrintOffTime=0)) THEN DT.DutyBegin  
                         WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=0) AND (@PrintOffTime=1)) THEN DT.DutyEnd  
                         WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=1) AND (@PrintOffTime=1)) THEN DT.DutyBegin+'/'+DT.DutyEnd
                         WHEN (PL.IsDutyEnd=1) AND ((@PrintOnTime=0) AND (@PrintOffTime=0)) THEN '' END)
 			 FROM PostflightLeg PL   
			 	INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0 
				INNER JOIN  PostflightCrew PC ON PC.POLegID = PL.POLegID  
				INNER JOIN Crew C ON C.CrewID = PC.CrewID 
				INNER JOIN (SELECT DISTINCT PC1.POLegID, AssociatedCrew = (SELECT RTRIM(C.CrewCD) + ',' FROM PostflightCrew PC INNER JOIN Crew C ON PC.CrewID = C.CrewID WHERE PC.POLegID = PC1.POLegID FOR XML PATH(''))
				FROM PostflightCrew PC1) ACrew ON PL.POLegID = ACrew.POLegID    
				INNER JOIN  Fleet F ON F.FleetID = PM.FleetID      
				LEFT OUTER JOIN  Company CM ON CM.HomebaseID = PM.HomebaseID  
				inner JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  
				LEFT OUTER JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID  
				LEFT OUTER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD
				inner JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID   
				inner JOIN Airport AD ON AD.AirportID = PL.DepartICAOID  
				LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=PM.POLogID AND DT.LegID=PL.POLegID
			    WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
			   AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
			    AND PC.Dutytype NOT IN ('P','S')  
			     --and c.IsDeleted = 0 AND c.IsStatus = 1 
	         AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
	         AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TypeCode, ',')) OR @TypeCode = '')  
	         AND PL.IsDeleted = 0
	order by maincrew
INSERT INTO #temp1		 
SELECT distinct
	 [DateFROM]=CONVERT(DATE,@DATEFROM),[DATETO]= CONVERT(DATE, @DATETO),   
		--[DateRange]=CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101),   
		[shDate]=NULL, 
		[TailNumber]='', 
		[aType]='NO ACTIVIT',  
		[FltTyp]='', 
		[DepICAO]='', 
		[ArrICAO]='',  
		[BlkOut]=NULL, 
		[BlkIn]=NULL,  
		[DutyHours]=NULL, 
		[bduty]=null, 
		[BlockTime]=NULL,	
		[FlightTime]=NULL,  
		[specification3]=null,
		[specification4]=null, 
		[[CUSTOM3DESCRIPTION]= (case when  @Spec3=0 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.')
			                            when @Spec3=1 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.0')
			                            when @Spec3=2 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.00')
			                            when @Spec3=3 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.000')
			                            when @Spec3=4 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.0000')
			                             when @Spec3=5 and @SpecLong3 is not null and @SpecLong3<>'' then CONVERT(VARCHAR(20),'0.00000')
			                            end )  ,  
			[CUSTOM4DESCRIPTION]= (case when  @Spec4=0 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.')
			                            when @Spec4=1 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.0')
			                            when @Spec4=2 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.00')
			                            when @Spec4=3 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.000')
			                            when @Spec4=4 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.0000')
			                            when @Spec4=5 and @SpecLong4 is not null and @SpecLong4<>'' then CONVERT(VARCHAR(20),'0.00000')
			                            end) , 
		[AssociatedCrew]='',  
		[TenToMin]=@TenToMin,
		[crewid]=c.crewid,                 
		[Crew]=c.crewcd+' '+C.LastName+','+C.FirstName+' '+ISNULL(c.MiddleInitial,''),  
		--[DUTY]=PC.Dutytype,
		[maincrew]=c.crewcd, 
		[chk]=(CASE WHEN ((@PrintOnTime=1) AND (@PrintOffTime=1)) THEN 'On/Off'  
					     WHEN ((@PrintOnTime=0) AND (@PrintOffTime=0)) THEN ''  
					     WHEN ((@PrintOnTime=1) AND (@PrintOffTime=0)) THEN 'On'  
					     WHEN ((@PrintOnTime=0) AND (@PrintOffTime=1)) THEN 'Off' END),  	
		[BLK]= NULL
		FROM Crew C
		--LEFT OUTER JOIN  PostflightCrew PC ON PC.CrewID = C.CrewID 
          WHERE C.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
          --and c.IsDeleted = 0 AND c.IsStatus = 1 
          AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
         
order by maincrew          
          delete from #temp1 where maincrew in(select distinct maincrew from #temp)
           
           
SELECT * FROM #temp1

union all           
select * from #temp 
 ORDER BY maincrew         

  -- EXEC spGetReportPOSTSubNonPilotLogInformation 'JWILLIAMS_11','2009/1/1','2009/12/12','',1,0
--select * from Crew where CrewID=1001151534 and CustomerID='10011'


GO


