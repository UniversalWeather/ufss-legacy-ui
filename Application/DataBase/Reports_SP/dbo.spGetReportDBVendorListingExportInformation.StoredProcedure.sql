IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBVendorListingExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBVendorListingExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE  [dbo].[spGetReportDBVendorListingExportInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30) --Mandatory
)
AS
 
-- ===============================================================================
-- SPC Name: [spGetReportDBVendorListingExportInformation]
-- Author: Askar
-- Create date: 31 December
-- Description: Get Vendor Listing
-- Revision History
-- Date        Name        Ver         Change
-- EXEC  spGetReportDBVendorListingExportInformation 'SUPERVISOR_99','10099'
-- ================================================================================
SELECT name= V.Name
	  ,vendtype=V.VendorType
	  ,vendcode = V.VendorCD
	  ,homebase=A.IcaoID
	  ,icao_id= CA.IcaoID
	  ,vcname = CASE WHEN ISNULL(VC.LastName,'')<>'' THEN VC.LastName +', ' ELSE '' END+ISNULL(VC.FirstName,'')+' '+ISNULL(LEFT(VC.MiddleName,1),'') ---V.VendorContactName
	  ,active= CASE WHEN V.IsInActive=1 THEN 'Y' ELSE 'N' END


FROM  Vendor V
LEFT OUTER JOIN (SELECT * FROM VendorContact WHERE IsChoice=1) VC  ON V.VendorID=VC.VendorID
LEFT OUTER JOIN Airport A ON V.HomebaseID=A.AirportID
LEFT OUTER JOIN Airport CA ON CA.AirportID=V.AirportID
WHERE V.CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND V.CustomerID = CONVERT(BIGINT, @UserCustomerID)
 And V.IsDeleted=0
 AND V.VendorType='V'
 ORDER BY V.VendorID

--EXEC spGetReportDBVendorListingExportInformation 'SUPERVISOR_99','10099'




GO


