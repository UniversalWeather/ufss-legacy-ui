IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportExportInformation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetReportAirportExportInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =========================================================================
-- SPC Name: spGetReportAirportExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get Airport Information for export report
-- Revision History
-- Date			Name		Ver		Change

-- =========================================================================

SET NOCOUNT ON

		SELECT
			[icao_id] = A.IcaoID
			,[city] = A.CityName
			,[state] = A.StateName
			,[ctrydesc] = A.CountryName
			,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
			,[airport_nm] = A.AirportName
			,[lat_deg] = A.LatitudeDegree
			,[lat_min] = A.LatitudeMinutes
			,[lat_ns] = A.LatitudeNorthSouth
			,[long_deg] = A.LongitudeDegrees
			,[long_min] = A.LongitudeMinutes
			,[long_ew] = A.LongitudeEastWest
			,[mag_var] = A.MagneticVariance
			,[var_ew] = A.VarianceEastWest
			,[elevation] = A.Elevation
			,[long_rnway] = A.LongestRunway
			,[entry_port] = A.IsEntryPort
			,[wind_zone] = A.WindZone
			,[us_taxable] = A.IsUSTax
			,[rural] = A.IsRural
			,[aportflag] = A.IsAirport
			,[heliflag] = A.IsHeliport
			,[airportinfo] = A.AirportInfo --''
			,[gmt_offset] = A.OffsetToGMT
			,[dst_yn] = A.IsDayLightSaving
			,[dstst_dt] = A.DayLightSavingStartDT
			,[dstst_tm] = A.DaylLightSavingStartTM
			,[dstfn_dt] = A.DayLightSavingEndDT
			,[dstfn_tm] = A.DayLightSavingEndTM
			,[tower_freq] = A.TowerFreq
			,[atis_freq] = A.ATISFreq
			,[arinc_freq] = A.ARINCFreq
			,[grnd_freq] = A.GroundFreq
			,[aprch_freq] = A.ApproachFreq
			,[dpart_freq] = A.DepartFreq
			,[freq_lbl_1] = A.Label1Freq
			,[freq_1] = A.Freq1
			,[freq_lbl_2] = A.Label2Freq
			,[freq_2] = A.Freq2
			,[tkoff_bias] = A.TakeoffBIAS
			,[landg_bias] = A.LandingBIAS
			,[Alerts]=''
			,[GeneralNotes]=''
			,[fpkflag] = A.IsFlightPakFlag
			,[fbocnt] = A.FBOCnt
			,[hotelcnt] = A.HotelCnt
			,[transcnt] = A.TransportationCnt
			,[catercnt] = A.CateringCnt
			,[fpkupdt] = A.FlightPakUPD
			,[metro] = M.MetroCD
			,[dstregion] = DST.DSTRegionCD
			,[ppr] = A.PPR
			,[iata] = A.Iata
			,[altdest1] = A.AlternateDestination1
			,[altdest2] = A.AlternateDestination2
			,[altdest3] = A.AlternateDestination3
			,[lastuser] = A.LastUpdUID
			,[lastupdt] = A.LastUpdTS
			,[inactive] = A.IsInActive
			,[rec_type] = A.RecordType
			,[new_icao] = A.NewICAO
			,[prev_icao] = A.PreviousICAO
			,[updt_date] = A.UpdateDT
			,[worldclock] = A.IsWorldClock
			,[wide_rnway] = A.WidthRunway
			,[fssphone] = A.FssPhone
			,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
			,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
			,[cloc] = A.CustomLocation
			,[cphone] = A.CustomPhoneNum
			,[immphone] = A.ImmigrationPhoneNum
			,[agphone] = A.AgPhoneNum
			,[cavail] = A.IsCustomAvailable
			,[aoe] = A.PortEntryType
			,[f_unicom] = A.Unicom
			,[f_clrdel1] = A.Clearence1DEL
			,[f_clrdel2] = A.Clearence2DEL
			,[f_atisphn] = A.AtisPhoneNum
			,[f_asos] = A.ASOS
			,[f_asosphn] = A.AsosPhoneNum
			,[f_awos] = A.AWOS
			,[f_awosphn] = A.AwosPhoneNum
			,[f_awostyp] = A.AwosType
			,[slots] = A.IsSlots
			,[fssname] = A.FssName
			,[apmgr] = A.AirportManager
			,[apmgrphn] = A.AirportManagerPhoneNum
			,[faa] = A.FAA
			,[uvid] = A.UWAID
			,[fix] = A.IsFIX
			,[ophrs] = A.HoursOfOperation
			,[slandfee] = A.LandingFeeSmall
			,[mlandfee] = A.LandingFeeMedium
			,[llandfee] = A.LandingFeeLarge
			,[euets] = A.IsEUETS
			,[fbo_code] = ''
			,[fbo_inactive] = 0
			,[fbo_choice] = 0
			,[fbo_name] = ''
			,[fbo_phone] = ''
			,[fbo_freq] = ''
			,[fbo_uvair] = 0
			,[fbo_fuel_brand] = ''
			,[fbo_crew_car] = 0
			,[hotel_code] = ''
			,[hotel_crew] = 0
			,[hotel_pax] = 0
			,[hotel_name] = ''
			,[hotel_phone] = ''
			,[hotel_rating] = ''
			,[hotel_miles_from] = 0
			,[trans_code] = ''
			,[trans_choice] = 0
			,[trans_name] = ''
			,[trans_phone] = ''
			,[cater_code] = ''
			,[cater_choice] = 0
			,[cater_name] = ''
			,[cater_phone] = ''
			,[section] = 0
			,[isdeleted] = A.IsDeleted
		FROM Airport A
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID
		WHERE A.IsDeleted = 0 
			AND A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
			AND A.IcaoID = RTRIM(@IcaoID)
		
	UNION ALL
	
		SELECT
			[icao_id] = A.IcaoID
			,[city] = A.CityName
			,[state] = A.StateName
			,[ctrydesc] = A.CountryName
			,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
			,[airport_nm] = A.AirportName
			,[lat_deg] = A.LatitudeDegree
			,[lat_min] = A.LatitudeMinutes
			,[lat_ns] = A.LatitudeNorthSouth
			,[long_deg] = A.LongitudeDegrees
			,[long_min] = A.LongitudeMinutes
			,[long_ew] = A.LongitudeEastWest
			,[mag_var] = A.MagneticVariance
			,[var_ew] = A.VarianceEastWest
			,[elevation] = A.Elevation
			,[long_rnway] = A.LongestRunway
			,[entry_port] = A.IsEntryPort
			,[wind_zone] = A.WindZone
			,[us_taxable] = A.IsUSTax
			,[rural] = A.IsRural
			,[aportflag] = A.IsAirport
			,[heliflag] = A.IsHeliport
			,[airportinfo] = A.AirportInfo
			,[gmt_offset] = A.OffsetToGMT
			,[dst_yn] = A.IsDayLightSaving
			,[dstst_dt] = A.DayLightSavingStartDT
			,[dstst_tm] = A.DaylLightSavingStartTM
			,[dstfn_dt] = A.DayLightSavingEndDT
			,[dstfn_tm] = A.DayLightSavingEndTM
			,[tower_freq] = A.TowerFreq
			,[atis_freq] = A.ATISFreq
			,[arinc_freq] = A.ARINCFreq
			,[grnd_freq] = A.GroundFreq
			,[aprch_freq] = A.ApproachFreq
			,[dpart_freq] = A.DepartFreq
			,[freq_lbl_1] = A.Label1Freq
			,[freq_1] = A.Freq1
			,[freq_lbl_2] = A.Label2Freq
			,[freq_2] = A.Freq2
			,[tkoff_bias] = A.TakeoffBIAS
			,[landg_bias] = A.LandingBIAS
			,[Alerts]=''
			,[GeneralNotes]=''
			,[fpkflag] = A.IsFlightPakFlag
			,[fbocnt] = A.FBOCnt
			,[hotelcnt] = A.HotelCnt
			,[transcnt] = A.TransportationCnt
			,[catercnt] = A.CateringCnt
			,[fpkupdt] = A.FlightPakUPD
			,[metro] = M.MetroCD
			,[dstregion] = DST.DSTRegionCD
			,[ppr] = A.PPR
			,[iata] = A.Iata
			,[altdest1] = A.AlternateDestination1
			,[altdest2] = A.AlternateDestination2
			,[altdest3] = A.AlternateDestination3
			,[lastuser] = A.LastUpdUID
			,[lastupdt] = A.LastUpdTS
			,[inactive] = A.IsInActive
			,[rec_type] = A.RecordType
			,[new_icao] = A.NewICAO
			,[prev_icao] = A.PreviousICAO
			,[updt_date] = A.UpdateDT
			,[worldclock] = A.IsWorldClock
			,[wide_rnway] = A.WidthRunway
			,[fssphone] = A.FssPhone
			,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
			,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
			,[cloc] = A.CustomLocation
			,[cphone] = A.CustomPhoneNum
			,[immphone] = A.ImmigrationPhoneNum
			,[agphone] = A.AgPhoneNum
			,[cavail] = A.IsCustomAvailable
			,[aoe] = A.PortEntryType
			,[f_unicom] = A.Unicom
			,[f_clrdel1] = A.Clearence1DEL
			,[f_clrdel2] = A.Clearence2DEL
			,[f_atisphn] = A.AtisPhoneNum
			,[f_asos] = A.ASOS
			,[f_asosphn] = A.AsosPhoneNum
			,[f_awos] = A.AWOS
			,[f_awosphn] = A.AwosPhoneNum
			,[f_awostyp] = A.AwosType
			,[slots] = A.IsSlots
			,[fssname] = A.FssName
			,[apmgr] = A.AirportManager
			,[apmgrphn] = A.AirportManagerPhoneNum
			,[faa] = A.FAA
			,[uvid] = A.UWAID
			,[fix] = A.IsFIX
			,[ophrs] = A.HoursOfOperation
			,[slandfee] = A.LandingFeeSmall
			,[mlandfee] = A.LandingFeeMedium
			,[llandfee] = A.LandingFeeLarge
			,[euets] = A.IsEUETS
			,[fbo_code] = F.FBOCD
			,[fbo_inactive] = F.IsInActive
			,[fbo_choice] = F.IsChoice
			,[fbo_name] = F.FBOVendor
			,[fbo_phone] = F.PhoneNUM1
			,[fbo_freq] = F.Frequency
			,[fbo_uvair] = F.IsUWAAirPartner
			,[fbo_fuel_brand] = F.FuelBrand
			,[fbo_crew_car] = F.IsCrewCar
			,[hotel_code] = ''
			,[hotel_crew] = 0
			,[hotel_pax] = 0
			,[hotel_name] = ''
			,[hotel_phone] = ''
			,[hotel_rating] = ''
			,[hotel_miles_from] = 0
			,[trans_code] = ''
			,[trans_choice] = 0
			,[trans_name] = ''
			,[trans_phone] = ''
			,[cater_code] = ''
			,[cater_choice] = 0
			,[cater_name] = ''
			,[cater_phone] = ''
			,[section] = 1
			,[isdeleted] = F.IsDeleted			
			--Section Field : Pending for data mapping
			--A.Alerts, A.GeneralNotes
		FROM Airport A 
			INNER JOIN FBO F 
				ON A.AirportID = F.AirportID	
				--AND A.CustomerID = F.CustomerID
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID
		WHERE F.IsDeleted = 0
			AND A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
			AND A.IcaoID = RTRIM(@IcaoID)
			
	UNION ALL
	
		SELECT
		[icao_id] = A.IcaoID
		,[city] = A.CityName
		,[state] = A.StateName
		,[ctrydesc] = A.CountryName
		,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
		,[airport_nm] = A.AirportName
		,[lat_deg] = A.LatitudeDegree
		,[lat_min] = A.LatitudeMinutes
		,[lat_ns] = A.LatitudeNorthSouth
		,[long_deg] = A.LongitudeDegrees
		,[long_min] = A.LongitudeMinutes
		,[long_ew] = A.LongitudeEastWest
		,[mag_var] = A.MagneticVariance
		,[var_ew] = A.VarianceEastWest
		,[elevation] = A.Elevation
		,[long_rnway] = A.LongestRunway
		,[entry_port] = A.IsEntryPort
		,[wind_zone] = A.WindZone
		,[us_taxable] = A.IsUSTax
		,[rural] = A.IsRural
		,[aportflag] = A.IsAirport
		,[heliflag] = A.IsHeliport
		,[airportinfo] = A.AirportInfo
		,[gmt_offset] = A.OffsetToGMT
		,[dst_yn] = A.IsDayLightSaving
		,[dstst_dt] = A.DayLightSavingStartDT
		,[dstst_tm] = A.DaylLightSavingStartTM
		,[dstfn_dt] = A.DayLightSavingEndDT
		,[dstfn_tm] = A.DayLightSavingEndTM
		,[tower_freq] = A.TowerFreq
		,[atis_freq] = A.ATISFreq
		,[arinc_freq] = A.ARINCFreq
		,[grnd_freq] = A.GroundFreq
		,[aprch_freq] = A.ApproachFreq
		,[dpart_freq] = A.DepartFreq
		,[freq_lbl_1] = A.Label1Freq
		,[freq_1] = A.Freq1
		,[freq_lbl_2] = A.Label2Freq
		,[freq_2] = A.Freq2
		,[tkoff_bias] = A.TakeoffBIAS
		,[landg_bias] = A.LandingBIAS
		,[Alerts]=''
	    ,[GeneralNotes]=''
		,[fpkflag] = A.IsFlightPakFlag
		,[fbocnt] = A.FBOCnt
		,[hotelcnt] = A.HotelCnt
		,[transcnt] = A.TransportationCnt
		,[catercnt] = A.CateringCnt
		,[fpkupdt] = A.FlightPakUPD
		,[metro] = M.MetroCD
		,[dstregion] = DST.DSTRegionCD
		,[ppr] = A.PPR
		,[iata] = A.Iata
		,[altdest1] = A.AlternateDestination1
		,[altdest2] = A.AlternateDestination2
		,[altdest3] = A.AlternateDestination3
		,[lastuser] = A.LastUpdUID
		,[lastupdt] = A.LastUpdTS
		,[inactive] = A.IsInActive
		,[rec_type] = A.RecordType
		,[new_icao] = A.NewICAO
		,[prev_icao] = A.PreviousICAO
		,[updt_date] = A.UpdateDT
		,[worldclock] = A.IsWorldClock
		,[wide_rnway] = A.WidthRunway
		,[fssphone] = A.FssPhone
		,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
		,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
		,[cloc] = A.CustomLocation
		,[cphone] = A.CustomPhoneNum
		,[immphone] = A.ImmigrationPhoneNum
		,[agphone] = A.AgPhoneNum
		,[cavail] = A.IsCustomAvailable
		,[aoe] = A.PortEntryType
		,[f_unicom] = A.Unicom
		,[f_clrdel1] = A.Clearence1DEL
		,[f_clrdel2] = A.Clearence2DEL
		,[f_atisphn] = A.AtisPhoneNum
		,[f_asos] = A.ASOS
		,[f_asosphn] = A.AsosPhoneNum
		,[f_awos] = A.AWOS
		,[f_awosphn] = A.AwosPhoneNum
		,[f_awostyp] = A.AwosType
		,[slots] = A.IsSlots
		,[fssname] = A.FssName
		,[apmgr] = A.AirportManager
		,[apmgrphn] = A.AirportManagerPhoneNum
		,[faa] = A.FAA
		,[uvid] = A.UWAID
		,[fix] = A.IsFIX
		,[ophrs] = A.HoursOfOperation
		,[slandfee] = A.LandingFeeSmall
		,[mlandfee] = A.LandingFeeMedium
		,[llandfee] = A.LandingFeeLarge
		,[euets] = A.IsEUETS
		,[fbo_code] = ''
		,[fbo_inactive] = 0
		,[fbo_choice] = 0
		,[fbo_name] = ''
		,[fbo_phone] = ''
		,[fbo_freq] = ''
		,[fbo_uvair] = 0
		,[fbo_fuel_brand] = ''
		,[fbo_crew_car] = 0
		,[hotel_code] = H.HotelCD
		,[hotel_crew] = H.IsCrew
		,[hotel_pax] = H.IsPassenger
		,[hotel_name] = H.Name
		,[hotel_phone] = H.PhoneNum
		,[hotel_rating] = H.HotelRating
		,[hotel_miles_from] = CONVERT(INT,ROUND(H.MilesFromICAO,0))
		,[trans_code] = ''
		,[trans_choice] = 0
		,[trans_name] = ''
		,[trans_phone] = ''
		,[cater_code] = ''
		,[cater_choice] = 0
		,[cater_name] = ''
		,[cater_phone] = ''
		,[section] = 2
		,[isdeleted] = H.IsDeleted		
		FROM Airport A 
			INNER JOIN Hotel H
				ON A.AirportID = H.AirportID
				--AND A.CustomerID = H.CustomerID				
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID				
		WHERE H.IsDeleted = 0 AND
			A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND A.IcaoID = RTRIM(@IcaoID)
	
	UNION ALL
	
		SELECT
			[icao_id] = A.IcaoID
			,[city] = A.CityName
			,[state] = A.StateName
			,[ctrydesc] = A.CountryName
			,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
			,[airport_nm] = A.AirportName
			,[lat_deg] = A.LatitudeDegree
			,[lat_min] = A.LatitudeMinutes
			,[lat_ns] = A.LatitudeNorthSouth
			,[long_deg] = A.LongitudeDegrees
			,[long_min] = A.LongitudeMinutes
			,[long_ew] = A.LongitudeEastWest
			,[mag_var] = A.MagneticVariance
			,[var_ew] = A.VarianceEastWest
			,[elevation] = A.Elevation
			,[long_rnway] = A.LongestRunway
			,[entry_port] = A.IsEntryPort
			,[wind_zone] = A.WindZone
			,[us_taxable] = A.IsUSTax
			,[rural] = A.IsRural
			,[aportflag] = A.IsAirport
			,[heliflag] = A.IsHeliport
			,[airportinfo] = A.AirportInfo
			,[gmt_offset] = A.OffsetToGMT
			,[dst_yn] = A.IsDayLightSaving
			,[dstst_dt] = A.DayLightSavingStartDT
			,[dstst_tm] = A.DaylLightSavingStartTM
			,[dstfn_dt] = A.DayLightSavingEndDT
			,[dstfn_tm] = A.DayLightSavingEndTM
			,[tower_freq] = A.TowerFreq
			,[atis_freq] = A.ATISFreq
			,[arinc_freq] = A.ARINCFreq
			,[grnd_freq] = A.GroundFreq
			,[aprch_freq] = A.ApproachFreq
			,[dpart_freq] = A.DepartFreq
			,[freq_lbl_1] = A.Label1Freq
			,[freq_1] = A.Freq1
			,[freq_lbl_2] = A.Label2Freq
			,[freq_2] = A.Freq2
			,[tkoff_bias] = A.TakeoffBIAS
			,[landg_bias] = A.LandingBIAS
			,[Alerts]=''
			,[GeneralNotes]=''
			,[fpkflag] = A.IsFlightPakFlag
			,[fbocnt] = A.FBOCnt
			,[hotelcnt] = A.HotelCnt
			,[transcnt] = A.TransportationCnt
			,[catercnt] = A.CateringCnt
			,[fpkupdt] = A.FlightPakUPD
			,[metro] = M.MetroCD
			,[dstregion] = DST.DSTRegionCD
			,[ppr] = A.PPR
			,[iata] = A.Iata
			,[altdest1] = A.AlternateDestination1
			,[altdest2] = A.AlternateDestination2
			,[altdest3] = A.AlternateDestination3
			,[lastuser] = A.LastUpdUID
			,[lastupdt] = A.LastUpdTS
			,[inactive] = A.IsInActive
			,[rec_type] = A.RecordType
			,[new_icao] = A.NewICAO
			,[prev_icao] = A.PreviousICAO
			,[updt_date] = A.UpdateDT
			,[worldclock] = A.IsWorldClock
			,[wide_rnway] = A.WidthRunway
			,[fssphone] = A.FssPhone
			,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
			,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
			,[cloc] = A.CustomLocation
			,[cphone] = A.CustomPhoneNum
			,[immphone] = A.ImmigrationPhoneNum
			,[agphone] = A.AgPhoneNum
			,[cavail] = A.IsCustomAvailable
			,[aoe] = A.PortEntryType
			,[f_unicom] = A.Unicom
			,[f_clrdel1] = A.Clearence1DEL
			,[f_clrdel2] = A.Clearence2DEL
			,[f_atisphn] = A.AtisPhoneNum
			,[f_asos] = A.ASOS
			,[f_asosphn] = A.AsosPhoneNum
			,[f_awos] = A.AWOS
			,[f_awosphn] = A.AwosPhoneNum
			,[f_awostyp] = A.AwosType
			,[slots] = A.IsSlots
			,[fssname] = A.FssName
			,[apmgr] = A.AirportManager
			,[apmgrphn] = A.AirportManagerPhoneNum
			,[faa] = A.FAA
			,[uvid] = A.UWAID
			,[fix] = A.IsFIX
			,[ophrs] = A.HoursOfOperation
			,[slandfee] = A.LandingFeeSmall
			,[mlandfee] = A.LandingFeeMedium
			,[llandfee] = A.LandingFeeLarge
			,[euets] = A.IsEUETS
			,[fbo_code] = ''
			,[fbo_inactive] = 0
			,[fbo_choice] = 0
			,[fbo_name] = ''
			,[fbo_phone] = ''
			,[fbo_freq] = ''
			,[fbo_uvair] = 0
			,[fbo_fuel_brand] = ''
			,[fbo_crew_car] = 0
			,[hotel_code] = ''
			,[hotel_crew] = 0
			,[hotel_pax] = 0
			,[hotel_name] = ''
			,[hotel_phone] = ''
			,[hotel_rating] = ''
			,[hotel_miles_from] = 0
			,[trans_code] = T.TransportCD
			,[trans_choice] = T.IsChoice
			,[trans_name] = T.TransportationVendor
			,[trans_phone] = T.PhoneNum
			,[cater_code] = ''
			,[cater_choice] = 0
			,[cater_name] = ''
			,[cater_phone] = ''
			,[section] = 3
			,[isdeleted] = T.IsDeleted			
		FROM Airport A 
			INNER JOIN Transport T
				ON A.AirportID = T.AirportID
				--AND A.CustomerID = T.CustomerID
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID				
		WHERE T.IsDeleted = 0 AND
			A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND A.IcaoID = RTRIM(@IcaoID)
	
	UNION ALL
	
		SELECT
			[icao_id] = A.IcaoID
			,[city] = A.CityName
			,[state] = A.StateName
			,[ctrydesc] = A.CountryName
			,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
			,[airport_nm] = A.AirportName
			,[lat_deg] = A.LatitudeDegree
			,[lat_min] = A.LatitudeMinutes
			,[lat_ns] = A.LatitudeNorthSouth
			,[long_deg] = A.LongitudeDegrees
			,[long_min] = A.LongitudeMinutes
			,[long_ew] = A.LongitudeEastWest
			,[mag_var] = A.MagneticVariance
			,[var_ew] = A.VarianceEastWest
			,[elevation] = A.Elevation
			,[long_rnway] = A.LongestRunway
			,[entry_port] = A.IsEntryPort
			,[wind_zone] = A.WindZone
			,[us_taxable] = A.IsUSTax
			,[rural] = A.IsRural
			,[aportflag] = A.IsAirport
			,[heliflag] = A.IsHeliport
			,[airportinfo] = A.AirportInfo
			,[gmt_offset] = A.OffsetToGMT
			,[dst_yn] = A.IsDayLightSaving
			,[dstst_dt] = A.DayLightSavingStartDT
			,[dstst_tm] = A.DaylLightSavingStartTM
			,[dstfn_dt] = A.DayLightSavingEndDT
			,[dstfn_tm] = A.DayLightSavingEndTM
			,[tower_freq] = A.TowerFreq
			,[atis_freq] = A.ATISFreq
			,[arinc_freq] = A.ARINCFreq
			,[grnd_freq] = A.GroundFreq
			,[aprch_freq] = A.ApproachFreq
			,[dpart_freq] = A.DepartFreq
			,[freq_lbl_1] = A.Label1Freq
			,[freq_1] = A.Freq1
			,[freq_lbl_2] = A.Label2Freq
			,[freq_2] = A.Freq2
			,[tkoff_bias] = A.TakeoffBIAS
			,[landg_bias] = A.LandingBIAS
			,[Alerts]=''
			,[GeneralNotes]=''
			,[fpkflag] = A.IsFlightPakFlag
			,[fbocnt] = A.FBOCnt
			,[hotelcnt] = A.HotelCnt
			,[transcnt] = A.TransportationCnt
			,[catercnt] = A.CateringCnt
			,[fpkupdt] = A.FlightPakUPD
			,[metro] = M.MetroCD
			,[dstregion] = DST.DSTRegionCD
			,[ppr] = A.PPR
			,[iata] = A.Iata
			,[altdest1] = A.AlternateDestination1
			,[altdest2] = A.AlternateDestination2
			,[altdest3] = A.AlternateDestination3
			,[lastuser] = A.LastUpdUID
			,[lastupdt] = A.LastUpdTS
			,[inactive] = A.IsInActive
			,[rec_type] = A.RecordType
			,[new_icao] = A.NewICAO
			,[prev_icao] = A.PreviousICAO
			,[updt_date] = A.UpdateDT
			,[worldclock] = A.IsWorldClock
			,[wide_rnway] = A.WidthRunway
			,[fssphone] = A.FssPhone
			,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
			,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
			,[cloc] = A.CustomLocation
			,[cphone] = A.CustomPhoneNum
			,[immphone] = A.ImmigrationPhoneNum
			,[agphone] = A.AgPhoneNum
			,[cavail] = A.IsCustomAvailable
			,[aoe] = A.PortEntryType
			,[f_unicom] = A.Unicom
			,[f_clrdel1] = A.Clearence1DEL
			,[f_clrdel2] = A.Clearence2DEL
			,[f_atisphn] = A.AtisPhoneNum
			,[f_asos] = A.ASOS
			,[f_asosphn] = A.AsosPhoneNum
			,[f_awos] = A.AWOS
			,[f_awosphn] = A.AwosPhoneNum
			,[f_awostyp] = A.AwosType
			,[slots] = A.IsSlots
			,[fssname] = A.FssName
			,[apmgr] = A.AirportManager
			,[apmgrphn] = A.AirportManagerPhoneNum
			,[faa] = A.FAA
			,[uvid] = A.UWAID
			,[fix] = A.IsFIX
			,[ophrs] = A.HoursOfOperation
			,[slandfee] = A.LandingFeeSmall
			,[mlandfee] = A.LandingFeeMedium
			,[llandfee] = A.LandingFeeLarge
			,[euets] = A.IsEUETS
			,[fbo_code] = ''
			,[fbo_inactive] = 0
			,[fbo_choice] = 0
			,[fbo_name] = ''
			,[fbo_phone] = ''
			,[fbo_freq] = ''
			,[fbo_uvair] = 0
			,[fbo_fuel_brand] = ''
			,[fbo_crew_car] = 0
			,[hotel_code] = ''
			,[hotel_crew] = 0
			,[hotel_pax] = 0
			,[hotel_name] = ''
			,[hotel_phone] = ''
			,[hotel_rating] = ''
			,[hotel_miles_from] = 0
			,[trans_code] = ''
			,[trans_choice] = 0
			,[trans_name] = ''
			,[trans_phone] = ''
			,[cater_code] = C.CateringCD
			,[cater_choice] = C.IsChoice
			,[cater_name] = C.CateringVendor
			,[cater_phone] = C.PhoneNum
			,[section] = 4
			,[isdeleted] = C.IsDeleted			
		FROM Airport A 
			INNER JOIN Catering C
				ON A.AirportID = C.AirportID
				--AND A.CustomerID = C.CustomerID				
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID					
		WHERE C.IsDeleted = 0 AND
			A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND A.IcaoID = RTRIM(@IcaoID)
	
	UNION ALL
	
		SELECT
			[icao_id] = A.IcaoID
			,[city] = A.CityName
			,[state] = A.StateName
			,[ctrydesc] = A.CountryName
			,[country] = dbo.GetCountryCDByCountryID(A.CountryID)
			,[airport_nm] = A.AirportName
			,[lat_deg] = A.LatitudeDegree
			,[lat_min] = A.LatitudeMinutes
			,[lat_ns] = A.LatitudeNorthSouth
			,[long_deg] = A.LongitudeDegrees
			,[long_min] = A.LongitudeMinutes
			,[long_ew] = A.LongitudeEastWest
			,[mag_var] = A.MagneticVariance
			,[var_ew] = A.VarianceEastWest
			,[elevation] = A.Elevation
			,[long_rnway] = A.LongestRunway
			,[entry_port] = A.IsEntryPort
			,[wind_zone] = A.WindZone
			,[us_taxable] = A.IsUSTax
			,[rural] = A.IsRural
			,[aportflag] = A.IsAirport
			,[heliflag] = A.IsHeliport
			,[airportinfo] = A.AirportInfo
			,[gmt_offset] = A.OffsetToGMT
			,[dst_yn] = A.IsDayLightSaving
			,[dstst_dt] = A.DayLightSavingStartDT
			,[dstst_tm] = A.DaylLightSavingStartTM
			,[dstfn_dt] = A.DayLightSavingEndDT
			,[dstfn_tm] = A.DayLightSavingEndTM
			,[tower_freq] = A.TowerFreq
			,[atis_freq] = A.ATISFreq
			,[arinc_freq] = A.ARINCFreq
			,[grnd_freq] = A.GroundFreq
			,[aprch_freq] = A.ApproachFreq
			,[dpart_freq] = A.DepartFreq
			,[freq_lbl_1] = A.Label1Freq
			,[freq_1] = A.Freq1
			,[freq_lbl_2] = A.Label2Freq
			,[freq_2] = A.Freq2
			,[tkoff_bias] = A.TakeoffBIAS
			,[landg_bias] = A.LandingBIAS
			,[Alerts]=''
			,[GeneralNotes]=''
			,[fpkflag] = A.IsFlightPakFlag
			,[fbocnt] = A.FBOCnt
			,[hotelcnt] = A.HotelCnt
			,[transcnt] = A.TransportationCnt
			,[catercnt] = A.CateringCnt
			,[fpkupdt] = A.FlightPakUPD
			,[metro] = M.MetroCD
			,[dstregion] = DST.DSTRegionCD
			,[ppr] = A.PPR
			,[iata] = A.Iata
			,[altdest1] = A.AlternateDestination1
			,[altdest2] = A.AlternateDestination2
			,[altdest3] = A.AlternateDestination3
			,[lastuser] = A.LastUpdUID
			,[lastupdt] = A.LastUpdTS
			,[inactive] = A.IsInActive
			,[rec_type] = A.RecordType
			,[new_icao] = A.NewICAO
			,[prev_icao] = A.PreviousICAO
			,[updt_date] = A.UpdateDT
			,[worldclock] = A.IsWorldClock
			,[wide_rnway] = A.WidthRunway
			,[fssphone] = A.FssPhone
			,[lwidth] = A.WidthLengthRunway --A.LengthWidthRunway
			,[wlength] = A.LengthWidthRunway --A.WidthLengthRunway
			,[cloc] = A.CustomLocation
			,[cphone] = A.CustomPhoneNum
			,[immphone] = A.ImmigrationPhoneNum
			,[agphone] = A.AgPhoneNum
			,[cavail] = A.IsCustomAvailable
			,[aoe] = A.PortEntryType
			,[f_unicom] = A.Unicom
			,[f_clrdel1] = A.Clearence1DEL
			,[f_clrdel2] = A.Clearence2DEL
			,[f_atisphn] = A.AtisPhoneNum
			,[f_asos] = A.ASOS
			,[f_asosphn] = A.AsosPhoneNum
			,[f_awos] = A.AWOS
			,[f_awosphn] = A.AwosPhoneNum
			,[f_awostyp] = A.AwosType
			,[slots] = A.IsSlots
			,[fssname] = A.FssName
			,[apmgr] = A.AirportManager
			,[apmgrphn] = A.AirportManagerPhoneNum
			,[faa] = A.FAA
			,[uvid] = A.UWAID
			,[fix] = A.IsFIX
			,[ophrs] = A.HoursOfOperation
			,[slandfee] = A.LandingFeeSmall
			,[mlandfee] = A.LandingFeeMedium
			,[llandfee] = A.LandingFeeLarge
			,[euets] = A.IsEUETS
			,[fbo_code] = ''
			,[fbo_inactive] = 0
			,[fbo_choice] = 0
			,[fbo_name] = ''
			,[fbo_phone] = ''
			,[fbo_freq] = ''
			,[fbo_uvair] = 0
			,[fbo_fuel_brand] = ''
			,[fbo_crew_car] = 0
			,[hotel_code] = ''
			,[hotel_crew] = 0
			,[hotel_pax] = 0
			,[hotel_name] = ''
			,[hotel_phone] = ''
			,[hotel_rating] = ''
			,[hotel_miles_from] = 0
			,[trans_code] = ''
			,[trans_choice] = 0
			,[trans_name] = ''
			,[trans_phone] = ''
			,[cater_code] = ''
			,[cater_choice] = 0
			,[cater_name] = ''
			,[cater_phone] = ''
			,[section] = 5
			,[isdeleted] = A.IsDeleted			
		FROM Airport A 
			LEFT OUTER JOIN (
				SELECT MetroID, MetroCD FROM Metro
				) M ON A.MetroID = M.MetroID
			LEFT OUTER JOIN (
				SELECT DSTRegionID, DSTRegionCD FROM DSTRegion
				) DST ON A.DSTRegionID = DST.DSTRegionID			
		WHERE A.IsDeleted = 0 AND
			A.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND A.IcaoID = RTRIM(@IcaoID)

		ORDER BY [icao_id], [section], [fbo_name], [hotel_name], [trans_name], [cater_name]   
-- EXEC spGetReportAirportExportInformation 'SUPERVISOR_99', 'KVNY'
GO
