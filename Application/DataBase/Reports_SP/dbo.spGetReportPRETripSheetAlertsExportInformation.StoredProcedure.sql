IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetAlertsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetAlertsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETripSheetAlertsExportInformation]
	    @UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TripNum AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS
-- ===============================================================================
-- SPC Name: spGetReportPRETripSheetAlertsExportInformation
-- Author: Abhishek S
-- Create date: 18 Jul 2012
-- Description: Get Trip Sheet Alerts Information in excel format
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
	SET NOCOUNT ON;
	
	  -----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
 
    


CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-------------------------------TailNum and Fleet Group Filteration----------------------	

SELECT DISTINCT
	  [tail_nmbr] = F.TailNum
	 ,[orig_nmbr] = PM.TripNUM
	 ,[desc] = PM.TripDescription
	 ,[Dept_Date]= CONVERT(DATE,PLDATE.DepartureDTTMLocal)
	 ,[Arr_Date]= CONVERT(DATE,PLDATE.ArrivalDTTMLocal)
     ,[notes]=PM.Notes   --Added for Mhtml
     ,[Trip_Dates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD)
	 FROM PreflightMain PM 
	 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0	 
	 INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
		FROM Fleet F 
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	 
	LEFT OUTER JOIN(SELECT MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,TripNUM,CustomerID,TripID FROM PreflightLeg WHERE IsDeleted = 0 GROUP BY TripNUM,CustomerID,TripID)PLDate ON PLDate.TripID=PL.TripID  AND PLDate.CustomerID=PL.CustomerID
	INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID 
	WHERE PM.IsDeleted = 0
		AND ISNULL(PM.Notes,'') <> ''
	    AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
		AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	    AND ( PM.TripNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '' ) 
        AND ( PM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomeBase = 0 ) 
		  
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'JWILLIAMS_11', '2012-01-01', '2012-12-09', '', '', '', 0
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'JWILLIAMS_11', '2012-01-01', '2012-12-09', '', 'N46E', '', 0
  -- EXEC spGetReportPRETripSheetAlertsExportInformation 'supervisor_99', '2013-01-09', '2013-01-09', '', '', '', 1

IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID 


GO


