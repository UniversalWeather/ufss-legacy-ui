
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewAlretsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewAlretsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPOSTCrewAlretsExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@AsOf AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@HomeBaseCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@ChecklistCode AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IncludeAllAlerts as bit
						
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTCrewAlretsExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Crew Alerts for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON
--drop table #CREWExp
--drop table #CREWExp1
DECLARE @TenToMin SMALLINT = 0;
	DECLARE @AppDTFormat VARCHAR(25)

SELECT @TenToMin = TimeDisplayTenMin,
	   @AppDTFormat=Upper(ApplicationDateFormat)
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @DATEFORMAT VARCHAR(20)= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN 
							'- -'
					WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN
							'. .'
					ELSE
							'/  /'
	          END

DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=1

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END

--select * from @TempCrewID
-----------------------------Crew and Crew Group Filteration---------------------- 

 	
CREATE TABLE #CREWExp1( asof date,crewcode VARCHAR(5),last_name VARCHAR(30),code CHAR(3),descr varchar(25),previous date,due date,type_code VARCHAR(10),freqmon int,basemon date,alert date,
			grace date,chkstatus varchar(25),passedduealert varchar(15),onetime varchar(15),completed varchar(15),totreqflthours NUMERIC(7,1),totflthours NUMERIC(7,3),IsDeleted bit,IsStatus bit,TenToMin numeric(1,0),DATFORMAT varchar(20))
CREATE TABLE #CREWExp( asof date,crewcode VARCHAR(5),last_name VARCHAR(30),code CHAR(3),descr varchar(25),previous date,due date,type_code VARCHAR(10),freqmon int,basemon date,alert date,
			grace date,chkstatus varchar(25),passedduealert varchar(15),onetime varchar(15),completed varchar(15),totreqflthours NUMERIC(7,1),totflthours NUMERIC(7,3),IsDeleted bit,IsStatus bit,TenToMin numeric(1,0),DATFORMAT varchar(20))			
 if(@IncludeAllAlerts=0)
 begin
INSERT INTO #CREWExp1
select asof,crewcode,last_name,code,descr,previous,due,type_code,freqmon,basemon,alert,grace,chkstatus,passedduealert,onetime
,completed,totreqflthours,sum(totflthours) totflthours,IsDeleted,IsStatus,TenToMin,DATFORMAT
from(
			SELECT distinct
			[asof]			= @Asof, 
			[crewcode]		= C.CrewCD,
			[last_name]		= C.LastName,
			[code]			= CCD.CheckListCD,
			[descr]			= CCL.CrewChecklistDescription,
	        [previous]		= CCD.PreviousCheckDT,
			[due]			= CCD.DueDT,
			[type_code]		= AC.AircraftCD,
			[freqmon]		= CCD.FrequencyMonth,
			[basemon]		= CCD.BaseMonthDT,
			[alert]			= CCD.AlertDT,
			[grace]			= CCD.GraceDT,
			[chkstatus]		= (CASE WHEN (CCD.IsOneTimeEvent='TRUE') And (CCD.IsCompleted='TRUE') THEN 'COMPLETED'
                              WHEN @Asof='' THEN ' '
							  WHEN (@Asof) < (CASE (CCD.AlertDT) WHEN '' THEN CCD.DueDT ELSE CCD.AlertDT END) THEN ''
							  WHEN (@Asof BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
						      WHEN (@Asof> CCD.DUEDT) AND (@Asof<=CCD.GRACEDT) THEN 'GRACE'
						      WHEN (@Asof > CCD.DUEDT) THEN 'PASSED DUE' END),
			[passedduealert]=(case when CCD.IsPassedDueAlert='0' then 'TRUE'
			                       WHEN CCD.IsPassedDueAlert='1' then 'FALSE'			
                                    WHEN CCD.IsPassedDueAlert IS NULL then 'FALSE' end),
		    [onetime]		= (case when CCD.IsOneTimeEvent='0' then 'TRUE'
		                           WHEN CCD.IsOneTimeEvent='1' then 'FALSE'			
                                    WHEN CCD.IsOneTimeEvent IS NULL then 'FALSE' end),
            [completed]		= (case when CCD.IsCompleted='0' then 'TRUE'
									WHEN CCD.IsCompleted='1' then 'FALSE'			
                                    WHEN CCD.IsCompleted IS NULL then 'FALSE' end),
            [totreqflthours]= CCD.TotalREQFlightHrs,
            --[totflthours] =(doubt)
			[totflthours]=CASE WHEN PM.AircraftID=CCD.AircraftID THEN  ROUND(PC.FlightHours,1) ELSE NULL END,
			--[totflthours]=(CASE WHEN (CP.TimeDisplayTenMin)=1 THEN SUM(pc.flighthours) when (CP.TimeDisplayTenMin)=0 THEN [dbo].[DHM2THM](pc.flighthours,7)END),
			c.IsDeleted,c.IsStatus,[TenToMin]=@TenToMin,
			[DATFORMAT]=@DATEFORMAT
          --INTO #CREWExp1
		FROM Crew c
		INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
		LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID AND CCD.IsDeleted=0 
		LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
		LEFT OUTER JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
		inner JOIN POSTFLIGHTCREW PC ON C.CREWID=PC.CREWID
		left outer JOIN POSTFLIGHTMAIN PM ON PM.POLogID=PC.POLogID AND PM.IsDeleted=0
		LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
		LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID    
		LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
        LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID
        
		WHERE C.CustomerID =CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		AND (CONVERT(DATE,@Asof) >= CONVERT(DATE,CCD.AlertDT))
		AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
		AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = ''))a
		group by asof,crewcode,last_name,code,descr,previous,due,type_code,freqmon,basemon,alert,grace,chkstatus,passedduealert,onetime
,completed,totreqflthours,IsDeleted,IsStatus,TenToMin,DATFORMAT
end
else
begin
INSERT INTO #CREWExp1
select asof,crewcode,last_name,code,descr,previous,due,type_code,freqmon,basemon,alert,grace,chkstatus,passedduealert,onetime
,completed,totreqflthours,sum(totflthours) totflthours,IsDeleted,IsStatus,TenToMin,DATFORMAT
from(
	SELECT distinct
			[asof]			= @Asof, 
			[crewcode]		= C.CrewCD,
			[last_name]		= C.LastName,
			[code]			= CCD.CheckListCD,
			[descr]			=  CCL.CrewChecklistDescription,
                               
	        [previous]		= CCD.PreviousCheckDT,
			[due]			= CCD.DueDT,
			[type_code]		= AC.AircraftCD,
			[freqmon]		= CCD.FrequencyMonth,
			[basemon]		= CCD.BaseMonthDT,
			[alert]			= CCD.AlertDT,
			[grace]			= CCD.GraceDT,
			[chkstatus]		=  (CASE WHEN (CCD.IsOneTimeEvent='TRUE') And (CCD.IsCompleted='TRUE') THEN 'COMPLETED'
                              WHEN @Asof='' THEN ' '
							  WHEN (@Asof) < (CASE (CCD.AlertDT) WHEN '' THEN CCD.DueDT ELSE CCD.AlertDT END) THEN ''
							  WHEN (@Asof BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
						      WHEN (@Asof> CCD.DUEDT) AND (@Asof<=CCD.GRACEDT) THEN 'GRACE'
						      WHEN (@Asof > CCD.DUEDT) THEN 'PASSED DUE' END),
			[passedduealert]=(case when CCD.IsPassedDueAlert='0' then 'TRUE'
			                       WHEN CCD.IsPassedDueAlert='1' then 'FALSE'			
                                    WHEN CCD.IsPassedDueAlert IS NULL then 'FALSE' end),
		    [onetime]		= (case when CCD.IsOneTimeEvent='0' then 'TRUE'
		                           WHEN CCD.IsOneTimeEvent='1' then 'FALSE'			
                                    WHEN CCD.IsOneTimeEvent IS NULL then 'FALSE' end),
            [completed]		= (case when CCD.IsCompleted='0' then 'TRUE'
									WHEN CCD.IsCompleted='1' then 'FALSE'			
                                    WHEN CCD.IsCompleted IS NULL then 'FALSE' end),
            [totreqflthours]= CCD.TotalREQFlightHrs,
			[totflthours]=CASE WHEN PM.AircraftID=CCD.AircraftID THEN ROUND(PC.FlightHours,1) ELSE NULL END,
		
			c.IsDeleted,c.IsStatus,[TenToMin]=@TenToMin,
			[DATFORMAT]=@DATEFORMAT
          --INTO #CREWExp1
		FROM Crew c
		INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
		INNER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
		LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
		LEFT OUTER JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
		inner JOIN POSTFLIGHTCREW PC ON C.CREWID=PC.CREWID
		left outer JOIN POSTFLIGHTMAIN PM ON PM.POLogID=PC.POLogID AND PM.IsDeleted=0
		LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
		LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID    
		LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
        LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
		WHERE C.CustomerID =CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		--and c.IsDeleted = 0 AND c.IsStatus = 1 
		--AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
		
		AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
		AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = ''))a
			group by asof,crewcode,last_name,code,descr,previous,due,type_code,freqmon,basemon,alert,grace,chkstatus,passedduealert,onetime
,completed,totreqflthours,IsDeleted,IsStatus,TenToMin,DATFORMAT
end



--SELECT DISTINCT * FROM #CREWExp1

--union all
insert into #crewexp
SELECT DISTINCT

			[asof]			= NULL, 
			[crewcode]		= C.CrewCD,
			[last_name]		= C.LastName,
			[code]			= NULL,
			[descr]			= 'NO ITEMS',
	        [previous]		= NULL,
			[due]			= NULL,
			[type_code]		= NULL,
			[freqmon]		= 0,
			[basemon]		= NULL,
			[alert]			= NULL,
			[grace]			= NULL,
			[chkstatus]		= NULL,
			[passedduealert]= 'FALSE',
		    [onetime]		= 'FALSE',
            [completed]		= 'FALSE',
            [totreqflthours]= NULL,
			[totflthours]   = NULL,
			c.IsDeleted,c.IsStatus,[TenToMin]=@TenToMin,
			[DATFORMAT]=@DATEFORMAT
           from Crew C 
           INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
          LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
		--LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
		--LEFT OUTER JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
		--left outer JOIN POSTFLIGHTCREW PC ON C.CREWID=PC.CREWID
		LEFT OUTER JOIN  Company CP ON C.HomebaseID= CP.HomeBaseID    
		LEFT OUTER JOIN  AIRPORT A ON A.AirportID = C.HomebaseID
		LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID  
        LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
           WHERE --C.CrewCD not in (SELECT DISTINCT CrewCD FROM #CREWExp1)  
			 C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		  
		AND (A.ICAOID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
		--AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
		
			ORDER BY crewcode		

DELETE FROM #CREWExp WHERE crewcode IN(SELECT DISTINCT crewcode from #CREWExp1 )

DELETE FROM #CREWExp WHERE ISDELETED=0 AND ISSTATUS=0 AND crewcode NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))



IF @IsZeroSuppressActivityCrewRpt=0
BEGIN 
SELECT DISTINCT * FROM #CREWExp
UNION ALL
SELECT DISTINCT * FROM #CREWExp1
ORDER BY crewcode
END
ELSE 
BEGIN
--DELETE FROM #CREWExp WHERE ISDELETED=0 AND ISSTATUS=0 AND crewcode NOT IN((SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')))
SELECT DISTINCT * FROM #CREWExp1
ORDER BY crewcode	
END

IF OBJECT_ID('tempdb..#CREWExp') IS NOT NULL
DROP TABLE #CREWExp 	

IF OBJECT_ID('tempdb..#CREWExp1') IS NOT NULL
DROP TABLE #CREWExp1 	


   -- EXEC spGetReportPOSTCrewAlretsExportInformation 'JWILLIAMS_13','2009/12/31','','','','','100',1

GO


