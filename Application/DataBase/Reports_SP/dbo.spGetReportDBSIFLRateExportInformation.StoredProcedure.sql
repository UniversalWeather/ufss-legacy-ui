IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBSIFLRateExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBSIFLRateExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[spGetReportDBSIFLRateExportInformation] 
(
        @UserCD AS VARCHAR(30), --Mandatory
        @UserCustomerID AS VARCHAR(30)--Mandatory
)
AS
-- ===============================================================================
-- SPC Name: [spGetReportDBSIFLRateExportInformation]
-- Author: 
-- Create date: 01 Nov 2013
-- Description: Get SIFL Rate
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT 
  [startdt]	=StartDT
, [enddt]	=EndDT
, [rate1]=Rate1
, [rate2]=Rate2
, [rate3]=Rate3
, [termchg]	=TerminalCHG
FROM FareLevel
WHERE CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND CustomerID = CONVERT(BIGINT,@UserCustomerID)
-- And IsInActive=0
 And IsDeleted=0

--EXEC spGetReportDBSIFLRateExportInformation 'SUPERVISOR_99','10099'


GO


