IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftStatusMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftStatusMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[spGetReportPREAircraftStatusMhtmlInformation]      
  @UserCD AS VARCHAR(30), --Mandatory          
  @AsOf AS DATETIME, --Mandatory        
  @TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values        
  @HomeBase AS BIT = 0      
AS        
-- ===============================================================================        
-- SPC Name: spGetReportAircraftStatusExportInformation       
-- Author: ASKAR         
-- Create date: 16 Jul 2012        
-- Description: Get Aircraft Status information for REPORTS        
-- Revision History        
-- Date   Name  Ver  Change        
--         
-- ================================================================================        
BEGIN      
      
DECLARE @SQLSCRIPT AS NVARCHAR(MAX) = '';      
DECLARE @ParameterDefinition AS NVARCHAR(100)      
SET NOCOUNT ON       
      
 DECLARE @day1 varchar(30) = CONVERT(VARCHAR,@AsOf, 101)      
 DECLARE @day2 varchar(30) = CONVERT(VARCHAR,@AsOf+1, 101)      
 DECLARE @day3 varchar(30) = CONVERT(VARCHAR,@AsOf+2, 101)      
 DECLARE @day4 varchar(30) = CONVERT(VARCHAR,@AsOf+3, 101)      
 DECLARE @day5 varchar(30) = CONVERT(VARCHAR,@AsOf+4, 101)      
 DECLARE @day6 varchar(30)= CONVERT(VARCHAR,@AsOf+5, 101)      
 DECLARE @day7 varchar(30) = CONVERT(VARCHAR,@AsOf+6, 101)      
 DECLARE @day8 varchar(30) = CONVERT(VARCHAR,@AsOf+7, 101)      
 DECLARE @day9 varchar(30) = CONVERT(VARCHAR,@AsOf+8, 101)      
 DECLARE @day10 varchar(30) = CONVERT(VARCHAR,@AsOf+9, 101)      
 DECLARE @day11 varchar(30) = CONVERT(VARCHAR,@AsOf+10, 101)      
 DECLARE @day12 varchar(30) = CONVERT(VARCHAR,@AsOf+11, 101)      
 DECLARE @day13 varchar(30) = CONVERT(VARCHAR,@AsOf+12, 101)      
 DECLARE @day14 varchar(30) = CONVERT(VARCHAR,@AsOf+13, 101)      
 DECLARE @day15 varchar(30)= CONVERT(VARCHAR,@AsOf+14, 101)      
       
 SELECT DISTINCT F.FleetID Fleet
       ,F.TailNum  
       ,F.AircraftCD AS Type_Code  
       ,B.AircraftCD as Aircraft_Code  
       ,AirframeHrs  
       ,EstDepartureDT AS AF_ASOF  
       ,AirframeCycle  
       ,Engine1Hrs  
       ,EstDepartureDT AS EN1_ASOF  
       ,Engine2Hrs  
       ,EstDepartureDT AS EN2_ASOF  
       ,Engine3Hrs  
       ,EstDepartureDT AS EN3_ASOF  
       ,Engine4Hrs  
       ,EstDepartureDT AS EN4_ASOF  
       ,Engine1Cycle  
       ,Engine2Cycle  
       ,Engine3Cycle  
       ,Engine4Cycle  
       ,Reverser1Cycle  
       ,Reverser2Cycle  
       ,Reverser3Cycle  
       ,Reverser4Cycle
       ,F.IsInActive inactive INTO #AT_TEMP   
 FROM PREFLIGHTMAIN PM
 INNER JOIN Fleet F ON PM.fleetid = F.FleetID      
 INNER JOIN Aircraft B ON PM.AircraftID = B.AircraftID 
 left outer join FleetInformation A ON PM.FleetID = A.FleetID      
 WHERE PM.IsDeleted = 0    
  AND CONVERT(DATE,PM.EstDepartureDT) BETWEEN CONVERT(DATE,@AsOf) AND CONVERT(DATE,@AsOf+14)  
  AND PM.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))       
CREATE TABLE #TempFleetInfo (tail_nmbr VARCHAR(9),
                     type_code CHAR(5),
                     ac_code VARCHAR(10),
                     af_hrs NUMERIC(11,3),
                     af_asof DATE,
                     af_landings NUMERIC(6,0),
                     en1_hrs NUMERIC(11,3),
                     en1_asof  DATE,
                     en2_hrs NUMERIC(11,3),
                     en2_asof DATE,
                     en3_hrs NUMERIC(11,3),
                     en3_asof  DATE,
                     en4_hrs NUMERIC(11,3),
                     en4_asof DATE,
                     en1_cycle NUMERIC(6,0),
                     en2_cycle NUMERIC(6,0),
                     en3_cycle NUMERIC(6,0),
                     en4_cycle NUMERIC(6,0),
                     re1_cycle NUMERIC(6,0),
                     re2_cycle NUMERIC(6,0),
                     re3_cycle NUMERIC(6,0),
                     re4_cycle NUMERIC(6,0),
                     day1 NUMERIC(19,1),
                     day2 NUMERIC(19,1),
                     day3 NUMERIC(19,1),
                     day4 NUMERIC(19,1),
                     day5 NUMERIC(19,1),
                     day6 NUMERIC(19,1),
                     day7 NUMERIC(19,1),
                     day8 NUMERIC(19,1),
                     day9 NUMERIC(19,1),
                     day10 NUMERIC(19,1),
                     day11 NUMERIC(19,1),
                     day12 NUMERIC(19,1),
                     day13 NUMERIC(19,1),
                     day14 NUMERIC(19,1),
                     day15 NUMERIC(19,1),
                     inactive BIT )
        
SET @SQLSCRIPT=      
  'INSERT INTO #TempFleetInfo
  SELECT DISTINCT TailNum  AS tail_nmbr  
       , Type_Code AS type_code  
       , Aircraft_Code AS ac_code  
       ,ISNULL(AirframeHrs,0) AS af_hrs  
       , AF_ASOF AS af_asof
       ,ISNULL(AirframeCycle,0) AS af_landings 
       ,ISNULL(Engine1Hrs,0) AS en1_hrs 
       ,EN1_ASOF AS en1_asof  
       ,ISNULL(Engine2Hrs,0) AS en2_hrs  
       ,EN2_ASOF AS en2_asof 
       ,ISNULL(Engine3Hrs,0) AS en3_hrs  
       ,EN3_ASOF AS en3_asof 
       ,ISNULL(Engine4Hrs,0) AS en4_hrs  
       ,EN4_ASOF AS en4_asof  
       ,ISNULL(Engine1Cycle,0) AS en1_cycle  
       ,ISNULL(Engine2Cycle,0) AS en2_cycle  
       ,ISNULL(Engine3Cycle,0) AS en3_cycle 
       ,ISNULL(Engine4Cycle,0) AS en4_cycle  
       ,ISNULL(Reverser1Cycle,0) AS re1_cycle  
       ,ISNULL(Reverser2Cycle,0) AS re2_cycle 
       ,ISNULL(Reverser3Cycle,0) AS re3_cycle 
       ,ISNULL(Reverser4Cycle,0) AS re4_cycle 
   ,ISNULL(CONVERT(NUMERIC(19,1),['+@day1+']),0) AS day1  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day2+']),0) AS day2  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day3+']),0) AS day3  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day4+']),0) AS day4  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day5+']),0) AS day5  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day6+']),0) AS day6  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day7+']),0) AS day7  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day8+']),0) AS day8  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day9+']),0) AS day9  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day10+']),0) AS day10  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day11+']),0) AS day11  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day12+']),0) AS day12  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day13+']),0) AS day13  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day14+']),0)  AS day14  
  ,ISNULL(CONVERT(NUMERIC(19,1),['+@day15+']),0) AS day15 
  ,inactive 
     FROM (SELECT    
                   PM.EstDepartureDT AS DEPDATE      
                  ,PL.ElapseTM AS ElapseTM  
                  ,T.*  
                        
             FROM  preflightmain PM  
                 inner join preflightleg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0  
   INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.IsDeleted 
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
						WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 
					  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID     
               inner join #AT_TEMP  T ON F.FleetID = T.Fleet  
    WHERE  F.IsDeleted = 0
    AND PM.ISDELETED = 0
  '      

IF @TailNUM <> '' BEGIN        
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';      
 END      
     
IF @HomeBase = 1 BEGIN      
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));      
 END       
      
      
SET @SQLSCRIPT = @SQLSCRIPT +' AND PM.TripStatus = '+ '''T''' +'      
  ) AS A      
  PIVOT (       
   SUM(ElapseTM) FOR A.DEPDATE IN (      
    ['+@day1+'],['+@day2+'],['+@day3+'],['+@day4+'],['+@day5+'],['+@day6+'],['+@day7+'],['+@day8+'],['+@day9+'],['+@day10+'],['+@day11+'],['+@day12+'],['+@day13+'],['+@day14+'],['+@day15+']      
   )       
  ) AS SUMELAPSEDTIME'      
--print @SQLSCRIPT       
SET @ParameterDefinition = '@FleetGroupCD AS Varchar(500)'     
EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @FleetGroupCD 
  INSERT INTO #TempFleetInfo(tail_nmbr,type_code,ac_code)
  SELECT DISTINCT F.TailNum,F.AircraftCD,A.AircraftCD FROM Fleet F LEFT OUTER JOIN Aircraft  A ON F.AircraftID=A.AircraftID
                                                 WHERE F.TailNum NOT IN(SELECT DISTINCT tail_nmbr FROM #TempFleetInfo)
                                                 AND  F.CustomerID =CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  

 SELECT DISTINCT tail_nmbr,type_code,ac_code,ISNULL(af_hrs,0) af_hrs,af_asof,ISNULL(af_landings,0) af_landings,ISNULL(en1_hrs,0) en1_hrs,en1_asof,ISNULL(en2_hrs,0) en2_hrs,en2_asof,ISNULL(en3_hrs,0) en3_hrs,en3_asof,ISNULL(en4_hrs,0)en4_hrs,en4_asof,
               	 ISNULL(en1_cycle,0)en1_cycle,ISNULL(en2_cycle,0)en2_cycle,ISNULL(en3_cycle,0)en3_cycle,ISNULL(en4_cycle,0)en4_cycle,ISNULL(re1_cycle,0)re1_cycle,ISNULL(re2_cycle,0)re2_cycle,ISNULL(re3_cycle,0)re3_cycle,ISNULL(re4_cycle,0)re4_cycle,ISNULL(day1,0)day1,ISNULL(day2,0)day2,ISNULL(day3,0)day3,ISNULL(day4,0) day4,
               	 ISNULL(day5,0)day5,ISNULL(day6,0)day6,ISNULL(day7,0)day7,ISNULL(day8,0)day8,ISNULL(day9,0)day9,ISNULL(day10,0)day10,ISNULL(day11,0)day11,ISNULL(day12,0)day12,ISNULL(day13,0)day13,ISNULL(day14,0)day14,ISNULL(day15,0)day15,ISNULL(inactive,0)inactive
        FROM #TempFleetInfo
        ORDER BY tail_nmbr,type_code,ac_code
 
 END

 --EXEC spGetReportPREAircraftStatusMhtmlInformation 'JWILLIAMS_13', '2012-10-15',''


GO


