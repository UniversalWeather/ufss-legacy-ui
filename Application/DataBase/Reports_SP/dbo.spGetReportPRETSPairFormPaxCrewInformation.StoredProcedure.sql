IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPairFormPaxCrewInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPairFormPaxCrewInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSPairFormPaxCrewInformation]
	@UserCD VARCHAR(30)
	,@TripID VARCHAR(30)
	,@LegNum VARCHAR(30) = ''
	,@SetNum SMALLINT
	,@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table
	,@TempSettings VARCHAR(MAX) = '' --'PAXMNORD::TRUE'
AS

BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSPairFormPaxCrewInformation
-- Author: AISHWARYA.M
-- Create date:9 OCT 2012
-- Description: Get Preflight Tripsheet Pair Form information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================
SET NOCOUNT ON
DECLARE @SetNum1 SMALLINT = 1;

IF OBJECT_ID('TEMPDB..#TEMP1') IS NOT NULL DROP TABLE #TEMP1; 
CREATE TABLE #TEMP1 (Row_Id INT IDENTITY,SetNo INT,CreworPassenger VARCHAR(20),Name VARCHAR(40),DateofBirth DATE,Nationality CHAR(3), RowID INT)

-- TO ADD A DUMMY RECORD TO DISPLAY BLANK FORM BASED IN THE FORMAT SETTINGS
----------------------------------------------------------------------------
	DECLARE @FormatSettings TABLE (
		BLANKFORM BIT
		,PAXMNORD BIT
		,BLANKDTTM BIT
	)
	INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 9, @TempSettings
	--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161675, 9, 'PAXMNORD::FALSE' 
	--SELECT * FROM @FormatSettings


	DECLARE	@BLANKFORM BIT, @PAXMNORD BIT, @BLANKDTTM BIT;
	SELECT @BLANKFORM = BLANKFORM, @PAXMNORD = PAXMNORD, @BLANKDTTM = BLANKDTTM FROM @FormatSettings
	--SELECT @BLANKFORM, @PAXMNORD, @BLANKDTTM

---------------------------------------------------------------------------	
	
  DECLARE @SQLSCRIPT AS  NVARCHAR(4000) = '';
  DECLARE @ParameterDefinition AS NVARCHAR(500)


                    
--------------TO EXCLUDE FIRST PILOT-----------------
DECLARE @TEMP TABLE(RowID INT,
                    CrewID BIGINT,
                    TripID BIGINT,
                    CREWCD VARCHAR(5),
                    LegNUM BIGINT,
                    Legid BIGINT) 

INSERT INTO @TEMP 
SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY PL.LegID,CrewCD ),CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
                                                 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
                                                 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
                                                 WHERE PCL.DutyTYPE IN('P') 
                                                 AND PM.TripID IN(@TripID)
                                                 AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
                                                 ORDER BY CrewCD DESC 


--------------------------------------------------

DECLARE @PaxCrew TABLE(RowID INT IDENTITY
                    ,CreworPassenger CHAR(1),
                    [Name] VARCHAR(160),
                    [DateofBirth] DATETIME,
                    [Nationality]  CHAR(3),
                    PaxCode  VARCHAR(5),
                    CrewCD CHAR(20),
                    SetNo INT
                   )


IF @PAXMNORD=1
BEGIN
INSERT INTO @PaxCrew(CreworPassenger,[Name],[DateofBirth],[Nationality],PaxCode,CrewCD)
SELECT DISTINCT [CreworPassenger] = 'C'
					,[Name] = C.LastName + ',' + C.FirstName
					,[DateofBirth] = C.BirthDT
					,[Nationality] = CASE WHEN CC.CountryCD = '' OR CC.CountryCD IS NULL THEN CO.CountryCD
										ELSE CC.CountryCD END
					,''
					,CrewCD

					FROM PreflightMain PM
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					INNER JOIN (SELECT PC.LEGID, PC.CrewID, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
						, PC.PassportID
						FROM PreflightCrewList PC
					) PCL ON PL.LegID = PCL.LegID
					INNER JOIN Crew C ON PCL.CrewID = C.CrewID
					LEFT OUTER JOIN CrewPassengerPassport CPP 
						ON PCL.PassportID = CPP.PassportID AND PCL.CrewID = CPP.CrewID AND CPP.IsDeleted = 0
					LEFT OUTER JOIN Country CC ON CPP.CountryID = CC.CountryID
					LEFT OUTER JOIN Country CO ON C.CountryID = CO.CountryID
					WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
					AND PM.TripID = CONVERT(BIGINT,@TripID)
					AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
					AND C.CrewID NOT IN(SELECT DISTINCT CrewID FROM @TEMP WHERE  RowID=1) 
					ORDER BY CrewCD 
					


INSERT INTO @PaxCrew(CreworPassenger,[Name],[DateofBirth],[Nationality],PaxCode,CrewCD)
SELECT DISTINCT [CreworPassenger] = 'P'
					,[Name] = P.LastName + ',' + P.FirstName
					,[DateofBirth] = P.DateOfBirth
					,[Nationality] = CASE WHEN CP.CountryCD = '' OR CP.CountryCD IS NULL THEN CO.CountryCD
										ELSE CP.CountryCD END
					,P.PassengerRequestorCD
					,''
					FROM PreflightMain PM
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
					INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
					LEFT OUTER JOIN CrewPassengerPassport CPP 
							ON CPP.PassengerRequestorID = PPL.PassengerID AND PPL.PassportID = CPP.PassportID
							AND CPP.IsDeleted = 0
					LEFT OUTER JOIN Country CP ON CPP.CountryID = CP.CountryID
					LEFT OUTER JOIN Country CO ON P.CountryID = CO.CountryID
					WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
					AND PM.TripID = CONVERT(BIGINT,@TripID)
					AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
					ORDER BY P.LastName + ',' + P.FirstName ---PassengerRequestorCD 


END
ELSE

BEGIN

INSERT INTO @PaxCrew(CreworPassenger,[Name],[DateofBirth],[Nationality],PaxCode,CrewCD)
SELECT DISTINCT [CreworPassenger] = 'P'
					,[Name] = P.LastName + ',' + P.FirstName
					,[DateofBirth] = P.DateOfBirth
					,[Nationality] = CASE WHEN CP.CountryCD = '' OR CP.CountryCD IS NULL THEN CO.CountryCD
										ELSE CP.CountryCD END
					,P.PassengerRequestorCD
					,''
					FROM PreflightMain PM
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
					INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
					LEFT OUTER JOIN CrewPassengerPassport CPP 
						ON CPP.PassengerRequestorID = PPL.PassengerID  AND PPL.PassportID = CPP.PassportID
							AND CPP.IsDeleted = 0
					LEFT OUTER JOIN Country CP ON CPP.CountryID = CP.CountryID
					LEFT OUTER JOIN Country CO ON P.CountryID = CO.CountryID
					WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
					AND PM.TripID = CONVERT(BIGINT,@TripID)
					AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
					ORDER BY PassengerRequestorCD DESC


INSERT INTO @PaxCrew(CreworPassenger,[Name],[DateofBirth],[Nationality],PaxCode,CrewCD)
SELECT DISTINCT [CreworPassenger] = 'C'
					,[Name] = C.LastName + ',' + C.FirstName
					,[DateofBirth] = C.BirthDT
					,[Nationality] = CASE WHEN CC.CountryCD = '' OR CC.CountryCD IS NULL THEN CO.CountryCD
										ELSE CC.CountryCD END
					,''
					,CrewCD

					FROM PreflightMain PM
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					INNER JOIN (SELECT PC.LEGID, PC.CrewID, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
						WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END
						, PC.PassportID
						FROM PreflightCrewList PC
					) PCL ON PL.LegID = PCL.LegID
					INNER JOIN Crew C ON PCL.CrewID = C.CrewID
					LEFT OUTER JOIN CrewPassengerPassport CPP 
						ON PCL.CrewID = CPP.CrewID AND PCL.PassportID = CPP.PassportID AND CPP.IsDeleted = 0
					LEFT OUTER JOIN Country CC ON CPP.CountryID = CC.CountryID
					LEFT OUTER JOIN Country CO ON C.CountryID = CO.CountryID
					WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
					AND PM.TripID = CONVERT(BIGINT,@TripID)
					AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
					AND C.CrewID NOT IN(SELECT DISTINCT CrewID FROM @TEMP WHERE  RowID=1) 
					ORDER BY CrewCD 
END

	DECLARE @COUNT INT 
	DECLARE @COUNT1 INT 
	SELECT @COUNT = COUNT(*) FROM @PaxCrew   
	SELECT @COUNT1 = COUNT(*) FROM @PaxCrew    
	SET @COUNT = @COUNT + (10 - (CASE WHEN @COUNT%10 = 0 THEN 10 ELSE @COUNT%10 END))

	IF @COUNT = 0 BEGIN SET @COUNT = 10 END; -- POPULATE 10 EMPTY ROWS FOR A BLANK PAGE

	WHILE (@COUNT1<@COUNT)
	BEGIN
		INSERT INTO @PaxCrew(Name,SetNo)VALUES(NULL,0);
		SET @COUNT1=@COUNT1+1
	END

	DECLARE @RowId1 INT=1,@RowId2 INT=10;
	IF @SetNum > 0
	BEGIN
		WHILE (@RowId2<=@COUNT)
		BEGIN
			UPDATE @PaxCrew SET SetNo=@SetNum1 WHERE RowId BETWEEN @RowId1 AND @RowId2
			SET @RowId1=@RowId2+1
			SET @RowId2=@RowId2+10
			SET @SetNum1=@SetNum1+1
		END
	END
	--SELECT @PAXMNORD
	IF @PAXMNORD=0
	BEGIN
		SELECT * FROM @PaxCrew  WHERE SetNo=@SetNum ORDER BY CreworPassenger DESC
	END ELSE
	BEGIN
		SELECT * FROM @PaxCrew WHERE SetNo=@SetNum 
	END
END
--EXEC spGetReportPRETSPairFormPaxCrewInformation 'JWILLIAMS_13',10013105580,'',1,'10013161675', 'PAXMNORD::TRUE'
--EXEC spGetReportPRETSPairFormPaxCrewInformation 'JWILLIAMS_13',0,'0',0,'10013161675', ''



GO


