IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerBillingSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerBillingSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- EXEC [dbo].[spGetReportPOSTPassengerBillingSummary]  '10013','JWILLIAMS_13','01-01-2009','03-01-2013','','','','',0
CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerBillingSummary] (
	@UserCustomerID VARCHAR(30),
	@UserCD AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATE,--Mandatory
	@DATETO AS DATE,--Mandatory
	@PassengerRequestorCD AS NVARCHAR(1000) = '',
	@TailNum AS NVARCHAR(1000) = '',-- [Optional], Comma delimited string with mutiple values
	@FleetGroupCD AS NVARCHAR(1000) = '',-- [Optional], Comma delimited string with mutiple values
	@UserHomebaseID AS VARCHAR(30),
	@IsHomebase AS BIT = 0,
	@PassengerGroupCD VARCHAR(500)='' 
	-- @TenToMin SMALLINT = 0
	)
AS
BEGIN
	-- ===============================================================================
	-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailInformation
	-- Author: 
	-- Create date: 
	-- Description: Get Department/Authorization Detail for REPORTS
	-- Revision History
	-- Date                 Name        Ver         Change
	-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
	-- ================================================================================
	SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
Declare @SuppressActivityPassenger BIT  
 	
SELECT @SuppressActivityPassenger=IsZeroSuppressActivityPassengerRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
	AND P.IsActive = 1
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER 
	AND P.IsActive = 1 
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
	AND P.IsActive = 1
END
-----------------------------Passenger and Passenger Group Filteration----------------------
	--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @TenToMin SMALLINT = 0;

	SELECT @TenToMin = TimeDisplayTenMin
	FROM Company
	WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
		AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	--AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
	SELECT DISTINCT DATERANGE = dbo.GetDateFormatByUserCD(@UserCD, @DATEFROM) + ' - ' + dbo.GetDateFormatByUserCD(@UserCD, @DATETO)
		,P.PassengerRequestorCD
		,ISNULL(LastName, '') + CASE 
			WHEN FirstName <> ''
				OR FirstName IS NOT NULL
				THEN ISNULL(', ' + FirstName, '')
			ELSE ''
			END AS PassengerName
		,
		--PP.PassengerLastName,
		DE.DepartmentCD
		,DA.AuthorizationCD
		,P.StandardBilling
		,PL.Distance
		,ROUND(PL.FlightHours,1) FlightHours
		,ROUND(PL.BlockHours,1) BlockHours
		,TOTAL_CHARGES = CASE WHEN  ISNULL(PL.PassengerTotal,0)=0 THEN 0 ELSE (
			(
				CASE 
					WHEN CR.ChargeUnit = 'N'
						THEN ((PL.Distance * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0))
					WHEN CR.ChargeUnit = 'S'
						THEN (((PL.Distance * CR.ChargeRate) * 1.15078) + ISNULL((PFE11.SUMExpenseAmt), 0))
					WHEN (
							CR.ChargeUnit = 'H'
							AND CO.AircraftBasis = 1
							)
						THEN (((ISNULL(ROUND(PL.BlockHours,1),0) * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0)))
					WHEN (
							CR.ChargeUnit = 'H'
							AND CO.AircraftBasis = 2
							)
						THEN ((ISNULL(ROUND(PL.FlightHours,1),0) * CR.ChargeRate) + ISNULL((PFE11.SUMExpenseAmt), 0))
					ELSE ISNULL((PFE11.SUMExpenseAmt), 0)
					END
				) / PL.PassengerTotal
			)  END
		,PFE11.SUMExpenseAmt
		,
		--(200+PFE11.SUMExpenseAmt) AS TOTAL_CHARGES,
		--FL.TailNum,
		CASE 
			WHEN TH.OldTailNUM = FL.TailNum
				THEN TH.NewTailNUM
			ELSE FL.TailNum
			END AS [Tail Number]
		,CONVERT(DATE, PO.EstDepartureDT) TRIPDATE
		,PO.DispatchNum
		,PO.LogNum
		,--PO.DepartmentID, PO.AuthorizationID, 
		--PL.POLegID, 
		PL.ScheduledTM ScheduleDTTMLocal
		,
		--PL.DepartICAOID, PL.ArriveICAOID,
		AD.IcaoID AS DepartICAOCD
		,AA.IcaoID AS ArriveICAOCD
		,PL.PassengerTotal AS PAX_TOT
		,FL.MaximumPassenger - PL.PassengerTotal AS ES_TOTAL
		,FL.AircraftCD
		,CR.ChargeUnit
		,CR.ChargeRate
		,
		--PFP.PassengerFirstName, FP.FlightPurposeCD,
		--PAX.FlightPurposeCDS AS FlightPurposeCD,
		--PAX.PaxNames AS PassengerName ,
		--ACC.AccountDesc ,ACC.ExpenseAmt,
		--AC.AccountDescription, PFE.ExpenseAMT,
		PFE11.IsBilling
		,[TenToMin] = @TenToMin
	INTO #PassTemp
	FROM PostflightMain PO
	JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID AND PL.IsDeleted = 0
		AND PO.CustomerID = PL.CustomerID
	/* LEFT JOIN Passenger P
		   ON PO.PassengerRequestorID = P.PassengerRequestorID AND PL.CustomerID = P.CustomerID  
	   LEFT JOIN PostflightPassenger PP
		   ON PL.POLegID =  PP.POLegID AND PP.CustomerID = PO.CustomerID  */
	LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID
	LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
	INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	--LEFT JOIN vDeptAuthGroup DE ON PL.DepartmentID = DE.DepartmentID AND PL.CustomerID = DE.CustomerID
	LEFT JOIN Department DE ON DE.DepartmentID = P.DepartmentID
	LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = P.AuthorizationID
	JOIN vFleetGroup FL ON FL.FleetID = PO.FleetID
		AND FL.CustomerID = PO.CustomerID --AND PL.CustomerID = FL.CustomerID
	LEFT JOIN TailHistory TH ON FL.TailNum = TH.OldTailNUM
		AND FL.CustomerID = TH.CustomerID
	-- LEFT JOIN FleetChargeRate FCR
	-- ON FL.FleetID = FCR.FleetID  AND FL.CustomerID = FCR.CustomerID 
	--FL.AircraftID = FCR.AircraftID AND PO.CustomerID = FCR.CustomerID AND PL.CustomerID = FCR.CustomerID
	-- AND  PO.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT
	LEFT JOIN Company CO ON FL.HomebaseID = CO.HomebaseID
		AND FL.CustomerID = CO.CustomerID
	LEFT JOIN Airport AI ON CO.HomebaseAirportID = AI.AIRPORTID
		AND AI.CustomerID = CO.CustomerID
	LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
	LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	/* JOIN PostflightPassenger PFP
		   ON PL.POLegID = PFP.POLegID AND PL.POLogID = PFP.POLogID AND PL.CustomerID = PFP.CustomerID*/
	/*JOIN PostflightExpense PFE
		   ON PL.POLegID = PFE.POLegID AND PL.POLogID = PFE.POLogID AND PL.CustomerID = PFE.CustomerID
	     JOIN Account AC
		   ON PFE.AccountID = AC.AccountID AND PFE.CustomerID = AC.CustomerID*/
	LEFT JOIN (
		SELECT DISTINCT PP4.POLogID
			,PP4.POLegID
			,PP4.IsBilling
			,PP4.CustomerID
			,SUMExpenseAmt = (
				SELECT ISNULL(SUM(PP2.ExpenseAMT), 0)
				FROM PostflightExpense PP2
				WHERE PP2.POLegID = PP4.POLegID
					AND IsBilling = 1
					AND PP2.IsDeleted = 0
				)
		FROM PostflightExpense PP4
		WHERE IsBilling = 1 
		AND PP4.IsDeleted = 0
		--AND POLegID = 100997291
		) PFE11 ON PL.POLegID = PFE11.POLegID
		AND PL.POLogID = PFE11.POLogID
	/* JOIN FlightPurpose FP 
		  ON PFP.FlightPurposeID = FP.FlightPurposeID AND PFP.CustomerID = FP.CustomerID*/
	/*--JOIN (SELECT PFE1.AccountID,AccountDescription ,SUM(ExpenseAMT) AS EXP_AMT FROM PostflightExpense PFE1 JOIN ACCOUNT ACC1 
		  ON PFE1.AccountID = ACC1.AccountID AND PFE1.CustomerID = ACC1.CustomerID GROUP BY PFE1.AccountID,AccountDescription)PFE2
		  ON PL.AccountID = PFE2.AccountID
	    JOIN TailHistory TH
		  ON TH.NewTailNUM = FL.TailNum */-- IF TAIL HISTORY ADDED MEANS NO DATA IS COMING.
	/*LEFT  JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,PaxNames = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$'  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH('')),
                      FlightPurposeCDS = (
                       SELECT ISNULL(P1.FlightPurposeCD,'')+'$$' 
                               FROM PostflightPassenger PP1 INNER JOIN FlightPurpose P1 ON PP1.FlightPurposeID=P1.FlightPurposeID 
                               WHERE PP1.POLegID = PP2.POLegID
                             FOR XML PATH('')
                             )
                              FROM PostflightPassenger PP2 
                              --WHERE PP2.POLegID = 100135919
				   ) PAX ON PL.POLegID = PAX.POLegID  -- NEW CHANGE
		JOIN(SELECT DISTINCT PP3.POLogID,PP3.POLegID,PP3.CustomerID,ExpenseAmt = (
                      SELECT ISNULL(CAST(PP1.ExpenseAMT AS VARCHAR(50)) + '','') +'$$'  
                               FROM PostflightExpense PP1 
                               --INNER JOIN Account P1 ON PP1.AccountID = P1.AccountID 
                               WHERE PP1.POLegID = PP3.POLegID
                               ORDER BY PP1.ExpenseAMT DESC
                             FOR XML PATH('')),
							AccountDesc = (
							 SELECT DISTINCT ISNULL(P1.AccountDescription,'')+'$$' 
                               FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP3.POLegID
                             FOR XML PATH('')
                             )
                       FROM PostflightExpense PP3 WHERE PP3.IsBilling =1)
                       ACC ON PL.POLegID =ACC.POLegID  AND PL.POLogID = ACC.POLogID*/
	LEFT OUTER JOIN (
		SELECT POLegID
			,ChargeUnit
			,ChargeRate
		FROM PostflightMain PM
		INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
		INNER JOIN FleetChargeRate FC ON PM.FleetID = FC.FleetID
			AND CONVERT(DATE, BeginRateDT) <= CONVERT(DATE, PL.ScheduledTM)
			AND EndRateDT >= CONVERT(DATE, PL.ScheduledTM)
		) CR ON PL.POLegID = CR.POLegID
	WHERE CONVERT(DATE, PL.ScheduledTM) BETWEEN @DateFrom
			AND @DateTo
		--AND (
		--	P.PassengerRequestorCD IN (
		--		SELECT DISTINCT RTRIM(S)
		--		FROM dbo.SplitString(@PassengerCD, ',')
		--		)
		--	OR @PassengerCD = ''
		--	)
		AND (
			FL.FleetGroupCD IN (
				SELECT DISTINCT RTRIM(S)
				FROM dbo.SplitString(@FleetGroupCD, ',')
				)
			OR @FleetGroupCD = ''
			)
		AND (
			FL.TailNum IN (
				SELECT DISTINCT RTRIM(S)
				FROM dbo.SplitString(@TailNUM, ',')
				)
			OR @TailNUM = ''
			)
		--AND CONVERT(DATE,PL.ScheduledTM) between CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
		--AND CONVERT(DATE,FCR.BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND FCR.EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
		--AND (PO.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID = 0)
		AND (
			PO.HomebaseID IN (CONVERT(BIGINT, @UserHomebaseID))
			OR @IsHomebase = 0
			)
		AND PO.CustomerID = CONVERT(BIGINT, @UserCustomerID)
		AND P.PassengerRequestorCD IS NOT NULL
		AND P.IsActive = 1
		AND PO.IsDeleted = 0

	--ORDER BY PL.LegNUM, DE.AuthorizationCD*/

IF @SuppressActivityPassenger=0
BEGIN
	INSERT INTO #PassTemp (
		PassengerRequestorCD
		,PassengerName
		,TenToMin
		)
	SELECT P.PassengerRequestorCD
		,ISNULL(LastName, '') + CASE 
			WHEN FirstName <> ''
				OR FirstName IS NOT NULL
				THEN ISNULL(', ' + FirstName, '')
			ELSE ''
			END
		,@TenToMin
	FROM Passenger P
	     INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	WHERE P.CustomerID = CONVERT(BIGINT, @UserCustomerID)
		--AND (
		--	P.PassengerRequestorCD IN (
		--		SELECT DISTINCT RTRIM(S)
		--		FROM dbo.SplitString(@PassengerCD, ',')
		--		)
		--	OR @PassengerCD = ''
		--	)
		--AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		--AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
		AND PassengerRequestorCD NOT IN (
			SELECT PassengerRequestorCD
			FROM #PassTemp
			WHERE PassengerRequestorCD IS NOT NULL
			)
			AND P.IsActive = 1

END
	SELECT DATERANGE
		,PassengerRequestorCD
		,PassengerName
		,
		--PP.PassengerLastName,
		DepartmentCD
		,AuthorizationCD
		,StandardBilling
		,Distance
		,FlightHours
		,BlockHours
		,TOTAL_CHARGES
		,SUMExpenseAmt
		,
		--(200+PFE11.SUMExpenseAmt) AS TOTAL_CHARGES,
		--FL.TailNum,
		[Tail Number]
		,TRIPDATE
		,DispatchNum
		,LogNum
		,--PO.DepartmentID, PO.AuthorizationID, 
		--PL.POLegID, 
		ScheduleDTTMLocal
		,
		--PL.DepartICAOID, PL.ArriveICAOID,
		DepartICAOCD
		,ArriveICAOCD
		,ISNULL(PAX_TOT,0)AS PAX_TOT
		,ES_TOTAL
		,AircraftCD
		,ChargeUnit
		,ChargeRate
		--PFP.PassengerFirstName, FP.FlightPurposeCD,
		--PAX.FlightPurposeCDS AS FlightPurposeCD,
		--PAX.PaxNames AS PassengerName ,
		--ACC.AccountDesc ,ACC.ExpenseAmt,
		--AC.AccountDescription, PFE.ExpenseAMT,
		,IsBilling
		,[TenToMin]
	FROM #PassTemp

	IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
		DROP TABLE #PassTemp
END







GO


