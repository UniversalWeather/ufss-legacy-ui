IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREPassengerItinerarySubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREPassengerItinerarySubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPREPassengerItinerarySubInformation]
         @LegID AS VARCHAR(30)   -- Mandatory

AS

-- =============================================================================
-- SPC Name: spGetReportPREPassengerItinerarySubInformation
-- Author: ABHISHEK.S
-- Create date: 23 August 2012
-- Description: Get Preflight Passenger Itinerary Sub Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==============================================================================

BEGIN
SET NOCOUNT ON;

DECLARE	
	@PIC VARCHAR(50), @PICPhone VARCHAR(25), @PICCaptainPager VARCHAR(25), 
	@OTHERCREW VARCHAR(500)
	
SET @OTHERCREW = ''
DECLARE @CrewID BIGINT, @DutyTYPE VARCHAR(1), @CustomerID BIGINT;

	DECLARE curCrewInfo CURSOR FOR
		SELECT  PC.CrewID, PC.DutyTYPE, PC.CustomerID 
		FROM PreflightCrewList PC
		WHERE PC.LegID = CONVERT(BIGINT,@LegID)
				ORDER BY (CASE WHEN PC.DutyType='P' THEN 1 
					   WHEN PC.DutyType='S' THEN 2
					   WHEN PC.DutyType='E' THEN 3
					   WHEN PC.DutyType='A' THEN 4 
					   WHEN PC.DutyType='O' THEN 5 ELSE 6 END),PC.PreflightCrewListID DESC

	OPEN curCrewInfo;
		FETCH NEXT FROM curCrewInfo INTO @CrewID, @DutyTYPE, @CustomerID;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- P S E A O I
			IF @DutyTYPE = 'P' 
				  BEGIN
						SELECT 	@PIC = FirstName +' '+ LastName,
							@PICPhone = CellPhoneNum,
							@PICCaptainPager = PagerNum
						FROM CREW WHERE CREWID = @CrewID AND CustomerID = @CustomerID
			END
			
			ELSE
					BEGIN
						
							
						SELECT @OTHERCREW = @OTHERCREW + ' ' + FirstName +' '+ LastName +','		
							FROM CREW WHERE CREWID = @CrewID AND CustomerID = @CustomerID
					END
			
			FETCH NEXT FROM curCrewInfo INTO @CrewID, @DutyTYPE, @CustomerID;
		END
	CLOSE curCrewInfo;
	DEALLOCATE curCrewInfo;

	SELECT 
	PIC = @PIC, PICPhone = @PICPhone, PICCaptainPager = @PICCaptainPager,
	--OtherCrew = @OtherCrew
	--OtherCrew = SUBSTRING(@OtherCrew,1,LEN(@OtherCrew)-1)	
	OtherCrew = CASE WHEN LEN(@OtherCrew) > 0 THEN SUBSTRING(@OtherCrew,1,LEN(@OtherCrew)-1) ELSE @OtherCrew END
END
--EXEC spGetReportPREPassengerItinerarySubInformation 10000351


GO


