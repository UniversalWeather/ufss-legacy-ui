IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vPostFlightCrewList]'))
DROP VIEW [dbo].[vPostFlightCrewList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







  -- View Name: vPostFlightCrewList
  -- Author: PUKAL K      
  -- Create date: Jan 02 2013    
  -- Description: Get Information from PostflightMain,PostflightLeg, PostFlightCrew and Crew
  -- Revision History      
  -- Date                 Name        Ver         Change
  -- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package


CREATE VIEW [dbo].[vPostFlightCrewList] AS
(SELECT PM.POLogID
      ,PM.LogNum
      ,PM.CustomerID
      ,PM.TripID
      ,PM.POMainDescription
      ,PM.DispatchNum
      ,PM.EstDepartureDT
      ,PM.RecordType
      ,PM.FleetID
      ,PM.PassengerRequestorID PMPassengerRequestorID
      ,PM.RequestorName
      ,PM.DepartmentDescription
      ,PM.AuthorizationDescription
      ,PM.ClientID PMClientID
      ,PM.HomebaseID PMHomebaseID
--      ,PM.IsPersonal PMIsPersonal
  --    ,PM.IsException PMIsException
      ,PM.Notes PMNotes
   --   ,PM.LastUpdUID PMLastUpdUID
   --   ,PM.LastUpdTS PMLastUpdTS
   --   ,PM.FlightNum PMFlightNum
      ,PM.IsCompleted PMIsCompleted
   --   ,PM.History
    --  ,PM.TechLog PMTechLog
      ,PM.AircraftID
    --  ,PM.IsLastAdj
      ,PM.IsDeleted PMIsDeleted
   --   ,PM.AccountID PMAccountID
      ,PM.DepartmentID PMDepartmentID
      ,PM.AuthorizationID PMAuthorizationID
	  ,PL.POLegID POLegID
      ,PL.LegNUM LegNUM
	  --,PL.CustomerID
      ,PL.DepartICAOID LegDepartICAOID
      ,PL.ArriveICAOID LegArriveICAOID
      ,PL.Distance LegDistance
      ,PL.ScheduledTM  LegScheduledTM
      ,PL.BlockOut LegBlockOut
      ,PL.BlockIN LegBlockIN
      ,PL.TimeOff LegTimeOff
      ,PL.TimeOn LegTimeOn
      ,ISNULL(PL.BlockHours,0) LegBlockHours
      ,ISNULL(PL.FlightHours,0) LegFlightHours
    --  ,PL.FedAviationRegNum
    --  ,PL.ClientID PLClientID
      ,PL.FlightPurpose LegFlightPurpose
      ,PL.DutyTYPE LegDutyTYPE
      ,PL.CrewCurrency LegCrewCurrency
      ,PL.IsDutyEnd LegIsDutyEnd
      ,PL.FuelOut LegFuelOut
      ,PL.FuelIn LegFuelIn
      ,PL.FuelUsed LegFuelUsed
      ,PL.FuelBurn LegFuelBurn
      ,PL.DelayTM LegDelayTM
      ,ISNULL(PL.DutyHrs,0) LegDutyHrs
      ,ISNULL(PL.RestHrs,0) LegRestHrs
      ,PL.NextDTTM LegNextDTTM
      ,MONTH(PL.NEXTDTTM) LegNextMonth
      ,PL.DepatureFBO LegDepatureFBO
      ,PL.ArrivalFBO LegArrivalFBO
     -- ,PL.CrewID
     -- ,PL.IsException
    --  ,PL.IsAugmentCrew PLIsAugmentCrew
      ,PL.BeginningDutyHours LegBeginningDutyHours
      ,ISNULL(PL.EndDutyHours,0)	LegEndDutyHours
    --  ,PL.LastUpdUID
     -- ,PL.LastUpdTS
      ,ISNULL(PL.PassengerTotal,0) LegPassengerTotal
      ,PL.ScheduledTM LegScheduleDTTMLocal 
      
      ,PL.BlockOutLocal LegBlockOutLocal
      ,PL.BlockInLocal LegBlockInLocal
      ,PL.TimeOffLocal LegTimeOffLocal
      ,PL.TimeOnLocal LegTimeOnLocal
      ,PL.OutboundDTTM LegOutboundDTTM
      ,PL.InboundDTTM LegInboundDTTM
      ,DATEADD(mm, DATEDIFF(mm,0,ScheduledTM),0) MonthBeginDate
      ,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,ScheduledTM)+1,0)) MonthEndDate
      ,YEAR(PL.ScheduledTM) LegScheduledYear
      ,MONTH(PL.ScheduledTM) LegScheduledMonth
      ,PL.AdditionalDist LegAdditionalDist
      ,PL.AVTrak LegAVTrak
    --  ,PL.FlightNum
    --  ,PL.CargoOut
    --  ,PL.CargoIn
    --  ,PL.CargoThru
      ,PL.StatuteMiles LegStatuteMiles 
--      ,PL.FlightCost
--      ,PL.TechLog
      ,PL.AirFrameHours LegAirFrameHours
      ,PL.AirframeCycle LegAirframeCycle
      ,PL.Engine1Hours LegEngine1Hours
      ,PL.Engine2Hours LegEngine2Hours
      ,PL.Engine3Hours LegEngine3Hours
      ,PL.Engine4Hours LegEngine4Hours
      ,PL.Engine1Cycle LegEngine1Cycle
      ,PL.Engine2Cycle LegEngine2Cycle
      ,PL.Engine3Cycle LegEngine3Cycle
      ,PL.Engine4Cycle LegEngine4Cycle
      ,PL.Re1Hours LegRe1Hours
      ,PL.Re2Hours LegRe2Hours
      ,PL.Re3Hours LegRe3Hours
      ,PL.Re4Hours LegRe4Hours
      ,PL.Re1Cycle 
      ,PL.Re2Cycle
      ,PL.Re3Cycle
      ,PL.Re4Cycle
      ,PL.FuelOn
      ,PL.IsDeleted
      ,PL.AccountID
      ,PL.DelayTypeID
      ,PL.PassengerRequestorID
      ,PL.DepartmentID
      ,PL.AuthorizationID
      ,PL.FlightCategoryID
	  ,PLC.PostflightCrewListID
      ,PLC.POLogID PCrewPOLogID
      ,PLC.POLegID PCrewPOLegID
      ,PLC.CustomerID PCrewCustomerID
 --     ,PLC.CrewID PLCCrewID
 --     ,PLC.CrewFirstName PLCCrewFirstName
 --     ,PLC.CrewMiddleName PLCCrewMiddleName
 --     ,PLC.CrewLastName PLCCrewLastName
      ,PLC.DutyTYPE PCrewDutyTYPE
      ,PLC.TakeOffDay PCrewTakeOffDay
      ,PLC.TakeOffNight PCrewTakeOffNight
      ,PLC.LandingDay PCrewLandingDay
      ,PLC.LandingNight PCrewLandingNight
--      ,PLC.ApproachPrecision
--      ,PLC.ApproachNonPrecision
--      ,PLC.Instrument
      ,PLC.Night PCrewNight
      ,PLC.RemainOverNight PCrewRON
      ,CASE WHEN PLC.RemainOverNight>0 AND 
					(MONTH(PL.InboundDTTM)< MONTH(ISNULL(PL.NextDTTM,PL.InboundDTTM))  
					OR YEAR(PL.InboundDTTM) < YEAR(ISNULL(PL.NextDTTM,PL.InboundDTTM))) THEN
				DATEDIFF(dd,PL.InboundDTTM,DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,PL.InboundDTTM)+1,0))) 
       ELSE PLC.RemainOverNight
       END MonthlyRon
       ,PLC.BeginningDuty PCrewBeginningDuty
      ,PLC.DutyEnd PCrewDutyEnd
      ,PLC.DutyHours PCrewDutyHours
--      ,PLC.Specification1
--      ,PLC.Sepcification2
      ,PLC.IsOverride PCrewIsOverride
      ,PLC.Discount PCrewDiscount
      ,PLC.BlockHours PCrewBlockHours
      ,PLC.FlightHours PCrewFlightHours
      ,PLC.IsAugmentCrew PCrewIsAugmentCrew
      ,PLC.IsRemainOverNightOverride PCrew
      ,PLC.Seat PCrewSeat
  --    ,PLC.LastUpdUID CrewLastUpdUID
  --    ,PLC.LastUpdTS CrewLastUpdTS 
      ,PLC.OutboundDTTM PCrewOutboundDTTM
      ,PLC.InboundDTTM PCrewInboundDTTM
      ,PLC.CrewDutyStartTM PCrewCrewDutyStartTM
      ,PLC.CrewDutyEndTM PCrewCrewDutyEndTM
      ,PLC.ItsNew PCrewItsNew
      --,PLC.Specification3 
--      ,PLC.Specification4
      ,PLC.DaysAway PCrewDaysAway
      ,PLC.IsDeleted  PCrewIsDeleted
      ,C.CrewID
      ,C.CrewCD 
      ,ISNULL(C.FirstName,'') FirstName
      ,ISNULL(C.LastName,'')  LastName
      ,ISNULL(C.MiddleInitial,'') MiddleInitial
      ,CrewTypeCD
   FROM PostflightMain PM
   JOIN PostflightLeg PL ON PM.POLogID = PL.POLOGID AND PM.CUSTOMERID=PL.CUSTOMERID AND PL.IsDeleted =0
   JOIN PostFlightCrew PLC ON PL.POLegID=PLC.POLegID and PL.CustomerID = PLC.CustomerID AND PLC.IsDeleted=0
   JOIN Crew C ON PLC.CrewID=C.CrewID AND PLC.CustomerID=C.CustomerID AND C.IsDeleted=0
   WHERE PM.IsDeleted=0 --AND PCL.POLogID=100136
   )








GO


