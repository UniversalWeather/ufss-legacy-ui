IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor1ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor1ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPassengerRequestor1ExportInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD VARCHAR(500)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor1ExportInformation
-- Author: SUDHAKAR J
-- Create date: 28 Jun 2012
-- Description: Get Passenger/CrewPassengerVisa export Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON  
  
  
  

  
SELECT * INTO #TempPaxReq FROM (  
   SELECT  
    [paxcode] = P.PassengerRequestorCD  
    ,[paxname] = P.PassengerName  
    ,[dept_code] = D.DepartmentCD  
    --,[dept_desc] = P.PassengerDescription  
    ,[dept_desc] = D.DepartmentName  
    ,[phone] = P.AdditionalPhoneNum  
    ,[emp_t_cng] = P.IsEmployeeType  
    ,[stdbilling] = P.StandardBilling  
    ,[auth_code] = DA.AuthorizationCD  
    --,[auth_desc] = P.AuthorizationDescription  
    ,[auth_desc] = DA.DeptAuthDescription  
    ,[last_name] = P.LastName  
    ,[first_name] = P.FirstName  
    ,[middleinit] = P.MiddleInitial  
    ,[dob] = P.DateOfBirth  
    ,[client] = C.ClientCD  
    ,[assoc_with] = P.AssociatedWithCD  
    ,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)  
    ,[active] = P.IsActive  
    ,[fltpurpose] = F.FlightPurposeCD  
    ,[ssn] = P.SSN  
    ,[title] = P.Title  
    ,[salarylvl] = P.SalaryLevel  
    ,[paxtype] = P.IsPassengerType  
    ,[lastuser] = P.LastUpdUID  
    --,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
    ,[lastupdt] = P.LastUpdTS  
    ,[sscoord] = P.IsScheduledServiceCoord  
    ,[cmpyname] = P.CompanyName  
    ,[empld_id] = P.EmployeeID  
    ,[fax] = P.BusinessFax  
    ,[email] = P.EmailAddress  
    --,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))  
    ,[pin_no] = ISNULL(P.PersonalIDNum,'')  
    ,[spouse_dep] = P.IsSpouseDependant  
    ,[acctnum] = A.AccountNum  
    ,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)  
    ,[siflsec] = P.IsSIFL  
    ,[gender] = P.Gender  
    ,[addlphone] = P.AdditionalPhoneNum  
    ,[requestor] = P.IsRequestor  
    ,[ctryresid] = CN.CountryCD  
    ,[tsastat] = P.TSAStatus  
    ,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
    ,[addr1] = P.Addr1  
    ,[addr2] = P.Addr2  
    ,[city] = P.City  
    ,[state] = P.StateName  
    ,[zipcode] = P.PostalZipCD  
    ,[isdeleted] = P.IsDeleted  
    --,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming  
    ,[nation] =CN.CountryName  
    ,[adcd] = ''  
    ,[adcddesc] = ''  
    ,[adcdvalue] = ''  
    ,[ppnum] = ''  
    ,[ppexpire] = NULL  
    ,[choice] = 0  
    ,[ppnation] = ''  
    ,[issueplc] = ''  
    ,[visanum] = ''  
    ,[country] = ''  
    ,[expiredt] = NULL  
    ,[notes] = ''  
    ,[name] = CM.CompanyName  
    ,[section] = 0   
    --[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info  
    ,[paxalerts]=P.PassengerAlert--Added for Mhtml  
    ,[paxscandoc]=P.PaxScanDoc--Added for Mhtml  
    ,[paxnotes] = P.Notes  
   FROM Passenger P  
   LEFT OUTER JOIN (  
    SELECT DepartmentID, DepartmentCD, DepartmentName FROM Department  
   ) D ON P.DepartmentID = D.DepartmentID  
   LEFT OUTER JOIN (  
    SELECT AuthorizationID, AuthorizationCD,DeptAuthDescription FROM DepartmentAuthorization  
   ) DA ON P.AuthorizationID = DA.AuthorizationID  
   LEFT OUTER JOIN (  
    SELECT ClientID, ClientCD FROM Client   
   ) C ON P.ClientID = C.ClientID  
   LEFT OUTER JOIN (  
    SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose   
   ) F ON P.FlightPurposeID = F.FlightPurposeID  
   LEFT OUTER JOIN (  
    SELECT AccountID, AccountNum FROM Account  
   ) A ON P.AccountID = A.AccountID  
   LEFT OUTER JOIN (  
    SELECT CountryID, CountryCD,CountryName FROM Country  
   ) CN ON P.CountryOfResidenceID = CN.CountryID  
   LEFT OUTER JOIN (  
       SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company  
   ) CM ON P.HomebaseID = CM.HomebaseAirportID  
  WHERE P.IsDeleted = 0  
   --AND P.PassengerRequestorCD = RTRIM(@PassengerCD)  
   AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
   AND P.PassengerRequestorCD IN (  
    select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')  
   )  
  
 UNION ALL  
  
  SELECT  
   [paxcode] = P.PassengerRequestorCD  
   ,[paxname] = P.PassengerName  
   ,[dept_code] = D.DepartmentCD  
   --,[dept_desc] = P.PassengerDescription  
   ,[dept_desc] = D.DepartmentName  
   ,[phone] = P.AdditionalPhoneNum  
   ,[emp_t_cng] = P.IsEmployeeType  
   ,[stdbilling] = P.StandardBilling  
   ,[auth_code] = DA.AuthorizationCD  
   --,[auth_desc] = P.AuthorizationDescription  
   ,[auth_desc] = DA.DeptAuthDescription  
   ,[last_name] = P.LastName  
   ,[first_name] = P.FirstName  
   ,[middleinit] = P.MiddleInitial  
   ,[dob] = P.DateOfBirth  
   ,[client] = C.ClientCD  
   ,[assoc_with] = P.AssociatedWithCD  
   ,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)  
   ,[active] = P.IsActive  
   ,[fltpurpose] = F.FlightPurposeCD  
   ,[ssn] = P.SSN  
   ,[title] = P.Title  
   ,[salarylvl] = P.SalaryLevel  
   ,[paxtype] = P.IsPassengerType  
   ,[lastuser] = P.LastUpdUID  
   --,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[lastupdt] = P.LastUpdTS  
   ,[sscoord] = P.IsScheduledServiceCoord  
   ,[cmpyname] = P.CompanyName  
   ,[empld_id] = P.EmployeeID  
   ,[fax] = P.BusinessFax  
   ,[email] = P.EmailAddress  
  -- ,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))
   ,[pin_no] = ISNULL(P.PersonalIDNum,'')       
   ,[spouse_dep] = P.IsSpouseDependant  
   ,[acctnum] = A.AccountNum  
   ,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)  
   ,[siflsec] = P.IsSIFL  
   ,[gender] = P.Gender  
   ,[addlphone] = P.AdditionalPhoneNum  
   ,[requestor] = P.IsRequestor  
   ,[ctryresid] = CN.CountryCD  
   ,[tsastat] = P.TSAStatus  
   ,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[addr1] = P.Addr1  
   ,[addr2] = P.Addr2  
   ,[city] = P.City  
   ,[state] = P.StateName  
   ,[zipcode] = P.PostalZipCD  
   ,[isdeleted] = P.IsDeleted  
   --,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming  
   ,[nation] =CN.CountryName  
   ,[adcd] =  PAI.AdditionalINFOCD --Coming from PAXDATA.DBF  
   ,[adcddesc] = PAI.AdditionalINFODescription --Coming from PAXDATA.DBF  
   --,[adcdvalue] = dbo.FlightPakDecrypt(IsNull(PAI.AdditionalINFOValue,'')) --Coming from PAXDATA.DBF  
   ,[adcdvalue] = IsNull(PAI.AdditionalINFOValue,'')
   ,[ppnum] = ''  
   ,[ppexpire] = NULL  
   ,[choice] = 0  
   ,[ppnation] = ''  
   ,[issueplc] = ''  
   ,[visanum] = ''  
   ,[country] = ''  
   ,[expiredt] = NULL  
   ,[notes] = ''  
   ,[name] = CM.CompanyName  
   ,[section] = 1   
   --[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info  
   ,[paxalerts]=P.PassengerAlert--Added for Mhtml  
   ,[paxscandoc]=P.PaxScanDoc--Added for Mhtml  
   ,[paxnotes] = P.Notes  
  FROM Passenger P  
   INNER JOIN PassengerAdditionalInfo PAI  
    ON P.CustomerID = PAI.CustomerID  
    AND P.PassengerRequestorID = PAI.PassengerRequestorID  
   LEFT OUTER JOIN (  
    SELECT DepartmentID, DepartmentCD, DepartmentName FROM Department  
   ) D ON P.DepartmentID = D.DepartmentID  
   LEFT OUTER JOIN (  
    SELECT AuthorizationID, AuthorizationCD,DeptAuthDescription FROM DepartmentAuthorization  
   ) DA ON P.AuthorizationID = DA.AuthorizationID  
   LEFT OUTER JOIN (  
    SELECT ClientID, ClientCD FROM Client   
   ) C ON P.ClientID = C.ClientID  
   LEFT OUTER JOIN (  
    SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose   
   ) F ON P.FlightPurposeID = F.FlightPurposeID  
   LEFT OUTER JOIN (  
    SELECT AccountID, AccountNum FROM Account  
   ) A ON P.AccountID = A.AccountID  
   LEFT OUTER JOIN (  
    SELECT CountryID, CountryCD,CountryName FROM Country  
   ) CN ON P.CountryOfResidenceID = CN.CountryID  
   LEFT OUTER JOIN (  
       SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company  
   ) CM ON P.HomebaseID = CM.HomebaseAirportID  
  WHERE PAI.IsDeleted = 0  
   --AND P.PassengerRequestorCD = RTRIM(@PassengerCD)  
   AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
   AND P.PassengerRequestorCD IN (  
    select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')  
   )  
     
 UNION ALL  
  
  SELECT  
   [paxcode] = P.PassengerRequestorCD  
   ,[paxname] = P.PassengerName  
   ,[dept_code] = D.DepartmentCD  
   --,[dept_desc] = P.PassengerDescription  
   ,[dept_desc] = D.DepartmentName  
   ,[phone] = P.AdditionalPhoneNum  
   ,[emp_t_cng] = P.IsEmployeeType  
   ,[stdbilling] = P.StandardBilling  
   ,[auth_code] = DA.AuthorizationCD  
   --,[auth_desc] = P.AuthorizationDescription  
   ,[auth_desc] = DA.DeptAuthDescription  
   ,[last_name] = P.LastName  
   ,[first_name] = P.FirstName  
   ,[middleinit] = P.MiddleInitial  
   ,[dob] = P.DateOfBirth  
   ,[client] = C.ClientCD  
   ,[assoc_with] = P.AssociatedWithCD  
   ,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)  
   ,[active] = P.IsActive  
   ,[fltpurpose] = F.FlightPurposeCD  
   ,[ssn] = P.SSN  
   ,[title] = P.Title  
   ,[salarylvl] = P.SalaryLevel  
   ,[paxtype] = P.IsPassengerType  
   ,[lastuser] = P.LastUpdUID  
   --,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[lastupdt] = P.LastUpdTS  
   ,[sscoord] = P.IsScheduledServiceCoord  
   ,[cmpyname] = P.CompanyName  
   ,[empld_id] = P.EmployeeID  
   ,[fax] = P.BusinessFax  
   ,[email] = P.EmailAddress  
   --,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))  
   ,[pin_no] = ISNULL(P.PersonalIDNum,'')  
   ,[spouse_dep] = P.IsSpouseDependant  
   ,[acctnum] = A.AccountNum  
   ,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)  
   ,[siflsec] = P.IsSIFL  
   ,[gender] = P.Gender  
   ,[addlphone] = P.AdditionalPhoneNum  
   ,[requestor] = P.IsRequestor  
   ,[ctryresid] = CN.CountryCD  
   ,[tsastat] = P.TSAStatus  
   ,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[addr1] = P.Addr1  
   ,[addr2] = P.Addr2  
   ,[city] = P.City  
   ,[state] = P.StateName  
   ,[zipcode] = P.PostalZipCD  
   ,[isdeleted] = P.IsDeleted  
   --,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming  
   ,[nation] =CN.CountryName  
   ,[adcd] = ''  
   ,[adcddesc] = ''  
   ,[adcdvalue] = ''  
   --,[ppnum] = dbo.FlightPakDecrypt(IsNull(CPP.PassportNUM,'')) --Coming from PAXPASS.DBF  
   ,[ppnum] = IsNull(CPP.PassportNUM,'') --Coming from PAXPASS.DBF  
   --,[ppexpire] = CONVERT(varchar(10), CPP.PassportExpiryDT, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) --Coming from PAXPASS.DBF  
   ,[ppexpire] = CPP.PassportExpiryDT  
   ,[choice] = CPP.Choice --Coming from PAXPASS.DBF  
   ,[ppnation] = dbo.GetCountryCDByCountryID(CPP.CountryID)  
   ,[issueplc] = CPP.IssueCity --Coming from PAXPASS.DBF  
   ,[visanum] = ''  
   ,[country] = ''  
   ,[expiredt] = NULL  
   ,[notes] = ''  
   ,[name] = CM.CompanyName  
   ,[section] = 2   
   --[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info  
   ,[paxalerts]=P.PassengerAlert--Added for Mhtml  
   ,[paxscandoc]=P.PaxScanDoc--Added for Mhtml  
   ,[paxnotes] = P.Notes  
  FROM Passenger P  
   INNER JOIN CrewPassengerPassport CPP  
     ON P.CustomerID = CPP.CustomerID  
     AND P.PassengerRequestoriD = CPP.PassengerRequestoriD  
   LEFT OUTER JOIN (  
    SELECT DepartmentID, DepartmentCD, DepartmentName FROM Department  
   ) D ON P.DepartmentID = D.DepartmentID  
   LEFT OUTER JOIN (  
    SELECT AuthorizationID, AuthorizationCD,DeptAuthDescription FROM DepartmentAuthorization  
   ) DA ON P.AuthorizationID = DA.AuthorizationID  
   LEFT OUTER JOIN (  
    SELECT ClientID, ClientCD FROM Client   
   ) C ON P.ClientID = C.ClientID  
   LEFT OUTER JOIN (  
    SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose   
   ) F ON P.FlightPurposeID = F.FlightPurposeID  
   LEFT OUTER JOIN (  
    SELECT AccountID, AccountNum FROM Account  
   ) A ON P.AccountID = A.AccountID  
   LEFT OUTER JOIN (  
    SELECT CountryID, CountryCD, CountryName FROM Country  
   ) CN ON P.CountryOfResidenceID = CN.CountryID  
   LEFT OUTER JOIN (  
       SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company  
   ) CM ON P.HomebaseID = CM.HomebaseAirportID  
  WHERE CPP.IsDeleted = 0  
   --AND P.PassengerRequestorCD = RTRIM(@PassengerCD)  
   AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
   AND P.PassengerRequestorCD IN (  
    select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')  
   )  
     
 UNION ALL  
  
   SELECT  
    [paxcode] = P.PassengerRequestorCD  
    ,[paxname] = P.PassengerName  
    ,[dept_code] = D.DepartmentCD  
    --,[dept_desc] = P.PassengerDescription  
    ,[dept_desc] = D.DepartmentName  
    ,[phone] = P.AdditionalPhoneNum  
    ,[emp_t_cng] = P.IsEmployeeType  
    ,[stdbilling] = P.StandardBilling  
    ,[auth_code] = DA.AuthorizationCD  
    --,[auth_desc] = P.AuthorizationDescription  
    ,[auth_desc] = DA.DeptAuthDescription  
    ,[last_name] = P.LastName  
    ,[first_name] = P.FirstName  
    ,[middleinit] = P.MiddleInitial  
    ,[dob] = P.DateOfBirth  
    ,[client] = C.ClientCD  
    ,[assoc_with] = P.AssociatedWithCD  
    ,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)  
    ,[active] = P.IsActive  
    ,[fltpurpose] = F.FlightPurposeCD  
    ,[ssn] = P.SSN  
    ,[title] = P.Title  
    ,[salarylvl] = P.SalaryLevel  
    ,[paxtype] = P.IsPassengerType  
    ,[lastuser] = P.LastUpdUID  
    --,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
    ,[lastupdt] = P.LastUpdTS  
    ,[sscoord] = P.IsScheduledServiceCoord  
    ,[cmpyname] = P.CompanyName  
    ,[empld_id] = P.EmployeeID  
    ,[fax] = P.BusinessFax  
    ,[email] = P.EmailAddress  
    --,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))  
    ,[pin_no] = ISNULL(P.PersonalIDNum,'')  
    ,[spouse_dep] = P.IsSpouseDependant  
    ,[acctnum] = A.AccountNum  
    ,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)  
    ,[siflsec] = P.IsSIFL  
    ,[gender] = P.Gender  
    ,[addlphone] = P.AdditionalPhoneNum  
    ,[requestor] = P.IsRequestor  
    ,[ctryresid] = CN.CountryCD  
    ,[tsastat] = P.TSAStatus  
    ,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
    ,[addr1] = P.Addr1  
    ,[addr2] = P.Addr2  
    ,[city] = P.City  
    ,[state] = P.StateName  
    ,[zipcode] = P.PostalZipCD  
    ,[isdeleted] = P.IsDeleted  
    --,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming  
    ,[nation] =CN.CountryName  
    ,[adcd] = ''  
    ,[adcddesc] = ''  
    ,[adcdvalue] = ''  
    ,[ppnum] = ''  
    ,[ppexpire] = NULL  
    ,[choice] = 0  
    ,[ppnation] = ''  
    ,[issueplc] = ''  
    ,[visanum] = ''  
    ,[country] = ''  
    ,[expiredt] = NULL  
    ,[notes] = ''  
    ,[name] = CM.CompanyName  
    ,[section] = 3   
    --[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info  
    ,[paxalerts]=P.PassengerAlert--Added for Mhtml  
    ,[paxscandoc]=P.PaxScanDoc--Added for Mhtml  
    ,[paxnotes] = P.Notes  
   FROM Passenger P  
   LEFT OUTER JOIN (  
    SELECT DepartmentID, DepartmentCD, DepartmentName FROM Department  
   ) D ON P.DepartmentID = D.DepartmentID  
   LEFT OUTER JOIN (  
    SELECT AuthorizationID, AuthorizationCD,DeptAuthDescription FROM DepartmentAuthorization  
   ) DA ON P.AuthorizationID = DA.AuthorizationID  
   LEFT OUTER JOIN (  
    SELECT ClientID, ClientCD FROM Client   
   ) C ON P.ClientID = C.ClientID  
   LEFT OUTER JOIN (  
    SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose   
   ) F ON P.FlightPurposeID = F.FlightPurposeID  
   LEFT OUTER JOIN (  
    SELECT AccountID, AccountNum FROM Account  
   ) A ON P.AccountID = A.AccountID  
   LEFT OUTER JOIN (  
    SELECT CountryID, CountryCD,CountryName FROM Country  
   ) CN ON P.CountryOfResidenceID = CN.CountryID  
   LEFT OUTER JOIN (  
      SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company  
   ) CM ON P.HomebaseID = CM.HomebaseAirportID  
  WHERE P.IsDeleted = 0  
   --AND P.PassengerRequestorCD = RTRIM(@PassengerCD)  
   AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
   AND P.PassengerRequestorCD IN (  
    select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')  
   )  
     
 UNION ALL  
  
  SELECT  
   [paxcode] = P.PassengerRequestorCD  
   ,[paxname] = P.PassengerName  
   ,[dept_code] = D.DepartmentCD  
   --,[dept_desc] = P.PassengerDescription  
   ,[dept_desc] = D.DepartmentName  
   ,[phone] = P.AdditionalPhoneNum  
   ,[emp_t_cng] = P.IsEmployeeType  
   ,[stdbilling] = P.StandardBilling  
   ,[auth_code] = DA.AuthorizationCD  
   --,[auth_desc] = P.AuthorizationDescription  
   ,[auth_desc] = DA.DeptAuthDescription  
   ,[last_name] = P.LastName  
   ,[first_name] = P.FirstName  
   ,[middleinit] = P.MiddleInitial  
   ,[dob] = P.DateOfBirth  
   ,[client] = C.ClientCD  
   ,[assoc_with] = P.AssociatedWithCD  
   ,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)  
   ,[active] = P.IsActive  
   ,[fltpurpose] = F.FlightPurposeCD  
   ,[ssn] = P.SSN  
   ,[title] = P.Title  
   ,[salarylvl] = P.SalaryLevel  
   ,[paxtype] = P.IsPassengerType  
   ,[lastuser] = P.LastUpdUID  
   --,[lastupdt] = CONVERT(varchar(10), P.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[lastupdt] = P.LastUpdTS  
   ,[sscoord] = P.IsScheduledServiceCoord  
   ,[cmpyname] = P.CompanyName  
   ,[empld_id] = P.EmployeeID  
   ,[fax] = P.BusinessFax  
   ,[email] = P.EmailAddress  
   --,[pin_no] = dbo.FlightPakDecrypt(ISNULL(P.PersonalIDNum,''))  
   ,[pin_no] = ISNULL(P.PersonalIDNum,'')  
   ,[spouse_dep] = P.IsSpouseDependant  
   ,[acctnum] = A.AccountNum  
   ,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)  
   ,[siflsec] = P.IsSIFL  
   ,[gender] = P.Gender  
   ,[addlphone] = P.AdditionalPhoneNum  
   ,[requestor] = P.IsRequestor  
   ,[ctryresid] = CN.CountryCD  
   ,[tsastat] = P.TSAStatus  
   ,[tsadttm] = CONVERT(varchar(10), P.TSADTTM, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
   ,[addr1] = P.Addr1  
   ,[addr2] = P.Addr2  
   ,[city] = P.City  
   ,[state] = P.StateName  
   ,[zipcode] = P.PostalZipCD  
   ,[isdeleted] = P.IsDeleted  
   --,[nation] = dbo.GetCountryCDByCountryID(P.CountryID) --Not sure from which table it is coming  
   ,[nation] =CN.CountryName  
   ,[adcd] = ''  
   ,[adcddesc] = ''  
   ,[adcdvalue] = ''  
   ,[ppnum] = ''  
   ,[ppexpire] = NULL  
   ,[choice] = 0  
   ,[ppnation] = ''  
   ,[issueplc] = ''  
   --,[visanum] = dbo.FlightPakDecrypt(IsNull(CPV.VisaNum,''))  
   ,[visanum] = IsNull(CPV.VisaNum,'')  
   ,[country] = dbo.GetCountryCDByCountryID(CPV.CountryID)  
   ,[expiredt] = CPV.ExpiryDT  
   ,[notes] = CPV.Notes  
   ,[name] = CM.CompanyName  
   ,[section] = 4   
   --[section] 0 = Passenger, 1 = Pax Additioanl Info, 2 = Passport Info, 3 = 'Unknown', 4 = Visa Info  
   ,[paxalerts]=P.PassengerAlert--Added for Mhtml  
   ,[paxscandoc]=P.PaxScanDoc--Added for Mhtml  
   ,[paxnotes] = P.Notes  
  FROM Passenger P  
   INNER JOIN CrewPassengerVisa CPV  
    ON P.CustomerID = CPV.CustomerID  
    AND P.PassengerRequestorID = CPV.PassengerRequestorID  
   LEFT OUTER JOIN (  
    SELECT DepartmentID, DepartmentCD ,DepartmentName FROM Department  
   ) D ON P.DepartmentID = D.DepartmentID  
   LEFT OUTER JOIN (  
    SELECT AuthorizationID, AuthorizationCD,DeptAuthDescription FROM DepartmentAuthorization  
   ) DA ON P.AuthorizationID = DA.AuthorizationID  
   LEFT OUTER JOIN (  
    SELECT ClientID, ClientCD FROM Client   
   ) C ON P.ClientID = C.ClientID  
   LEFT OUTER JOIN (  
    SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose   
   ) F ON P.FlightPurposeID = F.FlightPurposeID  
   LEFT OUTER JOIN (  
    SELECT AccountID, AccountNum FROM Account  
   ) A ON P.AccountID = A.AccountID  
   LEFT OUTER JOIN (  
    SELECT CountryID, CountryCD, CountryName FROM Country  
   ) CN ON P.CountryOfResidenceID = CN.CountryID  
   LEFT OUTER JOIN (  
       SELECT HomebaseID,CompanyName,HomebaseAirportID FROM Company  
   ) CM ON P.HomebaseID = CM.HomebaseAirportID  
  WHERE CPV.IsDeleted = 0  
   --AND P.PassengerRequestorCD = RTRIM(@PassengerCD)  
   AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  
   AND P.PassengerRequestorCD IN (  
    select DISTINCT RTRIM(s) from dbo.SplitString(RTRIM(@PassengerCD), ',')  
   )  
 ) A --INTO #TempPaxReq  
  
  
  
 
-- INSERT 1 DUMMY RECORD FOR EACH SECTION IF THERE IS NO ROW  
 
  IF NOT EXISTS(SELECT SECTION FROM #TempPaxReq WHERE SECTION=1)
 
 BEGIN
INSERT INTO #TempPaxReq (  
 paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,section,paxalerts,paxscandoc,paxnotes  
 )   
SELECT paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,1 Section,paxalerts,paxscandoc,paxnotes 
 FROM #TempPaxReq WHERE SECTION = 0  
END
 
  IF NOT EXISTS(SELECT SECTION FROM #TempPaxReq WHERE SECTION=2)
 
 BEGIN
INSERT INTO #TempPaxReq (  
 paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,section,paxalerts,paxscandoc,paxnotes  
 )   
SELECT paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,2 Section,paxalerts,paxscandoc,paxnotes 
 FROM #TempPaxReq WHERE SECTION = 0  
END

 IF NOT EXISTS(SELECT SECTION FROM #TempPaxReq WHERE SECTION=3)
 
 BEGIN
   
INSERT INTO #TempPaxReq (  
 paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,section,paxalerts,paxscandoc,paxnotes  
 )  
 SELECT paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,3,paxalerts,paxscandoc,paxnotes  
 FROM #TempPaxReq WHERE SECTION = 0  
 END
 
  IF NOT EXISTS(SELECT SECTION FROM #TempPaxReq WHERE SECTION=4)
 
 BEGIN 
   
INSERT INTO #TempPaxReq (  
 paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,section,paxalerts,paxscandoc,paxnotes  
 )  
 SELECT paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
 ,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
 ,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
 ,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
 ,name,4,paxalerts,paxscandoc,paxnotes  
 FROM #TempPaxReq WHERE SECTION = 0  
---TO CHECK  
  END

  
  
  
---END TO CHECK  
  
  
SELECT DISTINCT * FROM #TempPaxReq ORDER BY PAXCODE, section 

  IF OBJECT_ID('TEMPDB..#TempPaxReq') IS NOT NULL DROP TABLE #TempPaxReq 
 
-- EXEC spGetReportPassengerRequestor1ExportInformation 'SUPERVISOR_99', 'EXEC2,EXEC1'  
-- EXEC spGetReportPassengerRequestor1ExportInformation 'SUPERVISOR_99', 'EXEC2'  
  
/*  
paxcode,paxname,dept_code,dept_desc,phone,emp_t_cng,stdbilling,auth_code,auth_desc,last_name,first_name,middleinit,dob,client  
,assoc_with,homebase,active,fltpurpose,ssn,title,salarylvl,paxtype,lastuser,lastupdt,sscoord,cmpyname,empld_id,fax,email  
,pin_no,spouse_dep,acctnum,nationcode,siflsec,gender,addlphone,requestor,ctryresid,tsastat,tsadttm,addr1,addr2,city,state  
,zipcode,isdeleted,nation,adcd,adcddesc,adcdvalue,ppnum,ppexpire,choice,ppnation,issueplc,visanum,country,expiredt,notes  
,name,section,paxalerts,paxscandoc  
*/

GO


