IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPAXDocumentsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPAXDocumentsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSPAXDocumentsInformation]
	@UserCD VARCHAR(30),
	@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
	@LegNum AS VARCHAR(30) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSCrewInformation
-- Author: AISHWARYA.M
-- Create date: 30 Jul 2012
-- Description: Get Preflight TripSheet Crew information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON 	
SELECT --LegID = CONVERT(VARCHAR,PL.LegID),
		PaxDocs = F.UWAFilePath
		,DocName = F.UWAFileName
		,PAXNames = ISNULL(P.LastName,'') + ', ' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
		FROM 
		( 
			SELECT DISTINCT PP.PassengerID FROM PreflightLeg PL
			INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
			INNER JOIN FlightPurpose FP ON PP.FlightPurposeID=FP.FlightPurposeID
			WHERE PL.TripID = CONVERT(BIGINT,@TripID)
			AND PL.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
		) PL
		INNER JOIN Passenger P ON PL.PassengerID = P.PassengerRequestorID
		INNER JOIN FileWarehouse F ON F.RecordID = PL.PassengerID 
		WHERE F.UWAWebpageName = 'PassengerCatalog.aspx' 
		  AND F.IsDeleted=0  
END

--spGetReportPRETSPAXDocumentsInformation 'supervisor_99', '10099123637',''




GO


