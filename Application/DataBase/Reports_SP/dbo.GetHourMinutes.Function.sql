 
/****** Object:  UserDefinedFunction [dbo].[GetHourMinutes]    Script Date: 11/29/2012 19:10:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetHourMinutes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetHourMinutes]
GO
 
/****** Object:  UserDefinedFunction [dbo].[GetHourMinutes]    Script Date: 11/29/2012 19:10:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetHourMinutes](@DecimalVal DECIMAL(10,2))
RETURNS VARCHAR(10)
--Declare @Hour decimal(10,2)
--SET @Hour = 4.2
--SELECT [dbo].[GetHourMinutes] (11.6)
AS BEGIN
DECLARE @time VARCHAR(10)
SET @time=(SELECT CONVERT(VARCHAR, CONVERT(INT,FLOOR(@DecimalVal)))+':'+
REPLICATE('0',(2 - LEN(CONVERT (VARCHAR, CONVERT(INT,(@DecimalVal - FLOOR(@DecimalVal)) * 60.0)))))
+CONVERT (VARCHAR, CONVERT(INT,(@DecimalVal - FLOOR(@DecimalVal)) * 60.0)))
RETURN @time
END
GO


