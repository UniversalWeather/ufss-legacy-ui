IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftArrivalExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftArrivalExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPOSTAircraftArrivalExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@IcaoID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@RequestorCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
				
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTAircraftArrivalExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight Aircraft Arrival for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON

			SELECT		
			[arricao_id]=AA.IcaoID,
			AA.ArriveICAOID AS [icao_count],
			[city] = AA.CityName,
			[state] = AA.StateName,
			[ctrydesc] = AA.CountryName,
			[airport_nm] = AA.AirportName,
			pax_name = P.PassengerName,
			paxcount = CASE WHEN P.PassengerRequestorCD IS NOT NULL THEN COUNT(P.PassengerRequestorCD) ELSE NULL END
			
     	FROM PostflightMain PM 
			INNER JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted=0
			LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = PL.POLegID
			LEFT OUTER JOIN(SELECT COUNT(PL.ArriveICAOID) ArriveICAOID,AirportID,IcaoID,CityName,StateName,CountryName,AirportName FROM Airport A INNER JOIN PostflightLeg PL ON A.AirportID=PL.ArriveICAOID AND PL.IsDeleted=0
	                       WHERE  PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	                       AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	                                  GROUP BY AirportID,IcaoID,CityName,StateName,CountryName,AirportName) AA ON AA.AirportID = PL.ArriveICAOID
			LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
			LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = PL.PassengerRequestorID --AND PP.IsRequestor=1
WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
    AND PM.IsDeleted=0
	AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
	AND (PP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
	AND (AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '')
GROUP BY  AA.IcaoID, AA.CityName, AA.StateName, AA.CountryName, AA.AirportName, P.PassengerRequestorCD, P.PassengerName, P.IsRequestor,AA.ArriveICAOID
ORDER BY [icao_count] DESC
		
		--INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID
		--LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = PL.POLegID
		--LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
		--INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
		--LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = PL.PassengerRequestorID 	    
		--WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		--AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
		--AND (PP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
		--AND (AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '')
		
		--GROUP BY  AA.IcaoID, AA.CityName, AA.StateName, AA.CountryName, AA.AirportName
		--ORDER BY arricao_id desc,ctrydesc,[state] desc,city
 
  
   -- EXEC spGetReportPOSTAircraftArrivalExportInformation 'jwilliams_13','2000/8/21','2012/8/22',''



GO


