IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWItineraryLegPaxInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWItineraryLegPaxInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportCQRWItineraryLegPaxInformation]
	@CQLegID VARCHAR(30) 
	,@PONEPERLIN bit = 0 -- 1 --> Nbr/PaxName, 0 --> All pax names with space delimiter 
AS

BEGIN
-- ================================================================================
-- SPC Name: spGetReportCQRWItineraryLegPaxInformation
-- Author: AISHWARYA.M
-- Create date: 30 Aug 2012
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- =================================================================================

SET NOCOUNT ON

---SELECT @PONEPERLIN
    DECLARE @tblTripInfo AS TABLE (  
		RowID INT IDENTITY(1,1)
		, PassengerName VARCHAR(MAX) 
		, PassengerCD VARCHAR(10) 
		, RowNum INT
    ) 
 
	IF @PONEPERLIN = 1 BEGIN   
		INSERT INTO @tblTripInfo (PassengerName, PassengerCD)
		SELECT [PassengerName] = CASE WHEN P.LastName <>'' THEN  ISNULL(P.LastName,'') + ','  ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
				, P.PassengerRequestorCD
			   FROM CQLeg QL 
			    INNER JOIN  CQPassenger CL ON QL.CQLegID=CL.CQLegID 
				INNER JOIN Passenger P ON CL.PassengerRequestorID = P.PassengerRequestorID
				WHERE CL.CQLegID = CONVERT(BIGINT,@CQLegID)
				  AND QL.IsDeleted=0
				ORDER BY P.PassengerRequestorCD
	END ELSE BEGIN
		INSERT INTO @tblTripInfo (PassengerName) 
			
				
	SELECT DISTINCT Paxnames = ( 
								SELECT  CASE WHEN P.LastName <>'' THEN  ISNULL(P.LastName,'') + ','  ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') + '          '
												FROM CQLeg QL 
											    INNER JOIN CQPassenger CL ON QL.CQLegID=CL.CQLegID
												INNER JOIN Passenger P ON CL.PassengerRequestorID = P.PassengerRequestorID
												WHERE CL.CQLegID = CONVERT(BIGINT,@CQLegID)
												  AND QL.IsDeleted=0
												ORDER BY P.PassengerRequestorCD
												FOR XML PATH('')
                                )
                                
			
/*			SELECT Paxnames FROM (
				SELECT DISTINCT Paxnames = ( 
					SELECT [PassengerName] = ISNULL(P.LastName,'') + ',' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') + '$$$'
					FROM CQPassenger CL 
					INNER JOIN Passenger P ON CL.PassengerRequestorID = P.PassengerRequestorID
					WHERE CL.CQLegID = CONVERT(BIGINT,@CQLegID)
					AND CL.CQLegID = CL2.CQLegID
					ORDER BY P.PassengerRequestorCD
					FOR XML PATH('')
				)
				FROM  CQPassenger CL2 
				WHERE CL2.CQLegID=@CQLegID
			) X WHERE Paxnames IS NOT NULL  */
	END

	IF @PONEPERLIN =1 BEGIN   
		UPDATE @tblTripInfo SET RowNum = RowID
	END ELSE BEGIN
		UPDATE @tblTripInfo SET RowNum = NULL
	END
	 
	 
	IF @PONEPERLIN=0
	
	BEGIN
	SELECT RIGHT(RTRIM(CONVERT(CHAR(5),'000'+CONVERT(CHAR(3),RowNum))),3) RowNum, PassengerName FROM @tblTripInfo ORDER BY PassengerCD
	END 

	ELSE 

	BEGIN

	   IF EXISTS(SELECT PassengerName FROM @tblTripInfo)
			
			 BEGIN
			 SELECT RIGHT(RTRIM(CONVERT(CHAR(5),'000'+CONVERT(CHAR(3),RowNum))),3) RowNum, PassengerName FROM @tblTripInfo ORDER BY PassengerCD
			 END 
	   ELSE 
			 BEGIN
			 SELECT NULL RowNum,'NO PASSENGERS' PassengerName
			 END
	     
	   
	 END


END 

	

                                
--EXEC spGetReportCQRWItineraryLegPaxInformation  '10099108912',1











GO


