IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTRequestorChargeBackDetailByDepartmentInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTRequestorChargeBackDetailByDepartmentInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTRequestorChargeBackDetailByDepartmentInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),
	@YEAR AS INT, --Mandatory
	@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@IsHomebase BIT = 0
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTRequestorChargeBackDetailByDepartmentInformation
-- Author:  A.Akhila
-- Create date: 1 Jul 2012
-- Description: Get Requestor Charge Back Detail By Department for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
Declare @SuppressActivityDept BIT  
	
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
DECLARE @TenToMin SMALLINT = 0;
DECLARE @AircraftBasis SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin,@AircraftBasis = AircraftBasis FROM Company 
																	WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
																	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
				
DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID

DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 


-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempFleetID TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT,
	TailNum VARCHAR(10)
	)

IF @TailNUM <> ''
	BEGIN
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum 
		FROM Fleet F
		WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
	END
IF @FleetGroupCD <> ''
	BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum
		FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	END
ELSE IF @TailNUM = '' AND @FleetGroupCD = ''
	BEGIN 
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum
	FROM Fleet F 
	WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	END

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TEMPTODAY DATETIME=@FromDate,
        @DATETO DATETIME= @ToDate;
DECLARE @Month TAble(MonthNo INT,Year INT);

WITH dates(DATEPARAM)
           AS (SELECT @TEMPTODAY AS DATETIME
               UNION ALL
               SELECT Dateadd(DAY, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO @Month (MonthNo,Year)
      SELECT DISTINCT MONTH(DATEPARAM),YEAR(DATEPARAM)
      FROM   Dates
      OPTION (maxrecursion 10000)

DECLARE @DeptRequestor TABLE(DATE INT
                            ,SCHYEAR INT
                            ,Requestor VARCHAR(63)
                            ,DepartmentID BIGINT
                            ,DepartmentName VARCHAR(40)
                            ,BlockHours NUMERIC(12,3)
                            ,ChargeBack NUMERIC(20,2)
                            ,DepartmentCD VARCHAR(8)
                            ,Category INT
                            ,LogNum BIGINT
                            ,LegNum BIGINT
                            )


INSERT INTO @DeptRequestor
SELECT DISTINCT 
	[DATE] = MONTH(POL.ScheduledTM)
	,SCHYEAR = YEAR(POL.ScheduledTM)
	,[Requestor] = ISNULL(P.PassengerName,'')
	,DepartmentID=D.DepartmentID
	,[DepartmentName] = ISNULL(CONVERT(VARCHAR(20),D.DepartmentCD)+'-','-UNALLOCATED DEPT')+ISNULL(D.DepartmentName,'')
	,[BlockHours] = CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END
	,[ChargeBack] = CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							(CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END,0)) 
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END)
						)ELSE
						NULL
						END
	,ISNULL(D.DepartmentCD,'Z')
	,CASE WHEN P.PassengerName IS NULL THEN 1 ELSE 3 END Category
	,POM.LogNum
	,POL.LegNUM
FROM PostflightMain POM
	INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted = 0
	INNER JOIN Fleet F ON F.FleetID = POM.FleetID
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
	LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POL.PassengerRequestorID
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
	LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate,BeginRateDT,EndRateDT
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
			AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) FCR ON POL.POLegID = FCR.POLegID  	
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND CONVERT(DATE, POL.ScheduledTM) BETWEEN   CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)
  AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
  AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
  AND POM.IsDeleted=0


DECLARE  @Count INT


IF @DepartmentCD='' AND @SuppressActivityDept=0
BEGIN
	INSERT INTO @DeptRequestor
	SELECT DISTINCT DD.MonthNo
				   ,DD.Year
				   ,'NO REQUESTOR ASSIGNED'
				   ,[DepartmentID] = D.DepartmentID
				   ,[Department] = D.DepartmentCD+'-'+ISNULL(D.DepartmentName,'')
				   ,NULL
				   ,0
				   ,D.DepartmentCD
				   ,2
				   ,NULL
				   ,NULL
	FROM Department D CROSS JOIN (SELECT MonthNo,YEAR FROM @Month M 
													   )DD--WHERE NOT EXISTS (SELECT DISTINCT DATE,SCHYEAR FROM @DeptRequestor DR WHERE DR.DATE=M.MonthNo AND DR.SCHYEAR=M.Year)
	WHERE D.IsDeleted=0
	 AND D.IsInActive=0 
	 AND D.CustomerID = CONVERT(BIGINT,@UserCustomerID)
	 AND D.DepartmentID NOT IN(SELECT DISTINCT ISNULL(DepartmentID,0) FROM @DeptRequestor)
	 
SELECT @Count=COUNT(*) FROM 
		 (SELECT DISTINCT Requestor,DepartmentCD FROM @DeptRequestor)TEMP
	
	SELECT DISTINCT DATE
				   ,SCHYEAR
				   ,Requestor 
				   ,DepartmentID 
				   ,DepartmentName 
				   ,BlockHours BlockHours
				   ,FLOOR(ISNULL(ChargeBack,0)*10)*0.1 ChargeBack 
				   ,DepartmentCD 
				   ,Category
				   ,@TenToMin TenToMin
				   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthStart,0 )-1),3) MonthStart
				   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthEnd,0 )-1),3) MonthEnd 
				   ,@YEAR FiscalYearStart
				   ,CASE WHEN @MonthStart >1 THEN @YEAR+1 ELSE @YEAR END  FiscalYearEnd  
				   ,CASE WHEN @AircraftBasis=1 THEN 'Block Hours:' ELSE 'Flight Hours:' END BlkFltLabel
		     ,CountVal=@Count
		     ,LogNum
		     ,LegNum
		  FROM @DeptRequestor 
	ORDER BY SCHYEAR,DATE,Category,Requestor,DepartmentCD--,DATE
END

ELSE

BEGIN
SELECT @Count=COUNT(*) FROM 
		 (SELECT DISTINCT Requestor,DepartmentCD FROM @DeptRequestor)TEMP

SELECT DISTINCT DATE
               ,SCHYEAR
               ,Requestor 
               ,DepartmentID 
               ,DepartmentName 
               ,BlockHours BlockHours
			   ,FLOOR(ISNULL(ChargeBack,0)*10)*0.1 ChargeBack 
               ,DepartmentCD 
               ,Category
               ,@TenToMin TenToMin
			   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthStart,0 )-1),3) MonthStart
			   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthEnd,0 )-1),3) MonthEnd 
			   ,@YEAR FiscalYearStart
			   ,CASE WHEN @MonthStart >1 THEN @YEAR+1 ELSE @YEAR END  FiscalYearEnd 
			   ,CASE WHEN @AircraftBasis=1 THEN 'Block Hours:' ELSE 'Flight Hours:' END BlkFltLabel
			   ,CountVal=@Count
			   ,LogNum
			   ,LegNum
		  FROM @DeptRequestor 
UNION ALL

SELECT DISTINCT DD.MonthNo
               ,DD.Year
               ,Requestor 
               ,DepartmentID 
               ,DepartmentName 
               ,NULL BlockHours
			   ,FLOOR(ISNULL(0,0)*10)*0.1 ChargeBack 
               ,DepartmentCD 
               ,Category
               ,@TenToMin TenToMin
			   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthStart,0 )-1),3) MonthStart
			   ,LEFT(DateName(MONTH,DateAdd( MONTH,@MonthEnd,0 )-1),3) MonthEnd 
			   ,@YEAR FiscalYearStart
			   ,CASE WHEN @MonthStart >1 THEN @YEAR+1 ELSE @YEAR END  FiscalYearEnd 
			   ,CASE WHEN @AircraftBasis=1 THEN 'Block Hours:' ELSE 'Flight Hours:' END BlkFltLabel
			   ,CountVal=@Count
			   ,LogNum
			   ,LegNum
		  FROM @DeptRequestor CROSS JOIN (SELECT MonthNo,YEAR FROM @Month M WHERE NOT EXISTS (SELECT DISTINCT DATE,SCHYEAR FROM @DeptRequestor DR WHERE DR.DATE=M.MonthNo AND DR.SCHYEAR=M.Year)
													   )DD 
END


         


--SELECT * FROM #REQUESTOR  WHERE (BlockHours<>0 OR ChargeBack<>0)

--EXEC spGetReportPOSTRequestorChargeBackDetailByDepartmentInformation 'SUPERVISOR_99', '10099', '2009', '', '', '', 0,''









GO


