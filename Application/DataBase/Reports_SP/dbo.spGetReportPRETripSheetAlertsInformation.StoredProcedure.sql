IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripSheetAlertsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripSheetAlertsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETripSheetAlertsInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory		
		@TripNum AS NVARCHAR(500) = '', -- [Optional INTEGER], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS


-- ===============================================================================
-- SPC Name: spGetReportPRETripSheetAlertsInformation
-- Author: Abhishek S
-- Create date: 16 Jul 2012
-- Description: Get TripSheet Alerts information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(500)

SET @SQLSCRIPT = 'SELECT DISTINCT

 [FileNumber] = PM.TripNUM
,[TailNumber] = F.TailNum
,[DepDate] = [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATEFROM) + ''-'' + [dbo].[GetShortDateFormatByUserCD](@UserCD,@DATETO)
,[TripDates] = [dbo].[GetTripDatesByTripIDUserCD](PM.TripID,@UserCD)
,[Description] = PM.TripDescription
,[Alerts] = PM.Notes

  FROM
   PreflightMain PM 
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0

   INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
				LEFT OUTER JOIN FleetGroupOrder FGO
				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG 
				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
				WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
					OR @FleetGroupCD = '''' ) 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
	WHERE PM.CustomerID = ' + CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) + '
         AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN @DATEFROM AND @DATETO 
         AND PM.Notes <> '''' 
         AND PM.TripStatus = ''T''
         AND PM.ISDELETED = 0 '
 --AND PM.EstDepartureDT BETWEEN @DATEFROM AND @DATETO        
    --Construct OPTIONAL clauses
   IF @TripNum <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND CONVERT(VARCHAR,PM.TripNUM) IN (''' +REPLACE( CASE WHEN RIGHT(@TripNum, 1) = ',' THEN LEFT(@TripNum, LEN(@TripNum) - 1) ELSE @TripNum END ,',', ''', ''') + ''')';
	END       
    IF @TailNum <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';
	END
	--IF @FleetGroupCD <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupID IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	--END	
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
	
	SET @SQLSCRIPT = @SQLSCRIPT + 'ORDER BY [FileNumber]'
	
	--PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD VARCHAR(500),@TripNum NVARCHAR(500)'
	
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @FleetGroupCD,@TripNum 
	
	-- EXEC spGetReportPRETripSheetAlertsInformation 'TIM', '2000-01-01', '2013-01-01', '', '', '', 0
--EXEC spGetReportPRETripSheetAlertsInformation 'supervisor_99', '2013-01-01', '2013-06-01', '', '', '', 0	


--SELECT * FROM PreflightMain where CustomerID = 10011 and TripNUM = 1907

--UPDATE PreflightMain
--SET Notes = 'Hi 1907'
--where CustomerID = 10011 and TripNUM = 1907

GO


