IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftTypeUtilization]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftTypeUtilization]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[spGetReportPOSTAircraftTypeUtilization]           
@UserCD AS VARCHAR(30), --Mandatory             
@DATEFROM AS DATETIME, --Mandatory             
@DATETO AS DATETIME, --Mandatory             
@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values             
@HomeBaseCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values             
@IsHomebase As bit=0               
AS             
             
-- ===============================================================================             
-- SPC Name: spGetReportPOSTAircraftTypeUtilization             
-- Author:  PremPrakash.S             
-- Create date: 6 Jul 2012             
-- Description: Get Aircraft Type Utilization             
-- Revision History             
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================             
             
             
SET NOCOUNT ON        
if(@HomeBaseCD=null)
BEGIN
	SET @HomeBaseCD='';
END

DECLARE @CUSTOMERID BIGINT=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))



               
 DECLARE @TenToMin INT
 SET @TenToMin=(Select Company.TimeDisplayTenMin From Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
       
  DECLARE @units varchar(15)
  SET @units=(SELECT CONVERT(varchar,(CASE WHEN (CP.FuelPurchase='1') THEN 'Gallons'
                WHEN (CP.FuelPurchase='2') THEN 'LTR'
				WHEN (CP.FuelPurchase='3') THEN 'IMP GAL'
				WHEN (CP.FuelPurchase='4') THEN 'Pounds'
				ELSE 'Kilos'
				END)
			 ) FROM Company CP 
			   WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  
			   AND CP.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
       
 

 /*
 SELECT AircraftType.AircraftType
       ,AircraftType.logid
       ,AircraftType.NoOfTrip
       ,CASE WHEN PL.LegNumCount=SUM(AircraftType.NoofLegs) THEN AircraftType.NoofLogs ELSE 0 END AS NoofLogs
       ,SUM(AircraftType.NoofLegs) AS NoofLegs
       ,SUM(AircraftType.BlockHours) AS BlockHours
       ,SUM(AircraftType.FlightHours) AS FlightHours
       ,SUM(AircraftType.NautMiles) AS NautMiles
       ,SUM(AircraftType.NoofPax) AS NoofPax
       ,SUM(AircraftType.FuelBurn) AS FuelBurn
       ,SUM(AircraftType.GallONs) AS GallONs
       ,SUM(AircraftType.TotalCost) AS TotalCost
       ,AircraftType.TenToMin
       ,AircraftType.Units
       ,SUM(AircraftType.AvgPrice) AS AvgPrice
 FROM
 (SELECT  DISTINCT     
		[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
		  ,[AircraftType]=Act.AircraftCD              
		  ,[NoOfTrip]=(CASE WHEN PFM.TripID IS NULL then COUNT(DISTINCT PFM.LogNUM) ELSE COUNT(DISTINCT PFM.TripID) END)
		  ,[NoofLogs]=COUNT(DISTINCT PFM.LogNUM)
		  ,[logid]=PFM.POLogID                   
		  ,[NoofLegs]=COUNT(DISTINCT PFL.LegNUM)
		  ,[BlockHours]=ISNULL(PFL.BlockHours,0) 
		  ,[FlightHours]=ISNULL(ROUND(PFL.FlightHours,1),0)          
		  ,[NautMiles]=ISNULL(PFL.Distance,0) 
		  ,[NoofPax]=ISNULL(PFL.PassengerTotal,0)             
		  ,[FuelBurn]=ISNULL(PFL.FuelUsed,0)             
		  ,[GallONs]=CASE WHEN (PFE.PurchaseDT>= @DATEFROM or PFE.PurchaseDT<=@DATETO) THEN (CAST(ROUND((dbo.GetFuelCONversiON(PFE.FuelPurchase,PFE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))) end
		  ,[TotalCost]=ISNULL(PFE.ExpenseAMT,0)     
		  ,[TenToMin]=@TenToMin
		  ,[Units] =@units
		  ,[AvgPrice]=  CASE WHEN Cp.FuelPurchase <> PFE.FuelPurchase and (ISNULL(pFe.FuelQTY,0))<>0 THEN DBO.GetFuelPriceCONversiON(PFE.FuelPurchase,ISNULL(pFe.ExpenseAMT,0)/pFe.FuelQTY,@UserCD)
													   ELSE PFe.UnitPrice End
		   
		   FROM postflightmain PFM                           
				INNER JOIN  FLEET FLT ON PFM.FleetID = FLT.FleetID              
				INNER JOIN PostflightLeg PFL ON  PFL.POLogID=PFM.POLogID AND PFL.IsDeleted = 0            
				INNER JOIN Aircraft ACT ON ACT.AircraftID = PFM.AircraftID
				LEFT JOIN PostflightExpense PFE ON PFE.POLogID = PFL.POLogID AND PFL.POLegID = PFE.POLegID AND PFE.IsDeleted = 0    
				LEFT OUTER JOIN Company CP ON CP.HomebaseID = PFM.HomebaseID          
				LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
				LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=PFM.HomebaseID AND UM.CustomerID=PFM.CustomerID AND UM.UserName= @UserCD
		 WHERE  PFM.CustomerID =  @CUSTOMERID  
		  AND CONVERT(DATE,PFL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
		  AND (ACt.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
		  AND (PfM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
		  AND (A.IcaoID IN (CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE A.IcaoID END) or @HomeBaseCD='')
		  AND PFM.IsDeleted = 0
		 GROUP BY ACT.AircraftCD,PFL.BlockHours,PFL.FlightHours,PFL.Distance,PFL.PassengerTotal,PFL.FuelUsed,PFE.FuelQTY,PFE.ExpenseAMT
				 ,CP.FuelPurchase,PFM.POLogID,PFE.ExpenseAMT,PFE.FuelQTY,PFE.FuelPurchase,PFE.PurchaseDT,PFE.UnitPrice,PFM.TripID, PFL.LegNUM
         )AircraftType
         LEFT OUTER JOIN (SELECT PL.POLogID,COUNT(PL.LegNUM) LegNumCount FROM PostflightLeg PL WHERE PL.CustomerID=@CUSTOMERID AND PL.IsDeleted = 0
					                                          GROUP BY PL.POLogID
                          ) PL ON AircraftType.logid=PL.POLogID
GROUP BY AircraftType.AircraftType,AircraftType.logid,AircraftType.NoofLogs,AircraftType.NoofTrip
        ,AircraftType.TenToMin,AircraftType.DateRange,AircraftType.Units,PL.LegNumCount

*/
 SELECT AircraftType.AircraftType
       ,AircraftType.logid
       ,NoOfTrip=PL.TripCount
       ,NoofLogs=PL.LogCount 
       ,NoofLegs=PL.LegNumCount
       ,SUM(CASE WHEN Rnk=1 THEN AircraftType.BlockHours ELSE 0 END) AS BlockHours
       ,SUM(CASE WHEN Rnk=1 THEN AircraftType.FlightHours ELSE 0 END) AS FlightHours
       ,SUM(CASE WHEN Rnk=1 THEN  AircraftType.NautMiles ELSE 0 END) AS NautMiles
       ,SUM(CASE WHEN Rnk=1 THEN  AircraftType.NoofPax ELSE 0 END) AS NoofPax
       ,SUM(CASE WHEN Rnk=1 THEN AircraftType.FuelBurn ELSE 0 END) AS FuelBurn
       ,SUM(AircraftType.GallONs) AS GallONs
       ,SUM(AircraftType.TotalCost) AS TotalCost
       ,AircraftType.TenToMin
       ,AircraftType.Units
       ,SUM(AircraftType.AvgPrice) AS AvgPrice
 FROM
 (SELECT  DISTINCT     
        [DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO)
          ,[AircraftType]=Act.AircraftCD              
          ,[NoOfTrip]=0
          ,[NoofLogs]=0
          ,[logid]=PFM.POLogID                   
          ,[NoofLegs]=0
          ,[BlockHours]=ISNULL(ROUND(PFL.BlockHours,1),0)
          ,[FlightHours]=ISNULL(ROUND(PFL.FlightHours,1),0)             
          ,[NautMiles]=ISNULL(PFL.Distance,0)
          ,[NoofPax]=ISNULL(PFL.PassengerTotal,0)             
          ,[FuelBurn]=ISNULL(PFL.FuelUsed,0)             
          ,[GallONs]=CASE WHEN (PFE.PurchaseDT>= @DATEFROM or PFE.PurchaseDT<=@DATETO) THEN (CAST(ROUND((dbo.GetFuelCONversiON(PFE.FuelPurchase,PFE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))) end
          ,[TotalCost]=ISNULL(PFE.ExpenseAMT,0)     
          ,[TenToMin]=@TenToMin
          ,[Units] =@units
          ,[AvgPrice]=  CASE WHEN Cp.FuelPurchase <> PFE.FuelPurchase and (ISNULL(pFe.FuelQTY,0))<>0 THEN DBO.GetFuelPriceCONversiON(PFE.FuelPurchase,ISNULL(pFe.ExpenseAMT,0)/pFe.FuelQTY,@UserCD)
                                                       ELSE PFe.UnitPrice End
           
          ,ROW_NUMBER()OVER(PARTITION BY PFL.POLEGID ORDER BY PFL.POLEGID) Rnk  
           FROM postflightmain PFM                           
                INNER JOIN  FLEET FLT ON PFM.FleetID = FLT.FleetID              
                INNER JOIN PostflightLeg PFL ON  PFL.POLogID=PFM.POLogID AND PFL.IsDeleted = 0            
                INNER JOIN Aircraft ACT ON ACT.AircraftID = PFM.AircraftID
                LEFT JOIN PostflightExpense PFE ON PFE.POLogID = PFL.POLogID AND PFL.POLegID = PFE.POLegID AND PFE.IsDeleted = 0 AND PFE.FuelQTY > 0    
                LEFT OUTER JOIN Company CP ON CP.HomebaseID = PFM.HomebaseID          
                LEFT OUTER JOIN AIRPORT A ON A.AirportID = CP.HomebaseAirportID
                LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=PFM.HomebaseID AND UM.CustomerID=PFM.CustomerID AND UM.UserName= @UserCD
         WHERE  PFM.CustomerID =  @CUSTOMERID  
          AND CONVERT(DATE,PFL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
          AND (ACt.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
          AND (PfM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
          --AND (A.IcaoID IN (CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) ELSE A.IcaoID END) or @HomeBaseCD='')
		  AND ( 
	A.IcaoID IN 
	(
		SELECT DISTINCT RTRIM(s) FROM dbo.SplitString(CASE WHEN @IsHomebase = 0 AND @HomeBaseCD <> '' THEN @HomeBaseCD ELSE A.IcaoID END, ',')
	))
          AND PFM.IsDeleted = 0
         GROUP BY ACT.AircraftCD,PFL.BlockHours,PFL.FlightHours,PFL.Distance,PFL.PassengerTotal,PFL.FuelUsed,PFE.FuelQTY,PFE.ExpenseAMT
                 ,CP.FuelPurchase,PFM.POLogID,PFE.ExpenseAMT,PFE.FuelQTY,PFE.FuelPurchase,PFE.PurchaseDT,PFE.UnitPrice,PFM.TripID, PFL.LegNUM,PFL.POLegID
         )AircraftType
         LEFT OUTER JOIN (SELECT (CASE WHEN PM.TripID IS NULL then COUNT(DISTINCT PL.POLogID) ELSE COUNT(DISTINCT PM.TripID) END) TripCount,PL.POLogID,COUNT( DISTINCT PL.POLogID) LogCount,COUNT(PL.LegNUM) LegNumCount FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID WHERE PL.CustomerID=@CUSTOMERID AND PL.IsDeleted = 0 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
                                                              GROUP BY PL.POLogID,PM.TripID
                          ) PL ON AircraftType.LOGID=PL.POLogID
GROUP BY AircraftType.AircraftType,AircraftType.logid,AircraftType.TenToMin,AircraftType.DateRange,AircraftType.Units,PL.LegNumCount,AircraftType.NoofLegs,PL.TripCount,PL.LogCount

-- spGetReportPOSTAircraftTypeUtilizatiON  'supervisor_99' ,'09-Jan-09','09-Jul-09','','',1



GO


