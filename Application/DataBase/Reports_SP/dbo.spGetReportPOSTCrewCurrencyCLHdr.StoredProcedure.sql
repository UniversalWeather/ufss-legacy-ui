IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrencyCLHdr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrencyCLHdr]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE  [dbo].[spGetReportPOSTCrewCurrencyCLHdr]  
(    
@UserCD Varchar(30),
@ChecklistCode varchar(5000)='',
@AsOf DateTime         
)    
As    

Begin    
   Declare @DateRange Varchar(30)
   Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@AsOf )  
   
   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';                        
   DECLARE @ParameterDefinition AS NVARCHAR(400)        
    
   IF OBJECT_ID('tempdb..#Tmp') is not null    
      DROP table #Tmp    
                      
   Create Table #Tmp(CrewCheckId BigInt, CrewCheckCD Varchar(10),CrewChecklistDescription varchar(25))   
   --IF @ChecklistCode  =''          
   --   BEGIN                    
       --SET @SQLSCRIPT = 'Insert Into #Tmp Select Top 3 CrewCheckID, CrewCheckCD,CrewChecklistDescription From CrewCheckList 
       --Where CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                    
   --   END         
  -- ELSE 
   IF @ChecklistCode  <> ''          
     BEGIN                                
      SET @SQLSCRIPT = 'Insert Into #Tmp Select Top 3 CrewCheckID,CrewCheckCD,CrewChecklistDescription         
      From CrewChecklist Where CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) + 
      ' And CrewCheckCD IN (''' + REPLACE(CASE WHEN RIGHT(@ChecklistCode, 1) = ',' THEN         
      LEFT(@ChecklistCode, LEN(@ChecklistCode) - 1) ELSE @ChecklistCode END, ',', ''', ''') + ''')'                              
     END      
     
    --print @SQLSCRIPT    
   SET @ParameterDefinition = '@UserCD As Varchar(30),@ChecklistCode As Varchar(5000)'            
   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition,@UserCD,@ChecklistCode    
       
   Declare @RowId BIGINT    
   Declare @SNo Int    
   Set @SNo =1    
   
   Declare @Col1 Varchar(150)=''          
   Declare @Col2 Varchar(150)=''          
   Declare @Col3 Varchar(150)=''   
    
   Declare @ChkLstRowCount Int=0
   Set @ChkLstRowCount=(SELECT COUNT(*) FROM #Tmp)
   
   WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0          
   BEGIN           
     Set @RowId=(Select Top 1 CrewCheckID From #Tmp)   
      
     IF @SNo = 1          
     Begin     
       Set @Col1 = (SELECT CrewChecklistDescription From #Tmp Where CrewCheckID=@RowId)  
     End  
     Else IF @SNo = 2          
     Begin         
       Set @Col2 = (SELECT CrewChecklistDescription From #Tmp Where CrewCheckID=@RowId)   
     End          
     Else IF @SNo = 3          
     Begin         
        Set @Col3 = (SELECT CrewChecklistDescription From #Tmp Where CrewCheckID=@RowId) 
     End          
     Set @SNo = @SNo + 1          
     Delete From #Tmp Where CrewCheckID=@RowId           
   END             
   
   Select @Col1 Col1,@Col2 Col2,@Col3 Col3,@DateRange DateRange
   
   IF OBJECT_ID('tempdb..#Tmp') is not null    
      DROP table #Tmp    
End  
--exec spGetReportPOSTCrewCurrencyCLHdr 'matt','100,120','2009-1-1'


GO


