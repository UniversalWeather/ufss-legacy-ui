IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogCheckListInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogCheckListInformation]

/****** Object:  StoredProcedure [dbo].[spGetReportPOSTPilotLogCheckListInformation]    Script Date: 09/01/2014 19:10:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTPilotLogCheckListInformation]
(
	@UserCD AS VARCHAR(30), --Mandatory  
	@DATEFROM AS DATETIME, --Mandatory  
	@DATETO AS DATETIME, --Mandatory  
	@ChecklistCode AS NVARCHAR(1000) , -- [Optional], Comma delimited string with mutiple values
	@CrewCD as VARCHAR(100)
)
AS

BEGIN

SELECT 
	C.CustomerID,
    [DateFROM] = CONVERT(DATE, @DATEFROM)
	,[DATETO] = CONVERT(DATE, @DATETO)
	,C.[CrewID]
	,C.[CREWCD]
	,[Checklist/Checklist Item]=ccl.CrewChecklistDescription
	,[Checklist/Last]=CCD.PreviousCheckDT
	,[Checklist/Due] =CCD.DueDT 
	,[Checklist/Alert]=CCD.AlertDT
	,[Checklist/Grace]=CCD.GraceDT
	,[Checkllist/Date] = CASE WHEN (@DATETO > CCD.GraceDT) THEN 'PAST DUE'
								WHEN (@DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT) THEN 'GRACE'
								WHEN (@DATETO >= CCD.AlertDT AND @DATETO <= CCD.DueDT) THEN 'ALERT'
								WHEN (@DATETO < CCD.AlertDT) THEN ''
								ELSE '' END
	,CHECKLISTCODE=CCL.CrewCheckCD
	,C.LastName
    ,C.FirstName
    ,C.MiddleInitial
	FROM Crew C 
		INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=C.CrewID and CCD.CustomerID= C.CustomerID
		INNER JOIN CrewCheckList CCL on ccd.CheckListCD = ccl.CrewCheckCD and  ccl.IsDeleted =0 and CCD.IsDeleted=0 and CCD.CustomerID= CCL.CustomerID
	WHERE (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '' OR @ChecklistCode IS NULL)
		AND c.IsDeleted = 0 AND C.CrewCD = @CrewCD
		AND C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	order by ccd.CheckListCD
END


GO
