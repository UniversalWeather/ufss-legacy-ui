IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SplitString]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[SplitString]   
    (  
        @str nvarchar(4000),   
        @separator char(1)  
    )  
 RETURNS @ReturnVal TABLE 
(
    zeroBasedOccurance INT,
    s VARCHAR(400)
 )
AS  
BEGIN


 with tokens(p, a, b) AS  (  
            select   
                1 ,   
                1 ,   
                charindex(@separator, @str)   
            union all  
            select  
                p + 1,   
                b + 1,   
                charindex(@separator, @str, b + 1)  
            from tokens  
            where b > 0  
)
 insert into @ReturnVal(zeroBasedOccurance,s)
   select  
            p-1 zeroBasedOccurance,  
            substring(  
                @str,   
                a,   
                case when b > 0 then b-a ELSE 4000 end)   
            AS s  
        from tokens option (maxrecursion 0)

 
RETURN ;

 

END


GO


