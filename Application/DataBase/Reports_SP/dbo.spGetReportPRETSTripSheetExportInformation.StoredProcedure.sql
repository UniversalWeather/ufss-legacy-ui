IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSheetExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSheetExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPRETSTripSheetExportInformation]    
 @UserCD            AS VARCHAR(30)        
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM    
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM    
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)    
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal    
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal     
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)    
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)    
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)    
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)    
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD    
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase    
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)    
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD       
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'    
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'    
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'    
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'    
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'    
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'    
AS    
BEGIN    
  
-- EXEC spGetReportPRETSTripSheetExportInformation 'ELIZA_9','3820','','','','','','','','','','','','',0,0,0,0,0
-- =========================================================================    
-- SPC Name: spGetReportPRETSTripSheetExportInformation    
-- Author: AISHWARYA.M    
-- Create date: 13 Sep 2012    
-- Description: Get Preflight Tripsheet Report Export information for REPORTS    
-- Revision History    
-- Date  Name  Ver  Change    
--     
-- ==========================================================================    
SET NOCOUNT ON    
    
 -- [Start] Report Criteria --    
 DECLARE @tblTripInfo AS TABLE (    
 TripID BIGINT    
 , LegID BIGINT    
 , PassengerID BIGINT    
 )    
    
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)    
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,    
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,    
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,     
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService    
--select * from @tblTripInfo    
 -- [End] Report Criteria --    
    
--Reqcode [PreflightMain.PassengerRequestorID]:  PassengerCD    
--paxname   - Passenger Name    
   
--requestor [PreflightLeg.PassengerRequestorID]: PassengerCD    
--reqname   - Passenger Name    
    
--Crewcode [PreflightMain.ReleasedBy] [PreflightMain.CrewID]    
--Releasedby: Crew.[Full Name]    
    
---------------    
   
CREATE TABLE #tblFlag     
 (    
 ROWID INT    
 ,flag CHAR(1)    
 )    
     
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (1,'A')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (2,'B')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (3,'O')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (4,'D')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (5,'C')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (6,'P')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (7,'F')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (8,'E')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (9,'H')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (10,'Q')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (11,'G')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (12,'I')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (13,'J')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (14,'K')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (15,'L')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (16,'M')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (17,'N')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (18,'S')    
 INSERT INTO #tblFlag(ROWID, FLAG) VALUES (19,'T')    
    
-----------------    
  
SELECT * INTO #tblTripSheet FROM     
(    
  SELECT DISTINCT    
  [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)    
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)    
  ,[orig_nmbr] = PM.TripNUM    
  ,[estdepdt] = dbo.GetShortDateFormatByUserCD(@UserCD,PM.EstDepartureDT) --PM.EstDepartureDT  
  ,[dispatchno] = PM.DispatchNUM    
  ,[tail_nmbr] = F.TailNum    
  ,[type_code] = A.AircraftCD    
  ,[desc] = PM.TripDescription    
  ,[reqcode] = Req.PassengerRequestorCD --PM.PassengerRequestorID    
  ,[requestor] = LReq.PassengerRequestorCD    
  --,[reqname] = PM.RequestorLastName + ',' + PM.RequestorFirstName + ',' + PM.RequestorMiddleName    
  ,[reqname] = LReq.LastName + ', ' + LReq.FirstName + ' ' + LReq.MiddleInitial    
  ,[dispatcher] = UM.UserName --UM.FirstName    
  ,[dsptname] = UM.FirstName + ' ' + UM.LastName --PM.DispatcherUserName   
  ,[crewcode] = RC.CrewCD    
  ,[releasedby] = RC.LastName + ', ' + RC.FirstName + ' ' + RC.MiddleInitial --PM.ReleasedBy    
  ,[verifynmbr] = PM.VerifyNUM    
  ,[reqaddlphone] = Req.AdditionalPhoneNum    
  --,[paxname] = PP.PassengerFirstName + ',' + PP.PassengerMiddleName + ' ' + PP.PassengerLastName     
  ,[paxname] = Req.LastName + ', ' + Req.FirstName + ' ' + Req.MiddleInitial    
  ,[ppnum] =  ISNULL(PPassport.PassportNUM,'')  
  --,[ppnum] = dbo.FlightPakDecrypt(ISNULL(PPassport.PassportNUM,''))    
  ,[nationcode] = CO.CountryCD    
  ,[citizen] = CO.CountryName     
  ,[dob] = dbo.GetShortDateFormatByUserCD(@UserCD,Req.DateOfBirth)--Req.DateOfBirth --PAX.DateOfBirth    
  ,[phone] = Req.PhoneNum --PAX.PhoneNum    
  ,[leg_num] = PL.LegNum    
  ,[legid] = PL.LegID    
  ,[depicao_id] = AD.IcaoID    
  ,[depcity] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName + ', ' + AD.CountryName --AD.CityName    
  ,[depnamecitystate] = AD.AirportName + ',' + AD.CityName + ',' + AD.StateName    
  ,[arricao_id] = AA.IcaoID    
  ,[arrcity] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName + ', ' + AA.CountryName --AA.CityName    
  ,[arrnamecitystate] = AA.AirportName + ',' + AA.CityName + ',' + AA.StateName    
  ,[fltno] = PM.FlightNUM    
  ,[pax_total] = PL.PassengerTotal    
  ,[depzulu] = AD.OffsetToGMT    
  ,[arrzulu] = AA.OffsetToGMT    
  ,[locdep] = dbo.GetShortDateFormatByUserCD(@UserCD,PL.DepartureDTTMLocal) + ' ' + LEFT(convert(varchar, PL.DepartureDTTMLocal, 8),5) --CONVERT(VARCHAR(10),PL.DepartureDTTMLocal,101) --PL.DepartureDTTMLocal    
  ,[locarr] = dbo.GetShortDateFormatByUserCD(@UserCD,PL.ArrivalDTTMLocal) + ' ' + LEFT(convert(varchar, PL.ArrivalDTTMLocal, 8),5) --CONVERT(VARCHAR(10),PL.ArrivalDTTMLocal,101) --PL.ArrivalDTTMLocal    
  ,[gmtdep] = dbo.GetShortDateFormatByUserCD(@UserCD,PL.DepartureGreenwichDTTM) + ' ' + LEFT(convert(varchar, PL.DepartureDTTMLocal, 8),5) --CONVERT(VARCHAR(10),PL.DepartureGreenwichDTTM,101) --PL.DepartureGreenwichDTTM    
  ,[gmtarr] = dbo.GetShortDateFormatByUserCD(@UserCD,PL.ArrivalGreenwichDTTM) + ' ' + LEFT(convert(varchar, PL.DepartureDTTMLocal, 8),5) --CONVERT(VARCHAR(10),PL.ArrivalGreenwichDTTM,101) --PL.ArrivalGreenwichDTTM    
  ,[timeapprox] = PL.IsApproxTM    
  ,[distance] = PL.Distance    
  ,[elp_time] = PL.ElapseTM     
  ,[code] = FBA.FBOCD  
  ,[purpose] = PL.FlightPurpose    
  ,[cat_code] = FC.FlightCatagoryCD  
  ,[fltcatdesc] = FC.FlightCatagoryDescription    
  ,[farnum] = PL.FedAviationRegNUM    
  ,[depfbo_desc] = FBD.FBOVendor    
  ,[depfbo_phone] = FBD.PhoneNUM1    
  ,[depfbo_phone2] = FBD.PhoneNUM2    
  ,[depfbo_freq] = FBD.Frequency    
  ,[depfbo_fax] = FBD.FaxNum    
  ,[depfbo_uvair] = FBD.IsUWAAirPartner    
  ,[depfbo_fuel_brand] = FBD.FuelBrand    
  ,[depfbo_negfuelpr] = FBD.NegotiatedFuelPrice    
  ,[depfbo_lastfuelpr] = FBD.LastFuelPrice    
  ,[depfbo_lastfueldt] = dbo.GetShortDateFormatByUserCD(@UserCD,FBD.LastFuelDT)--FBD.LastFuelDT    
  ,[depfbo_contact] = FBD.Contact    
  ,[depfbo_addr1] = FBD.Addr1    
  ,[depfbo_addr2] = FBD.Addr2    
  ,[depfbo_paytype] = FBD.PaymentType    
  ,[depfbo_postedpr] = FBD.PostedPrice    
  ,[depfbo_fuelqty] = FBD.FuelQty    
  ,[arrfbo_desc] = FBA.FBOVendor     
  ,[arrfbo_phone] = FBA.PhoneNUM1    
  ,[arrfbo_phone2] = FBA.PhoneNUM2    
  ,[arrfbo_freq]= FBA.Frequency    
  ,[arrfbo_fax] = FBA.FaxNum    
  ,[arrfbo_uvair] = FBA.IsUWAAirPartner    
  ,[arrfbo_fuel_brand] = FBA.FuelBrand     
  ,[arrfbo_negfuelpr] = FBA.NegotiatedFuelPrice    
  ,[arrfbo_lastfuelpr] = FBA.LastFuelPrice    
  ,[arrfbo_lastfueldt] =  dbo.GetShortDateFormatByUserCD(@UserCD,FBA.LastFuelDT) --FBA.LastFuelDT    
  ,[arrfbo_contact] = FBA.Contact    
  ,[arrfbo_addr1] = FBA.Addr1    
  ,[arrfbo_addr2] = FBA.Addr2    
  ,[arrfbo_paytype] = FBA.PaymentType    
  ,[arrfbo_postedpr] = FBA.PostedPrice    
  ,[arrfbo_fuelqty] = FBA.FuelQty    
  ,[dept_code] = D.DepartmentCD    
  ,[dept_desc] = D.DepartmentName    
  ,[auth_code] = DA.AuthorizationCD    
  ,[auth_desc] = DA.DeptAuthDescription    
  ,[authphone] = DA.AuthorizerPhoneNum --CQFILE.DBF/PHONE    
  ,[winds] = PL.WindsBoeingTable    
   --(Crew Hotel)    
  ,[crewh_name] = CH.Name    
  ,[crewh_phone] = CH.PhoneNum    
  ,[crewh_fax] = CH.FaxNum    
  ,[crewh_rate] = CH.NegociatedRate    
  ,[crewh_addr1] = CH.Addr1    
  ,[crewh_addr2] = CH.Addr2    
  ,[crewh_rooms] = CH.NoofRooms    
   --(Arr Crew Trans)    
  ,[crewta_name] = TCA.TransportationVendor    
  ,[crewta_phone] = TCA.PhoneNum    
  ,[crewta_fax] = TCA.FaxNum    
  ,[crewta_rate] = TCA.NegotiatedRate    
  ----(Dep Crew Trans)    
  ,[crewtd_name] = TCD.TransportationVendor    
  ,[crewtd_phone] =  TCD.PhoneNum    
  ,[crewtd_fax] = TCD.FaxNum    
  ,[crewtd_rate] = TCD.NegotiatedRate    
  ----(Pax Hotel)    
  ,[hotel_name] = PH.Name    
  ,[hotel_phone] = PH.PhoneNum    
  ,[hotel_fax] = PH.FaxNum    
  ,[hotel_rate] = PH.NegociatedRate    
  ,[hotel_rooms] = PH.NoofRooms    
  ----(Arr Pax Trans)    
  ,[paxta_name] = TPA.TransportationVendor    
  ,[paxta_phone] = TPA.PhoneNum    
  ,[paxta_fax] = TPA.FaxNum    
  ,[paxta_rate] = TPA.NegotiatedRate    
  ----(Dep Pax Trans)    
  ,[paxtd_name] = TPD.TransportationVendor    
  ,[paxtd_phone] = TPD.PhoneNum    
  ,[paxtd_fax] = TPD.FaxNum    
  ,[paxtd_rate] = TPD.NegotiatedRate    
  ----(Arr Catering)    
  ,[catera_name] = CA.CateringVendor    
  ,[catera_phone] = CA.PhoneNum    
  ,[catera_fax] = CA.FaxNum    
  ,[catera_rate] = CA.NegotiatedRate    
  ----(Dep Catering)    
  ,[caterd_name] = CD.CateringVendor    
  ,[caterd_phone] = CD.PhoneNum    
  ,[caterd_fax] = CD.FaxNum    
  ,[caterd_rate] = CD.NegotiatedRate    
  ----(Addl Crew Hotel)    
  ,[main_name] = ACH.Name    
  ,[main_phone] = ACH.PhoneNum    
  ,[main_fax] = ACH.FaxNum    
  ,[main_rate] = ACH.NegociatedRate    
  ,[main_addr1] = ACH.Addr1    
  ,[main_addr2] = ACH.Addr2   
     
  ,[piccode] = PIC.CrewCD     
  ,[pic] = PIC.LastName + ', ' + PIC.FirstName + ' ' + PIC.MiddleInitial    
  ,[siccode] = SIC.CrewCD    
  ,[sic] = SIC.LastName + ', ' +SIC.FirstName + ' ' + SIC.MiddleInitial    
  ,[pic_mblphn] = PIC.CellPhoneNum    
  ,[sic_mblphn] = SIC.CellPhoneNum    
  ,[fltphone] = F.FlightPhoneNum    
  ,[infltphone] =F.FlightPhoneIntlNum   
  --,[fuel_burn] = ''  -- ???   
  ,[fuel_burn] = ISNULL(dbo.FuelBurn(@UserCD,A.AircraftCD,PL.ElapseTM,PL.PowerSetting),0)  
  ,[ac_code] = F.AircraftCD    
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID)    
  ,[fuel_load] = PL.FuelLoad    
  ,[revisndesc] = PM.RevisionDescriptioin    
  ,[revisnnmbr] = PM.RevisionNUM    
  ,[dsptemail] = UM.EmailID    
  ,[dsptphone] = UM.PhoneNum    
  ,[custdesc]  = CASE PM.IsQuote WHEN 1 THEN PMA.DeptAuthDescription ELSE '' END    
  ,[reqauth] = CASE PM.IsQuote WHEN 1 THEN PMA.AuthorizationCD ELSE '' END  --DA.AuthorizationCD    
  ,[cqflag] = PM.IsQuote    
  ,[custphone] = 'R2'--CQFILE.DBF/PHONE    
  ,[estfuelqty] = PM.EstFuelQTY    
  --(Dep FBO)    
  ,[depfbo_city] =  FBD.CityName    
  ,[depfbo_state] = FBD.StateName    
  ,[depfbo_zip] = FBD.PostalZipCD    
  --(Arr FBO)    
  ,[arrfbo_city] = FBA.CityName    
  ,[arrfbo_state] = FBA.StateName    
  ,[arrfbo_zip] = FBA.PostalZipCD    
  --(Crew Hotel)    
  ,[crewh_city] = CH.CityName    
  ,[crewh_state] = CH.StateName    
  ,[crewh_zip] = CH.PostalZipCD    
  --(Pax Hotel)    
  ,[hotel_addr1] = PH.Addr1    
  ,[hotel_addr2] = PH.Addr2    
  ,[hotel_city] = PH.CityName    
  ,[hotel_state] = PH.StateName    
  ,[hotel_zip] = PH.PostalZipCD    
  --(Addl Crew Hotel)    
  ,[main_city] = ACH.CityName    
  ,[main_state] = ACH.StateName    
  ,[main_zip] = ACH.PostalZipCD   
    
  --mhtml fields  
  ,[TRIPALERTS]=''  
  ,[AIRCRAFTNOTES]=''  
  ,[PAXNOTES]=''  
  ,[PAXALERTS]=''  
  ,[CrewNotes]=''  
  ,[FedAviationNo]=''  
  ,[DEPFBO_REMARKS]=''  
  ,[DEPFBO_CONFIRM]=''  
  ,[DEPFBO_COMMENTS]=''  
  ,[ARRFBO_REMARKS]=''  
  ,[ARRFBO_CONFIRM]=''  
  ,[ARRFBO_COMMENTS]=''  
  ,[AIRPORT_NOTES]=''  
  ,[AIRPORT_ALERTS]=''  
  ,[OutboundInt]=''  
  ,[CREWH_REMARKS]=''  
  ,[CREWH_CONFIRM]=''  
  ,[CREWH_COMMENTS]=''  
  ,[CREWTA_REMARKS]=''  
  ,[CREWTA_CONFIRM]=''  
  ,[CREWTA_COMMENTS]=''  
  ,[CREWTD_REMARKS]=''  
  ,[CREWTD_CONFIRM]=''  
  ,[CREWTD_COMMENTS]=''  
  ,[HOTEL_CONFIRM]=''  
  ,[HOTEL_COMMENTS]=''  
  ,[HOTEL_REMARKS]=''  
  ,[PAXTA_CONFIRM]=''  
  ,[PAXTA_COMMENTS]=''  
  ,[PAXTA_REMARKS]=''  
  ,[PAXTD_CONFIRM]=''  
  ,[PAXTD_COMMENTS]=''  
  ,[PAXTD_REMARKS]=''  
  ,[CATERA_REMARKS]=''  
  ,[CATERA_CONFIRM]=''  
  ,[CATERA_COMMENTS]=''  
  ,[CATERD_REMARKS]=''  
  ,[CATERD_CONFIRM]=''  
  ,[CATERD_COMMENTS]=''  
  ,[MAIN_REMARKS]=''  
  ,[MAIN_CONFIRM]=''  
  ,[MAIN_COMMENTS]=''  
  ,[CREWMEMBERS]=''  
  ,[Additional_Crew]=''  
  ,[MNBR]=''  
  ,[MCNG]=''  
  ,[MCODE]=''  
  ,[MPASS]=''  
  ,[MLEGS]=''  
  ,[MPAXNAME]=''  
  ,[MPAXCODE]=''  
  ,[MTSASTAT]=''  
  ,[TRIPCREWS]=''  
  ,[Duty_Type]=''  
  ,[CREWNAMES]=''  
  ,[MOBPHONES]=''  
  ,[CREWLEGS]=''  
  ,[VENDORINFO]=''  
 ,[FBONAME]=''  
 ,[BESTPRICE]=''  
 ,[GALLONFROM]=''  
 ,[GALLONTO]=''  
 ,[Unit Price]=''  
 ,[LEASTPRICE]=''  
 ,[UPDATEDTTM]=''  
 ,[EFFDATE]=''  
      
   FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M    
   INNER JOIN PreflightMain PM ON M.TripID = PM.TripID    
   INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID    
   LEFT JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT, CountryName FROM Airport)AD ON AD.AirportID = PL.DepartICAOID    
   LEFT JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT, CountryName FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID    
   LEFT JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID    
   INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID    
       
   --FOR PIC    
  LEFT JOIN(    
   SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial     
   FROM PreflightCrewList PC    
   INNER JOIN Crew C ON PC.CrewID = C.CrewID WHERE PC.DutyTYPE = 'P'    
  ) PIC ON PL.LegID = PIC.LegID    
    
  LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND PM.DispatcherUserName = UM.UserName    
  LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID    
  LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID    
  LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID    
      
  --FOR SIC       
  LEFT OUTER JOIN(    
   SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial     
   FROM PreflightCrewList PC    
   INNER JOIN Crew C ON PC.CrewID = C.CrewID    
   WHERE PC.DutyTYPE = 'S'    
  ) SIC ON PL.LegID = SIC.LegID    
  
   LEFT OUTER JOIN (    
  SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization    
   ) PMA ON PM.AuthorizationID = PMA.AuthorizationID    
                
   LEFT OUTER JOIN (    
  SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization    
   ) DA ON PL.AuthorizationID = DA.AuthorizationID    
   LEFT OUTER JOIN (    
  SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization    
   ) RA ON LReq.AuthorizationID = RA.AuthorizationID    
   LEFT OUTER JOIN Country CO ON Req.CountryID = CO.CountryID --PAX.CountryID = CO.CountryID    
   LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID    
   LEFT OUTER JOIN Department D ON LReq.DepartmentID = D.DepartmentID --PAX.DepartmentID = D.DepartmentID    
       
   LEFT OUTER JOIN (    
    --SELECT PFL.LEGID, F.*  FROM PreflightFBOList PFL  INNER JOIN FBO F   
 SELECT  PFL.PreflightFBOName FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,  
        F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,  
        F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,  
        PFL.LegID, F.FBOCD  
 FROM PreflightFBOList PFL  
 INNER JOIN FBO F   
    ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1    
   ) FBD ON PL.LegID = FBD.LegID    
    
   LEFT OUTER JOIN (    
    --SELECT PFL.LEGID, F.* FROM PreflightFBOList PFL  INNER JOIN FBO F   
 SELECT  PFL.PreflightFBOName FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,  
        F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,  
        F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,  
        PFL.LegID, F.FBOCD  
 FROM PreflightFBOList PFL  
 INNER JOIN FBO F       
    ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1    
   ) FBA ON PL.LegID = FBA.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT  PTL.LegID, T.* FROM PreflightTransportList PTL INNER JOIN Transport T   
    SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,  
  [NegotiatedRate]=PTL.PhoneNum4, --T.Remarks,   
  PTL.LegID,PTL.ConfirmationStatus,PTL.Comments  
 FROM PreflightTransportList PTL   
 --INNER JOIN Transport T ON PTL.TransportID = T.TransportID   
 WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'    
   ) TPD ON PL.LegID = TPD.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT  PTL.LegID, T.* FROM PreflightTransportList PTL INNER JOIN Transport T   
    SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,  
  [NegotiatedRate]=PTL.PhoneNum4, --T.Remarks,   
  PTL.LegID,PTL.ConfirmationStatus,PTL.Comments  
 FROM PreflightTransportList PTL   
 --INNER JOIN Transport T ON PTL.TransportID = T.TransportID   
    WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'    
   ) TPA ON PL.LegID = TPA.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT  PTL.LegID, T.* FROM PreflightTransportList PTL INNER JOIN Transport T   
    SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,  
  [NegotiatedRate]=PTL.PhoneNum4, --T.Remarks,   
  PTL.LegID,PTL.ConfirmationStatus,PTL.Comments  
 FROM PreflightTransportList PTL   
 --INNER JOIN Transport T ON PTL.TransportID = T.TransportID   
 WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'    
   ) TCD ON PL.LegID = TCD.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT PTL.LegID, T.* FROM PreflightTransportList PTL INNER JOIN Transport T   
    SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,  
  [NegotiatedRate]=PTL.PhoneNum4, --T.Remarks,   
  PTL.LegID,PTL.ConfirmationStatus,PTL.Comments  
 FROM PreflightTransportList PTL   
 --INNER JOIN Transport T ON PTL.TransportID = T.TransportID   
 WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'    
   ) TCA ON PL.LegID = TCA.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT PCL.LegID, C.* FROM PreflightCateringDetail PCL INNER JOIN Catering C   
 SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,  
  [NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,PCL.LegID  
     --C.Remarks  
 FROM PreflightCateringDetail PCL   
 --INNER JOIN Catering C ON PCL.CateringID = C.CateringID   
 WHERE PCL.ArriveDepart = 'D'    
   ) CD ON PL.LegID = CD.LegID    
       
   LEFT OUTER JOIN (    
    --SELECT PCL.LegID, C.* FROM PreflightCateringDetail PCL INNER JOIN Catering C   
 SELECT [CateringVendor]=PCL.ContactName,[PhoneNum]=PCL.ContactPhone,[FaxNum]=PCL.ContactFax,  
  [NegotiatedRate]=PCL.Cost,PCL.CateringComments,PCL.CateringConfirmation,PCL.LegID  
     --C.Remarks  
 FROM PreflightCateringDetail PCL   
 --INNER JOIN Catering C ON PCL.CateringID = C.CateringID   
    WHERE PCL.ArriveDepart = 'A'       
   ) CA ON PL.LegID = CA.LegID    
      
      
  --Pax Hotel    
  LEFT OUTER JOIN (   
     /*  
    SELECT     
       PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.Addr1,HP.Addr2,HP.CityName,    
       HP.StateName,HP.PostalZipCD,HP.Remarks,HP.NegociatedRate,    
       PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments     
       FROM PreflightPassengerList PPL     
       INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID    
       INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID    
     */   
  SELECT   
    [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,  
    PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments,  
    --HP.Addr1,HP.Addr2,HP.CityName,HP.StateName,HP.PostalZipCD,  
    [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, PPH.CityName, PPH.StateName, PPH.PostalZipCD,PPL.LegID  
    --[Remarks]=HP.Remarks,  
  FROM PreflightPassengerList PPL   
  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID  
  --LEFT OUTER JOIN Hotel HP ON PPH.HotelID = HP.HotelID     
  ) PH ON PL.legID = PH.LegID    
    
  --Crew Hotel    
      
  LEFT OUTER JOIN (    
    /*     
     SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,    
    HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,    
    PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments      
     FROM PreflightCrewList PCLL     
     INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID    
     INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID    
     WHERE PCLL.DutyType IN ('P', 'S')   
    */  
 SELECT   [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,  
  PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,  
  [Addr1] = PCH.Address1, [Addr2] = PCH.Address2, PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID  
  --HC.Addr1,HC.Addr2,HC.CityName,HC.StateName,HC.PostalZipCD,  
  --[Remarks]=HC.Remarks,PCLL.LegID  
 FROM PreflightCrewList PCLL   
 --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
 INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
 INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
 --LEFT OUTER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
 WHERE PCLL.DutyType IN ('P', 'S')  
  ) CH ON PL.legID = CH.LegID    
    
  --Addnl Crew / Maint Crew - Hotel    
  LEFT OUTER JOIN (  /*     
     SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,    
     HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,    
     PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments       
     FROM PreflightCrewList PCLL     
     INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID    
     INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID    
     WHERE PCLL.DutyType NOT IN ('P', 'S')    
     */  
 SELECT   [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,  
  PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,  
  [Addr1] = PCH.Address1, [Addr2] = PCH.Address2, PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID  
  --HC.Addr1,HC.Addr2,HC.CityName,HC.StateName,HC.PostalZipCD,  
  --[Remarks]=HC.Remarks,PCLL.LegID  
 FROM PreflightCrewList PCLL   
 INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
 INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID 
 --LEFT OUTER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
 WHERE PCLL.DutyType NOT IN ('P', 'S')  
  ) ACH ON PL.legID = ACH.LegID    
      
  LEFT OUTER JOIN (    
   --SELECT PassengerRequestorID, [PassportNUM] = dbo.FlightPakDecrypt(PassportNUM), Choice    
   SELECT PassengerRequestorID, PassportNUM, Choice    
   FROM CrewPassengerPassport     
  ) PPassport ON Req.PassengerRequestorID = PPassport.PassengerRequestorID AND PPassport.Choice = 1    
      
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))    
  --AND PM.TripID = 1000142 --@TripID     
 --ORDER BY PM.TripNUM  
) main    
    
-- select * from #tblTripSheet    
    
 SELECT A.*, B.* FROM #tblFlag A OUTER APPLY #tblTripSheet B   
 ORDER BY B.orig_nmbr, B.leg_num, A.ROWID 
    
IF OBJECT_ID('TEMPDB..#tblFlag') IS NOT NULL  
DROP TABLE #tblFlag  
  
IF OBJECT_ID('TEMPDB..#tblTripSheet') IS NOT NULL  
DROP TABLE #tblTripSheet   
  
END



GO


