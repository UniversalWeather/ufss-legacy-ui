IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREMaintenanceAlertssub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREMaintenanceAlertssub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetReportPREMaintenanceAlertssub]   
    @UserCD AS VARCHAR(30) ,  
    @UserCustomerID AS BIGINT,  
    @FleetGroupCD AS NVARCHAR(1000) = '',  
    @TailNum AS VARCHAR(100) = ''  
      
AS      
BEGIN  
SET NOCOUNT ON;  
  
DECLARE  @TempFleetID TABLE       
(     
ID INT NOT NULL IDENTITY (1,1),   
FleetID BIGINT,  
HomeBaseID BIGINT,  
TailNum VARCHAR(9)  
  )  
    
  
IF @TailNum <> ''  
BEGIN  
INSERT INTO @TempFleetID  
SELECT DISTINCT F.FleetID,F.HomebaseID,F.TailNum   
FROM Fleet F  
WHERE F.CustomerID = @UserCustomerID  
AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  
END  
  
IF @FleetGroupCD <> ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID, F.HomebaseID,F.TailNum  
FROM Fleet F   
LEFT OUTER JOIN FleetGroupOrder FGO  
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
LEFT OUTER JOIN FleetGroup FG   
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
AND F.CustomerID = @UserCustomerID    
AND FG.IsDeleted=0  
END  
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID,F.HomebaseID,F.TailNum  
FROM Fleet F   
WHERE F.CustomerID = @UserCustomerID    
AND F.IsDeleted=0  
AND F.IsInActive = 0  
END  
-------------------------------TailNum and Fleet Group Filteration----------------------  
  
Declare @SuppressActivityDept BIT    
   
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID   
AND IsZeroSuppressActivityAircftRpt IS NOT NULL  
AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
  
SET NOCOUNT ON;  
--DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
DECLARE @TenToMin SMALLINT = 0;  
  
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
    
 declare @AircraftBasis numeric(1,0);  
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
  
  
  
SELECT DISTINCT F.TailNum,F.FleetID,  
       FltBlkHrsToInsptlabel =(CASE @AircraftBasis WHEN 2 THEN CONVERT(VARCHAR(30),'Flt Hrs To Inspt')   
                                           WHEN 1 THEN CONVERT(VARCHAR(30),'Blk Hrs To Inspt') END)  
  ,PL.DepartureDTTMLocal  
  ,PL.ElapseTM  
  ,PM.TripNUM  
  ,PL.LegNUM  
  ,D.IcaoID AS DIcaoID  
  ,A.IcaoID AS AIcaoID  
  ,POSTFLEET.HoursVal  
     ,AircraftBasis = @AircraftBasis  
  ,TenToMin = @TenToMin  
  ,F.InspectionHrs  
  ,F.LastInspectionDT  
  ,F.WarningHrs   
 FROM PreflightMain PM   
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
               INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
               INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID  
               LEFT OUTER JOIN Airport D ON PL.DepartICAOID=D.AirportID  
               LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID  
               LEFT OUTER JOIN (    
        SELECT SUM(CASE WHEN @AircraftBasis=2 THEN FlightHours ELSE BlockHours END) HoursVal,F.FleetID   
          FROM PostflightMain PM   
             INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0  
             INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
          WHERE PM.CustomerID=@UserCustomerID  
           AND PM.IsDeleted=0  
            AND CONVERT(DATE,PL.ScheduledTM) >   CONVERT(DATE,F.LastInspectionDT)  
           GROUP BY F.FleetID)POSTFLEET ON POSTFLEET.FleetID=F.FleetID  
 WHERE F.IsDeleted=0  
   --AND F.IsInActive=0  
   AND PM.IsDeleted = 0  
   AND PM.TripStatus IN ('T','H')
   AND CONVERT(DATE,PL.DepartureDTTMLocal)  > CONVERT(DATE,GETDATE())   
   AND F.LastInspectionDT IS NOT NULL   
    ORDER BY TailNum,PL.DepartureDTTMLocal             
  
  
    
    
IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL  
DROP TABLE #TEMP   
      
END  
  
  
  
  
GO


