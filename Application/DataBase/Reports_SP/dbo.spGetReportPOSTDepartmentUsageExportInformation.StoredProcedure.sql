IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentUsageExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentUsageExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentUsageExportInformation]
	(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@DepartmentGroupID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
	)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentUsageExportInformation
-- Author:  A.Akhila
-- Create date: 17 Aug 2012
-- Description: Get Department Usage for EXPORT REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =0
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempFleetID TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT,
	TailNum VARCHAR(10)
	)

IF @TailNUM <> ''
	BEGIN
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum 
		FROM Fleet F
		WHERE F.CustomerID = @CUSTOMER
			AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
	END
IF @FleetGroupCD <> ''
	BEGIN 
		INSERT INTO @TempFleetID
		SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
		FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
			AND F.CustomerID = @CUSTOMER 
	END
ELSE IF @TailNUM = '' AND @FleetGroupCD = ''
	BEGIN 
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMER 
	END

-----------------------------TailNum and Fleet Group Filteration----------------------
-----------------------------Department Group Filteration----------------------
DECLARE @TempDepartmentID TABLE 
	( 
	ID INT NOT NULL IDENTITY (1,1), 
	DEPARTMENTID BIGINT
	)
IF @DepartmentGroupID <> '' 
	BEGIN 
	INSERT INTO @TempDepartmentID
	SELECT DISTINCT D.DepartmentID
	FROM Department D 
		LEFT OUTER JOIN DepartmentGroupOrder DGO ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID
		LEFT OUTER JOIN DepartmentGroup DG ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID
	WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, ',')) 
		AND D.CustomerID = @CUSTOMER 
	END
-----------------------------Department Group Filteration----------------------


CREATE TABLE #DEPARTINFO
	(
	[DEPARTMENTID] BIGINT
	,[Date Range] NVARCHAR(100)
	,[lognum] BIGINT
	,[reqdept] NVARCHAR(8)
	,[reqauth ] NVARCHAR(8)		                       
	,[ac_code ] CHAR(4)
	,[legid] BIGINT
	,[dept_code] NVARCHAR(8)
	,[deptdesc] NVARCHAR(25)
	,[auth_code] NVARCHAR(8)
	,[authdesc] NVARCHAR(25)
	,[activity] NVARCHAR(10)
	,[tot_line] NVARCHAR(10)
	,[leg_cnt] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[inact] BIT
	,[pax] BIGINT
	)
INSERT INTO #DEPARTINFO
SELECT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[Date Range] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
	,[lognum] = '0'--POM.LogNum
	,[reqdept] = ' '--D.DepartmentCD					              
	,[reqauth ] = ' '--DA.AuthorizationCD				                       
	,[ac_code ] = F.AircraftCD
	,[legid] = '0'--POL.POLegID
	,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                 
	,[deptdesc] = CASE WHEN D.DepartmentCD = ' ' OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END                
	,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END                                                                         
	,[authdesc] = CASE WHEN DA.AuthorizationCD = ' ' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END    
	,[activity] = ' ' 
	,[tot_line] =' '
	,[leg_cnt] = COUNT(POL.POLegID)
	,[tail_nmbr] = F.TailNum
	,[blk_hrs] = ROUND(POL.BlockHours,1)
	,[flt_hrs] = ROUND(POL.FlightHours,1)
	,[distance] = POL.Distance
	,[inact] = D.IsInActive
	,[pax] = POL.PassengerTotal
FROM PostflightLeg POL
	LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
	LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
	LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
	LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
	INNER JOIN ( SELECT DISTINCT DEPARTMENTID FROM @TempDepartmentID ) D1 ON D1.DEPARTMENTID = D.DepartmentID
WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 
    AND POL.IsDeleted=0
	AND POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
	GROUP BY D.DEPARTMENTID, pol.legnum, POM.LogNum, F.AircraftCD, POL.POLegID, D.DepartmentCD, D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription, F.TailNum , POL.BlockHours, POL.FlightHours, POL.Distance, D.IsInActive, POL.PassengerTotal
IF @DepartmentGroupID=''
	BEGIN
		INSERT INTO #DEPARTINFO
		SELECT
			[DEPARTMENTID] = D.DEPARTMENTID
			,[Date Range] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
			,[lognum] = '0'--POM.LogNum
			,[reqdept] = ' '--D.DepartmentCD					              
			,[reqauth ] = ' '--DA.AuthorizationCD				                       
			,[ac_code ] = F.AircraftCD
			,[legid] = '0'--POL.POLegID
			,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                 
			,[deptdesc] = CASE WHEN D.DepartmentCD = ' ' OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END                
			,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END                                                                         
			,[authdesc] = CASE WHEN DA.AuthorizationCD = ' ' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END    
			,[activity] = ' '
			,[tot_line] =' '
			,[leg_cnt] = COUNT(POL.POLegID)
			,[tail_nmbr] = F.TailNum
			,[blk_hrs] = ROUND(POL.BlockHours,1)
			,[flt_hrs] = ROUND(POL.FlightHours,1)
			,[distance] = POL.Distance
			,[inact] = isnull(D.IsInActive,0)
			,[pax] = POL.PassengerTotal
		FROM PostflightLeg POL
			LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
			LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
			INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID
			LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
			LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
			LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
			LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
		WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 
		    AND POL.IsDeleted=0
			AND POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
			AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
			AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
			AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
			AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)
		GROUP BY D.DEPARTMENTID, pol.legnum, POM.LogNum, F.AircraftCD, POL.POLegID, D.DepartmentCD, D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription, F.TailNum , POL.BlockHours, POL.FlightHours, POL.Distance, D.IsInActive, POL.PassengerTotal
	END
CREATE TABLE #DEPARTEMPTY
	(
	[DEPARTMENTID] BIGINT
	,[Date Range] NVARCHAR(100)
	,[lognum] BIGINT
	,[reqdept] NVARCHAR(8)
	,[reqauth ] NVARCHAR(8)		                       
	,[ac_code ] CHAR(4)
	,[legid] BIGINT
	,[dept_code] NVARCHAR(8)
	,[deptdesc] NVARCHAR(25)
	,[auth_code] NVARCHAR(8)
	,[authdesc] NVARCHAR(25)
	,[activity] NVARCHAR(10)
	,[tot_line] NVARCHAR(10)
	,[leg_cnt] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[inact] BIT
	,[pax] BIGINT
	)
INSERT INTO #DEPARTEMPTY
	SELECT
		[DEPARTMENTID] = D.DEPARTMENTID
		,[Date Range] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
		,[lognum] = '0'
		,[reqdept] = ''				              
		,[reqauth ] = ''			                       
		,[ac_code ] = NULL
		,[legid] = '0'
		,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                 
		,[deptdesc] = CASE WHEN D.DepartmentCD = ' ' OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END                
		,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END                                                                         
		,[authdesc] = CASE WHEN DA.AuthorizationCD = ' ' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END    
		,[activity] = ''                    --have dependency with charter quoter (cqfile)
		,[tot_line] =NULL
		,[leg_cnt] = NULL
		,[tail_nmbr] =NULL
		,[blk_hrs] = NULL
		,[flt_hrs] = NULL
		,[distance] = NULL
		,[inact] = 0
		,[pax] = NULL
	FROM DEPARTMENT D
		LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID
		INNER JOIN ( SELECT DISTINCT DEPARTMENTID FROM @TempDepartmentID ) D1 ON D1.DEPARTMENTID = D.DepartmentID
	WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsDeleted=0 AND D.IsInActive=0
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
		DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
IF @DepartmentGroupID=''
	BEGIN
	INSERT INTO #DEPARTEMPTY
		SELECT
			[DEPARTMENTID] = D.DEPARTMENTID
			,[Date Range] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
			,[lognum] = '0'
			,[reqdept] = ''				              
			,[reqauth ] = ''			                       
			,[ac_code ] = NULL
			,[legid] = '0'
			,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                 
			,[deptdesc] = CASE WHEN D.DepartmentCD = ' ' OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END                
			,[auth_code] = CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END                                                                         
			,[authdesc] = CASE WHEN DA.AuthorizationCD = ' ' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END    
			,[activity] = ''                    --have dependency with charter quoter (cqfile)
			,[tot_line] =NULL
			,[leg_cnt] = NULL
			,[tail_nmbr] =NULL
			,[blk_hrs] = NULL
			,[flt_hrs] = NULL
			,[distance] = NULL
			,[inact] = 0
			,[pax] = NULL
		FROM DEPARTMENT D
			LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID
		WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsDeleted=0 AND D.IsInActive=0
			AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
			AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
			DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
	END

IF @IsZeroSuppressActivityDeptRpt=0
BEGIN
	SELECT * FROM #DEPARTINFO
UNION ALL
	SELECT * FROM #DEPARTEMPTY
	ORDER BY [dept_code], [auth_code], [ac_code ], [tail_nmbr]
END
ELSE
BEGIN
	SELECT * FROM #DEPARTINFO
	ORDER BY [dept_code], [auth_code], [ac_code ], [tail_nmbr]
END


IF OBJECT_ID('tempdb..#DEPARTINFO') IS NOT NULL
DROP TABLE #DEPARTINFO 	

IF OBJECT_ID('tempdb..#DEPARTEMPTY') IS NOT NULL
DROP TABLE #DEPARTEMPTY 	


--EXEC spGetReportPOSTDepartmentUsageExportInformation 'SUPERVISOR_99','2009-12-01','2010-1-1', '', '', '', '', '', 0


GO


