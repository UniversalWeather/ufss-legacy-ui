IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRCorporateAircraftApprovalForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRCorporateAircraftApprovalForm]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCRCorporateAircraftApprovalForm] 
( 
    @UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30), --Mandatory
	@CRTripNUM AS VARCHAR(500),
	@CRLegNum AS VARCHAR(500)=''
)
AS
-- =============================================
-- SPC Name:spGetReportCRCorporateAircraftApprovalForm
-- Author: Askar
-- Create date:07th May 2013
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON

 DECLARE @TenToMin INT
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                                                              AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 


DECLARE @TripID BIGINT
SELECT @TripID=TripID FROM CRMain WHERE CRTripNUM=@CRTripNUM AND CustomerID=@UserCustomerID


IF @TripID IS  NULL
BEGIN
SELECT DISTINCT  ---Group Header Information--
       TripNUM=CM.CRTripNUM 
      ,TailNo=F.TailNum
      ,Aircraft_Type=AT.AircraftCD
      ,Trip_Requestor= P.LastName + ', ' + ISNULL(P.FirstName,'')--CM.RequestorName
      ,Requestor_Phone=P.PhoneNum
      ,Trip_Dates= [dbo].[GetShortDateFormatByUserCD](@UserCD,TD.DepartureDTTMLocal)+' - '+[dbo].[GetShortDateFormatByUserCD](@UserCD,TD.ArrivalDTTMLocal)
      ----Detail----
      ,Leg=CL.LegNUM
      ,Date=CONVERT(DATE,CL.DepartureDTTMLocal)
      ,FromCity=DEP.CityName
      ,ToCity=ARR.CityName
      ,Flt_Hrs=ISNULL(CL.ElapseTM,0)
      ,Miles=CL.Distance
      ,No_Of_Passengers=CL.PassengerTotal
      ,Billed_To=D.DepartmentCD+' \ '+DA.AuthorizationCD
      ,Passengers=SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1)
      ,@TenToMin TenToMin

 FROM CRMain CM INNER JOIN  CRLeg CL ON CM.CRMainID=CL.CRMainID AND CL.IsDeleted = 0
                LEFT OUTER JOIN CRPassenger CP ON CP.CRLegID=CL.CRLegID
                LEFT OUTER JOIN Passenger P ON CM.PassengerRequestorID=P.PassengerRequestorID
                LEFT OUTER JOIN Fleet F ON CM.FleetID=F.FleetID
                LEFT OUTER JOIN Aircraft AT ON CM.AircraftID=AT.AircraftID
                LEFT OUTER JOIN Department D ON CL.DepartmentID=D.DepartmentID
                LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=CL.AuthorizationID
                LEFT OUTER JOIN Airport DEP ON CL.DAirportID=DEP.AirportID
                LEFT OUTER JOIN Airport ARR ON ARR.AirportID=CL.AAirportID 
                LEFT OUTER JOIN ( SELECT DISTINCT PP2.CRLegID, PaxNames = (
                       SELECT ISNULL(RIGHT(P1.FirstName,1),'') + '. ' + ISNULL(P1.LastName,'') + ', ' 
                               FROM CRPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerRequestorID=P1.PassengerRequestorID 
                               WHERE PP1.CRLegID = PP2.CRLegID
                             FOR XML PATH(''))
                              FROM CRPassenger PP2
				   ) PAX ON CL.CRLegID = PAX.CRLegID 
				 
                INNER JOIN ( SELECT CM.CRMainID,CM.CRTripNUM,MIN(CL.DepartureDTTMLocal) DepartureDTTMLocal,MAX(CL.ArrivalDTTMLocal) ArrivalDTTMLocal FROM CRMain CM INNER JOIN CRLeg CL ON CM.CRMainID=CL.CRMainID
                                                                                                WHERE CM.CRTripNUM=@CRTripNUM 
                                                                                                 AND CM.CustomerID=@UserCustomerID GROUP BY CM.CRMainID,CM.CRTripNUM)TD ON TD.CRMainID=CM.CRMainID AND TD.CRTripNUM=CM.CRTripNUM        
  WHERE CM.CustomerID=CONVERT(BIGINT,@UserCustomerID)
    AND CM.CRTripNUM=CONVERT(BIGINT,@CRTripNUM)
    AND (CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
    AND CM.IsDeleted = 0
  ORDER BY CL.LegNUM
  
END
ELSE
BEGIN
SELECT DISTINCT  ---Group Header Information--
       TripNUM=CM.CRTripNUM 
      ,TailNo=F.TailNum
      ,Aircraft_Type=AT.AircraftCD
      ,Trip_Requestor= P.LastName + ', ' + ISNULL(P.FirstName,'')--CM.RequestorName
      ,Requestor_Phone=P.PhoneNum
      ,Trip_Dates= [dbo].[GetShortDateFormatByUserCD](@UserCD,TD.DepartureDTTMLocal)+' - '+[dbo].[GetShortDateFormatByUserCD](@UserCD,TD.ArrivalDTTMLocal)
      ----Detail----
      ,Leg=PL.LegNUM
      ,Date=CONVERT(DATE,PL.DepartureDTTMLocal)
      ,FromCity=DEP.CityName
      ,ToCity=ARR.CityName
      ,Flt_Hrs=ISNULL(PL.ElapseTM,0)
      ,Miles=PL.Distance
      ,No_Of_Passengers=PL.PassengerTotal
      ,Billed_To=D.DepartmentCD+' \ '+DA.AuthorizationCD
     ,Passengers=SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1)
     ,@TenToMin TenToMin

 FROM CRMain CM INNER JOIN  CRLeg CL ON CM.CRMainID=CL.CRMainID AND CL.IsDeleted = 0 
                INNER JOIN PreflightMain PM ON CM.TripID=PM.TripID AND PM.IsDeleted = 0
               INNER JOIN  PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
               INNER JOIN Fleet F ON F.FleetID=PM.FleetID
               LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
               LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID=P.PassengerRequestorID
               LEFT OUTER JOIN Airport DEP ON PL.DepartICAOID=DEP.AirportID
               LEFT OUTER JOIN Airport ARR ON ARR.AirportID=PL.ArriveICAOID
               LEFT OUTER JOIN Department D ON PL.DepartmentID=D.DepartmentID
               LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
               LEFT OUTER JOIN Aircraft AT ON F.AircraftID=AT.AircraftID
               LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT ISNULL(RIGHT(P1.FirstName,1),'') + '. ' + ISNULL(P1.LastName,'') + ', ' 
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID 
			 INNER JOIN ( SELECT CM.CRMainID,CRTripNUM,PL.TripID,MIN(PL.DepartureDTTMLocal) DepartureDTTMLocal,MAX(PL.ArrivalDTTMLocal) ArrivalDTTMLocal FROM CRMain CM INNER JOIN PreflightMain PM ON PM.TripID=CM.TripID AND PM.IsDeleted = 0
			                                                                                     INNER JOIN PreflightLeg PL ON CM.TripID=PL.TripID AND PL.IsDeleted = 0
                                                                                                WHERE CM.CRTripNUM=@CRTripNUM
                                                                                                AND CM.CustomerID=@UserCustomerID
                                                                                                AND CM.IsDeleted = 0
                                                                                                GROUP BY CM.CRMainID,CRTripNUM,PL.TripID,PM.TripNUM   
                        )TD ON TD.CRMainID=CM.CRMainID AND TD.CRTripNUM=CM.CRTripNUM
                
  WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID)
    AND CM.CRTripNUM=CONVERT(BIGINT,@CRTripNUM)
    AND (CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
    AND CM.IsDeleted = 0
  ORDER BY PL.LegNUM
  
 END
 
 
 

          
                        
END
--EXEC spGetReportCRCorporateAircraftApprovalForm 'SUPERVISOR_99','10099','10'


--SELECT * FROM CRMain WHERE CRTripNUM=9



GO


