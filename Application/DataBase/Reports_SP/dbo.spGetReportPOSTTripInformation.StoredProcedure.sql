IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTTripInformation] 
( 
	@UserCD AS VARCHAR(30), --Mandatory 
	@UserCustomerID AS VARCHAR(30),          
	@DATEFROM AS DATE , --Mandatory  
	@DATETO AS DATE , --Mandatory  
	@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@LogNum NVARCHAR(1000) = '',
	@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase
	@UserHomebaseID AS VARCHAR(30)  
 )   
AS 
-- ================================================================================ 
-- SPC Name: [spGetReportPOSTTripInformation]  
-- Author: HARIHARAN S  
-- Create date: 01/30/2013  
-- Description: Get Trip Detail for REPORT
-- Revision History  
-- Date   Name  Ver  Change  
-- ================================================================================
BEGIN            
SET NOCOUNT ON  
         
DECLARE @TenToMin SMALLINT = 0;  
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @TenToMin = TimeDisplayTenMin,
@AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 

-----------------------------TailNum and Fleet Group Filteration----------------------
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------
SELECT DISTINCT
[LogNumber] = POM.LogNum
,[Leg] = POL.LegNUM
--,[Tripdate] = CONVERT(DATE,POM.EstDepartureDT)
,CONVERT(DATE ,POL.OutboundDTTM) TRIPDATE
,[TailNumber] = F.TailNum
,[Rqstr] = PP.PassengerRequestorCD
,[DepICAO] = AD.IcaoID
,[ArrICAO] = AA.IcaoID
,[Miles] = POL.Distance
,[FlightHours] = ROUND(POL.FlightHours,1)
,[BlockHours] = ROUND(POL.BlockHours,1)
,[NbrPax] = POL.PassengerTotal
,[EmptySeats] = ISNULL(F.MaximumPassenger,0) - ISNULL(POL.PassengerTotal,0)
,[DeptCD] = DE.DepartmentCD
,[AuthCD] = DA.AuthorizationCD
,[ChargeRate] = FCR.ChargeRate
,[Aircraft Type] = AF.AircraftCD 
,SUBSTRING(ACrew.AssociatedCrew,1,LEN(ACrew.AssociatedCrew)-1) CrewCD
,[Per] = FCR.ChargeUnit
,[TotalChargesLEG] = --(CASE WHEN CONVERT(DATE,POM.EstDepartureDT) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						--THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							--)ELSE
						--NULL
						--END) 
						+ (ISNULL(POE.ExpenseAmt,0))

,[Code] = PAX.CODE
,[PassengerName] = PAX.PASSENGERNAME
,[PassengerBilling] = PAX.PASSENGERBILLING
,[Dept] = PAX.DEPT
,[Auth] = PAX.AUTH
,[PAXTOTAL] = POL.PassengerTotal
,[TotalChargesPax] = (REPLACE(Leg, LegNUM,
						'$'+CONVERT(VARCHAR(20),CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
							FLOOR((
							((CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END)) 
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							NULL
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal)*100)*0.01
						END)
						))
,[Account] = EXC.ACCOUNT
,[ExpenseAmount] = EXC.EXPENSEAMOUNT
--,[BillingCode] = CASE WHEN POL.LegNUM=1 THEN Bill.StandardBilling ELSE NULL END
--,[TotalChargesBILLING] = FLOOR(ISNULL(Bill.StdVal,0)*100)*0.01

,[TenToMin] = @TenToMin
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POL.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0 
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
INNER JOIN Aircraft AF ON AF.AircraftID = F.AircraftID
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
--LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON POL.AuthorizationID = DA.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN FleetChargeRate FCR ON F.FleetID = FCR.FleetID AND F.CustomerID = FCR.CustomerID AND POM.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT
INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID
LEFT OUTER JOIN ( SELECT DISTINCT PP2.ExpenseAMT,PP2.POLegID,PP2.AccountID,
					  ACCOUNT = (
                      SELECT ISNULL(P1.AccountDescription,'')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH('')),
                      EXPENSEAMOUNT = (
                      SELECT ISNULL(CONVERT(VARCHAR,PP1.ExpenseAMT),'')+'$$' 
                      FROM PostflightExpense PP1 INNER JOIN Account P1 ON PP1.AccountID=P1.AccountID 
                               WHERE PP1.POLegID = PP2.POLegID AND PP1.IsBilling=1
                               FOR XML PATH(''))
                      FROM PostflightExpense PP2
                     
				 ) EXC ON POL.POLegID = EXC.POLegID 
                               
LEFT OUTER JOIN ( SELECT DISTINCT PP2.PostflightPassengerListID,PP2.POLegID,PP2.FlightPurposeID,
					  PASSENGERNAME = (
                      SELECT ISNULL(P1.LastName+', ','') + ISNULL(P1.FirstName,'') +' '+ ISNULL(P1.MiddleInitial,'')+'$$' 
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.PassengerRequestorCD
                               FOR XML PATH('')),
                      CODE = (
                      SELECT ISNULL(P1.PassengerRequestorCD,'')+'$$'
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.PassengerRequestorCD
                               FOR XML PATH('')),
                       Leg = (
                      SELECT ISNULL(CONVERT(VARCHAR(2),PL.LegNUM),'')+'$$'
                      FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID
                                                   INNER JOIN PostflightLeg PL ON PP1.POLegID=PL.POLegID AND PL.IsDeleted = 0 
                               WHERE PP1.POLegID = PP2.POLegID
                               FOR XML PATH('')),
                               
                  	  PASSENGERBILLING = (
						  SELECT ISNULL(PP1.Billing,'')+'$$'
						  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
								   WHERE PP1.POLegID = PP2.POLegID
								   ORDER BY P1.PassengerRequestorCD
								   FOR XML PATH('')),
				      DEPT = (
		                  SELECT ISNULL(D.DepartmentCD,'')+'$$'
		                  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
													   LEFT OUTER JOIN Department D ON D.DepartmentID = P1.DepartmentID
				                   WHERE PP1.POLegID = PP2.POLegID
				                   ORDER BY P1.PassengerRequestorCD
				                   FOR XML PATH('')),
					  AUTH = (
		                  SELECT ISNULL(A.AuthorizationCD,'')+'$$'
		                  FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
													   LEFT OUTER JOIN DepartmentAuthorization A ON A.AuthorizationID = P1.AuthorizationID
				                   WHERE PP1.POLegID = PP2.POLegID
				                   ORDER BY P1.PassengerRequestorCD
				                   FOR XML PATH(''))
					  FROM PostflightPassenger PP2
				 ) PAX ON POL.POLegID = PAX.POLegID  

LEFT OUTER JOIN Account A ON POE.AccountID = A.AccountID AND POE.CustomerID = A.CustomerID
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
LEFT OUTER JOIN (
						SELECT DISTINCT PC1.POLegID, AssociatedCrew = (
						SELECT RTRIM(C.CrewCD) + ','
						FROM PostflightCrew PC 
						INNER JOIN Crew C ON PC.CrewID = C.CrewID
						WHERE PC.POLegID = PC1.POLegID
						order by C.CrewCD
						FOR XML PATH('')
						)
						FROM PostflightCrew PC1
						) ACrew ON POL.POLegID = ACrew.POLegID 



--LEFT OUTER JOIN 
--(SELECT TEMP.POLogID,TEMP.LogNum,TEMP.CustomerID,TEMP.StandardBilling,SUM(TEMP.StdVAlues) StdVal  FROM 
--(SELECT DISTINCT POM.POLogID,PL.LegNUM,PL.PassengerTotal,P.PassengerRequestorCD,POM.LOGNUM,P.StandardBilling,POM.CustomerID,
--									(CASE WHEN PL.PassengerTotal=0 
--							THEN 0 
--							ELSE 
--							(((CASE WHEN CONVERT(DATE,PL.ScheduleDTTMLocal) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
--								THEN(
--									CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.BlockHours,0)) 
--									WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0))
--									WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(PL.Distance,0) * 1.1508) END
--									)ELSE
--								NULL
--								END) 
--								+ (POE.ExpenseAmt))/PL.PassengerTotal)
--							END)
--								 StdVAlues
--FROM PostflightMain POM
--		LEFT OUTER JOIN PostflightLeg PL ON POM.POLogID = PL.POLogID
--		LEFT OUTER JOIN (
--					SELECT POLegID,PM.CustomerID, ChargeUnit, ChargeRate, BeginRateDT, EndRateDT
--					FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
--										   INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID 
--					AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduleDTTMLocal) AND EndRateDT>=CONVERT(DATE,PL.ScheduleDTTMLocal)
--				   ) FCR ON PL.POLegID = FCR.POLegID
--		LEFT OUTER JOIN PostflightPassenger POP ON PL.POLegID=POP.POLegID
--		LEFT OUTER JOIN PostflightExpense POE ON POE.POLegID = PL.POLegID
--        LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
--		 WHERE POM.CUSTOMERID=DBO.GetCustomerIDbyUserCD(@UserCD)
--)TEMP
--GROUP BY TEMP.StandardBilling,TEMP.POLogID,TEMP.StdVAlues,TEMP.CustomerID,TEMP.LogNum) Bill ON Bill.POLogID = POM.POLogID





WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
    AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0
END      
--EXEC spGetReportPOSTTripInformation 'SUPERVISOR_99','10099','01-01-2009','03-01-2013','','','3','','',0,'' 


GO


