IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetScheduleAircraftExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetScheduleAircraftExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPREFleetScheduleAircraftExportInformation]

		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
		
AS
-- =============================================
-- SPC Name: spGetReportPREFleetScheduleDateAircraftExportInformation
-- Author: SINDHUJA.K
-- Create date: 19 July 2012
-- Description: Get Preflight Fleet Schedule/Aircraft Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON  
	    DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';	    
		Declare @FromDate datetime  
		Declare @ToDate datetime  
		Declare @Count int  
		Declare @index int  

		Set @FromDate = @DATEFROM  
		Set @ToDate = @DATETO  

	DECLARE  @TempTable  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1),   
		PDate DateTime,  
		activedate nvarchar(25),
		--tail_nmbr bigint,
		tail_nmbr varchar(30),
		deptime datetime,
		arrtime datetime,
		depicao_id bigint,
		arricao_id bigint,
		dep_city varchar(25),
		arr_city varchar(25),
		dep_airport varchar(25),
		arr_airport varchar(25),
		dep_state varchar(25),
		arr_state varchar(25),
		dep_country varchar(60),
		arr_country varchar(60),
		elp_time numeric(7,3),
		[desc]  varchar(60),
		pax_total int	     
	)  

	Set @Count=DATEDIFF(DAY,@FromDate,@ToDate)  
	Set @index=0  

	while(@index<=@Count)  
	Begin  
			insert into @TempTable 
			(
				PDate,
				activedate,
				tail_nmbr
				
			)   
			values (
					convert(varchar(10),DATEADD(DAY,@index,@FromDate),121),
					LEFT(DATENAME(WEEKDAY, DATEADD(DAY,@index,@FromDate)),3) + ' ' +
					RIGHT(CONVERT(VARCHAR(12), DATEADD(DAY,@index,@FromDate), 101),9),
					(select F.TailNum from FLEET f where TailNum in(@TailNUM))
				   )  
		Set @index=@index+1  
	End  
	
		set @SQLSCRIPT='Update @TempTable   
	  
			 Set [tail_nmbr] = F.TailNum,
			[deptime] = PL.DepartureDTTMLocal,
			[arrtime] = PL.ArrivalDTTMLocal, 
			[depicao_id] =  PL.DepartICAOID,
			[arricao_id]  =  PL.ArriveICAOID,
			[dep_city] = A.CityName,
			[arr_city] = A.CityName,
			[dep_airport] = A.AirportName,
			[arr_airport] = A.AirportName,
			[dep_state] = A.StateName,
			[arr_state] = A.StateName,
			[dep_country] = C.CountryName,
			[arr_country] = C.CountryName,
			[elp_time] = PL.ElapseTM,
			[desc] = C.CountryName,
			[pax_total] = PL.PassengerTotal
  
		FROM   
		 PreflightMain PM   
			 Inner Join PreflightLeg PL On PM.TripID = PL.TripID AND PL.ISDELETED = 0  
			 Inner Join FLEET F On PM.FleetID=F.FleetID  
			 Inner Join AIRPORT A On PL.DepartICAOID=A.AirportID  
			 Inner Join Country C On A.CountryID=C.CountryID 
			 LEFT OUTER JOIN FleetGroupOrder FG ON F.FleetID = FG.FleetID  
		Where CONVERT(varchar, PM.EstDepartureDT, 101)=CONVERT(varchar, PDate,101)
		AND PM.ISDELETED = 0'
		

	  --Construct OPTIONAL clauses
	IF @TailNUM <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END
	IF @FleetGroupCD <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupID IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	END	
	
	Select 
	        tail_nmbr,
			activedate,
	        deptime,
	        arrtime,
			depicao_id,
			arricao_id,
			dep_city,
			arr_city,
			dep_airport,
			arr_airport,
			dep_state,
			arr_state,
			dep_country,
			arr_country,
			elp_time,
			[desc],
			pax_total
	  from @TempTable 
    
  -- EXEC spGetReportPREFleetScheduleAircraftExportInformation 'UC','2012/2/3','2012/2/5','',''

GO


