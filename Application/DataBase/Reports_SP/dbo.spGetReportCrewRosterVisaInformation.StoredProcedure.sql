IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewRosterVisaInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewRosterVisaInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportCrewRosterVisaInformation]
	@UserCD VARCHAR(30)
	, @CrewCD VARCHAR(7)
AS
-- =============================================
-- SPC Name: spGetReportCrewRosterVisaInformation
-- Author: SUDHAKAR J
-- Create date: 31 May 2012
-- Description: Get CrewChecklist information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- 
-- =============================================
SET NOCOUNT ON

DECLARE @CUSTOMERID BIGINT;
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT 
	CPV.VisaTYPE, 
	--[VisaNum] = dbo.FlightPakDecrypt(ISNULL(CPV.VisaNum,'')),
	[VisaNum] = ISNULL(CPV.VisaNum,''),
	dbo.GetShortDateFormatByUserCD(@UserCD, CPV.ExpiryDT) AS ExpiryDT, 
	CO.CountryCD, 
	CPV.Notes
	FROM CrewPassengerVisa CPV
	INNER JOIN Crew C ON CPV.CrewID = C.CrewID
	LEFT OUTER JOIN Country CO ON CPV.CountryID = CO.CountryID
	WHERE C.CrewCD = @CrewCD
	AND CPV.IsDeleted=0
	AND C.CustomerID = @CUSTOMERID

-- EXEC spGetReportCrewRosterVisaInformation 'jwilliams_13', '1GV'



GO


