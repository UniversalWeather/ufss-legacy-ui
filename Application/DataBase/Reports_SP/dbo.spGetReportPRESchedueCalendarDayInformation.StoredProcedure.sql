IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarDayInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarDayInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
EXEC spGetReportPRESchedueCalendarDayInformation 'Ajeet', '10002', '2014-12-21',1,'2014-12-27','','','','','','',@Footer=0
*/
CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarDayInformation]  
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)---=10098
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfDays INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@SortBy INT = 1 --1-TailNum,2-Dep Date/Time
	  ,@IsActivityOnly BIT=0
	  ,@IsFirstLastLeg BIT=0
	  ,@IsCrewCalActivity	BIT=0
	  ,@IsShowTripStatus BIT=0
	  ,@IsCummulativeETE BIT=1
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarWeeklyFleetInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Day information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
SET NOCOUNT ON    
 DECLARE @DATEFROM DATETIME=@BEGINDATE,@DATETO DATETIME=@ENDDATE   
 DECLARE @TotalEteTemp TABLE     
(       
 TripId BIGINT,     
 TotalETE numeric(14,3)    
)
INSERT INTO @TotalEteTemp(TripId, TotalETE)  
SELECT TripId, ISNULL(SUM(ElapseTM),0) 
FROM PreflightLeg 
WHERE CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND IsDeleted = 0 
GROUP BY TripID
 
 SELECT @ECrew=CASE WHEN ISNULL(@EFleet,'')<>'' THEN '' ELSE @ECrew END  
SELECT @BlackWhiteClr = CASE WHEN @Color = 0 THEN 1 ELSE @BlackWhiteClr END
   

		DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
										WHEN @Vendors=2 THEN 1 
										WHEN @Vendors=3 THEN 0
										END)  
               
	
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		


IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                AND UM.UserName = RTRIM(@UserCD)

END


DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9),CrewID BIGINT)



 DECLARE @TableTrip TABLE(ID INT IDENTITY,
						  TripNUM BIGINT,
                          FlightNUM VARCHAR(12),
                          PassengerTotal INT,
                          PassengerRequestorCD VARCHAR(5),
                          DepartmentCD VARCHAR(8),
                          AuthorizationCD VARCHAR(8),
                          TripDescription VARCHAR(40),
                          FlightPurpose VARCHAR(40),
                          SeatsAvailabe INT,
                          CrewList VARCHAR(300),
                          ETE NUMERIC(7,3),
                          TripStatus CHAR(2),
                          FlightCatagoryCD CHAR(4),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          FleetID BIGINT,
                          TailNum VARCHAR(9),
                          TypeCode VARCHAR(10),
                          DutyTYPE CHAR(2),
                          NextLocalDTTM DATETIME,
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(25),
                          ArrivelICAOID VARCHAR(25),
                          DepCity VARCHAR(25),
                          ArrCity VARCHAR(25),
                          DepState VARCHAR(25),
                          ArrState VARCHAR(25),
                          LegNum BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          IsApprox BIT,
                          FuelLoad DECIMAL,
                          OutBoundInstruction VARCHAR(30),
                          CrewID BIGINT,
                          CrewCD VARCHAR(5),
                          CrewVal VARCHAR(5),
                          CrewValID BIGINT,
                          FgColor VARCHAR(8),
                          BgColr  VARCHAR(8),
                          FirstLastLeg CHAR(1)
                          )
 


 ----INSERT FOR GIVEN DATE RANGE----  
 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,PM.FlightNUM
     ,PL.PassengerTotal
     ,P.PassengerRequestorCD
     ,DEP.DepartmentCD
     ,DA.AuthorizationCD
     ,PM.TripDescription
     ,PL.FlightPurpose
     ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
     ,CrewCD=SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1)
     ,PL.ElapseTM
     ,PM.TripStatus
     ,FC.FlightCatagoryCD
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 'F' ELSE PL.DutyTYPE END
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=1 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=1 THEN A.CityName ELSE A.AirportName END
     ,D.CityName
     ,A.CityName
     ,D.StateName
     ,A.StateName
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,PL.IsApproxTM
     ,CAST(PL.FuelLoad AS DECIMAL)
     ,PL.OutbountInstruction
     ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
     ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
     ,CrewValID=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewID ELSE NULL END
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
     ,'Z'
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
            --  LEFT OUTER JOIN PreflightLeg PLN ON PL.TripID=PLN.TripID AND PL.LegNUM+1=PLN.LegNUM
              LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID AND PCL.IsDeleted = 0
			  LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
	   AND PM.IsDeleted = 0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
	   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


----INSERT FOR GIVEN DATE RANGE INCLUDING PREVIOUS LEGS REST RECORDS-----
 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,PM.FlightNUM
     ,PL.PassengerTotal
     ,P.PassengerRequestorCD
     ,DEP.DepartmentCD
     ,DA.AuthorizationCD
     ,PM.TripDescription
     ,PL.FlightPurpose
     ,SeatsAvailabe=PL.SeatTotal-PL.PassengerTotal
     ,CrewCD=SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1)
     ,ETE=PL.ElapseTM
     ,PM.TripStatus
     ,FC.FlightCatagoryCD
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 'F' ELSE PL.DutyTYPE END
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'  THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE  CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,D.CityName
     ,A.CityName
     ,D.StateName
     ,A.StateName
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID 
     ,PL.IsApproxTM
     ,CAST(PL.FuelLoad AS DECIMAL)
     ,PL.OutbountInstruction
     ,CASE WHEN TailNum IS NOT NULL THEN 0 ELSE PCL.CrewID END
     ,CASE WHEN TailNum IS NOT NULL THEN NULL ELSE CW.CrewCD END
     ,CrewVal=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewCD ELSE NULL END
     ,CrewValID=CASE WHEN ISNULL(@ECrew,'') <> '' THEN CW.CrewID ELSE NULL END
     ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
     ,'Z'
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              LEFT OUTER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID AND PCL.IsDeleted = 0
			  LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID

      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted = 0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (CW.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ECrew, ',')) OR @ECrew = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
      AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
	   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))

IF @IsCrewCalActivity=1
BEGIN
INSERT INTO @CurDate(DateWeek,CrewID)
SELECT T.*,CC.CrewID FROM Crew CC CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                 WHERE CC.CustomerID=CONVERT(BIGINT,@UserCustomerID)  AND CC.IsDeleted=0 AND CC.IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0)

END
ELSE
BEGIN
DELETE FROM @TableTrip WHERE RecordTye='C' AND FleetID IS NULL 

END
----INSERTING ALL THE DATES FOR EACH FLEET----
INSERT INTO @CurDate(DateWeek,FleetID,TailNum)
SELECT T.*,F.FleetID,F.TailNum FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')

 


-----FOR REST RECORDS-----
INSERT INTO @TableTrip(TripID,FleetID,TailNum,TypeCode,DepartureDTTMLocal,DepICAOID,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,
						TripNUM,LegNum,RecordTye,PassengerTotal,IsApprox,TripStatus,CrewList,ETE,FlightCatagoryCD,PassengerRequestorCD,CrewVal,CrewValID,FgColor,BgColr,FirstLastLeg,FlightNUM,FuelLoad)
SELECT  TT.TripID,CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,TT.ArrivelICAOID,'R',
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,
                TT.LegNum,RecordTye,TT.PassengerTotal,TT.IsApprox,TT.TripStatus,TT.CrewList,ETE=0,TT.FlightCatagoryCD,TT.PassengerRequestorCD,TT.CrewVal,TT.CrewValID,
                FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END),'A',FlightNUM,FuelLoad
                  FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                             WHERE CONVERT(DATE,CD.DateWeek)>CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                             --  AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)

-----FOR MAINTENANCE RECORDS-----
INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,TripDescription,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,DepICAOID,CrewVal,CrewValID,FgColor,BgColr,FirstLastLeg)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,TripDescription,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,TT.ArrivelICAOID,TT.CrewVal,TT.CrewValID,
                FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END),'Z' FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID  OR CD.CrewID=TT.CrewID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye IN('C','M')
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


                  
DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)



DECLARE @TempTable TABLE(ID INT IDENTITY
						,TripID Bigint
                        ,WeekNumber INT
                        ,FleetID BIGINT
                        ,TailNum VARCHAR(9)
                        ,TripNUM BIGINT
                        ,location VARCHAR(200) 
                        ,DepICAOID VARCHAR(25)
                        ,ArrICAOID VARCHAR(25)
                        ,FlightNUM VARCHAR(12)
                        ,pax_total INT 
                        ,reqcode VARCHAR(5)
                        ,DepartmentCD VARCHAR(8)
                        ,AuthorizationCD VARCHAR(8)
                        ,FlightPurpose VARCHAR(40)
                        ,SeatsAvailable INT
                        ,crewlist VARCHAR(300) 
                        ,ETE NUMERIC(7,3)
                        ,TripStatus CHAR(2)
                        ,FlightCatagoryCD VARCHAR(25) 
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,SORTORDER INT
                        ,RecordType VARCHAR(1)
                        ,TypeCode VARCHAR(10)
                        ,IsApprox BIT
                        ,FuelLoad NUMERIC(7,3)
                        ,OutBoundInstruction VARCHAR(30)
                        ,DutyType CHAR(2)
                        ,RnkFirst INT
                        ,RnkLast  INT
                        ,CrewVal VARCHAR(5)
                        ,CrewValID BIGINT
                         ,FgColor VARCHAR(8)
                        ,BgColr  VARCHAR(8)) 
 


INSERT INTO @TempTable   
SELECT TT.TripID,DATEPART(DAY, DATEDIFF(DAY, @BeginDate, DepartureDTTMLocal)/7 * 7)/7 + 1 WeekNumber
     ,TT.FleetID
      ,TT.TailNum
      ,TripNUM
	 ,CASE WHEN DutyTYPE='R' THEN '(R) '+ArrivelICAOID 
	       WHEN RecordTye='M' OR RecordTye='C' THEN '('+DutyTYPE+') '+SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)+'***'+(CASE WHEN CrewCD IS NOT NULL THEN CrewCD+'- ' ELSE '' END )+ISNULL(TripDescription,'')
	       ELSE  SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)+' '+DepICAOID+' | '+ArrivelICAOID+' '+SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5) END Location
     ,DepICAOID
     ,ArrivelICAOID
     ,FlightNUM
     ,PassengerTotal
     ,PassengerRequestorCD
     ,DepartmentCD
     ,AuthorizationCD
     ,FlightPurpose
     ,SeatsAvailabe
     ,CrewList
     ,ETE
     ,TripStatus
     ,FlightCatagoryCD
     ,CONVERT(DATE,DepartureDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,TT.IsApprox
     ,TT.FuelLoad
     ,TT.OutBoundInstruction
     ,TT.DutyTYPE
     ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,FirstLastLeg DESC,DepartureDTTMLocal ASC)
     ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,FirstLastLeg DESC,DepartureDTTMLocal DESC)
     ,TT.CrewVal
     ,TT.CrewValID
    ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M' THEN AD.ForeGrndCustomColor
									 WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C' THEN CDT.ForeGrndCustomColor
									 WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.ForeGrndCustomColor	
								     WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.ForeGrndCustomColor  ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD AND TT.RecordTye='M'  THEN AD.BackgroundCustomColor
							       WHEN TT.DutyTYPE=CDT.DutyTypeCD AND TT.RecordTye='C'  THEN CDT.BackgroundCustomColor 
							       WHEN TT.DutyTYPE='R' AND ISNULL(@EFleet,'')<>'' THEN AD.BackgroundCustomColor	
								   WHEN TT.DutyTYPE='R' AND ISNULL(@ECrew,'')<>'' THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)
      FROM @TableTrip TT  
      LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
	  LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      ORDER BY TT.TripID,TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum



 DECLARE @DateVal DATE=@BeginDate;

IF @IsActivityOnly=1
  
  BEGIN

      WHILE @DateVal <= @EndDate
       
       BEGIN
       
		INSERT INTO @TempTable(Date)
		SELECT DISTINCT @DateVal WHERE  NOT EXISTS(SELECT Date FROM @TempTable WHERE  Date=@DateVal)  
    
      SET @DateVal=DATEADD(DAY,1,@DateVal)
      
      END
  END
 
 ELSE
   
 BEGIN
   
    WHILE @DateVal <= @EndDate
      
      BEGIN

       INSERT INTO @TempTable(FleetID,TailNum,Date,TypeCode)
       SELECT DISTINCT F.FleetID,F.TailNum,@DateVal,AT.AircraftCD FROM Fleet F LEFT OUTER JOIN Aircraft AT ON F.AircraftID=AT.AircraftID
                                                                                        LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                              WHERE F.IsDeleted=0
                                               AND F.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                                               AND F.IsInActive=0
                                               AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                               AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
                                               AND F.FleetID NOT IN(SELECT ISNULL(FleetID,0) FROM @TempTable WHERE Date=@DateVal)  
      
       SET @DateVal=DATEADD(DAY,1,@DateVal)
     
  END
 END



----FINAL RETRIEVAL----
IF @IsFirstLastLeg=0
BEGIN
SELECT ID
      --,WeekNumber
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,TT.FleetID
      ,TypeCode
      ,TailNum
      ,Date SortDate
      ,LEFT(datename(dw,DATE),3)+' '+dbo.GetShortDateFormatByUserCD(@UserCD,Date) Date
      ,location
	  ,crewlist=CASE WHEN TT.FleetID IS NOT NULL THEN (CASE WHEN ISNULL(@ECrew,'')<>'' THEN CrewVal ELSE crewlist END) ELSE NULL END
	 ,TT.IsApprox
     ,TT.FuelLoad
     ,TT.OutBoundInstruction
     ,TT.TripNUM
     ,TT.TripStatus
     ,TT.DepartureDTTMLocal
     ,TT.ArrivalDTTMLocal
     ,CASE WHEN TT.ETE = 0 OR TT.DutyType='R' THEN NULL ELSE TT.ETE END ETE 
	 ,CASE WHEN CME.Total= 0 THEN NULL
			WHEN TT.RecordType='C' THEN  FLOOR(TT.ETE*10)*0.1
			WHEN TT.RecordType='M' THEN  FLOOR(TT.ETE*10)*0.1 ELSE CME.Total END CummETE
	  ,TE.TotalETE
     ,TT.DepICAOID
     ,TT.ArrICAOID
     ,TT.pax_total
     ,TT.DutyType
     ,TT.FlightNUM
     ,TT.FlightCatagoryCD
     ,TT.reqcode
     ,TT.AuthorizationCD
    ,CASE WHEN CME.Total= 0 THEN NULL ELSE CME.Total END Total
    ,CrewVal
    ,FgColor
    ,BgColr		
 FROM @TempTable TT
     LEFT OUTER JOIN (SELECT DISTINCT CrewValID,FleetID,TripNUM,DepartureDTTMLocal Departure,ETE, (SELECT SUM(ETE) 
																									FROM @TempTable b 
																									WHERE b.ID <= a.ID AND b.TripNUM=a.TripNUM AND a.FleetID=b.FleetID) AS Total 
					FROM @TempTable a)CME ON CME.Departure=TT.DepartureDTTMLocal AND CME.TripNUM=TT.TripNUM AND TT.FleetID=CME.FleetID AND ISNULL(TT.CrewValID,99)=ISNULL(CME.CrewValID,99)
	LEFT OUTER JOIN @TotalEteTemp TE on(TE.TripID = TT.TripID)
  WHERE DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY (CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER
END
ELSE
BEGIN

SELECT ID
      --,WeekNumber
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,TT.FleetID
      ,TypeCode
      ,TailNum
      ,Date SortDate
      ,LEFT(datename(dw,DATE),3)+' '+dbo.GetShortDateFormatByUserCD(@UserCD,Date) Date
      ,location
	  ,crewlist=CASE WHEN TT.FleetID IS NOT NULL THEN (CASE WHEN ISNULL(@ECrew,'')<>'' THEN CrewVal ELSE crewlist END) ELSE NULL END
	 ,TT.IsApprox
     ,TT.FuelLoad
     ,TT.OutBoundInstruction
     ,TT.TripNUM
     ,TT.TripStatus
     ,TT.DepartureDTTMLocal
     ,TT.ArrivalDTTMLocal
     ,CASE WHEN TT.ETE = 0 OR TT.DutyType='R' THEN NULL ELSE TT.ETE END ETE 
	 ,CASE WHEN CME.Total= 0 THEN NULL
			WHEN TT.RecordType='C' THEN  FLOOR(TT.ETE*10)*0.1
			WHEN TT.RecordType='M' THEN  FLOOR(TT.ETE*10)*0.1 ELSE CME.Total END CummETE
	  ,TE.TotalETE
     ,TT.DepICAOID
     ,TT.ArrICAOID
     ,TT.pax_total
     ,TT.DutyType
     ,TT.FlightNUM
     ,TT.FlightCatagoryCD
     ,TT.reqcode
     ,TT.AuthorizationCD
    ,CASE WHEN CME.Total= 0 THEN NULL ELSE CME.Total END Total
    ,CrewVal
    ,FgColor      
     ,BgColr		
 FROM @TempTable TT
       LEFT OUTER JOIN (SELECT DISTINCT CrewValID,FleetID,TripNUM,DepartureDTTMLocal Departure,ETE, (SELECT SUM(ETE) 
																									 FROM @TempTable b 
																									 WHERE b.ID <= a.ID AND b.TripNUM=a.TripNUM AND a.FleetID=b.FleetID) AS Total 
						FROM @TempTable a)CME ON CME.Departure=TT.DepartureDTTMLocal AND CME.TripNUM=TT.TripNUM AND TT.FleetID=CME.FleetID AND ISNULL(TT.CrewValID,99)=ISNULL(CME.CrewValID,99)
 LEFT OUTER JOIN @TotalEteTemp TE on(TE.TripID = TT.TripID)
  WHERE (RnkFirst=1 OR RnkLast=1 OR DutyType='R') 
   AND DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY (CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER

END

 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
 DROP TABLE #TempTable
 END
GO


