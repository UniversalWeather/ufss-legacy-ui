

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub]
   @UserCD AS VARCHAR(30),			-- Mandatory
   @TripID AS VARCHAR(200),         -- Mandatory
   @LegID AS VARCHAR(30)                -- Mandatory	
    
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSGeneralDeclarationSub
-- Author: ABHISHEK.S
-- Create date: 13th September 2012
-- Description: Get TripSheetReportWriter General Declaration Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
BEGIN
	
	SET NOCOUNT ON;	

	
	SELECT
	[TRIPID]       =  PM.TripID,
	[Leg]          =  PL.LegID,
	[OWNEROPRTR]   =  FP.OwnerLesse,
    [STATENREG]    =  FP.StateName +'  '+  F.TailNUM,
	[DEPFROMCITY]  =  A.CityName,  
	[DEPFROMCNTRY] =  A.CountryName,
	[DATE]         =  CONVERT(varchar(10), PL.DepartureDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD)),
	[ARRTOCITY]    =  B.CityName,
	[ARRTOCNTRY]   =  B.CountryName
	
	 FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL
				        ON PM.TripID = PL.TripID
				INNER JOIN (SELECT FleetID, TailNum FROM Fleet) F
				        ON PM.FleetID = F.FleetID
				INNER JOIN FleetPair FP
				        ON F.FleetID = FP.FleetID
				LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS A
			            ON A.AirportID = PL.DepartICAOID
		        LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS B
			            ON B.AirportID = PL.ArriveICAOID
			    --INNER JOIN 	@tblTripInfo T ON T.TripID = PM.TripID
			    --        AND T.LegID = PL.LegID							
				--INNER JOIN Country C
				--        ON FP.CountryID = C.CountryID			       
				
		     
	WHERE PM.IsDeleted = 0
	             AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	               AND CONVERT(VARCHAR,PM.TripID)  = @TripID
	               AND PL.LegID IN ( CONVERT(BIGINT,@LegID))		    
END
-- EXEC spGetReportPRETSGeneralDeclarationSub 'UC', '1000142' , ''

GO


