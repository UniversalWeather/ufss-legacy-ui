IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFuelLocators]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFuelLocators]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetReportFuelLocators]
    @UserCD AS VARCHAR(30),
    @UserCustomerID AS BIGINT
AS
BEGIN

SET NOCOUNT ON;

SELECT FuelCode = F.FuelLocatorCD,
       FuelDescription = F.FuelLocatorDescription,
       ClientCode = C.ClientCD 
 FROM FuelLocator F
      LEFT JOIN Client C 
        ON F.ClientID = C.ClientID AND F.CustomerID = C.CustomerID
 WHERE F.CustomerID = @UserCustomerID
   AND F.IsDeleted=0
   AND F.IsInActive=0
 ORDER BY FuelLocatorCD
END      
GO


