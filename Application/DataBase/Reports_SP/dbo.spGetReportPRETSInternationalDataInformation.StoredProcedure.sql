IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSInternationalDataInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSInternationalDataInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSInternationalDataInformation]
	 @UserCD  AS VARCHAR(30) 
	 ,@TripID  AS VARCHAR(30)  
	
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSInternationalDataInformation
-- Author: AISHWARYA.M
-- Create date:27 SEP 2012
-- Description: Get Preflight Tripsheet International Data information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================
	
	SELECT	[Trip Number]= PM.TripNUM
			,[Description]= PM.TripDescription
			,[Trip Dates]= dbo.GetTripDatesByTripIDUserCDForTripSheet(PM.TripID,@UserCD)
			,[Requestor]= ISNULL(P.LastName,'')+ ', ' +ISNULL(P.FirstName,'')+ ' ' +ISNULL(P.MiddleInitial,'')
			,[Tail Number]= F.TailNum
			,[Contact Phone]= P.PhoneNum
			,[Type]= A.AircraftCD
			,[Dispatch Number]= PM.DispatchNUM
			,[Aircraft]= F.TailNum
			,[Decal Number]= FP.BuyAircraftAdditionalFeeDOM
			,[Colors]= FP.AircraftColor1
			,[Colors2]= FP.AircraftColor2
			,[Aircraft Make]= FP.AircraftMake
			,[Aircraft Model]= FP.AircraftModel
			,[Trim]= FP.AircraftTrimColor
			,[OwnerLessee Name]= FP.OwnerLesse
			,[Street Address]= FP.Addr1 
			,[AptSuite Number]= FP.Addr2
			,[City]= FP.CityName
			,[State]= FP.StateName
			,[Country]= C.CountryCD
			,TripID = CONVERT(VARCHAR,PM.TripID)
			,[DispatcherName] = UM.FirstName + ' ' + UM.LastName --PM.DsptnName,
			,[DispatcherPhone] = UM.PhoneNum
			,[Dispatcheremail] = UM.EmailID
			
			FROM PreflightMain PM			
			INNER JOIN Fleet F ON  PM.FleetID = F.FleetID
			INNER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
			LEFT OUTER JOIN FleetPair FP ON PM.CustomerID = FP.CustomerID AND F.FleetID = FP.FleetID
			LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
			LEFT OUTER JOIN Country C ON FP.CountryID = C.CountryID
			LEFT OUTER JOIN UserMaster UM ON RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = @TripID
			
END

--EXEC spGetReportPRETSInternationalDataInformation 'JWILLIAMS_11','100111825'












GO


