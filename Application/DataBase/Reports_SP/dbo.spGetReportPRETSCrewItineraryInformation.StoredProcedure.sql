IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCrewItineraryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCrewItineraryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSCrewItineraryInformation]
	@UserCD VARCHAR(30),
	--@TripID VARCHAR(50),
	@LegId VARCHAR(50),
	@LegNum AS VARCHAR(30) = '',
	@ReportHeaderID VARCHAR(30) = '', -- TemplateID from TripSheetReportHeader table
	@TempSettings VARCHAR(MAX) = '' --'PAXMNORD::TRUE' PONEPERLIN::FALSE PAXORDER
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSCrewItineraryInformation
-- Author: SINDHUJA.K
-- Create date: 11 SEP 2012
-- Description: Get Preflight TripSheet Itinerary Crew information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON 	

DECLARE @FormatSettings TABLE (
			LMESSAGE	BIT
		,	MESSAGEPOS	SMALLINT
		,	[MESSAGE]	VARCHAR(MAX)
		,	ONELEG	BIT
		,	LEGNOTES	SMALLINT
		,	LEGCREW	SMALLINT
		,	ETEHRS	SMALLINT
		,	PONEPERLIN	BIT
		,	BLANKLINES	BIT
		,	PCNG	BIT
		,	PBUSPERS	BIT
		,	PDEPTCODE	BIT
		,	PDEPTDESC	BIT
		,	CDNAME	BIT
		,	CDPHONE	BIT
		,	CDREMARKS	BIT
		,	PAXHOTEL	BIT
		,	HOTELPHONE	BIT
		,	HOTELREM	BIT
		,	PTANAME	BIT
		,	PTAPHONE	BIT
		,	PTAREMARKS	BIT
		,	CREWHOTEL	BIT
		,	CREWHPHONE	BIT
		,	CREWHADDR	BIT
		--,	MAINT	BIT
		--,	MAINTPHONE	BIT
		--,	MAINTADDR	BIT
		,	FILTFERRY	BIT
		,	SUPPAXNOTE	BIT
		,	SUPCRWNOTE	BIT
		,	DISPATCHNO	BIT
		,	PURPOSE	BIT
		,	AIRFLTPHN	BIT
		,	ALLOWSHADE	BIT
		,	ARRFBP	BIT
		,	PTACONFIRM	BIT
		,	PTACOMMENT	SMALLINT
		,	PTAFAX	BIT
		,	PTDNAME	BIT
		,	PTDPHONE	BIT
		,	PTDCONFIRM	BIT
		,	PTDCOMMENT	SMALLINT
		,	PTDREMARKS	BIT
		,	PTDFAX	BIT
		,	CTANAME	BIT
		,	CTAPHONE	BIT
		,	CTARATE	BIT
		,	CTACONFIRM	BIT
		,	CTACOMMENT	SMALLINT
		,	CTAREMARKS	BIT
		,	CTAFAX	BIT
		,	CTDNAME	BIT
		,	CTDPHONE	BIT
		,	CTDRATE	BIT
		,	CTDCONFIRM	BIT
		,	CTDCOMMENT	SMALLINT
		,	CTDREMARKS	BIT
		,	CTDFAX	BIT
		,	CDCONFIRM	BIT
		,	CDCOMMENT	SMALLINT
		,	CDFAX	BIT
		,	CANAME	BIT
		,	CAPHONE	BIT
		,	CACONFIRM	BIT
		,	CACOMMENT	SMALLINT
		,	CAREMARKS	BIT
		,	CAFAX	BIT
		,	HOTELROOMS	BIT
		,	HOTELCONF	BIT
		,	HOTELCOMM	SMALLINT
		,	HOTELFAX	BIT
		,	CREWHNRATE	BIT
		,	CREWHROOMS	BIT
		,	CREWHCONF	BIT
		,	CREWHCOMM	SMALLINT
		,	CREWHFAX	BIT
		,	CREWHREM	BIT
		,	OUTBOUND	BIT
		,	TERMTOAFBO	SMALLINT
		,	DEPFBP	BIT
		,	TERMTODFBO	SMALLINT
		--,	MAINTFAX	BIT
		--,	MAINTNRATE	BIT
		--,	MAINTROOMS	BIT
		--,	MAINTCONF	BIT
		--,	MAINTCOMM	SMALLINT
		--,	MAINTREM	BIT
		,	PAXNBR	BIT
		,	PRINTLOGO	BIT
		,	LINESEP	BIT
		,	REQUESTOR	BIT
		,	DEPICAO	BIT
		,	ARRICAO	BIT
		,	PRNFIRSTPG	BIT
		,	INTLFLTPHN	BIT
		,	CREWPILOT1	VARCHAR(100)
		,	CREWADDL	VARCHAR(100)
		,	CREWPILOT2	VARCHAR(100)
		,	FBOALPRICE	BIT
		,	FBOALDATE	BIT
		,	FBOACONT	BIT
		,	FBOAADDR	BIT
		,	FBOAREM	BIT
		,	FBOAPHONE2	BIT
		,	FBOAPAYTYP	BIT
		,	FBOAPOSTPR	BIT
		,	FBOAFUELQT	BIT
		,	FBOACONF	BIT
		,	FBOACOMM	SMALLINT
		,	FBODPHONE	BIT
		,	FBODFREQ	BIT
		,	FBODFAX	BIT
		,	FBODUVAIR	BIT
		,	FBODFUEL	BIT
		,	FBODNPRICE	BIT
		,	FBODLPRICE	BIT
		,	FBODLDATE	BIT
		,	FBODCONT	BIT
		,	FBODADDR	BIT
		,	FBODREM	BIT
		,	FBODPHONE2	BIT
		,	FBODPAYTYP	BIT
		,	FBODPOSTPR	BIT
		,	FBODFUELQT	BIT
		,	FBODCONF	BIT
		,	FBODCOMM	SMALLINT
		,	FBOAPHONE	BIT
		,	FBOAFREQ	BIT
		,	FBOAFAX	BIT
		,	FBOAUVAIR	BIT
		,	FBOAFUEL	BIT
		,	FBOANPRICE	BIT
		,	DISPATCHER	BIT
		,	DSPTEMAIL	BIT
		,	DSPTPHONE	BIT
		,	TRIPNO	BIT
		,	[DESC]	BIT
		,	TRIPDATES	BIT
		,	TMREQ	BIT
		,	TAILNMBR	BIT
		,	CONTPHONE	BIT
		,	TYPECODE	BIT
		,	CQDESC	SMALLINT
		,	REVISNNO	BIT
		,	REVISNDESC	BIT
		,	TIMEFORMAT	BIT
		,	CRMBLPHN	SMALLINT
		,	HOTELADDR	BIT
		,	PAXORDER	BIT
		,	CHRMONTH	BIT
		,	LEGSEP		BIT
		,	LEGNOTESF	SMALLINT
		,	CQNAME		SMALLINT
		,	CQCONTACT	SMALLINT
		,   DISPATCHIN  SMALLINT
		,DCREWHTL BIT
		,DCREWHNRTE BIT
		,DCREWHADDR BIT
		,DCREWHCOMM SMALLINT
		,DCREWHCONF BIT
		,DCREWHFAX BIT
		,DCREWHROOM BIT
		,DCREWHPHON BIT
		,DCREWHREM BIT
		--,DMAINHOTEL BIT
		--,DMAINHADD BIT
		--,DMAINHCOMM SMALLINT
		--,DMAINHCON BIT
		--,DMAINHFAX BIT
		--,DMAINHRATE BIT
		--,DMAINHPHON BIT
		--,DMAINHREM BIT
		--,DMAINHROOM BIT
		,DPAXHOTEL BIT
		,DPAXHPHONE BIT 
		,DPAXHREM BIT
		,DPAXHROOMS BIT
		,DPAXHCONF BIT
		,DPAXHCOMM SMALLINT
		,DPAXHFAX BIT
		,DPAXHADDR BIT
		)
		
	
		INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 3, @TempSettings
	--	INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161675, 3, 'BLANKFORM::FALSE' 
		--SELECT * FROM @FormatSettings


		DECLARE	@CRMBLPHN	SMALLINT, @LEGCREW SMALLINT;
		SELECT @CRMBLPHN = CRMBLPHN,@LEGCREW=LEGCREW FROM @FormatSettings
	

DECLARE @CrewTable TABLE (LegId BIGINT,
                   PIC VARCHAR(MAX),
                   SIC VARCHAR(MAX),
                   ADDL VARCHAR(MAX))

IF @LEGCREW=2 OR @LEGCREW=1

BEGIN

IF @CRMBLPHN=4
BEGIN

SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = (CASE WHEN CrewPnames<>'' THEN SUBSTRING(CrewPnames,1,LEN(CrewPnames)-1) ELSE '' END),
		[SIC] = (CASE WHEN CrewSnames <> '' THEN SUBSTRING(CrewSnames ,1,LEN(CrewSnames)-1) ELSE '' END),
		 [ADDL] = (CASE WHEN CrewAnames<>'' THEN SUBSTRING(CrewAnames  ,1,LEN(CrewAnames)-1) ELSE '' END)  
	FROM 
	(
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+',' ELSE NULL END,'')  +'' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewPPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewSPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum <>'' THEN  C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID 
	LEFT OUTER JOIN (
	SELECT DISTINCT PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END ,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 ),
	 CrewAPhone = (
		SELECT ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )
	FROM PreflightCrewList PC2		
	) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId)
	
END ELSE IF @CRMBLPHN=3
BEGIN

SELECT DISTINCT [LegID] = CONVERT(VARCHAR(30),PL.LegID), 
		[PIC] = ISNULL(PCL.FirstName,'') + ' ' +ISNULL(PCL.LastName,'')+(CASE WHEN  PCL.CellPhoneNum<>'' THEN '***'+PCL.CellPhoneNum ELSE '' END),
		[SIC] = ISNULL(PCL1.FirstName,'') + ' ' +ISNULL(PCL1.LastName,'')+(CASE WHEN  PCL1.CellPhoneNum<>'' THEN '***'+PCL1.CellPhoneNum ELSE '' END),
		[ADDL]=SUBSTRING(A.CrewAnames,1,LEN(A.CrewAnames)-4)
	
	FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	                      LEFT OUTER JOIN (SELECT RANK()OVER (ORDER BY PC.PreflightCrewListID) RNK,C.LastName,C.FirstName,C.CellPhoneNum,LegID FROM  PreflightCrewList PC INNER JOIN Crew C ON C.CrewID=PC.CrewID
	                                                                                            WHERE DutyTYPE='P' AND (C.LastName IS NOT NULL OR C.FirstName IS NOT NULL) AND PC.LegID = CONVERT (BIGINT,@LegId) ) PCL ON PCL.LegID=PL.LegID AND RNK=1
	                      LEFT OUTER JOIN (SELECT RANK()OVER (ORDER BY PC.PreflightCrewListID) RK,C.LastName,C.FirstName,C.CellPhoneNum,LegID FROM  PreflightCrewList PC INNER JOIN Crew C ON C.CrewID=PC.CrewID
	                                                                                            WHERE DutyTYPE='S' AND (C.LastName IS NOT NULL OR C.FirstName IS NOT NULL) AND PC.LegID = CONVERT (BIGINT,@LegId) ) PCL1 ON PCL1.LegID=PL.LegID AND RK=1
	                      LEFT OUTER JOIN (SELECT * FROM  PreflightCrewList WHERE DutyTYPE NOT IN('P','S')) PCL2 ON PCL2.LegID=PL.LegID
	                      LEFT OUTER JOIN Crew C2 ON C2.CrewID=PCL2.CrewID
	                      
	                      	LEFT OUTER JOIN (
	SELECT DISTINCT PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+'' ELSE NULL END ,'')+ '' 
		      + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN ' - '+C.CellPhoneNum+',' ELSE NULL END,'')+ '***'
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )FROM PreflightCrewList PC2		
	) A ON PL.LegID = A.LegID
	WHERE PL.LegID = CONVERT (BIGINT,@LegId)	



END

ELSE IF @CRMBLPHN=2

BEGIN

SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = (CASE WHEN CrewPnames<>'' THEN SUBSTRING(CrewPnames,1,LEN(CrewPnames)-1) ELSE '' END),
		[SIC] = (CASE WHEN CrewSnames <> '' THEN SUBSTRING(CrewSnames ,1,LEN(CrewSnames)-1) ELSE '' END),
		[ADDL]=SUBSTRING(A.CrewAnames,1,LEN(A.CrewAnames)-4)
	FROM (
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+',' ELSE NULL END,'')  +'' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewPPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1  ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewSPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum <>'' THEN  C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID
	 	LEFT OUTER JOIN (
							SELECT DISTINCT PC2.LegID, CrewAnames = (
								SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+'' ELSE NULL END ,'')+ '' 
									  + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN ' - '+C.CellPhoneNum+',' ELSE NULL END,'')+ '***'
								FROM PreflightCrewList PC
								INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
								WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
								FOR XML PATH('')
							 )FROM PreflightCrewList PC2		
							) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId)	

END
ELSE --IF @CRMBLPHN=1
BEGIN

SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = SUBSTRING(P.CrewPnames,1,LEN(P.CrewPnames)-4),
		[SIC] = SUBSTRING(S.CrewSnames,1,LEN(S.CrewSnames)-4),
		[ADDL] = (CASE WHEN CrewAnames<>'' THEN SUBSTRING(CrewAnames  ,1,LEN(CrewAnames)-1) ELSE '' END) 
	FROM (
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+' ' ELSE NULL END,'')  +''
			       + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN '***'+C.CellPhoneNum+',' ELSE NULL END,'')+ '***' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+' ' ELSE NULL END,'') + '' 
			       + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN '***'+C.CellPhoneNum+',' ELSE NULL END,'')+ '***'
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID
	
	
	LEFT OUTER JOIN (
	SELECT DISTINCT  PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END ,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 ),
	 CrewAPhone = (
		SELECT ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )
	FROM PreflightCrewList PC2		
	) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId)	

END

END
ELSE

BEGIN

IF @CRMBLPHN=4
BEGIN
SELECT TEMP.LegID,TEMP.PIC+' ,'+ TEMP.SIC+' ,'+TEMP.ADDL Crew FROM 
(SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = (CASE WHEN CrewPnames<>'' THEN SUBSTRING(CrewPnames,1,LEN(CrewPnames)-1) ELSE '' END),
		[SIC] = (CASE WHEN CrewSnames <> '' THEN SUBSTRING(CrewSnames ,1,LEN(CrewSnames)-1) ELSE '' END),
		 [ADDL] = (CASE WHEN CrewAnames<>'' THEN SUBSTRING(CrewAnames  ,1,LEN(CrewAnames)-1) ELSE '' END)  
	FROM 
	(
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+',' ELSE NULL END,'')  +'' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewPPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewSPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum <>'' THEN  C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID 
	LEFT OUTER JOIN (
	SELECT DISTINCT PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END ,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 ),
	 CrewAPhone = (
		SELECT ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )
	FROM PreflightCrewList PC2		
	) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId))TEMP
	
END ELSE IF @CRMBLPHN=3
BEGIN

SELECT TEMP.LegID,TEMP.PIC+' ,'+ TEMP.SIC+' ,'+TEMP.ADDL Crew FROM 
(SELECT DISTINCT [LegID] = CONVERT(VARCHAR(30),PL.LegID), 
		[PIC] = ISNULL(PCL.FirstName,'') + ' ' +ISNULL(PCL.LastName,'')+(CASE WHEN  PCL.CellPhoneNum<>'' THEN ' - '+PCL.CellPhoneNum ELSE '' END),
		[SIC] = ISNULL(PCL1.FirstName,'') + ' ' +ISNULL(PCL1.LastName,'')+(CASE WHEN  PCL1.CellPhoneNum<>'' THEN ' - '+PCL1.CellPhoneNum ELSE '' END),
		[ADDL]=SUBSTRING(A.CrewAnames,1,LEN(A.CrewAnames)-1)
	
	FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
	                      LEFT OUTER JOIN (SELECT RANK()OVER (ORDER BY PC.PreflightCrewListID) RNK,C.LastName,C.FirstName,C.CellPhoneNum,LegID FROM  PreflightCrewList PC INNER JOIN Crew C ON C.CrewID=PC.CrewID
	                                                                                            WHERE DutyTYPE='P' AND (C.LastName IS NOT NULL OR C.FirstName IS NOT NULL) AND PC.LegID = CONVERT (BIGINT,@LegId) ) PCL ON PCL.LegID=PL.LegID AND RNK=1
	                      LEFT OUTER JOIN (SELECT RANK()OVER (ORDER BY PC.PreflightCrewListID) RK,C.LastName,C.FirstName,C.CellPhoneNum,LegID FROM  PreflightCrewList PC INNER JOIN Crew C ON C.CrewID=PC.CrewID
	                                                                                            WHERE DutyTYPE='S' AND (C.LastName IS NOT NULL OR C.FirstName IS NOT NULL) AND PC.LegID = CONVERT (BIGINT,@LegId) ) PCL1 ON PCL1.LegID=PL.LegID AND RK=1
	                      LEFT OUTER JOIN (SELECT * FROM  PreflightCrewList WHERE DutyTYPE NOT IN('P','S')) PCL2 ON PCL2.LegID=PL.LegID
	                      LEFT OUTER JOIN Crew C2 ON C2.CrewID=PCL2.CrewID
	                      
	                      	LEFT OUTER JOIN (
	SELECT DISTINCT PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+'' ELSE NULL END ,'')+ '' 
		      + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN ' - '+C.CellPhoneNum+',' ELSE NULL END,'')+ ''
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )FROM PreflightCrewList PC2		
	) A ON PL.LegID = A.LegID
	WHERE PL.LegID = CONVERT (BIGINT,@LegId)	)TEMP	



END

ELSE IF @CRMBLPHN=2

BEGIN
SELECT TEMP.LegID,TEMP.PIC+' ,'+ TEMP.SIC+' ,'+TEMP.ADDL Crew FROM 
(SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = (CASE WHEN CrewPnames<>'' THEN SUBSTRING(CrewPnames,1,LEN(CrewPnames)-1) ELSE '' END),
		[SIC] = (CASE WHEN CrewSnames <> '' THEN SUBSTRING(CrewSnames ,1,LEN(CrewSnames)-1) ELSE '' END),
		[ADDL]=SUBSTRING(A.CrewAnames,1,LEN(A.CrewAnames)-1)
	FROM (
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+',' ELSE NULL END,'')  +'' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewPPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1  ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 ),
		 CrewSPhone = (
			SELECT TOP 1 ISNULL(CASE WHEN C.CellPhoneNum <>'' THEN  C.CellPhoneNum+',' ELSE NULL END,'') + '' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID
	 	LEFT OUTER JOIN (
							SELECT DISTINCT PC2.LegID, CrewAnames = (
								SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+'' ELSE NULL END ,'')+ '' 
									  + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN ' - '+C.CellPhoneNum+',' ELSE NULL END,'')+ ''
								FROM PreflightCrewList PC
								INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
								WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
								FOR XML PATH('')
							 )FROM PreflightCrewList PC2		
							) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId))TEMP	

END
ELSE --IF @CRMBLPHN=1
BEGIN

SELECT TEMP.LegID,TEMP.PIC+' ,'+ TEMP.SIC+' ,'+TEMP.ADDL Crew FROM 
(SELECT [LegID] = CONVERT(VARCHAR(30),P.LegID), 
		[PIC] = SUBSTRING(P.CrewPnames,1,LEN(P.CrewPnames)-4),
		[SIC] = SUBSTRING(S.CrewSnames,1,LEN(S.CrewSnames)-4),
		[ADDL] = (CASE WHEN CrewAnames<>'' THEN SUBSTRING(CrewAnames  ,1,LEN(CrewAnames)-1) ELSE '' END) 
	FROM (
		SELECT DISTINCT PC2.LegID, CrewPnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName <> '' THEN C.LastName+' ' ELSE NULL END,'')  +''
			       + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '***' 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'P'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) P
	LEFT OUTER JOIN (
		SELECT DISTINCT PC2.LegID, CrewSnames = (
			SELECT TOP 1 ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE WHEN C.LastName<>'' THEN  C.LastName+' ' ELSE NULL END,'') + '' 
			       + ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '***'
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
			WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE = 'S'
			ORDER BY PC.PreflightCrewListID ASC 
			FOR XML PATH('')
		 )
		FROM PreflightCrewList PC2		
	) S ON P.LegID = S.LegID
	
	
	LEFT OUTER JOIN (
	SELECT DISTINCT  PC2.LegID, CrewAnames = (
		SELECT ISNULL(C.FirstName,'') + ' ' + ISNULL(CASE  WHEN C.LastName<>'' THEN  C.LastName+',' ELSE NULL END ,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 ),
	 CrewAPhone = (
		SELECT ISNULL(CASE WHEN C.CellPhoneNum<>'' THEN C.CellPhoneNum+',' ELSE NULL END,'')+ '' 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
		WHERE PC.LegID = CONVERT (BIGINT,@LegId) AND PC.DutyTYPE NOT IN ('P', 'S')
		FOR XML PATH('')
	 )
	FROM PreflightCrewList PC2		
	) A ON P.LegID = A.LegID
	WHERE P.LegID = CONVERT (BIGINT,@LegId))TEMP	

END

END


		
 END






GO


