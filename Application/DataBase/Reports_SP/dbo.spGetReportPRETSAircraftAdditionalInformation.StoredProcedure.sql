IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSAircraftAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSAircraftAdditionalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSAircraftAdditionalInformation]
	@UserCD VARCHAR(50)--MANDATORY
	,@TripID VARCHAR(30) --MANDATORY
	,@LegNum VARCHAR(30)= ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSAircraftAdditionalInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight TripSheet Aircraft Additional information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

SELECT FDescription = FP.FleetDescription
	 ,AdditionalInformation = FP.InformationValue
	 
	  FROM PreflightMain PM 
	  --INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	  INNER JOIN (SELECT FleetID, FleetDescription, InformationValue, IsDeleted FROM FleetProfileDefinition) FP 
		ON PM.FleetID = FP.FleetID
	  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	  AND PM.TripID = CONVERT(BIGINT,@TripID)
	  AND FP.IsDeleted = 0
	  --AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')

END

--EXEC spGetReportPRETSAircraftAdditionalInformation 'supervisor_99', 10099107564,''



GO


