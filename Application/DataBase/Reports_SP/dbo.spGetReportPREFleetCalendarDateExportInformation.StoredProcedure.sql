IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetCalendarDateExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetCalendarDateExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPREFleetCalendarDateExportInformation]
@UserCD VARCHAR(30)--Mandatory
	,@DATEFROM DATETIME--Mandatory
	,@DATETO DATETIME--Mandatory
	,@TailNum VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@FleetGroupCD VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@AircraftCD VARCHAR(500) = ''--[Optional], Comma delimited string with mutiple values
	,@IsHomeBase BIT = 0
	,@HomeBaseCD VARCHAR(500) = ''
	--,@SortBy CHAR(10) = 'TAILNUMBER'
AS
-- ===========================================================
--	SPC Name: spGetReportPREFleetCalendarDateExportInformation
-- Author: AISHWARYA.M
-- Create date: 18 Jul 2012
-- Description: Get Fleet Calender Date Export information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 
-- ============================================================
SET NOCOUNT ON 

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)
DECLARE @Loopdate DATETIME
 
Declare @SuppressActivityAircft BIT  
 	
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@Customer 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
 DECLARE @TenToMin INT
 SET @TenToMin=(SELECT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	  AND F.IsDeleted=0
	  AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,FleetID BIGINT
	,TailNum VARCHAR(25)
	,AircraftID BIGINT
	,[AC_CODE] CHAR(4)
	)
	
	SET @String = @TailNum
	SET @Delimiter = ','
	SET @String = @String + @Delimiter
	SET @Pos = charindex(@Delimiter, @String)
	
	CREATE TABLE #TempTable
		(
		ID INT identity(1, 1) NOT NULL
		,[CustomerID] BIGINT
		,[TailNum] CHAR(6)
		,[FileNum] BIGINT
		,DepartureDTTMLocal DATETIME
		,ArrivalDTTMLocal DATETIME
		,DepartICAOID CHAR(4)
		,ArriveICAOID CHAR(4)
		,[Duty] CHAR(2)
		,[Description] VARCHAR(40)
		,[cat] CHAR(4)
		,[Rqstr] VARCHAR(60)
		,[Pax] INT
		,[Pic] VARCHAR(250)
		,[Sic] VARCHAR(250)
		,[Crew] VARCHAR(50)
		,[Ete] NUMERIC(7, 3)
		,[Date] DATE
		,[NextLocalDTTM] DATETIME
		,FleetID BIGINT
		,TripID BIGINT
		,LegID BIGINT
		,[AC_CODE] CHAR(4)
		,IsApproxTM BIT
		,ron CHAR(5) 
		,leg_num BIGINT
		,type_code VARCHAR(10)
		,vendcode VARCHAR(5)
		,[desc] VARCHAR(40)
		,rec_type CHAR(1)
		,grndtime BIT
		,trueron CHAR(5)
		,dayfill BIT
		,nextloc DATETIME
		,auth_code VARCHAR(10)
		,resv_avail INT
		,purpose VARCHAR(40)
		,private BIT
		,ordernum INT
		,aircraft_code CHAR(4)
		,home_base CHAR(4)
		,fhomebase CHAR(4)
		,Notes VARCHAR(MAX)
		)
		
		


	
DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM

BEGIN
SET @SQLSCRIPT =' 
			 INSERT INTO #TempTailNum (FleetID,TailNum, AircraftID, [AC_CODE])
			SELECT DISTINCT F.FleetID,F.TailNum, F.AircraftID, F.AircraftCD
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID 
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID 
			LEFT OUTER JOIN Company C ON C.HomebaseID=F.HomebaseID
			LEFT OUTER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
			LEFT OUTER JOIN Aircraft  AT ON AT.AircraftID=F.AircraftID
			INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			LEFT OUTER JOIN UserMaster UM ON UM.HomebaseID=F.HomebaseID AND UM.CustomerID=F.CustomerID AND UM.UserName= @UserCD
			WHERE  F.CustomerID = @CUSTOMER'
			
				
		IF @IsHomeBase = 1 BEGIN
			SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
		END ELSE BEGIN
			IF @HomeBaseCD <> '' BEGIN
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBaseCD, 1) = ',' THEN LEFT(@HomeBaseCD, LEN(@HomeBaseCD) - 1) ELSE @HomeBaseCD END, ',', ''', ''') + ''')';
			END	
		END
		IF @AircraftCD <> ''
		BEGIN
		    SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
		 END
		
		SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY F.AircraftCD '
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500),@CUSTOMER AS BIGINT'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD,@CUSTOMER 
	END

DELETE FROM #TempTailNum WHERE FleetID NOT IN(
	(SELECT DISTINCT PM.FleetID FROM PreflightMain PM INNER JOIN #TempTailNum T ON PM.FleetID=T.FleetID
	                                     WHERE PM.CustomerID = @CUSTOMER ))



		INSERT INTO #TempTable
			(
			[Date]
			,[TailNum]
			,[FileNum]
			,DepartureDTTMLocal
			,ArrivalDTTMLocal
			,DepartICAOID 
			,ArriveICAOID 
			,[Duty]
			,[Description]
			,[cat] 
			,[Rqstr] 
			,[Pax] 
			,[Pic] 
			,[Sic] 
			,[Crew] 
			,[Ete]
			,[NextLocalDTTM] 
			,CustomerID
			,FleetID 
			,TripID 
			,LegID 
			,[AC_CODE]
			,IsApproxTM 
			,leg_num 
			,type_code
			,vendcode
			,[desc]
			,rec_type
			,nextloc
			,auth_code 
			,resv_avail
			,purpose 
		    ,private
		    ,ordernum
		    ,aircraft_code
		    ,Notes 
			)
				
		SELECT CONVERT(DATE,PL.DepartureDTTMLocal)
			,F.TailNum 
			,CONVERT(VARCHAR,PM.TripNUM)
			,PL.DepartureDTTMLocal
			,PL.ArrivalDTTMLocal
			,A.ICAOID 
			,AA.ICAOID 
			,CASE WHEN PM.RecordType = 'T' THEN 'F' ELSE  PM.RecordType END
			,PM.TripDescription 
			,FC.FlightCatagoryCD 
			,P.PassengerRequestorCD
			,PL.PassengerTotal 
			,PIC.CrewCD
			,SIC.CrewCD
			,CrewList=(SUBSTRING(CrewList,1,LEN(CrewList)-1))
			,PL.ElapseTM 
			,PL.NextLocalDTTM 
			,F.CustomerID 
			,F.FleetID
			,PM.TripID
			,PL.LegID
			,F.AircraftCD
			,PL.IsApproxTM
			,PL.LegNUM
			,AT.AircraftCD
			,V.VendorCD
			,PM.TripDescription
			,PM.RecordType
			,PL.NextLocalDTTM
			,DA.AuthorizationCD
			,PL.ReservationAvailable
			,PL.FlightPurpose
			,PL.IsPrivate
			,ISNULL(FGO.OrderNum,99999)
			,F.AircraftCD
			,PL.Notes
			
		FROM PreflightMain PM
		INNER JOIN  ( SELECT DISTINCT FLEETID FROM #TempFleetID ) F1  ON F1.FleetID = PM.FleetID
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
		--INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID
		--INNER JOIN Crew C ON PC.CrewID = C.CrewID
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=PL.PassengerRequestorID
		LEFT OUTER JOIN Airport A ON A.AirportID = PL.DepartICAOID
		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
		 LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ',' 
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID AND PL.IsDeleted = 0
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid AND PC1.DutyTYPE NOT IN ('P','S')
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
		) C ON PM.tripid = C.tripid 
		INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD AircraftCD,VendorID
				FROM Fleet F 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		LEFT OUTER JOIN FleetGroupOrder FGO ON FGO.FleetID=F.FleetID AND F.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG ON FG.FleetGroupID=FGO.FleetGroupID AND FG.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
		INNER JOIN #TempFleetID TEMP ON TEMP.FleetId = F.FleetId
		LEFT JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
		INNER JOIN AIRCRAFT AT ON PM.AircraftID=AT.AircraftID
	    LEFT OUTER JOIN (SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY PCL.PreflightCrewListID) Rnk,CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
														 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('P') ) PIC ON PIC.TripID=PM.TripID AND RNK=1 AND PL.LegID=PIC.LegID
		 LEFT OUTER JOIN (SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.LegID  ORDER BY PCL.PreflightCrewListID) Rk,CW.CrewID,PM.TripID,CREWCD,PL.LegNUM,PL.LegID,PCL.DutyTYPE FROM PreflightMain PM INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  AND PL.IsDeleted = 0
														 INNER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('S') ) SIC ON SIC.TripID=PM.TripID AND RK=1 AND PL.LegID=SIC.LegID
		WHERE PM.ISDELETED = 0 
		AND   TripStatus IN('T','H')
		AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE, @DateFrom) AND CONVERT(DATE, @DateTo)
		AND F.CustomerID = @CUSTOMER
		AND (AT.AircraftCD IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')))OR @AircraftCD='')
		AND (PM.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
		
		

	
	----PRINT @SQLSCRIPT



 

		DECLARE @Date DATETIME
				,@TailNumber CHAR(6)
				,@FileNum BIGINT
				,@DepartureDTTMLocal DATETIME
				,@ArrivalDTTMLocal DATETIME
				,@DepartICAOID CHAR(4)
				,@ArriveICAOID CHAR(4),@CrPurpose VARCHAR(40),@CrOrdernum INT,@CrLegnum BIGINT,@CrLegid BIGINT,@CRtype_code VARCHAR(10),@Crvendcode VARCHAR(5);
		DECLARE @Duty CHAR(2)
				,@Description VARCHAR(40)
				,@cat CHAR(4)
				,@Rqstr VARCHAR(60)
				,@Pax INT
				,@Pic CHAR(4)
				,@Sic CHAR(4)
				,@Crew CHAR(50),@CrRecType CHAR(1);
		DECLARE @Ete NUMERIC(7, 3)
				,@NextLocalDTTM DATETIME
				,@CustomerID BIGINT
				,@FleetID BIGINT
				,@TripID BIGINT
				,@LegID BIGINT
				,@AC_CODE CHAR(4);
		declare @temp int = 0;						
		DECLARE curFleetCalendarAircraft CURSOR FAST_FORWARD READ_ONLY
		FOR
		SELECT TT.Date
			,TT.TailNum
			,TT.FileNum
			,TT.DepartureDTTMLocal
			,TT.ArrivalDTTMLocal
			,TT.DepartICAOID 
			,TT.ArriveICAOID 
			,TT.Duty
			,TT.Description
			,TT.cat 
			,TT.Rqstr 
			,TT.Pax 
			,TT.Pic 
			,TT.Sic 
			,TT.Crew 
			,TT.Ete
			,TT.NextLocalDTTM 
			,TT.CustomerID
			,TT.AC_CODE
			,TT.purpose
			,TT.ordernum
			,TT.leg_num
			,TT.LegID
			,TT.type_code
			,TT.vendcode
			,TT.rec_type
			,TT.FleetID
		FROM #TEMPTABLE TT
		WHERE CONVERT(Date, TT.ArrivalDTTMLocal, 101) < CONVERT(Date, TT.[NextLocalDTTM], 101)
		--ORDER BY [Date]
		
		OPEN curFleetCalendarAircraft
		FETCH NEXT
		FROM curFleetCalendarAircraft
		INTO @Date
				,@TailNumber 
				,@FileNum 
				,@DepartureDTTMLocal 
				,@ArrivalDTTMLocal 
				,@DepartICAOID 
				,@ArriveICAOID 
				,@Duty 
				,@Description 
				,@cat
				,@Rqstr 
				,@Pax 
				,@Pic
				,@Sic 
				,@Crew 
				,@Ete 
				,@NextLocalDTTM 
				,@CustomerID
				,@AC_CODE
				,@CrPurpose
				,@CrOrdernum
				,@CrLegnum
				,@CrLegid
				,@CRtype_code
				,@Crvendcode
				,@CrRecType
				,@FleetID; 
				
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @Loopdate = '';
		
		/*	-- Removed on 31 May 2013 --	
		INSERT INTO #TempTable ([Date], TailNum, FileNum, DepartureDTTMLocal, ArrivalDTTMLocal, DepartICAOID, ArriveICAOID, Duty, [Description],
							 Rqstr,Pic, Sic, Crew, NextLocalDTTM, CustomerID, AC_CODE,IsApproxTM)
	                 VALUES ( @ArrivalDTTMLocal ,@TailNumber, @FileNum, @ArrivalDTTMLocal,
							  CONVERT(DATETIME, CONVERT(VARCHAR(10),@ArrivalDTTMLocal, 101) + ' 23:59', 101), @ArriveICAOID, @ArriveICAOID, 'R',
							  @Description, @Rqstr,@Pic, @Sic, @Crew,NULL, @CustomerID, @AC_CODE,0)	
		
		*/
		set @temp = @temp + 1;
		SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
			WHILE(CONVERT(DATE, @Loopdate, 101) < CONVERT(DATE, @NextLocalDTTM, 101))
			BEGIN
			
			INSERT INTO #TempTable ([Date], TailNum, FileNum, DepartureDTTMLocal, ArrivalDTTMLocal, DepartICAOID, ArriveICAOID, Duty, [Description],
							 Rqstr,Pic, Sic, Crew, NextLocalDTTM, CustomerID, AC_CODE,IsApproxTM,purpose,ordernum,leg_num,LegID,type_code,vendcode,ron,trueron,rec_type,FleetID)					 
			VALUES ( @Loopdate ,@TailNumber, @FileNum, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101),
							CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101), @ArriveICAOID, @ArriveICAOID, 'R',
							@Description, @Rqstr,@Pic, @Sic, @Crew,NULL, @CustomerID, @AC_CODE,0,@CrPurpose,@CrOrdernum,@CrLegnum,@CrLegid,@CRtype_code,@Crvendcode,'TRUE','TRUE',@CrRecType,@FleetID)				
							
				SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
			END
		
		
		FETCH NEXT
		FROM curFleetCalendarAircraft
		INTO @Date
				,@TailNumber 
				,@FileNum 
				,@DepartureDTTMLocal 
				,@ArrivalDTTMLocal 
				,@DepartICAOID 
				,@ArriveICAOID 
				,@Duty 
				,@Description 
				,@cat
				,@Rqstr 
				,@Pax 
				,@Pic
				,@Sic 
				,@Crew 
				,@Ete 
				,@NextLocalDTTM 
				,@CustomerID
				,@AC_CODE 
				,@CrPurpose
				,@CrOrdernum
				,@CrLegnum
				,@CrLegid
				,@CRtype_code
				,@Crvendcode
				,@CrRecType
				,@FleetID; 
		END

		CLOSE curFleetCalendarAircraft;

		DEALLOCATE curFleetCalendarAircraft;


SET @TEMPTODAY = @DATEFROM
	  CREATE TABLE #TblDates  (AllDates DATE);

      WITH dates(DATEPARAM)
           AS (SELECT @TEMPTODAY AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO #TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
      

INSERT INTO #TempTailNum (FleetID,TailNum, [AC_CODE])
SELECT FleetID,TailNum,AC_CODE FROM #TempTable WHERE FleetID NOT IN (SELECT ISNULL(FleetID,0) FROM #TempTailNum)

				
				INSERT INTO #TempTable (TailNum, [Date],AC_CODE,FleetID) 
				SELECT DISTINCT TempT.TailNum, AllDates,TempT.AC_CODE,TempT.FleetID
				FROM #TempTailNum TempT
			  CROSS JOIN #TblDates
			--  OUTER APPLY dbo.GetFleetCurrentAirpotCD(AllDates, TempT.FleetID) AS A 
				WHERE NOT EXISTS(SELECT T.TailNum FROM #TempTable T 
					WHERE T.TailNum = TempT.TailNum AND CONVERT(DATE,T.[Date]) = AllDates 
				)
				



UPDATE #TempTable SET home_base=TEMP.ICAOID FROM(
				SELECT TOP 1 A.IcaoID ICAOID,PM.FleetID,PL.DepartureDTTMLocal DEP_DATE
					FROM PreflightLeg PL
					INNER JOIN PreflightMain PM ON PL.TripID = PM.TripID AND PM.IsDeleted = 0
					INNER JOIN #TempTable TT ON TT.FleetID=PM.FleetID
					INNER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
					AND CONVERT(DATE, PL.ArrivalDTTMLocal) <= CONVERT(DATE, Date)
					AND PM.TripStatus = 'T'
					AND PM.CustomerID=@CUSTOMER
					AND PL.IsDeleted = 0
					ORDER BY PL.ArrivalDTTMLocal DESC
			)TEMP
WHERE #TempTable.FleetID=TEMP.FleetID
  AND CONVERT(DATE,#TempTable.Date)=CONVERT(DATE,TEMP.DEP_DATE)
 AND #TempTable.DepartureDTTMLocal IS NULL

  

  
UPDATE #TempTable SET home_base=TEMP.ICAOID FROM(
				SELECT  DISTINCT AA.IcaoID ICAOID,F.FleetID
					FROM  Fleet F INNER JOIN Company C ON F.HomebaseID=C.HomebaseID
					              INNER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID
					              
					WHERE F.CustomerID=@CUSTOMER
			)TEMP
WHERE #TempTable.FleetID=TEMP.FleetID
AND #TempTable.FileNum IS NULL 
AND #TempTable.DepartureDTTMLocal IS NULL

  
UPDATE #TempTable SET fhomebase=TEMP.ICAOID FROM(
				SELECT  DISTINCT AA.IcaoID ICAOID,F.FleetID
					FROM  Fleet F INNER JOIN Company C ON F.HomebaseID=C.HomebaseID
					              INNER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID
					              
					WHERE F.CustomerID=@CUSTOMER
			)TEMP
WHERE #TempTable.FleetID=TEMP.FleetID
AND #TempTable.DepartureDTTMLocal IS NOT NULL

     --------------------------------------------
    

DECLARE @CrDepartureDTTMLocal DATETIME,@CrTailNumber VARCHAR(30),@Cricao_id CHAR(4),@CrDutyTYPE CHAR(2),@crTripDescription VARCHAR(40),@CrArrivalDTTMLocal DATETIME;
DECLARE @DATE1 INT,@Count INT=0,@activedate DATETIME,@CrArrivalDTTMLocal1 DATETIME,@CrAircraftCD CHAR(4),@CrIsApproxTM BIT;
DECLARE @DATElOC DATE,@NextLocalDTTM1 DATETIME;



DECLARE C1 CURSOR FAST_FORWARD READ_ONLY FOR

SELECT DISTINCT CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) < CONVERT(DATE,@DATEFROM) THEN @DATEFROM  ELSE PL.DepartureDTTMLocal END DepartureDTTMLocal,F.TailNum,A.IcaoID, PL.DutyTYPE,PM.TripDescription,
CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) > CONVERT(DATE,@DATETO) THEN @DATETO  ELSE PL.ArrivalDTTMLocal END ArrivalDTTMLocal,F.AircraftCD,PL.IsApproxTM, 
PL.ElapseTM,PM.TripNUM,PL.LegNUM,PL.LegID,AT.AircraftCD,V.VendorCD,PM.RecordType,PL.NextLocalDTTM,ISNULL(FGO.OrderNum,99999)
 FROM PreflightMain PM inner join PreflightLeg PL on PM.TripID=PL.TripID AND PL.ISDELETED = 0
 INNER JOIN Fleet F ON F.FleetID=PM.FleetID
 LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
 LEFT OUTER JOIN FleetGroupOrder FGO ON FGO.FleetID=F.FleetID AND F.CustomerID = FGO.CustomerID
 LEFT OUTER JOIN FleetGroup FG ON FG.FleetGroupID=FGO.FleetGroupID AND FG.CustomerID = FGO.CustomerID
 LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
  INNER JOIN ( SELECT DISTINCT FLEETID FROM #TempFleetID )F1 ON F1.FLEETID=F.FLEETID 
  INNER JOIN #TempTailNum TEMP ON TEMP.FleetId = F.FleetId
OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
 WHERE PM.RecordType='M'
 AND PL.DutyTYPE<>'R'
 AND PM.CustomerID=@CUSTOMER
 AND PM.ISDELETED = 0
 AND (PM.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)
 --AND F.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD) 

AND (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	)

 OPEN C1
 FETCH NEXT FROM C1 INTO @CrDepartureDTTMLocal,@CrTailNumber,@Cricao_id,@CrDutyTYPE,@crTripDescription,@CrArrivalDTTMLocal,@CrAircraftCD,@CrIsApproxTM,@Ete,
	                         @FileNum,@CrLegnum,@CrLegid,@CRtype_code,@Crvendcode,@CrRecType,@NextLocalDTTM,@CrOrdernum
	WHILE @@FETCH_STATUS = 0
	
BEGIN 

  
	SET @DATE1=(SELECT DATEDIFF(DAY,@CrDepartureDTTMLocal,@CrArrivalDTTMLocal))
	SET @activedate=@CrDepartureDTTMLocal
    WHILE(@Count<=@DATE1)
	BEGIN
	 SET @DATElOC=(SELECT TOP 1 DepartureDTTMLocal FROM #TempTable WHERE [Date]=@activedate AND [TailNum]=@CrTailNumber)

	 IF @Count=@DATE1 AND @Count <> 0
	 

	 BEGIN  SET @CrArrivalDTTMLocal1=@CrArrivalDTTMLocal; SET @NextLocalDTTM1=@NextLocalDTTM END
	 ELSE BEGIN SET @CrArrivalDTTMLocal1=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepartureDTTMLocal, 101) + ' 23:59') 
	            SET @NextLocalDTTM1=CONVERT(DATE,@CrArrivalDTTMLocal1+1) END
 	  
 	 IF @DATElOC IS NULL 
 	 BEGIN
 	 
	UPDATE #TempTable SET
	                       DepartureDTTMLocal=@CrDepartureDTTMLocal,
	                       DepartICAOID=@Cricao_id,
	                       ArrivalDTTMLocal=@CrArrivalDTTMLocal1,
	                       ArriveICAOID=@Cricao_id,
	                       [Duty]=@CrDutyTYPE,
	                       [Description]=@crTripDescription,
	                       AC_CODE=@CrAircraftCD,
	                       IsApproxTM=@CrIsApproxTM,
	                       Ete=@Ete,
	                       FileNum=@FileNum,
	                       leg_num=@CrLegnum,
	                       LegID=@CrLegid,
	                       type_code=@CRtype_code,
	                       vendcode=@Crvendcode,
	                       rec_type=@CrRecType,
	                       nextloc=@NextLocalDTTM1,
	                       ordernum=@CrOrdernum
	                     
	                   WHERE [Date]=CONVERT(DATE,@activedate)
	                     AND [TailNum]=@CrTailNumber

	 END
	 ELSE 
	 BEGIN
	 INSERT INTO #TempTable([Date],[TailNum],DepartureDTTMLocal,DepartICAOID,ArrivalDTTMLocal,ArriveICAOID,[Duty],[Description],AC_CODE,IsApproxTM,Ete,Pax,
	                        FileNum,leg_num,LegID,type_code,vendcode,rec_type,nextloc,ordernum) 
	VALUES(@activedate,@CrTailNumber,@CrDepartureDTTMLocal,@Cricao_id,@CrArrivalDTTMLocal1,@Cricao_id,@CrDutyTYPE,@crTripDescription,@CrAircraftCD,@CrIsApproxTM,@Ete,0,
	                         @FileNum,@CrLegnum,@CrLegid,@CRtype_code,@Crvendcode,@CrRecType,@NextLocalDTTM1,@CrOrdernum)
	 END

	 SET @CrDepartureDTTMLocal=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepartureDTTMLocal+1, 101) + ' 00:00')

	 SET @Count=@Count+1 
	 SET @DATElOC=@DATElOC
	  SET @activedate=@activedate+1
	 END
	  SET @Count=0

	 FETCH NEXT FROM C1 INTO @CrDepartureDTTMLocal,@CrTailNumber,@Cricao_id,@CrDutyTYPE,@crTripDescription,@CrArrivalDTTMLocal,@CrAircraftCD,@CrIsApproxTM,@Ete,
	                         @FileNum,@CrLegnum,@CrLegid,@CRtype_code,@Crvendcode,@CrRecType,@NextLocalDTTM,@CrOrdernum
END
	CLOSE C1;
	DEALLOCATE C1;
----------------------------------------------		                   
IF @SuppressActivityAircft=0
BEGIN
	SELECT A.* FROM
	    (
	    SELECT DISTINCT
			 tail_nmbr = T.TailNum
			,[activedate] = T.[Date]
			,locdep=DepartureDTTMLocal
			,depicao_id=DepartICAOID 
			,locarr=ArrivalDTTMLocal 
			,arricao_id=ArriveICAOID 
			,requestor=Rqstr 
			,dept_code=auth_code
			,cat_code=cat 
			,duty_type=Duty
			,elp_time=FLOOR(ISNULL(Ete,0)*10)*0.1
			,timeapprox=CASE IsApproxTM WHEN 1 THEN 'TRUE' ELSE 'FALSE' END
			,pax_total=ISNULL(Pax ,0)
			,ron=ISNULL(ron,'FALSE')
			,pic_code=Pic 
			,sic_code=Sic 
			,orig_nmbr=ISNULL(FileNum,0)
			,leg_num=ISNULL(leg_num,0)
			,legid=ISNULL(LegID,0)
			,type_code=type_code
			,vendcode=vendcode
			,[desc]=[Description]
			,rec_type=rec_type
			,grndtime=ISNULL(grndtime,0)
			,trueron=ISNULL(trueron,'FALSE')
			,dayfill=ISNULL(dayfill,0) 
			,nextloc=nextloc
			,ac_code=AC_CODE
			,auth_code=auth_code
			,resv_avail=ISNULL(resv_avail,0)
			,purpose=purpose
			,private=CASE private WHEN 1 THEN 'TRUE' ELSE 'FALSE' END
			,ordernum=ISNULL(ordernum,0)
			,aircraft_code=aircraft_code
			,home_base=home_base
			,fhomebase=fhomebase
			,[TenToMin] = @TenToMin
			,T.[Date] Date1
			,T.Crew AdditionalCrew
			,T.Notes
			FROM #TempTable T
			 )A
		    WHERE (CONVERT(DATE,[Date1]) BETWEEN CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO) OR locdep IS NULL)
			ORDER BY Date1,[AC_CODE],locdep
			
END

ELSE

BEGIN

	SELECT A.* FROM
	    (
	    SELECT DISTINCT
			 tail_nmbr = T.TailNum
			,[activedate] = T.[Date]
			,locdep=DepartureDTTMLocal
			,depicao_id=DepartICAOID 
			,locarr=ArrivalDTTMLocal 
			,arricao_id=ArriveICAOID 
			,requestor=Rqstr 
			,dept_code=auth_code
			,cat_code=cat 
			,duty_type=Duty
			,elp_time=FLOOR(ISNULL(Ete,0)*10)*0.1
			,timeapprox=CASE IsApproxTM WHEN 1 THEN 'TRUE' ELSE 'FALSE' END
			,pax_total=ISNULL(Pax ,0)
			,ron=ISNULL(ron,'FALSE')
			,pic_code=Pic 
			,sic_code=Sic 
			,orig_nmbr=ISNULL(FileNum,0)
			,leg_num=ISNULL(leg_num,0)
			,legid=ISNULL(LegID,0)
			,type_code=type_code
			,vendcode=vendcode
			,[desc]=[Description]
			,rec_type=rec_type
			,grndtime=ISNULL(grndtime,0)
			,trueron=ISNULL(trueron,'FALSE')
			,dayfill=ISNULL(dayfill,0) 
			,nextloc=nextloc
			,ac_code=AC_CODE
			,auth_code=auth_code
			,resv_avail=ISNULL(resv_avail,0)
			,purpose=purpose
			,private=CASE private WHEN 1 THEN 'TRUE' ELSE 'FALSE' END
			,ordernum=ISNULL(ordernum,0)
			,aircraft_code=aircraft_code
			,home_base=home_base
			,fhomebase=fhomebase
			,[TenToMin] = @TenToMin
			,T.[Date] Date1
			,T.Crew AdditionalCrew
			,T.Notes
			FROM #TempTable T
			 )A
		    WHERE (CONVERT(DATE,[Date1]) BETWEEN CONVERT(DATE,@DATEFROM) AND  CONVERT(DATE,@DATETO))
		    AND locdep IS NOT NULL
			ORDER BY Date1,[AC_CODE],locdep
			

END
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID
IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE #TempTailNum
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable
IF OBJECT_ID('tempdb..#TblDates') IS NOT NULL
DROP TABLE #TblDates
	
	--EXEC spGetReportPREFleetCalendarDateExportInformation 'JWILLIAMS_13', '2010/10/20', '2010/12/27', '', '', '', 0, ''
	--EXEC spGetReportPREFleetCalendarDateExportInformation 'ELIZA_9', '2012-10-20', '2012-10-30', '', '', '', 0, 'KHOU'
	

GO


