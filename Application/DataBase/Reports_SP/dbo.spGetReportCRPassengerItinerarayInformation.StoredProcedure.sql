IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRPassengerItinerarayInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRPassengerItinerarayInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportCRPassengerItinerarayInformation]
(
	@UserCD VARCHAR(30) --Mandatory 
	,@UserCustomerID VARCHAR(30) --Mandatory
	,@DATEFROM DATE --Mandatory
	,@DATETO DATE   --Mandatory
	,@PaxID VARCHAR(100) --Mandatory
	,@PassengerGroupCD VARCHAR(500)='' --Optional
	,@PrintFlightCrew BIT = 0
	,@PrintLegNotes BIT = 0
	,@PrintPassengerManifest BIT = 0
	,@PrintTerminalInformation BIT = 0
	,@PrintOneLegPerPage BIT = 0
)
AS
BEGIN
-- =====================================================
-- SPC Name:spGetReportCRPassengerItinerarayInformation
-- Author: Aishwarya.M
-- Create date:2013 May 08
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ======================================================
DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PaxID <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @UserCustomerID
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaxID, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @UserCustomerID  
END
ELSE IF @PaxID = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @UserCustomerID  
	AND P.IsDeleted=0
END

DECLARE @PassTemp TABLE(   ID INT,
                            TripID BIGINT,
                            LEGID BIGINT,
                            PassengerCD VARCHAR(5),
							Passenger VARCHAR(63),
							PassengerID BIGINT
						)




INSERT INTO @PassTemp
SELECT ROW_NUMBER()OVER(PARTITION BY TripID,LEGID ORDER BY PassengerCD) ,PASS.* FROM
 (

 SELECT DISTINCT CRM.TripID,ISNULL(PL.LEGID,CRL.CRLegID) LegID,CASE WHEN CRM.TripID IS NOT NULL THEN  P1.PassengerRequestorCD ELSE P.PassengerRequestorCD END PassengerCD
 ,CASE WHEN CRM.TripID IS NOT NULL THEN ISNULL(P1.FirstName,'') ELSE ISNULL(P.FirstName,'') END + ' ' + CASE WHEN CRM.TripID IS NOT NULL THEN ISNULL(P1.LastName,'') ELSE ISNULL(P.LastName,'') END  Passenger
 ,CASE WHEN CRM.TripID IS NOT NULL THEN P1.PassengerRequestorID  ELSE P.PassengerRequestorID END PassID
                           FROM CRMain CRM 
                                  INNER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID AND CRL.IsDeleted = 0
								  LEFT OUTER JOIN PreflightMain PM ON PM.TripID = CRM.TripID AND PM.IsDeleted = 0
								  LEFT OUTER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
								  LEFT OUTER JOIN CRPassenger CRP ON CRP.CRLegID = CRL.CRLegID
								  LEFT OUTER JOIN Passenger P ON CRP.PassengerRequestorID=P.PassengerRequestorID
								  LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID=PL.LegID
								  LEFT OUTER JOIN Passenger P1 ON PPL.PassengerID = P1.PassengerRequestorID
			              WHERE CRM.CustomerID=@UserCustomerID
			              AND ISNULL(PM.EstDepartureDT,CRM.EstDepartureDT) BETWEEN @DATEFROM AND @DATETO
			              AND CRM.IsDeleted = 0
			              --AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM DBO.SplitString(@PaxID, ',')) OR @PaxID = '') 
			              -- AND PL.LEGID IS NOT NULL

)Pass	

        

SELECT DISTINCT PaxName = ISNULL(TripPax.FirstName,ISNULL(P.FirstName,'')) + ' ' + ISNULL(TripPax.LastName,ISNULL(P.LastName,''))--ISNULL(P.FirstName,'') + ' ' + ISNULL(P.LastName,'')
		,TripDates = dbo.GetShortDateFormatByUserCD(@UserCD,ISNULL(PL.DepartureDTTMLocal,CRL.DepartureDTTMLocal)) + ' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,ISNULL(PL.ArrivalDTTMLocal,CRL.ArrivalDTTMLocal))
		,LocDate = ISNULL(PL.DepartureDTTMLocal,CRL.DepartureDTTMLocal)
		,TailNumber = F.TailNum
		,AircraftType = AC.AircraftCD 
		,Departure = ISNULL(ARD.CityName,AD.CityName)+ ', ' + ISNULL(ARD.StateName, AD.StateName)
		,DepAirport = ISNULL(ARD.AirportName, AD.AirportName)
		,Arrival = ISNULL(ARA.CityName,AA.CityName)+ ', ' + ISNULL(ARA.StateName, AA.StateName)
		,ArrAirport = ISNULL(ARA.AirportName, AA.AirportName)
		,Depart = ISNULL(PL.DepartureDTTMLocal,CRL.DepartureDTTMLocal)
		,Arrive = ISNULL(PL.ArrivalDTTMLocal,CRL.ArrivalDTTMLocal)
		,FltTime = ISNULL(PL.ElapseTM,CRL.ElapseTM)
		,Passengers = ISNULL(PL.PassengerTotal,CRL.PassengerTotal)
		,Flightcrew =CASE WHEN CRM.TripID IS NOT NULL AND CREW.Crew IS NULL THEN '' ELSE  SUBSTRING(ISNULL(Crew.Crew,'No Crew Assignedd'),1,LEN(ISNULL(Crew.Crew,'No Crew Assignedd '))-1) END
		,FlightInformation = ISNULL(PL.Notes,CRL.Notes)
		--,PassengerConfirmed = ''
		,DepartureTerminal = ISNULL(PFD.FBOVendor,CRFD.FBOVendor) + ' ' + ISNULL(PFD.PhoneNUM1,CRFD.PhoneNUM1)
		,ArrivalTerminal = ISNULL(PFA.FBOVendor,CRFA.FBOVendor) + ' ' + ISNULL(PFA.PhoneNUM1,CASE WHEN PFA.FBOVendor IS NULL THEN CRFA.PhoneNUM1 ELSE NULL END)
        ,PR.PassRight PassRight
        ,PLT.PassLeft  PassLeft
        ,Leg = ISNULL(PL.LegID,CRL.CRLegID)
        ,Paxcode = ISNULL(TripPax.PassengerRequestorCD,P.PassengerRequestorCD)
        ,CRM.CRTripNUM,PM.TripNUM

		FROM CRMain CRM
		INNER JOIN CRLeg CRL ON CRM.CRMainID = CRL.CRMainID AND CRL.IsDeleted = 0
		LEFT OUTER JOIN CRPassenger CRP ON CRL.CRLegID = CRP.CRLegID
		LEFT OUTER JOIN Passenger P ON CRP.PassengerRequestorID = P.PassengerRequestorID
		LEFT OUTER JOIN Fleet F ON CRM.FleetID = F.FleetID
		LEFT OUTER JOIN Aircraft AC ON CRM.AircraftID = AC.AircraftID
		LEFT OUTER JOIN Airport AD ON CRL.DAirportID = AD.AirportID
		LEFT OUTER JOIN Airport AA ON CRL.AAirportID = AA.AirportID
		LEFT OUTER JOIN PreflightMain PM ON PM.TripID=CRM.TripID AND PM.IsDeleted = 0
		LEFT OUTER JOIN PreflightLeg PL ON PL.TripID=PM.TripID AND PL.IsDeleted = 0
		LEFT OUTER JOIN PreflightPassengerList PPL ON PPL.LegID = PL.LegID
		LEFT OUTER JOIN Passenger TripPax ON TripPax.PassengerRequestorID = PPL.PassengerID
		LEFT OUTER JOIN Airport ARD ON PL.DepartICAOID = ARD.AirportID
		LEFT OUTER JOIN Airport ARA ON PL.ArriveICAOID = ARA.AirportID 
		--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'A'
				) CRFA ON CRFA.CRLegID = CRL.CRLegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'D'
				) CRFD ON CRFD.CRLegID = CRL.CRLegID
		LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID, Crew = (
				SELECT ISNULL(C.FirstName,'') + ' ' +C.LastName + ', ' 
				FROM PreflightCrewList PC
				INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				WHERE PC.LegID=PC2.LegID
				ORDER BY(CASE PC.DutyTYPE 
							WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
							WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END) 
				FOR XML PATH('')
				)
		FROM PreflightCrewList PC2		
		) Crew ON PL.LegID = Crew.LegID
		
		--PREFLIGHT	TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1,PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsArrivalFBO = 1 
				) PFA ON PFA.LegID = PL.LegID 
		--PREFLIGHT FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, PFL.PreflightFBOName FBOVendor, PFL.LegID
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsDepartureFBO = 1
				) PFD ON PFD.LegID = PL.LegID
				
	    LEFT OUTER JOIN ( SELECT DISTINCT PT1.TripID,PT1.LEGID,PassRight=(
                                SELECT CASE WHEN Passenger IS NOT NULL AND Passenger<>'' THEN Passenger +'***' ELSE '' END FROM @PassTemp PT 
                                                                                                                           WHERE PT.LEGID=PT1.LEGID
                                                                                                                             AND (ID%2<>0) ORDER BY PassengerCD FOR XML PATH(''))
                                    
                                                   FROM @PassTemp PT1)PR ON PR.LEGID=ISNULL(PL.LEGID,CRL.CRLegID)
       LEFT OUTER JOIN ( SELECT DISTINCT PT1.TripID,PT1.LEGID,PassLeft=(
                                SELECT CASE WHEN Passenger IS NOT NULL AND Passenger<>'' THEN Passenger +'***' ELSE '' END FROM @PassTemp PT 
                                                                                                                           WHERE  PT.LEGID=PT1.LEGID
                                                                                                                             AND (ID%2=0) ORDER BY PassengerCD FOR XML PATH('') )
                                    
                                                    FROM @PassTemp PT1)PLT ON PLT.LEGID=ISNULL(PL.LEGID,CRL.CRLegID)
        INNER JOIN @TempPassengerID TP ON TP.PassengerID = ISNULL(TripPax.PassengerRequestorID, P.PassengerRequestorID) 
		INNER JOIN  @PassTemp PT ON PT.PassengerID=ISNULL(TripPax.PassengerRequestorID, P.PassengerRequestorID)
		
		WHERE CRM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
			AND ISNULL(PM.EstDepartureDT,CRM.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
			AND CRM.IsDeleted = 0
			---AND (ISNULL(TripPax.PassengerRequestorCD,P.PassengerRequestorCD) IN (SELECT DISTINCT RTRIM(S) FROM DBO.SplitString(@PaxID, ',')) OR @PaxID = '')
			ORDER BY Depart,Paxcode, Leg
			
			


END		
	--EXEC spGetReportCRPassengerItinerarayInformation 'SUPERVISOR_99','10099','2010-05-01','2013-05-01',''
	

GO


