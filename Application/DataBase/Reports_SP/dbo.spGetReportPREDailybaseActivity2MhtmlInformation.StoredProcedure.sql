IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDailybaseActivity2MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDailybaseActivity2MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ===============================================================================      
-- SPC Name: spGetReportPREDailybaseActivity2ExportInformation     
-- Author: Abhishek S      
-- Create date: 27 Jul 2012      
-- Description: Get DailybaseActivity2 Export Information for REPORTS      
-- Revision History      
-- Date   Name  Ver  Change      
--       
-- ================================================================================     
CREATE PROCEDURE [dbo].[spGetReportPREDailybaseActivity2MhtmlInformation]  
        @UserCD AS VARCHAR(30), -- Mandatory  
  @DATEFROM AS DATETIME,  -- Mandatory  
  @DATETO AS DATETIME,    -- Mandatory  
  @ICAOID AS NVARCHAR(500) = '', -- Mandatory  
  @TripNUM AS NVARCHAR(500) = '', -- [Optional]  
  @TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values  
  @HomeBase AS BIT = 0  
   
AS  
BEGIN   
 SET NOCOUNT ON;  
   
 DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';  
    DECLARE @ParameterDefinition AS NVARCHAR(500)   
  
    --DECLARE  @ICAOID_N NVARCHAR(500)   
 --   DECLARE  @ICAOID_N BIGINT;   
 --SET @ICAOID_N=(SELECT AirportID FROM Airport WHERE ICAOID=@ICAOID AND IsDeleted = 0 AND CONVERT(VARCHAR,CustomerID) = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)))  
   
 SET @SQLSCRIPT = ' 
 SELECT DISTINCT  
     [locdep]=CONVERT(varchar(10), PL.DepartureDTTMLocal, dbo.GetReportDayMonthFormatByUserCD(@UserCD))  
    ,[orig_nmbr]=PM.TripNUM  
    ,[legid]=PL.LegID 
    ,outbound ='''' 
    ,[fuel_load]=ISNULL(PL.FuelLoad,0)
    ,[tail_nmbr]=F.TailNum  
    ,[ac_code]=F.AircraftCD  
    ,[activity]=CASE WHEN (A.AirportID = PL.DepartICAOID AND A.IcaoID = @ICAOID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END 
    ,[icao]=CASE WHEN (A.AirportID = PL.DepartICAOID AND A.IcaoID = @ICAOID) THEN CONVERT(varchar(10),A.IcaoID)
                                  ELSE CONVERT(varchar(10),B.IcaoID) END 
    ,[timeout]=CASE WHEN (A.AirportID = PL.DepartICAOID AND A.IcaoID = @ICAOID) THEN CONVERT(varchar(5), PL.DepartureDTTMLocal , 108) 
                                      ELSE CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108)END
    ,[pax_total] = PL.PassengerTotal  
    ,[activity2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND B.IcaoID = @ICAOID) THEN ''DEPARTS'' ELSE ''ARRIVES'' END 
    ,[icao2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND A.IcaoID = @ICAOID)THEN CONVERT(varchar(10),B.IcaoID) 
                                        ELSE CONVERT(varchar(10),A.IcaoID)END
    ,[timeout2] = CASE WHEN (PL.ArriveICAOID = B.AirportID AND B.IcaoID = @ICAOID) THEN CONVERT(varchar(5), PL.DepartureDTTMLocal , 108)
                                  ELSE CONVERT(varchar(5), PL.ArrivalDTTMLocal , 108) END
    ,[paxname]   =         CASE WHEN PL.PassengerTotal = 0 THEN '' '' ELSE PP.PassengerFirstName + '','' + PP.PassengerMiddleName + '' '' + PP.PassengerLastName END 
    ,[blocked]   =         CASE WHEN PL.PassengerTotal = 0 THEN NULL ELSE PP.IsBlocked END  
    ,[sort]      =         (CASE(PP.IsBlocked)WHEN ''TRUE'' THEN ''1'' ELSE 0 END)   
    ,[fltpurpose]=         CASE WHEN PL.PassengerTotal = 0 THEN '' '' ELSE FP.FlightPurposeCD END 
   FROM PreflightMain PM   
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
		INNER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS A ON A.AirportID = PL.DepartICAOID 
		INNER JOIN (SELECT IcaoID,AirportID FROM AIRPORT )AS B ON B.AirportID = PL.ArriveICAOID 
		INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD 
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
				WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
				OR @FleetGroupCD = '''' ) 					
		   ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID 
		LEFT OUTER JOIN PreflightPassengerList PP  
				ON PL.LegID = PP.LegID  
	    LEFT OUTER JOIN FlightPurpose FP
				ON PP.FlightPurposeID = FP.FlightPurposeID 		            
    WHERE PM.IsDeleted = 0  
		AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
		AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)   
		AND (PM.TripStatus = ''T'' OR PM.TripStatus = ''S'')   
		AND PM.RecordType <> ''M''
		AND (A.AirportID IN(SELECT AirportID FROM Airport WHERE ICAOID=@ICAOID AND IsDeleted = 0)
			 OR B.AirportID IN (SELECT AirportID FROM Airport WHERE ICAOID=@ICAOID AND IsDeleted = 0))  
		'    
          
 -- Construct optional Clauses  
     
 IF @TripNUM <> '' BEGIN    
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';  
   --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
   -- OR @FleetGroupCD = '' ) 
    
 END  
    IF @TailNUM <> '' BEGIN    
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';  
 END  
   
 IF @HomeBase = 1 BEGIN  
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));  
 END   
   
    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @ICAOID AS NVARCHAR(500),@FleetGroupCD AS VARCHAR(200)'  
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @ICAOID,@FleetGroupCD  
 PRINT @SQLSCRIPT  
  
 -- EXEC spGetReportPREDailybaseActivity2ExportInformation 'TIM','2012-10-01','2012-10-22','KHOU','', '', 'TEST,123', 0  
 -- EXEC spGetReportPREDailybaseActivity2ExportInformation 'eliza_9','2008-07-20','2012-07-22 ','','', '', '', 0  
 -- EXEC spGetReportPREDailybaseActivity2MhtmlInformation 'JWILLIAMS_13','2011-01-01','2012-01-01','','', '', '', 0 
      
 END  


GO


