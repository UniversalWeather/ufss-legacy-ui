IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLOCHotelInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLOCHotelInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportLOCHotelInformation]  
	    @UserCustomerID BIGINT
       ,@IcaoID VARCHAR(4)
       ,@AirportID VARCHAR(30)
       ,@CountryCD VARCHAR(2)
       ,@CountryID VARCHAR(30)
       ,@MilesFrom INT 
       ,@CityName VARCHAR(25)
       ,@MetroCD VARCHAR(3)
       ,@StateName VARCHAR(25)
       ,@AirportName VARCHAR(25) 
       ,@IATA VARCHAR(3)
AS

-- ================================================================================  
-- SPC Name: spGetReportLOCHotelInformation  
-- Author: Askar  
-- Create date: 17 Sep 2013  
-- Description:  Get the Locator information of Airport  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- =================================================================================   

BEGIN       
    --1.city,country and state,2.icao 3.iata,city,country,state  
SELECT DISTINCT HotelName=ISNULL(H.Name,'')
      ,H.CityName
      ,H.StateName
      ,Country=C.CountryName
      ,H.HotelCD
      ,ICAO=A.IcaoID
      ,H.HotelRating
      ,H.NegociatedRate
      ,H.PhoneNum
	  ,LatitudeRad=CASE WHEN UPPER(H.LatitudeNorthSouth) = 'S' THEN (0-(H.LatitudeDegree + (H.LatitudeMinutes / 60))) * PI() / 180  ELSE (H.LatitudeDegree + (H.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRad=CASE WHEN UPPER(H.LongitudeEastWest) = 'W' THEN (0-(H.LongitudeDegrees + (H.LongitudeMinutes / 60))) * PI() / 180  ELSE (H.LongitudeDegrees + (H.LongitudeMinutes / 60)) * PI() / 180 END 
	  ,LatitudeRadCityCenter=CASE WHEN UPPER(H.LatitudeNorthSouth) = 'S' Then (0-(H.LatitudeDegree + (H.LatitudeMinutes / 60))) * PI() / 180  ELSE (H.LatitudeDegree + (H.LatitudeMinutes / 60)) * PI() / 180 END
	  ,LongitudeRadCityCenter =CASE WHEN UPPER(H.LongitudeEastWest) = 'W' THEN (0-(H.LongitudeDegrees + (H.LongitudeMinutes / 60))) * PI() / 180  ELSE (H.LongitudeDegrees + (H.LongitudeMinutes / 60)) * PI() / 180 END 			
	  ,H.MilesFromICAO
	  ,H.LatitudeDegree
	  ,H.LatitudeMinutes
	  ,H.LatitudeNorthSouth
	  ,H.LongitudeDegrees
	  ,H.LongitudeMinutes
	  ,H.LongitudeEastWest
	  --,Latitude=(CASE WHEN H.LatitudeNorthSouth='N' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(4),H.LatitudeDegree)+CONVERT(VARCHAR(10),FLOOR(ISNULL((H.LatitudeMinutes/60),0)*100)*0.01)
	  --,Longitude=(CASE WHEN H.LongitudeEastWest='E' THEN '+' ELSE '-' END)+CONVERT(VARCHAR(4),H.LongitudeDegrees)+CONVERT(VARCHAR(10),FLOOR(ISNULL((H.LongitudeMinutes/60),0)*100)*0.01)
	  ,Latitude=(CASE WHEN H.LatitudeNorthSouth='S' THEN '-' ELSE '' END)
	  ,Longitude=(CASE WHEN H.LongitudeEastWest='W' THEN '-' ELSE '' END)
	  ,LatitudeCalculation = (H.LatitudeDegree + (FLOOR(ISNULL((H.LatitudeMinutes/60),0)*100)*0.01))
	  ,LongitudeCalculation = (H.LongitudeDegrees + (FLOOR(ISNULL((H.LongitudeMinutes/60),0)*100)*0.01))
	  ,C.CountryCD
	  INTO  #TempResultTable		
      FROM Hotel H
           LEFT OUTER JOIN Airport A ON A.AirportID=H.AirportID 
		   LEFT OUTER JOIN Metro M ON H.MetroID = M.MetroID        
		   LEFT OUTER JOIN Country C ON H.CountryID = C.CountryID 
      WHERE (((H.CustomerID = @UserCustomerID)) OR (ISNULL(CONVERT(BIGINT,@AirportID),0))=0 OR H.AirportID = CONVERT(BIGINT,@AirportID))
         AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID='')
         AND (C.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ',')) OR @CountryCD='')
         AND (H.CityName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CityName, ',')) OR @CityName='')
         AND (M.MetroCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MetroCD, ',')) OR @MetroCD='')
         AND (H.StateName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ',')) OR @StateName='')
         AND (H.Name IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportName, ',')) OR @AirportName='')
         AND (A.Iata IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IATA , ',')) OR @IATA='')
         AND H.IsDeleted=0
         AND H.CustomerID=@UserCustomerID
         AND H.MilesFromICAO  <= CASE WHEN @MilesFrom=0 THEN 999999 ELSE @MilesFrom END
        
        
    
 SELECT HotelName
       ,Country
       ,CityName
       ,StateName
       ,ICAO
       ,HotelRating
       ,NegociatedRate
       ,PhoneNum
       ,Miles=CONVERT(INT,MilesFromICAO)--ISNULL((CAST( ACOS(SIN(LatitudeRadCityCenter) * SIN(LatitudeRad) + COS(LatitudeRadCityCenter) * COS(LatitudeRad) * COS(LongitudeRad - (LongitudeRadCityCenter))) * 3959   AS INT)),0) 
       ,LatitudeDegree
	   ,LatitudeMinutes
	   ,LatitudeNorthSouth
	   ,LongitudeDegrees
	   ,LongitudeMinutes
	   ,LongitudeEastWest
	   ,Latitude
	   ,Longitude
	   ,LatitudeCalculation
	   ,LongitudeCalculation
	   ,CountryCD
	   ,HotelCD
     FROM #TempResultTable 
     ORDER BY HotelName    
 
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE #TempResultTable 
        
END

	   


GO


