IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotTypeAnalysis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotTypeAnalysis]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



        
        
CREATE  Procedure  [dbo].[spGetReportPOSTPilotTypeAnalysis]        
(     
		@DATEFROM Datetime,        
		@DATETO Datetime ,        
		@CrewCD NVARCHAR(1000)='',        
		@CrewGroupCD  NVARCHAR(1000)='',        
		@AircraftCD NVARCHAR(1000)='' ,         
		@UserCD AS VARCHAR(30),        
		@HomeBaseCD AS NVARCHAR(1000)='' 
)        
AS     

    
-- =============================================================================== 
-- SPC Name: spGetReportPOSTPilotTypeAnalysis 
-- Author: PREMPRAKASH.S 
-- Create date: 01 Aug 2012 
-- Description: Get Crew workLoad information for REPORTS 
-- Revision History 
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================ 
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @ShortDT DATETIME = dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO);
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT,
CrewCD nvarchar(10),
LastName nvarchar(30)
)
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
,c.CrewCD
,C.LastName
FROM Crew c
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE c.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1 
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID 
,c.CrewCD
,C.LastName
FROM Crew c
LEFT OUTER JOIN CrewGroupOrder cGO
ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CustomerID AND C.IsDeleted=0 
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )--AND C.IsStatus=1 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID
,c.CrewCD ---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
,C.LastName
FROM Crew C 
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
 
WHERE C.CustomerID = @customerID AND C.IsDeleted=0 AND C.IsStatus=1 
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )
END
 
 
--SELECT * FROM @TempCrewID
 
 
-----------------------------Crew and Crew Group Filteration---------------------- 
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND isStatus=1
DECLARE @TenToMin SMALLINT = 0;
 
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID =@CustomerID
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
 
DECLARE @CrewTemp TABLE (CrewID bigint,
CrewCD varchar(10)
,LastName varchar(30));
INSERT INTO @CrewTemp (CrewID, CrewCD, LastName) 
SELECT DISTINCT C.CrewID, C.CrewCD, C.LastName
FROM Crew C
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID
LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
WHERE C.CrewID IN (SELECT CrewID FROM @TempCrewID)
 
 
 
 
--Temp Fleet
 
DECLARE @TempFleet TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
AircraftID BIGINT,
FleetID BIGINT,
TailNum NVARCHAR(10)
--AircraftCD NVARCHAR(10)
);
IF @AircraftCD <> ''
BEGIN
INSERT INTO @TempFleet
SELECT F.AircraftID, F.FleetID ,F.TAILNUM
FROM Fleet F
WHERE F.CustomerID = @CustomerID
AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
(SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@CustomerID AND
AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))))
END
IF @AircraftCD = ''
BEGIN 
INSERT INTO @TempFleet
SELECT F.AircraftID, F.FleetID ,F.TAILNUM
FROM Fleet F
WHERE F.CustomerID = @CustomerID
END
 
--SELECT * FROM @TempFleet
--End Temp Fleet
--DROP TABLE #CREWINFO
SELECT DISTINCT
POC.CREWID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
, C.CrewCD CrewCode
, C.LastName CrewName
, [AircraftType] = AC.AircraftCD
, SUM(CASE WHEN POL.IsDutyEnd=1 THEN ISNULL(ROUND(POC.DutyHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID )DutyHours
, sum(ISNULL(ROUND(POC.BlockHours,1),0)) OVER (PARTITION BY POC.CREWID, AC.AircraftID ) BlockHours
, sum(ISNULL(ROUND(POC.FlightHours,1),0)) OVER (PARTITION BY POC.CREWID, AC.AircraftID ) FlightHours
, sum(CASE WHEN POC.DutyTYPE='P' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID )PICHours
, sum(CASE WHEN POC.DutyTYPE='S' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID, AC.AircraftID ) SICHours
, sum(ISNULL(POC.Night,0)) over (partition by POC.CREWID, AC.AircraftID ) NiteTime
, SUM(ISNULL(POC.Instrument,0)) over (partition by POC.CREWID, AC.AircraftID )InstTime
, SUM(ISNULL(POC.TakeOffDay,0)) over (partition by POC.CREWID, AC.AircraftID )TakeoffsDt
, SUM(ISNULL(POC.TakeOffNight,0)) over (partition by POC.CREWID, AC.AircraftID )TakeoffsNt
, SUM(ISNULL(POC.LandingDay,0)) over (partition by POC.CREWID, AC.AircraftID )LandingsDt
, SUM(ISNULL(POC.LandingNight,0)) over (partition by POC.CREWID, AC.AircraftID )LandingsNt
, SUM(ISNULL(POC.ApproachPrecision,0)) over (partition by POC.CREWID, AC.AircraftID )ApproachPr
, SUM(ISNULL(POC.ApproachNonPrecision,0)) over (partition by POC.CREWID, AC.AircraftID ) ApproachNpr
, @TenToMin TenToMin
, CONVERT(DATE,NULL) StartDate
, CONVERT(DATE,NULL) EndDate
,[Date] = CONVERT(DATE, NULL) 
INTO #CREWINFO
FROM PostflightCrew POC
-- INNER JOIN ( SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = POC.CrewID
INNER JOIN Crew C ON C.CrewID = POC.CrewID
INNER JOIN PostflightLeg POL ON POC.POLegID = POL.POLegID AND POL.IsDeleted = 0
INNER JOIN PostflightMain POM ON POC.POLogID = POM.POLogID
INNER JOIN Fleet F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
INNER JOIN @TempCrewID TC ON C.CrewID=TC.CrewID
LEFT OUTER JOIN Airport AA ON C.HomebaseID=AA.AirportID

WHERE C.IsDeleted=0 
AND C.CustomerID = @CustomerID
AND CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
AND POM.FleetID IN (SELECT FleetID from @TempFleet) AND POC.CUSTOMERID=@CustomerID
AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )
AND POM.IsDeleted = 0
 

 
DECLARE @CurDate TABLE(DateWeek DATE,CrewCD VARCHAR(5),CrewMember VARCHAR(60),CrewID BIGINT)

INSERT INTO @CurDate
SELECT T.DATE,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CrewID FROM Crew C  
                INNER JOIN @TempCrewID TC ON TC.CrewID=C.CrewID
                CROSS JOIN (SELECT DATE FROM  DBO.[fnDateTable](@DateFrom,@DateTo) ) T
                WHERE C.CustomerID=@CUSTOMERID 
                  AND C.IsDeleted=0
                  AND C.IsStatus=1 
 
INSERT INTO #CREWINFO(CREWID,NiteTime,InstTime,TakeoffsDt,TakeoffsNt,LandingsDt,LandingsNt,ApproachPr,ApproachNpr,StartDate,EndDate,DutyHours,BlockHours,FlightHours,PICHours,SICHours,AircraftType)
SELECT PSL.CrewID
      ,PSL.Night
      ,PSL.Instrument
      ,PSL.TakeOffDay
      ,PSL.TakeOffNight
      ,PSL.LandingDay
      ,PSL.LandingNight
      ,PSL.ApproachPrecision
      ,PSL.ApproachNonPrecision
      ,CASE WHEN  PSL.SessionDT <= @DATEFROM THEN @DATEFROM
                              WHEN  PSL.SessionDT >= @DATETO THEN @DATETO ELSE PSL.SessionDT END
       ,DATEADD(DAY,(DATEDIFF(DAY,PSL.DepartureDTTMLocal,PSL.ArrivalDTTMLocal)+1),PSL.SessionDT)
       ,ISNULL(ROUND(PSL.DutyHours,1),0) DutyHours
       ,ISNULL(ROUND(PSL.FlightHours,1),0) FlightHours
       ,ISNULL(ROUND(PSL.FlightHours,1),0) FlightHours
       ,CASE WHEN PSL.DutyTYPE='P' THEN ROUND(PSL.FlightHours,1) ELSE 0 END
       ,CASE WHEN PSL.DutyTYPE='S' THEN ROUND(PSL.FlightHours,1) ELSE 0 END
       ,AT.AircraftCD
      FROM PostflightSimulatorLog  PSL
           INNER JOIN Crew C ON PSL.CrewID=C.CrewID
           INNER JOIN @TempCrewID TC ON C.CrewID = TC.CREWID
           INNER JOIN Aircraft AT ON AT.AircraftID=PSL.AircraftID
           LEFT OUTER JOIN CrewDutyType CD ON PSL.DutyTypeID=CD.DutyTypeID
           LEFT OUTER  JOIN Client CL ON C.ClientID=PSL.ClientID
    WHERE PSL.CustomerID=@CUSTOMERID
    AND (CONVERT(DATE,PSL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) OR
         CONVERT(DATE,PSL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO))
    
       
INSERT INTO #CREWINFO(CREWID,NiteTime,InstTime,TakeoffsDt,TakeoffsNt,LandingsDt,LandingsNt,ApproachPr,ApproachNpr,[Date],DutyHours,BlockHours,FlightHours,PICHours,SICHours,AircraftType)
SELECT DISTINCT CI.CREWID,CI.NiteTime,CI.InstTime,CI.TakeoffsDt,CI.TakeoffsNt,CI.LandingsDt,CI.LandingsNt,CI.ApproachPr,CI.ApproachNpr,CD.DateWeek,CI.DutyHours,CI.BlockHours,CI.FlightHours,CI.PICHours,CI.SICHours,CI.AircraftType
				FROM Crew C INNER JOIN  @CurDate CD ON CD.CrewID=C.CrewID
				            INNER JOIN @TempCrewID TC ON C.CrewID = TC.CREWID
				            LEFT JOIN #CrewInfo CI ON CD.CrewID=CI.CrewID
			    WHERE  CONVERT(DATE,CD.DateWeek) > CONVERT(DATE,CI.StartDate)
				  AND  CONVERT(DATE,CD.DateWeek) < CONVERT(DATE, CI.EndDate ) 
 
IF @CrewCD<>'' OR @CrewGroupCD <>''
BEGIN
SELECT 
CT.CREWID
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,CT.CrewCD [CrewCode]
,CT.LastName [CrewName]
,ISNULL(CI.[AircraftType],'') AircraftCD
,ISNULL(CI.[DutyHours],0) DutyHours
,ISNULL(CI.[BlockHours],0) BlockHours
,ISNULL(CI.[FlightHours],0) FlightHours
,ISNULL(CI.[PICHours],0) PICHours
,ISNULL(CI.[SICHours],0) SICHours
,ISNULL(CI.[NiteTime],0) NiteTime
,ISNULL(CI.[InstTime],0) InstTime
,ISNULL(CI.[TakeoffsDt],0) TakeoffsDt
,ISNULL(CI.[TakeoffsNt],0) TakeoffsNt
,ISNULL(CI.LandingsDt,0) LandingsDt
,ISNULL(CI.[LandingsNt],0) LandingsNt
,ISNULL(CI.[ApproachPr],0) ApproachPr
,ISNULL(CI.[ApproachNpr],0) ApproachNpr
,[TenToMin] = @TenToMin
FROM @TempCrewID CT 
LEFT JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID --AND CT.CrewID IN (SELECT CREWID FROM #CREWINFO)
ORDER BY CrewCD
END 
IF @CrewCD='' AND @CrewGroupCD=''
BEGIN
SELECT 
CT.CREWID
--,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ @ShortDT
,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,CT.CrewCD [CrewCode]
,CT.LastName [CrewName]
,ISNULL(CI.[AircraftType],'') AircraftCD
,ISNULL(CI.[DutyHours],0) DutyHours
,ISNULL(CI.[BlockHours],0) BlockHours
,ISNULL(CI.[FlightHours],0) FlightHours
,ISNULL(CI.[PICHours],0) PICHours
,ISNULL(CI.[SICHours],0) SICHours
,ISNULL(CI.[NiteTime],0) NiteTime
,ISNULL(CI.[InstTime],0) InstTime
,ISNULL(CI.[TakeoffsDt],0) TakeoffsDt
,ISNULL(CI.[TakeoffsNt],0) TakeoffsNt
,ISNULL(CI.LandingsDt,0) LandingsDt
,ISNULL(CI.[LandingsNt],0) LandingsNt
,ISNULL(CI.[ApproachPr],0) ApproachPr
,ISNULL(CI.[ApproachNpr],0) ApproachNpr
,[TenToMin] = @TenToMin
FROM @TempCrewID CT 
INNER JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID --AND CT.CrewID IN (SELECT CREWID FROM #CREWINFO)
ORDER BY CrewCD
END 

IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE  #CREWINFO
 
 
--EXEC spGetReportPOSTPilotTypeAnalysis '8/1/2000','8/10/2017','','' ,'','JWILLIAMS_13' ,'' 


GO

