IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetAnnualStatisticsFlightHrsInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightHrsInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




  
CREATE PROCEDURE [dbo].[spGetReportPOSTFleetAnnualStatisticsFlightHrsInformation1]              
( @UserCD VARCHAR(30), --Mandatory 
  @MONTHFROM AS VARCHAR(3)='', --Mandatory
  @MONTHTO AS  VARCHAR(3)='', --Mandatory                           
  @YEAR INT='', -- Mandatory              
  @TailNum VARCHAR(5000)='',              
  @FleetGroupCD VARCHAR(5000)='',              
  @DepartmentCD VARCHAR(5000)='',              
  @DepartmentGroupID VARCHAR(5000)='',              
  @HomeBaseCD VARCHAR(5000)='',              
  @FlightCatagoryCD VARCHAR(5000)='',              
  @IsHomebase bit=0,
  @PrintType Char(1)='B'             
 )                  
 AS           
 SET NOCOUNT ON          
 
-- ===============================================================================
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
             
   BEGIN  
   
   SET @MONTHFROM=(SELECT CASE WHEN @MONTHFROM<> '' AND @MONTHFROM IS NOT NULL THEN @MONTHFROM ELSE  'JAN' END )
   SET @MONTHTO=(SELECT CASE WHEN @MONTHTO<> '' AND @MONTHTO IS NOT NULL THEN @MONTHTO ELSE  'DEC' END )
   
   DECLARE  @DATEFROM  DATETIME =(SELECT DATEADD(month,( MONTH(CAST(@MONTHFROM+ '1 2010' AS datetime)) )-1,DATEADD(year,@YEAR-1900,0)))
   DECLARE @DATETO DATETIME =(SELECT DATEADD(day,-1,DATEADD(month,( MONTH(CAST(@MONTHTO+ '1 2010' AS datetime))),DATEADD(year,@YEAR-1900,0))))  


                         
   Declare @TenToMin Int
   Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))                                  
   DECLARE @ParameterDefinition AS NVARCHAR(400) 
   
   DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

 Declare @SuppressActivityAircraft BIT  

SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '' 
   
   IF OBJECT_ID('tempdb..#TmpTble') is not null                        
      DROP table #TmpTble 
                
      Create Table #TmpTble (Tot Numeric(10,2), TailNum Varchar(9), ScheduledTM DateTime)                             
                   
       SET @SQLSCRIPT =  'Insert Into #TmpTble SELECT IsNull(SUM(ROUND(PostflightLeg.BlockHours,1)),0) AS Tot, '              
       IF @PrintType='H'              
          BEGIN              
            SET @SQLSCRIPT =  'Insert Into #TmpTble SELECT IsNull(SUM(ROUND(PostflightLeg.FlightHours,1)),0) AS Tot, '              
          END                  
                        
          SET @SQLSCRIPT = @SQLSCRIPT + '       
              F.TailNum ,PostflightLeg.ScheduledTM                    
              FROM  (SELECT * FROM  PostflightLeg WHERE IsDeleted=0) PostflightLeg INNER JOIN                    
              PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0  
              INNER JOIN                    
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum,F.HomeBaseID         
                FROM Fleet F             
                LEFT OUTER JOIN FleetGroupOrder FGO            
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID            
                LEFT OUTER JOIN FleetGroup FG             
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID     
                WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) OR @FleetGroupCD = ''''             
              ) F ON F.FleetID = PostflightMain.FleetID      
              LEFT JOIN       
              ( SELECT Distinct D.DepartmentID,D.DepartmentCD     
                FROM   Department D             
                LEFT OUTER JOIN DepartmentGroupOrder DGO            
                ON D.DepartmentID = DGO.DepartmentID AND D.CustomerID = DGO.CustomerID            
                LEFT OUTER JOIN DepartmentGroup DG             
                ON DGO.DepartmentGroupID = DG.DepartmentGroupID AND DGO.CustomerID = DG.CustomerID 
                WHERE DG.DepartmentGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentGroupID, '','')) OR @DepartmentGroupID = ''''                          
              ) D ON D.DepartmentID = PostflightMain.DepartmentID                
              LEFT JOIN                    
              FlightCatagory ON PostflightLeg.FlightCategoryID = FlightCatagory.FlightCategoryID     
              LEFT JOIN
              Company ON PostflightMain.HomebaseID = Company.HomebaseID 
              LEFT JOIN
              Airport ON Company.HomebaseAirportID =  Airport.AirportID                  
              WHERE PostflightMain.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
                                  
        --Construct OPTIONAL clauses                              
  
  IF @YEAR <> '' BEGIN
  SET @SQLSCRIPT = @SQLSCRIPT + 'AND CONVERT(DATE,PostflightLeg.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)'
  END

  IF @TailNum <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNum IN (''' + REPLACE(CASE WHEN RIGHT(@TailNum, 1) = ',' THEN LEFT(@TailNum, LEN(@TailNum) - 1) ELSE @TailNum END, ',', ''', ''') + ''')';                              
  END                               
                              
  IF @DepartmentCD  <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND D.DepartmentCD IN (''' + REPLACE(CASE WHEN RIGHT(@DepartmentCD , 1) = ',' THEN LEFT(@DepartmentCD, LEN(@DepartmentCD) - 1) ELSE @DepartmentCD END, ',', ''', ''') + ''')';                              
  END                               
                                  
  IF @FlightCatagoryCD  <> '' BEGIN                     
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND FlightCatagory.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';               
   
  END                    
  IF @HomeBaseCD  <> '' BEGIN                                
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND Airport.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@HomeBaseCD, 1) = ',' THEN LEFT(@HomeBaseCD, LEN(@HomeBaseCD) - 1) ELSE @HomeBaseCD END, ',', ''', ''') + ''')';                              
  END                
  IF @IsHomebase = 1 BEGIN            
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND PostflightMain.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));            
  END               
  IF @PrintType='F'              
     BEGIN              
       SET @SQLSCRIPT = @SQLSCRIPT + ' GROUP BY  PostflightLeg.FlightHours, F.TailNum,PostflightLeg.ScheduledTM '            
     END                           
  Else  
     BEGIN              
      SET @SQLSCRIPT = @SQLSCRIPT + ' GROUP BY  PostflightLeg.BlockHours, F.TailNum,PostflightLeg.ScheduledTM '              
     END   
                 
  SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @YEAR As Int, @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNum As VARCHAR(5000), @FleetGroupCD As VARCHAR(5000),                
  @DepartmentCD As VARCHAR(5000),@DepartmentGroupID As VARCHAR(5000),@HomeBaseCD aS VARCHAR(5000),                
  @FlightCatagoryCD As VARCHAR(5000),@IsHomebase Bit'                              
              
  EXECUTE sp_executesql @SQLSCRIPT , @ParameterDefinition, @UserCD, @YEAR, @DATEFROM, @DATETO,@TailNum, @FleetGroupCD,@DepartmentCD,@DepartmentGroupID,@HomeBaseCD,@FlightCatagoryCD,@IsHomebase                     
  
   IF OBJECT_ID('tempdb..#Tmp') is not null                        
      DROP table #Tmp              
  Declare @TailNumber Varchar(9)               
  Declare @Jan1 Numeric(10,2)=0
  Declare @Feb1 Numeric(10,2)=0
  Declare @Mar1 Numeric(10,2)=0
  Declare @Apr1 Numeric(10,2)=0
  Declare @May1 Numeric(10,2)=0
  Declare @Jun1 Numeric(10,2)=0
  Declare @Jul1 Numeric(10,2)=0
  Declare @Aug1 Numeric(10,2)=0
  Declare @Sep1 Numeric(10,2)=0
  Declare @Oct1 Numeric(10,2)=0
  Declare @Nov1 Numeric(10,2)=0
  Declare @Dec1 Numeric(10,2)=0
  Declare @YTD1 Numeric(10,2)=0
     
  Create Table #Tmp (TailNum Varchar(9))  
  
  INSERT INTO #Tmp 
  SELECT DISTINCT TailNum  
  FROM   Fleet F LEFT OUTER JOIN FleetGroupOrder FGO              
         ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID LEFT OUTER JOIN FleetGroup FG               
         ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
  WHERE (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
        (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
         F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
        
   
  IF OBJECT_ID('tempdb..#TmpRep') is not null                        
      DROP table #TmpRep         
  CREATE TABLE #TmpRep (TailNum Varchar(9),Jan1 Numeric(10,2), Feb1 Numeric(10,2), Mar1 Numeric(10,2), Apr1 Numeric(10,2), May1 Numeric(10,2), Jun1 Numeric(10,2),  
                                          Jul1 Numeric(10,2), Aug1 Numeric(10,2), Sep1 Numeric(10,2), Oct1 Numeric(10,2), Nov1 Numeric(10,2), Dec1 Numeric(10,2),YTD1 Numeric(10,2))
   Declare @RowCnt Int=0
   WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0               
   BEGIN          
    Select Top 1 @TailNumber=TailNum From #Tmp             
    
    Set @Jan1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=1)
    Set @Feb1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=2)
    Set @Mar1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=3)
    Set @Apr1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=4)
    Set @May1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=5)
    Set @Jun1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=6)
    Set @Jul1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=7)
    Set @Aug1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=8)
    Set @Sep1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=9)
    Set @Oct1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=10)
    Set @Nov1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=11)
    Set @Dec1=(Select IsNull(SUM(Tot),0) From #TmpTble Where TailNum=@TailNumber And MONTH(ScheduledTM)=12)
    Set @YTD1= @Jan1 + @Feb1 + @Mar1 + @Apr1 + @May1 + @Jun1 + @Jul1 + @Aug1 + @Sep1 + @Oct1 + @Nov1 + @Dec1
     
    Insert Into #TmpRep(TailNum,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1)
    Values (@TailNumber,@Jan1,@Feb1,@Mar1,@Apr1,@May1,@Jun1,@Jul1,@Aug1,@Sep1,@Oct1,@Nov1,@Dec1,@YTD1) 
    
    Delete From #Tmp Where TailNum=@TailNumber 
    Set @RowCnt=1
   END  
   
   Declare @RowCnts Int=0
   IF @TailNum<>'' OR @FleetGroupCD<>''
   Begin
     Delete From #TmpRep Where YTD1=0
     Set @RowCnts=(Select Count(*) From #TmpRep)
     Set @RowCnt=0
     IF @RowCnts>0
     Begin
        Set @RowCnt=1
     End 
   End
   
    IF @SuppressActivityAircraft=1
   
   BEGIN
   DELETE FROM #TmpRep WHERE (Jan1 IS NULL OR Jan1=0.00)
				         AND  (Feb1 IS NULL OR Feb1=0.00)
				         AND  (Mar1 IS NULL OR Mar1=0.00)
				         AND  (Apr1 IS NULL OR Apr1=0.00)
				         AND  (May1 IS NULL OR May1=0.00)
				         AND  (Jun1 IS NULL OR Jun1=0.00)
				         AND  (Jul1 IS NULL OR Jul1=0.00)
				         AND  (Aug1 IS NULL OR Aug1=0.00)
				         AND  (Sep1 IS NULL OR Sep1=0.00)
				         AND  (Oct1 IS NULL OR Oct1=0.00)
				         AND  (Nov1 IS NULL OR Nov1=0.00)
				         AND  (Dec1 IS NULL OR Dec1=0.00)
				         AND  (YTD1 IS NULL OR YTD1=0.00)
				         AND  (Jan1 IS NULL OR Jan1=0.00)
   END
   
   IF @SuppressActivityAircraft=0
   BEGIN
   
   IF @RowCnt=1
     Begin
     Select TailNum,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin From #TmpRep       
     End
   Else 
     Begin
     Select '' As TailNum,0.00 As Jan1,0.00 As Feb1,0.00 As Mar1,0.00 As Apr1,0.00 As May1,0.00 As Jun1,0.00 As Jul1,0.00 As Aug1,0.00 As Sep1,0.00 As Oct1,0.00 As Nov1,0.00 As Dec1,0.00 As YTD1,2 As TenToMin 
     End
     
     END
   ELSE
    BEGIN
       Select TailNum,Jan1,Feb1,Mar1,Apr1,May1,Jun1,Jul1,Aug1,Sep1,Oct1,Nov1,Dec1,YTD1,@TenToMin TenToMin From #TmpRep       
     END
   IF OBJECT_ID('tempdb..#Tmp') is not null                        
     DROP table #Tmp 
   IF OBJECT_ID('tempdb..#TmpRep') is not null                        
     DROP table #TmpRep                             
   IF OBJECT_ID('tempdb..#TmpTble') is not null                        
     DROP table #TmpTble
--Exec spGetReportPOSTFleetAnnualStatisticsFlightHrsInformation1 'SUPERVISOR_99','JAN','FEB',2009,'','','','','','',1,'B'
--Exec spGetReportPOSTFleetAnnualStatisticsFlightHrsInformation1 'SUPERVISOR_99','JAN','FEB','2009-1-1','2009-12-31','2009','','','','','','',1,'B'
                               
END 



GO


