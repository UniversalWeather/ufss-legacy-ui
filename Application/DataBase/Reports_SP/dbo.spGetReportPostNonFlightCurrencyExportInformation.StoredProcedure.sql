
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPostNonFlightCurrencyExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPostNonFlightCurrencyExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spGetReportPostNonFlightCurrencyExportInformation]    Script Date: 08/06/2012 
SET ANSI_NULLS ON
--GO

SET QUOTED_IDENTIFIER ON
--GO



-- ===============================================================================
-- SPC Name: spGetReportPostNonFlightCurrencyExportInformation
-- Author:Badrinath
-- Create date: 06 August 2012
-- Description: Get Non flight Currency Export Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
*/

CREATE PROCEDURE [dbo].[spGetReportPostNonFlightCurrencyExportInformation]
	  @UserCD AS VARCHAR(30)
	 ,@AsOf Datetime
	 ,@CrewCD NVARCHAR(1000)=''
	 ,@CrewGroupCD NVARCHAR(1000)=''
	 ,@AircraftCD NVARCHAR(1000)=''
	 ,@IsHomebase as bit=0
	AS

SET NOCOUNT ON

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
	DECLARE @AppDTFormat VARCHAR(25)
	SELECT @AppDTFormat=Upper(ApplicationDateFormat) FROM Company 
	       WHERE HomeBaseID =  dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
	       AND CustomerID = @CUSTOMER
	       
	DECLARE @DATEFORMAT VARCHAR(20)= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN 
							'- -'
					WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN
							'. .'
					ELSE
							'/  /'
	          END
	       
	--       select distinct ApplicationDateFormat from Company


DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID =@CUSTOMER																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=0
--DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @CrewCD <> '' 
      BEGIN
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            WHERE c.CustomerID = @CUSTOMER AND C.IsDeleted=0 --AND C.IsStatus=1  
            AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
      END
IF @CrewGroupCD <> '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew C
            LEFT OUTER JOIN CrewGroupOrder cGO
                  ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
            LEFT OUTER JOIN CrewGroup CG 
                  ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
            WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
                  AND C.CustomerID = @CUSTOMER AND C.IsDeleted=0 AND C.IsStatus=1 
      END
ELSE IF @CrewCD = '' AND @CrewGroupCD = '' 
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
            FROM Crew C 
            WHERE C.CustomerID = @CUSTOMER AND C.IsDeleted=0 AND C.IsStatus=1 
      END

DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 0
--SELECT * FROM @DEPARTMENT

SELECT DISTINCT
			  [asof]= @Asof, 
              [crewcode] =C.CrewCD,
              [lastname]=C.LastName,
			  [code]=CCD.CheckListCD,
			  [typecode] =AC.AircraftCD,
			  [desc]=CCl.CrewChecklistDescription,
			  [previous]=CCD.PreviousCheckDT,
              [due]=CCD.DueDT,
              [alert]=CCD.AlertDT,
              [grace]=CCD.GraceDT,
              [DATEFORMAT]=@DATEFORMAT
INTO #NONFLIGEXP
FROM Crew c
    INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
	INNER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID AND CCD.IsDeleted=0 AND C.IsDeleted=0
	LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID 
	LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID		
	LEFT OUTER JOIN Airport a on a.AirportID =c.HomebaseID
WHERE c.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	--and c.IsDeleted = 0 --AND c.IsStatus = 1
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	AND (C.HomebaseID =dbo.GetHomeBaseAirportIDByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
ORDER BY CrewCoDe	
IF @IsZeroSuppressActivityCrewRpt=0
BEGIN
SELECT * FROM #NONFLIGEXP
UNION ALL
SELECT DISTINCT
	          [asof]=null, 
	          [crewcode] =C.CrewCD,
              [lastname]=C.LastName,
			  [code]=null,
			  [typecode] =null,
			  [desc]='NO ITEMS',
			  [previous]=null,
              [due]=null,
              [alert]=null,
              [grace]=null,
              [DATEFORMAT]=@DATEFORMAT
FROM Crew C 
    INNER JOIN (SELECT distinct CREWID FROM @TempCrewID ) C1 ON C1.CREWID = C.CrewID
	LEFT OUTER JOIN @CREW CR ON CR.CREWID = C.CrewID
	--AND C.IsDeleted = 0 AND C.IsStatus = 0
	LEFT OUTER JOIN CREWCheckListDetail CCD ON C.CrewID=CCD.CrewID	 
	LEFT OUTER JOIN  Aircraft AC ON CCD.AircraftID=AC.AircraftID 
	LEFT OUTER join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND C.CustomerID=ccl.CustomerID	
	LEFT OUTER JOIN Airport a on a.AirportID =c.HomebaseID
WHERE c.crewcd not in (select DISTINCT crewcode from #NONFLIGEXP)  
	and C.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
	--AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftTypeCD, ',')) OR @AircraftTypeCD = '')
	--AND (C.HomebaseID = dbo.GetHomeBaseAirportIDByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
	--and c.IsDeleted = 0 AND c.IsStatus = 1
ORDER BY CrewCoDe
END
ELSE
BEGIN
SELECT * FROM #NONFLIGEXP
ORDER BY CrewCoDe
END
   
IF OBJECT_ID('tempdb..#NONFLIGEXP') IS NOT NULL
DROP TABLE #NONFLIGEXP


-- EXEC spGetReportPostNonFlightCurrencyExportInformation 'jwilliams_13','2019/12/31','','','',1



GO


