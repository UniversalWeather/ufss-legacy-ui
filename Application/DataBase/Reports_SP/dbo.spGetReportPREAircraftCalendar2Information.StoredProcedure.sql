IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar2Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar2Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spGetReportPREAircraftCalendar2Information]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FlightCatagoryCD AS NVARCHAR(1000) = '',
		@IsHomeBase AS BIT = 0,
		@IsAvailable AS BIT =0
AS
-- ==============================================================================
-- SPC Name: spGetReportPREAircraftCalendar2Information
-- Author: SUDHAKAR J
-- Create date: 11 Jul 2012
-- Description: Get Preflight Aircraft Calendar information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

BEGIN
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

--Declare @SuppressActivityAircft BIT  
--SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMER
--										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
--										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------
 Declare @TenToMin Int
  Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 
	          
	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(200)
	


	SET @SQLSCRIPT = '
		INSERT INTO #TempAircraftCal (DeptDateOrg, FleetID, TailNum, RowOrder, DepartArrive, City,
			StateName, Airport, SeatsAvailable, IsDeleted, FleetGroupID,FlightCatagoryCD, AircraftCD, HomebaseID, LegID,LegNum,TripNum)
		SELECT X.*
		FROM (
			SELECT
				[DateSort] = CONVERT(DATE, PL.DepartureDTTMLocal)
				,[FleetID] = PM.FleetID
				,[TailNum] = F.TailNum
				,[RowOrder] = ''D'' --Departure
				,[DepartArrive] = PL.DepartureDTTMLocal
				,[City] = AD.IcaoID + ''  '' + AD.CityName
				,[State] = AD.StateName
				,[Airport] = AD.AirportName
				,[SeatsAvailable] = CASE WHEN PL.ReservationAvailable=0 AND @IsAvailable=0 THEN  NULL ELSE  PL.ReservationAvailable  END
				,[IsDeleted] = PL.IsDeleted
				,[FleetGroupID] = ''''
				,[FlightCatagoryCD]  = FlightCatagoryCD
				,[AircraftCD] = A.AircraftCD
				,[HomebaseID] = PM.HomebaseID
				,PL.LegID
				,PL.LegNum
				,PM.TripNum
			FROM PreflightLeg PL 
				LEFT OUTER JOIN (
					SELECT AirportID, IcaoID, CityName, StateName, AirportName FROM Airport
				) AD ON PL.DepartICAOID = AD.AirportID
				INNER JOIN (
					SELECT TRIPID, FLEETID, HomebaseID, AircraftID, EstDepartureDT, CustomerID,TripStatus,TripNum
						FROM PreflightMain WHERE ISDELETED=0
				) PM ON PL.TripID = PM.TripID
				LEFT OUTER JOIN (
					SELECT AircraftID, AircraftCD FROM Aircraft
				) A ON PM.AircraftID = A.AircraftID
				LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = PL.FlightCategoryID
				INNER JOIN (
 					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,
 					F.MaximumPassenger
					FROM Fleet F 
				) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
				INNER JOIN #TempFleetID TF ON TF.FleetID = F.FleetID
			WHERE PM.CustomerID = ' + CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				+ ' AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)	
				AND PM.Tripstatus IN (''T'',''H'')
				AND PL.IsDeleted=0	
	
			UNION ALL

			SELECT
				[DateSort] = CONVERT(DATE, PL.DepartureDTTMLocal)
				,[FleetID] = PM.FleetID
				,[TailNum] = F.TailNum
				,[RowOrder] = ''A'' -- Arrival
				,[DepartArrive] = PL.ArrivalDTTMLocal
				,[City] = AA.IcaoID + ''  '' + AA.CityName 
				,[State] = AA.StateName
				,[Airport] = AA.AirportName
				,[SeatsAvailable] = CASE WHEN PL.ReservationAvailable=0 AND @IsAvailable=0 THEN  NULL ELSE  PL.ReservationAvailable  END
				,[IsDeleted] = PL.IsDeleted
				,[FleetGroupID] = ''''
				,[FlightCatagoryCD]  = FlightCatagoryCD
				,[AircraftCD] = A.AircraftCD
				,[HomebaseID] = PM.HomebaseID
				,PL.LegID
				,PL.LegNum
				,PM.TripNum
			FROM PreflightLeg PL 
				LEFT OUTER JOIN ( 
					SELECT AirportID, IcaoID, CityName, StateName, AirportName FROM Airport
				) AA ON PL.ArriveICAOID = AA.AirportID
				INNER JOIN (
					SELECT TRIPID, FLEETID, HomebaseID, AircraftID, EstDepartureDT, CustomerID,TripStatus,TripNum
						FROM PreflightMain WHERE IsDeleted=0
				) PM ON PL.TripID = PM.TripID
				LEFT OUTER JOIN (
					SELECT AircraftID, AircraftCD FROM Aircraft
				) A ON PM.AircraftID = A.AircraftID
				LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = PL.FlightCategoryID
				INNER JOIN (
 					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,
 						F.MaximumPassenger
					FROM Fleet F 
				) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
				INNER JOIN #TempFleetID TF ON TF.FleetID = F.FleetID
			WHERE PM.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				+ ' AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)	
			AND PM.Tripstatus IN (''T'',''H'') AND PL.IsDeleted=0	

			
		) X WHERE X.IsDeleted = 0 
	'
DECLARE @SQLSCRIPT2 VARCHAR(4000) = ''
	IF @AircraftCD <> '' BEGIN  
		SET @SQLSCRIPT2 = @SQLSCRIPT2 + ' AND X.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
	END
	IF @FlightCatagoryCD <> '' BEGIN  
		SET @SQLSCRIPT2 = @SQLSCRIPT2 + ' AND X.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';
	END	
	--IF @FlightCatagoryCD <> '' BEGIN  
	--	SET @SQLSCRIPT2 = @SQLSCRIPT2 + ' AND X.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';
	--	--SET @SQLSCRIPT = @SQLSCRIPT+@SQLSCRIPT2+ ' AND X.FlightCatagoryCD IN (''' + REPLACE(CASE WHEN RIGHT(@FlightCatagoryCD, 1) = ',' THEN LEFT(@FlightCatagoryCD, LEN(@FlightCatagoryCD) - 1) ELSE @FlightCatagoryCD END, ',', ''', ''') + ''')';
	--END
	
	IF @IsHomeBase = 1 BEGIN
		SET @SQLSCRIPT2 = @SQLSCRIPT2 + ' AND X.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
	END	
	
 IF @IsAvailable =1 BEGIN  
		SET @SQLSCRIPT2 = @SQLSCRIPT2 + 'AND X.SeatsAvailable > 0 
			'
	END
	
	/*
	
	SET @SQLSCRIPT2 = @SQLSCRIPT2 + 'AND X.TripNum NOT IN (
					SELECT DISTINCT PM.TripNUM 
					FROM PreflightMain PM 
					INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
					  WHERE PM.CustomerID = @CUSTOMER
						AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
						AND PL.ReservationAvailable < 0) */

--AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = ''))

	CREATE TABLE #TempAircraftCal (
		DeptDateOrg DATE, FleetID BIGINT, TailNum VARCHAR(10), RowOrder CHAR(1), DepartArrive DATETIME
		, City VARCHAR(50), StateName VARCHAR(25), Airport VARCHAR(25), SeatsAvailable INT
		, IsDeleted BIT, FleetGroupID BIGINT,FlightCatagoryCD VARCHAR(50),  AircraftCD CHAR(10), HomebaseID BIGINT, LegID BIGINT,LegNum BIGINT,TripNum BIGINT
		)
	SET @ParameterDefinition =  '@DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS VARCHAR(500),@CUSTOMER AS BIGINT,@IsAvailable AS BIT'
DECLARE @SQLSCRIPT3 NVARCHAR(MAX);
SET @SQLSCRIPT3 = @SQLSCRIPT + @SQLSCRIPT2
EXECUTE SP_EXECUTESQL @SQLSCRIPT3, @ParameterDefinition, @DATEFROM, @DATETO, @FleetGroupCD,@CUSTOMER,@IsAvailable
--print @SQLSCRIPT3
----------------------------------------------
-- Inserting Dates with NO data
DECLARE @COUNT INT;
SELECT @COUNT = COUNT(*) FROM #TempAircraftCal
	DECLARE	 @TEMPTODAY AS DATETIME = CONVERT(DATE, @DATEFROM)
	
--IF @SuppressActivityAircft = 0
BEGIN
	WHILE (@TEMPTODAY <=  CONVERT(DATE, @DATETO) AND @COUNT > 0 )
		BEGIN
			IF NOT EXISTS (SELECT DISTINCT DeptDateOrg FROM #TempAircraftCal WHERE DeptDateOrg = @TEMPTODAY)
			BEGIN
				INSERT INTO #TempAircraftCal (DeptDateOrg) VALUES (@TEMPTODAY)
			END
			SET @TEMPTODAY = @TEMPTODAY + 1
		END
END
-----------------------------------------------
SELECT DISTINCT DeptDateOrg, FleetID, TailNum,LegNum,RowOrder, DepartArrive, City,SUBSTRING(datename(dw,DeptDateOrg),1,3) [DateName],
			StateName, Airport, SeatsAvailable,
			(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange], AircraftCD,TripNum,FlightCatagoryCD
FROM #TempAircraftCal 
ORDER BY DeptDateOrg,AircraftCD, TripNum,LegNum, RowOrder DESC
-- EXEC spGetReportPREAircraftCalendar2Information 'supervisor_99', '2011-01-01', '2011-09-30'

IF OBJECT_ID('tempdb..#TempAircraftCal') IS NOT NULL
drop table #TempAircraftCal
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
drop table #TempFleetID


END




--------exec spGetReportPREAircraftCalendar2Information 'supervisor_99','2010-08-01','2010-08-10'







GO


