IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripsheetValidationBatchReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripsheetValidationBatchReport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETripsheetValidationBatchReport]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@TripNum AS VARCHAR(300) = '' -- [Optional], Comma delimited string with mutiple values
)
AS
-- ===============================================================================
-- SPC Name: spGetReportPRETripsheetValidationBatchReport
-- Author:  A.Akhila
-- Create date: 18 Feb 2013
-- Description: Get Tripsheet Validation Batch Report for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================
BEGIN
SET NOCOUNT ON
--SELECT DISTINCT PFTE2.TRIPID, EXCEPTION = 
--					(SELECT   ISNULL(ExceptionDescription,'') +'$$' 
--					 FROM PreflightTripException PFTE1 
--						INNER JOIN PreflightMain PM2 ON PM2.TripID = PFTE1.TRIPID
--					 WHERE PFTE2.TripID=PFTE1.TripID
--					  AND PM2.CustomerID = CONVERT(BIGINT,@UserCustomerID)
--					  AND (PM2.TripNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '')
--					 ORDER BY PFTE1.ExceptionDescription
--                     FOR XML PATH('')
--                     )
--                     INTO #EXCEP
--                     FROM PreflightTripException PFTE2 
SELECT DISTINCT
[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
,[TripNo] = PM.TripNUM
,[TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD)--ISNULL(dbo.GetShortDateFormatByUserCD(@UserCD,(CONVERT(VARCHAR(30),PM.EstDepartureDT))),'')  +' - '+ ISNULL(dbo.GetShortDateFormatByUserCD(@UserCD,(CONVERT(VARCHAR(30),PM.EstArrivalDT))),'')
,[Desc] = PM.TripDescription
,[Requestor] =ISNULL(P.LastName,'') +','+ ISNULL(P.FirstName,'') +' '+ ISNULL(P.MiddleInitial,'')
,[ContactNo] = P.PhoneNum
,[TailNumber] = F.TailNum
,[Type] = AC.AircraftCD
,[ValidationErrorInformation] = PFTE.ExceptionDescription--T.Exception
,TripExceptionID
FROM PreflightMain PM 
		INNER JOIN Fleet F ON F.FleetID=PM.FleetID AND F.IsDeleted=0
		INNER JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
		INNER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID	
		INNER JOIN PreflightTripException PFTE ON PFTE.TripID=PM.TripID		
WHERE PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND PM.IsDeleted=0
  AND CONVERT(DATE,PM.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND (PM.TripNUM  IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@TripNum, ',')) OR @TripNum = '')
 ORDER BY TripNUM,TripExceptionID
END
--IF OBJECT_ID('tempdb..#EXCEP') IS NOT NULL
--DROP TABLE #EXCEP
--EXEC spGetReportPRETripsheetValidationBatchReport 'SUPERVISOR_99','10099', '2000-1-1', '2020-1-1', ''


GO


