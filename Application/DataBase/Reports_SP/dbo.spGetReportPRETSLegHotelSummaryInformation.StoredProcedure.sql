IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegHotelSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegHotelSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSLegHotelSummaryInformation]
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
		@LegNum AS VARCHAR(30) = ''
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSLegHotelSummaryInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Itinerary information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
  SELECT [LegId] = CONVERT(VARCHAR,PL.LegID)
    		,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM)
			,[HDescription] = ISNULL(PPH.PreflightHotelName,'')
			,[HPhone] = ISNULL(PPH.PhoneNum1,'')
			,[HFax] = ISNULL(PPH.FaxNum,'')
			,[HAddress1] = ISNULL(PPH.Address1,'')
			,[HAddress2] = ISNULL(PPH.Address2,'')
			,[HCity] = ISNULL(PPH.CityName,'')
			,[HState] = ISNULL(PPH.StateName,'')
			,[HZip] = ISNULL(PPH.PostalZipCD,'')
			,[HRemarks] = ISNULL(PPH.Remarks,'')
			,[HRate] = ISNULL(PPH.Rate,0)
			,[HRooms] = ISNULL(PPH.NoofRooms,0)
			,[HConfirm] = ISNULL(PPH.ConfirmationStatus,'')
			,[HComments] = ISNULL(PPH.Comments,'')
			,[HTYPE] = 'PH'
			,[CrewPaxCD] = PPH.PassengerRequestorCD
			,[OrderNUM] = 9 --PPL.OrderNUM -- FIELD YET TO BE POPULATED FROM PREFLIGHT
			,[IsHotelEmpty] = CASE WHEN ISNULL(PPH.PreflightHotelName,'') = '' AND
						ISNULL(PPH.PhoneNum1,'') = '' AND
						ISNULL(PPH.FaxNum,'') = '' AND
						ISNULL(PPH.Address1,'') = '' AND
						ISNULL(PPH.Address2,'') = '' AND
						ISNULL(PPH.CityName,'') = '' AND
						ISNULL(PPH.StateName,'') = '' AND
						ISNULL(PPH.PostalZipCD,'') = '' AND
						ISNULL(PPH.Remarks,'') = '' AND
						ISNULL(PPH.Rate,0) = 0 AND																																																															
						ISNULL(PPH.NoofRooms,0) = 0 AND
						ISNULL(PPH.ConfirmationStatus,'') = '' AND
						ISNULL(PPH.Comments,'') = ''
					THEN 1 ELSE 0 END
				,[Flag]	 = ''
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			LEFT OUTER JOIN (
				SELECT PPL.LegID, PPH.PreflightHotelName, PPH.PhoneNum1, PPH.FaxNum, PPH.Address1, PPH.Address2,
					PPH.CityName, PPH.StateName, PPH.PostalZipCD, HP.Remarks, PPH.Rate, PPL.NoofRooms, PPH.ConfirmationStatus,
					PPH.Comments, P.PassengerRequestorCD
				FROM PreflightPassengerList PPL
				INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
				INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
				LEFT OUTER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			) PPH ON PL.LegID = PPH.LegID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')			

			UNION ALL
		
			--Crew Hotel AND Addnl Crew / Maint Crew - Hotel	
			SELECT  [LegId] = CONVERT(VARCHAR,PL.LegID)
				,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM) 
				,ISNULL(PCH.PreflightHotelName,'')
				,ISNULL(PCH.PhoneNum1,'')
				,ISNULL(PCH.FaxNum,'')
				,ISNULL(PCH.Address1,'')
				,ISNULL(PCH.Address2,'')
				,ISNULL(PCH.CityName,'')
				,ISNULL(PCH.StateName,'')
				,ISNULL(PCH.PostalZipCD,'')
				,ISNULL(HC.Remarks,'')
				,ISNULL(PCH.Rate,0)
				,ISNULL(PCLL.NoofRooms,0)
				,ISNULL(PCH.ConfirmationStatus,'')
				,ISNULL(PCH.Comments,'')
				,HTYPE = 'CH'
				,[CrewPaxCD] = PCLL.CrewCD
				,[OrderNUM] =	--CASE PCLL.DutyType WHEN 'P' THEN 1 WHEN 'S' THEN 2 ELSE 3 END  
								CASE PCLL.DutyTYPE WHEN 'P' THEN 1 
								WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
								WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END 
				,[IsHotelEmpty] = CASE WHEN ISNULL(PCH.PreflightHotelName,'') = '' AND
							ISNULL(PCH.PhoneNum1,'') = '' AND
							ISNULL(PCH.FaxNum,'') = '' AND
							ISNULL(PCH.Address1,'') = '' AND
							ISNULL(PCH.Address2,'') = '' AND
							ISNULL(PCH.CityName,'') = '' AND
							ISNULL(PCH.StateName,'') = '' AND
							ISNULL(PCH.PostalZipCD,'') = '' AND
							ISNULL(HC.Remarks,'') = '' AND
							ISNULL(PCH.Rate,0) = 0 AND																																																															
							ISNULL(PCLL.NoofRooms,0) = 0 AND
							ISNULL(PCH.ConfirmationStatus,'') = '' AND
							ISNULL(PCH.Comments,'') = ''
						THEN 1 ELSE 0 END
					,[Flag] = CASE WHEN PCH.isArrivalHotel = 1 THEN 'A'
								WHEN PCH.isDepartureHotel = 1 THEN 'D' ELSE NULL END
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PL.LegID AND PCH.crewPassengerType = 'C'
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			LEFT JOIN (SELECT CrewCD, LegID, C.CrewID, NoofRooms, DutyTYPE
						   FROM PreflightCrewList PCL 
							    INNER JOIN Crew C ON PCL.CrewID = C.CrewID --AND PCL.DutyTYPE  IN ('P','S')
					  ) PCLL ON PL.LegID = PCLL.LegID
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID --CREW ASSIGNED
															AND PHCP.CrewID = PCLL.CREWID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')

/*
			UNION ALL
			--Crew Hotel AND Addnl Crew / Maint Crew - Hotel	
			SELECT  [LegId] = CONVERT(VARCHAR,PL.LegID)
					,[LegNUM] = CONVERT(VARCHAR,PL.LegNUM) 
					,ISNULL(PCH.PreflightHotelName,'')
					,ISNULL(PCH.PhoneNum1,'')
					,ISNULL(PCH.FaxNum,'')
					,ISNULL(PCH.Address1,'')
					,ISNULL(PCH.Address2,'')
					,ISNULL(PCH.CityName,'')
					,ISNULL(PCH.StateName,'')
					,ISNULL(PCH.PostalZipCD,'')
					,ISNULL(HC.Remarks,'')
					,ISNULL(PCH.Rate,0)
					,ISNULL(PCLL.NoofRooms,0)
					,ISNULL(PCH.ConfirmationStatus,''),ISNULL(PCH.Comments,''), HTYPE = 'MH'
					,[CrewPaxCD] = PCLL.CrewCD
					,[OrderNUM] =	--CASE PCLL.DutyType WHEN 'P' THEN 1 WHEN 'S' THEN 2 ELSE 3 END  
								CASE PCLL.DutyTYPE WHEN 'P' THEN 1 
								WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
								WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END 
					,[IsHotelEmpty] = CASE WHEN ISNULL(PCH.PreflightHotelName,'') = '' AND
							ISNULL(PCH.PhoneNum1,'') = '' AND
							ISNULL(PCH.FaxNum,'') = '' AND
							ISNULL(PCH.Address1,'') = '' AND
							ISNULL(PCH.Address2,'') = '' AND
							ISNULL(PCH.CityName,'') = '' AND
							ISNULL(PCH.StateName,'') = '' AND
							ISNULL(PCH.PostalZipCD,'') = '' AND
							ISNULL(HC.Remarks,'') = '' AND
							ISNULL(PCH.Rate,0) = 0 AND																																																															
							ISNULL(PCLL.NoofRooms,0) = 0 AND
							ISNULL(PCH.ConfirmationStatus,'') = '' AND
							ISNULL(PCH.Comments,'') = ''
						THEN 1 ELSE 0 END 
					,[Flag] = CASE WHEN PCH.isArrivalHotel = 1 THEN 'A'
								WHEN PCH.isDepartureHotel = 1 THEN 'D' ELSE NULL END
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PL.LegID AND PCH.crewPassengerType = 'C'
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			LEFT JOIN (SELECT CrewCD, LegID, C.CrewID, NoofRooms, DutyTYPE
						   FROM PreflightCrewList PCL 
							    INNER JOIN Crew C ON PCL.CrewID = C.CrewID AND PCL.DutyTYPE  IN ('P','S')
					  ) PCLL ON PL.LegID = PCLL.LegID
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID --CREW ASSIGNED
															AND PHCP.CrewID = PCLL.CREWID
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	*/	
			ORDER BY LegNUM, [HTYPE], OrderNUM, CrewPaxCD
	END

--EXEC spGetReportPRETSLegHotelSummaryInformation 'SUPERVISOR_99', '10099124333','1'

		
	















GO


