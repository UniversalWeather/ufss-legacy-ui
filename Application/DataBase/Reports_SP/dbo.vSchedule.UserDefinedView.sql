IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vSchedule]'))
DROP VIEW [dbo].[vSchedule]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  CREATE VIEW  [dbo].[vSchedule] AS
(SELECT 
   /*PRE_FLT.
		,PRE_FLT.TripID
        ,PRE_FLT.PLArr_DT
		,PRE_FLT.Dpr_DT
		,PRE_FLT.PMRecordType
		,PRE_FLT.PLDutyType*/
		PRE_FLT.*
		,APRT.AirportID
		,APRT.AirportName
		,APRT.CustomerID 
		,APRT.CityName
		,APRT.StateName
		,APRT.CountryName
		,APRT.IcaoID
		,CASE WHEN PRE_FLT.PLArriveCAOID= HB.HomebaseAirportID THEN 'Y'
              ELSE 'N' END HB_FLG
    FROM
    (SELECT
			PM.TRIPID PMTripID
			,PM.CustomerID PMCustomerID
			,PM.FLEETID PMFleetID
			,PM.TripDescription  PMTripDescription
			,PM.RecordType PMRecordType
			,PM.TripStatus
			,PM.EstDepartureDT PMEstDepartureDT
			,PM.EstArrivalDT PMEstArrivalDT
			,PM.AircraftID PMAircraftID
			,PM.PassengerRequestorID PMPassengerRequestorID
			,PM.RequestorFirstName PMRequestorFirstName
			,PM.RequestorMiddleName PMRequestorMiddleName
			,PM.RequestorLastName PMRequestorLastName
			,PM.RequestorPhoneNUM PMRequestorPhoneNUM
			,PM.DepartmentDescription PMDepartmentDescription
			,PM.AuthorizationDescription PMAuthorizationDescription
			,PM.RequestDT PMRequestDT
			,PM.HOMEBASEID PMHOMEBASEID
			,PM.IsAirportAlert PMIsAirportAlert
			,PM.IsCrew PMIsCrew
			,PM.BeginningGMTDT PMBeginningGMTDT
			,PM.EndingGMTDT PMEndingGMTDT
			,PL.Dutytype PLDutyType
			, PL.TripID PLTripID
			, PL.LegID PLLegID
			,PL.LegNUM PLLegNUM
			,PL.CustomerID PLCusotemrID
			,PL.DepartICAOID PLDepartICAOID
			, PL.ArriveICAOID  PLArriveCAOID 
			,PL.AccountID PLAccountID
			,PL.AdditionalCrew PLAdditionalCrew
			, CONVERT(DATE,case when PM.RECORDTYPE='M' THEN PL.DepartureDTTMLocal ELSE PL.ArrivalDTTMLocal END) PLArr_DT
			, PL.DepartureDTTMLocal PLDpr_DT
			, PL.DepartureDTTMLocal PLDepartureDTTMLocal
			, PL.ArrivalDTTMLocal PLArrivalDTTMLocal
			,PL.DepartureGreenwichDTTM PLDepartureGreenwichDTTM
			,PL.ArrivalGreenwichDTTM PLArrivalGreenwichDTTM
			,PL.DepartureGreenwichDTTM
			,PL.AuthorizationID PLAuthorizationID
			,PL.AuthorizationName PLAuthorizationName
			,PL.ClientID PLClientID
			,PL.CustomerID PLCustomerID
			,PL.DepartmentID PLDepartmentID
			,PL.DepartmentName PLDepartmentName
			,PL.Distance PLDistance
			,PL.ElapseTM PLElapseTM
			,PL.DutyHours PLDutyHours
			,PL.IsDutyEnd PLIsDutyEnd
			,PL.IsScheduledServices PLIsScheduledServices
			,PL.FBOID	PLFBOID
			,PL.FlightCategoryID PLFlightCategoryID
			,PL.FlightPurpose PLFlightPurpose
			--,PL.LegNUM PLLegNUM 
			,PL.NextLocalDTTM PLNextLocalDTTM
			,PL.PassengerTotal PLPassengerTotal
			,PL.UWAID PLUWAID
			, Lst_Leg.Rnk
--			,Lst_Leg.PMRecordType 
--			,Lst_Leg.PLDutyType
       FROM PreflightMain PM
       JOIN (
              SELECT 
					PL.TripID PLTripID
					,PL.LegID PLLegID
					,PL.CustomerID PLCustomerID
					, RANK() OVER(PARTITION BY PM.CUSTOMERID, PM.fleetID,CONVERT(DATE,PL.ArrivalDTTMLocal) ORDER BY PL.ArrivalDTTMLocal DESC) Rnk
               FROM PreflightLeg PL JOIN PreflightMain  PM
					ON PL.TripID =PM.TripID
              --WHERE PL.ArrivalDTTMLocal BETWEEN @DATEFROM AND @DATETO
			  ) Lst_Leg
	   ON PM.TripID = Lst_Leg.PLTripID 
	   JOIN PreflightLeg PL    
       ON	PL.TripID = Lst_Leg.PLTripID
			AND PL.LegID = Lst_Leg.PLLegID
			AND Lst_Leg.Rnk = 1
		--WHERE PM.FleetID IN (10013152857,10013152856) AND PM.CustomerID = @CustomerID
			AND PM.TripStatus IN ('T','H')
			--AND PM.RecordType <>'M'
        --AND Lst_Leg.PLArrivalDTTMLocal BETWEEN @DATEFROM AND @DATETO
		) PRE_FLT 
    LEFT JOIN   
    (SELECT FLT.FleetID,FLT.TailNum, C.HomebaseAirportID,FLT.CustomerID
       FROM Fleet FLT 
       JOIN Company C 
        ON FLT.HomebaseID = C.HomebaseID
     --WHERE FLT.FleetId IN (10013152857,10013152856) 
    ) HB
    ON PRE_FLT.PMFleetID = HB.FleetID AND PRE_FLT.PLCusotemrID=HB.CustomerID
	JOIN Airport APRT ON PRE_FLT.PLArriveCAOID = APRT.AIRPORTID)


GO


