IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewExpenseExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewExpenseExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spGetReportPOSTCrewExpenseExportInformation] 
(
  @UserCustomerID AS BIGINT,
  @UserCD AS VARCHAR(30), --Mandatory
  @DATEFROM AS DATETIME, --Mandatory
  @DATETO AS DATETIME, --Mandatory
  @CrewCD AS NVARCHAR(1000) = '', 
  @TailNum AS NVARCHAR(1000) = '',
  @LogNum  AS NVARCHAR(1000) = '',
  @AccountNum AS NVARCHAR(1000) = ''	
)
AS	
----EXEC spGetReportPOSTCrewExpenseExportInformation 10098,'BRUCE_98','2012-01-01','2012-12-31',''
-- ===============================================================================
-- SPC Name: [spGetReportPOSTDelayDetailInformation]
-- Author: Askar
-- Create date: 01 August
-- Description: Get Crew Expense Information
-- Revision History
-- Date                 Name        Ver         Change
-- 05-12-2013		Askar		2.3			  
-- ================================================================================
			
BEGIN

Declare @SuppressActivityCrew BIT  
SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@UserCustomerID
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))


SET NOCOUNT ON;

		
SELECT DISTINCT 
       PE.SlipNUM AS slipnum,
       PM.LogNum AS lognum,
       ISNULL(PL.POLegID,0) AS legid,
       PL.LegNUM  AS [leg_num],
       AC.AccountNum AS acctnum,
       PE.AccountPeriod AS acctperiod,
       AH.IcaoID  AS homebase,
       F.TailNum AS [tail_nmbr],
       CONVERT(DATE,PE.PurchaseDT) AS PurchaseDT,
       icao_id=A.IcaoID,
       fbo_code=FB.FBOCD,
       fuel_loc=FL.FuelLocatorCD,
       pay_type=PT.PaymentTypeCD,
       pay_vendor=V.VendorCD,
       crewcode=C.CrewCD, 
       cat_code=FC.FlightCatagoryCD,
       dispatchno=PM.DispatchNum,
       invnum=PE.InvoiceNUM,
       expamt=PE.ExpenseAMT,
       fuelpurch=PE.FuelPurchase,
       PE.FuelQTY,
       PE.UnitPrice,
       fedtax=PE.FederalTAX,
       statetax=PE.SateTAX,
       salestax=PE.SaleTAX,
       notes = NULL,
       autocalc=PE.IsAutomaticCalculation,
       billing=PE.IsBilling,
       lastuser=PE.LastUpdUID,
       lastupdt=PE.LastUpdTS,
       PE.PostEDPR,
       PE.PostFuelPrice,
       PE.IsDeleted,
       acctdesc=AC.AccountDescription,
       fboname=FB.FBOVendor,
       C.LastName+', '+ C.FirstName+' '+ ISNULL(C.MiddleInitial,'') AS CrewName,
       paydesc=PT.PaymentTypeDescription,
       V.Name AS [pay_name],
       C.CrewID
  INTO #PassTemp     
  FROM PostflightMain PM 
  INNER JOIN PostflightLeg PL ON PL.POLogID = PM.POLogID AND PM.IsDeleted = 0
  INNER JOIN PostflightExpense PE ON PL.POLegID=PE.POLegID AND PL.IsDeleted = 0
  INNER JOIN Crew C ON PE.CrewID = C.CrewID
  INNER JOIN Fleet F ON PE.FleetID = F.FleetID AND F.IsDeleted=0 AND F.IsInActive=0
  INNER JOIN Account AC ON PE.AccountID = AC.AccountID
  INNER JOIN FBO FB ON PE.FBOID = FB.FBOID
  LEFT JOIN FuelLocator FL ON FL.FuelLocatorID = PE.FuelLocatorID
  LEFT JOIN Airport A ON PE.AirportID = A.AirportID
  LEFT OUTER JOIN Company CO ON PE.HomebaseID=CO.HomebaseID
  LEFT JOIN Airport AH ON CO.HomebaseAirportID = AH.AirportID
  LEFT JOIN PaymentType PT ON PE.PaymentTypeID = PT.PaymentTypeID
  LEFT JOIN VENDOR V ON PE.PaymentVendorID = V.VendorID
  LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = PE.FlightCategoryID
  WHERE CONVERT(DATE,PE.PurchaseDT)  BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)  
    AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD ='')
    AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM ='')
    AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (AC.AccountNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AccountNum, ',')) OR @AccountNum = '')
    AND C.IsStatus = 1 
    AND PE.CustomerID = @UserCustomerID --AND(V.VendorType = 'C' OR V.VendorType IS NULL)
    AND PE.IsDeleted = 0
    
    
    
IF @SuppressActivityCrew=0
BEGIN


INSERT INTO #PassTemp(crewcode,CrewName,CrewID,legid,IsDeleted,autocalc,billing)
SELECT C.CrewCD,C.LastName+', '+ C.FirstName+' '+ ISNULL(C.MiddleInitial,'') AS CrewName,C.CrewID,0,0,0,0
       FROM Crew C 
       WHERE C.CustomerID=@UserCustomerID
         AND C.IsDeleted=0
         AND C.IsStatus=1
		 AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
	     AND CrewID NOT IN(SELECT ISNULL(CrewID,0) FROM #PassTemp WHERE CrewCD  IS NOT NULL )
         
END
SELECT slipnum=ISNULL(slipnum,0)
	  ,lognum=ISNULL(lognum,0)	
	  ,legid=ISNULL(legid,0)	
	  ,leg_num=ISNULL(leg_num,0)	
	  ,acctnum	
	  ,acctperiod	
	  ,homebase	
	  ,tail_nmbr	
	  ,purchasedt	
	  ,icao_id	
	  ,fbo_code	
	  ,fuel_loc	
	  ,pay_type	
	  ,pay_vendor	
	  ,crewcode	
	  ,cat_code	
	  ,dispatchno	
	  ,invnum	
	  ,expamt=ISNULL(expamt,0)	
	  ,fuelpurch=ISNULL(fuelpurch,0)	
	  ,fuelqty=ISNULL(fuelqty,0)
	  ,unitprice=ISNULL(unitprice,0)	
	  ,fedtax=ISNULL(fedtax,0)	
	  ,statetax=ISNULL(statetax,0)
	  ,salestax=ISNULL(salestax,0)
	  ,notes	
	  ,autocalc=CASE WHEN autocalc=1 THEN 'TRUE' ELSE 'FALSE' END	
	  ,billing=CASE WHEN billing=1 THEN 'TRUE' ELSE 'FALSE' END		
	  ,lastuser	
	  ,lastupdt	
	  ,postedpr=ISNULL(postedpr,0)	
	  ,postfuelprice=ISNULL(postfuelprice,0)	
	  ,isdeleted	
	  ,acctdesc	
	  ,fboname	
	  ,crewname	
	  ,paydesc	
	  ,pay_name
 FROM #PassTemp ORDER BY crewcode, [tail_nmbr], LogNum,SlipNUM

IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
DROP TABLE #PassTemp

END





GO


