IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetOperationHistoryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetOperationHistoryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE Procedure [dbo].[spGetReportPOSTFleetOperationHistoryInformation]
		 @MONTH AS VARCHAR(10),  
         @YEAR  AS INT,  
         @UserCD AS VARCHAR(30),  
		 @TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values		
		 @FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFleetOperationHistoryInformation
-- Author:  D.Mullai
-- Create date: 06-08-2012
-- Description: Get Postflight Fleet Operation History for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
--DROP TABLE #temp1
--DROP TABLE #temp

DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1

DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
CREATE TABLE #temp (TailNum varchar(9),AircraftCD varchar(20),poleg bigint,POLegID bigint,BlockHours numeric(6,3),FlightHours numeric(6,3),TimeDisplayTenMin numeric (1,0),Distance numeric(5,0),FuelUsed numeric(7,0),
PassengerTotal numeric(4,0), paxtot numeric(5,0), Monthval int,months date) ;
CREATE TABLE #temp1(TailNum varchar(9),AircraftCD varchar(20),poleg bigint,POLegID bigint,BlockHours numeric(6,3),FlightHours numeric(6,3),TimeDisplayTenMin numeric (1,0),Distance numeric(5,0),FuelUsed numeric(7,0),
PassengerTotal numeric(4,0), paxtot numeric(5,0), Monthval int,months date) ;
DECLARE @EndDate date 
DECLARE @StartDate date 
DECLARE @StartDateval date 
DECLARE @ParameterDefinition AS NVARCHAR(MAX) 
DECLARE @date DATE 
DECLARE @A int 
declare @e int 
DECLARE @d DATETIME
if(@YEAR%4=0)
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-365, @EndDate) 

end
else 
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-364, @EndDate)
--print @StartDate  
end
------------------
--print @StartDate
--print @EndDate
-----------------
DECLARE @Monthval int 
DECLARE @intFlag INT 
SET @intFlag=1 
DECLARE @Monthdisplay TABLE 
( 
Monthval int ,Monthnameval varchar(20), 
dateFuel datetime,tenth int) 
 
WHILE (@intFlag <=12) 
BEGIN 
INSERT INTO @Monthdisplay (Monthval,Monthnameval,dateFuel,tenth)
VALUES(DATEpart(month,@StartDate) , DATENAME(month,@StartDateval) ,@StartDate,@TenToMin) 
SET @StartDate= DateAdd(month,1, @StartDate) 
SET @intFlag =@intFlag+1 
end
-------------
--select * from @Monthdisplay 
-----------
if(@YEAR%4=0)
begin
SET @StartDate=DateAdd(DD,-365, @EndDate) 
--print @StartDate
end
else 
begin
SET @StartDate=DateAdd(DD,-364, @EndDate) 
end
 
 
 --select * from @Monthdisplay 
 
IF @FleetGroupCD <> '' AND @TailNUM<>''
BEGIN

insert into #temp
SELECT DISTINCT TEMP1.* FROM 
(
select TailNum as TailNum,AircraftCD,count(POlegID) as poleg,POLegID,BlockHours,FlightHours,tenth as TimeDisplayTenMin,Distance,FuelUsed,
PassengerTotal,(PassengerTotal*Distance) as paxtot, m.Monthval,dateFuel as months
from @Monthdisplay m 
left outer join 
( 
SELECT distinct F.TailNum,f.AircraftCD,F.FleetID,pl.POlegID, ROUND(PL.BlockHours,1) BlockHours, ROUND(PL.FlightHours,1) FlightHours,@TenToMin as TimeDisplayTenMin,pl.Distance,pl.FuelUsed,
PassengerTotal, DATEpart(MONTH,PL.ScheduledTM) AS months
 
FROM PostflightMain pm
INNER JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0 
INNER JOIN (
SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
  AND F.IsInActive=0
AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
left outer join Company c on c.HomebaseID=pm.HomebaseID 
WHERE CONVERT(DATE,ScheduledTM) >= CONVERT(DATE,@StartDate) AND CONVERT(DATE,ScheduledTM) <=CONVERT(DATE,@EndDate) 
and pl.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
AND PM.IsDeleted = 0
--and (f.TailNum IN (SELECT DISTINCT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID=FGO.FleetID AND F.CustomerID=FGO.CustomerID
-- LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
-- WHERE F.IsDeleted = 0))
-- and (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
 
) a 
on m.Monthval = a.months 
--WHERE (A.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
group by TailNum,BlockHours,FlightHours,Distance,FuelUsed,PassengerTotal, m.Monthval,dateFuel,POLegID,TimeDisplayTenMin,tenth,AircraftCD
----order by dateFuel
UNION ALL
select TailNum as TailNum,AircraftCD,count(POlegID) as poleg,POLegID,BlockHours,FlightHours,tenth as TimeDisplayTenMin,Distance,FuelUsed,
PassengerTotal,(PassengerTotal*Distance) as paxtot, m.Monthval,dateFuel as months
from @Monthdisplay m 
left outer join 
( 
SELECT distinct F.TailNum,f.AircraftCD,F.FleetID,pl.POlegID, ROUND(PL.BlockHours,1) BlockHours, ROUND(PL.FlightHours,1) FlightHours,@TenToMin as TimeDisplayTenMin,pl.Distance,pl.FuelUsed,
PassengerTotal, DATEpart(MONTH,PL.ScheduledTM) AS months 
 
FROM PostflightMain pm
INNER JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
INNER JOIN (
SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
AND F.IsInActive=0
-- AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
left outer join Company c on c.HomebaseID=pm.HomebaseID 
WHERE CONVERT(DATE,ScheduledTM) >= CONVERT(DATE,@StartDate) AND CONVERT(DATE,ScheduledTM) <=CONVERT(DATE,@EndDate) 
and pl.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
--and (f.TailNum IN (SELECT DISTINCT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID=FGO.FleetID AND F.CustomerID=FGO.CustomerID
-- LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
-- WHERE F.IsDeleted = 0))
and (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
AND PM.IsDeleted = 0 
) a 
on m.Monthval = a.months 
--WHERE (A.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
group by TailNum,BlockHours,FlightHours,Distance,FuelUsed,PassengerTotal, m.Monthval,dateFuel,POLegID,TimeDisplayTenMin,tenth,AircraftCD
)TEMP1
--order by TEMP1.dateFuel
END
 
ELSE 

BEGIN
insert into #temp
select distinct TailNum as TailNum,AircraftCD,count(POlegID) as poleg,POLegID,BlockHours,FlightHours,tenth as TimeDisplayTenMin,Distance,FuelUsed,
PassengerTotal,(PassengerTotal*Distance) as paxtot, m.Monthval,dateFuel as months
from @Monthdisplay m 
left outer join 
( 
SELECT distinct F.TailNum,f.AircraftCD,F.FleetID,pl.POlegID, ROUND(PL.BlockHours,1) BlockHours,ROUND(PL.FlightHours,1) FlightHours,@TenToMin as TimeDisplayTenMin,pl.Distance,pl.FuelUsed,
PassengerTotal, DATEpart(MONTH,PL.ScheduledTM) AS months
 
FROM PostflightMain pm
INNER JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
INNER JOIN (
SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
AND F.IsInActive=0
AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
left outer join Company c on c.HomebaseID=pm.HomebaseID 
WHERE CONVERT(DATE,ScheduledTM) >= CONVERT(DATE,@StartDate) AND CONVERT(DATE,ScheduledTM) <=CONVERT(DATE,@EndDate) 
and pl.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
and (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
AND PM.IsDeleted = 0
 
) a 
on m.Monthval = a.months 
--WHERE (A.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
group by TailNum,BlockHours,FlightHours,Distance,FuelUsed,PassengerTotal, m.Monthval,dateFuel,POLegID,TimeDisplayTenMin,tenth,AircraftCD
order by dateFuel
END
 
DELETE FROM #temp WHERE Monthval IN(SELECT Monthval FROM #temp WHERE POLegID IS NOT NULL)
AND POLegID IS NULL
--SELECT * FROM #temp ORDER BY Monthval
IF @FleetGroupCD <> '' AND @TailNUM<>''
BEGIN
insert into #temp1
SELECT distinct TailNum,AircraftCD,NULL,NULL,NULL,NULL,@TenToMin as TimeDisplayTenMin,NULL,NULL,NULL,NULL,NULL,null
FROM (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
AND F.IsInActive=0
AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
) F 
left outer JOIN PostflightMain pm ON pm.FleetID = f.FleetID AND PM.IsDeleted = 0 
left outer JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
left outer JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
-- AND f.TailNum NOT IN (SELECT DISTINCT t.TailNum FROM #temp t)
and (f.TailNum IN (SELECT DISTINCT TailNum FROM Fleet F LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID=FGO.FleetID AND F.CustomerID=FGO.CustomerID
LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0 AND F.IsInActive=0))
--and (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
-- AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
UNION ALL
SELECT distinct TailNum,AircraftCD,NULL,NULL,NULL,NULL,@TenToMin as TimeDisplayTenMin,NULL,NULL,NULL,NULL,NULL,null
FROM (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
AND F.IsInActive=0
) F 
left outer JOIN PostflightMain pm ON pm.FleetID = f.FleetID AND PM.IsDeleted = 0
left outer JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
left outer JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
and (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')

END
ELSE
BEGIN
insert into #temp1
SELECT distinct TailNum,AircraftCD,NULL,NULL,NULL,NULL,@TenToMin as TimeDisplayTenMin,NULL,NULL,NULL,NULL,NULL,nulL
FROM (SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
FROM Fleet F 
left outer JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE F.IsDeleted = 0
AND F.IsInActive=0
AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
) F 
left outer JOIN PostflightMain pm ON pm.FleetID = f.FleetID AND PM.IsCompleted = 0
left outer JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
left outer JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
left outer JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
-- AND f.TailNum NOT IN (SELECT DISTINCT t.TailNum FROM #temp t)
and (f.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
-- AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
--order by dateFuel
end

delete from #temp1 where TailNum in(select distinct TailNum from #temp)
 
 IF  @IsZeroSuppressActivityAircftRpt=1 AND (SELECT COUNT(*) FROM #temp WHERE TailNum IS NOT NULL)=0
BEGIN
DELETE FROM #temp WHERE TailNum IS NULL
END

--SELECT distinct MU.* FROM(
IF @IsZeroSuppressActivityAircftRpt = 0
BEGIN
SELECT * FROM #temp1
UNION ALL 
SELECT * FROM #temp 
END
ELSE 
BEGIN
SELECT * FROM #temp
END
--)mu
--ORDER by mu.months


--spGetReportPOSTFleetOperationHistoryInformation 'jun',2011,'JWILLIAMS_11','','' 






GO


