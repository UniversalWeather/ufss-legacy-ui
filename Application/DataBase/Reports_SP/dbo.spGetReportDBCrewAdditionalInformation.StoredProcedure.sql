IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCrewAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCrewAdditionalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE  [dbo].[spGetReportDBCrewAdditionalInformation] 
(
        @UserCD AS VARCHAR(30) --Mandatory
        ,@UserCustomerID AS VARCHAR(30)
)
AS
 
 

-- ===============================================================================
-- SPC Name: [spGetReportDBCrewAdditionalInformation]
-- Author: Ravi R
-- Create date: 23 Feb 2013
-- Description: Get Crew Additional Information
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT 
  [Code] = CrewInfoCD
  , [Additional Information Description] = CrewInformationDescription
FROM CrewInformation
WHERE CustomerID=CONVERT(BIGINT, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND CustomerID=CONVERT(BIGINT,@UserCustomerID)
-- And IsInActive=0
 And IsDeleted=0

--EXEC spGetReportDBCrewAdditionalInformation 'SUPERVISOR_99',10099



GO


