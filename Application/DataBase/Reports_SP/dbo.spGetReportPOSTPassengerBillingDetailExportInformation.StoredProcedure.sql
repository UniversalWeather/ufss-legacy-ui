IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerBillingDetailExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerBillingDetailExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerBillingDetailExportInformation]
		(
		@UserCD AS VARCHAR(30), 
		@UserCustomerID AS VARCHAR(30),
		@DATEFROM AS DATETIME, 
		@DATETO AS DATETIME, 
		@PassengerRequestorCD AS NVARCHAR(1000) = '', 
		@DepartmentCD AS NVARCHAR(1000) = '',
		@AuthorizationCD AS NVARCHAR(1000) = '',
		@TailNum AS NVARCHAR(1000) = '', 
		@FleetGroupCD AS NVARCHAR(1000) = '',
		@UserHomebaseID AS VARCHAR(30),
		@IsHomebase AS BIT = 0,
		@PassengerGroupCD VARCHAR(500)='' 	
		)
AS		

--EXEC spGetReportPOSTPassengerBillingDetailExportInformation 'SUPERVISOR_99','10099','2013-09-01','2013-09-30','','','','','','',0,''

BEGIN
SET NOCOUNT ON

Declare @SuppressActivityPassenger BIT  
 	
SELECT @SuppressActivityPassenger=IsZeroSuppressActivityPassengerRpt FROM Company WHERE CustomerID=@UserCustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
										 
 Declare @AircraftBasis numeric(1,0);
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)


-----------------------------Passenger and Passenger Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
	AND P.IsActive = 1
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
	AND P.IsActive = 1
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
	AND P.IsActive = 1
END

-----------------------------Passenger and Passenger Group Filteration----------------------
	
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
			AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)		
	
----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END


-----------------------------TailNum and Fleet Group Filteration----------------------
			
SELECT DISTINCT
		PM.LogNum AS lognum
       ,PM.EstDepartureDT AS estdepdt
       ,F.TailNum AS tail_nmbr
       ,PM.DispatchNum AS dispatchno
       ,DL.DepartmentCD AS reqdept
       ,DAL.AuthorizationCD AS reqauth
       ,[depicao_id] = AD.IcaoID
       ,[arricao_id] = AA.IcaoID 
       ,[schedttm] = PL.ScheduledTM
       , ISNULL(PL.POLegID,0) AS legid
       ,P.PassengerRequestorCD AS paxcode
       , ISNULL(LastName,'')+CASE WHEN FirstName<>'' OR FirstName IS NOT NULL THEN  ISNULL(', '+FirstName,'') ELSE '' END AS paxname
       ,PP.Billing AS stdbilling
       ,DP.DepartmentCD AS [dept_code]
       ,DPA.AuthorizationCD AS [auth_code]
       ,PL.Distance AS distance
       ,PL.FlightHours AS [flt_hrs]
       ,PL.BlockHours AS [blk_hrs]
       ,CR.ChargeRate AS [chrg_rate]
       ,CR.ChargeUnit AS [chrg_unit]
       ,F.MaximumPassenger AS [max_pax]
       ,AC.AircraftCD AS [type_code]
       , PL.PassengerTotal AS [pax_total] 
        ,F.AircraftCD 
	   ,[Total_Charges] = ((CASE WHEN CR.ChargeUnit = 'N' THEN  ((PL.Distance*CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN CR.ChargeUnit = 'S' THEN (((PL.Distance*CR.ChargeRate)*1.15078)+ISNULL((PFE11.SUMExpenseAmt),0)) 
				WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis = 1) THEN ((PL.BlockHours *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				WHEN (CR.ChargeUnit = 'H'AND @AircraftBasis= 2) THEN ((PL.FlightHours *CR.ChargeRate)+ISNULL((PFE11.SUMExpenseAmt),0))
				ELSE ISNULL((PFE11.SUMExpenseAmt),0)
				END)/PL.PassengerTotal)
       ,[TenToMin] = @TenToMin
       ,PL.LegNUM
       ,[EXP_ACCOUNT] = EX.AccountDescription
       ,[EXP_AMOUNT] = EX.ExpenseAMT
   INTO #PassTemp  
FROM PostflightMain PM JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
                       LEFT JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       LEFT JOIN Department DP ON DP.DepartmentID = P.DepartmentID
                       LEFT JOIN DepartmentAuthorization DPA ON DPA.AuthorizationID = P.AuthorizationID
                       LEFT JOIN Department DL ON DL.DepartmentID = PL.DepartmentID
                       LEFT JOIN DepartmentAuthorization DAL ON DAL.AuthorizationID = PL.AuthorizationID
                       INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
                       JOIN Fleet F ON PM.FleetID =F.FleetID AND PM.CustomerID = F.CustomerID  
                       JOIN @TempFleetID TF ON TF.FleetID=F.FleetID
                       LEFT JOIN (SELECT DISTINCT PP4.POLogID,PP4.POLegID,PP4.IsBilling , PP4.CustomerID,SUMExpenseAmt = (
                                  SELECT ISNULL(SUM(PP2.ExpenseAMT),0) FROM PostflightExpense PP2 
																	    WHERE PP2.POLegID = PP4.POLegID
	                                                                   AND IsBilling = 1)
                                                    FROM PostflightExpense PP4 WHERE IsBilling =1
                                  )PFE11  ON PL.POLegID =PFE11.POLegID  AND PL.POLogID = PFE11.POLogID										
                     LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID AND AC.CustomerID = F.CustomerID
                     LEFT JOIN Department D ON P.DepartmentID = D.DepartmentID AND P.CustomerID = D.CustomerID 
					   LEFT JOIN DepartmentAuthorization DA ON DA.AuthorizationID = P.AuthorizationID 
					   LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID
                       LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
                       LEFT OUTER JOIN (SELECT POLegID, ChargeUnit, ChargeRate FROM PostflightMain PM 
                                             INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID
			                                 INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
                                          ) CR ON PL.POLegID = CR.POLegID  
					   LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
																	  INNER JOIN Account A ON PE.AccountID = A.AccountID
																  WHERE PE.IsDeleted = 0
																  AND PE.POLegID=PE1.POLegID
																  FOR XML PATH('')
													)
							,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(10),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
																	  INNER JOIN Account A ON PE.AccountID = A.AccountID
																  WHERE PE.IsDeleted = 0
																  AND PE.POLegID=PE1.POLegID
																  FOR XML PATH('')
													)
										FROM PostflightExpense PE1
								
										)EX ON EX.POLegID = PL.POLegID  
WHERE CONVERT(DATE,PL.ScheduledTM)  BETWEEN CONVERT(DATE,@DateFrom) AND CONVERT(DATE,@DateTo)
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
AND (PM.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID) AND P.PassengerRequestorCD IS NOT NULL
AND P.IsActive = 1
AND PM.IsDeleted = 0




IF @SuppressActivityPassenger=0
BEGIN
INSERT INTO #PassTemp(paxcode
,paxname,TenToMin,legid)
SELECT P.PassengerRequestorCD,ISNULL(LastName,'')+CASE WHEN FirstName<>'' OR FirstName IS NOT NULL THEN  ISNULL(', '+FirstName,'') ELSE '' END ,@TenToMin
      ,0 FROM Passenger P 
            INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
       WHERE P.CustomerID=CONVERT(BIGINT,@UserCustomerID)
		 AND PassengerRequestorCD NOT IN(SELECT paxcode FROM #PassTemp WHERE paxcode  IS NOT NULL)
         AND P.IsActive = 1
END


SELECT lognum
      ,estdepdt
      ,tail_nmbr
      ,dispatchno
      ,reqdept
      ,reqauth
      ,depicao_id
      ,arricao_id
      ,schedttm
      ,ISNULL(legid,0) AS legid
      ,paxcode
      ,paxname
      ,stdbilling
      ,dept_code
      ,auth_code
      ,distance
      ,flt_hrs
      ,blk_hrs
      ,chrg_rate
      ,totline = ''
      ,chrg_unit
      ,max_pax
      ,type_code
      ,pax_total
      ,[Total_Charges] 		
      ,[TenToMin]
      ,LegNUM
      ,EXP_ACCOUNT
      ,EXP_AMOUNT
        FROM #PassTemp
       ORDER BY schedttm,lognum,LegNUM 

IF OBJECT_ID('tempdb..#PassTemp') IS NOT NULL
DROP TABLE #PassTemp

END







GO


