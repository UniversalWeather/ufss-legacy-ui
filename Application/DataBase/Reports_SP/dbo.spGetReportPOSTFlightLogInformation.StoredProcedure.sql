IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightLogInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFlightLogInformation] 
  (  
  @UserCD AS VARCHAR(30),  
  @LogID AS VARCHAR(30)  
  ) 
AS
BEGIN
--	Stored Procedure name 'spGetReportPOSTFlightLogInformation' 

--DECLARE @UserCD AS VARCHAR(30);  
--DECLARE @LogID AS VARCHAR(30);
DECLARE @LogFixed INT;
DECLARE @HomebaseID BIGINT;
DECLARE @COUNT AS INT;
DECLARE @LegCnt AS INT;
Declare @TenToMin INT;    
DECLARE @DateFormate INT; 

--SET @UserCD='JWILLIAMS_10'
--SET @LogID='100103'
Set @TenToMin=(Select Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  
And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) 

DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SET @HomebaseID=dbo.GetHomeBaseByUserCD(LTRIM(@UserCD));

SELECT @DateFormate=CONVERT(INT,DayMonthFormat) 
				FROM Company 
				WHERE HomebaseID=@HomebaseID AND CustomerID=@CustomerID

DECLARE @IsFixedRotary VARCHAR(5)
SELECT @IsFixedRotary=ar.IsFixedRotary from 
PostflightMain pm INNER JOIN Fleet fl on pm.FleetID=fl.FleetID INNER JOIN Aircraft ar on ar.AircraftID=fl.AircraftID
where pm.POLogID=@LogID

IF(@IsFixedRotary='F')
BEGIN
SELECT @LogFixed=LogFixed FROM Company WHERE CustomerID=@CustomerID 
										 AND LogFixed IS NOT NULL
										 AND HomebaseID=@HomebaseID
END
ELSE IF(@IsFixedRotary='R')
BEGIN
SELECT @LogFixed=LogRotary FROM Company WHERE CustomerID=@CustomerID 
										 AND LogRotary IS NOT NULL
										 AND HomebaseID=@HomebaseID
END
IF(LEN(ISNULL(@LogFixed,''))=0)
BEGIN
SET @LogFixed=1
END
										 
										 
DECLARE @tblTripInfo AS TABLE 
(  
POLOGID BIGINT,
POLEGID BIGINT,
CustomerID BIGINT 
)  

INSERT INTO @tblTripInfo (POLOGID, POLEGID,CustomerID)
SELECT PM.POLogID,POLegID,@CustomerID FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
WHERE PM.CustomerID=@CustomerID
AND CONVERT(VARCHAR(30),PM.POLogID)=@LogID
AND PM.IsDeleted = 0

SET @COUNT=(SELECT COUNT(*) FROM @tblTripInfo)

DECLARE @FlightLog AS TABLE(
	POLOGID BIGINT,
	POLEGID BIGINT,
	SequenceOrder BIGINT,
	RowName NVARCHAR(MAX),
	DisplayRowName NVARCHAR(MAX),
	ColumnName BIGINT,
	ColumnValue NVARCHAR(MAX)
)

INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
SELECT TI.POLOGID,TI.POLEGID,TS.SequenceOrder,
TS.OriginalDescription RowName,
CASE 
WHEN (LEN(ISNULL(TS.CustomDescription,''))!=0) 
THEN TS.CustomDescription 
ELSE TS.OriginalDescription 
END  DisplayRowName,
CONVERT(BIGINT,PL.LegNUM) ColumnName
FROM @tblTripInfo TI INNER JOIN TSFlightLog TS ON TI.CustomerID=TS.CustomerID AND TS.IsPrint = 1 AND Category = 'FLIGHT LOG' AND TS. HomebaseID=@HomebaseID AND TS.IsDeleted=0
INNER JOIN PostflightLeg PL ON TI.POLEGID=PL.POLegID

DECLARE @MaxColumn BIGINT
Select @MaxColumn=max(ColumnName) from @FlightLog
--Print @MaxColumn
WHILE(@MaxColumn <= 10)
BEGIN
SET @MaxColumn += 1
INSERT INTO @FlightLog(POLOGID,RowName,DisplayRowName,ColumnName)
SELECT TI.POLOGID,TS.OriginalDescription RowName,
CASE 
WHEN (LEN(ISNULL(TS.CustomDescription,''))!=0) 
THEN TS.CustomDescription 
ELSE TS.OriginalDescription 
END  DisplayRowName,
@MaxColumn ColumnName
FROM @tblTripInfo TI INNER JOIN TSFlightLog TS ON TI.CustomerID=TS.CustomerID AND TS.IsPrint = 1 
AND Category = 'FLIGHT LOG' AND TS. HomebaseID=@HomebaseID AND TS.IsDeleted=0
INNER JOIN PostflightLeg PL ON TI.POLEGID=PL.POLegID
END

IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='LANDING (D/N)')
BEGIN
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Day Landing' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='LANDING (D/N)'
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Night Landing' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='LANDING (D/N)'
	DELETE FROM @FlightLog WHERE RowName='LANDING (D/N)'
END

IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='TAKEOFF (D/N)')
BEGIN
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Day Takeoff' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='TAKEOFF (D/N)'
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Night Takeoff' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='TAKEOFF (D/N)'
	DELETE FROM @FlightLog WHERE RowName='TAKEOFF (D/N)'
END

IF EXISTS(SELECT * FROM @FlightLog WHERE RowName='APPROACH (P/N)')
BEGIN
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Prec App' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='APPROACH (P/N)'
	INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName)
	SELECT POLOGID,POLEGID,SequenceOrder,'Non-Prec App' RowName,DisplayRowName,ColumnName
	FROM @FlightLog 
	WHERE RowName='APPROACH (P/N)'
	DELETE FROM @FlightLog WHERE RowName='APPROACH (P/N)'
END

DECLARE @tblLeg AS TABLE
(
POLogID BIGINT,
POLegID BigInt,   
LogNumber BigInt,   
DepartICAO Varchar(50),  
ArriveICAO Varchar(50),  
SchedLocalDate varchar(15),   
SchedLocalDeparture varchar(15),   
DepartmentCode Varchar(10),  
AuthorizationCode Varchar(10),  
FarPart Varchar(5),   
Mileage  Numeric(4,0),                              
BlockHrs varchar(20),  
FlightHrs varchar(20),
BlockOut Varchar(5), 
TimeOff Varchar(5),
BlockIN Varchar(5),  
TimeOn Varchar(5),
DeleyType Varchar(10),  
DelayTime Varchar(10),   
Passengers Numeric(4,0),   
FuelOut Numeric(6,0),  
FuelIn Numeric(6,0),  
FuelBurn Numeric(8,0),   
PicSic Varchar(40),    
DayTakeoff Varchar(40),  
NightTakeoff Varchar(40),  
DayLanding Varchar(40),  
NightLanding Varchar(40),  
PrecApp Varchar(40),  
NonPrecApp Varchar(40),  
InstTime Varchar(40),  
NightTime Varchar(40),  
AddlCrew Varchar(40),  
DutyOn Varchar(10),  
DutyOff Varchar(10),  
TotalDuty Varchar(40),
DutyType CHAR(1),
LegNUM BIGINT,
PASSENGERNAME VARCHAR(100)
)                             
   
INSERT INTO @tblLeg 
SELECT DISTINCT 
PL.POLogID ,
PL.POLegID,   
PL.LegNUM,   
ADeprt.IcaoID,  
AArr.ICAOID,  
Convert(Varchar(10),Convert(Varchar(3),DATEPART(mm, PL.ScheduledTM)) + '/' + Convert(Varchar(3),DATEPART(dd, PL.ScheduledTM)))
,Left(Convert(Varchar(10),PL.ScheduledTM,108),5),       
DT.DepartmentCD,  
DA.AuthorizationCD,  
PL.FedAviationRegNum,  
PL.Distance,    
Case @TenToMin When 2 Then Convert(Varchar,Dbo.GetHourMinutes(ROUND(PL.BlockHours,1)))  Else  Convert(Varchar,Convert(decimal(7,1),ROUND(PL.BlockHours,1))) End As   BLOCKHRS ,      
Case @TenToMin When 2 Then Convert(Varchar,Dbo.GetHourMinutes(ROUND(PL.FlightHours,1))) Else  Convert(Varchar,Convert(decimal(7,1),ROUND(PL.FlightHours,1))) End As  FLIGHTHRS ,      
PL.BlockOut, 
PL.TimeOff,
PL.BlockIN,  
PL.TimeOn,
IsNull(DelayType.DelayTypeCD,''),    
Case @TenToMin When 2 Then  Convert(Varchar,Dbo.GetHourMinutes(PL.DelayTM))  Else  Convert(Varchar,Convert(decimal(7,1),PL.DelayTM)) End As DelayTime,  
PL.PassengerTotal,  
PL.FuelOut,    
PL.FuelIn,    
PL.FuelUsed,  
C.CrewCD,Convert(Varchar,PC.TakeOffDay)
,Convert(Varchar,PC.TakeOffNight)
,Convert(Varchar,PC.LandingDay)
,Convert(Varchar,PC.LandingNight)
,Convert(Varchar,PC.ApproachPrecision)
,Convert(Varchar,PC.ApproachNonPrecision)
,Case @TenToMin When 2 Then  Convert(Varchar,Dbo.GetHourMinutes(FLOOR(PC.Instrument*10)*0.1))  Else  Convert(Varchar,FLOOR(PC.Instrument*10)*0.1) End
,Case @TenToMin When 2 Then  Convert(Varchar,Dbo.GetHourMinutes(FLOOR(PC.Night*10)*0.1))  Else  Convert(Varchar,FLOOR(PC.Night*10)*0.1) End
,''
,CONVERT(CHAR(5),PC.CrewDutyStartTM, 108)
,CONVERT(CHAR(5),PC.CrewDutyEndTM, 108)
,Case @TenToMin When 2 Then  Convert(Varchar,Dbo.GetHourMinutes(FLOOR(ROUND(PC.DutyHours,1)*10)*0.1))  Else  Convert(Varchar,FLOOR(ROUND(PC.DutyHours,1)*10)*0.1) End
,PC.DutyTYPE
,PL.LegNUM
,''
From PostflightMain PM 
INNER JOIN PostFlightLeg PL ON PL.POLogID = PM.POLogID AND PL.IsDeleted = 0 				   
LEFT OUTER JOIN PostFlightCrew PC ON PL.POLegID = PC.POLegID  
LEFT OUTER JOIN Department DT ON PL.DepartmentID = DT.DepartmentID 
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID AND DA.DepartmentID = DT.DepartmentID 
LEFT OUTER JOIN Airport AS AArr ON AArr.AirportID = PL.ArriveICAOID 
INNER JOIN Airport AS ADeprt ON ADeprt.AirportID = PL.DepartICAOID 
LEFT OUTER JOIN DelayType ON PL.DelayTypeID=DelayType.DelayTypeID
LEFT OUTER JOIN Crew C ON C.CrewID = PC.CrewID   
Where  PM.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)  AND CONVERT(VARCHAR(30),PL.POLogID)=@LogID
AND PM.IsDeleted = 0

UPDATE FL 
SET FL.ColumnValue = (
SELECT TOP 1 CASE(FL.RowName)
			WHEN 'MILEAGE' THEN CONVERT(NVARCHAR(MAX),TL.Mileage)
			WHEN 'BLOCK HRS' THEN CONVERT(NVARCHAR(MAX),TL.BlockHrs)
			WHEN 'FLIGHT HRS' THEN CONVERT(NVARCHAR(MAX),TL.FLIGHTHRS)
			WHEN 'DELAY TYPE' THEN CONVERT(NVARCHAR(MAX),TL.DeleyType)
			WHEN 'DELAY TIME' THEN CONVERT(NVARCHAR(MAX),TL.DelayTime)
			WHEN 'PASSENGERS' THEN CONVERT(NVARCHAR(MAX),TL.Passengers)
			WHEN 'FUEL OUT' THEN CONVERT(NVARCHAR(MAX),TL.FuelOut)
			WHEN 'FUEL IN' THEN CONVERT(NVARCHAR(MAX),TL.FuelIn)
			WHEN 'FUEL BURN' THEN CONVERT(NVARCHAR(MAX),TL.FuelBurn)
			WHEN 'UTC OUT' THEN CONVERT(NVARCHAR(MAX),TL.BlockOut)
			WHEN 'UTC IN' THEN CONVERT(NVARCHAR(MAX),TL.BlockIN)
			WHEN 'UTC OFF' THEN CONVERT(NVARCHAR(MAX),TL.TimeOff)
			WHEN 'UTC ON' THEN CONVERT(NVARCHAR(MAX),TL.TimeOn)
			WHEN 'HOME OUT' THEN CONVERT(NVARCHAR(MAX),TL.BlockOut)
			WHEN 'HOME IN' THEN CONVERT(NVARCHAR(MAX),TL.BlockIN)
			WHEN 'HOME OFF' THEN CONVERT(NVARCHAR(MAX),TL.TimeOff)
			WHEN 'HOME ON' THEN CONVERT(NVARCHAR(MAX),TL.TimeOn)
			WHEN 'LOCAL OUT' THEN CONVERT(NVARCHAR(MAX),TL.BlockOut)
			WHEN 'LOCAL IN' THEN CONVERT(NVARCHAR(MAX),TL.BlockIN)
			WHEN 'LOCAL OFF' THEN CONVERT(NVARCHAR(MAX),TL.TimeOff)
			WHEN 'LOCAL ON' THEN CONVERT(NVARCHAR(MAX),TL.TimeOn)
			WHEN 'PASSENGER COUNT' THEN CONVERT(NVARCHAR(MAX),TL.Passengers)
			END
FROM @tblLeg
WHERE POLegID=FL.POLEGID
)
FROM @FlightLog AS FL
INNER JOIN @tblLeg AS TL 
       ON FL.POLEGID = TL.PoLegID 
WHERE FL.RowName in (SELECT Distinct RowName FROM @FlightLog)
  AND FL.POLEGID = TL.PoLegID;

UPDATE FL 
SET FL.ColumnValue = (SELECT TOP 1 CASE(FL.RowName)
			WHEN 'DUTY OFF' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.DutyOff,'')+ISNULL(':'+T1.DutyOff,''))
			WHEN 'DUTY ON' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.DutyOn,'')+ISNULL(':'+T1.DutyOn,''))
			WHEN 'INST TIME' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.InstTime,'')+ISNULL(':'+T1.InstTime,''))
			WHEN 'NIGHT TIME' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.NightTime,'')+ISNULL(':'+T1.NightTime,''))
			WHEN 'TOTAL DUTY' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.TotalDuty,'')+ISNULL(':'+T1.TotalDuty,''))
			WHEN 'Day Landing' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.DayLanding,'')+ISNULL(':'+T1.DayLanding,''))
			WHEN 'Night Landing' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.NightLanding,'')+ISNULL(':'+T1.NightLanding,''))
			WHEN 'Day Takeoff' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.DayTakeoff,'')+ISNULL(':'+T1.DayTakeoff,''))
			WHEN 'Night Takeoff' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.NightTakeoff,'')+ISNULL(':'+T1.NightTakeoff,''))
			WHEN 'Prec App' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.PrecApp,'')+ISNULL(':'+T1.PrecApp,''))
			WHEN 'Non-Prec App' THEN CONVERT(NVARCHAR(MAX),ISNULL(T2.NonPrecApp,'')+ISNULL(':'+T1.NonPrecApp,''))
			END
FROM @tblLeg T LEFT OUTER JOIN (SELECT LegNUM,POLOGID,DutyOff,DutyOn,InstTime,NightTime,TotalDuty,DayLanding,NightLanding,DayTakeoff,NightTakeoff,PrecApp,NonPrecApp FROM @tblLeg WHERE POLegID=FL.POLEGID AND DutyType='P') T2 ON T.POLOGID=T2.POLOGID
LEFT OUTER JOIN (SELECT LegNUM,POLOGID,DutyOff,DutyOn,InstTime,NightTime,TotalDuty,DayLanding,NightLanding,DayTakeoff,NightTakeoff,PrecApp,NonPrecApp FROM @tblLeg WHERE POLegID=FL.POLEGID AND DutyType='S') T1 ON T.POLOGID=T1.POLOGID
WHERE POLegID=FL.POLEGID
)
FROM @FlightLog AS FL
INNER JOIN @tblLeg AS TL ON FL.POLEGID = TL.PoLegID 
WHERE FL.RowName IN ('DUTY OFF','DUTY ON','INST TIME','NIGHT TIME','TOTAL DUTY','Day Landing','Night Landing','Day Takeoff','Night Takeoff','Prec App','Non-Prec App') AND FL.POLEGID = TL.PoLegID;

INSERT INTO @FlightLog(POLOGID,POLEGID,SequenceOrder,RowName,DisplayRowName,ColumnName,ColumnValue)
SELECT DISTINCT PL.POLogID,PL.POLegID,-1 SequenceOrder ,
			CASE WHEN @LogFixed =1 THEN 'Leg#Depart ICAO#Arrive ICAO#Sched Local Date#Sched Local Departure#Department Code#Authorization Code#FAR Part'
			ELSE 'Leg#Depart ICAO#Arrive ICAO#Sched GMT Date#Sched GMT Departure#Department Code#Authorization Code#FAR Part' END AS  RowName,
			CASE WHEN @LogFixed =1 THEN 'Leg#Depart ICAO#Arrive ICAO#Sched Local Date#Sched Local Departure#Department Code#Authorization Code#FAR Part'
			ELSE 'Leg#Depart ICAO#Arrive ICAO#Sched GMT Date#Sched GMT Departure#Department Code#Authorization Code#FAR Part' END AS  DisplayRowName,
			CONVERT(BIGINT,PL.LegNUM) ColumnName
,CONVERT(VARCHAR(10),PL.LegNUM)+'#'+AD.ICAOID+'#'+AA.ICAOID
+'#'+ 
CASE WHEN @DateFormate=1 THEN ISNULL(Convert(Varchar(3),DATEPART(dd, PL.ScheduledTM)),'')+ '/' +ISNULL(Convert(Varchar(3),DATEPART(mm, PL.ScheduledTM)),'')
ELSE ISNULL(Convert(Varchar(3),DATEPART(mm, PL.ScheduledTM)),'') + '/' + ISNULL(Convert(Varchar(3),DATEPART(dd, PL.ScheduledTM)),'')
END
+'#'+ISNULL(Left(Convert(Varchar(10),PL.ScheduledTM,108),5),'')+'#'+ISNULL(D.DepartmentCD,'')+'#'+ISNULL(DA.AuthorizationCD,'')+'#'+ISNULL(PL.FedAviationRegNum,'') AS ColumnValue
FROM @tblTripInfo M 
INNER JOIN PostflightLeg PL ON M.POLegID = PL.POLegID AND PL.IsDeleted = 0 
INNER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID   
INNER JOIN AIRPORT AA ON PL.ArriveICAOID=AA.AirportID
LEFT OUTER JOIN Department D ON D.DepartmentID = PL.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = PL.AuthorizationID AND D.DepartmentID = DA.DepartmentID    
WHERE PL.POLegID=M.POLEGID

DECLARE @TEMPCREW TABLE(RowID INT,
							CrewID BIGINT,
							POLOGID BIGINT,
							LegId BIGINT,
							CREWCD VARCHAR(5),
							LegNUM BIGINT,
							POLEGID BIGINT,
							DutyType CHAR(1)) 

		INSERT INTO  @TEMPCREW 
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.POLEGID  ORDER BY CrewCD ),CW.CrewID,PM.POLogID,PL.POLegID,CREWCD,PL.LegNUM,PL.POLegID,PCL.DutyTYPE 
		FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID  AND PL.IsDeleted = 0
														 INNER JOIN PostflightCrew PCL ON PL.POLegID=PCL.POLegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('P') 
														 AND PM.POLogID IN(SELECT DISTINCT POLOGID FROM @tblTripInfo)
														 AND PM.IsDeleted = 0
														 ORDER BY CrewCD DESC 
		INSERT INTO  @TEMPCREW 
		SELECT  ROW_NUMBER() OVER ( PARTITION BY PL.POLEGID  ORDER BY CrewCD ),CW.CrewID,PM.POLogID,PL.POLegID,CREWCD,PL.LegNUM,PL.POLegID,PCL.DutyTYPE 
		FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0 
														 INNER JOIN PostflightCrew PCL ON PL.POLegID=PCL.POLegID 
														 INNER JOIN CREW CW ON CW.CrewID=PCL.CrewID 
														 WHERE PCL.DutyTYPE IN('S') 
														 AND PM.POLogID IN(SELECT DISTINCT POLOGID FROM @tblTripInfo)
														 AND PM.IsDeleted = 0
														 ORDER BY CrewCD DESC 
														 
DECLARE @Pic_AddCrew TABLE(
	POLOGID BIGINT,
	POLEGID BIGINT,
	ColumnName NVARCHAR(MAX),
	ColumnValue NVARCHAR(MAX))

	
INSERT @Pic_AddCrew(POLOGID,POLEGID,ColumnName,ColumnValue)
SELECT DISTINCT FL.POLOGID,FL.POLEGID,'PIC/SIC' ColumnName,STUFF(
	(
		SELECT ':'+CREWCD
		FROM  @TEMPCREW P
		WHERE P.LegId=FL.POLEGID
		FOR XML PATH('')
	),1,1,'') AS ColumnValue 
FROM @FlightLog FL INNER JOIN  @TEMPCREW PS ON FL.POLEGID=PS.POLEGID AND FL.POLOGID=PS.POLOGID

INSERT @Pic_AddCrew(POLOGID,POLEGID,ColumnName,ColumnValue)
SELECT DISTINCT PM.POLOGID,PL.POLEGID,'ADDL CREW' ColumnName,SUBSTRING(CREW.CrewList,1,LEN(Crew.CrewList)-1) ColumnValue   
FROM (SELECT DISTINCT POLOGID, POLEGID FROM @tblTripInfo)M  
INNER JOIN PostflightMain PM ON M.POLOGID = PM.POLOGID AND PM.IsDeleted = 0 
INNER JOIN PostflightLeg PL ON M.POLEGID = PL.POLegID AND PL.IsDeleted = 0 
INNER JOIN PostflightCrew PCL ON PL.POLegID = PCL.POLegID  
INNER JOIN (            
SELECT DISTINCT PC2.POLegID, CrewList = (            
SELECT C.CrewCD + '/'            
FROM PostflightCrew PC1            
INNER JOIN Crew C ON PC1.CrewID = C.CrewID            
WHERE PC1.POLegID = PC2.POLEGID    
AND PC1.DutyTYPE NOT IN('P','S')          
FOR XML PATH('')            
)                     
FROM PostflightCrew PC2             
) Crew ON PL.POLegID = Crew.POLegID INNER JOIN @FlightLog FL ON PL.POLegID=FL.POLEGID
WHERE PM.CustomerID = @CustomerID

INSERT @Pic_AddCrew(POLOGID,POLEGID,ColumnName,ColumnValue)
SELECT DISTINCT PM.POLOGID,PL.POLEGID,'PASSENGER NAME' ColumnName,SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) AS ColumnValue
					FROM  @tblTripInfo TI INNER JOIN @tblLeg T ON TI.POLEGID=T.POLegID  
					INNER JOIN PostflightMain PM ON T.POLOGID = PM.POLOGID AND PM.ISDELETED = 0  
					INNER JOIN PostflightLeg PL ON T.POLEGID = PL.POLegID AND PM.ISDELETED = 0
					INNER JOIN PostflightPassenger POP ON POP.POLegID = PL.POLegID
					LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
					LEFT OUTER JOIN ( SELECT DISTINCT PP2.POLegID,PaxNames = (
                      SELECT ISNULL(P1.LastName,' ')+','  
                               FROM PostflightPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.POLegID = PP2.POLegID
                               ORDER BY P1.MiddleInitial
                             FOR XML PATH(''))
                             FROM PostflightPassenger PP2
				   ) PAX ON PL.POLegID = PAX.POLegID    
					LEFT OUTER JOIN (SELECT T2.LegNUM,T2.POLOGID,T2.PASSENGERNAME FROM @tblLeg T2 INNER JOIN @tblTripInfo TI2 ON T2.POLegID=TI2.POLEGID WHERE DutyType='P') T2 ON T.POLOGID=T2.POLOGID 
					LEFT OUTER JOIN (SELECT T3.LegNUM,T3.POLOGID,T3.PASSENGERNAME FROM @tblLeg T3 INNER JOIN @tblTripInfo TI2 ON T3.POLegID=TI2.POLEGID WHERE DutyType='S') T1 ON T.POLOGID=T1.POLOGID   
					WHERE PM.CustomerID = @CustomerID
					
UPDATE @FlightLog
SET ColumnValue=PC.ColumnValue
FROM @FlightLog FL INNER JOIN @Pic_AddCrew PC ON FL.POLOGID=PC.POLOGID AND FL.POLEGID=PC.POLEGID AND FL.RowName=PC.ColumnName 


IF @LogFixed=1
	BEGIN
		IF @IsFixedRotary='R'  
			BEGIN
				SELECT * FROM @FlightLog WHERE RowName NOT IN('LANDING (D/N)','TAKEOFF (D/N)','APPROACH (P/N)','HOME OUT','HOME OFF','HOME ON','HOME IN','UTC OUT','UTC OFF','UTC ON','UTC IN')ORDER BY POLEGID,SequenceOrder
			END
		ELSE 
			BEGIN
				SELECT * FROM @FlightLog WHERE RowName NOT IN('LANDING (D/N)','TAKEOFF (D/N)','APPROACH (P/N)','LOCAL OUT','LOCAL OFF','LOCAL ON','LOCAL IN','UTC OUT','UTC OFF','UTC ON','UTC IN')ORDER BY POLEGID,SequenceOrder
			END
	END
ELSE
	BEGIN
		SELECT * FROM @FlightLog WHERE RowName NOT IN('LANDING (D/N)','TAKEOFF (D/N)','APPROACH (P/N)','HOME OUT','HOME OFF','HOME ON','HOME IN','LOCAL OUT','LOCAL OFF','LOCAL ON','LOCAL IN')ORDER BY POLEGID,SequenceOrder
	END
END



GO