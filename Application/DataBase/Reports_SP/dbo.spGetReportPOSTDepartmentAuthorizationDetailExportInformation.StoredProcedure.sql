IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationDetailExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationDetailExportInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationDetailExportInformation
-- Author:  A.Akhila
-- Create date: 13 Aug 2012
-- Description: Get Department/Authorization Detail for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =0

  -----------------------------TailNum and Fleet Group Filteration----------------------  
  DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
  DECLARE @TempFleetID TABLE   
  (   
  ID INT NOT NULL IDENTITY (1,1),   
  FleetID BIGINT,  
  TailNum VARCHAR(10)  
  )  
     
  IF @TailNUM <> ''  
  BEGIN  
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum   
  FROM Fleet F  
  WHERE F.CustomerID = @CUSTOMER  
  AND F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))  
  END  
  IF @FleetGroupCD <> ''  
  BEGIN   
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
  FROM Fleet F   
  LEFT OUTER JOIN FleetGroupOrder FGO  
  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
  LEFT OUTER JOIN FleetGroup FG   
  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
  WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
  AND F.CustomerID = @CUSTOMER   
  END  
  ELSE IF @TailNUM = '' AND @FleetGroupCD = ''  
  BEGIN   
  INSERT INTO @TempFleetID  
  SELECT DISTINCT F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID  
  FROM Fleet F   
  WHERE F.CustomerID = @CUSTOMER   
  END  
     
  -----------------------------TailNum and Fleet Group Filteration----------------------   
CREATE TABLE #DEPARTINFO
	(
	[DEPARTMENTID] BIGINT
	,[DateRange] NVARCHAR(100)
	,[dept_desc] NVARCHAR(25)
	,[auth_desc] NVARCHAR(25)
	,[dept_code] NVARCHAR(8)
	,[auth_code] NVARCHAR(8)
	,[legid] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[schedttm] DATETIME
	,[flt_date] DATETIME
	,[dispatchno] NVARCHAR(12)
	,[lognum] BIGINT
	,[reqdept] NVARCHAR(8)
	,[reqauth] NVARCHAR(8)
	,[authdesc] NVARCHAR(25)
	,[leg_num] BIGINT
	,[cat_code] NVARCHAR(4)
	,[fltcatdesc] NVARCHAR(25)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[max_pax] NUMERIC(3,0)
	,[pax_total] BIGINT
	,[depicao_id] CHAR(4)
	,[arricao_id] CHAR(4)
	,[es_total] BIGINT
	,[dep_city_name] NVARCHAR(25)
	,[arr_city_name] NVARCHAR(25)
	,[tot_line] NVARCHAR(10)
	,[paxname] NVARCHAR(200)
	,[fltpurpose] CHAR(2)
	,[t_blk_hrs] NUMERIC(6,3)
	,[t_flt_hrs] NUMERIC(6,3)
	,[t_pax_total] NUMERIC(4,0)
	
	)
INSERT INTO #DEPARTINFO
SELECT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[Date Range] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
	,[dept_desc] = CASE WHEN D.DepartmentName = ' ' OR D.DepartmentName  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END            --used instead of PostflightMain.POMainDescription2
	,[auth_desc] = CASE WHEN DA.DeptAuthDescription = '' OR DA.DeptAuthDescription  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END             --used instead of PostflightMain.POMainDescription2
	,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                  
	,[auth_code] =  CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END          
	,[legid] = POL.POLegID
	,[tail_nmbr] = F.TailNum
	,[schedttm] = POL.ScheduledTM
	,[flt_date] = CONVERT (DATE, POL.ScheduledTM)
	,[dispatchno] = POM.DispatchNum
	,[lognum] = POM.LogNum 
	,[reqdept] = D.DepartmentCD
	,[reqauth] = DA.AuthorizationCD
	,[authdesc] = DA.DeptAuthDescription
	,[leg_num] = POL.LegNUM
	,[cat_code] = FC.FlightCatagoryCD
	,[fltcatdesc] = CASE WHEN POM.LogNum IS NULL THEN 'NO ACTIVITY' ELSE FC.FlightCatagoryDescription END
	,[blk_hrs] = ROUND(POL.BlockHours,1) 
	,[flt_hrs] = ROUND(POL.FlightHours,1) 
	,[distance] = POL.Distance
	,[max_pax] = F.MaximumPassenger
	,[pax_total] = POL.PassengerTotal
	,[depicao_id] = A.IcaoID
	,[arricao_id] = AA.IcaoID
	,[es_total] = ISNULL((F.MaximumPassenger - POL.PassengerTotal),0)
	,[dep_city_name] = A.CityName
	,[arr_city_name] = AA.CityName
	,[tot_line] = ''
	,[paxname] = P.PassengerName
	,[fltpurpose] =  FP.FlightPurposeCD
	-- ,ROW_NUMBER() OVER(PARTITION BY POL.POLegID ORDER BY POL.BlockHours,POL.FlightHours,POL.PassengerTotal) AS ROW
	,[t_blk_hrs] = ISNULL(CASE WHEN (ROW_NUMBER() OVER(PARTITION BY POL.POLegID ORDER BY POL.BlockHours,POL.FlightHours,POL.PassengerTotal))=1
								THEN POL.BlockHours ELSE '0' END,'0')
	,[t_flt_hrs] = ISNULL(CASE WHEN (ROW_NUMBER() OVER(PARTITION BY POL.POLegID ORDER BY POL.BlockHours,POL.FlightHours,POL.PassengerTotal))=1
								THEN POL.FlightHours ELSE '0' END,'0')
	,[t_pax_total] = ISNULL(CASE WHEN (ROW_NUMBER() OVER(PARTITION BY POL.POLegID ORDER BY POL.BlockHours,POL.FlightHours,POL.PassengerTotal))=1
								THEN POL.PassengerTotal ELSE '0' END,'0')
	
FROM PostflightLeg POL  
	LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
	LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
	LEFT OUTER JOIN Passenger P ON POP.PassengerID = P.PassengerRequestorID
	INNER JOIN Fleet F ON F.FleetID = POM.FleetID 
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F1.FleetID = F.FleetID  
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID  
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0   
	AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID  
	--LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID  
	LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID  
	LEFT OUTER JOIN Airport A ON A.AirportID = POL.DepartICAOID  
	LEFT OUTER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID   
	LEFT OUTER JOIN FlightPurpose FP ON FP.FlightPurposeID = POP.FlightPurposeID
WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 AND  POL.IsDeleted=0 AND
	POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)  
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')  
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')  
	AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase = 0)  
GROUP BY D.DEPARTMENTID, POL.POLegID, POL.LEGNUM, DepartmentCD, D.DepartmentName, DA.DeptAuthDescription 
	, DA.AuthorizationCD, FC.FlightCatagoryCD, FC.FlightCatagoryDescription, TripLegID, BlockHours, FlightHours, Distance, PassengerTotal
	, POM.LogNum, f.TailNum, POL.ScheduledTM, POM.DispatchNum, A.IcaoID, A.CityName, AA.IcaoID, AA.CityName, f.MaximumPassenger
	, POP.PassengerFirstName, POP.PassengerLastName, POP.PassengerMiddleName, FP.FlightPurposeCD, P.PassengerName
CREATE TABLE #DEPARTEMPTY
	(
	[DEPARTMENTID] BIGINT
	,[Date Range] NVARCHAR(100)
	,[dept_desc] NVARCHAR(25)
	,[auth_desc] NVARCHAR(25)
	,[dept_code] NVARCHAR(8)
	,[auth_code] NVARCHAR(8)
	,[legid] BIGINT
	,[tail_nmbr] VARCHAR(9)
	,[schedttm] DATETIME
	,[flt_date] DATETIME
	,[dispatchno] NVARCHAR(12)
	,[lognum] BIGINT
	,[reqdept] NVARCHAR(8)
	,[reqauth] NVARCHAR(8)
	,[authdesc] NVARCHAR(25)
	,[leg_num] BIGINT
	,[cat_code] NVARCHAR(4)
	,[fltcatdesc] NVARCHAR(25)
	,[blk_hrs] NUMERIC(6,3)
	,[flt_hrs] NUMERIC(6,3)
	,[distance] NUMERIC(5,0)
	,[max_pax] NUMERIC(3,0)
	,[pax_total] BIGINT
	,[depicao_id] CHAR(4)
	,[arricao_id] CHAR(4)
	,[es_total] BIGINT
	,[dep_city_name] NVARCHAR(25)
	,[arr_city_name] NVARCHAR(25)
	,[tot_line] NVARCHAR(10)
	,[paxname] NVARCHAR(200)
	,[fltpurpose] CHAR(2)
	,[t_blk_hrs] NUMERIC(6,3)
	,[t_flt_hrs] NUMERIC(6,3)
	,[t_pax_total] NUMERIC(4,0)
	
	)
INSERT INTO #DEPARTEMPTY
SELECT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[DateRange] = CONVERT(VARCHAR, @DATEFROM, 101) +'-'+ CONVERT(VARCHAR, @DATETO, 101)
	,[dept_desc] = CASE WHEN D.DepartmentName = ' ' OR D.DepartmentName  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END            --used instead of PostflightMain.POMainDescription2
	,[auth_desc] = CASE WHEN DA.DeptAuthDescription = '' OR DA.DeptAuthDescription  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END             --used instead of PostflightMain.POMainDescription2
	,[dept_code] = CASE WHEN D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END                  
	,[auth_code] =  CASE WHEN DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END          
	,[legid] = '0'
	,[tail_nmbr] = NULL
	,[schedttm] = NULL
	,[flt_date] = NULL
	,[dispatchno] = NULL
	,[lognum] = '0'
	,[reqdept] = NULL
	,[reqauth] = NULL
	,[authdesc] = NULL
	,[leg_num] = '0'
	,[cat_code] = NULL
	,[fltcatdesc] = 'NO ACTIVITY'
	,[blk_hrs] = NULL
	,[flt_hrs] = NULL
	,[distance] = NULL
	,[max_pax] = NULL
	,[pax_total] = NULL
	,[depicao_id] = NULL
	,[arricao_id] = NULL
	,[es_total] = NULL
	,[dep_city_name] = NULL
	,[arr_city_name] = NULL
	,[tot_line] = NULL
	,[paxname] = NULL
	,[fltpurpose] =  NULL
	,[t_blk_hrs] = NULL
	,[t_flt_hrs] = NULL
	,[t_pax_total] = NULL
	
FROM DEPARTMENT D
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
	AND D.DepartmentID = DA.DepartmentID
	WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0
	AND D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	DELETE FROM #DEPARTEMPTY WHERE DEPARTMENTID IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)
IF @IsZeroSuppressActivityDeptRpt = 0
BEGIN 
	SELECT * FROM #DEPARTINFO
	UNION ALL
	SELECT * FROM #DEPARTEMPTY
	ORDER BY [dept_code], [auth_code], [schedttm], [lognum], [leg_num]
END
ELSE
BEGIN
	SELECT * FROM #DEPARTINFO
	ORDER BY [dept_code], [auth_code], [schedttm], [lognum], [leg_num]
END

IF OBJECT_ID('tempdb..#DEPARTINFO') IS NOT NULL
DROP TABLE #DEPARTINFO	

IF OBJECT_ID('tempdb..#DEPARTEMPTY') IS NOT NULL
DROP TABLE #DEPARTEMPTY	


--EXEC spGetReportPOSTDepartmentAuthorizationDetailExportInformation 'supervisor_99','2009-01-01','2009-1-3', '', '', '', '',0


GO


