IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewcheckListExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewcheckListExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportCrewcheckListExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportCrewcheckListExportInformation
-- Author: SUDHAKAR J
-- Create date: 31 May 2012
-- Description: Get CrewChecklist information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 14 Jun 2012	Sudhakar J	1.1		Added parameter @CustomerID
-- 29 Aug		Sudhakar	1.2		ClientID filter
-- 
-- =============================================
SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
--SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT 
		[code] = CrewCheckCD
		,[desc] = CrewChecklistDescription
		,[schd_chk] = IsScheduleCheck
	FROM [CrewChecklist]
	WHERE 
		IsDeleted = 0
		AND CustomerID = @CUSTOMERID
		--AND ClientID = ISNULL(@CLIENTID, ClientID)
	ORDER BY CrewCheckCD
-- EXEC spGetReportCrewcheckListExportInformation 'UC'

GO


