IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripInformationMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripInformationMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTTripInformationMain] 
( 
	@UserCD AS VARCHAR(30), --Mandatory 
	@UserCustomerID AS VARCHAR(30),          
	@DATEFROM AS DATE , --Mandatory  
	@DATETO AS DATE , --Mandatory  
	@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@LogNum NVARCHAR(1000) = '',
	@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase
	@UserHomebaseID AS VARCHAR(30)  
 )   
AS 
-- ================================================================================ 
-- SPC Name: [spGetReportPOSTTripInformationMain]  
-- Author: HARIHARAN S  
-- Create date: 01/30/2013  
-- Description: Get Trip Detail for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
-- ================================================================================
BEGIN            
SET NOCOUNT ON  

-----------------------------TailNum and Fleet Group Filteration----------------------
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------
SELECT DISTINCT
POM.LogNum
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
    AND (DE.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0
END   
--EXEC spGetReportPOSTTripInformationMain 'SUPERVISOR_99','10099','01-01-2009','01-01-2010','','','','','',0,'' 
  

GO


