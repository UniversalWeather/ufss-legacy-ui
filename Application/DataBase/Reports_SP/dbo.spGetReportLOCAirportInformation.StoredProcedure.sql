IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLOCAirportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLOCAirportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportLOCAirportInformation]  
	    @UserCustomerID BIGINT
       ,@IcaoID VARCHAR(4)
       ,@AirportID VARCHAR(30)  
       ,@CountryCD VARCHAR(2)
       ,@CountryID VARCHAR(30)=''    
       ,@Runway NUMERIC(6,0)=0
       ,@CityName VARCHAR(25)
       ,@MetroCD VARCHAR(3)
       ,@IATA VARCHAR(3)
       ,@MilesFROM INT 
       ,@StateName VARCHAR(25)
       ,@AirportName VARCHAR(25) 
       ,@IsHeliport BIT    
	   ,@IsEntryPort BIT    
       ,@IsInActive BIT=0    
AS

-- ================================================================================  
-- SPC Name: spGetReportLOCAirportInformation  
-- Author: Askar  
-- Create date: 17 Sep 2013  
-- Description:  Get the Locator information of Airport  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- =================================================================================   
BEGIN                     
        
DECLARE @PointLatitude DECIMAL(18,10)        
DECLARE @PointLongitutde DECIMAL(18,10)         
DECLARE @FROMCityCenter BIT = 0        
DECLARE @FROMOneAirportCenter BIT = 0        
DECLARE @FROMOneCityCenter BIT = 0      
DECLARE @CtryID BIGINT
SELECT @CtryID=CountryID FROM Country WHERE CountryCD=@CountryCD  
        
IF (CONVERT(BIGINT,@AirportID) > 0)        
BEGIN   

   
SELECT @UserCustomerID  = CustomerID FROM AIRPORT WHERE AirportID = CONVERT(BIGINT,@AirportID)          
END        
        
-- IF airport and miles are entered        
IF((CONVERT(BIGINT,@AirportID) > 0 )  AND (@MilesFROM > 0 ))        
BEGIN 

 SET @FROMOneAirportCenter = 1        
 SELECT @PointLatitude = LatitudeRad, @PointLongitutde = LongitudeRad FROM        
  (SELECT CASE WHEN UPPER(LatitudeNorthSouth) = 'S' THEN (0-(LatitudeDegree + (LatitudeMinutes / 60))) * PI() / 180  ELSE (LatitudeDegree + (LatitudeMinutes / 60)) * PI() / 180 END LatitudeRad,        
          CASE WHEN UPPER(LongitudeEastWest) = 'W' THEN (0-(LongitudeDegrees + (LongitudeMinutes / 60))) * PI() / 180  ELSE (LongitudeDegrees + (LongitudeMinutes / 60)) * PI() / 180 END LongitudeRad,        
          IcaoID,        
    CityName FROM Airport WHERE AirportID = CONVERT(BIGINT,@AirportID) and CustomerID = @UserCustomerID  )         
 Temp1  
       
END        
        
-- IF only airport enter and miles not entered        
IF( ((CONVERT(BIGINT,@AirportID) > 0 )  AND (@MilesFROM = 0 )) OR ((@CityName is not NULL) AND (@MilesFROM = 0 )) OR ((@CityName is NULL) AND (CONVERT(BIGINT,@AirportID) =0 )))        
BEGIN        

    SET @FROMCityCenter = 1        
END        
        
IF ( (@CityName is not NULL) AND (@MilesFROM > 0 ))        
BEGIN 
   
SET @FROMOneCityCenter = 1    
 
 SELECT @PointLatitude = LatitudeRad, @PointLongitutde = LongitudeRad FROM        
  (SELECT CASE WHEN UPPER(LatitudeNorthSouth) = 'S' THEN (0-(LatitudeDegree + (LatitudeMinutes / 60))) * PI() / 180  ELSE (LatitudeDegree + (LatitudeMinutes / 60)) * PI() / 180 END LatitudeRad,        
          CASE WHEN UPPER(LongitudeEastWest) = 'W' THEN (0-(LongitudeDegrees + (LongitudeMinutes / 60))) * PI() / 180  ELSE (LongitudeDegrees + (LongitudeMinutes / 60)) * PI() / 180  END LongitudeRad        
      FROM WorldCity    
      WHERE UPPER(CityName) = UPPER(RTRIM(LTRIM(@CityName)))        
       AND UPPER(ISNULL(StateName,'')) = UPPER(RTRIM(LTRIM(@StateName)))        
        AND CountryID =CONVERT(BIGINT,(CASE WHEN ISNULL(@CountryID,'') <> '' THEN @CountryID ELSE  @CtryID END))          
       )Temp1        
       SET @StateName   = ''  
END   

IF((CONVERT(BIGINT,@AirportID) > 0 )  AND ((UPPER(RTRIM(LTRIM(@IcaoID)))) <> ''))
BEGIN

SET @IcaoID   = ''


END     
           

    --1.city,country and state,2.icao 3.iata,city,country,state          
SELECT A.AirportName        
  ,A.IcaoID        
  ,City=A.CityName        
  ,State=A.StateName        
  ,Country=C.CountryName        
  ,RunwayLength=A.LongestRunway         
  ,Helipt=A.IsHeliport        
  ,IsEntryPort=A.IsEntryPort      
  ,CASE WHEN UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) * PI() / 180  ELSE (A.LatitudeDegree + (A.LatitudeMinutes / 60)) * PI() / 180 END LatitudeRad        
  ,CASE WHEN UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) * PI() / 180  ELSE (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) * PI() / 180 END LongitudeRad        
  ,CASE WHEN UPPER(W.LatitudeNorthSouth) = 'S' Then (0-(W.LatitudeDegree + (W.LatitudeMinutes / 60))) * PI() / 180  ELSE (W.LatitudeDegree + (W.LatitudeMinutes / 60)) * PI() / 180 END LatitudeRadCityCenter        
  ,CASE WHEN UPPER(W.LongitudeEastWest) = 'W' Then (0-(W.LongitudeDegrees + (W.LongitudeMinutes / 60))) * PI() / 180  ELSE (W.LongitudeDegrees + (W.LongitudeMinutes / 60)) * PI() / 180 END LongitudeRadCityCenter            
,A.AirportID        
  ,A.LatitudeDegree        
  ,A.LatitudeMinutes        
  ,A.LatitudeNorthSouth        
  ,A.LongitudeDegrees        
  ,A.LongitudeMinutes        
  ,A.LongitudeEastWest        
  ,Latitude=(CASE WHEN A.LatitudeNorthSouth='S' THEN '-' ELSE '' END)         
  ,Longitude=(CASE WHEN A.LongitudeEastWest='W' THEN '-' ELSE '' END)        
  ,LatitudeCalculation = (A.LatitudeDegree + (FLOOR(ISNULL((A.LatitudeMinutes/60),0)*100)*0.01))         
  ,LongitudeCalculation = (A.LongitudeDegrees + (FLOOR(ISNULL((A.LongitudeMinutes/60),0)*100)*0.01))         
  ,C.CountryCD        
  ,CASE WHEN UPPER(A.LatitudeNorthSouth) = 'S' Then (0-(A.LatitudeDegree + (A.LatitudeMinutes / 60))) else (A.LatitudeDegree + (A.LatitudeMinutes / 60)) END OrigLatitude        
  ,CASE WHEN UPPER(A.LongitudeEastWest) = 'W' Then (0-(A.LongitudeDegrees + (A.LongitudeMinutes / 60))) else (A.LongitudeDegrees + (A.LongitudeMinutes / 60)) END OrigLongitude         
  INTO  #TempResultTable         
  FROM Airport A              
  LEFT OUTER JOIN Metro M ON A.MetroID = M.MetroID                
  LEFT OUTER JOIN Country C ON A.CountryID = C.CountryID         
  LEFT OUTER JOIN  WorldCity W  ON UPPER(A.CityName) = UPPER(RTRIM(LTRIM(W.CityName))) and (CASE WHEN RTRIM(LTRIM(ISNULL(W.StateName,'')))='' THEN '1' ELSE UPPER(A.StateName) END) = ISNULL(UPPER(RTRIM(LTRIM(W.StateName))),'1') and A.CountryID = W.CountryID AND  @FromOneAirportCenter = 0 AND  @FromOneCityCenter = 0         
      WHERE (A.CustomerID=@UserCustomerID OR A.CustomerID=10000)      
        AND (@FromOneAirportCenter = 1 OR ISNULL(CONVERT(BIGINT,@AirportID),0)=0 OR A.AirportID = CONVERT(BIGINT,@AirportID))         
        AND (A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID='')        
        AND (C.CountryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CountryCD, ',')) OR @CountryCD='')        
        AND  ISNULL(A.LongestRunway,0)>=@Runway        
        AND (A.CityName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CityName, ',')) OR @CityName='' OR @FromOneCityCenter = 1)        
        AND (M.MetroCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@MetroCD, ',')) OR @MetroCD='')        
        AND (A.StateName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@StateName, ',')) OR @StateName='')        
        AND (A.AirportName IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AirportName, ',')) OR @AirportName='')        
        AND (A.Iata IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IATA , ',')) OR @IATA='')        
        AND ISNULL(A.IsHeliport, 0) = CASE WHEN ISNULL(@IsHeliport,0)=0 THEN ISNULL(A.IsHeliport, 0)ELSE @IsHeliport END          
  AND ISNULL(A.IsEntryPort, 0) = CASE WHEN ISNULL(@IsEntryPort,0)=0 THEN ISNULL(A.IsEntryPort, 0)ELSE @IsEntryPort END         
  AND ((A.IsInActive =@IsInActive) OR @IsInActive = 1)         
  AND A.IsDeleted=0        


SELECT AirportName        
      ,T1.IcaoID        
      ,City        
      ,State        
      ,Country        
      ,RunwayLength        
      --,CASE WHEN  ISNULL(Helipt,0)=0  THEN 'FALSE' ELSE 'TRUE' END as Helipt    
      ,Helipt    
      ,IsEntryPort        
      ,Miles=CONVERT(INT,CASE WHEN @FromCityCenter = 1 THEN ISNULL((CAST( ACOS(SIN(LatitudeRadCityCenter) * SIN(LatitudeRad) + COS(LatitudeRadCityCenter) * COS(LatitudeRad) * COS(LongitudeRad - (LongitudeRadCityCenter))) * 3959   AS INT)),0) ELSE ISNULL((CAST( ACOS(SIN(@PointLatitude) * SIN(LatitudeRad) + COS(@PointLatitude) * COS(LatitudeRad) * COS(LongitudeRad - (@PointLongitutde))) * 3959   AS INT)),0) END ,0)        
      ,T1.LatitudeDegree        
      ,T1.LatitudeMinutes        
 ,T1.LatitudeNorthSouth        
      ,T1.LongitudeDegrees        
      ,T1.LongitudeMinutes        
      ,T1.LongitudeEastWest        
      ,T1.Latitude        
      ,T1.Longitude        
      ,T1.LatitudeCalculation        
      ,T1.LongitudeCalculation        
      ,CountryCD        
,OrigLatitude        
      ,OrigLongitude  
      ,ACOS(SIN(@PointLatitude) * SIN(LatitudeRad) + COS(@PointLatitude) * COS(LatitudeRad) * COS(LongitudeRad - (@PointLongitutde))) * 3959 miles 
      ,@PointLatitude AS PointLatitude       
      ,LatitudeRad AS LatitudeRad
      ,LongitudeRad AS LongitudeRad
      ,@PointLongitutde AS PointLongitutde
  FROM #TempResultTable T1        
 INNER JOIN (SELECT DISTINCT(IcaoID), MAX(AirportID) AirportID FROM #TempResultTable GROUP BY IcaoID) T2 ON T1.AirportID=T2.AirportID         
 WHERE  @FromCityCenter = 1 OR (@MilesFrom=0 OR ACOS(SIN(@PointLatitude) * SIN(LatitudeRad) + COS(@PointLatitude) * COS(LatitudeRad) * COS(LongitudeRad - (@PointLongitutde))) * 3959  <= @MilesFrom)        
 ORDER BY AirportName        
               
END    

	   
IF OBJECT_ID('tempdb..#TempResultTable') IS NOT NULL
DROP TABLE  #TempResultTable 






GO


