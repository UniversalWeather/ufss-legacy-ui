IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREDestinationExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREDestinationExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPREDestinationExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@IcaoID AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomeBase AS BIT = 0
AS
-- ===============================================================================
-- SPC Name: spGetReportPREDestinationInformation
-- Author: Aishwarya.M
-- Create date: 10 Aug 2012
-- Description: Get Destination Export information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------

CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------



		SELECT   arricao_id = A.ICAOID   
				,city = A.CityName
				,tail_nmbr = F.TailNum
				,estdepdt = PM.EstDepartureDT
				,locarr = PL.ArrivalDTTMLocal
				,locdep = PL.DepartureDTTMLocal
				,orig_nmbr = PM.TripNUM
				,trip_desc = dbo.TripLocation(PM.TripID,CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))))
				,ac_code = F.AircraftCD
		FROM PreflightMain PM 
			INNER JOIN (
				SELECT TripID, TripNUM, ArriveICAOID, CustomerID,ArrivalDTTMLocal, DepartureDTTMLocal FROM PreflightLeg WHERE IsDeleted = 0
			) PL ON PM.TripID = PL.TripID
			INNER  JOIN (
					SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID 
					FROM Fleet F 
			  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
			INNER JOIN #TempFleetID TF ON TF.FleetID=F.FleetID
			LEFT OUTER JOIN ( 
				SELECT AirportID, IcaoID, CityName, CustomerID FROM Airport
			) A ON PL.ArriveICAOID = A.AirportID
		WHERE PM.IsDeleted = 0
		    AND PM.TripStatus = 'T'
			AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		    AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		    AND (A.IcaoID IN( (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')))OR @IcaoID='')
	        AND (PM.HomebaseID IN( CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomeBase=0)
		    ORDER BY arricao_id,locdep;


IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID
END

	
-- EXEC spGetReportPREDestinationExportInformation 'JWILLIAMS_13','2012/09/01','2012/09/07', '', '', '', 1

GO


