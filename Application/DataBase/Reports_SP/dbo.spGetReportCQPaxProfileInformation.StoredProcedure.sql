IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQPaxProfileInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQPaxProfileInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCQPaxProfileInformation]  
(  
     @UserCD VARCHAR(30),      
              @UserCustomerID VARCHAR(30),      
              @FileNumber VARCHAR(200)='',      
              @QuoteNUM VARCHAR(200)='',      
              @LegNUM VARCHAR(200)='',      
              @BeginDate DATETIME=NULL,      
              @EndDate DATETIME=NULL,      
              @TailNum VARCHAR(200)='',      
              @VendorCD VARCHAR(200)='',      
              @HomeBaseCD VARCHAR(200)='',      
              @AircraftCD VARCHAR(200)='',      
              @CQCustomerCD VARCHAR(30)='',      
              @SalesPersonCD VARCHAR(200)=''   
)  
AS  
-- ==========================================================      
-- SPC Name:spGetReportCQPaxProfileInformation       
-- Author: Aishwarya      
-- Create date: 12 June 2013      
-- Description: Get Report Charter Quote Pax Profile      
-- Revision History      
-- Date  Name  Ver  Change      
--       
-- ==========================================================  
BEGIN  
SELECT @QuoteNUM = CASE WHEN @QuoteNUM = '' OR (@BeginDate IS NOT NULL AND @BeginDate <> '' )THEN 1 ELSE @QuoteNUM END


IF @BeginDate IS NOT NULL AND @BeginDate <> ''  
 BEGIN  
  
  SELECT FileTemp.* FROM(
  SELECT DISTINCT FileNumber = CF.FileNUM  
  ,QuoteNumber = CM.QuoteNUM  
  ,TripDates = dbo.GetShortDateFormatByUserCD(@UserCD,MIN(CL.DepartureDTTMLocal)) + ' - ' + dbo.GetShortDateFormatByUserCD(@UserCD,MAX(CL.ArrivalDTTMLocal))  
  ,TailNumber = F.TailNum  
  ,AircraftType  = A.AircraftCD  
  ,CQDescription = CF.CQFileDescription  
  ,CustomerName = CQC.CQCustomerName  
  ,PaxName = ISNULL(P.LastName,'') + ', ' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,PaxCode = P.PassengerRequestorCD  
  ,PaxNotes = P.Notes
  ,PaxInfoCD = PA.AdditionalINFOCD 
  ,PaxInfoDesc = PA.AdditionalINFODescription
  ,PaxInfoValue = ISNULL(PA.AdditionalINFOValue,'') 
  ,MinDeptDate=MIN(CL.DepartureDTTMLocal)
  ,MaxArrDate=MAX(CL.ArrivalDTTMLocal)
  ,DENSE_RANK()OVER(ORDER BY MIN(CL.DepartureDTTMLocal),MAX(CL.ArrivalDTTMLocal),CF.FileNUM) Rnk 
  ,PA.PassengerAdditionalInfoID
  
  FROM CQFile CF   
   INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID  AND CM.IsDeleted=0
   INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID  AND CL.IsDeleted=0
   INNER JOIN Fleet F ON CM.FleetID = F.FleetID  
   INNER JOIN CQPassenger CP ON CP.CQLegID = CL.CQLegID  
   INNER JOIN Passenger P ON CP.PassengerRequestorID = P.PassengerRequestorID  
   INNER JOIN Company C ON C.HomebaseID=F.HomebaseID    
   LEFT OUTER JOIN CQCustomer CQC ON CF.CQCustomerID = CQC.CQCustomerID  
   LEFT OUTER JOIN PassengerAdditionalInfo PA ON P.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted = 0
   LEFT OUTER JOIN Aircraft A ON F.AircraftID = A.AircraftID  
   LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID   
   LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID  
   LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID   
   WHERE  
	( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )      
    AND ( V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )      
    AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )      
    AND ( A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )      
    AND ( SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ',')) OR @SalesPersonCD = '' )      
    AND CF.EstDepartureDT BETWEEN @BeginDate AND @EndDate      
    AND CF.CustomerID = CONVERT(BIGINT,@UserCustomerID)       
    AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')      
    AND CM.QuoteNUM = @QuoteNUM
    GROUP BY  CF.FileNUM,CM.QuoteNUM,F.TailNum,A.AircraftCD,CF.CQFileDescription  
       ,CQC.CQCustomerName,P.LastName,P.FirstName,P.MiddleInitial,P.PassengerRequestorCD,P.Notes,PA.AdditionalINFOCD,PA.AdditionalINFODescription,PA.AdditionalINFOValue,PA.PassengerAdditionalInfoID  
    )FileTemp
    WHERE Rnk=1
    ORDER BY MinDeptDate,MaxArrDate,FileNumber,PassengerAdditionalInfoID
 END  
 ELSE  
 BEGIN  
  SELECT DISTINCT FileNumber = CF.FileNUM  
  ,QuoteNumber = CM.QuoteNUM  
  ,TripDates = dbo.GetShortDateFormatByUserCD(@UserCD,MIN(CL.DepartureDTTMLocal)) + ' - ' + dbo.GetShortDateFormatByUserCD(@UserCD,MAX(CL.ArrivalDTTMLocal))  
  ,TailNumber = F.TailNum  
  ,AircraftType  = A.AircraftCD  
  ,CQDescription = CF.CQFileDescription  
  ,CustomerName = CQC.CQCustomerName  
  ,PaxName = ISNULL(P.LastName,'') + ', ' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,PaxCode = P.PassengerRequestorCD  
  ,PaxNotes = P.Notes 
  ,PaxInfoCD = PA.AdditionalINFOCD 
  ,PaxInfoDesc = PA.AdditionalINFODescription 
  ,PaxInfoValue = ISNULL(PA.AdditionalINFOValue,'') 
  ,MinDeptDate=MIN(CL.DepartureDTTMLocal)
  ,MaxArrDate=MAX(CL.ArrivalDTTMLocal) 
  ,PA.PassengerAdditionalInfoID
    
  FROM CQFile CF   
   INNER JOIN CQMain CM ON CF.CQFileID = CM.CQFileID AND CM.IsDeleted=0  
   INNER JOIN CQLeg CL ON CM.CQMainID = CL.CQMainID AND CL.IsDeleted=0 
   INNER JOIN Fleet F ON CM.FleetID = F.FleetID  
   INNER JOIN CQPassenger CP ON CP.CQLegID = CL.CQLegID  
   INNER JOIN Passenger P ON CP.PassengerRequestorID = P.PassengerRequestorID  
   INNER JOIN Company C ON C.HomebaseID=F.HomebaseID    
   LEFT OUTER JOIN CQCustomer CQC ON CF.CQCustomerID = CQC.CQCustomerID  
   LEFT OUTER JOIN PassengerAdditionalInfo PA ON P.PassengerRequestorID = PA.PassengerRequestorID AND PA.IsDeleted = 0 
   LEFT OUTER JOIN Aircraft A ON F.AircraftID = A.AircraftID  
   LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID   
   LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID  
   LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID    
   WHERE 
   ( CF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )      
    AND ( CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )      
    AND ( CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' )        
    AND CF.CustomerID = CONVERT(BIGINT,@UserCustomerID)   
    GROUP BY CF.FileNUM,CM.QuoteNUM,F.TailNum,A.AircraftCD,CF.CQFileDescription  
      ,CQC.CQCustomerName,P.LastName,P.FirstName,P.MiddleInitial,P.PassengerRequestorCD,P.Notes,PA.AdditionalINFOCD,PA.AdditionalINFODescription,PA.AdditionalINFOValue,PA.PassengerAdditionalInfoID  
	ORDER BY MinDeptDate,MaxArrDate,FileNumber,PA.PassengerAdditionalInfoID
      
 END  
END   
  
--EXEC spGetReportCQPaxProfileInformation 'SUPERVISOR_99','10099','10','','','','','','','','','',''


GO


