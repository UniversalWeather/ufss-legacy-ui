IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddTripSheetReportDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddTripSheetReportDetail]
GO


SET ANSI_NULLS ON
GO


SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddTripSheetReportDetail]
(@TripSheetReportDetailID bigint,
@CustomerID bigint,
@TripSheetReportHeaderID bigint,
@ReportNumber int,
@ReportDescription varchar(35),
@IsSelected bit,
@Copies int,
@ReportProcedure varchar(8),
@ParameterVariable varchar(10),
@ParameterDescription varchar(100),
@ParameterType char(1),
@ParameterWidth int,
@ParameterCValue varchar(60),
@ParameterLValue bit,
@ParameterNValue int,
@ParameterValue varchar(MAX),
@ReportOrder int,
@TripSheetOrder int,
@LastUpdUID varchar(30),
@LastUpdTS datetime,
@IsDeleted bit,
@ParameterSize varchar(10),
@WarningMessage varchar(100))
AS BEGIN

	SET @LastUpdTS = GETUTCDATE()
	
	
	
	IF EXISTS(SELECT TripSheetReportDetailID FROM TripSheetReportDetail WHERE TripSheetReportDetailID=@TripSheetReportDetailID AND CustomerID=@CustomerID)
		BEGIN		
			UPDATE	TripSheetReportDetail
			SET	[CustomerID]=@CustomerID
				,[TripSheetReportHeaderID]=@TripSheetReportHeaderID
				,[ReportNumber]=@ReportNumber
				,[ReportDescription]=@ReportDescription
				,[IsSelected]=@IsSelected
				,[Copies]=@Copies
				,[ReportProcedure]=@ReportProcedure
				,[ParameterVariable]=@ParameterVariable
				,[ParameterDescription]=@ParameterDescription
				,[ParameterType]=@ParameterType
				,[ParameterWidth]=@ParameterWidth
				,[ParameterCValue]=@ParameterCValue
				,[ParameterLValue]=@ParameterLValue
				,[ParameterNValue]=@ParameterNValue
				,[ParameterValue]=@ParameterValue
				,[ReportOrder]=@ReportOrder
				,[TripSheetOrder]=@TripSheetOrder
				,[LastUpdUID]=@LastUpdUID
				,[LastUpdTS]=@LastUpdTS
				,[IsDeleted]=@IsDeleted
				,[ParameterSize]=@ParameterSize
				,[WarningMessage]=@WarningMessage
			WHERE [TripSheetReportDetailID]=@TripSheetReportDetailID
		END
	ELSE
		BEGIN
			EXECUTE dbo.usp_GetSequenceNumber @CustomerID, 'MasterModuleCurrentNo',  @TripSheetReportDetailID OUTPUT
	
			INSERT INTO TripSheetReportDetail
			   ([TripSheetReportDetailID]
			   ,[CustomerID]
			   ,[TripSheetReportHeaderID]
			   ,[ReportNumber]
			   ,[ReportDescription]
			   ,[IsSelected]
			   ,[Copies]
			   ,[ReportProcedure]
			   ,[ParameterVariable]
			   ,[ParameterDescription]
			   ,[ParameterType]
			   ,[ParameterWidth]
			   ,[ParameterCValue]
			   ,[ParameterLValue]
			   ,[ParameterNValue]
			   ,[ParameterValue]
			   ,[ReportOrder]
			   ,[TripSheetOrder]
			   ,[LastUpdUID]
			   ,[LastUpdTS]
			   ,[IsDeleted]
			   ,[ParameterSize]
			   ,[WarningMessage])
			VALUES
			   (@TripSheetReportDetailID,
			   @CustomerID,
			   @TripSheetReportHeaderID,
			   @ReportNumber,
			   @ReportDescription,
			   @IsSelected,
			   @Copies,
			   @ReportProcedure,
			   @ParameterVariable,
			   @ParameterDescription,
			   @ParameterType,
			   @ParameterWidth,
			   @ParameterCValue,
			   @ParameterLValue,
			   @ParameterNValue,
			   @ParameterValue,
			   @ReportOrder,
			   @TripSheetOrder,
			   @LastUpdUID,
			   @LastUpdTS,
			   @IsDeleted,
			   @ParameterSize,
			   @WarningMessage)
		END

	SELECT @TripSheetReportDetailID as TripSheetReportDetailID

END
GO