IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDelayTypeInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDelayTypeInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportDelayTypeInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportDelayTypeInformation
-- Author: SUDHAKAR J
-- Create date: 31 May 2012
-- Description: Get Delay Type information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 14 Jun 2012	Sudhakar J	1.1		Added parameters @CustomerID
-- 29 Aug		Sudhakar	1.1		ClientID filter
-- 
-- =============================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
--SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SELECT [CustomerID]
      ,[DelayTypeCD]
      ,[DelayTypeDescription]
  FROM [DelayType]
  WHERE 
    IsDeleted = 0
	AND CustomerID = @CUSTOMERID
	--AND ClientID = ISNULL(@CLIENTID, ClientID)
  ORDER BY 	CustomerID, DelayTypeCD
-- EXEC spGetReportDelayTypeInformation 'UC'

GO


