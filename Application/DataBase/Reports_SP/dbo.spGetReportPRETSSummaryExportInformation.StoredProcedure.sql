IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








  
CREATE PROCEDURE [dbo].[spGetReportPRETSSummaryExportInformation]  
 @UserCD            AS VARCHAR(30)      
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal   
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
  ,@ReportHeaderID VARCHAR(30) = '' -- 10099165805 TemplateID from TripSheetReportHeader table
 ,@TempSettings VARCHAR(MAX) = '' --'PAXMNORD::TRUE' PONEPERLIN::FALSE PAXORDER
AS  

BEGIN  
  
  

-- =========================================================================  
-- SPC Name: spGetReportPRETSSummaryExportInformation  
-- Author: AISHWARYA.M  
-- Create date: 21 Sep 2012  
-- Description: Get Preflight Tripsheet Summary Export information for REPORTS  
-- Revision History  
-- Date			Name  Ver  Change  
-- 15May 2014	Aishwarya.M Changes made in crew hotel join
-- ==========================================================================  
SET NOCOUNT ON  
-- [Start] Report Criteria --  
 DECLARE @tblTripInfo AS TABLE (  
 TripID BIGINT  
 , LegID BIGINT  
 , PassengerID BIGINT  
 )  
  
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
 -- [End] Report Criteria --  
  

			
			
DECLARE @FormatSettings TABLE (
	NOGO	BIT
,	PASSENG	BIT
,	PASSPHONE	BIT
,	PASSCNG	BIT
,	PASSPURP	SMALLINT
,	CREW	BIT
,	LEGNOTES	SMALLINT
,	TRIPALERTS	BIT
,	FBO	BIT
,	FBODEP	BIT
,	AIRARRIV	BIT
,	ANOTES	SMALLINT
,	PAXHOTEL	BIT
,	PAXTRANS	BIT
,	CREWHOTEL	BIT
,	CREWTRANS	BIT
--,	MAINHOTEL	BIT
,	DEPCATER	BIT
,	OUTINST	BIT
,	PAXRPTPOS	SMALLINT
,	PASSDEPT	BIT
,	DISPATCHNO	BIT
,	PURPOSE	BIT
,	ZULUTIME	BIT
,	CHRMONTH	BIT
,   LEGNOTESF SMALLINT
,   DISPATCHIN SMALLINT
,   DSPTEMAIL BIT
,   DISPATCHER BIT
,   DSPTPHONE BIT
,	DCREWHOTEL BIT
,	DMAINHOTEL BIT
,	BUSPHONE BIT
,	PRIMARYMOB BIT
)
INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 13, @TempSettings
--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161677, 13, 'FBODEP::FALSE' 

DECLARE @PURPOSE BIT,@PASSENG BIT,@CREW BIT,@FBO BIT,@AIRARRIV BIT,@ANOTES BIT,@PAXHOTEL BIT,
        @PAXTRANS BIT,@CREWHOTEL BIT,@CREWTRANS BIT,@MAINHOTEL BIT,@DEPCATER BIT,@OUTINST BIT,@TRIPALERTS BIT,@LEGNOTES SMALLINT; 

SELECT @LEGNOTES=LEGNOTES,@PURPOSE=PURPOSE,@PASSENG=PASSENG,@CREW=CREW,@FBO=FBO,@AIRARRIV=AIRARRIV,@ANOTES=ANOTES,@PAXHOTEL=PAXHOTEL,
       @PAXTRANS=PAXTRANS,@CREWHOTEL=CREWHOTEL,@CREWTRANS=CREWTRANS,@MAINHOTEL=DMAINHOTEL,@DEPCATER=DEPCATER,@OUTINST=OUTINST,@TRIPALERTS=TRIPALERTS FROM @FormatSettings



SELECT @PURPOSE=0,@PASSENG=0,@CREW=0,@FBO=0,@AIRARRIV=0,@ANOTES=0,@PAXHOTEL=0,
       @PAXTRANS=0,@CREWHOTEL=0,@CREWTRANS=0,@MAINHOTEL=0,@DEPCATER=0,@OUTINST=0,@TRIPALERTS=0




SELECT DISTINCT orig_nmbr
      ,dispatchno
      ,lowdate
      ,highdate
      ,tail_nmbr
      ,type_code
      ,[desc]
      ,reqcode
      ,paxname
      ,phone
      ,mainflag
      ,flag
      ,CASE WHEN flag='L' THEN l_leg ELSE 0 END l_leg
      ,CONVERT(DATE,l_date) l_date
      ,l_local
      ,l_from
      ,l_to
      ,l_local2
      ,CASE WHEN flag='L' THEN l_arrzulu ELSE 0 END l_arrzulu 
      ,CASE WHEN flag='L' THEN l_depzulu ELSE 0 END l_depzulu
      ,CASE WHEN timeapprox=1 THEN 'TRUE' ELSE 'FALSE' END timeapprox
      ,CASE WHEN flag='L' THEN l_dist ELSE 0 END l_dist
      ,l_ete
      ,CASE WHEN flag='L' THEN l_fuel ELSE 0 END l_fuel
      ,CASE WHEN flag='L' THEN l_pax ELSE 0 END  l_pax
      ,l_fbo
      ,l_freq
      ,l_phone
      ,l_nogo
      ,p_passenger
      ,p_phone
      ,p_dept
      ,p_cng
      ,p_legs
      ,ISNULL(c_crewnum,0) c_crewnum
      ,ISNULL(n_legnum,0) n_legnum
      ,ISNULL(f_num,0) f_num
      ,f_depart
      ,f_name
      ,f_phone
      ,f_fax
      ,f_freq
      ,CASE WHEN f_uv=1 THEN 'TRUE' ELSE 'FALSE' END f_uv
      ,f_fuel
      ,ISNULL(f_last,0) f_last
      ,f_date
      ,ISNULL(f_neg,0) f_neg
      ,f_comments
      ,ISNULL(a_num,0)a_num
      ,ISNULL(i_num,0) i_num
      ,ISNULL(h_num,0) h_num
      ,h_name
      ,h_phone
      ,h_fax
      ,ISNULL(h_rate,0) h_rate
      ,ISNULL(t_num,0) t_num
      ,[t_name ]
      ,t_phone
      ,t_fax
      ,ISNULL(t_rate,0) t_rate
      ,ISNULL(r_num,0) r_num
      ,r_name
      ,r_phone
      ,r_fax
      ,ISNULL(r_rate,0) r_rate
      ,ISNULL(x_num,0) x_num
      ,[x_name ]
      ,x_phone
      ,x_fax
      ,ISNULL(x_rate,0) x_rate
      ,ISNULL(m_num,0) m_num
      ,m_phone
      ,m_fax
      ,[m_name ]
      ,ISNULL(m_rate,0) m_rate
      ,ISNULL(d_num,0) d_num
      ,d_name
      ,d_phone
      ,d_fax
      ,ISNULL(d_rate,0) d_rate
      ,ISNULL(o_num,0) o_num
      ,ISNULL(o_flag,0) o_flag
      ,outdata1
      ,outdata2
      ,outdata3
      ,outdata4
      ,outdata5
      ,outdata6
      ,outdata7
      ,numlegs
      ,purpose
      ,CASE WHEN flag='L' THEN fuel_load ELSE 0 END fuel_load 
      ,A_ALERTS
      ,N_LEGNOTES
      ,C_CREW
      ,I_NOTES
      ,H_COMMENTS
      ,T_COMMENTS
      ,R_COMMENTS
      ,X_COMMENTS
      ,M_COMMENTS
      ,D_COMMENTS
      ,Z_ALERTS
      ,OUT_COMMENT
      ,RowID FROM 
(
SELECT --flag 'L' Leg Info  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'L'  
  ,[l_leg] = PL.LegNUM  
  ,[l_date] = PL.DepartureDTTMLocal  
  ,[l_local] = SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5)  
  ,[l_from] = AD.IcaoID  
  ,[l_to] = AA.IcaoID  
  ,[l_local2] = SUBSTRING (CONVERT(VARCHAR(30), PL.ArrivalDTTMLocal ,113),13,5) 
  ,[l_depzulu] =  Cast(DateDiff(MINUTE,PL.DepartureGreenwichDTTM,PL.DepartureDTTMLocal)*1.0/60 as numeric(18,2))
  ,[l_arrzulu] = Cast(DateDiff(MINUTE,PL.ArrivalGreenwichDTTM,PL.ArrivalDTTMLocal)*1.0/60 as numeric(18,2))
  ,[timeapprox] = PL.IsApproxTM  
  ,[l_dist] = PL.Distance  
  ,[l_ete] = PL.ElapseTM  
  ,[l_fuel] = dbo.FuelBurn(@UserCD,A.AircraftCD,PL.ElapseTM,PL.PowerSetting)  
  ,[l_pax] = ISNULL(PL.PassengerTotal,0)  
  ,[l_fbo] = FBOA.FBOVendor --(To FBO)  
  ,[l_freq] = FBOA.Frequency  
  ,[l_phone] = FBOA.PhoneNUM1  
  ,[l_nogo] = PL.GoTime  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL  
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=1
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
  LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID     
  LEFT OUTER JOIN (  
	SELECT  PFL.PreflightFBOName FBOVendor,PFL.ConfirmationStatus,PFL.Comments,F.PhoneNUM1,F.PhoneNUM2,F.FaxNum,
        F.FuelBrand,F.LastFuelPrice,F.LastFuelDT,F.NegotiatedFuelPrice,F.PaymentType,F.PostedPrice,
        F.FuelQty,F.Contact,F.Frequency,F.IsUWAAirPartner,F.Addr1,F.Addr2,F.CityName,F.StateName,F.PostalZipCD,F.Remarks,
        PFL.LegID, F.FBOCD
	FROM PreflightFBOList PFL
	INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1  
   ) FBOA ON PL.LegID = FBOA.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
  --  AND @PURPOSE=1
    

    
  UNION ALL  
    
  SELECT --flag 'C' Crew  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'C'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] =NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = PL.LegNum  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=3
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
   LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON P.FlightPurposeID = FP.FlightPurposeID  
    WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
      AND @CREW=0  
  
  UNION ALL  
    
  SELECT --flag 'F' Fbo  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'F'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = PL.LegNum  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = FBDA.FBOVendor  
  ,[f_phone] = FBDA.PhoneNUM1  
  ,[f_fax] = FBDA.FaxNum  
  ,[f_freq] = FBDA.Frequency  
  ,[f_uv] = FBDA.IsUWAAirPartner  
  ,[f_fuel] = FBDA.FuelBrand  
  ,[f_last] = FBDA.LastFuelPrice  
  ,[f_date] = FBDA.LastFuelDT  
  ,[f_neg] = FBDA.NegotiatedFuelPrice  
  ,[f_comments] = FBDA.Comments  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
 ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=4
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID    
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID   
   LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID    
  LEFT OUTER JOIN (  
          
   SELECT PF.LegID, PF.Comments, FD.FBOVendor, FD.PhoneNUM1, FD.Frequency , FD.FuelBrand, FD.FaxNum, FD.IsUWAAirPartner, 
   FD.LastFuelPrice, FD.LastFuelDT, FD.NegotiatedFuelPrice  
   FROM PreflightFBOList PF  
   INNER JOIN FBO FD ON PF.FBOID = FD.FBOID AND PF.IsDepartureFBO = 1  

    
  ) FBDA ON PL.LegID = FBDA.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND @FBO=0
 
     UNION ALL
     
     SELECT --flag 'F' Fbo  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'F'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = PL.LegNum  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = FBDA.FBOVendor  
  ,[f_phone] = FBDA.PhoneNUM1  
  ,[f_fax] = FBDA.FaxNum  
  ,[f_freq] = FBDA.Frequency  
  ,[f_uv] = FBDA.IsUWAAirPartner  
  ,[f_fuel] = FBDA.FuelBrand  
  ,[f_last] = FBDA.LastFuelPrice  
  ,[f_date] = FBDA.LastFuelDT  
  ,[f_neg] = FBDA.NegotiatedFuelPrice  
  ,[f_comments] = FBDA.Comments  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
 ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=4
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
  LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  LEFT OUTER JOIN (  
          
      
   SELECT PF.LegID, PF.Comments, FA.FBOVendor, FA.PhoneNUM1, FA.Frequency , FA.FuelBrand, FA.FaxNum, FA.IsUWAAirPartner, 
   FA.LastFuelPrice, FA.LastFuelDT, FA.NegotiatedFuelPrice  
   FROM PreflightFBOList PF  
   INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1 
    
  ) FBDA ON PL.LegID = FBDA.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND @FBO=0
    
     UNION ALL  
    
  SELECT --flag 'N' PL.Notes OR PL.CrewNotes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD 
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'N'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = PL.LegNUM 
  
  --CASE WHEN (LEN(ISNULL(PL.Notes, '')) > 0) THEN PL.LegNum   
  --     ELSE   
  --     CASE WHEN (LEN(ISNULL(PL.CrewNotes, '')) > 0) THEN PL.LegNum END  
  --        END  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=5
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
  LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID     
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
   -- AND PL.Notes<>''
    AND PL.Notes IS NOT NULL 
   AND @LEGNOTES <>4 
     
    
  UNION ALL  
    
  SELECT --flag 'H' Pax Hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD 
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'H'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = PL.LegNum--(Pax Hotel)  
  ,[h_name] = PH.Name  
  ,[h_phone] = PH.PhoneNum  
  ,[h_fax] = PH.FaxNum  
  ,[h_rate] = PH.NegociatedRate    
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = PH.PassengerRequestorCD   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=8
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
LEFT JOIN  (SELECT DISTINCT  P.PassengerRequestorCD,PPL.PassengerLastName,PPL.LegID,PPL.PreflightPassengerListID,[Name] = PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,
	PPH.FaxNum,[NegociatedRate]=PPH.Rate, PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments ,RANK() OVER (PARTITION BY  PPL.PassengerLastName ORDER BY PPL.PassengerLastName ASC) Rnk
	FROM PreflightPassengerList PPL   
	INNER JOIN Passenger P ON PPL.PassengerID=P.PassengerRequestorID
	INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID 


	INNER JOIN @tblTripInfo T ON T.LegID=PPL.LegID 
	INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID ) PH ON PL.legID = PH.LegID    
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @PAXHOTEL=0 
 
 

    
  UNION ALL  
    
  SELECT --flag 'T' Pax Transport  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'T'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = PL.LegNum  
  ,[t_name ] = PT.TransportationVendor --(Pax Transportation)  
  ,[t_phone] = PT.PhoneNum  
  ,[t_fax] = PT.FaxNum  
  ,[t_rate] = PT.NegotiatedRate    
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=9
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
  LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID    
  LEFT OUTER JOIN (
	  SELECT PTL.LegID, [TransportationVendor]=PTL.PreflightTransportName, [PhoneNum]=PTL.PhoneNum1, 
		PTL.FaxNum, [NegotiatedRate]=PTL.PhoneNum4
	  FROM PreflightTransportList PTL  
	  INNER JOIN Transport T ON PTL.TransportID = T.TransportID  
	  WHERE PTL.CrewPassengerType = 'P'  AND PTL.IsArrivalTransport=1
  ) PT ON PL.LegID = PT.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
    AND @PAXTRANS=0  
    
  UNION ALL  
    
  SELECT --flag 'R' crew hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD 
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'R'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = PL.LegNum  
  ,[r_name] = CH.Name --(Crew Hotel)  
  ,[r_phone] = CH.PhoneNum  
  ,[r_fax] = CH.FaxNum  
  ,[r_rate] = CH.NegociatedRate  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = CH.CrewCD   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=10
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  --LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID     
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID    
  LEFT OUTER JOIN (     
   --SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.NegociatedRate,  
   --  PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments    
   --FROM PreflightCrewList PCLL   
   --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
   --INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
   	SELECT PPL.LegID,[Name] = PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,CrewCD,
	PPH.FaxNum,[NegociatedRate]=PPH.Rate, PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments,RANK() OVER (PARTITION BY  C.CrewCD ORDER BY  C.CrewCD ASC) Rnk   
	FROM PreflightCrewList PPL   
	--INNER JOIN PreflightCrewHotelList PPH ON PPL.PreflightCrewListID = PPH.PreflightCrewListID 
	INNER JOIN PreflightHotelList PPH ON PPH.LegID = PPL.LegID AND PPH.crewPassengerType = 'C'
																AND PPH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PPH.PreflightHotelListID
														AND PHCP.CrewID = PPL.CrewID
	INNER JOIN Crew C ON C.CrewID=PPL.CrewID 
	INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID 
   WHERE PPL.DutyType IN ('P', 'S')  
  ) CH ON PL.legID = CH.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @CREWHOTEL=0
    
  UNION ALL  
    
  SELECT --flag 'X' crew transportation  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'X'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = PL.LegNUM  
  ,[x_name ] = CT.TransportationVendor --(Crew Transportation)  
  ,[x_phone] = CT.PhoneNum  
  ,[x_fax] = CT.FaxNum  
  ,[x_rate] = CT.NegotiatedRate  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad 
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
 ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=11
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID    
  --LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID    
    
  LEFT OUTER JOIN (
	  SELECT PTL.LegID, [TransportationVendor]=PTL.PreflightTransportName, 
		[PhoneNum]=PTL.PhoneNum1, PTL.FaxNum, [NegotiatedRate]=PTL.PhoneNum4
	  FROM PreflightTransportList PTL  
	  INNER JOIN Transport T ON PTL.TransportID = T.TransportID  AND PTL.IsArrivalTransport=1
	  WHERE PTL.CrewPassengerType = 'C'  
  ) CT ON PL.LegID = CT.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @CREWTRANS=0 
    
  UNION ALL  
    
  SELECT --flag 'M' Addtnl Crew Hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'M'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = PL.LegNum  
  ,[m_name ] = ACH.Name   --Addtnl/Maintenance Crew Hotel  
  ,[m_phone] = ACH.PhoneNum  
  ,[m_fax] = ACH.FaxNum  
  ,[m_rate] = ACH.NegociatedRate  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = CrewCD
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=12
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
 LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  LEFT OUTER JOIN (     
   --SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.NegociatedRate,  
   --   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments     
   --FROM PreflightCrewList PCLL   
   --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
   --INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID 
   
	SELECT PPL.LegID, [Name] = PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,
	PPH.FaxNum,[NegociatedRate]=PPH.Rate, PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments ,C.CrewCD  
	FROM PreflightCrewList PPL   
	--INNER JOIN PreflightCrewHotelList PPH ON PPL.PreflightCrewListID = PPH.PreflightCrewListID 
	INNER JOIN PreflightHotelList PPH ON PPH.LegID = PPL.LegID AND PPH.crewPassengerType = 'C'
																AND PPH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PPH.PreflightHotelListID
														AND PHCP.CrewID = PPL.CrewID
	INNER JOIN Crew C ON C.CrewID=PPL.CrewID 
	INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID    
    WHERE PPL.DutyType NOT IN ('P', 'S')  
  ) ACH ON PL.legID = ACH.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @MAINHOTEL=0
    
  UNION ALL  
    
  SELECT --flag 'D'  Departure Catering  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'D'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = PL.LegNum  
  ,[d_name] = CD.CateringVendor  
  ,[d_phone] = CD.PhoneNum  
  ,[d_fax] = CD.FaxNum  
  ,[d_rate] = CD.NegotiatedRate  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID)--No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]='' 
  ,[RowID]=13 FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
 LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID    
  LEFT OUTER JOIN (
	  --SELECT PCL.LegID, C.CateringVendor, C.PhoneNum, C.FaxNum, C.NegotiatedRate  
	SELECT PCL.LegID, [CateringVendor]=PCL.ContactName, [PhoneNum]=PCL.ContactPhone, 
		[FaxNum]=PCL.ContactFax, [NegotiatedRate]=PCL.Cost, PCL.CateringComments,PCL.CateringConfirmation 
	FROM PreflightCateringDetail PCL   
	INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'  
  )CD ON PL.LegID = CD.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND @DEPCATER=0
  
 UNION ALL
 
 
SELECT --flag 'P' Passenger  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'P'
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = ISNULL(PPL.PassengerLastName,'') +CASE WHEN PPL.PassengerFirstName<>'' THEN ', '+PPL.PassengerFirstName ELSE '' END  + ' ' + ISNULL(PPL.PassengerMiddleName,'')  
  ,[p_phone] =P2.PhoneNum  
  ,[p_dept] = D.DepartmentCD  
  ,[p_cng] = P.IsEmployeeType  
  ,[p_legs] =FD.FlightPurposeCDS--FP.FlightPurposeCD  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = P2.PassengerRequestorCD   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=2
  FROM (SELECT DISTINCT * FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID   
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID AND Main.PassengerID = PPL.PassengerID  
 -- INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
   LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  INNER JOIN Passenger P2 ON PPL.PassengerID = P2.PassengerRequestorID  
  LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PPL.FlightPurposeID = FP.FlightPurposeID  
  LEFT OUTER JOIN (SELECT DepartmentID, DepartmentCD FROM Department) D ON P.DepartmentID = D.DepartmentID  
  
  INNER JOIN (SELECT DISTINCT  PP2.PASSENGERID, FlightPurposeCDS = (
				--SELECT ISNULL(CASE WHEN FP.FlightPurposeCD  IS NOT NULL THEN (CASE @PAXMANIF WHEN 2 THEN 'X  ' WHEN 3 THEN 'XX ' ELSE FP.FlightPurposeCD + ' ' END)END,CASE @PAXMANIF WHEN 1 THEN '$$ ***' ELSE '$$ ' END) + ''
				SELECT  FP.FlightPurposeCD + ' '
				FROM PreflightLeg PL LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID AND PP.PassengerID = PP2.PassengerID 
				LEFT OUTER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID AND PP.CustomerID = FP.CustomerID
				INNER JOIN @tblTripInfo PM ON PL.TripID=PM.TripID
				WHERE PL.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND PL.LegID=PM.LegID
				AND PM.PassengerID=PP2.PassengerID
				ORDER BY PL.LegNUM
				FOR XML PATH('')
			)
			FROM  PreflightLeg PL2
			LEFT JOIN  PreflightPassengerList PP2 ON PL2.LegID = PP2.LegID	
			INNER JOIN @tblTripInfo PM ON PL2.TripID=PM.TripID
          ) FD  ON FD.PassengerID=PPL.PassengerID
  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @PASSENG=0
  --  AND   
    
 UNION ALL
 
 SELECT --flag 'A' Arrival airport.alerts  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName 
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')     
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'A'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = CASE WHEN ISNULL (AA.Alerts,'')= '' THEN '' ELSE PL.LegNUM END  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=6
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  --INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
 LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND AA.Alerts IS NOT NULL 
    AND AA.Alerts <>''
    AND @AIRARRIV=0

UNION ALL

SELECT --flag 'I' Arrival Airport.generalnotes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD  
  --,[paxname] =P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')    
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'I'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = CASE WHEN ISNULL (AA.GeneralNotes, '') = '' THEN '' ELSE PL.LegNUM END  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=7
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID 
 LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID      
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
    AND @ANOTES=0 
    AND AA.GeneralNotes IS NOT NULL
    AND AA.GeneralNotes <>''
  
  UNION ALL
  
SELECT --flag 'O' Outbound Instructions  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = PPL.PassengerFirstName + ',' + PPL.PassengerMiddleName + ',' + PPL.PassengerLastName  
  --,[paxname] = P.FirstName + ',' + P.LastName + ' ' + P.MiddleInitial  
  ,[paxname] = CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'O'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = PL.LegNum  
  ,[o_flag] = PTOf.o_flag  
  ,[outdata1] = CASE WHEN PTO1.OutboundInstructionNUM = 1 THEN  PTO1.OutboundDescription END  
  ,[outdata2] = CASE WHEN PTO2.OutboundInstructionNUM = 2 THEN  PTO2.OutboundDescription END  
  ,[outdata3] = CASE WHEN PTO3.OutboundInstructionNUM = 3 THEN  PTO3.OutboundDescription END  
  ,[outdata4] = CASE WHEN PTO4.OutboundInstructionNUM = 4 THEN  PTO4.OutboundDescription END  
  ,[outdata5] = CASE WHEN PTO5.OutboundInstructionNUM = 5 THEN  PTO5.OutboundDescription END  
  ,[outdata6] = CASE WHEN PTO6.OutboundInstructionNUM = 6 THEN  PTO6.OutboundDescription END  
  ,[outdata7] = CASE WHEN PTO7.OutboundInstructionNUM = 7 THEN  PTO7.OutboundDescription END  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=14
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
 -- LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 1 AND OutboundDescription IS NOT NULL )PTO1  ON PL.LegID = PTO1.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 2 AND OutboundDescription IS NOT NULL)PTO2  ON PL.LegID = PTO2.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 3 AND OutboundDescription IS NOT NULL)PTO3  ON PL.LegID = PTO3.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 4 AND OutboundDescription IS NOT NULL)PTO4  ON PL.LegID = PTO4.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 5 AND OutboundDescription IS NOT NULL)PTO5  ON PL.LegID = PTO5.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 6 AND OutboundDescription IS NOT NULL)PTO6  ON PL.LegID = PTO6.LegID 
  LEFT OUTER JOIN (SELECT * FROM PreflightTripOutbound WHERE  OutboundInstructionNUM = 7 AND OutboundDescription IS NOT NULL)PTO7  ON PL.LegID = PTO7.LegID 
      
  LEFT OUTER JOIN (  
    SELECT TOP 1 LegID, [o_flag] = OutboundInstructionNUM FROM PreflightTripOutbound  
    ) PTOf ON PTOf.LegID = PTO1.LegID  
    
    
  LEFT OUTER JOIN Company C ON PM.HomebaseID = C.HomebaseID AND PM.CustomerID = C.CustomerID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND @OUTINST=0
    AND PL.OutbountInstruction IS NOT NULL
    AND PL.OutbountInstruction <>''
  UNION ALL
  
    
  SELECT DISTINCT--flag 'Z' PM.Notes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = P.PassengerRequestorCD
  --,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[paxname] =  CASE WHEN P.LastName IS NOT NULL THEN P.LastName + ', ' ELSE '' END + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
  ,[phone] = P.PhoneNum  
  ,[mainflag] = CASE WHEN ISNULL (PM.Notes,'')= '' THEN '' ELSE 2 END --IF PM.NOTES EXISTS  
  ,[flag] = 'Z'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 0  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL  
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  ,[RowID]=15
  
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
--  LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
LEFT JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID      
   LEFT JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID   
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
     AND PM.Notes IS NOT NULL 
     AND @TRIPALERTS=0
     AND PM.Notes <> ''
    
  )TripSummary
WHERE flag in ('L','P','C')

ORDER BY RowID,h_num,r_num,m_num,outdata1,f_num,f_comments DESC







END

--ORDER BY TripSummary.orig_nmbr,CASE f_num,f_comments DESC,outdata1


--EXEC spGetReportPRETSSummaryExportInformation 'ELIZA_9','3820','','','','','','','','','','','',''  
  



GO


