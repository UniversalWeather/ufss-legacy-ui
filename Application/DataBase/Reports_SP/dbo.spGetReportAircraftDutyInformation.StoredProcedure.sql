IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAircraftDutyInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAircraftDutyInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportAircraftDutyInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportAircraftDutyInformation
-- Author: SUDHAKAR J
-- Create date: 31 May 2012
-- Description: Get the Aircraft Duty information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 14 Jun 2012	Sudhakar J	1.1		Added parameters @CustomerID

-- =============================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT [CustomerID]
		  ,[AircraftDutyCD]
		  ,[AircraftDutyDescription]
		  ,C.ClientCD
		  ,[GraphColor]
		  ,[LastUpdUID]
		  ,[LastUpdTS]
		  ,[ForeGrndCustomColor]
		  ,[BackgroundCustomColor]
		  ,[IsCalendarEntry]
		  ,[DefaultStartTM]
		  ,[DefualtEndTM]
		  ,[IsAircraftStandby]
		  ,[IsDeleted]
	FROM [AircraftDuty] A
		LEFT OUTER JOIN (
			SELECT ClientID, ClientCD FROM Client
		) C ON A.ClientID = C.ClientID
	WHERE 
		IsDeleted = 0
		AND CustomerID = @CUSTOMERID
		AND ( A.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	
	ORDER BY AircraftDutyCD	
-- EXEC spGetReportAircraftDutyInformation 'TIM'

GO


