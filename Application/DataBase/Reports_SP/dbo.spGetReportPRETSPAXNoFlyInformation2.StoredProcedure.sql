IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPAXNoFlyInformation2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[spGetReportPRETSPAXNoFlyInformation2]
	@UserCD VARCHAR(50)--Mandatory
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum AS VARCHAR(50) = ''
	,@ReportHeaderID VARCHAR(30) = '' -- TemplateID from TripSheetReportHeader table
	,@TempSettings VARCHAR(MAX) = '' --'PAXMNORD::TRUE' PONEPERLIN::FALSE PAXORDER
	,@PassengerCD VARCHAR(30) = ''
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSPAXNoFlyInformation2
-- Author: AISHWARYA.M
-- Create date: 30 Aug 2012
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- =================================================================================

SET NOCOUNT ON

	-- TO ADD A DUMMY RECORD TO DISPLAY BLANK FORM BASED IN THE FORMAT SETTINGS
	----------------------------------------------------------------------------
		

		
		DECLARE @FormatSettings TABLE (
			LMESSAGE	BIT
		,	MESSAGEPOS	SMALLINT
		,	[MESSAGE]	VARCHAR(MAX)
		,	ONELEG	BIT
		,	LEGNOTES	SMALLINT
		,	LEGCREW	SMALLINT
		,	ETEHRS	SMALLINT
		,	PONEPERLIN	BIT
		,	BLANKLINES	BIT
		,	PCNG	BIT
		,	PBUSPERS	BIT
		,	PDEPTCODE	BIT
		,	PDEPTDESC	BIT
		,	CDNAME	BIT
		,	CDPHONE	BIT
		,	CDREMARKS	BIT
		,	PAXHOTEL	BIT
		,	HOTELPHONE	BIT
		,	HOTELREM	BIT
		,	PTANAME	BIT
		,	PTAPHONE	BIT
		,	PTAREMARKS	BIT
		,	CREWHOTEL	BIT
		,	CREWHPHONE	BIT
		,	CREWHADDR	BIT
		--,	MAINT	BIT
		--,	MAINTPHONE	BIT
		--,	MAINTADDR	BIT
		,	FILTFERRY	BIT
		,	SUPPAXNOTE	BIT
		,	SUPCRWNOTE	BIT
		,	DISPATCHNO	BIT
		,	PURPOSE	BIT
		,	AIRFLTPHN	BIT
		,	ALLOWSHADE	BIT
		,	ARRFBP	BIT
		,	PTACONFIRM	BIT
		,	PTACOMMENT	SMALLINT
		,	PTAFAX	BIT
		,	PTDNAME	BIT
		,	PTDPHONE	BIT
		,	PTDCONFIRM	BIT
		,	PTDCOMMENT	SMALLINT
		,	PTDREMARKS	BIT
		,	PTDFAX	BIT
		,	CTANAME	BIT
		,	CTAPHONE	BIT
		,	CTARATE	BIT
		,	CTACONFIRM	BIT
		,	CTACOMMENT	SMALLINT
		,	CTAREMARKS	BIT
		,	CTAFAX	BIT
		,	CTDNAME	BIT
		,	CTDPHONE	BIT
		,	CTDRATE	BIT
		,	CTDCONFIRM	BIT
		,	CTDCOMMENT	SMALLINT
		,	CTDREMARKS	BIT
		,	CTDFAX	BIT
		,	CDCONFIRM	BIT
		,	CDCOMMENT	SMALLINT
		,	CDFAX	BIT
		,	CANAME	BIT
		,	CAPHONE	BIT
		,	CACONFIRM	BIT
		,	CACOMMENT	SMALLINT
		,	CAREMARKS	BIT
		,	CAFAX	BIT
		,	HOTELROOMS	BIT
		,	HOTELCONF	BIT
		,	HOTELCOMM	SMALLINT
		,	HOTELFAX	BIT
		,	CREWHNRATE	BIT
		,	CREWHROOMS	BIT
		,	CREWHCONF	BIT
		,	CREWHCOMM	SMALLINT
		,	CREWHFAX	BIT
		,	CREWHREM	BIT
		,	OUTBOUND	BIT
		,	TERMTOAFBO	SMALLINT
		,	DEPFBP	BIT
		,	TERMTODFBO	SMALLINT
		--,	MAINTFAX	BIT
		--,	MAINTNRATE	BIT
		--,	MAINTROOMS	BIT
		--,	MAINTCONF	BIT
		--,	MAINTCOMM	SMALLINT
		--,	MAINTREM	BIT
		,	PAXNBR	BIT
		,	PRINTLOGO	BIT
		,	LINESEP	BIT
		,	REQUESTOR	BIT
		,	DEPICAO	BIT
		,	ARRICAO	BIT
		,	PRNFIRSTPG	BIT
		,	INTLFLTPHN	BIT
		,	CREWPILOT1	VARCHAR(100)
		,	CREWADDL	VARCHAR(100)
		,	CREWPILOT2	VARCHAR(100)
		,	FBOALPRICE	BIT
		,	FBOALDATE	BIT
		,	FBOACONT	BIT
		,	FBOAADDR	BIT
		,	FBOAREM	BIT
		,	FBOAPHONE2	BIT
		,	FBOAPAYTYP	BIT
		,	FBOAPOSTPR	BIT
		,	FBOAFUELQT	BIT
		,	FBOACONF	BIT
		,	FBOACOMM	SMALLINT
		,	FBODPHONE	BIT
		,	FBODFREQ	BIT
		,	FBODFAX	BIT
		,	FBODUVAIR	BIT
		,	FBODFUEL	BIT
		,	FBODNPRICE	BIT
		,	FBODLPRICE	BIT
		,	FBODLDATE	BIT
		,	FBODCONT	BIT
		,	FBODADDR	BIT
		,	FBODREM	BIT
		,	FBODPHONE2	BIT
		,	FBODPAYTYP	BIT
		,	FBODPOSTPR	BIT
		,	FBODFUELQT	BIT
		,	FBODCONF	BIT
		,	FBODCOMM	SMALLINT
		,	FBOAPHONE	BIT
		,	FBOAFREQ	BIT
		,	FBOAFAX	BIT
		,	FBOAUVAIR	BIT
		,	FBOAFUEL	BIT
		,	FBOANPRICE	BIT
		,	DISPATCHER	BIT
		,	DSPTEMAIL	BIT
		,	DSPTPHONE	BIT
		,	TRIPNO	BIT
		,	[DESC]	BIT
		,	TRIPDATES	BIT
		,	TMREQ	BIT
		,	TAILNMBR	BIT
		,	CONTPHONE	BIT
		,	TYPECODE	BIT
		,	CQDESC	SMALLINT
		,	REVISNNO	BIT
		,	REVISNDESC	BIT
		,	TIMEFORMAT	BIT
		,	CRMBLPHN	SMALLINT
		,	HOTELADDR	BIT
		,	PAXORDER	BIT
		,	CHRMONTH	BIT
		,	LEGNOTESF	SMALLINT
		,   LEGSEP      BIT
		,	CQNAME		SMALLINT
		,	CQCONTACT	SMALLINT
		,   DISPATCHIN SMALLINT
		,DCREWHTL BIT
		,DCREWHNRTE BIT
		,DCREWHADDR BIT
		,DCREWHCOMM SMALLINT
		,DCREWHCONF BIT
		,DCREWHFAX BIT
		,DCREWHROOM BIT
		,DCREWHPHON BIT
		,DCREWHREM BIT
		--,DMAINHOTEL BIT
		--,DMAINHADD BIT
		--,DMAINHCOMM SMALLINT
		--,DMAINHCON BIT
		--,DMAINHFAX BIT
		--,DMAINHRATE BIT
		--,DMAINHPHON BIT
		--,DMAINHREM BIT
		--,DMAINHROOM BIT
		,DPAXHOTEL BIT
		,DPAXHPHONE BIT 
		,DPAXHREM BIT
		,DPAXHROOMS BIT
		,DPAXHCONF BIT
		,DPAXHCOMM SMALLINT
		,DPAXHFAX BIT
		,DPAXHADDR BIT
		)
		INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 3, @TempSettings
		--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161675, 3, 'BLANKFORM::FALSE' 
		--SELECT * FROM @FormatSettings


		DECLARE	@PONEPERLIN BIT;
		SELECT @PONEPERLIN = PONEPERLIN FROM @FormatSettings
		---SELECT @PONEPERLIN
DECLARE @PAXORDER BIT,@BLANKLINES BIT;
SELECT @PAXORDER=PAXORDER FROM @FormatSettings	
SELECT  @BLANKLINES=BLANKLINES FROM @FormatSettings



	---------------------------------------------------------------------------	

    DECLARE @tblTripInfo AS TABLE (  
		RowID INT IDENTITY(1,1)
		, PassengerName VARCHAR(MAX) 
		, Code VARCHAR(30)
		, Cng CHAR(6)
		, Dept VARCHAR(30)
		, DepartmentDesc VARCHAR(50) 
		, FlightPurposeCode VARCHAR(50)
		, RowNum INT
		,LegID BIGINT
		,PassengerTotal INT
    ) 
 

 
	IF @PONEPERLIN = 0 BEGIN   
		INSERT INTO @tblTripInfo
		SELECT [PassengerName] = 
				CASE when P.LastName is not null then
				(P.LastName + ',' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') + '   ')
				when P.PassengerName is not null then
				P.PassengerName
				else
				(ISNULL(P.LastName,'') + ',' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') + '   ')
				end
			   ,[Code] = P.PassengerRequestorCD
			   ,[Cng] = P.IsEmployeeType
			   ,[Dept] = D.DepartmentCD 
			   ,[DepartmentDesc] = D.DepartmentName
			   ,[FlightPurposeCode] = FP.FlightPurposeCD --In Itinerary Report give field name As(Bus/Pers)
			   , 0
			   ,PL.LegID
			   ,PL.PassengerTotal
			   FROM PreflightMain PM  
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
				INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				LEFT OUTER JOIN Department D ON P.DepartmentID = D.DepartmentID	
				INNER JOIN FlightPurpose FP ON PP.FlightPurposeID = FP.FlightPurposeID	
				WHERE PM.TripID = CONVERT(BIGINT,@TripID)
				AND PP.IsDeleted=0
				AND (P.PassengerRequestorCD IN (SELECT DISTINCT S FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '' OR @PassengerCD is NULL)
				AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '' OR @LegNum is NULL)
				ORDER BY OrderNUM--(CASE (@PAXORDER) WHEN 0 THEN P.LastName ELSE  P.PassengerRequestorCD+','+CONVERT(VARCHAR(4),PL.LegNUM) END)
	            
	            
	           IF @PassengerCD = NULL OR @PassengerCD = ''
	           BEGIN 
				   DECLARE @RowVal INT=1,@Row INT =(SELECT PassengerTotal-COUNT(Code) FROM @tblTripInfo GROUP BY PassengerTotal )

				   WHILE @RowVal  <= @Row 
		           
				   BEGIN
		           
				   INSERT INTO @tblTripInfo(PassengerName)VALUES('BLOCKED')
				   SET @RowVal=@RowVal+1
		           
				   END
	           END
	               
	
	
	END ELSE BEGIN
		INSERT INTO @tblTripInfo (PassengerName, FlightPurposeCode) 
			SELECT Paxnames +' BLOCKED('+CONVERT(VARCHAR(6),Blk.Count)+')', '999' FROM (
			SELECT DISTINCT PP2.LegID,Paxnames = ( 
				SELECT ISNULL(P.LastName+',','')+' ' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'') + ' '-- ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.LastName,'') + '; '
				FROM PreflightMain PM 
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
				INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID
				WHERE PL.LegID = PP2.LegID
				AND PP.IsDeleted=0
				AND PM.TripID = CONVERT(BIGINT,@TripID)
				AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND (P.PassengerRequestorCD IN (SELECT DISTINCT S FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '' OR @PassengerCD is NULL)
				AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
				ORDER BY PP.OrderNUM--(CASE (@PAXORDER) WHEN 0 THEN P.LastName ELSE  P.PassengerRequestorCD END)
				FOR XML PATH('')
			)
			FROM  PreflightPassengerList PP2 
			) X 
			LEFT JOIN (SELECT PL.LegID,(PL.PassengerTotal - COUNT(PPL.PassengerID))Count FROM PreflightLeg PL INNER JOIN PreflightPassengerList PPL ON PL.LegID=PPL.LegID
			                    WHERE PL.TripID=@TripID
			                     AND PL.CustomerID= dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			                     AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			                     GROUP BY PL.LegID,PL.PassengerTotal
			                     HAVING (PL.PassengerTotal - COUNT(PPL.PassengerID)) > 0
			               )Blk ON Blk.LegID=X.LegID
	      WHERE Paxnames IS NOT NULL
			                     
			--SELECT * FROM @tblTripInfo
	END
	

DECLARE @COUNTER INT = 0;

	DECLARE @tblPONEPERLIN TABLE (PassengerName VARCHAR(250), Code VARCHAR(30)
		, Cng CHAR(6)
		, Dept VARCHAR(30)
		, DepartmentDesc VARCHAR(50) 
		, FlightPurposeCode VARCHAR(50)
		, RowNum INT)
    DECLARE @PerLine VARCHAR(10)='001'

    INSERT INTO @tblPONEPERLIN(PassengerName, FlightPurposeCode)
    SELECT PassengerName, FlightPurposeCode FROM  @tblTripInfo	

	-- ADD 10 EMPTY RECORDS WITH LINES
	 IF @BLANKLINES=0
	 
	 BEGIN
	 
	WHILE @COUNTER < 10 BEGIN 
		INSERT INTO @tblTripInfo (PassengerName, Dept, Cng, FlightPurposeCode, DepartmentDesc)
		VALUES ('______________________________', '______', '___', '______', '__________________________')
		
		SET @COUNTER = @COUNTER + 1;
	END	
	
	END
	
 
  IF @PONEPERLIN<>1 OR @BLANKLINES <>1 

	
 BEGIN

	  SET @COUNTER=0
	
	WHILE @COUNTER < 10 BEGIN 
	    
		INSERT INTO @tblPONEPERLIN (PassengerName, Dept, Cng, FlightPurposeCode, DepartmentDesc)
		VALUES (@PerLine+'______________________________', '______', '___', '______', '__________________________')
		
		SET @COUNTER = @COUNTER + 1;
		SET @PerLine=RIGHT(RTRIM(CONVERT(CHAR(5),'000'+CONVERT(CHAR(3),@PerLine+1))),3) 
	END	
	
	END
	




	IF @PONEPERLIN = 1 BEGIN   
		UPDATE @tblTripInfo SET RowNum = CASE WHEN RowID = 1 THEN 0 ELSE RowID - 1 END
	END ELSE BEGIN
		UPDATE @tblTripInfo SET RowNum = RowID
	END
	


IF @PONEPERLIN = 1  

BEGIN

SELECT * FROM @tblPONEPERLIN
END
ELSE
BEGIN

SELECT RIGHT(RTRIM(CONVERT(CHAR(5),'000'+CONVERT(CHAR(3),RowNum))),3) RowNum,
       RowID,PassengerName,Code,Cng,Dept,DepartmentDesc,FlightPurposeCode FROM @tblTripInfo	
END		

END 



--EXEC spGetReportPRETSPAXNoFlyInformation2 'ERICK_1', '1000158811',''




GO


