IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelPurchaseSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryExportInformation]                
 (                
  @UserCD AS VARCHAR(30),  --Mandatory                
  @DATEFROM DATETIME, --Mandatory                
  @DATETO DATETIME, --Mandatory                
  @TailNum VARCHAR(5000)='',--Optional                
  @FleetGroupCD VARCHAR(5000)='' --Optional                
 )                
-- =============================================                
-- Author: Mathes                
-- Create date: 31-07-2012                
-- Description: Fleet Fuel Purchase Summary - Report                 
-- =============================================                
AS                
 
SET NOCOUNT ON             
BEGIN     

 Declare @SuppressActivityAircraft BIT  
 DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SELECT @SuppressActivityAircraft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMERID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
         
  DECLARE @SQLSCRIPT AS NVARCHAR(Max) = '';            
  DECLARE @ParameterDefinition AS NVARCHAR(1000)          
  DECLARE @FltTailNum AS VARCHAR(7)
  DECLARE @RecCnt Int=0
  DECLARE @PrevCnt Int=0
  DECLARE @SQLSCRIPTSUB AS NVARCHAR(4000) = '';            
  Declare @TailNumber Varchar(10)
  Declare @ItmDesc Varchar(2000)
  Declare @ItmAmount Varchar(2000)
  Declare @Itm1 Varchar(150)
  Declare @Itm2 Varchar(150)
  Declare @Itm1All Varchar(1500)
  Declare @Itm2All Varchar(1000) 
  DECLARE @FltID as BigInt
 	DECLARE @AppDTFormat VARCHAR(25)
SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END
            
  DECLARE @TempAFS TABLE                
          ( tail_nmbr Varchar(9),
            ac_code Varchar(10),
            fuel_loc Char(4),  
            [desc] Varchar(32), 
            icao_id Char(4),   
            fbo_code Varchar(4),      
            purchasedt DateTime,              
            dispatchno Varchar(12),
            city Varchar(30),              
            name Varchar(30),              
            fuelqty Numeric(15,2), 
            fuelpurch Numeric(1,0),  
            fedtax Numeric(17,2),             
            statetax Numeric(17,2),              
            salestax Numeric(17,2), 
            autocalc Bit,
            unitprice Numeric(10,4),    
            expamt Numeric(17,2),
            ACCTLABEL varchar(2000),
            ACCTAMT varchar(2000),
            DTFORMAT varchar(20))
            
  IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
      DROP table #TmpFltTail    
      
  Create Table #TmpFltTail (FleetID BigInt,TailNum Varchar(10),AircraftCD char(12))  
  Declare @AircraftCD varchar(12) 
  
  --INSERT INTO #TmpFltTail
  --SELECT DISTINCT F.FleetID,F.TailNum 
  --FROM   Fleet F LEFT OUTER JOIN FleetGroupOrder FGO              
  --       ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID LEFT OUTER JOIN FleetGroup FG               
  --       ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
  --WHERE (TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '') AND   
  --      (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '') AND
  --       F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  --       And F.IsInActive<>1
  IF @TailNum ='' AND @FleetGroupCD =''
  Begin
   INSERT INTO #TmpFltTail SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD  FROM   Fleet F  
   WHERE F.IsInActive<>1 AND F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  End
  
  IF @TailNum <>''
  Begin
    INSERT INTO #TmpFltTail SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD FROM Fleet F
    WHERE TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  AND
    F.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
    And F.IsInActive<>1
  End 
   
  IF @FleetGroupCD <>''
  Begin
   INSERT INTO #TmpFltTail SELECT DISTINCT FLEET.FleetID, TailNum,AircraftCD  
   FROM  FleetGroupOrder INNER JOIN Fleet ON FleetGroupOrder.FleetID = Fleet.FleetID    
   INNER JOIN FleetGroup ON FleetGroup.FleetGroupID=FleetGroupOrder.FleetGroupID 
   WHERE FleetGroupOrder.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) AND
   FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) AND
   Fleet.FleetID Not IN(SELECT DISTINCT FleetID From #TmpFltTail)
  End  
         
  WHILE ( SELECT  COUNT(*) FROM #TmpFltTail ) > 0               
   BEGIN          
     Select Top 1 @FltID=FleetID,@FltTailNum=TailNum,@AircraftCD=AircraftCD From #TmpFltTail   
     Set @PrevCnt = (Select Count(*) From @TempAFS)          
     Insert Into @TempAFS SELECT  
            TailNum=(Select @FltTailNum),
            F.AircraftCD,
            FuelLocator.FuelLocatorCD,  
            FuelLocator.FuelLocatorDescription, 
            Airport.IcaoID, 
            FBO.FBOCD ,           
            PostflightExpense.PurchaseDT,              
            PostflightExpense.DispatchNUM,  
            Airport.CityName,          
            FBO.FBOVendor name,  
            DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD) FuelQTY,   
            IsNull(PostflightExpense.FuelPurchase,0),                      
            IsNull(PostflightExpense.FederalTAX,0),                     
            IsNull(PostflightExpense.SateTAX,0),                       
            IsNull(PostflightExpense.SaleTAX,0),  
            PostflightExpense.IsAutomaticCalculation,
           Case WHEN Company.FuelPurchase <> PostflightExpense.FuelPurchase  THEN  (PostflightExpense.FuelQTY*PostflightExpense.UnitPrice)/NULLIF(DBO.GetFuelConversion(PostflightExpense.FuelPurchase,PostflightExpense.FuelQTY,@UserCD),0) 
                                               ELSE PostflightExpense.UnitPrice End As UnitPrice,
            IsNull(PostflightExpense.ExpenseAMT,0),'','',
            @DATEFORMAT DTFORMAT
     FROM   PostflightExpense      
            LEFT OUTER JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID        
            INNER JOIN
              ( SELECT DISTINCT F.AircraftCD,F.FleetID,F.TailNum               
                FROM   Fleet F  
                LEFT OUTER JOIN FleetGroupOrder FGO              
                ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID              
                LEFT OUTER JOIN FleetGroup FG               
                ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID 
                Where F.FleetID=@FltID 
                AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')           
             ) F ON F.FleetID = PostflightExpense.FleetID   
            LEFT OUTER JOIN Airport ON PostflightExpense.AirportID = Airport.AirportID       
            LEFT OUTER JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID       
            LEFT OUTER JOIN Account ON PostflightExpense.AccountID = Account.AccountID 
            LEFT OUTER JOIN Company ON PostflightExpense.HomebaseID = Company.HomebaseID     
     WHERE  PostflightExpense.IsAutomaticCalculation=1 And PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And   
            CONVERT(DATE,PostflightExpense.PurchaseDT) BETWEEN CONVERT(DATE,'' + convert(varchar(20),@DATEFROM) +'' )AND CONVERT(DATE,''+convert(varchar(20),@DATETO)  +'')  
            AND PostflightExpense.IsDeleted=0       
    
      Set @RecCnt = (Select Count(*) From @TempAFS)
      
      IF @PrevCnt = @RecCnt
      Begin
       Insert Into @TempAFS(tail_nmbr,ac_code) Values (@FltTailNum,@AircraftCD)
      End
      Delete From #TmpFltTail Where TailNum=@FltTailNum 
    END        
    
  IF OBJECT_ID('tempdb..#TmpTail') is not null                        
       DROP table #TmpTail  

    Create Table #TmpTail(TailNum Varchar(10),AircraftCD Varchar(10))     
 
    INSERT INTO #TmpTail SELECT Distinct tail_nmbr,ac_code FROM @TempAFS
     WHILE ( SELECT  COUNT(*) FROM #TmpTail ) > 0             
     BEGIN        
       Select Top 1 @TailNumber=TailNum From #TmpTail  
         IF OBJECT_ID('tempdb..#TmpRpt') is not null                
          DROP table #TmpRpt       
         
        Create Table #TmpRpt(ItmDesc Varchar(2000),ItmAmt varchar(1000),ItmDate DateTime)            
                    
       --========================================= 
	SET @SQLSCRIPTSUB='Insert Into #TmpRpt Select DISTINCT Account.AccountDescription ,Sum(PostflightExpense.ExpenseAMT) As Total,           
		 PostflightExpense.PurchaseDT FROM  PostflightExpense        
		 INNER JOIN FuelLocator ON PostflightExpense.FuelLocatorID = FuelLocator.FuelLocatorID    
		 INNER JOIN Account ON PostflightExpense.AccountID = Account.AccountID          
		 INNER JOIN FLEET  ON Fleet.FleetID = PostflightExpense.FleetID     
		 LEFT OUTER JOIN Airport ON PostflightExpense.AirportID = Airport.AirportID         
		 LEFT OUTER JOIN FBO ON PostflightExpense.FBOID = FBO.FBOID   
		 WHERE PostflightExpense.ISDELETED=0    
		 GROUP By Account.AccountDescription, PostflightExpense.CustomerID,Fleet.TailNum ,PostflightExpense.PurchaseDT		 
		 HAVING PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And          
		 CONVERT(DATE,PostflightExpense.PurchaseDT) BETWEEN CONVERT(DATE,''' + convert(varchar(20),@DATEFROM) +''' )AND CONVERT(DATE,'''+convert(varchar(20),@DATETO)  +''')             
		 AND Fleet.TailNum=@TailNumber Order By PostflightExpense.PurchaseDT
		 
		  '
	    
	    SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @DATEFROM AS DATETIME, @DATETO AS DATETIME, @TailNumber AS VARCHAR(10)'                
        EXECUTE sp_executesql @SQLSCRIPTSUB, @ParameterDefinition, @UserCD, @DATEFROM, @DATETO, @TailNumber
	    SET @Itm1All ='' 
	    SET @Itm2All =''  
	     WHILE ( SELECT  COUNT(*) FROM #TmpRpt  ) > 0             
         BEGIN        
          Select Top 1 @Itm1=ItmDesc,@Itm2=ItmAmt From #TmpRpt  
	      
	      IF @Itm1All =''
	         BEGIN
	          SET @Itm1All = @Itm1
	         END
	      ELSE
	         BEGIN
	          SET @Itm1All = @Itm1All + ' </BR> ' + @Itm1
	         END
	      
	      IF @Itm2All =''
	         BEGIN
	          SET @Itm2All = @Itm2
	         END
	      ELSE
	         BEGIN
	          SET @Itm2All = @Itm2All + ' </BR>' + @Itm2
	         END
	      Delete From #TmpRpt Where ItmDesc=@Itm1
	     END
	     
	    --==============================================
	    Update @TempAFS Set ACCTLABEL=@Itm1All Where tail_nmbr=@TailNumber 
	    Update @TempAFS Set ACCTAMT=@Itm2All Where tail_nmbr=@TailNumber 
	    
	    Delete From #TmpTail Where TailNum=@TailNumber 
	  END 	   


IF @SuppressActivityAircraft=1
BEGIN
DELETE from @TempAFS WHERE icao_id IS NULL
END




Select * from @TempAFS Order By tail_nmbr, purchasedt 


    
    
--IF OBJECT_ID('tempdb..#TmpFltTail') is not null                        
--DROP table #TmpFltTail

--IF OBJECT_ID('tempdb..#TmpTail') is not null                        
--       DROP table #TmpTail
       
 --IF OBJECT_ID('tempdb..#TmpRpt') is not null                
 --         DROP table #TmpRpt      
    -- Exec spGetReportPOSTFleetFuelPurchaseSummaryExportInformation 'supervisor_99','01-jan-2000','31-dec-2033','' ,''                   
END 
GO


