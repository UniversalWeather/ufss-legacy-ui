IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItineraryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItineraryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRECorporateItineraryInformation]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TripID AS VARCHAR(30),   --Mandatory
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
		As
-- ==========================================================================================
-- SPC Name: spGetReportPRECorporateItineraryInformation
-- Author: ABHISHEK.S
-- Create date: 27th August 2012
-- Description: Get Corporate Itinerary Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
-- Added [TripID]
-- 
--[TripDates] = CONVERT(varchar, PL.DepartureDTTMLocal, 22) + '' '' + CONVERT(varchar, PL.ArrivalDTTMLocal, 22) ,	
		--SET @SQLSCRIPT = '
			SELECT distinct
			[TripID] = PM.TripID,
			[LegID]	= PL.LegID,
			[LegNum] = PL.LegNUM,
			[Notes] = PL.Notes,
			[ETD1]=PL.DepartureDTTMLocal,
			[ETD2]=A.AirportName,
			[ETD3]=A.CityName,
			[ETA1]=PL.ArrivalDTTMLocal,
			[ETA2]=AA.AirportName,
			[ETA3]=AA.CityName,		
			[ETE1] = PL.ElapseTM,
			[ETE2] = FBD.FBOVendor,
			[ETE3] = FBD.PhoneNum1,
			[Passengers] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),
		    (SELECT DISTINCT Company.TimeDisplayTenMin FROM Company WHERE CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)) AS TenToMin
		FROM PreflightMain PM 
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
	    INNER JOIN Airport A ON PL.DepartICAOID = A.AirportID 
	    INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	    INNER JOIN (
				SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.HomeBaseID, F.AircraftCD
				FROM Fleet F 
				LEFT OUTER JOIN FleetGroupOrder FGO
				ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
				LEFT OUTER JOIN FleetGroup FG 
				ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
							OR @FleetGroupCD = '' ) 
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	 
		LEFT OUTER JOIN (
			SELECT DISTINCT PP2.LegID, Paxnames = ( 
				SELECT   ISNULL(P.LastName,'')+ ', ' + ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.MiddleInitial,'')+';'
				FROM PreflightPassengerList PP1 INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID
				WHERE PP1.LegID = PP2.LegID AND PP1.FlightPurposeID is Not NULL
				ORDER BY P.LastName,P.FirstName
				FOR XML PATH('')
			)
			FROM  PreflightPassengerList PP2
        ) PAX ON PL.LegID = PAX.LegID 		    
	    LEFT OUTER JOIN (
				SELECT PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		AND PM.TripID = convert(Bigint,@TripID)
		 AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		 AND PM.IsDeleted = 0
	
	--Construct OPTIONAL clauses
			
	--IF @TailNUM <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';						
	--END
	
	--IF @TripNUM <> '' BEGIN  
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripNUM IN (' + CASE WHEN RIGHT(@TripNUM, 1) = ',' THEN LEFT(@TripNUM, LEN(@TripNUM) - 1) ELSE @TripNUM END + ')';
	--END		
	--	SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY PAX.LegID'

	
	--SET @ParameterDefinition =  '@UserCD AS VARCHAR(30),@DATEFROM AS DATETIME, @DATETO AS DATETIME, @FleetGroupCD AS NVARCHAR(500) '
 --  EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	, @DATEFROM, @DATETO, @FleetGroupCD 
   

    
    -- EXEC spGetReportPRECorporateItineraryInformation 'JWILLIAMS_13','2012-09-01','2012-09-09', '', ''
   --  EXEC spGetReportPRECorporateItineraryInformation 'JWILLIAMS_11','100111827',''



GO


