IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendar1SubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1SubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1SubInformation]
--@TripID VARCHAR(30)-- MANDATORY  
	@ActiveDate DATETIME --MANDATORY
	,@FleetID VARCHAR(30)
AS  

  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPREWeeklyCalendar1SubInformation  
-- Author: Sudhakar J  
-- Create date: 23 Jul 2012  
-- Description: Get Preflight Weekly Calendar1 information for Main Report  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
SET NOCOUNT ON  
  
--DECLARE @FleetID BIGINT = 10000;  
--DECLARE @ActiveDate DATETIME = '2012-07-20';  

 DECLARE @TableTailInfo TABLE (  
   RowID INT IDENTITY(1,1) NOT NULL  
  ,Loc VARCHAR(30)  
  ,Arr DATETIME  
  ,Dep DATETIME
  ,ArrZulu DATETIME
  ,DepZulu DATETIME  
  ,Pax INT 
  --,CrewCode VARCHAR(50)  
  ,TripNum BIGINT  
  --,RequestorCD VARCHAR(5)  
  ,ArrtimeApprox BIT -- TO DISPLAY '?' SYMBOL BESIDES Arrival Time  
  ,DeptimeApprox BIT
  ,DutyType CHAR(2)
  ,TripDesc VARCHAR(40)
  ,RecordType CHAR(1)  -- TO DISPLAY '?' SYMBOL BESIDES Departure Time  
  ,CrewCode VARCHAR(300)  
  ,RequestorCD VARCHAR(5)  
 )  
  
  
  
 DECLARE @LocA VARCHAR(4), @LocD VARCHAR(4), @Arr DATETIME, @Dep DATETIME, @ArrZulu DATETIME, @DepZulu DATETIME, @LegNum  BIGINT, 
  @Pax INT, @CrewCode VARCHAR(500), @TripNum BIGINT, @IsApproxTM BIT, @CrTripDescription VARCHAR(40),@CrDutyType CHAR(2);  
 DECLARE @PrevRowID INT,@CrRecordType  CHAR(1),@NoOfRows INT;  
 DECLARE @PrevLoc VARCHAR(4) = '',@CrCrewCode VARCHAR(300),@CrPassengerRequestorCD  VARCHAR(5),@CRTripNum BIGINT;
  
  
  
 DECLARE curTailInfo CURSOR FAST_FORWARD READ_ONLY FOR  
  SELECT TEMP.* FROM ( SELECT  DISTINCT    
    LocA = AA.IcaoID  
    ,LocD = AD.IcaoID  
    ,Arr= PL.ArrivalDTTMLocal  
    ,Dep = PL.DepartureDTTMLocal 
    ,ArrZulu = PL.ArrivalGreenwichDTTM
    ,DepZulu = PL.DepartureGreenwichDTTM 
    ,Pax = PL.PassengerTotal  
    --,CrewCode = C.CrewList  
    ,TripNumber = PM.TripNUM    
    ,IsApproxTM = ISNULL(PL.IsApproxTM, 0) 
    ,PL.DutyType
    ,PM.TripDescription 
    ,PM.RecordType 
    ,PL.LegNum
    ,P.PassengerRequestorCD 
    , C.CrewList   
    ,PM.TripNum
      
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID  
   INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID     
   INNER JOIN (SELECT CustomerID, FleetID, TailNum, HomebaseID FROM Fleet)F ON PM.FleetID = F.FleetID  
   INNER JOIN Company CO ON CO.HomebaseID = F.HomebaseID  
   LEFT OUTER JOIN (  
		   SELECT DISTINCT PC2.tripid, CrewList = (  
			 SELECT distinct C.CrewCD + ' '  
			 FROM PreflightCrewList PC1 
			 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
			 WHERE Pl.tripid = PC2.tripid  
			 FOR XML PATH('')  
			 )           
			FROM PreflightLeg PC2  
	   ) C ON PM.tripid = C.tripid  
    LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PL.PassengerRequestorID   
   WHERE PM.CustomerID = F.CustomerID  
    AND F.FleetID = PM.FleetID
    AND PM.FLEETID=@FleetID
    --AND PM.TRIPID = @TripID
    AND PM.TripStatus IN('T','H')
    AND ISNULL(PL.DutyType,'')<>'MX'
    --AND CONVERT(DATE, PM.EstDepartureDT, 101) =  CONVERT(DATE, @ActiveDate, 101)  
    --Requirement says to compare with PM.EstDepartureDT which seems not correct in this scenario  
   	AND  CONVERT(DATE,@ActiveDate)=CONVERT(DATE, PL.DepartureDTTMLocal) 
   	
   UNION ALL
   
    SELECT DISTINCT      
    LocA = AA.IcaoID  
    ,LocD = AD.IcaoID  
    ,Arr= PL.ArrivalDTTMLocal  
    ,Dep = PL.DepartureDTTMLocal
    ,ArrZulu = PL.ArrivalGreenwichDTTM
    ,DepZulu = PL.DepartureGreenwichDTTM   
    ,Pax = PL.PassengerTotal  
    --,CrewCode = C.CrewList  
    ,TripNumber = PM.TripNUM    
    ,IsApproxTM = ISNULL(PL.IsApproxTM, 0) 
    ,PL.DutyType
    ,PM.TripDescription 
    ,PM.RecordType 
    ,PL.LegNum
    ,NULL
    ,NULL
    ,PM.TripNum
    --,RequestorCD = P.PassengerRequestorCD  
      
   FROM PreflightMain PM  
   INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  
   INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID  
   INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID     
   INNER JOIN (SELECT CustomerID, FleetID, TailNum, HomebaseID FROM Fleet)F ON PM.FleetID = F.FleetID  
   INNER JOIN Company CO ON CO.HomebaseID = F.HomebaseID  
   --LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID  
   WHERE PM.CustomerID = F.CustomerID  
    AND F.FleetID = PM.FleetID
    AND PM.FLEETID=@FleetID
    --AND PM.TRIPID = @TripID
    AND PL.DutyType='MX'
    --AND CONVERT(DATE, PM.EstDepartureDT, 101) =  CONVERT(DATE, @ActiveDate, 101)  
    --Requirement says to compare with PM.EstDepartureDT which seems not correct in this scenario  
   	AND  (CONVERT(DATE,@ActiveDate) BETWEEN CONVERT(DATE, PL.DepartureDTTMLocal) AND  CONVERT(DATE, PL.ArrivalDTTMLocal)))TEMP
   	 ORDER BY TEMP.TripNum,TEMP.LegNum  
     
 OPEN curTailInfo;  
  FETCH NEXT FROM curTailInfo INTO @LocA, @LocD, @Arr, @Dep, @ArrZulu, @DepZulu, @Pax, @TripNum, @IsApproxTM,@CrDutyType,@CrTripDescription,@CrRecordType,@LegNum,@CrPassengerRequestorCD,@CrCrewCode,@CRTripNum;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
  
   IF @CrRecordType='M' AND CONVERT(DATE,@Dep)<>CONVERT(DATE,@ActiveDate)
    BEGIN SET @Dep=CONVERT(DATETIME, CONVERT(VARCHAR(10),@ActiveDate, 101) + ' 00:00')  END
  
    
   IF @CrRecordType='M' AND CONVERT(DATE,@Arr)<>CONVERT(DATE,@ActiveDate)
    BEGIN  SET @Arr=CONVERT(DATETIME, CONVERT(VARCHAR(10),@ActiveDate, 101) + ' 23:59')  END
  
   IF @PrevLoc = '' BEGIN  
    -- INSERT FOR DEPARTURE  
    --INSERT INTO @TableTailInfo (Loc, Arr, Dep, Pax, CrewCode, TripNum, RequestorCD, ArrtimeApprox, DeptimeApprox)  
     IF @CrRecordType='M'
     BEGIN
         INSERT INTO @TableTailInfo (Loc, Arr, ArrZulu, DepZulu, Dep, Pax, TripNum, ArrtimeApprox, DeptimeApprox,DutyType,TripDesc,RecordType,CrewCode,RequestorCD)  
     VALUES (@LocD, @Arr, @Dep, @ArrZulu, @DepZulu, @Pax, @TripNum, 0, @IsApproxTM,@CrDutyType,@CrTripDescription,@CrRecordType,@CrCrewCode,@CrPassengerRequestorCD) 
     END
     ELSE
     BEGIN
    INSERT INTO @TableTailInfo (Loc, Arr,  Dep,ArrZulu, DepZulu,  Pax, TripNum, ArrtimeApprox, DeptimeApprox,DutyType,TripDesc,RecordType,CrewCode,RequestorCD)  
     VALUES (@LocD, NULL, @Dep, NULL, @DepZulu, @Pax, @TripNum, 0, @IsApproxTM,@CrDutyType,@CrTripDescription,@CrRecordType,@CrCrewCode,@CrPassengerRequestorCD)  
   END    
END  
   ELSE BEGIN  
    UPDATE @TableTailInfo SET Dep = @Dep, DeptimeApprox = @IsApproxTM,Pax=@Pax 
    WHERE RowID = @PrevRowID AND Loc = @PrevLoc  
   END  
     
   
    
   -- INSERT FOR ARRIVAL  
    IF @CrRecordType<>'M'
    BEGIN 
   INSERT INTO @TableTailInfo (Loc, Arr, Dep,  ArrZulu, DepZulu,  Pax, TripNum, ArrtimeApprox, DeptimeApprox,DutyType,TripDesc,RecordType,CrewCode,RequestorCD)  
    VALUES (@LocA, @Arr,  NULL,  @ArrZulu, NULL,NULL,@TripNum, @IsApproxTM, 0,@CrDutyType,@CrTripDescription,@CrRecordType,@CrCrewCode,@CrPassengerRequestorCD)  
  END
  
   SELECT @PrevRowID = SCOPE_IDENTITY()  
   SET @PrevLoc = @LocA  
  
   FETCH NEXT FROM curTailInfo INTO @LocA, @LocD, @Arr, @Dep, @ArrZulu, @DepZulu, @Pax, @TripNum, @IsApproxTM,@CrDutyType,@CrTripDescription,@CrRecordType,@LegNum,@CrPassengerRequestorCD,@CrCrewCode,@CRTripNum;  
  END  
 SET @NoOfRows = @@CURSOR_ROWS;   
 CLOSE curTailInfo;  
 DEALLOCATE curTailInfo;   
 
 
   
 IF @NoOfRows > 0 BEGIN  
  SELECT T.RowID,T.Loc,T.Arr,T.Dep,T.ArrZulu,T.DepZulu,T.Pax, T.TripNum,T.ArrtimeApprox,T.DeptimeApprox,
         CASE(T.DutyType) WHEN 'MX' THEN T.DutyType ELSE '' END DutyType,CASE(T.DutyType) WHEN 'MX' THEN T.TripDesc ELSE '' END TripDesc
         ,RecordType,CrewCode,RequestorCD
       FROM @TableTailInfo T  
 END  
 ELSE BEGIN  
	  SELECT DISTINCT 1 as RowID, A.ICAOID as Loc, NULL as Arr, NULL as Dep, NULL AS ArrZulu, NULL AS DepZulu, NULL as Pax,   
	   CR.TripNUM as TripNum, 0 as ArrtimeApprox, 0 AS DeptimeApprox,'' DutyType,'' TripDesc,'' RecordType,CR.CrewCode CrewCode,CR.RequestorCD RequestorCD
	  FROM 
	Fleet F   
      LEFT OUTER JOIN(  SELECT      
							CrewCode = C.CrewList  
							,TripID = CONVERT(VARCHAR(30),PM.TripID)
							,RequestorCD = P.PassengerRequestorCD 
							,TripNUM = CONVERT(VARCHAR(30),PM.TripNUM) 
							,[RecordType]=PM.RecordType,PM.FleetID
						      
						   FROM PreflightMain PM  
						   LEFT OUTER JOIN (  
							   SELECT DISTINCT PC2.tripid, CrewList = (  
								 SELECT distinct C.CrewCD + ' '  
								 FROM PreflightCrewList PC1 
								 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
								 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
								 WHERE Pl.tripid = PC2.tripid  
								 FOR XML PATH('')  
								 )           
								FROM PreflightLeg PC2  
						   ) C ON PM.tripid = C.tripid  
						   INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
						   LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PL.PassengerRequestorID  
						   WHERE PM.FleetID = CONVERT(BIGINT, @FleetID)
						  AND PL.LegID IN ( SELECT LegID FROM PreflightLeg 
							WHERE  CONVERT(DATE, @ActiveDate) >= CONVERT(DATE,ArrivalDTTMLocal) 
							AND CONVERT(DATE, @ActiveDate)< CONVERT(DATE,NextLocalDTTM)
							  AND PL.LegID NOT IN(SELECT LegID FROM PreflightLeg WHERE CONVERT(DATE, DepartureDTTMLocal, 101) =  CONVERT(DATE, @ActiveDate, 101) )
							) )CR ON CR.FleetID=F.FleetID
	  OUTER APPLY dbo.GetFleetCurrentAirpotCD(@ActiveDate, @FleetID) AS A
	  WHERE F.FleetID =  CONVERT(BIGINT,@FleetID)	

	

 END  
END 
 --EXEC spGetReportPREWeeklyCalendar1SubInformation  '2012-09-06', '10099152856' 
 
 --SELECT * FROM PreflightLeg WHERE TripID=1896 
 





GO


