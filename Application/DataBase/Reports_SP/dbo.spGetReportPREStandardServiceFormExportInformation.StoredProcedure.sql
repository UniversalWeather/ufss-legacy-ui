IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREStandardServiceFormExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREStandardServiceFormExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREStandardServiceFormExportInformation]
		@UserCD AS VARCHAR(30)  --Mandatory
		,@UserCustomerID AS VARCHAR(20) --Mandatory
		,@TripNUM VARCHAR(10) --Mandatory
		,@LegNUM VARCHAR(100)
		
AS
-- ===============================================================================
-- SPC Name: spGetReportPREStandardServiceFormExportInformation
-- Author: Aishwarya.M
-- Create date: 24 Dec 2013
-- Description: Get Standard Service form Export Information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- ================================================================================

SET NOCOUNT ON;

BEGIN

SELECT [name] = CO.CompanyName
		,[orig_nmbr] = PM.TripNUM
		,[leg_num] = PL.LegNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = AC.AircraftCD
		,[pic] = PCC.LastName
		,[arrival_day] = DATENAME(WEEKDAY,PL.ArrivalDTTMLocal)
		,[arr_date] = PL.ArrivalDTTMLocal
		,[loc_arreta] = SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalDTTMLocal,113),13,5) 
		,[zulu_arreta] = SUBSTRING(CONVERT(VARCHAR(30),PL.ArrivalGreenwichDTTM,113),13,5)
		,[depicao_id] = DA.IcaoID
		,[departure_day] = DATENAME(WEEKDAY,PL.DepartureDTTMLocal)
		,[dep_date] = PL.DepartureDTTMLocal
		,[loc_depeta] = SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5)
		,[zulu_depeta] = SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureGreenwichDTTM,113),13,5)
		,[arricao_id] = AA.IcaoID
		,[arr_city] = AA.CityName
		,[arr_airport] = AA.AirportName
		,[arr_fbo] = FBA.PreflightFBOName
		,[freq] = FBA.Frequency
		,[fbo_phone] = FBA.PhoneNUM1
		,[fbo_fax] = FBA.FaxNum
		,[pic_full] = PCC.Crew
		,[sic_full] = SCC.Crew
		,[numcrew] = CrewCnt.Crewcount
		,[numpax] = PL.PassengerTotal
		

FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN Fleet F ON PM.FleetID = F.FleetID
	LEFT OUTER JOIN Aircraft AC ON F.AircraftID = AC.AircraftID
	LEFT OUTER JOIN Airport DA ON PL.DepartICAOID = DA.AirportID
	LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID
	LEFT OUTER JOIN Company CO ON CO.HomebaseID = PM.HomebaseID
	LEFT OUTER JOIN
				(SELECT PF.PreflightFBOName, FB.PhoneNUM1, FB.FaxNum, FB.Frequency, PF.LegID
					FROM FBO FB
					INNER JOIN PreflightFBOList PF ON PF.FBOID = FB.FBOID AND PF.IsArrivalFBO = 1
				) FBA ON FBA.LegID = PL.LegID
	LEFT OUTER JOIN (SELECT COUNT(CrewID) Crewcount,LegID
						FROM PreflightCrewList  
						WHERE CustomerID = @UserCustomerID 
						GROUP BY LegID)CrewCnt ON CrewCnt.LegID = PL.LegID
	LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID
						 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
						 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=@UserCustomerID)PCC ON PCC.LegID=PL.LegID AND Rnk=1
	LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID
						 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
						 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=@UserCustomerID)SCC ON SCC.LegID=PL.LegID AND SCC.Rnk=1
		WHERE PM.CustomerID=@UserCustomerID
		AND PM.TripNUM=CONVERT(BIGINT,@TripNUM) 
		AND PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ','))
		AND PM.IsDeleted = 0
	
	--EXEC spGetReportPREStandardServiceFormExportInformation 'SUPERVISOR_99','10099','2021',''
		
END

GO


