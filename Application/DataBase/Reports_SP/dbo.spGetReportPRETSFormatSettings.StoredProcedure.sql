IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFormatSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFormatSettings]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSFormatSettings](
	@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table
	,@ReportNumber SMALLINT -- 1 = 'OVERVIEW', 2 = 'TRIPSHEET' etc
	,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'
) 
AS
BEGIN

IF OBJECT_ID('TEMPDB..#tblTSSettings') IS NOT NULL 
DROP TABLE #tblTSSettings

	--DECLARE @tblTSSettings AS TABLE  (
	CREATE TABLE #tblTSSettings (
		RecID INT NOT NULL IDENTITY (1,1)
		,TripSheetReportHeaderID BIGINT
		,TripSheetReportDetailID BIGINT
		,ReportNumber SMALLINT
		,ReportDescription VARCHAR(100)
		,ParameterCD VARCHAR(100) 
		,ParameterType VARCHAR(20) 
		,ParameterCValue VARCHAR(20)
		,ParameterLValue BIT
		,ParameterNValue INT
		,ParameterValue VARCHAR(MAX)
	)

	--INSERT INTO #tblTSSettings (
	--	TripSheetReportHeaderID
	--	,TripSheetReportDetailID
	--	,ReportNumber
	--	,ReportDescription
	--	,ParameterCD
	--	,ParameterType
	--	,ParameterCValue
	--	,ParameterLValue
	--	,ParameterNValue
	--	,ParameterValue
	--) 
	--SELECT TripSheetReportHeaderID, TripSheetReportDetailID, 
	--ReportNumber, ReportDescription, [ParameterCD] = ParameterVariable, 
	--ParameterType, ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	--FROM TripSheetReportDetail
	--WHERE TripSheetReportHeaderID = CONVERT(BIGINT,@ReportHeaderID)
	--AND ReportNumber = @ReportNumber
	--AND IsDeleted=0

IF @ReportNumber=3
BEGIN
	INSERT INTO #tblTSSettings (
		TripSheetReportHeaderID
		,TripSheetReportDetailID
		,ReportNumber
		,ReportDescription
		,ParameterCD
		,ParameterType
		,ParameterCValue
		,ParameterLValue
		,ParameterNValue
		,ParameterValue
	) 
	SELECT TripSheetReportHeaderID, TripSheetReportDetailID, 
	ReportNumber, ReportDescription, [ParameterCD] = ParameterVariable, 
	ParameterType, ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	FROM TripSheetReportDetail
	WHERE TripSheetReportHeaderID = CONVERT(BIGINT,@ReportHeaderID)
	AND ReportNumber = @ReportNumber
	AND IsDeleted=0
	ORDER BY (CASE WHEN ParameterVariable='LMESSAGE'  THEN  1
						WHEN ParameterVariable='MESSAGEPOS' THEN  2
						WHEN ParameterVariable='MESSAGE' THEN  3
						WHEN ParameterVariable='ONELEG' THEN  4
						WHEN ParameterVariable='LEGNOTES' THEN  5
						WHEN ParameterVariable='LEGCREW' THEN  6
						WHEN ParameterVariable='ETEHRS' THEN  7
						WHEN ParameterVariable='PONEPERLIN' THEN  8
						WHEN ParameterVariable='BLANKLINES' THEN  9
						WHEN ParameterVariable='PCNG' THEN  10
						WHEN ParameterVariable='PBUSPERS' THEN  11
						WHEN ParameterVariable='PDEPTCODE' THEN  12
						WHEN ParameterVariable='PDEPTDESC' THEN  13
						WHEN ParameterVariable='CDNAME' THEN  14
						WHEN ParameterVariable='CDPHONE' THEN  15
						WHEN ParameterVariable='CDREMARKS' THEN  16
						WHEN ParameterVariable='PAXHOTEL' THEN  17
						WHEN ParameterVariable='HOTELPHONE' THEN  18
						WHEN ParameterVariable='HOTELREM' THEN  19
						WHEN ParameterVariable='PTANAME' THEN  20
						WHEN ParameterVariable='PTAPHONE' THEN  21
						WHEN ParameterVariable='PTAREMARKS' THEN  22
						WHEN ParameterVariable='CREWHOTEL' THEN  23
						WHEN ParameterVariable='CREWHPHONE' THEN  24
						WHEN ParameterVariable='CREWHADDR' THEN  25
						WHEN ParameterVariable='MAINT' THEN  26
						WHEN ParameterVariable='MAINTPHONE' THEN  27
						WHEN ParameterVariable='MAINTADDR' THEN  28
						WHEN ParameterVariable='FILTFERRY' THEN  29
						WHEN ParameterVariable='SUPPAXNOTE' THEN  30
						WHEN ParameterVariable='SUPCRWNOTE' THEN  31
						WHEN ParameterVariable='DISPATCHNO' THEN  32
						WHEN ParameterVariable='PURPOSE' THEN  33
						WHEN ParameterVariable='AIRFLTPHN' THEN  34
						WHEN ParameterVariable='ALLOWSHADE' THEN  35
						WHEN ParameterVariable='ARRFBP' THEN  36
						WHEN ParameterVariable='PTACONFIRM' THEN  37
						WHEN ParameterVariable='PTACOMMENT' THEN  38
						WHEN ParameterVariable='PTAFAX' THEN  39
						WHEN ParameterVariable='PTDNAME' THEN  40
						WHEN ParameterVariable='PTDPHONE' THEN  41
						WHEN ParameterVariable='PTDCONFIRM' THEN  42
						WHEN ParameterVariable='PTDCOMMENT' THEN  43
						WHEN ParameterVariable='PTDREMARKS' THEN  44
						WHEN ParameterVariable='PTDFAX' THEN  45
						WHEN ParameterVariable='CTANAME' THEN  46
						WHEN ParameterVariable='CTAPHONE' THEN  47
						WHEN ParameterVariable='CTARATE' THEN  48
						WHEN ParameterVariable='CTACONFIRM' THEN  49
						WHEN ParameterVariable='CTACOMMENT' THEN  50
						WHEN ParameterVariable='CTAREMARKS' THEN  51
						WHEN ParameterVariable='CTAFAX' THEN  52
						WHEN ParameterVariable='CTDNAME' THEN  53
						WHEN ParameterVariable='CTDPHONE' THEN  54
						WHEN ParameterVariable='CTDRATE' THEN  55
						WHEN ParameterVariable='CTDCONFIRM' THEN  56
						WHEN ParameterVariable='CTDCOMMENT' THEN  57
						WHEN ParameterVariable='CTDREMARKS' THEN  58
						WHEN ParameterVariable='CTDFAX' THEN  59
						WHEN ParameterVariable='CDCONFIRM' THEN  60
						WHEN ParameterVariable='CDCOMMENT' THEN  61
						WHEN ParameterVariable='CDFAX' THEN  62
						WHEN ParameterVariable='CANAME' THEN  63
						WHEN ParameterVariable='CAPHONE' THEN  64
						WHEN ParameterVariable='CACONFIRM' THEN  65
						WHEN ParameterVariable='CACOMMENT' THEN  66
						WHEN ParameterVariable='CAREMARKS' THEN  67
						WHEN ParameterVariable='CAFAX' THEN  68
						WHEN ParameterVariable='HOTELROOMS' THEN  69
						WHEN ParameterVariable='HOTELCONF' THEN  70
						WHEN ParameterVariable='HOTELCOMM' THEN  71
						WHEN ParameterVariable='HOTELFAX' THEN  72
						WHEN ParameterVariable='CREWHNRATE' THEN  73
						WHEN ParameterVariable='CREWHROOMS' THEN  74
						WHEN ParameterVariable='CREWHCONF' THEN  75
						WHEN ParameterVariable='CREWHCOMM' THEN  76
						WHEN ParameterVariable='CREWHFAX' THEN  77
						WHEN ParameterVariable='CREWHREM' THEN  78
						WHEN ParameterVariable='OUTBOUND' THEN  79
						WHEN ParameterVariable='TERMTOAFBO' THEN  80
						WHEN ParameterVariable='DEPFBP' THEN  81
						WHEN ParameterVariable='TERMTODFBO' THEN  82
						WHEN ParameterVariable='MAINTFAX' THEN  83
						WHEN ParameterVariable='MAINTNRATE' THEN  84
						WHEN ParameterVariable='MAINTROOMS' THEN  85
						WHEN ParameterVariable='MAINTCONF' THEN  86
						WHEN ParameterVariable='MAINTCOMM' THEN  87
						WHEN ParameterVariable='MAINTREM' THEN  88
						WHEN ParameterVariable='PAXNBR' THEN  89
						WHEN ParameterVariable='PRINTLOGO' THEN  90
						WHEN ParameterVariable='LINESEP' THEN  91
						WHEN ParameterVariable='REQUESTOR' THEN  92
						WHEN ParameterVariable='DEPICAO' THEN  93
						WHEN ParameterVariable='ARRICAO' THEN  94
						WHEN ParameterVariable='PRNFIRSTPG' THEN  95
						WHEN ParameterVariable='INTLFLTPHN' THEN  96
						WHEN ParameterVariable='CREWPILOT1' THEN  97
						WHEN ParameterVariable='CREWADDL' THEN  98
						WHEN ParameterVariable='CREWPILOT2' THEN  99
						WHEN ParameterVariable='FBOALPRICE' THEN  100
						WHEN ParameterVariable='FBOALDATE' THEN  101
						WHEN ParameterVariable='FBOACONT' THEN  102
						WHEN ParameterVariable='FBOAADDR' THEN  103
						WHEN ParameterVariable='FBOAREM' THEN  104
						WHEN ParameterVariable='FBOAPHONE2' THEN  105
						WHEN ParameterVariable='FBOAPAYTYP' THEN  106
						WHEN ParameterVariable='FBOAPOSTPR' THEN  107
						WHEN ParameterVariable='FBOAFUELQT' THEN  108
						WHEN ParameterVariable='FBOACONF' THEN  109
						WHEN ParameterVariable='FBOACOMM' THEN  110
						WHEN ParameterVariable='FBODPHONE' THEN  111
						WHEN ParameterVariable='FBODFREQ' THEN  112
						WHEN ParameterVariable='FBODFAX' THEN  113
						WHEN ParameterVariable='FBODUVAIR' THEN  114
						WHEN ParameterVariable='FBODFUEL' THEN  115
						WHEN ParameterVariable='FBODNPRICE' THEN  116
						WHEN ParameterVariable='FBODLPRICE' THEN  117
						WHEN ParameterVariable='FBODLDATE' THEN  118
						WHEN ParameterVariable='FBODCONT' THEN  119
						WHEN ParameterVariable='FBODADDR' THEN  120
						WHEN ParameterVariable='FBODREM' THEN  121
						WHEN ParameterVariable='FBODPHONE2' THEN  122
						WHEN ParameterVariable='FBODPAYTYP' THEN  123
						WHEN ParameterVariable='FBODPOSTPR' THEN  124
						WHEN ParameterVariable='FBODFUELQT' THEN  125
						WHEN ParameterVariable='FBODCONF' THEN  126
						WHEN ParameterVariable='FBODCOMM' THEN  127
						WHEN ParameterVariable='FBOAPHONE' THEN  128
						WHEN ParameterVariable='FBOAFREQ' THEN  129
						WHEN ParameterVariable='FBOAFAX' THEN  130
						WHEN ParameterVariable='FBOAUVAIR' THEN  131
						WHEN ParameterVariable='FBOAFUEL' THEN  132
						WHEN ParameterVariable='FBOANPRICE' THEN  133
						WHEN ParameterVariable='DISPATCHER' THEN  134
						WHEN ParameterVariable='DSPTEMAIL' THEN  135
						WHEN ParameterVariable='DSPTPHONE' THEN  136
						WHEN ParameterVariable='TRIPNO' THEN  137
						WHEN ParameterVariable='DESC' THEN  138
						WHEN ParameterVariable='TRIPDATES' THEN  139
						WHEN ParameterVariable='TMREQ' THEN  140
						WHEN ParameterVariable='TAILNMBR' THEN  141
						WHEN ParameterVariable='CONTPHONE' THEN  142
						WHEN ParameterVariable='TYPECODE' THEN  143
						WHEN ParameterVariable='CQDESC' THEN  144
						WHEN ParameterVariable='REVISNNO' THEN  145
						WHEN ParameterVariable='REVISNDESC' THEN  146
						WHEN ParameterVariable='TIMEFORMAT' THEN  147
						WHEN ParameterVariable='CRMBLPHN' THEN  148
						WHEN ParameterVariable='HOTELADDR' THEN  149
						WHEN ParameterVariable='PAXORDER' THEN  150
						WHEN ParameterVariable='CHRMONTH' THEN  151
						WHEN ParameterVariable='LEGSEP' THEN  152
						WHEN ParameterVariable='LEGNOTESF' THEN  153
						WHEN ParameterVariable='CQNAME' THEN  154
						WHEN ParameterVariable='CQCONTACT' THEN  155
						WHEN ParameterVariable='DISPATCHIN' THEN  156 ELSE 9999 END
		)
END

ELSE IF @ReportNumber=2
BEGIN
	INSERT INTO #tblTSSettings (
		TripSheetReportHeaderID
		,TripSheetReportDetailID
		,ReportNumber
		,ReportDescription
		,ParameterCD
		,ParameterType
		,ParameterCValue
		,ParameterLValue
		,ParameterNValue
		,ParameterValue
	) 
	SELECT TripSheetReportHeaderID, TripSheetReportDetailID, 
	ReportNumber, ReportDescription, [ParameterCD] = ParameterVariable, 
	ParameterType, ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	FROM TripSheetReportDetail
	WHERE TripSheetReportHeaderID = CONVERT(BIGINT,@ReportHeaderID)
	AND ReportNumber = @ReportNumber
	AND IsDeleted=0
	ORDER BY (CASE  WHEN ParameterVariable='ONEBITEG' OR ParameterVariable='ONELEG'   THEN 1
					WHEN ParameterVariable='GENNOTES'  THEN 2
					WHEN ParameterVariable='TRIPALERTS'  THEN 3
					WHEN ParameterVariable='FULLCREW'  THEN 4
					WHEN ParameterVariable='DEPTAUTH'  THEN 5
					WHEN ParameterVariable='LEGNOTES'  THEN 6
					WHEN ParameterVariable='FUELBURN'  THEN 7
					WHEN ParameterVariable='NOTES'  THEN 8
					WHEN ParameterVariable='ALERTS'  THEN 9
					WHEN ParameterVariable='OUTBOUND'  THEN 10
					WHEN ParameterVariable='PAXMANIF'  THEN 11
					WHEN ParameterVariable='PAXCODE'  THEN 12
					WHEN ParameterVariable='PAXDEPT'  THEN 13
					WHEN ParameterVariable='PAXPHONE'  THEN 14
					WHEN ParameterVariable='PAXNBR'  THEN 15
					WHEN ParameterVariable='PAXCNG'  THEN 16
					WHEN ParameterVariable='ARRFBP'  THEN 17
					WHEN ParameterVariable='FBOAPHONE'  THEN 18
					WHEN ParameterVariable='FBOAFREQ'  THEN 19
					WHEN ParameterVariable='FBOAFAX'  THEN 20
					WHEN ParameterVariable='FBOAUVAIR'  THEN 21
					WHEN ParameterVariable='FBOAFUEL'  THEN 22
					WHEN ParameterVariable='FBOANPRICE'  THEN 23
					WHEN ParameterVariable='FBOALPRICE'  THEN 24
					WHEN ParameterVariable='FBOALDATE'  THEN 25
					WHEN ParameterVariable='FBOACONT'  THEN 26
					WHEN ParameterVariable='FBOAADDR'  THEN 27
					WHEN ParameterVariable='FBOAREM'  THEN 28
					WHEN ParameterVariable='PAXHOTEL'  THEN 29
					WHEN ParameterVariable='HOTELPHONE'  THEN 30
					WHEN ParameterVariable='HOTELFAX'  THEN 31
					WHEN ParameterVariable='HOTELCOMM'  THEN 32
					WHEN ParameterVariable='PTANAME'  THEN 33
					WHEN ParameterVariable='PTAPHONE'  THEN 34
					WHEN ParameterVariable='PTAFAX'  THEN 35
					WHEN ParameterVariable='PTAREMARKS'  THEN 36
					WHEN ParameterVariable='CDNAME'  THEN 37
					WHEN ParameterVariable='CDPHONE'  THEN 38
					WHEN ParameterVariable='CDFAX'  THEN 39
					WHEN ParameterVariable='CDREMARKS'  THEN 40
					WHEN ParameterVariable='CREWHOTEL'  THEN 41
					WHEN ParameterVariable='CREWHPHONE'  THEN 42
					WHEN ParameterVariable='CREWHFAX'  THEN 43
					WHEN ParameterVariable='CREWHNRATE'  THEN 44
					WHEN ParameterVariable='CREWHADDR'  THEN 45
					WHEN ParameterVariable='CREWHCOMM'  THEN 46
					WHEN ParameterVariable='CTANAME'  THEN 47
					WHEN ParameterVariable='CTAPHONE'  THEN 48
					WHEN ParameterVariable='CTAFAX'  THEN 49
					WHEN ParameterVariable='CTARATE'  THEN 50
					WHEN ParameterVariable='CTAREMARKS'  THEN 51
					WHEN ParameterVariable='MAINTHOTEL'  THEN 52
					WHEN ParameterVariable='MAINTPHONE'  THEN 53
					WHEN ParameterVariable='MAINTFAX'  THEN 54
					WHEN ParameterVariable='MAINTNRATE'  THEN 55
					WHEN ParameterVariable='MAINTADDR'  THEN 56
					WHEN ParameterVariable='MAINTCOMM'  THEN 57
					WHEN ParameterVariable='PAXRPTPOS'  THEN 58
					WHEN ParameterVariable='FBOAPHONE2'  THEN 59
					WHEN ParameterVariable='PXLRGEFON'  THEN 60
					WHEN ParameterVariable='HOTELREM'  THEN 61
					WHEN ParameterVariable='CANAME'  THEN 62
					WHEN ParameterVariable='CAPHONE'  THEN 63
					WHEN ParameterVariable='CAFAX'  THEN 64
					WHEN ParameterVariable='CAREMARKS'  THEN 65
					WHEN ParameterVariable='CREWHREM'  THEN 66
					WHEN ParameterVariable='STATECODE'  THEN 67
					WHEN ParameterVariable='SUPPAXNOTE'  THEN 68
					WHEN ParameterVariable='SUPCRWNOTE'  THEN 69
					WHEN ParameterVariable='DISPATCHNO'  THEN 70
					WHEN ParameterVariable='PURPOSE'  THEN 71
					WHEN ParameterVariable='PAXNOTES'  THEN 72
					WHEN ParameterVariable='ACNOTES'  THEN 73
					WHEN ParameterVariable='FBOAPAYTYP'  THEN 74
					WHEN ParameterVariable='FBOAPOSTPR'  THEN 75
					WHEN ParameterVariable='FBOAFUEBITQT' OR ParameterVariable='FBOAFUELQT'  THEN 76
					WHEN ParameterVariable='AIRFLTPHN'  THEN 77
					WHEN ParameterVariable='CRMBBITPHN' OR ParameterVariable='CRMBLPHN'  THEN 78
					WHEN ParameterVariable='DISPATCHER'  THEN 79
					WHEN ParameterVariable='TERMTOAFBO'  THEN 80
					WHEN ParameterVariable='ALLOWSHADE'  THEN 81
					WHEN ParameterVariable='CREWHROOMS'  THEN 82
					WHEN ParameterVariable='CREWHCONF'  THEN 83
					WHEN ParameterVariable='HOTELROOMS'  THEN 84
					WHEN ParameterVariable='HOTELCONF'  THEN 85
					WHEN ParameterVariable='PTACONFIRM'  THEN 86
					WHEN ParameterVariable='PTACOMMENT'  THEN 87
					WHEN ParameterVariable='PTDNAME'  THEN 88
					WHEN ParameterVariable='PTDPHONE'  THEN 89
					WHEN ParameterVariable='PTDFAX'  THEN 90
					WHEN ParameterVariable='PTDREMARKS'  THEN 91
					WHEN ParameterVariable='PTDCONFIRM'  THEN 92
					WHEN ParameterVariable='PTDCOMMENT'  THEN 93
					WHEN ParameterVariable='CTACONFIRM'  THEN 94
					WHEN ParameterVariable='CTACOMMENT'  THEN 95
					WHEN ParameterVariable='CTDNAME'  THEN 96
					WHEN ParameterVariable='CTDPHONE'  THEN 97
					WHEN ParameterVariable='CTDFAX'  THEN 98
					WHEN ParameterVariable='CTDRATE'  THEN 99
					WHEN ParameterVariable='CTDREMARKS'  THEN 100
					WHEN ParameterVariable='CTDCONFIRM'  THEN 101
					WHEN ParameterVariable='CTDCOMMENT'  THEN 102
					WHEN ParameterVariable='CACONFIRM'  THEN 103
					WHEN ParameterVariable='CACOMMENT'  THEN 104
					WHEN ParameterVariable='CDCONFIRM'  THEN 105
					WHEN ParameterVariable='CDCOMMENT'  THEN 106
					WHEN ParameterVariable='FBOACONF'  THEN 107
					WHEN ParameterVariable='FBOACOMM'  THEN 108
					WHEN ParameterVariable='DEPFBP'  THEN 109
					WHEN ParameterVariable='FBODPHONE'  THEN 110
					WHEN ParameterVariable='FBODFREQ'  THEN 111
					WHEN ParameterVariable='FBODFAX'  THEN 112
					WHEN ParameterVariable='FBODUVAIR'  THEN 113
					WHEN ParameterVariable='FBODFUEL'  THEN 114
					WHEN ParameterVariable='FBODNPRICE'  THEN 115
					WHEN ParameterVariable='FBODLPRICE'  THEN 116
					WHEN ParameterVariable='FBODLDATE'  THEN 117
					WHEN ParameterVariable='FBODCONT'  THEN 118
					WHEN ParameterVariable='FBODADDR'  THEN 119
					WHEN ParameterVariable='FBODREM'  THEN 120
					WHEN ParameterVariable='FBODPHONE2'  THEN 121
					WHEN ParameterVariable='FBODPAYTYP'  THEN 122
					WHEN ParameterVariable='FBODPOSTPR'  THEN 123
					WHEN ParameterVariable='FBODFUEBITQT' OR ParameterVariable='FBODFUELQT' THEN 124
					WHEN ParameterVariable='TERMTODFBO'  THEN 125
					WHEN ParameterVariable='FBODCONF'  THEN 126
					WHEN ParameterVariable='FBODCOMM'  THEN 127
					WHEN ParameterVariable='FLTCAT'  THEN 128
					WHEN ParameterVariable='PASSPORT'  THEN 129
					WHEN ParameterVariable='CITIZEN'  THEN 130
					WHEN ParameterVariable='PAXDOB'  THEN 131
					WHEN ParameterVariable='HOTELRATE'  THEN 132
					WHEN ParameterVariable='PTARATE'  THEN 133
					WHEN ParameterVariable='PTDRATE'  THEN 134
					WHEN ParameterVariable='CARATE'  THEN 135
					WHEN ParameterVariable='CDRATE'  THEN 136
					WHEN ParameterVariable='CONDOUTBND'  THEN 137
					WHEN ParameterVariable='PAXALERTS'  THEN 138
					WHEN ParameterVariable='FARNUM'  THEN 139
					WHEN ParameterVariable='CNTRYCODE'  THEN 140
					WHEN ParameterVariable='MAINTREM'  THEN 141
					WHEN ParameterVariable='MAINTCONF'  THEN 142
					WHEN ParameterVariable='AIRADDINFO'  THEN 143
					WHEN ParameterVariable='PAXADDINFO'  THEN 144
					WHEN ParameterVariable='FUELLOAD'  THEN 145
					WHEN ParameterVariable='AUTHPHONE'  THEN 146
					WHEN ParameterVariable='LMESSAGE'  THEN 147
					WHEN ParameterVariable='MESSAGE'  THEN 148
					WHEN ParameterVariable='MESSAGEPOS'  THEN 149
					WHEN ParameterVariable='FLIGHTNO'  THEN 150
					WHEN ParameterVariable='LINESEP'  THEN 151
					WHEN ParameterVariable='ARRAIRPORT'  THEN 152
					WHEN ParameterVariable='DEPAIRPORT'  THEN 153
					WHEN ParameterVariable='REQUESTOR'  THEN 154
					WHEN ParameterVariable='SORTBYDATE'  THEN 155
					WHEN ParameterVariable='CREWLASTLG'  THEN 156
					WHEN ParameterVariable='PRINTLOGO'  THEN 157
					WHEN ParameterVariable='PRNFIRSTPG'  THEN 158
					WHEN ParameterVariable='INTLFLTPHN'  THEN 159
					WHEN ParameterVariable='PAXVISA'  THEN 160
					WHEN ParameterVariable='CREWPILOT1'  THEN 161
					WHEN ParameterVariable='CREWPILOT2'  THEN 162
					WHEN ParameterVariable='CREWADDL'  THEN 163
					WHEN ParameterVariable='PAXADDLPHN'  THEN 164
					WHEN ParameterVariable='REQADDLPHN'  THEN 165
					WHEN ParameterVariable='WINDS'  THEN 166
					WHEN ParameterVariable='DSPTEMAIL'  THEN 167
					WHEN ParameterVariable='DSPTPHONE'  THEN 168
					WHEN ParameterVariable='TRIPNO'  THEN 169
					WHEN ParameterVariable='DESC'  THEN 170
					WHEN ParameterVariable='TRIPDATES'  THEN 171
					WHEN ParameterVariable='TMREQ'  THEN 172
					WHEN ParameterVariable='TAILNMBR'  THEN 173
					WHEN ParameterVariable='CONTPHONE'  THEN 174
					WHEN ParameterVariable='TYPECODE'  THEN 175
					WHEN ParameterVariable='CQDESC'  THEN 176
					WHEN ParameterVariable='REVISNNO'  THEN 177
					WHEN ParameterVariable='REVISNDESC'  THEN 178
					WHEN ParameterVariable='FUELDTLS'  THEN 179
					WHEN ParameterVariable='FUELVEND'  THEN 180
					WHEN ParameterVariable='BOLDNOTES'  THEN 181
					WHEN ParameterVariable='HOTELADDR'  THEN 182
					WHEN ParameterVariable='PAXORDER'  THEN 183
					WHEN ParameterVariable='TSASTATUS'  THEN 184
					WHEN ParameterVariable='VERIFYNO'  THEN 185
					WHEN ParameterVariable='ACFTRLSE'  THEN 186
					WHEN ParameterVariable='CBPXMLS'  THEN 187
					WHEN ParameterVariable='CHRMONTH'  THEN 188
					WHEN ParameterVariable='DALERTS'  THEN 189
					WHEN ParameterVariable='ATISFREQ'  THEN 190
					WHEN ParameterVariable='DEPATIS'  THEN 191
					WHEN ParameterVariable='ARRATIS'  THEN 192
					WHEN ParameterVariable='CLRNCE'  THEN 193
					WHEN ParameterVariable='CRWADLINFO'  THEN 194
					WHEN ParameterVariable='CRWDOB'  THEN 195
					WHEN ParameterVariable='CRWPNUM'  THEN 196
					WHEN ParameterVariable='CRWNATION'  THEN 197
					WHEN ParameterVariable='CRWPDATE'  THEN 198
					WHEN ParameterVariable='PAXREQDOC'  THEN 199
					WHEN ParameterVariable='LEGSEP'  THEN 200
					WHEN ParameterVariable='CQNAME'  THEN 201
					WHEN ParameterVariable='CQCONTACT'  THEN 202
					WHEN ParameterVariable='PAXNOTESF'  THEN 203
					WHEN ParameterVariable='DISPATCHIN'  THEN 204
					WHEN ParameterVariable='LGMINFUEL'  THEN 205
					WHEN ParameterVariable='LGPOWERSET'  THEN 206
					WHEN ParameterVariable='LGAIRSPEED'  THEN 207  ELSE 9999 END )

END

ELSE

BEGIN
	INSERT INTO #tblTSSettings (
		TripSheetReportHeaderID
		,TripSheetReportDetailID
		,ReportNumber
		,ReportDescription
		,ParameterCD
		,ParameterType
		,ParameterCValue
		,ParameterLValue
		,ParameterNValue
		,ParameterValue
	) 
	SELECT TripSheetReportHeaderID, TripSheetReportDetailID, 
	ReportNumber, ReportDescription, [ParameterCD] = ParameterVariable, 
	ParameterType, ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	FROM TripSheetReportDetail
	WHERE TripSheetReportHeaderID = CONVERT(BIGINT,@ReportHeaderID)
	AND ReportNumber = @ReportNumber
	AND IsDeleted=0
END
		IF OBJECT_ID('TEMPDB..#tblTS') IS NOT NULL 
		DROP TABLE #tblTS
		CREATE  TABLE #tblTS(ParameterCD VARCHAR(100),ParamValue VARCHAR(MAX), ParameterType CHAR(1))
		INSERT INTO #tblTS
		SELECT ParameterCD, [ParamValue] = CASE ParameterType 
				WHEN 'L' THEN CONVERT(VARCHAR,ISNULL(ParameterLValue,0))
				WHEN 'N' THEN CONVERT(VARCHAR,ISNULL(ParameterNValue,0))
				WHEN 'M' THEN CONVERT(VARCHAR(MAX),ISNULL(ParameterValue,''))
				ELSE CONVERT(VARCHAR,ISNULL(ParameterCValue,'')) END
			, ParameterType	
		FROM #tblTSSettings

		--[START TEMPSETTINGS UPDATE]-------------
		IF @TempSettings <> '' 
		BEGIN
			DECLARE @separator CHAR(2), @keyValueSeperator CHAR(2)
			SET @separator = '||'
			SET @keyValueSeperator = '::'

			DECLARE @separator_position INT , @keyValueSeperatorPosition INT
			DECLARE @match VARCHAR(MAX) 
			
			SET @TempSettings = @TempSettings + @separator
			
			WHILE PATINDEX('%' + @separator + '%' , @TempSettings) <> 0 
			BEGIN
				SELECT @separator_position =  PATINDEX('%' + @separator + '%' , @TempSettings)
				SELECT @match = LEFT(@TempSettings, @separator_position - 1)
				IF @match <> '' 
				BEGIN
					SELECT @keyValueSeperatorPosition = PATINDEX('%' + @keyValueSeperator + '%' , @match)
					IF @keyValueSeperatorPosition <> -1 
					BEGIN
						--INSERT @OutTable VALUES (LEFT(@match,@keyValueSeperatorPosition -1),RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition))
						--print LEFT(@match,@keyValueSeperatorPosition -1) + ' - ' + RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1)
						--UPDATE #tblTS SET ParamValue = RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) WHERE ParameterCD = LEFT(@match,@keyValueSeperatorPosition -1)
						UPDATE #tblTS SET ParamValue = CASE 
											WHEN RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) = 'True' THEN '1'
											WHEN RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) = 'False' THEN '0'
											ELSE RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition-1) END
											WHERE ParameterCD = LEFT(@match,@keyValueSeperatorPosition -1)
					END
				END		
				SELECT @TempSettings = STUFF(@TempSettings, 1, @separator_position + 1, '')
			END
		END	
		--[END TEMPSETTINGS UPDATE]-------------

	-- Corrective meassure, reports are designed in a this way: 1 for HIDE and 0 fro DISPLAY
	UPDATE #tblTS SET ParamValue = CASE ParamValue WHEN '1' THEN '0' WHEN '0' THEN '1' ELSE ParamValue END
	WHERE ParameterType = 'L'
	ALTER TABLE #tblTS DROP COLUMN ParameterType

--select * from #tblTS
	DECLARE @columns VARCHAR(MAX)
	DECLARE @query VARCHAR(MAX)
	SELECT @columns = COALESCE(@columns + ',[' + cast(ParameterCD as varchar) + ']',
	'[' + cast(ParameterCD as varchar)+ ']')
	FROM #tblTS

	SET @query = 'SELECT * FROM #tblTS
	PIVOT ( MAX(ParamValue) FOR [ParameterCD] IN (' + @columns + ') ) AS P'

	EXECUTE(@query)
		DROP TABLE #tblTS
END
    /*
	--- START TESTING ---
	SELECT TripSheetReportHeaderID, TripSheetReportDetailID, 
	ReportNumber, ReportDescription, [ParameterCD] = ParameterVariable,ParameterDescription, 
	ParameterType, ParameterCValue, ParameterLValue, ParameterNValue, ParameterValue
	FROM TripSheetReportDetail
	WHERE TripSheetReportHeaderID = 10099163261
	AND ReportNumber = 2
	--- END TESTING ---
	*/
    
    --UPDATE TripSheetReportDetail SET ParameterLValue =1 WHERE TripSheetReportHeaderID = 10013161677 AND TripSheetReportDetailID =10013165297

    

--  EXEC  spGetReportPRETSFormatSettings 10013161677, 2 --Customer13 Default, TRIPSHEET    
--  EXEC  spGetReportPRETSFormatSettings 10013161677, 1 --Customer13 Default, OVERVIEW

--  EXEC  spGetReportPRETSFormatSettings 10013161677, 2, 'TERMTOAFBO::2' --OVERVIEW
--  EXEC  spGetReportPRETSFormatSettings 10013161677, 1, 'FBOS::0||DISPATCHNO::True||PURPOSE::False' --OVERVIEW




GO


