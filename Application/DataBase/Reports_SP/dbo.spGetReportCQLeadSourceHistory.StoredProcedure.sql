IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQLeadSourceHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQLeadSourceHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCQLeadSourceHistory]
	(
	@UserCD AS VARCHAR(30),
	@UserCustomerID AS VARCHAR(30),
	@DATEFROM DATETIME, --MANDATORY  
	@DATETO DATETIME, --MANDATORY 
	@CQCustomerCD VARCHAR(500)='',
	@CustomerName VARCHAR(500)=''
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportCQLeadSourceHistory
-- Author: Akhila
-- Create date: 
-- Description: Charter Quote Customer Information based on lead source
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#PAXTABLE') IS NOT NULL
DROP TABLE #PAXTABLE

CREATE TABLE #PAXTABLE
(
  [CustomerName] VARCHAR(60)
 ,[LeadSourceCD] CHAR(4)
 ,[LeadSource] VARCHAR(30)
 ,[TripsQuoted] INT
 ,[TripsBooked] INT
 ,[TripPaxCountForBookedTrips] VARCHAR(100)
 ,[TotalQuotedAmtForBookedTrips] NUMERIC(17,2)
 ,[FileNo] INT
 ,[QuoteNo] INT
 ,[TailNo] VARCHAR(9)
 ,[LegNo] INT
 ,[ArrICAO] CHAR(4)
 ,[DepICAO] CHAR(4)
 ,[ETA] DATETIME
 ,[ETD] DATETIME
 ,[PAXCount] NUMERIC(4,0)
 ,[PAXName] VARCHAR(300)
 ,LeadSourceID BIGINT
 ,CQCustomerID BIGINT
 ,CQMainID BIGINT
 ,[RNK] BIGINT
 ,CQCustomerCD VARCHAR(6)
 ,Name VARCHAR(40)
 ,CQLegID BIGINT
 ,TripID  BIGINT
) 

Declare @FromDate VARCHAR(10)=dbo.GetShortDateFormatByUserCD(@UserCustomerID,@DATEFROM)
      ,@ToDate VARCHAR(10)=dbo.GetShortDateFormatByUserCD(@UserCustomerID,@DATETO);




INSERT INTO #PAXTABLE 
([CustomerName],[LeadSourceCD],[LeadSource] ,[TripPaxCountForBookedTrips]
,[FileNo],[QuoteNo],[TailNo],[LegNo],[ArrICAO],[DepICAO],[ETA],[ETD],[PAXCount],[PAXName],LeadSourceID,CQCustomerID
,CQMainID,[RNK],CQCustomerCD,Name,CQLegID,TripID)

SELECT DISTINCT ISNULL(CP.CQCustomerName,'')+ ' (' + ISNULL(CP.CQCustomerCD,'')+'  '+ ')'
      ,CP.LeadSourceCD
      ,CP.LeadSourceDescription
      ,NULL--TripCnt.Cnt
      ,CP.FileNUM
      ,CP.QuoteNUM
      ,CP.TailNum
      ,CP.LegNUM
      ,CP.AIcaoID
      ,CP.DIcaoID
      ,CP.InboundDTTM
      ,CP.OutboundDTTM
      ,CP.PassengerTotal
      ,NULL
      ,CP.LeadSourceID
      ,CP.CQCustomerID
      ,CP.CQMainID
      ,RANK () OVER (PARTITION BY CP.CQCustomerName, CP.LeadSourceCD ORDER BY  CP.CQCustomerName, CP.LeadSourceCD, CP.FileNUM, CP.QuoteNUM, CP.TailNum ,CP.LegNUM)
      ,CP.CQCustomerCD
      ,CP.CQCustomerName
      ,CP.POLegID
      ,CP.TripID
   FROM [dbo].[vCQPostFlight] CP

			 
WHERE CP.CustomerID = CONVERT(BIGINT,@UserCustomerID)
 AND  CONVERT(DATE,CP.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
 AND (CP.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')
 AND (CP.LeadSourceCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CustomerName, ','))OR @CustomerName='')




INSERT INTO #PAXTABLE (LeadSourceCD,LeadSourceID,LeadSource,CQCustomerCD,CQCustomerID,CustomerName,Name,RNK)
SELECT DISTINCT LS.LeadSourceCD,LS.LeadSourceID,LS.LeadSourceDescription,CC.CQCustomerCD,CC.CQCustomerID,
       ISNULL(CF.CQCustomerName,'')+ ' (' + ISNULL(CC.CQCustomerCD,'')+'  '+ ')',CF.CQCustomerName,
       RANK () OVER (PARTITION BY CF.CQCustomerName, LS.LeadSourceCD ORDER BY  CF.CQCustomerName, LS.LeadSourceCD)
        FROM CQFile CF
         INNER JOIN CQMain CM ON CM.CQFileID = CF.CQFileID
         INNER JOIN (SELECT LeadSourceID,LeadSourceCD,LeadSourceDescription FROM LeadSource)LS ON LS.LeadSourceID = CF.LeadSourceID
         LEFT OUTER JOIN (SELECT CQCustomerCD,CQCustomerID FROM CQCustomer)CC ON CC.CQCustomerID = CF.CQCustomerID
         WHERE CF.CustomerID=@UserCustomerID
            AND  NOT EXISTS(SELECT T.LeadSourceCD FROM #PAXTABLE T 
					WHERE T.LeadSourceID = LS.LeadSourceID AND   ISNULL(T.Name,'1')=ISNULL(CF.CQCustomerName,'1'))
            AND CONVERT(DATE,CF.EstDepartureDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
            AND(CC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')
            AND CM.TripID IS NULL 
             AND CF.IsDeleted=0





UPDATE XX SET PAXName=SUBSTRING(PP.PaxNames,1,LEN(PP.PaxNames)-1),TripPaxCountForBookedTrips=TripCnt.Cnt FROM #PAXTABLE XX
                     LEFT OUTER JOIN 
                      (SELECT DISTINCT POLegID, PaxNames = (
						  SELECT ISNULL(P1.LastName,'') +', ' 
							   FROM PostflightPassenger CQP
							   INNER JOIN Passenger P1 ON CQP.PassengerID=P1.PassengerRequestorID
							   WHERE CQP.POLegID = CQP2.POLegID
							 FOR XML PATH('')
							 )
						  FROM PostflightPassenger CQP2)PP ON PP.POLegID=XX.CQLegID
					LEFT OUTER JOIN (SELECT PM.POLogID,PM.LogNum,TripID,COUNT(DISTINCT PassengerID) Cnt FROM PostflightMain PM 
                                                 INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
                                                 INNER JOIN  PostflightPassenger PP ON PP.POLegID=PL.POLegID
                                                 --INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID
                                                WHERE PM.CustomerID=@UserCustomerID
                                               GROUP BY PM.POLogID,PM.LogNum,TripID
                                         )TripCnt ON TripCnt.TripID=XX.TripID


DECLARE @TripInfo TABLE(CQCustomerID BIGINT,
                  LeadSourceID BIGINT,
                  TripBooked BIGINT,
                  Trips_Quoted BIGINT,
                  Total_Quote_Amt INT,
                  CQCustomerName VARCHAR(40))
                  
INSERT INTO @TripInfo
SELECT CF.CQCustomerID,LS.LeadSourceID,TripBooked = SUM(CASE WHEN PM.TripID IS NOT NULL THEN 1 ELSE 0 END) 
				,COUNT(CM.QuoteNUM) Trips_Quoted,SUM(CASE WHEN PM.TripID IS NOT NULL THEN CM.QuoteTotal ELSE 0 END) Total_Quote_Amt,
				CF.CQCustomerName
				FROM CQMain CM
				INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID
				INNER JOIN LeadSource LS ON LS.LeadSourceID = CF.LeadSourceID
				LEFT OUTER JOIN PostflightMain PM ON PM.TripID=CM.TripID
				WHERE CF.EstDepartureDT BETWEEN @DateFrom And @DateTo
				 AND CF.CustomerID = @UserCustomerID
				GROUP BY LS.LeadSourceID,CF.CQCustomerName,CF.CQCustomerID



UPDATE XX SET TripsBooked=CQ.TripBooked,TripsQuoted=CQ.Trips_Quoted,TotalQuotedAmtForBookedTrips=CQ.Total_Quote_Amt
		   FROM #PAXTABLE XX
			INNER JOIN (SELECT * FROM @TripInfo)CQ 
			ON CQ.LeadSourceID=XX.LeadSourceID AND ISNULL(XX.Name,'1')=ISNULL(CQ.CQCustomerName,'1')  AND ISNULL(XX.CQCustomerID,999)=ISNULL(CQ.CQCustomerID,999)
			 AND ISNULL(XX.Name,'1')=ISNULL(CQ.CQCustomerName,'1')
			 AND ISNULL(XX.CQCustomerID,999)=ISNULL(CQ.CQCustomerID,999)
			 WHERE CQ.LeadSourceID=XX.LeadSourceID
			 AND ISNULL(XX.Name,'1')=ISNULL(CQ.CQCustomerName,'1')
			 AND ISNULL(XX.CQCustomerID,999)=ISNULL(CQ.CQCustomerID,999)
                                    			          


                                    			          



SELECT DISTINCT  
  [DateRange]=@FromDate +' - '+ @ToDate
 ,[CustomerName]
 ,[LeadSourceCD]
 ,[LeadSource]
 ,CASE WHEN [RNK]=1 THEN ISNULL([TripsQuoted],0) ELSE 0 END [TripsQuoted]
 ,CASE WHEN [RNK]=1 THEN ISNULL([TripsBooked],0) ELSE 0 END [TripsBooked]
 ,CASE WHEN [RNK]=1 THEN ISNULL([TripPaxCountForBookedTrips],0) ELSE 0 END [TripPaxCountForBookedTrips]
 ,CASE WHEN [RNK]=1 THEN ISNULL([TotalQuotedAmtForBookedTrips],0) ELSE 0 END [TotalQuotedAmtForBookedTrips]
 ,[FileNo]
 ,[QuoteNo]
 ,[TailNo]
 ,[LegNo]
 ,[ArrICAO]
 ,[DepICAO]
 ,[ETA]
 ,[ETD]
 ,[PAXCount] 
 ,[PAXName]
 ,LeadSourceID
 ,CQCustomerID
 ,[RNK]
 ,CQCustomerCD
FROM #PAXTABLE PT
WHERE (LeadSourceCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CustomerName, ','))OR @CustomerName='')
ORDER BY [CustomerName],CQCustomerCD,LeadSource,[LeadSourceCD],[FileNo],[QuoteNo],[TailNo],[LegNo]
 
IF OBJECT_ID('tempdb..#PAXTABLE') IS NOT NULL
DROP TABLE #PAXTABLE
END 

--EXEC spGetReportCQLeadSourceHistory 'SUPERVISOR_16','10016','01-01-2000','01-01-2010','',''
GO