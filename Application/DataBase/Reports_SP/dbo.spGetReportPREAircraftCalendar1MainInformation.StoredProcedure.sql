IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar1MainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar1MainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportPREAircraftCalendar1MainInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
		--@HomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
AS

-- ===============================================================================
-- SPC Name: spGetReportPREAircraftCalendar1Information
-- Author: Sudhakar J 
-- Create date: 04 Sep 2012
-- Description: Get Aircraft Calender information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON

	
-----------------------------TailNum and Fleet Group Filteration----------------------

 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------
CREATE TABLE #TempFleetID ( FleetID BIGINT, TailNum VARCHAR(9), AircraftCD VARCHAR(10),Type_Code VARCHAR(4) )	

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID, F.TailNum, A.AircraftCD,F.AircraftCD
	FROM Fleet F
	INNER JOIN Aircraft A ON F.AircraftID = A.AircraftID AND F.CustomerID = A.CustomerID
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID, F.TailNum, A.AircraftCD,F.AircraftCD
	FROM Fleet F 
	INNER JOIN Aircraft A ON F.AircraftID = A.AircraftID AND F.CustomerID = A.CustomerID
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID, F.TailNum, A.AircraftCD,F.AircraftCD
	FROM Fleet F 
	   INNER JOIN Aircraft A ON F.AircraftID = A.AircraftID AND F.CustomerID = A.CustomerID
	WHERE F.IsDeleted=0
	  AND F.CustomerID = @CUSTOMER  
	  AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)

	-- Populate the complete Fllet List for each Date in the given range
	CREATE TABLE #TempFleetInfo (
	    TailSetNo INT,
	    Fleet1 VARCHAR(20) NULL,
	    Fleet2 VARCHAR(20) NULL,
	    Fleet3 VARCHAR(20) NULL,
	    Fleet4 VARCHAR(20) NULL,	    	    
	    Fleet5 VARCHAR(20) NULL
	)	


	DECLARE @Count INT, @index INT, @Set INT, @PrevSet INT;
	DECLARE @curFleetID BIGINT, @curTailNum VARCHAR(9), @curAircraftCD VARCHAR(10);
	SET @index =1; SET @Set = 1; SET @PrevSet = 0;
	Declare @FleetTemp VARCHAR(20);

	DECLARE curTailInfo CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT FleetID, TailNum, AircraftCD FROM #TempFleetID ORDER BY Type_Code;
						
	OPEN curTailInfo;
	Set @Count = @@CURSOR_ROWS 
	FETCH NEXT FROM curTailInfo INTO @curFleetID, @curTailNum, @curAircraftCD;
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		--PRINT convert(varchar,@Set) + '/' + convert(varchar,@Index) + '/' + convert(varchar,@curFleetID);
		IF @PrevSet <> @Set BEGIN
		-- INSERT SET NUMBER
			INSERT INTO #TempFleetInfo (TailSetNo) VALUES (@Set);
			SET @PrevSet = @Set;	
		END
	
		-- UPDATE FLEETS FOR EACH SET
		UPDATE #TempFleetInfo SET
			Fleet1 = (CASE WHEN @Index = 1 THEN RTRIM(@curTailNum) + '***' + RTRIM(@curAircraftCD) ELSE Fleet1 END),
			Fleet2 = (CASE WHEN @Index = 2 THEN RTRIM(@curTailNum) + '***' + RTRIM(@curAircraftCD) ELSE Fleet2 END),
			Fleet3 = (CASE WHEN @Index = 3 THEN RTRIM(@curTailNum) + '***' + RTRIM(@curAircraftCD) ELSE Fleet3 END),
			Fleet4 = (CASE WHEN @Index = 4 THEN RTRIM(@curTailNum) + '***' + RTRIM(@curAircraftCD) ELSE Fleet4 END),
			Fleet5 = (CASE WHEN @Index = 5 THEN RTRIM(@curTailNum) + '***' + RTRIM(@curAircraftCD) ELSE Fleet5 END)
		WHERE TailSetNo = @Set
			
		FETCH NEXT FROM curTailInfo INTO @curFleetID, @curTailNum, @curAircraftCD;
		SET @FleetTemp = '';
		SET @Index = @Index + 1;
		IF @Index = 6 BEGIN
			SET @Index = 1;
			SET @Set = @Set + 1;
		END
	END
	CLOSE curTailInfo;
	DEALLOCATE curTailInfo;	



WITH T(date) AS ( SELECT @DATEFROM UNION ALL SELECT DateAdd(day,1,T.date) FROM T WHERE T.date < @DATETO )
SELECT TF.TailSetNo, T.date AS ActiveDate, TF.Fleet1, TF.Fleet2, TF.Fleet3, TF.Fleet4, TF.Fleet5,
CONVERT(VARCHAR(5), T.date, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS DisplayDT
,(dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) + '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)) [DateRange]
INTO #AircraftMain
FROM T OUTER APPLY #TempFleetInfo TF OPTION (MAXRECURSION 32767) 

SELECT * FROM #AircraftMain ORDER BY  TailSetNo, ActiveDate



IF OBJECT_ID('tempdb..#AircraftMain') IS NOT NULL 
DROP table #AircraftMain	
IF OBJECT_ID('tempdb..#TempFleetInfo') IS NOT NULL 
DROP table #TempFleetInfo	
IF OBJECT_ID('tempdb..#TempFleet') IS NOT NULL 
DROP table #TempFleet	
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL 
DROP table #TempFleetID

END 
---EXEC spGetReportPREAircraftCalendar1MainInformation 'JWILLIAMS_11','2012-08-01', '2012-08-12', ''
GO


