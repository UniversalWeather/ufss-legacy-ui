IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCustomerUVMaintainedAirports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCustomerUVMaintainedAirports]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spGetReportDBCustomerUVMaintainedAirports] 
(
        @UserCD AS VARCHAR(30) --Mandatory
       ,@UserCustomerID AS VARCHAR(30)
       ,@Airports As NVARCHAR(1000) = '' 
)
AS

BEGIN
 

-- ===============================================================================
-- SPC Name: spGetReportDBCustomerUVMaintainedAirports
-- Author: Askar R
-- Create date: 23 Feb 2013
-- Description: Get Customer Additional Information
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================
IF @Airports='UV'

BEGIN

SELECT DISTINCT A.IcaoID ICAO,
       A.CityName City,
       A.StateName State,
       CR.CountryCD Country,
       A.AirportName Airport_Name,
       A.IsInActive Inactive,
       'UV'  Custom_UV
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         AND UWAID IS NOT NULL
 ORDER BY A.IcaoID
END 

ELSE IF  @Airports='CUSTOM'        
BEGIN

SELECT DISTINCT A.IcaoID ICAO,
       A.CityName City,
       A.StateName State,
       CR.CountryCD Country,
       A.AirportName Airport_Name,
       A.IsInActive Inactive,
        'Custom'  Custom_UV
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         AND UWAID IS NULL
         ORDER BY A.IcaoID
END 
ELSE
BEGIN 
SELECT DISTINCT A.IcaoID ICAO,
       A.CityName City,
       A.StateName State,
       CR.CountryCD Country,
       A.AirportName Airport_Name,
       A.IsInActive Inactive,
       CASE WHEN A.UWAID='' OR  A.UWAID IS NULL THEN 'CUSTOM' ELSE 'UV' END  Custom_UV
       FROM  Airport A INNER JOIN Country CR ON CR.CountryID=A.CountryID
       WHERE A.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND A.CustomerID=CONVERT(BIGINT,@UserCustomerID)
         ORDER BY A.IcaoID
END

END





--EXEC spGetReportDBCustomerUVMaintainedAirports 'SUPERVISOR_99',10099,'UV'






GO


