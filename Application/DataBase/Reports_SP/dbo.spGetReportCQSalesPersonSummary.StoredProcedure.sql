IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQSalesPersonSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQSalesPersonSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportCQSalesPersonSummary]
     
    @UserCD AS VARCHAR(30),
    @UserCustomerID BIGINT,
	@CQCustomerCD VARCHAR(1000)='',
	@DateFrom DATETIME, --MANDATORY  
    @DateTo DATETIME, --MANDATORY 
    @SalesPersonCD VARCHAR(1000)=''

AS
-- ===============================================================================
-- SPC Name: spGetReportCQSalesPersonSummary
-- Author: Akhila
-- Create date: 
-- Description: Charter Quote Customer details based on Sales Person
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
BEGIN
--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
SET NOCOUNT ON;
/*
   SELECT TEMP.Sales_Rep,TEMP.CQCustomerName,COUNT(TEMP.Trips_Quoted) Trips_Quoted,
        SUM(TEMP.Total_Quote_Amt) Total_Quote_Amt, SUM(TripBooked)TripBooked FROM 
 (SELECT DISTINCT CASE WHEN LastName<>'' AND LastName IS NOT NULL THEN LastName+', ' ELSE '' END + ISNULL(FirstName,'')+ ' (' +SalesPersonCD+ ')' AS Sales_Rep,
		   CQCustomerName,
		   Trips_Quoted,
		   Total_Quote_Amt,ISNULL(TripBooked,0)TripBooked
	   
	 FROM 
		
	 (  SELECT  CM.TripID,SUM( CM.QuoteNUM) Trips_Quoted,SUM(CM.QuoteTotal) Total_Quote_Amt,SP.SalesPersonCD,SP.LastName,SP.FirstName,CQC.CQCustomerCD,CQC.CQCustomerName,CQC.CQCustomerID
         FROM CQMain CM INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID
			            LEFT OUTER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID
			            INNER JOIN SalesPerson SP ON SP.SalesPersonID = CF.SalesPersonID
			            LEFT JOIN (SELECT DISTINCT PRE.TripID,COUNT(DISTINCT PRE.TripNUM) TripNum FROM  PreflightMain PRE 
																			JOIN PostflightMain POST
																				 ON PRE.TripID = POST.TripID
						
									                          WHERE IsQuote = 1 AND IsLog = 1 
									                          GROUP  BY PRE.TripID
		                            )BB ON BB.TripID=CM.TripID
			            --INNER JOIN PreflightMain PM ON CM.TripID=PM.TripID
		   WHERE CF.QuoteDT BETWEEN @DateFrom And @DateTo
           AND(SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ','))OR @SalesPersonCD='')
		   AND(CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')
		   AND(CF.CustomerID = @UserCustomerID)
	       GROUP BY  SP.SalesPersonCD,SP.LastName,SP.FirstName,CQC.CQCustomerCD,CQC.CQCustomerName,CQC.CQCustomerID,CM.TripID
	    )
	    AA
	 
		LEFT JOIN 
		(SELECT DISTINCT COUNT(DISTINCT PRE.TripNUM) TripBooked, PRE.TripID,
						 CC.CQCustomerID FROM 
			PreflightMain PRE 
			JOIN PostflightMain POST
				 ON PRE.TripID = POST.TripID
			LEFT JOIN CQCustomer CC 
				ON CC.CustomerID = PRE.CustomerID
			INNER JOIN CQMain CM ON CM.TripID=PRE.TripID
			WHERE IsQuote = 1 AND IsLog = 1 
			GROUP BY PRE.TripID, CC.CQCustomerID
		 )BB 
		 ON  AA.CQCustomerID = BB.CQCustomerID AND BB.TripID=AA.TripID
	)TEMP
GROUP BY TEMP.Sales_Rep,TEMP.CQCustomerName

*/
 SELECT SP.* FROM 
(
 SELECT CASE WHEN LastName<>'' AND LastName IS NOT NULL THEN LastName+', ' ELSE '' END + ISNULL(FirstName,'')+ ' (' +SalesPersonCD+ ')' AS Sales_Rep 
       ,CQC.CQCustomerName,CQC.CQCustomerCD
       ,COUNT(CM.QuoteNUM) Trips_Quoted
       ,SUM(CASE WHEN PM.TripID IS NOT NULL THEN CM.QuoteTotal ELSE 0 END) Total_Quote_Amt
       ,SUM(CASE WHEN PM.TripID IS NOT NULL THEN 1 ELSE 0 END)TripBooked
       ,SP.SalesPersonCD
       
       FROM CQMain CM INNER JOIN CQFile CF ON CM.CQFileID = CF.CQFileID
			          INNER JOIN SalesPerson SP ON SP.SalesPersonID = CF.SalesPersonID
			          INNER JOIN CQCustomer CQC ON CQC.CQCustomerID=CF.CQCustomerID
			          LEFT OUTER JOIN PostflightMain PM ON PM.TripID=CM.TripID
			WHERE CF.QuoteDT BETWEEN @DateFrom And @DateTo
		   AND CF.CustomerID = @UserCustomerID
		   AND SP.IsDeleted=0

	GROUP BY SalesPersonCD,CQC.CQCustomerCD,CQC.CQCustomerName,LastName,FirstName
)SP		  
WHERE (SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPersonCD, ','))OR @SalesPersonCD='')
 AND  (CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ','))OR @CQCustomerCD='')        
 ORDER BY SalesPersonCD,CQCustomerCD,CQCustomerName
END
-- EXEC [dbo].[spGetReportCQSalesPersonSummary]  'JWILLIAMS_13','','01-01-2010','02-01-2012',''


GO