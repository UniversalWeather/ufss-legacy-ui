IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBPayableVendorsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBPayableVendorsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




  
  
CREATE PROCEDURE  [dbo].[spGetReportDBPayableVendorsExportInformation]   
(  
        @UserCD AS VARCHAR(30) --Mandatory  
       ,@UserCustomerID AS VARCHAR(30)  
       ,@VendorCD AS VARCHAR(500)  
)  
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportDBPayableVendorsExportInformation  
-- Author: Askar R  
-- Create date: 2 Jan 2014  
-- Description: Get Vendor  Information  
-- Revision History  
-- Date        Name        Ver         Change  
--EXEC spGetReportDBPayableVendorsExportInformation 'SUPERVISOR_99','10099','V1000'
--   
-- ================================================================================  
SELECT DISTINCT name_a=V.Name
			   ,vendtype=V.VendorType
			   ,vendcode=V.VendorCD
			   ,b_name=V.BillingName
			   ,b_addr1=V.BillingAddr1
			   ,b_addr2=V.BillingAddr2
			   ,b_city=V.BillingCity
			   ,b_state=V.BillingState
			   ,b_zip=V.BillingZip
			   ,b_country=CR.CountryCD
			   ,b_metro=M.MetroCD
			   ,b_phone=VC.PhoneNum
			   ,b_fax=VC.FaxNum
		       ,Notes_A=V.Notes
			   ,taxid=V.TaxID
			   ,terms=V.Terms
			   ,homebase=A.IcaoID
			   ,icao_id=CT.IcaoID
			   ,choice=CASE WHEN VC.IsChoice=1 THEN 'TRUE' ELSE 'FALSE' END
			   ,name_b=ISNULL(VC.FirstName,'') +' '+ISNULL(VC.LastName,'') 
			   ,addr1=VC.Addr1
			   ,addr2=VC.Addr2
			   ,city=VC.CityName
			   ,state=VC.StateName
			   ,zip=VC.PostalZipCD
			   ,country=CRC.CountryCD
			   ,phone=VC.PhoneNum
			   ,fax=VC.FaxNum
			   ,VC.VendorContactCD 
			   ,MoreInfo=VC.MoreInfo
			   ,Notes_B=VC.Notes
       FROM Vendor V  INNER JOIN VendorContact VC ON V.VendorID=VC.VendorID 
					  LEFT JOIN Company C ON V.HomebaseID=C.HomebaseAirportID  
					  LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID  
					  LEFT JOIN Airport CT ON V.AirportID=CT.AirportID  
					  LEFT JOIN Country CR ON CR.CountryID=V.CountryID 
					  LEFT JOIN Country CRC ON CRC.CountryID=VC.CountryID  
					  LEFT JOIN Metro M ON M.MetroID=V.MetroID
       WHERE V.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
        AND V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ','))  
         AND V.CustomerID=CONVERT(BIGINT,@UserCustomerID)   
       And V.IsInActive=1 
       And V.IsDeleted=0  
       AND VC.IsDeleted=0
  ORDER BY choice DESC,VendorContactCD
END  
--SELECT * FROM VendorContact WHERE CustomerID=10099
--SELECT * FROM Vendor WHERE CustomerID=10099
 --EXEC spGetReportDBPayableVendorsExportInformation 'SUPERVISOR_99',10099,'P1000'



GO


