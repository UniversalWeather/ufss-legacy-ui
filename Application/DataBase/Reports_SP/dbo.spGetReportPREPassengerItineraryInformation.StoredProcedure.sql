IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREPassengerItineraryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREPassengerItineraryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[spGetReportPREPassengerItineraryInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@PaxID VARCHAR(500) = '', --Optional,
		@PassengerGroupCD VARCHAR(500)='' 
		
AS

-- =============================================
-- SPC Name: spGetReportPREPassengerItineraryInformation
-- Author: SINDHUJA.K
-- Create date: 02 August 2012
-- Description: Get Preflight Passenger Itinerary  information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================


SET NOCOUNT ON



DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PaxID <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PaxID, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PaxID = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------



		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(500)
		DECLARE @depicao_os NUMERIC(6,2),@arricao_os NUMERIC(6,2);
		
			SELECT	DISTINCT		
			[TripDates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD),
			[DepDate] =  PL.DepartureDTTMLocal,
			[LegID] = PL.LegID,
			[From] = ISNULL(AA.CityName,'') + ', ' + ISNULL(AA.StateName,'') + '*' + ISNULL(AA.AirportName,''),
			[To] = ISNULL(A.CityName,'') + ', ' + ISNULL(A.StateName,'') + '*' +  ISNULL(A.AirportName,''),
			[Depart] =PL.DepartureDTTMLocal,
			[Arrive] = PL.ArrivalDTTMLocal,
			[FltTime] = CONVERT(NUMERIC(9,2), PL.ElapseTM),			
			[PassengerNm] = PX.Paxnm,
			[TotPax] = PL.PassengerTotal,
			[ArrDate] =  CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) = CONVERT(DATE,PL.DepartureDTTMLocal) THEN NULL ELSE PL.ArrivalDTTMLocal END,
			[TCEnrouteis] =ISNULL(CASE WHEN ((CASE(A.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN A.OffsetToGMT+1 ELSE A.OffsetToGMT END)END))<> 0 THEN
                           (CASE(A.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN A.OffsetToGMT+1 ELSE A.OffsetToGMT END)END)-
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END) END,0),
			[TCHomeBaseis] = ISNULL(CASE WHEN ((CASE(A.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN A.OffsetToGMT+1 ELSE A.OffsetToGMT END)END))<> 0 THEN
                           (CASE(A.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN A.OffsetToGMT+1 ELSE A.OffsetToGMT END)END)-
                           (CASE(AA.IsDayLightSaving) WHEN 1 THEN (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) THEN AA.OffsetToGMT+1 ELSE AA.OffsetToGMT END)END) END,0),
		    [TerminalA] = FBA.FBOVendor,
		    [TerminalAPh] = FBA.PhoneNUM1,		     
		    [TerminalD] = FBD.FBOVendor,
		    [TerminalDPh] = FBD.PhoneNUM1,	
			[Aircraft] = F.TailNum ,
			[Aircraft1] = AR.AircraftDescription ,			
			[Passengers] = SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1),
			[FlightInformation] = PL.Notes
			,PX.PassengerRequestorCD
			,[LEG_NUM] = ROW_NUMBER() OVER(PARTITION BY PX.Paxnm ORDER BY PL.LegID)
			
			FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL  On PM.TripID = PL.TripID AND PL.IsDeleted = 0  
				INNER JOIN FLEET F On PM.FleetID = F.FleetID
				LEFT OUTER JOIN Aircraft AR ON F.AircraftID = AR.AircraftID							 
				LEFT OUTER JOIN AIRPORT AA ON PL.DepartICAOID = AA.AirportID 
				LEFT OUTER JOIN AIRPORT A On PL.ArriveICAOID = A.AirportID
				LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID

			 LEFT OUTER JOIN (
			SELECT DISTINCT PP2.LegID, Paxnames = ( 
				SELECT  ISNULL(P.FirstName,'')+ ' ' + ISNULL(P.LastName,'') + '; '
				FROM PreflightPassengerList PP1 INNER JOIN Passenger P ON PP1.PassengerID = P.PassengerRequestorID
				WHERE PP1.LegID = PP2.LegID AND PP1.FlightPurposeID is Not NULL
				--ORDER BY P.FirstName
				FOR XML PATH('')
			)
			FROM  PreflightPassengerList PP2
        ) PAX ON PL.LegID = PAX.LegID			       
		INNER JOIN (				             
             SELECT DISTINCT PP3.LegID,PP3.PassengerID, ISNULL(P.FirstName,'') + ' ' + ISNULL(P.LastName,'')  As Paxnm,P.PassengerRequestorCD PassengerRequestorCD
               FROM PreflightPassengerList PP3 
               INNER JOIN Passenger P ON PP3.PassengerID = P.PassengerRequestorID AND PP3.FlightPurposeID is Not NULL                                       		      
           )PX ON PL.LegID = PX.LegID 
         INNER JOIN @TempPassengerID TP ON TP.PassengerID=PX.PassengerID
				
           	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		       AND CONVERT(DATE,PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		       AND PM.IsDeleted = 0
		       AND PM.TripStatus = 'T'
                                   
		     ORDER BY PassengerNm , Depart


		
   --EXEC spGetReportPREPassengerItineraryInformation 'jwilliams_13','2012-08-15', '2013-08-16'
   
 -- select tripnum from Preflightleg where legid= 100115357
--SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.FBOVendor 
--				FROM PreflightFBOList PFL
--				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1 where PFL.LegID = 100135181

GO


