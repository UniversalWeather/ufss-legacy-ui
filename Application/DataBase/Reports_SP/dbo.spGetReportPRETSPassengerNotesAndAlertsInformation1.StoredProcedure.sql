IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSPassengerNotesAndAlertsInformation1]
	@UserCD VARCHAR(50)--MANDATORY
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum VARCHAR(30) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSPassengerNotesAndAlertsInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight Passenger Notes And Alerts information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
SELECT  PassengerName = P.PassengerName
		,Notes = P.Notes
		,Alerts = P.PassengerAlert
		,[LegID] = CONVERT(VARCHAR,PAX.LegID)
		,[LegNum] = CONVERT(VARCHAR,PAX.LegNUM )
	FROM (
		SELECT DISTINCT PPL.PassengerID, PL.LegID, PL.LegNUM 
		FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
			INNER JOIN FlightPurpose FP ON PPL.FlightPurposeID=FP.FlightPurposeID
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PM.TripID = CONVERT(BIGINT,@TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	) PAX
	
	INNER JOIN (
		SELECT PassengerRequestorID, PassengerName, Notes, PassengerAlert 
		FROM Passenger
	) P ON PAX.PassengerID = P.PassengerRequestorID
	
END

--EXEC spGetReportPRETSPassengerNotesAndAlertsInformation1 'JWILLIAMS_13', 100131807, ''


GO


