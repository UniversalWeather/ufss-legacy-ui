GO
/****** Object:  StoredProcedure [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerInformation]    Script Date: 02/20/2014 12:22:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarMonthlyPlannerInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerInformation]
GO
/****** Object:  StoredProcedure [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerInformation]    Script Date: 02/20/2014 12:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarMonthlyPlannerInformation] 
		@UserCD VARCHAR(50)--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@BeginDate DATETIME --Mandatory
       ,@NoOfMonths INT=1
       ,@EndDate DATETIME--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	    ---Display Options---
	  ,@IsBoth INT=1  ---1-Both,2-Fleet,3-Crew
	  ,@IsActivityOnly BIT=0
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@Color BIT = 0
	  ,@EFleet VARCHAR(MAX)=''
	  ,@ECrew VARCHAR(MAX)=''
	  ,@NonReversedColor BIT = 0
	  ,@DisplayDays INT=1	
AS  

BEGIN  
--For 8 hours flightpak is showing wrong
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarMonthlyPlannerInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Weekly Fleet information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
           
         
 SET NOCOUNT ON  

SET @NoOfMonths =2*@NoOfMonths
 --DECLARE @BeginDate DATETIME=DATEADD(mm,(YEAR(@GoToDate) - 1900)* 12 + MONTH(@GoToDate) - 1,1 - 1);
 --DECLARE @EndDate   DATETIME=DATEADD(mm,(YEAR(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1900)* 12 + MONTH(DATEADD(M,@NoOfMonths-1,@GoToDate)) - 1,1 - 1) ;


  ;WITH CTE_DatesTable
  AS 
  (
    SELECT @BeginDate AS [date],1 MonthNo
    UNION ALL
    SELECT DATEADD(HH, (CASE WHEN @DisplayDays=1 THEN 360 
							 WHEN @DisplayDays=2 THEN 180
						     WHEN @DisplayDays=3 THEN 120
							 WHEN @DisplayDays=4 THEN 90
							 WHEN @DisplayDays=5 THEN 15 END), [date]),MonthNo+1 MonthNo
    FROM CTE_DatesTable
    WHERE DATEADD(HH, (CASE WHEN @DisplayDays=1 THEN 360 
							 WHEN @DisplayDays=2 THEN 180
						     WHEN @DisplayDays=3 THEN 120
							 WHEN @DisplayDays=4 THEN 90
							 WHEN @DisplayDays=5 THEN 15 END), [date]) <= @EndDate
  )
  SELECT date BeginDate
		,DATEADD(HH,(CASE WHEN @DisplayDays=1 THEN 348 
						  WHEN @DisplayDays=2 THEN 348
						  WHEN @DisplayDays=3 THEN 232
						  WHEN @DisplayDays=4 THEN 174
						  WHEN @DisplayDays=5 THEN 30 END),DATE) EndDate
		,MonthNo
		,DateVal=CASE WHEN MONTH(@BeginDate)=MONTH(@EndDate) THEN LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate)) ELSE  LEFT(DATENAME(M,@BeginDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@BeginDate))+' - '+LEFT(DATENAME(M,@EndDate),3)+' '+CONVERT(VARCHAR(4),YEAR(@EndDate)) END
		
		 FROM CTE_DatesTable
		 WHERE MonthNo <= @NoOfMonths
		 option (maxrecursion 0)
		 
		 
				
END; 



 --EXEC spGetReportPRESchedueCalendarMonthlyPlannerInformation 'UC', '2012-07-05 ', '2012-07-29', '', ''

















