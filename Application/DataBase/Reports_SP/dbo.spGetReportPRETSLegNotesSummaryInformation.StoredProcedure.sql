IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegNotesSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegNotesSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSLegNotesSummaryInformation]
	@UserCD VARCHAR(50)--MANDATORTY
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LEGNOTESF AS SMALLINT = 1
	,@LegNum VARCHAR(150) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSLegNotesInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight TripSheet Leg Notes information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

  SELECT [LegID]=CONVERT(VARCHAR,PL.LegID)
		,[LegNum]=CONVERT(VARCHAR,PL.LegNUM)
		--,PassengerNotes = ISNULL(PL.Notes,'')
		--,CrewNotes = ISNULL(PL.CrewNotes,'')
		--,Notes = ISNULL(AA.GeneralNotes,'')
		--,DepartureAlerts = ISNULL(AD.Alerts,'')
		--,ArrivalAlerts = ISNULL(AA.Alerts,'')
		--,AircraftNotes = ISNULL(F.Notes,'')
		--,AirportNotes = ISNULL(AA.GeneralNotes,'')
		--,NotesAndAnnouncements = ISNULL(CO.ApplicationMessage,'')
		--,TripsheetAlerts = ISNULL(PM.Notes,'')
		,[PassengerNotes] = CASE 
						WHEN ISNULL(PL.Notes,'') = '' AND ISNULL(PL.CrewNotes,'') = '' THEN ''
						ELSE 'PASSENGER NOTES'+'<BR>' +
							--+ISNULL(PL.Notes,'')
							CASE WHEN @LEGNOTESF = 2 THEN '<B>' + PL.Notes + '</B>'
								WHEN @LEGNOTESF = 3 THEN '<I>' + PL.Notes + '</I>'
								WHEN @LEGNOTESF = 4 THEN '<U>' + PL.Notes + '</U>'
								WHEN @LEGNOTESF = 5 THEN '<B><I><U>' + PL.Notes + '</U></I></B>'							
							ELSE PL.Notes
							END
						END
		,[CrewNotes] = CASE WHEN ISNULL(PL.Notes,'') = '' AND ISNULL(PL.CrewNotes,'') = ''
						THEN ''
						ELSE '----------<BR>'+'CREW NOTES'+'<BR>' + --ISNULL(PL.CrewNotes,'')
							CASE WHEN @LEGNOTESF = 2 THEN '<B>' + PL.CrewNotes + '</B>'
								WHEN @LEGNOTESF = 3 THEN '<I>' + PL.CrewNotes + '</I>'
								WHEN @LEGNOTESF = 4 THEN '<U>' + PL.CrewNotes + '</U>'
								WHEN @LEGNOTESF = 5 THEN '<B><I><U>' + PL.CrewNotes + '</U></I></B>'							
							ELSE PL.Notes
							END
						END						
		FROM PreflightMain PM
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		--INNER JOIN (SELECT AirportID, IcaoID, Alerts  FROM Airport) AD ON PL.DepartICAOID = AD.AirportID  
		--INNER JOIN (SELECT AirportID, IcaoID, Alerts, GeneralNotes FROM Airport) AA ON PL.ArriveICAOID = AA.AirportID 
		--INNER JOIN (SELECT FleetID, Notes, CustomerID FROM Fleet) F 
		--	ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		--INNER JOIN (SELECT HomebaseID, CustomerID, ApplicationMessage FROM Company) CO 
		--	ON CO.HomebaseID = PM.HomebaseID AND PM.CustomerID = CO.CustomerID
		WHERE PM.CustomerID =  dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		AND PM.TripID = CONVERT(BIGINT,@TripID)
		AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
		AND (ISNULL(PL.Notes,'') + ISNULL(PL.CrewNotes,'') <> '')
		ORDER BY PM.TripID,LegNum
END

--spGetReportPRETSLegNotesSummaryInformation 'SUPERVISOR_99','10099107564',''




GO


