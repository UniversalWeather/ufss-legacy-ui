IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelSummaryTailNumInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelSummaryTailNumInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetReportPOSTFuelSummaryTailNumInformation]
@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IcaoID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
		
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFuelSummaryTailNumInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Vendor, Payment and Fuel locator information for the Tail Number
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
        
	SET @SQLSCRIPT = '
	        SELECT   DISTINCT
			[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +'' - ''+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
			[TailNum] = F.TailNum,
			[FuelLocator] = FL.FuelLocatorCD+'' ''+FL.FuelLocatorDescription,
			[Date] = PE.PurchaseDT,
			[DispatchNum] = PE.DispatchNUM,
			[IcaoID] = A.IcaoID,
			[Vendor] = V.Name,		
			[PaymentType] = PT.PaymentTypeCD+'' ''+PT.PaymentTypeDescription,
			[InvoiceNum] = PE.InvoiceNUM,
			[PurchaseQty] = CAST(ROUND((dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,4)),
			[ActualPurchaseQty] =  cast(dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD) as numeric(15,5)),
			[Units] = (CASE WHEN (c.FuelPurchase=''1'') THEN ''GAL''
                             WHEN (c.FuelPurchase=''2'') THEN ''LTR''
						     WHEN (c.FuelPurchase=''3'') THEN ''IMP''
						     WHEN (c.FuelPurchase=''4'') THEN ''POUNDS''
						     ELSE ''KILOS''
						     END),
			[UnitPrice]   = Case WHEN C.FuelPurchase <> Pe.FuelPurchase THEN DBO.GetFuelPriceConversion(PE.FuelPurchase,Pe.UnitPrice,@UserCD)
                                               ELSE Pe.UnitPrice End,
			[Cost] =  pe.ExpenseAMT
		FROM PostflightExpense PE 
		INNER JOIN  PostflightMain PM ON PM.POLogID = PE.POLogID
		INNER JOIN POSTFLIGHTLEG PL ON PL.POLEGID = PE.POLEGID AND PL.ISDELETED = 0
	    LEFT OUTER JOIN  PaymentType PT ON PE.PaymentTypeID = PT.PaymentTypeID 
	    LEFT OUTER JOIN  Vendor V ON PE.PaymentVendorID = V.VendorID 
		LEFT OUTER JOIN  FuelLocator FL ON PE.FuelLocatorID = FL.FuelLocatorID 
		LEFT OUTER JOIN  Airport A ON PE.AirportID = A.AirportID 
		INNER JOIN  Fleet F ON F.FleetID = PE.FleetID 
       ----LEFT OUTER JOIN  Company c ON PE.HomeBaseID = C.HomeBaseID 
		INNER JOIN Company C ON F.CustomerID = C.CustomerID  AND C.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD)
        INNER JOIN UserMaster UM ON UM.HomebaseID=C.HomebaseID AND UM.UserName=@UserCD AND UM.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		LEFT OUTER JOIN FleetGroupOrder FGO 
					ON PE.FleetID = FGO.FleetID AND PE.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG
                   ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		
		WHERE PE.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				+ ' AND CONVERT(DATE,PE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PE.IsDeleted=0 AND PE.FuelQTY > 0	
	'
	
	--Construct OPTIONAL clauses
			
	IF @TailNUM <> '' BEGIN  
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
	END	
	IF @FleetGroupCD <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';
	END	
	IF @IcaoID <> '' BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND A.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@IcaoID, 1) = ',' THEN LEFT(@IcaoID, LEN(@IcaoID) - 1) ELSE @IcaoID END, ',', ''', ''') + ''')';
	END	

   -- PRINT @SQLSCRIPT
	SET @ParameterDefinition =  '@DATEFROM AS DATETIME, @DATETO AS DATETIME,@UserCD AS VARCHAR(30)'
    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @DATEFROM, @DATETO, @UserCD 
    
    
  -- EXEC spGetReportPOSTFuelSummaryTailNumInformation 'jwilliams_11','2011/2/7','2012/12/12', '', ''
  






GO


