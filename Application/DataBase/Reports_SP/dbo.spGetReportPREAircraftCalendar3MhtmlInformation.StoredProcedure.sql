IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar3MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar3MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar3MhtmlInformation]
	@UserCD VARCHAR(30) --Mandatory
	,@DateFrom DATETIME --Mandatory
	,@DateTo DATETIME --Mandatory
	,@TailNum VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@FleetGroupCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@AircraftCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@HomeBase BIT = 0
	
AS
-- ===============================================================================
-- SPC Name: spGetReportPREAircraftCalendar3ExportInformation
-- Author: AISHWARYA.M
-- Create date: 11 Jul 2012
-- Description: Get Preflight Aircraft Calendar Export information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================

SET NOCOUNT ON

	DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(100)

	DECLARE @NextString NVARCHAR(40)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @String NVARCHAR(40)
	DECLARE @Delimiter NVARCHAR(40)
	DECLARE @Loopdate DATETIME
	
	CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,TailNum VARCHAR(25)
	,FleetID BIGINT
	)

	SET @String = @TailNum
	SET @Delimiter = ','
	SET @String = @String + @Delimiter
	SET @Pos = charindex(@Delimiter, @String)
	
	CREATE TABLE #TEMPTABLE (
	ID INT identity(1, 1) NOT NULL
	,activedate DATETIME
	,tail_nmbr CHAR(6)
	,aircraft_code CHAR(10)
	,depicao_id CHAR(4)
	,arricao_id CHAR(4)
	,DepartureDTTMLocal DATETIME
	,ArrivalDTTMLocal DATETIME
	,duty_type CHAR(2)
	,requestor VARCHAR(63)
	,requestor_phone VARCHAR(25)
	,pax_total INT
	,AdditionalCrew VARCHAR(25)
	,comments VARCHAR(25)
	,fbo_name VARCHAR(60)
	,fbo_phone VARCHAR(25)
	,fbo_freq CHAR(7)
	,fbo_fuel NUMERIC(15,2)
	,crew_hotel_name VARCHAR(60)
	,crew_hotel_phone VARCHAR(25)
	,crew_rate NUMERIC(17,2)
	,pax_car_name VARCHAR(60)
	,pax_car_phone VARCHAR(25)
	,pax_hotel_name VARCHAR(60)
	,pax_hotel_phone VARCHAR(25)
	,NextLocalDTTM DATETIME
	,[CustomerID] BIGINT
	,LegID BIGINT
	,HomebaseID BIGINT
	)
	DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM
	
	IF (@TailNum = '')
	BEGIN
		SET @SQLSCRIPT = 
			' INSERT INTO #TempTailNum (TailNum, FleetID)
			SELECT DISTINCT F.TailNum, F.FleetID
			FROM Fleet F 
			LEFT OUTER JOIN FleetGroupOrder FGO
			ON F.FleetID = FGO.FleetID 
			LEFT OUTER JOIN FleetGroup FG 
			ON FGO.FleetGroupID = FG.FleetGroupID 
			WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
				OR @FleetGroupCD = '''' ) 
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		--	AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))
			'		
	IF @HomeBase = 1 
	BEGIN
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))
    END	
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD 

	END
	ELSE
	BEGIN
		WHILE (@pos <> 0)
		BEGIN
			SET @NextString = substring(@String, 1, @Pos - 1)

			INSERT INTO #TempTailNum(TailNum, FleetID)
			SELECT @NextString, F.FleetID FROM Fleet F WHERE F.TailNum = @NextString

			SET @String = substring(@String, @pos + 1, len(@String))
			SET @pos = charindex(@Delimiter, @String)
		END
	END
--select * from #TempTailNum	

	WHILE (@TEMPTODAY <= @DATETO)
	BEGIN
	SET @SQLSCRIPT = '
	
		INSERT INTO #TEMPTABLE
		 (
			activedate 
			,tail_nmbr
			,aircraft_code 
			,depicao_id 
			,arricao_id 
			,DepartureDTTMLocal 
			,ArrivalDTTMLocal 
			,duty_type 
			,requestor 
			,requestor_phone 
			,pax_total 
			,AdditionalCrew 
	        ,comments 
			,fbo_name 
			,fbo_phone 
			,fbo_freq 
			,fbo_fuel 
			,crew_hotel_name 
			,crew_hotel_phone 
		    ,crew_rate 
			,pax_car_name 
			,pax_car_phone 
			,pax_hotel_name 
			,pax_hotel_phone
			,NextLocalDTTM 
			,[CustomerID] 
			,LegID 
			,HomebaseID 
		)
		
		SELECT @TEMPTODAY
				,F.TailNum
				,F.AircraftCD
				,A.IcaoID
				,AA.IcaoID
				,PL.DepartureDTTMLocal
				,PL.ArrivalDTTMLocal
				,PL.DutyTYPE
				,P.PassengerName
				,P.PhoneNum
				,PL.PassengerTotal
				,AdditionalCrew =''''
	            ,comments =''''
				,PF.PreflightFBOName
				,PF.PhoneNum1
				,FB.Frequency
				,FB.FuelQty
				,PH.PreflightHotelName
				,PH.PhoneNum1
				,H.NegociatedRate
				,PT.PreflightTransportName 
				,PT.PhoneNum1
				,PH.PreflightHotelName
				,PH.PhoneNum1
				,PL.NextLocalDTTM
				,F.CustomerID
				,PL.LegID
				,F.HomebaseID
				FROM PreflightMain PM
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0
				INNER JOIN (SELECT AirportID, IcaoID FROM Airport) A ON A.AirportID = PL.DepartICAOID
				INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON AA.AirportID = PL.ArriveICAOID
				INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID
						FROM Fleet F 
						LEFT OUTER JOIN FleetGroupOrder FGO
						ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
						LEFT OUTER JOIN FleetGroup FG 
						ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
						WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
							OR @FleetGroupCD = '''' ) 
				  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
				INNER JOIN #TempTailNum TEMP ON TEMP.TailNum = F.TailNum
				LEFT OUTER JOIN (SELECT PassengerName, PhoneNum, PassengerRequestorID FROM Passenger)P ON P.PassengerRequestorID = PM.PassengerRequestorID
				LEFT OUTER JOIN (SELECT Frequency, FuelQty, FBOID FROM FBO) FB ON FB.FBOID = PL.FBOID
				LEFT OUTER JOIN (SELECT PreflightFBOName, PhoneNum1, LegID FROM PreflightFBOList)PF ON PL.LegID = PF.LegID 
				LEFT OUTER JOIN (SELECT HotelID,LegID, PreflightHotelName, PhoneNum1 FROM PreflightHotelXrefList) PH ON PH.LegID = PL.LegID
			    LEFT OUTER JOIN (SELECT LegID, PreflightTransportName, PhoneNum1, CrewPassengerType FROM PreflightTransportList) PT 
					ON PT.LegID = PL.LegID AND PT.CrewPassengerType =''P''
				LEFT OUTER JOIN (SELECT HotelID,NegociatedRate FROM HOTEL)H ON PH.HotelID=H.HotelID 
				INNER JOIN AIRCRAFT AT ON PM.AircraftID=AT.AircraftID
				WHERE CONVERT(DATE, @TEMPTODAY, 101) = CONVERT(DATE, PL.DepartureDTTMLocal, 101)
				AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			--	AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) 
				AND PM.ISDELETED = 0
				'
				
				--Construct OPTIONAL clauses
		IF @TailNUM <> '' BEGIN  
			SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
		END
		
		IF @AircraftCD <> '' BEGIN  
				SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
			END
			
		--IF @HomeBase = 1 BEGIN
		--		SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));
		--	END	
		
		--PRINT @SQLSCRIPT
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME, @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY, @FleetGroupCD
		
		SET @TEMPTODAY = @TEMPTODAY + 1
	END	
--	SELECT * FROM #TEMPTABLE

	DECLARE @activedate DATETIME
	,@tail_nmbr CHAR(6)
	,@aircraft_code CHAR(10)
	,@depicao_id CHAR(4)
	,@arricao_id CHAR(4)
	,@DepartureDTTMLocal DATETIME
	,@ArrivalDTTMLocal DATETIME
	,@duty_type CHAR(2)
	,@requestor VARCHAR(63)
	,@requestor_phone VARCHAR(25)
	,@pax_total INT
	,@AdditionalCrew VARCHAR(25)
	,@comments VARCHAR(25)
	,@fbo_name VARCHAR(60)
	,@fbo_phone VARCHAR(25)
	,@fbo_freq CHAR(7)
	,@fbo_fuel NUMERIC(15,2)
	,@crew_hotel_name VARCHAR(60)
	,@crew_hotel_phone VARCHAR(25)
	,@crew_rate NUMERIC(17,2)
	,@pax_car_name VARCHAR(60)
	,@pax_car_phone VARCHAR(25)
	,@pax_hotel_name VARCHAR(60)
	,@pax_hotel_phone VARCHAR(25)
	,@NextLocalDTTM DATETIME
	,@CustomerID BIGINT
	,@crewhotelname VARCHAR(60) 
	,@crewhotelphone VARCHAR(25)
	,@paxcarname VARCHAR(60)
    ,@paxcarphone VARCHAR(25)
	,@paxhotelname  VARCHAR(60)
	,@paxhotelphone VARCHAR(25)
	
	DECLARE curAircraftCalendarIIIExp CURSOR
	FOR
	SELECT TT.activedate 
	,TT.tail_nmbr 
	,TT.aircraft_code 
	,TT.depicao_id 
	,TT.arricao_id 
	,TT.DepartureDTTMLocal 
	,TT.ArrivalDTTMLocal 
	,TT.duty_type 
	,TT.requestor 
	,TT.requestor_phone 
	,TT.pax_total 
	,TT.AdditionalCrew
	,TT.comments 
	,TT.fbo_name 
	,TT.fbo_phone 
	,TT.fbo_freq 
	,TT.fbo_fuel 
	,TT.crew_hotel_name 
	,TT.crew_hotel_phone 
	,crew_rate 
	,TT.pax_car_name 
	,TT.pax_car_phone 
	,TT.pax_hotel_name 
	,TT.pax_hotel_phone
	,TT.NextLocalDTTM 
	,TT.CustomerID 
	FROM #TempTable TT
	WHERE CONVERT(DATE, TT.DepartureDTTMLocal, 101) < CONVERT(DATE, TT.[NextLocalDTTM], 101)
		AND TT.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		ORDER BY activedate
		
	OPEN curAircraftCalendarIIIExp
	
	FETCH NEXT
	FROM curAircraftCalendarIIIExp
	INTO  @activedate 
	,@tail_nmbr 
	,@aircraft_code 
	,@depicao_id 
	,@arricao_id 
	,@DepartureDTTMLocal 
	,@ArrivalDTTMLocal 
	,@duty_type 
	,@requestor 
	,@requestor_phone 
	,@pax_total
	,@AdditionalCrew
	,@comments  
	,@fbo_name 
	,@fbo_phone 
	,@fbo_freq 
	,@fbo_fuel
	,@crewhotelname 
	,@crewhotelphone 
	,@crew_rate
	,@paxcarname
    ,@paxcarphone
	,@paxhotelname
	,@paxhotelphone
	,@NextLocalDTTM 
	,@CustomerID 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
			 
	SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
	WHILE(CONVERT(DATE, @Loopdate, 101) < CONVERT(DATE, @NextLocalDTTM, 101))
	BEGIN
	
		INSERT INTO #TEMPTABLE (activedate, tail_nmbr, aircraft_code, depicao_id, arricao_id,
			DepartureDTTMLocal, ArrivalDTTMLocal, duty_type, requestor, requestor_phone, 
			pax_total, fbo_name, fbo_phone, fbo_freq, fbo_fuel,crew_rate, NextLocalDTTM, CustomerID)
			 
		VALUES(@Loopdate, @tail_nmbr, @aircraft_code, @arricao_id, @arricao_id, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101), 
			CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101), 'R' ,@requestor ,@requestor_phone ,@pax_total ,@fbo_name 
			,@fbo_phone ,@fbo_freq ,@fbo_fuel,@crew_rate,NULL ,@CustomerID )
			
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
	END	
			
	FETCH NEXT
	FROM curAircraftCalendarIIIExp
	INTO  @activedate 
	,@tail_nmbr 
	,@aircraft_code 
	,@depicao_id 
	,@arricao_id 
	,@DepartureDTTMLocal 
	,@ArrivalDTTMLocal 
	,@duty_type 
	,@requestor 
	,@requestor_phone 
	,@pax_total 
	,@AdditionalCrew
	,@comments 
	,@fbo_name 
	,@fbo_phone 
	,@fbo_freq 
	,@fbo_fuel
	,@crewhotelname 
	,@crewhotelphone 
	,@crew_rate
	,@paxcarname
    ,@paxcarphone
	,@paxhotelname
	,@paxhotelphone
	,@NextLocalDTTM 
	,@CustomerID 	
		
	END
	
	CLOSE curAircraftCalendarIIIExp;

	DEALLOCATE curAircraftCalendarIIIExp;	
	
	SET @TEMPTODAY = @DATEFROM
		WHILE (@TEMPTODAY <= @DATETO)
		BEGIN			
			INSERT INTO #TempTable (tail_nmbr, activedate, depicao_id, aircraft_code) 
			SELECT DISTINCT TempT.TailNum, @TEMPTODAY, dbo.GetHomeBaseCDByUserCD(LTRIM(@UserCD)), FL.AircraftCD
			FROM #TempTailNum TempT 
			INNER JOIN (SELECT AircraftCD, FleetID FROM FLEET)FL ON TempT.FleetID = FL.FleetID
			WHERE NOT EXISTS(SELECT T.tail_nmbr FROM #TempTable T 
				WHERE T.tail_nmbr = TempT.TailNum AND T.activedate = @TEMPTODAY 
			)
		SET @TEMPTODAY = @TEMPTODAY + 1
		END 

	SELECT
	 flag = 1 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total 
	,AdditionalCrew=''
	,comments=''
	,fbo_name = ''
	,fbo_phone = ''
	,fbo_freq = ''
	,fbo_fuel = null
	,crew_hotel_name
	,crew_hotel_phone
	,crew_rate
	,pax_car_name
	,pax_car_phone
	,pax_hotel_name
	,pax_hotel_phone
	,notes=''
	FROM #TEMPTABLE
	
UNION ALL
		
	SELECT
	 flag = 2 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total 
	,AdditionalCrew=''
	,comments=''
	,fbo_name 
	,fbo_phone
	,fbo_freq 
	,fbo_fuel
	,crew_hotel_name
	,crew_hotel_phone
	,crew_rate
	,pax_car_name
	,pax_car_phone
	,pax_hotel_name
	,pax_hotel_phone
	,notes=''
	FROM #TEMPTABLE
	
UNION ALL

	SELECT
	 flag = 3 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total
	,AdditionalCrew=''
	,comments='' 
	,fbo_name 
	,fbo_phone
	,fbo_freq 
	,fbo_fuel
	,crew_hotel_name 
	,crew_hotel_phone 
	,crew_rate
	,pax_car_name
	,pax_car_phone
	,pax_hotel_name
	,pax_hotel_phone
	,notes=''
	FROM #TEMPTABLE
	
UNION ALL

	SELECT
	 flag = 4 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total 
	,AdditionalCrew=''
	,comments=''
	,fbo_name 
	,fbo_phone
	,fbo_freq 
	,fbo_fuel
	,crew_hotel_name 
	,crew_hotel_phone 
	,crew_rate
	,pax_car_name 
	,pax_car_phone
	,pax_hotel_name
	,pax_hotel_phone
	,notes=''
	FROM #TEMPTABLE
	
UNION ALL

	SELECT
	 flag = 5 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total 
	,AdditionalCrew = ''
	,comments = '' 
	,fbo_name 
	,fbo_phone
	,fbo_freq 
	,fbo_fuel
	,crew_hotel_name 
	,crew_hotel_phone 
	,crew_rate
	,pax_car_name 
	,pax_car_phone
	,pax_hotel_name 
	,pax_hotel_phone
	,notes='' 
	FROM #TEMPTABLE
	
UNION ALL

	SELECT
	 flag = 6 --flag 1 = trip main, 2 = arrival fbo, 3 = crew hotel, 4 = pax transport, 5 = pax hotel, 6 = leg notes 
	,activedate 
	,tail_nmbr 
	,aircraft_code 
	,depicao_id 
	,arricao_id 
	,DepartureDTTMLocal
	,ArrivalDTTMLocal 
	,duty_type 
	,requestor 
	,requestor_phone
	,pax_total
	,AdditionalCrew = ''
	,comments = ''
	,fbo_name 
	,fbo_phone
	,fbo_freq 
	,fbo_fuel
	,crew_hotel_name 
	,crew_hotel_phone 
	,crew_rate
	,pax_car_name 
	,pax_car_phone
	,pax_hotel_name 
	,pax_hotel_phone 
	,notes=''
	FROM #TEMPTABLE
	ORDER BY activedate, tail_nmbr, flag
	
	--EXEC spGetReportPREAircraftCalendar3MhtmlInformation 'jwilliams_13', '2012-09-01', '2012-09-02', '', '', '',1 
	--EXEC spGetReportPREAircraftCalendar3ExportInformation 'UC', '2012-07-20', '2012-07-22', 'N331UV', '', '',1

GO


