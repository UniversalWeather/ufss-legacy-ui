IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor2Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor2Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPassengerRequestor2Information]
	@UserCD VARCHAR(30),
	@PassengerCD varchar(500)	
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor2Information
-- Author: SUDHAKAR J
-- Create date: 18 Jun 2012
-- Description: Get Passenger/CrewPassengerVisa Information for Report Passenger/Requestor II
-- Revision History
-- Date			Name		Ver		Change
-- 29 Aug		Sudhakar	1.1		ClientID filter
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ===============================================================================================

SET NOCOUNT ON
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100);
--DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
--SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
--SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SET @SQLSCRIPT = 'SELECT
		P.CustomerID
		,P.PassengerName
		,P.PassengerRequestorCD
		,[PhoneNum] = P.AdditionalPhoneNum --,P.PhoneNum
		--,[Passport] = dbo.FlightPakDecrypt(IsNull(PPI.PassportNUM,'''')) + '' - '' + dbo.GetCountryCDByCountryID(PPI.CountryID)
		,[Passport] = ''@#$'' + IsNull(PPI.PassportNUM,'''') + ''@#$ - '' + dbo.GetCountryCDByCountryID(PPI.CountryID)
		--,[VISA] = dbo.FlightPakDecrypt(IsNull(CPV.VisaNum,'''')) + '' - '' + dbo.GetCountryCDByCountryID(CPV.CountryID) --CoutryCD is appended as per the As-Is PDF report
		,[VISA] = ''@#$'' + IsNull(CPV.VisaNum,'''') + ''@#$ - '' + dbo.GetCountryCDByCountryID(CPV.CountryID) --CoutryCD is appended as per the As-Is PDF report
		,[HomeBase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
		,P.IsEmployeeType
		,P.IsActive --[STATUS] Not sure from which table it is coming
		,[Nationality] = dbo.GetCountryCDByCountryID(P.CountryID) --[NATLY] Not sure from which table it is coming
		--,CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) AS DateOfBirth
		,dbo.GetShortDateFormatByUserCD(@UserCD,P.DateOfBirth) AS DateOfBirth
		,[DATEOFLASTLEGLOG] = CONVERT(VARCHAR,dbo.GetShortDateFormatByUserCD(@UserCD,POLOG.ScheduledTM)) + '', '' + CONVERT(VARCHAR,POLOG.LogNum)
	FROM	Passenger P 
			LEFT OUTER JOIN CrewPassengerVisa CPV
				ON P.CustomerID = CPV.CustomerID AND  CPV.IsDeleted = 0 AND
				 P.PassengerRequestorID = CPV.PassengerRequestorID
			LEFT OUTER JOIN (
				SELECT CustomerID, PassengerRequestorID, PassportNum, CountryID, IsDeleted
				FROM CrewPassengerPassport WHERE IsDeleted = 0
			) PPI ON P.CustomerID = PPI.CustomerID AND P.PassengerRequestorID = PPI.PassengerRequestorID
			LEFT OUTER JOIN (
				SELECT A.* FROM (
					SELECT RANK() OVER(PARTITION BY POP.PassengerID ORDER BY POL.ScheduledTM DESC ) RNK
					 ,POP.PassengerID, POM.LogNum, POL.ScheduledTM, POM.CustomerID
					FROM PostflightPassenger POP
					INNER JOIN PostflightLeg POL ON POP.POLegID = POL.POLegID AND POP.CustomerID = POL.CustomerID AND POL.IsDeleted=0
					INNER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID AND POL.CustomerID = POM.CustomerID AND POM.IsDeleted=0
				) A
				WHERE A.RNK = 1
			) POLOG ON P.CustomerID = POLOG.CustomerID AND P.PassengerRequestorID = POLOG.PassengerID 
	WHERE P.IsDeleted = 0
	AND P.CustomerID = ' + convert(varchar,dbo.GetCustomerIDbyUserCD(@UserCD))	
	--AND P.ClientID = ISNULL(@CLIENTID, P.ClientID)
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND P.PassengerRequestorCD IN (''' + REPLACE(CASE WHEN RIGHT(@PassengerCD, 1) = ',' THEN LEFT(@PassengerCD, LEN(@PassengerCD) - 1) ELSE @PassengerCD END, ',', ''', ''') + ''')';
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30)'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD	
	--PRINT @SQLSCRIPT
-- EXEC spGetReportPassengerRequestor2Information 'TIM', 'MAL01,HKIOE,EJM01,MT01,ASJ01,Cake,OPAC'

-- EXEC spGetReportPassengerRequestor2Information 'ELIZA_9', 'CVILL'

--SELECT A.* FROM (
--	SELECT RANK() OVER(PARTITION BY POP.PassengerID ORDER BY POL.ScheduledTM DESC ) RNK
--	 ,POP.PassengerID, POM.LogNum, POL.ScheduledTM, POM.CustomerID
--	FROM PostflightPassenger POP
--	INNER JOIN PostflightLeg POL ON POP.POLegID = POL.POLegID AND POP.CustomerID = POL.CustomerID
--	INNER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID AND POL.CustomerID = POM.CustomerID
--) A
--WHERE A.RNK = 1




GO


