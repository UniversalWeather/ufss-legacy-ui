IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetOperationHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetOperationHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  proc [dbo].[spGetReportPOSTFleetOperationHistoryExportInformation]      
  @MONTH AS VARCHAR(10),
  @YEAR  AS INT,
  @UserCD AS VARCHAR(30),
  @TailNUM AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values		
  @FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
  
AS    
BEGIN


-- ===============================================================================
-- SPC Name: spGetReportPOSTFleetOperationExportHistory
-- Author: D.Mullai
-- Create date:
-- Description: Get Fleet Operation History  REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1

-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(9)
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
END
-----------------------------TailNum and Fleet Group Filteration----------------------	
 DECLARE @EndDate datetime        
DECLARE @StartDate datetime       
DECLARE @StartDateval datetime       
DECLARE @ParameterDefinition AS NVARCHAR(MAX)      
DECLARE @date DATETIME      
DECLARE @A int      
declare @e int 
DECLARE @d DATETIME
if(@YEAR%4=0)
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-365, @EndDate) 

end
else 
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-364, @EndDate)
--print @StartDate  
end
------------------
--print @StartDate
--print @EndDate
-----------------
     
DECLARE @Monthval int        
DECLARE @intFlag INT        
SET @intFlag=1        
DECLARE @Monthdisplay TABLE        
(      
ID INT identity(1,1) , Monthval int ,Monthnameval varchar(20),      
dateFuel datetime  ) 
WHILE (@intFlag <=12)        
BEGIN             
 --SET @StartDate= DateAdd(month,1, @StartDate)      
 INSERT INTO @Monthdisplay (Monthval,Monthnameval,dateFuel)       
 VALUES(DATEpart(month,@StartDate) , DATENAME(month,@StartDateval) ,@StartDate) 
 SET @StartDate= DateAdd(month,1, @StartDate)         
    SET @intFlag =@intFlag+1         
END       

     

-------------------------------------------------------------------------------    

-------------
 --select * from @Monthdisplay 
-----------
------------------------------------------------------------------------------
if(@YEAR%4=0)
begin
SET @StartDate=DateAdd(DD,-365, @EndDate) 
--print @StartDate
end
else 
begin
SET @StartDate=DateAdd(DD,-364, @EndDate) 
end  

select TailNum,AircraftCD,count(POlegID) as poleg,POlegID,BlockHours,FlightHours,Distance,FuelUsed,
ScheduledTM,PassengerTotal,(PassengerTotal*Distance) as paxtot,FleetGroupCD, m.Monthval,dateFuel as months

INTO #MainTailTab
from @Monthdisplay m        
left join       
(        
SELECT f.TailNum,f.AircraftCD,pl.POlegID, ROUND(pl.BlockHours,1)BlockHours,ROUND(pl.FlightHours,1) FlightHours,pl.Distance,pl.ScheduledTM,pl.FuelUsed,pl.PassengerTotal,f.FleetGroupCD,
DATEpart(MONTH,ScheduledTM) AS months FROM      
 PostflightMain pm 
 INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
INNER JOIN PostflightLeg pl ON pl.POLogID = Pm.POLogID AND PL.IsDeleted = 0
INNER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD,FG.FleetGroupCD
		FROM Fleet F 
		left outer JOIN FleetGroupOrder FGO
		ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		left outer JOIN FleetGroup FG 
		ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
		WHERE F.IsDeleted = 0 AND F.IsInActive=0
		) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID        
        
WHERE CONVERT(DATE,ScheduledTM) >= CONVERT(DATE,@StartDate) AND CONVERT(DATE,ScheduledTM) <=CONVERT(DATE,@EndDate) 
  and pm.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
  AND PM.IsDeleted = 0     
) a
      
on m.Monthval = a.months 


--AND (a.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')

group by TailNum,BlockHours,FlightHours,Distance,FuelUsed,POlegID,ScheduledTM,PassengerTotal, m.Monthval,dateFuel,FleetGroupCD,AircraftCD
order by dateFuel
-------------------------------------------------------------------------------------

IF @IsZeroSuppressActivityAircftRpt = 0
BEGIN
INSERT INTO #MainTailTab(TailNum,AircraftCD)
SELECT TF.TailNum ,F.AircraftCD FROM @TempFleetID TF 
               LEFT OUTER JOIN Fleet F ON TF.FleetID=F.FleetID

               WHERE TF.TailNum NOT IN(SELECT DISTINCT  T.TAILNUM FROM #MainTailTab T WHERE TailNum IS NOT NULL)
END
--ELSE
--BEGIN 
--INSERT INTO #MainTailTab(TailNum)
--SELECT TailNum FROM @TempFleetID WHERE TailNum NOT IN(SELECT DISTINCT  TAILNUM FROM #MainTailTab WHERE TailNum IS NOT NULL)
--END

DECLARE @TEMPTail TABLE  
    (  
 tail_nmbr char(6),
 AircraftCD char(6),
  POLEG bigInt,
 POLEGid Int,
 pldate date,
 inactive char(8) DEFAULT 'FALSE',
 lf_mon1 INT default 0,	
 lf_mon2 INT default 0,
 lf_mon3 INT default 0,	
 lf_mon4 INT default 0,
 lf_mon5 INT default 0,
 lf_mon6 INT default 0,
 lf_mon7 INT default 0,
 lf_mon8 INT default 0,
 lf_mon9 INT default 0,
 lf_mon10 INT default 0,
 lf_mon11 INT default 0,
 lf_mon12 INT default 0,
 bh_mon1 numeric(10,3) default 0,
 bh_mon2 numeric(10,3) default 0,
 bh_mon3 numeric(10,3) default 0,
 bh_mon4 numeric(10,3) default 0,
 bh_mon5 numeric(10,3) default 0,
 bh_mon6 numeric(10,3) default 0,
 bh_mon7 numeric(10,3) default 0,	 
 bh_mon8 numeric(10,3) default 0,
 bh_mon9 numeric(10,3) default 0,
 bh_mon10 numeric(10,3) default 0,
 bh_mon11 numeric(10,3) default 0,
 bh_mon12 numeric(10,3) default 0,
 fh_mon1 numeric(10,3) default 0,
 fh_mon2 numeric(10,3) default 0,
 fh_mon3 numeric(10,3) default 0,
 fh_mon4 numeric(10,3) default 0,
 fh_mon5 numeric(10,3) default 0,
 fh_mon6 numeric(10,3) default 0,
 fh_mon7 numeric(10,3) default 0,
 fh_mon8 numeric(10,3) default 0,	
 fh_mon9 numeric(10,3) default 0,
 fh_mon10 numeric(10,3) default 0,
 fh_mon11 numeric(10,3) default 0,	
 fh_mon12 numeric(10,3) default 0,
 mf_mon1 numeric(10,3) default 0,
 mf_mon2 numeric(10,3) default 0,
 mf_mon3 numeric(10,3) default 0,
 mf_mon4 numeric(10,3) default 0,
 mf_mon5 numeric(10,3) default 0,
 mf_mon6 numeric(10,3) default 0,
 mf_mon7 numeric(10,3) default 0,
 mf_mon8 numeric(10,3) default 0,
 mf_mon9 numeric(10,3) default 0,	
 mf_mon10 numeric(10,3) default 0,
 mf_mon11 numeric(10,3) default 0,
 mf_mon12 numeric(10,3) default 0,
 fb_mon1 numeric(10,3) default 0,
 fb_mon2 numeric(10,3) default 0,
 fb_mon3 numeric(10,3) default 0,
 fb_mon4 numeric(10,3) default 0,
 fb_mon5 numeric(10,3) default 0,
 fb_mon6 numeric(10,3) default 0,
 fb_mon7 numeric(10,3) default 0,
 fb_mon8 numeric(10,3) default 0,
 fb_mon9 numeric(10,3) default 0,
 fb_mon10 numeric(10,3) default 0,
 fb_mon11 numeric(10,3) default 0,
 fb_mon12 numeric(10,3) default 0,
 p_mon1 numeric(10,3) default 0,
 p_mon2 numeric(10,3) default 0,
 p_mon3	numeric(10,3) default 0,
 p_mon4 numeric(10,3) default 0,
 p_mon5 numeric(10,3) default 0,
 p_mon6 numeric(10,3) default 0,	
 p_mon7 numeric(10,3) default 0,
 p_mon8 numeric(10,3) default 0,
 p_mon9 numeric(10,3) default 0,
 p_mon10 numeric(10,3) default 0,	
 p_mon11 numeric(10,3) default 0,
 p_mon12 numeric(10,3) default 0,
 pm_mon1 numeric(10,3) default 0,
 pm_mon2 numeric(10,3) default 0,
 pm_mon3 numeric(10,3) default 0,
 pm_mon4 numeric(10,3) default 0,
 pm_mon5 numeric(10,3) default 0,
 pm_mon6 numeric(10,3) default 0,
 pm_mon7 numeric(10,3) default 0,
 pm_mon8 numeric(10,3) default 0,
 pm_mon9 numeric(10,3) default 0,
 pm_mon10 numeric(10,3) default 0,
 pm_mon11 numeric(10,3) default 0,
 pm_mon12 numeric(10,3) default 0
)


 INSERT INTO @TEMPTail(tail_nmbr,AircraftCD,POlegid, POLEG,pldate)
  SELECT distinct tailnum,AircraftCD,poleg,polegid,ScheduledTM FROM #MainTailTab
    UPDATE @TEMPTail SET lf_mon1= MT.poleg, bh_mon1= mt.blockhours, fh_mon1= mt.FlightHours, mf_mon1= mt.distance, fb_mon1= mt.FuelUsed, p_mon1= mt.PassengerTotal,pm_mon1=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.POLEG
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=1)
  --select polegid from #MainTailTab
  --select POLEG from @TEMPTail
  UPDATE @TEMPTail SET lf_mon2= MT.poleg, bh_mon2= mt.blockhours, fh_mon2= mt.FlightHours, mf_mon2= mt.distance, fb_mon2= mt.FuelUsed, p_mon2= mt.passengertotal ,pm_mon2=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=2)
  
  UPDATE @TEMPTail SET lf_mon3= MT.poleg, bh_mon3= mt.blockhours, fh_mon3= mt.FlightHours, mf_mon3= mt.distance, fb_mon3= mt.FuelUsed , p_mon3= mt.passengertotal,pm_mon3=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=3)
  
  UPDATE @TEMPTail SET lf_mon4= MT.poleg, bh_mon4= mt.blockhours, fh_mon4= mt.FlightHours, mf_mon4= mt.distance, fb_mon4= mt.FuelUsed, p_mon4= mt.passengertotal ,pm_mon4=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=4)
  
  UPDATE @TEMPTail SET lf_mon5= MT.poleg, bh_mon5= mt.blockhours, fh_mon5= mt.FlightHours, mf_mon5= mt.distance, fb_mon5= mt.FuelUsed, p_mon5= mt.passengertotal,pm_mon5=MT.PAXTOT 
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=5)
  
  UPDATE @TEMPTail SET lf_mon6= MT.poleg, bh_mon6= mt.blockhours, fh_mon6= mt.FlightHours, mf_mon6= mt.distance, fb_mon6= mt.FuelUsed, p_mon6= mt.passengertotal,pm_mon6=MT.PAXTOT 
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=6)
  
  UPDATE @TEMPTail SET lf_mon7= MT.poleg, bh_mon7= mt.blockhours, fh_mon7= mt.FlightHours, mf_mon7= mt.distance, fb_mon7= mt.FuelUsed, p_mon7= mt.passengertotal,pm_mon7=MT.PAXTOT 
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=7)
  
  UPDATE @TEMPTail SET lf_mon8= MT.poleg, bh_mon8= mt.blockhours, fh_mon8= mt.FlightHours, mf_mon8= mt.distance, fb_mon8= mt.FuelUsed, p_mon8= mt.passengertotal ,pm_mon8=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=8)
  
  UPDATE @TEMPTail SET lf_mon9= MT.poleg, bh_mon9= mt.blockhours, fh_mon9= mt.FlightHours, mf_mon9= mt.distance, fb_mon9= mt.FuelUsed, p_mon9= mt.passengertotal ,pm_mon9=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=9)
  
  UPDATE @TEMPTail SET lf_mon10= MT.poleg, bh_mon10= mt.blockhours, fh_mon10= mt.FlightHours, mf_mon10= mt.distance, fb_mon10= mt.FuelUsed, p_mon10= mt.passengertotal ,pm_mon10=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=10)
  
  UPDATE @TEMPTail SET lf_mon11= MT.poleg, bh_mon11= mt.blockhours, fh_mon11= mt.FlightHours, mf_mon11= mt.distance, fb_mon11= mt.FuelUsed, p_mon11= mt.passengertotal ,pm_mon11=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=11)
  
  UPDATE @TEMPTail SET lf_mon12= MT.poleg, bh_mon12= mt.blockhours, fh_mon12= mt.FlightHours, mf_mon12= mt.distance, fb_mon12= mt.FuelUsed, p_mon12= mt.passengertotal ,pm_mon12=MT.PAXTOT
  FROM   @TEMPTail TT
  INNER JOIN  #MainTailTab MT ON MT.polegid = TT.poleg
  WHERE DATENAME(month,ScheduledTM) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=12)
  
 select * from @TEMPTail WHERE tail_nmbr IS NOT NULL
  

IF OBJECT_ID('tempdb..#MainTailTab') IS NOT NULL
DROP TABLE #MainTailTab 	
  
  END
 -- EXEC spGetReportPOSTFleetOperationHistoryExportInformation 'aug',2009,'supervisor_99'
  -- EXEC spGetReportPOSTFleetOperationHistoryInformation 'aug',2013,'tim'



GO


