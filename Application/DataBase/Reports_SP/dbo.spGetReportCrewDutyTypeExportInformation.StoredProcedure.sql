IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewDutyTypeExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewDutyTypeExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportCrewDutyTypeExportInformation]
	@UserCD varchar(30)
AS
-- ===============================================================================
-- SPC Name: spGetReportCrewDutyTypeExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get Crew Duty Type information for REPORT export
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

	SELECT 
		[code] = DutyTypeCD
		,[desc] = DutyTypesDescription
		,[val_pnts] = ValuePTS
		,[BackgroundCustomColor] = GraphColor
		,[wkendpnts] = WeekendPTS
		,[wkldindex] = IsWorkLoadIndex
	FROM [CrewDutyType]
	WHERE 
	IsDeleted = 0
	AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	ORDER BY DutyTypeCD
--EXEC spGetReportCrewDutyTypeExportInformation 'UC'

GO


