IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTSIFLExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTSIFLExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[spGetReportPOSTSIFLExportInformation]
       @UserCD AS VARCHAR(30),
	   @UserCustomerID VARCHAR(30),
	   @LogID VARCHAR(30) 


AS
BEGIN

SET NOCOUNT ON;
DECLARE @CUSTOMER BIGINT =@UserCustomerID
 
SELECT  FlightLog=PM.LogNum
               ,TailNumber=F.TailNum
               ,DispatchNum=PM.DispatchNum
               ,Description=PM.POMainDescription
               ,TripDate= PM.EstDepartureDT 
               ,P.PassengerRequestorCD
               ,PassengerName=CASE WHEN ISNULL(P.FirstName,'')<>'' THEN P.LastName+', '+ISNULL(P.FirstName,'') ELSE P.LastName END
               ,DeptID=AD.IcaoID
               ,ArrID=AA.IcaoID
               ,DepCity=AD.CityName
               ,ArrCity=AA.CityName
               ,AircraftMultiplier=CONVERT(VARCHAR(9),PS.AircraftMultiplier)
			   ,distance=PS.Distance   
			   ,distance1=CONVERT(VARCHAR(20),PS.Distance1)
			   ,distance2=CONVERT(VARCHAR(20),PS.Distance2)
			   ,distance3=CONVERT(VARCHAR(20),PS.Distance3)
			   ,rate1=CONVERT(VARCHAR(20),PS.Rate1)
               ,rate2=CONVERT(VARCHAR(20),PS.Rate2)
               ,rate3=CONVERT(VARCHAR(20),PS.Rate3)
			   ,[SIFL Total]=PS.AmtTotal 
			   ,[TeminalCharge]=PS.TeminalCharge  
			   ,PL.LegNUM 
			   ,PS.EmployeeTYPE TypeCode 
			   ,assoc_with=ISNULL(AP.PassengerRequestorCD,'') 
			   ,AssociatedPassenger= CASE WHEN ISNULL(AP.FirstName,'')<>'' THEN AP.LastName+', '+AP.FirstName ELSE AP.LastName END
               ,PS.POLegID
               ,PassOrder=CASE WHEN ISNULL(P.FirstName,'')<>'' THEN P.LastName+', '+ISNULL(P.FirstName,'') ELSE P.LastName END
               ,POSIFLID
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID =PL.POLogID AND PM.CustomerID = PL.CustomerID 
																				AND PL.IsDeleted = 0
					   INNER JOIN Fleet F ON F.FleetID=PM.FleetID
                       INNER JOIN PostflightPassenger PP ON PL.POLegID = PP.POLegID   
                       INNER JOIN Passenger P ON PP.PassengerID = P.PassengerRequestorID 
                       INNER JOIN PostflightSIFL PS ON PM.POLogID = PS.POLogID AND PP.PassengerID = PS.PassengerRequestorID AND PM.CustomerID = PS.CustomerID  AND PS.POLegID=PL.POLegID
                       INNER JOIN Airport AD ON PS.DepartICAOID=AD.AirportID
                       INNER JOIN Airport AA ON PS.ArriveICAOID=AA.AirportID
                       LEFT OUTER JOIN Passenger AP ON AP.PassengerRequestorID=PS.AssociatedPassengerID
WHERE PM.POLogID=CONVERT(BIGINT,@LogID)
 AND PM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
 AND PM.IsDeleted = 0
 

END


GO


