IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewRatingInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewRatingInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportCrewRatingInformation]  
 @UserCD varchar(30),  
 @CrewCD char(7)  
AS  
-- =============================================  
-- SPC Name: spGetReportCrewRatingInformation  
-- Author: SUDHAKAR J  
-- Create date: 14 Jun 2012  
-- Description: Get Crew Rating Information for Report, Crew Roaster  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- =============================================  
  
SET NOCOUNT ON  
DECLARE @TenToMin SMALLINT = 0;  
 SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)  
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)   
  
 SELECT  
  CR.CustomerID  
  ,C.CrewCD  
  ,A.AircraftCD  
  ,CR.CrewRatingDescription  
  ,CR.IsPilotinCommand  
  ,CR.IsSecondInCommand  
  ,[TimeInType] = CASE WHEN ISNULL(CR.TotalTimeInTypeHrs,0)=0 THEN NULL ELSE CR.TotalTimeInTypeHrs END --TimeInType  
  --,dbo.GetDateFormatByUserCD(@UserCD, CR.AsOfDT) AS AsOfDT  
  ,dbo.GetDateFormatByUserCD(@UserCD, CR.TotalUpdateAsOfDT) AS AsOfDT  
  ,[TenToMin] = @TenToMin  
 FROM CrewRating CR   
 INNER JOIN (  
  SELECT CrewID, CrewCD FROM Crew  
 ) C ON CR.CrewID = C.CrewID  
 LEFT OUTER JOIN (  
  SELECT AircraftID, AircraftCD FROM Aircraft  
 ) A ON CR.AircraftTypeID = A.AircraftID  
 WHERE CR.IsDeleted = 0  
 AND CR.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) AND C.CrewCD = RTRIM(@CrewCD)  
   
  -- EXEC spGetReportCrewRatingInformation 'jwiliiams_13', '1GV'
GO


