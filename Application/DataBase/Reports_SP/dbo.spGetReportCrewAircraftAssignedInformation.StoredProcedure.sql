IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewAircraftAssignedInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewAircraftAssignedInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportCrewAircraftAssignedInformation]
	@UserCD varchar(30),
	@CrewCD char(7)
AS
-- =============================================
-- SPC Name: spGetReportCrewAircraftAssignedInformation
-- Author: SUDHAKAR J
-- Create date: 14 Jun 2012
-- Description: Get Crew Checklist Date Information for Report, Crew Roaster
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SET NOCOUNT ON

	SELECT
		F.TailNum
		,AircraftTypeCD = A.AircraftCD
		,F.TypeDescription
	FROM CrewAircraftAssigned CAA
		INNER JOIN (SELECT CrewID, CrewCD FROM Crew ) C ON CAA.CrewID = C.CrewID
		INNER JOIN (
			SELECT CustomerID, FleetID, TailNum, AircraftID, TypeDescription, ClientID 
			FROM Fleet
		) F ON CAA.CustomerID = F.CustomerID 
			AND CAA.FleetID = F.FleetID
		LEFT OUTER JOIN (
			SELECT AircraftID, AircraftCD  FROM Aircraft
		) A ON F.AircraftID = A.AircraftID
	WHERE CAA.IsDeleted = 0
		AND CAA.CustomerID = @CUSTOMERID
		AND ( ClientID = @CLIENTID OR @CLIENTID IS NULL )
		AND C.CrewCD = RTRIM(@CrewCD)
	
  -- EXEC spGetReportCrewAircraftAssignedInformation 'ELIZA_3', 'ECJ'


GO


