IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportFleetProfileMhtmlInformation]  
 @UserCD VARCHAR(30)  
 ,@TailNum char(6)  
AS  
-- ===============================================================================================  
-- SPC Name: spGetReportFleetProfileMhtmlInformation  
-- Author: A.Akhila 
-- Create date: 21 Jun 2012  
-- Description: Get Passenger/CrewPassengerVisa Information for Mhtml Report Passenger/Requestor I  
-- Revision History  
-- Date      Name     Ver   Change  
-- 
-- 
-- ===============================================================================================  
SET NOCOUNT ON  
  
DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;  
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));  
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));  
  
 SELECT
        [ac_code]    = F.AircraftCD,										
		[tail_nmbr]   = F.TailNum,								
		[serialnum]  = F.SerialNum,								
		[type_code]   = A.AircraftCD,								
		[type_desc]  = F.TypeDescription,								
		[max_reserv]  = F.MaximumReservation,								
		[lastinspdt] = dbo.GetShortDateFormatByUserCD(@UserCD,F.LastInspectionDT), 								
		[insp_hrs]    = F.InspectionHrs,								
		[warn_hrs]    = F.WarningHrs, 								
		[max_pax]    = F.MaximumPassenger, 								
		[homebase]   = AP.IcaoID,							
		[max_fuel]    = F.MaximumFuel,								
		[min_fuel]    = F.MinimumFuel,								
		[bow]        = F.BasicOperatingWeight, 								
        [min_runway] = F.MimimumRunway,  										
        [inactive]   = F.IsInActive,										
		[display31]  = F.IsDisplay31,								
        [mult_ctrl]  = F.SIFLMultiControlled,										
		[mult_nctrl] = F.SIFLMultiNonControled, 								
		[notes]= F.Notes,		--Added for Mhtml						
		[comp_code]  = F.ComponentCD,								
        [client]     = C.ClientCD,										
		[fleettype]  = F.FleetType, 								
		[fltphone]   = F.FlightPhoneNum, 								
        [infltphone] = F.FlightPhoneIntlNum,--Added for Mhtml										
		[class]      = F.Class,								
		[vendcode]   =  V.VendorCD,								
		[cqyrmade]   = F.YearMade, 								
		[cqextcolor] = F.ExteriorColor,								
		[cqintcolor] = F.InteriorColor, 								
		[cqafis]     = F.IsAFIS,								
		[cquvdata]   = F.IsUWAData,								
		[fpcruzsp]   = F.FlightPlanCruiseSpeed, 								
		[fpmxfltlvl] = F.FlightPlanMaxFlightLevel,								
		[maxtkoff]   = F.MaximumTakeOffWeight,								
		[maxlnding]  = F.MaximumLandingWeight ,								
		[maxzero]    = F.MaximumWeightZeroFuel,								
		[taxifuel]   = F.TaxiFuel ,								
		[multi_sec]  = F.MultiSec ,								
		[custcolor]  = F.ForeGrndCustomColor ,								
		[bcustcolor] = F.BackgroundCustomColor,								
		[fleetsize]  = F.FleetSize , 								
		[fltscandoc]= f.FltScanDoc,								
		[lastuser]   = F.LastUpdUID,								
		[lastupdt]  = F.LastUpdTS,								
		[minday]     = F.MinimumDay,								
		[istdcrw]    = F.StandardCrewIntl,          								
		[dstdcrw]    = F.StandardCrewDOM,								
		[far91]      = F.IsFAR91,								
		[far135]     = F.IsFAR135,								
		[mxcrew]     =  CW.CrewCD, 								
		[econcode]   =  EC.EmergencyContactCD ,  								
		[taxdlyadj]  = F.IsTaxDailyAdj,
		[isdeleted]  = F.IsDeleted,								
		[taxlndfee]  = F.IsTaxLandingFee,								
		[decal_nmbr] = FP.BuyAircraftAdditionalFeeDOM, 								
		[ac_make]    = FP.AircraftMake, 								
		[ac_model]   = FP.AircraftModel,								
		[colors_1]   = FP.AircraftColor1,								
		[colors_2]   = FP.AircraftColor2,								
		[trim]       = FP.AircraftTrimColor,								
		[owner_less] = FP.OwnerLesse,								
        [addr1]      = FP.Addr1,								
		[addr2]      = FP.Addr2, 								
		[city]      =  ISNULL(FP.CityName,''),								
		[state]  = FP.StateName,								
		[zipcode]    = FP.PostalZipCD,								
		[country]    = ISNULL(CY.CountryCD,''), 								
		[af_hrs]     = FI.AirframeHrs,								
		[af_cycle]   = FI.AirframeCycle,								
		[af_asof]    = dbo.GetShortDateFormatByUserCD(@UserCD,FI.AIrframeAsOfDT), 								
		[en1_hrs]    = FI.Engine1Hrs,								
		[en1_cycle]  = FI.Engine1Cycle,								
		[en1_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Engine1AsOfDT),								
		[en2_hrs]    = FI.Engine2Hrs,								
		[en2_cycle]  = FI.Engine2Cycle,								
		[en2_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Engine2AsOfDT),								
		[en3_hrs]    = FI.Engine3Hrs,								
		[en3_cycle]  = FI.Engine3Cycle,								
		[en3_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Engine3AsOfDT),								
		[en4_hrs]    = FI.Engine4Hrs,								
		[en4_cycle]  = FI.Engine4Cycle, 								
		[en4_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Engine4AsOfDT),								
		[re1_hrs]    = FI.Reverse1Hrs,								
		[re1_cycle]  = FI.Reverser1Cycle,								
		[re1_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Reverser1AsOfDT),  								
		[re2_hrs]    = FI.Reverse2Hrs,								
		[re2_cycle]  = FI.Reverser2Cycle, 								
		[re2_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Reverser2AsOfDT), 								
		[re3_hrs]    = FI.Reverse3Hrs,  								
		[re3_cycle]  = FI.Reverser3Cycle,								
		[re3_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Reverser3AsOfDT),								
		[re4_hrs]    = FI.Reverse4Hrs,								
		[re4_cycle]  = FI.Reverser4Cycle,								
		[re4_asof]   = dbo.GetShortDateFormatByUserCD(@UserCD,FI.Reverser4AsOfDT),								
		[mach_fuel1hrs]  = FI.MACHFuel1Hrs,								
		[mach_fuel2hrs]  = FI.MACHFuel2Hrs,								
		[mach_fuel3hrs]  = FI.MACHFuel3Hrs,								
		[mach_fuel4hrs]  = FI.MACHFuel4Hrs,								
   		[mach_fuel5hrs]  = FI.MACHFuel5Hrs,								
		     [mach_fuel6hrs]  = FI.MACHFuel6Hrs,								
		[mach_fuel7hrs]  = FI.MACHFuel7Hrs, 								
		[mach_fuel8hrs]  = FI.MACHFuel8Hrs,								
		[mach_fuel9hrs]  = FI.MACHFuel9Hrs,								
		[mach_fuel10hrs] = FI.MACHFuel10Hrs,								
		[mach_fuel11hrs] = FI.MACHFuel11Hrs,								
		[mach_fuel12hrs] = FI.MACHFuel12Hrs,								
		[mach_fuel13hrs] = FI.MACHFuel13Hrs,								
		[mach_fuel14hrs] = FI.MACHFuel14Hrs,								
		[mach_fuel15hrs] = FI.MACHFuel15Hrs,								
		[lrc_fuel1hrs]   = FI.LongRangeFuel1Hrs,  								
		[lrc_fuel2hrs]   = FI.LongRangeFuel2Hrs,								
		[lrc_fuel3hrs]   = FI.LongRangeFuel3Hrs,								
		[lrc_fuel4hrs]   = FI.LongRangeFuel4Hrs, 								
		[lrc_fuel5hrs]   = FI.LongRangeFuel5Hrs,								
		[lrc_fuel6hrs]   = FI.LongRangeFuel6Hrs, 								
		[lrc_fuel7hrs]   = FI.LongRangeFuel7Hrs,								
		[lrc_fuel8hrs]   = FI.LongRangeFuel8Hrs, 								
		[lrc_fuel9hrs]   = FI.LongRangeFuel9Hrs,								
		[lrc_fuel10hrs]  = FI.LongRangeFuel10Hrs, 								
		[lrc_fuel11hrs]  = FI.LongRangeFuel11Hrs, 								
		[lrc_fuel12hrs]  = FI.LongRangeFuel12Hrs,								
		[lrc_fuel13hrs]  = FI.LongRangeFuel13Hrs,								
		[lrc_fuel14hrs]  = FI.LongRangeFuel14Hrs,								
		[lrc_fuel15hrs]  = FI.LongRangeFuel15Hrs, 								
		[hrc_fuel1hrs]   = FI.HighSpeedFuel1Hrs,								
		[hrc_fuel2hrs]   = FI.HighSpeedFuel2Hrs,								
		[hrc_fuel3hrs]   = FI.HighSpeedFuel3Hrs,								
		[hrc_fuel4hrs]   = FI.HighSpeedFuel4Hrs,								
		[hrc_fuel5hrs]   = FI.HighSpeedFuel5Hrs,								
		[hrc_fuel6hrs]   = FI.HighSpeedFuel6Hrs,								
		[hrc_fuel7hrs]   = FI.HighSpeedFuel7Hrs,								
		[hrc_fuel8hrs]   = FI.HighSpeedFuel8Hrs,  								
		[hrc_fuel9hrs]   = FI.HighSpeedFuel9Hrs, 								
		[hrc_fuel10hrs]  = FI.HighSpeedFuel10Hrs,								
		[hrc_fuel11hrs]  = FI.HighSpeedFuel11Hrs,								
		[hrc_fuel12hrs]  = FI.HighSpeedFuel12Hrs,								
		[hrc_fuel13hrs]  = FI.HighSpeedFuel13Hrs,								
		[hrc_fuel14hrs]  = FI.HighSpeedFuel14Hrs,								
		[hrc_fuel15hrs]  = FI.HighSpeedFuel51Hrs,								
		[powr_set]       = A.PowerSetting,								
		[comp_desc]      = FC.FleetComponentDescription,								
		[cmplastinspdt]  = dbo.GetShortDateFormatByUserCD(@UserCD,FC.LastInspectionDT),								
		[hdc_flag]       = FC.IsHrsDaysCycles,								
		[cmpinsp_hrs]    = ISNULL(FC .InspectionHrs,'0'),								
		[cmpwarn_hrs]    = ISNULL(FC .WarningHrs,'0'), 								
		[rtyfixrf]       = A.IsFixedRotary,								
		[infocode]       = FPI.FleetInfoCD, 								
		[infodesc]       = FPI.FleetProfileAddInfDescription,								
		--[infovalue]      = dbo.flightpakdecrypt(ISNULL(FPD.InformationValue,'')),	
		[infovalue]      = ISNULL(FPD.InformationValue,''),													
		[ratebeg]        = dbo.GetShortDateFormatByUserCD(@UserCD,FCR.BeginRateDT),								
		[rateend]        = dbo.GetShortDateFormatByUserCD(@UserCD,FCR.EndRateDT), 								
		[chrg_rate]      = ISNULL(FCR.ChargeRate,'0'),								
		[chrg_unit]      = FCR.ChargeUnit,								
		[section]        = '0'							

    
		 FROM  Aircraft A
		   INNER JOIN Fleet F
				   ON A.AircraftID = F.AircraftID
		   LEFT JOIN FleetChargeRate FCR
				   ON F.FleetID = FCR.FleetID
		   LEFT JOIN FleetComponent FC
				   ON F.FleetID = FC.FleetID
		   LEFT JOIN FleetInformation FI
				   ON F.FleetID = FI.FleetID
		   LEFT JOIN FleetPair FP
				   ON F.FleetID = FP.FleetID
		   LEFT JOIN FleetProfileDefinition FPD 
				   ON F.FleetID = FPD.FleetID  
		   LEFT JOIN FleetProfileInformation FPI
				   ON FPD.FleetProfileInformationID = FPI.FleetProfileInformationID
		   LEFT OUTER JOIN Client C
				   ON F.ClientID = C.ClientID
		   LEFT JOIN EmergencyContact EC
				   ON EC.EmergencyContactID = F.EmergencyContactID
		   LEFT JOIN Country CY
				   ON FP.CountryID = CY.CountryID
		   LEFT JOIN Vendor V
				   ON F.VendorID = V.VendorID	
		   left join Crew CW
		           ON F.CrewID = CW.CrewID
		   LEFT JOIN Company CM
	             ON F.HomebaseID = CM.HomebaseAirportID
	      LEFT JOIN Airport AP
	             ON CM.HomebaseAirportID = AP.AirportID	 
				   
		 WHERE F.IsDeleted = 0  
		 AND F.CustomerID = @CUSTOMERID  
		 AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL ) 
		 AND F.TailNum = RTRIM(@TailNum)  
  
-- EXEC spGetReportFleetProfileMhtmlInformation 'SUPERVISOR_99', 'N46E'


GO


