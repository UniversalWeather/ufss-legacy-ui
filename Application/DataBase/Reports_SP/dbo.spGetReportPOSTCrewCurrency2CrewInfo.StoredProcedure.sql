IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrency2CrewInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2CrewInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
         
CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrency2CrewInfo]                        
( 
 @UserCD Varchar(30), 
 @AsOfDate DateTime, 
 @CrewCD Varchar(2000)='',              
 @CrewGroupCD Varchar(2000)='',
 @AircraftCD Varchar(5000)='',
 @IsInactive Bit=0
)                      
AS                                          
-- ===============================================================================                      
-- SPC Name: spGetReportPOSTCrewCurrency2Information                       
-- Author:  Mathes                      
-- Create date: 30 Aug 2012                      
-- Description: Get Crew Currency for REPORTS                      
-- Revision History                      
-- Date   Name  Ver  Change                      
--                       
-- ================================================================================                      
  
SET NOCOUNT ON                           
BEGIN

 Declare @DateRange Varchar(30)
 Set @DateRange= dbo.GetDateFormatByUserCD(@UserCD,@AsOfDate)  
 
   SELECT DISTINCT  
   C.CrewID,C.CrewCD,C.LastName,C.FirstName,
   C.FirstName + ' ' + C.MiddleInitial + ' ' + C.LastName As CrewName ,
   @DateRange DateRange 
   FROM PostflightLeg INNER JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0 INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID left JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID 
                      LEFT JOIN                      
   ( SELECT DISTINCT C.CrewID,C.CrewCD,C.FirstName,C.LastName,C.CustomerID,C.IsStatus,C.MiddleInitial
     FROM  Crew C                 
     left JOIN CrewGroupOrder CGO                
     ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID                 
     left JOIN CrewGroup CG                 
     ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID   
   ) C ON C.CrewID = PostflightCrew.CrewID 
   WHERE C.IsStatus =1 AND PostflightLeg.CustomerID =   + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                             
   AND Convert(Date,PostflightLeg.ScheduledTM)<=@AsOfDate  
   AND PostflightLeg.IsDeleted=0  
  
END