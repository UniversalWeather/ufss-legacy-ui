IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItinerarySubInformation1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation1]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@LegID AS VARCHAR(30)   --Mandatory

AS
-- ==========================================================================================
-- SPC Name: spGetReportPRECorporateItinerarySubInformation1
-- Author: ABHISHEK.S
-- Create date: 7th NOV 2012
-- Description: Get Corporate Itinerary Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
SET NOCOUNT ON
	BEGIN 
SELECT DISTINCT
			[TripID] = PM.TripID,
			[FileNo] = PM.TripNUM,
			[LegID] = PL.LegID,						
			[Passengers] = ISNULL(P.LastName,'')+ Case when ISNULL(P.LastName,'')='' then '' Else ', ' End + ISNULL(P.FirstName,'') + ' ' +ISNULL(P.MiddleInitial,'') ,
			[PaxAccomodations] = HP.Name,
			[pax_hotel_phone] = HP.PhoneNum 
			
		FROM PreflightMain PM 
		         INNER JOIN PreflightLeg PL
		                ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
				 INNER JOIN PreflightPassengerList PPL 
						ON PPL.LegID = PL.LegID
				INNER JOIN Passenger P
						ON PPL.PassengerID = P.PassengerRequestorID
				 INNER JOIN PreflightPassengerHotelList PPH
						ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
				 INNER JOIN Hotel HP
				        ON PPH.HotelID = HP.HotelID 
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		AND PL.LegID = convert(Bigint,@LegID)
		AND PM.IsDeleted = 0

		 
		  
	END
	
	-- EXEC spGetReportPRECorporateItinerarySubInformation1 'jwilliams_13','10013104304'
GO


