IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFuelSlipTransactionsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFuelSlipTransactionsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTFuelSlipTransactionsExportInformation]
(
@UserCD AS VARCHAR(30), --Mandatory
@DATEFROM AS DATETIME, --Mandatory
@DATETO AS DATETIME, --Mandatory
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
@FleetGroupCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTFuelSlipTransactionsExportInformation
-- Author:  A.Akhila
-- Create date: 6 Aug 2012
-- Description: Get Fuel Slip Transactions for EXPORT REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON  
declare @units varchar(15) 
set @units=(select convert(varchar,(CASE WHEN (cp.FuelPurchase='1') THEN 'GAL'
WHEN (cp.FuelPurchase='2') THEN 'LTR'
WHEN (cp.FuelPurchase='3') THEN 'IMP'
WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
END)) from Company cp where 
CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	---LEFT OUTER JOIN FleetGroupOrder FGO
	--ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	--LEFT OUTER JOIN FleetGroup FG 
	--ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	---AND 
	F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration----------------------
	SELECT DISTINCT
    [slipnum] = SlipNUM
   ,[purchasedt] = CONVERT(DATE,PurchaseDT)
   ,[tail_nmbr] = F.TailNum
   ,[dispatchno] = case when POE.DispatchNUM =''then '0' else CONVERT(VARCHAR(12),POE.DispatchNUM) end
   ,[icao_id] = A.IcaoID
   ,[airport_nm] = A.AirportName 
   ,[fbo_name] = FB.FBOVendor
   ,[pay_type] = PT.PaymentTypeCD
   ,[paydesc] = PT.PaymentTypeDescription
   ,[fbo_code] = FB.FBOCD
   ,[fuel_loc] = FL.FuelLocatorCD
   ,[invnum] = InvoiceNUM
   ,[fuelpurch ] = ISNULL(POE.FuelPurchase,0)
   ,[fuelqty] = CAST(ROUND((dbo.GetFuelConversion(POE.FuelPurchase,POE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))
   ,[unitprice] = Case WHEN C.FuelPurchase <> POE.FuelPurchase THEN DBO.GetFuelPriceConversion(POE.FuelPurchase,POE.UnitPrice,@UserCD)
                                               ELSE POE.UnitPrice End
   ,[expamt] = ISNULL(ExpenseAMT,0)
   ,[postfuelprice] = Case WHEN C.FuelPurchase <> POE.FuelPurchase THEN DBO.GetFuelPriceConversion(POE.FuelPurchase,POE.PostFuelPrice,@UserCD)
                                               ELSE POE.PostFuelPrice End
   ,[costdiff] = ''
   ,[percentdiff] = ''

   INTO #TEMP
FROM [PostflightExpense]POE 

INNER JOIN Company C ON C.HomebaseID = POE.HomebaseID 
LEFT JOIN FuelLocator FL ON POE.FuelLocatorID = FL.FuelLocatorID  
LEFT OUTER JOIN FBO FB ON POE.FBOID = FB.FBOID AND FB.AirportID = POE.AirportID  
LEFT JOIN Airport A ON POE.AirportID = A.AirportID  
LEFT JOIN PostflightMain POM ON POE.POLogID = POM.POLogID AND POM.IsDeleted=0
LEFT JOIN PaymentType PT ON POE.PaymentTypeID = PT.PaymentTypeID  

INNER JOIN Fleet F ON F.FleetID = POM.FleetID
		 INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1 ON F.FleetID=F1.FleetID


WHERE   POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
AND CONVERT(DATE,POE.PurchaseDT) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
AND POE.IsDeleted=0


IF @TailNum <> '' OR @FleetGroupCD <> '' 
BEGIN

SELECT * FROM #TEMP

END
ELSE 
BEGIN
SELECT * FROM #TEMP
END

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
DROP TABLE #TEMP

--EXEC spGetReportPOSTFuelSlipTransactionsExportInformation 'supervisor_99','2009-01-01','2010-12-12', '', ''

GO


