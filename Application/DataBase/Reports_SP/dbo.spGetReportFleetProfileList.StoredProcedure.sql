IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFleetProfileList]
	@UserCD VARCHAR(30)
	,@TailNum char(1000)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileList
-- Author: Anil Singh
-- Create date: 07/09/2015
-- Description: Get Fleet Profile List for Report 
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

DECLARE @TenToMin INT
SELECT @TenToMin = Company.TimeDisplayTenMin FROM Company WHERE CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
                    AND Company.HomebaseID = dbo.GetHomeBaseByUserCD(@UserCD) 

	SELECT DISTINCT
		 [AcrftCD]    = F.AircraftCD
		,[AcrftTN]    = F.TailNum
		,[AcrftTCD]   = A.AircraftCD
		,[TypeDes]    = F.TypeDescription
		,[FRWing]     = A.IsFixedRotary
		,[SNum]		  = F.SerialNum
		,[Status]     = F.IsInActive
		,[DFPhn]      = F.FlightPhoneNum
		
	FROM Fleet F 
	     LEFT OUTER JOIN FleetComponent FC
			     ON F.CustomerID = FC.CustomerID
		        AND F.FleetID = FC.FleetID
	     LEFT OUTER JOIN Aircraft A
			     ON F.AircraftID = A.AircraftID
	     
	WHERE F.IsDeleted = 0
	  AND F.CustomerID = @CUSTOMERID
	  AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	  AND F.TailNum in (
		SELECT s FROM [dbo].[SplitString] (RTRIM(@TailNum),',')
	  )

GO


