IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSheetItineraryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSheetItineraryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spGetReportPRETSTripSheetItineraryExportInformation]
	@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
    ,@ReportHeaderID VARCHAR(30) = '' -- 10001281316 TemplateID from TripSheetReportHeader table
    ,@TempSettings VARCHAR(MAX) = '' --'PAXMNORD::TRUE' PONEPERLIN::FALSE PAXORDER
AS

BEGIN

-- =========================================================================
-- SPC Name: spGetReportPRETSTripSheetItineraryExportInformation
-- Author: SINDHUJA.K
-- Create date: 13 Sep 2012
-- Description: Get Preflight Tripsheet Itinerary Report Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --



DECLARE @FormatSettings TABLE (
			LMESSAGE	BIT
		,	MESSAGEPOS	SMALLINT
		,	[MESSAGE]	VARCHAR(100)
		,	ONELEG	BIT
		,	LEGNOTES	SMALLINT
		,	LEGCREW	SMALLINT
		,	ETEHRS	SMALLINT
		,	PONEPERLIN	BIT
		,	BLANKLINES	BIT	
		,	PCNG	BIT
		,	PBUSPERS	BIT
		,	PDEPTCODE	BIT
		,	PDEPTDESC	BIT
		,	CDNAME	BIT
		,	CDPHONE	BIT
		,	CDREMARKS	BIT
		,	PAXHOTEL	BIT
		,	HOTELPHONE	BIT
		,	HOTELREM	BIT
		,	PTANAME	BIT
		,	PTAPHONE	BIT
		,	PTAREMARKS	BIT
		,	CREWHOTEL	BIT
		,	CREWHPHONE	BIT
		,	CREWHADDR	BIT
		--,	MAINT	BIT
		--,	MAINTPHONE	BIT
		--,	MAINTADDR	BIT
		,	FILTFERRY	BIT
		,	SUPPAXNOTE	BIT
		,	SUPCRWNOTE	BIT
		,	DISPATCHNO	BIT
		,	PURPOSE	BIT
		,	AIRFLTPHN	BIT
		,	ALLOWSHADE	BIT
		,	ARRFBP	BIT
		,	PTACONFIRM	BIT
		,	PTACOMMENT	SMALLINT
		,	PTAFAX	BIT
		,	PTDNAME	BIT
		,	PTDPHONE	BIT
		,	PTDCONFIRM	BIT
		,	PTDCOMMENT	SMALLINT
		,	PTDREMARKS	BIT
		,	PTDFAX	BIT
		,	CTANAME	BIT
		,	CTAPHONE	BIT
		,	CTARATE	BIT
		,	CTACONFIRM	BIT
		,	CTACOMMENT	SMALLINT
		,	CTAREMARKS	BIT
		,	CTAFAX	BIT
		,	CTDNAME	BIT
		,	CTDPHONE	BIT
		,	CTDRATE	BIT
		,	CTDCONFIRM	BIT
		,	CTDCOMMENT	SMALLINT
		,	CTDREMARKS	BIT
		,	CTDFAX	BIT
		,	CDCONFIRM	BIT
		,	CDCOMMENT	SMALLINT
		,	CDFAX	BIT
		,	CANAME	BIT
		,	CAPHONE	BIT
		,	CACONFIRM	BIT
		,	CACOMMENT	SMALLINT
		,	CAREMARKS	BIT
		,	CAFAX	BIT
		,	HOTELROOMS	BIT
		,	HOTELCONF	BIT
		,	HOTELCOMM	SMALLINT
		,	HOTELFAX	BIT
		,	CREWHNRATE	BIT
		,	CREWHROOMS	BIT
		,	CREWHCONF	BIT
		,	CREWHCOMM	SMALLINT
		,	CREWHFAX	BIT
		,	CREWHREM	BIT
		,	OUTBOUND	BIT
		,	TERMTOAFBO	SMALLINT
		,	DEPFBP	BIT
		,	TERMTODFBO	SMALLINT
		--,	MAINTFAX	BIT
		--,	MAINTNRATE	BIT
		--,	MAINTROOMS	BIT
		--,	MAINTCONF	BIT
		--,	MAINTCOMM	SMALLINT
		--,	MAINTREM	BIT
		,	PAXNBR	BIT
		,	PRINTLOGO	BIT
		,	LINESEP	BIT
		,	REQUESTOR	BIT
		,	DEPICAO	BIT
		,	ARRICAO	BIT
		,	PRNFIRSTPG	BIT
		,	INTLFLTPHN	BIT
		,	CREWPILOT1	VARCHAR(100)
		,	CREWADDL	VARCHAR(100)
		,	CREWPILOT2	VARCHAR(100)
		,	FBOALPRICE	BIT
		,	FBOALDATE	BIT
		,	FBOACONT	BIT
		,	FBOAADDR	BIT
		,	FBOAREM	BIT
		,	FBOAPHONE2	BIT
		,	FBOAPAYTYP	BIT
		,	FBOAPOSTPR	BIT
		,	FBOAFUELQT	BIT
		,	FBOACONF	BIT
		,	FBOACOMM	SMALLINT
		,	FBODPHONE	BIT
		,	FBODFREQ	BIT
		,	FBODFAX	BIT
		,	FBODUVAIR	BIT
		,	FBODFUEL	BIT
		,	FBODNPRICE	BIT
		,	FBODLPRICE	BIT
		,	FBODLDATE	BIT
		,	FBODCONT	BIT
		,	FBODADDR	BIT
		,	FBODREM	BIT
		,	FBODPHONE2	BIT
		,	FBODPAYTYP	BIT
		,	FBODPOSTPR	BIT
		,	FBODFUELQT	BIT
		,	FBODCONF	BIT
		,	FBODCOMM	SMALLINT
		,	FBOAPHONE	BIT
		,	FBOAFREQ	BIT
		,	FBOAFAX	BIT
		,	FBOAUVAIR	BIT
		,	FBOAFUEL	BIT
		,	FBOANPRICE	BIT
		,	DISPATCHER	BIT
		,	DSPTEMAIL	BIT
		,	DSPTPHONE	BIT
		,	TRIPNO	BIT
		,	[DESC]	BIT
		,	TRIPDATES	BIT
		,	TMREQ	BIT
		,	TAILNMBR	BIT
		,	CONTPHONE	BIT
		,	TYPECODE	BIT
		,	CQDESC	SMALLINT
		,	REVISNNO	BIT
		,	REVISNDESC	BIT
		,	TIMEFORMAT	BIT
		,	CRMBLPHN	SMALLINT
		,	HOTELADDR	BIT
		,	PAXORDER	BIT
		,	CHRMONTH	BIT
		,	LEGSEP		BIT
		,	LEGNOTESF	SMALLINT
		,	CQNAME		SMALLINT
		,	CQCONTACT	SMALLINT
       ,   DISPATCHIN  SMALLINT
       ,DcrewHotel bit
       ,DCREWHNRTE bit
       ,DCREWHADDR bit
       ,DCREWHCOMM smallint
       ,DCREWHCONF bit
       ,DCREWHFAX bit
       ,DCREWHROOM bit
       ,DCREWHPHON bit
       ,DCREWHREM bit
       ,DPAXHOTEL bit
       ,DPAXHPHONE bit
       ,DPAXHREM bit
       ,DPAXHROOMS bit
       ,DPAXHCONF bit
       ,DPAXHCOMM smallint
       ,DPAXHFAX bit
       ,DPAXHADDR bit
		)
		
	

INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 3, @TempSettings

DECLARE @DEPFBP	BIT,@PTDNAME BIT,@PTANAME BIT,@PAXHOTEL BIT,@CTDNAME BIT,@CTANAME BIT,@CREWHOTEL BIT,@MAINT	BIT,@LEGNOTES SMALLINT,@OUTBOUND BIT

SELECT @DEPFBP=DEPFBP,@PTDNAME=PTDNAME,@PTANAME=PTANAME,@PAXHOTEL=PAXHOTEL,@CTDNAME=CTDNAME,@CTANAME=CTANAME,
       @CREWHOTEL=CREWHOTEL,@MAINT=null,@LEGNOTES=LEGNOTES,@OUTBOUND=OUTBOUND FROM @FormatSettings



--SELECT @DEPFBP=0,@PTDNAME=0,@PTANAME=0,@PAXHOTEL=0,@CTDNAME=0,@CTANAME=0,
--       @CREWHOTEL=0,@MAINT=0,@LEGNOTES=0,@OUTBOUND=0 --FROM @FormatSettings


---------------



DECLARE  @tblFlag TABLE  
	(	
	 ROWID INT
	,flag CHAR(1)
	)
	IF @DEPFBP=0    BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (1,'A') END
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (2,'B')
	IF @PTDNAME=0   BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (3,'O') END
	IF @PTANAME=0   BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (4,'D') END
	--IF @PAXHOTEL=0  BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (5,'C') END
	IF @CTDNAME=0   BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (6,'P') END
	IF @CTANAME=0   BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (7,'F') END 
	--IF @CREWHOTEL=0 BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (8,'E') END 
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (9,'H')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (10,'Q')
	--IF @MAINT=0    BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (11,'G') END
	--IF @LEGNOTES<>4 
	--BEGIN 
	--INSERT INTO @tblFlag(ROWID, FLAG) VALUES (12,'K') 
	
	--END
	--IF @OUTBOUND=0 BEGIN INSERT INTO @tblFlag(ROWID, FLAG) VALUES (13,'L') END
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (14,'I')
	--INSERT INTO @tblFlag(ROWID, FLAG) VALUES (13,'J')

-----------------
--SELECT T.flag,TripItinerary.* FROM 


SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CONVERT(VARCHAR(5),'') CrewCD
		INTO #TripItineray
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN (SELECT P.*,ROW_NUMBER()OVER(PARTITION BY PP.LEGID ORDER BY PP.PassengerID) RNK FROM PreflightPassengerList  PP INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID) Pax ON PP.PassengerID = Pax.PassengerRequestorID AND PAX.RNK=1
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))


SELECT TempTrip.flag,* FROM(
SELECT * FROM #TripItineray CROSS JOIN @tblFlag T

UNION ALL

SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CONVERT(VARCHAR(5),'') CrewCD
		 ,[ROWID]=12
		 ,FLAG='K' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN (SELECT P.*,ROW_NUMBER()OVER(PARTITION BY PP.LEGID ORDER BY PP.PassengerID) RNK FROM PreflightPassengerList  PP INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID) Pax ON PP.PassengerID = Pax.PassengerRequestorID AND PAX.RNK=1
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
		INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
AND PL.Notes IS NOT NULL
AND PL.Notes <> ''
AND @LEGNOTES<>4 

UNION ALL

SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CONVERT(VARCHAR(5),'') CrewCD
		 ,[ROWID]=13
		 ,FLAG='L' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN (SELECT P.*,ROW_NUMBER()OVER(PARTITION BY PP.LEGID ORDER BY PP.PassengerID) RNK FROM PreflightPassengerList  PP INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID) Pax ON PP.PassengerID = Pax.PassengerRequestorID AND PAX.RNK=1
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
		INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
AND PL.OutbountInstruction IS NOT NULL
AND PL.OutbountInstruction <> ''
AND @OUTBOUND=0 

UNION ALL
---C - PAX HOTEL
SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CrewCD=NULL
	    ,[ROWID]=5
		 ,FLAG='C' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		LEFT OUTER JOIN Passenger Pax ON PP.PassengerID = Pax.PassengerRequestorID
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID --AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
		INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
  AND @PAXHOTEL=0

UNION ALL
--E - CREW HOTEL 

SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		 ,CrewCD=CH.Crew
		 ,[ROWID]=8
		 ,FLAG='E' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN (SELECT P.*,ROW_NUMBER()OVER(PARTITION BY PP.LEGID ORDER BY PP.PassengerID) RNK FROM PreflightPassengerList  PP INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID) Pax ON PP.PassengerID = Pax.PassengerRequestorID AND PAX.RNK=1
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,PCLL.CrewID,C.CrewCD Crew
			 --ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
				INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			INNER JOIN Crew C ON PCLL.CrewID=C.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID-- AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
		INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
 AND @CREWHOTEL=0 

UNION ALL

--G - MAINT HOTEL
SELECT DISTINCT 
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CrewCD=ACH.CrewCD
		,[ROWID]=11
		 ,FLAG='G' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN (SELECT P.*,ROW_NUMBER()OVER(PARTITION BY PP.LEGID ORDER BY PP.PassengerID) RNK FROM PreflightPassengerList  PP INNER JOIN Passenger P ON PP.PassengerID=P.PassengerRequestorID) Pax ON PP.PassengerID = Pax.PassengerRequestorID AND PAX.RNK=1
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID			
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID,C.CrewCD 
			 ---  ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		INNER JOIN Crew C ON PCLL.CrewID=C.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID --AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
 -- AND @MAINT=0

--SELECT * FROM #Trip CROSS JOIN @tblFlag T


UNION ALL

--J - FOR EACH PAX
SELECT DISTINCT
         [lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[descrptn] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD
		,[requestor] = LReq.PassengerRequestorCD
		,[reqname] =   ISNULL(LReq.LastName+ ', ','') + ISNULL(LReq.FirstName,'')+ ' ' + ISNULL(LReq.MiddleInitial,'')
		,[paxname] =   ISNULL(Req.LastName+ ', ','') +ISNULL(Req.FirstName,'') + ' ' + ISNULL(Req.MiddleInitial,'')
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[legid] = PL.LegID
		,[timeapprox] = PL.IsApproxTM
		,[depicao_id] = AD.IcaoID
		,[depnamecitystate] = AD.AirportName + ', ' + AD.CityName + ', ' + AD.StateName
		,[arricao_id] = AA.IcaoID
		,[arrnamecitystate] = AA.AirportName + ', ' + AA.CityName + ', ' + AA.StateName
		,[pax_total] = PL.PassengerTotal
		,[depzulu] = AD.OffsetToGMT
		,[arrzulu] = AA.OffsetToGMT
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[gmtdep] = PL.DepartureGreenwichDTTM
		,[gmtarr] = PL.ArrivalGreenwichDTTM
		,[distance] = PL.Distance
		,[elp_time] = PL.ElapseTM -- Always in Minutes format
		,[c_elp_time] = PL.ElapseTM -- Always in TENTHs format
		,[code] = LF.FBOCD --PL.FBOID
		,[purpose] = PL.FlightPurpose
		--,[Notes] = ''
		--,[CrewNotes] = ''
		--,[Message] = ''
		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rooms] = PH.NoofRooms
		--,[hotel_Confirm] = ''
		--,[hotel_comments] = ''
		--,[hotel_remarks] = ''
		--(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = ISNULL(TCA.NegotiatedRate,0)
		--,[crewta_remarks] = ''
		--,[crewta_confirm] = ''
		--,[crewta_comments] = ''
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = ISNULL(TCD.NegotiatedRate,0)		
		--,[crewtd_remarks] = ''
		--,[crewtd_confirm] = ''
		--,[crewtd_comments] = ''
		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		--,[paxta_confirm] = ''
		--,[paxta_comments] = ''
		--,[paxta_remarks] = ''
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		--,[paxtd_confirm] = ''
		--,[paxtd_comments] = ''
		--,[paxtd_remarks] = ''
		 --(Crew Hotel)
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = ISNULL(CH.NegociatedRate,0)
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		--,[crewh_remarks] = ''
		,[crewh_rooms] =ISNULL(CH.NoofRooms,0)
		--,[crewh_confirm] = ''
		--,[crewh_comments] = ''
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ISNULL(ACH.NegociatedRate,0)
		,[main_rooms] = ISNULL(ACH.NoofRooms,0)
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		--,[main_remarks] = ''
		--,[main_confirm] = ''
		--,[main_comments] = ''
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		--,[catera_remarks] = ''
		--,[catera_confirm] = ''
		--,[catera_comments] = ''
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = ISNULL(CD.NegotiatedRate,0)
		--,[caterd_remarks] = ''
		--,[caterd_confirm] = ''
		--,[caterd_comments] = ''
		--,[Passengers] = ''
		--,[Outbound] = ''
		-----(From Fbo)
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[arrfbo_phone2] = FBA.PhoneNUM2
		,[arrfbo_freq]= FBA.Frequency
		,[arrfbo_fax] = FBA.FaxNum
		,[arrfbo_uvair] = ISNULL(FBA.IsUWAAirPartner,0)
		,[arrfbo_fuel_brand] = FBA.FuelBrand 
		,[arrfbo_negfuelpr] = ISNULL(FBA.NegotiatedFuelPrice,0)
		,[arrfbo_lastfuelpr] = ISNULL(FBA.LastFuelPrice,0)
		,[arrfbo_lastfueldt] =  FBA.LastFuelDT
		,[arrfbo_contact] = FBA.Contact
		,[arrfbo_addr1] = FBA.Addr1
		,[arrfbo_addr2] = FBA.Addr2
		,[arrfbo_paytype] = FBA.PaymentType
		,[arrfbo_postedpr] = ISNULL(FBA.PostedPrice,0)
		,[arrfbo_fuelqty] = ISNULL(FBA.FuelQty,0)
		--,[arrfbo_remarks] = ''
		--,[arrfbo_confirm] = ''
		--,[arrfbo_comments] = ''
		------(To Fbo)
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[depfbo_phone2] = FBD.PhoneNUM2
		,[depfbo_freq] = FBD.Frequency
		,[depfbo_fax] = FBD.FaxNum
		,[depfbo_uvair] = FBD.IsUWAAirPartner
		,[depfbo_fuel_brand] = FBD.FuelBrand
		,[depfbo_negfuelpr] = ISNULL(FBD.NegotiatedFuelPrice,0)
		,[depfbo_lastfuelpr] = ISNULL(FBD.LastFuelPrice,0)
		,[depfbo_lastfueldt] = FBD.LastFuelDT
		,[depfbo_contact] = FBD.Contact
		,[depfbo_addr1] = FBD.Addr1
		,[depfbo_addr2] = FBD.Addr2
		,[depfbo_paytype] = FBD.PaymentType
		,[depfbo_postedpr] = ISNULL(FBD.PostedPrice,0)
		,[depfbo_fuelqty] = ISNULL(FBD.FuelQty,0)
		--,[depfbo_remarks] = ''
		--,[depfbo_confirm] = ''
		--,[depfbo_comments] = ''
		,[pic] = ISNULL(PIC.FirstName, '') + ' ' + ISNULL(PIC.LastName, '')-- + ' ' + ISNULL(PIC.MiddleInitial, '')
		,[sic] = ISNULL(SIC.FirstName, '') + ' ' + ISNULL(SIC.LastName, '')-- + ' ' + ISNULL(SIC.MiddleInitial, '')
		,[addlcrew] = CASE WHEN LEN(CR.Crewnames) > 1 THEN LEFT(CR.Crewnames, LEN(CR.Crewnames) - 1) ELSE '' END -- TRIM Tailing ', '
		,[ac_code] = F.AircraftCD
		,[pic_mblphn] = PIC.CellPhoneNum
		,[sic_mblphn] = SIC.CellPhoneNum
		,[fltphone] = F.FlightPhoneNum
		,[infltphone] =F.FlightPhoneIntlNum
		,[crewfield] = ''
		,[dispatcher] = UM.UserName
		,[dsptname] =  ISNULL(UM.FirstName,'') + ' ' + ISNULL(UM.LastName,'') --PM.DsptnName
		,[dsptemail] = UM.EmailID
		,[dsptphone] = UM.PhoneNum
		,[revisndesc] = PM.RevisionDescriptioin
		,[revisnnmbr] = ISNULL(PM.RevisionNUM,0)
		,[auth_desc] = CASE PM.IsQuote WHEN 1 THEN DA.DeptAuthDescription ELSE '' END
		,[reqauth] =   CASE PM.IsQuote WHEN 1 THEN DA.AuthorizationCD ELSE '' END --RA.AuthorizationCD
		,[cqflag] = PM.IsQuote
		,[custphone] = 'R2'--CQFILE.DBF/PHONE
		--(Dep FBO)
		,[depfbo_city] =  FBD.CityName
		,[depfbo_state] = FBD.StateName
		,[depfbo_zip] = FBD.PostalZipCD
		--(Arr FBO)
		,[arrfbo_city] = FBA.CityName
		,[arrfbo_state] = FBA.StateName
		,[arrfbo_zip] = FBA.PostalZipCD
		--(Crew Hotel)
		,[crewh_city] = CH.CityName
		,[crewh_state] = CH.StateName
		,[crewh_zip] = CH.PostalZipCD
		--(Pax Hotel)
		,[hotel_addr1] = PH.Addr1
		,[hotel_addr2] = PH.Addr2
		,[hotel_city] = PH.CityName
		,[hotel_state] = PH.StateName
		,[hotel_zip] = PH.PostalZipCD
		--(Addl Crew Hotel)
		,[main_city] = ACH.CityName
		,[main_state] = ACH.StateName
		,[main_zip] = ACH.PostalZipCD
		--,[nbr] = PL.PassengerTotal --No. of Passengers
		,[nbr] = PH.RNK
		,[paxcode] = Pax.PassengerRequestorCD
		,[nonpax] = PP.IsNonPassenger --PreflightPassengerList.IsNonPassenger
		,[blocked] = PP.IsBlocked
		,[blocksort] = CONVERT(SMALLINT,PP.IsBlocked) --IF PreflightPassengerList.IsBlocked = .T. display 1 else display 0
		,[passname] = ISNULL(Pax.LastName+', ' , '') + ISNULL(Pax.FirstName, '')  + ' ' + ISNULL(Pax.MiddleInitial, '') --PreflightPassengerList.PassengerLastName, PassengerFirstName PassengerMiddleName
		,[dept_code] = D.DepartmentCD
		,[emp_t_cng] = Pax.IsEmployeeType
		,[fltpurpose] = FP.FlightPurposeCD --FlightPurpose.FlightPurposeCD
		,[dept_desc] = D.DepartmentName
		,CrewCD=NULL
		 ,[ROWID]=15
		 ,FLAG='J' 
		FROM @tblTripInfo M
		INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
		INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
		INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
		INNER JOIN PreflightPassengerList PP ON PL.LEGID = PP.LegID --AND M.PassengerID = PP.PassengerID
		INNER JOIN Passenger Pax ON PP.PassengerID = Pax.PassengerRequestorID
		LEFT OUTER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND RTRIM(PM.DispatcherUserName) = RTRIM(UM.UserName)
		 --FOR PIC
		INNER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'P'
		) PIC ON PL.LegID = PIC.LegID
		LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		LEFT OUTER JOIN Passenger LReq ON PL.PassengerRequestorID = LReq.PassengerRequestorID
		LEFT OUTER JOIN Crew RC ON PM.CrewID = RC.CrewID
		--FOR SIC			
		LEFT OUTER JOIN(
			SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
			FROM PreflightCrewList PC
			INNER JOIN Crew C ON PC.CrewID = C.CrewID
			WHERE PC.DutyTYPE = 'S'
		) SIC ON PL.LegID = SIC.LegID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) DA ON PM.AuthorizationID = DA.AuthorizationID
		LEFT OUTER JOIN (
			SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription, AuthorizerPhoneNum FROM DepartmentAuthorization
		) RA ON Req.AuthorizationID = RA.AuthorizationID
		LEFT OUTER JOIN Department D ON PM.DepartmentID = D.DepartmentID 
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsDepartureFBO = 1
			) FBD ON PL.LegID = FBD.LegID
		LEFT OUTER JOIN (
				SELECT PFL.ConfirmationStatus, PFL.Comments, PFL.LEGID, F.* 
				FROM PreflightFBOList PFL
				INNER JOIN FBO F ON PFL.FBOID = F.FBOID AND PFL.IsArrivalFBO = 1
			) FBA ON PL.LegID = FBA.LegID
       LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 	) TPD ON PL.LegID = TPD.LegID
	   LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		) TPA ON PL.LegID = TPA.LegID
		LEFT OUTER JOIN (
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID			
			FROM PreflightTransportList PTL 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		                 ) TCD ON PL.LegID = TCD.LegID
		LEFT OUTER JOIN (  
			SELECT  [TransportationVendor]=PTL.PreflightTransportName ,[PhoneNum]=PTL.PhoneNum1,PTL.FaxNUM,
				[NegotiatedRate]=PTL.PhoneNum4,PTL.LegID
			FROM PreflightTransportList PTL 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		) TCA ON PL.LegID = TCA.LegID
		LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'D'
		) CD ON PL.LegID = CD.LegID
       LEFT OUTER JOIN (
			SELECT [CateringVendor] = PCL.ContactName, [PhoneNum] = PCL.ContactPhone, 
				[FaxNum] = PCL.ContactFax, [NegotiatedRate] = PCL.Cost, PCL.LegID
			FROM PreflightCateringDetail PCL 
			WHERE PCL.ArriveDepart = 'A'			
		) CA ON PL.LegID = CA.LegID	
		--Pax Hotel
		LEFT OUTER JOIN ( 
		SELECT [Name]=PPH.PreflightHotelName,[PhoneNum]=PPH.PhoneNum1,PPH.FaxNUM,[NegociatedRate]=PPH.Rate,
			   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments, [Addr1] = PPH.Address1, [Addr2] = PPH.Address2, 
			   PPH.CityName, PPH.StateName, PPH.PostalZipCD, PPL.LegID, PPL.PassengerID,
			   ROW_NUMBER()OVER(PARTITION BY PPL.LEGID ORDER BY PPL.PassengerID) RNK
          FROM PreflightPassengerList PPL 
		  INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		) PH ON PL.legID = PH.LegID AND PH.PassengerID =PP.PassengerID AND PH.RNK=1
       --Crew Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			 PCH.CityName, PCH.StateName, PCH.PostalZipCD,PCLL.LegID,
			 ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID AND CH.RNK=1
      --Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (	
		SELECT [Name]=PCH.PreflightHotelName,[PhoneNum]=PCH.PhoneNum1,PCH.FaxNUM,[NegociatedRate]=PCH.Rate,
			   PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments,[Addr1] = PCH.Address1, [Addr2] = PCH.Address2, 
			   PCH.CityName, PCH.StateName, PCH.PostalZipCD, PCLL.LegID ,
			   ROW_NUMBER()OVER(PARTITION BY PCLL.LEGID ORDER BY PCLL.CrewID) RNK
		FROM PreflightCrewList PCLL 
		--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C' AND PCH.isArrivalHotel = 1
		INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID AND PHCP.CrewID = PCLL.CrewID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID AND ACH.RNK=1
		LEFT OUTER JOIN (
		  SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewFirstName + ' ' + ISNULL(PC1.CrewLastName+' - ','') + ISNULL(C.CellPhoneNum + ', ','')
				FROM PreflightCrewList PC1 
				INNER JOIN (SELECT CellPhoneNum, CREWID FROM Crew) C ON PC1.CrewID = C.CrewID
				WHERE PC1.LegID = PC2.LegID
				AND PC1.DutyType NOT IN ('P', 'S')
				FOR XML PATH('')
				)
				FROM PreflightCrewList PC2
			) CR ON PL.LegID = CR.LegID
		LEFT OUTER JOIN ( SELECT FBOID, FBOCD FROM FBO ) LF ON PL.FBOID = LF.FBOID
		LEFT OUTER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON PP.FlightPurposeID = FP.FlightPurposeID
WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
)TempTrip
ORDER BY leg_num,ROWID,CASE WHEN flag='C' OR flag='J' THEN paxcode 
                            WHEN flag='E' OR flag='G' THEN CrewCD END





IF OBJECT_ID('tempdb..#TripItineray') IS NOT NULL
DROP TABLE #TripItineray




END

--EXEC spGetReportPRETSTripSheetItineraryExportInformation 'ELIZA_9', '3820', '', '', '', '', '', '', '', '', '', '', ''
--EXEC spGetReportPRETSTripSheetItineraryExportInformation 'UC', '', '', '', '20120720', '20120725', '', '', '', '', '', '', '','',1,1,1,1,1,1	


GO


