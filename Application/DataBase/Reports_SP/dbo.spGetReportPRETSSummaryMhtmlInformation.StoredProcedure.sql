IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSSummaryMhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSSummaryMhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSSummaryMhtmlInformation]  
 @UserCD            AS VARCHAR(30)      
 ,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM  
 ,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM  
 ,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)  
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal  
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal   
 ,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)  
 ,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)  
 ,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)  
 ,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)  
 ,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD  
 ,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase  
 ,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)  
 ,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD     
 ,@IsTrip   AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'  
 ,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'  
 ,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'  
 ,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'  
 ,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'  
 ,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'  
AS  
BEGIN  
  
-- =========================================================================  
-- SPC Name: spGetReportPRETSSummaryMhtmlInformation  
-- Author: AISHWARYA.M  
-- Create date: 21 Sep 2012  
-- Description: Get Preflight Tripsheet Summary Mhtml information for REPORTS  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- ==========================================================================  
SET NOCOUNT ON  
-- [Start] Report Criteria --  
 DECLARE @tblTripInfo AS TABLE (  
 TripID BIGINT  
 , LegID BIGINT  
 , PassengerID BIGINT  
 )  
  
 INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)  
 EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,  
  @EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,  
  @HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold,   
  @IsWorkSheet, @IsUnFulFilled, @IsScheduledService  
 -- [End] Report Criteria --  
  
--Reqcode [PreflightMain.PassengerRequestorID]:  PassengerCD  
--paxname   - Passenger Name  
  
--requestor [PreflightLeg.PassengerRequestorID]: PassengerCD  
--reqname   - Passenger Name  
  
--Crewcode [PreflightMain.ReleasedBy] [PreflightMain.CrewID]  
--Releasedby: Crew.[Full Name]  
  
SELECT --flag 'L' Leg Info  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'L'  
  ,[l_leg] = PL.LegNUM  
  ,[l_date] = PL.DepartureDTTMLocal  
  ,[l_local] = PL.DepartureDTTMLocal  
  ,[l_from] = AD.IcaoID  
  ,[l_to] = AA.IcaoID  
  ,[l_local2] = PL.ArrivalDTTMLocal  
  ,[l_arrzulu] = AA.OffsetToGMT --(of Arrival ICAO ID)  
  ,[l_depzulu] = AD.OffsetToGMT --(of Dept ICAO ID)  
  ,[timeapprox] = PL.IsApproxTM  
  ,[l_dist] = PL.Distance  
  ,[l_ete] = PL.ElapseTM  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = ISNULL(PL.PassengerTotal,0)  
  ,[l_fbo] = FBOA.FBOVendor --(To FBO)  
  ,[l_freq] = FBOA.Frequency  
  ,[l_phone] = FBOA.PhoneNUM1  
  ,[l_nogo] = PL.GoTime  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL  
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad 
  --mhtml 
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
    
  LEFT OUTER JOIN (  
   SELECT PF.LegID, PF.Comments, FA.FBOVendor, FA.PhoneNUM1, FA.Frequency , FA.FuelBrand, FA.FaxNum, FA.IsUWAAirPartner, FA.LastFuelPrice, FA.LastFuelDT, FA.NegotiatedFuelPrice  
   FROM PreflightFBOList PF  
   INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1  
   ) FBOA ON PL.LegID = FBOA.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'P' Passenger  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'P'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = PPL.PassengerFirstName + ',' + PPL.PassengerLastName + '' + PPL.PassengerMiddleName  
  ,[p_phone] = P.PhoneNum  
  ,[p_dept] = D.DepartmentCD  
  ,[p_cng] = P.IsEmployeeType  
  ,[p_legs] = FP.FlightPurposeCD  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT * FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID AND Main.PassengerID = PPL.PassengerID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  INNER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON P.FlightPurposeID = FP.FlightPurposeID  
  LEFT OUTER JOIN (SELECT DepartmentID, DepartmentCD FROM Department) D ON P.DepartmentID = D.DepartmentID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'C' Crew  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'C'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] =NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = PL.LegNum  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  INNER JOIN (SELECT FlightPurposeID, FlightPurposeCD FROM FlightPurpose) FP ON P.FlightPurposeID = FP.FlightPurposeID  
   
  UNION ALL  
    
  SELECT --flag 'F' Fbo  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'F'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = PL.LegNum  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = FBDA.FBOVendor  
  ,[f_phone] = FBDA.PhoneNUM1  
  ,[f_fax] = FBDA.FaxNum  
  ,[f_freq] = FBDA.Frequency  
  ,[f_uv] = FBDA.IsUWAAirPartner  
  ,[f_fuel] = FBDA.FuelBrand  
  ,[f_last] = FBDA.LastFuelPrice  
  ,[f_date] = FBDA.LastFuelDT  
  ,[f_neg] = FBDA.NegotiatedFuelPrice  
  ,[f_comments] = FBDA.Comments  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
 ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
  LEFT OUTER JOIN (  
   SELECT PF.LegID, PF.Comments, FD.FBOVendor, FD.PhoneNUM1, FD.Frequency , FD.FuelBrand, FD.FaxNum, FD.IsUWAAirPartner, FD.LastFuelPrice, FD.LastFuelDT, FD.NegotiatedFuelPrice  
   FROM PreflightFBOList PF  
   INNER JOIN FBO FD ON PF.FBOID = FD.FBOID AND PF.IsDepartureFBO = 1  
   UNION     
   SELECT PF.LegID, PF.Comments, FA.FBOVendor, FA.PhoneNUM1, FA.Frequency , FA.FuelBrand, FA.FaxNum, FA.IsUWAAirPartner, FA.LastFuelPrice, FA.LastFuelDT, FA.NegotiatedFuelPrice  
   FROM PreflightFBOList PF  
   INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1  
   ) FBDA ON PL.LegID = FBDA.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
     UNION ALL  
    
  SELECT --flag 'N' PL.Notes OR PL.CrewNotes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'N'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = CASE WHEN (LEN(ISNULL(PL.Notes, '')) > 0) THEN PL.LegNum   
       ELSE   
       CASE WHEN (LEN(ISNULL(PL.CrewNotes, '')) > 0) THEN PL.LegNum END  
          END  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'A' Arrival airport.alerts  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'A'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = CASE WHEN ISNULL (AA.Alerts,'')= '' THEN '' ELSE PL.LegNUM END  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'I' Arrival Airport.generalnotes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] =P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'I'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = CASE WHEN ISNULL (AA.GeneralNotes, '') = '' THEN '' ELSE PL.LegNUM END  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'H' Pax Hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'H'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = PL.LegNum--(Pax Hotel)  
  ,[h_name] = PH.Name  
  ,[h_phone] = PH.PhoneNum  
  ,[h_fax] = PH.FaxNum  
  ,[h_rate] = PH.NegociatedRate    
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
   ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT OUTER JOIN ( SELECT   
   PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.NegociatedRate,  
   PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments   
   FROM PreflightPassengerList PPL   
   INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID  
   INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID  
  ) PH ON PL.legID = PH.LegID    
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'T' Pax Transport  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'T'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = PL.LegNum  
  ,[t_name ] = PT.TransportationVendor --(Pax Transportation)  
  ,[t_phone] = PT.PhoneNum  
  ,[t_fax] = PT.FaxNum  
  ,[t_rate] = PT.NegotiatedRate    
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID    
  LEFT OUTER JOIN (SELECT PTL.LegID, T.TransportationVendor, T.PhoneNum, T.FaxNum, T.NegotiatedRate FROM PreflightTransportList PTL  
  INNER JOIN Transport T ON PTL.TransportID = T.TransportID  
  WHERE PTL.CrewPassengerType = 'P'  
  )PT ON PL.LegID = PT.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'R' crew hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'R'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = PL.LegNum  
  ,[r_name] = CH.Name --(Crew Hotel)  
  ,[r_phone] = CH.PhoneNum  
  ,[r_fax] = CH.FaxNum  
  ,[r_rate] = CH.NegociatedRate  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
    ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID      
  LEFT OUTER JOIN (     
   SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.NegociatedRate,  
     PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments    
   FROM PreflightCrewList PCLL   
   --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
   	INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
																AND PCH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
   INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
   WHERE PCLL.DutyType IN ('P', 'S')  
  ) CH ON PL.legID = CH.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'X' crew transportation  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'X'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = CT.TransportationVendor --(Crew Transportation)  
  ,[x_phone] = CT.PhoneNum  
  ,[x_fax] = CT.FaxNum  
  ,[x_rate] = CT.NegotiatedRate  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad 
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
 ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
    
  LEFT OUTER JOIN (SELECT PTL.LegID, T.TransportationVendor, T.PhoneNum, T.FaxNum, T.NegotiatedRate FROM PreflightTransportList PTL  
  INNER JOIN Transport T ON PTL.TransportID = T.TransportID  
  WHERE PTL.CrewPassengerType = 'C'  
  )CT ON PL.LegID = CT.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'M' Addtnl Crew Hotel  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'M'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = PL.LegNum  
  ,[m_name ] = ACH.Name   --Addtnl/Maintenance Crew Hotel  
  ,[m_phone] = ACH.PhoneNum  
  ,[m_fax] = ACH.FaxNum  
  ,[m_rate] = ACH.NegociatedRate  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT OUTER JOIN (     
   SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.NegociatedRate,  
      PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments     
   FROM PreflightCrewList PCLL   
   --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID  
   	INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
																AND PCH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
   INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID  
   WHERE PCLL.DutyType NOT IN ('P', 'S')  
  ) ACH ON PL.legID = ACH.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'D'  Departure Catering  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'D'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = PL.LegNum  
  ,[d_name] = CD.CateringVendor  
  ,[d_phone] = CD.PhoneNum  
  ,[d_fax] = CD.FaxNum  
  ,[d_rate] = CD.NegotiatedRate  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL   
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID)--No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT OUTER JOIN (SELECT PCL.LegID, C.CateringVendor, C.PhoneNum, C.FaxNum, C.NegotiatedRate   
  FROM PreflightCateringDetail PCL   
  INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'  
  )CD ON PL.LegID = CD.LegID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'O' Outbound Instructions  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  --,[paxname] = PPL.PassengerFirstName + ',' + PPL.PassengerMiddleName + ',' + PPL.PassengerLastName  
  ,[paxname] = P.FirstName + ',' + P.LastName + ' ' + P.MiddleInitial  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = 1  
  ,[flag] = 'O'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = PL.LegNum  
  ,[o_flag] = PTOf.o_flag  
  ,[outdata1] = CASE WHEN PTO.OutboundInstructionNUM = 1 THEN C.RptTabOutbdInstLab1 + ' : ' + PTO.OutboundDescription END  
  ,[outdata2] = CASE WHEN PTO.OutboundInstructionNUM = 2 THEN C.RptTabOutbdInstLab2 + ' : ' + PTO.OutboundDescription END  
  ,[outdata3] = CASE WHEN PTO.OutboundInstructionNUM = 3 THEN C.RptTabOutbdInstLab3 + ' : ' + PTO.OutboundDescription END  
  ,[outdata4] = CASE WHEN PTO.OutboundInstructionNUM = 4 THEN C.RptTabOutbdInstLab4 + ' : ' + PTO.OutboundDescription END  
  ,[outdata5] = CASE WHEN PTO.OutboundInstructionNUM = 5 THEN C.RptTabOutbdInstLab5 + ' : ' + PTO.OutboundDescription END  
  ,[outdata6] = CASE WHEN PTO.OutboundInstructionNUM = 6 THEN C.RptTabOutbdInstLab6 + ' : ' + PTO.OutboundDescription END  
  ,[outdata7] = CASE WHEN PTO.OutboundInstructionNUM = 7 THEN C.RptTabOutbdInstLab7 + ' : ' + PTO.OutboundDescription END  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
   ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  LEFT OUTER JOIN PreflightTripOutbound PTO ON PL.LegID = PTO.LegID  
  LEFT OUTER JOIN (  
    SELECT TOP 1 LegID, [o_flag] = OutboundInstructionNUM FROM PreflightTripOutbound  
    ) PTOf ON PL.LegID = PTO.LegID  
  LEFT OUTER JOIN Company C ON PM.HomebaseID = C.HomebaseID AND PM.CustomerID = C.CustomerID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    
  UNION ALL  
    
  SELECT --flag 'Z' PM.Notes  
  [orig_nmbr] = PM.TripNum  
  ,[dispatchno] = PM.DispatchNum  
  ,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(PM.TripID, @UserCD), 8)  
  ,[tail_nmbr] = F.TailNum  
  ,[type_code] = A.AircraftCD  
  ,[desc] = PM.TripDescription  
  ,[reqcode] = PM.PassengerRequestorID  
  ,[paxname] = P.FirstName + ',' + P.MiddleInitial + ',' + P.LastName  
  ,[phone] = P.PhoneNum  
  ,[mainflag] = CASE WHEN ISNULL (PM.Notes,'')= '' THEN '' ELSE 2 END --IF PM.NOTES EXISTS  
  ,[flag] = 'Z'  
  ,[l_leg] = NULL  
  ,[l_date] = NULL  
  ,[l_local] = NULL  
  ,[l_from] = NULL  
  ,[l_to] = NULL  
  ,[l_local2] = NULL  
  ,[l_arrzulu] = NULL  
  ,[l_depzulu] = NULL  
  ,[timeapprox] = NULL  
  ,[l_dist] = NULL  
  ,[l_ete] = NULL  
  ,[l_fuel] = 'WIP'  
  ,[l_pax] = NULL  
  ,[l_fbo] = NULL  
  ,[l_freq] = NULL  
  ,[l_phone] = NULL  
  ,[l_nogo] = NULL  
  ,[p_passenger] = NULL  
  ,[p_phone] = NULL  
  ,[p_dept] = NULL  
  ,[p_cng] = NULL  
  ,[p_legs] = NULL  
  ,[c_crewnum] = NULL  
  ,[n_legnum] = NULL  
  ,[f_num] = NULL  
  ,[f_depart] = ''--No code written for this    
  ,[f_name] = NULL  
  ,[f_phone] = NULL  
  ,[f_fax] = NULL  
  ,[f_freq] = NULL  
  ,[f_uv] = NULL  
  ,[f_fuel] = NULL  
  ,[f_last] = NULL  
  ,[f_date] = NULL  
  ,[f_neg] = NULL  
  ,[f_comments] = NULL  
  ,[a_num] = NULL  
  ,[i_num] = NULL  
  ,[h_num] = NULL  
  ,[h_name] = NULL  
  ,[h_phone] = NULL  
  ,[h_fax] = NULL  
  ,[h_rate] = NULL  
  ,[t_num] = NULL  
  ,[t_name ] = NULL  
  ,[t_phone] = NULL  
  ,[t_fax] = NULL  
  ,[t_rate] = NULL  
  ,[r_num] = NULL  
  ,[r_name] = NULL  
  ,[r_phone] = NULL  
  ,[r_fax] = NULL  
  ,[r_rate] = NULL  
  ,[x_num] = NULL  
  ,[x_name ] = NULL  
  ,[x_phone] = NULL  
  ,[x_fax] = NULL  
  ,[x_rate] = NULL  
  ,[m_num] = NULL  
  ,[m_name ] = NULL  
  ,[m_phone] = NULL  
  ,[m_fax] = NULL  
  ,[m_rate] = NULL  
  ,[d_num] = NULL  
  ,[d_name] = NULL  
  ,[d_phone] = NULL  
  ,[d_fax] = NULL  
  ,[d_rate] = NULL  
  ,[o_num] = NULL  
  ,[o_flag] = NULL  
  ,[outdata1] = NULL  
  ,[outdata2] = NULL  
  ,[outdata3] = NULL  
  ,[outdata4] = NULL  
  ,[outdata5] = NULL  
  ,[outdata6] = NULL  
  ,[outdata7] = NULL  
  ,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = PM.TripID) --No. of Legs for a Particular Trip  
  ,[purpose] = PL.FlightPurpose  
  ,[fuel_load] = PL.FuelLoad  
  ,[A_ALERTS]=''
  ,[N_LEGNOTES]=''
  ,[C_CREW]=''
  ,[I_NOTES]=''
  ,[H_COMMENTS]=''
  ,[T_COMMENTS]=''
  ,[R_COMMENTS]=''
  ,[X_COMMENTS]=''
  ,[M_COMMENTS]=''
  ,[D_COMMENTS]=''
  ,[Z_ALERTS]=''
  ,[OUT_COMMENT]=''
  
  FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) Main  
  INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID  
  INNER JOIN PreflightLeg PL ON Main.LegID = PL.LegID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT FROM Airport)AD ON PL.DepartICAOID = AD.AirportID  
  INNER JOIN (SELECT AirportID, IcaoID, OffsetToGMT, Alerts, GeneralNotes FROM Airport)AA ON PL.ArriveICAOID = AA.AirportID  
  INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft)A ON PM.AircraftID = A.AircraftID  
  INNER JOIN (SELECT FleetID,TailNum FROM Fleet)F ON PM.FleetID = F.FleetID  
  --INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID  
  INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID  
  WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
      
END  
  
-- EXEC spGetReportPRETSSummaryMhtmlInformation 'ELIZA_9','3820','','','','','','','','','','','',''


GO


