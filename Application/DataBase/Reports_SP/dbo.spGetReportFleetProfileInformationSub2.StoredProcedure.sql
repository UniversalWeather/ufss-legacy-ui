IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFleetProfileInformationSub2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFleetProfileInformationSub2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportFleetProfileInformationSub2]
	 @UserCD VARCHAR(30)
	,@TailNum char(6)
AS
-- ===============================================================================================
-- SPC Name: spGetReportFleetProfileInformationSub2
-- Author: Abhishek.s
-- Create date: 27 Sep 2012
-- Description: Get Fleet Profile Sub Report 2 Information
-- Revision History
-- Date			Name		Ver		Change
-- 27 Sep		Abhishek	1.1		SubRep2 for Fleet Profile
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @CLIENTID   BIGINT,
        @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SELECT DISTINCT		
         
         [AIDesc]     = FPD.FleetDescription
        ,[AInfo]      = IsNull(FPD.InformationValue,'')
        --dbo.FlightPakDecrypt(IsNull(FPD.InformationValue,''))
				
      FROM Fleet F 
INNER JOIN FleetProfileDefinition FPD
	    ON F.FleetID = FPD.FleetID AND FPD.IsDeleted = 0
	             
		WHERE F.IsDeleted = 0
	  AND F.CustomerID = @CUSTOMERID
	  AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	  AND F.TailNum = RTRIM(@TailNum)
	  
 -- EXEC spGetReportFleetProfileInformationSub2 'SUPERVISOR_99', 'N46F'


GO


