IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBSalesPersons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBSalesPersons]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE  [dbo].[spGetReportDBSalesPersons]   
(  
        @UserCD AS VARCHAR(30) --Mandatory  
       ,@UserCustomerID AS VARCHAR(30)  
)  
AS  
BEGIN  
   
  
-- ===============================================================================  
-- SPC Name: spGetReportDBPayableVendors  
-- Author: Askar R  
-- Create date: 23 Feb 2013  
-- Description: Get Vendor  Information  
-- Revision History  
-- Date        Name        Ver         Change  
--   
-- ================================================================================  
  
SELECT SP.SalesPersonCD Code,
	   CASE WHEN SP.LastName<>'' THEN SP.LastName +', ' ELSE '' END+ISNULL(FirstName,'')+' '+ISNULL(MiddleName,'') Description 
       FROM SalesPerson SP
       WHERE SP.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
         AND SP.IsDeleted=0
         --AND SP.IsInActive=0  
-- And IsInActive=0  
 --And V.IsDeleted=0 
 ORDER BY SalesPersonCD 
  
END  
  
--EXEC spGetReportDBSalesPersons 'SUPERVISOR_16',10016

GO


