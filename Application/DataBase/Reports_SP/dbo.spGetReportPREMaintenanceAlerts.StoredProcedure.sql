IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREMaintenanceAlerts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREMaintenanceAlerts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPREMaintenanceAlerts]   
    @UserCD AS VARCHAR(30) ,  
    @UserCustomerID AS BIGINT,  
    @FleetGroupCD AS NVARCHAR(1000) = '',  
    @TailNum AS VARCHAR(100) = ''  
      
AS      
BEGIN  
SET NOCOUNT ON;  
  
  
Declare @SuppressActivityAircft BIT    
    
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@UserCustomerID   
           AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
  
DECLARE  @TempFleetID TABLE       
(     
ID INT NOT NULL IDENTITY (1,1),   
FleetID BIGINT,  
HomeBaseID BIGINT,  
TailNum VARCHAR(9)  
  )  
    
  
IF @TailNum <> ''  
BEGIN  
INSERT INTO @TempFleetID  
SELECT DISTINCT F.FleetID,F.HomebaseID,F.TailNum   
FROM Fleet F  
WHERE F.CustomerID = @UserCustomerID  
AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))  
END  
  
IF @FleetGroupCD <> ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID, F.HomebaseID,F.TailNum  
FROM Fleet F   
LEFT OUTER JOIN FleetGroupOrder FGO  
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
LEFT OUTER JOIN FleetGroup FG   
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))   
AND F.CustomerID = @UserCustomerID    
AND FG.IsDeleted=0  
END  
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''  
BEGIN    
INSERT INTO @TempFleetID  
SELECT DISTINCT  F.FleetID,F.HomebaseID,F.TailNum  
FROM Fleet F   
WHERE F.CustomerID = @UserCustomerID    
AND F.IsDeleted=0  
AND F.IsInActive=0  
END  
  
-------------------------------TailNum and Fleet Group Filteration----------------------  
  
Declare @SuppressActivityDept BIT    
   
SELECT @SuppressActivityDept=IsZeroSuppressActivityDeptRpt FROM Company WHERE CustomerID=@UserCustomerID   
AND IsZeroSuppressActivityAircftRpt IS NOT NULL  
AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))  
  
SET NOCOUNT ON;  
--DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
DECLARE @TenToMin SMALLINT = 0;  
  
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
    
 declare @AircraftBasis numeric(1,0);  
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)  
  
  
SELECT * INTO #TEMP FROM   
(SELECT DISTINCT   
       TailNum =F.TailNum,  
       F.FleetID, ---CASE WHEN TH.OldTailNUM = F.TailNum THEN TH.NewTailNUM ELSE F.TailNum END,  
       F.LastInspectionDT,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN F.InspectionHrs ELSE 0 END InspectionHrs,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN F.WarningHrs ELSE 0 END WarningHrs,   
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN CONVERT(DATE,PL.ScheduledTM) ELSE NULL END AS ScheduledTM,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN PM.DispatchNum  ELSE NULL END DispatchNum,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN PM.LogNum  ELSE NULL END AS POLOGNUM ,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN PL.LegNUM  ELSE NULL END AS POLEGNUM,  
       [PODEP_ICAO] =CASE WHEN F.LastInspectionDT IS NOT NULL THEN AD.IcaoID ELSE NULL END,  
       [POARR_ICAO] =CASE WHEN F.LastInspectionDT IS NOT NULL THEN  AA.IcaoID ELSE NULL END,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN ISNULL(PL.Distance,0) ELSE 0 END Distance,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN PL.BlockHours ELSE 0 END BlockHours,  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN PL.FlightHours ELSE 0 END FlightHours,   
       FltBlkHrsToInsptlabel =(CASE @AircraftBasis WHEN 2 THEN CONVERT(VARCHAR(30),'Flt Hrs To Inspt')   
                                           WHEN 1 THEN CONVERT(VARCHAR(30),'Blk Hrs To Inspt') END),  
       CASE WHEN F.LastInspectionDT IS NOT NULL THEN (CASE WHEN @AircraftBasis = 1 THEN F.InspectionHrs-PL.BlockHours   
            WHEN @AircraftBasis = 2 THEN F.InspectionHrs-PL.FlightHours End) ELSE NULL END AS FltBlkHrsToInspt,  
       Commnet = CASE WHEN F.LastInspectionDT IS NOT NULL THEN (CASE WHEN  @AircraftBasis = 1 THEN (CASE WHEN F.InspectionHrs >= PL.BlockHours THEN '* OVERDUE *'    
                                                          WHEN F.InspectionHrs < PL.BlockHours THEN '* WARNING *' END)   
                      WHEN  @AircraftBasis = 2 THEN (CASE WHEN F.InspectionHrs >= PL.FlightHours THEN '* OVERDUE *'    
                                                          WHEN F.InspectionHrs < PL.FlightHours THEN '* WARNING *' END) END) ELSE NULL END,     
       CONVERT(DATE,NULL) AS DEPDATE,                                                      
       CONVERT(NUMERIC(7,3),NULL)  AS SCHEDHOURS,  
       NULL AS PRTRIPNUM,  
       NULL AS PRLEGNUM,  
       [PRDEP_ICAO] = CONVERT(VARCHAR(4),'') ,  
       [PRARR_ICAO] = CONVERT(VARCHAR(4),''),  
       AircraftBasis = @AircraftBasis,  
       TenToMin = @TenToMin  
       FROM PostflightMain PM  
       JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0  
       JOIN Fleet F ON PM.FleetID =F.FleetID AND PM.FleetID = F.FleetID AND  PM.CustomerID = F.CustomerID    
       ---LEFT JOIN TailHistory TH ON TH.OldTailNUM = F.TailNum AND TH.CustomerID = F.CustomerID  
       JOIN @TempFleetID TF ON F.FleetID = TF.FleetID  
       LEFT JOIN Company C ON F.HomebaseID = C.HomebaseID  
       LEFT JOIN FleetGroup FG ON C.HomebaseID = FG.HomebaseID     
       LEFT JOIN Airport AP ON C.HomebaseAirportID = AP.AirportID   
       LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID  
       LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID  
       WHERE PM.CustomerID = @UserCustomerID  
       AND F.IsDeleted = 0 AND F.IsInActive = 0 AND PM.IsDeleted = 0  
       AND CONVERT(DATE,PL.ScheduledTM) >   CONVERT(DATE,F.LastInspectionDT)  
       )T  
  
INSERT INTO #TEMP(TailNum,FleetID,LastInspectionDT,InspectionHrs,WarningHrs,ScheduledTM,DispatchNum,POLOGNUM,  
                  POLEGNUM,[PODEP_ICAO],[POARR_ICAO],Distance,BlockHours,FlightHours,FltBlkHrsToInsptlabel,FltBlkHrsToInspt,  
                  Commnet,DEPDATE,SCHEDHOURS,PRTRIPNUM,PRLEGNUM,[PRDEP_ICAO],[PRARR_ICAO],AircraftBasis,TenToMin)  
SELECT F.TailNum,F.FleetID,F.LastInspectionDT,F.InspectionHrs,F.WarningHrs,NULL,NULL,NULL,  
       NULL,NULL,NULL,0,NULL,NULL,(CASE @AircraftBasis WHEN 2 THEN CONVERT(VARCHAR(30),'Flt Hrs To Inspt')   
                                           WHEN 1 THEN CONVERT(VARCHAR(30),'Blk Hrs To Inspt') END),NULL,NULL,NULL,NULL,NULL,NULL,  
       NULL,NULL,@AircraftBasis,@TenToMin  
  
 FROM Fleet F INNER JOIN @TempFleetID TF ON F.FleetID=TF.FleetID  
                 WHERE F.IsDeleted=0  
                   AND F.IsInActive=0  
                   AND F.FleetID NOT IN(SELECT ISNULL(FleetID,0) FROM #TEMP)  
  
  
--INSERT INTO #TEMP(TailNum,FleetID,LastInspectionDT,InspectionHrs,WarningHrs,ScheduledTM,DispatchNum,POLOGNUM,  
--                  POLEGNUM,[PODEP_ICAO],[POARR_ICAO],Distance,BlockHours,FlightHours,FltBlkHrsToInsptlabel,FltBlkHrsToInspt,  
--                  Commnet,DEPDATE,SCHEDHOURS,PRTRIPNUM,PRLEGNUM,[PRDEP_ICAO],[PRARR_ICAO],AircraftBasis,TenToMin,CheckVal)   
--SELECT DISTINCT F.TailNum,F.FleetID,GETDATE(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,  
--       (CASE @AircraftBasis WHEN 1 THEN CONVERT(VARCHAR(30),'Flt Hrs To Inspt')   
--                                           WHEN 2 THEN CONVERT(VARCHAR(30),'Blk Hrs To Inspt') END),NULL,NULL,PL.DepartureDTTMLocal,PL.ElapseTM,PM.TripNUM,PL.LegNUM,D.IcaoID,A.IcaoID,  
--      @AircraftBasis,  
--   @TenToMin,  
--   'R' CheckVal   
-- FROM PreflightMain PM   
--               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID  
--               INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
--               INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID  
--               LEFT OUTER JOIN Airport D ON PL.DepartICAOID=D.AirportID  
--               LEFT OUTER JOIN Airport A ON PL.ArriveICAOID=A.AirportID  
-- WHERE F.IsDeleted=0  
--   AND F.IsInActive=0  
--   AND CONVERT(DATE,PL.DepartureDTTMLocal)  > CONVERT(DATE,GETDATE())   
                  
    
  
 IF @SuppressActivityAircft=0  
 BEGIN   
    
SELECT DISTINCT TailNum,dbo.GetShortDateFormatByUserCD(@UserCD,LastInspectionDT) LastInspectionDT,InspectionHrs,WarningHrs  
      ,dbo.GetShortDateFormatByUserCD(@UserCD,ScheduledTM) ScheduledTM,DispatchNum,POLOGNUM,POLEGNUM,PODEP_ICAO,POARR_ICAO,Distance,  
         BlockHours,FlightHours,FltBlkHrsToInsptlabel,FltBlkHrsToInspt,Commnet,DEPDATE,SCHEDHOURS,PRTRIPNUM,PRLEGNUM,[PRDEP_ICAO],[PRARR_ICAO],@AircraftBasis AS AircraftBasis, @TenToMin AS TenToMin,  
         TT.CkeckVal ,ScheduledOrder=ScheduledTM    
         FROM #TEMP T  
              LEFT OUTER JOIN (SELECT DISTINCT ROW_NUMBER()OVER(PARTITION BY PM.FleetID ORDER BY PM.FLEETID,PM.TRIPID)Rnk,PM.FleetID,PM.TripNUM CkeckVal  
          FROM PreflightMain PM   
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
               INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
               INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID  
          WHERE F.IsDeleted=0  
            AND F.IsInActive=0  
            AND PM.IsDeleted = 0  
            AND PM.TripStatus IN ('T','H')
            AND CONVERT(DATE,PL.DepartureDTTMLocal)  > CONVERT(DATE,GETDATE())   
            AND F.LastInspectionDT IS NOT NULL )TT ON TT.FleetID=T.FleetID AND Rnk=1   
  
    
   
 ORDER BY TailNum,ScheduledOrder    
END  
  
ELSE  
  
BEGIN  
  
SELECT DISTINCT TEMP.* FROM (  
SELECT DISTINCT TailNum,dbo.GetShortDateFormatByUserCD(@UserCD,LastInspectionDT) LastInspectionDT,InspectionHrs,WarningHrs  
      ,dbo.GetShortDateFormatByUserCD(@UserCD,ScheduledTM) ScheduledTM,DispatchNum,POLOGNUM,POLEGNUM,PODEP_ICAO,POARR_ICAO,Distance,  
         BlockHours,FlightHours,FltBlkHrsToInsptlabel,FltBlkHrsToInspt,Commnet,DEPDATE,SCHEDHOURS,PRTRIPNUM,PRLEGNUM,[PRDEP_ICAO],[PRARR_ICAO],@AircraftBasis AS AircraftBasis, @TenToMin AS TenToMin,  
         TT.CkeckVal ,ScheduledOrder=ScheduledTM     
         FROM  (SELECT FleetID FROM #TEMP WHERE LastInspectionDT IS NOT NULL AND POLOGNUM IS NOT NULL)POST  
                LEFT OUTER JOIN  #TEMP T ON POST.FleetID=T.FleetID  
                LEFT OUTER JOIN (SELECT DISTINCT ROW_NUMBER()OVER(PARTITION BY PM.FleetID ORDER BY PM.FLEETID,PM.TRIPID)Rnk,PM.FleetID,PM.TripNUM CkeckVal  
          FROM PreflightMain PM   
               INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0  
               INNER JOIN Fleet F ON PM.FleetID=F.FleetID  
               INNER JOIN @TempFleetID TF ON TF.FleetID=F.FleetID  
          WHERE F.IsDeleted=0  
            AND F.IsInActive=0  
            AND PM.IsDeleted = 0 
            AND PM.TripStatus IN ('T','H') 
            AND CONVERT(DATE,PL.DepartureDTTMLocal)  > CONVERT(DATE,GETDATE())   
            AND F.LastInspectionDT IS NOT NULL )TT ON TT.FleetID=T.FleetID AND Rnk=1  
  WHERE LastInspectionDT IS NOT NULL  
  
)TEMP  
   
   
 ORDER BY TailNum,ScheduledOrder     
END  
IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL  
DROP TABLE #TEMP   
      
END  
  
  
  
  
  
  
  
GO


