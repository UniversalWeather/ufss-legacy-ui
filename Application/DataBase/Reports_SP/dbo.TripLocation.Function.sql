IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TripLocation]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[TripLocation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[TripLocation](@TripID BIGINT,@CustomerID BIGINT)
RETURNS VARCHAR(200)
AS
BEGIN
DECLARE @CrTripID BIGINT,@CrLEGID BIGINT,@CrDIcaoID CHAR(4),@CrAIcaoID CHAR(4);    
DECLARE @PrevICAOID CHAR(4); 

DECLARE @TripRoute VARCHAR(200) = ''; 
   
DECLARE C1 CURSOR FOR    
SELECT PL.TRIPID, PL.LEGID, [DIcaoID] = AD.IcaoID, [AIcaoID] = AA.IcaoID    
FROM PreflightLeg PL    
INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID    
INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID    
WHERE PL.CustomerID = @CustomerID    
AND PL.TripID=@TripID    
ORDER BY PL.TripID, PL.LegID   
OPEN C1;    
FETCH NEXT FROM C1 INTO @CrTripID,@CrLEGID,@CrDIcaoID,@CrAIcaoID;    
WHILE @@FETCH_STATUS = 0    
BEGIN 
	IF @TripRoute = '' BEGIN
		SET @TripRoute = @CrDIcaoID + '-' + @CrAIcaoID
	END ELSE IF @PrevICAOID = @CrDIcaoID    
	BEGIN    
		SET @TripRoute = @TripRoute+ '-' +@CrAIcaoID
	END  
	ELSE BEGIN
		SET @TripRoute = @TripRoute+ '-' + @CrDIcaoID + '-' + @CrAIcaoID	
	END		
	SET @PrevICAOID=@CrAIcaoID    
	FETCH NEXT FROM C1 INTO @CrTripID,@CrLEGID,@CrDIcaoID,@CrAIcaoID;    
END    
CLOSE C1;    
DEALLOCATE C1;  
RETURN   @TripRoute
END
 
--SELECT dbo.TripLocation(1001150,10011)







GO


