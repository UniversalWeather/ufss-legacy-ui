IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewCurrExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewCurrExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
CREATE PROCEDURE [dbo].[spGetReportPOSTCrewCurrExportInformation]                              
  (
   @UserCD AS VARCHAR(30), --Mandatory                           
   @AsOf DateTime, --Mandatory                          
   @CrewCD Varchar(5000)='',                         
   @CrewGroupCD Varchar(5000)='',                            
   @AircraftCD Varchar(5000)='',                       
   @ChecklistCode varchar(5000)='',             
   @IsInactive Bit=0
   )                            
AS                            
SET NOCOUNT ON                           
   BEGIN                              
   
       	DECLARE @AppDTFormat VARCHAR(25)
SELECT
	  @AppDTFormat=Upper(ApplicationDateFormat)
	  FROM Company 
	  WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	  AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

	DECLARE @DATEFORMAT VARCHAR(20)
	= CASE WHEN  @AppDTFormat = 'ITALIAN' THEN '- -'
		   WHEN @AppDTFormat = 'ANSI' OR @AppDTFormat ='GERMAN' THEN '. .'
		   ELSE '/  /'
	       END
	                          
   Declare @StartDate DateTime             
   Set @StartDate= @AsOf -30            
            
   Declare @NightTODate DateTime            
   Declare @NightLandDate DateTime            
   Declare @InstDate DateTime   
   
   Declare @NightFromDate DateTime=DATEADD(d,-90,@AsOf)    -- 90 Days from @AsOfDate                    
   Declare @InstFromDate DateTime=DATEADD(m,-6,@AsOf)   -- 6 Months from @AsOfDate            
  
   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = ''     
   DECLARE @SQLSCRIPTCHKLIST AS NVARCHAR(4000) = ''                           
   DECLARE @ParameterDefinition AS NVARCHAR(400)                            
   
   IF OBJECT_ID('tempdb..#TmpCC') is not null          
      DROP table #TmpCC     
   IF OBJECT_ID('tempdb..#TmpLoop') is not null          
      DROP table #TmpLoop                            
   IF OBJECT_ID('tempdb..#Tmp') is not null          
      DROP table #Tmp     
   Create Table #TmpCC(CrewID BIGINT,CrewCD Varchar(5),LastName Varchar(30),FirstName Varchar(30),AircraftCD Varchar(30),
   Col1 DateTime,Col2 DateTime,Col3 DateTime,NightTO DateTime,NightLnd DateTime,Instrument DateTime)          
                                 
                        
   SET @SQLSCRIPT = 'insert into #TmpCC  
    SELECT DISTINCT C.CrewID,C.CrewCD,C.LastName,C.FirstName,Aircraft.AircraftCD, 
   Null As Col1 , Null As Col2, Null As Col3, Null As NightTO,Null As NightLnd,Null As Instrument 
 
   FROM PostflightLeg inner JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID AND PostflightMain.IsDeleted=0  INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID left JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID 
                      LEFT JOIN                      
   ( SELECT DISTINCT C.CrewID,C.CrewCD,C.FirstName,C.LastName,C.CustomerID,c.IsStatus
     FROM  Crew C                 
     left JOIN CrewGroupOrder CGO                
     ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID                 
     left JOIN CrewGroup CG                 
     ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID 
     WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, '','')) OR @CrewGroupCD = ''''  
   ) C ON C.CrewID = PostflightCrew.CrewID LEFT JOIN    
   CrewCheckList ON C.CustomerID = CrewCheckList.CustomerID 
   WHERE C.IsStatus =1 AND PostflightLeg.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                             
   + ' AND Convert(Date,PostflightLeg.ScheduledTM)<=@AsOf 
       AND PostflightLeg.IsDeleted=0
   '        
                               
                                
 IF @CrewCD  <> '' BEGIN                              
 SET @SQLSCRIPT = @SQLSCRIPT + ' AND C.CrewCD IN (''' + REPLACE(CASE WHEN RIGHT(@CrewCD, 1) = ',' THEN LEFT(@CrewCD, LEN(@CrewCD) - 1) ELSE @CrewCD END, ',', ''', ''') + ''')';                            
 END                             
                            
 IF @AircraftCD  <> '' BEGIN                              
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND Aircraft.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';                            
 END                             
 IF @ChecklistCode  <> '' BEGIN                              
  SET @SQLSCRIPT = @SQLSCRIPT + ' AND CrewChecklist.CrewCheckCD IN (''' + REPLACE(CASE WHEN RIGHT(@ChecklistCode, 1) = ',' THEN LEFT(@ChecklistCode, LEN(@ChecklistCode) - 1) ELSE @ChecklistCode END, ',', ''', ''') + ''')';                   
 END                               
 IF @IsInactive =1            
 BEGIN                              
     SET @SQLSCRIPT = @SQLSCRIPT + ' AND Fleet.IsInActive=0 '                
 END   
       
 SET @SQLSCRIPT = @SQLSCRIPT + ' ORDER BY C.LASTNAME,C.FIRSTNAME '                           
                          
 SET @ParameterDefinition =  '@AsOf AS DATETIME, @CrewCD AS Varchar(5000), @CrewGroupCD As Varchar(5000), @AircraftCD As Varchar(5000), @ChecklistCode As Varchar(5000), @IsInactive Bit'                            
 EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @AsOf, @CrewCD, @CrewGroupCD,@AircraftCD,@ChecklistCode,@IsInactive                                               
          
   Create Table #Tmp(CrewCheckId BigInt, CrewCheckCD Varchar(10),CrewChecklistDescription varchar(25))   
   --IF @ChecklistCode  =''          
   --   BEGIN                    
   --    SET @SQLSCRIPTCHKLIST = 'Insert Into #Tmp Select Top 3 CrewCheckID, CrewCheckCD,CrewChecklistDescription From CrewCheckList 
   --    Where CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))                    
   --   END         
   --ELSE 
   IF @ChecklistCode  <> ''          
     BEGIN                                
      SET @SQLSCRIPTCHKLIST = 'Insert Into #Tmp Select Top 3 CrewCheckID,CrewCheckCD,CrewChecklistDescription         
      From CrewChecklist Where CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) + 
      ' And CrewCheckCD IN (''' + REPLACE(CASE WHEN RIGHT(@ChecklistCode, 1) = ',' THEN         
      LEFT(@ChecklistCode, LEN(@ChecklistCode) - 1) ELSE @ChecklistCode END, ',', ''', ''') + ''')'                              
     END  
     

      
           
   IF @SQLSCRIPTCHKLIST <> ''
   BEGIN   
     DECLARE @ParamDefinition AS NVARCHAR(400)               
     SET @ParamDefinition =  '@UserCD As Varchar(30),@ChecklistCode As Varchar(5000)'                             
     EXECUTE sp_executesql @SQLSCRIPTCHKLIST, @ParamDefinition,@UserCD,@ChecklistCode          
   END
          
   Declare @Col1 Varchar(25)=''          
   Declare @Col2 Varchar(25)=''          
   Declare @Col3 Varchar(25)=''    
     
   Declare @CruCD Varchar(20)  
   Declare @AcCD Varchar(20)  
             
   Declare @RowId BIGINT          
   Declare @SNo Int          
   Set @SNo =1  
 
   Declare @ChkLstRowCount Int=0
   Set @ChkLstRowCount=(SELECT COUNT(*) FROM #Tmp)
  
   WHILE ( SELECT  COUNT(*) FROM #Tmp ) > 0          
   BEGIN           
     Set @RowId=(Select Top 1 CrewCheckID From #Tmp)   
	 IF @SNo = 1          
     Begin 
        Set @Col1 = (SELECT CrewCheckCD From #Tmp Where CrewCheckID=@RowId) 
	 End     
     Else IF @SNo = 2          
     Begin            
       Set @Col2 = (SELECT CrewCheckCD From #Tmp Where CrewCheckID=@RowId)   
     End          
     Else IF @SNo = 3          
     Begin   
         Set @Col3 = (SELECT CrewCheckCD From #Tmp Where CrewCheckID=@RowId)   
     End          
     Set @SNo = @SNo + 1          
     Delete From #Tmp Where CrewCheckID=@RowId           
   END             


--PRINT @Col1
--PRINT @Col2
--PRINT @Col3

   Declare @CustID Int
   Set @CustID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))     
   Declare @CrId Bigint          
   Select * Into  #TmpLoop From #TmpCC           
   WHILE ( SELECT  COUNT(*) FROM #TmpLoop ) > 0          
   BEGIN           
     SELECT Top 1 @CrId=CrewID,@CruCD=CrewCD,@AcCD=AircraftCD From #TmpLoop  
     

 
--SELECT TOP 1   DueDT FROM CrewCheckListDetail WHERE CrewID=@CrId And CheckListCD=@Col1 And CustomerID=@CustID  AND DueDT <> ''


     
     IF @Col1 <>''
     Begin  
     Update #TmpCC Set Col1 =(SELECT Top 1 DueDT FROM CrewCheckListDetail WHERE CrewID=@CrId And CheckListCD=@Col1 And CustomerID=@CustID )
                   WHERE  CrewID=@CrId 
     End
     
     IF @Col2 <>''
     Begin                
     Update #TmpCC Set Col2 =(SELECT Top 1 DueDT FROM CrewCheckListDetail WHERE CrewID=@CrId And CheckListCD=@Col2 And CustomerID=@CustID)
                   WHERE  CrewID=@CrId 
     End
     
     IF @Col3 <>''
     Begin                                       
     Update #TmpCC Set Col3 =(SELECT Top 1 DueDT FROM CrewCheckListDetail WHERE CrewID=@CrId And CheckListCD=@Col3 And CustomerID=@CustID)      
                   WHERE  CrewID=@CrId     
     End
     
     Set   @NightTODate = (        
           SELECT TOP 1 PostflightLeg.ScheduledTM 
                      FROM PostflightLeg INNER JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID and PostflightMain.IsDeleted=0 INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID  
           WHERE  PostflightCrew.CrewID=@CrId And Aircraft.AircraftCD=@AcCD And            
                  PostflightCrew.TakeOffNight>0 And  
                   PostflightLeg.IsDeleted=0
                  and PostflightLeg.ScheduledTM Between @NightFromDate And @AsOf )            
       
        
    IF @NightTODate='' OR @NightTODate is null            
    BEGIN            
    Set    @NightLandDate = (        
           SELECT TOP (1) PostflightSimulatorLog.SessionDT
           FROM   PostflightSimulatorLog INNER JOIN
                  Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID
           WHERE  PostflightSimulatorLog.CrewID =@CrId AND Aircraft.AircraftCD=@AcCD AND 
                  PostflightSimulatorLog.TakeOffNight>0 And  
                  PostflightSimulatorLog.SessionDT Between @NightFromDate And @AsOf)         
   END                
       
   Set    @NightLandDate = (        
           SELECT TOP 1 PostflightLeg.ScheduledTM 
                      FROM PostflightLeg INNER JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID And PostflightMain.IsDeleted=0 INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID  
           WHERE  PostflightCrew.CrewID=@CrId And Aircraft.AircraftCD=@AcCD And            
                  PostflightCrew.LandingNight>0 And  
                  PostflightLeg.IsDeleted=0
                  and PostflightLeg.ScheduledTM Between @NightFromDate And @AsOf )         
                    
                 
   IF @NightLandDate='' OR @NightLandDate is null            
   BEGIN            
    Set    @NightTODate = (        
           SELECT TOP (1) PostflightSimulatorLog.SessionDT
           FROM   PostflightSimulatorLog INNER JOIN
                  Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID
           WHERE  PostflightSimulatorLog.CrewID =@CrId  AND Aircraft.AircraftCD=@AcCD AND 
                  PostflightSimulatorLog.LandingNight>0 And  
                  PostflightSimulatorLog.SessionDT Between @NightFromDate And @AsOf)             
   END 
   
                         
    Set    @InstDate = (        
           SELECT TOP 1 PostflightLeg.ScheduledTM 
                      FROM PostflightLeg INNER JOIN
                      PostflightMain ON PostflightLeg.POLogID = PostflightMain.POLogID and PostflightMain.IsDeleted=0 INNER JOIN
                      PostflightCrew ON PostflightLeg.POLegID = PostflightCrew.POLegID INNER JOIN
                      Fleet ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN
                      Aircraft ON Fleet.AircraftID = Aircraft.AircraftID  
           WHERE  PostflightCrew.CrewID=@CrId And Aircraft.AircraftCD=@AcCD And            
                  PostflightCrew.Instrument>0 And    
                  PostflightLeg.IsDeleted=0 and 
                  PostflightLeg.ScheduledTM Between @InstFromDate And @AsOf )    
                  
   IF @InstDate='' OR @InstDate is null            
   BEGIN            
    Set    @InstDate = (        
           SELECT TOP (1) PostflightSimulatorLog.SessionDT
           FROM   PostflightSimulatorLog INNER JOIN
                  Aircraft ON PostflightSimulatorLog.AircraftID = Aircraft.AircraftID
           WHERE  PostflightSimulatorLog.CrewID =@CrId AND Aircraft.AircraftCD=@AcCD AND 
                  PostflightSimulatorLog.Instrument>0 And  
                  PostflightSimulatorLog.SessionDT Between @InstFromDate And @AsOf)                
   END                
         
      Update #TmpCC Set NightTO=@NightTODate,NightLnd=@NightLandDate,Instrument=@InstDate WHERE CrewID=@CrId AND AircraftCD=@AcCD 
      
     Delete From #TmpLoop Where CrewID=@CrId           
   END  
   
   
    

           
   IF OBJECT_ID('tempdb..#TmpLoop') is not null          
      DROP table #TmpLoop                    
   IF OBJECT_ID('tempdb..#Tmp') is not null          
      DROP table #Tmp      
      
 Select crewid, CrewCD crewcode, [DATFORMAT]=@DATEFORMAT,
 Case When LastName IS Null Then FirstName
      When FirstName Is Null Then LastName 
      Else LastName + ', ' + FirstName End crewname,
 AircraftCD type_code, NightTO night_to,NightLnd night_lnd,Instrument instrument, Col1 dcheck1, Col2 dcheck2 ,Col3 dcheck3  From #TmpCC  Order By LastName ,FirstName         
           
    
 --Exec spGetReportPOSTCrewCurrExportInformation 'supervisor_99','2010-12-31'                         
                     
END   
GO


