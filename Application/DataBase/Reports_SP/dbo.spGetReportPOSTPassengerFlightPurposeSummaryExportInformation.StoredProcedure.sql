IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerFlightPurposeSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerFlightPurposeSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerFlightPurposeSummaryExportInformation]
	(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FlightPurposeCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@PassengerGroupCD VARCHAR(500)='' 
	)	
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTPassengerFlightPurposeSummaryExportInformation
-- Author:  A.Akhila
-- Create date: 13 Aug 2012
-- Description: Get Passenger Flight Purpose Summary for EXPORT REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
	DECLARE @TenToMin SMALLINT = 0;
	SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
														AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------	
	SELECT DISTINCT
		[datefrom] = @DATEFROM
		,[dateto] = @DATETO
		,[lognum] = POM.LogNum
		,[tail_nmbr] = F.TailNum
		,[estdepdt] = POM.EstDepartureDT
		,[logcount] = POM.LogNum
		,[legcount] = COUNT(POL.POLegID)
		,[legid] = POL.POLegID
		,[leg_num] = POL.LegNUM
		,[schedttm] = POL.ScheduledTM
		,[flt_hrs] = ROUND(POL.FlightHours,1)
		,[distance] = POL.Distance
		,[statemiles] = ISNULL(POL.Distance *1.1508,0)
		,[paxcode] = P.PassengerRequestorCD
		,[fltpurpose] = FP.FlightPurposeCD
		,[paxname] = P.PassengerName
		,[fltpurdesc] = FP.FlightPurposeDescription
		,[cflt_hrs] = CASE WHEN C.AircraftBasis = 1 THEN SUM(ROUND(POL.BlockHours,1)) ELSE SUM(ROUND(POL.FlightHours,1)) END
		,[TenToMin] = @TenToMin
	FROM [PostflightMain] POM
		INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
		INNER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
		INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
		INNER JOIN Fleet F ON F.FleetID = POM.FleetID
		INNER JOIN FlightPurpose FP ON FP.FlightPurposeID = POP.FlightPurposeID
		INNER JOIN Company C ON C.HomebaseID = POM.HomebaseID
		INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	    AND POM.IsDeleted=0 
		AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
		--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
		AND (FP.FlightPurposeCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightPurposeCD, ',')) OR @FlightPurposeCD = '')
		
	GROUP BY 
		POM.LogNum, F.TailNum, POM.EstDepartureDT, POL.POLegID, POL.LegNUM, POL.ScheduledTM, POL.FlightHours,
		POL.Distance, POL.StatuteMiles, P.PassengerRequestorCD, FP.FlightPurposeCD, P.PassengerName, 
		FP.FlightPurposeDescription, C.AircraftBasis
	ORDER BY P.PassengerName, FP.FlightPurposeCD
	--EXEC spGetReportPOSTPassengerFlightPurposeSummaryExportInformation 'supervisor_99','2000/1/1','2012/12/12', '', ''


GO


