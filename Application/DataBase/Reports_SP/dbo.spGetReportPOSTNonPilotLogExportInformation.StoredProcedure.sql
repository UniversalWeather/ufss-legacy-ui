IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotLogExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotLogExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





        CREATE Procedure [dbo].[spGetReportPOSTNonPilotLogExportInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@CrewCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@CrewGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AircraftCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@PrintOnTime AS bit=0, 
		@PrintOffTime AS bit=0  
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTNonPilotLogExportInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight NonPilot Log for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
		
		--DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';

DECLARE @dutycounter12 AS int=0; 
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=1

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT,
crewcd varchar(30),
LASTNAME VARCHAR(80),
FIRSTNAME VARCHAR(80),
MiddleInitial VARCHAR(80)
)
 
IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT c.CrewID ,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial
FROM Crew c
WHERE c.CustomerID = @CUSTOMER
AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
END
IF @CrewGroupCD <> ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C 
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @CUSTOMER 
AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
BEGIN 
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID ,c.crewcd,C.LastName,C.FirstName,C.MiddleInitial
FROM Crew C 
WHERE C.CustomerID = @CUSTOMER 
END

--select * from @TempCrewID
-----------------------------Crew and Crew Group Filteration---------------------- 
CREATE TABLE #DUTYTOTAL
(
POLogID BIGINT
,LOGNUM INT
,LegID BIGINT
,IsStatus nvarchar(1)
,DutyBegin VARCHAR(20)
,DutyEnd VARCHAR(20)
,CrewID BIGINT
)
DECLARE @DUTY TABLE 
(
DutyBegin VARCHAR(20),
DutyEnd VARCHAR(20),
POLogID BIGINT,
LegID BIGINT
,CrewID BIGINT
)
INSERT INTO #DUTYTOTAL 
SELECT PM.POLogID,PM.LogNum,PL.POLegID,PL.IsDutyEnd,PC.BeginningDuty,PC.DutyEnd,PC.CrewID 
FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
INNER JOIN PostflightCrew PC ON PL.POLegID=PC.POLegID
WHERE PM.IsDeleted=0 AND
PM.LogNum in(SELECT distinct LogNum FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted=0
WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.IsDeleted=0 AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
INSERT INTO @DUTY 
SELECT DISTINCT CUR.DutyBegin,NXT.DutyEnd,CUR.POLogID,NXT.LegID,CUR.CrewID FROM 
(SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=0 AND DutyEnd IS NULL)CUR
JOIN (SELECT RANK() OVER (PARTITION BY LOGNUM,CREWID ORDER BY LEGID) RNK,LOGNUM, DutyBegin, DutyEnd,POLogID,LEGID,CrewID FROM #DUTYTOTAL WHERE IsStatus=1 AND DutyBegin IS NULL)NXT
ON CUR.LOGNUM=NXT.LOGNUM AND CUR.RNK=NXT.RNK AND CUR.CrewID=NXT.CrewID
UPDATE #DUTYTOTAL SET DutyBegin=TEST1.DutyBegin FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID=TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NOT NULL
UPDATE #DUTYTOTAL SET DutyBegin=NULL FROM (SELECT DISTINCT DutyBegin,CrewID,LegID FROM @DUTY )TEST1
WHERE #DUTYTOTAL.LegID<>TEST1.LegID
AND #DUTYTOTAL.DutyEnd IS NULL

SELECT  a.lognum,a.legid,A.CREWID,a.farnum,a.crewcode,a.type_code,a.dutyend,a.last_name,a.first_name,a.middleinit
	,a.schedttm,a.tail_nmbr,a.crewcurr,a.depicao_id,a.arricao_id,a.dutycounter,a.dc,a.fltdate,a.dutytotal
	,a.blkout,a.blkin,a.dutyhrs,a.blk_hrs,a.flt_hrs,a.assoc_crew,a.tot_line,a.override1,a.dutybeg,a.end1
	,a.ontime,a.offtime,a.spec3,a.spec4,a.speclong3,a.speclong4,a.specdec3,a.specdec4 
	INTO #TEMP
	FROM(
	
 SELECT distinct
            --[DUTYTYPE]=PC.Dutytype,
			[lognum]=PM.LOGNUM,
			[legid]=PL.POLegID,[CREWID]=C.CREWID,
			[farnum]=PL.FedAviationRegNum,
			[crewcode]=C.CrewCD,
			[type_code]=ac.AircraftCD,
			[dutyend]=(CASE WHEN (PL.IsDutyEnd=1) THEN 'TRUE'
			                WHEN (PL.IsDutyEnd=0) THEN 'FALSE'
			                END),
			[last_name]=C.LastName,
			[first_name]=C.FirstName,
			[middleinit]=C.MiddleInitial,
			[schedttm]=PL.ScheduledTM,
			[tail_nmbr]=F.TAILNUM,
			[crewcurr]=PL.CrewCurrency,
			[depicao_id]=AD.IcaoID,
			[arricao_id]=AA.IcaoID,
			[dutycounter]=NULL,--case (ISNULL(PC.dutyend,'')) when '' then ''  ELSE CONVERT(VARCHAR(5),@dutycounter12+1)  END ,
			[dc]=ISNULL(PC.dutyend,''),
			[fltdate]=pl.ScheduledTM,                
			[dutytotal]= (CASE WHEN (PC.dutyend is not null) THEN PC.dutyhours
			               END),
			
			[blkout]=PL.BlockOut,
			[blkin]=PL.BlockIN,
			[dutyhrs]=ROUND(PC.DutyHours,1),
			[blk_hrs]=ROUND(PC.BlockHours,1),
			[flt_hrs]=ROUND(PC.FlightHours,1),
			[assoc_crew]=ACrew.AssociatedCrew, 
			[tot_line]=' ',
			[override1]=(CASE WHEN(PC.IsOverride=1) THEN 'TRUE'
			                WHEN (PC.IsOverride=0) THEN 'FALSE'
			                END),
			[dutybeg]=PC.BeginningDuty,
			[end1]=PC.DutyEnd,
			[ontime]=CASE WHEN PL.IsDutyEnd=1 THEN REPLACE(ISNULL(DT.DutyBegin,''),':','') ELSE (PC.BeginningDuty) END,
			[offtime]=PC.DutyEnd,
			[spec3]=PC.Specification3,
			[spec4]=PC.Specification4,
			[speclong3]=CM.SpecificationLong3,
			[speclong4]=CM.SpecificationLong4,
			[specdec3]=CM.Specdec3,
			[specdec4]=CM.Specdec4
			  	FROM PostflightLeg PL 
		INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0
		INNER JOIN  PostflightCrew PC ON PC.POLegID = PL.POLegID		
		INNER JOIN  Fleet F ON F.FleetID = PM.FleetID
	    LEFT OUTER JOIN (SELECT DISTINCT * FROM #DUTYTOTAL) DT ON DT.POLogID=PM.POLogID AND DT.LegID=PL.POLegID
		INNER JOIN (  
        SELECT DISTINCT C.CustomerID, C.CREWID,CG.CrewGroupCD,C.LastName,C.CrewCD, CG.HomebaseID,c.FirstName,c.MiddleInitial
        FROM CREW C  
        LEFT OUTER JOIN CREWGROUPORDER CGO ON C.CREWID = CGO.CREWID AND C.CustomerID = CGO.CustomerID  
		left outer JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
        WHERE c.IsDeleted = 0 
		--AND (CG.CREWGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CREWGroupCD, ',')) OR @CREWGroupCD = '')
		)C ON Pc.CREWID = C.CREWID AND Pc.CustomerID = C.CustomerID 
		
		INNER JOIN (SELECT DISTINCT PC1.POLegID, AssociatedCrew = (SELECT RTRIM(C.CrewCD) + ',' 
		FROM PostflightCrew PC INNER JOIN Crew C ON PC.CrewID = C.CrewID 
		WHERE PC.POLegID = PC1.POLegID order by crewcd  FOR XML PATH(''))
		FROM PostflightCrew PC1) ACrew ON PL.POLegID = ACrew.POLegID  
       
        INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID 
		INNER JOIN Airport AD ON AD.AirportID = PL.DepartICAOID	
		left outer JOIN  Company CM ON CM.HomebaseID = PM.HomebaseID
		left outer JOIN  CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID and ccd.IsDeleted=0	
		left outer join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and ccl.IsDeleted=0
		left outer JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID        
		
	 
		WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		  AND PL.IsDeleted=0
				 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
				 AND (PC.Dutytype not IN('P','S') or PC.Dutytype is null)
				 --AND (C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
                 AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = ''))A
                 
--select * from #TEMP
 --DECLARE @LEGID BIGINT;
 declare @lognum bigint,@legid bigint,@CREWID BIGINT,@farnum as char(3),@crewcode varchar(5),@type_code as char(3),@dutyend varchar(20),@last_name varchar(30),@first_name varchar(20),@middleinit varchar(20)
	,@schedttm  datetime,@tail_nmbr varchar(9),@crewcurr as char(3),@depicao_id as char(4),@arricao_id as char(4),@dutycounter varchar(20),@dc varchar(5),@fltdate datetime,@dutytotal varchar(20)
	,@blkout varchar(5),@blkin varchar(5),@dutyhrs numeric(7,3),@blk_hrs numeric(7,3),@flt_hrs numeric(7,3),@assoc_crew varchar(20),@tot_line varchar(20),@override1 varchar(20),@dutybeg varchar(5),@end1 varchar(5)
	,@ontime varchar(5),@offtime varchar(5),@spec3 numeric(7,3),@spec4 numeric(7,3),@speclong3 varchar(25),@speclong4 varchar(25),@specdec3 int,@specdec4 int;--@dutycounter varchar(20),;
 DECLARE @PrevCount INT,@DUTYCOUNT VARCHAR(20);
 DECLARE @DUTYCOUNT1 INT=0;
 DECLARE curTailInfo CURSOR FAST_FORWARD READ_ONLY FOR  SELECT * FROM  #TEMP ORDER BY CREWCODE
 
OPEN curTailInfo;              
 -- PRINT @@CURSOR_ROWS              
  FETCH NEXT FROM curTailInfo INTO 
     @lognum,@legid,@CREWID,@farnum,@crewcode,@type_code,@dutyend,@last_name,@first_name,@middleinit
	,@schedttm,@tail_nmbr,@crewcurr,@depicao_id,@arricao_id,@dutycounter,@dc,@fltdate,@dutytotal
	,@blkout,@blkin,@dutyhrs,@blk_hrs,@flt_hrs,@assoc_crew,@tot_line,@override1,@dutybeg,@end1
	,@ontime,@offtime,@spec3,@spec4,@speclong3,@speclong4,@specdec3,@specdec4
      
 -- PRINT @DUTYCOUNT1     
  WHILE @@FETCH_STATUS = 0              
  BEGIN
 IF @dc='0'
 BEGIN 
 UPDATE #TEMP SET DUTYCOUNTER=0 WHERE legid=@legid AND CREWID=@CREWID;
 END
ELSE IF @dc<>'0' AND @dc<>''
 BEGIN
    SET @DUTYCOUNT1=@DUTYCOUNT1+1;
    PRINT @DUTYCOUNT1
   UPDATE #TEMP SET DUTYCOUNTER=@DUTYCOUNT1 WHERE legid=@legid AND CREWID=@CREWID;
  --SET @DUTYCOUNT1=@DUTYCOUNT1+1;
--   ---PRINT @DUTYCOUNT1
  END
  
 ELSE IF @dc=''
  BEGIN
   UPDATE #TEMP SET DUTYCOUNTER=@PrevCount WHERE legid=@legid AND CREWID=@CREWID;
 END
 
  SET @PrevCount=@DUTYCOUNT1
    FETCH NEXT FROM curTailInfo INTO 
     @lognum,@legid,@CREWID,@farnum,@crewcode,@type_code,@dutyend,@last_name,@first_name,@middleinit
	,@schedttm,@tail_nmbr,@crewcurr,@depicao_id,@arricao_id,@dutycounter,@dc,@fltdate,@dutytotal,@blkout,
	@blkin,@dutyhrs,@blk_hrs,@flt_hrs,@assoc_crew,@tot_line,@override1,@dutybeg,@end1,@ontime,
	@offtime,@spec3,@spec4,@speclong3,@speclong4,@specdec3,@specdec4;                      
 END            
 CLOSE curTailInfo;              
 DEALLOCATE curTailInfo;
   
IF @IsZeroSuppressActivityCrewRpt = 0
BEGIN     
SELECT * FROM  #TEMP  --WHERE DUTYCOUNTER <> 0
     UNION ALL

SELECT DISTINCT null,null,crewid,null,crewcd,'NO ACTIVITY',NULL,LASTNAME,FIRSTNAME,MiddleInitial,
	            null,null,null,null,null,null,null,null,null,null,
			    null,null,null,null,null,null,'FALSE',null,null,null,
			    null,null,null,null,null,null,null 
			    FROM @TempCrewID WHERE CrewID NOT IN(SELECT CrewID FROM  #TEMP)
     ORDER BY CREWCODE
END
ELSE
BEGIN
SELECT * FROM  #TEMP
 ORDER BY CREWCODE
END	 

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
drop table #TEMP


IF OBJECT_ID('tempdb..#DUTYTOTAL') IS NOT NULL
drop table #DUTYTOTAL
       
  
   -- EXEC spGetReportPOSTNonPilotLogExportInformation 'eliza_9','2010/8/3','2012/9/5','','',''
   -- EXEC spGetReportPOSTNonPilotLogInformation 'TIM','2010/8/3','2012/9/5','','',''
  
    
--select * from crew where CustomerID='10099'




GO


