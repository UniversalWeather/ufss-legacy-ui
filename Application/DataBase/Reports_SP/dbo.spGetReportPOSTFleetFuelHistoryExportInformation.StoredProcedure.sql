IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelHistoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelHistoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE  proc [dbo].[spGetReportPOSTFleetFuelHistoryExportInformation]        
  @MONTH AS VARCHAR(10),  
  @YEAR  AS INT,  
  @UserCD AS VARCHAR(30) ,
  @TailNum as nVarchar(1000)='', 
  @FleetGroupCD as  nVarchar(1000)='' 
    
AS      
BEGIN 
-- ===============================================================================  
-- SPC Name: spGetReportPOSTFleetFuelHistory  
-- Author: PREMPRAKASH.S  
-- Create date: 03 aug 2012  
-- Description: Get Fuel information based on Tail Number Details for Export Information
-- Revision History  
-- Date                 Name        Ver         Change  
 --EXEC spGetReportPOSTFleetFuelHistoryExportInformation 'Oct',2012,'eliza_9','N4753','1605'  
 --EXEC spGetReportPOSTFleetFuelHistory 'aug',2012,'UC'  
-- ================================================================================  
      
DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityAircftRpt=1

-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT,
		TailNum VARCHAR(9)
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID,F.TailNum 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID,F.TailNum---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER 
	AND F.IsDeleted = 0
	AND F.IsInActive = 0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------	
 DECLARE @EndDate datetime        
DECLARE @StartDate datetime       
DECLARE @StartDateval datetime       
DECLARE @ParameterDefinition AS NVARCHAR(MAX)      
DECLARE @date DATETIME      
DECLARE @A int      
declare @e int 
DECLARE @d DATETIME
------Framing start Date and End Date from the given year and month 
if(@YEAR%4=0)
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-365, @EndDate) 

end
else 
begin
SELECT @A=DATEPART(mm,CAST(Convert(varchar(10),@MONTH)+ '1900' AS DATETIME)) 
SET @d = CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ 1 ) as date) 
select @E= datepart(dd, dateadd(dd, -(datepart(dd, dateadd(mm, 1, @d))),dateadd(mm, 1, @d))) 
SELECT @EndDate= CAST(RTRIM(Convert(varchar(10),@YEAR)*10000+ @A *100+ @e) as date) 
--print @EndDate 
SET @StartDate=DateAdd(D,-364, @EndDate)
--print @StartDate  
end
------------------
--print @StartDate
--print @EndDate
-----------------
     
DECLARE @Monthval int        
DECLARE @intFlag INT        
SET @intFlag=1        
DECLARE @Monthdisplay TABLE        
(      
ID INT identity(1,1) , Monthval int ,Monthnameval varchar(20),      
dateFuel datetime  ) 
------Inserting 12 months into @Monthdisplay table  
WHILE (@intFlag <=12)        
BEGIN             
 --SET @StartDate= DateAdd(month,1, @StartDate)      
 INSERT INTO @Monthdisplay (Monthval,Monthnameval,dateFuel)       
 VALUES(DATEpart(month,@StartDate) , DATENAME(month,@StartDateval) ,@StartDate) 
 SET @StartDate= DateAdd(month,1, @StartDate)         
    SET @intFlag =@intFlag+1         
END       

     

-------------------------------------------------------------------------------    

-------------
 --select * from @Monthdisplay 
-----------
------------------------------------------------------------------------------
-------  Leap year calculation
if(@YEAR%4=0)
begin
SET @StartDate=DateAdd(DD,-365, @EndDate) 
--print @StartDate
end
else 
begin
SET @StartDate=DateAdd(DD,-364, @EndDate) 
end  
----To Retrieve fuel and expense information for each month based on Tail number
select distinct TailNum, POLogID, FuelQTY,QTY,ExpenseAMT,PurchaseDT,AircraftCD,A.PostflightExpenseID, m.Monthval,dateFuel as months
INTO #MainTailTab 
from @Monthdisplay m  
left join         
(          
SELECT 
f.TailNum, 
POLogID, 
[FuelQTY]=dbo.GetFuelConversion(pfe.FuelPurchase,pfe.FuelQTY,@UserCD),
[QTY]=  dbo.GetFuelConversion(pfe.FuelPurchase,pfe.FuelQTY,@UserCD),
ExpenseAMT,
PurchaseDT,
AircraftCD,
PostflightExpenseID ,        
DATEpart(MONTH,PurchaseDT) AS months    FROM  Fleet f        
INNER join PostflightExpense pfe on f.FleetID = pfe.FleetID and pfe.IsDeleted = 0     
LEFT JOIN FleetGroupOrder FGO on FGO.FleetID = F.FleetID          
LEFT JOIN FleetGroup FG on FG.FleetGroupID = FGO.FleetGroupID 
 WHERE PurchaseDT >= @StartDate  AND  PurchaseDT <=@EndDate 
 and pfe.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
)a
on m.Monthval = a.months 
order by dateFuel
-----------------------------------------------------------------------------------------------------------------------
IF @IsZeroSuppressActivityAircftRpt = 0
BEGIN
INSERT INTO #MainTailTab(TailNum,AircraftCD)
SELECT TF.TailNum ,F.AircraftCD FROM @TempFleetID TF 
               LEFT OUTER JOIN Fleet F ON TF.FleetID=F.FleetID

               WHERE TF.TailNum NOT IN(SELECT DISTINCT  T.TAILNUM FROM #MainTailTab T WHERE TailNum IS NOT NULL)
END
DECLARE @TEMPTail TABLE    
    (    
 tail_nmbr char(6),  
 PostflightExpenseID BIGINT ,  
 ac_code char(30),  
 Fuel1 NUMERIC(17,2) default 0.00,    
 Fuel2 NUMERIC(17,2) default 0.00,    
 Fuel3 NUMERIC(17,2) default 0.00,    
 Fuel4 NUMERIC(17,2) default 0.00,    
 Fuel5 NUMERIC(17,2) default 0.00,    
 Fuel6 NUMERIC(17,2) default 0.00,    
 Fuel7 NUMERIC(17,2) default 0.00,    
 Fuel8 NUMERIC(17,2) default 0.00,    
 Fuel9 NUMERIC(17,2) default 0.00,    
 Fuel10 NUMERIC(17,2) default 0.00,    
 Fuel11 NUMERIC(17,2) default 0.00,    
 Fuel12 NUMERIC(17,2) default 0.00 ,   
 Cost1 NUMERIC(17,2) default 0.00,    
 Cost2 NUMERIC(17,2) default 0.00,    
 Cost3 NUMERIC(17,2) default 0.00,    
 Cost4 NUMERIC(17,2) default 0.00,    
 Cost5 NUMERIC(17,2) default 0.00,    
 Cost6 NUMERIC(17,2) default 0.00,    
 Cost7 NUMERIC(17,2) default 0.00,    
 Cost8 NUMERIC(17,2) default 0.00,    
 Cost9 NUMERIC(17,2) default 0.00,    
 Cost10 NUMERIC(17,2) default 0.00,    
 Cost11 NUMERIC(17,2) default 0.00,    
 Cost12 NUMERIC(17,2) default 0.00    
)   
  INSERT INTO @TEMPTail(tail_nmbr,ac_code,PostflightExpenseID)  
  SELECT TailNum,AircraftCD,PostflightExpenseID FROM #MainTailTab
  UPDATE @TEMPTail SET Fuel1= MT.FuelQTY ,Cost1=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=1)
  UPDATE @TEMPTail SET Fuel2= MT.FuelQTY ,Cost2=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=2)
  UPDATE @TEMPTail SET Fuel3= MT.FuelQTY ,Cost3=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=3)
  UPDATE @TEMPTail SET Fuel4= MT.FuelQTY ,Cost4=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=4)
  UPDATE @TEMPTail SET Fuel5= MT.FuelQTY ,Cost5=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=5)
  UPDATE @TEMPTail SET Fuel6= MT.FuelQTY ,Cost6=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=6)
  UPDATE @TEMPTail SET Fuel7= MT.FuelQTY ,Cost7=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=7) 
  UPDATE @TEMPTail SET Fuel8= MT.FuelQTY ,Cost8=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=8)
  UPDATE @TEMPTail SET Fuel9= MT.FuelQTY ,Cost9=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=9) 
  UPDATE @TEMPTail SET Fuel10= MT.FuelQTY ,Cost10=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=10)
  UPDATE @TEMPTail SET Fuel11= MT.FuelQTY ,Cost11=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=11) 
  UPDATE @TEMPTail SET Fuel12= MT.FuelQTY ,Cost12=MT.ExpenseAMT   
  FROM   @TEMPTail TT  
  INNER JOIN  #MainTailTab MT ON MT.PostflightExpenseID = TT.PostflightExpenseID   
  WHERE  DATENAME(month,PurchaseDT) = (SELECT DATENAME(month,dateFuel) FROM  @Monthdisplay WHERE ID=12)
 
  SELECT * FROM @TEMPTail where tail_nmbr is not null   
  
  IF OBJECT_ID('tempdb..#MainTailTab') IS NOT NULL
DROP TABLE #MainTailTab 	

  END







GO


