IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerFlightSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerFlightSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerFlightSummaryInformation]
	(
	@UserCD AS VARCHAR(30), --Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@FlightCatagoryCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@SortBy as varchar(1),
	@PassengerGroupCD VARCHAR(500)='' 
	)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTPassengerFlightSummaryInformation
-- Author:  A.Akhila
-- Create date: 1 Jul 2012
-- Description: Get Passenger Flight Summary for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @PASSENGER TABLE (PASSENGERREQUESTORID BIGINT)
INSERT INTO @PASSENGER SELECT PassengerRequestorID FROM Passenger WHERE IsDeleted = 0 

DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @IsZeroSuppressActivityPassengerRpt BIT;
SELECT @IsZeroSuppressActivityPassengerRpt = IsZeroSuppressActivityPassengerRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityPassengerRpt=0
IF OBJECT_ID('tempdb..#PASSENGERINFO') IS NOT NULL
DROP TABLE #PASSENGERINFO 
SELECT
	[PASSENGERREQUESTORID] = P.PASSENGERREQUESTORID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[Code] = P.PassengerRequestorCD
	,[PassengerName] = P.LastName + ' , ' + P.FirstName
	,[LegsFlown] = COUNT(POP.PassengerID)
	,[BlockHours] = ISNULL(ROUND(POL.BlockHours,1),0)	
	,[FlightHours] = ISNULL(ROUND(POL.FlightHours,1),0)
	,[NauticalMiles] = ISNULL(POL.Distance,0)
	,[StatuteMiles] = ISNULL(POL.Distance *1.1508,0)
	,[INVISIBLElegid] = POL.POLegID
	,[TenToMin] = @TenToMin
INTO #PASSENGERINFO
FROM [PostflightPassenger] POP
	INNER JOIN PostflightLeg POL ON  POL.POLegID = POP.POLegID AND POL.IsDeleted = 0
	INNER JOIN PostflightMain POM ON POM.POLogID = POP.POLogID AND POM.IsDeleted = 0
	INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
	LEFT JOIN FlightCatagory F ON F.FlightCategoryID = POL.FlightCategoryID
	INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
WHERE P.IsDeleted=0 AND P.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
 AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
	AND (F.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
GROUP BY P.PASSENGERREQUESTORID, POL.POLegID, P.PassengerRequestorCD, P.LastName, P.FirstName, POL.BlockHours, POL.FlightHours, POL.Distance

IF @IsZeroSuppressActivityPassengerRpt=0
BEGIN
	SELECT * FROM #PASSENGERINFO
		UNION ALL
	SELECT DISTINCT
		[PASSENGERREQUESTORID] = P.PASSENGERREQUESTORID
		,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
		,[Code] = P.PassengerRequestorCD
		,[PassengerName] = P.LastName + ' , ' + P.FirstName
		,[LegsFlown] = NULL
		,[BlockHours] = NULL
		,[FlightHours] = NULL
		,[NauticalMiles] = NULL
		,[StatuteMiles] = NULL
		,[INVISIBLElegid] = NULL
		,[TenToMin] = @TenToMin
	FROM PASSENGER P
		INNER JOIN @PASSENGER PT ON P.PassengerRequestorID = PT.PASSENGERREQUESTORID
		INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
	WHERE P.PASSENGERREQUESTORID NOT IN (SELECT DISTINCT PASSENGERREQUESTORID FROM #PASSENGERINFO)
		AND P.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		--ORDER BY PassengerName
		--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
END
ELSE 
BEGIN
SELECT * FROM #PASSENGERINFO
--ORDER BY PassengerName
END

--ORDER BY (CASE WHEN @SortBy = 'b' THEN [PassengerName] ELSE [Code] END)
--EXEC spGetReportPOSTPassengerFlightSummaryInformation 'jwilliams_13','2000-01-01','2012-12-12', '', '','b'





GO


