IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor1MainInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor1MainInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportPassengerRequestor1MainInformation]
	@UserCD VARCHAR(30)
	,@PassengerCD varchar(500)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor1MainInformation
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Get Passenger/CrewPassengerVisa Information for Report Passenger/Requestor I
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
SET NOCOUNT ON
BEGIN
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100)
		
	--SELECT '''' + REPLACE(CASE WHEN RIGHT(@PassengerCD, 1) = ',' THEN LEFT(@PassengerCD, LEN(@PassengerCD) - 1) ELSE @PassengerCD END, ',', ''',''') + ''''
--select @PassengerCD AS PAXCodes
SELECT distinct s AS PAXCodes 
FROM dbo.SplitString( @PassengerCD,',')
END
-- EXEC spGetReportPassengerRequestor1MainInformation 'UC', 'WKOKA,ECJ,MT01,HKIOE,EJM01,WKOKA'

GO


