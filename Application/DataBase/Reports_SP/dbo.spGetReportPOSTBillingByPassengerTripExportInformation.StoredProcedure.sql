IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTBillingByPassengerTripExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTBillingByPassengerTripExportInformation]         
( 
@UserCD AS VARCHAR(30), --Mandatory
@UserCustomerID AS VARCHAR(30),           
@DATEFROM AS DATE , --Mandatory          
@DATETO AS DATE , --Mandatory           
@LogNum NVARCHAR(1000) = '',
@PassengerRequestorCD  AS VARCHAR(1000) = '',
@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values    
@FleetGroupCD AS NVARCHAR(4000) = '',
@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase   
@UserHomebaseID AS VARCHAR(30)  ,
@PassengerGroupCD VARCHAR(500)='' 
)   
AS
--================================================================================              
-- SPC Name: [spGetReportPOSTBillingByPassengerTrip]          
-- Author: HARIHARAN S          
-- Create date: 02/18/2013          
-- Description: Get Trip Detail by billing for REPORTS          
-- Revision History          
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
--================================================================================              
BEGIN            
SET NOCOUNT ON  


-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END
-----------------------------TailNum and Fleet Group Filteration---------------------- 

 DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------    
DECLARE @TenToMin SMALLINT = 0;  
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @TenToMin = TimeDisplayTenMin,
@AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 


SELECT DISTINCT [Leg] = POL.LegNUM
,[LogNumber] = POM.LogNum
,[EstDeptDate] = POM.EstDepartureDT
,[TailNumber] = FL.TailNum
,[Rqstr] = PP.PassengerRequestorCD
,[RqstrDept] = ReqD.DepartmentCD
,[RqstrAuth] = ReqAuth.AuthorizationCD
,[legid] = POL.POLegID
,[DepICAO] = AD.IcaoID
,[ArrICAO] = AA.IcaoID
,[distance] = POL.Distance
,[schedttm] = POL.OutboundDTTM
,[FlightHours] = ROUND(POL.FlightHours,1)
,[BlockHours] = ROUND(POL.BlockHours,1)
,[DeptCD] = DE.DepartmentCD
,[AuthCD] = DA.AuthorizationCD
,[chrgRate] = CR.ChargeRate
,[chrgUnit] = CR.ChargeUnit
,[maxpax] = FL.MaximumPassenger
,[typecode] = AR.AircraftCD
,[paxtotal] = POL.PassengerTotal
,[TotalChargesLEG] = CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
						+ (ISNULL(POE.ExpenseAmt,0))

,[paxcode] = P.PassengerRequestorCD
,[paxname] = P.LastName + ', ' + P.FirstName
,[IncludeIsBilling] = POE.IsBilling
,[stdbilling] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
						(
						ISNULL(((CASE WHEN CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							)ELSE
						NULL
						END
						) 
						+ (ISNULL(POE.ExpenseAmt,0)))/POL.PassengerTotal,0)
						)
						END 
,[paxdept] = PaxD.DepartmentCD
,[paxAuth] = PaxAuth.AuthorizationCD
,[TotalChargesPax] = CASE WHEN POL.PassengerTotal=0 
						THEN 0 
						ELSE 
							FLOOR((
							((CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(CR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END)) 
							WHEN 'N' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(CR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) 
							ELSE
							'0.00'
							END) + (ISNULL(POE.ExpenseAmt,0)))
							/POL.PassengerTotal)*100)*0.01
						END
,[billingcode] = POP.Billing
,[expaccount] = EX.AccountDescription
,[exp_amount] = EX.ExpenseAMT
,[TenToMin] = @TenToMin

INTO #PaxTemp
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POL.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
INNER JOIN Fleet FL ON FL.FleetID = POM.FleetID AND FL.CustomerID = POM.CustomerID
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
LEFT OUTER JOIN Department DE ON POL.DepartmentID = DE.DepartmentID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID = POL.AuthorizationID
LEFT OUTER JOIN FleetChargeRate FCR ON FL.FleetID = FCR.FleetID AND FL.CustomerID = FCR.CustomerID AND CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,POL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,POL.ScheduledTM) AND FCR.IsDeleted=0
INNER JOIN @TempFleetID T ON T.FleetID = FL.FleetID
LEFT OUTER JOIN Account A ON POE.AccountID = A.AccountID AND POE.CustomerID = A.CustomerID
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
INNER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
LEFT OUTER JOIN (
			SELECT POLegID, ChargeUnit, ChargeRate
			FROM PostflightMain PM INNER JOIN  PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
			                       INNER JOIN FleetChargeRate FC ON PM.FleetID=FC.FleetID AND FC.IsDeleted=0
			WHERE CONVERT(DATE,BeginRateDT)<= CONVERT(DATE,PL.ScheduledTM) AND EndRateDT>=CONVERT(DATE,PL.ScheduledTM)
			AND PM.IsDeleted = 0
           ) CR ON POL.POLegID = CR.POLegID 
LEFT OUTER JOIN Passenger RPax ON RPax.PassengerRequestorID = POM.PassengerRequestorID
LEFT OUTER JOIN Department ReqD ON ReqD.DepartmentID = RPax.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization ReqAuth ON ReqAuth.AuthorizationID = RPax.AuthorizationID
LEFT OUTER JOIN Department PaxD ON PaxD.DepartmentID = P.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization PaxAuth ON PaxAuth.AuthorizationID = P.AuthorizationID
LEFT OUTER JOIN Aircraft AR ON AR.AircraftID = FL.AircraftID
LEFT OUTER JOIN (
			SELECT AccountDescription, ExpenseAMT, PE.POLegID FROM PostflightExpense PE 
					INNER JOIN Account A ON PE.AccountID = A.AccountID
					WHERE PE.IsDeleted = 0
				)EX ON EX.POLegID = POL.POLegID
INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID

WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
	-- AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '')
	AND (DE.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	--AND (FL.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0


SELECT DISTINCT [Leg]
				,[LogNumber]
				,[EstDeptDate]
				,[TailNumber]
				,[Rqstr]
				,[RqstrDept] 
				,[RqstrAuth] 
				,PT.legid
				,[DepICAO] 
				,[ArrICAO] 
				,[distance] 
				,[schedttm] 
				,[FlightHours] 
				,[BlockHours]
				,[DeptCD]
				,[AuthCD]
				,[chrgRate] 
				,[chrgUnit]
				,[maxpax]
				,[typecode]
				,[paxtotal]
				,[TotalChargesLEG] 
				,[paxcode]
				,[paxname] 
				,[IncludeIsBilling]
				,[stdbilling] 
				,[paxdept]
				,[paxAuth]
				,[TotalChargesPax] 
				,PT.pcode
				,PT.pname
				,[billingcode]
				,PT.requiredDept
				,PT.requiredAuth
				,PT.paxcharges
				,[expaccount]
				,[exp_amount]
				  FROM #PaxTemp PTEMP
				  INNER JOIN (SELECT PX.legid,
									pcode = (
										SELECT paxcode+' ' FROM #PaxTemp PTMP 
															WHERE PX.LEGID = PTMP.legid 
															FOR XML PATH('')),
									pname = (
										SELECT paxname + ' ' FROM #PaxTemp PTMP 
															WHERE PX.LEGID = PTMP.legid 
															FOR XML PATH('')),
									requiredDept = (
										SELECT paxdept + ' ' FROM #PaxTemp PTMP 
															WHERE PX.LEGID = PTMP.legid 
															FOR XML PATH('')),
									requiredAuth = (
										SELECT paxAuth + ' ' FROM #PaxTemp PTMP 
															WHERE PX.LEGID = PTMP.legid 
															FOR XML PATH('')),
									paxcharges = (
										SELECT '$' + CONVERT(VARCHAR(30),TotalChargesPax) + ' ' FROM #PaxTemp PTMP 
															WHERE PX.LEGID = PTMP.legid 
															FOR XML PATH(''))	
												FROM (SELECT LEGID FROM #PaxTemp)PX )PT ON  PT.legid=PTemp.legid
       
IF OBJECT_ID('tempdb..#PaxTemp') IS NOT NULL
DROP TABLE  #PaxTemp

END
--EXEC spGetReportPOSTBillingByPassengerTripExportInformation 'supervisor_99','10099','01-01-2009','03-03-2013','2','','','','','',0 ,''         






GO


