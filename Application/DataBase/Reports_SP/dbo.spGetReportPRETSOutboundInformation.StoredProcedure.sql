IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSOutboundInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSOutboundInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSOutboundInformation]
	@UserCD VARCHAR(50),--MANDATORY
	@TripID  AS VARCHAR(50),  -- [BIGINT is not supported by SSRS]
	@LegNum	VARCHAR(50) = ''	
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSOutboundInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight TripSheet Outbound information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON
	
	DECLARE @HomebaseID BIGINT;
	SET @HomebaseID=dbo.GetHomeBaseByUserCD(LTRIM(@UserCD));
	SELECT [LegId] = CONVERT(VARCHAR,PL.LegID),
		--[LegNUM] = CONVERT(VARCHAR,PL.LegNUM),
		PL.FuelLoad,
		PL.CrewFuelLoad,
		Comments = PL.OutbountInstruction, 
		OBInstructions = 
		CASE 
		    WHEN PTO.OutboundInstructionNUM = 1 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab1 <> '' THEN  C.RptTabOutbdInstLab1 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 2 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab2 <> '' THEN  C.RptTabOutbdInstLab2 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 3 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab3 <> '' THEN  C.RptTabOutbdInstLab3 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 4 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab4 <> '' THEN  C.RptTabOutbdInstLab4 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 5 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab5 <> '' THEN  C.RptTabOutbdInstLab5 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 6 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab6 <> '' THEN  C.RptTabOutbdInstLab6 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
			WHEN PTO.OutboundInstructionNUM = 7 THEN ISNULL((CASE WHEN  C.RptTabOutbdInstLab7 <> '' THEN  C.RptTabOutbdInstLab7 ELSE NULL END + ' : ') + ISNULL( PTO.OutboundDescription,''),'')
		END
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN PreflightTripOutbound PTO ON PL.LegID = PTO.LegID
	INNER JOIN Company C ON C.HomebaseID = @HomebaseID AND PM.CustomerID = C.CustomerID
	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	AND PM.TripID = CONVERT(BIGINT,@TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND PTO.IsDeleted = 0
	--AND ISNULL(PTO.OutboundDescription,'') <> ''
	ORDER BY PTO.OutboundInstructionNUM
	
END

--EXEC spGetReportPRETSOutboundInformation 'UC', '100006941'
--EXEC spGetReportPRETSOutboundInformation 'supervisor_99', '10099107564','1'



GO


