IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetREportPRETSPAXNotesExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetREportPRETSPAXNotesExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetREportPRETSPAXNotesExportInformation]
@UserCD            AS VARCHAR(30)    
,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'

AS
BEGIN
-- ==========================================================================
-- SPC Name: spGetREportPRETSPAXNotesExportInformation
-- Author: AISHWARYA.M
-- Create date: 13 Sep 2012
-- Description: Get Preflight Tripsheet Report PAX Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ===========================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
--SELECT * FROM @tblTripInfo	
		SELECT --PL.LegID, 
		PM.TRIPID,
				[orig_nmbr] = PM.TripNUM
				,[desc] = PM.TripDescription
				,[paxname] = ISNULL(P.LastName, '') + ',' + ISNULL(P.FirstName,'') + ' ' + ISNULL(P.MiddleInitial,'')
				,[paxnotes] = P.Notes
				,[gmtbeg] = PM.BeginningGMTDT
				,[gmtend] = PM.EndingGMTDT
				,[tail_nmbr] = F.TailNum
				,[phone] = P.PhoneNum
				,[type_code] = A.AircraftCD
				,[fltphone] = F.FlightPhoneNum
				,[infltphone] = F.FlightPhoneIntlNum
				,[dispatchno] = PM.DispatchNUM
				,[trippaxcode] = PAX.PassengerRequestorCD
				,[trippaxname] = ISNULL(PAX.LastName,'') + ',' + ISNULL(PAX.FirstName,'') + ' ' + ISNULL(PAX.MiddleInitial,'')
				,[crewcode] = C.CrewCD
				,[releasedby] = C.LastName + ',' + C.FirstName + '' + C.MiddleInitial
				,[verifynmbr] = PM.VerifyNUM
				FROM (SELECT DISTINCT TRIPID FROM @tblTripInfo) Main
				INNER JOIN PreflightMain PM ON Main.TripID = PM.TripID
				LEFT OUTER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
				INNER JOIN Fleet F ON PM.FleetID = F.FleetID
				INNER JOIN (
					SELECT DISTINCT PL.TripID, PPL.PassengerID FROM PreflightLeg PL
					INNER JOIN PreflightPassengerList PPL ON PL.LegID = PPL.LegID
					INNER JOIN (SELECT DISTINCT LegID FROM @tblTripInfo) SL ON PL.LegID = SL.LegID
				) PPL ON PM.TripID = PPL.TripID
				LEFT OUTER JOIN Crew C ON PM.CrewID = C.CrewID
				LEFT OUTER JOIN Passenger PAX ON PPL.PassengerID = PAX.PassengerRequestorID
				LEFT OUTER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
				WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				--AND PM.TripID = @TripID
				--ORDER BY PL.LegID	
END

--EXEC spGetREportPRETSPAXNotesExportInformation 'TIM', '100', '', '', '20120720', '20120725', 'ZZKWGS', 'test', '', '', '', '', ''
--EXEC spGetREportPRETSPAXNotesExportInformation 'supervisor_99', 1992




GO


