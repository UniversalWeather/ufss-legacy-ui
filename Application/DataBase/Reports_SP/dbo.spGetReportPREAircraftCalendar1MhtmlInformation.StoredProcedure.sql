IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar1MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar1MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[spGetReportPREAircraftCalendar1MhtmlInformation]    
    @UserCD AS VARCHAR(30) --Mandatory      
    ,@DATEFROM AS DATETIME --Mandatory      
    ,@DATETO AS DATETIME --Mandatory      
    ,@TailNUM AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values      
    ,@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values      
    --,@HomeBase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase      
AS    
BEGIN    
-- ===============================================================================    
-- SPC Name: spGetReportPREAircraftCalendar1ExportInformation    
-- Author: Askar    
-- Create date: 23 Jul 2012     
-- Description: Get Preflight Aircraft Calendar1 information for REPORTS     
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================    
SET NOCOUNT ON    
    
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';    
DECLARE @ParameterDefinition AS NVARCHAR(100)    
    
 -- Populate the complete Tail List for each Date in the given range    
 CREATE TABLE #TempTailDate (    
  RowID INT identity(1, 1) NOT NULL    
  ,FleetID BIGINT  
  ,HomeBaseID BIGINT   
  ,TailNum VARCHAR(20)    
  ,DeptDateOrg DATETIME     
 )    
  
 CREATE TABLE #TempFleetCalendar (    
  RowID INT identity(1, 1) NOT NULL    
  ,TailNum VARCHAR(20),DeptDateOrg DATETIME,AircraftID BIGINT,FleetID BIGINT,TripID BIGINT    
  ,LegID BIGINT,DepartICAOID CHAR(4),Depart DATETIME,ArriveICAOID CHAR(4),Arrive DATETIME    
  ,NextLocalDTTM DATETIME, DutyType CHAR(2), PassTotal INT,    
  TripNUM BIGINT,CrewCD VARCHAR(250),Aircraft_Type VARCHAR(10))    
     
 DECLARE  @TEMPTODAY AS DATETIME = @DATEFROM    
    
 WHILE (@TEMPTODAY <= @DATETO)    
  BEGIN    
   SET @SQLSCRIPT = '  
    INSERT INTO #TempTailDate (FleetID,HomeBaseID,TailNum, DeptDateOrg)  
    SELECT DISTINCT F.FleetID,C.HOMEBASEAIRPORTID,F.TailNum,@TEMPTODAY   
    FROM Fleet F  
    INNER JOIN COMPANY C ON F.HOMEBASEID = C.HOMEBASEID
    LEFT OUTER JOIN FleetGroupOrder FGO   
     ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
    LEFT OUTER JOIN FleetGroup FG   
     ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
    WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
     --AND f.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))  
    '  
    IF @FleetGroupCD <> '' BEGIN    
     SET @SQLSCRIPT = @SQLSCRIPT + ' AND FG.FleetGroupCD IN (''' + REPLACE(CASE WHEN RIGHT(@FleetGroupCD, 1) = ',' THEN LEFT(@FleetGroupCD, LEN(@FleetGroupCD) - 1) ELSE @FleetGroupCD END, ',', ''', ''') + ''')';  
    END  
   --PRINT @SQLSCRIPT  
     
   SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME'  
   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY      
   ---------------------------------------------------------------------  
   -- Populate actual trips and empty trips for all the Fleet    
   SET @SQLSCRIPT = '    
    INSERT INTO #TempFleetCalendar ( TailNum,DeptDateOrg,AircraftID,FleetID,TripID,LegID,DepartICAOID,    
    Depart,ArriveICAOID,Arrive,NextLocalDTTM,DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type)    
        
    SELECT DISTINCT F.TailNum,@TEMPTODAY,A.AircraftID,F.FleetID,PM.TripID,PL.LegID,AD.IcaoID,PL.DepartureDTTMLocal,AA.IcaoID,PL.ArrivalDTTMLocal,    
           PL.NextLocalDTTM,PL.DutyTYPE,PL.PassengerTotal,PM.TripNUM,SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1) CrewList,A.AircraftCD    
    FROM PreflightMain PM      
        INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID  AND PL.IsDeleted=0    
        --INNER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID      
        --INNER JOIN Crew C ON PC.CrewID = C.CrewID      
         INNER JOIN (          
       SELECT DISTINCT PC2.LegID, CrewList = (          
        SELECT C.CrewCD + '',''          
        FROM PreflightCrewList PC1          
        INNER JOIN Crew C ON PC1.CrewID = C.CrewID          
        WHERE PC1.LegID = PC2.LegID          
        FOR XML PATH('''')          
        )                   
       FROM PreflightCrewList PC2           
     ) Crew ON PL.LegID = Crew.LegID      
        INNER JOIN (SELECT AirportID,IcaoID FROM Airport)AD ON AD.AirportID = PL.DepartICAOID      
        INNER JOIN (SELECT AirportID,IcaoID FROM Airport)AA  ON AA.AirportID = PL.ArriveICAOID         
        INNER JOIN (    
       SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.AircraftID    
       FROM Fleet F     
       LEFT OUTER JOIN FleetGroupOrder FGO    
       ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID    
       LEFT OUTER JOIN FleetGroup FG     
       ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID    
		WHERE (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, '','')) 
			OR @FleetGroupCD = '''' )
	      AND F.IsDeleted=0
	      AND F.IsInActive=0  
       ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID    
     INNER JOIN Aircraft A  ON A.AircraftID=F.AircraftID     
    WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))     
    AND CONVERT(VARCHAR(10), PL.DepartureDTTMLocal, 103) = CONVERT(VARCHAR(10), @TEMPTODAY, 103)  
    AND PM.IsDeleted=0  
    --AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))      
   '    
    
   --IF @HomeBase = 1 BEGIN    
   -- SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)));    
   --END 
   
   IF @TailNUM <> '' BEGIN  
	 SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.TailNUM IN (''' + REPLACE(CASE WHEN RIGHT(@TailNUM, 1) = ',' THEN LEFT(@TailNUM, LEN(@TailNUM) - 1) ELSE @TailNUM END, ',', ''', ''') + ''')';
  END      
       
   --PRINT @SQLSCRIPT    
   SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TEMPTODAY AS DATETIME, @FleetGroupCD AS VARCHAR(500)'    
   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TEMPTODAY, @FleetGroupCD    
       
   SET @TEMPTODAY = @TEMPTODAY + 1    
  END    
 -- PRINT @SQLSCRIPT
---SELECT * FROM #TempTailDate    
 -----------------------------------------------------------    
 --Populate IDLE TIME records    
  --Insert 1: IdleDepartDateTime = CurrentLeg.ArrivalDateTime and IdleArrivalDateTime = CurrentLeg.ArrivalDate 23:59    
  --LoopDate = CurrentLeg.ArrivalDate ++    
  --LOOP WHILE LoopDate < NextLocDate    
  -- Insert 2: IdleDepartDateTime = LoopDate 00:01 and IdleArrivalDateTime = LoopDate 23:59    
  -- LoopDate ++    
  --END LOOP    
  --Insert 3: IdleDepartDateTime = NextLocDate 00:01 and IdleArrivalDateTime = NextLocDateTime     
 DECLARE @crTailNum  VARCHAR(20),@crDeptDateOrg DATETIME, @crAircraftID BIGINT, @crFleetID BIGINT;    
 DECLARE @crTripID BIGINT, @crLegID BIGINT,@crDepart DATETIME;    
 DECLARE @crDepartICAOID CHAR(4), @crArrive DATETIME, @crArriveICAOID CHAR(4);    
 DECLARE @crNextLocalDTTM DATETIME, @crDutyType CHAR(2),@crPassTotal INT,@crCrewCD VARCHAR(250),@crAircraft_Type VARCHAR(10),@crTripNUM BIGINT;    
 DECLARE @Loopdate DATETIME;     
     
--SELECT TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
--   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type    
--  FROM #TempFleetCalendar     
--  WHERE CONVERT(VARCHAR, Arrive, 101) < CONVERT(VARCHAR, NextLocalDTTM, 101)     
--  ORDER BY TailNum, DeptDateOrg;    
      
 DECLARE curCrewCal CURSOR FOR    
  SELECT TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type    
  FROM #TempFleetCalendar     
  WHERE CONVERT(VARCHAR, Arrive, 101) < CONVERT(VARCHAR, NextLocalDTTM, 101)     
  ORDER BY TailNum, DeptDateOrg;    
    
 OPEN curCrewCal;    
 FETCH NEXT FROM curCrewCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crDepart,     
                              @crArriveICAOID,@crArrive, @crNextLocalDTTM, @crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type;    
 WHILE @@FETCH_STATUS = 0    
 BEGIN    
  SET @Loopdate = '';    
  --STEP 1:    
     INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type)    
   VALUES (@crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,@crArrive,       
     @crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 23:59', 101),  NULL, 'R',    
     @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type)    
      
  --STEP 2:    
  SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:00', 101));    
  WHILE(CONVERT(VARCHAR, @Loopdate, 101) < CONVERT(VARCHAR, @crNextLocalDTTM, 101))    
  BEGIN    
   --PRINT 'Loopdate: ' + CONVERT(VARCHAR, @Loopdate, 101)    
   INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
   DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type)    
   VALUES (@crTailNum, @Loopdate, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 00:01', 101),       
     --@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),  NULL, 'R',    
     'RON',CONVERT(DATETIME, CONVERT(VARCHAR(10),@Loopdate, 101) + ' 23:59', 101),  NULL, NULL,    
     @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type)    
   SET @Loopdate = DATEADD(DAY, 1, @Loopdate);    
  END    
  ----STEP 3:    
  --INSERT INTO #TempFleetCalendar (TailNum, DeptDateOrg, AircraftID, FleetID, TripID, LegID,    
  -- DepartICAOID,Depart,  ArriveICAOID,Arrive,  NextLocalDTTM, DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type)    
  -- VALUES (@crTailNum, @Loopdate, @crAircraftID, @crFleetID, @crTripID, @crLegID,@crArriveICAOID,CONVERT(DATETIME, CONVERT(VARCHAR(10),@crArrive, 101) + ' 00:01', 101),       
  --   @crArriveICAOID, @crNextLocalDTTM,  NULL, 'R',    
  --   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type)    
         
   FETCH NEXT FROM curCrewCal INTO @crTailNum, @crDeptDateOrg, @crAircraftID, @crFleetID, @crTripID, @crLegID, @crDepartICAOID,@crDepart,     
                              @crArriveICAOID,@crArrive, @crNextLocalDTTM, @crDutyType,@crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type;    
 END    
 CLOSE curCrewCal;    
 DEALLOCATE curCrewCal;   
    
-----------------------------------------------------------  
--Populate NO trips for rest of the Fleet [Just insert FleetID and Date]  
 INSERT INTO #TempFleetCalendar (FleetID,TailNum, DeptDateOrg,DepartICAOID)  
  SELECT DISTINCT TD.FleetID,TD.TailNum, TD.DeptDateOrg,A.IcaoID FROM #TempTailDate TD  
  LEFT OUTER JOIN Airport A ON A.AirportID=TD.HomeBaseID
  WHERE NOT EXISTS(SELECT CC.FleetID,CC.TailNum,CC.DeptDateOrg FROM #TempFleetCalendar CC WHERE CC.FleetID = TD.FleetID AND CC.DeptDateOrg = TD.DeptDateOrg)  
-----------------------------------------------------------   
      
--SELECT DISTINCT DeptDateOrg,TailNum,DepartICAOID,Depart,ArriveICAOID,Arrive,    
--                DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type  FROM #TempFleetCalendar    
--  ORDER BY DeptDateOrg,TailNum    

--SELECT * FROM #TempFleetCalendar
--------- MAIN CALENDAR -------------    
CREATE TABLE #TailTable (  
   SetNumber VARCHAR(10),       
   Tail1 VARCHAR(20)   
  ,Tail2 VARCHAR(20)    
  ,Tail3 VARCHAR(20)    
  ,Tail4 VARCHAR(20)    
  ,Tail5 VARCHAR(20)   
 )     
  
 CREATE TABLE #TEMP(RowID INT IDENTITY,TailNum VARCHAR(20))  
 INSERT INTO #TEMP  
 SELECT DISTINCT TailNum FROM #TempFleetCalendar ORDER BY TailNum  
  
  DECLARE @SetNumber INT=1,    
--@COUNTER INT=0,    
@i INT=1,@j INT=6     
DECLARE @COUNT INT=1                                            
WHILE (@COUNT<=(SELECT  COUNT(distinct(tailnum)) FROM #TempFleetCalendar) )       
          BEGIN        
           INSERT INTO #TailTable(SetNumber,Tail1,Tail2,Tail3,Tail4,Tail5)   
           VALUES(@SetNumber,  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+1),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+2),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+3),  
                  (SELECT DISTINCT TailNum FROM #TEMP WHERE RowID=@i+4))  
         --  SET @COUNTER=@COUNTER+1    
           SET @i=@j    
           SET @j=@j+5    
           SET @SetNumber=@SetNumber+1    
           SET @COUNT=@COUNT+5    
         END    
         --select * from #TailTable
-------------------------------------    
DECLARE @TempTable TABLE (    
   ID INT identity(1, 1) NOT NULL    
  ,activedate DATE    
  ,tail_nmbr1 VARCHAR(20)     
  ,depicao_id1 CHAR(4)    
  ,deptime1 DATETIME    
  ,arricao_id1 CHAR(4)    
  ,arrtime1 DATETIME    
  ,flight1 CHAR(2)    
  ,pax_total1 INT    
  ,orig_nmbr1 BIGINT    
  ,crewlist1 VARCHAR(250)    
  ,tail_nmbr2 VARCHAR(20)    
  ,depicao_id2 CHAR(4)    
  ,deptime2 DATETIME    
  ,arricao_id2 CHAR(4)    
  ,arrtime2 DATETIME    
  ,flight2 CHAR(2)    
  ,pax_total2 INT    
  ,orig_nmbr2 BIGINT    
  ,crewlist2 VARCHAR(250)    
  ,tail_nmbr3 VARCHAR(20)    
  ,depicao_id3 CHAR(4)    
  ,deptime3 DATETIME    
  ,arricao_id3 CHAR(4)    
  ,arrtime3 DATETIME    
  ,flight3 CHAR(2)    
  ,pax_total3 INT    
  ,orig_nmbr3 BIGINT    
  ,crewlist3 VARCHAR(250)     
  ,tail_nmbr4 VARCHAR(20)    
  ,depicao_id4 CHAR(4)    
  ,deptime4 DATETIME    
  ,arricao_id4 CHAR(4)    
  ,arrtime4 DATETIME    
  ,flight4 CHAR(2)    
  ,pax_total4 INT    
  ,orig_nmbr4 BIGINT    
  ,crewlist4 VARCHAR(250)    
  ,tail_nmbr5 VARCHAR(20)    
  ,depicao_id5 CHAR(4)    
  ,deptime5 DATETIME    
  ,arricao_id5 CHAR(4)    
  ,arrtime5 DATETIME    
  ,flight5 CHAR(2)    
  ,pax_total5 INT    
  ,orig_nmbr5 BIGINT    
  ,crewlist5 VARCHAR(250)    
  ,tail_1 VARCHAR(20)    
  ,tail_2 VARCHAR(20)    
  ,tail_3 VARCHAR(20)    
  ,tail_4 VARCHAR(20)    
  ,tail_5 VARCHAR(20)    
  ,type_1 VARCHAR(10)    
  ,type_2 VARCHAR(10)     
  ,type_3 VARCHAR(10)     
  ,type_4 VARCHAR(10)     
  ,type_5 VARCHAR(10)   
  ,rptgroup INT)    
 DECLARE @TailVal VARCHAR(20);          
DECLARE @PrevDt DATE=NULL,@ID INT,@Tail VARCHAR(20),@COUNT1 INT;  
DECLARE @Prevset INT=1,@PrevDeptICAOID CHAR(4),@PrevAType VARCHAR(9);   
 --SELECT SetNumber,Tail1 FROM #TailTable   
 --SELECT DISTINCT DeptDateOrg,TailNum,DepartICAOID,Depart,ArriveICAOID,Arrive,    
 --               DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type  FROM #TempFleetCalendar    
 -- ORDER BY  DeptDateOrg,TailNum   
DECLARE @Set1 INT,@Tail1 VARCHAR(20),@Set2 INT,@Tail2 VARCHAR(20),@Set3 INT,@Tail3 VARCHAR(20),@Set4 INT,@Tail4 VARCHAR(20),@Set5 INT,@Tail5 VARCHAR(20);   
DECLARE CalendarInfo CURSOR FOR   
SELECT DISTINCT DeptDateOrg,TailNum,DepartICAOID,Depart,ArriveICAOID,Arrive,    
                DutyType,PassTotal,TripNUM,CrewCD,Aircraft_Type  FROM #TempFleetCalendar    
  ORDER BY  DeptDateOrg,TailNum   
          
 OPEN CalendarInfo;              
           
   FETCH NEXT FROM CalendarInfo INTO @crDeptDateOrg,@crTailNum,@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,  
                                   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type;        
  WHILE @@FETCH_STATUS = 0              
  BEGIN 
    
  SELECT @SetNumber=SetNumber FROM #TailTable WHERE Tail1=@crTailNum OR Tail2=@crTailNum OR Tail3=@crTailNum OR Tail4=@crTailNum OR Tail5=@crTailNum 
  SELECT @TailVal=(CASE(@crTailNum) WHEN Tail1 THEN 1  
						   WHEN Tail2 THEN 2
						   WHEN Tail3 THEN 3
						   WHEN Tail4 THEN 4
						   WHEN Tail5 THEN 5 END) FROM #TailTable WHERE SetNumber=@SetNumber

  
  --SELECT DISTINCT @Tail1=Tail1 FROM #TailTable WHERE SetNumber=@SetNumber
  --SELECT DISTINCT @Tail2=Tail2 FROM #TailTable WHERE SetNumber=@SetNumber
  --SELECT DISTINCT @Tail3=Tail3 FROM #TailTable WHERE SetNumber=@SetNumber
  --SELECT DISTINCT @Tail4=Tail4 FROM #TailTable WHERE SetNumber=@SetNumber
  --SELECT DISTINCT @Tail5=Tail5 FROM #TailTable WHERE SetNumber=@SetNumber
IF(@crDeptDateOrg!=@PrevDt)  
      BEGIN  
        SELECT @COUNT1=COUNT(*) FROM @TempTable WHERE activedate=@PrevDt  
        IF @COUNT1=1  
          BEGIN  
          INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
          END  
        ELSE IF (@COUNT1 >1 AND @COUNT1 < 8)  
         BEGIN  
            WHILE(@COUNT1<8)  
             BEGIN  
              INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@PrevDt,@Prevset)  
              SET @COUNT1=@COUNT1+1    
             END  
         END  
      END  
      
 -- print @TailVal
IF(@TailVal=1)  
  
  BEGIN  
    SELECT DISTINCT @Tail1=Tail1 FROM #TailTable WHERE SetNumber=@SetNumber
--PRINT @crTailNum+','+@Tail1  
 --PRINT @PrevDt  
 -- PRINT CONVERT(DATE,@crDeptDateOrg)  
 -- PRINT @crTailNum 
 
  IF(@crTailNum=@Tail1 AND @PrevDt IS NULL) 
  BEGIN
  
   INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)  
     
   INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)  

 END
 IF(@crTailNum=@Tail1 AND CONVERT(DATE,@crDeptDateOrg)!=@PrevDt)
 -- PRINT @crTailNum+','+@Tail1  
   --PRINT @PrevDt  
   --PRINT @crDeptDateOrg  
  -- PRINT @Tail1  
   BEGIN  
   --PRINT @crTailNum+','+@Tail1  
   --PRINT @PrevDt  
   --PRINT @crDeptDateOrg 

   INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(CONVERT(DATE,@crDeptDateOrg),(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)  
     
   INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)  
---SELECT * FROM @TempTable
   END  
 ELSE IF(@crTailNum=@Tail1 AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt)  
      BEGIN  
       INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)  
     
     INSERT INTO @TempTable(activedate,tail_nmbr1,depicao_id1,deptime1,arricao_id1,arrtime1,flight1,pax_total1,orig_nmbr1,crewlist1,tail_1,type_1,rptgroup)  
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)  
       END 
        
 ELSE IF(@crTailNum!=@Tail AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt AND @crTailNum=@Tail1)  
       BEGIN  
           UPDATE @TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id1=@crDepartICAOID,deptime1=@crDepart,arricao_id1=@crArriveICAOID,arrtime1=@crArrive,flight1=@crDutyType,pax_total1=@crPassTotal,orig_nmbr1=@crTripNUM,tail_1=@crTailNum,type_1=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg
           SET @ID=SCOPE_IDENTITY()  
           UPDATE @TempTable SET tail_nmbr1=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id1=@crDepartICAOID,crewlist1=@crCrewCD,tail_1=@crTailNum,type_1=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID+1 
       END  
  
 --SET @PrevDt1=@crDeptDateOrg
 -- SELECT * FROM @TempTable
END   
        
 --SELECT SetNumber,Tail2 FROM #TailTable    
ELSE IF (@TailVal=2) 
BEGIN

SELECT DISTINCT @Tail2=Tail2 FROM #TailTable WHERE SetNumber=@SetNumber

IF( CONVERT(DATE,@crDeptDateOrg)=@PrevDt AND @crTailNum=@Tail) --@crTailNum=@Tail2 AND
      BEGIN  
        INSERT INTO @TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO @TempTable(activedate,tail_nmbr2,depicao_id2,deptime2,arricao_id2,arrtime2,flight2,pax_total2,orig_nmbr2,crewlist2,tail_2,type_2,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  

 ELSE IF(@crTailNum!=@Tail AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt) --AND @crTailNum=@Tail2)  
       BEGIN  
          SET @ID=SCOPE_IDENTITY()
           UPDATE @TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id2=@crDepartICAOID,deptime2=@crDepart,arricao_id2=@crArriveICAOID,arrtime2=@crArrive,flight2=@crDutyType,pax_total2=@crPassTotal,orig_nmbr2=@crTripNUM,tail_2=@crTailNum,type_2=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID-1 
           UPDATE @TempTable SET tail_nmbr2=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist2=@crCrewCD,tail_2=@crTailNum,type_2=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID

       END  
   END
ELSE IF (@TailVal=3) 
BEGIN

SELECT DISTINCT @Tail3=Tail3 FROM #TailTable WHERE SetNumber=@SetNumber

IF( CONVERT(DATE,@crDeptDateOrg)=@PrevDt AND @crTailNum=@Tail) --@crTailNum=@Tail3 AND
      BEGIN  
        INSERT INTO @TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO @TempTable(activedate,tail_nmbr3,depicao_id3,deptime3,arricao_id3,arrtime3,flight3,pax_total3,orig_nmbr3,crewlist3,tail_3,type_3,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  

 ELSE IF(@crTailNum!=@Tail AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt) --AND @crTailNum=@Tail3)  
       BEGIN  
          SET @ID=SCOPE_IDENTITY()
           UPDATE @TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id3=@crDepartICAOID,deptime3=@crDepart,arricao_id3=@crArriveICAOID,arrtime3=@crArrive,flight3=@crDutyType,pax_total3=@crPassTotal,orig_nmbr3=@crTripNUM,tail_3=@crTailNum,type_3=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID-1 
           UPDATE @TempTable SET tail_nmbr3=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist3=@crCrewCD,tail_3=@crTailNum,type_3=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID

       END  
   END
   
 ELSE IF (@TailVal=4) 
BEGIN

SELECT DISTINCT @Tail4=Tail4 FROM #TailTable WHERE SetNumber=@SetNumber

IF( CONVERT(DATE,@crDeptDateOrg)=@PrevDt AND @crTailNum=@Tail) --@crTailNum=@Tail4 AND
      BEGIN  
        INSERT INTO @TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO @TempTable(activedate,tail_nmbr4,depicao_id4,deptime4,arricao_id4,arrtime4,flight4,pax_total4,orig_nmbr4,crewlist4,tail_4,type_4,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  

 ELSE IF(@crTailNum!=@Tail AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt) --AND @crTailNum=@Tail4)  
       BEGIN  
          SET @ID=SCOPE_IDENTITY()
           UPDATE @TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id4=@crDepartICAOID,deptime4=@crDepart,arricao_id4=@crArriveICAOID,arrtime4=@crArrive,flight4=@crDutyType,pax_total4=@crPassTotal,orig_nmbr4=@crTripNUM,tail_4=@crTailNum,type_4=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID-1 
           UPDATE @TempTable SET tail_nmbr4=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist4=@crCrewCD,tail_4=@crTailNum,type_4=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID

       END  
   END
   
 ELSE IF (@TailVal=5) 
BEGIN

SELECT DISTINCT @Tail5=Tail5 FROM #TailTable WHERE SetNumber=@SetNumber

IF( CONVERT(DATE,@crDeptDateOrg)=@PrevDt AND @crTailNum=@Tail) --@crTailNum=@Tail5 AND
      BEGIN  
        INSERT INTO @TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,@crPassTotal,@crTripNUM,NULL,@crTailNum,@crAircraft_Type,@SetNumber)
        INSERT INTO @TempTable(activedate,tail_nmbr5,depicao_id5,deptime5,arricao_id5,arrtime5,flight5,pax_total5,orig_nmbr5,crewlist5,tail_5,type_5,rptgroup)
                VALUES(@crDeptDateOrg,(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),NULL,NULL,NULL,NULL,NULL,NULL,NULL,@crCrewCD,@crTailNum,@crAircraft_Type,@SetNumber)
       END  

 ELSE IF(@crTailNum!=@Tail AND CONVERT(DATE,@crDeptDateOrg)=@PrevDt) --AND @crTailNum=@Tail5)  
       BEGIN  
          SET @ID=SCOPE_IDENTITY()
           UPDATE @TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),depicao_id5=@crDepartICAOID,deptime5=@crDepart,arricao_id5=@crArriveICAOID,arrtime5=@crArrive,flight5=@crDutyType,pax_total5=@crPassTotal,orig_nmbr5=@crTripNUM,tail_5=@crTailNum,type_5=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID-1 
           UPDATE @TempTable SET tail_nmbr5=(CASE WHEN @crDepart IS NULL THEN NULL ELSE @crTailNum END),crewlist5=@crCrewCD,tail_5=@crTailNum,type_5=@crAircraft_Type WHERE rptgroup=@SetNumber AND activedate=@crDeptDateOrg AND ID=@ID

       END  
 END

SET @PrevDt=@crDeptDateOrg  
SET @Tail=@crTailNum  
SET @Prevset=@SetNumber  
SET @PrevDeptICAOID=@crDepartICAOID  
SET @PrevAType=@crAircraft_Type 
  
  
FETCH NEXT FROM CalendarInfo INTO @crDeptDateOrg,@crTailNum,@crDepartICAOID,@crDepart,@crArriveICAOID,@crArrive,@crDutyType,  
                                   @crPassTotal,@crTripNUM,@crCrewCD,@crAircraft_Type;    
END  
CLOSE CalendarInfo;              
DEALLOCATE CalendarInfo; 
  
  SELECT @COUNT1=COUNT(*) FROM @TempTable WHERE activedate=@DATETO 
  SELECT @Prevset=rptgroup FROM @TempTable WHERE activedate=@DATETO  
        IF @COUNT1=1  
          BEGIN  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
           INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
          END  
        ELSE IF (@COUNT1 >1 AND @COUNT1 < 8)  
         BEGIN  
            WHILE(@COUNT1<8)  
             BEGIN  
              INSERT INTO @TempTable(activedate,rptgroup)  
                VALUES(@DATETO,@Prevset)  
              SET @COUNT1=@COUNT1+1    
             END  
         END  
   
 --SELECT * FROM @TempTable
 SET @SetNumber=1;

 WHILE (@SetNumber<=(SELECT DISTINCT COUNT(*) FROM #TailTable))
 BEGIN
 UPDATE @TempTable SET tail_1=(SELECT DISTINCT tail_1 FROM @TempTable WHERE rptgroup=@SetNumber AND tail_1 IS NOT NULL),
					   tail_2=(SELECT DISTINCT tail_2 FROM @TempTable WHERE rptgroup=@SetNumber AND tail_2 IS NOT NULL),
					   tail_3=(SELECT DISTINCT tail_3 FROM @TempTable WHERE rptgroup=@SetNumber AND tail_3 IS NOT NULL),
					   tail_4=(SELECT DISTINCT tail_4 FROM @TempTable WHERE rptgroup=@SetNumber AND tail_4 IS NOT NULL),
					   tail_5=(SELECT DISTINCT tail_5 FROM @TempTable WHERE rptgroup=@SetNumber AND tail_5 IS NOT NULL),
					   type_1=(SELECT DISTINCT type_1 FROM @TempTable WHERE rptgroup=@SetNumber AND type_1 IS NOT NULL),
					   type_2=(SELECT DISTINCT type_2 FROM @TempTable WHERE rptgroup=@SetNumber AND type_2 IS NOT NULL),
					   type_3=(SELECT DISTINCT type_3 FROM @TempTable WHERE rptgroup=@SetNumber AND type_3 IS NOT NULL),
					   type_4=(SELECT DISTINCT type_4 FROM @TempTable WHERE rptgroup=@SetNumber AND type_4 IS NOT NULL),
					   type_5=(SELECT DISTINCT type_5 FROM @TempTable WHERE rptgroup=@SetNumber AND type_5 IS NOT NULL)
				WHERE rptgroup=@SetNumber
 SET @SetNumber=@SetNumber+1
 END

 --UPDATE @TempTable SET tail_nmbr1=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE @TempTable SET tail_nmbr2=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE @TempTable SET tail_nmbr3=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE @TempTable SET tail_nmbr4=NULL WHERE orig_nmbr1 IS NULL	
 --UPDATE @TempTable SET tail_nmbr5=NULL WHERE orig_nmbr1 IS NULL				  
   
 SELECT * FROM @TempTable ORDER BY [ID]                                
END
  
 -- EXEC spGetReportPREAircraftCalendar1ExportInformation 'JWILLIAMS_11', '2012-10-01 ', '2012-10-30', '', '' 
 
 --select * from Fleet
 
 --select * from PreflightLeg


GO


