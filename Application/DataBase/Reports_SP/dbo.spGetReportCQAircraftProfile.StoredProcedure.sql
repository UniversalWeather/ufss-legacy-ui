IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQAircraftProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQAircraftProfile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportCQAircraftProfile] 
			(@UserCD VARCHAR(30)  
              ,@UserCustomerID VARCHAR(30)  
              ,@FileNumber VARCHAR(200)='',  
               @QuoteNUM VARCHAR(200)='',  
               @LegNUM VARCHAR(200)='',  
               @DateFrom DATETIME=NULL,  
               @DateTo DATETIME=NULL,  
               @TailNum VARCHAR(200)='',  
               @VendorCD VARCHAR(200)='',  
               @HomeBaseCD VARCHAR(200)='',  
               @AircraftCD VARCHAR(200)='',  
               @Customer VARCHAR(30)='',  
               @SalesPerson VARCHAR(200)=''  
)                                                
AS  
  
-- =============================================  
-- SPC Name:spGetReportCQAircraftProfile    
-- Author: Askar  
-- Create date: 23 Feb 2013  
-- Description: Get Report Charter Quote Aircraft Profile  
-- Revision History  
-- Date  Name  Ver  Change  
--   
-- =============================================  
  
BEGIN  


SELECT DISTINCT F.TailNum Tail,  
       F.SerialNum Serial,  
       CASE WHEN A.AircraftCD IS NOT NULL OR A.AircraftCD<>'' THEN  A.AircraftCD+' - '+A.AircraftDescription ELSE '' END Type, 
       FP.ManufactureYear, 
       F.MaximumPassenger Seats,  
       CASE WHEN FPD.IsPrintable=1 THEN FPD.FleetDescription ELSE '' END FleetDescription
         
 FROM Fleet F LEFT OUTER JOIN FleetPair FP ON FP.FleetID=F.FleetID  
              LEFT OUTER JOIN FleetProfileDefinition FPD ON FPD.FleetID=F.FleetID AND FPD.IsPrintable=1  
              LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID  
 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)
   AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )
   AND F.IsDeleted=0 
   ORDER BY F.TailNum


/*  
SELECT DISTINCT C.CompanyName Company_Name,  
       F.TailNum Tail,  
       F.SerialNum Serial,  
       A.AircraftCD+'-'+A.AircraftTypeCD Type,  
       F.MaximumPassenger Seats,  
       FPD.FleetDescription,
       FP.ManufactureYear  
         
 FROM CQFile CF LEFT OUTER JOIN CQMain CM ON CF.CQFileID=CM.CQFileID  
                        INNER JOIN Fleet F ON F.FleetID=CM.FleetID  
                        LEFT OUTER JOIN FleetPair FP ON FP.FleetID=F.FleetID  
                        LEFT OUTER JOIN FleetProfileDefinition FPD ON FPD.FleetID=F.FleetID  
                        LEFT OUTER JOIN CQLeg CL ON CL.CQMainID=CM.CQMainID  
                        LEFT OUTER JOIN Vendor V ON V.VendorID=CM.VendorID  
                        INNER JOIN Company C ON C.HomebaseID=F.HomebaseID  
                        LEFT OUTER JOIN Aircraft A ON F.AircraftID=A.AircraftID  
                        LEFT OUTER JOIN Airport AA ON AA.AirportID=C.HomebaseAirportID  
                        LEFT OUTER JOIN SalesPerson SP ON CF.SalesPersonID=SP.SalesPersonID  
                        INNER JOIN CQCustomer CQC ON CQC.CustomerID=CF.CustomerID  
   WHERE ( CF.FileNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FileNumber, ',')) OR @FileNumber = '' )  
    AND ( CM.QuoteNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '' )  
    AND ( CL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '' )  
    AND ( F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '' )  
    AND ( V.VendorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@VendorCD, ',')) OR @VendorCD = '' )  
    AND ( AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '' )  
    AND ( A.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '' )  
    AND ( SP.SalesPersonCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@SalesPerson, ',')) OR @SalesPerson = '' )  
    AND CF.EstDepartureDT BETWEEN @DateFrom AND @DateTo  
    AND CM.CustomerID =dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))  
    AND CQC.CustomerID=@CustomerID  
    AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@Customer, ',')) OR @Customer='')  

*/
                         
END                                             
                                                
--------EXEC  spGetReportCQAircraftProfile 'supervisor_16','10016','','','','2007-01-01','2010-01-01','','','','','',''                                   


GO


