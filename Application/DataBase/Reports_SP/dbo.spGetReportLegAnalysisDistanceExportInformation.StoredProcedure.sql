IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportLegAnalysisDistanceExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportLegAnalysisDistanceExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportLegAnalysisDistanceExportInformation]      
(    
  @CustomerID BIGINT,    
  @IcaoID CHAR(4),    
  @FromDate Date,    
  @ToDate Date    
 )    
-- =============================================    
-- Author: Mathes    
-- Create date: 01-08-2012    
-- Description: Leg Analysis/Hours(FC) - Report     
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- =============================================    
AS    
SET NOCOUNT ON    
BEGIN    
      IF OBJECT_ID('tempdb..#TempDist') is not null    
      DROP table #TempDist    
         
		Create Table #TempDist (DistId Int,DistFrom Int,DistTo Int)    
		INSERT INTO #TempDist (DistId,DistFrom,DistTo)    
		Select 1,0,24  union All     
		Select 2,25,49  union All     
		Select 3,50,74  union All     
		Select 4,75,99  union All     
		Select 5,100,124  union All     
		Select 6,125,149  union All     
		Select 7,150,174  union All     
		Select 8,175,199  union All     
		Select 9,200,249  union All     
		Select 10,250,299  union All     
		Select 11,300,399  union All     
		Select 12,400,499  union All     
		Select 13,500,599  union All     
		Select 14,600,699  union All     
		Select 15,700,799  union All     
		Select 16,800,899  union All     
		Select 17,900,999  union All     
		Select 18,1000,1099  union All     
		Select 19,1100,1199  union All     
		Select 20,1200,1299  union All     
		Select 21,1300,1399  union All     
		Select 22,1400,1499  union All     
		Select 23,1500,1599  union All     
		Select 24,1600,1699  union All     
		Select 25,1700,1799  union All     
		Select 26,1800,1899  union All     
		Select 27,1900,1999  union All     
		Select 28,2000,2099  union All     
		Select 29,2100,2199  union All     
		Select 30,2200,2299  union All     
		Select 31,2300,2399  union All     
		Select 32,2400,2499  union All     
		Select 33,2500,2599  union All     
		Select 34,2600,2699  union All     
		Select 35,2700,2799  union All     
		Select 36,2800,2899  union All     
		Select 37,2900,2999  union All     
		Select 38,3000,3099  union All     
		Select 39,3100,3199  union All     
		Select 40,3200,3299  union All     
		Select 41,3300,3399  union All     
		Select 42,3400,3499  union All     
		Select 43,3500,3599  union All     
		Select 44,3600,3699  union All     
		Select 45,3700,3799  union All     
		Select 46,3800,3899  union All     
		Select 47,3900,3999  union All     
		Select 48,4000,9999     
		    
		Declare @DistId Int    
		Declare @DistFrom Int    
		Declare @DistTo Int    
		   
		Declare @RowId Int    
		Declare @NumRows Int     
       
        IF OBJECT_ID('tempdb..#TempReport') is not null    
          DROP table #TempReport    
           
        Create Table #TempReport (FromMile Int,
                                  ToMile Int,
                                  FleetID BIGINT, 
                                  TailNum CHAR(6),
                                  TotLine Char(1),
                                  AircraftCD CHAR(3),
                                  NoOfLegs Int,
                                  TotalLegs Int,
                                  PercOfLegs Numeric(6,2),
                                  BlockHours Numeric(10,2),
                                  FlightHours Numeric(10,2),
                                  Miles Int,
                                  FuelBurn Int,
                                  AvgFBperLeg Numeric(6,2),
                                  NoOfPax Int, 
                                  PctOfPax Numeric(6,2))  
                                              
        Declare @TailNum CHAR(6)
        Declare @AircraftCD CHAR(3)
        Declare @NoOfLegs INT
        Declare @TotalLegs INT
        Declare @PercOfLegs Numeric(6,2)
        Declare @BlockHours Numeric(10,2) 
        Declare @FlightHours Numeric(10,2)
        Declare @Miles Int
        Declare @FuelBurn Int
        Declare @AvgFB Numeric(6,2)  
        Declare @NoOfPax Int
        Declare @PctOfPax Numeric(6,2)
        
        Declare @TotBlockHours Numeric(10,2)
        Declare @TotFlightHours Numeric(10,2)
                             
        IF OBJECT_ID('tempdb..#TempFleet') is not null    
           DROP table #TempFleet    
         
		  SELECT  Distinct Fleet.FleetID  Into #TempFleet      
				  FROM    Fleet INNER JOIN    
				  FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
				  FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
				  PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
				  PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
				  PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID                          
		  WHERE   PostflightLeg.CustomerID=@CustomerID And     
				  CONVERT(DATE,PostflightLeg.ScheduledTM) BETWEEN CONVERT(DATE,@FromDate) AND CONVERT(DATE,@Todate)
           
          BEGIN    
          Declare @FleetID BigInt    
          WHILE ( SELECT  COUNT(*) FROM #TempFleet ) > 0    
          BEGIN    
            SELECT Top 1 @FleetID=FleetId From #TempFleet        
            ------------------------------------------------------------------------------Distinct FleetID starts    
            BEGIN                   
            SET @TotBlockHours=0
            SET @TotFlightHours=0
            
            SET @RowId = 1       
            SET @NumRows =  (SELECT COUNT(*) FROM #TempDist)     
            IF @NumRows > 0     
                WHILE (@RowId <= (SELECT MAX(DistID) FROM #TempDist))      
                BEGIN    
    
				Set @DistId=0    
				Set @DistFrom=0     
				Set @DistTo=0    
 
				SELECT @DistId=DistId,@DistFrom=DistFrom,@DistTo=DistTo From #TempDist Where DistId=@RowId     
				-----------------------------------------------------------------------------Actual Report Area Starts    
				
				SELECT   @FleetID=ResultSet.FleetID,				         
				         @TailNum=ResultSet.TailNum,  
						 @AircraftCD=ResultSet.AircraftCD,  
						 @NoOfLegs=ResultSet.NoOfLegs,
						 @TotalLegs=ResultSet.TotalLegs,
						 @PercOfLegs=ResultSet.PercOfLegs,
						 @BlockHours=ResultSet.BlockHours,     
						 @FlightHours=ResultSet.FlightHours,
						 @Miles=ResultSet.Distance,  
						 @FuelBurn=ResultSet.FuelUsed,    
						 @AvgFB=ResultSet.AvgFB,        
						 @NoOfPax=ResultSet.NoOfPax,  
						 @PctOfPax=ResultSet.PctOfPax
				FROM   ( SELECT
				         Fleet.FleetID,				         
				         Fleet.TailNum,  
						 Fleet.AircraftCD, 
						 IsNull(Count(PostflightLeg.Distance),0) As NoOfLegs,
						 IsNull(SUM(PostflightLeg.Distance),0) As TotalLegs,
						 CAST(ROUND( IsNull(Count(PostflightLeg.Distance),0)/IsNull(SUM(PostflightLeg.Distance),1)* 100, 2, 1) AS DECIMAL(9,2)) As PercOfLegs,
						 PostflightLeg.BlockHours,     
						 PostflightLeg.FlightHours,
						 PostflightLeg.Distance,  
						 PostflightLeg.FuelUsed,    
						 PostflightLeg.FuelUsed/IsNull(SUM(PostflightLeg.Distance),1 ) As AvgFB,        
						 IsNull(SUM(PostflightLeg.PassengerTotal),0) As NoOfPax,  
						 CAST(ROUND( IsNull(Count(PostflightLeg.Distance),0)/ IsNull(SUM(PostflightLeg.PassengerTotal),1) * 100 , 2, 1) AS DECIMAL(9,2)) As PctOfPax,						 
						 PostflightLeg.CustomerID,
						 PostflightLeg.ScheduledTM
				 FROM    Fleet INNER JOIN    
						 FleetGroupOrder ON Fleet.FleetID = FleetGroupOrder.FleetID INNER JOIN    
						 FleetGroup ON FleetGroupOrder.FleetGroupID = FleetGroup.FleetGroupID INNER JOIN    
						 PostflightMain ON Fleet.FleetID = PostflightMain.FleetID INNER JOIN    
						 PostflightLeg ON PostflightMain.POLogID = PostflightLeg.POLogID INNER JOIN    
						 PostflightPassenger ON PostflightLeg.POLegID = PostflightPassenger.POLegID   
				GROUP BY Fleet.FleetID,
				         PostflightLeg.CustomerID,
				         Fleet.TailNum,      
						 Fleet.AircraftCD,    
						 PostflightLeg.ScheduledTM,
						 PostflightLeg.Distance ,    
						 PostflightLeg.BlockHours,     
						 PostflightLeg.FlightHours,  
						 PostflightLeg.FuelUsed,  
						 PostflightLeg.PassengerTotal              
                HAVING   PostflightLeg.CustomerID=@CustomerID And   
                         Fleet.FleetID=@FleetID And  
                         PostflightLeg.Distance Between @DistFrom And @DistTo And  
                         PostflightLeg.ScheduledTM BETWEEN @FromDate AND @Todate)  
                ResultSet              
                
     
               Insert Into #TempReport(FromMile,ToMile,FleetID,TailNum,TotLine,AircraftCD,NoOfLegs,TotalLegs, PercOfLegs,
                      BlockHours,FlightHours,Miles,FuelBurn,AvgFBperLeg,NoOfPax,PctOfPax)
               Values(@DistFrom,@DistTo,@FleetID,@TailNum,'',@AircraftCD,@NoOfLegs,@TotalLegs,@PercOfLegs,
                      @BlockHours,@FlightHours,@Miles,@FuelBurn,@AvgFB,@NoOfPax,@PctOfPax)   
                      
                       SET @TotBlockHours = @TotBlockHours + @BlockHours
                       SET @TotFlightHours = @TotFlightHours + @FlightHours
                     --the below line added because in excel report displaying total for 2 columns
               IF @RowId = 48
               BEGIN
                  Insert Into #TempReport(FromMile,ToMile,FleetID,TailNum,TotLine,AircraftCD,NoOfLegs,TotalLegs, PercOfLegs,
                          BlockHours,FlightHours,Miles,FuelBurn,AvgFBperLeg,NoOfPax,PctOfPax)
                  Values(0,0,0,@TailNum,'S',@AircraftCD,0,0,0,@TotBlockHours,@TotFlightHours,0,0,0,0,0) 
               END       
                            
                        ------------------------------------------------------------------------------Actual Report Area Ends    
                        Set @RowId=@RowId+1    
                    END        
         
         END        
         -----------------------------------------------------------------------------Distinct FleetID ends    
         DELETE FROM #TempFleet WHERE FleetID=@FleetID     
     END      
   END    
    
   IF OBJECT_ID('tempdb..#TempFleet') is not null    
      DROP table #TempFleet     
          
   IF OBJECT_ID('tempdb..#TempDist') is not null    
      DROP table #TempDist     
         
   SELECT  AircraftCD As [ac_code],FromMile As [beginmile],ToMile As [endmile],TailNum As[tail_nmbr],TotLine As [tot_line],
           NoOfLegs  As [numlegs], PercOfLegs  As [pctlegs],BlockHours  As [blk_hrs],FlightHours  As [flt_hrs],
           Miles  As [distance],FuelBurn  As [fuel_used],AvgFBperLeg  As [avgbleg],NoOfPax  As [numpax], PctOfPax  As [pctpax] 
   FROM    #TempReport     
       
   IF OBJECT_ID('tempdb..#TempReport') is not null    
      DROP table #TempReport        
 END


GO


