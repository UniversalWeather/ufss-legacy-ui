IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDateFormatStringByUserCDForTripSheet]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetDateFormatStringByUserCDForTripSheet]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetDateFormatStringByUserCDForTripSheet] (@UserCD VARCHAR(30),@IsLong SMALLINT)
RETURNS VARCHAR(10)
WITH EXECUTE AS CALLER
AS
BEGIN
    DECLARE @DateFormat VARCHAR(10)
	DECLARE @ApplicationDateFormat VARCHAR(25)
	SELECT @ApplicationDateFormat=Upper(ApplicationDateFormat) FROM Company 
	       WHERE HomeBaseID =  dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
	       AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
--------------------------------------------------
-- @IsLong = 0 --> SHORT DATE --> LIKE 'MM/dd/yy'
-- @IsLong = 1 --> LONG DATE --> LIKE 'MM/dd/yyyy'
-- @IsLong = 2 --> DATE AND MONTH --> LIKE 'MM/dd'
---------------------------------------------------


	IF @ApplicationDateFormat='AMERICAN'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='MM/dd/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='MM/dd'
	  END
	  ELSE
	  Begin
	    SET @DateFormat='MM/dd/yy'
	  End 
	END
	ELSE IF @ApplicationDateFormat='ANSI'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='yyyy.MM.dd'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='MM.dd'
	  END
	  Else 
	  Begin
	    SET @DateFormat='yy.MM.dd'
	  End 
	END
	ELSE IF @ApplicationDateFormat='BRITISH'  
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='dd/MM/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='dd/MM'
	  END
	  Else 
	  Begin
	    SET @DateFormat='dd/MM/yy'
	  End 
	END
	ELSE IF @ApplicationDateFormat='FRENCH'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='dd/MM/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='dd/MM'
	  END
	  Else 
	  Begin
	    SET @DateFormat='dd/MM/yy'
	  End 
	END
    ELSE IF @ApplicationDateFormat='GERMAN'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='dd.MM.yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='dd.MM'
	  END
	  Else 
	  Begin
	    SET @DateFormat='dd.MM.yy'
	  End 
	END   
	ELSE IF @ApplicationDateFormat='ITALIAN'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='dd-MM-yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='dd-MM'
	  END
	  Else 
	  Begin
	    SET @DateFormat='dd-MM-yy'
	  End 
	END   
	ELSE IF @ApplicationDateFormat='JAPAN'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='yyyy/MM/dd'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='MM/dd'
	  END
	  Else 
	  Begin
	    SET @DateFormat='yy/MM/dd'
	  End 
	END  
	ELSE IF @ApplicationDateFormat='TAIWAN' 
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='yyyy/MM/dd'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='MM/dd'
	  END
	  Else 
	  Begin
	    SET @DateFormat='yy/MM/dd'
	  End 
	END
	ELSE IF @ApplicationDateFormat='DMY'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='dd/MM/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='d/M'
	  END
	  Else 
	  Begin
	    SET @DateFormat='d/M/yy'
	  End 
	END
	ELSE IF @ApplicationDateFormat='MDY'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='MM/dd/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='M/d'
	  END
	  Else 
	  Begin
	    SET @DateFormat='M/d/yy'
	  End 
	END
	ELSE IF @ApplicationDateFormat='YMD'
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='yyyy/MM/dd'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='M/d'
	  END
	  Else 
	  Begin
	    SET @DateFormat='yy/M/d'
	  End 
	END 
	ELSE 
	BEGIN
	  IF @IsLong =1 
	  Begin
	   SET @DateFormat='MM/dd/yyyy'
	  End
	  Else IF @IsLong = 2 
	  BEGIN
		SET @DateFormat='MM/dd'
	  END
	  Else 
	  Begin
	    SET @DateFormat='MM/dd/yy'
	  End 
	END     
		
RETURN  @DateFormat
END; 

-- SELECT DBO.GetDateFormatStringByUserCDForTripSheet('JWILLIAMS_13', 1)
 


GO


