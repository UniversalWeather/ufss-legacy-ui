IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSTripSheetChecklistExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSTripSheetChecklistExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPRETSTripSheetChecklistExportInformation]
	@UserCD            AS VARCHAR(30)    
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
AS
BEGIN

-- =========================================================================
-- SPC Name: spGetReportPRETSTripSheetchecklistExportInformation
-- Author: SINDHUJA.K
-- Create date: 13 Sep 2012
-- Description: Get Preflight Tripsheet Checklist Report Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================
SET NOCOUNT ON

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --
	
CREATE TABLE #tblFlag 
	(
	ROWID INT
	,flag CHAR(1)
	)
	
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (1,'O')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (2,'D')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (3,'C')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (4,'P')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (5,'F')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (6,'E')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (7,'H')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (8,'Q')
	INSERT INTO #tblFlag(ROWID, FLAG) VALUES (9,'G')

-----------------
SELECT * INTO #tblTripSheet FROM 
(
		SELECT DISTINCT
		[TripID] = PM.TripID
		,[LegID] = PL.LegID
		,[orig_nmbr] = PM.TripNUM
		,[dispatchno] = PM.DispatchNUM
		--,[lowdate] = '' --LEFT(dbo.GetTripDatesByTripIDUserCD(@TripID, @UserCD), 8)
		,[lowdate] = CONVERT(varchar(10), (SELECT TOP 1 DepartureDTTMLocal FROM PreflightLeg
				WHERE TripID = PM.TripID ORDER BY LegNum ASC), dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		--,[highdate] = '' --RIGHT(dbo.GetTripDatesByTripIDUserCD(@TripID, @UserCD), 8)
		,[highdate] = CONVERT(varchar(10), (SELECT TOP 1 ArrivalDTTMLocal FROM PreflightLeg
				WHERE TripID = PM.TripID ORDER BY LegNum DESC), dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[tail_nmbr] = F.TailNum
		,[type_code] = A.AircraftCD
		,[desc] = PM.TripDescription
		,[reqcode] = Req.PassengerRequestorCD --PM.PassengerRequestorID
		,[paxname] = Req.FirstName + ',' + Req.LastName + ' ' + Req.MiddleInitial
		,[phone] = Req.PhoneNum --PAX.PhoneNum
		,[leg_num] = PL.LegNum
		,[locdep] = PL.DepartureDTTMLocal
		,[locarr] = PL.ArrivalDTTMLocal
		,[timeapprox] = PL.IsApproxTM
		,[chkgroup] = PL.CheckGroup
		,[dep24out] = PL.DepartureDTTMLocal
		,[arr24in] = PL.ArrivalDTTMLocal 
		,[depicao_id] = AD.IcaoID
		,[depcity] = AD.CityName
		,[arricao_id] = AA.IcaoID
		,[arrcity] = AA.CityName 
		,[depfbo_desc] = FBD.FBOVendor
		,[depfbo_phone] = FBD.PhoneNUM1
		,[arrfbo_desc] = FBA.FBOVendor 
		,[arrfbo_phone] = FBA.PhoneNUM1
		,[elp_time] = PL.ElapseTM 
		,[distance] = PL.Distance
		,[pax_total] = PL.PassengerTotal
		,[CrewList] = CR.Crewnames
		--,[Checkitem] = ''
		--,[completed] = ''
		--,[servicename] = ''
		 --(Crew Hotel)--
		,[Alerts] = ''
		,[Notes] = ''
		,[CrewNotes] = ''
		,[crewh_name] = CH.Name
		,[crewh_phone] = CH.PhoneNum
		,[crewh_fax] = CH.FaxNum
		,[crewh_rate] = CH.NegociatedRate
		,[crewh_addr1] = CH.Addr1
		,[crewh_addr2] = CH.Addr2
		,[crewh_remarks] = ''
		,[crewh_rooms] = CH.NoofRooms
		,[crewh_confirm] = ''
		,[crewh_comments] = ''
	
		 --(Arr Crew Trans)
		,[crewta_name] = TCA.TransportationVendor
		,[crewta_phone] = TCA.PhoneNum
		,[crewta_fax] = TCA.FaxNum
		,[crewta_rate] = TCA.NegotiatedRate
		,[crewta_remarks] = ''
		,[crewta_confirm] = ''
		,[crewta_comments] = ''
		
		----(Dep Crew Trans)
		,[crewtd_name] = TCD.TransportationVendor
		,[crewtd_phone] =  TCD.PhoneNum
		,[crewtd_fax] = TCD.FaxNum
		,[crewtd_rate] = TCD.NegotiatedRate
		,[crewtd_remarks] = ''
		,[crewtd_confirm] = ''
		,[crewtd_comments] = ''

		----(Pax Hotel)
		,[hotel_name] = PH.Name
		,[hotel_phone] = PH.PhoneNum
		,[hotel_fax] = PH.FaxNum
		,[hotel_rate] = PH.NegociatedRate
		,[hotel_rooms] = PH.NoofRooms
		,[hotel_confirm] = ''
		,[hotel_comments] = ''
		,[hotel_remarks] = ''

		----(Arr Pax Trans)
		,[paxta_name] = TPA.TransportationVendor
		,[paxta_phone] = TPA.PhoneNum
		,[paxta_fax] = TPA.FaxNum
		,[paxta_rate] = TPA.NegotiatedRate
		,[paxta_confirm] = ''
		,[paxta_comments] = ''
		,[paxta_remarks] = ''
		
		----(Dep Pax Trans)
		,[paxtd_name] = TPD.TransportationVendor
		,[paxtd_phone] = TPD.PhoneNum
		,[paxtd_fax] = TPD.FaxNum
		,[paxtd_rate] = TPD.NegotiatedRate
		,[paxtd_confirm] = ''
		,[paxtd_comments] = ''
		,[paxtd_remarks] = ''
	
		----(Arr Catering)
		,[catera_name] = CA.CateringVendor
		,[catera_phone] = CA.PhoneNum
		,[catera_fax] = CA.FaxNum
		,[catera_rate] = CA.NegotiatedRate
		,[catera_remarks] = ''
		,[catera_confirm] = ''
		,[catera_comments] = ''
		
		----(Dep Catering)
		,[caterd_name] = CD.CateringVendor
		,[caterd_phone] = CD.PhoneNum
		,[caterd_fax] = CD.FaxNum
		,[caterd_rate] = CD.NegotiatedRate
		,[caterd_remarks] = ''
		,[caterd_confirm] = ''
		,[caterd_comments] = ''
		
		----(Addl Crew Hotel)
		,[main_name] = ACH.Name
		,[main_phone] = ACH.PhoneNum
		,[main_fax] = ACH.FaxNum
		,[main_rate] = ACH.NegociatedRate
		,[main_addr1] = ACH.Addr1
		,[main_addr2] = ACH.Addr2
		,[main_remarks] = ''
		,[main_confirm] = ''
		,[main_comments] = ''
		,[main_rooms] = ACH.NoofRooms
		------------------------------------
		

		 FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
		 INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
		 INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
		 INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AD 
			ON AD.AirportID = PL.DepartICAOID
		 INNER JOIN (SELECT AirportID, AirportName, StateName, IcaoID, CityName, OffsetToGMT FROM Airport)AA 
			ON AA.AirportID = PL.ArriveICAOID
		 INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
		 
		 INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F 
			ON PM.FleetID = F.FleetID 
		 LEFT OUTER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
		 
		LEFT OUTER JOIN (
			SELECT PF.LegID, FA.FBOVendor, FA.PhoneNUM1
			FROM PreflightFBOList PF
			INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsArrivalFBO = 1
		) FBA ON PL.LegID = FBA.LegID
		
		LEFT OUTER JOIN (
			SELECT PF.LegID, FA.FBOVendor, FA.PhoneNUM1
			FROM PreflightFBOList PF
			INNER JOIN FBO FA ON PF.FBOID = FA.FBOID AND PF.IsDepartureFBO = 1
		) FBD ON PL.LegID = FBD.LegID
					
		 --LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID
		 --LEFT OUTER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID AND PFL.IsDepartureFBO = 1
		 --LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID AND PFL.IsArrivalFBO = 1

		 --LEFT OUTER JOIN PreflightTransportList PTL ON PL.LegID = PTL.LegID
		 --LEFT OUTER JOIN Transport TPD ON PTL.TransportID = TPD.TransportID AND PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
		 --LEFT OUTER JOIN Transport TPA ON PTL.TransportID = TPA.TransportID AND PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		 --LEFT OUTER JOIN Transport TCD ON PTL.TransportID = TCD.TransportID AND PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		 --LEFT OUTER JOIN Transport TCA ON PTL.TransportID = TCA.TransportID AND PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'

		 LEFT OUTER JOIN (
			SELECT PTL.LegID, TD.TransportationVendor, TD.PhoneNum, TD.FaxNum, TD.NegotiatedRate 
			FROM PreflightTransportList PTL
			INNER JOIN Transport TD ON PTL.TransportID = TD.TransportID 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
		 ) TPD  ON PL.LegID = TPD.LegID

		 LEFT OUTER JOIN (
			SELECT PTL.LegID, TD.TransportationVendor, TD.PhoneNum, TD.FaxNum, TD.NegotiatedRate 
			FROM PreflightTransportList PTL
			INNER JOIN Transport TD ON PTL.TransportID = TD.TransportID 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
		 ) TPA  ON PL.LegID = TPA.LegID

		 LEFT OUTER JOIN (
			SELECT PTL.LegID, TD.TransportationVendor, TD.PhoneNum, TD.FaxNum, TD.NegotiatedRate 
			FROM PreflightTransportList PTL
			INNER JOIN Transport TD ON PTL.TransportID = TD.TransportID 
			WHERE PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
		 ) TCD  ON PL.LegID = TCD.LegID

		 LEFT OUTER JOIN (
			SELECT PTL.LegID, TD.TransportationVendor, TD.PhoneNum, TD.FaxNum, TD.NegotiatedRate 
			FROM PreflightTransportList PTL
			INNER JOIN Transport TD ON PTL.TransportID = TD.TransportID 
			WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
		 ) TCA  ON PL.LegID = TCA.LegID
	 
		 --LEFT OUTER JOIN PreflightCateringDetail PCL ON PL.LegID = PCL.LegID
		 --LEFT OUTER JOIN Catering CD ON PCL.CateringID = CD.CateringID AND PCL.ArriveDepart = 'D'
		 --LEFT OUTER JOIN Catering CA ON PCL.CateringID = CA.CateringID AND PCL.ArriveDepart = 'A'	

		 LEFT OUTER JOIN (
			SELECT PCL.LegID, C.CateringVendor, C.PhoneNum, C.FaxNum, C.NegotiatedRate
			FROM PreflightCateringDetail PCL
			INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'D'		
		 ) CD ON PL.LegID = CD.LegID

		 LEFT OUTER JOIN (
			SELECT PCL.LegID, C.CateringVendor, C.PhoneNum, C.FaxNum, C.NegotiatedRate
			FROM PreflightCateringDetail PCL
			INNER JOIN Catering C ON PCL.CateringID = C.CateringID AND PCL.ArriveDepart = 'A'		
		 ) CA ON PL.LegID = CA.LegID
		 		
		--Pax Hotel
		LEFT OUTER JOIN ( SELECT 
			PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.Addr1,HP.Addr2,HP.CityName,
			HP.StateName,HP.PostalZipCD,HP.Remarks,HP.NegociatedRate,
			PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments 
			FROM PreflightPassengerList PPL 
			INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
		) PH ON PL.legID = PH.LegID

		--Crew Hotel
		
		LEFT OUTER JOIN (			
			SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
				 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
				 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments  
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
																AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			WHERE PCLL.DutyType IN ('P', 'S')
		) CH ON PL.legID = CH.LegID

		--Addnl Crew / Maint Crew - Hotel
		LEFT OUTER JOIN (			
			SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
					 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
					 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments   
			FROM PreflightCrewList PCLL 
			--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
			INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
																AND PCH.isArrivalHotel = 1
			INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
			INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			WHERE PCLL.DutyType NOT IN ('P', 'S')
		) ACH ON PL.legID = ACH.LegID

		LEFT OUTER JOIN (
			SELECT DISTINCT PC2.LegID, Crewnames = (
				SELECT PC1.CrewLastName + ',' 
				FROM (
					SELECT PC.LEGID, [CrewLastName] = C.LastName, PC.DutyTYPE, [RowID] = CASE PC.DutyTYPE 
						WHEN 'P' THEN 1 WHEN 'S' THEN 2 ELSE 3 END
					FROM PreflightCrewList PC
					INNER JOIN Crew C ON PC.CrewID = C.CrewID AND PC.CustomerID = C.CustomerID
				) PC1 WHERE PC1.LegID = PC2.LegID
				order by PC1.RowID, PC1.DutyTYPE
				FOR XML PATH('')
				)
			FROM PreflightCrewList PC2
		) CR ON PL.LegID = CR.LegID
			
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		--AND PM.TripID = @TripID	
	--ORDER BY  MAIN.LEGID, FLAG.RowID
) main

	--SELECT A.*, B.* FROM #tblFlag A OUTER APPLY #tblTripSheet1 B
	--SELECT B.* FROM #tblTripSheet B
	
--========= Checklist Table ===============
DECLARE @CLITEMS TABLE (
	RowID TINYINT,
	TripID BIGINT,
	LegID  BIGINT,
	CLITEM VARCHAR(100), 
	IsCompleted BIT,
	CLName VARCHAR(100)
)


	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 1, 'PAX HOTEL', ISNULL(PPH.IsCompleted, 0), HP.Name, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN  PreflightPassengerList PM ON M.legID = PM.LegID
	LEFT OUTER JOIN PreflightPassengerHotelList PPH ON PM.PreflightPassengerListID = PPH.PreflightPassengerListID
	LEFT OUTER JOIN Hotel HP ON PPH.HotelID = HP.HotelID

	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID)
	SELECT 2, 'PAX TRANSPORT', ISNULL(PTL.IsCompleted, 0), T.TransportationVendor,  M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN  PreflightTransportList PTL ON M.legID = PTL.LegID
	LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID
	WHERE  CrewPassengerType = 'P' AND IsArrivalTransport = 1
	---------------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID)
	SELECT 3, 'CREW HOTEL', ISNULL(PCH.IsCompleted, 0), HC.Name, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN PreflightCrewList PCLL ON M.legID = PCLL.LegID
	LEFT JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
														AND PCH.isArrivalHotel = 1
	LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
												AND PHCP.CrewID = PCLL.CrewID
	--LEFT OUTER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
	LEFT OUTER JOIN Hotel HC ON PCH.HotelID = HC.HotelID And PCLL.DutyType IN ('P', 'S')
	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 4, 'CREW TRANSPORT', ISNULL(PTL.IsCompleted, 0), T.TransportationVendor, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN  PreflightTransportList PTL ON M.legID = PTL.LegID
	INNER JOIN Transport T ON PTL.TransportID = T.TransportID
	WHERE  CrewPassengerType = 'C' AND IsArrivalTransport = 1
	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 5, 'ADDITIONAL CREW HOTEL', ISNULL(PCH.IsCompleted, 0), HC.Name, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN PreflightCrewList PCLL ON M.legID = PCLL.LegID
	--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
	INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.crewPassengerType = 'C'
														AND PCH.isArrivalHotel = 1
	INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
												AND PHCP.CrewID = PCLL.CrewID
	INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID And PCLL.DutyType NOT IN ('P', 'S')
	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 6, 'DEPARTURE CATERING', ISNULL(PCL.IsCompleted, 0), CD.CateringVendor, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN  PreflightCateringDetail PCL ON M.LegID = PCL.LegID
	LEFT OUTER JOIN Catering CD ON PCL.CateringID = CD.CateringID AND PCL.ArriveDepart = 'D' 	
	-----------------------------------------------------
	
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 7, 'FBO NOTIFICATION', ISNULL(PCHL.IsCompleted, 0), PCHL.ComponentDescription, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN PreflightCheckList PCHL ON M.LegID = PCHL.LegID
	LEFT OUTER JOIN TripManagerCheckList TCHL ON PCHL.CheckListID = TCHL.CheckListID AND CheckListCD = 'DFBO'
	-------------------------------------
	
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 8, 'NOTAM CHECK', ISNULL(PCHL.IsCompleted, 0), PCHL.ComponentDescription, M.TripID, M.LegID 
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN PreflightCheckList PCHL ON M.LegID = PCHL.LegID
	LEFT OUTER JOIN TripManagerCheckList TCHL ON PCHL.CheckListID = TCHL.CheckListID AND CheckListCD = 'DNOT'
	-------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, TripID, LegID) 
	SELECT 9, 'WEATHER CHECK', ISNULL(PCHL.IsCompleted, 0), PCHL.ComponentDescription, M.TripID, M.LegID
	FROM (SELECT DISTINCT TRIPID, LEGID FROM @tblTripInfo) M
	LEFT OUTER JOIN PreflightCheckList PCHL ON M.LegID = PCHL.LegID
	LEFT OUTER JOIN TripManagerCheckList TCHL ON PCHL.CheckListID = TCHL.CheckListID AND CheckListCD = 'DWX'
	---------------------------------------------

--=========================================


	--SELECT A.*, B.* FROM #tblFlag A OUTER APPLY #tblTripSheet1 B

	--SELECT ROWID, FLAG FROM #tblFlag
	--SELECT * FROM #tblTripSheet
	--SELECT C.* FROM @CLITEMS C
	--SELECT * FROM #tblTripSheet
	
	SELECT [RowID] = 0, [FLAG] = '', CLITEM, IsCompleted, CLName, TS.*
	FROM @CLITEMS CL
	INNER JOIN #tblTripSheet TS ON CL.TripID = TS.TRIPID AND CL.LegID = TS.LegID
	UNION ALL
	SELECT [RowID] = FL.ROWID,FL.FLAG, [CLITEM] = '', [IsCompleted] = '', [CLName] = '', TS.*
	FROM #tblFlag FL OUTER APPLY #tblTripSheet TS
	ORDER BY orig_nmbr, LEG_NUM, RowID

END

--EXEC spGetReportPRETSTripSheetChecklistExportInformation 'UC', '', '', '', '20120720', '20120725', 'ZZKWGS', '', '', '', '', '', ''
--EXEC spGetReportPRETSTripSheetChecklistExportInformation 'supervisor_99', '3895', '', '', '', '', '', '', '', '', '', '', '','',1,1,1,1,1,1	

--select TripNum from PreflightMain where TripID = 10001324252	


GO


