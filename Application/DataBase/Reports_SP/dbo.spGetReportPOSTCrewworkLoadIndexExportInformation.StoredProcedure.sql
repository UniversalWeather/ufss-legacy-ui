IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCrewworkLoadIndexExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCrewworkLoadIndexExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPOSTCrewworkLoadIndexExportInformation]    
(       
@Year NVARCHAR(4),
@UserCD NVARCHAR(20),
@CrewCD NVARCHAR(10)='',-- Optional
@CrewGroupCD NVARCHAR(10)='' -- Optional
)      
-- ===============================================================================      
-- SPC Name: spGetReportPOSTCrewworkLoadIndex      
-- Author:  PREMPRAKASH.S      
-- Create date: 01 Aug 2012    
-- Description: Get Crew workLoad  information for REPORTS      
-- Revision History      
-- Date                 Name        Ver         Change      
--   exec spGetReportPOSTCrewworkLoadIndexExportInformation 2012,'supervisor_99'       
--      
      
-- ================================================================================      
       
 AS      
 
BEGIN

/*DECLARE @Year NVARCHAR(4) = '2010'
DECLARE @UserCD NVARCHAR(20)='JWILLIAMS_13'
DECLARE @CrewCD NVARCHAR(10)=''--1gv,3cm'--'1GV';
DECLARE @CrewGroupCD NVARCHAR(10)=''--'1605';
DECLARE @FromDate DATE =''-- '2009-01-01';
DECLARE @ToDate DATE	 = ''--'2009-12-31';) AS*/

--SELECT DISTINCT * FROM Crew WHERE CustomerID =10013

DECLARE @CustomerID INT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @FromDate DATE =''-- '2009-01-01';
DECLARE @ToDate DATE = '' --'2009-12-31;

SET @FromDate = @Year + '-01-01'
SET @ToDate = @Year + '-12-31'
SET @FromDate = CONVERT (DATE, @FromDate)
SET @ToDate = CONVERT (DATE,@ToDate)

DECLARE @DateTable TABLE (SeqID INT IDENTITY, WorkDate DATE);
	
DECLARE @CrewDate TABLE
		(CrewID BIGINT,
		 CrewCD NVARCHAR(10),
 		 FirstName NVARCHAR(50),
		 LastName NVARCHAR(50),
		 MiddleInitial NVARCHAR(50),
		 CrewGroupID BIGINT,
		 CrewGroupCD NVARCHAR(10),
		 CrewTypeCD NVARCHAR(10),
		 WorkDate DATE,
		 ScheduledMonth SMALLINT);
	 
--DECLARE @CrewList TABLE 
--		(CrewID BIGINT,
--		 CrewCD nvarchar(10),
--		 FirstName NVARCHAR(50),
--		 LastName NVARCHAR(50),
--		 MiddleInitial NVARCHAR(50),
--		 CrewGroupID BIGINT,
--		 CrewGroupCD NVARCHAR(10),
--		 ScheduledMonth SMALLINT);		 
		 
--DECLARE @DutyType TABLE 
--		(DutyType NVARCHAR(10),
--		ValuePTS SMALLINT,
--		WeekendPTS SMALLINT)

--DECLARE @CrewWorkLoad TABLE
--		(CrewID BIGINT,
--		CrewCD NVARCHAR(10),
--		FirstName NVARCHAR(50),
--		LastName NVARCHAR(50),
--		MiddleInitial NVARCHAR(50),
--		CrewGroupCD NVARCHAR(10),
--		WorkDate DATE,
--		ActualDutyStartDTTM DATETIME,
--		ActualDutyEndDTTM DATETIME,
--		DutyType NVARCHAR(2),
--		DutyDay SMALLINT,
--		WeekEnd NVARCHAR(1),
--		ScheduledMonth SMALLINT)

--End Table Declaration
--select distinct IsWorkLoadIndex,IsOfficeDuty from CrewDutyType where customerID=10013

INSERT INTO @DateTable select * from  [dbo].[fnDateTable](@FromDate,@ToDate)

IF @CrewCD <>'' 
	BEGIN
		INSERT INTO @CrewDate  
		(
		CrewID, 
		 CrewCD ,
		 FirstName,
		 LastName,
		 MiddleInitial ,
		 CrewGroupID,
		 CrewGroupCD,
		 CrewTypeCD,
		 WorkDate,
		 ScheduledMonth)
		SELECT DISTINCT
				VG.Crewid
			   ,VG.CrewCD
			   ,VG.FirstName
			   ,VG.LastName
			   ,VG.MiddleInitial
			   ,VG.CrewTypeCD
			   ,''--VG.CrewGroupID
			   ,''--VG.CrewGroupCD
			   ,DT.WorkDate
			   ,MONTH(DT.WorkDate) 
		FROM vCrewGroup VG 
		CROSS JOIN @DateTable DT			   
		WHERE	VG.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
				AND VG.CustomerID=@CustomerID
	END	



IF @CrewGroupCD <> ''
	BEGIN
		INSERT INTO @CrewDate 
			(
			CrewID, 
			 CrewCD ,
			 FirstName,
			 LastName,
			 MiddleInitial ,
			 CrewGroupID,
			 CrewGroupCD,
			 CrewTypeCD,
			 WorkDate,
			 ScheduledMonth)
		SELECT VG.Crewid
			   ,VG.CrewCD
			   ,VG.FirstName
			   ,VG.LastName
			   ,VG.MiddleInitial
			   ,VG.CrewGroupID
			   ,VG.CrewGroupCD
			   ,VG.CrewTypeCD
			   ,DT.WorkDate
			   ,MONTH(DT.WorkDate)
		FROM vCrewGroup VG 
		CROSS JOIN @DateTable DT			   
		WHERE VG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
			AND VG.CustomerID=@CustomerID 
			AND VG.CrewCD NOT IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
--			AND VG.IsStatus=0 
--SELECT * FROM vCrewGroup		
		
	END	
IF 	@CrewCD ='' AND @CrewGroupCD = ''
BEGIN 

		INSERT INTO @CrewDate  
		(
		CrewID, 
		 CrewCD ,
		 FirstName,
		 LastName,
		 MiddleInitial ,
		 CrewGroupID,
		 CrewGroupCD,
		 CrewTypeCD,
		 WorkDate,
		 ScheduledMonth)
		SELECT DISTINCT
				VG.Crewid
			   ,VG.CrewCD
			   ,VG.FirstName
			   ,VG.LastName
			   ,VG.MiddleInitial
			   ,''--VG.CrewGroupID
			   ,''--VG.CrewGroupCD
			   ,CrewTypeCD
			   ,DT.WorkDate
			   ,MONTH(DT.WorkDate) 
		FROM vCrewGroup VG
		CROSS JOIN @DateTable DT
		WHERE VG.CustomerID=@CustomerID AND VG.IsDeleted =0 AND VG.IsStatus=1
END

--SELECT DISTINCT CrewID,FirstName,LastName,MiddleInitial,ScheduledMonth FROM  @CrewDate
-- Start Crew Calendar Calculation
DECLARE @RonValuePoints NUMERIC(5,2)=0.0;

SELECT @RonValuePoints = ValuePTS FROM CrewDutyType WHERE CustomerID=@CustomerID and DutyTypeCD='R'

SELECT CC.* INTO #CrewCalendar FROM
(SELECT 	CD.CrewID
			,CD.CrewCD
			,ISNULL(CD.FirstName,'') FirstName
			,ISNULL(CD.LastName,'') LastName
			,ISNULL(CD.MiddleInitial,'') MiddleInitial
			,CD.WorkDate
			,CT.isWorkLoadIndex
			,CT.isOfficeDuty
			,CASE WHEN CT.IsOfficeDuty = 1 THEN
					CASE WHEN	DATEDIFF(DD,CD.WorkDate,CT.ActualDutyStartDT)=0 THEN DATEDIFF(HH,CD.WorkDate,CT.ActualDutyStartDTTM)
						WHEN	CT.ActualDutyEndDT > CD.WorkDate THEN DATEDIFF(HH,CD.WorkDate,DATEADD(DD,1,CD.WorkDate))
						WHEN	DATEDIFF(DD,CT.ActualDutyENDDT,CD.WorkDate)=0 THEN DATEDIFF(HH,CD.WorkDate,CT.ActualDutyEndDTTM)
						ELSE 0
					END 
				ELSE 0 
			END OfficeDuty
			,CT.ActualDutyStartDTTM
			,CT.ActualDutyEndDTTM
			,CT.DutyType
			,DATEPART(DW,CD.WorkDate) DutyDay
			,CASE	WHEN CT.isWorkLoadIndex = 1 AND CT.isOfficeDuty=0 THEN
						CASE	WHEN DATEPART(dw,CD.WorkDate) IN(7,1) THEN 'Y'
								ELSE 'N' 
						END 
					ELSE 'NA'
			END WeekEnd
			,CT.WeekEndPTS
			,CT.ValuePTS
			,CASE WHEN CT.isWorkLoadIndex = 1 AND CT.isOfficeDuty=0 THEN
					CASE WHEN DATEPART(dw,CD.WorkDate) IN(7,1) THEN 1*CT.WeekEndPTS
						ELSE 1*CT.ValuePTS
					END 
				ELSE 0
			END WorkLoadPTS			
			,DATEPART(MM,CD.WorkDate) ScheduledMonth	
			FROM @CrewDate CD
			JOIN
			( SELECT 
					PCL.TripID
					,PCL.TripNUM
					,PCL.LegID
					,PCL.PCLCrewID CrewID
					,CONVERT(DATE,PCL.DepartureDTTMLocal) ActualDutyStartDT
					,CONVERT(DATE,PCL.ArrivalDTTMLocal) ActualDutyEndDT
					,PCL.DepartureDTTMLocal ActualDutyStartDTTM
					,PCL.ArrivalDTTMLocal  ActualDutyEndDTTM
					,PCL. DutyTYPE
					,CDT.ValuePTS
					,CDT.WeekEndPTS
					,CDT.isWorkLoadIndex
					,CDT.isOfficeDuty
				FROM vPreFlightCrewList PCL
				LEFT JOIN (SELECT DutyTypeCD,ValuePTS,WeekendPTS,isWorkLoadIndex,isOfficeDuty,CustomerID FROM CrewDutyType CDT 
							WHERE	CDT.CustomerID=@CustomerID 
							--AND CDT.IsWorkLoadIndex=1
							AND CDT.DutyTypeCD NOT IN ('G','F','R')) CDT 
					 ON PCL.DutyTYPE=CDT.DutyTypeCD AND PCL.CustomerID=CDT.CustomerID
				WHERE PCL.CustomerID =@CustomerID AND PCL.ArrivalYear=@Year --AND ArrivalMonth =1
					)CT --(CrewTrans)
		ON CD.CrewID=CT.CrewID 
		WHERE	CD.WorkDate >= CONVERT(DATE,CT.ActualDutyStartDTTM)
			AND	 CD.WorkDate <= CONVERT(DATE,CT.ActualDutyEndDTTM)) CC



--SELECT DISTINCT CC.CrewID,CC.CrewCD,CC.FirstName,CC.LastName,CC.MiddleInitial
--	,CC.ScheduledMonth ,sum(CC.OfficeDuty) over (PARTITION BY CC.CrewID,CC.ScheduledMonth)  +
--	SUM(CC.WorkLoadPTS) OVER (PARTITION BY CC.CrewID,CC.ScheduledMonth) WorkLoadIndex FROM #CrewCalendar	CC


--DROP TABLE #CrewCalendar
--End Crew Calendar Calculation

-- Start Calculation for trip records in PostFlight


SELECT CW.* INTO #CrewPostFlight FROM
(
SELECT	DISTINCT CWI.CrewID
		,CWI.CrewCD
		,CWI.FirstName
		,CWI.LastName
		,CWI.MiddleInitial
		,CWI.Ron
		,CWI.PreRon
		,CWI.ScheduledMonth
		,CWI.POLegID 
		,CWI.LegNum
		,CWI.POlogID
		,CWI.LogNum
		,COUNT(CWI.POLegID) OVER (PARTITION By CWI.CrewId,CWI.ScheduledMonth) + 
		 SUM(CWI.DutyHours)  OVER (PARTITION By CWI.CrewId,CWI.ScheduledMonth) +
		 SUM(CWI.BlockHours)  OVER (PARTITION By CWI.CrewId,CWI.ScheduledMonth) +
		 SUM(CASE WHEN CWI.Ron > 0 THEN CWI.Ron 
				ELSE CWI.PreRon 
			 END ) OVER (PARTITION By CWI.CrewId,CWI.ScheduledMonth)*@RonValuePoints
	  WorkLoadIndex,
	  CWI.CREWTYPECD
		--,SUM (
		--CASE WHEN LegRon > 0 AND 
		 FROM
(SELECT DISTINCT 
		PCL.CrewID
		,PCL.CrewCD
		,PCL.FirstName 
		,PCL.LastName 
		,PCL.MiddleInitial 
		,PCL.POLegID
		,CASE WHEN PCL.LegIsDutyEnd = 1 THEN PCL.PCrewDutyHours 
			ELSE 0
		END DutyHours
		,PCrewBlockHours BlockHours
		,PCL.MonthlyRon Ron
		,PCL.POLogID
		,PCL.LegNum
		,PCL.LogNum
	--,PCL.DutyType
	--,PCL.crewDutyType
	--,PCL.RemainOverNight RON
	--,PCL.OutboundDTTM
	--,PCL.InboundDTTM
	,CASE WHEN PCL.PCrewRon>0 AND PCL.LegScheduledMonth < PCL.legNextMonth THEN
			ISNULL((SELECT PCrewRon - MonthlyRon from [vPostFlightCrewList] 
			WHERE  LegNum = PCL.LegNum -1 and POLogID=PCL.POLogID AND CrewID=Pcl.CrewID),0) 
			ELSE 0
	END PreRon
	,PCL.legScheduledMonth ScheduledMonth
	,PCL.CREWTYPECD 
	FROM vPostFlightCrewList PCL
WHERE PCL.CustomerID=@CustomerID 
	AND PCL.CrewID IN (SELECT DISTINCT CrewID from @CrewDate)
AND PCL.LegScheduledYear =@year --AND PCL.LegScheduledMonth = 1
) CWI)CW

 --select * from #CrewCalendar
-- select * from #CrewPostFlight		  
--DROP TABLE #CrewCalendar
--DROP TABLE #CrewPostFlight		  

SELECT DISTINCT CL.CrewID,CL.CrewCD,CL.FirstName,CL.LastName,CL.MiddleInitial
,CL.ScheduledMonth,ISNULL(CWI.WorkLoadIndex,0) WorkLoadIndex,ISNULL(CREWTYPECD,'') CREWTYPECD,
'JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC' months
--,ISNULL(AVG(WorkLoadIndex) OVER (PARTITION BY CWI.ScheduledMonth),0)  MonthlyAvg 
FROM
 (SELECT DISTINCT CrewID,CrewCD, FirstName,LastName,MiddleInitial,ScheduledMonth FROM  @CrewDate) CL
LEFT JOIN 
	(
	SELECT  DISTINCT CW.CrewID,CW.CrewCD,CW.FirstName,CW.LastName,CW.MiddleInitial, ScheduledMonth
	,SUM(CW.WorkLoadIndex) OVER (PARTITION BY CW.CrewID,CW.ScheduledMonth) WorkLoadIndex,CREWTYPECD 
	FROM
		(SELECT DISTINCT '' CREWTYPECD,CC.CrewID,CC.CrewCD,CC.FirstName,CC.LastName,CC.MiddleInitial
		,CC.ScheduledMonth ,SUM(CC.OfficeDuty+CC.WorkLoadPTS) OVER (PARTITION BY CC.ScheduledMonth, CC.CrewID) WorkLoadIndex FROM #CrewCalendar	CC
		UNION ALL 
		SELECT DISTINCT CREWTYPECD,CPF.CrewID,CPF.CrewCD,CPF.FirstName,CPF.LastName,CPF.MiddleInitial,CPF.ScheduledMonth,CPF.WorkLoadIndex
		 FROM #CrewPostFlight CPF) CW 
	) CWI
	ON CL.CrewID = CWI.CrewID AND CL.ScheduledMonth = CWI.ScheduledMonth
	ORDER BY --CW.CrewCD,
		CL.ScheduledMonth

 --select * from #CrewCalendar
 --select * from #CrewPostFlight	
 	  
 IF OBJECT_ID('tempdb..#CrewCalendar') IS NOT NULL
DROP TABLE #CrewCalendar


IF OBJECT_ID('tempdb..#CrewPostFlight') IS NOT NULL
DROP TABLE #CrewPostFlight  
END



GO


