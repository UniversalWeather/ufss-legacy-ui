IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSChecklistInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSChecklistInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[spGetReportPRETSChecklistInformation]
   
        @UserCD AS VARCHAR(30), --Mandatory
		@TripID AS VARCHAR(50),
		@LegNum AS VARCHAR(30) = ''
		,@ReportHeaderID VARCHAR(30) -- TemplateID from TripSheetReportHeader table
		,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'	 
		AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPRETSChecklistInformation
-- Author: SINDHUJA.K
-- Create date: 24 July 2012
-- Description: Get Preflight Tripsheet Report Writer-Checklist information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 5 May 2014 Aishwarya.M       Changed crew Hotels join
-- =============================================
			


		DECLARE @CLITEMS TABLE (
	RowID TINYINT,
	CLITEM VARCHAR(100), 
	IsCompleted BIT,
	CLName VARCHAR(100),
	CrewPaxCD VARCHAR(200)
	)
	
	
	IF EXISTS(	SELECT 1, 'PAX HOTEL', PHL.IsCompleted, PHL.PreflightHotelName,P.PassengerRequestorCD 	
	FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.legID = PPL.LegID
			INNER JOIN PreflightHotelList PHL ON PHL.LegID = PPL.LegID AND isArrivalHotel = 1 AND PHL.crewPassengerType = 'P'
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PHL.PreflightHotelListID
																AND PHCP.PassengerRequestorID = PPL.PassengerID
			INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID
			WHERE PM.TripID = CONVERT(BIGINT, @TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''))
	BEGIN
	;With PaxCode AS (
			SELECT PM.TripID
			,PL.LegNUM
			,IsCompleted=(CASE WHEN PHL.Status='Completed' THEN 1 ELSE 0 END)
			,PHL.PreflightHotelName
			,CASE WHEN PHCP.PreflightHotelListID IS NOT NULL AND PHCP.PassengerRequestorID IS NOT NULL THEN 
			 P.PassengerRequestorCD ELSE ''  END Pax   
			,PHL.HotelID
	  FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
			INNER JOIN PreflightPassengerList PPL ON PL.legID = PPL.LegID
			INNER JOIN PreflightHotelList PHL ON PHL.LegID = PPL.LegID AND isArrivalHotel = 1 AND PHL.crewPassengerType = 'P'
			LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PHL.PreflightHotelListID
													AND PHCP.PassengerRequestorID = PPL.PassengerID
			INNER JOIN Passenger P ON PPL.PassengerID = P.PassengerRequestorID														
			INNER JOIN Hotel HC ON PHL.HotelID = HC.HotelID
			WHERE PM.TripID = CONVERT(BIGINT, @TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
)	


INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, CrewPaxCD)
		SELECT RowID,Label,IsCompleted,PreflightHotelName,Crew=LEFT(Crew,LEN(Crew)-1) FROM (
				SELECT 1 RowID,'PAX HOTEL' Label
					,IsCompleted
					,PreflightHotelName
					,Crew = (SELECT Pax +  ', ' FROM PaxCode CC 
						WHERE CC.HotelID = C.HotelID 
						AND CC.TripID = C.TripID 
						AND CC.LegNUM = C.LegNUM
						AND ISNULL(Pax,'') <> ''
						FOR XML PATH('')
						)FROM (SELECT DISTINCT HotelID,TripID,IsCompleted,LegNUM,PreflightHotelName FROM PaxCode) C 

				)TT
		END ELSE
	BEGIN
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (1, 'PAX HOTEL')
	END
	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (2, 'PAX TRANSPORT')
	UPDATE @CLITEMS SET IsCompleted=(CASE WHEN PTL.Status='Completed' THEN 1 ELSE 0 END), CLName = PTL.PreflightTransportName --T.TransportationVendor	
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN  PreflightTransportList PTL ON PL.legID = PTL.LegID AND CrewPassengerType = 'P'
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND RowID = 2
	---------------------------------------------------------
	IF EXISTS(SELECT 3, 'CREW HOTEL', PCH.IsCompleted, PCH.PreflightHotelName, C.CrewCD	
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN PreflightCrewList PCLL ON PL.legID = PCLL.LegID
	INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND isArrivalHotel = 1 AND PCH.crewPassengerType = 'C'
	LEFT JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
														AND PHCP.CrewID = PCLL.CrewID
	INNER JOIN Crew C ON PCLL.CrewID = C.CrewID
	WHERE PM.TripID = CONVERT(BIGINT, @TripID) And PCLL.DutyType IN ('P', 'S')
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''))	
	BEGIN
	
	;With CrewCode AS (
	SELECT PM.TripID
		  ,PL.LegNUM
		  ,IsCompleted=(CASE WHEN PHL.Status='Completed' THEN 1 ELSE 0 END)
		  ,PHL.PreflightHotelName
		  ,	CASE WHEN PCH.PreflightHotelListID IS NOT NULL AND PCH.CREWID IS NOT NULL THEN 
			 C.CREWCD ELSE ''  END Crew 
		  ,PHL.HotelID
		  FROM PreflightMain PM
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN PreflightCrewList PCLL ON PL.LegID = PCLL.LegID
				INNER JOIN Crew C ON PCLL.CrewID = C.CrewID
				INNER JOIN PreflightHotelList PHL ON PHL.LegID = PL.LegID AND PHL.crewPassengerType = 'C'
				LEFT JOIN PreflightHotelCrewPassengerList PCH ON PHL.PreflightHotelListID = PCH.PreflightHotelListID
															   AND PCH.CrewID = PCLL.CREWID															
				INNER JOIN Hotel HC ON PHL.HotelID = HC.HotelID
		  WHERE PM.TripID = CONVERT(BIGINT, @TripID)
			AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
					)
		  
INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName, CrewPaxCD)
		SELECT RowID,Label,IsCompleted,PreflightHotelName,Crew=LEFT(Crew,LEN(Crew)-1) FROM (
			SELECT 3 RowID,'CREW HOTEL' Label
					,IsCompleted
					,PreflightHotelName
					,Crew = (SELECT Crew +  ', ' FROM CrewCode CC 
						WHERE CC.HotelID = C.HotelID 
						AND CC.TripID = C.TripID 
						AND CC.LegNUM = C.LegNUM
						AND ISNULL(CREW,'') <> ''
						FOR XML PATH('')
)FROM (SELECT DISTINCT HotelID,TripID,IsCompleted,LegNUM,PreflightHotelName FROM CrewCode) C 

)TT

	END ELSE
	BEGIN
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (3, 'CREW HOTEL')
	END
	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (4, 'CREW TRANSPORT')
	UPDATE @CLITEMS SET IsCompleted=(CASE WHEN PTL.Status='Completed' THEN 1 ELSE 0 END), CLName = PTL.PreflightTransportName --T.TransportationVendor	
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN  PreflightTransportList PTL ON PL.legID = PTL.LegID AND CrewPassengerType = 'C'
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND RowID = 4

	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (5, 'ARRIVAL CATERING')
	UPDATE @CLITEMS SET IsCompleted =(CASE WHEN PCL.Status='Completed' THEN 1 ELSE 0 END), CLName = PCL.ContactName --CD.CateringVendor	
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN  PreflightCateringDetail PCL ON PL.LegID = PCL.LegID
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND RowID = 5 and PCL.ArriveDepart = 'A'

	-----------------------------------------------------
	INSERT INTO @CLITEMS (RowID, CLITEM) VALUES (6, 'DEPARTURE CATERING')
	UPDATE @CLITEMS SET IsCompleted =(CASE WHEN PCL.Status='Completed' THEN 1 ELSE 0 END), CLName = PCL.ContactName --CD.CateringVendor	
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN  PreflightCateringDetail PCL ON PL.LegID = PCL.LegID
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	AND RowID = 6 and PCL.ArriveDepart = 'D'

	
	-----------------------------------------------------
	IF EXISTS (
	SELECT  TCHL.CheckListDescription, PCHL.IsCompleted, PCHL.ComponentDescription 
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN PreflightCheckList PCHL ON PL.LegID = PCHL.LegID
	INNER JOIN TripManagerCheckList TCHL ON PCHL.CheckGroupID = TCHL.CheckGroupID 
		AND PCHL.ComponentCD = TCHL.CheckListCD AND PCHL.IsDeleted = 0 AND TCHL.isInactive=0
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = ''))

	BEGIN
	INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName)
	SELECT distinct 6 + ROW_NUMBER() OVER ( ORDER BY CheckListCD ), TCHL.CheckListDescription, 
	    PCHL.IsCompleted, 
		PCHL.ComponentDescription 
	FROM PreflightMain PM
	INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	INNER JOIN PreflightCheckList PCHL ON PL.LegID = PCHL.LegID
	INNER JOIN TripManagerCheckList TCHL ON PCHL.CheckGroupID = TCHL.CheckGroupID 
		AND PCHL.ComponentCD = TCHL.CheckListCD AND PCHL.IsDeleted = 0 AND TCHL.isInactive=0 
	WHERE PM.TripID = CONVERT(BIGINT, @TripID)
	AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
	END
		ELSE 
	BEGIN
		INSERT INTO @CLITEMS (RowID, CLITEM, IsCompleted, CLName)
		SELECT 6 + ROW_NUMBER() OVER ( ORDER BY CheckListCD ),CheckListDescription,0 IsCompleted, 
		'' ComponentDescription FROM(
								SELECT DISTINCT CheckListCD, TC.CheckListDescription, 
								0 IsCompleted, 
								'' ComponentDescription --PCHL.ComponentDescription 
							FROM (SELECT * FROM USERMASTER WHERE USERNAME = RTRIM(LTRIM(@UserCD))) UM
									INNER JOIN COMPANY C ON C.CUSTOMERID = UM.CUSTOMERID AND C.HOMEBASEID = C.HOMEBASEID
									INNER JOIN TripManagerCheckListGroup TCG ON TCG.CheckGroupID = C.DefaultCheckListGroupID
									INNER JOIN TripManagerCheckList TC ON TC.CheckGroupID = TCG.CheckGroupID AND TC.isInactive=0 
							)TT
	
	END
		
		
DECLARE @FormatSettings TABLE (
	ALERTS	BIT
	,NOTES	SMALLINT
	,LEG	SMALLINT
	,DISPATCHNO	BIT
	,PAXHOTEL	BIT
	,HOTELPHONE	BIT
	,HOTELFAX	BIT
	,HOTELREM	BIT
	,PTANAME	BIT
	,PTAPHONE	BIT
	,PTAFAX	BIT
	,PTAREMARKS	BIT
	,CANAME	BIT
	,CAPHONE	BIT
	,CAFAX	BIT
	,CAREMARKS	BIT
	,CREWHOTEL	BIT
	,CREWHPHONE	BIT
	,CREWHFAX	BIT
	,CREWHNRATE	BIT
	,CREWHADDR	BIT
	,CREWHREM	BIT
	,CTANAME	BIT
	,CTAPHONE	BIT
	,CTAFAX	BIT
	,CTARATE	BIT
	,CTAREMARKS	BIT
	,CREWHROOMS	BIT
	,CREWHCOMM	SMALLINT
	,CREWHCONF	BIT
	,HOTELROOMS	BIT
	,HOTELCONF	BIT
	,HOTELCOMM	SMALLINT
	,PTACONFIRM	BIT
	,PTACOMMENT	SMALLINT
	,PTDNAME	BIT
	,PTDPHONE	BIT
	,PTDFAX	BIT
	,PTDREMARKS	BIT
	,PTDCONFIRM	BIT
	,PTDCOMMENT	SMALLINT
	,CTACONFIRM	BIT
	,CTACOMMENT	SMALLINT
	,CTDNAME	BIT
	,CTDPHONE	BIT
	,CTDFAX	BIT
	,CTDRATE	BIT
	,CTDREMARKS	BIT
	,CTDCOMMENT	SMALLINT
	,CDNAME	BIT
	,CDPHONE	BIT
	,CDFAX	BIT
	,CDREMARKS	BIT
	,CDCONFIRM	BIT
	,CDCOMMENT	SMALLINT
	,CACONFIRM	BIT
	,CACOMMENT	SMALLINT
	,HOTELRATE	BIT
	,PTARATE	BIT
	,PTDRATE	BIT
	,CARATE	BIT
	,CDRATE	BIT
	,CTDCONFIRM	BIT
	,ONELEG	BIT
	--,MAINTHOTEL	BIT
	--,MAINTPHONE	BIT
	--,MAINTFAX	BIT
	--,MAINTNRATE	BIT
	--,MAINTADDR	BIT
	--,MAINTCOMM	SMALLINT
	--,MAINTREM	BIT
	--,MAINTCONF	BIT
	--,MAINROOMS	BIT
	,NOTESF     SMALLINT
	,DISPATCHIN SMALLINT
	,DSPTEMAIL BIT
	,DISPATCHER BIT
	,DSPTPHONE BIT
	,DCREWHTL BIT
	,DCREWHADDR BIT
	,DCREWHCOMM SMALLINT
	,DCREWHCONF BIT
	,DCREWHFAX BIT
	,DCREWHNRTE BIT
	,DCREWHROOM BIT
	,DCREWHPHON BIT
	,DCREWHREM BIT
	--,DMAINHOTEL BIT
	--,DMAINHADD BIT
	--,DMAINHCOMM SMALLINT
	--,DMAINHCON BIT
	--,DMAINHFAX BIT
	--,DMAINHRATE BIT
	--,DMAINHROOM BIT
	--,DMAINHPHON BIT
	--,DMAINHREM BIT
	,DPAXHOTEL BIT
	,DPAXHPHONE BIT
	,DPAXHFAX BIT
	,DPAXHREM BIT
	,DPAXHROOMS BIT
	,DPAXHCONF BIT
	,DPAXHCOMM SMALLINT
	,DPAXHRATE BIT
)
INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings @ReportHeaderID, 12, @TempSettings
--INSERT INTO @FormatSettings EXEC spGetReportPRETSFormatSettings 10013161677, 12, 'MAINTHOTEL::FALSE' 

DECLARE	@PAXHOTEL BIT,@MAINTHOTEL BIT,@CDNAME BIT,@CREWHOTEL BIT,@CTANAME BIT,@PTANAME BIT,@ARRIVALCATERING BIT
SELECT --@MAINTHOTEL=MAINTHOTEL, 
@ARRIVALCATERING=CANAME, 
@CDNAME=CDNAME, @CREWHOTEL=CREWHOTEL 
,@CTANAME=CTANAME,@PTANAME=PTANAME, @PAXHOTEL = PAXHOTEL FROM @FormatSettings

DECLARE @TypeList VARCHAR(15) = '';
IF @PAXHOTEL= '1' BEGIN SET @TypeList = @TypeList + '1,' END
--IF @MAINTHOTEL = '1' BEGIN SET @TypeList = @TypeList + '5,' END
IF @ARRIVALCATERING = '1' BEGIN SET @TypeList = @TypeList + '5,' END
IF @CDNAME = '1' BEGIN SET @TypeList = @TypeList + '6,' END
IF @CREWHOTEL = '1' BEGIN SET @TypeList = @TypeList + '3,' END
IF @CTANAME = '1' BEGIN SET @TypeList = @TypeList + '4,' END
IF @PTANAME = '1' BEGIN SET @TypeList = @TypeList + '2' END

SELECT * FROM @CLITEMS 
WHERE RowID NOT IN (SELECT CONVERT(INT,S) FROM DBO.SPLITSTRING(@TypeList,','))
			
End

--EXEC spGetReportPRETSChecklistInformation 'ERICK_1', 1000158811, '', 10001281316, 'MAINTHOTEL::FALSE||CDNAME::FALSE'


GO


