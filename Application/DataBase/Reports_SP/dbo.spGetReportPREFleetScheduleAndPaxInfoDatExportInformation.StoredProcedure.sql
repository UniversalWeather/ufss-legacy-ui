IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFleetScheduleAndPaxInfoDatExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFleetScheduleAndPaxInfoDatExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPREFleetScheduleAndPaxInfoDatExportInformation]

		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TailNum AS NVARCHAR(500) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(500) = '' -- [Optional], Comma delimited string with mutiple values
	
AS
BEGIN
-- =============================================
-- SPC Name: spGetReportPREFleetScheduleAndPaxInfoDatExportInformation
-- Author: SINDHUJA.K
-- Create date: 18 July 2012
-- Description: Get Preflight FleetScheduleAndPaxInfoDatExport information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
 
 DECLARE @TenToMin INT
      DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

      
      SET @TenToMin= (SELECT COM.TimeDisplayTenMin
                      FROM   Company COM
                             JOIN UserMaster UM
                               ON COM.CustomerID = UM.CustomerID
                                  AND COM.HomebaseID = UM.HomebaseID
                      WHERE  UM.UserName = @UserCD)

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNum <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	---LEFT OUTER JOIN FleetGroupOrder FGO
	--ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	--LEFT OUTER JOIN FleetGroup FG 
	--ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE --(FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
	---AND 
	F.CustomerID = @CUSTOMERID 
	AND F.IsDeleted=0 
END
-----------------------------TailNum and Fleet Group Filteration----------------------

      --Modified By Bala Ends
      DECLARE @BeginDate DATETIME
      DECLARE @EndDate DATETIME
      DECLARE @FromDate DATETIME
      DECLARE @ToDate DATETIME
      DECLARE @Count INT
      DECLARE @index INT
      DECLARE @NextString NVARCHAR(40)
      DECLARE @Pos INT
      DECLARE @NextPos INT
      DECLARE @String NVARCHAR(40)
      DECLARE @Delimiter NVARCHAR(40)
      DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
      DECLARE @ParameterDefinition AS NVARCHAR(100)

     
 

  
        BEGIN
         SET @FromDate = @DATEFROM
            SET @ToDate = @DATETO
            SET @BeginDate=@FromDate
            SET @EndDate=@ToDate
        END

      -- [Start] Storing given Tail Numbers [User Ipnut] in to Temp tables --------------
      CREATE TABLE #TempTailNum
        (
            ID                INT NOT NULL IDENTITY (1, 1),
            activedate        DATE,
            tail_nmbr         CHAR(6),
            FleetID           BIGINT,
            Depart DATETIME,
			Arrive DATETIME,
			D_icao_id      CHAR(4),
			A_icao_id      VARCHAR(60),
			dep_city         VARCHAR(25),
			arr_city         VARCHAR(25),
			dep_airport      VARCHAR(25),
			arr_airport      VARCHAR(25),
			dep_state        VARCHAR(25),
			arr_state        VARCHAR(25),
			dep_country      VARCHAR(60),
			arr_country      VARCHAR(60),
            elp_time          VARCHAR(40),
            [desc]            VARCHAR(40),
            pax_total         INT,
            orig_nmbr         BIGINT,
            legid             BIGINT,
            paxno             INT,
            paxname           VARCHAR(60),
            HomeBased         NVARCHAR(25),
            Type_Code         CHAR(4),
            HomebaseAirportID BIGINT,
            AirportID         BIGINT,
            SortDepart DATETIME,
            LegNum     BIGINT
        )

  
      DECLARE @TblDates TABLE (AllDates DATE);

      WITH dates(DATEPARAM)
           AS (SELECT @FromDate AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @ToDate)
      INSERT INTO @TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
      
     


      -- This will never return records  	
      --DELETE FROM #TempTailNum
      --WHERE  FleetID NOT IN((SELECT DISTINCT PM.FleetID
      --                       FROM   PreflightMain PM
      --                              INNER JOIN #TempTailNum T
      --                                      ON PM.FleetID = T.FleetID
      --                       WHERE  CONVERT(DATE, T.activedate) BETWEEN
      --                              CONVERT(DATE, @DateFrom) AND
      --                                       CONVERT(DATE, @DateTo)
      --                              AND PM.CustomerID =dbo.Getcustomeridbyusercd(Ltrim(@UserCD))))
                                    


	CREATE TABLE #TempTable 
	(
	ID           INT NOT NULL IDENTITY (1, 1),
	activedate   DATE,
	tail_nmbr    CHAR(6),
	FleetID      BIGINT,
	Depart DATETIME,
	Arrive DATETIME,
	D_icao_id      CHAR(4),
	A_icao_id     CHAR(4),
	dep_city         VARCHAR(25),
	arr_city         VARCHAR(25),
	dep_airport      VARCHAR(25),
	arr_airport      VARCHAR(25),
	dep_state        VARCHAR(25),
	arr_state        VARCHAR(25),
	dep_country      VARCHAR(60),
	arr_country      VARCHAR(60),
	elp_time     VARCHAR(40),
	[desc]       VARCHAR(40),
	pax_total    INT,
	HomeBased    NVARCHAR(25),
	Type_Code    CHAR(4),
	DutyType CHAR(3),
	RecordType CHAR(1),
	SortDepart DATETIME,
	orig_nmbr         BIGINT,
    legid             BIGINT,
    paxno             INT,
    paxname           VARCHAR(200),
    LegNum     BIGINT
	)

	INSERT INTO #TempTable
	SELECT TEMPFLEET.*	FROM   
			(SELECT DISTINCT PL.DepartureDTTMLocal         ACTIVEDATE,
			F.TailNum,
			F.FleetID,
			PL.DepartureDTTMLocal  DEPART,
			PL.ArrivalDTTMLocal    ARRIVE,
			D.IcaoID  DICAO_ID,
			A.IcaoID  AICAO_ID,
			D.CityName DCityName,
			A.CityName ACityName,
			D.AirportName DAirportName,
			A.AirportName AAirportName,
			D.StateName DStateName,
			A.StateName AStateName,
			D.CountryName DCountryName,
			A.CountryName ACountryName,
			CONVERT(VARCHAR, FLOOR(ISNULL(PL.ElapseTM,0)*10)*0.1) ELP_TIME,
			PM.TripDescription,
			PL.PassengerTotal             AS PAX_TOTAL,
			'' HomeBased,
			F.AircraftCD,
			PL.DutyTYPE,
			PM.RecordType,
			PL.DepartureDTTMLocal,
			PM.TripNUM,
			PL.LegID,
			CASE WHEN P1.PassengerRequestorID <> '' OR P1.PassengerRequestorID <> NULL  THEN RANK() OVER ( PARTITION BY  PL.LegID   ORDER BY P1.PassengerRequestorID ) ELSE 0 END PaxNo,
			ISNULL(P1.LastName+ ',','')  + ISNULL(P1.FirstName,'') + ' ' +ISNULL(P1.MiddleInitial,'') + ''  PaxNames,
			PL.LegNUM
			FROM   PreflightMain PM
			INNER JOIN PreflightLeg PL
			ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
			INNER JOIN (SELECT DISTINCT F.CustomerID,
					 F.FleetID,
					 F.TailNUM,
					 F.AircraftCD,
					 F.HomebaseID
					FROM    ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1
					 INNER JOIN Fleet F ON F1.FleetID=F.FleetID
					LEFT OUTER JOIN FleetGroupOrder FGO
								 ON F.FleetID = FGO.FleetID
									AND	F.CustomerID = FGO.CustomerID
					LEFT OUTER JOIN FleetGroup FG
								 ON FGO.FleetGroupID =
									FG.FleetGroupID
									AND	FGO.CustomerID = FG.CustomerID
					--WHERE  FG.FleetGroupCD IN (SELECT DISTINCT Rtrim(S)FROM	dbo.Splitstring(@FleetGroupCD, ','	))
					-- OR @FleetGroupCD = ''
					 ) F
			ON PM.FleetID = F.FleetID
			AND PM.CustomerID = F.CustomerID
	 
			 LEFT OUTER JOIN PreflightPassengerList PP1 ON PP1.LegID=PL.LegID
			 LEFT OUTER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
			INNER JOIN AIRPORT D
			ON PL.DepartICAOID = D.AirportID
			INNER JOIN Airport A ON PL.ArriveICAOID=A.AirportID
			WHERE  F.CustomerID = dbo.Getcustomeridbyusercd(Ltrim(@UserCD))
			AND CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE, @BeginDate) AND CONVERT(DATE, @EndDate)
			AND PM.IsDeleted = 0
	)TEMPFLEET
	ORDER BY TripNUM,LegID



	------------------------ Maintenance Start--------------------------------------------------

					

				

DELETE FROM #TempTable WHERE RecordType='M'


	DECLARE @DepArrive DATE,@CrDesc VARCHAR(40),@CrDepart DATETIME,@CrArrive DATETIME,@CrDutyType VARCHAR(50),@CrSortDepart DATETIME;
	DECLARE @CrTailNumber   VARCHAR(9),
	@Cractivedate   DATETIME,
	@CrDAType       CHAR(1),
	@CrDepartArrive DATETIME,
	@Cricao_id      CHAR(4),
	@CrHomeBased    VARCHAR(25),
	@Crcity         VARCHAR(25),
	@Crelp_time     VARCHAR(40),
	@PrevDate       DATETIME,
	@DATE           INT,
	@Prevloc        CHAR(4),
	@PrevDAType     CHAR(1),
	@PrevDepart     DATETIME,
	@CrType_Code    CHAR(4),
	@activedate     DATETIME,@CrArrivalDTTMLocal1 DATETIME;
	DECLARE  @CrState VARCHAR(25),@CrCountry VARCHAR(60),@CrAirport VARCHAR(25),@CrPax INT;

	SET @Count=0;
	

	DECLARE c1 CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT F.TailNum,
	       CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal) < CONVERT(DATE,@DATEFROM) THEN @DATEFROM  ELSE PL.DepartureDTTMLocal END DepartureDTTMLocal,
		   CASE WHEN CONVERT(DATE,PL.ArrivalDTTMLocal) > CONVERT(DATE,@DATETO) THEN CONVERT(DATETIME, CONVERT(VARCHAR(10),@DATETO, 101) + ' 23:59')  ELSE PL.ArrivalDTTMLocal END ArrivalDTTMLocal,
	        A.IcaoID,
		   A.CityName,
		   PM.TripDescription,
		   F.AircraftCD,
		   PL.DutyType,
		   CASE WHEN PL.DepartureDTTMLocal < @DATEFROM THEN @DATEFROM  ELSE PL.DepartureDTTMLocal END DepartureDTTMLocal,
			A.StateName,
			A.CountryName,
			A.AirportName,
		   PL.PassengerTotal 
	     FROM PreflightMain PM inner join PreflightLeg PL on PM.TripID=PL.TripID AND PL.ISDELETED = 0
             INNER JOIN Fleet F ON F.FleetID=PM.FleetID
             INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID )F1 ON F1.FLEETID=F.FLEETID
             OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
	WHERE PM.RecordType='M'
	  AND PL.DutyType <>'R'
      AND PM.CustomerID=dbo.Getcustomeridbyusercd(Ltrim(@UserCD))

      AND (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
	 AND PM.ISDELETED = 0
	--ORDER  BY F.TailNum,PL.DepartureDTTMLocal DESC


	OPEN c1

	FETCH NEXT FROM c1 INTO @CrTailNumber,@CrDepart,@CrArrive,@Cricao_id,@Crcity,@Crelp_time,@CrType_Code,@CrDutyType,@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax;

	WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @DATE=(SELECT DATEDIFF(DAY,@CrDepart,@CrArrive))
	SET @activedate=@CrDepart
     SET @CrDutyType= (SELECT (CASE (@CrDutyType) WHEN 'MX' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='MX'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'F' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='F'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'TR' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='TR'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'BJ' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='BJ'  AND CustomerID=@CUSTOMERID)
                                                 WHEN 'G' THEN (SELECT TOP 1 AircraftDutyDescription FROM AircraftDuty WHERE AircraftDutyCD='G'  AND CustomerID=@CUSTOMERID) END ))
    
	WHILE(@Count<=@DATE)
	    
		BEGIN

		
	 IF @Count=@DATE AND @Count <> 0
	
	 BEGIN  SET @CrArrivalDTTMLocal1=@CrArrive END
	 ELSE BEGIN SET @CrArrivalDTTMLocal1=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepart, 101) + ' 23:59') END
	 
		   INSERT INTO #TempTable(tail_nmbr,Depart,Arrive,D_icao_id,A_icao_id,dep_city,arr_city,dep_state,arr_state,dep_airport,arr_airport,dep_country,arr_country,elp_time,activedate,Type_Code,SortDepart,pax_total,[desc]) 
			VALUES (@CrTailNumber,@CrDepart,@CrArrivalDTTMLocal1,@Cricao_id,'',@Crcity,@CrDutyType,@CrState,'',@CrAirport,'',@CrCountry,'','0',@activedate,@CrType_Code,@CrSortDepart,@CrPax,@Crelp_time)              
		    
		    SET @CrDepart=CONVERT(DATETIME, CONVERT(VARCHAR(10),@CrDepart, 101) + ' 00:00')+1
			SET @Count=@Count+1 
			SET @activedate=@activedate+1

		END


	SET @Count=0


	
	FETCH NEXT FROM c1 INTO @CrTailNumber,@CrDepart,@CrArrive,@Cricao_id,@Crcity,@Crelp_time,@CrType_Code,@CrDutyType,@CrSortDepart,@CrState,@CrCountry,@CrAirport,@CrPax;
	END

	CLOSE c1;

	DEALLOCATE c1;




	---- ----------------------Maintenance End-------------------------------------
	

      INSERT INTO #TempTailNum
                  (activedate,tail_nmbr,FleetID,HomebaseAirportID,Type_Code,SortDepart)
      SELECT DISTINCT AllDates AS ACTIVEDATE,
				F.TailNum,F.FleetID,HomebaseAirportID,F.AircraftCD,AllDates
			  FROM   (SELECT DISTINCT F.CustomerID,
                              F.FleetID,
                              F.TailNUM,
                              F.AircraftCD,
                              F.HomebaseID
              FROM   ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1
                     INNER JOIN Fleet F  ON F1.FleetID = F.FleetID
                     LEFT OUTER JOIN FleetGroupOrder FGO
                                  ON F.FleetID = FGO.FleetID
                                     AND F.CustomerID = FGO.CustomerID
                     LEFT OUTER JOIN FleetGroup FG
                                  ON FGO.FleetGroupID = FG.FleetGroupID
                                     AND FGO.CustomerID = FG.CustomerID
                      ) F
             INNER JOIN Company C
                     ON F.HomebaseID = C.HomebaseID
             CROSS JOIN @TblDates
             	WHERE NOT EXISTS(SELECT T.tail_nmbr FROM #TempTable T 
					WHERE T.tail_nmbr = F.TailNum AND T.activedate = AllDates )
            AND  F.CustomerID = dbo.Getcustomeridbyusercd(Ltrim(@UserCD));
             

      WITH PreflightCTE
           AS (SELECT CONVERT(DATE, ArrivalDTTMLocal) AS ARRIVALDTTMLOCAL,
				INTMP.activedate,PL.ArriveICAOID,PM.FleetID,
				ROW_NUMBER()OVER(partition BY PM.FleetID, INTMP.activedate 
									ORDER BY PL.ArrivalDTTMLocal DESC) AS RK
               FROM   PreflightLeg PL
                      INNER JOIN PreflightMain PM
                              ON PL.TripID = PM.TripID AND PM.IsDeleted = 0
                      JOIN #TempTailNum INTMP
                        ON PM.FleetID = INTMP.FleetID
                           AND CONVERT(DATE, ArrivalDTTMLocal) <=
                               CONVERT(DATE, INTMP.activedate)
                           AND PM.TripStatus = 'T'
                           AND PL.IsDeleted = 0)
      UPDATE #TempTailNum
      SET    AirportID = S.ArriveICAOID
      FROM   #TempTailNum OUTTMP
             JOIN PreflightCTE S
               ON S.FleetID = OUTTMP.FleetID
                  AND S.activedate = OUTTMP.activedate
      WHERE  S.rk = 1

      UPDATE #TempTailNum
      SET    AirportID = C.HOMEBASEAIRPORTID
      FROM   #TempTailNum TMP
             JOIN FLEET F
               ON F.FleetId = TMP.FleetID
             INNER JOIN COMPANY C
                     ON F.HOMEBASEID = C.HOMEBASEID
      WHERE  AirportID IS NULL

      UPDATE #TempTailNum
      SET    D_icao_id = A.IcaoID,
             arr_city= CASE
                           WHEN TMP.HomebaseAirportID = A.AirportID THEN
                           'AIRCRAFT AT HOME BASE'
                           ELSE 'AIRCRAFT AWAY OVERNIGHT'
                         END,
             dep_city=A.CityName,
            -- arr_city=A.CityName,
             dep_state=A.StateName,
             dep_country=A.CountryName,
             dep_airport=A.AirportName,
             elp_time = '0.0'
      FROM   #TempTailNum TMP
             JOIN AIRPORT A
               ON A.AIRPORTid = TMP.AirportID


UPDATE #TempTable SET paxno=TEMP.PaxTot FROM
(SELECT DISTINCT T1.paxno,T1.paxname,MAX(T2.paxno) PaxTot,T1.orig_nmbr FROM #TempTable T1 INNER JOIN #TempTable T2 ON T1.LegNum-1=T2.LegNum
                                WHERE T1.orig_nmbr=1890  
                                GROUP BY T1.paxno,T1.paxname,T1.orig_nmbr )Temp 
WHERE #TempTable.orig_nmbr=Temp.orig_nmbr
  AND #TempTable.paxname=''                           
                                
                                


SELECT TempFleet.* FROM 
(SELECT activedate,tail_nmbr,Depart deptime,Arrive arrtime,D_icao_id depicao_id,A_icao_id arricao_id,
       dep_city,arr_city,dep_airport,arr_airport,dep_state,arr_state,dep_country,arr_country,ISNULL(elp_time,0)elp_time,
       [desc],
       Type_Code,SortDepart,ISNULL(pax_total,0)pax_total,ISNULL(orig_nmbr,0)orig_nmbr,ISNULL(legid,0)legid,ISNULL(paxno,0) paxno,paxname 
       FROM #TempTable

UNION ALL

SELECT activedate,tail_nmbr,Depart deptime,Arrive arrtime,D_icao_id depicao_id,A_icao_id arricao_id,
       dep_city,arr_city,dep_airport,arr_airport,dep_state,arr_state,dep_country,arr_country,ISNULL(elp_time,0)elp_time,
       [desc],
       Type_Code,SortDepart,ISNULL(pax_total,0)pax_total,ISNULL(orig_nmbr,0)orig_nmbr,ISNULL(legid,0)legid,ISNULL(paxno,0),paxname 
       FROM #TempTailNum)TempFleet
ORDER BY activedate,Type_Code,SortDepart

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable

IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE #TempTailNum


  END    
  
  	--[GetAircraftCurrentAirpotID]          
	-- EXEC spGetReportPREFleetScheduleAndPaxInfoDatExportInformation 'JWILLIAMS_11','2012-05-16','2012-10-16','n46e',''      
	-- EXEC spGetReportPREFleetScheduleAndPaxInfoDatExportInformation 'JWILLIAMS_13','2011-07-05','2012-07-20','',''    




GO


