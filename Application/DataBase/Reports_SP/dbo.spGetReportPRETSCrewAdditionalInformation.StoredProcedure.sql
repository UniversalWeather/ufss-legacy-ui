IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSCrewAdditionalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSCrewAdditionalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSCrewAdditionalInformation]
	@UserCD VARCHAR(50)--Mandatory
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum AS VARCHAR(50) = ''
	--,@PassengerCD AS VARCHAR(300) = ''
	
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSCrewAdditionalInformation
-- Author: AISHWARYA.M
-- Create date: 30 Aug 2012
-- Description: Get Preflight TripSheet Passenger No Fly List information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- =================================================================================
SET NOCOUNT ON
	SELECT --[LegID] = CONVERT(VARCHAR,PL.LegID)
			 CrewName = ISNULL(C.LastName,'') + ', ' + ISNULL(C.FirstName,'') + ' ' + ISNULL(C.MiddleInitial,'')
			,CrewCode = CONVERT(VARCHAR(MAX),ISNULL(C.CrewCD,''))
			,Code = CONVERT(VARCHAR(MAX),ISNULL(CI.CrewInfoCD,''))
			,CDescription = CONVERT(VARCHAR(MAX),ISNULL(CRD.InformationDESC,''))
			--,CAddlInfo = dbo.FlightPakDecrypt(ISNULL(CRD.InformationValue,''))
			,CAddlInfo = CONVERT(VARCHAR(MAX),ISNULL(CRD.InformationValue,''))
		   FROM (
				SELECT DISTINCT PCL.CrewID--, PL.LegID, PL.LegNUM 
				FROM PreflightMain PM
				INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
				INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
				WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				AND PM.TripID = CONVERT(BIGINT,@TripID)
				AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
			) CR
			INNER JOIN 
			(
				SELECT CrewID, CrewCD , LastName, FirstName, MiddleInitial, CustomerID
				FROM Crew
			) C ON CR.CrewID = C.CrewID
			INNER JOIN CrewDefinition CRD ON C.CrewID = CRD.CrewID 
			INNER JOIN CrewInformation CI ON CRD.CrewInfoID = CI.CrewInfoID --AND C.CustomerID = CI.CustomerID
			
			WHERE
			CRD.IsDeleted = 0
			AND CRD.InformationValue IS NOT NULL
			AND CRD.InformationValue <> ''
			--AND (P.PassengerRequestorCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerCD, ',')) OR @PassengerCD = '') 
			--AND PINFO.IsShowOnTrip = 1
			
END

--EXEC spGetReportPRETSCrewAdditionalInformation 'SUPERVISOR_99', '10099107564','1'








GO


