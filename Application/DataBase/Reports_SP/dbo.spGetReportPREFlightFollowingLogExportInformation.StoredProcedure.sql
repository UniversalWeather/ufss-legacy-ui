IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREFlightFollowingLogExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREFlightFollowingLogExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ===============================================================================    
-- SPC Name: spGetReportPREFlightFollowingLogExportInformation   
-- Author: Abhishek S    
-- Create date: 6th Aug 2012    
-- Description: Get Flight Following Export Information     
-- Revision History    
-- Date   Name  Ver  Change    
--     
-- ================================================================================ 
CREATE PROCEDURE [dbo].[spGetReportPREFlightFollowingLogExportInformation]

        @UserCD AS VARCHAR(30), -- Mandatory
        @Date AS DATETIME, --Mandatory
        @TailNum AS NVARCHAR(300) = '', -- [Optional], Comma Delimited String with multiple values
        @FleetGroupCD AS NVARCHAR(300) = '' -- [Optional], Comma Delimited String with multiple values

AS
BEGIN	
	SET NOCOUNT ON;
	
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));



CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMERID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
	AND FG.IsDeleted=0
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
	  AND F.IsDeleted=0
	  AND F.IsInActive=0
END
-------------------------------TailNum and Fleet Group Filteration----------------------	
    
    SELECT distinct [tail_nmbr] = F.TailNum
		   ,[orig_nmbr] = PM.TripNUM 
		   ,[leg_num] = PL.LegNUM
		   ,[legid] = PL.LegID1
		   ,[crew] = (SELECT SUBSTRING(C.CrewList,1,LEN(C.CrewList)-1)) 
		   ,[dtlocdep] = PL.DepartureDTTMLocal
		   ,[locdep] = CONVERT(VARCHAR(5),PL.DepartureDTTMLocal,108)
		   ,[dtgmtdep] = PL.DepartureGreenwichDTTM
		   ,[gmtdep] = CONVERT(VARCHAR(5),PL.DepartureGreenwichDTTM,108)
		   ,[dtlocarr] = PL.ArrivalDTTMLocal
		   ,[locarr] = CONVERT(VARCHAR(5),PL.ArrivalDTTMLocal,108)
		   ,[dtgmtarr] = PL.ArrivalGreenwichDTTM
		   ,[gmtarr] = CONVERT(VARCHAR(5),PL.ArrivalGreenwichDTTM,108)
		   ,[depicao_id] =D.IcaoID
		   ,[arricao_id] = A.IcaoID
		   ,[ac_code] = F.AircraftCD
 FROM PreflightMain PM
       INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted=0
	   INNER JOIN ( SELECT DISTINCT PC2.LEGID, CrewList = (
                 SELECT C.CrewCD + ', '  
				 FROM PreflightCrewList PC1 INNER JOIN Crew C ON PC1.CrewID = C.CrewID 
				   WHERE PC1.LEGID = PC2.LEGID 
				   ORDER BY (CASE PC1.DutyTYPE 
                                    WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
									WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)
                   FOR XML PATH('')) FROM PreflightCrewList PC2
		         ) C ON PL.LEGID = C.LEGID 
       INNER JOIN (
			SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.AircraftID 
			FROM Fleet F 						
	             ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID	
	   INNER JOIN (SELECT DISTINCT FLEETID FROM #TempFleetID ) F1 ON F.FleetID = F1.FleetID
	   LEFT OUTER JOIN(SELECT AircraftID, AircraftCD FROM Aircraft)AT ON AT.AircraftID = F.AircraftID 
	   LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
	   LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
	   WHERE PM.IsDeleted = 0
	     AND PM.TripStatus = 'T'
	     AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	     AND CONVERT(DATE,PL.DepartureDTTMLocal) = CONVERT(DATE,@Date)
	     ORDER BY dtlocdep


IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID   
	
    
END

-- EXEC spGetReportPREFlightFollowingLogExportInformation 'JWILLIAMS_11','2012-08-14','',''


GO


