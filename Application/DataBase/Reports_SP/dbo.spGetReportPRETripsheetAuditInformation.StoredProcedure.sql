IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETripsheetAuditInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETripsheetAuditInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETripsheetAuditInformation]
        
        
        @UserCD AS VARCHAR(30), --Mandatory
		@TripRangeFrom AS BIGINT, --Mandatory --TRIP range
		@TripRangeTo AS BIGINT, --Mandatory --TRIP range
		--@TripStatus As CHAR(12) -- comma delimited, no spaces allowed
		@Worksheet AS BIT = 1,
		@Tripsheet AS BIT = 1,
		@Unfulfilled AS BIT = 1,
		@Cancelled AS BIT = 1,
		@SchedSerc AS BIT = 1,
		@Hold AS BIT = 1
		
		AS
-- =============================================
-- SPC Name: [spGetReportPRETripsheetAuditInformation]
-- Author: SINDHUJA.K
-- Create date: 16 July 2012
-- Description: Get Preflight Tripsheet Audit information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
SET NOCOUNT ON
----------------
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @Worksheet = 1
		SET @TripStatus = 'W,'
		
		IF  @Tripsheet = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @Unfulfilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @Cancelled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @SchedSerc = 1
		SET @TripStatus = @TripStatus + 'S,'
		
		IF  @Hold = 1
		SET @TripStatus = @TripStatus + 'H'
		
----------------------------
--print 'TripStatus: ' + @TripStatus
		
		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
		DECLARE @ParameterDefinition AS NVARCHAR(100)
		
		SET @SQLSCRIPT = '
			SELECT
		    [TripNumber] = PM.TripNUM,
            [Description] = PM.TripDescription,
			[DispatchNumber] = PM.DispatchNUM,
			[Type] =  Case PM.RecordType 
			               WHEN ''T'' THEN ''Tripsheet'' 
						   WHEN ''M'' THEN ''Fleet Calendar'' 
						   WHEN ''C'' THEN ''Crew Calendar'' 
						   ELSE ''Unknown'' 
						   END,
			[TripStatus] = CASE PM.TripStatus WHEN ''X'' THEN ''C'' ELSE PM.TripStatus END,
			[CancellationDescription] = PM.CancelDescription,
			[TailNumber] = F.TailNUM,
			[User] = PM.LastUpdUID,
			[LastUpdated] =CONVERT(VARCHAR(10), PM.LastUpdTS, dbo.GetReportDayMonthFormatByUserCD(@UserCD)) 
			--RIGHT(CONVERT(VARCHAR(12), PM.LastUpdTS, 101), 9)
			FROM PreflightMain PM 
				INNER JOIN (SELECT FleetID, TailNUM FROM Fleet) F ON PM.FleetID = F.FleetID
				
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) 
		    AND PM.TripNUM BETWEEN @TripRangeFrom AND @TripRangeTo 
		    -- AND PM.RecordType = ''T'' 
		    AND PM.ISDELETED =0 ' 

	IF @TripStatus <> ''
	 BEGIN  
		SET @SQLSCRIPT = @SQLSCRIPT + ' AND PM.TripStatus IN (''' + REPLACE(CASE WHEN RIGHT(@TripStatus, 1) = ',' THEN LEFT(@TripStatus, LEN(@TripStatus) - 1) ELSE @TripStatus END, ',', ''', ''') + ''')';
	END			    
		    
	--PRINT @SQLSCRIPT
		    
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @TripRangeFrom AS BIGINT, @TripRangeTo AS BIGINT '
   EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @TripRangeFrom, @TripRangeTo 
    
    --EXEC spGetReportPRETripsheetAuditInformation 'jwilliams_11','1936','1937',1,1,1,1,1,1
    --EXEC spGetReportPRETripsheetAuditInformation 'jwilliams_11','1936','1937',0,0,0,1,0,0



GO


