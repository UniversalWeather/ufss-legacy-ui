IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSExport_Main]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSExport_Main]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetReportPRETSExport_Main]
	@UserCD VARCHAR(50)--Mandatory
	,@TripID BIGINT --Mandatory
AS
BEGIN
-- =========================================================================
-- SPC Name: spGetReportPRETSExport_Main
-- Author: AISHWARYA.M
-- Create date: 13 Sep 2012
-- Description: Get Preflight Tripsheet Report Export information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================
SET NOCOUNT ON

---------------
DECLARE @tblFlag TABLE
	(
	ROWID INT
	,FLAG CHAR(1)
	)
	
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (1,'A')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (2,'B')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (3,'O')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (4,'D')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (5,'C')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (6,'P')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (7,'F')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (8,'E')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (9,'H')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (10,'Q')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (11,'G')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (12,'I')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (13,'J')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (14,'K')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (15,'L')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (16,'M')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (17,'N')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (18,'S')
	INSERT INTO @tblFlag(ROWID, FLAG) VALUES (19,'T')

-----------------
SELECT * FROM @tblFlag FLAG
OUTER APPLY
(
	SELECT DISTINCT
	--PL.LegID,
	[flag] = ''
	,[lowdate] = LEFT(dbo.GetTripDatesByTripIDUserCD(@TripID, @UserCD), 8)
	,[highdate] = RIGHT(dbo.GetTripDatesByTripIDUserCD(@TripID, @UserCD), 8)
	,[orig_nmbr] = PM.TripNUM
	,[estdepdt] = PM.EstDepartureDT
	,[dispatchno] = PM.DispatchNUM
	,[tail_nmbr] = F.TailNum
	,[type_code] = A.AircraftCD
	,[desc] = PM.TripDescription
	,[reqcode] = PM.PassengerRequestorID
	,[requestor] = PM.PassengerRequestorID
	,[reqname] = PM.RequestorLastName + ',' + PM.RequestorFirstName + ',' + PM.RequestorMiddleName
	,[dispatcher] = UM.FirstName
	,[dsptname] = PM.DsptnName
	,[crewcode] = C.CrewCD
	,[releasedby] = PM.ReleasedBy
	,[verifynmbr] = PM.VerifyNUM
	,[reqaddlphone] = PAX.AdditionalPhoneNum
	,[paxname] = PP.PassengerFirstName + ',' + PP.PassengerMiddleName + ' ' +PP.PassengerLastName 
	--,[ppnum] =  PP.PassportNum 
	,[ppnum] = dbo.FlightPakDecrypt(ISNULL(PP.PassportNUM,''))
	,[nationcode] = CO.CountryCD
	,[citizen] = CO.CountryName 
	,[dob] = PAX.DateOfBirth
	,[phone] = PAX.PhoneNum
	,[leg_num] = PL.LegNum
	,[legid] = PL.LegID
	,[depicao_id] = AD.IcaoID
	,[depcity] = AD.CityName
	,[arricao_id] = AA.IcaoID
	,[arrcity] = AA.CityName
	,[fltno] = PM.FlightNUM
	,[pax_total] = PL.PassengerTotal
	,[depzulu] = AD.OffsetToGMT
	,[arrzulu] = AA.OffsetToGMT
	,[locdep] = PL.DepartureDTTMLocal
	,[locarr] = PL.ArrivalDTTMLocal
	,[gmtdep] = PL.DepartureGreenwichDTTM
	,[gmtarr] = PL.ArrivalGreenwichDTTM
	,[timeapprox] = PL.IsApproxTM
	,[distance] = PL.Distance
	,[elp_time] = PL.ElapseTM 
	,[code] = PL.FBOID
	,[purpose] = PL.FlightPurpose
	,[cat_code] = PL.FlightCategoryID
	,[fltcatdesc] = FC.FlightCatagoryDescription
	,[farnum] = PL.FedAviationRegNUM
	,[depfbo_desc] = FBD.FBOVendor
	,[depfbo_phone] = FBD.PhoneNUM1
	,[depfbo_phone2] = FBD.PhoneNUM2
	,[depfbo_freq] = FBD.Frequency
	,[depfbo_fax] = FBD.FaxNum
	,[depfbo_uvair] = FBD.IsUWAAirPartner
	,[depfbo_fuel_brand] = FBD.FuelBrand
	,[depfbo_negfuelpr] = FBD.NegotiatedFuelPrice
	,[depfbo_lastfuelpr] = FBD.LastFuelPrice
	,[depfbo_lastfueldt] = FBD.LastFuelDT
	,[depfbo_contact] = FBD.Contact
	,[depfbo_addr1] = FBD.Addr1
	,[depfbo_addr2] = FBD.Addr2
	,[depfbo_paytype] = FBD.PaymentType
	,[depfbo_postedpr] = FBD.PostedPrice
	,[depfbo_fuelqty] = FBD.FuelQty
	,[arrfbo_desc] = FBA.FBOVendor 
	,[arrfbo_phone] = FBA.PhoneNUM1
	,[arrfbo_phone2] = FBA.PhoneNUM2
	,[arrfbo_freq ]= FBA.Frequency
	,[arrfbo_fax] = FBA.FaxNum
	,[arrfbo_uvair] = FBA.IsUWAAirPartner
	,[arrfbo_fuel_brand] = FBA.FuelBrand 
	,[arrfbo_negfuelpr] = FBA.NegotiatedFuelPrice
	,[arrfbo_lastfuelpr] = FBA.LastFuelPrice
	,[arrfbo_lastfueldt] =  FBA.LastFuelDT
	,[arrfbo_contact] = FBA.Contact
	,[arrfbo_addr1] = FBA.Addr1
	,[arrfbo_addr2] = FBA.Addr2
	,[arrfbo_paytype] = FBA.PaymentType
	,[arrfbo_postedpr] = FBA.PostedPrice
	,[arrfbo_fuelqty] = FBA.FuelQty
	,[dept_code] = D.DepartmentCD
	,[dept_desc] = D.DepartmentName
	,[auth_code] = DA.AuthorizationCD
	,[auth_desc] = DA.DeptAuthDescription
	,[authphone] = 'CQFILE.DBF/PHONE'
	,[winds] = PL.WindsBoeingTable
	 --(Crew Hotel)
	,[crewh_name] = CH.Name
	,[crewh_phone] = CH.PhoneNum
	,[crewh_fax] = CH.FaxNum
	,[crewh_rate] = CH.NegociatedRate
	,[crewh_addr1] = CH.Addr1
	,[crewh_addr2] = CH.Addr2
	,[crewh_rooms] = CH.NoofRooms
	 --(Arr Crew Trans)
	,[crewta_name] = TCA.TransportationVendor
	,[crewta_phone] = TCA.PhoneNum
	,[crewta_fax] = TCA.FaxNum
	,[crewta_rate] = TCA.NegotiatedRate
	----(Dep Crew Trans)
	,[crewtd_name ] = TCD.TransportationVendor
	,[crewtd_phone] =  TCD.PhoneNum
	,[crewtd_fax] = TCD.FaxNum
	,[crewtd_rate] = TCD.NegotiatedRate
	----(Pax Hotel)
	,[hotel_name ] = PH.Name
	,[hotel_phone] = PH.PhoneNum
	,[hotel_fax] = PH.FaxNum
	,[hotel_rate] = PH.NegociatedRate
	,[hotel_rooms] = PH.NoofRooms
	----(Arr Pax Trans)
	,[paxta_name ] = TPA.TransportationVendor
	,[paxta_phone] = TPA.PhoneNum
	,[paxta_fax] = TPA.FaxNum
	,[paxta_rate] = TPA.NegotiatedRate
	----(Dep Pax Trans)
	,[paxtd_name ] = TPD.TransportationVendor
	,[paxtd_phone ] = TPD.PhoneNum
	,[paxtd_fax] = TPD.FaxNum
	,[paxtd_rate] = TPD.NegotiatedRate
	----(Arr Catering)
	,[catera_name ] = CA.CateringVendor
	,[catera_phone] = CA.PhoneNum
	,[catera_fax] = CA.FaxNum
	,[catera_rate] = CA.NegotiatedRate
	----(Dep Catering)
	,[caterd_name ] = CD.CateringVendor
	,[caterd_phone] = CA.PhoneNum
	,[caterd_fax] = CA.FaxNum
	,[caterd_rate] = CA.NegotiatedRate
	----(Addl Crew Hotel)
	,[main_name ] = ACH.Name
	,[main_phone] = ACH.PhoneNum
	,[main_fax] = ACH.FaxNum
	,[main_rate] = ACH.NegociatedRate
	,[main_addr1] = ACH.Addr1
	,[main_addr2] = ACH.Addr2
	,[piccode] = PIC.CrewCD 
	,[pic] = PIC.FirstName + '' + PIC.LastName + '' + PIC.MiddleInitial
	,[siccode] = SIC.CrewCD
	,[sic] = SIC.FirstName + '' +SIC.LastName + '' + SIC.MiddleInitial
	,[pic_mblphn] = PIC.CellPhoneNum
	,[sic_mblphn] = SIC.CellPhoneNum
	,[fltphone] = F.FlightPhoneNum
	,[infltphone] =F.FlightPhoneIntlNum
	,[fuel_burn] = ''
	,[ac_code] = F.AircraftCD
	,[numlegs] = (SELECT MAX(LEGNUM) FROM PREFLIGHTLEG WHERE TRIPID = @TripID)
	,[fuel_load] = PL.FuelLoad
	,[revisndesc] = PM.RevisionDescriptioin
	,[revisnnmbr] = PM.RevisionNUM
	,[dsptemail] = UM.EmailID
	,[dsptphone] = UM.PhoneNum
	,[custdesc]  = CASE PM.IsQuote WHEN 1 THEN PM.AuthorizationDescription ELSE '' END
	,[reqauth] = DA.AuthorizationCD
	,[cqflag] = PM.IsQuote
	,[custphone] = 'CQFILE.DBF/PHONE'
	,[estfuelqty] = PM.EstFuelQTY
	--(Dep FBO)
	,[depfbo_city ] =  FBD.CityName
	,[depfbo_state] = FBD.StateName
	,[depfbo_zip] = FBD.PostalZipCD
	--(Arr FBO)
	,[arrfbo_city ] = FBA.CityName
	,[arrfbo_state] = FBA.StateName
	,[arrfbo_zip] = FBA.PostalZipCD
	--(Crew Hotel)
	,[crewh_city ] = CH.CityName
	,[crewh_state] = CH.StateName
	,[crewh_zip] = CH.PostalZipCD
	--(Pax Hotel)
	,[hotel_addr1 ] = PH.Addr1
	,[hotel_addr2] = PH.Addr2
	,[hotel_city] = PH.CityName
	,[hotel_state] = PH.StateName
	,[hotel_zip] = PH.PostalZipCD
	--(Addl Crew Hotel)
	,[main_city ] = ACH.CityName
	,[main_state] = ACH.StateName
	,[main_zip] = ACH.PostalZipCD
	
	 FROM PreflightMain PM
	 INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
	 INNER JOIN (SELECT AirportID, IcaoID, CityName, OffsetToGMT FROM Airport)AD ON AD.AirportID = PL.DepartICAOID
	 INNER JOIN (SELECT AirportID, IcaoID, CityName, OffsetToGMT FROM Airport)AA ON AA.AirportID = PL.ArriveICAOID
	 INNER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft) A ON PM.AircraftID = A.AircraftID
	 INNER JOIN (SELECT LegID, CrewID, DutyTYPE FROM PreflightCrewList) PC ON PL.LegID = PC.LegID 
	 INNER JOIN (SELECT CrewID, CrewCD FROM Crew) C ON PC.CrewID = C.CrewID
	 INNER JOIN PreflightPassengerList PP ON PL.LegID = PP.LegID
	 INNER JOIN Passenger Pax ON PP.PassengerID = Pax.PassengerRequestorID
	 INNER JOIN (SELECT FleetID, TailNum, FlightPhoneNum, FlightPhoneIntlNum, AircraftCD FROM Fleet) F ON PM.FleetID = F.FleetID 
	 INNER JOIN Passenger Req ON PM.PassengerRequestorID = Req.PassengerRequestorID
	 INNER JOIN UserMaster UM ON PM.CustomerID = UM.CustomerID AND PM.DispatcherUserName = UM.UserName
	 	
	 --FOR PIC
	INNER JOIN(
		SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.DutyTYPE = 'P'
	) PIC ON PL.LegID = PIC.LegID
	
	--FOR SIC			
	LEFT OUTER JOIN(
		SELECT PC.DUTYTYPE, PC.LEGID, C.CREWCD, C.CellPhoneNum, C.LastName, C.FirstName, C.MiddleInitial 
		FROM PreflightCrewList PC
		INNER JOIN Crew C ON PC.CrewID = C.CrewID
		WHERE PC.DutyTYPE = 'S'
	) SIC ON PL.LegID = SIC.LegID
								
	 LEFT OUTER JOIN (
		SELECT AuthorizationID, AuthorizationCD, DeptAuthDescription FROM DepartmentAuthorization
					) DA ON PM.AuthorizationID = DA.AuthorizationID
	 LEFT OUTER JOIN Country CO ON PAX.CountryID = CO.CountryID
	 LEFT OUTER JOIN FlightCatagory FC ON PL.FlightCategoryID = FC.FlightCategoryID
	 LEFT OUTER JOIN Department D ON PAX.DepartmentID = D.DepartmentID
	 
	 LEFT OUTER JOIN PreflightFBOList PFL ON PL.LegID = PFL.LegID
	 LEFT OUTER JOIN FBO FBD ON PFL.FBOID = FBD.FBOID AND PFL.IsDepartureFBO = 1
	 LEFT OUTER JOIN FBO FBA ON PFL.FBOID = FBA.FBOID AND PFL.IsArrivalFBO = 1

	 LEFT OUTER JOIN PreflightTransportList PTL ON PL.LegID = PTL.LegID
	 LEFT OUTER JOIN Transport TPD ON PTL.TransportID = TPD.TransportID AND PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
	 LEFT OUTER JOIN Transport TPA ON PTL.TransportID = TPA.TransportID AND PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
	 LEFT OUTER JOIN Transport TCD ON PTL.TransportID = TCD.TransportID AND PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'C'
	 LEFT OUTER JOIN Transport TCA ON PTL.TransportID = TCA.TransportID AND PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'C'
	
	 LEFT OUTER JOIN PreflightCateringDetail PCL ON PL.LegID = PCL.LegID
	 LEFT OUTER JOIN Catering CD ON PCL.CateringID = CD.CateringID AND PCL.ArriveDepart = 'D'
	 LEFT OUTER JOIN Catering CA ON PCL.CateringID = CA.CateringID AND PCL.ArriveDepart = 'A'	
	
	
	--Pax Hotel
	LEFT OUTER JOIN ( SELECT 
		PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.Addr1,HP.Addr2,HP.CityName,
		HP.StateName,HP.PostalZipCD,HP.Remarks,HP.NegociatedRate,
		PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments 
		FROM PreflightPassengerList PPL 
		INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
		INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
	) PH ON PL.legID = PH.LegID

	--Crew Hotel
	
	LEFT OUTER JOIN (			
		SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
			 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
			 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments  
		FROM PreflightCrewList PCLL 
		INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
		WHERE PCLL.DutyType IN ('P', 'S')
	) CH ON PL.legID = CH.LegID

	--Addnl Crew / Maint Crew - Hotel
	LEFT OUTER JOIN (			
		SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
				 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
				 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments   
		FROM PreflightCrewList PCLL 
		INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
		WHERE PCLL.DutyType NOT IN ('P', 'S')
	) ACH ON PL.legID = ACH.LegID
	
	WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	AND PM.TripID = @TripID	
) MAIN	
ORDER BY  MAIN.LEGID, FLAG.RowID

END

	--EXEC spGetReportPRETSExport_Main 'UC', 10000350

GO


