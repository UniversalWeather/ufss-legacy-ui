IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPassengerRequestor2MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPassengerRequestor2MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportPassengerRequestor2MhtmlInformation]
	@UserCD VARCHAR(30),
	@PassengerCD varchar(500)
AS
-- ===============================================================================================
-- SPC Name: spGetReportPassengerRequestor2MhtmlInformation
-- Author: A.Akhila
-- Create date: 28 Jun 2012
-- Description: Get Passenger Mhtml Information for Report Passenger/Requestor II
-- Revision History
-- Date			Name		Ver		Change
-- 10 Oct		Akhila	1.1		ClientID filter
--
-- ===============================================================================================
SET NOCOUNT ON

DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
DECLARE @ParameterDefinition AS NVARCHAR(100);

DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
--SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

SET @SQLSCRIPT = '
	SELECT
		[paxname] = P.PassengerName
		,[paxcode] = P.PassengerRequestorCD
		--,[phone] = P.PhoneNum
		,[phone] = P.AdditionalPhoneNum --Business Phone
		,[homebase] = dbo.GetHomeBaseCDByHomeBaseID(P.HomeBaseID)
		,[emp_t_cng] = P.IsEmployeeType
		,[active] = P.IsActive
		,[nationcode] = dbo.GetCountryCDByCountryID(P.CountryID)
		,[dob] = CONVERT(varchar(10), P.DateOfBirth, dbo.GetReportDayMonthFormatByUserCD(@UserCD))
		,[dlastleg] = ''Unknown'' --Not sure from which table they are coming
		,[lognum] = ''Unknown'' --lognum is there in POPAX & POLEGS
		,[ltop] = ''Unknown'' --Not sure from which table they are coming
		,[norder] = ''Unknown'' --norder is coming from POPAX.DBF
		--,[Passport] = dbo.FlightPakDecrypt(IsNull(PPI.PassportNUM,''''))  + ''-'' + dbo.GetCountryCDByCountryID(PPI.CountryID)                                              --Added for Mhtml
		,[Passport] = ''@#$'' + IsNull(PPI.PassportNUM,'''') + ''@#$ - '' + dbo.GetCountryCDByCountryID(PPI.CountryID)                                              --Added for Mhtml
		--,[VISA] = dbo.FlightPakDecrypt(IsNull(CPV.VisaNum,'''')) + ''-'' + dbo.GetCountryCDByCountryID(CPV.CountryID) --CoutryCD is appended as per the As-Is PDF report   --Added for Mhtml
		,[VISA] = ''@#$'' + IsNull(CPV.VisaNum,'''') + ''@#$ - '' + dbo.GetCountryCDByCountryID(CPV.CountryID) --CoutryCD is appended as per the As-Is PDF report   --Added for Mhtml

	FROM Passenger P 
	LEFT OUTER JOIN CrewPassengerVisa CPV
				ON P.CustomerID = CPV.CustomerID
				AND P.PassengerRequestorID = CPV.PassengerRequestorID
	LEFT OUTER JOIN (
		SELECT CustomerID, PassengerRequestorID, PassportNum, CountryID 
		FROM dbo.CrewPassengerPassport
	) PPI
		ON P.CustomerID = PPI.CustomerID
		AND P.PassengerRequestorID = PPI.PassengerRequestorID
	WHERE P.IsDeleted = 0
	AND P.CustomerID = @CUSTOMERID
	'
	SET @SQLSCRIPT = @SQLSCRIPT + ' AND P.PassengerRequestorCD IN (''' + REPLACE(CASE WHEN RIGHT(@PassengerCD, 1) = ',' THEN LEFT(@PassengerCD, LEN(@PassengerCD) - 1) ELSE @PassengerCD END, ',', ''', ''') + ''')';
	SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @CUSTOMERID BIGINT'
	EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @CUSTOMERID

	--print @SQLSCRIPT
-- EXEC spGetReportPassengerRequestor2MhtmlInformation 'ELIZA_9', 'CCHIN'


GO


