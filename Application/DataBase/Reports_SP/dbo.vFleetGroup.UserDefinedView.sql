IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vFleetGroup]'))
DROP VIEW [dbo].[vFleetGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vFleetGroup] AS SELECT 
	F.TailNum
	,F.FleetID
	,FG.FleetGroupID
	,FGO.FleetGroupOrderID
	,F.CustomerID
	,FG.HomebaseID
	,FG.FleetGroupCD
	,FG.FleetGroupDescription
	,FGO.OrderNum
	,F.MaximumPassenger
	,F.AircraftCD
	,F.AircraftID
 FROM Fleet F
 LEFT JOIN 
FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
LEFT JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID=FG.CustomerID 
WHERE F.IsDeleted=0
AND F.IsInActive=0


GO


