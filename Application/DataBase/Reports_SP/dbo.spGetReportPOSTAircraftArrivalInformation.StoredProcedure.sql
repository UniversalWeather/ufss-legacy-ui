IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTAircraftArrivalInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTAircraftArrivalInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetReportPOSTAircraftArrivalInformation]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@DATEFROM AS DATETIME, --Mandatory
	@DATETO AS DATETIME, --Mandatory
	@IcaoID AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@RequestorCD AS NVARCHAR(1000) = '' -- [Optional], Comma delimited string with mutiple values
)
AS
---- ===============================================================================
---- SPC Name: spGetReportPOSTAircraftArrivalInformation
---- Author:  D.Mullai
---- Create date: 
---- Description: Get Postflight Aircraft Arrival for REPORTS
---- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
---- ================================================================================

SET NOCOUNT ON
SELECT   
	[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
	[ICAOID]=AA.IcaoID,
	[Arrv] =AA.ArriveICAOID, ---SUM(COUNT(PL.ArriveICAOID)) OVER (PARTITION BY AA.ICAOID),
	[City] = AA.CityName,
	[ST] = CAST(AA.StateName as varchar(3)),
	[Country] = AA.CountryName,
	[Airport] = AA.AirportName,
	[PassengerCD] = P.PassengerRequestorCD,
	[PassengerName] = P.PassengerName,
	[PassengerCount] = CASE WHEN P.PassengerRequestorCD IS NOT NULL THEN COUNT(P.PassengerRequestorCD) ELSE NULL END,
	[ISREQUESTOR] = P.IsRequestor
FROM PostflightMain PM 
	INNER JOIN  PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted=0
	LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = PL.POLegID
	LEFT OUTER JOIN(SELECT COUNT(PL.ArriveICAOID) ArriveICAOID,AirportID,IcaoID,CityName,StateName,CountryName,AirportName FROM Airport A INNER JOIN PostflightLeg PL ON A.AirportID=PL.ArriveICAOID AND PL.IsDeleted=0
																																		   INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted=0
	                       WHERE  PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	                       AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
	                                  GROUP BY AirportID,IcaoID,CityName,StateName,CountryName,AirportName) AA ON AA.AirportID = PL.ArriveICAOID 
	LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
	LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = PL.PassengerRequestorID --AND PP.IsRequestor=1
WHERE PL.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
    AND PM.IsDeleted=0
	AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
	AND (PP.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
	AND (AA.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID = '')
GROUP BY  AA.IcaoID, AA.CityName, AA.StateName, AA.CountryName, AA.AirportName, P.PassengerRequestorCD, P.PassengerName, P.IsRequestor,AA.ArriveICAOID
ORDER BY [Arrv] DESC
--EXEC spGetReportPOSTAircraftArrivalInformation 'SUPERVISOR_99','2009/1/1','2010/1/1', '', ''





--SET NOCOUNT ON
		
--		DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
--		DECLARE @ParameterDefinition AS NVARCHAR(100)

--	SET @SQLSCRIPT = '
	   
--			SELECT   
--			[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +'' - ''+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
--			[ICAOID]=AA.IcaoID,
--			count(PL.ArriveICAOID) AS [Arrv],
--			[City] = AA.CityName,
--			[ST] = cast(AA.StateName as varchar(3)),
--			[Country] = AA.CountryName,
--			[Airport] = AA.AirportName

--			FROM PostflightLeg PL 
--		INNER JOIN  PostflightMain PM ON PM.POLogID = PL.POLogID
--		LEFT OUTER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID 
	 
--		WHERE PL.CustomerID = ' + CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
--				+ ' AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
--	'
	
--	--Construct OPTIONAL clauses
			
	
--	IF @IcaoID <> '' BEGIN  
--		SET @SQLSCRIPT = @SQLSCRIPT + ' AND AA.IcaoID IN (''' + REPLACE(CASE WHEN RIGHT(@IcaoID, 1) = ',' THEN LEFT(@IcaoID, LEN(@IcaoID) - 1) ELSE @IcaoID END, ',', ''', ''') + ''')';
--	END	
    
--	SET @SQLSCRIPT = @SQLSCRIPT + '	group by  AA.IcaoID, AA.CityName, AA.StateName, AA.CountryName, AA.AirportName'
--	SET @SQLSCRIPT = @SQLSCRIPT + '	order by Arrv desc'
	 
--	--PRINT @SQLSCRIPT
--	SET @ParameterDefinition =  '@DATEFROM AS DATETIME, @DATETO AS DATETIME, @UserCD AS VARCHAR(30)'
--    EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @DATEFROM, @DATETO, @UserCD 
  
--   -- EXEC spGetReportPOSTAircraftArrivalInformation 'jwilliams_11','2009/01/21','2009/12/12', ''




GO


