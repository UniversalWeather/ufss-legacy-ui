IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewPassportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewPassportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetReportCrewPassportInformation]
	@UserCD varchar(30),
	@CrewCD char(7)
AS
-- =============================================
-- SPC Name: spGetReportCrewPassportInformation
-- Author: SUDHAKAR J
-- Create date: 14 Jun 2012
-- Description: Get Crew Checklist Date Information for Report, Crew Roaster
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT
		CPP.CustomerID
		,C.CrewCD
		,CPP.Choice
	--	,CPP.PassportNum
		--,dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')) AS PassportNum
		,ISNULL(CPP.PassportNum,'') AS PassportNum
		--,PassportExpiryDT
		,dbo.GetDateFormatByUserCD(@UserCD, CPP.PassportExpiryDT) AS PassportExpiryDT
		,CPP.IssueCity
		,[PassportCountry] = dbo.GetCountryCDByCountryID(CPP.CountryID) --CPP.PassportCountry
		,CPP.PilotLicenseNum
	FROM CrewPassengerPassport CPP
		LEFT OUTER JOIN (
			SELECT CrewID, CrewCD FROM Crew
		) C ON CPP.CrewID = C.CrewID
	WHERE CPP.IsDeleted = 0
		AND CPP.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
		AND C.CrewCD = RTRIM(@CrewCD)
	
  -- EXEC spGetReportCrewPassportInformation 'jwilliams_11', '1GV'



GO


