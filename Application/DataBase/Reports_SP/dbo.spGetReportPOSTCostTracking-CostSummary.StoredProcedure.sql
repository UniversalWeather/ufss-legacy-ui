IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTCostTracking-CostSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTCostTracking-CostSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





										
CREATE PROCEDURE [dbo].[spGetReportPOSTCostTracking-CostSummary]											
    @UserCD AS VARCHAR(30) ,
    @UserCustomerID AS BIGINT ,
    @YEAR AS NVARCHAR(4),
    @IcaoID varchar(30)='' ,
    @LogNum AS BIGINT = '',
    @TailNum AS VARCHAR(100) = '',
    @FleetGroupCD AS NVARCHAR(1000) = '',
    @CrewCD VARCHAR(500)='',
    @CrewGroupCD VARCHAR(500)='',
    @UserHomebaseID AS VARCHAR(30),
    @IsHomebase AS BIT = 0
    
    
   AS 
   
BEGIN   
------------------------------------Date Calculation------------------------------   
DECLARE @MonthStart INT,@MonthEnd INT;

SELECT @MonthStart=ISNULL(CASE WHEN FiscalYRStart<=0 OR FiscalYRStart IS NULL THEN 01 ELSE FiscalYRStart END,01) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID
SELECT @MonthEnd=ISNULL(CASE WHEN FiscalYREnd<=0 THEN 12 ELSE FiscalYREnd END,12) FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID WHERE C.CustomerID=@UserCustomerID

DECLARE  @FromDate DATE =(SELECT DATEADD(month,( @MonthStart)-1,DATEADD(year,@YEAR-1900,0)))
DECLARE @ToDate  DATE=(SELECT DATEADD(day,-1,DATEADD(month,( @MonthEnd),DATEADD(year,CASE WHEN @MonthStart >1 THEN  @YEAR+1 ELSE @YEAR END-1900,0)))) 

-----------------------------Crew and Crew Group Filteration----------------------
DECLARE  @TempCrewID TABLE     
(   
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT,
CrewCD VARCHAR(5),
CrewMember VARCHAR(60),
CustomerID BIGINT,
HomeBaseID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
INSERT INTO @TempCrewID
SELECT DISTINCT C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
FROM Crew C 
WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
AND C.CustomerID = @UserCustomerID  
AND C.IsDeleted=0
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO @TempCrewID
SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID--F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
FROM Crew C
LEFT OUTER JOIN CrewGroupOrder CGO 
ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
LEFT OUTER JOIN CrewGroup CG 
ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
AND C.CustomerID = @UserCustomerID 
AND C.IsDeleted=0 
AND C.IsStatus=1

END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = '' 
BEGIN 

INSERT INTO @TempCrewID
SELECT DISTINCT  C.CrewID,C.CrewCD,C.LastName+', '+ISNULL(C.FirstName,'') CrewMember,C.CustomerID,C.HomebaseID
FROM Crew C 
WHERE C.CustomerID = @UserCustomerID
AND C.IsDeleted=0
END

-----------------------------TailNum and @FleetGroupCD Filteration----------------------------------

DECLARE  @TempFleetID TABLE     
(   
ID INT NOT NULL IDENTITY (1,1), 
FleetID BIGINT,
HomeBaseID BIGINT,
TailNum VARCHAR(9)
  )
  

IF @TailNum <> ''
BEGIN
INSERT INTO @TempFleetID
SELECT DISTINCT F.FleetID,F.HomebaseID,F.TailNum 
FROM Fleet F
WHERE F.CustomerID = @UserCustomerID
AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
SELECT DISTINCT  F.FleetID, F.HomebaseID,F.TailNum
FROM Fleet F 
LEFT OUTER JOIN FleetGroupOrder FGO
ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
LEFT OUTER JOIN FleetGroup FG 
ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
AND F.CustomerID = @UserCustomerID  
AND FG.IsDeleted=0
END
ELSE IF @TailNum = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
SELECT DISTINCT  F.FleetID,F.HomebaseID,F.TailNum
FROM Fleet F 
WHERE F.CustomerID = @UserCustomerID  
AND F.IsDeleted=0
END
-------------------------------------------------------------------------------------------------------


DECLARE @DateTable TABLE (SeqID INT IDENTITY,MonthNo VARCHAR(10) ,Year VARCHAR(10),AccountID VARCHAR(15),AccountNum VARCHAR(100),AccountDescription VARCHAR(60) );

DECLARE @Today DATETIME, @nMonths TINYINT
SET @Today =@FromDate ---startdate
SET @nMonths = 12

SELECT T.* INTO #TEMP FROM 
(SELECT MonthNo = datepart(month,DATEADD(dd, -1, DATEADD(month, n.n + DATEDIFF(month, 0, @Today),0))) ,
Year =DATEPART(yyyy, DATEADD(dd, -1, DATEADD(month, n.n + DATEDIFF(month, 0, @Today),0)) )
FROM (SELECT TOP(@nMonths) n = ROW_NUMBER() OVER (ORDER BY NAME) FROM master.dbo.syscolumns) n
)T


--------------------------------------------------------------------------------------------
DECLARE @AccountNum TABLE(RowID INT IDENTITY,
				          AccountNum VARCHAR(32))
INSERT INTO @AccountNum
SELECT DISTINCT FederalTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                   WHERE C.CustomerID=@UserCustomerID 
                   AND FederalTax IS NOT NULL 
                   AND FederalTax <> ''
                   
UNION ALL
SELECT DISTINCT SaleTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
               WHERE C.CustomerID=@UserCustomerID 
               AND SaleTax IS NOT NULL 
               AND SaleTax <> ''
UNION ALL
SELECT DISTINCT StateTax FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID 
				   WHERE C.CustomerID=@UserCustomerID 
				   AND StateTax IS NOT NULL 
				   AND StateTax <> ''


-----------------------------------------------------------------------------------------------



INSERT INTO @DateTable
            (
             MonthNo,
             Year,
             AccountID,
             AccountNum,
             AccountDescription
             )
     SELECT DISTINCT 
            T.MonthNo,
            T.Year,
            AC.AccountID,
            AC.AccountNum,
            AC.AccountDescription FROM Account AC CROSS JOIN #TEMP T
            
 
  DECLARE @TEMP2 TABLE(FleetID BIGINT
					 ,AccountID BIGINT
					 ,AccountNum VARCHAR(32)
					 ,AccountDescription VARCHAR(40)
					 ,ScheduledMonth INT
					 ,ScheduledYear INT
					 ,ExpenseAMT NUMERIC(17,2)
 )  
 
 INSERT INTO @TEMP2                
SELECT * FROM 
(
 SELECT DISTINCT
    A1.FleetID,
    A1.AccountID,
    A1.AccountNum,
    A1.AccountDescription,
    A1.ScheduledMonth,
    A1.ScheduledYear,
    SUM(ExpenseAMT) OVER (PARTITION BY AccountNum,FleetID,ScheduledMonth) ExpenseAMT    
    FROM
     (SELECT DISTINCT
           F.FleetID,
		   AC.AccountID,    
		   AC.AccountNum,
		   AC.AccountDescription,
		   ScheduledMonth = MONTH(PE.PurchaseDT),
		   ScheduledYear = YEAR(PE.PurchaseDT),
		   ExpenseAMT = CASE WHEN C.FederalTax=AC.AccountNum THEN PE.FederalTAX 
							 WHEN C.SaleTax=AC.AccountNum THEN PE.SaleTAX 
							 WHEN C.StateTax=AC.AccountNum THEN PE.SateTAX
				 			 ELSE ISNULL(PE.ExpenseAMT,0) END, LogNum,SlipNUM    
	  FROM PostflightMain PM
		   JOIN PostflightLeg PL ON PM.POLogID = PL.POLogID AND PL.IsDeleted = 0
		   JOIN  PostflightExpense PE ON  PL.POLogID = PE.POLogID AND PE.POLegID=PL.POLegID AND PE.IsDeleted = 0
		   JOIN Account AC ON PE.AccountID = AC.AccountID
		   JOIN Fleet F ON F.FleetID = PE.FleetID
		   JOIN @TempFleetID TF ON TF.FleetID=F.FleetID 
		   LEFT JOIN Crew CR ON PE.CrewID = CR.CrewID
		   LEFT JOIN CrewGroupOrder CO ON CO.CrewID = CR.CrewID
		   LEFT JOIN CrewGroup CG ON CO.CrewGroupID = CG.CrewGroupID
		   LEFT JOIN @TempCrewID TC ON TC.CrewID = CR.CrewID
		   LEFT JOIN Company C ON PE.HomebaseID = C.HomebaseID
		   LEFT OUTER JOIN Airport AP ON AP.AirportID=PE.AirportID
		   --LEFT JOIN Airport AP ON C.HomebaseAirportID = AP.AirportID
		   LEFT JOIN Airport AD ON PL.DepartICAOID = AD.AirportID        
		   LEFT JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID 
		   LEFT OUTER  JOIN UserMaster UM ON AC.HomebaseID=UM.HomebaseID
		  -- LEFT OUTER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
	   WHERE CONVERT(DATE,PE.PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND CONVERT(DATE,@ToDate) 
			 AND (F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM ='')
			 AND (CR.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD ='')
			 AND (CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) OR @CrewGroupCD ='')
			 AND (AP.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@IcaoID, ',')) OR @IcaoID ='')
			 AND (PM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum ='')
			 AND (PE.HomebaseID IN (CONVERT(BIGINT,@UserHomebaseID)) OR @IsHomebase = 0)
			 AND PE.CustomerID = @UserCustomerID AND PM.IsDeleted = 0
			 )A1
			 ) T1

-------------------------------------------------------------------------------------------------------------- 
INSERT INTO @TEMP2
SELECT TT.FLEETID,
       AccountID,
       AccountNum,
       AccountDescription,
       MonthNo, 
       Year,
       0 AS ExpenseAMT FROM
(
SELECT DISTINCT 
       0 AS FLEETID,
       AC.AccountID,
       AC.AccountNum,
       AC.AccountDescription,
       DT.MonthNo, 
       DT.Year,
       0 AS ExpenseAMT
FROM Account AC INNER JOIN @AccountNum AN ON AC.AccountNum = AN.AccountNum
        INNER JOIN @DateTable DT ON AC.AccountID = DT.AccountID
        INNER JOIN Company C ON AC.HomebaseID=C.HomebaseID
        INNER  JOIN Airport A ON C.HomebaseAirportID=A.AirportID
        WHERE  C.CustomerID = @UserCustomerID
           AND AC.AccountNum NOT IN(SELECT DISTINCT AccountNum FROM @TEMP2)
            )TEMP
CROSS JOIN (SELECT DISTINCT  F.FleetID,F.TailNum FROM  PostflightMain PM
                             INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID
                             INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID
                             INNER JOIN Fleet F ON PE.FleetID=F.FleetID 
                             INNER JOIN @TempFleetID TI ON F.FleetID = TI.FleetID 
                      WHERE PM.CustomerID=@UserCustomerID AND F.IsDeleted = 0 
                      AND CONVERT(DATE,PE.PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND CONVERT(DATE,@ToDate) 
                   )TT
 
 
       
--------------------------------------------------------------------------------------------------------------
 
SELECT DISTINCT * INTO #TEMP2 FROM @TEMP2

UPDATE #TEMP2 SET ExpenseAMT =ActualVal.FederalTAX,FleetID = Fleet ,ScheduledMonth= ACTUALVAL.ScheduledMonth,ScheduledYear = ActualVal.ScheduledYear FROM 
(
SELECT SUM(Actual.FederalTAX)FederalTAX,Actual.FederalTaxAc,TailNum,FleetID Fleet,ScheduledMonth,ScheduledYear FROM
(

SELECT DISTINCT SlipNUM,AC.AccountNum FederalTaxAc,PE.FederalTAX,PM.LogNum,F.TailNum,F.FleetID,ScheduledMonth = MONTH(PE.PurchaseDT),
		        ScheduledYear = YEAR(PE.PurchaseDT) FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=1) AC ON AC.AccountNum=C.FederalTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PE.CustomerID = @UserCustomerID AND PM.IsDeleted = 0
        )Actual  
GROUP BY Actual.FederalTaxAc,TailNum,FleetID,ScheduledMonth,ScheduledYear 
)ActualVal
WHERE ActualVal.FederalTaxAc=AccountNum AND ActualVal.Fleet=#TEMP2.FleetID AND ActualVal.ScheduledMonth=#TEMP2.ScheduledMonth

UPDATE #TEMP2 SET ExpenseAMT=ActualVal.SaleTAX,FleetID = Fleet,ScheduledMonth= ACTUALVAL.ScheduledMonth,ScheduledYear = ActualVal.ScheduledYear  FROM 
(
SELECT SUM(Actual.SaleTAX)SaleTAX,Actual.SalesTaxAc,TailNum,FleetID Fleet,ScheduledMonth,ScheduledYear FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SalesTaxAc,PE.SaleTAX SaleTAX,PM.LogNum,F.TailNum,F.FleetID, ScheduledMonth = MONTH(PE.PurchaseDT),
		        ScheduledYear = YEAR(PE.PurchaseDT) FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=2) AC ON AC.AccountNum=C.SaleTax
WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SalesTaxAc,TailNum,FleetID,ScheduledMonth,ScheduledYear
)ActualVal
WHERE ActualVal.SalesTaxAc=AccountNum AND ActualVal.Fleet=#TEMP2.FleetID  AND ActualVal.ScheduledMonth=#TEMP2.ScheduledMonth

UPDATE #TEMP2 SET ExpenseAMT=ActualVal.SateTax,FleetID = Fleet,ScheduledMonth= ACTUALVAL.ScheduledMonth,ScheduledYear = ActualVal.ScheduledYear  FROM 
(
SELECT SUM(Actual.SateTAX)SateTax,Actual.SateTaxAc,TailNum,FleetID Fleet,ScheduledMonth,ScheduledYear  FROM
(
SELECT DISTINCT SlipNUM,AC.AccountNum SateTaxAc,PE.SateTAX SateTAX,PM.LogNum,F.TailNum, F.FleetID,ScheduledMonth = MONTH(PE.PurchaseDT),
		        ScheduledYear = YEAR(PE.PurchaseDT)  FROM PostflightMain PM
                                INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID AND PL.IsDeleted = 0
                                INNER JOIN PostflightExpense PE ON PE.POLogID=PL.POLogID AND PE.IsDeleted = 0
                                INNER JOIN Fleet F ON PE.FleetID=F.FleetID
                                INNER JOIN Company C ON PM.CustomerID=C.CustomerID
                                INNER JOIN (SELECT * FROM @AccountNum WHERE RowID=3) AC ON AC.AccountNum=C.StateTax
            WHERE (CONVERT(DATE,PurchaseDT) BETWEEN CONVERT(DATE,@FromDate) AND  CONVERT(DATE,@ToDate)) AND PM.IsDeleted =0 AND PE.CustomerID = @UserCustomerID
        )Actual  
GROUP BY Actual.SateTaxAc,TailNum,FleetID,ScheduledMonth,ScheduledYear      
)ActualVal
WHERE ActualVal.SateTaxAc=AccountNum AND ActualVal.Fleet=#TEMP2.FleetID   AND ActualVal.ScheduledMonth=#TEMP2.ScheduledMonth


--------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO #TEMP2 (
       FleetID,
       AccountID,    
       AccountNum,
       AccountDescription,
       ScheduledMonth,
       ScheduledYear,
       ExpenseAMT
)
( SELECT
DISTINCT
       T2.FleetID,
       T2.AccountID,    
       T2.AccountNum,
       T2.AccountDescription,
       DT.MonthNo ScheduledMonth,
       DT.Year ScheduledYear,
       0
FROM #TEMP2 T2 CROSS JOIN @DateTable DT --ON T2.AccountID = DT.AccountID
 WHERE NOT EXISTS (SELECT  T3.AccountNum FROM  #TEMP2 T3 WHERE T2.AccountID=T3.AccountID AND DT.MonthNo =T3.ScheduledMonth))

SELECT DISTINCT
       AccountID,    
       AccountNum,
       AccountDescription,
       ScheduledMonth,
       ScheduledYear,
       ExpenseAMT,
       'TRUE' AS lmon,
       DENSE_RANK () OVER (PARTITION BY AccountNum ORDER BY ScheduledYear,ScheduledMonth) AS RNK
     FROM #TEMP2 
     ORDER BY ScheduledYear,ScheduledMonth 
 

IF OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL 
DROP TABLE #TEMP


IF OBJECT_ID('TEMPDB..#TEMP2') IS NOT NULL 
DROP TABLE #TEMP2

END





GO


