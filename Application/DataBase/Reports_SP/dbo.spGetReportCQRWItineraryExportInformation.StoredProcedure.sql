IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQRWItineraryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQRWItineraryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportCQRWItineraryExportInformation]
  @UserCustomerID     AS VARCHAR(30)=''   --10112    
 ,@UserHomebaseID    AS VARCHAR(30)=''     
 ,@FileNumber           AS VARCHAR(30)  = '' --  23931    CQMain.FileNUM    
 ,@QuoteNUM          AS VARCHAR(100) = '' --      CQMain.QuoteNUM     
 ,@LegNUM            AS VARCHAR(100) = '' --      CQLeg.LegNUM       
 ,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal    
 ,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal     
 ,@TailNum           AS VARCHAR(30) = '' --     Single Selection, Fleet.TailNUM (Use FleetID of CQMain)    
 ,@AircraftCD        AS VARCHAR(30) = '' --     TypeCD, Single Selection     
 ,@VendorID   AS VARCHAR(30) = '' --     VendorCD, Single Selection    
 ,@CQCustomerID        AS VARCHAR(30) = '' --     CustomerCD, Single Selection    
 ,@HomeBaseCD        AS VARCHAR(30) = '' --     HomeBaseCD, Single Selection    
 ,@SalesPersonID     AS VARCHAR(30) = '' --     SalesPersonCD, Single Selection 
 ,@ReportHeaderID VARCHAR(30)=''--'' --101122 TemplateID from TripSheetReportHeader table
,@ReportNumber SMALLINT=1-- 1 = 'OVERVIEW', 2 = 'TRIPSHEET' etc
,@TempSettings VARCHAR(MAX) = '' --'FBOS::0||DISPATCHNO::1||PURPOSE::0'      
/*    
REPORT CRITERIA    
 Begin Date CQFile.estdept > = [Begin Date] AND [End Date] < = CQFile.estdept    
 End Date     
 Tail Number CQMain.Tail_Nmbr    
 Vendor CQMain.Vendcode    
 Home Base Cqfile.Homebase    
 Type Code CQMain.Type_Code    
 Customer CQFIle.Custcode    
 Sales Person CQFile.saleperid    
*/    
AS  
-- =============================================
-- SPC Name:spGetReportCQRWItineraryExportInformation
-- Author: Askar
-- Create date:08th Nov 2013
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN   
 
DECLARE @Itinerary TABLE(lowdate DATE,
						 highdate DATE,
						 tail_nmbr VARCHAR(9),
						 type_code VARCHAR(10),
						 [desc] VARCHAR(40),
						 custname VARCHAR(40),
						 contname VARCHAR(40),
						 contphone VARCHAR(25),
						 contfax VARCHAR(25),
						 cqfilenum INT,
						 quotenum INT,
						 quoteid INT,
						 leg_num BIGINT,
						 legid BIGINT,
						 depnamecitystate VARCHAR(25),
						 arrnamecitystate VARCHAR(25),
						 depzulu NUMERIC(6,2),
						 arrzulu NUMERIC(6,2),
						 locdep DATETIME,
						 locarr DATETIME,
						 gmtdep DATETIME,
						 gmtarr DATETIME,
						 distance NUMERIC(5,0),
						 depicao_id CHAR(4),
						 arricao_id CHAR(4),
						 elp_time NUMERIC(7,1),
						 paxname VARCHAR(60),
						 phone VARCHAR(25),
						 c_elp_time NUMERIC(7,3),
						 pax_total INT,
						 hotel_name VARCHAR(60),
						 hotel_phone VARCHAR(25),
						 cater_name VARCHAR(60),
						 cater_phone VARCHAR(25),
						 trans_name VARCHAR(60),
						 trans_phone VARCHAR(25),
						 code VARCHAR(10),
						 arrfbo_desc VARCHAR(60),
						 arrfbo_phone VARCHAR(25),
						 arrfbo_addr1 VARCHAR(25),
						 arrfbo_addr2 VARCHAR(25),
						 nbr INT,
						 paxcode VARCHAR(5),
						 nonpax  VARCHAR(10),
						 blocked VARCHAR(10),
						 blocksort BIT,
						 passname VARCHAR(80),
						 fltpurpose VARCHAR(100),
						 PassID BIGINT,
						 FBOID BIGINT,
						 HotelListID BIGINT,
						 TransportID BIGINT,
						 CateringID BIGINT,
						 Rnk INT,
						 --MHTML Fields
						 LMessage VARCHAR(MAX),
						 hotel_remarks VARCHAR(MAX),
						 cater_remarks VARCHAR(MAX),
						 trans_remarks VARCHAR(MAX),
						 PaxNotes VARCHAR(MAX),
						 crewNotes VARCHAR(MAX),
						 arrfbo_remarks VARCHAR(MAX),
						 passengers VARCHAR(MAX))
 
DECLARE @TSFormatSettings TABLE(LMESSAGE BIT,
									MESSAGEPOS SMALLINT,
									[MESSAGE] VARCHAR(MAX),
									ONELEG BIT,
									LEGNOTES SMALLINT,
									ETEHRS SMALLINT,
									PONEPERLIN BIT,
									FBO BIT,
									FBOPHONE BIT,
									FBOADDR BIT,
									FBOCOMM BIT,
									CATERING BIT,
									CATERPHONE BIT,
									CATERCOMM BIT,
									HOTEL BIT,
									HOTELPHONE BIT,
									HOTELCOMM BIT,
									TRANSPORT BIT,
									TRANSPHONE BIT,
									TRANSCOMM BIT,
									ATTENTO BIT,
									CNTCTPHONE BIT,
									CNTCTFAX BIT,
									LHOUR SMALLINT,
									ICAO BIT,
									PRINTLOGO BIT)
 
INSERT INTO @TSFormatSettings
EXEC spGetReportCQRWFormatSettings @ReportHeaderID ,1,@TempSettings 
DECLARE @FBO BIT,@HOTEL BIT,@TRANSPORT BIT,@CATERING BIT,@LEGNOTES SMALLINT,@PONEPERLIN BIT

SELECT @FBO=FBO,@HOTEL=HOTEL,@TRANSPORT=TRANSPORT,@CATERING=CATERING,@LEGNOTES=LEGNOTES,@PONEPERLIN=PONEPERLIN FROM @TSFormatSettings



DECLARE @BlockPax TABLE(PaxNo INT)
DECLARE @Count INT=1,@LastCount INT=999

WHILE @Count <=@LastCount
BEGIN

INSERT INTO @BlockPax
SELECT @Count

SET @Count=@Count+1

END

DECLARE @Pax TABLE(LegID BIGINT,
				   PaxNo INT)


IF @FileNumber <> ''     
BEGIN 
  INSERT INTO @Itinerary
  SELECT lowdate=CONVERT(DATE,CL.DepartureDTTMLocal),
         highdate=CONVERT(DATE,CL.ArrivalDTTMLocal),
         tail_nmbr=F.TailNum,
         type_code=AC.AircraftCD,
         [desc]=CQFileDescription,
         custname=QF.CQCustomerName,
         contname=QF.CQCustomerContactName,
         contphone=QF.PhoneNUM,
         contfax=QF.FaxNUM,
         cqfilenum=QF.FileNUM,
         quotenum=QM.QuoteNUM,
         quoteid=QM.QuoteID,
         leg_num=QL.LegNUM,
         legid=QL.CQLegID,
         depnamecitystate=D.CityName,
         arrnamecitystate=A.CityName,
         depzulu= CASE WHEN D.IsDayLightSaving = 1 AND QL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT THEN D.OffsetToGMT + 1 ELSE D.OffsetToGMT END,
         arrzulu=CASE WHEN A.IsDayLightSaving = 1 AND QL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT THEN A.OffsetToGMT + 1 ELSE A.OffsetToGMT END,
         locdep=QL.DepartureDTTMLocal,
         locarr=QL.ArrivalDTTMLocal,
         gmtdep=QL.DepartureGreenwichDTTM,
         gmtarr=QL.ArrivalGreenwichDTTM,
         distance=QL.Distance,
         depicao_id=D.IcaoID,
         arricao_id=A.IcaoID,
         elp_time=FLOOR(QL.ElapseTM)*10*0.1,
         paxname=NULL,
         phone=NULL,
         c_elp_time=QL.ElapseTM,
         pax_total=QL.PassengerTotal,
         hotel_name=H.CQHotelListDescription,
         hotel_phone=H.PhoneNUM,
         cater_name=CA.CQCateringListDescription,
         cater_phone=CA.PhoneNUM,
         trans_name=T.CQTransportListDescription,
         trans_phone=T.PhoneNUM,
         code=NULL,
         arrfbo_desc=CF.FBOVendor,
         arrfbo_phone=CF.PhoneNUM1,
         arrfbo_addr1=CF.Addr1,
         arrfbo_addr2=CF.Addr2,
         nbr=1,
         paxcode=P.PassengerRequestorCD,
         nonpax='FALSE',
         blocked='FALSE',
         blocksort=0,
         passname=ISNULL(P.LastName,'')+CASE WHEN ISNULL(P.FirstName,'')<>'' THEN ', '+P.FirstName ELSE '' END+' '+ISNULL(P.MiddleInitial,'') ,
         fltpurpos=QL.FlightPurposeDescription ,
         P.PassengerRequestorID,
         CF.FBOID,
         H.CQHotelListID,
         T.CQTransportListID,
         CA.CQCateringListID,
         ROW_NUMBER()OVER(PARTITION BY QL.LegID ORDER BY QL.LegID,OrderNum),  
         --MHTML Fields--
		 LMessage = ''
		,hotel_remarks = H.Remarks 
		,cater_remarks = CA.Remarks
		,trans_remarks = T.Remarks
		,PaxNotes = P.Notes
		,crewNotes = ''
		,arrfbo_remarks = CF.Remarks 
		,passengers = ''
     
  FROM CQFile QF     
  INNER JOIN CQMain QM ON QF.CQFileID = QM.CQFileID AND QM.IsDeleted=0    
  INNER JOIN CQLeg QL ON QM.CQMainID = QL.CQMainID  AND QL.IsDeleted=0 
  INNER JOIN(SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID GROUP BY CQMainID) CL ON CL.CQMainID=QL.CQMainID AND QL.IsDeleted=0
  LEFT OUTER JOIN CQPassenger CP ON CP.CQLegID=QL.CQLegID
  LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=CP.PassengerRequestorID
  LEFT OUTER JOIN Airport D ON QL.DAirportID=D.AirportID
  LEFT OUTER JOIN Airport A ON A.AirportID=QL.AAirportID
  LEFT OUTER JOIN DSTRegion DDST ON D.DSTRegionID = DDST.DSTRegionID
  LEFT OUTER JOIN DSTRegion ADST ON A.DSTRegionID = ADST.DSTRegionID
  LEFT OUTER JOIN Company C ON QF.CustomerID = C.CustomerID AND QF.HomebaseID = C.HomebaseID  
  LEFT OUTER JOIN (SELECT FleetID, TailNum FROM Fleet ) F ON QM.FleetID = F.FleetID    
  LEFT OUTER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft ) AC ON QM.AircraftID = AC.AircraftID     
  LEFT OUTER JOIN (
				SELECT PH.CQLegID, PH.CQHotelListDescription, PH.PhoneNUM, H.Remarks,PH.CQHotelListID
				FROM  CQHotelList PH 
				INNER JOIN Hotel H ON PH.HotelID = H.HotelID
				)H ON QL.CQLegID = H.CQLegID --Pax Hotel 
  LEFT OUTER JOIN (
				SELECT CCL.CQLegID, CCL.AirportID, CCL.CQCateringListDescription, CCL.PhoneNUM, C.Remarks,CCL.CQCateringListID
				FROM CQCateringList CCL 
				INNER JOIN Catering C ON CCL.CateringID = C.CateringID
				)CA ON QL.CQLegID = CA.CQLegID AND QL.DAirportID = CA.AirportID
  LEFT OUTER JOIN (
				SELECT CT.CQLegID, CT.AirportID, CT.CQTransportListDescription, CT.PhoneNUM, T.Remarks,CT.CQTransportListID
				FROM CQTransportList CT 				
				INNER JOIN Transport T ON CT.TransportID = T.TransportID And CT.RecordType = 'TP'
				) T ON QL.CQLegID = T.CQLegID AND QL.AAirportID = T.AirportID  -- ARRIVAL Transport
  LEFT OUTER JOIN (
				SELECT CFL.FBOID,CFL.CQLegID, CFL.AirportID, CF.FBOVendor, CF.Addr1, CF.Addr2, CF.Addr3, CF.PhoneNUM1, CF.Remarks 
				FROM CQFBOList CFL
				INNER JOIN FBO CF ON CFL.FBOID = CF.FBOID 
				)CF ON QL.CQLegID = CF.CQLegID AND QL.AAirportID = CF.AirportID 
     
  WHERE QF.CustomerID = CONVERT(BIGINT,@UserCustomerID)    
   AND QF.FileNUM = CONVERT(INT,@FileNumber)    
   AND (QM.QuoteNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@QuoteNUM, ',')) OR @QuoteNUM = '')    
   AND (QL.LegNUM  IN (SELECT DISTINCT CONVERT(INT,RTRIM(S)) FROM dbo.SplitString(@LegNUM, ',')) OR @LegNUM = '')     
END
ELSE 
BEGIN
  INSERT INTO @Itinerary
  SELECT lowdate=CONVERT(DATE,CL.DepartureDTTMLocal),
         highdate=CONVERT(DATE,CL.ArrivalDTTMLocal),
         tail_nmbr=F.TailNum,
         type_code=AC.AircraftCD,
         [desc]=CQFileDescription,
         custname=QF.CQCustomerName,
         contname=QF.CQCustomerContactName,
         contphone=QF.PhoneNUM,
         contfax=QF.FaxNUM,
         cqfilenum=QF.FileNUM,
         quotenum=QM.QuoteNUM,
         quoteid=QM.QuoteID,
         leg_num=QL.LegNUM,
         legid=QL.CQLegID,
         depnamecitystate=D.CityName,
         arrnamecitystate=A.CityName,
         depzulu= CASE WHEN D.IsDayLightSaving = 1 AND QL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT THEN D.OffsetToGMT + 1 ELSE D.OffsetToGMT END,
         arrzulu=CASE WHEN A.IsDayLightSaving = 1 AND QL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT THEN A.OffsetToGMT + 1 ELSE A.OffsetToGMT END,
         locdep=QL.DepartureDTTMLocal,
         locarr=QL.ArrivalDTTMLocal,
         gmtdep=QL.DepartureGreenwichDTTM,
         gmtarr=QL.ArrivalGreenwichDTTM,
         distance=QL.Distance,
         depicao_id=D.IcaoID,
         arricao_id=A.IcaoID,
         elp_time=FLOOR(QL.ElapseTM)*10*0.1,
         paxname=NULL,
         phone=NULL,
         c_elp_time=QL.ElapseTM,
         pax_total=QL.PassengerTotal,
         hotel_name=H.CQHotelListDescription,
         hotel_phone=H.PhoneNUM,
         cater_name=CA.CQCateringListDescription,
         cater_phone=CA.PhoneNUM,
         trans_name=T.CQTransportListDescription,
         trans_phone=T.PhoneNUM,
         code=NULL,
         arrfbo_desc=CF.FBOVendor,
         arrfbo_phone=CF.PhoneNUM1,
         arrfbo_addr1=CF.Addr1,
         arrfbo_addr2=CF.Addr2,
         nbr=1,
         paxcode=P.PassengerRequestorCD,
         nonpax='FALSE',
         blocked='TRUE',
         blocksort=0,
         passname=ISNULL(P.LastName,'')+CASE WHEN ISNULL(P.FirstName,'')<>'' THEN ', '+P.FirstName ELSE '' END+' '+ISNULL(P.MiddleInitial,'') ,
         fltpurpos=QL.FlightPurposeDescription ,
         P.PassengerRequestorID,
         CF.FBOID,
         H.CQHotelListID,
         T.CQTransportListID,
         CA.CQCateringListID,
         ROW_NUMBER()OVER(PARTITION BY QL.LegID ORDER BY QL.LegID,OrderNum),
                  --MHTML Fields--
		 LMessage = ''
		,hotel_remarks = H.Remarks 
		,cater_remarks = CA.Remarks
		,trans_remarks = T.Remarks
		,PaxNotes = P.Notes
		,crewNotes = ''
		,arrfbo_remarks = CF.Remarks 
		,passengers = ''         
     
  FROM CQFile QF     
  INNER JOIN CQMain QM ON QF.CQFileID = QM.CQFileID AND QM.IsDeleted=0    
  INNER JOIN CQLeg QL ON QM.CQMainID = QL.CQMainID  AND QL.IsDeleted=0 
  INNER JOIN(SELECT MIN(DepartureDTTMLocal) DepartureDTTMLocal,MAX(ArrivalDTTMLocal) ArrivalDTTMLocal,CQMainID FROM CQLeg WHERE CustomerID=@UserCustomerID GROUP BY CQMainID) CL ON CL.CQMainID=QL.CQMainID AND QL.IsDeleted=0
  LEFT OUTER JOIN CQPassenger CP ON CP.CQLegID=QL.CQLegID
  LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID=CP.PassengerRequestorID
  LEFT OUTER JOIN Airport D ON QL.DAirportID=D.AirportID
  LEFT OUTER JOIN Airport A ON A.AirportID=QL.AAirportID
  LEFT OUTER JOIN DSTRegion DDST ON D.DSTRegionID = DDST.DSTRegionID
  LEFT OUTER JOIN DSTRegion ADST ON A.DSTRegionID = ADST.DSTRegionID
  LEFT OUTER JOIN Company C ON QF.CustomerID = C.CustomerID AND QF.HomebaseID = C.HomebaseID  
  LEFT OUTER JOIN (SELECT FleetID, TailNum FROM Fleet ) F ON QM.FleetID = F.FleetID    
  LEFT OUTER JOIN (SELECT AircraftID, AircraftCD FROM Aircraft ) AC ON QM.AircraftID = AC.AircraftID     
  LEFT OUTER JOIN (
				SELECT PH.CQLegID, PH.CQHotelListDescription, PH.PhoneNUM, H.Remarks,PH.CQHotelListID
				FROM  CQHotelList PH 
				INNER JOIN Hotel H ON PH.HotelID = H.HotelID
				)H ON QL.CQLegID = H.CQLegID --Pax Hotel 
  LEFT OUTER JOIN (
				SELECT CCL.CQLegID, CCL.AirportID, CCL.CQCateringListDescription, CCL.PhoneNUM, C.Remarks,CCL.CQCateringListID
				FROM CQCateringList CCL 
				INNER JOIN Catering C ON CCL.CateringID = C.CateringID
				)CA ON QL.CQLegID = CA.CQLegID AND QL.DAirportID = CA.AirportID
  LEFT OUTER JOIN (
				SELECT CT.CQLegID, CT.AirportID, CT.CQTransportListDescription, CT.PhoneNUM, T.Remarks,CT.CQTransportListID
				FROM CQTransportList CT 				
				INNER JOIN Transport T ON CT.TransportID = T.TransportID And CT.RecordType = 'TP'
				) T ON QL.CQLegID = T.CQLegID AND QL.AAirportID = T.AirportID  -- ARRIVAL Transport
  LEFT OUTER JOIN (
				SELECT CFL.FBOID,CFL.CQLegID, CFL.AirportID, CF.FBOVendor, CF.Addr1, CF.Addr2, CF.Addr3, CF.PhoneNUM1, CF.Remarks 
				FROM CQFBOList CFL
				INNER JOIN FBO CF ON CFL.FBOID = CF.FBOID 
				)CF ON QL.CQLegID = CF.CQLegID AND QL.AAirportID = CF.AirportID 
     
  WHERE QF.CustomerID = CONVERT(BIGINT,@UserCustomerID)    
   AND CONVERT(DATE, QF.EstDepartureDT) BETWEEN CONVERT(DATE, @BeginDate) AND CONVERT(DATE, @EndDate)           
   AND ( F.TailNum = @TailNum OR @TailNum = '' )    
   AND ( AC.AircraftCD = @AircraftCD OR @AircraftCD = '' )    
   AND ( QM.VendorID = CONVERT(BIGINT,@VendorID) OR @VendorID = '' )    
   AND ( QF.CQCustomerID = CONVERT(BIGINT,@CQCustomerID) OR @CQCustomerID = '' )    
   AND ( QM.HomebaseID = CONVERT(BIGINT,@HomeBaseCD) OR @HomeBaseCD = '' )    
   AND ( QF.SalesPersonID = CONVERT(BIGINT,@SalesPersonID) OR @SalesPersonID = '' ) 
   ORDER BY QF.FileNUM,QL.LegID  
END


INSERT INTO @Pax
SELECT DISTINCT legid,BP.PaxNo FROM @Itinerary CROSS JOIN @BlockPax BP
     
  SELECT flag='B',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 filedesc=[desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose, 
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
			FROM @Itinerary
			WHERE FBOID IS NOT NULL 
			AND @FBO=1
			AND Rnk=1

UNION ALL

  SELECT flag='C',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose, 
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
		 FROM @Itinerary
			WHERE HotelListID IS NOT NULL 
			AND @HOTEL=1
			AND Rnk=1

UNION ALL

  SELECT flag='D',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose,
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 		 
		 FROM @Itinerary
			WHERE TransportID IS NOT NULL 
			AND @TRANSPORT=1
			AND Rnk=1
UNION ALL

  SELECT flag='G',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose, 
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
		 FROM @Itinerary 
					WHERE Rnk=1 
					AND @CATERING=1
					AND CateringID IS NOT NULL

UNION ALL

  SELECT flag='H',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose, 
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
		 FROM @Itinerary 
					WHERE Rnk=1
					AND @LEGNOTES=1


UNION ALL

  SELECT flag='I',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=(CASE WHEN PassID IS NOT NULL THEN 1 ELSE 0 END),
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose,
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
		  FROM @Itinerary 
					WHERE Rnk=1
UNION ALL


SELECT flag='J',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr,
		 paxcode,
		 nonpax=(CASE WHEN PassID IS NOT NULL THEN 'FALSE' ELSE '' END),
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose,
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers  FROM(
  SELECT flag='J',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr=ROW_NUMBER()OVER(PARTITION BY LegID ORDER BY LegID),
		 paxcode,
		 nonpax,
		 blocked,
		 blocksort,
		 passname,
		 fltpurpose,PassID,
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
		  FROM @Itinerary
					WHERE @PONEPERLIN=1
UNION ALL

SELECT flag='J',
		 lowdate ,
		 highdate ,
		 tail_nmbr,
		 type_code,
		 [desc],
		 custname,
		 contname,
		 contphone,
		 contfax,
		 cqfilenum,
		 quotenum,
		 quoteid,
		 leg_num,
		 TT.legid,
		 depnamecitystate,
		 arrnamecitystate,
		 depzulu=FLOOR(depzulu),
		 arrzulu=FLOOR(arrzulu),
		 locdep,
		 locarr,
		 gmtdep,
		 gmtarr,
		 distance,
		 depicao_id,
		 arricao_id,
		 elp_time,
		 paxname,
		 phone,
		 c_elp_time,
		 pax_total,
		 hotel_name,
		 hotel_phone,
		 cater_name,
		 cater_phone,
		 trans_name,
		 trans_phone,
		 code,
		 arrfbo_desc,
		 arrfbo_phone,
		 arrfbo_addr1,
		 arrfbo_addr2,
		 nbr,
		 paxcode,
		 nonpax,
		 blocked='TRUE',
		 blocksort=1,
		 'BLOCKED' passname,
		 fltpurpose,9999 PassID,
		LMessage, 
		hotel_remarks, 
		cater_remarks, 
		trans_remarks ,
		PaxNotes ,
		crewNotes ,
		arrfbo_remarks ,
		passengers 
         FROM @Itinerary TT INNER JOIN @Pax CD ON CD.LegID=TT.legid
							INNER JOIN (SELECT COUNT(1) PaxCount,CQLegID FROM  CQPassenger WHERE CustomerID=@UserCustomerID GROUP BY CQLegID)PX ON PX.CQLegID=TT.legid  
         WHERE CD.PaxNo <=(TT.pax_total-PX.PaxCount)
           AND TT.Rnk=1
           AND @PONEPERLIN=1
)Temp
     

ORDER BY cqfilenum,quotenum,leg_num,flag
	

END    
        
  --  exec  spGetReportCQRWItineraryExportInformation '10112','0', '23057', ''     
     
     
    
    
    
    
    
    
    



GO


