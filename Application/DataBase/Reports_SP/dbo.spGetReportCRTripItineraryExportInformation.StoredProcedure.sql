IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCRTripItineraryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCRTripItineraryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportCRTripItineraryExportInformation] 
( 
    @UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30), --Mandatory
	@CRTripNUM AS VARCHAR(500),
	@CRLegNum AS VARCHAR(500)='',
	@PrintOneLegPerPage BIT = 0,
	@PrintLegNotes BIT = 0
	,@FilterFerryLegs BIT=0
)
AS
-- =============================================
-- SPC Name:spGetReportCRTripItineraryExportInformation
-- Author: Askar
-- Create date:
-- Description: 
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN
SET NOCOUNT ON

DECLARE @CRTrip BIGINT;

SELECT DISTINCT @CRTrip=CRM.TripID FROM CRMain CRM INNER JOIN PreflightMain PM ON PM.TripID = CRM.TripID
                            WHERE CRM.CustomerID=@UserCustomerID
                              AND CRM.CRTripNUM=@CRTripNUM

DECLARE @TripFlag TABLE(Flag CHAR(1))

INSERT INTO @TripFlag
 SELECT 'B'UNION ALL SELECT 'I' UNION ALL SELECT 'J'
IF @PrintLegNotes=1 BEGIN INSERT INTO @TripFlag VALUES('H')  END


IF @CRTrip IS NOT NULL
BEGIN	

;With TripInfo AS(
SELECT flag=TF.Flag
	  ,TEMP.* FROM (
SELECT DISTINCT lowdate=PLL.DepartureDTTMLocal
			   ,highdate=PLL.ArrivalDTTMLocal
			   ,orig_nmbr=CRM.CRTripNUM
			   ,trip_nmbr=PM.TripNUM
			   ,tail_nmbr=F.TailNum
			   ,type_code=AC.AircraftCD
			   ,cr_stat=CRM.CorporateRequestStatus
			   ,CRdesc=CRMainDescription
			   ,reqcode=P.PassengerRequestorCD
			   ,paxname=P.PassengerName
			   ,phone=P.PhoneNum
			   ,leg_num=PL.LegNUM
			   ,legid=PL.LegID
			   ,timeapprox=CASE WHEN PL.IsApproxTM=1 THEN 'TRUE' ELSE 'FALSE' END
			   ,depicao_id=AD.IcaoID
			   ,depnamecitystate=AD.AirportName+(CASE WHEN ISNULL(AD.CityName,'')<>'' THEN ', '+AD.CityName ELSE '' END)+(CASE WHEN ISNULL(AD.StateName,'')<>'' THEN ', '+AD.StateName ELSE '' END)
			   ,arricao_id=AA.IcaoID
			   ,arrnamecitystate=AA.AirportName+(CASE WHEN ISNULL(AA.CityName,'')<>'' THEN ', '+AA.CityName ELSE '' END)+(CASE WHEN ISNULL(AA.StateName,'')<>'' THEN ', '+AA.StateName ELSE '' END)
			   ,pax_total=PL.PassengerTotal
			   ,depzulu=CASE WHEN AD.IsDayLightSaving = 1 AND PL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT THEN FLOOR(AD.OffsetToGMT + 1) ELSE FLOOR(AD.OffsetToGMT) END
			   ,arrzulu =CASE WHEN AA.IsDayLightSaving = 1 AND PL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT THEN FLOOR(AA.OffsetToGMT + 1) ELSE FLOOR(AA.OffsetToGMT) END
			   ,locdep=PL.DepartureDTTMLocal
			   ,locarr=PL.ArrivalDTTMLocal
			   ,gmtdep=PL.DepartureGreenwichDTTM
			   ,gmtarr=PL.ArrivalGreenwichDTTM
			   ,distance=PL.Distance
			   ,elp_time=FLOOR(PL.ElapseTM*10)*0.1
			   ,c_elp_time=PL.ElapseTM
			   ,crcode=CRFD.FBOCD
			   ,paxnotes = PL.Notes
			   ,crewnotes = ''
			   ,itinerarymessage = PM.DispatchNotes
			   ,depfbo_desc=CRFD.FBOVendor
			   ,depfbo_phone=CRFD.PhoneNUM1
			   ,depfbo_remarks = CRFD.Remarks
			   ,arrfbo_desc=CRFA.FBOVendor
			   ,arrfbo_phone=CRFA.PhoneNUM1
			   ,arrfbo_remarks = CRFA.Remarks
			   ,hotel_name=CRHP.Name
			   ,hotel_phone=CRHP.PhoneNUM
			   ,trans_name=CRT.TransportationVendor
			   ,trans_phone=CRT.PhoneNUM
			   ,crewh_name=ISNULL(CRHCP.Name,CRHCS.Name)
			   ,crewh_phone=ISNULL(CRHCP.PhoneNUM,CRHCS.PhoneNUM)
			   ,crewh_addr1=ISNULL(CRHCP.Addr1,CRHCS.Addr1) 
			   ,crewh_addr2=ISNULL(CRHCP.Addr2,CRHCS.Addr2) 
			   ,cater_name=CTRNG.CateringVendor
			   ,cater_remarks = CTRNG.Remarks
			   ,passengers = PAX.PaxNames
			   ,pic=PIC.CrewCD
			   ,sic=SIC.CrewCD
			   ,addlcrew=SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList)-1)
			   ,ac_code=F.AircraftCD
			   ,ROW_NUMBER()OVER(PARTITION BY PM.TripID ORDER BY PM.TripID,PL.LegNum)Rnk
		FROM PreflightMain PM
		INNER JOIN CRMain CRM ON CRM.TripID=PM.TripID AND CRM.IsDeleted=0
		INNER JOIN PreflightLeg PL ON PL.TripID = PM.TripID AND PM.IsDeleted = 0 
		INNER JOIN (SELECT  MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,TripID FROM PreflightLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY TripID) PLL ON PLL.TripID = PL.TripID
		INNER JOIN Fleet F ON F.FleetID = PM.FleetID
		LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = PM.AircraftID
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = PM.PassengerRequestorID
		LEFT OUTER JOIN Airport AA ON AA.AirportID =PL.ArriveICAOID
		LEFT OUTER JOIN Airport AD ON AD.AirportID =PL.DepartICAOID
		LEFT OUTER JOIN DSTRegion DDST ON AD.DSTRegionID = DDST.DSTRegionID
		LEFT OUTER JOIN DSTRegion ADST ON AA.DSTRegionID = ADST.DSTRegionID
				--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1,PFL.PreflightFBOName FBOVendor, PFL.LegID, F.Remarks
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsArrivalFBO = 1 
				) CRFA ON CRFA.LegID = PL.LegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, PFL.PreflightFBOName FBOVendor, PFL.LegID, F.Remarks,F.FBOCD
					FROM PreflightFBOList PFL
					INNER JOIN FBO F ON PFL.FBOID = F.FBOID
					WHERE PFL.IsDepartureFBO = 1
				) CRFD ON CRFD.LegID = PL.LegID
				
		--HOTEL
		LEFT OUTER JOIN
				(SELECT PPH.PhoneNum1 PhoneNUM, PPH.PreflightHotelName Name, PPL.LegID,RANK()OVER(PARTITION BY PPL.LegID ORDER BY PPL.LegID,PPH.PreflightPassengerHotelListID)Rnk
					FROM PreflightPassengerList PPL 
			             INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
					--WHERE CH.RecordType = 'P' --PAX HOTEL
				) CRHP ON CRHP.LegID = PL.LegID AND CRHP.Rnk=1
		LEFT OUTER JOIN
				(SELECT PCH.PhoneNum1 PhoneNUM, PCH.PreflightHotelName Name, PCLL.LegID, PCH.Address1 Addr1, PCH.Address2 Addr2,
						RANK()OVER(PARTITION BY PCLL.LegID ORDER BY PCLL.LegID,PCH.PreflightHotelListID)Rnk
					FROM PreflightCrewList PCLL 
				        --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				        INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1
																					AND PCH.crewPassengerType = 'C'
						INNER JOIN PreflightHotelCrewPassengerList PHCP ON PCH.PreflightHotelListID = PHCP.PreflightHotelListID
																		AND PHCP.CrewID = PCLL.CrewID
					WHERE PCLL.DutyType IN ('P')--PIC CREW HOTEL
				) CRHCP ON PL.LegID = CRHCP.LegID AND CRHCP.Rnk=1
		LEFT OUTER JOIN
				(SELECT PCH.PhoneNum1 PhoneNUM, PCH.PreflightHotelName Name, PCLL.LegID, PCH.Address1 Addr1, PCH.Address2 Addr2,
						RANK()OVER(PARTITION BY PCLL.LegID ORDER BY PCLL.LegID,PCH.PreflightHotelListID)Rnk
					FROM PreflightCrewList PCLL 
				        --INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				        INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1
																					AND PCH.crewPassengerType = 'C'
						INNER JOIN PreflightHotelCrewPassengerList PHCP ON PCH.PreflightHotelListID = PHCP.PreflightHotelListID
																		AND PHCP.CrewID = PCLL.CrewID
					WHERE PCLL.DutyType IN ('S')--SIC CREW HOTEL
				) CRHCS ON PL.LegID = CRHCS.LegID AND CRHCS.Rnk=1
		--LEFT OUTER JOIN
		--		(SELECT PCH.PhoneNum1 PhoneNUM, PCH.PreflightHotelName Name, PCLL.LegID
		--			FROM  PreflightCrewList PCLL 
		--		          INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
		--			WHERE PCLL.DutyType NOT IN ('P', 'S') --ADDL CREW / MAINT HOTEL
		--		) CRHA ON PL.LegID = CRHA.LegID
						
		--TRANSPORT 
		LEFT OUTER JOIN 
				(SELECT PTL.LegID, PTL.PhoneNum1 PhoneNUM, PTL.PreflightTransportName TransportationVendor
					FROM PreflightTransportList PTL 
				LEFT OUTER JOIN Transport T ON PTL.TransportID = T.TransportID 
					WHERE PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
				)CRT ON CRT.LegID = PL.LegID
			
		--CATERING
		LEFT OUTER JOIN
				(SELECT PCL.ContactName CateringVendor,PCL.LegID,PCL.ContactPhone,C.Remarks
					FROM PreflightCateringDetail PCL 
				         LEFT OUTER JOIN Catering C ON PCL.CateringID = C.CateringID 
				     WHERE PCL.ArriveDepart = 'D'
				)CTRNG ON CTRNG.LegID = PL.LegID
		LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
		LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID)Rnk,C.FirstName+' '+C.LastName Crew,LastName,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='P' AND PCL.CustomerID=@UserCustomerID)PIC ON PIC.LegID=PL.LegID AND PIC.Rnk=1
        LEFT OUTER JOIN(SELECT RANK()OVER(PARTITION BY PCL.LegID ORDER BY PCL.LegID,PCL.CrewID )Rnk,C.FirstName+' '+C.LastName Crew,PCL.LegID,CrewCD
                                                 FROM PreflightCrewList PCL INNER JOIN Crew C ON PCL.CrewID=C.CrewID
                                                 WHERE PCL.DutyTYPE='S' AND PCL.CustomerID=@UserCustomerID)SIC ON SIC.LegID=PL.LegID AND SIC.Rnk=1

		LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT C.CrewCD + ',' FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			 AND PC1.DutyTYPE NOT IN ('P','S') 
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID
	    LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (SELECT ISNULL(P1.LastName,'') + (CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN   ',' +ISNULL(P1.FirstName,'') ELSE '' END) + ' ' +ISNULL(P1.MiddleInitial,'') + '###'
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID
		WHERE PM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
		AND (CRM.CRTripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRTripNUM, ',')) OR @CRTripNUM = '')
		AND (PL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
		AND (FC.IsDeadorFerryHead IN (CASE WHEN @FilterFerryLegs=1 THEN 0 ELSE 1 END) OR FC.IsDeadorFerryHead IS NULL)
		AND PM.IsDeleted = 0
)TEMP	
CROSS JOIN @TripFlag TF	
)

SELECT DISTINCT flag
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo
UNION ALL
SELECT DISTINCT flag='A'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo  WHERE Rnk=1

UNION ALL
SELECT DISTINCT flag='C'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo
	            WHERE ISNULL(hotel_name,'') <> ''
	        
UNION ALL
SELECT DISTINCT flag='D'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo
	            WHERE ISNULL(trans_name,'') <> ''

UNION ALL
SELECT DISTINCT flag='E'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo
	            WHERE ISNULL(crewh_name,'') <> ''
UNION ALL
SELECT DISTINCT flag='G'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM TripInfo
	            WHERE ISNULL(cater_name,'') <> ''
ORDER BY leg_num,flag

END

ELSE

BEGIN
;With CrTripInfo AS(
SELECT DISTINCT flag=TF.Flag
			    ,lowdate=CLL.DepartureDTTMLocal
			   ,highdate=CLL.ArrivalDTTMLocal
			   ,orig_nmbr=CRM.CRTripNUM
			   ,trip_nmbr=0
			   ,tail_nmbr=F.TailNum
			   ,type_code=AC.AircraftCD
			   ,cr_stat=CRM.CorporateRequestStatus
			   ,CRdesc=CRMainDescription
			   ,reqcode=P.PassengerRequestorCD
			   ,paxname=P.PassengerName
			   ,phone=P.PhoneNum
			   ,leg_num=CRL.LegNUM
			   ,legid=CRL.CRLegID
			   ,timeapprox=CASE WHEN CRL.IsApproxTM=1 THEN 'TRUE' ELSE 'FALSE' END
			   ,depicao_id=AD.IcaoID
			   ,depnamecitystate=AD.AirportName+(CASE WHEN ISNULL(AD.CityName,'')<>'' THEN ', '+AD.CityName ELSE '' END)+(CASE WHEN ISNULL(AD.StateName,'')<>'' THEN ', '+AD.StateName ELSE '' END)
			   ,arricao_id=AA.IcaoID
			   ,arrnamecitystate=AA.AirportName+(CASE WHEN ISNULL(AA.CityName,'')<>'' THEN ', '+AA.CityName ELSE '' END)+(CASE WHEN ISNULL(AA.StateName,'')<>'' THEN ', '+AA.StateName ELSE '' END)
			   ,pax_total=CRL.PassengerTotal
			   ,depzulu=CASE WHEN AD.IsDayLightSaving = 1 AND CRL.DepartureDTTMLocal BETWEEN DDST.StartDT AND DDST.EndDT THEN FLOOR(AD.OffsetToGMT + 1) ELSE FLOOR(AD.OffsetToGMT) END
			   ,arrzulu =CASE WHEN AA.IsDayLightSaving = 1 AND CRL.ArrivalDTTMLocal BETWEEN ADST.StartDT AND ADST.EndDT THEN FLOOR(AA.OffsetToGMT + 1) ELSE FLOOR(AA.OffsetToGMT) END
			   ,locdep=CRL.DepartureDTTMLocal
			   ,locarr=CRL.ArrivalDTTMLocal
			   ,gmtdep=CRL.DepartureGreenwichDTTM
			   ,gmtarr=CRL.ArrivalGreenwichDTTM
			   ,distance=CRL.Distance
			   ,elp_time=FLOOR(CRL.ElapseTM*10)*0.1
			   ,c_elp_time=CRL.ElapseTM
			   ,crcode=FBOCD
			   ,paxnotes = CRL.Notes
			   ,crewnotes =CRFD.FBOCD
			   ,itinerarymessage = CD.CRDispatchNotes
			   ,depfbo_desc=CRFD.FBOVendor
			   ,depfbo_phone=CRFD.PhoneNUM1
			   ,depfbo_remarks = CRFD.Remarks
			   ,arrfbo_desc=CRFA.FBOVendor
			   ,arrfbo_phone=CRFA.PhoneNUM1
			   ,arrfbo_remarks = CRFD.Remarks
			   ,hotel_name=CRHP.Name
			   ,hotel_phone=CRHP.PhoneNUM
			   ,trans_name=CRT.TransportationVendor
			   ,trans_phone=CRT.PhoneNUM
			   ,crewh_name=CRHC.Name
			   ,crewh_phone=CRHC.PhoneNUM
			   ,crewh_addr1=CRHC.Addr1
			   ,crewh_addr2=CRHC.Addr2
			   ,cater_name=CTRNG.CateringVendor
			   ,cater_remarks = CTRNG.Remarks
			   ,passengers = PAX.PaxNames
			   ,pic='No Crew Assigned'
			   ,sic=''
			   ,addlcrew=''
			   ,ac_code=F.AircraftCD
			   ,ROW_NUMBER()OVER(PARTITION BY CRM.CRTripNum ORDER BY CRTripNum,LegNum)Rnk
		FROM CRMain CRM
		INNER JOIN CRLeg CRL ON CRL.CRMainID = CRM.CRMainID AND CRL.IsDeleted = 0
		INNER JOIN (SELECT  MIN(DepartureDTTMLocal)DepartureDTTMLocal,MAX(ArrivalDTTMLocal)ArrivalDTTMLocal,CRMainID FROM CRLeg WHERE CustomerID=@UserCustomerID AND IsDeleted=0 GROUP BY CRMainID) CLL ON CLL.CRMainID = CRL.CRMainID
		INNER JOIN Fleet F ON F.FleetID = CRM.FleetID
		LEFT OUTER JOIN CRDispatchNotes CD ON CRM.CRMainID = CD.CRMainID
		LEFT OUTER JOIN Aircraft AC ON AC.AircraftID = CRM.AircraftID
		LEFT OUTER JOIN Passenger P ON P.PassengerRequestorID = CRM.PassengerRequestorID
		LEFT OUTER JOIN Airport AA ON AA.AirportID =CRL.AAirportID
		LEFT OUTER JOIN Airport AD ON AD.AirportID =CRL.DAirportID
		LEFT OUTER JOIN DSTRegion DDST ON AD.DSTRegionID = DDST.DSTRegionID
		LEFT OUTER JOIN DSTRegion ADST ON AA.DSTRegionID = ADST.DSTRegionID

		--TO FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, F.Remarks, CRF.CRLegID
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'A'
				) CRFA ON CRFA.CRLegID = CRL.CRLegID 
		--FROM FBO
		LEFT OUTER JOIN 
				(SELECT F.PhoneNUM1, F.FBOVendor, F.Remarks ,CRF.CRLegID,F.FBOCD
					FROM CRFBOList CRF
					INNER JOIN FBO F ON CRF.FBOID = F.FBOID
					WHERE CRF.RecordType = 'D'
				) CRFD ON CRFD.CRLegID = CRL.CRLegID
				
		--HOTEL
		LEFT OUTER JOIN
				(SELECT CH.PhoneNUM PhoneNUM, CH.CRHotelListDescription Name, CH.CRLegID,'C' Flag
					FROM CRHotelList CH
					    LEFT OUTER JOIN Hotel H ON CH.HotelID = H.HotelID
					WHERE CH.RecordType = 'P' --PAX HOTEL
				) CRHP ON CRL.CRLegID = CRHP.CRLegID
		LEFT OUTER JOIN
				(
				SELECT CH.PhoneNUM PhoneNUM, CH.CRHotelListDescription Name, CH.CRLegID, H.Addr1, H.Addr2
					FROM CRHotelList CH
					LEFT OUTER JOIN Hotel H ON CH.HotelID = H.HotelID
				WHERE CH.RecordType = 'C' --CREW HOTEL
			) CRHC ON CRL.CRLegID = CRHC.CRLegID
					
	--TRANSPORT 
	LEFT OUTER JOIN 
			(SELECT CT.CRLegID,CT.PhoneNUM PhoneNUM, CT.CRTransportListDescription TransportationVendor
				FROM CRTransportList CT
				LEFT OUTER JOIN  Transport T ON CT.TransportID = T.TransportID
				WHERE CT.RecordType = 'P'
			)CRT ON CRT.CRLegID = CRL.CRLegID
		
	--CATERING
	LEFT OUTER JOIN
			(SELECT CA.CRCateringListDescription CateringVendor,CA.CRLegID,CA.PhoneNUM PhoneNum, C.Remarks
				FROM CRCateringList CA
				LEFT OUTER JOIN Catering C ON CA.CateringID = C.CateringID
				WHERE CA.RecordType = 'D'
			)CTRNG ON CTRNG.CRLegID = CRL.CRLegID
	LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=CRL.FlightCategoryID
	LEFT OUTER JOIN ( SELECT DISTINCT PP2.CRLegID, PaxNames = (SELECT ISNULL(P1.LastName,'') + (CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN   ',' +ISNULL(P1.FirstName,'') ELSE '' END) + ' ' +ISNULL(P1.MiddleInitial,'') + '###'
                               FROM CRPassenger PP1 INNER JOIN Passenger P1 ON PP1.PassengerRequestorID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                             FOR XML PATH(''))
                              FROM CRPassenger PP2
				   ) PAX ON CRL.CRLegID = PAX.CRLegID
	CROSS JOIN @TripFlag TF 


	WHERE CRM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND (CRM.CRTripNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRTripNUM, ',')) OR @CRTripNUM = '')
	AND (CRL.LegNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CRLegNum, ',')) OR @CRLegNum = '')
	AND (FC.IsDeadorFerryHead IN (CASE WHEN @FilterFerryLegs=1 THEN 0 ELSE 1 END) OR FC.IsDeadorFerryHead IS NULL)
	AND CRM.IsDeleted = 0
)

SELECT DISTINCT flag
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo
UNION ALL
SELECT DISTINCT flag='A'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo  WHERE Rnk=1

UNION ALL
SELECT DISTINCT flag='C'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo
	            WHERE ISNULL(hotel_name,'') <> ''
	        
UNION ALL
SELECT DISTINCT flag='D'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo
	            WHERE ISNULL(trans_name,'') <> ''

UNION ALL
SELECT DISTINCT flag='E'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo
	            WHERE ISNULL(crewh_name,'') <> ''
UNION ALL
SELECT DISTINCT flag='G'
	   ,lowdate
	   ,highdate
	   ,orig_nmbr
	   ,trip_nmbr
	   ,tail_nmbr
	   ,type_code
	   ,cr_stat
	   ,CRdesc
	   ,reqcode
	   ,paxname
	   ,phone
	   ,leg_num
	   ,legid
	   ,timeapprox
	   ,depicao_id
	   ,depnamecitystate
	   ,arricao_id
	   ,arrnamecitystate
	   ,pax_total
	   ,depzulu
	   ,arrzulu
	   ,locdep
	   ,locarr
	   ,gmtdep
	   ,gmtarr
	   ,distance
	   ,elp_time
	   ,c_elp_time
	   ,crcode
	   ,paxnotes
	   ,crewnotes
	   ,itinerarymessage
	   ,depfbo_desc
	   ,depfbo_phone
	   ,depfbo_remarks
	   ,arrfbo_desc
	   ,arrfbo_phone
	   ,arrfbo_remarks
	   ,hotel_name
	   ,hotel_phone
	   ,trans_name
	   ,trans_phone
	   ,crewh_name
	   ,crewh_phone
	   ,crewh_addr1
	   ,crewh_addr2
	   ,cater_name
	   ,cater_remarks
	   ,passengers
	   ,pic
	   ,sic
	   ,addlcrew
	   ,ac_code FROM CrTripInfo
	            WHERE ISNULL(cater_name,'') <> ''
ORDER BY leg_num,flag

END

END
--EXEC spGetReportCRTripItineraryExportInformation 'erick_1','10001','4','',0,0









GO


