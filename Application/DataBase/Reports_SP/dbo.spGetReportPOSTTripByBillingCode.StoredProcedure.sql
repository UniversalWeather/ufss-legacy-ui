IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripByBillingCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripByBillingCode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- SPC Name: [spGetReportPOSTTripByBillingCode]    
-- Author: HARIHARAN S    
-- Create date: 01/30/2013    
-- Description: Get Trip Detail for REPORTS    
-- Revision History    
-- Date   Name  Ver  Change    
--    
---- ================================================================================    
 --EXEC [dbo].[spGetReportPOSTTripByBillingCode]  'JWILLIAMS_13','01-01-2011','03-03-2013','381',''--,'381','','',0    
    
CREATE PROCEDURE [dbo].[spGetReportPOSTTripByBillingCode]   
  
( @UserCD AS VARCHAR(30), --Mandatory    
 @DATEFROM AS DATE , --Mandatory    
 @DATETO AS DATE , --Mandatory     
 @lognumber AS bigint ,--Manditory     
 @pax AS BIGINT   
 )     
AS BEGIN    
  
SET NOCOUNT ON   
  
  DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  

  
SET @pax = ISNULL(@pax,'')  
  
SELECT ISNULL(P.StandardBilling,'NULL') AS Billing_Code,/*p.PassengerRequestorCD,
				  DE.DepartmentCD,  
                DA.AuthorizationCD,     
				P.PassengerName   ,*/
				SUM((200+ISNULL(PFE.ExpenseAmt,0))) AS TOTAL_CHARGES      
		       
FROM PostflightMain PO  
INNER JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID  
AND PO.CustomerID = PL.CustomerID  
LEFT OUTER JOIN Department DE ON PL.DepartmentID = DE.DepartmentID 
LEFT OUTER JOIN DepartmentAuthorization DA  
ON PL.AuthorizationID=DA.AuthorizationID   
INNER JOIN Fleet FL ON FL.FleetID = PO.FleetID  
AND FL.CustomerID = PO.CustomerID   
INNER JOIN PostflightPassenger PF ON PO.POLogID=PF.POLogID AND PL.POLegID=PF.POLegID  
INNER JOIN Passenger P ON PF.PassengerID=P.PassengerRequestorID  
LEFT OUTER JOIN FleetChargeRate FCR ON FL.FleetID = FCR.FleetID  AND FCR.IsDeleted=0
AND FL.CustomerID = FCR.CustomerID     
AND PO.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT  
INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID  
INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID  
INNER JOIN PostflightExpense PFE ON PL.POLogID = PFE.POLogID  
AND PL.POLegID = PFE.POLegID  
AND PL.CustomerID = PFE.CustomerID  
WHERE  (PO.LogNum IN  
         (SELECT DISTINCT RTRIM(S)  
          FROM dbo.SplitString(@lognumber, ','))  
       OR @lognumber ='')  
  AND PO.EstDepartureDT BETWEEN @DateFrom AND @DateTo    
  AND PO.CustomerID = @CUSTOMERID  
GROUP BY
                ISNULL(P.StandardBilling,'NULL')--,
    --            DE.DepartmentCD,  
    --            DA.AuthorizationCD,   
    --            p.PassengerRequestorCD,
				--P.PassengerName      


--DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));  
  
--SET @pax = ISNULL(@pax,'')  
  
--SELECT ISNULL(P.StandardBilling,'NULL') AS Billing_Code,
--				SUM((200+ISNULL(PFE.ExpenseAmt,0))) AS TOTAL_CHARGES      
		       
--FROM PostflightMain PO  
--INNER JOIN PostflightLeg PL ON PO.POLogID = PL.POLogID  
--AND PO.CustomerID = PL.CustomerID  
--LEFT OUTER JOIN Department DE ON PL.DepartmentID = DE.DepartmentID 
--LEFT OUTER JOIN DepartmentAuthorization DA  
--ON PL.AuthorizationID=DA.AuthorizationID   
--INNER JOIN Fleet FL ON FL.FleetID = PO.FleetID  
--AND FL.CustomerID = PO.CustomerID   
--INNER JOIN PostflightPassenger PF ON PO.POLogID=PF.POLogID AND PL.POLegID=PF.POLegID  
--INNER JOIN Passenger P ON PF.PassengerID=P.PassengerRequestorID  
--LEFT OUTER JOIN FleetChargeRate FCR ON FL.FleetID = FCR.FleetID  
--AND FL.CustomerID = FCR.CustomerID     
--AND PO.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT  
--INNER JOIN Airport AD ON PL.DepartICAOID = AD.AirportID  
--INNER JOIN Airport AA ON PL.ArriveICAOID = AA.AirportID  
--INNER JOIN PostflightExpense PFE ON PL.POLogID = PFE.POLogID  
--AND PL.POLegID = PFE.POLegID  
--AND PL.CustomerID = PFE.CustomerID  
--WHERE  (PO.LogNum IN  
--         (SELECT DISTINCT RTRIM(S)  
--          FROM dbo.SplitString(@lognumber, ','))  
--       OR @lognumber ='')  
--  AND PO.EstDepartureDT BETWEEN @DateFrom AND @DateTo    
--  AND PO.CustomerID = @CUSTOMERID  
--GROUP BY
--                ISNULL(P.StandardBilling,'NULL')
                
END
GO


