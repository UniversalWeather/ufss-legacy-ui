IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDepartmentAuthorizationSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTDepartmentAuthorizationSummaryInformation]
	(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
	)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDepartmentAuthorizationSummaryInformation
-- Author:  A.Akhila
-- Create date: 31 Jul 2012
-- Description: Get Department Authorization Summary for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
DECLARE @DEPARTMENT TABLE (DEPARTMENTID BIGINT)
INSERT INTO @DEPARTMENT SELECT DepartmentID FROM Department WHERE IsDeleted = 0 AND IsInActive = 0
--SELECT * FROM @DEPARTMENT
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
					
DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																	WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																	AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =1
IF OBJECT_ID('tempdb..#DEPARTMENTINFO') IS NOT NULL
DROP TABLE #DEPARTMENTINFO 
 SELECT
     [DEPARTMENTID] = ISNULL(D.DEPARTMENTID,'')
     ,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
    ,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
   ,[DepartmentD] =D.DepartmentName            
   ,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
   ,[AuthorizationD] = DA.DeptAuthDescription    
    ,[Category] = ISNULL(CASE WHEN POM.LogNum IS NULL THEN 'NO ACTIVITY' ELSE CONVERT (VARCHAR, FC.FlightCatagoryCD) +' '+ FC.FlightCatagoryDescription END,' ')
    ,[LOG] = POM.POLogID
	,[LEG] = POL.POLegID
    ,[NoofLegs]= POL.POLegID
    ,[BlockHours] = ISNULL(ROUND(POL.BlockHours,1),0)
    ,[FlightHours] = ISNULL(ROUND(POL.FlightHours,1),0)
    ,[AvgBlkHrsLeg]= NULL
    ,[AvgFltHrsLeg] =NULL
    ,[NauticalMiles] = POL.Distance
    ,[AvgMilesLeg] = NULL
    ,[NoofPax] = POL.PassengerTotal
	,[TenToMin] = @TenToMin
INTO #DEPARTMENTINFO

FROM 
PostflightLeg POL
LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
 LEFT OUTER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
 LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID

LEFT OUTER JOIN Fleet F ON F.FleetID=POM.FleetID AND F.CustomerID=POM.CustomerID
LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization DA
ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 

AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
 LEFT JOIN Aircraft AC ON AC.AircraftID = F.AircraftID
WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 AND 
pom.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
AND POL.IsDeleted=0
--AND POM.LogNum in(SELECT distinct LogNum  FROM PostflightMain PM INNER JOIN PostflightLeg PL ON PM.POLogID=PL.POLogID  
--	WHERE CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) AND PM.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD))
AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
AND (POM.HomebaseID =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR  @IsHomebase=0)


  GROUP BY D.DEPARTMENTID, DepartmentCD, D.DepartmentName, DA.AuthorizationCD, DA.DeptAuthDescription 
   , FC.FlightCatagoryCD, FC.FlightCatagoryDescription, POM.POLogID, POM.LogNum, POL.POLegID, BlockHours, FlightHours, Distance, POL.PassengerTotal 
IF @IsZeroSuppressActivityDeptRpt = 0
BEGIN
SELECT CASE WHEN (D1.DEPARTMENTID=0) THEN NULL ELSE D1.DEPARTMENTID END AS [DEPARTMENTID] 
,[DateRange], [Department], [DepartmentD], [Authorization], [AuthorizationD], [Category]
, [LOG], [LEG], [NoofLegs], [BlockHours], [FlightHours], [AvgBlkHrsLeg], [AvgFltHrsLeg], [NauticalMiles], [AvgMilesLeg], [NoofPax], [TenToMin]
FROM #DEPARTMENTINFO D1
UNION ALL 
SELECT DISTINCT
     [DEPARTMENTID] = D.DEPARTMENTID
     ,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
    ,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
		   ,[DepartmentD] =D.DepartmentName            
		   ,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
		   ,[AuthorizationD] = DA.DeptAuthDescription    
    ,[Category] = ISNULL(NULL,'NO ACTIVITY')
    ,[LOG] = NULL
	,[LEG] = NULL
    ,[NoofLegs]= ISNULL(NULL,0)
    ,[BlockHours] = ISNULL(NULL,0)
    ,[FlightHours] = ISNULL(NULL,0)
    ,[AvgBlkHrsLeg]= ISNULL(NULL,0)
    ,[AvgFltHrsLeg] = ISNULL(NULL,0)
    ,[NauticalMiles] = ISNULL(NULL,0)
    ,[AvgMilesLeg] = ISNULL(NULL,0)
    ,[NoofPax] = ISNULL(NULL,0)
    ,[TenToMin] = @TenToMin

       FROM DEPARTMENT D
INNER JOIN @DEPARTMENT DT ON D.DepartmentID = DT.DEPARTMENTID
  INNER JOIN DepartmentAuthorization DA
   ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
 AND D.DepartmentID = DA.DepartmentID
 
WHERE D.DEPARTMENTID NOT IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTMENTINFO)	 AND 	
  D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
      AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
       
       
         ORDER BY [Department], [Authorization], [Category]
END
ELSE
BEGIN
SELECT CASE WHEN (D1.DEPARTMENTID=0) THEN NULL ELSE D1.DEPARTMENTID END AS [DEPARTMENTID] 
,[DateRange], [Department], [DepartmentD], [Authorization], [AuthorizationD], [Category]
, [LOG], [LEG], [NoofLegs], [BlockHours], [FlightHours], [AvgBlkHrsLeg], [AvgFltHrsLeg], [NauticalMiles], [AvgMilesLeg], [NoofPax], [TenToMin]
FROM #DEPARTMENTINFO D1
END
--EXEC spGetReportPOSTDepartmentAuthorizationSummaryInformation 'SUPERVISOR_99','2009-01-01','2009-1-1','','',1






GO


