IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDeptAuthSummaryStatisticsExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDeptAuthSummaryStatisticsExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spGetReportPOSTDeptAuthSummaryStatisticsExportInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
		)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDeptAuthSummaryStatisticsExportInformation
-- Author:  A.Akhila
-- Create date: 6 Aug 2012
-- Description: Get Dept/Auth Summary Statistics for EXPORT REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt =1
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempFleetID TABLE     
  (   
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT
  )
  
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END

ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------
	
CREATE TABLE #DEPARTINFO
	(
	[DEPARTMENTID] BIGINT
	,[dept_code] NVARCHAR(8)
	,[auth_code] NVARCHAR(8)
	,[dept_desc] NVARCHAR(25)
	,[auth_desc] NVARCHAR(25)
	,[TAILNUMBER] VARCHAR(9)
	,[LOGNUMBER] BIGINT
	,[domflt_hrs] NUMERIC(6,3)
	,[intlflt_hrs] NUMERIC(6,3)
	,[domflt_miles] NUMERIC(5,0)
	,[intlflt_miles] NUMERIC(5,0)
	,[dom_log] NUMERIC(1,0)
	,[intl_log] NUMERIC(1,0)
	
	,[DUTYTYPE] NVARCHAR(10)
	)
INSERT INTO #DEPARTINFO
SELECT DISTINCT
	[DEPARTMENTID] = D.DEPARTMENTID
	,[dept_code] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END
	,[auth_code] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END
	,[dept_desc] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD IS NULL THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END
	,[auth_desc] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD IS NULL  THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END
	,[TAILNUMBER] = F.TailNum
	,[LOGNUMBER] = POM.LogNum
	,[domflt_hrs] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN (CASE WHEN C.AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END)
						OVER(PARTITION BY POM.LogNum)
	,[intlflt_hrs] = SUM(CASE WHEN POL.DutyTYPE > 1 THEN (CASE WHEN C.AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END)
						OVER(PARTITION BY POM.LogNum)
	,[domflt_miles] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN POL.Distance ELSE 0 END)
						OVER(PARTITION BY POM.LogNum)
	,[intlflt_miles] = SUM(CASE WHEN POL.DutyTYPE > 1 THEN POL.Distance ELSE 0 END)
						OVER(PARTITION BY POM.LogNum)
	,[dom_log] = NULL
	,[intl_log] = NULL
	
	,[DUTYTYPE] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN 0 ELSE (CASE WHEN POL.DutyTYPE = 2 THEN 1 ELSE NULL END)END)OVER (PARTITION BY POM.LogNum)
FROM PostflightLeg POL
	LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
	INNER JOIN ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = POM.FleetID
	INNER JOIN  Fleet F ON F.FleetID = POM.FleetID
	LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID AND D.IsDeleted = 0 AND D.IsInActive= 0
	LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
	AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
	LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
WHERE 
	POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	AND POL.IsDeleted=0
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
	AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND ( POM.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
CREATE TABLE #DEPARTEMPTY
	(
	[DEPARTMENTID] BIGINT
	,[dept_code] NVARCHAR(8)
	,[auth_code] NVARCHAR(8)
	,[dept_desc] NVARCHAR(25)
	,[auth_desc] NVARCHAR(25)
	,[TAILNUMBER] VARCHAR(9)
	,[LOGNUMBER] BIGINT
	,[domflt_hrs] NUMERIC(6,3)
	,[intlflt_hrs] NUMERIC(6,3)
	,[domflt_miles] NUMERIC(5,0)
	,[intlflt_miles] NUMERIC(5,0)
	,[dom_log] NUMERIC(1,0)
	,[intl_log] NUMERIC(1,0)
	
	,[DUTYTYPE] NVARCHAR(10)
	)
IF  @TailNum =''
	BEGIN
	INSERT INTO #DEPARTEMPTY
		SELECT DISTINCT
		[DEPARTMENTID] = D.DEPARTMENTID
		,[dept_code] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD IS NULL THEN 'ZZZ@#' ELSE D.DepartmentCD END
		,[auth_code] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD IS NULL THEN 'ZZZ@#' ELSE DA.AuthorizationCD END
		,[dept_desc] = CASE WHEN D.DepartmentCD = '' OR D.DepartmentCD IS NULL THEN 'UNALLOCATED FLT ACTIVITY' ELSE D.DepartmentName END
		,[auth_desc] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD IS NULL  THEN 'UNALLOCATED FLT ACTIVITY' ELSE DA.DeptAuthDescription END
		,[TAILNUMBER] = NULL
		,[LOGNUMBER] =NULL
		,[domflt_hrs] = ISNULL(NULL,0)
		,[intlflt_hrs] = ISNULL(NULL,0)
		,[domflt_miles] = ISNULL(NULL,0)
		,[intlflt_miles] = ISNULL(NULL,0)
		,[dom_log] = ISNULL(NULL,0)
		,[intl_log] = ISNULL(NULL,0)
		
		,[DUTYTYPE] = NULL
		FROM DEPARTMENT D
		LEFT OUTER JOIN DepartmentAuthorization DA ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 AND D.DepartmentID = DA.DepartmentID
		WHERE D.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) AND D.IsDeleted=0 AND D.IsInActive=0
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	END
DELETE FROM #DEPARTEMPTY WHERE [DEPARTMENTID] IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTINFO)

IF @IsZeroSuppressActivityDeptRpt=0
BEGIN
	SELECT * FROM #DEPARTINFO
UNION ALL
	SELECT * FROM #DEPARTEMPTY
	ORDER BY [dept_code], [auth_code], [TAILNUMBER], [LOGNUMBER] 
END
ELSE
BEGIN
	SELECT * FROM #DEPARTINFO
	ORDER BY [dept_code], [auth_code], [TAILNUMBER], [LOGNUMBER] 
END


IF OBJECT_ID('tempdb..#DEPARTINFO') IS NOT NULL
DROP TABLE #DEPARTINFO 	

IF OBJECT_ID('tempdb..#DEPARTEMPTY') IS NOT NULL
DROP TABLE #DEPARTEMPTY 

--EXEC spGetReportPOSTDeptAuthSummaryStatisticsExportInformation 'SUPERVISOR_99','2009-12-01','2010-01-01', '', '', '', '',0







GO


