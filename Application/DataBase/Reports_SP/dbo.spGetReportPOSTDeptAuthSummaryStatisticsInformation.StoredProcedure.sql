IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTDeptAuthSummaryStatisticsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTDeptAuthSummaryStatisticsInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPOSTDeptAuthSummaryStatisticsInformation]
		(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@IsHomebase AS BIT = 0 --Boolean: 1 indicates to fetch only for user HomeBase
		)
AS

-- ===============================================================================
-- SPC Name: spGetReportPOSTDeptAuthSummaryStatisticsInformation
-- Author:  A.Akhila
-- Create date: 1 Aug 2012
-- Description: Get Dept/Auth Summary Statistics for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================
SET NOCOUNT ON
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

DECLARE @DEPARTMENT TABLE (DEPARTMENTID BIGINT)
INSERT INTO @DEPARTMENT SELECT DepartmentID FROM Department WHERE IsDeleted = 0 AND IsInActive = 0
--SELECT * FROM @DEPARTMENT



DECLARE @TenToMin SMALLINT = 0;
SELECT @TenToMin = TimeDisplayTenMin FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
					AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
DECLARE @IsZeroSuppressActivityDeptRpt BIT;
SELECT @IsZeroSuppressActivityDeptRpt = IsZeroSuppressActivityDeptRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
																				


 DECLARE @AircraftBasis NUMERIC(1,0);
 SELECT @AircraftBasis = AircraftBasis from Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD) 
	                                 AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityDeptRpt=1
IF OBJECT_ID('tempdb..#DEPARTMENTINFO') IS NOT NULL
DROP TABLE #DEPARTMENTINFO 
-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @TempFleetID TABLE     
  (   
	ID INT NOT NULL IDENTITY (1,1), 
	FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER  
	AND F.IsDeleted=0
	AND F.IsInActive=0
END
-----------------------------TailNum and Fleet Group Filteration----------------------	

	SELECT DISTINCT
		[DEPARTMENTID] = ISNULL(D.DEPARTMENTID,'')
		,[AUTHORIZATIONID] = DA.AUTHORIZATIONID
		,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
		,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
		,[DepartmentD] =D.DepartmentName            
		,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN 'UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
		,[AuthorizationD] = DA.DeptAuthDescription
		,[TailNumber] = ISNULL(F.TailNum,'')
		,[AIRCRAFTBASIS] =@AircraftBasis
		,[FlightHoursDom] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN (CASE WHEN @AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END) 
		OVER (PARTITION BY POM.LogNum)
		,[FlightHoursIntl] = SUM(CASE WHEN POL.DutyTYPE > 1 THEN (CASE WHEN @AircraftBasis = 1 THEN  ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END) ELSE 000000.000 END)
		OVER (PARTITION BY POM.LogNum)
		--,[DUTY] = POL.DutyTYPE
		,[LogNum] = POM.LogNum
		,[DutyType] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN 0 ELSE 1 END)OVER (PARTITION BY POM.LogNum)
		,[TripCountDom] = NULL--SUM(CASE WHEN POL.DutyTYPE = 0 THEN 00001 ELSE 00000 END)OVER (PARTITION BY POM.LogNum)
		,[TripCountIntl] = NULL--SUM(CASE WHEN POL.DutyTYPE > 0 THEN 00001 ELSE 00000 END)OVER (PARTITION BY POM.LogNum)
		,[NauticalMilesDom] = SUM(CASE WHEN POL.DutyTYPE = 1 THEN POL.Distance ELSE 0 END)OVER (PARTITION BY POM.LogNum)
		,[NauticalMilesIntl] = SUM(CASE WHEN POL.DutyTYPE > 1 THEN POL.Distance ELSE 0 END)OVER (PARTITION BY POM.LogNum)
		,[TenToMin] = @TenToMin
	INTO #DEPARTMENTINFO
	FROM PostflightLeg POL
		LEFT OUTER JOIN PostflightMain POM ON POL.POLogID = POM.POLogID and POM.IsDeleted=0
		INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = POM.FleetID
		LEFT OUTER JOIN (
		SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID, F.MaximumPassenger
		FROM Fleet F 
		LEFT OUTER JOIN FleetGroupOrder FGO ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
		LEFT OUTER JOIN FleetGroup FG ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID WHERE F.IsDeleted = 0
		AND (FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = '')
		) F ON POM.FleetID = F.FleetID AND POM.CustomerID = F.CustomerID
		LEFT OUTER JOIN Department D ON D.DepartmentID = POL.DepartmentID
		LEFT OUTER JOIN DepartmentAuthorization DA
		ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0 
		AND D.DepartmentID = DA.DepartmentID AND POL.AuthorizationID=DA.AuthorizationID
		--LEFT JOIN Company C ON C.HomebaseID = POM.HomebaseID
	WHERE ISNULL(D.IsDeleted,0) = 0 AND ISNULL(D.IsInActive,0)= 0 AND 
		POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
		AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
		AND(F.TailNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNum, ',')) OR @TailNum = '')
		AND POL.IsDeleted = 0
		AND (POM.HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @IsHomebase=0)
IF @IsZeroSuppressActivityDeptRpt=0
BEGIN 
	SELECT CASE WHEN (D1.DEPARTMENTID=0) THEN NULL ELSE D1.DEPARTMENTID END AS [DEPARTMENTID] ,[AUTHORIZATIONID]
		,[DateRange], [Department], [DepartmentD], [Authorization], [AuthorizationD], [TailNumber]
		,[AIRCRAFTBASIS], [FlightHoursDom], [FlightHoursIntl]--, [DUTY]
		, [LogNum], [DutyType], [TripCountDom], [TripCountIntl], [NauticalMilesDom], [NauticalMilesIntl], [TenToMin]
	FROM #DEPARTMENTINFO D1
		UNION ALL 
	SELECT DISTINCT
		[DEPARTMENTID] = D.DEPARTMENTID
		,[AUTHORIZATIONID] = DA.AUTHORIZATIONID
		,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
		,[Department] = CASE WHEN D.DepartmentCD = ''  OR D.DepartmentCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR,D.DepartmentCD) END 
		,[DepartmentD] =D.DepartmentName            
		,[Authorization] = CASE WHEN DA.AuthorizationCD = '' OR DA.AuthorizationCD  is null THEN ' UNALLOCATED FLT ACTIVITY' ELSE CONVERT (VARCHAR, DA.AuthorizationCD) END  
		,[AuthorizationD] = DA.DeptAuthDescription
		,[TailNumber] = NULL
		,[AIRCRAFTBASIS] = @AircraftBasis
		,[FlightHoursDom] = NULL
		,[FlightHoursIntl] = NULL
		--,[DUTY] = NULL
		,[LogNum] = NULL
		,[DutyType] = NULL
		,[TripCountDom] = NULL
		,[TripCountIntl] = NULL
		,[NauticalMilesDom] = NULL
		,[NauticalMilesIntl] = NULL
		,[TenToMin] = @TenToMin
	FROM DEPARTMENT D
		INNER JOIN @DEPARTMENT DT ON D.DepartmentID = DT.DEPARTMENTID
		INNER JOIN DepartmentAuthorization DA
		ON D.CustomerID = DA.CustomerID AND DA.IsDeleted = 0  
		AND D.DepartmentID = DA.DepartmentID
	WHERE D.DEPARTMENTID NOT IN (SELECT DISTINCT DEPARTMENTID FROM #DEPARTMENTINFO) AND 
		d.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		AND (D.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
		AND (DA.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	ORDER BY [Department], [Authorization], [TailNumber], [LogNum] 
END	
ELSE 
BEGIN
	SELECT CASE WHEN (D1.DEPARTMENTID=0) THEN NULL ELSE D1.DEPARTMENTID END AS [DEPARTMENTID] ,[AUTHORIZATIONID]
		,[DateRange], [Department], [DepartmentD], [Authorization], [AuthorizationD], [TailNumber]
		,[AIRCRAFTBASIS], [FlightHoursDom], [FlightHoursIntl]--, [DUTY]
		, [LogNum], [DutyType], [TripCountDom], [TripCountIntl], [NauticalMilesDom], [NauticalMilesIntl], [TenToMin]
	FROM #DEPARTMENTINFO D1
	ORDER BY [Department], [Authorization], [TailNumber], [LogNum] 
END
--EXEC spGetReportPOSTDeptAuthSummaryStatisticsInformation 'SUPERVISOR_99','2009-01-01','2009-1-1', '', '', ''




GO


