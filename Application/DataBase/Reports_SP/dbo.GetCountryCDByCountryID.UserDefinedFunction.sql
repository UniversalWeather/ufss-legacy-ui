IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCountryCDByCountryID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetCountryCDByCountryID]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Function Name: GetCountryCDByCountryID
-- Author: SUDHAKAR J
-- Create date: 29 Jun 2012
-- Description: Returns CountryCD from Country table for the given CountryID
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetCountryCDByCountryID] (@CountryID BIGINT)
RETURNS char(3)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @CountryCD char(4);
	SELECT @CountryCD = CountryCD FROM Country WHERE CountryID = @CountryID;
		
    RETURN @CountryCD;
END;

GO


