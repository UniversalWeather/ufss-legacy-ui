IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportDBCharterQuoteCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportDBCharterQuoteCustomer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE  [dbo].[spGetReportDBCharterQuoteCustomer] 
(
        @UserCD AS VARCHAR(30) --Mandatory
       ,@UserCustomerID AS VARCHAR(30)
       ,@CQCustomerCD AS VARCHAR(500)=''
)
AS
BEGIN
 

-- ===============================================================================
-- SPC Name: spGetReportDBCharterQuoteCustomer
-- Author: Askar R
-- Create date: 23 Feb 2013
-- Description: Get Customer Additional Information
-- Revision History
-- Date        Name        Ver         Change
-- 
-- ================================================================================

SELECT --Customer General Infromation----
       ISNULL(CQC.CQCustomerCD,'') CustomerCode,
       ISNULL(CQC.CQCustomerName,'') CompanyName,
       ISNULL(CQC.BillingPhoneNum,'') Phone,
       ISNULL( CQC.BillingFaxNum,'') Fax,
       ISNULL(CQC.BillingAddr1,'') Addr1,
       ISNULL(CQC.BillingAddr2,'') Addr2,
       ISNULL(CQC.BillingAddr3,'') Addr3,
       ISNULL(CQC.BillingCity,'') City,
       ISNULL(CQC.BillingState,'') StateProv,
       ISNULL(CQC.BillingZip,'') Postal,
       ISNULL(CR.CountryCD,'') Country,
       ISNULL(A.IcaoID ,'')HOMEBASE,
       ISNULL(CT.IcaoID,'') CLOSESTICAO,
       ISNULL(CQC.DiscountPercentage,0) DiscountPercent,
       ISNULL(CQC.Credit,0) CREDITLIMIT,
       ISNULL(CQC.IsApplicationFiled,0) CreditApplication,
       ISNULL(CQC.IsApproved,0) CreditApproval,
       ISNULL(CQC.Notes ,'') Notes,
       --Customer Contact Infromation----
       ISNULL(CQCC.IsChoice,0) MAINCHOICE,
       ISNULL(CQCC.LastName,'') +CASE WHEN CQCC.LastName<>'' THEN ', '+ISNULL(CQCC.FirstName,'') ELSE ISNULL(CQCC.FirstName,'') END + ' '+ISNULL(MiddleName,'') ContactName,
       ISNULL(CQCC.PhoneNum,'') ContactPHONE,
       ISNULL(CQCC.BusinessFaxNum,'') ContactFAX,
       ISNULL(CQCC.Addr1,'') ContactAddr1,
       ISNULL(CQCC.Addr2,'') ContactAddr2,
       ISNULL(CQCC.Addr3,'') ContactAddr3,
       ISNULL(CQCC.CityName,'') ContactCity,
       ISNULL(CQCC.StateName,'') ContactStateProv,
       ISNULL(CQCC.PostalZipCD,'') ContactPostal,
       ISNULL(CR.CountryCD,'') ContactCountry,
       --Customer Credit Infromation----
       ISNULL(CQCC.CreditName1,'') CRENAME1,
       ISNULL(CQCC.CreditNum1,'') CRENUMBEREXPIRATION1,
       ISNULL(CQCC.CreditName2,'') CRENAME2,
       ISNULL(CQCC.CreditNum2,'') CRENUMBEREXPIRATION2,
       ISNULL(CQCC.MoreInfo,'') CREAddInfo
        FROM CQCustomer CQC INNER JOIN CQCustomerContact CQCC ON CQC.CQCustomerID=CQCC.CQCustomerID
                            LEFT OUTER JOIN Company C ON CQC.HomebaseID=C.HomebaseID
                            LEFT OUTER JOIN Airport A ON C.HomebaseAirportID=A.AirportID
                            LEFT OUTER JOIN Airport CT ON CQC.AirportID=CT.AirportID
                            LEFT OUTER JOIN Country CR ON CR.CountryID=CQCC.CountryID
       WHERE CQC.CustomerID=CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
         AND (CQC.CQCustomerCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CQCustomerCD, ',')) OR @CQCustomerCD='')
         AND CQC.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
         --And CQC.IsInActive=0
         And CQC.IsDeleted=0
         AND CQCC.IsDeleted=0
        ORDER BY CustomerCode,--LastName
        ISNULL(CQCC.LastName,'') +CASE WHEN CQCC.LastName<>'' THEN ', '+ISNULL(CQCC.FirstName,'') ELSE ISNULL(CQCC.FirstName,'') END + ' '+ISNULL(MiddleName,'')
			
		 --AND CQCC.IsChoice=1

END



--EXEC spGetReportDBCharterQuoteCustomer 'SUPERVISOR_99',10099,'MCUS1'


GO


