IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationAPDX1Sub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDX1Sub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationAPDX1Sub]
   @UserCD AS VARCHAR(30),			-- Mandatory
  -- @TripID AS VARCHAR(200),         -- Mandatory
   @LegID AS VARCHAR(30)            -- Mandatory	
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSGeneralDeclarationAPDX1Sub
-- Author: ABHISHEK.S
-- Create date: 13th September 2012
-- Description: Get TripSheetReportWriter General Declaration Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
BEGIN
	
	SET NOCOUNT ON;	
	
--DECLARE @TEMP1 TABLE (Operator VARCHAR(100),NandR VARCHAR(50),FlightNo BIGINT , Date1 DATE,DepFrm VARCHAR(50),ArrAt VARCHAR(50), TNOFCrew VARCHAR(50), PNUM VARCHAR(100), CNTRY VARCHAR(20) ) 
  DECLARE @TEMP1 TABLE (LegId VARCHAR(30),Operator VARCHAR(100),NandR VARCHAR(9),FlightNo VARCHAR(12) , Date1 VARCHAR(10),DepFrm VARCHAR(100),ArrAt VARCHAR(100),PLACE VARCHAR(10), TNOFCrew VARCHAR(50), PNUM VARCHAR(100), CNTRY VARCHAR(20) ) 
INSERT INTO @TEMP1	
 
SELECT DISTINCT
[LegId]    =	PL.LegID,
[Operator] = 	CONVERT(VARCHAR(MAX),FP.OwnerLesse),
[NandR]    = 	F.TailNum,
[FlightNo] = 	PM.FlightNUM,
[Date1]    = 	dbo.GetShortDateFormatByUserCDForTripSheet(@UserCD,PL.DepartureDTTMLocal) ,
[DepFrm]   =	RTRIM(A.CityName)+','+RTRIM(A.CountryName),
[ArrAt]    =	RTRIM(B.CityName)+','+RTRIM(B.CountryName),
[PLACE]    =	NULL,
[TNOFCrew] =    ISNULL(CR.FirstName,'') +' '+ ISNULL(CR.MiddleInitial,'') +' '+ ISNULL(CR.LastName,''),
--[PNUM]     =    dbo.FlightPakDecrypt(ISNULL(CPP.PassportNum,'')),
[PNUM]   =    ISNULL(CPP.PassportNum,''),
[CNTRY]    =    C.CountryCD

FROM PREFLIGHTMAIN PM
				INNER JOIN PreflightLeg PL
				        ON PM.TripID = PL.TripID
				INNER JOIN (SELECT FleetID, TailNum FROM Fleet) F
				        ON PM.FleetID = F.FleetID
				LEFT JOIN FleetPair FP
				        ON F.FleetID = FP.FleetID
				LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS A
			            ON A.AirportID = PL.DepartICAOID
		        LEFT JOIN (SELECT IcaoID,AirportID,CityName,CountryName FROM AIRPORT )AS B
			            ON B.AirportID = PL.ArriveICAOID
			    LEFT JOIN PreflightCrewList PC
				        ON PL.LegID = PC.LegID
			    LEFT  JOIN (SELECT CrewID, CrewCD,FirstName,MiddleInitial,LastName,CountryID FROM Crew) CR
			            ON PC.CrewID = CR.CrewID 
			    LEFT JOIN CrewPassengerPassport CPP
				        --ON PC.CrewID = CPP.CrewID
				        --ON (SELECT DISTINCT PC.PassportID FROM PreflightCrewList PC WHERE CONVERT(VARCHAR,PC.LegID) = @LegID
												--									AND	PC.CrewID = CPP.CrewID ) = CPP.PassportID
						  ON PC.PassportID  = CPP.PassportID AND CPP.IsDeleted = 0						
		        LEFT JOIN Country C
			    	    ON CPP.CountryID = C.CountryID			    	    
WHERE PM.IsDeleted = 0
	             AND PM.CustomerID = CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
	              -- AND CONVERT(VARCHAR,PM.TripID)  = @TripID
	               AND CONVERT(VARCHAR,PL.LegID) IN (@LegID)
               
	               
DECLARE @COUNT INT 
SELECT @COUNT=COUNT(*) FROM @TEMP1             

WHILE @COUNT<7
BEGIN
SET @COUNT=@COUNT+1
END

IF(@COUNT%7)=0 
SET @COUNT=@COUNT
ELSE IF(@COUNT%7)=1
SET @COUNT=@COUNT+6
ELSE IF(@COUNT%7)=2
SET @COUNT=@COUNT+5
ELSE IF(@COUNT%7)=3
SET @COUNT=@COUNT+4
ELSE IF(@COUNT%7)=4
SET @COUNT=@COUNT+3
ELSE IF(@COUNT%7)=5
SET @COUNT=@COUNT+2
ELSE IF(@COUNT%7)=6
SET @COUNT=@COUNT+1

DECLARE @COUNT1 INT;
SELECT @COUNT1=COUNT(*) FROM @TEMP1

WHILE  @COUNT1<@COUNT
BEGIN 
 INSERT INTO @TEMP1(CNTRY) VALUES(NULL);
 SET @COUNT1 = @COUNT1 +1  
END

	SELECT * FROM @TEMP1
			    
END
-- EXEC spGetReportPRETSGeneralDeclarationAPDX1Sub 'TIM' ,  '100013169'

 -- EXEC  spGetReportPRETSGeneralDeclarationAPDX1Sub 'supervisor_99', '10099107565'







GO


