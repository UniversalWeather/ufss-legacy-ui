IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSLegNotesInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSLegNotesInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSLegNotesInformation]
	@UserCD VARCHAR(50)--MANDATORTY
	,@TripID  AS VARCHAR(50)  -- [BIGINT is not supported by SSRS]
	,@LegNum VARCHAR(150) = ''
AS
BEGIN
-- ===============================================================================
-- SPC Name: spGetReportPRETSLegNotesInformation
-- Author: AISHWARYA.M
-- Create date: 01 Sep 2012
-- Description: Get Preflight TripSheet Leg Notes information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

  SELECT [LegID]=CONVERT(VARCHAR,PL.LegID)
		,[LegNum]=CONVERT(VARCHAR,PL.LegNUM)
		,PassengerNotes = ISNULL(PL.Notes,'')
		,CrewNotes = ISNULL(PL.CrewNotes,'')
		,Notes = ISNULL(AA.GeneralNotes,'')
		,DepartureAlerts = ISNULL(AD.Alerts,'')
		,ArrivalAlerts = ISNULL(AA.Alerts,'')
		,AircraftNotes = ISNULL(F.Notes,'')
		,AirportNotes = ISNULL(AA.GeneralNotes,'')
		,NotesAndAnnouncements = ISNULL(CO.ApplicationMessage,'')
		,TripsheetAlerts = ISNULL(PM.Notes,'')
		,IsLegNotesEmpty = CASE WHEN ISNULL(PL.Notes,'') = '' AND 
									 ISNULL(PL.CrewNotes,'') = ''
									 THEN 1 ELSE 0 END
		,IsTripNotesEmpty = CASE WHEN LEN(N.Notes) > 0 THEN 0 ELSE 1 END
		FROM PreflightMain PM
		INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID
		INNER JOIN (SELECT AirportID, IcaoID, Alerts  FROM Airport) AD ON PL.DepartICAOID = AD.AirportID  
		INNER JOIN (SELECT AirportID, IcaoID, Alerts, GeneralNotes FROM Airport) AA ON PL.ArriveICAOID = AA.AirportID 
		INNER JOIN (SELECT FleetID, Notes, CustomerID FROM Fleet) F 
			ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
		INNER JOIN (SELECT HomebaseID, CustomerID, ApplicationMessage FROM Company) CO 
			ON --CO.HomebaseID = PM.HomebaseID AND 
			PM.CustomerID = CO.CustomerID
		LEFT OUTER JOIN (
			SELECT PM2.TRIPID, Notes = ( 
				SELECT RTRIM(ISNULL(PL.Notes,'')) + RTRIM(ISNULL(PL.CrewNotes,'')) + ''
				FROM PREFLIGHTLEG PL
				WHERE PM2.TripID = PL.TripID --AND PL2.LegID = PL.LegID
				FOR XML PATH('')
			)
			--, PL2.LegID
			FROM PreflightMain PM2
			--INNER JOIN PREFLIGHTLEG PL2 ON PM2.TRIPID = PL2.TRIPID
		) N ON PM.TRIPID = N.TRIPID --AND PL.LegID = N.LegID
		WHERE PM.CustomerID =  dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		AND PM.TripID = CONVERT(BIGINT,@TripID)
		AND CO.HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
		AND (PL.LegNUM IN (SELECT DISTINCT CONVERT(BIGINT,RTRIM(S)) FROM dbo.SplitString(@LegNum, ',')) OR @LegNum = '')
		ORDER BY PM.TripID,LegNum
END

--spGetReportPRETSLegNotesInformation 'SUPERVISOR_99','10099107564',''



GO


