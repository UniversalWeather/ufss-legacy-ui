IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPOSTTripExportInformation] 
( 
	@UserCD AS VARCHAR(30), 
	@UserCustomerID AS VARCHAR(30),          
	@DATEFROM AS DATE , 
	@DATETO AS DATE ,  
	@DepartmentCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@AuthorizationCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@LogNum NVARCHAR(1000) = '',
	@TailNum AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@FleetGroupCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values  
	@IsHomebase AS BIT = 0, --Boolean: 1 indicates to fetch only for user HomeBase
	@UserHomebaseID AS VARCHAR(30)  
 )   
AS 

BEGIN            
SET NOCOUNT ON  
         
DECLARE @TenToMin SMALLINT = 0;  
DECLARE @AircraftBasis NUMERIC(1,0); 
SELECT @TenToMin = TimeDisplayTenMin,
@AircraftBasis = AircraftBasis
 FROM Company WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)   
AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD) 

-----------------------------TailNum and Fleet Group Filteration----------------------
  
DECLARE @CustomerID BIGINT;
SET @CustomerID  = DBO.GetCustomerIDbyUserCD(@UserCD);

DECLARE  @TempFleetID  TABLE 
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
	)
IF @TailNUM <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CustomerID
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	LEFT JOIN vFleetGroup FG
	ON F.FleetID = FG.FleetID AND F.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMERID  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID
	FROM Fleet F 
	WHERE F.CustomerID = @CUSTOMERID  
END

-----------------------------TailNum and Fleet Group Filteration----------------------
SELECT DISTINCT
            [Leg] = POL.LegNUM
           ,[LogNumber] = POM.LogNum
           ,POM.EstDepartureDT
           ,F.TailNum
           ,Req.PassengerRequestorCD
           ,DEM.DepartmentCD AS reqdept
           ,DAM.AuthorizationCD AS reqauth
           ,POL.POLegID AS legid
           ,AD.IcaoID AS [DepICAO]
           ,AA.IcaoID AS [ArrICAO]
           ,POL.ScheduledTM AS schedttm
           ,POL.Distance
           ,ROUND(POL.FlightHours,1) AS flk_hrs
           ,ROUND(POL.BlockHours,1) AS blk_hrs
           ,DEL.DepartmentCD
           ,DAL.AuthorizationCD
           ,FCR.ChargeRate
           ,FCR.ChargeUnit
           ,F.MaximumPassenger
           ,AF.AircraftCD
           ,POL.PassengerTotal
           ,[TotalChargesLEG] = --(CASE WHEN CONVERT(DATE,POM.EstDepartureDT) BETWEEN CONVERT(DATE,FCR.BeginRateDT) AND CONVERT(DATE,FCR.EndRateDT)
						--THEN(
							CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
							--)ELSE
						--NULL
						--END) 
						+ (ISNULL(POE.ExpenseAmt,0))
		 ,[RequiredDepartment] = PaxD.DepartmentCD
		 ,[RequiredAuthorization] = PaxA.AuthorizationCD
		,[Charge] = 		CASE FCR.ChargeUnit WHEN 'H' THEN (ISNULL(FCR.ChargeRate,0) * (CASE WHEN @AircraftBasis=1 THEN ROUND(POL.BlockHours,1) ELSE ROUND(POL.FlightHours,1) END))           
							WHEN 'N' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0))
							WHEN 'S' THEN (ISNULL(FCR.ChargeRate,0) * ISNULL(POL.Distance,0) * 1.1508) END
						+ (ISNULL(POE.ExpenseAmt,0))
		 ,[EXP_ACCOUNT] = EX.AccountDescription
		 ,[EXP_AMOUNT] = EX.ExpenseAMT
         ,[TenToMin] = @TenToMin
FROM PostflightMain POM
LEFT OUTER JOIN PostflightLeg POL ON POM.POLogID = POL.POLogID AND POL.IsDeleted = 0
LEFT OUTER JOIN Passenger PP ON PP.PassengerRequestorID = POL.PassengerRequestorID
LEFT OUTER JOIN ((SELECT DISTINCT TEMP.ExpenseAmt,Temp.CustomerID,Temp.POLegID,Temp.POLogID,PE.IsBilling,PE.AccountID FROM 
								(SELECT SUM(ExpenseAMT)ExpenseAmt,PL.POLegID,PL.POLogID,PL.CustomerID 
								FROM PostflightExpense PE 
								INNER JOIN PostflightLeg PL ON PE.POLegID=PL.POLegID AND PL.IsDeleted = 0 
								INNER JOIN PostflightMain PM ON PL.POLogID=PM.POLogID AND PM.IsDeleted = 0
								WHERE PE.IsBilling=1
								AND PE.IsDeleted = 0
								GROUP BY PL.POLogID,PL.CustomerID,PL.POLegID)Temp
								INNER JOIN PostflightExpense PE ON PE.POLegID=Temp.POLegID))POE ON POL.POLegID = POE.POLegID AND POL.CustomerID=POE.CustomerID AND POL.POLogID=POE.POLogID
LEFT OUTER JOIN Fleet F ON F.FleetID = POM.FleetID
INNER JOIN Aircraft AF ON AF.AircraftID = F.AircraftID
LEFT OUTER JOIN Airport AD ON POL.DepartICAOID = AD.AirportID
LEFT OUTER JOIN Airport AA ON POL.ArriveICAOID = AA.AirportID
--LEFT OUTER JOIN vDeptAuthGroup DE ON POL.DepartmentID = DE.DepartmentID AND POL.AuthorizationID=DE.AuthorizationID AND POL.CustomerID = DE.CustomerID
LEFT OUTER JOIN Department DEM ON POM.DepartmentID = DEM.DepartmentID AND POL.CustomerID = DEM.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DAM ON POM.AuthorizationID = DAM.AuthorizationID AND POL.CustomerID = DAM.CustomerID
LEFT OUTER JOIN Department DEL ON  POL.DepartmentID = DEL.DepartmentID AND POL.CustomerID = DEL.CustomerID
LEFT OUTER JOIN DepartmentAuthorization DAL ON POL.AuthorizationID = DAL.AuthorizationID AND POL.CustomerID = DAL.CustomerID
LEFT OUTER JOIN FleetChargeRate FCR ON F.FleetID = FCR.FleetID AND F.CustomerID = FCR.CustomerID AND POM.EstDepartureDT BETWEEN FCR.BeginRateDT AND FCR.EndRateDT
INNER JOIN @TempFleetID T ON T.FleetID = F.FleetID
LEFT OUTER JOIN Account A ON POE.AccountID = A.AccountID AND POE.CustomerID = A.CustomerID
LEFT OUTER JOIN PostflightPassenger POP ON POL.POLegID=POP.POLegID
LEFT OUTER JOIN Passenger P ON POP.PassengerID=P.PassengerRequestorID
LEFT OUTER JOIN Department PaxD ON PaxD.DepartmentID = P.DepartmentID
LEFT OUTER JOIN DepartmentAuthorization PaxA ON PaxA.AuthorizationID = P.AuthorizationID
LEFT OUTER JOIN (SELECT PE1.POLegID,AccountDescription=(SELECT DISTINCT AccountDescription +'***' FROM PostflightExpense PE 
									  INNER JOIN Account A ON PE.AccountID = A.AccountID
								  WHERE PE.IsDeleted = 0
								  AND PE.POLegID=PE1.POLegID
								  FOR XML PATH('')
					)
			,ExpenseAMT=(SELECT DISTINCT CONVERT(VARCHAR(30),PE.ExpenseAMT) +' ' FROM PostflightExpense PE 
												  INNER JOIN Account A ON PE.AccountID = A.AccountID
											  WHERE PE.IsDeleted = 0
											  AND PE.POLegID=PE1.POLegID
											  FOR XML PATH('')
								)
					FROM PostflightExpense PE1
								)EX ON EX.POLegID = POL.POLegID 
LEFT OUTER JOIN Passenger Req ON Req.PassengerRequestorID = POM.PassengerRequestorID
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID) 
	AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')
    AND (DEL.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
    AND (DAL.AuthorizationCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AuthorizationCD, ',')) OR @AuthorizationCD = '')
	AND (POM.HomebaseID = CONVERT(BIGINT,@UserHomebaseID) OR @IsHomebase = 0)
	AND POM.IsDeleted = 0
END      




GO


