IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTNonPilotLogCheckListInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTNonPilotLogCheckListInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





       CREATE Procedure [dbo].[spGetReportPOSTNonPilotLogCheckListInformation]
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@TypeCode AS NVARCHAR(1000) = '',
		@CrewID AS VARCHAR(1000), --Mandatory
		@ChecklistCode AS NVARCHAR(1000) = ''
		
		
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTNonPilotLogCheckListInformation
-- Author:  D.Mullai
-- Create date: 
-- Description: Get Postflight NonPilot Log for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
		
	CREATE TABLE #NONPILOT(DateFROM DATE,DATETO DATE,CHECKLISTCODE CHAR(3),ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,crewid VARCHAR(100),CREW VARCHAR(5),case1 VARCHAR(25))

CREATE TABLE #NONPILOT1(DateFROM DATE,DATETO DATE,CHECKLISTCODE CHAR(3),ChecklistItem VARCHAR(25),ChecklistLast DATE,ChecklistDue DATE,
	ChecklistAlert DATE,ChecklistGrace DATE,crewid VARCHAR(100),CREW VARCHAR(5),case1 VARCHAR(25))

	INSERT INTO #NONPILOT
	     SELECT DISTINCT
	        [DateFROM]=CONVERT(DATE,@DATEFROM),
	        [DATETO]= dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
	        [CHECKLISTCODE] = NULL,   
			[ChecklistItem]=NULL,
			[ChecklistLast]=NULL,
			[ChecklistDue]=NULL,
			[ChecklistAlert]=NULL,
			[ChecklistGrace]=NULL,
			[crewid]=c.crewid,[CREW]=C.CREWCD,
			[case1]=  NULL
			  	FROM PostflightLeg PL 
		
		INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
		INNER JOIN Crew C ON C.CrewID = PC.CrewID
		
	 
		WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
		  AND PL.IsDeleted=0
		  AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
		  AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
				 
	AND PC.Dutytype IN('P','S') AND PC.Dutytype is null 
	--and c.IsDeleted = 0 AND c.IsStatus = 1 
	UNION 
	SELECT DISTINCT
	        [DateFROM]=CONVERT(DATE,@DATEFROM),
	        [DATETO]= dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),  
	        [CHECKLISTCODE] = CCL.CrewCheckCD, 
			[ChecklistItem]=ccl.CrewChecklistDescription,
			[ChecklistLast]=dbo.GetShortDateFormatByUserCD(@UserCD,CCD.PreviousCheckDT),
			[ChecklistDue]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.DueDT),
			[ChecklistAlert]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.AlertDT),
			[ChecklistGrace]= dbo.GetShortDateFormatByUserCD(@UserCD,CCD.GraceDT),
			[crewid]=c.crewid,[CREW]=C.CREWCD,
			[case1]=  (CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
						   WHEN (@DATETO > CCD.DUEDT) THEN 'PASSED DUE' END)
			  	FROM PostflightLeg PL 
		INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted=0
		INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
		INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID
		INNER JOIN Crew C ON C.CrewID = PC.CrewID
		INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and CCD.CustomerID=ccl.CustomerID
	    LEFT OUTER JOIN  Aircraft AC ON AC.AircraftID = PM.AircraftID  
		WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				 AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
				 AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
				 AND PL.IsDeleted=0
				 AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
	AND PC.Dutytype NOT IN('P','S') 
	AND (AC.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TypeCode, ',')) OR @TypeCode = '')  
	--and c.IsDeleted = 0 AND c.IsStatus = 1 
	
	INSERT INTO #NONPILOT1
	     SELECT DISTINCT
	        [DateFROM]=CONVERT(DATE,@DATEFROM),
	        [DATETO]= dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO),
	        [CHECKLISTCODE] = NULL,   
			[ChecklistItem]=NULL,
			[ChecklistLast]=NULL,
			[ChecklistDue]=NULL,
			[ChecklistAlert]=NULL,
			[ChecklistGrace]=NULL,
			[crewid]=c.crewid,[CREW]=C.CREWCD,
			[case1]=  NULL
			  	FROM CREW C
			  	
			  	WHERE C.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			  	--and c.IsDeleted = 0 AND c.IsStatus = 1  
			  	AND (C.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
            delete from #NONPILOT1 where crew in(select distinct crew from #NONPILOT)
           
SELECT * FROM #NONPILOT1
UNION ALL           
SELECT * FROM #NONPILOT

IF OBJECT_ID('tempdb..#NONPILOT1') IS NOT NULL
DROP TABLE #NONPILOT1
IF OBJECT_ID('tempdb..#NONPILOT') IS NOT NULL
DROP TABLE #NONPILOT	
   -- EXEC spGetReportPOSTNonPilotLogCheckListInformation 'jwilliams_13','2009/1/1','2009/12/12','','','100'




GO


