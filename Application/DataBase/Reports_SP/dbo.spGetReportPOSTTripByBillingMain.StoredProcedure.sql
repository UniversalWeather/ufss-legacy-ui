IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTTripByBillingMain]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTTripByBillingMain]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTTripByBillingMain]
(
	@UserCD AS VARCHAR(30), --Mandatory
	@UserCustomerID AS VARCHAR(30),--Mandatory
	@DATEFROM AS DATE ,--Mandatory
	@DATETO AS DATE , --Mandatory
	@LogNum AS VARCHAR(200) = '',
	@PassengerRequestorCD  AS VARCHAR(200) = '',
	@PassengerGroupCD VARCHAR(500)='' 
)
AS
-- ================================================================================   
-- SPC Name: [spGetReportPOSTTripByBillingMain]          
-- Author: A.AKHILA         
-- Create date: 09/03/2013          
-- Description: Get Trip Detail by billing MAIN for REPORTS          
-- Revision History          
-- Date   Name  Ver  Change          
-- ================================================================================   
BEGIN

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------
IF @PassengerRequestorCD <> '' OR @PassengerGroupCD <>''
BEGIN

SELECT DISTINCT POM.LogNum FROM PostflightMain POM
							LEFT OUTER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
							INNER JOIN PostflightPassenger POP ON POP.POLegID = POL.POLegID
							INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
							INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
							INNER JOIN Fleet F ON F.FleetID=POM.FleetID AND F.IsDeleted=0 AND F.IsInActive=0
WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND POM.IsDeleted=0
  AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')

END

ELSE


BEGIN

SELECT DISTINCT POM.LogNum FROM PostflightMain POM
							INNER JOIN PostflightLeg POL ON POL.POLogID = POM.POLogID AND POL.IsDeleted=0
							INNER JOIN Fleet F ON F.FleetID=POM.FleetID AND F.IsDeleted=0 AND F.IsInActive=0

WHERE POM.CustomerID = CONVERT(BIGINT,@UserCustomerID)
  AND POM.IsDeleted=0
  AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
  AND (POM.LogNum IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@LogNum, ',')) OR @LogNum = '')


END

END
--EXEC spGetReportPOSTTripByBillingMain 'SUPERVISOR_99','10099','2009/1/1','2009/1/14',''



GO


