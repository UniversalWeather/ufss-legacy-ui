IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportAirportHotelInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportAirportHotelInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportAirportHotelInformation]
	@UserCD varchar(30),
	@IcaoID char(4)
AS
-- =============================================
-- SPC Name: spGetReportAirportHotelInformation
-- Author: SUDHAKAR J
-- Create date: 12 Jun 2012
-- Description: Get Airport Hotel Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

		SELECT 
			H.CustomerID, H.IsCrew, H.IsPassenger, H.Name, 
			H.PhoneNum, H.HotelRating, H.MilesFromICAO
		FROM Hotel H
		INNER JOIN Airport A ON H.AirportID = A.AirportID
		WHERE H.IsDeleted = 0
		  AND H.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
		  AND A.IcaoID = @IcaoID
		
		ORDER BY H.Name
  -- EXEC spGetReportAirportHotelInformation 'EDE088D3-DE29-4A95-96AD-1B3EE2E5D87D', '2222'
  -- EXEC spGetReportAirportHotelInformation 'TIM', 'KHOU'


GO


