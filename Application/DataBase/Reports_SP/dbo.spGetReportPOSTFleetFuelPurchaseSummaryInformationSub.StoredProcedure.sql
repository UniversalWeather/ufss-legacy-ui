 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformationSub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformationSub]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

         
      
CREATE PROCEDURE [dbo].[spGetReportPOSTFleetFuelPurchaseSummaryInformationSub]        
 (        
  @UserCD AS VARCHAR(30),  --Mandatory        
  @DATEFROM DATETIME, --Mandatory        
  @DATETO DATETIME, --Mandatory        
  @TailNum CHAR(6)--Mandatory         
 )        
---- =============================================        
---- Author: Mathes        
---- Create date: 31-07-2012        
---- Description: Fleet Fuel Purchase Summary Sub- Report         
---- =============================================        
AS        
SET NOCOUNT ON        
BEGIN        
 Declare @FleetID BigInt
 Declare @AccountID BigInt
 Declare @ExpAmt Numeric(17,2)
 
 IF OBJECT_ID('tempdb..#Tmp') is not null                          
   DROP table #Tmp  
 Create Table #Tmp (AccountID BigInt,AccountDescription Varchar(50),FleetID BigInt,ExpenseAMT Numeric(17,2),PurchaseDT DateTime,IsAutomaticCalculation Bit )                
 
 IF OBJECT_ID('tempdb..#Tmp1') is not null                          
   DROP table #Tmp1  
 Create Table #Tmp1 (AccountID BigInt,FleetID BigInt)                
       
	Insert Into #Tmp SELECT DISTINCT Account.AccountID, Account.AccountDescription,PostflightExpense.FleetID, 0.00  As Total,         
		   PostflightExpense.PurchaseDT,PostFlightExpense.IsAutomaticCalculation 
	FROM   PostflightExpense INNER JOIN Account ON PostflightExpense.AccountID = Account.AccountID        
		   INNER JOIN FLEET  ON Fleet.FleetID = PostflightExpense.FleetID 
    WHERE PostflightExpense.IsDeleted=0 
	GROUP By  Account.AccountID,Account.AccountDescription, PostflightExpense.CustomerID,       
		   PostflightExpense.FleetID  ,Fleet.TailNum, PostflightExpense.PurchaseDT,PostFlightExpense.IsAutomaticCalculation       
	HAVING PostFlightExpense.IsAutomaticCalculation=0 And PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And 
		   Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO)     
		   AND Fleet.TailNum=@TailNum Order By PostflightExpense.PurchaseDT    
              
     Insert Into #Tmp1 Select AccountID,FleetID From #Tmp 
	 WHILE ( SELECT  COUNT(*) FROM #Tmp1 ) > 0                 
	 BEGIN            
	   Select Top 1 @AccountID=AccountID,@FleetID=FleetID From #Tmp1    
	   
	   Set @ExpAmt=(SELECT IsNull(SUM(PostflightExpense.ExpenseAMT),0)   
	   FROM   PostflightExpense     
	   WHERE  PostflightExpense.CustomerID=dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)) And 
			  Convert(Date,PostflightExpense.PurchaseDT) BETWEEN Convert(Date,@DATEFROM) AND Convert(Date,@DATETO)     
			  AND PostflightExpense.FleetID=@FleetID And PostflightExpense.AccountID=@AccountID AND PostflightExpense.IsDeleted=0)
	          
	   IF @ExpAmt>0
	   BEGIN
		Update #Tmp Set ExpenseAMT=@ExpAmt Where AccountID=@AccountID AND FleetID=@FleetID 
	   END
	   
	   Delete From #Tmp1 Where AccountID=@AccountID
     END      
   
   Select AccountDescription,ExpenseAMT Total,PurchaseDT From #Tmp 
  
   IF OBJECT_ID('tempdb..#Tmp1') is not null                          
   DROP table #Tmp1  
   IF OBJECT_ID('tempdb..#Tmp') is not null                          
   DROP table #Tmp  
   --Exec spGetReportPOSTFleetFuelPurchaseSummaryInformationSub 'jwilliams_13','01-Jan-2000','31-Dec-2012','N771AV'           
END  
 
 