IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTFlightOperationsSummaryInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTFlightOperationsSummaryInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ===============================================================================
-- SPC Name: spGetReportPOSTFlightOperationsSummaryInformation
-- Author:Badrinath/Mullai.D
-- Create date: 01 August 2012
-- Description: Get Flight Operation Information for REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

CREATE PROCEDURE [dbo].[spGetReportPOSTFlightOperationsSummaryInformation]
@UserCD AS VARCHAR(30)-- Mandatory  
,@DATEFROM datetime-- Mandatory  
,@DATETO datetime-- Mandatory  
,@TailNumber nvarchar(1000)=''  
,@FleetGroupCD nvarchar(1000)=''  
,@IsHomebase As bit=0
,@SortBy as varchar(1)
--,@PrintNauticalMiles as bit=0  
--,@PrintStatuteMiles as bit=0  
AS  
SET NOCOUNT ON  

DECLARE @IsZeroSuppressActivityAircftRpt BIT;
SELECT @IsZeroSuppressActivityAircftRpt = IsZeroSuppressActivityAircftRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @TenToMin Int
SET @TenToMin=(SELECT Company.TimeDisplayTenMin From Company Where CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)
                       And Company.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   
DECLARE @units VARCHAR(15) 
SET @units=(SELECT convert(VARCHAR,(CASE WHEN (cp.FuelPurchase='1') THEN 'Gallons'
                WHEN (cp.FuelPurchase='2') THEN 'LTR'
				WHEN (cp.FuelPurchase='3') THEN 'IMP GAL'
				WHEN (cp.FuelPurchase='4') THEN 'POUNDS'
				END)) from Company cp where 
       CustomerID =dbo.GetCustomerIDbyUserCD(@UserCD)  And Cp.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD))   

-----------------------------TailNum and Fleet Group Filteration----------------------

DECLARE @TempFleetID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNumber <> ''
BEGIN
	INSERT INTO @TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNumber, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNumber = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO @TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER
	AND F.IsDeleted = 0
	AND F.IsInActive = 0  
END
-----------------------------TailNum and Fleet Group Filteration----------------------	

CREATE TABLE #fleettemp (DateRange VARCHAR(30),TailNumber VARCHAR(9),typecode VARCHAR(26),
POLogID bigint,NoofTrips bigint,NoofLogs bigint,NoofLegs bigint,NoofDays bigint,BlockHours numeric(6,3),
FlightHours numeric(6,3),TenToMin numeric(1,0),AvgBlkHrsLeg numeric(16,3),AvgFltHrs numeric(16,3),NauticalMiles numeric(16,9),
StatuteMiles numeric(16,9),AvgMILeg numeric(16,3),NoofPax numeric(5,0),FuelBurn numeric(7,0),AvgFBHr numeric(15,3),units VARCHAR(15),
Gallons numeric(6,0),TotalCost numeric(17,5),AvgPrice numeric(17,2));

CREATE TABLE #fleettemp1 (DateRange VARCHAR(30),TailNumber VARCHAR(9),typecode VARCHAR(26),
POLogID bigint,NoofTrips bigint,NoofLogs bigint,NoofLegs bigint,NoofDays bigint,BlockHours numeric(6,3),
FlightHours numeric(6,3),TenToMin numeric(1,0),AvgBlkHrsLeg numeric(16,3),AvgFltHrs numeric(16,3),NauticalMiles numeric(16,9),
StatuteMiles numeric(16,9),AvgMILeg numeric(16,3),NoofPax numeric(5,0),FuelBurn numeric(7,0),AvgFBHr numeric(15,3),units VARCHAR(15),
Gallons numeric(6,0),TotalCost numeric(17,2),AvgPrice numeric(17,2));

CREATE TABLE #FLEETDAYS(TailNumber VARCHAR(9),POLogID bigint,
--leg VARCHAR(20),SCHEDULETM DATE,
minsche date,maxsche date);

CREATE TABLE #FLEETDAYS1(TailNumber VARCHAR(9),POLogID bigint,
--leg VARCHAR(20),SCHEDULETM DATE,
minsche date,maxsche date,NoofDays int);

INSERT INTO #FLEETDAYS
SELECT DISTINCT
[TailNumber] = F.TailNUM,  
[POLogID]=pl.pologid,
minsche=min(pl.ScheduledTM),
maxsche=max(pl.ScheduledTM)


FROM PostflightLeg PL
INNER JOIN PostFlightMain PM ON PM.POLogID=PL.POLogID AND PM.IsDeleted = 0  
INNER JOIN (SELECT POLOGID,(datediff(day,min(ScheduledTM),MAX(ScheduledTM))+1) Ron1 FROM PostflightLeg WHERE IsDeleted = 0 GROUP BY POLogID)PL1 ON PM.POLogID=PL1.POLOGID

 INNER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      left outer JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      left outer JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                      WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
                                      OR @FleetGroupCD = ''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
                      WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
AND PL.IsDeleted = 0 
group by f.TailNum,PL.POLogID--,PL.POLegID,PL.ScheduledTM
order by TailNumber                    
 


;WITH device_recording_row_num AS
(
    SELECT 
	ROW_NUMBER() OVER(ORDER BY A.TailNumber,A.minsche ) AS row_num,A.POLogID,A.minsche,A.maxsche,A.TailNumber
 FROM #FLEETDAYS A      
)


insert into #FLEETDAYS1 
SELECT
    [current].TailNumber,
    [current].POLogID,
    [current].minsche,
    [current].maxsche,
    (Case When [current].minsche=previous.maxsche then DateDiff(day,[current].minsche,[current].maxsche) 
          Else   DateDiff(day,[current].minsche,[current].maxsche)+1 End) as Noofdays
FROM 
    device_recording_row_num AS [current]
LEFT OUTER JOIN
    device_recording_row_num AS previous ON [current].TailNumber = previous.TailNumber AND [current].row_num = previous.row_num + 1

ORDER BY
    [current].TailNumber, [current].minsche




INSERT INTO #fleetTEMP

SELECT TEMP.DateRange
	  ,TEMP.TailNumber
	  ,TEMP.typecode
	  ,TEMP.POLogID
	  ,COUNT(DISTINCT TEMP.NoofTrips) NoofTrips
	  ,COUNT(DISTINCT NoofLogs) NoofLogs
	  ,COUNT(DISTINCT TEMP.NoofLegs) as NoofLegs
	  ,TEMP.NoofDays
	  ,SUM(TEMP.BlockHours) AS BlockHours
	  ,SUM(TEMP.FlightHours) AS FlightHours
	  ,TEMP.TenToMin
	  ,FLOOR(ISNULL((CASE(ISNULL(COUNT(TEMP.NoofLegs),0)) WHEN 0 THEN 0 ELSE SUM(TEMP.BlockHours)/COUNT(DISTINCT TEMP.NoofLegs) END),0)*1000)*0.001  AS AvgBlkHrsLeg
	  ,FLOOR(ISNULL((CASE(ISNULL(COUNT(TEMP.NoofLegs),0)) WHEN 0 THEN 0 ELSE SUM(TEMP.FlightHours)/COUNT(DISTINCT TEMP.NoofLegs) END),0)*1000)*0.001  AS AvgFltHrs
	  ,SUM(TEMP.NauticalMiles) AS NauticalMiles
	  ,SUM(TEMP.StatuteMiles) AS StatuteMiles
	  ,FLOOR(ISNULL(SUM(TEMP.AvgMILeg),0)*1000)*0.001  AS AvgMILeg
	  ,SUM(TEMP.NoofPax) AS NoofPax
	  ,SUM(TEMP.FuelBurn) AS FuelBurn
	  ,FLOOR(ISNULL(SUM(TEMP.AvgFBHr),0)*1000)*0.001  AS AvgFBHr
	  ,TEMP.Units,SUM(TEMP.Gallons) AS Gallons
	  ,SUM(TEMP.TotalCost) AS TotalCost
	  ,FLOOR(ISNULL(SUM(TEMP.AvgPrice),0)*100)*0.01 AS AvgPrice  FROM (
SELECT a.DateRange
	  ,A.TailNumber
	  ,A.typecode
	  ,A.POLogID
	  , NoofTrips
	  ,NoofLogs
	  ,NoofLegs
	  ,A.NoofDays
	  ,BlockHours=CASE WHEN RNK=1 THEN BlockHours ELSE 0 END
	  ,FlightHours=CASE WHEN RNK=1 THEN FlightHours ELSE 0 END
	  ,a.TenToMin
	  ,AvgBlkHrsLeg
	  ,AvgFltHrs
	  ,NauticalMiles=CASE WHEN RNK=1 THEN NauticalMiles ELSE 0 END
	  ,StatuteMiles=CASE WHEN RNK=1 THEN StatuteMiles ELSE 0 END
	  ,AvgMILeg=CASE WHEN RNK=1 THEN AvgMILeg ELSE 0 END
	  ,NoofPax=CASE WHEN RNK=1 THEN NoofPax ELSE 0 END
	  ,FuelBurn=CASE WHEN RNK=1 THEN FuelBurn ELSE 0 END
	  ,AvgFBHr=CASE WHEN RNK=1 THEN AvgFBHr ELSE 0 END
	  ,A.Units,Gallons
	  ,TotalCost
	  ,AvgPrice 
FROM 
(
 SELECT  DISTINCT
[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO),
[TailNumber] = F.TailNUM,  
[typecode]=f.AircraftCD,
[POLogID]=pl.pologid, 
[NoofTrips]= (case when PM.TripID is null then COUNT(PM.LogNUM) else COUNT(PM.TripID) end),
[NoofLogs] = Pm.LogNUM,  
[NoofLegs] = PL.legnum,
[NoofDays] =PL1.Ron1,  
[BlockHours] = ISNULL(ROUND(PL.BlockHours,1),0),  
[FlightHours] = ISNULL(ROUND(PL.FlightHours,1),0),  
[TenToMin]=@TenToMin,
[AvgBlkHrsLeg] = 0,	 
[AvgFltHrs] = 0,
[NauticalMiles] =PL.Distance,
[StatuteMiles] =((PL.Distance)*1.15078),
[AvgMILeg]= case(ISNULL(COUNT(PL.TripLEgID),0)) when 0 then 0 ELSE ISNULL(PL.Distance,0)/COUNT(PL.TripLegID) END ,  --IsNULL(PL.Distance/COUNT(PL.TripLEgID),0),  
[NoofPax] =PL.PassengerTotal,  
[FuelBurn] = PL.FuelUsed,  
[AvgFBHr] = case(ISNULL(pl.FlightHours,0)) when 0 then 0 ELSE ISNULL(pl.FuelUsed,0)/NULLIF(ROUND(pl.FlightHours,1),0) END ,  --IsNULL(PL.FuelUsed/PL.FlightHours,0),  
[Units] = @units,
[Gallons]= case when (pe.PurchaseDT>= @DATEFROM or pe.PurchaseDT<=@DATETO) then (CAST(ROUND((dbo.GetFuelConversion(PE.FuelPurchase,PE.FuelQTY,@UserCD)),1,1) AS NUMERIC(18,1))) end,  
[TotalCost]=PE.ExpenseAMT,

[AvgPrice]= Case WHEN Cp.FuelPurchase <> Pe.FuelPurchase and (ISNULL(pe.FuelQTY,0))<>0 THEN DBO.GetFuelPriceConversion(PE.FuelPurchase,ISNULL(pe.ExpenseAMT,0)/pe.FuelQTY,@UserCD)
                                               ELSE Pe.UnitPrice End

  
,ROW_NUMBER()OVER(PARTITION BY PL.POLEGID ORDER BY PL.POLEGID) Rnk 
FROM PostFlightLeg PL  
INNER JOIN PostFlightMain PM ON PM.POLogID=PL.POLogID AND PM.IsDeleted = 0 
INNER JOIN (SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = PM.FleetID
INNER JOIN (SELECT POLogID,NoofDays Ron1 FROM #FLEETDAYS1 GROUP BY POLogID,NoofDays)PL1 ON PM.POLogID=PL1.POLOGID 
LEFT OUTER JOIN PostFlightExpense PE ON PL.POLEgID=PE.POLEgID  
INNER JOIN (
                                      SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNUM, F.AircraftCD, F.HomeBaseID,F.MaximumPassenger
                                      FROM Fleet F
                                      LEFT OUTER JOIN FleetGroupOrder FGO
                                      ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
                                      LEFT OUTER JOIN FleetGroup FG
                                      ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
                                      --WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ','))
                                      --OR @FleetGroupCD = ''
                      ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID

LEFT OUTER JOIN Company cp ON cp.HomebaseID = f.HomebaseID 
LEFT OUTER JOIN Airport a ON a.AirportID = cp.HomebaseAirportID 
LEFT OUTER JOIN UserMaster um on um.HomebaseID=pm.HomebaseID and um.CustomerID=PM.CustomerID
AND UM.USERNAME= @USERCD
WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))  
AND CONVERT(DATE,PL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
AND (CP.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)
AND PL.IsDeleted = 0
GROUP BY F.TailNUM,pl.pologid,PL.PassengerTotal,PL.FlightHours,cp.TimeDisplayTenMin,PL.FuelUsed,cp.FuelPurchase,PL.StatuteMiles,PM.LogNum,PL.ScheduledTM,PL.NextDTTM,
PL.FuelIn,PL.FlightCost,PE.ExpenseAMT,PL.Distance,PE.FuelQTY,PL.BlockHours,PL.FlightHours,PL.InboundDTTM,PL.OutboundDTTM,PE.PurchaseDT,PL1.Ron1,PE.FuelPurchase,f.AircraftCD,
PE.UnitPrice,PM.TripID,PM.LogNum,PL.LegNUM,PL.POLEGID )A
)TEMP
GROUP BY TEMP.TailNumber,TEMP.POLogID,TEMP.NoofLogs,TEMP.NoofTrips,TEMP.TenToMin,TEMP.DateRange,TEMP.Units, TEMP.Noofdays,TEMP.typecode
ORDER BY typecode

INSERT INTO #fleettemp1
SELECT DISTINCT 
[DateRange] = dbo.GetDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+ dbo.GetDateFormatByUserCD(@UserCD,@DATETO), 
[TailNumber] = F.TailNUM, 
[typecode]=f.AircraftCD, 
[POLogID]=NULL,
[NoofTrips]= NULL,  
[NoofLogs] = NULL,  
[NoofLegs] = 0,
[NoofDays] = NULL,  
[BlockHours] = 0,  
[FlightHours] = 0,
[TenToMin]=@TenToMin,  
[AvgBlkHrsLeg] =  0 ,	 
[AvgFltHrs] = 0,
[NauticalMiles] =NULL,
[StatuteMiles] =NULL,
[AvgMILeg]= NULL,  
[NoofPax] =NULL,  
[FuelBurn] =NULL,  
[AvgFBHr] = NULL, 
[Units] =@units,

[Gallons]= NULL,  
[TotalCost]=NULL,

[AvgPrice]= NULL

FROM Fleet f
INNER JOIN  ( SELECT DISTINCT FLEETID FROM @TempFleetID ) F1  ON F1.FleetID = f.FleetID
LEFT OUTER JOIN Company cp ON cp.HomebaseID = f.HomebaseID 

        WHERE f.CustomerID=CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
          -- AND f.TailNum NOT IN (SELECT DISTINCT t.TailNum FROM #temp t)
         --AND (F.TailNUM IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNumber, ',')) OR @TailNumber = '')
--AND (cp.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @IsHomebase = 0)  
order by typecode
delete from #fleettemp1 where TailNumber in(SELECT distinct TailNumber from #fleettemp)

IF @IsZeroSuppressActivityAircftRpt = 0
BEGIN         
SELECT  [DateRange], 
		[TailNumber], 
		[typecode], 
		[POLogID],
		[NoofTrips],  
		[NoofLogs],  
		[NoofLegs],
		[NoofDays],  
		[BlockHours],  
		[FlightHours],
		[TenToMin],  
		[AvgBlkHrsLeg],	 
		[AvgFltHrs],
		[NauticalMiles]=ROUND([NauticalMiles],0),
		[StatuteMiles]=ROUND([StatuteMiles],0),
		[AvgMILeg],  
		[NoofPax],  
		[FuelBurn],  
		[AvgFBHr], 
		[Units],
		[Gallons],  
		[TotalCost],
		[AvgPrice] FROM #fleettemp1
UNION ALL
SELECT  [DateRange], 
		[TailNumber], 
		[typecode], 
		[POLogID],
		[NoofTrips],  
		[NoofLogs],  
		[NoofLegs],
		[NoofDays],  
		[BlockHours],  
		[FlightHours],
		[TenToMin],  
		[AvgBlkHrsLeg],	 
		[AvgFltHrs],
		[NauticalMiles]=ROUND([NauticalMiles],0),
		[StatuteMiles]=ROUND([StatuteMiles],0),
		[AvgMILeg],  
		[NoofPax],  
		[FuelBurn],  
		[AvgFBHr], 
		[Units],
		[Gallons],  
		[TotalCost],
		[AvgPrice] FROM #fleettemp
END
ELSE
BEGIN
SELECT [DateRange], 
		[TailNumber], 
		[typecode], 
		[POLogID],
		[NoofTrips],  
		[NoofLogs],  
		[NoofLegs],
		[NoofDays],  
		[BlockHours],  
		[FlightHours],
		[TenToMin],  
		[AvgBlkHrsLeg],	 
		[AvgFltHrs],
		[NauticalMiles]=ROUND([NauticalMiles],0),
		[StatuteMiles]=ROUND([StatuteMiles],0),
		[AvgMILeg],  
		[NoofPax],  
		[FuelBurn],  
		[AvgFBHr], 
		[Units],
		[Gallons],  
		[TotalCost],
		[AvgPrice] FROM #fleettemp
END

IF OBJECT_ID('tempdb..#fleettemp') IS NOT NULL
DROP TABLE #fleettemp

IF OBJECT_ID('tempdb..#fleettemp1') IS NOT NULL
DROP TABLE #fleettemp1

IF OBJECT_ID('tempdb..#FLEETDAYS') IS NOT NULL
DROP TABLE #FLEETDAYS

IF OBJECT_ID('tempdb..#FLEETDAYS1') IS NOT NULL
DROP TABLE #FLEETDAYS1

--EXEC spGetReportPOSTFlightOperationsSummaryInformation 'jwilliams_11','2011-01-01','2011-12-12','','',1,'b'










GO


