IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSGeneralDeclarationSub1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRETSGeneralDeclarationSub1]  
 @LegID AS VARCHAR(30)               -- Mandatory    
AS
-- ==========================================================================================
-- SPC Name: spGetReportPRETSGeneralDeclarationSub1
-- Author: ABHISHEK.S
-- Create date: 13th September 2012
-- Description: Get TripSheetReportWriter General Declaration Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
DECLARE @TripID BIGINT

SET @TripID=(SELECT DISTINCT  TripID FROM PreflightLeg WHERE LegID=@LegID)

SELECT ICAO1=ISNULL(P.[1], '') 
	  ,ICAO2=ISNULL(P.[2], '')
	  ,ICAO3=ISNULL(P.[3], '')
	  ,ICAO4=ISNULL(P.[4], '')
	  ,ICAO5=ISNULL(P.[5], '') FROM (
SELECT DISTINCT IcaoID=IcaoID
			  ,PL.LegNum Rnk
			   FROM PreflightMain PM
									INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID
									INNER JOIN Airport D ON PL.DepartICAOID=D.AirportID
							   WHERE PM.TripID=@TripID
)I
PIVOT
(
    MAX(IcaoID)
    FOR Rnk IN ([1], [2], [3], [4], [5])
) AS P
 -- EXEC  spGetReportPRETSGeneralDeclaration 'TIM', '', '', '', '2011-07-20', '2012-07-21', '', '', '', '', '', '', '',1,1,1,1,1,1 


GO


