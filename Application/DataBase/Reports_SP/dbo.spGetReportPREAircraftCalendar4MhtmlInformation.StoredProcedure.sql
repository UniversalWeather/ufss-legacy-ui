IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar4MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar4MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar4MhtmlInformation]
	@UserCD VARCHAR(30)-- Mandatory
	,@DateFrom DATETIME -- Mandatory
	,@DateTo DATETIME --Mandatory
	,@TailNum VARCHAR(500) = '' --Optional
	,@FleetGroupCD VARCHAR(500) = '' --Optional
AS
--==================================================================
--SPC Name: spGetReportPREAircraftCalendar4Information
--AUTHOR: AISHWARYA.M
--Create Date: 14/08/2012
--DESCRIPTION: Get Aircraft Calendar EXPORT information for REPORTS
--Revision History
--Date			Name		Ver		Change
--
--==================================================================
SET NOCOUNT ON

DECLARE @FromDate DATETIME
DECLARE @ToDate DATETIME
DECLARE @Count INT
DECLARE @index INT
DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)
DECLARE @Loopdate DATETIME

SET @FromDate = @DateFrom
SET @ToDate = @DateTo

DECLARE @TempTailNum TABLE (
	ID INT identity(1, 1) NOT NULL
	,TailNum VARCHAR(25)
	)

SET @String = @TailNum
SET @Delimiter = ','
SET @String = @String + @Delimiter
SET @Pos = charindex(@Delimiter, @String)

IF (@TailNUM = '')
BEGIN
	INSERT INTO @TempTailNum (TailNum)
	SELECT TailNum
	FROM Fleet
	WHERE Fleet.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		--AND Fleet.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))
END
ELSE
BEGIN
	WHILE (@pos <> 0)
	BEGIN
		SET @NextString = substring(@String, 1, @Pos - 1)

		INSERT INTO @TempTailNum
		VALUES (@NextString)

		SET @String = substring(@String, @pos + 1, len(@String))
		SET @pos = charindex(@Delimiter, @String)
	END
END

DECLARE @TempTable TABLE(
		activedate NVARCHAR(25)
		,tail_nmbr CHAR(6)
		,aircraft_code CHAR(3)
		,dep_time DATETIME
		,arr_time DATETIME
		,depicao_id CHAR(4)
		,arricao_id CHAR(4)
		,pDate DATETIME
		,NextLocalDTTM DATETIME
		,DepartureDTTMLocal DATETIME
		,ArrivalDTTMLocal DATETIME
		,CustomerID BIGINT
		)
		
SET @Count = DATEDIFF(DAY, @FromDate, @ToDate)
SET @index = 0

WHILE (@index <= @Count)
BEGIN
	INSERT INTO @TempTable (
		pDate
		,activedate
		,tail_nmbr
		)
	SELECT CONVERT(VARCHAR(10), DATEADD(DAY, @index, @FromDate), 121)
		,LEFT(DATENAME(WEEKDAY, DATEADD(DAY, @index, @FromDate)), 3) + ' ' + RIGHT(CONVERT(VARCHAR(12), DATEADD(DAY, @index, @FromDate), 101), 10)
		,TT.TailNum
	FROM Fleet F
	INNER JOIN @TempTailNum TT ON F.TailNum = TT.TailNum
	WHERE F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)

	SET @index = @index + 1
END

		UPDATE @TempTable
		SET activedate = T.activedate
			,tail_nmbr = T.tail_nmbr
			,aircraft_code = T.aircraft_code
			,dep_time = T.dep_time
			,arr_time = T.arr_time
			,depicao_id =T.depicao_id
			,arricao_id = T.arricao_id
		FROM
			(SELECT PL.DepartureDTTMLocal AS activedate
			  ,F.TailNum AS tail_nmbr
			  ,F.AircraftCD AS aircraft_code 
			  ,PL.DepartureDTTMLocal AS dep_time 
			  ,PL.ArrivalDTTMLocal AS arr_time 
			  ,A.ICAOID AS depicao_id 
			  ,AA.IcaoID AS arricao_id
			  ,F.CustomerID
			  ,PL.DepartureDTTMLocal
			  FROM PreflightLeg PL
			  INNER JOIN (SELECT TripID, FleetID FROM PreflightMain WHERE IsDeleted = 0) PM ON PM.TripID = PL.TripID
			  INNER JOIN (SELECT AirportID, IcaoID FROM Airport) A ON A.AirportID = PL.DepartICAOID
			  INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON AA.AirportID = PL.ArriveICAOID
			  --INNER JOIN (SELECT FleetID, TailNum, AircraftCD, CustomerID FROM Fleet) F ON F.FleetID = PM.FleetID
			  --LEFT JOIN FleetGroupOrder FG ON F.FleetID = FG.FleetID
			  INNER JOIN (
			  		SELECT DISTINCT F.FleetID, F.TailNum, F.AircraftCD, F.CustomerID
					FROM Fleet F 
					LEFT OUTER JOIN FleetGroupOrder FGO
					ON F.FleetID = FGO.FleetID 
					LEFT OUTER JOIN FleetGroup FG 
					ON FGO.FleetGroupID = FG.FleetGroupID 
					WHERE ( FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
						OR @FleetGroupCD = '' ) 
					AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			  ) F ON PM.FleetID = F.FleetID
			  WHERE PL.IsDeleted = 0
			) AS T
			INNER JOIN @TempTable TEMP ON TEMP.tail_nmbr = T.tail_nmbr
			AND CONVERT(VARCHAR, TEMP.PDate, 101) = CONVERT(VARCHAR, T.DepartureDTTMLocal, 101)
			AND T.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			AND (T.tail_nmbr IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '' ) 
						
	DECLARE @activedate NVARCHAR(25)
		,@tail_nmbr CHAR(6)
		,@aircraft_code CHAR(3)
		,@dep_time DATETIME
		,@arr_time DATETIME
		,@depicao_id BIGINT
		,@arricao_id BIGINT
		,@NextLocalDTTM DATETIME
		,@DepartureDTTMLocal DATETIME
		,@ArrivalDTTMLocal DATETIME
		,@CustomerID BIGINT
	
	DECLARE curAircraftIVExp CURSOR
	FOR
	SELECT TT.activedate
		,TT.tail_nmbr
		,TT.aircraft_code
		,TT.dep_time
		,TT.arr_time
		,TT.depicao_id
		,TT.arricao_id
		,TT.CustomerID
		,TT.NextLocalDTTM
		,TT.DepartureDTTMLocal 
		,TT.ArrivalDTTMLocal 
	FROM @TempTable TT
	WHERE CONVERT(VARCHAR, TT.DepartureDTTMLocal, 101) < CONVERT(VARCHAR, TT.[NextLocalDTTM], 101)
		AND TT.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
	ORDER BY activedate

	OPEN curAircraftIVExp

	FETCH NEXT
	FROM curAircraftIVExp
	INTO @activedate
		,@tail_nmbr
		,@aircraft_code
		,@dep_time
		,@arr_time
		,@depicao_id
		,@arricao_id
		,@CustomerID
		,@NextLocalDTTM
		,@DepartureDTTMLocal 
		,@ArrivalDTTMLocal 
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @Loopdate = '';

	UPDATE @TempTable
	SET @activedate =  LEFT(DATENAME(WEEKDAY, @Loopdate), 3) + ' ' + RIGHT(CONVERT(VARCHAR(12), @Loopdate, 101), 9)
			,tail_nmbr = @tail_nmbr 
			,aircraft_code = @aircraft_code
			,dep_time = @arr_time
			,arr_time = CONVERT(DATETIME, CONVERT(VARCHAR(10), @arr_time, 101) + ' 23:59', 101)
			,depicao_id =  @arricao_id
			,arricao_id = @arricao_id
			,NextLocalDTTM = NULL 
	WHERE CONVERT(DATETIME, pDate, 121) = CONVERT(DATETIME, @Loopdate, 121)
		  AND tail_nmbr = @tail_nmbr

	SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
		WHILE DATEDIFF(DAY, @Loopdate, @NextLocalDTTM) > 0
		BEGIN
			UPDATE @TempTable
			SET @activedate =  LEFT(DATENAME(WEEKDAY, @Loopdate), 3) + ' ' + RIGHT(CONVERT(VARCHAR(12), @Loopdate, 101), 9)
			,tail_nmbr = @tail_nmbr 
			,aircraft_code = @aircraft_code
			,dep_time =  CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + '00:01', 101) 
			,arr_time = CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + '23:59', 101)
			,depicao_id = @depicao_id 
			,arricao_id = @arricao_id
			,NextLocalDTTM = @NextLocalDTTM 
			WHERE CONVERT(DATETIME, pDate, 121) = CONVERT(DATETIME, @Loopdate, 121)
				  AND tail_nmbr = @tail_nmbr

			SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
		END
		
	UPDATE @TempTable
	SET @activedate = @Loopdate
			,tail_nmbr = @tail_nmbr 
			,aircraft_code = @aircraft_code
			,dep_time = @arr_time
			,arr_time = CONVERT(DATETIME, CONVERT(VARCHAR(10), @arr_time, 101) + '00:01', 101)
			,depicao_id = @arricao_id 
			,arricao_id = @arricao_id
			,NextLocalDTTM = NULL 
	WHERE CONVERT(DATETIME, pDate, 121) = CONVERT(DATETIME, @Loopdate, 121)
		  AND tail_nmbr = @tail_nmbr
	
	FETCH NEXT
	FROM curAircraftIVExp
	INTO @activedate
		,@tail_nmbr
		,@aircraft_code
		,@dep_time
		,@arr_time
		,@depicao_id
		,@arricao_id
		,@CustomerID
		,@NextLocalDTTM
		,@DepartureDTTMLocal 
		,@ArrivalDTTMLocal 

END	
	CLOSE curAircraftIVExp;

	DEALLOCATE curAircraftIVExp;

SELECT activedate
        ,'' Crew
		,tail_nmbr
		,aircraft_code
		,dep_time
		,arr_time
		,depicao_id
		,arricao_id
		,'' Passengers
		FROM @TempTable
		
--EXEC spGetReportPREAircraftCalendar4MhtmlInformation 'jwilliams_13', '2012/10/31', '2012/10/31', '', ''


GO


