IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREWeeklyCalendar1MhtmlInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1MhtmlInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[spGetReportPREWeeklyCalendar1MhtmlInformation]                
  @UserCD VARCHAR(50)-- MANDATORY                
 ,@DateFrom DATETIME --MANDATORY                
 ,@DateTo DATETIME --MANDATORY                
 ,@TailNum VARCHAR(500) = ''--OPTIONAL                
 ,@FleetGroupCD VARCHAR(500) = ''--OPTIONAL                
AS                
                   
BEGIN                
-- ===============================================================================                
-- SPC Name: spGetReportWeeklyCalendar1Information                
-- Author: ASKAR               
-- Create date: 23 Jul 2012                
-- Description: Get Preflight Weekly Calendar1 information for REPORTS                
-- Revision History                
-- Date   Name  Ver  Change                
--                 
-- ================================================================================                
                
SET NOCOUNT ON                
 DECLARE @FromDate DATETIME                
 DECLARE @ToDate DATETIME                
 DECLARE @Count INT                
 DECLARE @index INT                
                  
 SET @FromDate = @DateFrom                
 SET @ToDate = @DateTo                
                 
 DECLARE @TempTailNum TABLE (                
  ID int identity(1,1) Not Null,                
  TailNum VARCHAR(25)                
 )                 
                 
 DECLARE @TempTable TABLE (          
   ID INT IDENTITY                
  ,WeekNumber INT                
  ,tail_nmbr CHAR(6)          
  ,type_code CHAR(3)          
  ,ac_code VARCHAR(10)          
  ,locdep DATETIME          
  ,Date1 DATE          
  ,crewlist1 VARCHAR(40)           
  ,reqcode1 VARCHAR(5)          
  ,orig_nmbr1 BIGINT          
  ,Date2 DATE          
  ,crewlist2 VARCHAR(40)           
  ,reqcode2 VARCHAR(5)          
  ,orig_nmbr2 BIGINT          
  ,Date3 DATE          
  ,crewlist3 VARCHAR(40)           
  ,reqcode3 VARCHAR(5)          
  ,orig_nmbr3 BIGINT          
  ,Date4 DATE          
  ,crewlist4 VARCHAR(40)           
  ,reqcode4 VARCHAR(5)          
  ,orig_nmbr4 BIGINT          
  ,Date5 DATE          
  ,crewlist5 VARCHAR(40)           
  ,reqcode5 VARCHAR(5)          
  ,orig_nmbr5 BIGINT          
  ,Date6 DATE          
  ,crewlist6 VARCHAR(40)           
  ,reqcode6 VARCHAR(5)          
  ,orig_nmbr6 BIGINT          
  ,Date7 DATE          
  ,crewlist7 VARCHAR(40)           
  ,reqcode7 VARCHAR(5)          
  ,orig_nmbr7 BIGINT          
  ,location1  CHAR(4)          
  ,arrtime1 VARCHAR(10)          
  ,deptime1 VARCHAR(10)          
  ,arrtimeapprox1 BIT          
  ,deptimeapprox1 BIT          
  ,pax_total1 INT          
  ,location2  CHAR(4)          
  ,arrtime2 VARCHAR(10)          
  ,deptime2 VARCHAR(10)          
  ,arrtimeapprox2 BIT          
  ,deptimeapprox2 BIT          
  ,pax_total2 INT          
  ,location3  CHAR(4)          
  ,arrtime3 VARCHAR(10)          
  ,deptime3 VARCHAR(10)          
  ,arrtimeapprox3 BIT          
  ,deptimeapprox3 BIT          
  ,pax_total3 INT          
  ,location4  CHAR(4)          
  ,arrtime4 VARCHAR(10)          
  ,deptime4 VARCHAR(10)          
  ,arrtimeapprox4 BIT          
  ,deptimeapprox4 BIT          
  ,pax_total4 INT          
  ,location5  CHAR(4)          
  ,arrtime5 VARCHAR(10)          
  ,deptime5 VARCHAR(10)          
  ,arrtimeapprox5 BIT          
  ,deptimeapprox5 BIT          
  ,pax_total5 INT          
  ,location6  CHAR(4)          
  ,arrtime6 VARCHAR(10)          
  ,deptime6 VARCHAR(10)          
  ,arrtimeapprox6 BIT          
  ,deptimeapprox6 BIT          
  ,pax_total6 INT          
  ,location7  CHAR(4)          
  ,arrtime7 VARCHAR(10)          
  ,deptime7 VARCHAR(10)          
  ,arrtimeapprox7 BIT          
  ,deptimeapprox7 BIT          
  ,pax_total7 INT          
  ,calcdate  DATE          
  ,date_1 DATE          
  ,date_2 DATE          
  ,date_3 DATE          
  ,date_4 DATE          
  ,date_5 DATE          
  ,date_6 DATE          
  ,date_7 DATE
  ,airportcode VARCHAR(10)
  ,airportdesc VARCHAR(10)
  ,airportcity VARCHAR(10)
  ,crewcode VARCHAR(10)
  ,crewdesc VARCHAR(10)
  ,authcode VARCHAR(10)
  ,authdesc VARCHAR(10) )                
                 
--SET @Count = DATEDIFF(DAY, @DateFrom, @DateTo)                
--SET @index = 0                
                
--WHILE (@index <= @Count)                
--BEGIN                
--  INSERT INTO @TempTable (                
--   WeekNumber     
--   ,date_1                
--   ,date_2                
--   ,date_3                
--   ,date_4               
--   ,Date_5                
--   ,date_6                
--   ,date_7             
--  )                
--  VALUES (                 
--   Cast((DATEDIFF(DAY,@DateFrom,DATEADD(DAY, @index, @FromDate))/7) as INT)+1                
--   ,DateAdd(DAY,0, DATEADD(DAY, @index, @FromDate))                
--   ,DateAdd(DAY,1, DATEADD(DAY, @index, @FromDate))                
--   ,DateAdd(DAY,2, DATEADD(DAY, @index, @FromDate))                
--   ,DateAdd(DAY,3, DATEADD(DAY, @index, @FromDate))             
--   ,DateAdd(DAY,4, DATEADD(DAY, @index, @FromDate))                
--   ,DateAdd(DAY,5, DATEADD(DAY, @index, @FromDate))                
--   ,DateAdd(DAY,6, DATEADD(DAY, @index, @FromDate))           
--   --,CASE(datepart (dw, @FromDate)) WHEN 1 THEN @FromDate END                 
--  )           
            
--  -- calcdate = Sunday in the week          
--  UPDATE @TempTable SET calcdate = (          
--   CASE WHEN  (datepart(dw, date_1)-1)=0 THEN date_1          
--       WHEN  (datepart(dw, date_2)-1)=0 THEN date_2          
--       WHEN  (datepart(dw, date_3)-1)=0 THEN date_3          
--       WHEN  (datepart(dw, date_4)-1)=0 THEN date_4          
--       WHEN  (datepart(dw, date_5)-1)=0 THEN date_5          
--       WHEN  (datepart(dw, date_6)-1)=0 THEN date_6          
--       WHEN  (datepart(dw, date_7)-1)=0 THEN date_7 END          
--     )          
--  WHERE WeekNumber=Cast((DATEDIFF(DAY,@DateFrom,DATEADD(DAY, @index, @FromDate))/7) as INT)+1           
              
--  SET @index = @index + 7                  
--END                
             
--SELECT * FROM @TempTable               
DECLARE @tail_nmbr CHAR(6),@type_code CHAR(30),@ac_code VARCHAR(10),@locdep  DATETIME,          
  @locarr DATETIME,@Crewlist VARCHAR(40), @reqcode VARCHAR(5),@orig_nmbr BIGINT,          
  @location CHAR(4),@locationArr CHAR(4),@arrtime VARCHAR(10),@deptime VARCHAR(10),@isapproxtm BIT,          
  @pax_total INT          
             
 --DECLARE @DateVal DATE;   
 DECLARE @SQLSCRIPT NVARCHAR(MAX);  
 DECLARE @ParameterDefinition AS NVARCHAR(200);             
 DECLARE @ArrVal DATE;              
 DECLARE @calcdate DATE;      
 DECLARE @locationArrVal1 CHAR(4),@TailNumVal1 CHAR(6),@ID0 INT=NULL,@ID1 INT=NULL;      
 DECLARE @locationArrVal2 CHAR(4),@TailNumVal2 CHAR(6),@ID2 INT=NULL;       
 DECLARE @locationArrVal3 CHAR(4),@TailNumVal3 CHAR(6),@ID3 INT=NULL;       
 DECLARE @locationArrVal4 CHAR(4),@TailNumVal4 CHAR(6),@ID4 INT=NULL;       
 DECLARE @locationArrVal5 CHAR(4),@TailNumVal5 CHAR(6),@ID5 INT=NULL;       
 DECLARE @locationArrVal6 CHAR(4),@TailNumVal6 CHAR(6),@ID6 INT=NULL;       
 DECLARE @locationArrVal7 CHAR(4),@TailNumVal7 CHAR(6),@ID7 INT=NULL;             
 DECLARE @WEEK INT,@LegID INT,@SORTORDER INT;            
 DECLARE @date_1 VARCHAR(10),@date_2 VARCHAR(10),@date_3 VARCHAR(10),@date_4 VARCHAR(10),          
 @date_5 VARCHAR(10),@date_6 VARCHAR(10),@date_7 VARCHAR(10);   
 DECLARE @DATE1 DATE,@DATE2 DATE,@DATE3 DATE,@DATE4 DATE,@DATE5 DATE,@DATE6 DATE,@DATE7 DATE       
             
 DECLARE curTailInfo CURSOR FOR              
 SELECT A.* FROM(SELECT DISTINCT tail_nmbr = F.TailNum           
    ,type_code=F.AircraftCD           
        ,ac_code=A1.AircraftCD          
        ,locdep=PL.DepartureDTTMLocal            
        ,locarr=PL.ArrivalDTTMLocal          
        ,Crewlist = Crew.CrewList --PL.AdditionalCrew           
        ,reqcode=P.PassengerRequestorCD          
        ,orig_nmbr=PM.TripNUM          
        ,location = A.IcaoID  
        ,locationArr=''                   
        ,deptime=SUBSTRING (CONVERT(VARCHAR(30),PL.DepartureDTTMLocal,113),13,5)  
        ,arrtime=''        
        ,isapproxtm=ISNULL(PL.IsApproxTM, 0)    
        ,pax_total=PL.PassengerTotal
        ,1 AS SORTORDER,
        PL.LegID   LegID              
  FROM    PreflightMain PM              
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0            
    INNER JOIN Airport A ON A.AirportID = PL.DepartICAOID                              
    INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	  LEFT OUTER JOIN FleetGroupOrder FGO  
	  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
	  LEFT OUTER JOIN FleetGroup FG   
	  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
	  WHERE FG.FleetGroupCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = ''  
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
    INNER JOIN Aircraft A1 ON A1.AircraftID=PM.AircraftID           
    LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID              
    LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID            
	LEFT OUTER JOIN (        
		SELECT DISTINCT PC2.LegID, CrewList = (        
		 SELECT C.CrewCD + ' '        
		 FROM PreflightCrewList PC1        
		 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
		 WHERE PC1.LegID = PC2.LegID        
		 FOR XML PATH('')        
		 )                 
		FROM PreflightCrewList PC2         
	  ) Crew ON PL.LegID = Crew.LegID         
 LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID = P.PassengerRequestorID        
 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))                 
 AND CONVERT(DATE,PL.DepartureDTTMLocal) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
 AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')  
 AND PM.ISDELETED = 0
    
UNION ALL
 
 SELECT DISTINCT tail_nmbr = F.TailNum           
    ,type_code=F.AircraftCD           
        ,ac_code=A1.AircraftCD          
        ,locdep=PL.DepartureDTTMLocal            
        ,locarr=PL.ArrivalDTTMLocal          
        ,Crewlist = Crew.CrewList --PL.AdditionalCrew           
        ,reqcode=P.PassengerRequestorCD          
        ,orig_nmbr=PM.TripNUM  
        ,location=''            
        ,locationArr=AA.IcaoID  
        ,deptime=''         
        ,arrtime=SUBSTRING (CONVERT(VARCHAR(30),PL.ArrivalDTTMLocal,113),13,5)                  
        ,isapproxtm=ISNULL(PL.IsApproxTM, 0)    
        ,pax_total=PL.PassengerTotal 
         ,2 AS SORTORDER,
         PL.LegID   LegID             
  FROM    PreflightMain PM              
    INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.ISDELETED = 0                        
    INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID                 
    INNER JOIN (  
	  SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID  
	  FROM Fleet F   
	  LEFT OUTER JOIN FleetGroupOrder FGO  
	  ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID  
	  LEFT OUTER JOIN FleetGroup FG   
	  ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID  
	  WHERE FG.FleetGroupCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) OR @FleetGroupCD = ''  
	) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID  
    INNER JOIN Aircraft A1 ON A1.AircraftID=PM.AircraftID           
    LEFT OUTER JOIN PreflightCrewList PC ON PL.LegID = PC.LegID              
    LEFT OUTER JOIN Crew C ON PC.CrewID = C.CrewID            
	LEFT OUTER JOIN (        
		SELECT DISTINCT PC2.LegID, CrewList = (        
		 SELECT C.CrewCD + ' '        
		 FROM PreflightCrewList PC1        
		 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
		 WHERE PC1.LegID = PC2.LegID        
		 FOR XML PATH('')        
		 )                 
		FROM PreflightCrewList PC2         
	  ) Crew ON PL.LegID = Crew.LegID         
 LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID = P.PassengerRequestorID        
 WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))                 
 AND CONVERT(DATE,PL.DepartureDTTMLocal) between CONVERT(DATE,@DateFrom) And CONVERT(DATE,@DateTo)
 AND (F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ',')) OR @TailNUM = '')
  AND PM.ISDELETED = 0)A 
  ORDER BY A.tail_nmbr, A.locdep,A.LEGID,A.SORTORDER  
  
 
       
  DECLARE @PrevDate DATE,@PrevTail VARCHAR(25),@ID INT=NULL;        
 OPEN curTailInfo;                          
  FETCH NEXT FROM curTailInfo INTO @tail_nmbr,@type_code,@ac_code,@locdep,@locarr,@Crewlist,@reqcode,@orig_nmbr,@location,@locationArr,@deptime,@arrtime,@isapproxtm,@pax_total,@SORTORDER,@LegID;          
  WHILE @@FETCH_STATUS = 0              
  BEGIN              
  SET @ID0=(SELECT TOP 1 ID FROM @TempTable WHERE WeekNumber=@WEEK)
---SELECT @arrtime
 IF @deptime <>''
 BEGIN
IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=0) 
           
 BEGIN 
           
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
  location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,pax_total1,date_1)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  

      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE1 IS NULL)
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date1=@locdep,date_1=@locdep,Crewlist1=@Crewlist, reqcode1=@reqcode,orig_nmbr1=@orig_nmbr,location1=@location,         
                             deptime1=@deptime,arrtimeapprox1=0,deptimeapprox1=@isapproxtm,pax_total1=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
                  
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
    location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,pax_total1,calcdate,date_1)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  
    
  
    END 
    SET @DATE1=CONVERT(DATE,@locdep)     
  END         
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=1)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_2=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
  location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,date_2)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE2 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date2=@locdep,date_2=@locdep,Crewlist2=@Crewlist, reqcode2=@reqcode,orig_nmbr2=@orig_nmbr,location2=@location,         
                             deptime2=@deptime,arrtimeapprox2=0,deptimeapprox2=@isapproxtm,pax_total2=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
    location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,calcdate,date_2)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  
    
  
    END  
    SET @DATE2=CONVERT(DATE,@locdep)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=2)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
  location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,date_3)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE3 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date3=@locdep,date_3=@locdep,Crewlist3=@Crewlist, reqcode3=@reqcode,orig_nmbr3=@orig_nmbr,location3=@location,         
                             deptime3=@deptime,arrtimeapprox3=0,deptimeapprox3=@isapproxtm,pax_total3=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
    location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,calcdate,date_3)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  

  
    END  
    SET @DATE3=CONVERT(DATE,@locdep)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=3)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
  location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,date_4)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE4 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date4=@locdep,date_4=@locdep,Crewlist4=@Crewlist, reqcode4=@reqcode,orig_nmbr4=@orig_nmbr,location4=@location,         
                             deptime4=@deptime,arrtimeapprox4=0,deptimeapprox4=@isapproxtm,pax_total4=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
  
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
    location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,calcdate,date_4)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  
  
    END 
    SET @DATE4=CONVERT(DATE,@locdep)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=4)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,date_5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE5 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date5=@locdep,date_5=@locdep,Crewlist5=@Crewlist, reqcode5=@reqcode,orig_nmbr5=@orig_nmbr,location5=@location,         
                             deptime5=@deptime,arrtimeapprox5=0,deptimeapprox5=@isapproxtm,pax_total5=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
                    
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
    location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,calcdate,date_5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  

  
    END 
    SET @DATE5=CONVERT(DATE,@locdep)     
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=5)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,date_6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE6 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date6=@locdep,date_6=@locdep,Crewlist6=@Crewlist, reqcode6=@reqcode,orig_nmbr6=@orig_nmbr,location6=@location,         
                             deptime6=@deptime,arrtimeapprox6=0,deptimeapprox6=@isapproxtm,pax_total6=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
                    
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
    location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,calcdate,date_6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  
    
  
    END  
    SET @DATE6=CONVERT(DATE,@locdep)    
 END 
 
 ELSE-- IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=6)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NULL            
     BEGIN              

   --INSERT FOR Departure 
   INSERT INTO @TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,date_7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,    
  NULL,@deptime,0,@isapproxtm,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE7 IS NULL)        
        
    BEGIN    
      
    -- Curr Tail and Prev tail also should match for this update      
     UPDATE @TempTable SET  Date7=@locdep,date_7=@locdep,Crewlist7=@Crewlist, reqcode7=@reqcode,orig_nmbr7=@orig_nmbr,location7=@location,         
                             deptime7=@deptime,arrtimeapprox7=0,deptimeapprox7=@isapproxtm,pax_total7=@pax_total       
                    WHERE WeekNumber=@WEEK AND ID=@ID-1    
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
   -- INSERT FOR DEPARTURE      
     INSERT INTO @TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
    location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,calcdate,date_7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@location,NULL,    
    @deptime,0,@isapproxtm,@pax_total,@calcdate,CONVERT(DATE,@locdep))  
  
    END
    SET @DATE7=CONVERT(DATE,@locdep)      
END 

END

-------------------Departure------------------
  -----------------------------------------------------------------------------------
  ELSE 

BEGIN
  ----------------------Arrival-------------------------           
IF ((DATEDIFF(DAY,@DateFrom,@locarr)%7)=0)               
 BEGIN          
  
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep )
IF @ID0 IS NOT NULL            
     BEGIN              
      --INSERT FOR ARRIVAL    
       select  @locarr    
   INSERT INTO @TempTable (WeekNumber,Date1,tail_nmbr,type_code,ac_code,crewlist1,reqcode1,orig_nmbr1,    
  location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,pax_total1,date_1)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE1 IS NULL)
        
    BEGIN    
      
 UPDATE @TempTable SET  Date1=@locdep,Crewlist1=@Crewlist, reqcode1=@reqcode,orig_nmbr1=@orig_nmbr,location1=@locationArr,         
                           arrtime1=@arrtime,arrtimeapprox1=@isapproxtm,deptimeapprox1=@isapproxtm,pax_total1=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
        --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
	  location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,calcdate,date_2)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END 
    SET @DATE1=CONVERT(DATE,@locarr)     
  END         
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locarr)%7)=1)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_2=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
  location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,date_2)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE2 IS NULL)        
        
    BEGIN    
                    
    UPDATE @TempTable SET  Date2=@locdep,Crewlist2=@Crewlist, reqcode2=@reqcode,orig_nmbr2=@orig_nmbr,location2=@locationArr,         
                           arrtime2=@arrtime,arrtimeapprox2=@isapproxtm,deptimeapprox2=@isapproxtm,pax_total2=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
     ELSE            
        
    BEGIN    

 --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date2,tail_nmbr,type_code,ac_code,crewlist2,reqcode2,orig_nmbr2,    
	  location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,pax_total2,calcdate,date_2)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END  
    SET @DATE2=CONVERT(DATE,@locarr)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locarr)%7)=2)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              

      --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
  location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,date_3)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE3 IS NULL)        
        
    BEGIN    
      
                    
    UPDATE @TempTable SET  Date3=@locdep,Crewlist3=@Crewlist, reqcode3=@reqcode,orig_nmbr3=@orig_nmbr,location3=@locationArr,         
                           arrtime3=@arrtime,arrtimeapprox3=@isapproxtm,deptimeapprox3=@isapproxtm,pax_total3=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
     ELSE            
        
    BEGIN    
  --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date3,tail_nmbr,type_code,ac_code,crewlist3,reqcode3,orig_nmbr3,    
	  location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,pax_total3,calcdate,date_3)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END  
    SET @DATE3=CONVERT(DATE,@locarr)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locarr)%7)=3)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
  location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,date_4)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE4 IS NULL)        
        
    BEGIN    
      
    UPDATE @TempTable SET  Date4=@locdep,Crewlist4=@Crewlist, reqcode4=@reqcode,orig_nmbr4=@orig_nmbr,location4=@locationArr,         
                           arrtime4=@arrtime,arrtimeapprox4=@isapproxtm,deptimeapprox4=@isapproxtm,pax_total4=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
   ELSE            
        
    BEGIN    

 --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date4,tail_nmbr,type_code,ac_code,crewlist4,reqcode4,orig_nmbr4,    
	  location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,pax_total4,calcdate,date_4)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END 
    SET @DATE4=CONVERT(DATE,@locarr)    
 END 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locarr)%7)=4)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              
 
      --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,date_5)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE5 IS NULL)        
        
    BEGIN    
      
 UPDATE @TempTable SET  Date5=@locdep,Crewlist5=@Crewlist, reqcode5=@reqcode,orig_nmbr5=@orig_nmbr,location5=@locationArr,         
                           arrtime5=@arrtime,arrtimeapprox5=@isapproxtm,deptimeapprox5=@isapproxtm,pax_total5=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
     ELSE            
        
    BEGIN    
      
 --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date5,tail_nmbr,type_code,ac_code,crewlist5,reqcode5,orig_nmbr5,    
	  location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,pax_total5,calcdate,date_5)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END 
    SET @DATE5=CONVERT(DATE,@locdep)     
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=5)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              

  --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,date_6)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE6 IS NULL)        
        
    BEGIN    
                         
    UPDATE @TempTable SET  Date6=@locdep,Crewlist6=@Crewlist, reqcode6=@reqcode,orig_nmbr6=@orig_nmbr,location6=@locationArr,         
                           arrtime6=@arrtime,arrtimeapprox6=@isapproxtm,deptimeapprox6=@isapproxtm,pax_total6=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    
    ----
     ELSE            
        
    BEGIN    
    
     --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date6,tail_nmbr,type_code,ac_code,crewlist6,reqcode6,orig_nmbr6,    
	  location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,pax_total6,calcdate,date_6)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END  
    SET @DATE6=CONVERT(DATE,@locdep)    
 END 
 
 ELSE IF ((DATEDIFF(DAY,@DateFrom,@locdep)%7)=6)               
 BEGIN              
  SET @Week=(CONVERT(INT,(DATEDIFF(DAY,@DateFrom,@locdep)/7))+1)        
  --SET @calcdate=(SELECT TOP 1 calcdate FROM @TempTable WHERE WeekNumber=@WEEK AND date_1=@locdep ) 
            
  IF @ID0 IS NOT NULL            
     BEGIN              
      --INSERT FOR ARRIVAL      
   INSERT INTO @TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,date_7)              
   VALUES (@Week,CONVERT(DATE,@locdep),@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
  @arrtime,NULL,@isapproxtm,0,@pax_total,CONVERT(DATE,@locdep))  
      
     END      
        
  ELSE IF  (@tail_nmbr=@PrevTail AND @PrevDate!=CONVERT(DATE,@locdep) AND @DATE7 IS NULL)        
        
    BEGIN    
                    
    UPDATE @TempTable SET  Date7=@locdep,Crewlist7=@Crewlist, reqcode7=@reqcode,orig_nmbr7=@orig_nmbr,location7=@locationArr,         
                           arrtime7=@arrtime,arrtimeapprox7=@isapproxtm,deptimeapprox7=@isapproxtm,pax_total7=@pax_total       
                      WHERE WeekNumber=@WEEK AND ID=@ID
  
    END    

  ELSE            
        
  BEGIN    
      
        --INSERT FOR ARRIVAL      
	   INSERT INTO @TempTable (WeekNumber,Date7,tail_nmbr,type_code,ac_code,crewlist7,reqcode7,orig_nmbr7,    
	  location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,pax_total7,calcdate,date_7)              
	   VALUES (@Week,@locdep,@tail_nmbr,@type_code,@ac_code,@Crewlist,@reqcode,@orig_nmbr,@locationArr,    
	  @arrtime,NULL,@isapproxtm,0,@pax_total,@calcdate,@locdep) 
  
    END
    SET @DATE7=CONVERT(DATE,@locdep)      
 END 

   ----------------------Arrival-------------------------       
END
   SET @ID=SCOPE_IDENTITY()   
   SET @PrevTail=@tail_nmbr
   SET @PrevDate=@locdep
   FETCH NEXT FROM curTailInfo INTO @tail_nmbr,@type_code,@ac_code,@locdep,@locarr,@Crewlist,@reqcode,    
 @orig_nmbr,@location,@locationArr,@deptime,@arrtime,@isapproxtm,@pax_total,@SORTORDER,@LegID;              
                
 END               
            
 CLOSE curTailInfo;              
 DEALLOCATE curTailInfo;  
 ---SELECT * FROM @TempTable
 DECLARE @WEEKNUM INT=1;
 SELECT @WEEK=COUNT(DISTINCT WeekNumber) FROM @TempTable
---- SELECT @WEEK
WHILE (@WEEKNUM<=@WEEK)
 BEGIN
  UPDATE @TempTable SET date_1=@DATEFROM,  
                        date_2=@DATEFROM+1,  
                        date_3=@DATEFROM+2,  
                        date_4=@DATEFROM+3,  
                        date_5=@DATEFROM+4,  
                        date_6=@DATEFROM+5,  
                        date_7=@DATEFROM+6,  
                        calcdate=(CASE WHEN  (datepart(dw, @DATEFROM+1)-1)=0 THEN @DATEFROM          
       WHEN  (datepart(dw, @DATEFROM+1)-1)=0 THEN @DATEFROM+1         
       WHEN  (datepart(dw, @DATEFROM+2)-1)=0 THEN @DATEFROM+2          
       WHEN  (datepart(dw, @DATEFROM+3)-1)=0 THEN @DATEFROM+3          
       WHEN  (datepart(dw, @DATEFROM+4)-1)=0 THEN @DATEFROM+4          
       WHEN  (datepart(dw, @DATEFROM+5)-1)=0 THEN @DATEFROM+5          
       WHEN  (datepart(dw, @DATEFROM+6)-1)=0 THEN @DATEFROM+6 END)  
           WHERE WeekNumber=@WEEKNUM; 
 SET @DATEFROM=@DATEFROM+7
 SET @WEEKNUM=@WEEKNUM+1
 END
 
 --DECLARE @WeekNum INT=1    
  SELECT ID,WeekNumber,tail_nmbr,type_code,ac_code,locdep,Date1,crewlist1,reqcode1,ISNULL(orig_nmbr1,0)orig_nmbr1,        
        Date2,crewlist2,reqcode2,ISNULL(orig_nmbr2,0) orig_nmbr2,Date3,crewlist3,reqcode3,ISNULL(orig_nmbr3,0)orig_nmbr3,          
        Date4,crewlist4,reqcode4,ISNULL(orig_nmbr4,0)orig_nmbr4,Date5,crewlist5,reqcode5,ISNULL(orig_nmbr5,0)orig_nmbr5,         
        Date6,crewlist6,reqcode6,ISNULL(orig_nmbr6,0)orig_nmbr6,Date7,crewlist7,reqcode7,ISNULL(orig_nmbr7,0)orig_nmbr7,          
        location1,arrtime1,deptime1,arrtimeapprox1,deptimeapprox1,ISNULL(pax_total1,0) pax_total1,        
        location2,arrtime2,deptime2,arrtimeapprox2,deptimeapprox2,ISNULL(pax_total2,0) pax_total2,          
        location3,arrtime3,deptime3,arrtimeapprox3,deptimeapprox3,ISNULL(pax_total3,0) pax_total3,          
        location4,arrtime4,deptime4,arrtimeapprox4,deptimeapprox4,ISNULL(pax_total4,0) pax_total4,          
        location5,arrtime5,deptime5,arrtimeapprox5,deptimeapprox5,ISNULL(pax_total5,0) pax_total5,          
        location6,arrtime6,deptime6,arrtimeapprox6,deptimeapprox6,ISNULL(pax_total6,0) pax_total6,          
        location7,arrtime7,deptime7,arrtimeapprox7,deptimeapprox7,ISNULL(pax_total7,0) pax_total7,calcdate,          
        LEFT(datename(dw,date_1),3)+' '+LEFT(DATENAME(MONTH,date_1),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_1))+'/'+convert(varchar(10),YEAR(date_1))date_1,          
        LEFT(datename(dw,date_2),3)+' '+LEFT(DATENAME(MONTH,date_2),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_2))+'/'+convert(varchar(10),YEAR(date_2))date_2,          
        LEFT(datename(dw,date_3),3)+' '+LEFT(DATENAME(MONTH,date_3),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_3))+'/'+convert(varchar(10),YEAR(date_3))date_3,           
        LEFT(datename(dw,date_4),3)+' '+LEFT(DATENAME(MONTH,date_4),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_4))+'/'+convert(varchar(10),YEAR(date_4))date_4,          
        LEFT(datename(dw,date_5),3)+' '+LEFT(DATENAME(MONTH,date_5),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_5))+'/'+convert(varchar(10),YEAR(date_5))date_5,           
        LEFT(datename(dw,date_6),3)+' '+LEFT(DATENAME(MONTH,date_6),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_6))+'/'+convert(varchar(10),YEAR(date_6))date_6,           
        LEFT(datename(dw,date_7),3)+' '+LEFT(DATENAME(MONTH,date_7),3)+' '+CONVERT(VARCHAR(10),DATEPART(mm,date_7))+'/'+convert(varchar(10),YEAR(date_7))date_7,
        airportcode,airportdesc,airportcity,crewcode,crewdesc,authcode,authdesc     
       FROM @TempTable             
   ORDER BY WeekNumber,tail_nmbr
 END;                 
 ---EXEC spGetReportPREWeeklyCalendar1ExportInformation 'TIM', '2012-10-01', '2012-10-31', '', ''         
 /*        
 --select * from Fleet where TailNum in ('XOFNGA', 'SZTBP','ESMNM')        
 EXEC spGetReportPREWeeklyCalendar1Information 'UC', '2012-09-19', '2012-09-25', '', ''        
 declare @fleetid bigint = 10001212620        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-19'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-20'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-21'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-22'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-23'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-24'        
 EXEC spGetReportPREWeeklyCalendar1SubInformation @fleetid, '2012-09-25'        
 EXEC spGetReportPREWeeklyCalendar1MhtmlInformation 'jwilliams_13','2012-09-19','2012-09-25','',''        
 */  
  
GO


