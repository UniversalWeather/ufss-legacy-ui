IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportDayMonthFormatByUserCD]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetReportDayMonthFormatByUserCD]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Function Name: GetReportDayMonthFormatByUserCD
-- Author: SUDHAKAR J
-- Create date: 19 Jun 2012
-- Description: Returns DayMonthFormat for the reports content
-- Revision History
-- Date			Name		Ver		Change
-- 
--
-- ===============================================================================================
CREATE FUNCTION [dbo].[GetReportDayMonthFormatByUserCD] (@UserCD VARCHAR(20))
RETURNS TinyInt
WITH EXECUTE AS CALLER
AS
BEGIN

	--Dates to be displayed based on the Company.DayMonthFormat
	--IF Company.DayMonthFormat = 2 THEN 'DD/MM/YY' ELSE 'MM/DD/YY' [Defaulted to 'MM/DD/YY']

    DECLARE @RptDateFormat TinyInt= 1;
    
	SELECT @RptDateFormat = CASE DayMonthFormat WHEN 2 THEN 1 ELSE 3 END
	FROM Company 
	WHERE HomeBaseID =  dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
	AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	
	RETURN @RptDateFormat
END;

-- SELECT [dbo].[GetReportDayMonthFormatByUserCD] ('SUPERVISOR_99')


GO


