

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRETSFlightLogInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRETSFlightLogInformation]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetReportPRETSFlightLogInformation]
	@UserCD  AS VARCHAR(30)   
	,@TripNUM           AS VARCHAR(300) = '' --      PreflightMain.TripNUM
	,@LegNUM            AS VARCHAR(300) = '' --      PreflightLeg.LegNUM
	,@PassengerCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerID in PreflightPassengerList)
	,@BeginDate         AS DATETIME = null --      PreflightLeg.DepartureDTTMLocal
	,@EndDate           AS DATETIME = null --      PreflightLeg.ArrivalDTTMLocal	
	,@TailNUM           AS VARCHAR(300) = ''--      Fleet.TailNUM (Use FleetID of PreflightMain)
	,@DepartmentCD      AS VARCHAR(300) = '' --      Department.DepartmentCD (Use DepartmentID of PreflightLeg)
	,@AuthorizationCD   AS VARCHAR(300) = '' --      DepartmentAuthorization.AuthorizationCD (Use AuthorizationID of PreflightLeg)
	,@CatagoryCD        AS VARCHAR(300) = '' --      FlightCatagory.FlightCatagoryCD (Use FlightCategoryID of PreflightLeg)
	,@ClientCD          AS VARCHAR(300) = '' --      Client.ClientCD
	,@HomeBaseCD        AS VARCHAR(300) = '' --      UserMaster.HomeBase
	,@RequestorCD       AS VARCHAR(300) = '' --      Passenger.PassengerRequestorCD (Use PassengerRequestorID of PreflightMain)
	,@CrewCD            AS VARCHAR(300) = '' --      Crew.CrewCD   
	,@IsTrip			AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	,@IsCanceled        AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	,@IsHold            AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	,@IsWorkSheet       AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
	,@IsUnFulFilled     AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
	,@IsScheduledService AS BIT = 0 --   PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'S'
	
AS
BEGIN
-- ================================================================================
-- SPC Name: spGetReportPRETSFlightLogInformation
-- Author: AISHWARYA.M
-- Create date:27 SEP 2012
-- Description: Get Preflight Tripsheet Flight Log information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =================================================================================

	-- [Start] Report Criteria --
	DECLARE @tblTripInfo AS TABLE (
	TripID BIGINT
	, LegID BIGINT
	, PassengerID BIGINT
	)

	INSERT INTO @tblTripInfo (TripId, LegID, PassengerID)
	EXEC spGetReportPRETSCriteria @UserCD,@TripNUM,@LegNUM,@PassengerCD,@BeginDate,
		@EndDate,@TailNUM,@DepartmentCD,@AuthorizationCD,@CatagoryCD,@ClientCD,
		@HomeBaseCD,@RequestorCD,@CrewCD,@IsTrip, @IsCanceled, @IsHold, 
		@IsWorkSheet, @IsUnFulFilled, @IsScheduledService
	-- [End] Report Criteria --

		SELECT [TripNumber] = PM.TripNUM 
				,[Description] = PM.TripDescription
				,[Trip Dates] = dbo.GetTripDatesByTripIDUserCD(PM.TripID,@UserCD)
				,[Requestor] = P.LastName+ '' +P.FirstName+ '' +P.MiddleInitial
				,[TailNumber] =F.TailNum
				,[ContactPhone] = P.PhoneNum
				,[DispatchNum] = PM.DispatchNUM 
				,[Type] =  A.AircraftCD
				,[Leg] = PL.LegNUM
				--,[DepartICAO] = AD.IcaoID
				--,[ArriveICAO] =  AA.IcaoID
				--,[SchedUTCDate] = PL.DepartureGreenwichDTTM --(Displays only Date)
				--,[SchedUTCDep] = PL.DepartureGreenwichDTTM --(Displays Time)
				--,[SchedUTCArr] = PL.ArrivalGreenwichDTTM --(Dispalys Time)
				--,[PICSIC] = CASE  PCL.DutyTYPE  WHEN 'P' THEN C.CrewCD 
				--			WHEN 'S' THEN C.CrewCD END--for PreflightCrewList.DutyType = 'P'/ DutyType = 'S'
				--,[ADDLCREW] = (CASE (PCL.DutyTYPE) WHEN '!P' THEN C.CrewCD
				--                                   WHEN '!S' THEN C.CrewCD END) --for PreflightCrewList.DutyType is otherthan 'P' and 'S'
				FROM (SELECT DISTINCT TripID, LegID FROM @tblTripInfo)M
				INNER JOIN PreflightMain PM ON M.TripID = PM.TripID
				INNER JOIN PreflightLeg PL ON M.LegID = PL.LegID
				--INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AD ON PL.DepartICAOID = AD.AirportID
				--INNER JOIN (SELECT AirportID, IcaoID FROM Airport) AA ON PL.ArriveICAOID = AD.AirportID
				INNER JOIN Passenger P ON PM.PassengerRequestorID = P.PassengerRequestorID
				INNER JOIN Fleet F ON  PM.FleetID = F.FleetID
				LEFT OUTER JOIN Aircraft A ON PM.AircraftID = A.AircraftID
				--INNER JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID
				--INNER JOIN Crew C ON PCL.CrewID = C.CrewID
				WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
				ORDER BY PL.LegNUM
END 

--EXEC spGetReportPRETSFlightLogInformation 'TIM','3891','','','','','','','','','','','','','','','','','',''






GO


