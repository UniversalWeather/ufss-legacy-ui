IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar3ExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar3ExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar3ExportInformation]
	@UserCD VARCHAR(30) --Mandatory
	,@DATEFROM DATETIME --Mandatory
	,@DATETO DATETIME --Mandatory
	,@TailNum VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@FleetGroupCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@AircraftCD VARCHAR(500) = '' --[Optional], Comma delimited string with mutiple values
	,@IsHomeBase BIT = 0
	
AS
-- ===============================================================================
-- SPC Name: spGetReportPREAircraftCalendar3ExportInformation
-- Author: AISHWARYA.M
-- Create date: 11 Jul 2012
-- Description: Get Preflight Aircraft Calendar Export information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON


-----------------------------TailNum and Fleet Group Filteration----------------------
DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));

Declare @SuppressActivityAircft BIT  
SELECT @SuppressActivityAircft=IsZeroSuppressActivityAircftRpt FROM Company WHERE CustomerID=@CUSTOMER
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
CREATE TABLE  #TempFleetID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		FleetID BIGINT
  )
  

IF @TailNUM <> ''
BEGIN
	INSERT INTO #TempFleetID
	SELECT DISTINCT F.FleetID 
	FROM Fleet F
	WHERE F.CustomerID = @CUSTOMER
	AND F.TailNum  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TailNUM, ','))
END

IF @FleetGroupCD <> ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	LEFT OUTER JOIN FleetGroupOrder FGO
	ON F.FleetID = FGO.FleetID AND F.CustomerID = FGO.CustomerID
	LEFT OUTER JOIN FleetGroup FG 
	ON FGO.FleetGroupID = FG.FleetGroupID AND FGO.CustomerID = FG.CustomerID
	WHERE FG.FleetGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FleetGroupCD, ',')) 
	AND F.CustomerID = @CUSTOMER  
END
ELSE IF @TailNUM = '' AND  @FleetGroupCD = ''
BEGIN  
INSERT INTO #TempFleetID
	SELECT DISTINCT  F.FleetID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Fleet F 
	WHERE  F.CustomerID = @CUSTOMER 
	AND F.IsDeleted=0
	
END
-----------------------------TailNum and Fleet Group Filteration----------------------

   DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';
	DECLARE @ParameterDefinition AS NVARCHAR(100)

	DECLARE @NextString NVARCHAR(40)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @String NVARCHAR(40)
	DECLARE @Delimiter NVARCHAR(40)
	DECLARE @Loopdate DATETIME

	CREATE TABLE #TempTailNum  (
	ID INT identity(1, 1) NOT NULL
	,FleetID BIGINT
	,TailNum VARCHAR(25)
	,Type_Code CHAR(4)
	)

	SET @String = @TailNum
	SET @Delimiter = ','
	SET @String = @String + @Delimiter
	SET @Pos = charindex(@Delimiter, @String)
	
	CREATE TABLE #TempTable  (
		ID INT identity(1, 1) NOT NULL
		,[Date] DATETIME
		,[CustomerID] BIGINT
		,[TailNo] VARCHAR(9)
		,[From] CHAR(4)
		,[To] CHAR(4)
		,DepartureDTTMLocal DATETIME
		,ArrivalDTTMLocal DATETIME
		,[NextLocalDTTM] DATETIME
		,[Duty] CHAR(2)
		,[Requestor] VARCHAR(700) 
		,[Phone] VARCHAR(25)
		,[Pax] INT
		,[Crew] VARCHAR(500)
		,[Comments] VARCHAR(40)
		,LegID BIGINT
		,HomebaseID BIGINT
		,Type_Code CHAR(4)
		,TripNum BIGINT
		,TripID BIGINT
		,LegNUM BIGINT
		,DType CHAR(1)
		,depicao_id VARCHAR(4)
		,arricao_id VARCHAR(4)
		,fbo_name VARCHAR(60)
		,fbo_phone VARCHAR(25)
		,fbo_freq VARCHAR(7)
		,fbo_fuel NUMERIC(15,2)
		,crew_hotel_name VARCHAR(40)
		,crew_hotel_phone VARCHAR(25)
		,crew_rate NUMERIC(17,2)
		,pax_car_name VARCHAR(40)
		,pax_car_phone VARCHAR(25)
		,pax_hotel_name VARCHAR(40)
		,pax_hotel_phone VARCHAR(25)
		,Notes VARCHAR(MAX)
		)

	DECLARE	 @TEMPTODAY AS DATETIME = @DATEFROM
		

		SET @SQLSCRIPT = 
			' INSERT INTO #TempTailNum (FleetID,TailNum,Type_Code)
			SELECT DISTINCT F.FleetID,F.TailNum,F.AircraftCD
			FROM Fleet F
			LEFT OUTER JOIN Aircraft AT ON AT.AircraftID=F.AircraftID
			 WHERE FleetID IN(SELECT FleetID FROM #TempFleetID)
			AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
			--AND F.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))
			'		
			IF @IsHomeBase = 1 
			BEGIN
			SET @SQLSCRIPT = @SQLSCRIPT + ' AND F.HomebaseID = ' + CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))
			END	
		   IF @AircraftCD <> '' BEGIN  
	         SET @SQLSCRIPT = @SQLSCRIPT + ' AND AT.AircraftCD IN (''' + REPLACE(CASE WHEN RIGHT(@AircraftCD, 1) = ',' THEN LEFT(@AircraftCD, LEN(@AircraftCD) - 1) ELSE @AircraftCD END, ',', ''', ''') + ''')';
             END  
		SET @ParameterDefinition =  '@UserCD AS VARCHAR(30), @FleetGroupCD VARCHAR(500)'
		EXECUTE sp_executesql @SQLSCRIPT, @ParameterDefinition, @UserCD, @FleetGroupCD 






	INSERT INTO #TempTable
		( [Date]
		,[TailNo]
		,[From]
		,[To]  
		,DepartureDTTMLocal
		,ArrivalDTTMLocal		
		,[NextLocalDTTM]  
		,[Duty] 
		,[Requestor]
		,[Phone]
		,[Pax]
		,[Crew]
		,[Comments]
		,CustomerID
		,LegID
		,HomebaseID
		,Type_Code
		,TripNum
		,TripID
		,LegNum
		,depicao_id 
		,arricao_id 
		,fbo_name
		,fbo_phone
		,fbo_freq 
		,fbo_fuel 
		,crew_hotel_name
		,crew_hotel_phone
		,crew_rate
		,pax_car_name
		,pax_car_phone
		,pax_hotel_name
		,pax_hotel_phone
		,Notes)
			SELECT CONVERT(DATE, PL.DepartureDTTMLocal)
			,F.TailNum 
			,A.ICAOID 
			,AA.ICAOID 
			,PL.DepartureDTTMLocal 
			,PL.ArrivalDTTMLocal
			,PL.NextLocalDTTM
			,CASE WHEN ISNULL(PL.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(PM.TripStatus, '') = 'H' THEN  'H' ELSE 'F' END
									   ELSE PL.DutyTYPE END
		    ,ISNULL(P1.LastName+ ',' ,'')  + ISNULL(P1.FirstName,'') + ' ' +ISNULL(P1.MiddleInitial,'') 
			,P1.PhoneNum
			,PL.PassengerTotal 
			,( SUBSTRING(CrewList,1,LEN(CrewList)-1))
			,PM.TripDescription
			,F.CustomerID
			,PL.LegID
			,F.HomebaseID	
			,F.AircraftCD	
			,PM.TripNum
			,PM.TripID	
			,PL.LegNum
			,A.IcaoID
			,AA.IcaoID
			,PF.PreflightFBOName
		    ,PF.PhoneNum1
		    ,PF.Frequency
		    ,PF.LastFuelPrice
		    ,CHT.Name
		    ,CHT.PhoneNum
		    ,CHT.NegociatedRate
		    ,TPD.TransportationVendor 
		    ,TPD.PhoneNum
		    ,PHT.Name
		    ,PHT.PhoneNum 
		    ,PL.Notes
			
			FROM PreflightMain PM
			INNER JOIN PreflightLeg PL ON PM.TripID = PL.TripID AND PL.IsDeleted = 0
			INNER JOIN Airport A ON A.AirportID = PL.DepartICAOID
			INNER JOIN Airport AA ON AA.AirportID = PL.ArriveICAOID
			 LEFT OUTER JOIN (  
			   SELECT DISTINCT PC2.tripid, CrewList = (  
				 SELECT distinct C.CrewCD + ','  
				 FROM PreflightCrewList PC1 
				 INNER JOIN PreflightLeg PL ON PC1.LegID = PL.LegID
				 INNER JOIN Crew C ON PC1.CrewID = C.CrewID  
				 WHERE Pl.tripid = PC2.tripid  
				   AND PC1.DutyTYPE NOT IN('P','S')
				 FOR XML PATH('')  
				 )           
				FROM PreflightLeg PC2  
		   ) C ON PM.tripid = C.tripid 
		    --- LEFT OUTER JOIN PreflightPassengerList PP1  ON PL.LegID = PP1.LegID  
		     LEFT OUTER JOIN Passenger P1 ON PM.PassengerRequestorID=P1.PassengerRequestorID   
			 INNER JOIN (
						SELECT DISTINCT F.CustomerID, F.FleetID, F.TailNum, F.AircraftCD, F.HomebaseID
						FROM Fleet F 
				  ) F ON PM.FleetID = F.FleetID AND PM.CustomerID = F.CustomerID
	     	INNER JOIN #TempFleetID  T ON F.FleetID=T.FleetID
			LEFT OUTER JOIN AIRCRAFT AT ON PM.AircraftID=AT.AircraftID
			LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID = P.PassengerRequestorID
			
			--
			LEFT OUTER JOIN (
				SELECT PTL.ConfirmationStatus, PTL.Comments, PTL.LegID, T.TransportationVendor, T.PhoneNum 
				FROM PreflightTransportList PTL 
				INNER JOIN Transport T ON PTL.TransportID = T.TransportID AND PTL.IsDepartureTransport = 1 AND PTL.CrewPassengerType = 'P'
			) TPD ON PL.LegID = TPD.LegID
						
			LEFT OUTER JOIN (
				SELECT LegID, PreflightFBOName, PhoneNum1, Frequency, FuelQty,LastFuelPrice
				FROM PreflightFBOList FL
				INNER JOIN (SELECT FBOID, Frequency, FuelQty,LastFuelPrice FROM FBO) FB ON FL.FBOID = FB.FBOID AND FL.IsArrivalFBO = 1
			) PF ON PL.LegID = PF.LegID			
			
			--Pax Hotel
			LEFT OUTER JOIN ( SELECT 
				PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.Addr1,HP.Addr2,HP.CityName,
				HP.StateName,HP.PostalZipCD,HP.Remarks,HP.NegociatedRate,
				PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments 
			FROM PreflightPassengerList PPL 
			INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			) PHT ON PL.legID = PHT.LegID
			
			--Crew Hotel
			LEFT OUTER JOIN (			
				SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
					 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
					 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments  
				FROM PreflightCrewList PCLL 
				INNER JOIN PreflightHotelList PCH ON PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1 
																			AND crewPassengerType = 'C'
				INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
																			AND PCLL.CrewID = PHCP.CrewID
				--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			) CHT ON PL.legID = CHT.LegID	
					
			--
			
				WHERE  CONVERT(DATE, PL.DepartureDTTMLocal) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
				AND F.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
				AND PM.TripStatus IN('T','H')
				 AND (AT.AircraftCD IN(SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))OR @AircraftCD='')
				 AND PM.IsDeleted = 0
				 AND (PM.HomebaseID IN (CONVERT(BIGINT,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))OR @IsHomeBase=0)





DELETE FROM #TempTable WHERE Duty='MX'


INSERT INTO #TempTable( [Date],[TailNo],[From],[To] ,DepartureDTTMLocal,ArrivalDTTMLocal,[NextLocalDTTM],[Duty],[Requestor],[Phone]
		               ,[Pax],[Crew],[Comments],CustomerID,LegID,HomebaseID,Type_Code,TripNum,TripID,depicao_id,arricao_id)
SELECT CONVERT(DATETIME,CONVERT(DATE,PL.DepartureDTTMLocal)) DepartureDTTMLocal,T.TailNo,AD.IcaoID,AA.IcaoID,PL.DepartureDTTMLocal,
          PL.ArrivalDTTMLocal Arrive,
          PL.NextLocalDTTM,T.Duty,T.Requestor,T.Phone,PL.PassengerTotal,T.Crew,T.Comments,T.CustomerID,PL.LegID,PM.HomebaseID,T.Type_Code,T.TripNum,T.TripID,AD.IcaoID,AA.IcaoID
                                             FROM #TempTable T 
                                             INNER JOIN PreflightMain PM ON T.TripID=PM.TripID AND PM.IsDeleted = 0
                                             INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                             LEFT OUTER JOIN Airport AD ON PL.DepartICAOID=AD.AirportID
                                             LEFT OUTER JOIN Airport AA ON PL.ArriveICAOID=AA.AirportID
                                             WHERE PL.LegNUM=T.LegNUM-1







	DECLARE @Date DATETIME
		,@CustomerID BIGINT
		,@TailNo VARCHAR(9)
		,@From CHAR(4)
		,@To CHAR(4);
	DECLARE @DepartureDTTMLocal DATETIME
		,@ArrivalDTTMLocal DATETIME
		,@NextLocalDTTM DATETIME
		,@Duty CHAR(2)
		,@Requestor VARCHAR(700) 
		,@Phone VARCHAR(25)
		,@Pax INT
		,@Crew VARCHAR(500)
		,@Comments VARCHAR(40)
		,@Type_Code CHAR(4)
		,@CrLegId  BIGINT
		,@Crdepicao_id CHAR(4)
		,@Crarricao_id CHAR(4)
		
	
		
	DECLARE curAircraftCal CURSOR FAST_FORWARD READ_ONLY
	FOR
	SELECT TT.[Date] 
		,TT.CustomerID 
		,TT.TailNo 
		,TT.[From] 
		,TT.[To]  
		,TT.DepartureDTTMLocal
		,TT.ArrivalDTTMLocal		
		,TT.NextLocalDTTM 
		,TT.Duty 
		,TT.Requestor 
		,TT.Phone 
		,TT.Pax 
		,TT.Crew 
		,TT.Comments 
		,TT.Type_Code
		,TT.LegID
		,TT.depicao_id
		,TT.arricao_id
	
	FROM #TempTable TT
	WHERE CONVERT(DATE, TT.DepartureDTTMLocal) < CONVERT(DATE, TT.[NextLocalDTTM])
		AND TT.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		ORDER BY [Date]
	
	OPEN curAircraftCal 
	
	FETCH NEXT
	FROM curAircraftCal
	INTO @Date 
		,@CustomerID 
		,@TailNo 
		,@From 
		,@To 
		,@DepartureDTTMLocal 
		,@ArrivalDTTMLocal 
		,@NextLocalDTTM 
		,@Duty 
		,@Requestor
		,@Phone
		,@Pax 
		,@Crew 
		,@Comments 
		,@Type_Code
		,@CrLegId
		,@Crdepicao_id
		,@Crarricao_id
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @Loopdate = @ArrivalDTTMLocal;
		
		--INSERT INTO #TEMPTABLE ([Date], CustomerID, TailNo, [From], [To], DepartureDTTMLocal,  ArrivalDTTMLocal, NextLocalDTTM,
		--		Duty,Requestor,Phone,
		--		Pax, Crew, Comments,Type_Code,LegID) 
		--		VALUES(@Date, @CustomerID, @TailNo, @To, @To, @ArrivalDTTMLocal, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 23:59', NULL,
		--		'R',  @Requestor, @Phone,@Pax, @Crew, @Comments,@Type_Code,@CrLegId)
		
	SET @Loopdate = DATEADD(DAY, 1, CONVERT(DATETIME, CONVERT(VARCHAR(10), @ArrivalDTTMLocal, 101) + ' 00:00', 101));
	WHILE(CONVERT(DATE, @Loopdate) < CONVERT(DATE, @NextLocalDTTM))
	BEGIN
	
		INSERT INTO #TEMPTABLE ([Date], CustomerID, TailNo, [From], [To], DepartureDTTMLocal,  ArrivalDTTMLocal, NextLocalDTTM,
				Duty,Requestor,Phone,
				Pax, Crew, Comments,Type_Code,LegID,depicao_id,arricao_id) 
				VALUES(@Loopdate, @CustomerID, @TailNo, @To, @To, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101),
				CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 23:59', 101), NULL,
				'R', @Requestor, @Phone,@Pax,  @Crew, @Comments,@Type_Code,@CrLegId,@Crdepicao_id,@Crarricao_id)
			
			SET @Loopdate = DATEADD(DAY, 1, @Loopdate)
	END
		
		--INSERT INTO #TEMPTABLE ([Date], CustomerID, TailNo, [From], [To], DepartureDTTMLocal,  ArrivalDTTMLocal, NextLocalDTTM,
		--		Duty, Requestor,Phone,  
		--		Pax, Crew, Comments,Type_Code,LegID) 
		--		VALUES(@Loopdate, @CustomerID, @TailNo, @To, @To, CONVERT(DATETIME, CONVERT(VARCHAR(10), @Loopdate, 101) + ' 00:00', 101),
		--		@NextLocalDTTM, NULL,
		--		'R', @Requestor, @Phone, @Pax, @Crew, @Comments,@Type_Code,@CrLegId)
		
		FETCH NEXT
	FROM curAircraftCal
	INTO @Date 
		,@CustomerID 
		,@TailNo 
		,@From 
		,@To 
		,@DepartureDTTMLocal 
		,@ArrivalDTTMLocal 
		,@NextLocalDTTM 
		,@Duty
		,@Requestor
		,@Phone 
		,@Pax 
		,@Crew 
		,@Comments
		,@Type_Code
		,@CrLegId 
		,@Crdepicao_id
		,@Crarricao_id
	END
	
	CLOSE curAircraftCal;

	DEALLOCATE curAircraftCal;

	

DECLARE @CrDepartureDTTMLocal DATETIME,@CrTailNumber VARCHAR(30),@Cricao_id CHAR(4),@CrDutyTYPE CHAR(2),@crTripDescription VARCHAR(40),@CrArrivalDTTMLocal DATETIME;
DECLARE @DATE1 INT,@Count INT=0,@activedate DATETIME,@CrArrivalDTTMLocal1 DATETIME,@CrAircraftCD CHAR(4),@CrIsApproxTM BIT;
DECLARE @DATElOC DATE,@DATElOC1 DATE;





INSERT INTO #TempTable(Date,DepartureDTTMLocal,TailNo,[From],[To],Duty,Comments,ArrivalDTTMLocal,Type_Code,DType,depicao_id,arricao_id)
SELECT DISTINCT CONVERT(DATE,PL.DepartureDTTMLocal),
       PL.DepartureDTTMLocal, 
       F.TailNum,A.IcaoID,A.IcaoID, PL.DutyTYPE,PM.TripDescription,
       (CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,PL.ArrivalDTTMLocal) THEN PL.ArrivalDTTMLocal ELSE CONVERT(DATETIME, CONVERT(VARCHAR(10),PL.DepartureDTTMLocal, 101) + ' 23:59') END),
       F.AircraftCD,
       'D',A.IcaoID,''
 FROM PreflightMain PM inner join PreflightLeg PL on PM.TripID=PL.TripID AND PL.IsDeleted = 0
 INNER JOIN Fleet F ON F.FleetID=PM.FleetID
 LEFT OUTER JOIN Aircraft AA ON AA.AircraftID=PM.AircraftID
   INNER JOIN ( SELECT DISTINCT FLEETID FROM #TempFleetID )F1 ON F1.FLEETID=F.FLEETID
OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
 WHERE PM.RecordType='M'
 AND ISNULL(PL.DutyTYPE,'')<>'R'
 AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
 AND ( AA.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='') 
 AND PM.IsDeleted = 0
 --AND F.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD) 

AND (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	)

UNION ALL

SELECT CONVERT(DATE,PL.ArrivalDTTMLocal),(CASE WHEN CONVERT(DATE,PL.DepartureDTTMLocal)=CONVERT(DATE,PL.ArrivalDTTMLocal) THEN PL.DepartureDTTMLocal ELSE CONVERT(DATETIME, CONVERT(VARCHAR(10),PL.DepartureDTTMLocal, 101) + ' 00:00') END), 
       F.TailNum,A.IcaoID,A.IcaoID, PL.DutyTYPE,PM.TripDescription,
       PL.ArrivalDTTMLocal,F.AircraftCD
       ,'A',A.IcaoID,''
 FROM PreflightMain PM inner join PreflightLeg PL on PM.TripID=PL.TripID AND PL.IsDeleted = 0
 INNER JOIN Fleet F ON F.FleetID=PM.FleetID
 LEFT OUTER JOIN Aircraft AA ON AA.AircraftID=PM.AircraftID
  INNER JOIN ( SELECT DISTINCT FLEETID FROM #TempFleetID )F1 ON F1.FLEETID=F.FLEETID
OUTER APPLY dbo.GetFleetCurrentAirpotCD(PL.DepartureDTTMLocal, F.FleetID) AS A 
 WHERE PM.RecordType='M'
 AND ISNULL(PL.DutyTYPE,'')<>'R'
 AND PM.CustomerID=dbo.GetCustomerIDbyUserCD(@UserCD)
  AND ( AA.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD='') 
  AND PM.IsDeleted = 0
 --AND F.HomebaseID=dbo.GetHomeBaseByUserCD(@UserCD) 

AND (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	)
	


INSERT INTO #TempTailNum (FleetID,TailNum,Type_Code)
SELECT FleetID,TailNum,AircraftCD FROM Fleet WHERE TailNum IN(SELECT DISTINCT  TailNum FROM #TempTable)


DELETE FROM #TempTable WHERE CONVERT(DATE,Date)=CONVERT(DATE,DepartureDTTMLocal) 
                            AND CONVERT(DATE,Date)=CONVERT(DATE,ArrivalDTTMLocal)
                            AND DType='A'
                             

SET  @TEMPTODAY=@DATEFROM
	
	
		WHILE (CONVERT(DATE,@TEMPTODAY) <= CONVERT(DATE,@DATETO))
		BEGIN		
		
			INSERT INTO #TempTable (TailNo, [Date], [From],Type_Code) 
			SELECT DISTINCT F.TailNum, @TEMPTODAY, dbo.GetHomeBaseCDByUserCD(LTRIM(@UserCD)),F.AircraftCD
			FROM #TempTailNum TempT INNER JOIN Fleet F ON F.FleetID=TempT.FleetID
			INNER JOIN ( SELECT DISTINCT FLEETID FROM #TempFleetID )F1 ON F1.FLEETID=F.FLEETID
			WHERE NOT EXISTS(SELECT T.TailNo FROM #TempTable T 
				WHERE T.TailNo = TempT.TailNum AND T.[Date] = @TEMPTODAY 
				--AND  (F.HomebaseID =  CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR @IsHomeBase=0)
			)
		SET @TEMPTODAY = @TEMPTODAY + 1
		END 
	
--	     --------------------------------------------

--SET @SuppressActivityAircft=1
IF @SuppressActivityAircft=0
BEGIN
SELECT TEMP.* FROM 
	(SELECT DISTINCT  flag=1
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=NULL
					,fbo_phone=NULL
					,fbo_freq=NULL
					,fbo_fuel=NULL
					,crew_hotel_name=NULL
					,crew_hotel_phone=NULL
					,crew_rate=NULL
					,pax_car_name=NULL
					,pax_car_phone=NULL
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
					,Crew
					,Comments
					,Notes
				FROM #TempTable
UNION ALL

SELECT DISTINCT  flag=2
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=NULL
					,crew_hotel_phone=NULL
					,crew_rate=NULL
					,pax_car_name=NULL
					,pax_car_phone=NULL
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
				WHERE DepartureDTTMLocal IS NOT NULL
 
 UNION ALL

SELECT DISTINCT  flag=3
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
				WHERE DepartureDTTMLocal IS NOT NULL
 
 UNION ALL

SELECT DISTINCT  flag=4
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
				WHERE DepartureDTTMLocal IS NOT NULL
 
 UNION ALL

SELECT DISTINCT  flag=5
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
				WHERE DepartureDTTMLocal IS NOT NULL
 
 UNION ALL

SELECT DISTINCT  flag=6
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
				WHERE DepartureDTTMLocal IS NOT NULL)TEMP
WHERE activedate BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
ORDER BY activedate, aircraft_code,locdep

 

END
ELSE
BEGIN
		SELECT TEMP.* FROM 
	(SELECT DISTINCT  flag=1
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=NULL
					,fbo_phone=NULL
					,fbo_freq=NULL
					,fbo_fuel=NULL
					,crew_hotel_name=NULL
					,crew_hotel_phone=NULL
					,crew_rate=NULL
					,pax_car_name=NULL
					,pax_car_phone=NULL
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
  				FROM #TempTable
UNION ALL

SELECT DISTINCT  flag=2
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=NULL
					,crew_hotel_phone=NULL
					,crew_rate=NULL
					,pax_car_name=NULL
					,pax_car_phone=NULL
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
 
 UNION ALL

SELECT DISTINCT  flag=3
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
 
 UNION ALL

SELECT DISTINCT  flag=4
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=NULL
					,pax_hotel_phone=NULL
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				   ,Crew
				   ,Comments
				   ,Notes
				FROM #TempTable 
 
 UNION ALL

SELECT DISTINCT  flag=5
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				    ,Crew
				    ,Comments
				    ,Notes
				FROM #TempTable 
 
 UNION ALL

SELECT DISTINCT  flag=6
					,activedate=CONVERT(DATE,[Date])
					,tail_nmbr=[TailNo]
					,aircraft_code=Type_Code 
					,depicao_id=depicao_id
					,arricao_id=arricao_id
					,locdep=DepartureDTTMLocal
					,locarr=ArrivalDTTMLocal
					,duty_type=[Duty]
					,requestor=[Requestor]
					,requestor_phone=[Phone] 
					,pax_total=[Pax] 
					,fbo_name=fbo_name
					,fbo_phone=fbo_phone
					,fbo_freq=fbo_freq
					,fbo_fuel=fbo_fuel
					,crew_hotel_name=crew_hotel_name
					,crew_hotel_phone=crew_hotel_phone
					,crew_rate=ISNULL(crew_rate,0)
					,pax_car_name=pax_car_name
					,pax_car_phone=pax_car_phone
					,pax_hotel_name=pax_hotel_name
					,pax_hotel_phone=pax_hotel_phone
					,[DateAT] = dbo.GetShortDateFormatByUserCD(@UserCD,[Date])
					,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM)+ '-' + dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
				     ,Crew
				     ,Comments
				     ,Notes
				FROM #TempTable )TEMP
	WHERE CONVERT(DATE,activedate) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
	AND locdep IS NOT NULL
	ORDER BY activedate, aircraft_code,locdep
END			

	--EXEC spGetReportPREAircraftCalendar3ExportInformation 'TIM', '2012-09-01', '2012-09-30', '', '', '',1 
	--EXEC spGetReportPREAircraftCalendar3ExportInformation 'ERICK_1', '2010-05-01', '2011-05-01', '', '', '',0


IF OBJECT_ID('tempdb..#TempTailNum') IS NOT NULL
DROP TABLE #TempTailNum
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
DROP TABLE #TempTable
IF OBJECT_ID('tempdb..#TempFleetID') IS NOT NULL
DROP TABLE #TempFleetID








GO


