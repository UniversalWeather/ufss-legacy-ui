IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogSubInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogSubInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE PROCEDURE [dbo].[spGetReportPOSTPilotLogSubInformation]
(
	@UserCD AS VARCHAR(30), --Mandatory  
	@DATEFROM AS DATETIME, --Mandatory  
	@DATETO AS DATETIME, --Mandatory  
	@CrewCD AS NVARCHAR(1000) = '',  
	@CrewGroupCD AS CHAR(1000) = '',
	@ChecklistCode AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
	@HomeBaseCD AS CHAR(1000) = '', 
	@IsHomebase BIT = 0
		)
  AS
--[spGetReportPOSTPilotLogSubInformation] 'SUPERVISOR_99','2009-1-1','2010-1-1','','','','','','',0,0,0
-- ===============================================================================
-- SPC Name: spGetReportPOSTSubPilotLogInformation
-- Author: A.AKHILA
-- Create date: 
-- Description: Get Postflight SUB Pilot Log for REPORTS
-- Revision History
-- Date Name Ver Change
-- 
-- ================================================================================
BEGIN
SET NOCOUNT ON
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @CUSTOMERID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		HomeBaseID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID,NULL 
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,NULL ---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND CG.IsDeleted=0 
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,NULL 
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
	  AND C.IsDeleted=0
	  AND C.IsStatus=1
END
-----------------------------Crew and Crew Group Filteration----------------------

-----------------------------------HomeBase and HomeBaseCD Filteration-----

IF @IsHomebase=0 AND @HomeBaseCD =''

BEGIN
UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID FROM Crew C 
						 LEFT OUTER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 LEFT OUTER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
                   WHERE C.CustomerID=@CUSTOMERID
)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

IF @IsHomebase=1

BEGIN
UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID  FROM Crew C 
						 INNER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 INNER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
				    WHERE C.CustomerID=@CUSTOMERID
				      
)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

IF @HomeBaseCD <> '' AND @IsHomebase=0
BEGIN

UPDATE #TempCrewID SET HomeBaseID=HomeBase.HomebaseID  FROM 
(
SELECT C.HomebaseID,C.CrewID  FROM Crew C 
						 LEFT OUTER JOIN Company CO ON C.HomebaseID=CO.HomebaseAirportID
						 LEFT OUTER JOIN UserMaster UM ON CO.HomebaseID=UM.HomebaseID
						 INNER JOIN Airport A ON C.HomebaseID=A.AirportID
				    WHERE C.CustomerID=@CUSTOMERID
				      AND A.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ','))

)HomeBase
WHERE HomeBase.CrewID=#TempCrewID.CrewID
END

-----------------------------------HomeBase and HomeBaseCD Filteration-----




SELECT DISTINCT [CHECKLISTCODE] = CCL.CrewCheckCD,  
				[ChecklistItem]=ccl.CrewChecklistDescription,
				[ChecklistLast]=CCD.PreviousCheckDT,
				[ChecklistDue]= CCD.DueDT,
				[ChecklistAlert]= CCD.AlertDT,
				[ChecklistGrace]= CCD.GraceDT,
				[CrewID]=c.crewid,[CREW]=C.CREWCD,
				[case1]=  (CASE WHEN (@DATETO BETWEEN CCD.DUEDT AND CCD.ALERTDT) THEN 'ALERT'	
						   WHEN (@DATETO > CCD.DUEDT) THEN 'PASSED DUE' END)
			FROM PostflightLeg PL 
				INNER JOIN PostflightMain PM ON PM.POLogID = PL.POLogID AND PM.IsDeleted = 0
				INNER JOIN PostflightCrew PC ON PC.POLegID = PL.POLegID
				INNER JOIN CrewCheckListDetail CCD ON CCD.CrewID=PC.CrewID and CCD.CustomerID= pc.CustomerID
				INNER JOIN Crew C ON C.CrewID = PC.CrewID
				INNER JOIN CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD and  ccl.IsDeleted =0 and CCD.IsDeleted=0 and CCD.CustomerID= ccl.CustomerID
				INNER JOIN #TempCrewID TC ON TC.CrewID=C.CrewID AND TC.HomeBaseID=C.HomebaseID
			WHERE PL.CustomerID =  CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
				AND CONVERT(DATE,PL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
				--AND (pC.CrewID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewID, ','))OR @CrewID = '')
				AND (CCL.CrewCheckCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ChecklistCode, ',')) OR @ChecklistCode = '')
				AND c.IsDeleted = 0
				AND PL.IsDeleted = 0
           ORDER BY CHECKLISTCODE







END
IF OBJECT_ID('tempdb..#TempCrewID') IS NOT NULL
DROP TABLE #TempCrewID 

GO