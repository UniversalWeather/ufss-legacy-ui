IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotLogMHTMLInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotLogMHTMLInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
CREATE PROCEDURE  [dbo].[spGetReportPOSTPilotLogMHTMLInformation]   
  (
	  @UserCD AS VARCHAR(30), --Mandatory  
	  @DATEFROM AS DATETIME, --Mandatory  
	  @DATETO AS DATETIME, --Mandatory  
	  @CrewCD AS NVARCHAR(1000) = '',  
	  @CrewGroupCD AS CHAR(1000) = '',
	  @AircraftCD AS NVARCHAR(1000) = '',
	  @FlightCatagoryCD AS CHAR(1000) = '',
	  @HomeBase AS CHAR(1000) = '', 
	  @HomebaseID BIT = 0,
	  @PrintOnTime BIT = 0,
	  @PrintOffTime BIT = 0  
  ) 
AS  
   
-- ===============================================================================  
-- SPC Name: [spGetReportPOSTPilotLogMHTMLInformation]  
-- Author: Avanikanta Pradhan  
-- Create date: 06 August  
-- Description: Get POST flight Delay Details  
-- Revision History  
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON
DECLARE @CREW TABLE (CREWID BIGINT)
INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND IsStatus = 1
IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO 
SELECT distinct
[CREWID]= crew.CrewID
		 ----,[LEGID]=POL.POLegID
		 ----,[LOGID]=POM.POLogID		
         ,[crewcode] = crew.CrewCD
		 ,[lognum] = POM.LogNum 
		 ,[legid] = POL.POLegID
		 ,[dutyend] = POL.IsDutyEnd
		 ,[last_name] = crew.LastName
		 ,[first_name] = crew.FirstName
		 ,[middleinit] = crew.MiddleInitial
		 ,[schedttm] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END
		 ,[fltdate] = CASE WHEN C.DutyBasis = 2 OR POL.OutboundDTTM = NULL THEN POL.ScheduledTM ELSE  POL.OutboundDTTM END
		 ,[tail_nmbr] = F.TailNUM
		 ,[type_code] = CASE WHEN POM.LogNum='' OR POM.LogNum IS NULL THEN 'NO ACTIVITY' ELSE F.AircraftCD END		 
		 ,[tot_line] = ''
		 ,[farnum] = POL.FedAviationRegNum
		 ,[crewcurr] = POL.CrewCurrency
		 ,[depicao_id] = A.IcaoID
		 ,[arricao_id] = AA.IcaoID
		 ,[cat_code] = FC.FlightCatagoryCD
		 ,[dutycounter] = CASE WHEN POM.LogNum IS NULL OR POM.LogNum = '' THEN '0' END
		 ,[dutytotal] = CASE WHEN (CASE WHEN POL.InboundDTTM <= @DATETO THEN 'True' ELSE 'False' END) = 'True' OR ((CASE WHEN POC.CrewDutyStartTM >= @DATEFROM THEN POC.BeginningDuty END) <> '' AND (CASE WHEN POC.CrewDutyStartTM <= @DATETO THEN POC.DutyEnd END) <> '') THEN ROUND(POC.DutyHours,1) END
         ,[blkout] =  REPLACE(POL.BlockOut,':','')
		 ,[blkin] =   REPLACE(POL.BlockIN,':','')
		 ,[dutyhrs] = ROUND(POC.DutyHours,1)
		 ,[dutytype]= POC.DutyTYPE 
		 ,[blk_hrs] = ROUND(POC.BlockHours,1)
		 ,[flt_hrs] = ROUND(POC.FlightHours,1)
		 ,[night] = POC.Night
		 ,[instr] = POC.Instrument 
		 ,[tkoff_d] = POC.TakeOffDay
		 ,[tkoff_n] = POC.TakeOffNight
		 ,[app_p] = POC.ApproachPrecision
		 ,[app_n] = POC.ApproachNonPrecision
		 ,[lnding_d] = POC.LandingDay
		 ,[lnding_n] = POC.LandingNight
		 ,[override] = POC.IsOverride
		 ,[dutybeg] = POC.BeginningDuty
		 ,[end] = POC.DutyEnd
		 ,[ontime] =   REPLACE(POC.BeginningDuty,':','')
		 ,[offtime] =   REPLACE(POC.DutyEnd,':','')
		 ,[ron] = POC.RemainOverNight
		 ,[assoc_crew] = ACrew.AssociatedCrew
		 ,[item] = CCL.CrewChecklistDescription
		 ,[lastaccepted] = CCD.PreviousCheckDT 
		 ,[duedate] = CCD.DueDT
		 ,[alertdate] = CCD.AlertDT
		 ,[gracedate] = CCD.GraceDT
		 ,[msgcode] = CASE WHEN CCD.IsOneTimeEvent = 'True' AND CCD.IsCompleted ='True' THEN 'COMPLETED'
                    WHEN @DATETO > CCD.DueDT AND @DATETO <= CCD.GraceDT THEN 'GRACE'
                    WHEN @DATETO > CCD.DueDT THEN 'PAST DUE'
                    WHEN @DATETO BETWEEN CCD.AlertDT AND CCD.DueDT THEN 'ALERT'
                    ELSE ''
               END 
		 ,[outdttm] = POC.OutboundDTTM
		 ,[crewduty_type] = CDT.DutyTypeCD
		 ,[client] = CT.ClientCD
		 ,[spec3] = POC.Specification3
		 ,[spec4] = POC.Specification4
		 ,[speclong3] = C.SpecificationLong3
		 ,[speclong4] = C.SpecificationLong4
		 ,[specdec3] = C.Specdec3
		 ,[specdec4] = C.Specdec4
		 ,[estdepdt] = POM.EstDepartureDT
		 ,[leg_num] = POL.LegNUM
		 INTO #CREWINFO
	   	FROM PostflightMain POM 
		INNER JOIN  PostflightCrew POC ON POC.POLogID = POM.POLogID
		INNER JOIN PostflightLeg POL ON POL.POLegID = POC.POLegID AND POL.IsDeleted = 0
		inner JOIN (  
        SELECT DISTINCT Cw.CustomerID, Cw.CREWID,CG.CrewGroupCD,Cw.LastName,Cw.CrewCD, CG.HomebaseID,cw.FirstName,cw.MiddleInitial, Cw.IsStatus, Cw.IsDeleted
        FROM CREW Cw 
        left outer JOIN CREWGROUPORDER CGO ON Cw.CREWID = CGO.CREWID AND Cw.CustomerID = CGO.CustomerID  
		left outer JOIN CREWGROUP CG ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID  
		WHERE ((CG.CrewGroupCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) OR @CrewGroupCD = '')
		OR CG.CREWGroupCD = (CASE WHEN @CREWGroupCD = '' THEN CG.CREWGroupCD END))  
		) Crew ON poc.CREWID = Crew.CREWID AND poc.CustomerID = Crew.CustomerID and Crew.IsStatus=1 and Crew.IsDeleted=0
		
		INNER JOIN Fleet F ON F.FleetID = POM.FleetID AND F.CustomerID=POM.CustomerID
		LEFT JOIN Company C ON C.HomebaseID = F.HomebaseID
		INNER JOIN Airport A ON A.AirportID = POL.DepartICAOID
	    INNER JOIN Airport AA ON AA.AirportID = POL.ArriveICAOID
	    LEFT JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
	    LEFT JOIN PostflightSimulatorLog POSL ON POSL.CrewID = POC.CrewID
	    LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeID = POSL.DutyTypeID
	    LEFT JOIN Client CT ON CT.ClientID = POSL.ClientID
	    LEFT JOIN Aircraft AC ON AC.AircraftID = f.AircraftID
	   LEFT JOIN Airport AF ON AF.AirportID = C.HomebaseAirportID
     --LEFT JOIN CrewCheckListDetail CCLD ON CCLD.CrewID = crew.CrewID
        left JOIN CrewCheckListDetail CCD ON CCD.CrewID=PoC.CrewID AND CCD.IsDeleted=0 
	    --LEFT JOIN CrewCheckList CCL ON CCL.ClientID = CT.ClientID
	    left join CrewCheckList ccl on ccd.CheckListCD = ccl.CrewCheckCD AND ccl.IsDeleted=0 and ccl.CustomerID= PoC.CustomerID
   
		INNER JOIN (SELECT DISTINCT POC1.POLegID, AssociatedCrew = (
				 SELECT RTRIM(CW.CrewCD) + ',' 
				 FROM PostflightCrew POC 
				 INNER JOIN Crew CW ON POC.CrewID = CW.CrewID 
				 WHERE POC.POLegID = POC1.POLegID FOR XML PATH(''))
				 FROM PostflightCrew POC1) ACrew ON POL.POLegID = ACrew.POLegID

	   WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)))
       AND CONVERT(DATE,POL.OutboundDTTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
       --AND POM.POLogID='1000145572'
       AND (Crew.CrewCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
       AND (AC.AircraftCD   IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
	   AND (FC.FlightCatagoryCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')  
	     AND pom.HomebaseID IN(CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))) OR @HomebaseID=0
	   AND AF.IcaoID IN(CASE(@HomebaseID) WHEN 0 THEN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBase, ','))ELSE @HomeBase END)
	   AND POM.IsDeleted = 0
	
      -- AND (AF.IcaoID  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBase, ',')) OR @HomeBase = '')
      --AND (pom.HomebaseID = dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)) OR @HomebaseID = 0)  
 
SELECT * FROM #CREWINFO
UNION ALL 
SELECT DISTINCT
[CREWID]= crew.CrewID
		 --,[LEGID]=POL.POLegID
		 --,[LOGID]=POM.POLogID		
         ,[crewcode] = crew.CrewCD
		 ,[lognum] = NULL
		 ,[legid] = NULL
		 ,[dutyend] = NULL
		 ,[last_name] = crew.LastName
		 ,[first_name] = crew.FirstName
		 ,[middleinit] = crew.MiddleInitial
		 ,[schedttm] = NULL
		 ,[fltdate] = NULL
		 ,[tail_nmbr] = NULL
		 ,[type_code] = NULL
		 ,[tot_line] = ''
		 ,[farnum] = NULL
		 ,[crewcurr] = NULL
		 ,[depicao_id] = NULL
		 ,[arricao_id] = NULL
		 ,[cat_code] = NULL
		 ,[dutycounter] =ISNULL(NULL,0)
		 ,[dutytotal] = NULL
         ,[blkout] =  NULL
		 ,[blkin] = NULL
		 ,[dutyhrs] = NULL
		 ,[dutytype]= NULL
		 ,[blk_hrs] = NULL
		 ,[flt_hrs] = NULL
		 ,[night] = NULL
		 ,[instr] = NULL
		 ,[tkoff_d] = NULL
		 ,[tkoff_n] = NULL
		 ,[app_p] = NULL
		 ,[app_n] = NULL
		 ,[lnding_d] = NULL
		 ,[lnding_n] = NULL
		 ,[override] = NULL
		 ,[dutybeg] = NULL
		 ,[end] = NULL
		 ,[ontime] = NULL
		 ,[offtime] = NULL
		 ,[ron] = NULL
		 ,[assoc_crew] = NULL
		 ,[item] = NULL
		 ,[lastaccepted] = NULL
		 ,[duedate] = NULL
		 ,[alertdate] = NULL
		 ,[gracedate] = NULL
		 ,[msgcode] = NULL
		 ,[outdttm] = NULL
		 ,[crewduty_type] = NULL
		 ,[client] = NULL
		 ,[spec3] = NULL
		 ,[spec4] = NULL
		 ,[speclong3] = NULL
		 ,[speclong4] = NULL
		 ,[specdec3] = NULL
		 ,[specdec4] = NULL
		 ,[estdepdt] = NULL
		 ,[leg_num] = NULL
		 from Crew crew
		 left outer JOIN @CREW CT ON crew.CrewID = CT.CREWID
		  LEFT OUTER JOIN CrewCheckListDetail CCD ON CCD.CrewID=CREW.CrewID AND CCD.IsDeleted=0  
		LEFT OUTER JOIN CrewCheckList ccl on ccl.CrewCheckCD = ccd.CheckListCD AND ccl.IsDeleted=0 and ccl.CustomerID= crew.CustomerID
		 --left outer JOIN  PostflightCrew POC ON POC.CrewID = crew.CrewID AND poc.CustomerID = Crew.CustomerID 

		
		WHERE crew.IsDeleted=0 and crew.IsStatus=1 and 
		crew.CREWID NOT IN (SELECT DISTINCT CREWID FROM #CREWINFO)
		AND
		 crew.CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)
		AND(crew.CrewCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) OR @CrewCD = '')
		
		order by crewcode 
	
 
   --EXEC spGetReportPOSTPilotLogMHTMLInformation 'CPE�A_1','2012-01-01','2012-12-12','DFG','DFG','FG','DFG','DFG',0,0,0
   --EXEC spGetReportPOSTPilotLogMHTMLInformation 'CPE�A_1','2000/1/1','2012/12/12','','','','','',0,0,0


GO


