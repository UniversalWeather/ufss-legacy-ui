IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vCrewGroup]'))
DROP VIEW [dbo].[vCrewGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vCrewGroup]
AS SELECT C.*
	, CG.CrewGroupID
	,CG.CrewGroupCD
	,CG.CrewGroupDescription
	,CG.CrewGroupOrderID
	,CG.OrderNum
	FROM CREW C LEFT JOIN (SELECT CG.CrewGroupID
			,CG.CrewGroupCD
			,CG.CustomerID
			,CG.CrewGroupDescription
			,CGO.CrewGroupOrderID
			,CGO.CrewID
			,CGO.OrderNum
		 FROM CrewGroup CG
			JOIN CrewGroupOrder CGO
			ON CG.CrewGroupID= CGO.CrewGroupID AND CG.CustomerID=CGO.CustomerID) CG
	   ON C.CrewID=CG.CrewID AND C.CustomerID= CG.CustomerID

GO


