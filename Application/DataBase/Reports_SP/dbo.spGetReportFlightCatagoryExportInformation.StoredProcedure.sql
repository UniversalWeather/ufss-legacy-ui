IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportFlightCatagoryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportFlightCatagoryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spGetReportFlightCatagoryExportInformation]
	@UserCD varchar(30)
AS
-- =============================================
-- SPC Name: spGetReportFlightCatagoryExportInformation
-- Author: SUDHAKAR J
-- Create date: 27 Jun 2012
-- Description: Get Flight Catagory Information for Report
-- Revision History
-- Date		Name		Ver		Change
-- 29 Aug	Sudhakar	1.1		ClientID filter
-- 
-- =============================================

SET NOCOUNT ON


DECLARE @CLIENTID BIGINT, @CUSTOMERID BIGINT;
SELECT @CLIENTID = dbo.GetClientIDbyUserCD(RTRIM(@UserCD));
SELECT @CUSTOMERID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD));

	SELECT
		[fltcatcode] = F.FlightCatagoryCD
		,[fltcatdesc] = F.FlightCatagoryDescription
		,[graphcolor] = F.GraphColor
		,[client] = C.ClientCD
		,[lastuser] = F.LastUpdUID
		,[lastupdt] = F.LastUpdTS
		,[custcolor] = F.ForeGrndCustomColor
		,[bcustcolor] = F.BackgroundCustomColor
		,[calkey] = F.CallKey
		,[inactive] = F.IsInActive
	FROM FlightCatagory F
	LEFT OUTER JOIN Client C ON F.ClientID = C.ClientID
	WHERE F.IsDeleted = 0
	AND F.CustomerID = @CUSTOMERID
	AND ( F.ClientID = @CLIENTID OR @CLIENTID IS NULL )
	-- What about 'IsInactive', need to be considered in filter??
	ORDER BY F.FlightCatagoryCD
  -- EXEC spGetReportFlightCatagoryExportInformation 'UC'

GO


