IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREAircraftCalendar3FBOInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREAircraftCalendar3FBOInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPREAircraftCalendar3FBOInformation]
	@UserCD VARCHAR(30),
	@LegID VARCHAR(30) = ''
AS
-- ===============================================================================
-- SPC Name: spGetReportPREAircraftCalendar3FBOInformation
-- Author: AISHWARYA.M
-- Create date: 19 Jul 2012
-- Description: Get Preflight Aircraft Calendar FBO information for REPORTS
-- Revision History
-- Date			Name		Ver		Change
-- 
-- ================================================================================
SET NOCOUNT ON

IF @LegID <> '' BEGIN
	SELECT DISTINCT 
		 PL.LegID
		 ,[TailNum] = F.TailNum
		,[FBO] = PF.PreflightFBOName
		,[FPhone] = PF.PhoneNum1
		,[Freq] = PF.Frequency
		,[Fuel] = Isnull(PF.FuelQty,NULL)
		,[Rate] = Isnull(CHT.NegociatedRate,0.00)
		,[CrewHotel] = CHT.Name
		,[HPhone] = CHT.PhoneNum
		,[PaxCar] = TPD.TransportationVendor 
		,[TPhone] = TPD.PhoneNum
		,[PaxHotel] = PHT.Name
		,[PPhone] = PHT.PhoneNum 
		,[Notes] = PL.Notes
		FROM PreflightLeg PL
			INNER JOIN (SELECT TripID, FleetID, CustomerID, IsDeleted FROM PreflightMain) PM ON PM.TripID = PL.TripID AND PM.IsDeleted = 0
			INNER JOIN (SELECT FleetID, TailNum FROM Fleet) F ON PM.FleetID = F.FleetID

			--LEFT OUTER JOIN (SELECT LegID, PreflightHotelName, PhoneNum1,HOTELID FROM PreflightHotelXrefList) PH ON PH.LegID = PL.LegID
			--LEFT OUTER JOIN (SELECT HOTELID,NegociatedRate FROM Hotel)H ON PH.HOTELID=H.HotelID
			
			--LEFT OUTER JOIN (SELECT LegID, PreflightTransportName, PhoneNum1, CrewPassengerType FROM PreflightTransportList) PT
			--ON PT.LegID = PL.LegID  AND PT.CrewPassengerType = 'P'

			LEFT OUTER JOIN (
				SELECT PTL.ConfirmationStatus, PTL.Comments, PTL.LegID, T.TransportationVendor, T.PhoneNum 
				FROM PreflightTransportList PTL 
				INNER JOIN Transport T ON PTL.TransportID = T.TransportID AND PTL.IsArrivalTransport = 1 AND PTL.CrewPassengerType = 'P'
			) TPD ON PL.LegID = TPD.LegID
						
			LEFT OUTER JOIN (
				SELECT LegID, PreflightFBOName, PhoneNum1, Frequency, FuelQty
				FROM PreflightFBOList FL
				INNER JOIN (SELECT FBOID, Frequency, FuelQty FROM FBO) FB ON FL.FBOID = FB.FBOID AND FL.IsArrivalFBO = 1
			) PF ON PL.LegID = PF.LegID			
			
			--Pax Hotel
			LEFT OUTER JOIN ( SELECT 
				PPL.LegID,PPL.PreflightPassengerListID,HP.Name,HP.PhoneNum,HP.FaxNum,HP.Addr1,HP.Addr2,HP.CityName,
				HP.StateName,HP.PostalZipCD,HP.Remarks,HP.NegociatedRate,
				PPL.NoofRooms,PPH.ConfirmationStatus,PPH.Comments 
			FROM PreflightPassengerList PPL 
			INNER JOIN PreflightPassengerHotelList PPH ON PPL.PreflightPassengerListID = PPH.PreflightPassengerListID
			INNER JOIN Hotel HP ON PPH.HotelID = HP.HotelID
			) PHT ON PL.legID = PHT.LegID
			
			--Crew Hotel
			LEFT OUTER JOIN (			
				SELECT PCLL.LegID,HC.Name,HC.PhoneNum,HC.FaxNum,HC.Addr1,HC.Addr2,HC.CityName,
					 HC.StateName,HC.PostalZipCD,HC.Remarks,HC.NegociatedRate,
					 PCLL.NoofRooms,PCH.ConfirmationStatus,PCH.Comments  
				FROM PreflightCrewList PCLL 
				--INNER JOIN PreflightCrewHotelList PCH ON PCLL.PreflightCrewListID = PCH.PreflightCrewListID
				INNER JOIN PreflightHotelList PCH ON PCLL.LegID = PCH.LegID AND crewPassengerType = 'C' AND isArrivalHotel = 1
				INNER JOIN PreflightHotelCrewPassengerList PHCP ON PCH.PreflightHotelListID = PHCP.PreflightHotelListID 
																	AND PHCP.CrewID = PCLL.CrewID
				INNER JOIN Hotel HC ON PCH.HotelID = HC.HotelID
			) CHT ON PL.legID = CHT.LegID	
					
			WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
			AND PL.LegID = CONVERT(BIGINT,@LegID)
			AND PL.IsDeleted = 0
END ELSE BEGIN
	SELECT 
		 [LegID] = ''
		 ,[TailNum] = ''
		,[FBO] = ''
		,[FPhone] = ''
		,[Freq] = ''
		,[Fuel] = ''
		,[Rate] = ''
		,[CrewHotel] = ''
		,[HPhone] = ''
		,[PaxCar] = ''
		,[TPhone] = ''
		,[PaxHotel] = ''
		,[PPhone] = ''
		,[Notes] = ''

END
		
	 
			 
	--EXEC spGetReportPREAircraftCalendar3FBOInformation 'ERICK_1', 1000157204
	--10000351
	--10000354



GO


