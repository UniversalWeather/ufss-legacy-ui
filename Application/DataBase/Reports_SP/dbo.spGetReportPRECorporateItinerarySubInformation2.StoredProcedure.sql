IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRECorporateItinerarySubInformation2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetReportPRECorporateItinerarySubInformation2]
		
		@UserCD AS VARCHAR(30), --Mandatory
		@LegID AS VARCHAR(30)   --Mandatory

AS
-- ==========================================================================================
-- SPC Name: spGetReportPRECorporateItinerarySubInformation2
-- Author: ABHISHEK.S
-- Create date: 7th NOV 2012
-- Description: Get Corporate Itinerary Information For Reports
-- Revision History
-- Date		Name		Ver		Change
-- 
-- ==========================================================================================
SET NOCOUNT ON
	BEGIN 
SELECT DISTINCT
			[TripID] = PM.TripID,
			[FileNo] = PM.TripNUM,
			[LegID] = PL.LegID,						
			[CREW] = ISNULL(C.LastName,'')+Case when ISNULL(C.LastName,'')='' then '' Else ', ' End + ISNULL(C.FirstName,'') +' '+ ISNULL(C.MiddleInitial,''),		
			[CrewAccomodations] = HC.Name,
			[Crew_hotel_phone] = HC.PhoneNum 
			
		FROM PreflightMain PM 
		         INNER JOIN PreflightLeg PL
		                ON PM.TripID = PL.TripID AND PL.IsDeleted = 0		        
		         INNER JOIN PreflightCrewList PCLL
		                ON PCLL.LegID = PL.LegID 
		         INNER JOIN Crew C
		                ON PCLL.CrewID = C.CrewID
		         INNER JOIN PreflightHotelList PCH
						ON  PCH.LegID = PCLL.LegID AND PCH.isArrivalHotel = 1 AND PCH.crewPassengerType = 'C'
				 INNER JOIN PreflightHotelCrewPassengerList PHCP ON PHCP.PreflightHotelListID = PCH.PreflightHotelListID
													AND PHCP.CrewID = C.CrewID
		         --INNER JOIN PreflightCrewHotelList PCH
		         --       ON PCH.PreflightCrewListID = PCLL.PreflightCrewListID
		         INNER JOIN Hotel HC
					    ON PCH.HotelID = HC.HotelID	
		WHERE PM.CustomerID = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))
		AND PL.LegID = convert(Bigint,@LegID)
		AND PM.IsDeleted = 0

		  
	END
	
	-- EXEC spGetReportPRECorporateItinerarySubInformation2 'ERICK_1','1000157204'


GO


