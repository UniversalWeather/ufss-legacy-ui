IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPREMonthlyCrewCalendarExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPREMonthlyCrewCalendarExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[spGetReportPREMonthlyCrewCalendarExportInformation] 
  @UserCD AS VARCHAR(30),
  @MONTH AS CHAR(3),
  @YEAR  AS INT,
  @CrewDutyTypeCD AS VARCHAR(100)= '',
  @CrewCD AS NVARCHAR(500)= '',
  @CrewGroupCD AS NVARCHAR(500)= '',
  @AircraftCD AS NVARCHAR(500)= '',
  @IsHomeBase AS BIT = 0
 AS
 
 BEGIN
  --DROP TABLE #TempCrewID
  --DROP TABLE #CrewTemp


  DECLARE @date DATETIME
  DECLARE @CUSTOMERID BIGINT =  CONVERT(VARCHAR,dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD)));

Declare @SuppressActivityCrew BIT  
 	
SELECT @SuppressActivityCrew=IsZeroSuppressActivityCrewRpt FROM Company WHERE CustomerID=@CustomerID 
										 AND IsZeroSuppressActivityAircftRpt IS NOT NULL
										 AND HomebaseID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))
										 

--SET @@SuppressActivityCrew=1
 -----------------------------Crew and Crew Group Filteration----------------------

CREATE TABLE  #TempCrewID   
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		CrewID BIGINT,
		CustomerID BIGINT
  )
  

IF @CrewCD <> ''
BEGIN
	INSERT INTO #TempCrewID
	SELECT DISTINCT C.CrewID,C.CustomerID
	FROM Crew C
	WHERE C.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ',')) 
	AND C.CustomerID = @CUSTOMERID  
	AND C.IsDeleted=0
END

IF @CrewGroupCD <> ''
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,C.CustomerID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
	FROM Crew C 
		LEFT OUTER JOIN CrewGroupOrder CGO 
					ON C.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
		LEFT OUTER JOIN CrewGroup CG 
					ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
	WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
	AND C.CustomerID = @CUSTOMERID 
	AND C.IsDeleted=0 
	AND C.IsStatus=1
END
ELSE IF @CrewCD = '' AND  @CrewGroupCD = '' 
BEGIN  
INSERT INTO #TempCrewID
	SELECT DISTINCT  C.CrewID,C.CustomerID
	FROM Crew C 
	WHERE C.CustomerID = @CUSTOMERID  
	AND C.IsDeleted=0
END
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @SQLSCRIPT AS NVARCHAR(4000) = '';


 
SELECT @date=CONVERT(DATETIME,CONVERT(VARCHAR(40),CONVERT(VARCHAR(10),@YEAR)+'-'+@MONTH+'-'+'01' ))

DECLARE @DATETO DATE =(SELECT CONVERT(DATE,DATEADD(dd,-(DAY(DATEADD(mm,1,@date))),DATEADD(mm,1,@date)))); 
         DECLARE @TblDates TABLE (AllDates DATE);
          WITH dates(DATEPARAM)
           AS (SELECT @date AS DATETIME
               UNION ALL
               SELECT Dateadd(day, 1, DATEPARAM)
               FROM   Dates
               WHERE  DATEPARAM < @DATETO)
      INSERT INTO @TblDates (AllDates)
      SELECT DATEPARAM
      FROM   Dates
      OPTION (maxrecursion 10000)
   
 CREATE TABLE #CrewTemp (CrewID BIGINT,  
                   CrewMember VARCHAR(60), 
                   CrewCD VARCHAR(5),
                   ScheduleDate DATE,
                   HomeBase BIGINT
                   ,AircraftCD VARCHAR(10)
                   )    
       INSERT INTO #CrewTemp                
       SELECT  C.CrewID,LastName+','+FirstName,C.CrewCD,AllDates ScheduleDate,C.HomebaseID,'' 
            FROM  Crew C INNER JOIN #TempCrewID T ON C.CrewID=T.CrewID           
            CROSS JOIN @TblDates  
            WHERE  C.CustomerID = @CUSTOMERID
              AND C.IsDeleted=0
              AND C.IsStatus=1
              

      
DECLARE @CrewInfo TABLE(TripNUM BIGINT,
                        LegNUM BIGINT,
                        TripID BIGINT,
                        LegID BIGINT,
                        Depart DATETIME,
                        Arrive DATETIME,
                         NextLocal DATETIME,
                        TripStatus CHAR(1),
                        DutyTYPE CHAR(2),
                        CrewID BIGINT,
                        AircraftCD VARCHAR(10),
                        HomeBase BIGINT,
                        CrewCD VARCHAR(5)
                       )
  
  




INSERT INTO @CrewInfo            
SELECT  PM.TripNUM,PL.LegNUM,PM.TripID,PL.LegID,CASE WHEN PL.DepartureDTTMLocal < @DATE THEN @DATE  ELSE PL.DepartureDTTMLocal END  Depart,
                 CASE WHEN PL.ArrivalDTTMLocal > @DATE+30 THEN @DATE+30  ELSE PL.ArrivalDTTMLocal END  Arrive,PL.NextLocalDTTM,
                 PM.TripStatus, PL.DutyTYPE,CW.CrewID,AT.AircraftCD,CW.HomebaseID,CW.CrewCD FROM PreflightMain PM 
                            INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                            INNER JOIN (SELECT LegID , CrewID,CustomerID FROM PreflightCrewList) PC ON PL.LegID = PC.LegID AND PM.CustomerID=PC.CustomerID
                            INNER JOIN Crew CW ON PC.CrewID = CW.CrewID AND PC.CustomerID=CW.CustomerID
                            INNER JOIN #TempCrewID T ON CW.CrewID=T.CrewID
                            LEFT OUTER JOIN Aircraft AT ON PM.AircraftID=AT.AircraftID
                      
                      
WHERE 
 (	
			(CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.DepartureDTTMLocal) >= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.DepartureDTTMLocal) <= CONVERT(DATE,@DATETO))
		  OR
			(CONVERT(DATE,PL.ArrivalDTTMLocal) >= CONVERT(DATE,@DATE) AND CONVERT(DATE,PL.ArrivalDTTMLocal) <= CONVERT(DATE,@DATETO))
	 ) 
AND PM.CustomerID = @CUSTOMERID
AND PM.RecordType <> 'M'
AND PM.TripStatus IN ('T','H')
AND (AT.AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')) OR @AircraftCD = '')
AND PM.IsDeleted = 0
---AND (PM.HomebaseID = CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))) OR @IsHomeBase = 0 ) 
 

ORDER BY PL.DutyTYPE,PL.DepartureDTTMLocal,PL.ArrivalDTTMLocal,PL.TripID,PL.LegID

INSERT INTO @CrewInfo
SELECT T.TripNUM,PL.LegNUM,T.TripID,PL.LegID,CASE WHEN PL.DepartureDTTMLocal < @DATE THEN @DATE  ELSE PL.DepartureDTTMLocal END  Depart,
          CASE WHEN PL.ArrivalDTTMLocal > @DATETO THEN @DATETO  ELSE PL.ArrivalDTTMLocal END  Arrive,
          PL.NextLocalDTTM,T.TripStatus,PL.DutyTYPE,T.CrewID,T.AircraftCD,T.HomeBase,T.CrewCD F
                                             FROM @CrewInfo T 
                                             INNER JOIN PreflightMain PM ON T.TripID=PM.TripID AND PM.IsDeleted = 0
                                             INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
                                             WHERE PL.LegNUM=T.LegNUM-1
                                                
                                              --- AND T.TripNUM=1471




DECLARE @CrewCalendar TABLE(RowID INT IDENTITY,
                            TripNum BIGINT,
                            LegNum BIGINT,
                            TripID BIGINT,
                            LegID BIGINT,
                            ScheduleDate DATETIME,
                            TripStatus CHAR(1),
                            DutyType CHAR(2),
                            CrewID BIGINT,
                            CrewMember VARCHAR(100),
                            CrewCD VARCHAR(5),
                            AircraftCD VARCHAR(10),
                            HomeBase BIGINT,
                            TransCrew BIGINT)
------------------------------------------------------
IF @AircraftCD <> '' 
BEGIN
INSERT INTO @CrewCalendar        
SELECT DISTINCT TEMP.TripNUM,TEMP.LegNUM,TEMP.TripID,TEMP.LegID,
T.ScheduleDate,TEMP.TripStatus,  
                (CASE WHEN ISNULL(TEMP.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(TEMP.TripStatus, '') = 'H' THEN  'H' 
										     WHEN ISNULL(TEMP.TripStatus, '') = 'T' THEN  'F' ELSE '' END
									ELSE TEMP.DutyTYPE END) DutyTYPE,
                T.CrewID,T.CrewMember,T.CrewCD,TEMP.AircraftCD,T.HomeBase,TransCrew FROM #CrewTemp T  
LEFT OUTER JOIN (  
SELECT C.CrewID,CI.CrewCD,CI.AircraftCD,CI.HomeBase,CI.TripNUM,CI.TripID,C.CrewMember CrewMember
,CI.Depart,CI.LegNUM,CI.LegID,C.ScheduleDate
,CI.Arrive,CI.NextLocal
,CASE WHEN 
CONVERT(DATE,C.ScheduleDate) > CONVERT(DATE,CI.Arrive) AND CONVERT(DATE,C.ScheduleDate) < CONVERT(DATE,CI.NextLocal) THEN 'R' 
ELSE CI.DutyTYPE END DutyTYPE,CI.TripStatus ,CI.CrewID TransCrew
                               FROM #CrewTemp C LEFT OUTER JOIN @CrewInfo CI ON C.CrewID=CI.CrewID
                               WHERE CONVERT(DATE, C.ScheduleDate) >= CONVERT(DATE,CI.Depart)
                                 AND  CONVERT(DATE,C.ScheduleDate) <= CONVERT(DATE,ISNULL(CI.NextLocal,CI.Arrive))
                                
                                )TEMP ON T.CrewID=TEMP.CrewID AND T.ScheduleDate=TEMP.ScheduleDate
                              --  WHERE T.CrewID=1001351540
  
WHERE   (T.CrewCD IN (SELECT CrewCD FROM @CrewInfo WHERE AircraftCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ',')))) 
 --- AND  (T.CrewCD IN(SELECT CrewCD FROM @CrewInfo WHERE HomeBase =CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD))))) 
 ORDER BY T.ScheduleDate

 END
ELSE
BEGIN
INSERT INTO @CrewCalendar        
SELECT DISTINCT TEMP.TripNUM,TEMP.LegNUM,TEMP.TripID,TEMP.LegID,
T.ScheduleDate,TEMP.TripStatus,  
                (CASE WHEN ISNULL(TEMP.DutyTYPE, '') = '' THEN 
										CASE WHEN ISNULL(TEMP.TripStatus, '') = 'H' THEN  'H' 
										     WHEN ISNULL(TEMP.TripStatus, '') = 'T' THEN  'F' ELSE 'AA' END
									ELSE TEMP.DutyTYPE END) DutyTYPE,
                T.CrewID,T.CrewMember,T.CrewCD,TEMP.AircraftCD,T.HomeBase,TransCrew  FROM #CrewTemp T  
LEFT OUTER JOIN (  
SELECT C.CrewID,CI.CrewCD,CI.AircraftCD,CI.HomeBase,CI.TripNUM,CI.TripID,C.CrewMember CrewMember
,CI.Depart,CI.LegNUM,CI.LegID,C.ScheduleDate
,CI.Arrive,CI.NextLocal
,CASE WHEN 
CONVERT(DATE,C.ScheduleDate) > CONVERT(DATE,CI.Arrive) AND CONVERT(DATE,C.ScheduleDate) < CONVERT(DATE,CI.NextLocal) THEN 'R' 
ELSE CI.DutyTYPE END DutyTYPE,CI.TripStatus,CI.CrewID TransCrew   
                               FROM #CrewTemp C LEFT OUTER JOIN @CrewInfo CI ON C.CrewID=CI.CrewID
                               WHERE CONVERT(DATE, C.ScheduleDate) >= CONVERT(DATE,CI.Depart)
                                 AND  CONVERT(DATE,C.ScheduleDate) <= CONVERT(DATE,ISNULL(CI.NextLocal,CI.Arrive))
                                
                                )TEMP ON T.CrewID=TEMP.CrewID AND T.ScheduleDate=TEMP.ScheduleDate
                              --  WHERE T.CrewID=1001351540
  
 ORDER BY T.ScheduleDate

 END
----------------------------------------------



IF @SuppressActivityCrew=1 AND @CrewDutyTypeCD <> ''
BEGIN
DELETE FROM @CrewCalendar WHERE CrewID NOT IN(SELECT CrewID FROM @CrewCalendar WHERE TransCrew IS NOT NULL 
																AND DutyTYPE  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewDutyTypeCD, ',')) )
                           
END


IF @SuppressActivityCrew=1 AND @CrewDutyTypeCD = ''
BEGIN
DELETE FROM @CrewCalendar WHERE CrewID NOT IN(SELECT CrewID FROM @CrewCalendar WHERE TransCrew IS NOT NULL)
                            AND TransCrew IS NULL
END

--DROP TABLE #TEMP

 IF @CrewDutyTypeCD <> ''
 BEGIN
UPDATE @CrewCalendar SET DutyType='' WHERE (DutyTYPE NOT IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewDutyTypeCD, ',')))
                              AND DutyType <> ''
                              

 END                      

DECLARE @CrewCalTemp TABLE(
                           ScheduleDate DATE,
                           CrewID BIGINT,
                           CrewCD VARCHAR(5),
                           CrewMember VARCHAR(100),
                           DutyType CHAR(2),
                           Rnk INT)




INSERT INTO @CrewCalTemp
SELECT  DISTINCT T.ScheduleDate,T.CrewID,T.CrewCD,CrewMember,DutyType,
RANK() OVER (PARTITION BY T.CrewID,T.ScheduleDate ORDER BY T.DutyType ASC) Rnk   FROM @CrewCalendar T
				  INNER JOIN #TempCrewID C ON C.CrewID=T.CrewID 
					LEFT OUTER JOIN Airport AA ON T.HomeBase=AA.AirportID
					LEFT OUTER JOIN Company C1 ON AA.AirportID=C1.HomebaseAirportID
				
					--WHERE (AA.AirportID IN(SELECT DISTINCT TOP 1 HomebaseAirportID FROM COMPANY WHERE HOMEBASEID IN (CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))))OR @IsHomeBase=0)
					WHERE (C1.HomebaseID IN((CONVERT(VARCHAR,dbo.GetHomeBaseByUserCD(LTRIM(@UserCD)))))OR @IsHomeBase=0)
                 


DELETE FROM @CrewCalTemp WHERE Rnk > 1
                         AND DutyType='AA'


UPDATE @CrewCalTemp SET DutyType='' WHERE DutyType='AA'

--DELETE FROM @CrewCalTemp WHERE  CrewMember IN(SELECT CrewMember FROM @CrewCalTemp Group by CrewMember,ScheduleDate HAVING Count(ScheduleDate)>1)
--                    AND ScheduleDate IN(SELECT ScheduleDate FROM @CrewCalTemp Group by CrewMember,ScheduleDate HAVING Count(ScheduleDate)>1)
--                    AND DutyType =''
  
 SELECT DISTINCT CrewCD,CrewMember FROM @CrewCalTemp  
WHERE ScheduleDate BETWEEN CONVERT(DATE,@date) AND CONVERT(DATE,@DATETO)
ORDER BY CrewCD

                    

END






GO


