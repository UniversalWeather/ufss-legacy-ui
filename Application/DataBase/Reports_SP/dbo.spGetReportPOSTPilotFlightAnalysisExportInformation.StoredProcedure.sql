IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPilotFlightAnalysisExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPilotFlightAnalysisExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure  [dbo].[spGetReportPOSTPilotFlightAnalysisExportInformation]
	(
	 @UserCD AS VARCHAR(30), 
	 @DATEFROM Datetime,
	 @DATETO Datetime,
	 @CrewCD NVARCHAR(1000)='',
	 @CrewGroupCD  NVARCHAR(1000)='',
	 @AircraftCD NVARCHAR(1000)=''
	  
	 )
 AS     
-- ===============================================================================      
-- SPC Name: spGetReportPOSTPilotFlightAnalysisExportInformation      
-- Author:  PREMPRAKASH.S      
-- Create date: 01 Aug 2012      
-- Description: Get Crew workLoad  information for REPORTS      
-- Revision History      
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================      
DECLARE @IsZeroSuppressActivityCrewRpt BIT;
SELECT @IsZeroSuppressActivityCrewRpt = IsZeroSuppressActivityCrewRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityCrewRpt=1
       
DECLARE @CustomerID BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
DECLARE @ShortDT DATETIME = dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO);  
--DROP TABLE #CREWINFO     
-----------------------------Crew and Crew Group Filteration----------------------
DECLARE @TempCrewID TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
CrewID BIGINT
)
IF @CrewCD <> ''
      BEGIN
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            WHERE c.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1  
            AND c.CrewCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewCD, ','))
      END
IF @CrewGroupCD <> ''
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT c.CrewID 
            FROM Crew c
            LEFT OUTER JOIN CrewGroupOrder cGO
                  ON c.CrewID = CGO.CrewID AND C.CustomerID = CGO.CustomerID
            LEFT OUTER JOIN CrewGroup CG 
                  ON CGO.CrewGroupID = CG.CrewGroupID AND CGO.CustomerID = CG.CustomerID
            WHERE CG.CrewGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@CrewGroupCD, ',')) 
                  AND C.CustomerID = @CustomerID AND C.IsDeleted=0 --AND C.IsStatus=1 
      END
ELSE IF @CrewCD = '' AND @CrewGroupCD = ''
      BEGIN 
            INSERT INTO @TempCrewID
            SELECT DISTINCT C.CrewID---F.CustomerID, F.TailNUM, F.AircraftCD, F.HomebaseID
            FROM Crew C 
            WHERE C.CustomerID = @customerID AND C.IsDeleted=0 AND C.IsStatus=1 
      END
-----------------------------TailNum and Fleet Group Filteration---------------------- 

DECLARE @CREW TABLE (CREWID BIGINT)
      INSERT INTO @CREW SELECT CrewID FROM Crew WHERE IsDeleted = 0 AND isStatus=1
 
DECLARE @CrewTemp TABLE (CrewID bigint,
                                    CrewCD varchar(10)
                                    ,LastName varchar(30));
INSERT INTO @CrewTemp (CrewID, CrewCD, LastName) 
SELECT DISTINCT C.CrewID, C.CrewCD, C.LastName
FROM Crew C
WHERE C.CrewID IN (SELECT CrewID FROM @TempCrewID)
 
 --Temp Fleet
 
DECLARE @TempFleet TABLE 
( 
ID INT NOT NULL IDENTITY (1,1), 
AircraftID BIGINT,
FleetID BIGINT,
TailNum NVARCHAR(10)
--AircraftCD NVARCHAR(10)
);
IF @AircraftCD <> ''
      BEGIN
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @CustomerID
            AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
            (SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@CustomerID AND
                                                                  AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))))
      END
IF @AircraftCD = ''
      BEGIN 
            INSERT INTO @TempFleet
            SELECT F.AircraftID, F.FleetID ,F.TAILNUM
            FROM Fleet F
            WHERE F.CustomerID = @CustomerID
      --    AND F.FleetID IN (SELECT FleetID FROM Fleet F WHERE F.AircraftID IN 
      --    (SELECT AC.AircraftID from Aircraft AC WHERE AC.CustomerID=@CustomerID 
                                                                  --AND AC.AircraftCD IN (SELECT RTRIM(S) FROM dbo.SplitString(@AircraftCD, ','))
                                                                  --))  
      END
 
--SELECT * FROM @TempFleet
 
 
--End Temp Fleet
 
SELECT DISTINCT
	POC.CREWID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+  dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[log_num] = '0'
	,[leg_num] = '0'
	,[depicao_id] = ''
	,[arricao_id] = ''
	,[schedttm] = ''
	,[crewcode] = C.CrewCD 
	,[tot_line] = 'S'
	,[dutytype] = ''
	,[dutyend_a] = ''
	,[dutyend_b] = '0'--POL.IsDutyEnd
	,[dutyendflag] = '0'
	,[last_name] = C.LastName
	,[dutyhrs] = SUM(CASE WHEN POL.IsDutyEnd=1 THEN ISNULL(ROUND(POC.DutyHours,1),0) ELSE 0 END) over (Partition by POC.CREWID )
	,[blk_hrs] = SUM(ISNULL(ROUND(POC.BlockHours,1),0)) over (Partition by POC.CREWID )
	,[flt_hrs] = SUM(ISNULL(ROUND(POC.FlightHours,1),0)) over (Partition by POC.CREWID )
	,[pic_hours] = SUM(CASE WHEN POC.DutyTYPE='P' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID )
	,[sic_hours] = SUM(CASE WHEN POC.DutyTYPE='S' THEN ISNULL(ROUND(POC.BlockHours,1),0) ELSE 0 END) over (Partition by POC.CREWID )
	,[night] = SUM(ISNULL(POC.Night,0)) over (partition by POC.CREWID)
	,[instr] = SUM(ISNULL(POC.Instrument,0)) over (partition by POC.CREWID)
	,[tkoff_d] = SUM(ISNULL(POC.TakeOffDay,0)) over (partition by POC.CREWID)
	,[tkoff_n] = SUM(ISNULL(POC.TakeOffNight,0)) over (partition by POC.CREWID)
	,[lnding_d] = SUM(ISNULL(POC.LandingDay,0)) over (partition by POC.CREWID)
	,[lnding_n] = SUM(ISNULL(POC.LandingNight,0)) over (partition by POC.CREWID)
	,[app_p] = SUM(ISNULL(POC.ApproachPrecision,0)) over (partition by POC.CREWID)
	,[app_n] = SUM(ISNULL(POC.ApproachNonPrecision,0)) over (partition by POC.CREWID)
	
INTO #CREWINFO
FROM PostflightCrew POC
      INNER JOIN Crew C ON C.CrewID = POC.CrewID
      INNER JOIN PostflightLeg POL ON POC.POLegID = POL.POLegID AND POL.IsDeleted=0
      INNER JOIN PostflightMain POM ON POC.POLogID = POM.POLogID AND POM.IsDeleted=0
WHERE C.IsDeleted=0 
      --AND C.IsStatus=1 
      AND C.CustomerID = @CustomerID
      AND CONVERT(DATE,pol.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO) 
      AND pom.FleetID IN (SELECT FleetID from @TempFleet) AND POC.CUSTOMERID=@CustomerID
       --SELECT * FROM #cREWINFO

IF @IsZeroSuppressActivityCrewRpt=0
BEGIN 
SELECT DISTINCT
	CT.CREWID 
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+  dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[log_num] = '0'
	,[leg_num] = '0'
	,[depicao_id] = ''
	,[arricao_id] = ''
	,[schedttm] = ''
	,[crewcode] = CT.CrewCD 
	,[tot_line] = 'S'
	,[dutytype] = ''
	,[dutyend_a] = ''
	,[dutyend_b] = ISNULL(CI.[dutyend_b],0)
	,[dutyendflag] = ISNULL(CI.[dutyendflag],0)
	,[last_name] = CT.LastName
	,[dutyhrs] = ISNULL(CI.[dutyhrs],0)
	,[blk_hrs] = ISNULL(CI.[blk_hrs],0)
	,[flt_hrs] = ISNULL(CI.[flt_hrs],0)
	,[pic_hours] = ISNULL(CI.[pic_hours],0)
	,[sic_hours] = ISNULL(CI.[sic_hours],0)
	,[night] = ISNULL(CI.[night],0)
	,[instr] = ISNULL(CI.[instr],0)
	,[tkoff_d] = ISNULL(CI.[tkoff_d],0)
	,[tkoff_n] = ISNULL(CI.[tkoff_n],0)
	,[lnding_d] = ISNULL(CI.[lnding_d],0)
	,[lnding_n] = ISNULL(CI.[lnding_n],0)
	,[app_p] = ISNULL(CI.[app_p],0)
	,[app_n] = ISNULL(CI.[app_n],0)
FROM @CrewTemp CT 
	LEFT OUTER JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID
ORDER BY CrewCD
END
ELSE 
BEGIN
	SELECT DISTINCT
	CT.CREWID
	,[DateRange] = dbo.GetShortDateFormatByUserCD(@UserCD,@DATEFROM) +' - '+  dbo.GetShortDateFormatByUserCD(@UserCD,@DATETO)
	,[log_num] = '0'
	,[leg_num] = '0'
	,[depicao_id] = ''
	,[arricao_id] = ''
	,[schedttm] = ''
	,[crewcode] = CT.CrewCD 
	,[tot_line] = 'S'
	,[dutytype] = ''
	,[dutyend_a] = ''
	,[dutyend_b] = ISNULL(CI.[dutyend_b],0)
	,[dutyendflag] = ISNULL(CI.[dutyendflag],0)
	,[last_name] = CT.LastName
	,[dutyhrs] = ISNULL(CI.[dutyhrs],0)
	,[blk_hrs] = ISNULL(CI.[blk_hrs],0)
	,[flt_hrs] = ISNULL(CI.[flt_hrs],0)
	,[pic_hours] = ISNULL(CI.[pic_hours],0)
	,[sic_hours] = ISNULL(CI.[sic_hours],0)
	,[night] = ISNULL(CI.[night],0)
	,[instr] = ISNULL(CI.[instr],0)
	,[tkoff_d] = ISNULL(CI.[tkoff_d],0)
	,[tkoff_n] = ISNULL(CI.[tkoff_n],0)
	,[lnding_d] = ISNULL(CI.[lnding_d],0)
	,[lnding_n] = ISNULL(CI.[lnding_n],0)
	,[app_p] = ISNULL(CI.[app_p],0)
	,[app_n] = ISNULL(CI.[app_n],0)
FROM @CrewTemp CT 
	INNER JOIN #CREWINFO CI ON CT.CrewID=CI.CREWID
ORDER BY CrewCD
END

IF OBJECT_ID('tempdb..#CREWINFO') IS NOT NULL
DROP TABLE #CREWINFO	

--EXEC spGetReportPOSTPilotFlightAnalysisExportInformation 'SUPERVISOR_99','12/1/2009','1/1/2010','','',''


GO


