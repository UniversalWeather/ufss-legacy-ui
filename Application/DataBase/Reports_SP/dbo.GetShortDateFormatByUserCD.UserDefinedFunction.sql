IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetShortDateFormatByUserCD]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetShortDateFormatByUserCD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
CREATE FUNCTION [dbo].[GetShortDateFormatByUserCD] (@UserCD VARCHAR(30), @ReportDate DateTime)
RETURNS VARCHAR(10)
WITH EXECUTE AS CALLER
AS
BEGIN
    DECLARE @DateFormat VARCHAR(15)
	DECLARE @ApplicationDateFormat VARCHAR(25)
	SELECT @ApplicationDateFormat=Upper(ApplicationDateFormat) FROM Company 
	       WHERE HomeBaseID =  dbo.GetHomeBaseByUserCD(RTRIM(@UserCD))
	       AND CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD))
	
	  
	IF @ApplicationDateFormat='AMERICAN'
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,   1)  
	END
	ELSE IF @ApplicationDateFormat='ANSI'
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  2)
	END
	ELSE IF @ApplicationDateFormat='BRITISH'  
	BEGIN
	  SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  3)
	END
	ELSE IF @ApplicationDateFormat='FRENCH'
	BEGIN
	  SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  3)
	END
    ELSE IF @ApplicationDateFormat='GERMAN'
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  4) 
	END   
	ELSE IF @ApplicationDateFormat='ITALIAN'
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  5)
	END   
	ELSE IF @ApplicationDateFormat='JAPAN'
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate, 11)
	END  
	ELSE IF @ApplicationDateFormat='TAIWAN' 
	BEGIN
	   SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate, 11)
	END
	ELSE IF @ApplicationDateFormat='DMY'
	BEGIN
	  SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate,  3)
	END
	ELSE IF @ApplicationDateFormat='MDY'
	BEGIN
	  SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate, 1)
	END
	ELSE IF @ApplicationDateFormat='YMD'
	BEGIN
	  SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate, 11)
	END 
	ELSE 
	BEGIN
	    SET @DateFormat=CONVERT(VARCHAR(10), @ReportDate, 1)
	END     
		
    RETURN @DateFormat
END; 

 
GO


