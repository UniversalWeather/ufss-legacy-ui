IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCrewDefinitionInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCrewDefinitionInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetReportCrewDefinitionInformation]
	@UserCD varchar(30),
	@CrewCD char(7)
AS
-- =============================================
-- SPC Name: spGetReportCrewDefinitionInformation
-- Author: SUDHAKAR J
-- Create date: 14 Jun 2012
-- Description: Get Crew Definition Information for Report, Crew Roaster
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================

SET NOCOUNT ON

	SELECT
		CD.CustomerID
		,C.CrewCD
		,[InformationCD] = CI.CrewInfoCD
		,[InformationDESC] = CI.CrewInformationDescription
		--,dbo.FlightPakDecrypt(ISNULL(CD.InformationValue,'')) AS InformationValue
		,ISNULL(CD.InformationValue,'') AS InformationValue
	FROM CrewDefinition CD
	LEFT OUTER JOIN (
			SELECT CustomerID, CrewInfoID, CrewInfoCD, CrewInformationDescription 
			FROM CrewInformation 
		) CI ON CD.CrewInfoID = CI.CrewInfoID AND CD.CustomerID = CI.CustomerID
	LEFT OUTER JOIN (
		SELECT CustomerID, CrewID, CrewCD FROM Crew
		) C ON CD.CustomerID = C.CustomerID AND CD.CrewID = C.CrewID
	WHERE CD.IsDeleted = 0
	AND CD.CustomerID = dbo.GetCustomerIDbyUserCD(RTRIM(@UserCD)) 
	AND C.CrewCD = RTRIM(@CrewCD)
	AND ISNULL(CD.InformationValue,'') <> ''
	AND CD.isReptFilter = 1
	
  -- EXEC spGetReportCrewDefinitionInformation 'UC', '123'
  -- EXEC spGetReportCrewDefinitionInformation 'TIM', 'AJR'


GO


