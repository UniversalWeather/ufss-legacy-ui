IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportCQQuoteSub1Information]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportCQQuoteSub1Information]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportCQQuoteSub1Information] @CQFileID VARCHAR(30)
	,@QuoteNUM INT
AS
-- =============================================
-- SPC Name:spGetReportCQQuoteSubInformation
-- Author: Aishwarya.M
-- Create date: 06 Mar 2013
-- Description: Get Charter Quote Information for REPORTS
-- Revision History
-- Date		Name		Ver		Change
-- 
-- =============================================
BEGIN




SELECT DISTINCT QUOTE.*  FROM (
SELECT DISTINCT 7 Row_Id,CASE 
				WHEN CM.FlightChgDescription IS NOT NULL
					AND CM.FlightChgDescription<> ''
					THEN CASE 
							WHEN CM.IsPrintFlightChg = 1 /* AND CL.IsPositioning = 1*/
								THEN CM.FlightChgDescription+ ' (' + ISNULL(CONVERT(VARCHAR(30),CMM.FlightHours ),'')+ ' HRS' + ')'
							ELSE NULL
							END
				ELSE 'uncheck'
				END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintFlightChg = 1
				THEN CM.CustomFlightChg
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote TotalQuote
	FROM CQFile CQF
	    INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	    INNER JOIN (SELECT CQF.CQFileID,CM.QuoteNUM,SUM(CL.FlightHours) FlightHours FROM CQFile CQF
							   INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
			                   INNER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID
			                   INNER JOIN CQQuoteDetail CL ON CQM.CQQuoteMainID = CL.CQQuoteMainID	 
			             WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)
	                       AND CM.QuoteNUM = @QuoteNUM	
	                      GROUP BY CQF.CQFileID,CM.QuoteNUM
                  )CMM ON CMM.CQFileID=CQF.CQFileID			
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
UNION ALL


SELECT DISTINCT 4,
               CASE WHEN CM.IsPrintUsageAdj = 1 THEN  CM.UsageAdjDescription  ELSE 'uncheck' END
/*,CASE 
				WHEN CM.UsageAdjDescription IS NOT NULL
					AND CM.UsageAdjDescription <> ''
					THEN CASE 
							WHEN CM.IsPrintUsageAdj = 1
								THEN CM.UsageAdjDescription + ' (' + CONVERT(VARCHAR(30), CASE 
											WHEN CM.IsPrintUsageAdj = 1
												THEN CM.CustomUsageAdj
											ELSE 0
											END) + ' HRS' + ')'
							ELSE NULL
							END
				ELSE NULL
				END InfoDesc*/
		 ,CASE 
			WHEN CM.IsPrintUsageAdj = 1
				THEN CM.CustomUsageAdj
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0				
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM

UNION ALL

SELECT DISTINCT 5,CASE 
			WHEN CM.IsPrintAdditonalFees = 1
				THEN CM.AdditionalFeeDescription
			ELSE 'uncheck'
			END  InfoDesc
		 ,CASE 
			WHEN CM.IsPrintAdditonalFees = 1
				THEN CM.CustomAdditionalFees 
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	AND CM.IsDeleted=0				
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 5,CASE 
			WHEN CM.IsPrintLandingFees = 1
				THEN CM.LandingFeeDescription
			ELSE 'uncheck'
			END  InfoDesc
		 ,CASE 
			WHEN CM.IsPrintLandingFees = 1
				THEN CM.CustomLandingFee 
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	AND CM.IsDeleted=0				
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM		
				
--UNION ALL


--SELECT DISTINCT @CQFileID,CASE WHEN CM.StdCrewROMDescription IS NOT NULL AND CM.StdCrewROMDescription <>'' THEN CASE 
--				WHEN CM.IsPrintStdCrew  = 1
--					THEN --CM.StdCrewROMDescription 
--						CM.StdCrewROMDescription+ ' (' + CONVERT(VARCHAR(30), CASE 
--								WHEN CM.IsPrintStdCrew = 1
--									THEN CM.CustomStdCrewRON
--								ELSE 0
--								END) + ' RON' + ')'
--				ELSE NULL
--				END  ELSE NULL END  InfoDesc
--		 ,CASE 
--			WHEN CM.IsPrintStdCrew = 1
--				THEN CM.CustomStdCrewRON
--			ELSE 0
--			END InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID					
--	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--		AND CM.QuoteNUM = @QuoteNUM
--UNION ALL

--SELECT DISTINCT @CQFileID,CASE 
--			WHEN CM.IsPrintAdditionalCrew = 1
--				THEN CM.AdditionalCrewDescription
--			ELSE ''
--			END InfoDesc
--		 ,CASE 
--			WHEN CM.IsPrintAdditionalCrew = 1
--				THEN CM.CustomAdditionalCrew 
--			ELSE 0
--			END  InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID
--    WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--	  AND CM.QuoteNUM = @QuoteNUM
--UNION ALL

--SELECT DISTINCT @CQFileID,CASE 
--			WHEN CM.IsAdditionalCrewRON = 1
--				THEN CM.AdditionalDescriptionCrewRON
--			ELSE ''
--			END InfoDesc
--		 ,CASE 
--			WHEN CM.IsAdditionalCrewRON = 1
--				THEN CM.CustomAdditionalCrewRON 
--			ELSE 0
--			END InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID					
--	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--		AND CM.QuoteNUM = @QuoteNUM

--UNION ALL

--SELECT DISTINCT @CQFileID,CASE 
--			WHEN CM.IsPrintWaitingChg = 1
--				THEN CM.WaitChgDescription
--			ELSE ''
--			END InfoDesc
--		 ,CASE 
--			WHEN CM.IsPrintWaitingChg = 1
--				THEN CM.CustomWaitingChg 
--			ELSE 0
--			END InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID					
--	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--		AND CM.QuoteNUM = @QuoteNUM
--UNION ALL

--SELECT DISTINCT @CQFileID,CASE 
--			WHEN CM.IsPrintLandingFees = 1
--				THEN CM.LandingFeeDescription
--			ELSE ''
--			END InfoDesc
--		 ,CASE 
--			WHEN CM.IsPrintLandingFees = 1
--				THEN CM.CustomLandingFee 
--			ELSE 0
--			END InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID					
--	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--		AND CM.QuoteNUM = @QuoteNUM
UNION ALL

SELECT DISTINCT 6,CASE 
			WHEN CM.IsPrintSegmentFee = 1
				THEN CM.SegmentFeeDescription
			ELSE 'uncheck'
			END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintSegmentFee = 1
				THEN CM.CustomSegmentFee 
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	AND CM.IsDeleted=0			
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
		

UNION ALL

SELECT DISTINCT  8,CASE 
			WHEN CM.IsPrintSubtotal = 1
				THEN CM.SubtotalDescription
			ELSE 'uncheck'
			END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintSubtotal = 1
				THEN CM.CustomSubTotal
			ELSE 0
			END  InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	AND CM.IsDeleted=0			
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM

UNION ALL

SELECT DISTINCT 9
--,CASE 
--			WHEN CM.IsPrintDiscountAmt = 1
--				THEN CM.DescriptionDiscountAmt
--			ELSE ''
--			END  InfoDesc
		  ,CASE 
				WHEN  ISNULL(CM.DescriptionDiscountAmt,'') <> ''
					THEN CASE 
							WHEN CM.IsPrintDiscountAmt = 1
								THEN CM.DescriptionDiscountAmt + ' (' + ISNULL(CONVERT(VARCHAR(30),CQF.DiscountPercentage),'') + ' % ' + ')'
							ELSE 'uncheck'
							END
				ELSE NULL END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintDiscountAmt = 1
				THEN CM.CustomDiscountAmt
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0					
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 10,CASE 
			WHEN CM.IsPrintTaxes = 1
				THEN CM.TaxesDescription
			ELSE 'uncheck'
			END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintTaxes = 1
				THEN CM.CustomTaxes 
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0					
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 11,CASE 
			WHEN CM.IsPrintTotalQuote= 1
				THEN CM.TotalQuoteDescription
			ELSE 'uncheck'
			END InfoDesc
		 ,CASE 
			WHEN CM.IsPrintTotalQuote = 1
				THEN CM.CustomTotalQuote
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID	 AND CM.IsDeleted=0				
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM

--SELECT DISTINCT CASE 
--			WHEN CM.IsPrintTotalQuote= 1
--				THEN CM.InvoiceDescription
--			ELSE ''
--			END InfoDesc
--		 ,CASE 
--			WHEN CM.IsPrintInvoice = 1
--				THEN CM.CustomTotalQuote
--			ELSE 0
--			END InfoDescvalue
--	FROM CQFile CQF
--	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID					
--	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
--		AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT  12,CASE 
			WHEN (
					CM.IsPrintPay = 1
					--AND CM.PrepayTotal <> 0
					)
				THEN CM.PrepayDescription
			ELSE 'uncheck'
			END InfoDesc
		 ,CASE 
			WHEN (
					CM.IsPrintPay = 1
					--AND CM.PrepayTotal <> 0
					)
				THEN CM.CustomPrepay
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0					
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM
		
UNION ALL

SELECT DISTINCT 13,CASE 
			WHEN (CM.IsPrintRemaingAMT = 1
					)
				THEN CM.RemainingAmtDescription
			ELSE 'uncheck'
			END  InfoDesc
		 ,CASE 
			WHEN ( CM.IsPrintRemaingAMT = 1
					)
				THEN CM.CustomRemainingAmt 
			ELSE 0
			END InfoDescvalue,CM.CustomTotalQuote 
	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0					
	WHERE CQF.CQFileID = CONVERT(BIGINT, @CQFileID)
		AND CM.QuoteNUM = @QuoteNUM

UNION ALL

SELECT DISTINCT
        1 Row_Id,CASE WHEN CQA.IsPrintable=1 THEN CQA.CQQuoteFeeDescription ELSE 'uncheck' END InfoDesc 
		,CASE WHEN CQA.IsPrintable=1  THEN ISNULL(CQA.QuoteAmount,0) ELSE NULL END InfoDescvalue
		,CM.CustomTotalQuote TotalQuote

	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID
	LEFT OUTER JOIN CQQuoteAdditionalFees CQA ON CQA.CQQuoteMainID=CQM.CQQuoteMainID						
	WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)
	AND CM.QuoteNUM = @QuoteNUM
	AND CQA.OrderNUM =1
	AND CQA.IsPrintable=1

UNION ALL

SELECT DISTINCT
        2 Row_Id,CASE WHEN CQA.IsPrintable=1 THEN CQA.CQQuoteFeeDescription ELSE 'uncheck' END InfoDesc 
		,CASE WHEN CQA.IsPrintable=1  THEN ISNULL(CQA.QuoteAmount,0) ELSE NULL END InfoDescvalue
		,CM.CustomTotalQuote TotalQuote

	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID
	LEFT OUTER JOIN CQQuoteAdditionalFees CQA ON CQA.CQQuoteMainID=CQM.CQQuoteMainID						
	WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)
	AND CM.QuoteNUM = @QuoteNUM
	AND CQA.OrderNUM =2
	AND CQA.IsPrintable=1
	
UNION ALL

SELECT DISTINCT
        3 Row_Id
       ,CASE 
				WHEN  ISNULL(CQA.CQQuoteFeeDescription,'') <> ''
					THEN CASE 
							WHEN CQA.IsPrintable=1 
								THEN CQA.CQQuoteFeeDescription + ' (' + ISNULL(CONVERT(VARCHAR(30), CASE 
											WHEN CQA.IsPrintable=1 
												THEN CQA.Quantity
											ELSE 0
											END),'') + ' HRS' + ')'
							ELSE 'uncheck'
							END
				ELSE NULL END InfoDesc
        
        --,CASE WHEN CQA.IsPrintable=1 THEN CQA.CQQuoteFeeDescription ELSE '' END InfoDesc 
		,CASE WHEN CQA.IsPrintable=1  THEN ISNULL(CQA.QuoteAmount,0) ELSE NULL END InfoDescvalue
		,CM.CustomTotalQuote TotalQuote
	


	FROM CQFile CQF
	INNER JOIN CQMain CM ON CQF.CQFileID = CM.CQFileID AND CM.IsDeleted=0
	LEFT OUTER JOIN CQQuoteMain CQM ON CM.CQMainID = CQM.CQMainID
	LEFT OUTER JOIN CQQuoteAdditionalFees CQA ON CQA.CQQuoteMainID=CQM.CQQuoteMainID						
	WHERE CQF.CQFileID = CONVERT(BIGINT,@CQFileID)
	AND CM.QuoteNUM = @QuoteNUM
	AND CQA.OrderNUM =3
	AND CQA.IsPrintable=1
)QUOTE
WHERE InfoDesc <> 'uncheck'
  
 ORDER BY QUOTE.Row_Id
 

	--AND CQA.IsPrintable=1

END
	--EXEC spGetReportCQQuoteSub1Information 10099472,1		





GO


