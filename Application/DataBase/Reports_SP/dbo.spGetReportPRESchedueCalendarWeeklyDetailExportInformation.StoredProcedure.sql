IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPRESchedueCalendarWeeklyDetailExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyDetailExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetReportPRESchedueCalendarWeeklyDetailExportInformation]  
		@UserCD VARCHAR(50)--='BRUCE_98'--Mandatory
       ,@UserCustomerID  VARCHAR(30)
       ,@BeginDate DATETIME--='2013-01-01' --Mandatory
       ,@NoOfWeeks INT=1
       ,@EndDate DATETIME---='2013-01-01'--Mandatory
       ,@ClientCD VARCHAR(5)=''
       ,@RequestorCD VARCHAR(5)=''
       ,@DepartmentCD VARCHAR(8)=''
       ,@FlightCatagoryCD VARCHAR(25)=''
       ,@DutyTypeCD VARCHAR(2)=''
       ,@HomeBaseCD VARCHAR(25)=''	
       ,@IsTrip	BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'T'
	   ,@IsCanceled BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'X'
	   ,@IsHold BIT = 1 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'H'
	   ,@IsWorkSheet BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'W'
   	   ,@IsUnFulFilled BIT = 0 --PreflightMain.TripStatus, Retrieve all the trips where TripStatus = 'U'
   	   ,@IsAllCrew BIT=1
   	   ,@HomeBase BIT=0
	   ,@FixedWingCrew BIT=0
	   ,@RotaryWingCrew BIT=0
	   ,@TimeBase VARCHAR(5)='Local'   ---Local,UTC,Home
	   ,@Vendors INT=1  ---1-Active and Inactive,2-Active only,3-Inactive only
	   ---Display Options---
	  ,@IsAirport INT=1 ---1-ICAO,2-City,3-Airport Name
	  ,@IsActivityOnly BIT=1
	  ,@IsFirstLastLeg BIT=0
	  ,@IsCrew	BIT=0
	  ,@IsShowTripStatus BIT=0
	  ,@IsFlightNo BIT=0
	  ,@IsLegPurpose BIT=1
	  ,@IsSeatAvailable BIT=1
	  ,@IsPaxCount BIT=1
	  ,@IsPaxFullName BIT=0
	  ,@IsShowTrip BIT=1
	  ,@IsRequestor BIT=1
	  ,@IsDepartment BIT=1
	  ,@IsAuthorization  BIT=1
	  ,@IsFlightCategory BIT=1
	  ,@IsCummulativeETE BIT=1
	  ,@IsCrewFullName BIT=0
	  ,@IsPax BIT=1
	  ,@Footer BIT ---0 Skip Footer,1-Print Footer
	  ,@SortBy INT = 1 --1-TailNum,2-Dep Date/Time
	  ,@BlackWhiteClr BIT=0
	  ,@AircraftClr BIT=0
	  ,@EFleet VARCHAR(MAX)=''
AS  
BEGIN  
-- ===============================================================================  
-- SPC Name: spGetReportPRESchedueCalendarWeeklyDetailExportInformation  
-- Author: Askar 
-- Create date: 23 Jul 2013  
-- Description: Get Preflight Schedule Calendar Day information for REPORTS  
-- Revision History  
-- Date   Name  Ver  Change  
--   
-- ================================================================================  
  
SET NOCOUNT ON     

DECLARE @DateFrom DATETIME=@BeginDate,@DateTo DATETIME=@EndDate

DECLARE @VendorVal BIT=(CASE WHEN @Vendors=1 THEN 0 
                            WHEN @Vendors=2 THEN 1 
                            WHEN @Vendors=3 THEN 0
                            END)
		
		DECLARE @TripStatus AS VARCHAR(20) = '';
		
		IF @IsWorkSheet = 1
		SET @TripStatus = 'W,'
		
		IF  @IsTrip = 1
		SET @TripStatus = @TripStatus + 'T,'

		IF  @IsUnFulFilled = 1
		SET @TripStatus = @TripStatus + 'U,'
		
		IF  @IsCanceled = 1
		SET @TripStatus = @TripStatus + 'X,'
		
		IF  @IsHold = 1
		SET @TripStatus = @TripStatus + 'H'
		


IF @HomeBase=1
BEGIN

SELECT DISTINCT @HomeBaseCD=A.IcaoID FROM Company C INNER JOIN UserMaster UM ON C.HomebaseID=UM.HomebaseID
                               INNER JOIN Airport A ON A.AirportID=C.HomebaseAirportID
                WHERE C.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
                 AND UM.UserName = RTRIM(@UserCD)


END




-----Rest Record and Maintenace Reco
DECLARE @CurDate TABLE(DateWeek DATE,FleetID BIGINT,TailNum VARCHAR(9))

 
 DECLARE @TableTrip TABLE(TripNUM BIGINT,
                          CrewList VARCHAR(4000),
                          DepartureDTTMLocal DATETIME,
                          ArrivalDTTMLocal DATETIME,
                          FleetID BIGINT,
                          TailNum VARCHAR(9),
                          TypeCode VARCHAR(10),
                          DutyTYPE CHAR(2),
                          NextLocalDTTM DATETIME,
                          RecordTye CHAR(1),
                          DepICAOID VARCHAR(25),
                          ArrivelICAOID VARCHAR(25),
                          LegNum BIGINT,
                          TripID BIGINT,
                          LegID BIGINT,
                          Passenger VARCHAR(4000),
                          TripStatus CHAR(1),
                          LastUpdated DATETIME,
                          FlightNUM VARCHAR(12),
                          Requestor VARCHAR(5),
                          LegPurpose VARCHAR(40),
                          FlightCategory CHAR(4),
                          DepartmentCD VARCHAR(8),
                          AuthorizationCD VARCHAR(8),
                          FuelLoad NUMERIC(6,0),
                          OutbountInstruction VARCHAR(MAX),
                          SeatsAvailable INT,
                          PaxTotal INT,
                          ETE NUMERIC(7,3),
                          FgColor VARCHAR(8),
                          BgColr  VARCHAR(8)
                          )
 




 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=CASE WHEN @IsCrewFullName=1 THEN Crew.CrewList  ELSE SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList )-1) END
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,CASE WHEN @IsPaxFullName=1 THEN PAX.PaxNames ELSE SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) END
     ,PM.TripStatus
     ,PL.LastUpdTS
     ,PL.FlightNUM
     ,P.PassengerRequestorCD 
	 ,PL.FlightPurpose
	 ,FC.FlightCatagoryCD 
	 ,DEP.DepartmentCD 
	 ,DA.AuthorizationCD 
	 ,PL.FuelLoad
	 ,PL.OutbountInstruction 
	 ,PL.ReservationAvailable
	 ,PL.PassengerTotal 
	 ,PL.ElapseTM
	 ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
     ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT CASE WHEN @IsCrewFullName=1 THEN C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***' ELSE C.CrewCD+', ' END FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
	  		 LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT CASE WHEN @IsPaxFullName=1 THEN  ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END+ISNULL(P1.MiddleInitial,'')+' '+'###' ELSE PassengerRequestorCD+', ' END
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID   
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
       AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
      AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))


 INSERT INTO @TableTrip
 SELECT DISTINCT PM.TripNUM
     ,CrewCD=CASE WHEN @IsCrewFullName=1 THEN Crew.CrewList  ELSE SUBSTRING(Crew.CrewList,1,LEN(Crew.CrewList )-1) END
     ,CASE WHEN @TimeBase='Local' THEN PL.DepartureDTTMLocal WHEN @TimeBase='UTC'   THEN PL.DepartureGreenwichDTTM ELSE PL.HomeDepartureDTTM END DepartureDTTMLocal
     ,CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END ArrivalDTTMLocal
     ,PM.FleetID
     ,F.TailNum
     ,AFT.AircraftCD
     ,PL.DutyTYPE
     ,CASE WHEN CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END))=CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) THEN NULL ELSE CASE WHEN @TimeBase='Local' THEN (CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END) WHEN @TimeBase='UTC' THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END END NextLocalDTTM
     ,PM.RecordType
     ,CASE WHEN @IsAirport=1 THEN D.IcaoID 
           WHEN @IsAirport=2 THEN D.CityName ELSE D.AirportName END
     ,CASE WHEN @IsAirport=1 THEN A.IcaoID 
           WHEN @IsAirport=2 THEN A.CityName ELSE A.AirportName END
     ,PL.LegNUM
     ,PM.TripID
     ,PL.LegID
     ,CASE WHEN @IsPaxFullName=1 THEN PAX.PaxNames ELSE SUBSTRING(PAX.PaxNames,1,LEN(PAX.PaxNames)-1) END
     ,PM.TripStatus
     ,PL.LastUpdTS
     ,PL.FlightNUM
     ,P.PassengerRequestorCD 
	 ,PL.FlightPurpose
	 ,FC.FlightCatagoryCD 
	 ,DEP.DepartmentCD 
	 ,DA.AuthorizationCD 
	 ,PL.FuelLoad
	 ,PL.OutbountInstruction 
	 ,PL.ReservationAvailable
	 ,PL.PassengerTotal
	 ,PL.ElapseTM 
	  ,fcolor=CASE WHEN @BlackWhiteClr=1 THEN '#000000'  
                   WHEN @AircraftClr=1 THEN F.ForeGrndCustomColor
                   WHEN PM.RecordType='T' THEN FC.ForeGrndCustomColor  ELSE NULL END
      ,bcolor=CASE WHEN @BlackWhiteClr=1 THEN '#FFFFFF'
				   WHEN @AircraftClr=1 THEN F.BackgroundCustomColor
                   WHEN PM.RecordType='T' THEN FC.BackgroundCustomColor  ELSE NULL END
        FROM PreflightMain PM 
              INNER JOIN PreflightLeg PL ON PM.TripID=PL.TripID AND PL.IsDeleted = 0
              INNER JOIN (SELECT * FROM Fleet WHERE IsDeleted=0 AND IsInActive=0) F ON PM.FleetID=F.FleetID
              LEFT OUTER JOIN Company CO ON CO.HomebaseID=PM.HomebaseID
              LEFT OUTER JOIN Airport AT ON CO.HomebaseAirportID=AT.AirportID
              LEFT OUTER JOIN Airport D ON D.AirportID=PL.DepartICAOID
              LEFT OUTER JOIN Airport A ON A.AirportID=PL.ArriveICAOID
              LEFT OUTER JOIN Passenger P ON PL.PassengerRequestorID=P.PassengerRequestorID
              LEFT OUTER JOIN PreflightCrewList PCL ON PL.LegID=PCL.LegID
              LEFT OUTER JOIN (SELECT * FROM Crew WHERE IsDeleted=0 AND IsStatus=1 AND ((IsFixedWing =@FixedWingCrew) OR @FixedWingCrew = 0) AND ((IsRotaryWing =@RotaryWingCrew) OR @RotaryWingCrew = 0))  CW ON CW.CrewID=PCL.CrewID
              LEFT OUTER JOIN Department DEP ON DEP.DepartmentID=PL.DepartmentID
              LEFT OUTER JOIN DepartmentAuthorization DA ON DA.AuthorizationID=PL.AuthorizationID
              LEFT OUTER JOIN Aircraft AFT ON AFT.AircraftID=F.AircraftID
              LEFT OUTER JOIN (SELECT DISTINCT PC2.LegID,CrewList = (SELECT CASE WHEN @IsCrewFullName=1 THEN C.LastName+CASE WHEN C.FirstName <> '' THEN ', '+C.FirstName ELSE '' END +' '+C.MiddleInitial + '***' ELSE C.CrewCD+', ' END FROM PreflightCrewList PC1        
																			 INNER JOIN Crew C ON PC1.CrewID = C.CrewID        
																			 WHERE PC1.LegID = PC2.LegID   
																			  ORDER BY(CASE PC1.DutyTYPE WHEN 'P' THEN 1 WHEN 'S' THEN 2 WHEN 'E' THEN 3 WHEN 'I' THEN 4 
					                                                																		WHEN 'A' THEN 5 WHEN 'O' THEN 6 ELSE 7 END)       
																			 FOR XML PATH('') 
																		 ) FROM PreflightCrewList PC2         
	                              ) Crew ON PL.LegID = Crew.LegID 
	         LEFT OUTER JOIN Client C ON C.ClientID=PM.ClientID
	         LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID=PL.FlightCategoryID
	         LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
            LEFT OUTER JOIN ( SELECT DISTINCT PP2.LegID, PaxNames = (
                       SELECT CASE WHEN @IsPaxFullName=1 THEN ISNULL(P1.LastName,'') +CASE WHEN ISNULL(P1.FirstName,'')<>'' THEN  ', ' +P1.FirstName+' '  ELSE ' ' END +ISNULL(P1.MiddleInitial,'') +' '+'###' ELSE PassengerRequestorCD+', ' END
                               FROM PreflightPassengerList PP1 INNER JOIN Passenger P1 ON PP1.PassengerID=P1.PassengerRequestorID 
                               WHERE PP1.LegID = PP2.LegID
                               ORDER BY OrderNUM
                             FOR XML PATH(''))
                              FROM PreflightPassengerList PP2
				   ) PAX ON PL.LegID = PAX.LegID   
     
      WHERE PM.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      AND PM.IsDeleted=0
       AND (	
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC' THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.ArrivalDTTMLocal WHEN @TimeBase='UTC'   THEN PL.ArrivalGreenwichDTTM ELSE PL.HomeArrivalDTTM END)) <= CONVERT(DATE,@EndDate))
				  OR
					(CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) >= CONVERT(DATE,@BeginDate) AND CONVERT(DATE,(CASE WHEN @TimeBase='Local' THEN PL.NextLocalDTTM WHEN @TimeBase='UTC'  THEN PL.NextGMTDTTM ELSE PL.NextHomeDTTM END)) <= CONVERT(DATE,@EndDate))
			    )
       AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
       AND (C.ClientCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@ClientCD, ',')) OR @ClientCD = '')
       AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@RequestorCD, ',')) OR @RequestorCD = '')
       AND (DEP.DepartmentCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DepartmentCD, ',')) OR @DepartmentCD = '')
       AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
       AND (PL.DutyType IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@DutyTypeCD, ',')) OR @DutyTypeCD = '')
       AND (PM.TripStatus IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@TripStatus, ',')) OR PM.TripStatus IS NULL)
       AND (AT.IcaoID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@HomeBaseCD, ',')) OR @HomeBaseCD = '')
       AND PL.LegID NOT IN (SELECT ISNULL(LegID,0) FROM @TableTrip)
       AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))



INSERT INTO @CurDate(DateWeek,FleetID,TailNum)
SELECT T.*,F.FleetID,F.TailNum FROM Fleet F CROSS JOIN  (SELECT * FROM  DBO.[fnDateTable](@BeginDate,@EndDate)) T
                                            LEFT OUTER JOIN Vendor V ON V.VendorID=F.VendorID
                                 WHERE F.CustomerID=CONVERT(BIGINT,@UserCustomerID)  
                                   AND F.IsDeleted=0 
                                   AND F.IsInActive=0
                                   AND (F.FleetID IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@EFleet, ',')) OR @EFleet = '')
                                   AND (((V.IsInActive=@VendorVal) OR @VendorVal =CASE WHEN @Vendors=3 THEN NULL ELSE  0 END)OR ISNULL(V.IsInActive,'') =(CASE WHEN @Vendors=3 THEN '' END))
 



INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,DepICAOID,CrewList,Passenger,TripStatus,
                       LastUpdated,FlightNUM,Requestor,LegPurpose,FlightCategory,DepartmentCD,AuthorizationCD,FuelLoad,OutbountInstruction,SeatsAvailable,PaxTotal,FgColor,BgColr)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,'R',
        CASE WHEN CD.DateWeek=CONVERT(DATE,TT.NextLocalDTTM) THEN TT.NextLocalDTTM ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye
        ,TT.ArrivelICAOID,TT.CrewList,TT.Passenger,TT.TripStatus,TT.LastUpdated,TT.FlightNUM,TT.Requestor,TT.LegPurpose,TT.FlightCategory
        ,TT.DepartmentCD,TT.AuthorizationCD,TT.FuelLoad,TT.OutbountInstruction,TT.SeatsAvailable,TT.PaxTotal
        ,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END)
         FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID 
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal)   --- CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<CONVERT(DATE,TT.NextLocalDTTM) 
                                               AND RecordTye='T'
                                              -- AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


INSERT INTO @TableTrip(FleetID,TailNum,TypeCode,DepartureDTTMLocal,ArrivelICAOID,DutyTYPE,ArrivalDTTMLocal,TripNUM,LegNum,RecordTye,SeatsAvailable,PaxTotal
                      ,LastUpdated,DepICAOID,ETE,FgColor,BgColr)
SELECT  CD.FleetID,CD.TailNum,TT.TypeCode,CASE WHEN CD.DateWeek=CONVERT(DATE,TT.DepartureDTTMLocal) THEN TT.DepartureDTTMLocal ELSE CD.DateWeek END,TT.ArrivelICAOID,DutyTYPE,
                CASE WHEN CD.DateWeek=CONVERT(DATE,TT.ArrivalDTTMLocal) THEN TT.ArrivalDTTMLocal ELSE (CONVERT(DATETIME, CONVERT(VARCHAR(20),DateWeek, 101) + ' 23:59')) END,TT.TripNUM,TT.LegNum,RecordTye,0,0,TT.LastUpdated,TT.DepICAOID, FLOOR(DATEDIFF(MINUTE,'2013-11-19 00:00','2013-11-19 23:59')/CONVERT(DECIMAL(7,3),60)*10)*0.1 ETE
                ,FgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#000000' ELSE NULL END ),BgColor=(CASE WHEN @BlackWhiteClr=1 THEN  '#FFFFFF' ELSE NULL END)
                 FROM @TableTrip TT INNER JOIN @CurDate CD ON CD.FleetID=TT.FleetID
                                             WHERE CONVERT(DATE,CD.DateWeek)>=CONVERT(DATE,TT.DepartureDTTMLocal) 
                                               AND CONVERT(DATE,CD.DateWeek)<=CONVERT(DATE,TT.ArrivalDTTMLocal) 
                                               AND RecordTye IN('C','M')
                                               AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal) 
                                             --  AND  CONVERT(DATE,DepartureDTTMLocal) BETWEEN  CONVERT(DATE,@BeginDate) AND  CONVERT(DATE,@EndDate)


                  
DELETE FROM @TableTrip WHERE RecordTye IN('C','M') AND DepICAOID IS NOT NULL AND ArrivelICAOID IS NOT NULL AND CONVERT(DATE,DepartureDTTMLocal) <> CONVERT(DATE,ArrivalDTTMLocal)


DECLARE @TempTable TABLE(ID INT IDENTITY
                        ,WeekNumber INT
                        ,FleetID BIGINT
                        ,TailNum VARCHAR(9)
                        ,TripNUM BIGINT
                        ,DepTime VARCHAR(5)
                        ,DepLoc VARCHAR(25)
                        ,ArrTime VARCHAR(5)
                        ,ArrLoc VARCHAR(25) 
                        ,crewlist VARCHAR(4000) 
                        ,Date DATE
                        ,DepartureDTTMLocal DATETIME
                        ,ArrivalDTTMLocal DATETIME
                        ,IsArrival DATE
                        ,LegNum BIGINT
                        ,SORTORDER INT
                        ,RecordType VARCHAR(1)
                        ,TypeCode VARCHAR(10)
                        ,Passenger VARCHAR(4000)
                        ,TripStatus CHAR(1)
                        ,LastUpdated DATETIME
                        ,FlightNUM VARCHAR(12)
                        ,Requestor VARCHAR(5)
                        ,LegPurpose VARCHAR(40)
                        ,FlightCategory CHAR(4)
                        ,DepartmentCD VARCHAR(8)
                        ,AuthorizationCD VARCHAR(8)
                        ,FuelLoad NUMERIC(6,0)
                        ,OutbountInstruction VARCHAR(MAX)
                        ,SeatsAvailable INT
                        ,PaxTotal INT
                        ,ETE NUMERIC(7,3)
                        ,DutyType CHAR(2)
                        ,FgColor VARCHAR(8)
                        ,BgColr  VARCHAR(8)
                        ,RnkFirst INT
                        ,RnkLast  INT
                        ) 
 

INSERT INTO @TempTable   
SELECT 0 WeekNumber
     ,TT.FleetID
     ,TT.TailNum
     ,TripNUM
     ,SUBSTRING (CONVERT(VARCHAR(30),DepartureDTTMLocal ,113),13,5)
     ,DepICAOID
     ,SUBSTRING (CONVERT(VARCHAR(30),ArrivalDTTMLocal ,113),13,5)
     ,ArrivelICAOID
     ,CrewList
     ,CONVERT(DATE,DepartureDTTMLocal)
     ,DepartureDTTMLocal
     ,ArrivalDTTMLocal
     ,CASE WHEN CONVERT(DATE,DepartureDTTMLocal) < CONVERT(DATE,ArrivalDTTMLocal) THEN CONVERT(DATE,ArrivalDTTMLocal)  ELSE NULL END  IsArrival 
     ,LegNum
     ,1 AS SORTORDER
     ,TT.RecordTye
     ,TT.TypeCode
     ,TT.Passenger
     ,TT.TripStatus
     ,TT.LastUpdated
     ,TT.FlightNUM
     ,TT.Requestor
     ,TT.LegPurpose
     ,TT.FlightCategory
     ,TT.DepartmentCD
     ,TT.AuthorizationCD
     ,TT.FuelLoad
     ,TT.OutbountInstruction
     ,TT.SeatsAvailable
     ,TT.PaxTotal
     ,TT.ETE
     ,TT.DutyTYPE
     ,FgColor=ISNULL(TT.FgColor, CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.ForeGrndCustomColor
               WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.ForeGrndCustomColor ELSE TT.FgColor END)
            
     ,BgColr=ISNULL(TT.BgColr,CASE WHEN TT.DutyTYPE=AD.AircraftDutyCD THEN AD.BackgroundCustomColor
                   WHEN TT.DutyTYPE=CDT.DutyTypeCD THEN CDT.BackgroundCustomColor ELSE TT.BgColr END)
     ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal)
     ,ROW_NUMBER()OVER(PARTITION BY TripNUM,CONVERT(DATE,DepartureDTTMLocal) ORDER BY TripNUM,DepartureDTTMLocal DESC) 
   
      FROM @TableTrip TT  
		    LEFT JOIN AircraftDuty AD ON AD.AircraftDutyCD=TT.DutyTYPE AND AD.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
		    LEFT JOIN CrewDutyType CDT ON CDT.DutyTypeCD=TT.DutyTYPE AND CDT.CustomerID=CONVERT(BIGINT,@UserCustomerID) 
      WHERE CONVERT(DATE,DepartureDTTMLocal) BETWEEN CONVERT(DATE,@BeginDate) AND CONVERT(DATE,@EndDate)
      ORDER BY TailNum,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum



 DECLARE @WEEKNUM INT=1,@WEEK INT,@DateVal DATE=@BeginDate;

SELECT  @WEEK=((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) 
--SELECT  @WEEK= CASE WHEN (COUNT( DISTINCT WeekNumber))=0 THEN ((DATEDIFF(DAY,@BeginDate,@EndDate)+1)/7 ) ELSE COUNT( DISTINCT WeekNumber) END  FROM #TempTable

WHILE (@WEEKNUM<=@WEEK)

 BEGIN
  
  SET @BeginDate=DATEADD(DAY,7,@BeginDate)

      WHILE @DateVal < @BeginDate
       
       BEGIN
       
         UPDATE @TempTable SET WeekNumber=@WEEKNUM WHERE DATE=@DateVal
		
		INSERT INTO @TempTable(WeekNumber,RnkFirst,RnkLast)
		   SELECT DISTINCT @WEEKNUM,1,1
		                    WHERE NOT EXISTS(SELECT Date FROM @TempTable WHERE WeekNumber=@WEEKNUM) 

    
       SET @DateVal=DATEADD(DAY,1,@DateVal)
      
      END

 
     
 SET @WEEKNUM=@WEEKNUM+1
 
 END



IF @IsFirstLastLeg=0
BEGIN
SELECT DISTINCT ID
      ,WeekNumber
      ,DateRange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)
      ,LastUpdated
      ,TripNUM=CASE WHEN TT.TripNUM=0 THEN NULL ELSE TT.TripNUM END
      ,TripStatus
      ,FlightNUM
      ,Requestor
      ,dbo.GetShortDateFormatByUserCD(@UserCD,Date) TripDate 
      ,TT.TailNum
      ,DepTime=TT.DepartureDTTMLocal
      ,TT.DepLoc
      ,ArrTime=TT.ArrivalDTTMLocal
      ,TT.ArrLoc
      ,CASE WHEN TT.DutyType='R' THEN NULL ELSE  FLOOR(TT.ETE*10)*0.1 END ETE
        ,CASE WHEN CME.Total= 0 THEN NULL
			WHEN TT.RecordType='C' THEN  FLOOR(TT.ETE*10)*0.1
			WHEN TT.RecordType='M' THEN  FLOOR(TT.ETE*10)*0.1 ELSE CME.Total END CummETE
      ,TT.LegPurpose
      ,TT.FlightCategory
      ,TT.DepartmentCD
      ,TT.AuthorizationCD
      ,CASE WHEN TT.FuelLoad=0 THEN NULL ELSE TT.FuelLoad END FuelLoad
      ,TT.OutbountInstruction
      ,TT.crewlist
      ,TT.Passenger
      ,TT.SeatsAvailable
      ,TT.PaxTotal
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,Date SortDate
	  ,TT.FgColor
	  ,TT.BgColr  
	  	 ,TT.SORTORDER 
	  ,TT.LegNum  
	  ,Val=(CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END)    
   --   ,DateRange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)

 FROM @TempTable TT
 LEFT OUTER JOIN (SELECT TripNUM,DepartureDTTMLocal Departure,ETE, (SELECT FLOOR(SUM(ETE)*10)*0.1  FROM @TempTable b 
													    WHERE b.ID <= a.ID AND b.TripNUM=a.TripNUM) AS Total FROM @TempTable a)CME ON CME.Departure=TT.DepartureDTTMLocal AND CME.TripNUM=TT.TripNUM
 WHERE DATE BETWEEN @DATEFROM AND @DATETO

 ORDER BY (CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER

END
ELSE
BEGIN

SELECT DISTINCT ID
      ,WeekNumber
      ,DateRange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)
      ,LastUpdated
      ,TripNUM=CASE WHEN TT.TripNUM=0 THEN NULL ELSE TT.TripNUM END
      ,TripStatus
      ,FlightNUM
      ,Requestor
      ,dbo.GetShortDateFormatByUserCD(@UserCD,Date) TripDate 
      ,TT.TailNum
      ,DepTime=TT.DepartureDTTMLocal
      ,TT.DepLoc
      ,ArrTime=TT.ArrivalDTTMLocal
      ,TT.ArrLoc
      ,CASE WHEN TT.DutyType='R' THEN NULL ELSE  FLOOR(TT.ETE*10)*0.1 END ETE
      ,CASE WHEN CME.Total= 0 THEN NULL
			WHEN TT.RecordType='C' THEN  FLOOR(TT.ETE*10)*0.1
			WHEN TT.RecordType='M' THEN  FLOOR(TT.ETE*10)*0.1 ELSE CME.Total END CummETE
      ,TT.LegPurpose
      ,TT.FlightCategory
      ,TT.DepartmentCD
      ,TT.AuthorizationCD
      ,CASE WHEN TT.FuelLoad=0 THEN NULL ELSE TT.FuelLoad END FuelLoad
      ,TT.OutbountInstruction
      ,TT.crewlist
      ,TT.Passenger
      ,TT.SeatsAvailable
      ,TT.PaxTotal
      ,DATEPART(WEEKDAY, Date) WEEKDAY
      ,Date SortDate
      ,TT.FgColor
	  ,TT.BgColr 
	  ,TT.SORTORDER 
	  ,TT.LegNum  
	  ,Val=(CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END) 
     --- ,DateRange=dbo.GetShortDateFormatByUserCD(@UserCD,@DateFrom)+' - '+dbo.GetShortDateFormatByUserCD(@UserCD,@DateTo)

 FROM @TempTable TT
 LEFT OUTER JOIN (SELECT TripNUM,DepartureDTTMLocal Departure,ETE, (SELECT FLOOR(SUM(ETE)*10)*0.1  FROM @TempTable b 
													    WHERE b.ID <= a.ID AND b.TripNUM=a.TripNUM) AS Total FROM @TempTable a)CME ON CME.Departure=TT.DepartureDTTMLocal AND CME.TripNUM=TT.TripNUM
 WHERE (RnkFirst=1
   OR RnkLast=1) 
  AND DATE BETWEEN @DATEFROM AND @DATETO
 ORDER BY (CASE WHEN @SortBy=1 THEN TailNum ELSE CONVERT(VARCHAR(20),ISNULL(DepartureDTTMLocal,Date),126) END),SortDate,DepartureDTTMLocal,ArrivalDTTMLocal,TripNUM,LegNum,SORTORDER


END

 IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
 DROP TABLE #TempTable
    

 
 
 END; 


 --EXEC spGetReportPRESchedueCalendarWeeklyDetailExportInformation 'SUPERVISOR_99', 10099,'2012-07-05 ', 1,'2012-07-29', '', '',@Footer = 0







GO


