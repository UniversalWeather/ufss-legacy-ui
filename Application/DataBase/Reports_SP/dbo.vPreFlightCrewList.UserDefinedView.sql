IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vPreFlightCrewList]'))
DROP VIEW [dbo].[vPreFlightCrewList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  -- SPC Name: vPreFlightCrewList
  -- Author: PUKLA K      
  -- Create date: Jan 02 2013    
  -- Description: Get Information from PreflightMain,PrflightLeg and PreFlightCrewList
  -- Revision History      
  -- Date  Name  Ver  Change 


		CREATE VIEW [dbo].[vPreFlightCrewList] AS (
		SELECT PM.TripID
      ,PM.CustomerID
      ,PM.TripNUM
      ,PM.PreviousNUM
      ,PM.TripDescription
      ,PM.FleetID
      ,PM.DispatchNUM
      ,PM.TripStatus
      ,PM.EstDepartureDT
      ,PM.EstArrivalDT
      ,PM.RecordType
      ,PM.AircraftID
      ,PM.PassengerRequestorID
      ,PM.RequestorFirstName
      ,PM.RequestorMiddleName
      ,PM.RequestorLastName
      ,PM.RequestorPhoneNUM
      ,PM.DepartmentDescription
      ,PM.AuthorizationDescription
      ,PM.RequestDT
      ,PM.IsCrew
      ,PM.IsPassenger
      ,PM.IsQuote
      ,PM.IsScheduledServices
      ,PM.IsAlert
      --,PM.Notes
      ,PM.LogisticsHistory
      ,PM.HomebaseID
      --,PM.ClientID
      ,PM.IsAirportAlert
      ,PM.IsLog
      ,PM.BeginningGMTDT
      ,PM.EndingGMTDT
      ,PM.LastUpdUID PMLastUpdUID
      ,PM.LastUpdTS PMLastUpdTS
      ,PM.IsPrivate PMIsPrivate
      ,PM.IsCorpReq
      ,PM.Acknowledge
      ,PM.DispatchNotes
      ,PM.TripRequest
      ,PM.WaitList PMWaitList
      ,PM.FlightNUM PMFlightNUM
      ,PM.FlightCost PMFlightCost
      ,PM.AccountID PMAccountID
      ,PM.TripException
      ,PM.TripSheetNotes
      ,PM.CancelDescription
      ,PM.IsCompleted
      ,PM.IsFBOUpdFlag
      ,PM.RevisionNUM
      ,PM.RevisionDescriptioin
      ,PM.IsPersonal
      ,PM.DsptnName
      ,PM.ScheduleCalendarHistory
      ,PM.ScheduleCalendarUpdate
      ,PM.CommentsMessage
      ,PM.EstFuelQTY
      ,PM.CrewID PMCrewID
      ,PM.ReleasedBy
      ,PM.IsDeleted PMIsDeleted
      ,PM.VerifyNUM
      ,PM.APISSubmit
      ,PM.APISStatus
      ,PM.APISException
      ,PM.IsAPISValid
      ,PM.DepartmentID
      ,PM.AuthorizationID
      ,PM.DispatcherUserName
      ,PM.EmergencyContactID
      ,PL.LegID
      ,PL.LegNUM
      ,PL.DepartICAOID
      ,PL.ArriveICAOID
      ,PL.Distance
      ,PL.PowerSetting
      ,PL.TakeoffBIAS
      ,PL.LandingBias
      ,PL.TrueAirSpeed
      ,PL.WindsBoeingTable
      ,PL.ElapseTM
      ,PL.IsDepartureConfirmed
      ,PL.IsArrivalConfirmation
      ,PL.IsApproxTM
      --,PL.IsScheduledServices
      ,PL.DepartureDTTMLocal
      ,PL.ArrivalDTTMLocal
      ,YEAR(ArrivalDTTMLocal) ArrivalYear
      ,MONTH(ArrivalDTTMLocal) ArrivalMonth
      ,PL.DepartureGreenwichDTTM
      ,PL.ArrivalGreenwichDTTM
      ,PL.HomeDepartureDTTM
      ,PL.HomeArrivalDTTM
      ,PL.GoTime
      ,PL.FBOID
      ,PL.LogBreak
      ,PL.FlightPurpose
    --  ,PL.PassengerRequestorID
      ,PL.RequestorName
      --,PL.DepartmentID
      ,PL.DepartmentName
     -- ,PL.AuthorizationID
      ,PL.AuthorizationName
      ,PL.PassengerTotal
      ,PL.SeatTotal
      ,PL.ReservationTotal
      ,PL.ReservationAvailable
      ,PL.WaitNUM
      ,PL.DutyHours
      ,PL.RestHours
      ,PL.FlightHours
      ,PL.Notes
      ,PL.FuelLoad
      ,PL.OutbountInstruction
      ,PL.DutyTYPE
      ,PL.IsCrewDiscount
      ,PL.CrewDutyRulesID
      ,PL.IsDutyEnd
      ,PL.FedAviationRegNUM
      ,PL.ClientID
      --,PL.LegID
      ,PL.WindReliability
      ,PL.OverrideValue
      ,PL.CheckGroup
      ,PL.AdditionalCrew
      ,PL.PilotInCommand
      ,PL.SecondInCommand
      ,PL.NextLocalDTTM
      ,PL.NextGMTDTTM
      ,PL.NextHomeDTTM
      ,PL.LastUpdUID PLLastUpdUID
      ,PL.LastUpdTS PLLastUpdTS
      ,PL.CrewNotes
      ,PL.IsPrivate PLIsPrivate
      ,PL.WaitList PLWaitList
      ,PL.FlightNUM PLFlightNUM
      ,PL.FlightCost PLFlightCost
      ,PL.CrewFuelLoad
      ,PL.IsDeleted PLIsDeleted
      ,PL.UWAID
      ,PL.USCrossing
      ,PL.Response
      ,PL.ConfirmID
      ,PL.CrewDutyAlert
      ,PL.AccountID PLAccountID
      ,PL.FlightCategoryID
      ,PCL.PreflightCrewListID
      --,PCL.CustomerID
      ,PCL.CrewID PCLCrewID
      ,PCL.PassportID
      ,PCL.VisaID
      ,PCL.HotelName
      ,PCL.CrewFirstName
      ,PCL.CrewMiddleName
      ,PCL.CrewLastName
      ,PCL.DutyTYPE CrewDutyType
      ,PCL.OrderNUM
      ,PCL.IsDiscount
      ,PCL.LastUpdUID PCLLastUpdUID
      ,PCL.LastUpdTS PCLLastUpdTS
      ,PCL.IsBlackberrMailSent
      ,PCL.IsNotified
      ,PCL.IsDeleted PCLIsDeleted
      ,PCL.IsAdditionalCrew
      ,PCL.Street
      ,PCL.CityName
      ,PCL.StateName
      ,PCL.PostalZipCD
      ,PCL.NoofRooms
      ,PCL.ClubCard
      ,PCL.IsRateCap
      ,PCL.MaxCapAmount
				FROM Preflightmain PM
					JOIN PreflightLeg PL ON PM.TripID= PL.TripID  AND PM.CustomerID= PL. CustomerID
					JOIN PreflightCrewList PCL ON PL.LegID = PCL.LegID AND PL.CustomerID = PCL.CustomerID
				WHERE	PM.RecordType='C')


GO


