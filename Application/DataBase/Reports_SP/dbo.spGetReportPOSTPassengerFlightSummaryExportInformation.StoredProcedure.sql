IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetReportPOSTPassengerFlightSummaryExportInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetReportPOSTPassengerFlightSummaryExportInformation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetReportPOSTPassengerFlightSummaryExportInformation]
(
		@UserCD AS VARCHAR(30), --Mandatory
		@DATEFROM AS DATETIME, --Mandatory
		@DATETO AS DATETIME, --Mandatory
		@PassengerRequestorCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@PassengerGroupCD VARCHAR(500)='' ,
		@FlightCatagoryCD AS NVARCHAR(1000) = '', -- [Optional], Comma delimited string with mutiple values
		@SortBy as varchar(1),
		@SortCols as varchar (100)
	)
AS
-- ===============================================================================
-- SPC Name: spGetReportPOSTPassengerFlightSummaryExportInformation
-- Author:  A.Akhila
-- Create date: 1 Jul 2012
-- Description: Get Passenger Flight Summary for EXPORT REPORTS
-- Revision History
-- Date                 Name        Ver         Change
-- 29-05-2013		Mohanraja		2.3			Modified Column as ScheduledTM, instead of ScheduleDTTMLocal based on Changes made in PostFlightLeg SSIS Package
-- ================================================================================

SET NOCOUNT ON

DECLARE @IsZeroSuppressActivityPassengerRpt BIT;
SELECT @IsZeroSuppressActivityPassengerRpt = IsZeroSuppressActivityPassengerRpt FROM Company 
																				WHERE CustomerID = DBO.GetCustomerIDbyUserCD(@UserCD)
																				AND HomebaseID = DBO.GetHomeBaseByUserCD(@UserCD)
--SET @IsZeroSuppressActivityPassengerRpt=1

DECLARE @CUSTOMER BIGINT = dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD));
-----------------------------Passenger and Passenger Group Filteration----------------------

DECLARE @TempPassengerID TABLE     
	(   
		ID INT NOT NULL IDENTITY (1,1), 
		PassengerID BIGINT
  )
  

IF @PassengerRequestorCD <> ''
BEGIN
	INSERT INTO @TempPassengerID
	SELECT DISTINCT P.PassengerRequestorID
	FROM Passenger P
	WHERE P.CustomerID = @CUSTOMER
	AND P.PassengerRequestorCD  IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ','))
END

IF @PassengerGroupCD <> ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	LEFT OUTER JOIN PassengerGroupOrder PGO
	ON P.PassengerRequestorID = PGO.PassengerRequestorID AND P.CustomerID = PGO.CustomerID
	LEFT OUTER JOIN PassengerGroup PG 
	ON PGO.PassengerGroupID = PG.PassengerGroupID AND PGO.CustomerID = PG.CustomerID
	WHERE PG.PassengerGroupCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerGroupCD, ',')) 
	AND P.CustomerID = @CUSTOMER  
END
ELSE IF @PassengerRequestorCD = '' AND  @PassengerGroupCD = ''
BEGIN  
INSERT INTO @TempPassengerID
	SELECT DISTINCT  P.PassengerRequestorID
	FROM Passenger P 
	WHERE  P.CustomerID = @CUSTOMER  
	AND P.IsDeleted=0
END
-----------------------------Passenger and Passenger Group Filteration----------------------
	CREATE TABLE #PASSENGERLOG
		(
		[paxcode] NVARCHAR(5)
		,[last_name] NVARCHAR(20)
		,[first_name] NVARCHAR(20)
		,[legs_flown] BIGINT
		,[blk_hrs] NUMERIC(6,3)
		,[flt_hrs] NUMERIC(6,3)
		,[distance] NUMERIC(5,0)
		,[statute] NUMERIC(5,0)
		)
	INSERT INTO #PASSENGERLOG
		SELECT
			[paxcode] = P.PassengerRequestorCD
			,[last_name] = P.LastName
			,[first_name] = P.FirstName
			,[legs_flown] = COUNT(POP.PassengerID)
			,[blk_hrs] = ISNULL(ROUND(POL.BlockHours,1),0)
			,[flt_hrs] = ISNULL(ROUND(POL.FlightHours,1),0)
			,[distance] = ISNULL(POL.Distance,0)
			,[statute] = ISNULL(POL.Distance *1.1508,0)
		FROM [PostflightPassenger] POP
			INNER JOIN PostflightLeg POL ON  POL.POLegID = POP.POLegID AND POL.IsDeleted=0
			INNER JOIN PostflightMain POM ON POM.POLogID = POP.POLogID AND POM.IsDeleted = 0
			INNER JOIN Passenger P ON P.PassengerRequestorID = POP.PassengerID
			LEFT OUTER JOIN FlightCatagory FC ON FC.FlightCategoryID = POL.FlightCategoryID
			INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
		WHERE POM.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			AND CONVERT(DATE,POL.ScheduledTM) BETWEEN CONVERT(DATE,@DATEFROM) AND CONVERT(DATE,@DATETO)
			AND P.IsDeleted=0 AND P.IsActive=1
			--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
			AND (FC.FlightCatagoryCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@FlightCatagoryCD, ',')) OR @FlightCatagoryCD = '')
			AND POM.IsDeleted = 0
		GROUP BY POL.POLegID, P.PassengerRequestorCD, P.LastName, P.FirstName, POL.BlockHours, POL.FlightHours, POL.Distance--, FC.FlightCatagoryCD
		ORDER BY [paxcode]
	CREATE TABLE #PASSENGER
		(
		[paxcode] NVARCHAR(5)
		,[last_name] NVARCHAR(20)
		,[first_name] NVARCHAR(20)
		,[legs_flown] BIGINT
		,[blk_hrs] NUMERIC(6,3)
		,[flt_hrs] NUMERIC(6,3)
		,[distance] NUMERIC(5,0)
		,[statute] NUMERIC(5,0)
		)
	INSERT INTO #PASSENGER
		SELECT
			[paxcode] = P.PassengerRequestorCD
			,[last_name] = P.LastName
			,[first_name] = P.FirstName
			,[legs_flown] = NULL
			,[blk_hrs] = NULL
			,[flt_hrs] = NULL
			,[distance] = NULL
			,[statute] = NULL
		FROM Passenger P
		INNER JOIN @TempPassengerID TP ON TP.PassengerID=P.PassengerRequestorID
		WHERE P.CustomerID = CONVERT(VARCHAR, dbo.GetCustomerIDbyUserCD(LTRIM(@UserCD))) 
			--AND (P.PassengerRequestorCD IN (SELECT DISTINCT RTRIM(S) FROM dbo.SplitString(@PassengerRequestorCD, ',')) OR @PassengerRequestorCD = '')
			AND P.IsDeleted=0 AND P.IsActive=1
			ORDER BY [paxcode]
		DELETE FROM #PASSENGER WHERE paxcode IN (SELECT DISTINCT paxcode FROM #PASSENGERLOG)
		
DECLARE @SQLCode NVARCHAR(2000)
IF @IsZeroSuppressActivityPassengerRpt=0
	BEGIN
				
		IF  @SortCols<>''		
			BEGIN	
			SET @SQLCode=	'SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
							group by paxcode,last_name,first_name
							UNION ALL
							SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGER
							group by paxcode,last_name,first_name
							ORDER BY '+ @SortCols
			EXEC sp_executesql @SQLCode
			END
		ELSE	
			BEGIN	
			
				IF @SortBy='a'
					BEGIN	
					SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
					group by paxcode,last_name,first_name
						UNION ALL
					SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGER
					group by paxcode,last_name,first_name
					ORDER BY paxcode
				END
				ELSE
					BEGIN
					SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
					group by paxcode,last_name,first_name
						UNION ALL
					SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGER
					group by paxcode,last_name,first_name
					ORDER BY last_name
				END
			END
	END
ELSE
BEGIN
		IF  @SortCols<>''
			BEGIN			
			SET @SQLCode=	'SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
					group by paxcode,last_name,first_name
					ORDER BY '+ @SortCols
			EXEC sp_executesql @SQLCode
			END
		ELSE	
			BEGIN	

					IF @SortBy='a'
						BEGIN
						SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
					group by paxcode,last_name,first_name
						ORDER BY paxcode
						END
					ELSE
						BEGIN
						SELECT paxcode,last_name,first_name,sum(legs_flown) legs_flown,sum(blk_hrs) blk_hrs,sum(flt_hrs) flt_hrs,sum(distance) distance,sum(statute) statute FROM #PASSENGERLOG
					group by paxcode,last_name,first_name
						ORDER BY last_name
						END
			END
END

IF OBJECT_ID('tempdb..#PASSENGERLOG') IS NOT NULL
DROP TABLE #PASSENGERLOG

IF OBJECT_ID('tempdb..#PASSENGER') IS NOT NULL
DROP TABLE #PASSENGER
--EXEC spGetReportPOSTPassengerFlightSummaryExportInformation 'DISP','2000-01-01','2020-01-01', '', '','','','blk_hrs,paxcode,last_name'
GO