USE [FSS_DEV]
GO
/****** Object:  UserDefinedFunction [dbo].[GetHoursAway]    Script Date: 11/05/2014 22:59:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetHoursAway](@UserCD VARCHAR(30),@POLegID BIGINT, @POLogID BIGINT)
RETURNS Numeric(10,2)
 
AS BEGIN
DECLARE @HrsAway Numeric(10,2)
Declare @PrevOutboundDTTM DateTime
Declare @PrevInboundDTTM DateTime
Declare @OutboundDTTM DateTime
Declare @InboundDTTM DateTime
Declare @LegNUM Int=0
Declare @RecCnt Int=0



SELECT @LegNUM=LegNUM, @OutboundDTTM=OutboundDTTM,@InboundDTTM=InboundDTTM 
FROM   PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD)  And
POLogID=@POLogID And POLegID =@POLegID  

 
Set @RecCnt=(SELECT Count(*) FROM PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) And POLogID=@POLogID And LegNUM <@LegNUM )

SELECT TOP 1 @PrevOutboundDTTM=OutboundDTTM,@PrevInboundDTTM=InboundDTTM 
FROM   PostflightLeg 
Where  CustomerID = dbo.GetCustomerIDbyUserCD(@UserCD) And 
POLogID=@POLogID And LegNUM <@LegNUM  
Order By LegNUM Desc  

IF @RecCnt >0
Begin
  Set @HrsAway = (SELECT CAST(DateDiff(MINUTE ,@PrevInboundDTTM,@InboundDTTM)/60.0 AS DECIMAL(10,2)))
End
Else 
Begin
  Set @HrsAway = (SELECT CAST(DateDiff(MINUTE ,@OutboundDTTM,@InboundDTTM)/60.0 AS DECIMAL(10,2)))
End   
RETURN @HrsAway
END