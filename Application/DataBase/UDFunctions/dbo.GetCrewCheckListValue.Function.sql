

/****** Object:  UserDefinedFunction [dbo].[GetCrewCheckListValue]    Script Date: 06/18/2013 14:37:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCrewCheckListValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetCrewCheckListValue]
GO



/****** Object:  UserDefinedFunction [dbo].[GetCrewCheckListValue]    Script Date: 06/18/2013 14:37:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[GetCrewCheckListValue](@CrewID BIGINT)
RETURNS char(1)
AS 
BEGIN
--Declare @CrewID BIGINT = 1000552331;
	DECLARE @DateOnly date;
	SELECT  @DateOnly = CONVERT(DATE,GETDATE());
	DECLARE @CheckListCD char(3);
	DECLARE @TestDueDT date;
	DECLARE @TestAlertDT date;
	DECLARE @TestGraceDT date;
	DECLARE @TestPreviousDT date;
	DECLARE @TestBaseDT date;
	DECLARE @NonConflictEvent bit;
	DECLARE @CustomerID bigint;
	DECLARE @IsSched bit;
	--Print @DateOnly
	DECLARE @Red bit = 0;                                
	DECLARE @RetVal char(1);
	
	SET @RetVal = '1';
	SET @IsSched = 0;
    
    DECLARE @CheckListID bigint;
    DECLARE curTailInfo CURSOR FOR SELECT DISTINCT CheckListID FROM CrewchecklistDetail WHERE CrewID=@CrewID and isdeleted = 0        
	
	OPEN curTailInfo;            
          
	FETCH NEXT FROM curTailInfo INTO @CheckListID;   
	
	WHILE @@FETCH_STATUS = 0            
	BEGIN  
		BEGIN  
  
			SELECT @CustomerID = CustomerID,@CheckListCD=CheckListCD,@TestDueDT = CONVERT(DATE,DueDT) ,@TestAlertDT = CONVERT(DATE,AlertDT) ,@TestGraceDT = CONVERT(DATE,GraceDT) ,@TestPreviousDT = CONVERT(DATE,PreviousCheckDT) ,@TestBaseDT = CONVERT(DATE,BaseMonthDT) ,@NonConflictEvent = IsNoConflictEvent FROM CrewCheckListDetail WHERE CheckListID = @CheckListID        
			SELECT @IsSched = IsScheduleCheck FROM CrewCheckList WHERE CrewCheckCD = @CheckListCD AND CustomerID = @CustomerID AND IsDeleted = 0
		
			IF(@IsSched = 1)
			BEGIN
		   --   Print 'Sched'
		   --   Print @CheckListID
		      
		   --   Print 'DueNext'
		   --   Print @TestDueDT
		      
		   --   Print 'Alert'
		   --   Print @TestAlertDT
		      
		   --   Print 'GraceDT'
		   --   Print @TestGraceDT
		      
		   --   Print 'NonConflict'
		   --   Print @NonConflictEvent
	      
			  SET @RetVal = '0';
			  SET @Red = 1;	  
		    END
		  
			IF((@TestDueDT <> '' or @TestDueDT <> null) and (@TestGraceDT <> '' or @TestGraceDT <> null) and (@TestAlertDT <> '' or @TestAlertDT <> null))
			BEGIN
		  --  Print 'not null'
				IF ((@TestDueDT > @DateOnly) and (@TestAlertDT > @DateOnly) and (@TestGraceDT > @DateOnly))
				BEGIN
					IF (@NonConflictEvent <> 1)
					BEGIN
						SET @Red = 0;
					END
				END
	        
				IF (((@TestDueDT < @DateOnly) and (@DateOnly < @TestGraceDT)) or ((@TestGraceDT < @DateOnly) and (@DateOnly < @TestDueDT)))
				BEGIN
					IF (@NonConflictEvent <> 1)
					BEGIN
						SET @Red = 0;
					END
				END
	        
				IF ((@DateOnly > @TestDueDT) and (@TestGraceDT < @DateOnly))
				BEGIN
					IF (@NonConflictEvent <> 1)
					BEGIN
					--Print 'Date'
					--Print @CheckListID
						SET @Red = 1;
					END
				END
	        
				IF (((@TestDueDT < @DateOnly) and (@DateOnly < @TestAlertDT)) or ((@TestAlertDT < @DateOnly) and (@DateOnly < @TestDueDT)))
				BEGIN	        
					IF (@NonConflictEvent <> 1)
					BEGIN
					--Print 'Y'
					   SET @Red = 0;
					END
				END
			END
		
			IF(@Red = 1)
			BEGIN
				BREAK;
			END
		END  
    FETCH NEXT FROM curTailInfo INTO  @CheckListID
              
	END             
	CLOSE curTailInfo;            
	DEALLOCATE curTailInfo; 
	
 IF(@Red = 1)
 BEGIN 
	SET @RetVal = '0';
 END
 ELSE
 BEGIN
	SET @RetVal = '1';
 END
 
    RETURN @RetVal;
END

GO


