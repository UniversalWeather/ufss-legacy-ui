﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Web.Entities.SchedulingCalendar
{
    [Serializable]
    public class PrivateTrip
    {
        public bool IsShowArrivalDepartICAO { get; set; }
        public bool IsShowArrivalDepartTime { get; set; }
        public bool IsShowCrew { get; set; }
        public bool IsShowFlightNumber { get; set; }
        public bool IsShowTripPurpose { get; set; }
        public bool IsShowLegPurpose { get; set; }        
        public bool IsShowSeatsAvailable { get; set; }
        public bool IsShowPaxCount { get; set; }
        public bool IsShowRequestor { get; set; }
        public bool IsShowDepartment { get; set; }
        public bool IsShowAuthorization { get; set; }
        public bool IsShowFlightCategory { get; set; }
        public bool IsShowETE { get; set; }
        public bool IsShowCumulativeETE { get; set; }
        public bool IsShowTotalETE { get; set; }
        
       // public string DefaultPrivateTrip = "ArrivalDepartICAO-True~ArrivalDepartTime-True~Crew-True~FlightNumber-True~LegPurpose-True~SeatsAvailable-True~PaxCount-True~Requestor-True~Department-False~Authorization-False~FlightCategory-T~CumulativeETE-T~";
    }
}
