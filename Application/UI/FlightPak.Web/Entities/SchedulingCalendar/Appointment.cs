﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;

namespace FlightPak.Web.Entities.SchedulingCalendar
{
    public class Appointment
    {

        public Int64 LegId { get; set; }
        public Int64 LegNum { get; set; }
        public Int64 TripId { get; set; }
        public Int64 TripNUM { get; set; }
        public string TailNum { get; set; }
        public string TripStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime LocalDepartureTime { get; set; }
        public DateTime LocalArrivalTime { get; set; }
        public DateTime HomelDepartureTime { get; set; }
        public DateTime HomeArrivalTime { get; set; }
        public DateTime UTCDepartureTime { get; set; }
        public DateTime UTCArrivalTime { get; set; }
        public DateTime DepartureDisplayTime { get; set; }
        public DateTime ArrivalDisplayTime { get; set; }

        public int ReservationAvailable { get; set; }
        public string PassengerRequestorCD { get; set; }
        public Int64 PassengerRequestorId { get; set; }
        public string FlightNum { get; set; }
        public string TripPurpose { get; set; }
        public string FlightPurpose { get; set; }        
        public decimal? FuelLoad { get; set; }
        public int PassengerCount { get; set; }
        public Int64 DepartmentId { get; set; }
        public string DepartmentCD { get; set; }
        public string DepartmentName {get;set;}
        public Int64 AuthorizationId { get; set; }
        public string AuthorizationCD { get; set; }
        public Int64 HomebaseID { get; set; }
        public string HomebaseCD { get; set; }
        public string ETE { get; set; }
        public string CumulativeETE { get; set; }
        public string TotalETE { get; set; }
        public Int64 ClientId { get; set; }
        public string ClientCD { get; set; }
        public string OutboundInstruction { get; set; }

        public string ArrivalICAOID { get; set; }
        public string DepartureICAOID { get; set; }
        public string ArrivalAirportName { get; set; }
        public string DepartureAirportName { get; set; }
        public string ArrivalCity { get; set; }
        public string DepartureCity { get; set; }
        public string ArrivalCountry { get; set; }
        public string DepartureCountry { get; set; }
        public string Notes { get; set; }

        public Int64 FlightCategoryID { get; set; }
        public string FlightCategoryCode { get; set; }
        public string FlightCategoryDescription { get; set; }
        public string FlightCategoryForeColor { get; set; }
        public string FlightCategoryBackColor { get; set; }

        public Int64 CrewId { get; set; }
        public string CrewCD { get; set; }
        public string CrewFirstName { get; set; }
        public string CrewMiddleName { get; set; }
        public string CrewLastName { get; set; }
        public long CrewDutyType { get; set; }
        public string CrewDutyTypeCD { get; set; }
        public string CrewDutyTypeDescription { get; set; }
        public string CrewDutyForeColor { get; set; }
        public string CrewDutyBackColor { get; set; }
        public string CrewCodes { get; set; }
        public string CrewFullNames { get; set; }

        public Int64 AircraftDutyID { get; set; }
        public string AircraftDutyCD { get; set; }
        public string AircraftDutyDescription { get; set; }
        public string AircraftDutyForeColor { get; set; }
        public string AircraftDutyBackColor { get; set; }

      
        public string PassengerCodes { get; set; }
        public string PassengerNames { get; set; }

        // trip main properties
        public Int64 FleetID { get; set; }
        public string FirstLegICAOID { get; set; }
        public string LastLegICAOID { get; set; }
        public DateTime FirstLegStartTime { get; set; }
        public DateTime LastLegEndTime { get; set; }
        public string Description { get; set; }
        public string ElapseTM {get;set;}
        public string LastUpdatedTime {get;set;}
        public string LastUpdatedUser {get;set;}
        public string TripDate { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        
        public bool IsLog { get; set; }
        public bool IsStandByCrew { get; set; }
        public bool IsPrivateLeg { get; set; }
        public bool IsPrivateTrip { get; set; }
        public bool IsRONAppointment { get; set; }

        public string ToolTipSubject { get; set; }
        public string ToolTipDescription {get;set;}
        public string RecordType { get; set; }

        public string ViewMode { get; set; }
        public bool ShowRedEyeInfo { get; set; }
        public bool ShowArrivalDateInfo{ get; set; }

        public string FleetNotes { get; set; }
        public string CrewNotes { get; set; }
        public string AircraftForeColor { get; set; }
        public string AircraftBackColor { get; set; }
        public string TripNumberString { get; set; }

        public bool ShowAllDetails { get; set; }
        public bool ShowPrivateTripMenu { get; set; }
        public string CrewFuelLoad { get; set; }

    }
}