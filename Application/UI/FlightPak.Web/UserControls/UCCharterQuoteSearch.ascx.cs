﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Common;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.UserControls
{
    public partial class UCCharterQuoteSearch : System.Web.UI.UserControl
    {
        #region Declarations
        private ExceptionManager exManager;
        public string CQSessionKey = "CQFILE";

        public Int32 FileNUM
        {
            get { return Convert.ToInt32(lbFileNumber.Text); }
            set { lbFileNumber.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public Int64 QuoteNUM
        {
            get { return Convert.ToInt64(lbQuoteNum.Text); }
            set { lbQuoteNum.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public string TailNUM
        {
            get { return lblTailNo.Text; }
            set { lblTailNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public string TypeCode
        {
            get { return lblTypeCode.Text; }
            set { lblTypeCode.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public Label LastModified
        {
            get { return this.lbLastModified; }
        }

        public LinkButton lnkPreviewReport { get { return this.lnkInvoiceReport; } }

        public string ReportName { get; set; }
        public string ReportToolTip { get; set; }
        public string UserName { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbSearchMsg.Text = string.Empty;
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(ReportToolTip))
                    lnkInvoiceReport.ToolTip = ReportToolTip;

                hdnRptName.Value = ReportName;
                hdnUser.Value = UserName;

                if (Session[CQSessionKey] != null)
                {
                    CQFile File = new CQFile();
                    File = (CQFile)Session[CQSessionKey];

                    if (File != null && File.CQFileID != null)
                        hdnFileID.Value = File.CQFileID.ToString();
                    else
                        hdnFileID.Value = string.Empty;
                    lnkInvoiceReport.Enabled = true;

                    lnkHistory.CssClass = "history-icon";
                    lnkHistory.OnClientClick = "javascript:openWin('rdHistory');return false;";

                    lnkHistory.Enabled = true;


                }
                else
                {
                    lnkHistory.Enabled = false;
                    lnkHistory.OnClientClick = string.Empty;
                    lnkHistory.CssClass = "history-icon-disable";

                    lnkInvoiceReport.Enabled = false;
                }
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                lbSearchMsg.Text = string.Empty;
                if (!string.IsNullOrEmpty(SearchBox.Text))
                {
                    CQFile FileRequest = new CQFile();
                    if (Session["CQFILE"] != null)
                        FileRequest = (CQFile)Session["CQFILE"];

                    if (FileRequest != null)
                    {
                        if (FileRequest.Mode == CQRequestActionMode.Edit)
                        {
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File Unsaved, please Save or Cancel File', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "',alertCallBack);");
                        }
                        else
                        {
                            Int64 SearchFileNum = 0;
                            if (Int64.TryParse(SearchBox.Text, out SearchFileNum))
                            {
                                using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                                {
                                    //Method will be changed once manager completed
                                    var objRetVal = Service.GetCQRequestByFileNum(SearchFileNum, false);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        FileRequest = objRetVal.EntityList[0];
                                        if (FileRequest.CQMains != null)
                                        {
                                            foreach (CQMain quote in FileRequest.CQMains)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                    if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                    {
                                                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                        {
                                                            CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                            objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                            objImageFile.RecordType = "CQQuoteOnFile";
                                                            objImageFile.UWAFileName = fwh.UWAFileName;
                                                            objImageFile.RecordID = quote.CQMainID;
                                                            objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                            objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                            objImageFile.IsDeleted = false;
                                                            if (quote.ImageList == null)
                                                                quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                            quote.ImageList.Add(objImageFile);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        Session["CQFILE"] = FileRequest;

                                        // Uncomment once exception logic completed
                                        //var ObjretVal = Service.GetCQExceptionList(FileRequest.CQFileID);
                                        //if (ObjretVal.ReturnFlag)
                                        //    Session["CQEXCEPTION"] = ObjretVal.EntityList;
                                        //else
                                        //    Session["CQEXCEPTION"] = null;

                                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                    }
                                    else
                                    {
                                        //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File No. Does Not Exist', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "',alertCallBack);");
                                        lbSearchMsg.Text = System.Web.HttpUtility.HtmlEncode("File No. Does Not Exist");
                                    }
                                }
                            }
                            else
                            {
                                //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File No. Does Not Exist', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "',alertCallBack);");
                                lbSearchMsg.Text = System.Web.HttpUtility.HtmlEncode("File No. Does Not Exist");
                            }
                        }
                    }
                }
                else
                {
                    lbSearchMsg.Text = string.Empty;
                }
            }
        }

        protected void lnkInvoiceReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQFILE"] != null)
                        {


                            Int64 QuoteNumInProgress = 1;
                            CQFile FileRequest = new CQFile();
                            FileRequest = (CQFile)Session["CQFILE"];
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            //"RptCQInvoice"
                            if (!string.IsNullOrEmpty(hdnRptName.Value))
                            {

                                if (hdnRptName.Value == "RptCQInvoice")
                                    this.Page.GetType().InvokeMember("SaveInvoiceReport", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

                                else
                                    this.Page.GetType().InvokeMember("SaveQuoteReport", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

                                Session["TabSelect"] = "Charter";
                                Session["REPORT"] = hdnRptName.Value;
                                Session["FORMAT"] = "PDF";
                                Session["PARAMETER"] = "QuoteNUM=" + QuoteNumInProgress.ToString() + ";CQFileID=" + FileRequest.CQFileID.ToString() + ";UserCD=" + hdnUser.Value;
                                Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                                //ShowReport(hdnRptName.Value, QuoteNumInProgress, FileRequest.CQFileID);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "',alertCallBack);");
                }
            }
        }

        protected void btnAlert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }            
        }
    }
}
