﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard4.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard4" %>
<div class="modal fade upload-wizard-complete-modal custom-bootstrap" id="uploadWizardCompleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">File Upload Wizard</h4>
      </div>
      <div class="modal-body content-layer">
          <div class="alert-success">
			<div class="icon-sign success-icon">
            </div>
                <div class="center">	            
                    <h5>Success</h5>
                        <p>Your fuel pricing has been formatted and the prices can be accessed through Preflight > Logistics
                        </p>
                    <p id="pSuccessMsg">
                        	<b>Total Records Read from file : 2165 <br/>
							Total saved records: 2132							
							</b>
                    </p>
                </div>        			
         </div>
          <div class="butt-layout clearfix">
          	<button type="button" class="btn btn-blue pull-left hidden">Back</button>
            <button type="button" class="btn btn-red pull-right" data-dismiss="modal" id="btnExit">Exit</button>
        </div>
      </div>
    </div>
  </div>
</div>