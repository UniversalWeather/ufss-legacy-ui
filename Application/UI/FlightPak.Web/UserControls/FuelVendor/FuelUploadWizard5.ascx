﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard5.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard5" %>
<div class="modal fade upload-wizard-modal-5 custom-bootstrap drag-drop-done" id="uploadFieldMappingWizardModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">File Upload Wizard</h4>
            </div>
            <div class="content-layer">
                <p>Drag and drop the column name from the list on the right-hand side to the spreadsheet below.</p>
                <p>Only FBO, ICAO or IATA, Effective Date, Gallons From, Gallons To, Price are mandatory. The remaining columns can be ignored.</p>
                <ul class="list-unstyled text-color pull-left">
                    <li>
                        <span id="column1" class="importantColumns">FBO</span>
                            <span class="icon-bar"></span>
                    </li>
                    <li>
                        <span class="importantColumns drag-drop-done" id="column2">ICAO</span><span class="icon-bar">
                    </span>
                    </li>
                    <li><span class="importantColumns" id="column3">IATA</span><span class="icon-bar">
                    </span>
                    </li>
                    <li><span class="importantColumns drag-drop-done" id="column4">Effective Date</span><span class="icon-bar"></span>
                    
                    </li>
                    <li><span class="importantColumns" id="column5">Price</span><span class="icon-bar"></span>
                    
                    </li>
                    <li><span class="importantColumns" id="column6">Gallons From</span><span class="icon-bar"></span>                    
                    </li>
                    <li><span class="importantColumns" id="column7">Gallons To</span><span class="icon-bar"></span>                    
                    </li>
                </ul>
                <div class="text-center drag-drop-container pull-right hidden">
                    <p class="noMargin text-color">Drag and drop each header name on the</p>
                    <p class="text-color">left side onto the applicable column</p>
                </div>
                <div class="table-responsive pull-right">
                    <div class="panel panel-custom">
                        <table id="tblDropbox" class="table table-striped table-condensed table-text">
                            <tbody>
                                <tr>
                                    
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                                <tr class="text-center">
                                    
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                                <tr class="text-center">
                                   
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="butt-layout clearfix">
                    <button id="btnBackFromMappingWizard" type="button" class="btn btn-blue pull-left">Back</button>
                    <button id="btnContinueFromMappingWizard" type="button" class="btn btn-green pull-right">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>

