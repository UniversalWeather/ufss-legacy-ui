﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard6.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard6" %>
<div class="modal fade upload-wizard-complete-modal custom-bootstrap" id="uploadWizardFormatNotFound" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">File Upload Wizard</h4>
      </div>
      <div class="modal-body content-layer">
          <div class="alert-warning">
			<div class="icon-sign warning-icon"></div>
                <div class="center">	            
                    <h5>Fuel file formate Not Found</h5>
                    <p>Below is the file you submitted, with the columns not readjusted to Universal’s accepted file format Or The file format has changed since your last upload.</p>
                    <p>Please Contact FSS Support to add fuel vendors file format.</p>
                </div>        			
         </div>
          <div class="butt-layout clearfix">
              <asp:Button runat="server" ID="btnExit" OnClick="btnExit_OnClick" class="btn btn-red pull-right" data-dismiss="modal" Text="Exit"/>
        </div>
      </div>
    </div>
  </div>
</div>