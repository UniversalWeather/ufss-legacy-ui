﻿using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Helpers;
using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

namespace FlightPak.Web.UserControls.FuelVendor
{
    public partial class FuelUploadWizard3 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNotMappedCorrect_Click(object sender, EventArgs e)
        {   
        }

        protected void btnMappedCorrectly_Click(object sender, EventArgs e)
        {
            FlightPak.Web.FlightPakMasterService.FuelFileData objFuelFileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)Session["FuelFileData"];
            objFuelFileData.VendorID =Convert.ToInt64(Session["FuelVendorID"].ToString());            
            using(FlightPakMasterService.MasterCatalogServiceClient client = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (FlightPak.Web.Framework.Helpers.MiscUtils.HaveRequiredFuelColumnsFound(objFuelFileData))
                {
                    HttpContext.Current.Session.Remove("highlightFuelField");
                    var result = client.AddBulkFlightpakFuel(objFuelFileData);
                    if (File.Exists(objFuelFileData.FullPhysicalFilePath))
                    {
                        string filepath = Path.GetDirectoryName(objFuelFileData.FullPhysicalFilePath) + @"\" + Path.GetFileNameWithoutExtension(objFuelFileData.FullPhysicalFilePath)+".csv";
                        if (Path.GetFileName(filepath) == objFuelFileData.FileName)
                        {
                            FileValidators fileValidName = new FileValidators();
                            filepath = fileValidName.CleanFullPath(filepath);
                            var ValidateFolderPart = ConfigurationManager.AppSettings["FuelCsvFilesFolder"].ToLower();
                            if (File.Exists(filepath) && filepath.ToLower().Contains(ValidateFolderPart))
                                File.Delete(filepath);
                        }
                    }
                    if (result.ReturnFlag == true)
                    {
                        HttpContext.Current.Session[FuelFileConstants.InvalidFuelRecords] = result.EntityList[0]; // Invalid Records saved in session
                        StringBuilder msg = new StringBuilder(@"<b>");
                        msg.Append("<br/>Total saved records:");                        
                        msg.Append(result.EntityList.Count.ToString());// Total Records Saved
                        msg.Append("<br/>Invalid records:");
                        msg.Append(result.ErrorMessage);// Invalid Records- not saved
                        msg.Append(" <a href='FuelFileInvalidRecordsHandler.ashx?filename=" + Path.GetFileNameWithoutExtension(objFuelFileData.FullPhysicalFilePath) + "_ErrorLog'>Download Error Log</a> ");
                        msg.Append("</b>");
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Views.Settings.Logistics.FuelVendor), "SuccessWiard", "OpenSuccessWizard(\"" + msg.ToString() + "\");", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Views.Settings.Logistics.FuelVendor), "FileReadingError", "OpenMainWizard();", true);
                    }
                }
                else
                {
                    // Open cmapping wizard as still all required field are not available
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Views.Settings.Logistics.FuelVendor), "MappingModalOpening", "OpenMainWizard();", true);
                }
            }

        }
    }
}