﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.UserControls.FuelVendor
{
    public partial class FuelUploadWizard6 : System.Web.UI.UserControl
    {
        private ExceptionManager _exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExit_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                _exManager.Process(() =>
                {
                    if (Session["FuelFileData"] != null)
                    {
                        var objFuelFileData = (FuelFileData)Session["FuelFileData"];
                        if (File.Exists(objFuelFileData.FullPhysicalFilePath))
                        {
                            if (Path.GetFileName(objFuelFileData.FullPhysicalFilePath) == objFuelFileData.FileName)
                                File.Delete(objFuelFileData.FullPhysicalFilePath);
                        }
                    }
                }, Policy.UILayer);
            }
        }
    }
}