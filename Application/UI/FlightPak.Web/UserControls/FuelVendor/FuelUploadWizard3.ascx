﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard3.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard3" %>
<div class="modal fade upload-wizard-modal-4 custom-bootstrap" id="uploadWizardModalConfirmation" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">File Upload Wizard</h4>
      </div>
      <div class="content-layer">
          <div id="divDataLoadingM1" class="container-fluid" style="display:none;">
                    <div class="alert alert-loading">
                        <p><span class="icon-loading"></span>Loading...</p>
                    </div>
                </div>
      <p>Below is the file you submitted, with the columns readjusted
		to Universal’s accepted file format. Please confirm if the header
		columns in yellow are mapped appropriately.</p>
          <p id="pMsgIfChangedFormat" style="display:none;">The file format has changed since your last upload.  Please see changes highlighted in red.</p>
             
      <div class="table-responsive">  
          <div class="panel panel-custom">
            <table id="confirmationTable" class="table table-striped table-condensed">
                <tbody>                    
                </tbody>
            </table>
          </div>
      </div>
          <div class="butt-layout clearfix">
            <asp:Button runat="server" ClientIDMode="Static" ID="btnNotMappedCorrect" OnClientClick="return btnNotMappedcorrectlyClick();"  type="button"  class="btn btn-red pull-left" Text="If the column headers are not mapped correctly, click here"/>
            <asp:Button runat="server" ID="btnMappedCorrectly" OnClick="btnMappedCorrectly_Click"  type="button"  class="btn btn-green pull-right" Text="The column headers are correct."/>
        </div>
        </div>
      </div>
    </div>
  

</div>

