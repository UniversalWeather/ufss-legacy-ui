﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard2.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard2" %>
 <div class="modal fade upload-wizard-modal-2 custom-bootstrap" id="uploadWizardModal2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">File Upload Wizard</h4>
      </div>
      <div class="content-layer">
      <p>Below is a preview of the uploaded file. Click “Continue” to accept this file or click “Back” to upload a new file.</p>
             <div id="divDataLoading" class="container-fluid" style="display:none;">
                    <div class="alert alert-loading">
                        <p><span class="icon-loading"></span>Loading...</p>
                    </div>
                </div>
      <div class="panel panel-custom">
        <table id="previewTable" class="table table-striped table-condensed">
            <tbody>
             
            </tbody>
        </table>
      </div>
          <div class="butt-layout clearfix">
            <button id="btnBackPreviewModal" type="button" class="btn btn-blue pull-left">Back</button>
            <button id="btnContinueToConfirmationWizard" onClick="btnContinueToConfirmationWizard_Click();" type="button"  class="btn btn-green pull-right">Continue</button> 
        </div>
        </div>
      </div>
    </div>
  </div>
       
       