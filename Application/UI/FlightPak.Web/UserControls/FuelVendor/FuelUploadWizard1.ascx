﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FuelUploadWizard1.ascx.cs" Inherits="FlightPak.Web.UserControls.FuelVendor.FuelUploadWizard1" %>

<div class="modal fade upload-wizard-modal custom-bootstrap" id="uploadWizardModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">File Upload Wizard</h4>
            </div>
            <div class="modal-body content-layer">
                <div id="divDataLoading" class="container-fluid" style="display:none;">
                    <div class="alert alert-loading">
                        <p><span class="icon-loading"></span>Loading...</p>
                    </div>
                </div>            
                <div id="IE8Zone">
                    <p class="text-color1">To upload your CSV file, click below.</p>
                    <label id="lblbrowse" class="btn btn-blue btn-browse">Browse files</label>
                </div>
                <div id="zone" class="border-box clearfix">
                    <p id="pDropboxTopMsg1" class="noMargin text-color1">Drag and drop your CSV file here or </p>
                    <p class="text-color1">click below to upload:</p>

                    <button id="btnbrowse" type="button" class="btn btn-blue btn-browse">Browse files</button>
                </div>

                <div id="spanMsg2" class="bar-layout clearfix">
                    <div style="display: block" class="status-layout clearfix">
                        <span class="pull-left text-color2">Upload in Progress</span><span id="spanProgressValue" class="pull-right text-color2">0%</span>
                    </div>
                    <!-- The global progress bar -->
                    <div class="bar-layout clearfix">
                        <div class="grey-bar">
                            <div id="progress" class="green-bar"></div>
                        </div>
                    </div>
                </div>
                <div id="divMaintainHistoricalFuelData" style="text-align: left;margin-top: 10px;display: none">
                    <asp:CheckBox ID="chkIsMaintainHistoricalFuelData" Checked="True" Text="Maintain Historical Fuel Data" runat="server"/>
                </div>
                <div id="divSuccess" style="display: none" class="alert alert-success">
                    <div class="validate-status status-success">
                        <div class="pull-left">
                            <span class="icon-sign icon-success"></span>
                        </div>
                        <div class="hasGlyph">
                            <p>File uploaded successfully</p>
                        </div>
                    </div>
                </div>
                <div id="divError" style="display: none" class="alert alert-error">
                    <div class="validate-status status-error">
                        <div class="pull-left">
                            <span class="icon-sign icon-warning"></span>
                        </div>
                        <div class="hasGlyph">
                            <p id="pMsgText">Uploading Error. Please try one more time.</p>
                        </div>
                    </div>
                </div>
                <div class="butt-layout clearfix">
                    <button id="btnExit" type="button" class="btn btn-red pull-left" data-dismiss="modal">Exit</button>
                    <button id="btnContinueWizard1" type="button" class="btn btn-green pull-right">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>
