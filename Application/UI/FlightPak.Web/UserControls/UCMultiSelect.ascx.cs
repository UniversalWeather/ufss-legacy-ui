﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.UserControls
{
    public partial class UCMultiSelect : System.Web.UI.UserControl
    {
        private string strLoadEntity;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }


        private void LoadData()
        {
            FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();            

            switch (strLoadEntity)
            {
                case "Metro":                    
                    var objRetVal = ObjService.GetMetroCityList();

                    if (objRetVal.ReturnFlag == true)
                    {
                        gvDisplay.DataSource = objRetVal.EntityList;
                        gvDisplay.DataBind();
                    }                   
                    break;

                case "Airport":                   
                    var CustomerValue = ObjService.GetAirportList();

                    if (CustomerValue.ReturnFlag == true)
                    {
                        gvDisplay.DataSource = CustomerValue.EntityList;
                        gvDisplay.DataBind();
                    }                   
                    break;

                case "FleetGroup":                       
                       //var ObjRetVal = ObjService.GetFleetList();
                       //if (ObjRetVal.ReturnFlag == true)
                       //{
                       //    gvDisplay.DataSource = ObjRetVal.EntityList;
                       //    gvDisplay.DataBind();
                       //}  
                       break;
                default:
                    break;
            }


        }


        public string LoadEntity
        {
            get { return strLoadEntity; }
            set { strLoadEntity = value; }
        }




    }
}