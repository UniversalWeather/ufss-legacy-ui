﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPostflightBottomButtonControls.ascx.cs" Inherits="FlightPak.Web.UserControls.Postflight.UCPostflightBottomButtonControls" %>
<script type="text/javascript">
    $(function () {

        var tabName = getQuerystring("seltab", "PostFlight");

        $("#btnNext").click(function () {            
            var redirectUrl = "";
            if (tabName == "PostFlight" || tabName=="main") {
                redirectUrl = "/Views/Transactions/PostFlight/PostFlightLegs.aspx?seltab=legs";
            } else if (tabName == "legs") {
                redirectUrl = "/Views/Transactions/PostFlight/PostFlightCrew.aspx?seltab=crew";
            } else if (tabName == "crew") {
                redirectUrl = "/Views/Transactions/PostFlight/PostFlightPAX.aspx?seltab=pax";
            } else if (tabName == "pax") {
                redirectUrl = "/Views/Transactions/PostFlight/PostflightExpenses.aspx?seltab=expenses";
            }

            var result = POMaintainModelStatusBeforePageLeave(redirectUrl, "next",null);
            if (result)
                window.location = redirectUrl;

        });



        if (tabName == "PostFlight" || tabName == "main") {
            $("#tbPostflight").addClass("tabSelected");
        }
        else if (tabName == "legs") {
            $("#tbLegs").addClass("tabSelected");
        }
        else if (tabName == "crew") {
            $("#tbCrew").addClass("tabSelected");            
        }
        else if (tabName == "pax") {
            $("#tbPAX").addClass("tabSelected");
        }
        else if (tabName == "expenses") {
            $("#tbExpenses").addClass("tabSelected");
            $("#btnNext").css("display", "none");
            $("#btnCopyExpense").css("display", "");
        }
        else if (tabName == "reports") {
            $("#tbReports").addClass("tabSelected");
        }

    });    
</script>
<table width="100%" cellspacing="0" cellpadding="0" class="padtop_10">
    <tr>
        <td>
            <telerik:RadPanelBar ID="pnlLogNotes" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                runat="server" CssClass="postflight-panel-bar">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="false" Text="Log Notes" Value="pnlItemLogNotes">
                        <ContentTemplate>
                            <table class="note-box" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="po_lognotes">
                                        <textarea id="tbLogNotes" style="width:98%;height:80px" data-bind="enable:POEditMode, textInput: Postflight.PostflightMain.Notes" ></textarea>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
        </td>
    </tr>
</table>
<table width="100%" class="panel-border padtop_10 padleft_10 padright_10 padbottom_10" cellspacing="0" cellpadding="0">
    <tr>
        <td align="right">
            <input type="button" id="btnCopyExpense" value="Copy Expense" title="Copy Selected Record" style="display:none;" data-bind="event: { click: btnCopyExpense_Click }, css: CopyExpenseBtn" />
            <input type="button"  id="btnDelete" title="Delete Selected Record" value="Delete Log"  data-bind="visible: self.UserPrincipal.DeletePOLogManager, PostflightFooter: POLogID(), mode: POEditMode(), btn: 'Delete', event: { click: btnDeleteLogClick }"/>
            <input type="button" id="btnEditFlightLog"  value="Edit Flight Log" data-bind="visible: self.UserPrincipal.EditPOLogManager, PostflightFooter: POLogID(), mode: POEditMode(), btn: 'Edit', event: { click: btnEditLogClick }" />
            <input type="button" id="btnSave" title="Save Changes"  value="Save" data-bind="visible: self.UserPrincipal.AddPOLogManager, PostflightFooter: POLogID(), mode: POEditMode(), btn: 'Save', event: { click: self.SavePostflightLog_Click }"  />
            <input type="button"  id="btnCancel" title="Cancel All Changes" value="Cancel" data-bind="visible: self.UserPrincipal.AddPOLogManager, PostflightFooter: POLogID(), mode: POEditMode(), btn: 'Cancel', click: btnCancel_Click" />
            <input type="button" id="btnNext" title="Next" value='Next >' class="button"/>
        </td>
    </tr>
</table>
