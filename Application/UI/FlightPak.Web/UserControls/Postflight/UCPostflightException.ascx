﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPostflightException.ascx.cs"  %>

<style>
#gview_Exception .ui-widget-content {
    font-size: 11px;
    line-height: 14px;
    word-wrap: break-word;
    font-weight: normal;
    overflow: hidden;
    white-space: pre-wrap;
    height: 22px;
    padding: 0 2px 0 2px;
    border-bottom-width: 1px;
    border-bottom-color: inherit;
    border-bottom-style: solid;
}
#gview_Exception .ui-jqgrid-hdiv {
    display: none !important;
}

.Exceptionghead_0{
    background: #dbdfe4;
    font-size: 0.9em;
    line-height: 21px;
    color: #3b3b3b;
}
#Exception .ui-widget-content {
    border: 1px solid #DBDFE4;
    color: #222222;
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    -ms-border-radius: 0;
    border-radius: 0;
}
</style>
<table id="Exception"></table>