﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPostflightSearch.ascx.cs" Inherits="FlightPak.Web.UserControls.Postflight.UCPostflightSearch" %>
<%@ Register TagPrefix="uc" TagName="Help" Src="~/UserControls/UCPOHelpIcons.ascx" %>
<script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/jquery.cookie.js") %>"></script>
<table width="100%" cellpadding="0" cellspacing="0" >
                <tr style="width: 10%;">
                    <td>
                        <div id="tdSuccessMessage" style="width:23%" class="success_msg">
                            Log saved successfully.
                        </div>
                        <div id="tdErrorMessage" style="width:23%" class="error_msg">
                        </div>
                    </td>
                </tr>
                <tr style="width: 70%; float: right">
                    <td align="right" valign="bottom" class="tdLabel300">
                        <label id="lbLastModifiedDate" data-bind="text: Postflight.PostflightMain.LastUpdatedBy" class="acnt_infoTopresize"></label>
                    </td>

                    <td style="float: right">
                        <UC:Help ID="ucHelp" runat="server"></UC:Help>
                    </td>
                    <td>
                        <div class="mandatory subfix" style="float: right; margin-top: 0px">
                            <span>Bold</span> indicates required field
                        </div>
                    </td>

                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="left">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" width="37%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="global-search">
                                                                <span>
                                                                    <input type="button" id="btnSearch" class="searchsubmitbutton"  data-bind="event: { click: btnSearchClick }" />
                                                                    <input type="text" id="tbSearchBox" class="SearchBox_sc_crew" data-bind="value: LogSearch, event: { keypress: onSearchEnter }" />
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:LinkButton ID="lnkSeeAll" OnClientClick="javascript:openPostflightWinShared('RadSearchPopup');return false;"
                                                                runat="server" CssClass="link_small">See All Logs</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" width="47%">
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel120">
                                                            <span class="mnd_text">Log No.</span>
                                                            <label id="lbTripNo" data-bind="text: Postflight.PostflightMain.LogNum" />
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <span class="mnd_text">Tail No.</span>
                                                            <label id="lbTailNo" data-bind="text: Postflight.PostflightMain.Fleet.TailNum" />
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <span class="mnd_text">Type </span>
                                                            <label id="lbTypeCode" data-bind="text: Postflight.PostflightMain.Aircraft.AircraftCD" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="right" width="16%" style="display:none;" id="tdIsCompleteBox">
                                                <input type="checkbox" id="chkCompleted" title="Completed" data-bind="enable: POEditMode,checked: Postflight.PostflightMain.IsCompleted " />
                                                <label for="chkCompleted">Completed</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                <label id="lbSearchMsg" class="alert-text"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            