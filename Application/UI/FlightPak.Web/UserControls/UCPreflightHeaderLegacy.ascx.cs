﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;

namespace FlightPak.Web.UserControls
{
    public partial class UCPreflightHeaderLegacy : System.Web.UI.UserControl
    {
        //Replicate Save Cancel Delete Buttons in header
        public delegate void ButtonClick(object sender, EventArgs e);

        private Delegate _SaveToSession;
        public Delegate SaveToSession
        {
            set { _SaveToSession = value; }
        }



        private Delegate _SaveToSessionPAXCrew;
        public Delegate SaveToSessionPAXCrew
        {
            set { _SaveToSessionPAXCrew = value; }
        }


        //Replicate Save Cancel Delete Buttons in header
        public event ButtonClick SaveClick;
        public event ButtonClick DeleteClick;
        public event ButtonClick CancelClick;


        public Button NewTripButton { get { return this.btnNewTrip; } }
        public Button EditTripButton { get { return this.btnEditTrip; } }
        //Replicate Save Cancel Delete Buttons in header
        public Button DeleteTripButton { get { return this.btnDeleteTrip; } }
        public Button CancelTripButton { get { return this.btnCancel; } }
        public Button SaveTripButton { get { return this.btnSave; } }

        public Button MoveTripButton { get { return this.btnMoveTrip; } }
        public Button LogButton { get { return this.btnLog; } }
        public Button TravelsenseButton { get { return this.btnTravelSense; } }
        public Button CopyTripButton { get { return this.btnCopyTrip; } }
        public Button APISTripButton { get { return this.btnApis; } }
        public Button SIFLTripButton { get { return this.btnSifl; } }
        public Button EmailButton { get { return this.btnEmail; } } // PROD-38
        public RadTabStrip HeaderTab { get { return this.rtPreFlight; } }
        public Label HeaderMessage
        {
            get { return this.lbMessage; }
        }

        private Delegate _NewClick;
        public Delegate NewClick
        {
            set { _NewClick = value; }
        }

        private Delegate _EditClick;
        public Delegate EditClick
        {
            set { _EditClick = value; }
        }


        private Delegate _LogClick;
        public Delegate LogClick
        {
            set { _LogClick = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["CurrentPreFlightTrip"] != null)
            //{
            //    PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

            //    if (Trip.Mode == TripActionMode.NoChange)
            //    {
            //        btnNewTrip.Enabled = true;
            //        btnMoveTrip.Enabled = true;
            //        btnLog.Enabled = true;
            //        btnCopyTrip.Enabled = true;
            //        btnApis.Enabled = true;
            //        btnSifl.Enabled = true;
            //    }
            //    else if (Trip.Mode == TripActionMode.Edit)
            //    {
            //        btnNewTrip.Enabled = false;
            //        btnMoveTrip.Enabled = false;
            //        btnLog.Enabled = false;
            //        btnCopyTrip.Enabled = false;
            //        btnApis.Enabled = false;
            //        btnSifl.Enabled = false;
            //    }
            //}

            if (!IsPostBack)
                if (Session["CurrentPreFlightTrip"] == null && ((rtPreFlight.SelectedTab.Text != "Preflight") || (Request.QueryString["seltab"] != null && Request.QueryString["seltab"].ToLower() !="preflight")))
                {
                      if ((Request.QueryString["seltab"] != null && Request.QueryString["seltab"].ToLower() == "reports"))
                    {
                        rtPreFlight.FindTabByText("Reports").Selected = true;
                    }
                    else
                    {
                        rtPreFlight.FindTabByText("Preflight").Selected = true;
                        Response.Redirect(rtPreFlight.FindTabByText("Preflight").Value, true);
                    }
                }
        }

        protected void btNewTrip_Click(object sender, EventArgs e)
        {
            if (Session["AvaialbleCrew"] != null)
                Session.Remove("AvaialbleCrew");
            if (Session["tmpTable"] != null)
                Session.Remove("tmpTable");
            if (Session["Hotel"] != null)
                Session.Remove("Hotel");
            if (Session["AvailablePaxList"] != null)
                Session.Remove("AvailablePaxList");
            if (Session["PaxSummaryList"] != null)
                Session.Remove("PaxSummaryList");
            if (Session["PaxHotel"] != null)
                Session.Remove("PaxHotel");
            _NewClick.DynamicInvoke();
        }

        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            //Replicate Save Cancel Delete Buttons in header
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnDeleteTrip.Enabled = false;


            btnSave.CssClass = "ui_nav";
            btnCancel.CssClass = "ui_nav";
            btnDeleteTrip.CssClass = "ui_nav_disable";

            _EditClick.DynamicInvoke();
        }

        //Replicate Save Cancel Delete Buttons in header
        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (SaveClick != null)
                {
                    SaveClick(sender, e);
                }
            }

        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
            {
                CancelClick(sender, e);
            }
        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (DeleteClick != null)
                {
                    DeleteClick(sender, e);
                }
            }
        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            Session.Remove("PAXnotAssigned");
            Session.Remove("CrewnotAssigned");
            Session.Remove("HotelnotSaved");
            Session.Remove("PAXHotelnotSaved");
            if (_SaveToSessionPAXCrew != null)
                _SaveToSessionPAXCrew.DynamicInvoke();

            if (Session["AvailablePaxList"] != null)
                Session.Remove("AvailablePaxList");
            if (Session["PaxSummaryList"] != null)
                Session.Remove("PaxSummaryList");
            if (Session["PaxHotel"] != null)
                Session.Remove("PaxHotel");

            if (Session["AvaialbleCrew"] != null)
                Session.Remove("AvaialbleCrew");
            if (Session["tmpTable"] != null)
                Session.Remove("tmpTable");
            if (Session["Hotel"] != null)
                Session.Remove("Hotel");
            if (Session[FlightPak.Web.Framework.Constants.WebSessionKeys.CrewInSelection] != null)
                Session.Remove(FlightPak.Web.Framework.Constants.WebSessionKeys.CrewInSelection);
            if (Session[FlightPak.Web.Framework.Constants.WebSessionKeys.CrewInSummery] != null)
                Session.Remove(FlightPak.Web.Framework.Constants.WebSessionKeys.CrewInSummery);

            redirecttotab(Session["PageToNavigate"].ToString());
        }
        protected void No_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }



        protected void btLog_Click(object sender, EventArgs e)
        {
            _LogClick.DynamicInvoke();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (Request.QueryString["seltab"] != null)
            {
                string selectedItemValue = Request.QueryString["seltab"];
                RadTab selectedTab = new RadTab();
                if (selectedItemValue.ToLower() == "pax")
                    selectedItemValue = "PAX";

                if (selectedItemValue == "UWA")
                    selectedTab = rtPreFlight.FindTabByText("UWA Services");

                else if (selectedItemValue == "CR")
                    selectedTab = rtPreFlight.FindTabByText("Corporate Request");
                else if (selectedItemValue == "TRW")
                    selectedTab = rtPreFlight.FindTabByText("Tripsheet Report Writer");
                else
                    selectedTab = rtPreFlight.FindTabByText(Microsoft.Security.Application.Encoder.HtmlEncode(selectedItemValue));

                if (selectedTab != null)
                {
                    selectedTab.Selected = true;
                    while ((selectedTab != null) &&
                           (selectedTab.Parent.GetType() == typeof(RadTab)))
                    {
                        selectedTab = (RadTab)selectedTab.Parent;
                        selectedTab.Selected = true;
                    }
                }
            }

        }

      protected void redirecttotab(string selectedtab)
        {
            PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            var page = this.Page as BasePage;
            if (Trip != null)
            {
                page.RedirectToPage(Microsoft.Security.Application.Encoder.HtmlEncode(selectedtab));
            }
             else if (selectedtab.ToLower().Contains("=reports"))
            {
                page.RedirectToPage(Microsoft.Security.Application.Encoder.HtmlEncode(selectedtab));
            }
            else
            {
                rtPreFlight.FindTabByText("Preflight").Selected = true;
                page.RedirectToPage(Microsoft.Security.Application.Encoder.HtmlEncode(selectedtab));
            }
        }

        protected void rtPreFlight_Clicked(object sender, RadTabStripEventArgs e)
        {
            Session["PageToNavigate"] = e.Tab.Value;
            if (_SaveToSession != null)
                _SaveToSession.DynamicInvoke();

            if (Session["CurrentPreFlightTrip"] != null)
            {
                PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                if (Trip.Mode == TripActionMode.Edit)
                {
                    FlightPak.Web.Framework.Masters.PreflightLegacy MasterClass = (FlightPak.Web.Framework.Masters.PreflightLegacy)Page.Master;
                    MasterClass.ValidationTrip(PreflightService.Group.Main);
                }
            }

            if (Session["PAXnotAssigned"] != null || Session["CrewnotAssigned"] != null || Session["HotelnotSaved"] != null || Session["PAXHotelnotSaved"]!=null)
            {
                if (Session["PAXnotAssigned"] != null)
                {
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('" + Session["PAXnotAssigned"] + "',TripPAXCREWNotAssignedCallBackFn, 330, 110,null,'Confirmation!');");
                    Session.Remove("PAXnotAssigned");
                }
                if (Session["CrewnotAssigned"] != null)
                {
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('" + Session["CrewnotAssigned"] + "',TripPAXCREWNotAssignedCallBackFn, 330, 110,null,'Confirmation!');");
                    Session.Remove("CrewnotAssigned");
                }

                if (Session["HotelnotSaved"] != null)
                {
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Please save or cancel hotel information', 330, 110,'Preflight',alertCallBackfn);");
                    Session.Remove("HotelnotSaved");
                }
                if (Session["PAXHotelnotSaved"] != null)
                {
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Please save or cancel hotel information', 330, 110,'Preflight',alertCallBackfn);");
                    Session.Remove("PAXHotelnotSaved");
                }


            }
            else
            {
                if (_SaveToSessionPAXCrew != null)
                    _SaveToSessionPAXCrew.DynamicInvoke();



                redirecttotab(e.Tab.Value);

            }
        }
    }
}
