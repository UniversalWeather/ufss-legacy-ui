﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreflightHeader.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCPreflightHeader" %>
<script type="text/javascript">
   

    $(function () {
       
        var tabName = getQuerystring("seltab", "PreFlight");

        if (tabName == "PreFlight") {
            $("#tbPreflight").addClass("tabSelected");
        }
        else if (tabName == "Legs") {
            $("#tbLegs").addClass("tabSelected");
        }
        else if (tabName == "Crew") {
            $("#tbCrew").addClass("tabSelected");
            var subTabNameHotel = getQuerystring("subTab", "PreFlight");
            if (subTabNameHotel == "1") {
                crewTabModuleChange('2');
            }
        }
        else if (tabName == "Pax") {
            $("#tbPax").addClass("tabSelected");
            var subTabNameHotel = getQuerystring("subTab", "PreFlight");
            if (subTabNameHotel == "1") {
                $("#ulPaxLogistics").click();
            }
        }
        else if (tabName == "Logistics") {
            $("#tbLogistics").addClass("tabSelected");
            $("#btnNext").addClass("button-disable");
            $("#btnNext").removeClass("button");
            $("#btnNext").prop("disabled", true);

            var reqLegNum = getQuerystring("legNo", "Preflight");
            if (reqLegNum != "Preflight")
            {
                setTimeout(function () { $("#legTabLogistics li:eq(" + (reqLegNum - 1) + ")").click(); }, 300);
            }
            

        }
        else if (tabName == "Reports") {
            $("#tbReports").addClass("tabSelected");
        }

    });

    function PreflightModuleTabChange(ctrl) {
        var result = MaintainModelStatusBeforePageLeave(ctrl,"redirect");
        return result;
    }
</script>
<table id="PreflightHeader" name="PreflightHeader" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <input id="btnNewTrip" type="button" value="New Trip" data-bind="visible: self.UserPrincipal.AllowAddTripManager, PreflightHeader: TripID(), mode: EditMode(), btn: 'New', click: btNewTrip_Click" class="ui_nav_disable" disabled="disabled" />
            <input id="btnDeleteTrip" type="button" value="Delete Trip" data-bind="visible: self.UserPrincipal.AllowDeleteTripManager, PreflightHeader: TripID(), mode: EditMode(), btn: 'Delete', click: btnDelete_Click" class="ui_nav_disable" disabled="disabled" />
            <input id="btnEditTrip" type="button" value="Edit Trip" data-bind="visible: self.UserPrincipal.AllowEditTripManager, PreflightHeader: TripID(), mode: EditMode(), btn: 'Edit', click: btnEdit_Click" class="ui_nav_disable" disabled="disabled" />
            <input id="btnCancel" type="button" value="Cancel" data-bind="PreflightHeader: TripID(), mode: EditMode(), btn: 'Cancel', click: btnCancel_Click" class="ui_nav_disable" disabled="disabled" />
            <input id="btnSave" type="button" value="Save" data-bind="PreflightHeader: TripID(), mode: EditMode(), btn: 'Save', click: btnSave_Click" class="ui_nav_disable" disabled="disabled" />
            <input id="btnCopyTrip" type="button" value="Copy Trip" data-bind="PreflightHeader: TripID(), mode: EditMode(), btn: 'Copy'" onclick="javascript: openWinShared('rdCopyTrip')" class="ui_nav_disable" disabled="disabled" />
            <input id="btnMoveTrip" type="button" value="Move Leg" data-bind="PreflightHeader: TripID(), mode: EditMode(), btn: 'Move'"  onclick="    javascript: openWinShared('rdMoveTrip')" class="ui_nav_disable" disabled="disabled" />
        </td>
        <td align="center">
            <asp:Label ID="lbMessage" Text="" ForeColor="red"></asp:Label>
        </td>
        <td align="right">
            <input type="button" id="btnLog" value="Log" data-bind="visible: self.UserPrincipal.AddTripLog, PreflightHeader: TripID(), mode: EditMode(), btn: 'Log', click: btLogTrip_Click" class="ui_nav_disable" disabled="disabled" />
            <input type="button" id="btnSifl" value="SIFL" data-bind="visible: self.UserPrincipal.AddTripSIFL, PreflightHeader: TripID(), mode: EditMode(), btn: 'SIFL'" onclick="    javascript: openWinShared('rdSIFL'); return false;" class="ui_nav_disable" disabled="disabled" />
            <input type="button" id="btnEmail" value="E-mail" data-bind="PreflightHeader: TripID(), mode: EditMode(), btn: 'Email'" onclick="    javascript: openWinShared('rdPreflightEMAIL'); return false;" class="ui_nav_disable" disabled="disabled" />
        </td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <div class="UI-panel-bar">

                            <div id="ctl00_ctl00_MainContent_PreflightHeader_rtPreFlight" class="UI-bar UI-bar-Simple UI-bar-top-Simple">
                                <div class="tabLevel tabLevel1">
                            <ul class="tab-layout">
                                        <li  class="tab-content"  style="width: 67px;"><a onclick="return PreflightModuleTabChange(this);" id="tbPreflight" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightMain.aspx?seltab=PreFlight"><span class="tabOut"><span class="tabIn"><span class="tabTxt"  data-bind="style: { color: Preflight.PreflightMain.Notes() != '' && Preflight.PreflightMain.Notes() != null ? 'red' : '' }">Preflight</span></span></span></a></li>
                                        <li  class="tab-content" style="width: 55px;"><a onclick="return PreflightModuleTabChange(this);" id="tbLegs" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightLegs.aspx?seltab=Legs"><span class="tabOut"><span class="tabIn"><span class="tabTxt">Legs</span></span></span></a></li>
                                        <li  class="tab-content" style="width: 57px;"><a onclick="return PreflightModuleTabChange(this);" id="tbCrew" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightCrewInfo.aspx?seltab=Crew"><span class="tabOut"><span class="tabIn"><span class="tabTxt">Crew</span></span></span></a></li>
                                        <li  class="tab-content" style="width: 52px;"><a onclick="return PreflightModuleTabChange(this);" id="tbPax" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightPax.aspx?seltab=Pax"><span class="tabOut"><span class="tabIn"><span class="tabTxt">PAX</span></span></span></a></li>
                                        <li  class="tab-content" style="width: 76px;"><a onclick="return PreflightModuleTabChange(this);" id="tbLogistics" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightLogistics.aspx?seltab=Logistics"><span class="tabOut"><span class="tabIn"><span class="tabTxt">Logistics</span></span></span></a></li>
                                        <li  class="tab-content" style="width: 71px;"><a onclick="return PreflightModuleTabChange(this);" id="tbReports" class="panel-tab tabAfter" href="/views/Transactions/Preflight/PreflightReports.aspx?seltab=Reports"><span class="tabOut"><span class="tabIn"><span class="tabTxt">Reports</span></span></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="tblHidden" style="display: none;">
    <tr>
        <td>
            <asp:Button ID="btnYes" Text="Button"  />
            <asp:Button ID="btnNo" Text="Button" />
        </td>
    </tr>
</table>
