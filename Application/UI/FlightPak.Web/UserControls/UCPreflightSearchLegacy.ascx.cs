﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.PreflightService;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;

namespace FlightPak.Web.UserControls
{
    public partial class UCPreflightSearchLegacy : System.Web.UI.UserControl
    {
        #region comments

        //Defect: 3053. Fixed by  Prabhu
        //The system shall have the ability to provide a tool tip for the Revision description icon (hover) that displays the contents rather than Revision Description (current functionality).


        #endregion
        private delegate void SeeAllPreflightListSaveClick();
        private delegate void SeeAllPreflightListCancelClick();

        private Delegate _SelectTrip;
        public Delegate SelectTrip
        {
            set { _SelectTrip = value; }
        }



        private Delegate _EditClick;
        public Delegate EditClick
        {
            set { _EditClick = value; }
        }


        public decimal? RevisionNumber
        {
            set { lbRevNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public Int64 TripID
        {
            get { return Convert.ToInt64(lblTripNo.Text); }
            set { lblTripNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public Int64 TripNUM
        {
            get { return Convert.ToInt64(lblTripNumber.Text); }
            set { lblTripNumber.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }
        public string TailNo
        {
            get { return lblTailNo.Text; }
            set { lblTailNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public string TypeCode
        {
            get { return lblTypeCode.Text; }
            set { lblTypeCode.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        //public LinkButton EditButton
        //{
        //    get { return this.lkEdit; }
        //}

        public string RevisionDescription
        {
            get { return tbDescription.Text; }
            set { tbDescription.Text = value; }
        }

        public Label LastModified
        {
            get { return this.lbLastModified; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SeeAllPreflightListSaveClick seeallsave = new SeeAllPreflightListSaveClick(saveclick);
            //this.SeeAllPreflightMainList.SelectClick = seeallsave;

            SeeAllPreflightListCancelClick seeallcancel = new SeeAllPreflightListCancelClick(cancelclick);
            //this.SeeAllPreflightMainList.CancelClick = seeallcancel;
            //lblLastModified.Text = "Last Modified : " + DateTime.Now.ToLongDateString().ToString();
            //PreflightMain Trip = new PreflightMain();
            //if (Trip.State == TripEntityState.Added)
            //    lblLastModified.Text = "";//"Last Modified : " + DateTime.Now.ToLongDateString().ToString();
            //else if (Trip.State == TripEntityState.Modified)
            //{
            //    foreach (var leg in Trip.PreflightLegs.ToList())
            //    {
            //        lblLastModified.Text = leg.LastUpdTS.ToString();
            //    }
            //    lblLastModified.Text = "Last Modified : " + DateTime.Now.ToLongDateString().ToString();
            //}

            if (!IsPostBack)
            {
                lbTripSearchMessage.Text = string.Empty;
            }

        }



        protected void page_prerender(object sender, EventArgs e)
        {
            PreflightMain Trip = new PreflightMain();

            if (Session["CurrentPreFlightTrip"] != null)
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                if (Trip != null && Trip.TripID != null)
                    hdnTripID.Value = Trip.TripID.ToString();
                else
                    hdnTripID.Value = string.Empty;
                //fix for  859
                if (Trip.IsTripCopied != null && Trip.IsTripCopied == true)
                {
                    lblTripNumber.Attributes.Add("Style", "color:Orange");
                    lblTripNumber.ToolTip = "Copied Trip";
                }
                else
                {
                    lblTripNumber.ToolTip = string.Empty;
                }
                lnkHistory.Enabled = true;
                lnkHistory.CssClass = "history-icon";
                lnkHistory.OnClientClick = "javascript:openWin('rdHistory');return false;";

                imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                //imgbtnDesc.Attributes.Clear();
                //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");


                tbDescription.Text = Trip.RevisionDescriptioin;
                if (Trip.Mode == TripActionMode.Edit)
                {
                    tbDescription.Enabled= true;
                    imgbtnDesc.Enabled = true;
                    imgbtnDesc.CssClass = "note-icon";
                    btnSaveDesc.Visible = true;
                    btnCancelDesc.Visible = true;
                }
                else
                {
                    tbDescription.Enabled= false;
                    //imgbtnDesc.Enabled = false;
                    btnCancelDesc.Visible = false;
                    btnSaveDesc.Visible = false;
                    imgbtnDesc.CssClass = "note-icon-disable";
                }
            }
            else
            {
                lnkHistory.Enabled = false;
                lnkHistory.OnClientClick = "";
                lnkHistory.CssClass = "history-icon-disable";
                //imgbtnDesc.Enabled = false;
                tbDescription.Enabled = false;
                btnCancelDesc.Visible = false;
                btnSaveDesc.Visible = false;
                imgbtnDesc.CssClass = "note-icon-disable";
                hdnTripID.Value = string.Empty;
            }
        }


        protected void btnOpenPopup_Click(object sender, EventArgs e)
        {
            radwindowPopup.VisibleOnPageLoad = true;
        }
        protected void btnSaveDesc_Click(object sender, EventArgs e)
        {
            if (Session["CurrentPreFlightTrip"] != null)
            {
                PreflightMain Trip = new PreflightMain();
                Trip.Mode = TripActionMode.Edit;
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                Trip.RevisionDescriptioin = tbDescription.Text;
                //Defect: 3053
                imgbtnDesc.ToolTip = tbDescription.Text;
                //imgbtnDesc.Attributes.Clear();
                //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");
                Session["CurrentPreFlightTrip"] = Trip;
            }
            radwindowPopup.VisibleOnPageLoad = false;
            imgbtnDesc.Focus();
        }

        protected void btnCancelDesc_Click(object sender, EventArgs e)
        {
            if (Session["CurrentPreFlightTrip"] != null)
            {
                PreflightMain Trip = new PreflightMain();
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                tbDescription.Text = Trip.RevisionDescriptioin;
                //Defect: 3053
                imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                //imgbtnDesc.Attributes.Clear();
                //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");
            }
            radwindowPopup.VisibleOnPageLoad = false;
            imgbtnDesc.Focus();
        }

        protected void btnOpenSeeAll_Click(object sender, EventArgs e)
        {
            //rdSeeAllPreflightMain.VisibleOnPageLoad = true;
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            lbTripSearchMessage.Text = string.Empty;
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                PreflightMain Trip = new PreflightMain();
                if (Session["CurrentPreFlightTrip"] != null)
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                if (Trip != null)
                {
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        //lbTripSearchMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip Unsaved, please Save or Cancel Trip', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                    }
                    else
                    {
                        Int64 SearchTripNum = 0;
                        if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                        {
                            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                            {
                                var objRetVal = PrefSvc.GetTripByTripNum(SearchTripNum);
                                if (objRetVal.ReturnFlag)
                                {
                                    Trip = objRetVal.EntityList[0];
                                    Session["CurrentPreFlightTrip"] = Trip;

                                    RevisionNumber = Trip.RevisionNUM;
                                    TripID = Trip.TripID;
                                    TripNUM = (long)Trip.TripNUM;
                                    RevisionDescription = Trip.RevisionDescriptioin;
                                    //Defect: 3053
                                    imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                                    //imgbtnDesc.Attributes.Clear();
                                    //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                                    //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");

                                    if (Trip.Fleet != null)
                                        TailNo = Trip.Fleet.TailNum;
                                    if (Trip.Aircraft != null)
                                        TypeCode = Trip.Aircraft.AircraftCD;

                                    var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                    if (ObjretVal.ReturnFlag)
                                    {
                                        Session["PreflightException"] = ObjretVal.EntityList;
                                    }
                                    else
                                        Session.Remove("PreflightException");
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");

                                    //Response.Redirect("PreflightMain.aspx?seltab=PreFlight");
                                }
                                else
                                {
                                    lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                                    //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                                }
                            }
                        }
                        else
                        {
                            lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                            //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                        }
                    }
                }
                else
                {
                    Int64 SearchTripNum = 0;
                    if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTripByTripNum(SearchTripNum);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];
                                RevisionNumber = Trip.RevisionNUM;
                                TripID = Trip.TripID;
                                TripNUM = (long)Trip.TripNUM;
                                RevisionDescription = Trip.RevisionDescriptioin;
                                //Defect: 3053
                                imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                                //imgbtnDesc.Attributes.Clear();
                                //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                                //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");

                                if (Trip.Fleet != null)
                                    TailNo = Trip.Fleet.TailNum;
                                if (Trip.Aircraft != null)
                                    TypeCode = Trip.Aircraft.AircraftCD;
                                Session["CurrentPreFlightTrip"] = Trip;
                                var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["PreflightException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session.Remove("PreflightException");

                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                //Response.Redirect("PreflightMain.aspx?seltab=PreFlight");
                            }
                            else
                            {
                                lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                                //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                            }
                        }
                    }
                }
            }
            else
            {
                lbTripSearchMessage.Text = string.Empty;
            }
        }

        protected void SearchBox_TextChanged(object sender, EventArgs e)
        {
            lbTripSearchMessage.Text = string.Empty;
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                PreflightMain Trip = new PreflightMain();
                if (Session["CurrentPreFlightTrip"] != null)
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                if (Trip != null)
                {
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        //lbTripSearchMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip Unsaved, please Save or Cancel Trip', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");

                    }
                    else
                    {
                        Int64 SearchTripNum = 0;
                        if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                        {
                            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                            {
                                var objRetVal = PrefSvc.GetTripByTripNum(SearchTripNum);
                                if (objRetVal.ReturnFlag)
                                {
                                    Trip = objRetVal.EntityList[0];
                                    RevisionNumber = Trip.RevisionNUM;
                                    TripID = Trip.TripID;
                                    TripNUM = (long)Trip.TripNUM;
                                    RevisionDescription = Trip.RevisionDescriptioin;
                                    //Defect: 3053
                                    imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                                    //imgbtnDesc.Attributes.Clear();
                                    //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                                    //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");
                                    if (Trip.Fleet != null)
                                        TailNo = Trip.Fleet.TailNum;
                                    if (Trip.Aircraft != null)
                                        TypeCode = Trip.Aircraft.AircraftCD;

                                    Session["CurrentPreFlightTrip"] = Trip;
                                    var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                    if (ObjretVal.ReturnFlag)
                                    {
                                        Session["PreflightException"] = ObjretVal.EntityList;
                                    }
                                    else
                                        Session.Remove("PreflightException");
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");

                                    //Response.Redirect("PreflightMain.aspx?seltab=PreFlight");
                                }
                                else
                                {
                                    lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                                    //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");

                                }
                            }
                        }
                        else
                        {
                            lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                            //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                        }
                    }
                }
                else
                {
                    Int64 SearchTripNum = 0;
                    if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTripByTripNum(SearchTripNum);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];
                                RevisionNumber = Trip.RevisionNUM;
                                TripID = Trip.TripID;
                                TripNUM = (long)Trip.TripNUM;
                                RevisionDescription = Trip.RevisionDescriptioin;
                                //Defect: 3053
                                imgbtnDesc.ToolTip = Trip.RevisionDescriptioin;
                                //imgbtnDesc.Attributes.Clear();
                                //imgbtnDesc.Attributes.Add("onmouseover", "showTooltip(this)");
                                //imgbtnDesc.Attributes.Add("onmouseout", "hideTooltip(this)");
                                if (Trip.Fleet != null)
                                    TailNo = Trip.Fleet.TailNum;
                                if (Trip.Aircraft != null)
                                    TypeCode = Trip.Aircraft.AircraftCD;
                                Session["CurrentPreFlightTrip"] = Trip;
                                var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["PreflightException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session.Remove("PreflightException");

                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                // Response.Redirect("PreflightMain.aspx?seltab=PreFlight");
                            }
                            else
                            {
                                lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                                //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");

                            }
                        }
                    }
                    else
                    {
                        lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Trip No Does Not Exist");
                        //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip No. Does Not Exist', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                    }
                }

            }
            else
            {
                lbTripSearchMessage.Text = string.Empty;
            }

        }

        protected void TripEdit_Click(object sender, EventArgs e)
        {
            //_EditClick.DynamicInvoke();


        }

        protected void saveclick()
        {
            // TripID = SeeAllPreflightMainList.TripID;
            //TripNUM = SeeAllPreflightMainList.TripNUM;
            //rdSeeAllPreflightMain.VisibleOnPageLoad = false;
            _SelectTrip.DynamicInvoke();
        }
        protected void cancelclick()
        {
            //rdSeeAllPreflightMain.VisibleOnPageLoad = false;
        }
    }
}