﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreflightSearch.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCPreflightSearch"%>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
      <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/jquery.cookie.js") %>"></script>
    <script type="text/javascript">
        
     function ShowSuccessMessage() {
         setTimeout(function () {
             $("#tdSuccessMessage").css("display", "inline");
             $('#tdSuccessMessage').delay(60000 / 6).fadeOut(60000);
         }, 2000)
     }
     function EnableDisableRevisionNo() {
         var tab = getQuerystring("seltab", "PreFlight");
         if (tab == "PreFlight" && self.EditMode() && !UserPrincipal._IsAutoRevisionNum)
            $("#lbRevNo").prop('readonly', false).removeClass("inpt_non_edit");
         else
             $("#lbRevNo").prop('readonly', true).addClass("inpt_non_edit");
     }
    
        function maxLengthPaste(field, maxChars) {
            event.returnValue = false;
            if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
                return false;
            }
            event.returnValue = true;
        }
        function validateMaxLength(field, maxChars) {
            if (field.value.length >= maxChars) {
                event.returnValue = false;
                return false;
            } else return true;
        }
        function openTripDescription() {
            var radwindow = $find('<%=radwindowPopup.ClientID %>');
            radwindow.show();
        }

        function closeTripDescription() {
            var radwindow = $find('<%=radwindowPopup.ClientID %>');
            radwindow.close();
        }
    </script>
</telerik:RadCodeBlock>
        <script type="text/javascript">
            function CloseAndRebindSelect() {
                window.location.reload();
            }
        </script>
<telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="240px"
    Width="380px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None"
    Title="Description">
    <ContentTemplate>
        <div style="margin: 10px">
            <table cellpadding="2px" cellspacing="2px">
                <tr>
                    <td align="left">
                        Description:
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <textarea id="tbDescription" class="textarea310x100" cols="20" data-bind="value: Preflight.PreflightMain.RevisionDescriptioin, enable: EditMode" rows="2" onkeypress="return validateMaxLength(this,'200');"></textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <input type="button" id="btnCancelDesc" onclick="closeTripDescription();" data-bind="visible: EditMode, css: EditMode() == true ? 'button' : 'button-disable'" value="Cancel"/>
                                </td>
                                <td>
                                    <input type="button" id="btnSaveDesc" data-bind="event: { click: btnTripDescOk_Click }, visible: EditMode, css: EditMode() == true ? 'button' : 'button-disable'"  value="Ok" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</telerik:RadWindow>
<table width="100%" id="PreflightSearch" name="PreflightSearch" cellpadding="0" cellspacing="0">
    <tr>
            <td colspan="3">
                <div id="tdSuccessMessage" class="success_msg">
                    Record saved successfully.
                </div>
            </td>
    </tr>
    <tr>       
        <td style="width:50%">
            <asp:Label ID="lbLastModified" runat="server" data-bind="text: Preflight.PreflightMain.LastUpdatedBy" Text="" CssClass="acnt_info"></asp:Label>
        </td>
            
        <td style="float:right">
            <table>
                <tr>
                    <td align="left" valign="bottom">
                        <div class="mandatory subfix" style="position:inherit;font:12px Arial,Helvetica,sans-serif !important">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                    <td align="right">
                        <span class="prfl-nav-icons preflight" style="left:0"><a href="../../Help/ViewHelp.aspx?Screen=Preflight"
                            class="help-icon" target="_blank" style="margin:0;" title="Help"></a>
                            <a href="javascript:void(0)" id="lnkHistory" title="History" style="margin:0;" onclick='javascript:openWinShared("rdHistory");return false;' data-bind="css: HistoryIcon"></a>
                            <a id="preflightTripViewerLink" href="#"
                                target="_blank" title="Preview Report" style="margin:0;" class="print_preview_icon"></a></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" class="tdLabel180">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <input type="text" id="SearchBox" class="SearchBox_sc_crew" data-bind="value: TripSearch" onKeyPress="return fnAllowNumeric(this, event)" maxlength="19" />
                                </td>
                                <td>
                                    <input type="button" id="sa" class="searchsubmitbutton"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tdLabel80" align="left">
                        &nbsp;<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClientClick="javascript:openWin('RadRetrievePopup');return false;"
                            Text="See All Trips" CssClass="link_small"></asp:LinkButton>
                    </td>
                    <td align="center">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblTripNo" Visible="false" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                                <td class="mnd_text" style="width:90px;" >
                                    Trip No.
                                    <asp:Label ID="lblTripNumber" data-bind="text:Preflight.PreflightMain.TripNUM,style: { color: Preflight.PreflightMain.IsTripCopied() == true ? 'Orange' : 'black' }" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnTripID" runat="server" />

                                </td>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td class="tdLabel110">
                                                <span class="mnd_text">Revision No.&nbsp;
                                                    <input type="text" id="lbRevNo" data-bind="value: Preflight.PreflightMain.RevisionNUM" style="width:18px;height:14px;" class="inpt_non_edit" readonly="readonly"/>
                                                    </span>
                                            </td>
                                            <td>
                                                <input type="button"  id="imgbtnDesc" onclick="openTripDescription();" data-bind="attr: { title: Preflight.PreflightMain.RevisionDescriptioin }, css: NoteIcon" /> 
                                            </td>
                                            <td class="tdLabel100" style="width:120px;">
                                                <span class="mnd_text">Tail No. <asp:Label ID="lblTailNo" runat="server" data-bind="text:Preflight.PreflightMain.Fleet.TailNum" Font-Bold="true" Text=""  ></asp:Label> </span>
                                            </td>
                                            <td class="tdLabel100" style="width:120px;">
                                                <span class="mnd_text"  >Type <asp:Label ID="lblTypeCode" runat="server" data-bind="text:Preflight.PreflightMain.Aircraft.AircraftCD" Font-Bold="true" Text="" ></asp:Label></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <label class="alert-text" id="lbTripSearchMessage" style="color:red"></label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
