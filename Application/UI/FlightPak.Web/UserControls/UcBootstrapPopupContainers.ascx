﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcBootstrapPopupContainers.ascx.cs" Inherits="FlightPak.Web.UserControls.UcBootstrapPopupContainers" %>
<!-- Modal FleetProfile-->
        <div class="modal fade" id="ModalFleetProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close ModalFleetProfile" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Fleet Profile</h4>
                    </div>
                    <div class="modal-body" id="BodyFleetProfile">
                    </div>
                </div>
            </div>
        </div>
<!-- Modal ClientCode-->
        <div class="modal fade" id="ModalClientCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button"  class="close ModalClientCode" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Client Code</h4>
                    </div>
                    <div class="modal-body" id="BodyClientCode">
                    </div>
                </div>
            </div>
        </div>
<!-- Modal FleetProfileCatalog-->
        <div class="modal fade" id="ModalFleetProfileCatalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button"  class="close ModalFleetProfileCatalog" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Universal Weather and Aviation</h4>
                    </div>
                    <div class="modal-body" id="BodyFleetProfileCatalog">
                    </div>
                </div>
            </div>
        </div>
<!-- Modal AircraftPopup-->
        <div class="modal fade" id="ModalAircraftPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button"  class="close ModalAircraftPopup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Aircraft Types</h4>
                    </div>
                    <div class="modal-body" id="BodyAircraftPopup">
                    </div>
                </div>
            </div>
        </div>
<!-- Modal Dispatcher-->
        <div class="modal fade" id="ModalDispatcher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close ModalDispatcher" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Dispatcher</h4>
                    </div>
                    <div class="modal-body" id="BodyDispatcher">
                    </div>
                </div>
            </div>
        </div>
<!-- Modal AircraftEmergency-->
        <div class="modal fade" id="ModalAircraftEmergency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close ModalAircraftEmergency" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Aircraft Emergency</h4>
                    </div>
                    <div class="modal-body" id="BodyAircraftEmergency">
                    </div>
                </div>
            </div>
        </div>

