﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreflightSearchLegacy.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCPreflightSearchLegacy"%>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function maxLengthPaste(field, maxChars) {
            event.returnValue = false;
            if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
                return false;
            }
            event.returnValue = true;
        }
        function validateMaxLength(field, maxChars) {
            if (field.value.length >= maxChars) {
                event.returnValue = false;
                return false;
            } else return true;
        }
    </script>
</telerik:RadCodeBlock>
                <script type="text/javascript">
                    function CloseAndRebindSelect() {
                        window.location.reload();
                    }

                    function showTooltip(control) {
                        if (control.title != "") {
                            var ttext = control.title;
                            var tt = document.createElement('SPAN');
                            var tnode = document.createTextNode(ttext);
                            tt.appendChild(tnode);
                            control.parentNode.insertBefore(tt, control.nextSibling);
                            tt.className = "tooltipCss";
                            control.title = "";
                        }
                    }

                    function hideTooltip(control) {
                        if (control.nextSibling.childNodes.length > 0) {
                            var ttext = control.nextSibling.childNodes[0].nodeValue;
                            control.parentNode.removeChild(control.nextSibling);
                            control.title = ttext;
                        }
                    }

                </script>
<telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="240px"
    Width="380px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None"
    Title="Description">
    <ContentTemplate>
        <div style="margin: 10px">
            <table cellpadding="2px" cellspacing="2px">
                <tr>
                    <td align="left">
                        Description:
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="tbDescription" runat="server" CssClass="textarea310x100" TextMode="MultiLine" MaxLength="200" onkeypress="return validateMaxLength(this,'200');"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnCancelDesc" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancelDesc_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveDesc" runat="server" Text="Ok" CssClass="button" OnClick="btnSaveDesc_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</telerik:RadWindow>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <asp:Label ID="lbLastModified" runat="server" Text="" CssClass="acnt_info"></asp:Label>
        </td>
        <td align="right">
            <table>
                <tr>
                    <td align="left" valign="bottom">
                        <div class="mandatory subfix">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                    <td align="right">
                        <span class="prfl-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=Preflight"
                            class="help-icon" target="_blank" title="Help"></a>
                            <asp:LinkButton CssClass="history-icon" ID="lnkHistory" runat="server" OnClientClick="javascript:openWin('rdHistory');return false;"
                                ToolTip="History"></asp:LinkButton>
                            <a href="<%=ResolveClientUrl("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PreTSTripSheetMain.xml" +"&tripnum=" + Microsoft.Security.Application.Encoder.HtmlEncode(lblTripNumber.Text) +"&tripid=" + Microsoft.Security.Application.Encoder.HtmlEncode(hdnTripID.Value))%>"
                                target="_blank" title="Preview Report" class="print_preview_icon"></a></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" class="tdLabel180">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" OnTextChanged="SearchBox_TextChanged"
                                        ID="SearchBox"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button runat="server" UseSubmitBehavior="true" CssClass="searchsubmitbutton"
                                        CausesValidation="false" OnClick="Search_Click" ID="sa" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tdLabel80" align="left">
                        <%--<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClick="btnOpenSeeAll_Click"
                            Text="See All" CssClass="link_small"></asp:LinkButton>--%>
                        &nbsp;<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClientClick="javascript:openWin('RadRetrievePopup');return false;"
                            Text="See All Trips" CssClass="link_small"></asp:LinkButton>
                    </td>
                    <td align="center">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblTripNo" Visible="false" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                                <td class="mnd_text">
                                    Trip No.
                                    <asp:Label ID="lblTripNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnTripID" runat="server" />
                                </td>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td class="tdLabel100">
                                                <span class="mnd_text">Revision No.&nbsp;<asp:Label ID="lbRevNo" MaxLength="200"
                                                    runat="server" Font-Bold="true"></asp:Label></span>
                                            </td>
                                            <td>
                                                <asp:Button ID="imgbtnDesc" onmouseover ="javascript:showTooltip(this);" onmouseout="javascript:hideTooltip(this);" runat="server"  OnClick="btnOpenPopup_Click" />
                                                <%--<asp:Button ID="btnDesc" runat="server" OnClick="btnOpenPopup_Click" CssClass="note-icon" />--%>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="tdLabel100">
                                                <span class="mnd_text">Tail No.
                                                <asp:Label ID="lblTailNo" runat="server" Font-Bold="true" Text=""></asp:Label> </span>
                                            </td>
                                            <td class="tdLabel100">
                                                <span class="mnd_text">Type Code
                                                <asp:Label ID="lblTypeCode" runat="server" Font-Bold="true" Text=""></asp:Label></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label CssClass="alert-text" runat="server" ID="lbTripSearchMessage" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
