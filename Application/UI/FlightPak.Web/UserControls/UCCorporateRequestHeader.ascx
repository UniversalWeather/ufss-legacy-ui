﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCorporateRequestHeader.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCCorporateRequestHeader" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
<script type="text/javascript">
    function TripPAXNotAssignedCallBackFn(arg) {
        if (arg == true) {
            document.getElementById('<%=btnYes.ClientID%>').click();
        }
        else {
            document.getElementById('<%=btnNo.ClientID%>').click;
        }
    }

</script>
<table width="100%" cellpadding="0" cellspacing="0"   id="tblHeader" runat ="server" visible = "true">
    <tr>
        <td align="left">
            <asp:Button ID="btnNewCR" runat="server" Text="New Request" OnClick="btnNewCR_Click"
                CssClass="ui_nav" />
            <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Request"
                OnClick="btnDelete_Click" CssClass="ui_nav" />
            <asp:Button ID="btnEditCR" runat="server" Text="Edit Request" OnClick="btnEditCR_Click"
                CssClass="ui_nav" />
            <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                OnClick="btnCancel_Click" CssClass="ui_nav" />
            <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                ValidationGroup="Save" CssClass="ui_nav" />
            <asp:Button ID="btnCopyCR" runat="server" Text="Copy Request" CssClass="ui_nav" OnClientClick="javascript:openWin('rdCopyTrip');return false;" />
        </td>
        <td align="center">
            <asp:Label runat="server" ID="lbMessage" Text="" ForeColor="red"></asp:Label>
        </td>
        <td align="right">
            <asp:Button ID="btnChangeQueue" runat="server" Text="Change Queue" OnClick="btChangeQueue_Click"
                CssClass="ui_nav" OnClientClick="javascript:openWin('rdChangeQueue');return false;" />
        </td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <telerik:RadTabStrip ID="rtCorpRequest" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                Align="Justify" OnTabClick="rtCorpRequest_Clicked">
                <Tabs>
                    <telerik:RadTab Value="CorporateRequestMain.aspx?seltab=CorporateRequest" Text="Request"
                        Selected="true">
                    </telerik:RadTab>
                    <telerik:RadTab Value="CorporateRequestLegs.aspx?seltab=Legs" Text="Legs">
                    </telerik:RadTab>
                    <telerik:RadTab Value="CorporateRequestPax.aspx?seltab=Pax" Text="PAX">
                    </telerik:RadTab>
                    <telerik:RadTab Value="CorporateRequestLogistics.aspx?seltab=Logistics" Text="Logistics">
                    </telerik:RadTab>
                    <telerik:RadTab Value="CorporateRequestReports.aspx?seltab=Reports" Text="Reports">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
        </td>
        <td align="right" valign="bottom" width="40%">
            <asp:Label ID="lblLastModified" runat="server" Text="" CssClass="acnt_info"></asp:Label>
        </td>
    </tr>
</table>
<table id="tblHidden" style="display: none;">
    <tr>
        <td>
            <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
            <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
        </td>
    </tr>
</table>
