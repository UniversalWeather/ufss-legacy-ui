﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PreflightService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar;
using FlightPak.Web.CalculationService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Tracing = FlightPak.Common.Tracing;
using System.Text;

namespace FlightPak.Web.UserControls
{
    public partial class UCFilterCriteria : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RestoreDefaultSettings();
            }
            if (Convert.ToString(hdnHomebase.Value).Split(',').Length > 1)
            {
                lbHomebase.Attributes.Add("style", "display:block;");
            }
            else
            {
                lbHomebase.Attributes.Add("style", "display:none;");
            }

        }

        /// <summary>
        /// When tab changes
        /// </summary>
        /// <param name="sender">Tab</param>
        /// <param name="e">args</param>
        protected void TabSchedulingCalendar_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            RestoreDefaultSettings();
        }

        protected void btnCopyYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (var client = new PreflightServiceClient())
                {
                    if (hdReset.Value == "UserDefault")
                    {

                        // get settings from database and update filterSettings option to user settings... and then save
                        Session["SCUserSettings"] = ScheduleCalendarBase.GetUserSettings();
                        var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];
                        var userSettings = client.RetrieveFilterSettings();
                        // For new user userSettings will be string.Empty
                        userSettings = GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, userSettings);
                        var settingsObj = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);
                        filterAndDisplaySettings.Filters = settingsObj.Filters;
                        client.SaveUserDefaultSettings(XMLSerializationHelper.Serialize(filterAndDisplaySettings));
                        Session["SCUserSettings"] = filterAndDisplaySettings;

                    }
                    else
                    {
                        string settings = string.Empty;
                        var existingUser = client.IsUserSettingsAvailable();
                        settings = client.RetrieveSystemDefaultSettings();
                        var settingsObjFromDb = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(settings);
                        PopulateFilters(CurrentAdvancedFilterTab.FilterCriteria, settings, true);

                        settings = GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, settings);
                        var settingsObjFromControls = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(settings);

                        settingsObjFromDb.Filters = settingsObjFromControls.Filters;
                        client.SaveUserDefaultSettings(XMLSerializationHelper.Serialize(settingsObjFromDb));
                        Session["SCUserSettings"] = settingsObjFromDb;

                    }
                }
            }
        }
        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }
        /// <summary>
        /// Saves default settings
        /// </summary>
        /// <param name="sender">save button</param>
        /// <param name="e">args</param>
        protected void SaveDefault_Click(object sender, EventArgs e)
        {
            if (ValidateValue())
            {
                hdReset.Value = "UserDefault";
                RadWindowManager1.RadConfirm("Set User Calendar Defaults?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
            }
        }


        /// <summary>
        /// Restores default settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RestoreSystemDefault_Click(object sender, EventArgs e)
        {
            if (ValidateValue())
            {
                hdReset.Value = "SystemDefault";
                RadWindowManager1.RadConfirm("Do you wish to set the Filter Criteria to the Initial System Default Values?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
            }
        }

        /// <summary>
        /// Encapsulates the data available from the controls into a single entity
        /// to be used in WCF service.
        /// </summary>
        /// <returns>Advanced filter settings</returns>
        public string GetFilterSettings(CurrentAdvancedFilterTab selectedTab, string userSettings)
        {
            var settings = new AdvancedFilterSettings();

            if (!string.IsNullOrWhiteSpace(userSettings)) // existing user
            {
                settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);
            }

            //Set Filter criteria

            if (settings.Filters == null)
                settings.Filters = new FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria();

            var filters = settings.Filters;
            SetFilterCriteria(ref filters);
            return XMLSerializationHelper.Serialize(settings);
        }


        /// <summary>
        /// Resores the default settings for the current user if there is settings specific to user
        /// else loads the system default settings.
        /// </summary>
        private void RestoreDefaultSettings()
        {
            using (var client = new PreflightServiceClient())
            {
                string settings = retriveSystemDefaultSettings;
                bool? existingUser = userSettingAvailable;
                if (!existingUser.HasValue)
                    existingUser = client.IsUserSettingsAvailable();
                if (existingUser.HasValue && existingUser.Value)
                {
                    // settings = client.RetrieveFilterSettings();
                    AdvancedFilterSettings settingsObj = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.AdvancedFilterSettings)Session["SCUserSettings"];
                    settings = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(settingsObj);
                    PopulateFilters(CurrentAdvancedFilterTab.FilterCriteria, settings, false);
                }
                else
                {
                    if (string.IsNullOrEmpty(settings))
                        settings = client.RetrieveSystemDefaultSettings();
                    PopulateFilters(CurrentAdvancedFilterTab.FilterCriteria, settings, true);
                }
            }
        }

        public bool? userSettingAvailable { get; set; }

        public string retriveSystemDefaultSettings { get; set; }

        /// <summary>
        /// Populates the controls with the value from the deserialized entity.
        /// </summary>
        /// <param name="settings">Advanced filter settings</param>
        public void PopulateFilters(CurrentAdvancedFilterTab selectedTab, string xmlSettings, bool isSystemDefault)
        {
            //Populates Filter criteria

            var settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(xmlSettings);

            if (selectedTab == CurrentAdvancedFilterTab.FilterCriteria)
            {
                switch (settings.Filters.TimeBase)
                {
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.Home:
                        radHome.Checked = true;
                        radLocal.Checked = false;
                        radUTC.Checked = false;
                        break;
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.Local:
                        radLocal.Checked = true;
                        radHome.Checked = false;
                        radUTC.Checked = false;
                        break;
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.UTC:
                        radUTC.Checked = true;
                        radLocal.Checked = false;
                        radHome.Checked = false;
                        break;
                    default: 
                        radLocal.Checked = true;
                        radHome.Checked = false;
                        radUTC.Checked = false;
                        break;
                }

                switch (settings.Filters.VendorsFilter)
                {
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.IncludeActiveVendors:
                        radIncludeActiveVendors.Checked = true;
                        break;
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.OnlyShowActiveVendors:
                        radOnlyShowActiveVendors.Checked = true;
                        break;
                    case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.ExcludeActiveVendors:
                        radExcludeActiveVendors.Checked = true;
                        break;
                }


                // if (isSystemDefault || Session["SCClientID"] != null) // if client is assinged to user, show default client code and make the control readonly
                if (isSystemDefault)
                {
                    if (Session["SCClientID"] != null)
                    {
                        Int64 clientID = (Int64)Session["SCClientID"];
                        string clientCD = (string)Session["SCClientCD"];
                        tbClientCode.Text = clientCD;
                        hdnClientCode.Value = clientID.ToString();
                        tbClientCode.ReadOnly = true;
                        btnClientCode.Visible = false;
                    }
                    else
                    {
                        tbClientCode.Text = string.Empty;
                        hdnClientCode.Value = null;
                        tbClientCode.ReadOnly = false;
                        btnClientCode.Visible = true;
                    }
                }
                else
                {
                    tbClientCode.Text = settings.Filters.Client;
                    hdnClientCode.Value = settings.Filters.ClientID;
                    tbClientCode.ReadOnly = false;
                    btnClientCode.Visible = true;
                }

                tbRequestor.Text = settings.Filters.Requestor;
                hdnRequestor.Value = settings.Filters.RequestorID;
                tbDepartment.Text = settings.Filters.Department;
                hdnDepartment.Value = settings.Filters.DepartmentID;
                tbFlightCategory.Text = settings.Filters.FlightCategory;
                hdnDepartment.Value = settings.Filters.FlightCategoryID;
                tbCrewDuty.Text = settings.Filters.CrewDuty;
                hdnCrewDuty.Value = settings.Filters.CrewDutyID;
                if (isSystemDefault)
                {

                    if (Session["SCIsHomeBaseDeActivated"] != null && (bool)Session["SCIsHomeBaseDeActivated"] == false &&
                                                        Session["SCHomeBaseID"] != null)// if homebaseDeactivation is not set and user has default homebase...
                    {
                        Int64 homeBaseID = (Int64)Session["SCHomeBaseID"];
                        string homeBaseCD = (string)Session["SCHomeBaseCD"];
                        tbHomebase.Text = homeBaseCD;
                        hdnHomebase.Value = homeBaseID.ToString();
                    }
                    else // clear homebase values when no, homebase deactivation is not enabled...
                    {
                        tbHomebase.Text = string.Empty;
                        hdnHomebase.Value = string.Empty;
                    }
                }
                else
                {
                    hdnHomebase.Value = settings.Filters.HomeBaseID;
                    //tbHomebase.Text = settings.Filters.HomeBase; 
                    tbHomebase.Text = string.Empty;
                    // TODO:optimise below code by get hombaseCode by input homebaseid

                    using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CompanyVal = CompanyService.GetCompanyMasterListInfo();
                        if (CompanyVal.EntityList.Count > 0 && !string.IsNullOrEmpty(hdnHomebase.Value))
                        {
                            string[] values = hdnHomebase.Value.Split(',');
                            string lastValue = values.Last();
                            tbHomebase.Text = CompanyVal.EntityList.Where(x => x.HomebaseID == Convert.ToInt64(lastValue)).Select(x => x.HomebaseCD).First();
                        }
                    }
                }

                if (settings.Filters.HomeBaseID != null)
                {
                    var homeBaseCount = settings.Filters.HomeBaseID.Split(',');
                    if (homeBaseCount.Count() > 1)
                    {
                        lbHomebase.Attributes.Add("style", "display:block;");
                    }
                    else
                    {
                        lbHomebase.Attributes.Add("style", "display:none;");
                    }
                }

                // lstHomeBase.Items.Add(new ListItem(settings.Filters.HomeBase)); //todo: get collection of strings from list box
                chkTrip.Checked = settings.Filters.Trip;
                chkHold.Checked = settings.Filters.Hold;
                chkWorkSheet.Checked = settings.Filters.WorkSheet;
                chkAllCrew.Checked = settings.Filters.AllCrew;
                chkSchedServ.Checked = settings.Filters.SchedServ;
                chkHomeBase.Checked = settings.Filters.HomeBaseOnly;
                chkCanceled.Checked = settings.Filters.Canceled;
                chkFixedWingCrew.Checked = settings.Filters.FixedWingCrewOnly;
                chkUnFulFilled.Checked = settings.Filters.UnFulfilled;
                chkRotaryWingCrew.Checked = settings.Filters.RotaryWingCrewCrewOnly;
                
            }
        }

        /// <summary>
        /// Sets the filter criteria.
        /// </summary>
        /// <param name="filter"></param>
        private void SetFilterCriteria(ref FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filter)
        {
            if (radHome.Checked)
                filter.TimeBase = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.Home;
            else if (radLocal.Checked)
                filter.TimeBase = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.Local;
            else if (radUTC.Checked)
                filter.TimeBase = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.UTC;
            else
                filter.TimeBase = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TimeBase.None;

            if (radIncludeActiveVendors.Checked)
                filter.VendorsFilter = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.IncludeActiveVendors;
            else if (radOnlyShowActiveVendors.Checked)
                filter.VendorsFilter = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.OnlyShowActiveVendors;
            else if (radExcludeActiveVendors.Checked)
                filter.VendorsFilter = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.ExcludeActiveVendors;
            else
                filter.VendorsFilter = FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.None;

            //if (Session["SCClientID"] != null)
            //{
            //    Int64 clientID = (Int64)Session["SCClientID"];
            //    string clientCD = (string)Session["SCClientCD"];
            //    filter.Client = !string.IsNullOrEmpty(clientCD)? clientCD.Trim() : string.Empty;
            //    filter.ClientID = clientID.ToString();
            //    hdnClientCode.Value = clientCD;
            //}
            //else
            //{
            filter.Client = tbClientCode.Text.Trim();
            filter.ClientID = !string.IsNullOrEmpty(filter.Client) ? hdnClientCode.Value.Trim() : null;

            // }


            filter.Requestor = tbRequestor.Text.Trim();
            filter.RequestorID = !string.IsNullOrEmpty(filter.Requestor) ? hdnRequestor.Value.Trim() : null;
            filter.Department = tbDepartment.Text.Trim();
            filter.DepartmentID = !string.IsNullOrEmpty(filter.Department) ? hdnDepartment.Value.Trim() : null;
            filter.FlightCategory = tbFlightCategory.Text.Trim();
            filter.FlightCategoryID = !string.IsNullOrEmpty(filter.FlightCategory) ? hdnFlightCategory.Value.Trim() : null;
            filter.CrewDuty = tbCrewDuty.Text.Trim();
            filter.CrewDutyID = !string.IsNullOrEmpty(filter.CrewDuty) ? hdnCrewDuty.Value.Trim() : null;

            //if (Session["SCIsHomeBaseDeActivated"] != null && (bool)Session["SCIsHomeBaseDeActivated"] == false &&
            //                                           Session["SCHomeBaseID"] != null)// if homebaseDeactivation is not set and user has default homebase...
            //{
            //    Int64 homeBaseID = (Int64)Session["SCHomeBaseID"];
            //    string homeBaseCD = (string)Session["SCHomeBaseCD"];
            //    filter.HomeBaseID = homeBaseID.ToString();
            //    filter.HomeBase = homeBaseCD; 
            //}
            //else
            //{
            filter.HomeBase = tbHomebase.Text; //todo:  collection of strings from list box
            if (string.IsNullOrEmpty(filter.HomeBase))
            {
                hdnHomebase.Value = string.Empty;
            }

            filter.HomeBaseID = hdnHomebase.Value;
            if (filter.HomeBaseID != null)
            {
                var homeBaseCount = filter.HomeBaseID.Split(',');
                if (homeBaseCount.Count() > 1)
                {
                    lbHomebase.Attributes.Add("style", "display:block;");
                }
                else
                {
                    lbHomebase.Attributes.Add("style", "display:none;");
                }
            }

            // }


            filter.Trip = chkTrip.Checked;
            filter.Hold = chkHold.Checked;
            filter.WorkSheet = chkWorkSheet.Checked;
            filter.AllCrew = chkAllCrew.Checked;
            filter.SchedServ = chkSchedServ.Checked;
            filter.HomeBaseOnly = chkHomeBase.Checked;
            filter.Canceled = chkCanceled.Checked;
            filter.FixedWingCrewOnly = chkFixedWingCrew.Checked;
            filter.UnFulfilled = chkUnFulFilled.Checked;
            filter.RotaryWingCrewCrewOnly = chkRotaryWingCrew.Checked;
            
        }




        ///// <summary>
        ///// for clearing the text of invalid input for popup inputs
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        protected void hdnok_Click(object sender, EventArgs e)
        {
            string controlName = hdnControl.Value;
            TextBox control = (TextBox)this.FindControl(controlName);
            control.Text = string.Empty;
            control.Focus();

            // ScriptManager.GetCurrent(Page).Focus();
            //ScriptManager.GetCurrent(Page).SetFocus(control.ClientID);
            //this.Page.SetFocus(control.ClientID);



            return;
        }

        public bool ValidateValue()
        {
            StringBuilder ValueInfo = new StringBuilder();
            string strNextLine = "<br/>";
            bool val = true;
            if (!string.IsNullOrEmpty(tbClientCode.Text.Trim()))
            {

                using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objScheduler.GetClientByClientCD(tbClientCode.Text.Trim()).EntityList;

                    if (objRetVal.Count() <= 0)
                    {
                        hdnControl.Value = tbClientCode.ClientID;
                        tbClientCode.Text = string.Empty;
                        ValueInfo.Append(" Invalid Client Code " + strNextLine);
                        hdnClientCode.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnClientCode.Value = ((FlightPak.Web.FlightPakMasterService.ClientByClientCDResult)objRetVal[0]).ClientID.ToString();
                    }
                }
            }
            else
            {
                hdnClientCode.Value = null;
            }
            if (!string.IsNullOrEmpty(tbRequestor.Text.Trim()))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objScheduler.GetPassengerRequestorByPassengerRequestorCD(tbRequestor.Text.Trim()).EntityList;

                    if (objRetVal.Count() <= 0)
                    {
                        if (val)
                        {
                            hdnControl.Value = tbRequestor.ClientID;

                        }
                        tbRequestor.Text = string.Empty;
                        ValueInfo.Append(" Invalid Requestor Code " + strNextLine);
                        hdnRequestor.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnRequestor.Value = ((FlightPak.Web.FlightPakMasterService.PassengerRequestorResult)objRetVal[0]).PassengerRequestorID.ToString();
                    }
                }
            }
            else
            {
                hdnRequestor.Value = null;
            }



            if (!string.IsNullOrEmpty(tbDepartment.Text.Trim()))
            {

                using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objScheduler.GetDepartmentByDepartmentCD(tbDepartment.Text.Trim()).EntityList;

                    if (objRetVal.Count() <= 0)
                    {
                        if (val)
                        {
                            hdnControl.Value = tbDepartment.ClientID;
                        }
                        tbDepartment.Text = string.Empty;
                        ValueInfo.Append(" Invalid Department Code " + strNextLine);
                        hdnDepartment.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnDepartment.Value = ((FlightPak.Web.FlightPakMasterService.GetDepartmentByDepartmentCDResult)objRetVal[0]).DepartmentID.ToString();
                    }
                }
            }
            else
            {
                hdnDepartment.Value = null;
            }


            if (!string.IsNullOrEmpty(tbFlightCategory.Text.Trim()))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objScheduler.GetFlightCategoryByCategoryCD(tbFlightCategory.Text.Trim()).EntityList;

                    if (objRetVal.Count() <= 0)
                    {
                        if (val)
                        {
                            hdnControl.Value = tbFlightCategory.ClientID;
                        }
                        tbFlightCategory.Text = string.Empty;
                        ValueInfo.Append(" Invalid Flight Category Code " + strNextLine);
                        hdnFlightCategory.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnFlightCategory.Value = ((FlightPak.Web.FlightPakMasterService.FlightCategoryByCategoryCDResult)objRetVal[0]).FlightCategoryID.ToString();
                    }
                }
            }
            else
            {
                hdnFlightCategory.Value = null;
            }


            if (!string.IsNullOrEmpty(tbCrewDuty.Text.Trim()))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.ToString().ToUpper().Trim().Equals(tbCrewDuty.Text.ToUpper().Trim())).ToList();

                    if (objRetVal.Count() <= 0)
                    {
                        if (val)
                        {
                            hdnControl.Value = tbCrewDuty.ClientID;
                        }
                        tbCrewDuty.Text = string.Empty;
                        ValueInfo.Append("Invalid Crew Duty Type Code " + strNextLine);
                        hdnCrewDuty.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnCrewDuty.Value = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyTypeID.ToString();
                    }
                }
            }
            else
            {
                hdnCrewDuty.Value = null;
            }

            if (!string.IsNullOrEmpty(tbHomebase.Text.Trim()))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();

                    if (objRetVal.Count() <= 0)
                    {
                        if (val)
                        {
                            hdnControl.Value = tbHomebase.ClientID;
                        }
                        tbHomebase.Text = string.Empty;
                        ValueInfo.Append("Invalid Home Base Code " + strNextLine);
                        hdnHomebase.Value = null;
                        val = false;
                    }
                    else
                    {
                        hdnHomebase.Value = hdnHomebase.Value;

                    }
                }
            }
            else
            {
                hdnHomebase.Value = null;
                lbHomebase.Attributes.Add("style", "display:none;");
            }

            if (!val)
            {
                string msgToDisplay = ValueInfo.ToString().Trim();
                RadWindowManager1.RadAlert(msgToDisplay, 330, 100, "System Messages", "Messageok");
            }
            return val;
        }
    }



}