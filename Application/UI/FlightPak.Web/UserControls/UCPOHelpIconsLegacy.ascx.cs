﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Base;
using FlightPak.Web.Framework.Prinicipal;

namespace FlightPak.Web.UserControls
{
    public partial class UCPOHelpIconsLegacy : BaseSecuredUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["POSTFLIGHTMAIN"] != null)
                {
                    lnkHistory.Enabled = true;
                    lnkHistory.CssClass = "history-icon";
                    lnkHistory.OnClientClick = "javascript:openWin('rdHistory');return false;";
                    lnkReport.Enabled = true;

                    //tbLogNum.Text = ((PostflightService.PostflightMain)Session["POSTFLIGHTMAIN"]).LogNum.Value.ToString();                    
                    if (((PostflightService.PostflightMain)Session["POSTFLIGHTMAIN"]).POLogID != null)
                        tbLogID.Text = ((PostflightService.PostflightMain)Session["POSTFLIGHTMAIN"]).POLogID.ToString();

                }
                else
                {
                    lnkHistory.Enabled = false;
                    lnkHistory.OnClientClick = "";
                    lnkHistory.CssClass = "history-icon-disable";
                    lnkReport.Enabled = false;
                }
            }
        }

        #region "Reports"
        /// <summary>
        /// To show reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbLogID.Text))
                {
                    Session["REPORT"] = "RptFlightLog";
                    Session["FORMAT"] = "PDF";
                    Session["TabSelect"] = "PostFlight";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";LogID=" + tbLogID.Text;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", String.Format("callToJSforReportDownload()"), true);
                }
            }

        }
        #endregion
    }
}