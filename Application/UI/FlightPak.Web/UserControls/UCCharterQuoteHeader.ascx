﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCharterQuoteHeader.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCCharterQuoteHeader" %>
<script type="text/javascript">
    function TripPAXNotAssignedCallBackFn(arg) {
        if (arg == true)
            document.getElementById('<%=btnYes.ClientID%>').click();
    }

</script>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="rtCharterQuote" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            OnTabClick="rtCharterQuote_Clicked" Align="Justify">
                            <Tabs>
                                <telerik:RadTab Text="File" Value="CharterQuoteRequest.aspx?seltab=File" Selected="true">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Quote On File" Value="QuoteOnFile.aspx?seltab=QuoteOnFile">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Quote & Leg Details" Value="QuoteAndLegDetails.aspx?seltab=QuoteAndLeg">
                                </telerik:RadTab>
                                <telerik:RadTab Text="PAX" Value="CharterQuotePax.aspx?seltab=PAX">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Logistics" Value="CharterQuoteLogistics.aspx?seltab=Logistics">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Reports" Value="QuoteReports.aspx?seltab=Reports">
                                </telerik:RadTab>
                                <telerik:RadTab Text="CQ Report Writer" Value="CQReportWriter.aspx?seltab=CQRW">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                    </td>
                    <%--<td align="right" valign="bottom" width="40%">
                        <asp:Label ID="lbLastModified" runat="server" Text="" CssClass="acnt_info"></asp:Label>
                    </td>--%>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="tblHidden" style="display: none;">
    <tr>
        <td>
            <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
        </td>
    </tr>
</table>
