﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCharterQuoteSearch.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCCharterQuoteSearch" %>
<script type="text/javascript">

    function alertCallBack(arg) {
        document.getElementById('<%=btnAlert.ClientID%>').click();
    }

    function lnkUniversalLocator_OnClientClick() {
        var url = '<% =ResolveClientUrl("~/Views/Utilities/UniversalLocator.aspx") %>';
        window.open(url, 'UniversalLocator', 'width=998,height=550,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=' + (screen.height - 550) / 2 + ',left=' + (screen.width - 998) / 2);
        return false;
    }

    function openWin() {
        var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "radCQSearch");
    }

    function ConfirmClose(WinName) {
        var oManager = GetRadWindowManager();
        var oWnd = oManager.GetWindowByName(WinName);
        //Find the Close button on the page and attach to the
        //onclick event
        var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
        CloseButton.onclick = function () {
            CurrentWinName = oWnd.Id;
            //radconfirm is non-blocking, so you will need to provide a callback function
            radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
        }
    }

    function OnCharterQuoteSearchAllClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=lnkSeeAll.ClientID%>").value = arg.ClientCD;
            }
            else {
                document.getElementById("<%=lnkSeeAll.ClientID%>").value = "";

            }
        }
    }
    function GetDimensions(sender, args) {
        var bounds = sender.getWindowBounds();
        return;
    }
    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        return oWindow;
    }

    function CloseAndRebindSelect() {
        window.location.reload();
    }
</script>
<div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
         <td style="width:33%">
             <asp:Label ID="lbLastModified" runat="server" Text=""  CssClass="acnt_info"></asp:Label>             
            </td>
            <td style="width:30%">
             <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>            
        <td align="right" style="float: right;">
            
                <table>
                    <tr>
                         <td align="left" valign="bottom">
                        <div class="mandatory subfix">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                        <td align="right">
                            <span class="chtrquote-nav-icons">
                                <asp:LinkButton ID="lnkUniversalLocator" runat="server" ToolTip="Universal Locator"
                                    Text="Universal Locator" OnClientClick="javascript:lnkUniversalLocator_OnClientClick();">
                                    <img src="<%=ResolveClientUrl("~/App_Themes/Default/images/locator_widget.png") %>" alt="Help" width="17" height="17" />
                                </asp:LinkButton>
                                <a href="../../Help/ViewHelp.aspx?Screen=Charter" class="help-icon" target="_blank"
                                    title="Help"></a>
                                <asp:LinkButton CssClass="history-icon" ID="lnkHistory" runat="server" OnClientClick="javascript:openWin('rdHistory');return false;"
                                    ToolTip="History"></asp:LinkButton>
                                <a href="<%=ResolveClientUrl("~/Views/Reports/CQReportViewer.aspx?xmlFilename=CQItinerary.xml" +"&filenum=" + Microsoft.Security.Application.Encoder.HtmlEncode(lbFileNumber.Text) +"&fileid="+ Microsoft.Security.Application.Encoder.HtmlEncode(hdnFileID.Value)+"&isexpressquote=false")%>"
                                    title="Charter Quote Report Writer" class="print_preview_icon"></a>
                                <asp:LinkButton ID="lnkInvoiceReport" runat="server" CssClass="print_preview_icon_CQ"
                                    ToolTip="Preview Report" OnClick="lnkInvoiceReport_Click" CausesValidation="false"></asp:LinkButton>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
          
    </tr>       
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" class="tdLabel180">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" OnTextChanged="Search_Click"
                                            ID="SearchBox"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" CssClass="searchsubmitbutton" CausesValidation="false"
                                            OnClick="Search_Click" ID="sa" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel100" align="left" valign="middle">
                            <%--<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClick="btnOpenSeeAll_Click"
                            Text="See All" CssClass="link_small"></asp:LinkButton>--%>
                            &nbsp;<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClientClick="javascript:OpenCQSearch();return false;"
                                Text="See All Files" CssClass="link_small"></asp:LinkButton>
                        </td>
                        <td valign="middle">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="mnd_text">
                                        File No.
                                        <asp:Label ID="lbFileNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                        <asp:HiddenField runat="server" ID="hdnFileID" />
                                    </td>
                                    <td class="mnd_text">
                                        Quote No.
                                        <asp:Label ID="lbQuoteNum" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    </td>
                                    <td class="mnd_text">
                                        Tail No.
                                        <asp:Label ID="lblTailNo" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    </td>
                                    <td class="mnd_text">
                                        Type Code
                                        <asp:Label ID="lblTypeCode" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lbSearchMsg" runat="server" CssClass="alert-text"></asp:Label>
                            <asp:HiddenField ID="hdnRptName" runat="server" />
                            <asp:HiddenField ID="hdnUser" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table id="tblHidden" style="display: none;">
        <tr>
            <td>
                <asp:Button ID="btnAlert" runat="server" Text="Button" OnClick="btnAlert_Click" />
            </td>
        </tr>
    </table>
</div>
