﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCSeeAllPostflightMain.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCSeeAllPostflightMain" %>
<%@ Reference VirtualPath="~/Framework/Masters/PostFlight.Master" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        var oArg = new Object();
        var grid = $find("<%= dgSearch.ClientID %>");

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function returnToParent() {
            //create the argument that will be returned to the parent page
            oArg = new Object();
            grid = $find("<%= dgSearch.ClientID %>");
            var MasterTable = grid.get_masterTableView();
            var selectedRows = MasterTable.get_selectedItems();
            var SelectedMain = false;
            for (var i = 0; i < selectedRows.length; i++) {
                var row = selectedRows[i];
                var cell1 = MasterTable.getCellByColumnUniqueName(row, "TripID");
                var cell2 = MasterTable.getCellByColumnUniqueName(row, "AuthorizationID");
            }

            if (selectedRows.length > 0) {
                oArg.TripID = cell1.innerHTML;
                oArg.AuthorizationID = cell2.innerHTML;
            }
            else {
                oArg.TripID = "";
                oArg.AuthorizationID = "";
            }


            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        function Close() {
            GetRadWindow().Close();
        }
    </script>
</telerik:RadCodeBlock>
<div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table>
        <tr>
            <td valign="top">
                <fieldset>
                    <legend>Search</legend>
                    <table width="100%">
                        <tr>
                            <td class="tdLabel120">
                                <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                            </td>
                            <td>
                                Client Code
                            </td>
                            <td class="tdLabel120">
                                <asp:TextBox ID="tbSearchClientCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnRequestor" OnClientClick="javascript:openWin('RadRequestorPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                            </td>
                            <td>
                                TailNo
                            </td>
                            <td class="tdLabel120">
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="Button1" OnClientClick="javascript:openWin('RadRequestorPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel120">
                                <asp:CheckBox ID="CheckBox1" Text="Personel Travel Only" runat="server" />
                            </td>
                            <td>
                                Strarting Depart Date
                            </td>
                            <td class="tdLabel120">
                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="Button2" OnClientClick="javascript:openWin('RadRequestorPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" Text="Search" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel120">
                                <asp:CheckBox ID="CheckBox2" Text="Completed" runat="server" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="preflight-custom-grid">
                <telerik:RadGrid ID="dgSearch" runat="server" AllowSorting="true" Visible="true"
                    OnNeedDataSource="dgPostflightMainDetails_BindData" OnSelectedIndexChanged="dgSearch_SeelctedIndexChanged"
                    AutoGenerateColumns="false" PageSize="10" AllowPaging="true" AllowFilteringByColumn="true"
                    PagerStyle-AlwaysVisible="true">
                    <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" DataKeyNames="POLogID">
                        <Columns>
                            <telerik:GridBoundColumn DataField="LogNum" HeaderText="LogNo" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="TripID" HeaderText="Trip No." CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DispatchNum" HeaderText="Dispatch" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequestorName" HeaderText="TailNbr" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Depart" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequestorNamer" HeaderText="Reqstr" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DepartureDescription" HeaderText="Dept" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AutherizationDescription" HeaderText="Auth" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DescriptionID" HeaderText="Description" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Excp" HeaderText="Excp" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="pers" HeaderText="pers" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="HomebaseID" HeaderText="Home Base" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FleetID" HeaderText="FltNo" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsCompleted" HeaderText="Completed" CurrentFilterFunction="Contains"
                                FilterDelay="4000" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; text-align: right; clear: both;">
                                <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                    Ok</button>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="false" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td>
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
</div>
