﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrewAvailabilityGrid.ascx.cs" Inherits="FlightPak.Web.UserControls.Preflight.CrewAvailabilityGrid" %>

<script type="text/javascript">
    var selectedRowData = null;
    var agt = navigator.userAgent.toLowerCase();
    var jqgridTableId = '#CrewsAvailabilityGridID';
    var jqgridCheckListTable = "#CheckListGridID";
    var hdClientCodeID = "";
    var hdIsTripPrivacy = "";
    var hdHomebaseID = "";
    var hdSelectedTripNUM = "";
    var hdHomeBase = "";
    var airport = "";
    var homeBase = "";
    var MultiSelectFlag = true;
    var CheckListMultiSelectFlag = false;
    var Filter = false;
    var GlobalIdentity;
    var selectedCrewsList = new Array();

    $(document).ready(function () {

        UserIdentity(PopupLoad);
        $.extend(jQuery.jgrid.defaults, {
            prmNames: {
                page: "page", rows: "size", order: "dir", sort: "sort"
            }
        });



    });
  function tbCrewGroup_Filter_CheckedChanged() {
        var tbCrewGroup = document.getElementById('tbCrewGroup').value;
        var label = document.getElementById('lbvalidCrewGroup');
        var hdCrewGroup = document.getElementById("hdnCrewGroupID");
        var btnCrewGroup = document.getElementById("btnCrewGroup");
        CrewGroupValidation(tbCrewGroup, label, hdCrewGroup, btnCrewGroup);
    }
    function OnClientCrewGroupClose(oWnd, args) {
        var combo = $find("tbCrewGroup");
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("hdnCrewGroupID").value = arg.CrewGroupID;
                document.getElementById("tbCrewGroup").value = arg.CrewGroupCD;
                tbCrewGroup_Filter_CheckedChanged();
            }
            else {
                document.getElementById("hdnCrewGroupID").value = "";
                document.getElementById("tbCrewGroup").value = "";
            }
        }
    }
    function ShowEditForm(id, CrewName) {
        var hdnNonLogTripSheet = GlobalIdentity._fpSettings._IsNonLogTripSheetIncludeCrewHIST.toString();
        window.radopen("CrewActivityPopup.aspx?CrewID=" + id + "&CrewName=" + CrewName + "&NonLogTripSheet=" + hdnNonLogTripSheet.toString(), "UserListDialog");
        return false;
    }
    function ShowInsertForm() {
        window.radopen("CrewActivityPopup.aspx", "UserListDialog");
        return false;
    }
    function Filter_CheckedChanged() {
        Filter = true;
        reloadPage();
    }

    function RowAvailableDblClick(sender, eventArgs) {
        window.radopen("CrewActivityPopup.aspx?CrewCD=" + eventArgs.getDataKeyValue("CrewID") + "&CrewName=" + eventArgs.getDataKeyValue("CrewName"), "UserListDialog");
    }
    function PopupLoad(Identity) {
        GlobalIdentity = Identity;
        hdIsTripPrivacy = "false";
        if (!Identity._isSysAdmin) {
            if (Identity._isTripPrivacy) {
                hdIsTripPrivacy = "true";
            }
        }

        if (Identity != null) {
            hdHomebaseID = Identity._homeBaseId;
        }
        else {
            hdHomebaseID = "0";
        }


        if (Identity._airportId != null)
            airport = Identity._airportId;

        if (Identity._clientId != null) {
            hdClientCodeID = Identity._clientId;
        }
        if (Identity._airportICAOCd != null) {
            homeBase = Identity._airportICAOCd;
        }
    }

    function openWin(radWin) {
        var url = '';

        if (radWin == "rdCrewGroupPopup") {
            url = '/Views/Settings/People/CrewGroupPopup.aspx?CrewGroupCD=' + document.getElementById('tbCrewGroup').value;
        }
        else if (radWin == "RadRetrievePopup") {
            url = '../../Transactions/Preflight/PreflightSearch.aspx';
        }
        if (url != "")
            var oWnd = radopen(url, radWin);
    }

    /// <summary>
    /// Method to Get Minutes from decimal Value
    /// </summary>
    /// <param name="time">Pass Time Value</param>
    /// <returns>Retruns Tenths Converted Value</returns>
    function ConvertTenthsToMins(time) {

        var result = "00:00";
        var val;
        if (time.indexOf(".") != -1) {
            var timeArray = time.split('.');
            var hour = timeArray[0];
            var minute = timeArray[1].toString();//Convert.ToDecimal(timeArray[1]);
            var decimal_min = parseFloat(("0.").concat(minute));
            var decimalOfMin = 0;
            if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                decimalOfMin = (decimal_min * 60 / 100);
            if (hour.toString().Length < 7) {
                if (hour.toString().Length == 0) {
                    hour = ("0000000" + hour);
                }
                if (hour.toString().Length == 1) {
                    hour = ("000000" + hour);
                }
                else if (hour.toString().Length == 2) {
                    hour = ("00000" + hour);
                }
                else if (hour.toString().Length == 3) {
                    hour = ("0000" + hour);
                }
                else if (hour.toString().Length == 4) {
                    hour = ("000" + hour);
                }
                else if (hour.toString().Length == 5) {
                    hour = ("00" + hour);
                }
                else if (hour.toString().Length == 6) {
                    hour = ("0" + hour);
                }
            }
            if (hour.toString().Length > 2) {
                hour = hour.toString().substring(0, 7);
            }
            var finalval = Math.round(parseFloat(hour) + decimalOfMin).toFixed(2);
            result = finalval.toString().replace(".", ":");
        }
        else if (parseInt(time)) {
            result = parseInt(time);
        }

        return result;

    }


    function OnClientCrewGroupClose(oWnd, args) {
        var combo = $find("tbCrewGroup");
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("hdnCrewGroupID").value = arg.CrewGroupID;
                document.getElementById("tbCrewGroup").value = arg.CrewGroupCD;
                tbCrewGroup_Filter_CheckedChanged();
            }
            else {
                document.getElementById("hdnCrewGroupID").value = "";
                document.getElementById("tbCrewGroup.").value = "";
            }
        }
    }


    // this function is used to display the details of a particular crew
    function ShowCrewDetailsPopup(Crewid) {
        if (self.EditMode() == true) {
            var oWnd = window.radopen("/Views/Settings/People/CrewRoster.aspx?IsPopup=&CrewID=" + Crewid, "rdCrewPage");
            oWnd.add_close(OnClientCrewRosterPopup);
        }
        else {
            window.radopen("/Views/Transactions/Preflight/PreflightCrewPage.aspx?CrewID=" + Crewid, "rdCrewPage");
        }

        return false;
    }

    function OnClientCrewRosterPopup(oWnd, args) {
        reloadPage();
    }

    function InitializeCrewAvailibilityGrid()
    {

        var iswinsafari = isWindowsSafari();
        jQuery(jqgridTableId).jqGrid({
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CrewAvailabilityGrid',
            mtype: 'POST',
            datatype: "json",
            cache: false,
            async: true,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                $(jqgridTableId).jqGrid('clearGridData');
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData.chkCrewAvailabilityActive = $('#chkCrewAvailabilityActive').prop("checked");
                postData.chkPICSICOnly = $('#chkPICSICOnly').prop("checked");
                postData.chkCrewInactive = $('#chkCrewInactive').prop("checked");
                postData.chkTypeRatedOnly = $('#chkTypeRatedOnly').prop("checked");
                postData.chkNoConflictsOnly = $('#chkNoConflictsOnly').prop("checked");
                postData.chkHomeBaseOnly = $('#chkHomeBaseOnly').prop("checked");
                postData.tbCrewGroup = $('#tbCrewGroup').val();
                postData.View = $("input[name='radlstViews']:checked").val();
                postData.hdnIsCrewAutoAvailability = true;
                postData.hdnLeg = "1";// $("input[id$='hdnLeg']").val();
                postData.airportId = airport;
                postData.Filter = Filter;
                postData.hdnCrewGroupID = $('#hdnCrewGroupID').val();
                postData.homeBase = homeBase;
                postData.requestedby = "Currency";
                postData.crewIds = null;
                return JSON.stringify(postData);
            },
            height: 300,
            width: 714,
            autowidth: false,
            shrinkToFit: false,
            rowNum: 20,
            ignoreCase: true,
            multiselectWidth: 30,
            multiselect: MultiSelectFlag,
            cmTemplate: { resizable: !iswinsafari },
            pager: "#pg_gridPager",
            viewrecords: true,
            colNames: ['CrewID', 'Crew Activity', 'Crew Code', 'Crew Name', 'Standby', 'Tail Assigned', 'A/C Type', 'Conflicts', 'Checklist', 'Day T/O', 'Day Landing', 'Night T/O', 'Night Landing', 'Appr.', 'Instr.', '7 Days', '30 Days', 'Month', '90 Days', 'Home', 'Crew Rating', 'Notes', 'IsStatus'],
            colModel: [
                 { name: 'CrewID', index: 'CrewID', key: true, hidden: true, frozen: true, sortable: false },

                 {
                     name: 'Crew Activity', index: 'CrewCode', width: 55, frozen: true, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                         return '<a id="EditLink" style="cursor: pointer;" class="time-icon" onclick="ShowEditForm(' + rowObject['CrewID'] + ',\'' + rowObject['CrewName'] + '\')" ToolTip="Crew Activity"></a>';
                     }
                 },
                 {
                     name: 'CrewCD', index: 'CrewCD', width: 50, frozen: true, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                         return '<a ID="lnkCrewCode" style="cursor: pointer;"  class="tdtext100"   onclick="return ShowCrewDetailsPopup(' + rowObject['CrewID'] + ');">' + (cellvalue != null ? cellvalue : "") + '</a>';
                     }
                 },
                {
                    name: 'CrewName', index: 'CrewName', width: 160, frozen: true, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {

                        var fullname = "";
                        if (cellvalue != undefined) {
                            cellvalue = $.trim(cellvalue);
                            fullname = cellvalue.split(" ");
                        }

                        var crewName = "";
                        if (fullname.length > 0) {
                            if (fullname.length > 2) {
                                crewName = fullname[0] + ", ";
                                crewName += fullname[1];
                                crewName += " " + fullname[2];
                            }
                            if (fullname.length == 2) {
                                crewName += fullname[0] + ", ";
                                crewName += fullname[1];
                            }
                            crewName = $.trim(crewName);
                        }
                        cellvalue = crewName;


                        return cellvalue;
                       
                    }
                },
                {
                    name: 'Standby', index: 'Standby', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == "1")
                            return '<center><span ID="lbStandby" >Y</span></center>';
                        else
                            return '<center><span ID="lbStandby" ></span></center>';
                    }
                },
                {
                    name: 'TailAssigned', index: 'TailAssigned', width: 60, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == "1")
                            return '<center><span ID="lbTailAssigned" >Y</span></center>';
                        else
                            return '<center><span ID="lbTailAssigned" ></span></center>';
                    }
                },
                { name: 'Actype', index: 'Actype', width: 100, sortable: true, search: true, searchoptions: { sopt: ['cn'] } },
                {
                    name: 'Conflicts', index: 'Conflicts', width: 60, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == "1")
                            return '<center><span ID="lbConflicts" >*</span></center>';
                        else
                            return '<center><span ID="lbConflicts" ></span></center>';
                    }
                },
                {
                    name: 'ChkList', index: 'ChkList', width: 65, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == "1")
                            return '<center><span ID="lbCheckList" style="color:Red" >!</span></center>';
                        else
                            return '<center><span ID="lbCheckList" ></span></center>';
                    }
                },
                { name: 'Daytkoff', index: 'Daytkoff', width: 55, sortable: true, search: false },
                { name: 'DayLnding', index: 'DayLnding', width: 55, sortable: true, search: false },
                { name: 'Nighttkoff', index: 'Nighttkoff', width: 55, sortable: true, search: false },
                { name: 'NightLnding', index: 'NightLnding', width: 55, sortable: true, search: false },
                { name: 'Appr', index: 'Appr', width: 55, sortable: true, search: false },
                {
                    name: 'Instr', index: 'Instr', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {

                        if ((GlobalIdentity._fpSettings._TimeDisplayTenMin == 2) && (!IsNullOrEmpty(cellvalue))) {
                            cellvalue = ConvertTenthsToMins(Math.round(parseFloat((cellvalue).toString())).toFixed(3).toString());
                            if (cellvalue.toString().indexOf(":") < 0) {
                                cellvalue = cellvalue + ":00";
                            }
                        }
                        else {
                            if (cellvalue == "0")
                                cellvalue = "00:00";
                            else
                                cellvalue = (Math.round(parseFloat((cellvalue).toString())).toFixed(1).toString());
                        }
                        return cellvalue;
                    }
                },
                {
                    name: 'SevenDays', index: 'SevenDays', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if ((GlobalIdentity._fpSettings._TimeDisplayTenMin == 2) && (!IsNullOrEmpty(cellvalue))) {
                            cellvalue = ConvertTenthsToMins(Math.round(parseFloat((cellvalue).toString())).toFixed(3).toString());
                            if (cellvalue.toString().indexOf(":") < 0) {
                                cellvalue = cellvalue + ":00";
                            }
                        }
                        else {
                            if (cellvalue == "0")
                                cellvalue = "00:00";
                            else
                                cellvalue = (Math.round(parseFloat((cellvalue).toString())).toFixed(1).toString());
                        }
                        return cellvalue;
                    }
                },
                {
                    name: 'ThirtyDays', index: 'ThirtyDays', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if ((GlobalIdentity._fpSettings._TimeDisplayTenMin == 2) && (!IsNullOrEmpty(cellvalue))) {
                            cellvalue = ConvertTenthsToMins(Math.round(parseFloat((cellvalue).toString())).toFixed(3).toString());
                            if (cellvalue.toString().indexOf(":") < 0) {
                                cellvalue = cellvalue + ":00";
                            }
                        }
                        else {
                            if (cellvalue == "0")
                                cellvalue = "00:00";
                            else
                                cellvalue = (Math.round(parseFloat((cellvalue).toString())).toFixed(1).toString());
                        }
                        return cellvalue;
                    }
                },
                {
                    name: 'CalMon', index: 'CalMon', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if ((GlobalIdentity._fpSettings._TimeDisplayTenMin == 2) && (!IsNullOrEmpty(cellvalue))) {
                            cellvalue = ConvertTenthsToMins(Math.round(parseFloat((cellvalue).toString())).toFixed(3).toString());
                            if (cellvalue.toString().indexOf(":") < 0) {
                                cellvalue = cellvalue + ":00";
                            }
                        }
                        else {
                            if (cellvalue == "0")
                                cellvalue = "00:00";
                            else
                                cellvalue = (Math.round(parseFloat((cellvalue).toString())).toFixed(1).toString());
                        }
                        return cellvalue;
                    }
                },
                {
                    name: 'NinetyDays', index: 'NinetyDays', width: 55, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                        if ((GlobalIdentity._fpSettings._TimeDisplayTenMin == 2) && (!IsNullOrEmpty(cellvalue))) {
                            cellvalue = ConvertTenthsToMins(Math.round(parseFloat((cellvalue).toString())).toFixed(3).toString());
                            if (cellvalue.toString().indexOf(":") < 0) {
                                cellvalue = cellvalue + ":00";
                            }
                        }
                        else {
                            if (cellvalue == "0")
                                cellvalue = "00:00";
                            else
                                cellvalue = (Math.round(parseFloat((cellvalue).toString())).toFixed(1).toString());
                        }
                        return cellvalue;
                    }
                },
                { name: 'Home', index: 'Home', width: 100, sortable: true, search: true, searchoptions: { sopt: ['cn'] } },
                {
                    name: 'CrewRating', index: 'CrewRating', width: 155, sortable: true, search: true, searchoptions: { sopt: ['cn'] }, formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            if (cellvalue.length > 0) {
                                return cellvalue.slice(cellvalue.length - 1);
                            }
                        }
                        return cellvalue;
                    }
                },
               { name: 'Notes', index: 'Notes', width: 55, sortable: true, search: false, hidden: true },
               { name: 'IsStatus', index: 'IsStatus', width: 55, sortable: true, search: false, hidden: true }
            ],
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
            },
            onSelectRow: function (id, status) {
                var rowData = $(this).getRowData(id);
                if (status) 
                    selectedCrewsList.push(rowData);
                else {
                    for (var rowId = 0; rowId < selectedCrewsList.length; rowId++) {
                        if (selectedCrewsList[rowId].CrewID == id) 
                            selectedCrewsList.splice(rowId, 1);
                    }
                }
            },
            onSelectAll: function (ids, status) {
                if (status) {
                    for (var rowId = 0; rowId < ids.length; rowId++) {
                        var rowData = $(jqgridTableId).jqGrid("getRowData", ids[rowId]);
                        if (status) {
                            if ($.inArray(rowData.CrewID, selectedCrewsList) < 0)
                                selectedCrewsList.push(rowData);
                        }
                    }
                }
                else {
                    for (var crewId = 0; crewId < ids.length; crewId++) {
                        for (var rowId = 0; rowId < selectedCrewsList.length; rowId++) {
                            if (selectedCrewsList[rowId].CrewID == ids[crewId]) 
                                selectedCrewsList.splice(rowId, 1);
                        }
                    }
                }
            },
            beforeSelectRow: function (rowid, e) {
                if (self.EditMode())
                    return true;
                else
                    return false;
            },
            beforeRequest: function () {
                RequestStart();
            },
            gridComplete: function () {
            },
            loadComplete: function () {
                if (isWindowsSafari()) {
                    $('.frozen-div th#CrewsAvailabilityGridID_CrewName').css("width", "183px");
                    $('#CrewsAvailabilityGridID_frozen tr td:nth-child(5)').css("width", "183px");
                }
                for (var rowId = 0; rowId < selectedCrewsList.length; rowId++) {
                    jQuery(jqgridTableId).setSelection(selectedCrewsList[rowId].CrewID, false);
                }
                var rows = jQuery(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                if (rows.length == 20)
                    $('#cb_CrewsAvailabilityGridID.cbox').attr('checked', true);
                if (isWindowsSafari()) {

                    if ($('#CrewsAvailabilityGrid div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                        $('#CrewsAvailabilityGrid div[class="ui-jqgrid-bdiv"]').scroll(function () {
                            if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                                $('#CrewsAvailabilityGrid .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                            } else {
                                $('#CrewsAvailabilityGrid .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');
                            }
                        });
                    }
                        $("#CrewsAvailabilityGridID_frozen tr").each(function (i) {
                            var id = $(this).attr("id");
                            if (id != undefined || id != '') {
                                var paddingT = $(this).children("td").css("padding-top");
                                var height = $(this).children("td").css("height");
                                $("#CrewsAvailabilityGridID #" + id + " td").each(function () {
                                    $(this).css("padding", paddingT).css("height", height);
                                });
                            }
                        });
                }
                else if (isMacSafari()) {

                    if ($('#CrewsSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                        $('#CrewsSelection div[class="ui-jqgrid-bdiv"]').scroll(function () {
                            if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {
                                $('#CrewsAvailabilityGrid .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                            } else {
                                $('#CrewsAvailabilityGrid .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');
                            }
                        });
                    }
                }
                else {
                    
                    if ($('#CrewsAvailabilityGrid div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {
                        $('#CrewsAvailabilityGrid .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    }
                }
                ResponseEnd();

                EnableDisableControlsofGrid(self.EditMode());
            }
        });
        $("<span>All<br /></span>").appendTo("#jqgh_CrewsAvailabilityGridID_cb");
        $(jqgridTableId).jqGrid('setFrozenColumns');
        $(jqgridTableId).jqGrid('filterToolbar', { autosearch: true, defaultSearch: 'cn', searchOnEnter: false });
        $(jqgridTableId).jqGrid('navGrid', '#pg_gridPager', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
        $(jqgridTableId).jqGrid('navButtonAdd', "#pg_gridPager", {
            caption: "", title: "Reload Grid", buttonicon: "ui-icon-refresh",
            onClickButton: function () {
                reloadPage();
            }
        });
        
        $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });//.trigger('reloadGrid');

    }

    function EnableDisableControlsofGrid(enable) {
        if (enable) {
            $("#gview_CrewsAvailabilityGridID input:not(.alwaysdisabled),#gview_CrewsAvailabilityGridID select").removeAttr("disabled");
            

        } else {
            $("#gview_CrewsAvailabilityGridID input:not(.alwaysdisabled),#gview_CrewsAvailabilityGridID select").attr("disabled", "disabled");
            
        }


    }
    
    function fullNameFormatter(cellvalue, options, rowObject) {
        var mname = rowObject.MiddleInitial == null ? "" : rowObject.MiddleInitial;
        var lname = rowObject.LastName == null ? "" : rowObject.LastName;
        var fname = rowObject.FirstName == null ? "" : rowObject.FirstName;
        return lname + ', ' + fname + ' ' + mname;
    }

    function dateFormatter(cellvalue, options, rowObject) {
        var AppdateFormat = GlobalIdentity._fpSettings._ApplicationDateFormat;
        if (cellvalue == "0001-01-01T00:00:00") {
            cellvalue = "";
        }
        else
        {
            cellvalue= moment(cellvalue).format(AppdateFormat.toUpperCase());
        }
        return cellvalue;
    }

    function crewChecklistGrid()
    {
        jQuery(jqgridCheckListTable).jqGrid({
            url: '/Views/Utilities/ApiCallerWithFilter.aspx',
            mtype: 'GET',
            datatype: "json",
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8'},
            serializeGridData: function (postData) {
                $(jqgridCheckListTable).jqGrid("clearGridData");
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData.apiType = 'fss';
                postData.method = 'CrewCheckList';
                postData.sendCustomerId = true;
                postData.isStatus = $('#chkCrewAvailabilityActiveView2').prop("checked");
                postData.RequestedView = $("input[name='radlstViews']:checked").val();
                var ids = jQuery(jqgridTableId).getGridParam('selarrrow');
                postData.CrewIDs = ids.join(',');
                return postData;
            },
            height: 300,
            width: 705,
            autowidth: false,
            shrinkToFit: false,
            rowNum: $("#rowNum").val(),
            ignoreCase: true,
            multiselect: CheckListMultiSelectFlag,
            pager: "#pg_gridPager2",
            viewrecords: true,
            colNames: ['CrewID', 'ROWNUMBER','Crew Code', 'Crew Name', 'LastName', 'MiddleInitial', 'Alert', 'Grace', 'Aircraft Type', 'Aircraft Desc', 'Description', 'Due'],
            colModel: [
                    { name: 'ROWNUMBER', index: 'ROWNUMBER', key: true, hidden: true, sortable: false },
                    { name: 'CrewID', index: 'CrewID',hidden: true, sortable: false },
                    { name: 'CrewCD', index: 'CrewCD', width: 50, sortable: true, search: true },
                    { name: 'LastName', index: 'LastName', width: 200, formatter: fullNameFormatter},
                    { name: 'FirstName', index: 'FirstName', hidden: true, sortable: false },
                    { name: 'MiddleInitial', index: 'MiddleInitial', hidden: true, sortable: false },
                    { name: 'AlertDT', index: 'AlertDT', width: 100, sortable: true, search: false, formatter: dateFormatter },
                    { name: 'GraceDT', index: 'GraceDT', width: 100, sortable: true, search: false, formatter: dateFormatter },
                    { name: 'AircraftCD', index: 'AircraftCD', width: 70, sortable: true, search: true },
                    { name: 'AircraftDescription', index: 'AircraftDescription', width: 80, sortable: true, search: true },
                    { name: 'CrewChecklistDescription', index: 'CrewChecklistDescription', width: 80, sortable: true, search: true },
                    { name: 'DueDT', index: 'DueDT', width: 100, sortable: true, search: false, formatter: dateFormatter }
            ],
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
            },
            onSelectRow: function (id) {
                var rowData = $(this).getRowData(id);
            },
            gridComplete: function () {
            },
            afterInsertRow: function (id, data,rowElem) {

                if (rowElem.IsScheduleCheck && data.DueDT && data.AlertDT && data.GraceDT) {
                    var d = new Date();
                    var DueNextSource = new Date($.fn.fmatter.call(this, "date", data.DueDT, null, null));
                    var AlertSource1 = new Date($.fn.fmatter.call(this, "date", data.AlertDT, null, null));
                    var GraceSource2 = new Date($.fn.fmatter.call(this, "date", data.GraceDT, null, null));

                    if (((DueNextSource < d) && (d < GraceSource2)) || ((GraceSource2 < d) && (d < DueNextSource))) {
                        $("#gview_CheckListGridID").find('#' + id).removeClass('ui-widget-content');
                        $("#gview_CheckListGridID").find('#' + id).addClass('row-orange');
                    }

                    if ((d > DueNextSource) && (GraceSource2 < d)) {
                        $("#gview_CheckListGridID").find('#' + id).removeClass('ui-widget-content');
                        $("#gview_CheckListGridID").find('#' + id).addClass('row-red');
                    }

                    if (((DueNextSource < d) && (d < AlertSource1)) || ((AlertSource1 < d) && (d < DueNextSource))) {
                        $("#gview_CheckListGridID").find('#' + id).removeClass('ui-widget-content');
                        $("#gview_CheckListGridID").find('#' + id).addClass('row-yellow');
                    }
                }
            }
        });
        $("#pg_gridPager2_left").html($('#pagesizebox'));
        $(jqgridCheckListTable).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        $(jqgridCheckListTable).jqGrid('navGrid', '#pg_gridPager2', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
    }

    $(document).ready(function () {
        $("#CheckListGrid").hide();
        $(".view2").hide();
        $('input[type="radio"][name="radlstViews"]').change(function () {
            if (this.value == 'Currency') {
                $("#CheckListGrid").hide();
                $("#CrewsAvailabilityGrid").show();
                $(jqgridTableId).jqGrid('clearGridData');
                $(jqgridTableId).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
                $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
                $(".view1").show();
                $(".view2").hide();
            }
            else if (this.value == 'Checklist') {
                $("#CrewsAvailabilityGrid").hide();
                $("#CheckListGrid").show();
                $(jqgridCheckListTable).jqGrid('clearGridData');
                crewChecklistGrid();
                $(jqgridCheckListTable).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
                $(".view2").show();
                $(".view1").hide();
            }
            else if (this.value == 'Indiv Checklist') {
                $("#CrewsAvailabilityGrid").hide();
                $("#CheckListGrid").show();
                $(jqgridCheckListTable).jqGrid('clearGridData');
                crewChecklistGrid();
                $(jqgridCheckListTable).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
                $(".view2").show();
                $(".view1").hide();
            }
        });

        
    });

    //this function is used to close this window
    function CloseAndRebind(arg) {
        GetRadWindow().Close(arg);
        GetRadWindow().BrowserWindow.location.reload(false);
    }
    function reloadPageSize(jqgridTableId, rowNumId) {
        var myGrid = $(jqgridTableId);
        var currentValue = $(rowNumId).val();
        myGrid.setGridParam({ rowNum: currentValue });
        myGrid.trigger('reloadGrid',[{page:1}]);
    }

    function FilterView2_CheckedChanged() {
        var myGrid = $(jqgridCheckListTable);
        var currentValue = $("#rowNum").val();
        myGrid.setGridParam({ rowNum: currentValue });
        myGrid.trigger('reloadGrid');
    }
    function reloadPage() {
        $(jqgridTableId).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
        $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
    }
    function chkHomeBaseOnly_CheckedChanged() {
        if ($('#chkHomeBaseOnly').prop("checked")) {
            $("#gs_Home").val(homeBase);
            $(jqgridTableId).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
            $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
        }
        else {
            $("#gs_Home").val("");
            $(jqgridTableId).jqGrid('setGridParam', { datatype: 'json', loadonce: false }).trigger('reloadGrid');
            $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
        }
        var e = jQuery.Event("keypress");
        e.which = 13; //choose the one you want
        e.keyCode = 13;
        $("#gs_Home").trigger(e);
    }

</script>


<style type="text/css">
    th[role='columnheader'] div {
        height: auto !important;
        overflow: hidden;
        padding-right: 4px;
        padding-top: 2px;
        position: relative;
        vertical-align: text-top;
        white-space: normal !important;
    }

    .row-red {
        background-color: red;
    }

    .row-yellow {
        background-color: yellow;
    }

    .row-orange {
        background-color: orange;
    }

    .frozen-div {
        height:67px !important;
        overflow:hidden;
    }

        .frozen-div th {
            height:65px !important;
        }

    .frozen-bdiv {
        top:68px !important;
         overflow:hidden;
    }
    .box1
    {
        padding:0px !important;
    }
</style>


<div id="DivExternalForm">
    <input type="hidden" id="hdnCrewGroupID" />
    <input id="Airport" type="hidden" />
    <div style="text-align: left; float: left; width: 718px; padding: 5px 0 0 0px;">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <%-- After Migration data-bind="enable: EditMode"--%>
                <td class="tdLabel160" valign="top">
                    <div class="view1">
                        <input type="checkbox" id="chkCrewAvailabilityActive" data-bind="enable: EditMode"
                            onchange="Filter_CheckedChanged();" checked="checked"/>
                        Crew Availability Active
                    </div>
                    <div class="view2">
                        <input type="checkbox" id="chkCrewAvailabilityActiveView2"  data-bind="enable: EditMode"
                            onchange="FilterView2_CheckedChanged();" checked="checked" />
                        Crew Availability Active
                    </div>
                </td>
                <td align="left" class="tdLabel100">
                    <div class="view1">
                        <input type="checkbox" id="chkPICSICOnly" onchange="Filter_CheckedChanged();"  data-bind="enable: EditMode" />
                        PIC/SIC Only
                    </div>
                </td>
                <td class="tdLabel110" valign="top">
                    <div class="view1">
                        <input type="checkbox" id="chkCrewInactive" onchange="Filter_CheckedChanged();"  data-bind="enable: EditMode" />
                        Inactive Crew
                    </div>
                </td>
                <td valign="top" class="tdLabel120">
                    <div class="view1">
                        <input type="checkbox" id="chkTypeRatedOnly" onchange="Filter_CheckedChanged();"  data-bind="enable: EditMode" />
                        Type Rated Only
                    </div>
                </td>
                <td valign="top">
                    <div class="view1">
                        <input type="checkbox" id="chkNoConflictsOnly" onchange="Filter_CheckedChanged();"  data-bind="enable: EditMode" />
                        No Conflicts Only
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="view1">
                        <input type="checkbox" id="chkHomeBaseOnly" onchange="chkHomeBaseOnly_CheckedChanged();"  data-bind="enable: EditMode" />
                        Home Base Only
                    </div>
                </td>
                <td>
                    <div class="view1">
                        <label id="lbCrewGroup">Crew Group</label>
                    </div>
                </td>
                <td colspan="2">
                    <div class="view1">
                        <input type="text" id="tbCrewGroup" class="text60" maxlength="4" onkeypress="return fnAllowAlphaNumeric(this, event)" onchange="tbCrewGroup_Filter_CheckedChanged();"  data-bind="enable: EditMode" />
                        <input type="button" id="btnCrewGroup" tooltip="View Crew Groups" onclick="javascript: openWin('rdCrewGroupPopup'); return false;"
                            data-bind="enable: EditMode, css: BrowseBtn" />
                        <label id="lbvalidCrewGroup" class="alert-text"></label>
                    </div>
                </td>

            </tr>
        </table>
    </div>
    <div style="text-align: right; float: right; width: 350px; padding: 5px 0 0 0px;">
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td>Switch Views:
                                                    </td>
                    <td>
                        <table >
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="radio" name="radlstViews" checked="checked" value="Currency" data-bind="enable: EditMode">Currency
                                    </td>
                                    <td>
                                        <input type="radio" name="radlstViews" value="Checklist"  data-bind="enable: EditMode">Checklist
                                    </td>
                                    <td>
                                        <input type="radio" name="radlstViews" value="Indiv Checklist"  data-bind="enable: EditMode">Indiv Checklist
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="text-align: left; float: left; width: 718px; padding: 0px 0 0 0px;">
        <div id="CrewsAvailabilityGrid" class="jqgrid">
            <div>
                <table cellpadding="0" cellspacing="0" class="box1">
                    <tr>
                        <td id="tdOfCrewsAvailabilityTable">
                            <table id="CrewsAvailabilityGridID" class="table table-striped table-hover table-bordered preflight_crew_availability_grid"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                            </div>
                            <a href="#" onclick="return ShowInsertForm();"></a>
                          
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="CheckListGrid" class="jqgrid">
            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <table id="CheckListGridID" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager2"></div>
                                 <div id="pagesizebox">
                                    <span>Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridCheckListTable, $('#rowNum')); return false;" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
