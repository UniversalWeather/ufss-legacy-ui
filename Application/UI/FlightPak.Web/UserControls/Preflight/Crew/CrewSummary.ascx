﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrewSummary.ascx.cs" Inherits="FlightPak.Web.UserControls.Preflight.Crew.CrewSummary" %>
<script type="text/javascript">
    var selectedRowData = null;
    var CrewsSummaryjqgridTableId = '#CrewsSummaryID';
    var hdnPassCrew = "";
    var hdnAssignPassLeg = "";
    var sortFlag = true;
    $(document).ready(function () {
        $.extend(jQuery.jgrid.defaults, {
            prmNames: {
                page: "page", rows: "size", order: "dir", sort: "sort"
            }
        });
    });



    $(document).ready(function () {
        jQuery(CrewsSummaryjqgridTableId).jqGrid({
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CrewSummaryGrid',
            mtype: 'POST',
            datatype: "local",
            cache: false,
            async: true,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData._search = undefined;
                postData.nd = undefined;
                postData.size = undefined;
                postData.page = undefined;
                postData.sort = undefined;
                postData.dir = undefined;
                postData.filters = undefined;
                
                return JSON.stringify(postData);
            },
            height: 200,
            width: 718,
            autowidth: false,
            shrinkToFit: false,
            rowNum: 10000,
            multiselect: false,
            pager: "#pg_gridPagerCrewsSummary",
            pgbuttons: false,
            viewrecords: false,
            pgtext: "",
            pginput: false,

            colNames: ['CrewID', 'Crew Code', 'Crew Name', 'Leg No.', 'Street', 'City', 'State', 'Postal', 'Passport', 'Nationality', 'Choice', 'Expiration Date', 'Issue Date'],
            colModel: [
                        { name: 'CrewID', index: 'CrewID', key: true, hidden: true, sortable: false },
                        {
                            name: 'CrewCD', index: 'CrewCD', width: 80, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<span ID="lnkCrewCode" >' + cellvalue + '</span>';
                            }
                        },
                       { name: 'CrewName', index: 'CrewName', width: 150, sortable: false, search: false },
                       { name: 'Leg', index: 'Leg', width: 60, sortable: true,sorttype:"number", search: false, firstsortorder: 'asc' },
                       {
                           name: 'Street', index: 'Street', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               var onchange = "onchange='UpdateSummary(this);'";
                               return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;" ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["Leg"] + '" col="Street"  data-bind="enable: EditMode"/>';
                           }
                       },
                       {
                           name: 'City', index: 'City', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               var onchange = "onchange='UpdateSummary(this);'";
                               return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"  ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["Leg"] + '" col="City"  data-bind="enable: EditMode"/>';
                           }
                       },
                       {
                           name: 'State', index: 'State', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               var onchange = "onchange='UpdateSummary(this);'";
                               return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"    ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["Leg"] + '" col="State"  data-bind="enable: EditMode"/>';
                           }
                       },
                       {
                           name: 'Postal', index: 'Postal', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               var onchange = "onchange='UpdateSummary(this);'";
                               return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"   ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["Leg"] + '" col="Postal"  data-bind="enable: EditMode"/>';
                           }
                       },

                       {
                           name: 'Passport', index: 'Passport', width: 130, sortable: false, search: false, formatoptions: { disabled: true }, formatter: function (cellvalue, options, rowObject) {
                               var ShowCrewPassport = "return ShowCrewPassportPopup('" + rowObject["CrewID"] + "','" + rowObject["Leg"] + "','" + rowObject["CrewName"] + "');";
                               var Content = "";
                               Content = Content + '<input type="hidden" id="hdnVisaID" value="' + rowObject["VisaID"] + '">';
                               Content = Content + '<input type="hidden" id="hdnPassID" value="' + rowObject["PassportID"] + '">';
                               Content = Content + '<input type="hidden" id="hdnPassNum" value="' + rowObject["Passport"] + '">';
                               Content = Content + '<input type="hidden" id="hdnCrewName" value="' + rowObject["CrewName"] + '">';
                               Content = Content + '<input type="text" style="font-family:Arial;font-size:12px;"  class="alwaysdisabled aspNetDisabled tdLabel70" disabled="disabled" id="tbPassport' + rowObject["CrewID"] + rowObject["Leg"] +  '" value="' + ((rowObject["Passport"] == null) ? '' : rowObject["Passport"]) + '">';
                               Content = Content + '<a style="display:inline-block;height:16px;width:16px;" href="#" class="browse-button" id="lnkPassport" onclick="' + ShowCrewPassport + '" data-bind="enable: EditMode,css: BrowseBtn"></a>';
                               return Content;
                           }
                       },

                       { name: 'Nation', index: 'Nation', width: 80, sortable: false, search: false },
                       
                       {
                           name: 'Choice1', index: 'Choice1', width: 50, sortable: false, search: false, formatoptions: { disabled: true },
                           formatter: function (cell, options, rowObject) {
                               if (cell == "True") {
                                   return '<input type="checkbox" checked="checked" offval="no" class="alwaysdisabled" disabled="disabled">';
                               }
                               else {
                                   return '<input type="checkbox" class="alwaysdisabled" disabled="disabled">';

                               }
                           }
                       },
                       { name: 'PassportExpiryDT', index: 'PassportExpiryDT', width: 100, sortable: false, search: false },
                      { name: 'IssueDT', index: 'IssueDT', width: 100, sortable: false, search: false }

            ],
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
            },
            onSelectRow: function (id) {
                var rowData = $(this).getRowData(id);
            },
            gridComplete: function () {
                setTimeout(function () {
                    if (sortFlag) {
                        //DisplayRows();
                        $(CrewsSummaryjqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
                        $(CrewsSummaryjqgridTableId).jqGrid("sortGrid", "Leg", true, "asc");
                        sortFlag = false;
                    }
                }, 1000);
            },
            loadComplete: function (rowData) {
                var tableHeader = $("#gbox_CrewsSummaryID").find('.ui-jqgrid-hbox');//.css("position", "relative");
                
                $("#gbox_CrewsSummaryID").find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
                    tableHeader.css('right', scrollPositionX);
                }).bind(
                    'jsp-scroll-y',
                    function (event, scrollPositionY, isAtTop, isAtBottom) {

                    }
                ).jScrollPane({
                    scrollbarWidth: 15,
                    scrollbarMargin: 0
                });
                var bdiv = $("#gbox_CrewsSummaryID").find('.ui-jqgrid-bdiv');
                //Must reset jsPane style to default.
                $(bdiv).find('.jspContainer').find('.jspPane').attr("style", "");

                $(".jspHorizontalBar").css("height", "10px");
                EnableDisableControlsofGrid(self.EditMode());
            }
            
        });
        
    });
    function EnableDisableControlsofGrid(enable) {
        if (enable) {
            $("#gview_CrewsSelectionID input:not(.alwaysdisabled),#gview_CrewsSelectionID select").removeAttr("disabled");
            $("#gview_CrewsSummaryID input:not(.alwaysdisabled)").removeAttr("disabled");

        } else {
            $("#gview_CrewsSelectionID input:not(.alwaysdisabled),#gview_CrewsSelectionID select").attr("disabled", "disabled");
            $("#gview_CrewsSummaryID input:not(.alwaysdisabled)").attr("disabled", "disabled");
        }
    }
    // this function is used to display the crew passport visa type popup 
    function ShowCrewPassportPopup(CrewID, Leg, CrewName) {
        //window.radopen("/Views/Transactions/Preflight/CrewPassportVisa.aspx?CrewID=" + CrewID, "rdCrewPassport");
        window.radopen("/Views/Transactions/Preflight/CrewPassportVisa.aspx?CrewID=" + CrewID + "&Leg=" + Leg + "&CrewName=" + CrewName, "rdCrewPassport");
        hdnPassCrew = CrewID;
        hdnAssignPassLeg = Leg;
        return false;
    }

    function OnClientVisaClose(oWnd, args) {
        var arg = args.get_argument();
        if (arg != null) {
            document.getElementById('hdnCrewInfo').value = arg.val;
            setAlertTextToYesNo();
            jConfirm('Update Passport Number for All Crew Legs?', "Confirmation!", function (r) {
                if (r) {
                    var data = $(CrewsSummaryjqgridTableId).jqGrid('getGridParam', 'data');
                    for (var i = 0; i < data.length; i++) {
                        if (hdnPassCrew == data[i].CrewID) {
                            SendSaveSummary(data[i].Leg, hdnPassCrew, "PassportID", arg.PassportID);
                            SendSaveSummary(data[i].Leg, hdnPassCrew, "PassportNum", arg.passportNum);
                            $("#tbPassport" + hdnPassCrew + data[i].Leg).val(arg.passportNum);
                        }
                    }
                }
                else {
                    SendSaveSummary(hdnAssignPassLeg, hdnPassCrew, "PassportID", arg.PassportID);
                    SendSaveSummary(hdnAssignPassLeg, hdnPassCrew, "PassportNum", arg.passportNum);
                    $("#tbPassport" + hdnPassCrew + hdnAssignPassLeg).val(arg.passportNum);
                }
                sortFlag = true;
                $(CrewsSummaryjqgridTableId).setGridParam({ datatype: 'json', loadonce: true}).trigger('reloadGrid');
            });
            setAlertTextToOkCancel();

        }
        return false;
    }

    function DisplayRows() {
        myGrid = $(CrewsSummaryjqgridTableId);
        var dataArray = myGrid.jqGrid('getGridParam', 'data');
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i].Display == "False" || dataArray[i].Display == null) {
                myGrid.jqGrid('delRowData', dataArray[i].CrewID);
            }
        }
    }

    function UpdateSummary(control) {
        var LegNumber = control.getAttribute("leg-no");
        var CrewID = control.getAttribute("row-id");
        var col = control.getAttribute("col");
        var val = control.value;
        SendSaveSummary(LegNumber, CrewID, col, val);
    }

    function SendSaveSummary(LegNumber, CrewID, col, val) {
        $("#UpdateLoading").show();
        var params = '{ColName: "' + col + '",Value: "' + val + '",CrewID: "' + CrewID + '",LegNumber: ' + LegNumber + '}';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SaveCrewSummaryCells',
            async: false,
            data: params,
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var returnResult = verifyReturnedResult(rowData);
                if (returnResult == true && rowData.d.Success == true) {
                    crewSelectionLegs = rowData.d.Result.results;
                    $("#UpdateLoading").hide();
                    setTimeout(function () {
                        $("#UpdateLoading").hide();
                    }, 2000);
                }
            },
            error: function (xhr, status, error) {
                setTimeout(function () {
                    $("#UpdateLoading").hide();
                }, 2000);
                jAlert("Error on Saving", "Preflight - CrewInfo");
            }
        });
    }

    function LoadCrewSummaryExpand() {
        sortFlag = true;
        $(CrewsSummaryjqgridTableId).setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    }

</script>

<div style="float: left; text-align: left; width: 718px; padding: 5px 0 0 0px;">


    <div style="float: left; width:718px; padding: 5px 0 0 0px;">
        <div id="CrewsSummary" class="jqgrid">
            <div>
                <table cellpadding="0" cellspacing="0" class="box1 preflight_crew_classsumme2">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" id="CrewsSummaryID" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPagerCrewsSummary">  <div id="UpdateLoading" style="display:none;">
                                    <input type="button" id="btnCrewGroup" tooltip="Loading" class="browse-button-Validation-Loading" onclick="javascript: return false;" />
                                 Updating 
                                </div></div>
                              
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
