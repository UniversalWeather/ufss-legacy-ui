﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrewSelection.ascx.cs" Inherits="FlightPak.Web.UserControls.Preflight.Crew.CrewSelection" %>

<style>
    .insertInto-icon-grid {
    background: url('../../../App_Themes/Default/images/insert_file_icon.png') left top no-repeat !important;
    display: block;
    height: 16px;
    text-indent: -9999px;
    width: 16px;
    margin: 4px 2px 0;
    float: left;
}
    body.winsaf .preflight_crewlist_grid .preflightcrew_selectiongrid .frozen-bdiv table.ui-jqgrid-btable tr td:nth-child(1) {
width: 52px!important;
text-align: center!important;
}
    .ui-jqgrid .ui-jqgrid-htable th div {
    height: auto !important;
    overflow: hidden;
    padding-right: 4px;
    padding-top: 0px;
    position: relative;
    vertical-align: text-top;
    white-space: normal !important;
    font-size: 11px;
}
</style>
<script type="text/javascript">

    if (isWindowsSafari()) {
        $('body').attr('class', 'winsaf');
    }
    else if (isMacSafari()) {
        $('body').attr('class', 'macsaf');
    }

    var dutyTypeDropdownHtml;
    var alertTitle = "Preflight";
    var IsCrewInfoGridLoded = false;
    var agt = navigator.userAgent.toLowerCase();
    var selectedRowData = null;
    var CrewsSelectionjqgridTableId = '#CrewsSelectionID';
    var CrewsSelection = "#CrewsSelection";
    var hdClientCodeID = "";
    var hdIsTripPrivacy = "";
    var hdHomebaseID = "";
    var hdSelectedTripNUM = "";
    var hdHomeBase = "";
    var airport = "";
    var homeBase = "";
    var GlobalIdentity;
    var crewSelectionLegs;
    var ColNames = ['CrewID', 'Crew Notified', 'Crew Code', 'Name', 'OrderNUM', 'Street', 'Postal', 'City', 'State', 'Passport', 'PassportID', 'VisaID', 'AddlNotes', 'Notes', 'PassportExpiryDT', 'Nation', 'tempStatus'];
    var ColModel = [
                 { name: 'CrewID', index: 'CrewID', key: true, hidden: true, frozen: true, sortable: false },
                  {
                      name: 'IsNotified', index: 'IsNotified', title: false, align: "center", width: 36, frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                          if (cellvalue == "True") {
                              return '<input id="chkNotified" onchange="CrewNotified(this);" class="chkNotified" row-id="' + rowObject["CrewID"] + '" data="' + rowObject["IsNotified"] + '" type="checkbox" ' + (rowObject["IsNotified"] == 'True' ? 'checked=checked' : '') + '" data-bind="enable: EditMode" />';
                          } else { return '<input id="chkNotified" onchange="CrewNotified(this);" class="chkNotified"  row-id="' + rowObject["CrewID"] + '" data="' + rowObject["IsNotified"] + '" type="checkbox" ' + (rowObject["IsNotified"] == 'True' ? 'checked=checked' : '') + '" data-bind="enable: EditMode" />'; }
                      }
                  },
                 {
                     name: 'CrewCD', index: 'CrewCD', width: 50, frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                         var styl = "";
                         if ($.trim(rowObject["Notes"]) != "")
                             styl = 'style="color: red;"';
                             return '<span ID="lnkCrewCode" ' + styl + ' >' + cellvalue + '</span>';
                     }
                 },
                {
                    name: 'CrewName', index: 'CrewName', width: 200, frozen: true, sortable: true, search: false,
                    formatter: function (cellvalue, options, rowObject) {
                        var styl = "";
                        if ($.trim(rowObject["Notes"]) != "")
                            styl = 'style="color: red;"';
                        return '<div ID="divCrewName" ' + styl + ' >' + cellvalue + '</div>';
                    }
                },
                { name: 'OrderNUM', index: 'OrderNUM', width: 100, sortable: false, search: false, hidden: true },
                { name: 'Street', index: 'Street', width: 60, sortable: false, search: false, hidden: true },
                { name: 'Postal', index: 'Postal', width: 65, sortable: false, search: false, hidden: true },
                { name: 'City', index: 'City', width: 55, sortable: false, search: false, hidden: true },
                { name: 'State', index: 'State', width: 55, sortable: false, search: false, hidden: true },
                { name: 'Passport', index: 'Passport', width: 55, sortable: false, search: false, hidden: true },
                { name: 'PassportID', index: 'PassportID', width: 55, sortable: false, search: false, hidden: true },
                { name: 'VisaID', index: 'VisaID', width: 55, sortable: false, search: false, hidden: true },
                { name: 'AddlNotes', index: 'AddlNotes', width: 55, sortable: false, search: false, hidden: true },
                { name: 'Notes', index: 'Notes', width: 55, sortable: false, search: false, hidden: true },
                { name: 'PassportExpiryDT', index: 'PassportExpiryDT', width: 55, sortable: false, search: false, hidden: true },
                { name: 'Nation', index: 'Nation', width: 55, sortable: false, search: false, hidden: true },
                { name: 'tempStatus', index: 'tempStatus', width: 55, sortable: false, search: false, hidden: true }
    ];

    function EnableDisableControlsofGrid(enable) {
        if (enable) {
            $("#gview_CrewsSelectionID input:not(.alwaysdisabled),#gview_CrewsSelectionID select").removeAttr("disabled");
            $("#gview_CrewsSummaryID input:not(.alwaysdisabled)").removeAttr("disabled");

        } else {
            $("#gview_CrewsSelectionID input:not(.alwaysdisabled),#gview_CrewsSelectionID select").attr("disabled", "disabled");
            $("#gview_CrewsSummaryID input:not(.alwaysdisabled)").attr("disabled", "disabled");
        }
    }

    $(document).ready(function () {
        GetLegs();

        self.EditMode.subscribe(function (newValue) {
            EnableDisableControlsofGrid(newValue);
        });

        $.extend(jQuery.jgrid.defaults, {
            prmNames: {
                page: "page", rows: "size", order: "dir", sort: "sort"
            }
        });

        $("#Submit1").click(function () {
            CrewCodeValidate();
            return false;
        });

       
        $('#ddlAllLegs').on('change', function () {
            var SelectedValue = this.value;
            var selectedRows = jQuery(CrewsSelectionjqgridTableId).jqGrid('getGridParam', 'selarrrow');
            if (selectedRows.length > 0) {
                $.each(jQuery(CrewsSelectionjqgridTableId).jqGrid('getGridParam', 'selarrrow'), function (index, value) {
                    $('#' + value).find('select').each(function () {
                        $(this).val(SelectedValue);
                        DisableandCollapsePanel(this, "dropdown2", "1");
                    });
                })
                Save_CrewInfo();
                $(CrewsSelectionjqgridTableId).jqGrid('resetSelection');
            }
            else {
                jAlert("Please select record.", "Preflight - CrewInfo");
                $(this).val("0");
            }
        });
    });

    function CrewCodeValidate() {
        var hdnCrewGroupID = document.getElementById("hdnCrewGroupID").value;
        var CrewCD = document.getElementById("SearchBoxCrew").value;
        var CrewID = document.getElementById("hdnCrewInfoId").value;
        var btn = document.getElementById("Submit1");
        var airportId = airport;
        CrewCodeValidation(CrewCD, CrewID, airportId, hdnCrewGroupID, btn, AddCrews);
    }

    function Save_CrewInfo(arg1) {
        if (IsNullOrEmptyOrUndefined(arg1)) {
            ShowJqGridLoader($("#gbox_CrewsSelectionID"));
        }
        OrderNumbers();
        UpdateModel_Legs();
        var dataArray = $(CrewsSelectionjqgridTableId).jqGrid('getGridParam', 'data');
        if (dataArray != null && dataArray.length > 0) {
            $.each(dataArray, function () {
                this.CrewCD = replaceSpecialChars(this.CrewCD);
                this.CrewName = replaceSpecialChars(this.CrewName);
                this.Notes = replaceSpecialChars(this.Notes);
                this.AddlNotes = replaceSpecialChars(this.AddlNotes);
            });
        }
        var myJsonString = "'" + JSON.stringify(dataArray) + "'";
        var params = '{GridJson: ' + myJsonString + '}';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SaveCrewInfoGrid',
            async: false,
            data: params,
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var returnResult = verifyReturnedResult(rowData);
                if (returnResult == true && rowData.d.Success == true) {
                    crewSelectionLegs = rowData.d.Result.results;
                    LoadCrewSummaryExpand();
                    setTimeout(function () {
                        HideJqGridLoader($("#gbox_CrewsSelectionID"));
                        $('#ddlAllLegs').val("0");
                    }, 2000);
                }
            },
            error: function (xhr, status, error) {
                setTimeout(function () {
                    HideJqGridLoader($("#gbox_CrewsSelectionID"));
                }, 2000);
                jAlert("Error on Saving", "Preflight - CrewInfo");
            }
        });
    }
    function Save_Validation_CrewInfo() {
        var flag = false;
        $(CrewsSelectionjqgridTableId).find('select').each(function () {
            var value = $(this).val();
            if (value == "0") {
                flag = true;
            }
        });
        if (flag) {
            jConfirm("Warning - Crew Not Assigned To Any Legs And Will Not Be Saved. Continue Without Saving Your Entry", "Preflight - CrewInfo", Save_CrewInfo);
        } else {
            Save_CrewInfo();
        }
    }
    function OrderNumbers() {
        myGrid = $(CrewsSelectionjqgridTableId);
        var rows = myGrid.jqGrid('getDataIDs');
        for (i = 0; i < rows.length; i++) {
            var rowData = myGrid.jqGrid('getRowData', rows[i]);
            myGrid.jqGrid('setCell', rowData.CrewID, 'OrderNUM', i + 1);
        }
    }

    function UpdateModel_Legs() {
        $(CrewsSelectionjqgridTableId).find('select').each(function () {
            var rowid = $(this).attr("id").replace("ddl", "");
            var columnName = $(this).attr("leg-id");
            var value = $(this).val();
            $(CrewsSelectionjqgridTableId).jqGrid('getLocalRow', rowid)[columnName] = value;
            $(CrewsSelectionjqgridTableId).jqGrid('getLocalRow', rowid)["IsDiscontinue_" + columnName] = $(this).siblings('.hdnDiscontinue').val();
        });
    }

    function AddCrews(Crews) {
        $.each(Crews, function (key, Crew) {
            if (Crew.IsStatus == "true" || Crew.IsStatus == true || Crew.IsStatus == "Yes") {
            var FindRow = $(CrewsSelectionjqgridTableId).getRowData(Crew.CrewID);
            var selRowId = $(CrewsSelectionjqgridTableId).jqGrid('getGridParam', 'selrow');
            var isInsert=document.getElementById('hdnInsert').value;
            
            Crew.tempStatus = "added";
            var fullname = "";
            if (Crew.CrewName != undefined) {
                Crew.CrewName = $.trim(Crew.CrewName).replace(",","");
                fullname = Crew.CrewName.split(" ");
            }

            var crewName = "";
            if (fullname.length > 0) {
                if (fullname.length > 2) {
                    crewName = fullname[0] + ", ";
                    crewName += fullname[1];
                    crewName += " " + fullname[2];
                }
                if (fullname.length == 2) {
                    crewName += fullname[0] + ", ";
                    crewName += fullname[1];
                }
                crewName = $.trim(crewName);
            }
            Crew.CrewName = crewName;
            if (FindRow.CrewID == undefined) {
                var parameters =
                    {
                        rowID: Crew.CrewID,
                        initdata: Crew,
                        position: "last",
                        useDefValues: false,
                        useFormatter: false,
                        addRowParams: { extraparam: {} }
                    };
                if (isInsert == "true" && selRowId != null && selRowId != undefined) {
                        $(CrewsSelectionjqgridTableId).addRowData(parameters.rowID, parameters.initdata, "before", selRowId);
                } else
                    $(CrewsSelectionjqgridTableId).jqGrid('addRow',parameters);
            }
         }
        });
        OrderNumbers();
        Save_CrewInfo();
    }
    function GetLegs() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/TripLegs',
            async: false,
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var returnResult = verifyReturnedResult(rowData);
                if (returnResult == true && rowData.d.Success == true) {
                    crewSelectionLegs = rowData.d.Result.results;
                    var width = 0;
                    if (crewSelectionLegs.length == 3) {
                        width = 0;
                    }
                    else if (crewSelectionLegs.length == 2) {
                        width = 0;
                    }
                    else if (crewSelectionLegs.length == 1) {
                        width = 0;
                        if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) { width += 0; }
                    }
                    $.each(crewSelectionLegs, function (index, Leg) {
                        var LegName = "Leg" + Leg.LegNUM + "<br/>" + Leg.LegDate + "<br/>" + Leg.LegDepart;
                        var LegNum = "Leg" + Leg.LegNUM;
                        var LegCrewList = Leg.CrewList;
                        var LegIsDiscontinueList = Leg.IsDiscontinue;
                        var fitwidthdivholder = $('<div></div>');
                        var paddingcount = 6;
                        $(fitwidthdivholder).css("display", "inline-block");

                        $(fitwidthdivholder).html(LegName);
                        $('body').append(fitwidthdivholder);
                        var fitWidth = fitwidthdivholder.width();
                        $(fitwidthdivholder).remove();
                        ColNames.push(LegName);
                        ColModel.push({
                            name: LegNum, index: LegNum, width: (fitWidth + paddingcount + width), title: false, frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                                var CrewList = LegCrewList.split(",");
                                var IsDiscontinueList = LegIsDiscontinueList.split(",");
                                var IsDiscontinue = IsDiscontinueList[rowObject["OrderNUM"] - 1];
                                var ID = CrewList[rowObject["OrderNUM"] - 1];
                                dutyTypeDropdownHtml=GetDutyTypeDPHtml(ID, IsDiscontinue, LegNum, rowObject["CrewID"])
                                return dutyTypeDropdownHtml;
                            }
                        });
                    });
                }
            },
            error: function (xhr, status, error) {
                jAlert("Error on Loading Preflight Legs", "Preflight - CrewInfo");
            }
        });
    }
    
    function GetDutyTypeDPHtml(ID, IsDiscontinue, LegNum, CrewId) {
        dutyTypeDropdownHtml = "<select  onchange='javascript:DisableandCollapsePanel(this,\"dropdown\",\"1\");' " + (IsDiscontinue == "True" ? ' style = "background-color: rgb(255, 0, 0);" ' : '') + " leg-id='" + LegNum + "'  id='ddl" + CrewId + "' data-bind='enable: EditMode'>";
        dutyTypeDropdownHtml += "<option value='0' " + ((ID == 0 || ID == "") ? "selected='selected'" : "") + ">Select</option>";
        dutyTypeDropdownHtml += "<option value='P' " + ((ID == "P" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">PIC</option>";
        dutyTypeDropdownHtml += "<option value='S' " + ((ID == "S" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">SIC</option>";
        dutyTypeDropdownHtml += "<option value='E' " + ((ID == "E" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">Engineer</option>";
        dutyTypeDropdownHtml += "<option value='I' " + ((ID == "I" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">Instructor</option>";
        dutyTypeDropdownHtml += "<option value='A' " + ((ID == "A" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">Attendant</option>";
        dutyTypeDropdownHtml += "<option value='O' " + ((ID == "O" && IsDiscontinue == "False") ? "selected='selected'" : "") + ">Other</option>";
        dutyTypeDropdownHtml += "<option value='DP'" + ((ID == "P" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-PIC</option> ";
        dutyTypeDropdownHtml += "<option value='DS'" + ((ID == "S" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-SIC</option> ";
        dutyTypeDropdownHtml += "<option value='DE'" + ((ID == "E" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-Engineer</option> ";
        dutyTypeDropdownHtml += "<option value='DA'" + ((ID == "A" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-Attendant</option> ";
        dutyTypeDropdownHtml += "<option value='DI'" + ((ID == "I" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-Instructor</option> ";
        dutyTypeDropdownHtml += "<option value='DO'" + ((ID == "O" && IsDiscontinue == "True") ? "selected='selected'" : "") + ">Disc-Other</option> ";
        dutyTypeDropdownHtml += '<input type="hidden" class="hdnDiscontinue" value= "' + (IsDiscontinue == "" ? "False" : IsDiscontinue) + '" />';
        dutyTypeDropdownHtml += '<input type="hidden" class="hdnOldValue" value="' + (ID == undefined ? "0" : ID) + '" />';
        return dutyTypeDropdownHtml;
    }
    $(document).ready(function () {
        var iswinsafari = isWindowsSafari();


        jQuery(CrewsSelectionjqgridTableId).jqGrid({
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CrewSelectionGrid',
            mtype: 'POST',
            datatype: "json",
            cache: false,
            async: false,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData._search = undefined;
                postData.nd = undefined;
                postData.size = undefined;
                postData.page = undefined;
                postData.sort = undefined;
                postData.dir = undefined;
                postData.filters = undefined;

                return JSON.stringify(postData);
            },
            height: 200,
            width: 700,
            autowidth: false,
            shrinkToFit: false,
            rowNum: 10000,
            multiselect: MultiSelectFlag,
            pager: "#pg_gridPagerCrewSelection",
            pgbuttons: false,
            viewrecords: false,
            cmTemplate: { resizable: !iswinsafari },
            pgtext: "",
            pginput: false,
            colNames: ColNames,
            colModel: ColModel,
            loadui: 'disable',
            multiselectWidth: 30,
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
            },
            onSelectRow: function (id) {
                var rowData = $(this).getRowData(id);
            },
            beforeSelectRow: function (rowid, e) {
                if (self.EditMode())
                    return true;
                else
                    return false;
            },
            gridComplete: function () {
                var Grid = $(CrewsSelectionjqgridTableId);
                var grid_ids = $(CrewsSelectionjqgridTableId).jqGrid('getDataIDs');
                for (var i = 0; i < grid_ids.length; i++) {
                    var rowid = grid_ids[i];
                    var row = Grid.getRowData(rowid);
                    if (!IsNullOrEmpty($.trim(row.Notes))) {
                        Grid.setCell(rowid, 'CrewCD', '', '', { 'title': row.Notes });
                        Grid.setCell(rowid, 'CrewName', '', '', { 'title': row.Notes });
                    }
                }
                UpdateModel_Legs();
            },
            beforeRequest: function () {
                CreateJqGridLoader($("#gbox_CrewsSelectionID"));
                ShowJqGridLoader($("#gbox_CrewsSelectionID"));
            },
            loadComplete: function (rowData) {
                EnableDisableControlsofGrid(self.EditMode());
                HideJqGridLoader($("#gbox_CrewsSelectionID"));

                var grid_ids = $(CrewsSelectionjqgridTableId).jqGrid('getDataIDs');
                for (var i = 0; i < grid_ids.length; i++) {
                    var rowid = grid_ids[i];
                    $(CrewsSelectionjqgridTableId).setSelection(rowid, false);
                }

                if (IsCrewInfoGridLoded == false) {
                    crewGridFrozenColAlign();
                } else {
                    crewGridFrozenColAlignAfercolAdd();
                }
                setscroll();
                if (isWindowsSafari()) {
                     $('.frozen-div th' + CrewsSelectionjqgridTableId + '_cb').css("width", "50px");
                     $(CrewsSelectionjqgridTableId + ' tr td:first-child').css("width", "30px");
                }
            },
            afterInsertRow: function (rowid, rowdata, rowelem) {
                $(CrewsSelectionjqgridTableId).setSelection(rowid, false);
                setscroll();
            }
        });

        $(CrewsSelectionjqgridTableId).jqGrid('setFrozenColumns');
        //$(CrewsSelectionjqgridTableId).jqGrid('filterToolbar', { autosearch: true, defaultSearch: 'cn' });
        $(CrewsSelectionjqgridTableId).jqGrid('navGrid', '#pg_gridPagerCrewSelection', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
        $(CrewsSelectionjqgridTableId).jqGrid('navButtonAdd', "#pg_gridPagerCrewSelection", {
            caption: "", title: "Delete Selected Crew(s)", buttonicon: "ui-icon ui-icon-trash",
            onClickButton: function () {
                var myGrid = $(CrewsSelectionjqgridTableId);
                var selRowIds = myGrid.jqGrid('getGridParam', 'selarrrow');
                if (selRowIds.length > 0) {

                    setAlertTextToYesNo();

                    jConfirm("Are you sure you want to delete this record?", "Confirmation", function (r) {
                        if (r) {

                            var temp = new Array();
                            jQuery.each(selRowIds, function (i, val) {
                                temp.push(val);
                            });
                            jQuery.each(temp, function (i, val) {
                                myGrid.jqGrid('delRowData', val);
                            });
                            OrderNumbers();
                            Save_CrewInfo();
                            $(CrewsSelectionjqgridTableId).jqGrid('resetSelection');
                        }
                    });
                    setAlertTextToOkCancel();
                } else {
                    jAlert("Please select the record to delete!", alertTitle);
                }
                return;
            }
        });
        $(CrewsSelectionjqgridTableId).jqGrid('navButtonAdd', "#pg_gridPagerCrewSelection", {
            caption: "", title: "Insert Crew(s)", buttonicon: "insertInto-icon-grid",
            onClickButton: function () { ShowCrewRosterPopup('insert'); }
        });
        $(CrewsSelectionjqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });//.trigger('reloadGrid');
    });


    function setscroll()
    {
        if (isWindowsSafari()) {

            if ($('#CrewsSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                $('#CrewsSelection div[class="ui-jqgrid-bdiv"]').scroll(function () {
                    if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                        $('#CrewsSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    } else {

                        $('#CrewsSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                    }
                });
            }

        }
        else if (isMacSafari()) {


            if ($('#CrewsSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                $('#CrewsSelection div[class="ui-jqgrid-bdiv"]').scroll(function () {
                    if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                        $('#CrewsSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    } else {

                        $('#CrewsSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                    }
                });
            }

        }
        else {
            
            if ($('#CrewsSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                $('#CrewsSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
            }
        }
    }

    function crewGridFrozenColAlign() {
        //if (crewSelectionLegs.length <= 3) {
        if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) {
            var mainWidth = $(".preflightcrew_selectiongrid .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width();
            if (isWindowsSafari()) {
                mainWidth += 5;
            }
            mainWidth = mainWidth + 14;
            if (crewSelectionLegs.length < 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv.ui-jqgrid-bdiv').css('height', '200px'); }
            if (crewSelectionLegs.length == 3) { mainWidth += 4; }
            if (crewSelectionLegs.length >= 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv').css('height', '182px!important'); }
            if (crewSelectionLegs.length > 3) {
                mainWidth += crewSelectionLegs.length;

            }
            $(".preflightcrew_selectiongrid .frozen-div.ui-state-default.ui-jqgrid-hdiv").width(mainWidth);
            $(".preflightcrew_selectiongrid .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width(mainWidth);
            $(".preflightcrew_selectiongrid div.frozen-bdiv, .preflightpax_information div.frozen-bdiv").width(mainWidth + 1);
            $("#CrewsSelectionID_frozen").width(mainWidth + 1);


        }

        //}

        $(CrewsSelection).find("#jqgh_CrewsSelectionID_cb").find("span").remove();
        $(CrewsSelection).find(".frozen-div table thead tr").find("#jqgh_CrewsSelectionID_cb").find("span").remove();
        $(CrewsSelection).find(".frozen-div table thead tr").find("#jqgh_CrewsSelectionID_cb").prepend("<span> Select <br/> All <br/></span>");
        $(CrewsSelection).find(".jqgfirstrow td:first-child,td[aria-describedby='CrewsSelectionID_cb']").attr("style", "width: 30px;");
        $(CrewsSelection).find("td[aria-describedby='CrewsSelectionID_IsNotified']").attr("style", "width: 36px;");
        $(CrewsSelection).find("td[aria-describedby='CrewsSelectionID_CrewName']").attr("style", "text-align: left!important;");
        $(CrewsSelection).find("th[id='CrewsSelectionID_CrewName']").attr("style", "width: 200px;border-right: solid 1px #d3d3d3 !important;");
        IsCrewInfoGridLoded = true;
    }

    function crewGridFrozenColAlignAfercolAdd() {

        if (agt.indexOf("safari") == -1 && agt.indexOf("chrome") != -1) {
            if (crewSelectionLegs.length < 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv.ui-jqgrid-bdiv').css('height', '200px'); }
            if (crewSelectionLegs.length >= 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv').css('height', '182px'); }
        }
        if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) {
            if (crewSelectionLegs.length <= 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv.ui-jqgrid-bdiv').css('height', '200px'); }
            if (crewSelectionLegs.length > 3) { $('.preflightcrew_selectiongrid div.frozen-bdiv').css('height', '182px'); }
            var mainWidth = $(".preflightcrew_selectiongrid .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width();
            $("#CrewsSelectionID_frozen").width(mainWidth + 1);
            $('.preflightcrew_selectiongrid .frozen-div.ui-state-default.ui-jqgrid-hdiv>table tr th').each(function () {
                var hWidth = $(this).width();
                var hID = $(this).attr("id");
                $('#CrewsSelectionID_frozen tr td').each(function () {
                    if ($(this).attr("aria-describedby") == hID) {
                        if ($(this).attr("aria-describedby").indexOf("_CrewCD") > 1 || $(this).attr("aria-describedby").indexOf("_CrewName") > 1) {
                            $(this).attr("style", "width: " + hWidth + "px !important;");
                            $(this).width(hWidth);
                        }
                        if ($(this).attr("aria-describedby").indexOf("_cb") > 1 || $(this).attr("aria-describedby").indexOf("_IsNotified") > 1) {
                            $(this).attr("style", "width: " + hWidth + "px !important;text-align:center!important;");
                            $(this).width(hWidth);
                        }
                    }
                });
            });
        }
    }

    // this function is used to display the aircraft type popup 
    function ShowCrewRosterPopup(ParameterCall) {

        if (ParameterCall == 'insert') {
            document.getElementById('hdnInsert').value = "true";
        } else {
            document.getElementById('hdnInsert').value = "false";
        }
        var oWnd = radopen("/Views/Transactions/Preflight/PreflightCrewRosterPopup.aspx?CrewID=" + document.getElementById('hdnCrewInfoId').value + "&CrewCD=" + document.getElementById('hdnCrewCD').value + "", "radCrewRosterPopup"); // Added for Preflight Crew Tab
        return false;
        //}
    }
    function OnradCrewRosterPopupClose(oWnd, args) {
        var arg = args.get_argument();
        if (arg != null) {
            AddCrews(arg.val);
            document.getElementById('hdnCrewInfo').value = arg.val;
            <%--// $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("RebindAndNavigate");--%>
            crewGridFrozenColAlignAfercolAdd();
        }
        return false;
    }

    function openWin(radWin) {
        var url = '';

        if (radWin == "rdCrewGroupPopup") {
            url = '/Views/Settings/People/CrewGroupPopup.aspx?CrewGroupCD=' + document.getElementById('tbCrewGroup').value;
        }

        if (url != "")
            var oWnd = radopen(url, radWin);
    }

    function CrewNotified(obj) {
        var rowid = $(obj).attr("row-id");
        $(CrewsSelectionjqgridTableId).jqGrid('getLocalRow', rowid)["IsNotified"] = $(obj).is(':checked');

        var rowData = jQuery(CrewsSelectionjqgridTableId).jqGrid('getRowData', rowid);
        if (rowData.tempStatus != "1") {
            rowData.tempStatus = "2";
        }

        Save_CrewInfo();
    }

    function DisableandCollapsePanel(obj, strtype, hdncontinue) {

        if (strtype == "dropdown" || strtype == "dropdown2") {
            var str = obj.id.toString();
            var find = '_';
            var re = new RegExp(find, 'g');
            //var newstr = str.replace(re, "$");
            var n = str;
            var CompareValue = 0;

            var rowData = jQuery(CrewsSelectionjqgridTableId).jqGrid('getRowData', obj.id.substring(3));
            if (rowData.tempStatus != "1") {
                rowData.tempStatus = "2";
            }

            if (strtype == "dropdown" && (obj.value == "DS" || obj.value == "DP" || obj.value == "DE" || obj.value == "DA" || obj.value == "DI" || obj.value == "DO") && $(obj).siblings('.hdnOldValue').val() != "0") {
                CompareValue = "1";
            }

            if (CompareValue == "0") {
                obj.style.backgroundColor = "#FFFFFF";
            }
            else {
                obj.style.backgroundColor = "#FF0000";
            }
           
            if ($(obj).siblings('.hdnOldValue').val() != "0" || ($(obj).siblings('.hdnOldValue').val() == "0" && obj.value != "D")) {
                $(obj).siblings('.hdnDiscontinue').val((obj.value == "DS" || obj.value == "DP" || obj.value == "DE" || obj.value == "DA" || obj.value == "DI" || obj.value == "DO") ? true : false);
                $(obj).siblings('.hdnOldValue').val(obj.value);                             
            } else {
                obj.value = "0";
                $(obj).siblings('.hdnDiscontinue').val(false);                
            }
            
        }
    }


</script>
<style>
    #CrewsSelection .ui-row-ltr {
        height: 32px !important;
    }

    #CrewsSelection .frozen-div {
        height: 51px !important;
        overflow: hidden;
    }

    #CrewsSelection .frozen-div th {
        height: 44px !important;
    }

    #CrewsSelection .frozen-bdiv {
        top: 52px !important;
        overflow: hidden;
    }

    #CrewsSelection .box1 {
        padding: 0px !important;
    }
</style>
<div style="float: left; text-align: left; width: 718px; padding: 5px 0 0 0px;">
    <table width="200px" cellpadding="0" cellspacing="0" class="nav_bg_preflight">
        <tr>
            <td align="left" class="tdLabel140">
                <input type="text" id="SearchBoxCrew" class="SearchBox_sc_crew" data-bind="enable: EditMode" />
            </td>
            <td align="left">
                <input type="button" id="Submit1" class="searchsubmitbutton" />
            </td>
            <td class="tdLabel520">
                <a id="lnkCrewRoster" data-bind="visible: EditMode" onclick="javascript:return ShowCrewRosterPopup('all');" class="link_small" href="javascript:return ShowCrewRosterPopup('all');">Crew Roster</a>
                <input id="hdnCrewInfo" type="hidden" />
                <input id="hdnCrewInfoId" type="hidden" />
                <input id="hdnCrewCD" type="hidden" />
                <input id="hdnInsert" type="hidden" />
            </td>
        </tr>
        <tr>
            <td class="nav-3" colspan="3"></td>
        </tr>
        <tr>
            <td>(Search by Crew Code)
            </td>
        </tr>
    </table>
</div>
<fieldset class="preflight_crewlist_grid">
    <legend>Crew</legend>
    <div style="text-align: right; float: right; width: 200px; padding: 5px 0 0 0px;">
        <table>
            <tr>
                <td class="tdLabel90" align="right">
                    <asp:Button ID="btnAddremoveAssignedCrewColumns" runat="server" Visible="false" CssClass="button"
                        Text="+/- Columns" />
                </td>
                <td class="tdLabel70">All Legs
                </td>
                <td class="tdLabel90">
                    <select id="ddlAllLegs" data-bind="enable: EditMode">
                        <option value="0">Select</option>
                        <option value="P">PIC</option>
                        <option value="S">SIC</option>
                        <option value="E">Engineer</option>
                        <option value="I">Instructor</option>
                        <option value="A">Attendant</option>
                        <option value="O">Other</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>
    <div style="float: left; width: 700px; padding: 5px 0 0 0px;">
        <div id="CrewsSelection" class="jqgrid preflightcrew_selectiongrid" data-bind="enable: EditMode">
            <div>
                <table cellpadding="0" cellspacing="0" class="box1">
                    <tr>
                        <td>
                            <table id="CrewsSelectionID" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">

                                <div role="group" id="pg_gridPagerCrewSelection" data-bind="visible: EditMode">
                                    <%--<div id="CrewSelectionUpdateLoading" style="display:none;">
                                    <input type="button" id="btnCrewGroup" tooltip="Loading" class="browse-button-Validation-Loading" onclick="javascript: return false;" />
                                 Updating 
                                </div>--%>
                                </div>

                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</fieldset>
