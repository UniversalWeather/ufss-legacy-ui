﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaxSummary.ascx.cs" Inherits="FlightPak.Web.UserControls.Preflight.Pax.PaxSummary" %>
<script type="text/javascript">
    
</script>

<div style="float: left; text-align: left; width: 718px; padding: 5px 0 0 0px;">


    <div style="float: left; width: 700px; padding: 5px 0 0 0px;">
        <div id="PaxsSummary" class="jqgrid">
            <div>
                <table cellpadding="0" cellspacing="0" class="box1 preflight_crew_classsumme">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" id="PaxsSummaryID" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPagerPaxsSummary">  <div id="UpdateLoading" style="display:none;">
                                    <input type="button" id="btnPaxGroup" tooltip="Loading" class="browse-button-Validation-Loading" onclick="javascript: return false;" />
                                 Updating 
                                </div></div>
                              
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
