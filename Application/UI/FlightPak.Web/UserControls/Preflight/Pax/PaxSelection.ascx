﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaxSelection.ascx.cs" Inherits="FlightPak.Web.UserControls.Preflight.Pax.PaxSelection" %>

<style>
    #PaxSelection .ui-row-ltr {
        height: 30px !important;
    }

    #PaxSelection .frozen-div {
        height:50px !important;
        overflow: hidden;
    }

        #PaxSelection .frozen-div th {
            height:44px !important;
        }

    #PaxSelection .frozen-bdiv {
        top:52px !important;
    }
    .frozen-bdiv.ui-jqgrid-bdiv{
        height:183px!important;
    }

    #PaxSelection .box1 {
        padding: 0px !important;
    }
      #PaxSelection #gview_PaxSelectionID{
          position:relative;
      }
    
    .keycolor{
          margin:0 5px 0;
          padding:0px;
          float:left;
          line-height:20px;
      }

    .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column
    {
        padding-bottom:0px;
    }
    .tdLabel600 .parts_color_highlighter{float:left;line-height:23px;margin:0 5px;}
    .tdLabel600 .parts_color_highlighter span{width:15px;height:15px;border-radius:50%;font-size:1px;display: inline-block;;margin-right:5px;}
    .tdLabel600 .parts_color_highlighter span.red_color{background-color:red;}
    .tdLabel600 .parts_color_highlighter span.orange_color{background-color:orange;}
    .tdLabel600 .parts_color_highlighter span.green_color{background-color:green;}
    .tdLabel600 .parts_color_highlighter span.grey_color{background-color:grey;}
</style>


  <div style="float: left; text-align: left; width: 718px; padding: 5px 0 0 0px;">
                <table width="200px" cellpadding="0" cellspacing="0" class="nav_bg_preflight">
                    <tr>
                        <td align="left"  class="tdLabel140">
                            <input id="SearchBoxPax" class="SearchBox_sc_crew" type="text" data-bind="enable: EditMode" />
                        </td>
                        <td align="left">
                            <input type="button" class="searchsubmitbutton" id="Submit1"  data-bind="enable: EditMode"/>
                        </td>
                        <td class="tdLabel520">
                            <a id="lnkPaxRoaster" href=""  onclick="javascript:return ShowPaxInfoPopup('all');" class="link_small" data-bind="visible: EditMode">PAX Table</a>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="nav-3" colspan="3">                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2>
                            (Search by Passenger Code)
                        </td>
                        <td><div style="padding-right:10px" id="lblMsg"></div></td>
                    </tr>
                </table>
            </div>
<fieldset>
    <legend>PAX</legend>
    <div class="wraper_verification" style="text-align: right; padding: 5px 0 0 0px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="tdLabel140">TSA Verification No.
                </td>
                <td class="tdLabel180">
                    <input id="tbTsaVerfiNum" class="text150 textbox_verifications" maxlength="40" data-bind="enable: EditMode, value: self.Preflight.PreflightMain.VerifyNUM" />
                </td>
                <td class="tdLabel50" data-bind="visible: self.Preflight.PreflightMain.Company.IsEnableTSA() == false">Status
                </td>
                <td class="tdLabel180">
                      <input id="tbStaus" class="tdLabel160 textbox_verifications" maxlength="40" readonly="readonly" data-bind="enable: EditMode, visible: self.Preflight.PreflightMain.Company.IsEnableTSA() == false" />
                </td>
                <td class="tdLabel60">All Legs
                </td>
                <td>
                    <select id="ddlFlightPurpose" data-bind="enable: EditMode" >
                        <option value="0" selected="selected">All Legs</option>
                        <option value="0" >Select</option>
                    </select>
                </td>
                <td class="tdLabel87" align="right">
                    <%--<asp:Button ID="btnAddremoveColumns" runat="server" Visible="false" CssClass="button"
                        Text="+/- Columns" />--%>
                </td>
            </tr>
        </table>
    </div>
    <div style="float: left; width: 700px; padding: 5px 0 0 0px;">
        <div id="PaxSelection" class="jqgrid preflightpax_selectionlist" data-bind="enable: EditMode">
            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <table id="PaxSelectionID" class="table table-striped table-hover table-bordered preflight_paxselection_grid"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">

                                <div role="group" id="pg_gridPagerPaxSelection" data-bind="visible: EditMode">
                                    <%--<div id="PaxSelectionUpdateLoading" style="display: none;height:25px">
                                        <input type="button" id="btnPaxGroup" tooltip="Loading" class="browse-button-Validation-Loading" onclick="javascript: return false;" />
                                        Updating 
                                    </div>--%>
                                </div>

                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="float: left; width: 700px; padding: 5px 0 0 7px;">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="tdLabel600">
                    <div data-bind="visible: self.Preflight.PreflightMain.Company.IsEnableTSA() == true">
                        <p class="keycolor">
                            Key Color:
				        </p>
                        <div class="parts_color_highlighter">
                            <span class="red_color">&nbsp;</span>TSA No Fly
				        </div>
                        <div class="parts_color_highlighter">
                            <span class="orange_color">&nbsp;</span>TSA Selectee
                        </div>
                        <div class="parts_color_highlighter">
                            <span class="green_color">&nbsp;</span>TSA Cleared
                        </div>
                        <div class="parts_color_highlighter">
                            <span class="grey_color">&nbsp;</span>TSA N/A
                        </div>
                    </div>
                </td>
                <td class="tdtext100" align="left">
                    <input id="btnCheckTsa" type="button" value="Check TSA" data-bind="enable: EditMode, visible: self.Preflight.PreflightMain.Company.IsEnableTSA() == false, css: EditMode() == true ? 'button' : 'button-disable'" onclick="    CheckTSAClick();" />
                </td>
            </tr>
        </table>
    </div>

</fieldset>
