﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CorporateRequestService;
using Telerik.Web.UI;
namespace FlightPak.Web.UserControls
{
    public partial class UCCorporateRequestSearch : System.Web.UI.UserControl
    {
        private delegate void SeeAllCorporateReqeustSaveClick();
        private delegate void SeeAllCorporateReqeustCancelClick();

        private Delegate _SelectTrip;
        public Delegate SelectTrip
        {
            set { _SelectTrip = value; }
        }



        private Delegate _EditClick;
        public Delegate EditClick
        {
            set { _EditClick = value; }
        }

        public Int64 CRMainID
        {
            get { return Convert.ToInt64(lblRequestNo.Text); }
            set { lblRequestNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        public Int64 CRTripNUM
        {
            get { return Convert.ToInt64(lblRequestNumber.Text); }
            set { lblRequestNumber.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }
        public string TailNo
        {
            get { return lblTailNo.Text; }
            set { lblTailNo.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }

        //2955
        public string TypeCode
        {
            get { return lblTypeCode.Text; }
            set { lblTypeCode.Text = System.Web.HttpUtility.HtmlEncode(value.ToString()); }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            SeeAllCorporateReqeustSaveClick seeallsave = new SeeAllCorporateReqeustSaveClick(saveclick);

            SeeAllCorporateReqeustCancelClick seeallcancel = new SeeAllCorporateReqeustCancelClick(cancelclick);
            if (!IsPostBack)
            {
                lbTripSearchMessage.Text = string.Empty;
            }
        }



        protected void page_prerender(object sender, EventArgs e)
        {
            CRMain CorpRequest = new CRMain();

            if (Session["CurrentCorporateRequest"] != null)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                hdnCRTripID.Value = CorpRequest.CRMainID.ToString();
                lnkHistory.Enabled = true;
                lnkHistory.CssClass = "history-icon";
                lnkHistory.OnClientClick = "javascript:openWin('rdHistory');return false;";
            }
            else
            {
                lnkHistory.Enabled = false;
                lnkHistory.OnClientClick = "";
                lnkHistory.CssClass = "history-icon-disable";
            }


        }


        protected void btnOpenPopup_Click(object sender, EventArgs e)
        {
            radwindowPopup.VisibleOnPageLoad = true;
        }
        //protected void btnSaveDesc_Click(object sender, EventArgs e)
        //{
        //    if (Session["CurrentCorporateRequest"] != null)
        //    {
        //        CRMain CorpRequest = new CRMain();
        //        Trip.Mode = TripActionMode.Edit;
        //        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
        //        Trip.RevisionDescriptioin = tbDescription.Text;
        //        Session["CurrentCorporateRequest"]= Trip;
        //    }
        //    radwindowPopup.VisibleOnPageLoad = false;            
        //}

        //protected void btnCancelDesc_Click(object sender, EventArgs e)
        //{
        //    if (Session["CurrentCorporateRequest"]!= null)
        //    {
        //        PreflightMain Trip = new PreflightMain();
        //        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
        //        tbDescription.Text = Trip.RevisionDescriptioin;
        //    }
        //    radwindowPopup.VisibleOnPageLoad = false;          
        //}

        protected void btnOpenSeeAll_Click(object sender, EventArgs e)
        {

        }

        protected void Search_Click(object sender, EventArgs e)
        {
            lbTripSearchMessage.Text = string.Empty;
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                CRMain CorpRequest = new CRMain();

                if (Session["CurrentCorporateRequest"] != null)
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                if (CorpRequest != null)
                {
                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request Unsaved, please Save or Cancel Request', 330, 110);");
                    }
                    else
                    {
                        Int64 SearchTripNum = 0;
                        if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                        {
                            using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                            {
                                var objRetVal = CorpSvc.GetRequestByTripNum(SearchTripNum);
                                if (objRetVal.ReturnFlag)
                                {
                                    CorpRequest = objRetVal.EntityList[0];
                                    Session["CurrentCorporateRequest"] = CorpRequest;

                                    CRMainID = CorpRequest.CRMainID;
                                    CRTripNUM = (long)CorpRequest.CRTripNUM;
                                    TailNo = CorpRequest.Fleet.TailNum;
                                    if (CorpRequest.Aircraft != null && CorpRequest.Aircraft.AircraftCD != null)
                                        TypeCode = CorpRequest.Aircraft.AircraftCD;
                                    var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                    if (ObjretVal.ReturnFlag)
                                    {
                                        Session["CorporateRequestException"] = ObjretVal.EntityList;
                                    }
                                    else
                                        Session["CorporateRequestException"] = null;
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                }
                                else
                                {
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                                }
                            }
                        }
                        else
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                    }
                }
                else
                {
                    Int64 SearchTripNum = 0;
                    if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                    {
                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            var objRetVal = CorpSvc.GetRequestByTripNum(SearchTripNum);
                            if (objRetVal.ReturnFlag)
                            {
                                CorpRequest = objRetVal.EntityList[0];
                                CRMainID = CorpRequest.CRMainID;
                                CRTripNUM = (long)CorpRequest.CRTripNUM;
                                TailNo = CorpRequest.Fleet.TailNum;
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session["CorporateRequestException"] = null;

                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                            }
                            else
                            {
                                //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                                lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Request No. Does Not Exist");
                            }
                        }
                    }
                }
            }
            else
            {
                lbTripSearchMessage.Text = string.Empty;
            }
        }

        protected void SearchBox_TextChanged(object sender, EventArgs e)
        {
            lbTripSearchMessage.Text = string.Empty;
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                CRMain CorpRequest = new CRMain();
                if (Session["CurrentCorporateRequest"] != null)
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                if (CorpRequest != null)
                {
                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {
                        //lbTripSearchMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip Unsaved, please Save or Cancel Trip', 330, 110);");

                    }
                    else
                    {
                        Int64 SearchTripNum = 0;
                        if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                        {
                            using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                            {
                                var objRetVal = CorpSvc.GetRequestByTripNum(SearchTripNum);
                                if (objRetVal.ReturnFlag)
                                {
                                    CorpRequest = objRetVal.EntityList[0];
                                    CRMainID = CorpRequest.CRMainID;
                                    CRTripNUM = (long)CorpRequest.CRTripNUM;
                                    if (CorpRequest.Fleet != null)
                                    {
                                        TailNo = CorpRequest.Fleet.TailNum;
                                    }
                                    if (CorpRequest.Aircraft != null)
                                    {
                                        TypeCode = CorpRequest.Aircraft.AircraftCD;
                                    }
                                    Session["CurrentCorporateRequest"] = CorpRequest;
                                    var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                    if (ObjretVal.ReturnFlag)
                                    {
                                        Session["CorporateRequestException"] = ObjretVal.EntityList;
                                    }
                                    else
                                        Session["CorporateRequestException"] = null;
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                }
                                else
                                {
                                    //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                                    lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Request No. Does Not Exist");
                                }
                            }
                        }
                        else
                        {
                            lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Request No. Does Not Exist");
                            //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                        }
                    }
                }
                else
                {
                    Int64 SearchTripNum = 0;
                    if (Int64.TryParse(SearchBox.Text, out SearchTripNum))
                    {
                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            var objRetVal = CorpSvc.GetRequestByTripNum(SearchTripNum);
                            if (objRetVal.ReturnFlag)
                            {
                                CorpRequest = objRetVal.EntityList[0];
                                CRMainID = CorpRequest.CRMainID;
                                CRTripNUM = (long)CorpRequest.CRTripNUM;
                                TailNo = CorpRequest.Fleet.TailNum;
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session["CorporateRequestException"] = null;

                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                            }
                            else
                            {
                                lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Request No. Does Not Exist");
                                //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");

                            }
                        }
                    }
                    else
                    {
                        lbTripSearchMessage.Text = System.Web.HttpUtility.HtmlEncode("Request No. Does Not Exist");
                        //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Request No. Does Not Exist', 330, 110);");
                    }
                }

            }
            else
            {
                lbTripSearchMessage.Text = string.Empty;
            }

        }

        protected void TripEdit_Click(object sender, EventArgs e)
        {

        }

        protected void saveclick()
        {
            _SelectTrip.DynamicInvoke();
        }
        protected void cancelclick()
        {

        }
    }
}
