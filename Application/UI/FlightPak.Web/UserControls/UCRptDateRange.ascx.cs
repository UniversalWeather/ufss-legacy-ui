﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Base;

namespace FlightPak.Web.UserControls
{
    public partial class UCRptDateRange : BaseSecuredUserControl
    {
        private Boolean blnRequired;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Declare default format
                string dateFormat = "MM/dd/yyyy";

                if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                    dateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                // Assign into Date Picker
                RadDatePicker1.DateInput.DateFormat = dateFormat;
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {
            rfvDate.Enabled = blnRequired;
            rfvDate.ValidationGroup = strValidationgroup;
        }

        public Boolean Required
        {
            get { return blnRequired; }
            set { blnRequired = value; }
        }

        private string strValidationgroup;
        public string DateValidationgroup
        {
            get { return strValidationgroup; }
            set { strValidationgroup = value; }
        }
    }
}