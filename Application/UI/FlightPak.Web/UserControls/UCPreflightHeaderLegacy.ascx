﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreflightHeaderLegacy.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCPreflightHeaderLegacy" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
<script type="text/javascript">
    function TripPAXCREWNotAssignedCallBackFn(arg) {
        if (arg == true) {
            document.getElementById('<%=btnYes.ClientID%>').click();
        }
        else {
            document.getElementById('<%=btnNo.ClientID%>').click;
        }
    }

    function alertCallBackfn(arg) {
        document.getElementById('<%=btnNo.ClientID%>').click();
    }

</script>
<table id="Header1" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <asp:Button ID="btnNewTrip" runat="server" Text="New Trip" OnClick="btNewTrip_Click"
                CssClass="ui_nav" />
            <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Trip"
                OnClick="btnDelete_Click" CssClass="ui_nav" />
            <asp:Button ID="btnEditTrip" runat="server" Text="Edit Trip" OnClick="btnEditTrip_Click"
                CssClass="ui_nav" />
            <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                OnClick="btnCancel_Click" CssClass="ui_nav" />
            <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                ValidationGroup="Save" CssClass="ui_nav" />
            <asp:Button ID="btnCopyTrip" runat="server" Text="Copy Trip" CssClass="ui_nav" OnClientClick="javascript:openWin('rdCopyTrip');return false;" />
            <asp:Button ID="btnMoveTrip" runat="server" Text="Move Leg" CssClass="ui_nav" OnClientClick="javascript:openWin('rdMoveTrip');return false;" />
        </td>
        <td align="center">
            <asp:Label runat="server" ID="lbMessage" Text="" ForeColor="red"></asp:Label>
        </td>
        <td align="right">
            <asp:Button ID="btnLog" runat="server" Text="Log" OnClick="btLog_Click" CssClass="ui_nav" />
            <asp:Button ID="btnSifl" runat="server" Text="SIFL" OnClientClick="javascript:openWin('rdSIFL');return false;"
                CssClass="ui_nav" />
            <asp:Button ID="btnApis" runat="server" Text="APIS" CssClass="ui_nav" OnClientClick="javascript:openWin('rdPreflightAPIS');return false;" />
            <asp:Button ID="btnTravelSense" runat="server" Text="Travel$ense" CssClass="ui_nav"
                OnClientClick="javascript:openWin('rdTravelSense');return false;" />
            <asp:Button ID="btnEmail" runat="server" Text="E-mail" CssClass="ui_nav" OnClientClick="javascript:openWin('rdPreflightEMAIL');return false;" />
        </td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="rtPreFlight" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            Align="Justify" OnTabClick="rtPreFlight_Clicked">
                            <Tabs>
                                <telerik:RadTab Value="PreflightMain.aspx?seltab=PreFlight" Text="Preflight" Selected="true">
                                </telerik:RadTab>
                                <telerik:RadTab Value="PreflightLegs.aspx?seltab=Legs" Text="Legs">
                                </telerik:RadTab>
                                <telerik:RadTab Value="PreflightCrewInfo.aspx?seltab=Crew" Text="Crew">
                                </telerik:RadTab>
                                <telerik:RadTab Value="PreflightPax.aspx?seltab=Pax" Text="PAX">
                                </telerik:RadTab>
                                <telerik:RadTab Value="PreflightLogistics.aspx?seltab=Logistics" Text="Logistics">
                                </telerik:RadTab>
                                <telerik:RadTab Value="PreflightUVServices.aspx?seltab=UWA" Text="UWA Services">
                                </telerik:RadTab>
                               
                                <telerik:RadTab Visible="false" Value="PreflightCR.aspx?seltab=CR" Text="Corporate Request">
                                </telerik:RadTab>
                                 <telerik:RadTab  Value="PreflightReports.aspx?seltab=Reports" Text="Reports">
                                </telerik:RadTab>
                                 <telerik:RadTab Visible="False" Value="PreflightTripsheetReporWriter.aspx?seltab=TRW" Text="Tripsheet Report Writer">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="tblHidden" style="display: none;">
    <tr>
        <td>
            <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
            <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
        </td>
    </tr>
</table>
