﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CorporateRequestService;

namespace FlightPak.Web.UserControls
{
    public partial class UCCorporateRequestHeader : System.Web.UI.UserControl
    {
        //Replicate Save Cancel Delete Buttons in header
        public delegate void ButtonClick(object sender, EventArgs e);

        private Delegate _SaveCRToSession;
        public Delegate SaveCRToSession
        {
            set { _SaveCRToSession = value; }
        }



        private Delegate _SaveToSessionPAX;
        public Delegate SaveToSessionPAX
        {
            set { _SaveToSessionPAX = value; }
        }

        private Delegate _SaveToSessionmain;
        public Delegate SaveToSessionmain
        {
            set { _SaveToSessionmain = value; }
        }
        //Replicate Save Cancel Delete Buttons in header
        public Button DeleteTripButton { get { return this.btnDeleteTrip; } }
        public Button CancelTripButton { get { return this.btnCancel; } }
        public Button SaveTripButton { get { return this.btnSave; } }


        public Button NewTripButton { get { return this.btnNewCR; } }
        public Button EditTripButton { get { return this.btnEditCR; } }
        public Button CopyTripButton { get { return this.btnCopyCR; } }
        public Button ChangeQueue { get { return this.btnChangeQueue; } }
        public RadTabStrip HeaderTab { get { return this.rtCorpRequest; } }
        public Label HeaderMessage
        {
            get { return this.lbMessage; }
        }

        public Label LastModified
        {
            get { return this.lblLastModified; }
        }

        private Delegate _NewClick;
        public Delegate NewClick
        {
            set { _NewClick = value; }
        }

        private Delegate _EditClick;
        public Delegate EditClick
        {
            set { _EditClick = value; }
        }


        private Delegate _LogClick;
        public Delegate LogClick
        {
            set { _LogClick = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (CorporateRequestService.CorporateRequestServiceClient ObjService = new CorporateRequestService.CorporateRequestServiceClient())
            {
                var ObjRetVal = ObjService.GetCRChangeQueue();
                List<CorporateRequestService.GetAllCRMain> lstAccount = new List<CorporateRequestService.GetAllCRMain>();
                if (ObjRetVal.ReturnFlag == true)
                {
                    lstAccount = ObjRetVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                }
                if (lstAccount.Count > 0)
                {
                    btnChangeQueue.CssClass = "ui_nav_alert";
                }
            }
        }

        protected void btnNewCR_Click(object sender, EventArgs e)
        {
            if (Session["CorpAvailablePaxList"] != null)
                Session["CorpAvailablePaxList"] = null;
            if (Session["CorpPaxSummaryList"] != null)
                Session["CorpPaxSummaryList"] = null;
            _NewClick.DynamicInvoke();
        }

        protected void btnEditCR_Click(object sender, EventArgs e)
        {
            //Replicate Save Cancel Delete Buttons in header
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnDeleteTrip.Enabled = false;


            btnSave.CssClass = "ui_nav";
            btnCancel.CssClass = "ui_nav";
            btnDeleteTrip.CssClass = "ui_nav_disable";


            _EditClick.DynamicInvoke();
        }



        protected void Yes_Click(object sender, EventArgs e)
        {           
            Session["CorpPAXnotAssigned"] = null;
            if (_SaveToSessionPAX != null)
                _SaveToSessionPAX.DynamicInvoke();
            Session["CorpMainDateCheck"] = null;
            if (_SaveToSessionmain != null)
                _SaveToSessionmain.DynamicInvoke();
            if (Session["CorpAvailablePaxList"] != null)
                Session["CorpAvailablePaxList"] = null;
            if (Session["CorpPaxSummaryList"] != null)
                Session["CorpPaxSummaryList"] = null;
            redirecttotab(Session["CorpPageToNavigate"].ToString());
        }
        protected void No_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }



        protected void btChangeQueue_Click(object sender, EventArgs e)
        {
            //_LogClick.DynamicInvoke();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["seltab"] != null)
            {
                string selectedItemValue = Request.QueryString["seltab"];
                RadTab selectedTab = new RadTab();
                if (selectedItemValue.ToLower() == "pax")
                    selectedItemValue = "PAX";
                else if (selectedItemValue.ToLower() == "reports")
                    selectedItemValue = "Reports";

                selectedTab = rtCorpRequest.FindTabByText(Microsoft.Security.Application.Encoder.HtmlEncode(selectedItemValue));
              

                if (selectedTab != null)
                {
                    selectedTab.Selected = true;
                    while ((selectedTab != null) &&
                           (selectedTab.Parent.GetType() == typeof(RadTab)))
                    {
                        selectedTab = (RadTab)selectedTab.Parent;
                        selectedTab.Selected = true;
                    }
                }
            }
        }

        protected void redirecttotab(string selectedtab)
        {
            var basePage = this.Page as BasePage;
            
            CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            if (CorpRequest != null)            
            {
                basePage.RedirectToPage(Microsoft.Security.Application.Encoder.UrlEncode(selectedtab));
            }
            else if (selectedtab.ToLower() == "corporaterequestreports.aspx?seltab=reports" ||
                     selectedtab.ToLower() == "corporaterequestmain.aspx?seltab=corporaterequest" ||
                     selectedtab.ToLower() == "corporaterequestlegs.aspx?seltab=legs" ||
                     selectedtab.ToLower() == "corporaterequestpax.aspx?seltab=pax" ||
                     selectedtab.ToLower() == "corporaterequestlogistics.aspx?seltab=logistics"
                    )
            {
                basePage.RedirectToPage(Microsoft.Security.Application.Encoder.UrlEncode(selectedtab));
            }
            else
                rtCorpRequest.FindTabByText("Request").Selected = true;
        }


        protected void rtCorpRequest_Clicked(object sender, RadTabStripEventArgs e)
        {
            Session["CorpPageToNavigate"] = e.Tab.Value;
            if (_SaveCRToSession != null)
                _SaveCRToSession.DynamicInvoke();

            if (Session["CurrentCorporateRequest"] != null)
            {
                CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                {
                    FlightPak.Web.Framework.Masters.CorporateRequest MasterClass = (FlightPak.Web.Framework.Masters.CorporateRequest)Page.Master;
                    MasterClass.ValidationRequest(CorporateRequestService.Group.Main);
                }             
            }

            if (Session["CorpMainDateCheck"] != null)
            {
                if (Session["CorpMainDateCheck"] != null)
                    //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add("alert('" + Session["CorpMainDateCheck"] + "')");
                    RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(@"radalert('" + Session["CorpMainDateCheck"] + "', 360, 50,'Request Alert');");
               // RadAjaxManager.GetCurrent(Page).ResponseScripts.Add("Sys.Application.add_load(function(){radalert('" + Session["CorpMainDateCheck"] + "', 330, 210);})"); 
                Session["CorpMainDateCheck"] = null;
            }
            //else
            //{
            //    if (_SaveToSessionmain != null)
            //        _SaveToSessionmain.DynamicInvoke();

            //    redirecttotab(e.Tab.Value);
            //}

            else if (Session["CorpPAXnotAssigned"] != null)
            {
                if (Session["CorpPAXnotAssigned"] != null)
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('" + Session["CorpPAXnotAssigned"] + "',TripPAXNotAssignedCallBackFn, 330, 110,null,'Confirmation!');");
                Session["CorpPAXnotAssigned"] = null;
            }
            else
            {
                if (_SaveToSessionmain != null)
                    _SaveToSessionmain.DynamicInvoke();

                if (_SaveToSessionPAX != null)
                    _SaveToSessionPAX.DynamicInvoke();

                redirecttotab(e.Tab.Value);
            }            
        }

        //Replicate Save Cancel Delete Buttons in header
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (DeleteClick != null)
                {
                    DeleteClick(sender, e);
                }
            }
        }

        //Replicate Save Cancel Delete Buttons in header
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
            {
                CancelClick(sender, e);
            }
        }

        //Replicate Save Cancel Delete Buttons in header
        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (SaveClick != null)
                {
                    SaveClick(sender, e);
                }
            }

        }

        //Replicate Save Cancel Delete Buttons in header
        public event ButtonClick SaveClick;
        public event ButtonClick DeleteClick;
        public event ButtonClick CancelClick;
    }
}