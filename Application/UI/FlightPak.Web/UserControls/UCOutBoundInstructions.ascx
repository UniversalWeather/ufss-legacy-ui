﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCOutBoundInstructions.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCOutBoundInstructions" %>
<table id="OutboundInstructions" style="padding-top: 10px;" width="100%">
    <tr>
        <td colspan="2">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tdLabel40">
                        <b>leg 1:</b>
                    </td>
                    <td class="tdLabel70">
                        Depart ICAO
                    </td>
                    <td class="tdLabel80">
                        <asp:Label ID="lblDepartICAO" runat="server" Visible="true"></asp:Label>
                    </td>
                    <td class="tdLabel70">
                        Arrival ICAO
                    </td>
                    <td>
                        <asp:Label ID="lblArrivalICAO" runat="server" Visible="true"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div class="nav-6">
            </div>
        </td>
    </tr>
    <tr>
        <td class="tdLabel80">
            Coffee
        </td>
        <td>
            <asp:TextBox ID="tbCoffee" runat="server" CssClass="text455"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Newspaper
        </td>
        <td>
            <asp:TextBox ID="tbNewspaper" runat="server" CssClass="text455"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Juice
        </td>
        <td>
            <asp:TextBox ID="tbJuice" runat="server" CssClass="text455"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Fuel Load
        </td>
        <td>
            <asp:TextBox ID="tbFuelLoad" runat="server" CssClass="text80"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Fuel
        </td>
        <td>
            <asp:TextBox ID="tbFuel" runat="server" CssClass="text200"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Comments
        </td>
        <td>
            <asp:TextBox ID="tbComments" runat="server" TextMode="MultiLine" CssClass="textarea310x50"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td colspan="2" align="left">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tdLabel180" align="right">
                        <asp:Button ID="btnCancel" OnClick="Cancel_click" runat="server" Text="Cancel" CssClass="button" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" OnClick="Save_click" runat="server" Text="Save" CssClass="button" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
