﻿<%@ Control Language="C#" AutoEventWireup="true" ClassName="Calendar" CodeBehind="UCScheduleCalendarHeader.ascx.cs"
    Inherits="FlightPak.Web.UserControls.ScheduleCalendarHeader" %>
<script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
<script type="text/javascript">
    document.getElementById("anchorCalendar").setAttribute("class", "selector-active");

    function UCHeaderPleaseWait() {

        document.getElementById("<%=UCHeaderPleaseWait.ClientID %>").style.display = 'block';
    }
</script>
<div runat="server" id="UCHeaderPleaseWait" class="pageloader">
</div>
<telerik:RadTabStrip ID="rtSchedulingCalendar" runat="server" Skin="Simple" ReorderTabsOnSelect="true" CssClass="tabSchedulingCalendar"
    Align="Justify" OnTabClick="rtSchedulingCalendar_Clicked" Width="550px" onclick="javascript:UCHeaderPleaseWait();">
    <Tabs>
        <telerik:RadTab Value="Weekly" Text="Weekly" Selected="true" TabIndex="1">
        </telerik:RadTab>
        <telerik:RadTab Value="Monthly" Text="Monthly" TabIndex="2">
        </telerik:RadTab>
        <telerik:RadTab Value="Day" Text="Day" TabIndex="3">
        </telerik:RadTab>
        <telerik:RadTab Value="Planner" Text="Planner" TabIndex="4">
        </telerik:RadTab>
        <telerik:RadTab Value="BusinessWeek" Text="Business Week" TabIndex="5">
        </telerik:RadTab>
        <telerik:RadTab Value="Corporate" Text="Corporate" TabIndex="6">
        </telerik:RadTab>
        <telerik:RadTab Value="Reports" Text="Reports" TabIndex="7">
        </telerik:RadTab>       
    </Tabs>
</telerik:RadTabStrip>
