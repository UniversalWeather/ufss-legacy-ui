﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.UserControls
{
    public partial class UCRptMonthYear : System.Web.UI.UserControl
    {
        public Boolean isyear = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadYear();
            }
        }

        private void LoadYear()
        {
            Int32 year = DateTime.Now.Year;

            for (int i = year - 32; i < year + 39; i++)
            {
                ddlYear.Items.Add(i.ToString());
            }

            ListItem selectedListItem = ddlYear.Items.FindByValue(year.ToString());

            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            };

        }


        public Boolean IsYear
        {
            set { isyear = value; }
        }  
    }
}