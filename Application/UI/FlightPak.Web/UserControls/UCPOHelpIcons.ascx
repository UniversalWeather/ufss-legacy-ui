﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPOHelpIcons.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCPOHelpIcons" %>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <span class="prfl-nav-icons">
                <a href="#" id="lnkReport" class="link-report" title="Preview Report" data-bind="click: lnkReportPreviewClick, enable: ((POLogID() != undefined || POLogID() != null || POLogID() != 0) && (POEditMode()==false)), css: FlightLogIcon"/>
                <a href="javascript:void(0)" id="lnkHistory" title="History" style="margin:0;" onclick='javascript:openPostflightWinShared("rdHistory");return false;' data-bind="css: HistoryIcon"></a>
                <a href="../../Help/ViewHelp.aspx?Screen=Postflight" class="help-icon" target="_blank" title="Help"></a>
            </span>
        </td>
    </tr>
</table>