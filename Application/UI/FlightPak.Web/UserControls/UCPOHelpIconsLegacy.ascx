﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPOHelpIconsLegacy.ascx.cs" Inherits="FlightPak.Web.UserControls.UCPOHelpIconsLegacy" %>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <span class="prfl-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=Postflight"
                class="help-icon" target="_blank" title="Help"></a>
                <asp:LinkButton CssClass="history-icon" ID="lnkHistory" runat="server" OnClientClick="javascript:openPostflightWinShared('rdHistory');return false;"
                    ToolTip="History"></asp:LinkButton>
                <asp:LinkButton ID="lnkReport" runat="server" CssClass="print_preview_icon" ToolTip="Preview Report" OnClick="lnkReport_Click"
                    CausesValidation="false"></asp:LinkButton>
            </span>
        </td>
    </tr>
</table>
<table style="display: none;">
    <tr>
        <td>
            <asp:TextBox ID="tbLogNum" runat="server"></asp:TextBox>
            <asp:TextBox ID="tbLogID" runat="server"></asp:TextBox>
        </td>
    </tr>
</table>
