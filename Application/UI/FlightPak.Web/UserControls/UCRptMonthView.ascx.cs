﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.UserControls
{
    public partial class UCRptMonthView : System.Web.UI.UserControl
    {
        private Boolean blnRequired;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Page_Init(object sender, EventArgs e)
        {
            rfvMonthView.Enabled = blnRequired;
        }

        public Boolean Required
        {
            get { return blnRequired; }
            set { blnRequired = value; }
        }


    }
}