﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using System.Data.SqlClient;
using System.Data;
using Telerik.Web.UI;
using System.Configuration;
using FlightPak.Web.PreflightService;

namespace FlightPak.Web.UserControls
{
    public partial class UCFleetTreeView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            fill_Tree();
        }
        
        private void fill_Tree()
        {
          
            using (var preflightServiceClient = new MasterCatalogServiceClient())
            {
                var fleetList = preflightServiceClient.GetFleetProfileList().EntityList;
                //create parent node
                RadTreeNode parentNode = new RadTreeNode();

                //create child node
                RadTreeNode childNode = new RadTreeNode();
                //loop through each category and get the qualification descriptions in each category
               
                    parentNode = new RadTreeNode();
                    parentNode.Text = "Entire Fleet";
                    parentNode.Value = "Entire Fleet";

                    //loop through each qualification description and add to the parent node
                    foreach (var fleet in fleetList)
                    {
                        childNode = new RadTreeNode();
                        childNode.Text = fleet.TailNum + " - " + fleet.AircraftCD;
                        childNode.Value = fleet.TailNum;
                        parentNode.Nodes.Add(childNode);
                    }
                    //add each parent node to the tree view
                    parentNode.ToolTip = "Click to see subcategories";

                    FleetTree.Nodes.Add(parentNode);
                    FleetTree.CheckBoxes = true;
                    FleetTree.CheckChildNodes = true;
                
            }
           

           

        }
    }
}