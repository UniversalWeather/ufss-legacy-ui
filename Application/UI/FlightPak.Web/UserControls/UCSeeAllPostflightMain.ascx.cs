﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Web.PostflightService;

namespace FlightPak.Web.UserControls
{
    public partial class UCSeeAllPostflightMain : System.Web.UI.UserControl
    {
        private Delegate _saveClick;
        private Delegate _cancelClick;

        public Delegate SaveClick
        {
            set { _saveClick = value; }
        }

        public Delegate CancelClick
        {
            set { _cancelClick = value; }
        }

        private string TripID;
        
        /// <summary>
        /// Wire up the SavePostflightToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            //Added for Reassign the Selected Value and highlight the specified row in the Grid
            try
            {

                if (Request.QueryString["TripID"] != null)
                {
                    TripID = Request.QueryString["TripID"];
                }
            }
            catch (System.NullReferenceException ex)
            {
                //Manually Handled
                TripID = string.Empty;
            }

        }
        /// <summary>
        /// Bind Postflight Trips into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgPostflightMainDetails_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
            {
                var ObjRetval = objService.GetPostflightTrips(0, 0, 0, false, false, DateTime.MinValue); //.GetPostFlightSearchList();
                if (ObjRetval.ReturnFlag)
                {
                    dgSearch.DataSource = ObjRetval.EntityList;
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgSearch;
            SelectItem();
        }
        private void SelectItem()
        {

            if (TripID != null || TripID != string.Empty)
            {

                foreach (GridDataItem item in dgSearch.MasterTableView.Items)
                {
                    if (item["TripID"].Text.Trim() == TripID)
                    {
                        item.Selected = true;
                    }

                }
            }
        }

        protected void Save_click(object sender, EventArgs e)
        {
            _saveClick.DynamicInvoke();
        }
        protected void Cancel_click(object sender, EventArgs e)
        {
            _cancelClick.DynamicInvoke();
        }
        protected void dgSearch_SeelctedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    foreach (GridDataItem item in dgSearch.MasterTableView.Items)
                    {
                        if (item.Selected)
                        {
                            PostflightService.PostflightMain obj = new PostflightService.PostflightMain();
                            obj.POLogID = Convert.ToInt64(item.GetDataKeyValue("POLogID").ToString());

                            var result = objService.GetTrip(obj);
                            Session["POSTFLIGHTMAIN"] = (PostflightService.PostflightMain)((List<PostflightService.PostflightMain>)result.EntityList.ToList())[0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Manually Handled
            }

        }
    }
}