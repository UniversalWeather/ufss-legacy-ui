﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.UserControls
{
    public partial class UCReportHtmlSelection : System.Web.UI.UserControl
    {
        private Dictionary<string, string> _dicColumnList = new Dictionary<string, string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadListBoxSource.DataSource = _dicColumnList;
                RadListBoxSource.DataValueField = "Key";
                RadListBoxSource.DataTextField = "Value";
                RadListBoxSource.DataBind();
            }
    
        }

        public Dictionary<string, string> ColumnList
        {
            set { _dicColumnList = value; }
        }      
    }
}