﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCorporateRequestSearch.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCCorporateRequestSearch" %>
<script type="text/javascript">

    function maxLength(field, maxChars) {
        if (field.value.length >= maxChars) {
            event.returnValue = false;
            return false;
        }
    }

    function maxLengthPaste(field, maxChars) {
        event.returnValue = false;
        if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
            return false;
        }
        event.returnValue = true;
    }
    function CloseAndRebindSelect() {
        window.location.reload();
    }
</script>
<telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="240px"
    Width="380px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None"
    Title="Description">
    <ContentTemplate>
        <div style="margin: 10px">
            <table cellpadding="2px" cellspacing="2px">
                <%--<tr>
                    <td align="left">
                        Description can be upto 200 characters
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="tbDescription" runat="server" CssClass="textarea310x100" TextMode="MultiLine"
                            AutoPostBack="true" onKeyPress='return maxLength(this,"200")' onpaste='return maxLengthPaste(this,"200")'></asp:TextBox>
                    </td>
                </tr>--%>
                <%-- <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnCancelDesc" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancelDesc_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveDesc" runat="server" Text="Ok" CssClass="button" OnClick="btnSaveDesc_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
            </table>
        </div>
    </ContentTemplate>
</telerik:RadWindow>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2" align="right">
            <table>
                <tr>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                    <td align="right">
                        <span class="prfl-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=Request" class="help-icon"
                            target="_blank" title="Help"></a>
                            <asp:LinkButton CssClass="history-icon" ID="lnkHistory" runat="server" OnClientClick="javascript:openWin('rdHistory');return false;"
                                ToolTip="History"></asp:LinkButton>
                            <a href="<%=ResolveClientUrl("~/Views/Reports/ReportViewer.aspx?xmlFilename=CRTripItinerary.xml" +"&CRTripNUM=" + Microsoft.Security.Application.Encoder.HtmlEncode(lblRequestNumber.Text) +"&CRTripID=" + Microsoft.Security.Application.Encoder.HtmlEncode(hdnCRTripID.Value))%>"
                                target="_blank" title="Preview Report" class="print_preview_icon"></a></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" class="tdLabel180">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" OnTextChanged="SearchBox_TextChanged"
                                        ID="SearchBox"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button runat="server" UseSubmitBehavior="true" CssClass="searchsubmitbutton"
                                        CausesValidation="false" OnClick="Search_Click" ID="sa" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tdLabel120" align="left">
                        &nbsp;<asp:LinkButton ID="lnkSeeAll" runat="server" CausesValidation="false" OnClientClick="javascript:openWin('RadRetrievePopup');return false;"
                            Text="See All Request" CssClass="link_small"></asp:LinkButton>
                    </td>
                    <td align="center">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblRequestNo" Visible="false" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                                <td class="mnd_text">
                                    Request No.
                                    <asp:Label ID="lblRequestNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td class="mnd_text">
                                                Tail No.
                                                <asp:Label ID="lblTailNo" runat="server" Font-Bold="true" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td class="mnd_text">
                                                Type Code :
                                                <asp:Label ID="lblTypeCode" runat="server" Font-Bold="true" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label CssClass="alert-text" runat="server" ID="lbTripSearchMessage" ForeColor="Red"></asp:Label>
                        <asp:HiddenField ID="hdnCRTripID" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
