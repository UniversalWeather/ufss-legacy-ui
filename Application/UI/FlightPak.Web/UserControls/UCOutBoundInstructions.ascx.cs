﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FlightPak.Web.UserControls
{
    public partial class UCOutBoundInstructions : System.Web.UI.UserControl
    {
        private Delegate _saveClick;
        private Delegate _cancelClick;

        public Delegate SaveClick
        {
            set { _saveClick = value; }
        }

        public Delegate CancelClick
        {
            set { _cancelClick = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Save_click(object sender, EventArgs e)
        {
            _saveClick.DynamicInvoke();
        }
        protected void Cancel_click(object sender, EventArgs e)
        {
            _cancelClick.DynamicInvoke();
        }
     
    }
}