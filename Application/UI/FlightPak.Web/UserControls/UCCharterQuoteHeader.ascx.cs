﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;

namespace FlightPak.Web.UserControls
{
    public partial class UCCharterQuoteHeader : System.Web.UI.UserControl
    {
        #region Declarations
        private Delegate _SaveToSession;
        public Delegate SaveToSession
        {
            set { _SaveToSession = value; }
        }

        private Delegate _SaveToSessionPAX;
        public Delegate SaveToSessionPAX
        {
            set { _SaveToSessionPAX = value; }
        }

        public RadTabStrip HeaderTab { get { return this.rtCharterQuote; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Session["CQFILE"] != null)
            {
                string selTab = Request.QueryString["seltab"];

                if (string.IsNullOrEmpty(selTab) && Session["CQTab"] != null)
                {
                    selTab = Session["CQTab"].ToString();
                    var page = this.Page as BasePage;
                    page.RedirectToPage(Microsoft.Security.Application.Encoder.HtmlEncode(selTab));
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Request.QueryString["seltab"] != null)
                {
                    string selectedItemValue = Request.QueryString["seltab"];
                    RadTab selectedTab = new RadTab();

                    if (selectedItemValue.ToLower() == "file")
                        selectedTab = rtCharterQuote.FindTabByText("File");
                    else if (selectedItemValue.ToLower() == "quoteonfile")
                        selectedTab = rtCharterQuote.FindTabByText("Quote On File");
                    else if (selectedItemValue.ToLower() == "quoteandleg")
                        selectedTab = rtCharterQuote.FindTabByText("Quote & Leg Details");
                    else if (selectedItemValue.ToLower() == "pax")
                        selectedTab = rtCharterQuote.FindTabByText("PAX");
                    else if (selectedItemValue.ToLower() == "logistics")
                        selectedTab = rtCharterQuote.FindTabByText("Logistics");
                    else if (selectedItemValue.ToLower() == "reports")
                        selectedTab = rtCharterQuote.FindTabByText("Reports");
                    else if (selectedItemValue.ToLower() == "cqrw")
                        selectedTab = rtCharterQuote.FindTabByText("CQ Report Writer");

                    

                    if (selectedTab != null)
                    {
                        selectedTab.Selected = true;
                        while ((selectedTab != null) &&
                               (selectedTab.Parent.GetType() == typeof(RadTab)))
                        {
                            selectedTab = (RadTab)selectedTab.Parent;
                            selectedTab.Selected = true;
                        }
                    }
                }
            }
        }

        protected void redirecttotab(string selectedtab)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedtab))
            {
                Session.Remove("CQPageToNavigate");
                var basePage = this.Page as BasePage;
                basePage.RedirectToPage(Microsoft.Security.Application.Encoder.HtmlEncode(selectedtab));
            }
        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Session.Remove("CQPAXnotAssigned");
                Session.Remove("CrewnotAssigned");

                if (_SaveToSessionPAX != null)
                    _SaveToSessionPAX.DynamicInvoke();
                redirecttotab(Session["CQPageToNavigate"].ToString());
            }
        }

        protected void rtCharterQuote_Clicked(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Session["CQPageToNavigate"] = e.Tab.Value;
                Session["CQTab"] = e.Tab.Value;

                if (_SaveToSession != null)
                    _SaveToSession.DynamicInvoke();

                if (Session["CQFILE"] != null)
                {
                    CQFile FileRequest = (CQFile)Session["CQFILE"];

                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        FlightPak.Web.Framework.Masters.CharterQuote MasterClass = (FlightPak.Web.Framework.Masters.CharterQuote)Page.Master;
                        MasterClass.ValidateQuote();
                    }
                }

                if (Session["CQPAXnotAssigned"] != null)
                {
                    if (Session["CQPAXnotAssigned"] != null)
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('" + Session["CQPAXnotAssigned"] + "',TripPAXNotAssignedCallBackFn, 330, 110,null,'Confirmation!');");

                    Session.Remove("CQPAXnotAssigned");
                }
                else
                {
                    if (_SaveToSessionPAX != null)
                        _SaveToSessionPAX.DynamicInvoke();

                    redirecttotab(e.Tab.Value);
                }
            }
        }
    }

}