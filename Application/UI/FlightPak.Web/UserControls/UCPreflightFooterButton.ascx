﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreflightFooterButton.ascx.cs" Inherits="FlightPak.Web.UserControls.UCPreflightFooterButton" %>
<script type="text/javascript">
    $(function () {
        var tabName = getQuerystring("seltab", "PreFlight");
        if (tabName == "Logistics") {
            $("#btnNext").hide();
        }


        $("#btnNext").click(function () {
            var tabName = getQuerystring("seltab", "PreFlight");
            var redirectUrl = "";
            if (tabName == "PreFlight") {
                redirectUrl = "/views/Transactions/Preflight/PreflightLegs.aspx?seltab=Legs";
            } else if (tabName == "Legs") {
                redirectUrl = "/views/Transactions/Preflight/PreflightCrewInfo.aspx?seltab=Crew";
            } else if (tabName == "Crew") {
                redirectUrl = "/views/Transactions/Preflight/PreflightPax.aspx?seltab=Pax";
            } else if (tabName == "Pax") {
                redirectUrl = "/views/Transactions/Preflight/PreflightLogistics.aspx?seltab=Logistics";
            }
            var result=MaintainModelStatusBeforePageLeave(redirectUrl, "next");
            if(result)
                window.location = redirectUrl;

 
        });

    });
</script>
<input type="button" ID="btnDeleteTrip" title="Delete Selected Record" data-bind="PreflightFooter: TripID(), mode: EditMode(), btn: 'Delete', click: btnDelete_Click" value="Delete Trip" class="button" />
<input type="button" ID="btnEditTrip" title="Edit Selected Record" data-bind="PreflightFooter: TripID(), mode: EditMode(), btn: 'Edit', click: btnEdit_Click" value="Edit Trip" class="button" />
<input type="button" ID="btnCancel" title="Cancel All Changes" data-bind="PreflightFooter: TripID(), mode: EditMode(), btn: 'Cancel', click: btnCancel_Click" value="Cancel" class="button" />
<input type="button" ID="btnSave" title="Save Changes" value="Save" data-bind="PreflightFooter: TripID(), mode: EditMode(), btn: 'Save', click: btnSave_Click" class="button" />
<input type="button" ID="btnNext" value="Next" title="Next" data-bind="PreflightFooter: TripID(), mode: EditMode(), btn: 'Next'" class="button" />