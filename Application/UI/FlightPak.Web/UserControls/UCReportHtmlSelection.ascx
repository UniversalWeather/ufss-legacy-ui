﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCReportHtmlSelection.ascx.cs" Inherits="FlightPak.Web.UserControls.UCReportHtmlSelection" %>

  <div class="divGridPanel">
  <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">
  <table>
  <tr>
  <td> Available fields </td>  
  <td>
  Selected fields
  </td>
  </tr>
  <tr>
  <td>
 <telerik:RadListBox
        runat="server" ID="RadListBoxSource"
        Height="200px" Width="200px"
        AllowTransfer="true" TransferToID="RadListBoxDestination" 
        TransferMode="Move" 
        AllowTransferDuplicates="false" 
        AllowTransferOnDoubleClick ="true" 
        EnableDragAndDrop ="true"    
        SelectionMode="Multiple" />
        </td>
        <td>
    
    <telerik:RadListBox
        runat="server" ID="RadListBoxDestination" 
        AllowReorder="true"  
        EnableDragAndDrop ="true" 
        SelectionMode="Multiple" 
            AllowTransferOnDoubleClick ="true"        
        Height="200px" Width="200px" />
        </td>
        </tr>
        </table>
    
</telerik:RadAjaxPanel>
</div> 