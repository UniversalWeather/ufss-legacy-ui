﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;

namespace FlightPak.Web.UserControls
{
    public partial class ScheduleCalendarHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Session["CurrentPreFlightTrip"] = null;
            //Session.Remove("PreflightException");
            if (Request.QueryString["seltab"] != null)
            {
                string selectedItemValue = Request.QueryString["seltab"];
                RadTab selectedTab = rtSchedulingCalendar.FindTabByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedItemValue));
                if (selectedTab != null)
                {
                    selectedTab.Selected = true;
                    while ((selectedTab != null) &&
                           (selectedTab.Parent.GetType() == typeof(RadTab)))
                    {
                        selectedTab = (RadTab)selectedTab.Parent;
                        selectedTab.Selected = true;
                    }
                }
            }
        }
        protected void rtSchedulingCalendar_Clicked(object sender, RadTabStripEventArgs e)
        {
            //if (_SaveToSession != null)
            //    _SaveToSession.DynamicInvoke();

            if (e.Tab.Value == "Weekly")
                Response.Redirect("SchedulingCalendar.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "Monthly")
                Response.Redirect("Monthly.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "Day")
                Response.Redirect("DayView.aspx?seltab=" + e.Tab.Value);
            //else if (e.Tab.Value == "Planner")
            //    Response.Redirect("SchedulingCalendar.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "BusinessWeek")
                Response.Redirect("BusinessWeek.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "Corporate")
                Response.Redirect("CorporateView.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "Planner")
                Response.Redirect("Planner.aspx?seltab=" + e.Tab.Value);
            else if (e.Tab.Value == "Reports")
                Response.Redirect("CalendarReports.aspx?seltab=" + e.Tab.Value);           
        }
    }
}