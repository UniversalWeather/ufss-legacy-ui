﻿<%@ Control Language="C#" AutoEventWireup="true" ClassName="UCFilterCriteria" CodeBehind="UCFilterCriteria.ascx.cs"
    Inherits="FlightPak.Web.UserControls.UCFilterCriteria" %>

<script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
<script type="text/javascript">

    // Script for Confirmation Message
    function confirmCopyCallBackFn(arg) {
        if (arg == true) {
            document.getElementById('<%=btnCopyYes.ClientID%>').click();
        }
        else {
            document.getElementById('<%=btnCopyNo.ClientID%>').click;
        }

    }
    // Script for Validating in valid codes
    function Messageok(arg) {
        if (arg == true) {
            var Control = document.getElementById('<%=hdnControl.ClientID%>').value;
            var ControlID = document.getElementById(Control);
            ControlID.focus();
        }
        return false;
    }
    function openWin(radWin) {

        var url = '';
        if (radWin == "rdClientCodePopup") {
            url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCode.ClientID%>').value;
        }
        if (radWin == "radPaxInfoPopup") {
            url = '/Views/Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorID=' + document.getElementById('<%=hdnRequestor.ClientID%>').value;
        }
        if (radWin == "rdDepartmentAuthorizationPopup") {
            url = '/Views/Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
        }

        if (radWin == "rdFlightCategoryPopup") {
            url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCategoryID=' + document.getElementById('<%=hdnFlightCategory.ClientID%>').value;
        }

        if (radWin == "radCrewDutyTypePopup") {
            url = '/Views/Settings/People/CrewDutyTypePopup.aspx?DutyTypeID=' + document.getElementById('<%=hdnCrewDuty.ClientID%>').value;
        }

        if (radWin == "rdHomebasePopup") {
            url = '/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx?HomeBaseID=' + document.getElementById('<%=hdnHomebase.ClientID%>').value;
        }

        var oWnd = radopen(url, radWin);
    }



    function ConfirmClose(WinName) {
        var oManager = GetRadWindowManager();
        var oWnd = oManager.GetWindowByName(WinName);
        //Find the Close button on the page and attach to the
        //onclick event
        var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
        CloseButton.onclick = function () {
            CurrentWinName = oWnd.Id;
            //radconfirm is non-blocking, so you will need to provide a callback function
            radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
        }
    }

    function OnClientClientCodeClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                document.getElementById("<%=hdnClientCode.ClientID%>").value = arg.ClientID;

            }
            else {
                document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                document.getElementById("<%=hdnClientCode.ClientID%>").value = "";
            }
        }
    }
    function OnClientRequestorPopupClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbRequestor.ClientID%>").value = arg.PassengerRequestorCD;
                document.getElementById("<%=hdnRequestor.ClientID%>").value = arg.PassengerRequestorID;

            }
            else {
                document.getElementById("<%=tbRequestor.ClientID%>").value = "";
                document.getElementById("<%=hdnRequestor.ClientID%>").value = "";

            }
        }
    }

    function OnClientDepartmentAuthorizationPopupClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                document.getElementById("<%=hdnDepartment.ClientID%>").value = arg.DepartmentID;

            }
            else {
                document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                document.getElementById("<%=hdnDepartment.ClientID%>").value = "";

            }
        }
    }

    function OnClientFlightCategoryClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {

                document.getElementById("<%=tbFlightCategory.ClientID%>").value = arg.FlightCatagoryCD;
                document.getElementById("<%=hdnFlightCategory.ClientID%>").value = arg.FlightCategoryID;
            }
            else {
                document.getElementById("<%=tbFlightCategory.ClientID%>").value = "";
                document.getElementById("<%=hdnFlightCategory.ClientID%>").value = "";

            }
        }
    }

    function OnClientHomebaseMultipleSelectionClose(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbHomebase.ClientID%>").value = arg.HomebaseCD;
                document.getElementById("<%=hdnHomebase.ClientID%>").value = arg.HomebaseID;

            }
            else {
                document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                document.getElementById("<%=hdnHomebase.ClientID%>").value = "";
            }
        }
        var idCount = document.getElementById("<%=hdnHomebase.ClientID%>").value.split(',');
        if (idCount.length > 1) {

            document.getElementById("<%=lbHomebase.ClientID%>").style.display = "block";

        }
        else {
            document.getElementById("<%=lbHomebase.ClientID%>").style.display = "none";

        }
    }


    function OnClientCrewDutyTypeClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("<%=tbCrewDuty.ClientID%>").value = arg.DutyTypeCD;
                document.getElementById("<%=hdnCrewDuty.ClientID%>").value = arg.DutyTypeID;

            }
            else {
                document.getElementById("<%=tbCrewDuty.ClientID%>").value = "";
                document.getElementById("<%=hdnCrewDuty.ClientID%>").value = "";


            }
        }
    }
    function GetDimensions(sender, args) {
        var bounds = sender.getWindowBounds();
        return;
    }
    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        return oWindow;
    }


    function UCShowPleaseWait() {

        document.getElementById("<%=UCPleaseWait.ClientID %>").style.display = 'block';
    }


    function showPleaseWait() {

        document.getElementById("<%=UCPleaseWait.ClientID %>").style.display = 'block';
    }
</script>


<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
        </telerik:RadWindow>
        <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
        </telerik:RadWindow>
        <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
            ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
            VisibleStatusbar="false">
        </telerik:RadWindow>
        <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
        </telerik:RadWindow>
        <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
        </telerik:RadWindow>
        <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
        </telerik:RadWindow>
        <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
            ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
            VisibleStatusbar="false">
        </telerik:RadWindow>
        <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
            OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

    <div class="filter-btn-area">
        <div runat="server" id="UCPleaseWait" class="pageloader">
        </div>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <asp:Button ID="btnSaveDefaults" Text="Save Defaults" runat="server" ValidationGroup="save"
                        OnClick="SaveDefault_Click" CssClass="ui_nav_sm" OnClientClick="UCShowPleaseWait();" />
                </td>
                <td align="left">
                    <asp:Button ID="btnReset" Text="Reset To System Defaults" runat="server" CssClass="ui_nav_sm"
                        OnClick="RestoreSystemDefault_Click" OnClientClick="UCShowPleaseWait();" />
                    <asp:HiddenField ID="hdReset" runat="server" />
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnCopyYes" runat="server" Text="Button" OnClick="btnCopyYes_Click"
                        OnClientClick="UCShowPleaseWait();" />
                    <asp:Button ID="btnCopyNo" runat="server" Text="Button" OnClick="btnCopyNo_Click" />
                    <input type="hidden" id="hdnControl" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <div class="filter-content">
        <table class="border-box-nobrd" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" style="scroll: auto;">
                        <tr>
                            <td valign="top" align="left">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" class="brd_btm" width="100%">
                                                <tr>
                                                    <td class="tdLabel70">
                                                        <span class="smfont_b">Time Base:</span>
                                                    </td>
                                                    <td class="tdLabel50">
                                                        <asp:RadioButton ID="radLocal" runat="server" Checked="true" Text="Local" GroupName="TimeBase"
                                                            class="smfont" />
                                                    </td>
                                                    <td class="tdLabel45">
                                                        <asp:RadioButton ID="radUTC" runat="server" Text="UTC" GroupName="TimeBase" class="smfont" />
                                                    </td>
                                                    <td class="tdLabel50">
                                                        <asp:RadioButton ID="radHome" runat="server" Text="Home" GroupName="TimeBase" class="smfont" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel100">
                                            <table width="100%">
                                                <tr>
                                                    <td class="tdLabel50">
                                                        <span class="smfont">Client</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbClientCode" runat="server" CssClass="text70" MaxLength="5" onkeydown="return (event.keyCode!=13);"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                        <input type="hidden" id="hdnClientCode" runat="server" />
                                                        <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('rdClientCodePopup');return false;"
                                                            CssClass="browse-button" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel50">
                                                        <span class="smfont">Requestor</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbRequestor" runat="server" CssClass="text70" MaxLength="5" onkeydown="return (event.keyCode!=13);"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                        <input type="hidden" id="hdnRequestor" runat="server" />
                                                        <asp:Button ID="btnRequestor" OnClientClick="javascript:openWin('radPaxInfoPopup');return false;"
                                                            CssClass="browse-button" runat="server" />
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbcvRequestor" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td class="tdLabel50">
                                                        <span class="smfont">Department</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbDepartment" runat="server" CssClass="text70" MaxLength="8" onkeydown="return (event.keyCode!=13);"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                        <input type="hidden" id="hdnDepartment" runat="server" />
                                                        <asp:Button ID="btnDepartment" OnClientClick="javascript:openWin('rdDepartmentAuthorizationPopup');return false;"
                                                            CssClass="browse-button" runat="server" />
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                <td>
                                                    <asp:Label ID="lbcvDepartment" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td class="tdLabel90">
                                                        <span class="smfont">Flight Category</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbFlightCategory" runat="server" CssClass="text70 tbCategory" MaxLength="4"
                                                            onkeydown="return (event.keyCode!=13);" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                        <input type="hidden" id="hdnFlightCategory" runat="server" />
                                                        <asp:Button ID="btnFlightCategory" OnClientClick="javascript:openWin('rdFlightCategoryPopup');return false;"
                                                            CssClass="browse-button" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel90">
                                                        <span class="smfont">Crew Duty</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbCrewDuty" runat="server" CssClass="text70" MaxLength="2" onkeydown="return (event.keyCode!=13);"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                        <input type="hidden" id="hdnCrewDuty" runat="server" />
                                                        <asp:Button ID="btnCrewDuty" OnClientClick="javascript:openWin('radCrewDutyTypePopup');return false;"
                                                            CssClass="browse-button" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel90">
                                                        <span class="smfont">Home Base</span>
                                                    </td>
                                                    <td>
                                                        <div class="homebase_multiple">
                                                            <asp:TextBox ID="tbHomebase" runat="server" CssClass="text70" MaxLength="5" onkeydown="return (event.keyCode!=13);"
                                                                onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                            <input type="hidden" id="hdnHomebase" runat="server" />
                                                            <asp:Button ID="btnHomeBase" OnClientClick="javascript:openWin('rdHomebasePopup');return false;"
                                                                CssClass="browse-button" runat="server" />
                                                            <asp:Label ID="lbHomebase" runat="server" Text="(...)" CssClass="smfont_multiple" /></div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <div class="tblspace_5">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkTrip" runat="server" Text="Trip" class="smfont" />
                                                    </td>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkHold" runat="server" Text="Hold" class="smfont" />
                                                    </td>
                                                </tr>
                                                <% %><tr>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkWorkSheet" runat="server" Text="Worksheet" class="smfont" />
                                                    </td>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkSchedServ" runat="server" Text="Sched Serv" class="smfont" Visible="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkAllCrew" runat="server" Text="All Crew" class="smfont" />
                                                    </td>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkHomeBase" runat="server" Text="Home Base Only" class="smfont" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkCanceled" runat="server" Text="Canceled" class="smfont" />
                                                    </td>
                                                    <td class="tdLabel52">
                                                        <asp:CheckBox ID="chkUnFulFilled" runat="server" Text="Unfulfilled" class="smfont" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel52" colspan="2">
                                                        <asp:CheckBox ID="chkFixedWingCrew" runat="server" Text="Fixed Wing Crew Only" class="smfont" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel52" colspan="2">
                                                        <asp:CheckBox ID="chkRotaryWingCrew" runat="server" Text="Rotary Wing Crew Only"
                                                            class="smfont" />
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="horizontalLine">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="radIncludeActiveVendors" runat="server" Text="Include Active Vendors"
                                                            GroupName="VendorOption" class="smfont" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="radOnlyShowActiveVendors" runat="server" Text="Only Show Active Vendors"
                                                            GroupName="VendorOption" class="smfont" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="radExcludeActiveVendors" runat="server" Text="Exclude Active Vendors"
                                                            GroupName="VendorOption" class="smfont" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                    <td align="left">
                                        <div class="tblspace_5">
                                        </div>
                                    </td>
                                </tr>--%>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

