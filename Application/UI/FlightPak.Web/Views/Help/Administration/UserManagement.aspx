﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="FlightPak.Web.Views.Help.UserManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">User Management</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            <div class=WordSection1>

<h3 style='line-height:150%'><a name="_Toc335040827"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>Configuring User Groups</span></a><span
style='font-family:"Calibri","sans-serif"'> </span></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>The purpose of
User Groups is to allow the FSS System Administrator to define access
rights for different “groups” of users within the FSS. For example, you
may have a group of Dispatch Interns in your office and would like to restrict
them to “view only” access.  Creating a “View Only” group and then setting up
specific access rights for that group will accomplish this. Once the Group and
Access rights are assigned, the individual User logins can be created and
assigned to Groups.</p>

<h3 style='line-height:150%'><a name="_Toc335040828"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access Groups Administration page?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the Administration tab on the left menu.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Groups Administration link.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040829"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I create a new Group?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Group Administration page,
click on the Add button at the bottom of the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>User Group</b> – This is a required
field and must be unique.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Description</b> - A description
associated with the User Group (i.e. Pilots, Interns, or Dispatchers, etc.).
This is a required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Now you will need to assign ACCESS rights to the
Group that you are creating. There are four (4) types of access that can be
defined for each area within FSS. They are: VIEW, ADD, EDIT, and DELETE.
These Access types work as follows:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>VIEW</span></b><span lang=EN-GB> allows records to be viewed by the
user.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>ADD</span></b><span lang=EN-GB> allows records to be added by the
user.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>EDIT</span></b><span lang=EN-GB> allows existing records to be
modified by the user. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>DELETE</span></b><span lang=EN-GB> allows existing records to be
deleted by the user. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Define the necessary access rights.   </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>There are several different combinations of
access that can be granted to a user. Example – View / Add; All; View / Add /
Edit.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button to save the newly
created Group and defined access.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040830"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I edit an existing Group?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Group Administration page,
click on the Group record that you want to edit. Use the sort / search / scroll
functionalities to locate the Group to be edited.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button at the bottom of the
grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-GB>User Group</span></b><span lang=EN-GB> field can’t
be edited, but you can edit the <b>Description</b> field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Modify access rights as necessary.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button to save the
changes.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040831"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I copy an existing Group to create a new Group?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Group Administration page,
click on the Group record that you want to copy. Use the sort / search / scroll
functionalities to locate the Group to be copied.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Copy</b> button at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>A new window will display.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the new <b>User Group</b> name.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Description.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button to complete the
copy function.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The new group will have the same access rights
as the “source” group.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040832"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I delete an existing User Group?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Groups Administration page,
click on the Group record that you want to delete. Use the sort / search /
scroll functionalities to locate the Group to be deleted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Delete</b> icon at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You will receive a warning message <b>“Are you
sure you want to delete this record?”</b> </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>Yes</b> button to delete the record.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040833"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>Configuring User Accounts</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>FSS
provides the ability to setup multiple Home Bases and users must be assigned to
a Home Base. Users can be assigned to more than one Home Base. </p>

<h3 style='line-height:150%'><a name="_Toc335040834"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access the User Administration page?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Database</b> button on the
global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand <b>Administration</b> tab on the left
menu.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>User Administration</b> link.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040835"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I setup a new User and assign them to Groups?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the <b>User Administration</b>
page, click on the Add button at the bottom of the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>User Name</b> – This will be user’s
login name. This is a required field and must be unique.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the user’s <b>First Name</b> – This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the user’s <b>Middle Name</b> – This is an
optional field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the user’s <b>Last Name</b> – This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the user’s <b>Home Base</b> – This is a
required field and must be a valid Home Base in the Company Profile.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter user’s <b>E-mail address </b>– This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Turn on the <b>Active</b> flag.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Optionally you can enter/setup the following
fields,</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB> Client Code</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Travel Coordinator</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>UWA Triplink ID</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>UWA Triplink Password</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Phone</span></b><span lang=EN-GB> N<b>umber</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Primary Contact</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Secondary Contact</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Override Corp Req Time Limit</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Corp Req Approver</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Sys Adm- Unrestricted access</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Dispatcher</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Trip Privacy Setting</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-GB>Assign</span></b><span lang=EN-GB> one or
multiple groups to the user by moving the selected groups from the available
Groups list.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:150%'><b><i><span
lang=EN-GB>Note – The user will have access rights of all groups they are
assigned to.</span></i></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button to save the
newly created user. </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The system will send an E-mail (to the e-mail
address entered while creating the user) to the user with the User Name and a
system generated password.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040836"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I edit existing Users and/or the Group they are
assigned to?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the <b>User Administration</b>
page, click on the User you want to edit. Use the sort / search / scroll
functionalities to locate the user to be edited.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Edit</b> button at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You can edit <b>all fields</b> other than the <b>User
Name.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Change the Groups assigned to the user as
necessary.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button to save the
changes.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040837"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I delete an existing User?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the <b>User Administration</b>
page, click on the User you want to delete. Use the sort / search / scroll
functionalities to locate the user to be deleted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click in the <b>Delete</b> icon at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You will receive a warning message <b>“Are you
sure you want to delete this record?”</b> </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>Yes</b> button to delete the record</span></p>

<h3 style='line-height:150%'><a name="_Toc335040838"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I lock out a User from FSS?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the <b>User Administration</b>
page, click on the User you want to lock out. Use the sort / search / scroll
functionalities to locate the user to be locked out.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Edit</b> icon at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Turn ON <b>User Lock</b> field.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button to save the changes.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040839"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I reset a User’s Password?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the User Administration page,
click on the User record that you want to reset password for. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Edit</b> icon at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>Reset User Password</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The system will send an <b>E-mail</b> with a
temporary password to the user’s registered E-mail address.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;line-height:150%'><b><i><span
lang=EN-GB>Note – The user will logon using the temporary password and will be
prompted to enter a new password.</span></i></b></p>

<p class=MsoNormal>Click on the <b>Cancel</b> button to exit from edit mode
function.</p>

</div>
             
            </td>
        </tr>
    </table>

</asp:Content>
