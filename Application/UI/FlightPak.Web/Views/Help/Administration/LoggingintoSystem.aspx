﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="LoggingintoSystem.aspx.cs" Inherits="FlightPak.Web.Views.Help.LoggingintoSystem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Logging into System</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Flight Scheduling Softwareis a web based application; to start the
application, open your browser and type the following URL.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'>Training Environment - <span style=" color:#3366cc;">http://fpkdemo/</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Supported Browsers –</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Internet Explorer</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Google Chrome</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Mozilla Firefox</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>Safari</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
line-height:150%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><i><span style='font-size:10.0pt;
line-height:150%'>Contact your local system administrator if you need any
assistance with installing and configuring your web browser.</span></i></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;text-autospace:none'><i><span style='font-size:10.0pt;
line-height:150%'>&nbsp;</span></i></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
Flight Scheduling Softwareapplication will prompt you to login.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
in the Username and Password that you received through your E-mail. <i>Contact
your local Flight Scheduling SoftwareSystem Administrator for your Username and Password if you
do not have one.</i></span></p>

<h3 style='line-height:150%'><a name="_Toc335040819"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I change my password?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
change your Flight Scheduling Softwarepassword click on the Change Password link in the user
profile section.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your old password</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your new password</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Confirm
your new password</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Change Password button</span></p>

<h3 style='line-height:150%'><a name="_Toc335040820"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>What if I forget my password?</span></a></h3>

<p class=MsoFooter style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>If you forget your Password, take the
following steps to reset your password</span></p>

<p class=MsoFooter style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the “Forgot your password?” link on the home page.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
will be prompted to enter your Username in order to fetch the secret question.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
will have to enter the answer to your secret question that was previously set
in your user profile. <i>If you don’t know the answer, contact your local
Flight Scheduling SoftwareSystem Administrator to reset your password.</i></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
new password will be system generated and sent to the registered E-mail id.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
will be prompted to change your password when you login using the system
generated password.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040821"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How Can I exit the system?</span></a></h3>

<p class=MsoFooter style='text-align:justify;line-height:150%'><span
lang=EN-GB>To logout and exit FSS, click the Logout button on your global
menu.</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>
            </td>
        </tr>
    </table>
</asp:Content>
