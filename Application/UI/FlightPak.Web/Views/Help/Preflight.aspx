﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="Preflight.aspx.cs" Inherits="FlightPak.Web.Views.Help.Preflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Preflight</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>Preflight is used to plan trips. The
key information captured for the trip include, </span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Main
information (Aircraft, Department/Authorization, etc.)</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Legs</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Assigned
Crew</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Assigned
passengers</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Logistics</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Service
Request to UWA</span></p>

<h3 style='line-height:150%'><a name="_Toc335040878"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access Preflight module?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Pre-flight</b> button on the
global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>New Trip</b> button, to create a new
Trip</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Or if you are in the Scheduling Calendar, right
click and choose <b>Add New Trip using Preflight </b>(to create a New Trip) or
<b>Preflight</b> (to open an existing Trip) link</span></p>

<h3 style='line-height:150%'><a name="_Toc335040879"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I search for an existing trip?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If you
know the Trip number, enter the Trip number in the search box under the Pre-flight
tab and click on <b>search icon</b> </span><span style='font-size:10.0pt;
line-height:150%'><img width=25 height=20 id="Picture 114"
src="8_files/image001.jpg"></span><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the Trip exists, the system will open the
Trip.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the Trip doesn’t exist, you will receive an
error message indicating the Trio No. doesn’t exist.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040880"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>What if I don’t know the Trip No.?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If you
don’t know your Trip number, you can search by various filter criteria by
clicking on “See All” link.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>“See All”</b> link next to the Trip
No. Search box.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter your filter criteria and click on the <b>Search</b>
button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The grid will display all Trips that matched
your Search criteria.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You can use sort and / or scroll features to
locate the Trip that you want to find.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Trip record that you want to open.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>OK</b> button to open the Trip.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040881"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I edit an existing Trip?</span></a></h3>

<p class=MsoListParagraph style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>By
default the Trip you selected would open in the read only mode and you can
click on various tabs on the Trip to view the details. To edit the selected
Trip, click on <b>Edit Trip</b> button right above the Pre-flight tab.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040882"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I create a New Trip?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Pre-flight</b> button on the
global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>New Trip</b> button.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Or if you are in the Scheduling Calendar, right
click and select <b>Add New Trip using Preflight </b>from the drop down.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b>Trip Main
Details</b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the <b>Departure Date.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Tail No.</b> lookup to view the Fleet Profile table. Select the Tail
No. of the trip by clicking the desired fleet and click the <b>OK</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Requestor lookup</b> to view the Passenger/Requestor table. Select
the Requestor of the trip by clicking the desired name and click the <b>OK</b>
button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Account No. lookup to view the Accounts table. Select the Account by
clicking the desired Account and click on the OK button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
Department and Authorization.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter the
<b>Trip Purpose.</b></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Next</b> button at the bottom of the page or click on the <b>Legs</b>
tab to proceed with building legs for your trip.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Building Legs</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>By
default, your aircraft’s home base will be displayed in the <b>departure ICAO</b>
field. You can change the departure ICAO, if required.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the desired <b>arrival ICAO</b> code in the Arrives field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your desired <b>Departure or Arrival Time</b> either in Local or UTC or Home
Base times.  The system will calculate other times automatically.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
this is the only (or last) leg of the day, select the <b>End of Duty Day</b>
checkbox next to the crew duty rules section.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><i><span
style='font-size:10.0pt;line-height:150%'>Note – At this point, you have
entered all the required data for the trip to be saved and a Trip No. to be
assigned by the system if you click the Save button.</span></i></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
there are more legs to be added to this trip, click on the Add Leg button to
begin planning your next leg.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the Arrives ICAO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your desired <b>Departure or Arrival Date and Time </b>either in Local or UTC
or Home Base times.  The system will calculate other dates and times
automatically.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
this is the last leg of the day, select the <b>End of Duty Day</b> checkbox
next to the crew duty rules section.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>When
you have completed adding your flight leg data, click on the <b>Crew tab</b> or
the <b>Next button</b> at the bottom of the page, to begin adding crewmembers
to each leg of the trip.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><i><span
style='font-size:10.0pt;line-height:150%'>Note - It is not necessary, at this
time, to click on the Save button, however, it is highly recommended to avoid
unnecessary loss of data.  To begin working in the trip again, you will need to
click on the Edit Trip button.</span></i></b></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Adding Crew</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
choose the crew most suited for the trip, you may turn on or turn off
checkmarks in the <b>available filters</b> (above the Crew Availability grid)
and you will notice that the list of available crewmembers changes depending on
the following filters</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Crew
Availability Active</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Type
Rated Only</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>PIC/SIC
Only</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>No
Conflicts Only</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Home
Base Only</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Crew
Group</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
select your crew, <b>click one or multiple Crew</b> in the Crew Availability
grid, and then click on the <b>Assign button </b></span><b><span
style='font-size:10.0pt;line-height:150%'><img width=22 height=23
id="Picture 188" src="8_files/image002.jpg"></span></b><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'> at the bottom of the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Once
you have selected the crew (and when you have all Crewmembers for the trip in
the Assigned Crew grid) you can assign command positions.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Crew that you would like to assign as <b>PIC</b> (Pilot in Command) to
highlight the Crew, and then select PIC in the <b>All Legs drop down</b> (top
section of the Assigned Crew grid).  </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Next,
select the next crewmember that would like to assign as <b>SIC </b>to highlight
the Crew, and then select SIC in the All Legs drop down. Repeat this process as
needed for Engineers, Attendants, Instructors, etc.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Alternatively,
you can edit a Crew record by clicking the Edit icon </span><span
style='font-size:10.0pt;line-height:150%'><img width=25 height=24
id="Picture 202" src="8_files/image003.png"></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>to assign their role and / or to
update their destination address (the default address would be populated from
the Crew Roster Database table).</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>When
you have completed assigning crew, and are ready to assign passengers to the
manifest, click on the Passenger tab or the Next button.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><i><span
style='font-size:10.0pt;line-height:150%'>Note - It is not necessary, at this
time, to click on the Save button, however, it is highly recommended to avoid
unnecessary loss of data.  To begin working in the trip again, you will need to
click on the Edit Trip button.</span></i></b></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Adding Passengers</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>From
the Passenger tab, click on the <b>PAX Catalog</b> link to open the Passenger /
Requestor table</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
<b>one or multiple Passenger</b> and click on the <b>OK</b> button to place all
selected Passengers on the trip.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The selected
passenger will be assigned to all legs.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
default flight purpose will also be assigned to all passengers / all legs. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
Flight Purpose code can be customized for each passenger by leg</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
remove a passenger from a particular leg, simply remove the Flight Purpose code
(i.e. EB, GP, etc.) displayed in the legs grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
change the Flight Purpose code for a passenger, select the desired code from
the drop down.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
change the Flight Purpose code in all legs of a specific passenger, highlight
the passenger record and then select the desired Flight Purpose in the All Legs
drop down.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Logistics tab or the Next button to begin adding logistics to your trip
(e.g. FBO/Handler, Catering, Leg Notes, etc.).</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:150%'><b><i><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Note - It is not
necessary, at this time, to click on the Save button, however, it is highly
recommended to avoid unnecessary loss of data.  To begin working in the trip
again, you will need to click on the Edit Trip button.</span></i></b></p>

<p class=MsoListParagraphCxSpLast style='line-height:150%'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Adding Logistics</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the desired section, FBO/Handler, Catering and Fuel to begin adding/managing
the logistics that will be associated with this trip.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
will be able to select your FBO, Catering and Fuel for all legs</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
move from leg to leg, highlight the leg number tab you are working</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>
            </td>
        </tr>
    </table>

</asp:Content>
