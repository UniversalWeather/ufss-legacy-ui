﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewDutyRulesHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewDutyRulesHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Duty Rules</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Crew Duty Rules tables
                        are used to define the rules that a crew will operate under for <a name="_GoBack">
                        </a>trips you are planning. As you plan the trip, the software will examine these
                        crew duty rules and alerts to ensure you do not exceed them. The software also allows
                        you to migrate from one set of rules to another depending on the type of trip you
                        are running. You can also assign Duty Day Start/End times, maximum and minimum duty
                        hours allowed and minimum fixed rest hours for your aircraft. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Abbreviated
                            description. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is an explanation of the crew rules you are assigning. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        U.S. F.A.R. Rules:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Click the circle to the left of the rule that describes this policy. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Duty Day Start:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The time the duty day begins before the scheduled time or block in time depending
                            on the setting as specified in the <a href="<%=ResolveClientUrl("~/Views/Help/Company/ConfiguringCompanyProfile.aspx?Screen=ConfiguringCompanyProfile") %>" style="font:12px arial;" >Company Profile</a>
                            for Postflight. In Crew Duty, this value will be automatically placed before the
                            scheduled departure time of the first leg of your duty day. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Duty Day End:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The time after the last leg of the duty day or after block out time. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Maximum Duty Hours Allowed:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The maximum duty hours allowed for this crew. When this maximum is exceeded, the
                            user will be alerted. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Maximum Flight Hours Allowed:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The maximum flight hours allowed per day for this crew. When this maximum is exceeded,
                            the user will be alerted. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Minimum Fixed Rest:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The minimum rest hours allowed for this crew per day. When this minimum is exceeded,
                            the user will be alerted. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Minimum Rest Period (Last Duty Day):</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This area has a multiplier that is driven by the previous duty day included in this
                            specific trip. For example, if the multiplier is 1 and you had 5 duty hours the
                            minimum rest period would be 5. If there was more than one leg of travel for that
                            time period, the system retains the most restrictive calculation. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Plus:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> This is
                            a number to be added to the minimum rest hours for that day. For example, if you
                            have a minimum rest period of 5 plus 2, you will have a new total minimum rest of
                            7. </span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
