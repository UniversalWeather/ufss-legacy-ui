﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="PassengerAdditionalInfoHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.PassengerAdditionalInfoHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passenger Additional Info</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Passenger Additional
                        Information Table contains codes and descriptions for every piece of additional
                        information you want to keep on record for each passenger are maintained here. A
                        few examples of additional passenger information items are emergency contact numbers
                        and pagers. This database is simply used for setting up data fields. The data stored
                        here is a valuable scheduling tool. It provides easy access to personal data you
                        might otherwise repeatedly have to contact the passenger for. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are key Passenger
                        Additional Information Table menu features: </span>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                            This code establishes the sort order of PAX Additional Info records. It cannot be
                            changed by the user once it has been set. The numeric code number assigned determines
                            the order in which this database will be sorted and displayed on screen displays
                            and reports. It is strongly recommended that you begin with code number 100, and
                            increment each new code by at least ten. This will leave room in the database to
                            add new codes in the future. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. This is a label that describes the code and will display on reports.<a
                                name="_GoBack"></a></span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
