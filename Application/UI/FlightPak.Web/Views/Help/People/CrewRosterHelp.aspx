﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewRosterHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewRosterHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Roster</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Crew Roster Table contains
                        information about specific crew members. The table is used to for maintaining names,
                        addresses, phone numbers, E-mail addresses, home base ICAOs and other items such
                        as passport and license numbers for each crew member. This table provides access
                        to additional crew information, the crew check list, type ratings information and
                        details pertaining to the aircraft each crewmember is assigned to. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Crew Reports and Crew Calendars
                        are use information from the crew roster table.</span></p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>. Some of the key features
                        are:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Additional Info. —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Links to the <a href="<%=ResolveClientUrl("~/Views/Help/People/CrewRosterAddInfoHelp.aspx?Screen=CrewAddInfoHelp") %>" style="font:12px arial;" >Crew Additional Information Table</a>
                            and allows you to record, maintain and view additional contact data for each of
                            your crewmembers. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Client Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            You can associate a crew with the desired client code. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Type Ratings —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is where all of the type ratings held by a particular pilot are stored and
                            viewed as well as their time and type, day/night hours, etc. The Type Analysis report
                            is controlled by this database. A warning will display if a crewmember is assigned
                            to an aircraft he or she is not type rated for. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Crew Checklist —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Contains records pertaining to crew certification, training and health data per
                            individual crewmember. These fields are created in the <a href="<%=ResolveClientUrl("~/Views/Help/People/CrewCheckListHelp.aspx?Screen=CrewChecklistHelp") %>" style="font:12px arial;" >Crew
                                Checklist</a> database and have Previous, Due Next, Alert, Grace, Frequency
                            of Months and Set to the end of the month dates associated with each. </span>
                    </li>
                </ul>
                <p class="MsoNormal" style='margin-left: 36.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>For example: </span>
                </p>
                <ul type="disc">
                    <ol start="1" type="1">
                        <li class="MsoNormal" style='line-height:20px;'><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Input your user defined dates for each of these, if the check box for <i>Set to end
                                of month</i> is selected, all of these numbers will automatically default to
                            the last day of the month. </span></li>
                        <li class="MsoNormal" style='line-height:20px;'><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            To activate the Alert date, input <i>Previous and Due Next</i> dates, if your Alert
                            day is 30, it will automatically default to 30 days before the Due Next date. </span>
                        </li>
                        <li class="MsoNormal" style='line-height:20px;'><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            If the Grace date field is set to 30 days, this would be exactly 30 days after the
                            <i>Due Next</i> date and that’s when they are no longer valid. </span></li>
                        <li class="MsoNormal" style='line-height:20px;'><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            If you set <i>Frequency of months</i> to 6, this is driven from the base date, which
                            activates your Due, <i>Next</i> date every 6 months.</span></li>
                    </ol>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Active —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Select
                            this check box if this crew is actively flying for you. If you do not want to delete
                            this crewmember from your records for reporting purposes, leave the box unchecked.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Fixed Wing/Rotary Wing —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select the check boxes that correspond<a name="_GoBack"></a> with the type of aircraft
                            the crewmember is certified to fly. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Aircraft assigned —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Aircraft each crewmember is assigned to fly as defined in the <a href="<%=ResolveClientUrl("~/Views/Help/People/CrewCheckListHelp.aspx?Screen=CrewChecklistHelp") %>" style="font:12px arial;" >
                                Crew Check List Table</a>. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Passports —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Allows you to set up multiple passports. Here is where you’ll store passport numbers,
                            expiration dates, place of issue, nationality and pilot license numbers. The Choice
                            check box allows you to declare your default passport.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Visa –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Allows
                            you to add Visa records for a specific crew member </span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
