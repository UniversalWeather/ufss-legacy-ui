﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewCheckListHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewCheckListHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Checklist Codes</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <a name="_GoBack"></a><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Each
                        record of the Crew Checklist Table represents a particular item of data about a
                        crewmember. This table allows you to keep track of critical qualification and due
                        date information such as FAA medical exams, flight safety training and CPR training
                        for each crewmember. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:
                    </span>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Each check
                            list item is assigned a code of no more than three characters. It is strongly recommend
                            that you code Crew Check list Codes so that they are grouped together logically.
                            For example, begin by using code 100 for F.A.A Medical, then index each subsequent
                            entry by at least 10 or more (e.g. 120 for F.A.A. Checkride, 130 for Training, etc.
                            This will allow flexibility should you decide to add new codes in the future.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Crew Checklist:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Type a description of the item you want to track. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Scheduled Check:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Click the check box if you want this item checked every time you schedule a crewmember
                            for a trip. If there is a conflict such as an expiration or warning, the user will
                            be notified. </span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
