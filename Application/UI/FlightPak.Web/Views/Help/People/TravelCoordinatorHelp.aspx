﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="TravelCoordinatorHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.TravelCoordinatorHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Travel Coordinator</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Travel Coordinator
                        Table allows users to setup special accounts for use with the Corporate Request
                        Module. Once created, these Travel Coordinator codes can be attached to user accounts
                        to provide a high degree of security and greatly enhances the user-friendliness
                        of the Corporate Request Module. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>A Travel Coordinator can
                        be assigned to a Requestor and or Aircraft. This is don’t be selecting the travel
                        coordinator from the Available grid and “dragging” to the selected grid for Requestor
                        or Aircraft. To unassign <a name="_GoBack"></a>a travel coordinator - “drag” a selected
                        travel coordinator to the available grid. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Key fields include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code — </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Required.
                            This is five-character, alpha or numeric code identifies your Travel Coordinator.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        First Name -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Last Name -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home and Business Phone Numbers </span></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Primary and Secondary Cell / Mobile Phone numbers</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            -</span><span lang="EN-US" style='font-family: "Arial","sans-serif"'> </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Business and Personal E-mail Addresses</span></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home and Business Fax Numbers-</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Pager –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Optional
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Mobile -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Optional
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. Each Travel Coordinator must be associated with a valid Home Base that
                            exists within FlightPak.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Notes -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The
                            Notes area provides a dedicated textbox for recording any notes or information pertinent
                            to each Travel Coordinator record. </span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
