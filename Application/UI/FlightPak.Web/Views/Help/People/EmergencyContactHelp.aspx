﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="EmergencyContactHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.EmergencyContactHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Emergency Contact</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Emergency Contact Table
                        contains the information for your emergency contacts.<a name="_GoBack"></a> Records
                        stored in this table can be linked to a specific aircraft or to a specific trip.
                        The information stored in the table includes: </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='margin-left: 10pt; text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Emergency
                        Contact Code</span></p>
                <p class="MsoListParagraphCxSpMiddle" style='margin-left: 10pt; text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>First, Middle
                        and Last Name </span>
                </p>
                <p class="MsoListParagraphCxSpLast" style='margin-left: 10pt; text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Addresses,
                        Phone Numbers, E-mail addresses, Fax numbers </span>
                </p>
            </td>
        </tr>
    </table>
</asp:Content>
