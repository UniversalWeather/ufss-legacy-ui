﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FlightPurposeHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.FlightPurposeHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Flight Purposes</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Each record of the Flight
                        Purposes Table represents a reason why a passenger is on board an aircraft for a
                        particular leg. This database must be set up correctly in order for the SIFL calculator
                        functions to work properly. Each record is assigned a code of no more than two characters
                        and a description. Select the check box(es) that best describe the type of service
                        associated with your particular purpose. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"; color: #C00000'>Flight
                        Purposes are the reasons why passengers are on board the aircraft, not the reason
                        why the aircraft is flying. </span></i>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                            This two-character purpose code will be used in sorting reports. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Client Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Four-character code that identifies the client for this particular flight purpose.
                            You can select a code from the <a href="<%=ResolveClientUrl("~/Views/Help/People/ClientCodeHelp.aspx?Screen=ClientHelp") %>" style="font:12px arial;" >Client Table</a> by
                            clicking the button to the right of the data. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. An explanation detailing the purpose of this trip. This is a descriptive
                            label that prints on reports. This field should not be maintained without consultation
                            with all users of this data. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Default Purpose Checkbox—</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Selecting this check box automatically populates the flight purpose when adding
                            a new passenger in the passenger database. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Personal Travel Checkbox—</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Selecting this check box indicates the code is used for personal travel, SIFL calculations
                            will be generated in Preflight and Postflight when a passenger travels and is assigned
                            a Personal Travel Flight Purpose <a name="_GoBack"></a></span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
