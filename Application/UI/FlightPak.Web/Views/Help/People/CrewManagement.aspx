﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="CrewManagement.aspx.cs" Inherits="FlightPak.Web.Views.Help.CrewManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Management</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Crew
Roster is used to add and maintain all information related to a specific crew
member. The Crew Roster contains personal information about a crew member such
as name, address, home, business and mobile /cell phone numbers, home base.
Crew Roster also contains sub tables that contain other information for a
specific crew member. These sub tables include additional crew information,
crew check list, type ratings information and crew currency and any aircraft
each crewmember is assigned to. </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040859"><span
style='font-family:"Calibri","sans-serif"'>How can I access the Crew Roster
Table?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the People  tab</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Crew Roster link</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040860"><span
style='font-family:"Calibri","sans-serif"'>How can I Add a Crew Member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Crew Roster table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Crew member Last name. This is a
required field.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Crew member first name. This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Home base for the Crew member. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button, the system will assign
a Crew Code to the crew member based on the Crew code rules defined in the
Company Profile – Database tab. Crew codes can’t be edited </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>&nbsp;</p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;margin-bottom:.0001pt;text-align:justify;line-height:150%'>The
rest of the fields on the Crew Maintenance display are optional.</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:56.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>These optional fields include address and phone
numbers</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Gender code. F – Female or M – Male </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Address 1, Address 2, Address 3</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>City</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>State / Province</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Postal Code</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country. Country must be valid in the Country table </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Personal, Business, and Mobile/ Cell Phone numbers</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Pager, Fax Numbers</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Business and Other E-mail address</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;line-height:150%'>&nbsp;</p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>There are fields with citizenship and related
data. This information may be used as part of APIS filing or if Visas are
required. These fields include </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Date of Birth. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Social Security No.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>City of Birth.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>State / Province of Birth.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country of Birth code. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country of Citizenship code. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Hire Date. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Termination Date - This date would be entered if the crew member is
no longer employed. Terminated crew members will not display when you access
the Crew Roster from Pre-flight. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Crew member can also be configured with
filters. When these filters are set, the Crew member may only be visible in the
Crew Roster when those filters are set. These filters include </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Client Code</span></b><span lang=EN-GB> - If the Crew member is
assigned to a Client Code, if that Client code is selected in Pre-flight Trip,
when available crew are displayed, only those crew members assigned to the
Client code selected will display as default. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Active Crew</span></b><span lang=EN-GB> - This indicate the crew
member is active. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Fixed Wing and Rotary Wing</span></b><span lang=EN-GB> - If a Pre-flight
Trip is configured with an aircraft type of fixed or rotary, only crew members
checked as fixed or rotary will display in crew availability as default.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>No Calendar Display</span></b><span lang=EN-GB> - If selected, the
crew member will not appear on scheduling calendars. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Department Code</span></b><span lang=EN-GB> - Enter a valid
department code from the Department Table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The crew members pilots’ license and / or
additional license information  can be entered in the following fields</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>License No, Expiry Date, City, Country and License type</span></b><span
lang=EN-GB>.   Country must be a valid country in the Country Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Additional License No, Expiry Date Country and License type</span></b><span
lang=EN-GB>.   Country must be a valid country in the Country Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Crew Type</span></b><span lang=EN-GB> - This is a description field
that can be used to define crew. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
line-height:150%'><span lang=EN-GB> </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Notes can be entered specific to that crew
member. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Images specific to a crewmember can be attached.
</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>                                </p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Crew Type
Rating </b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>After a Crew member has been added to the Crew
Roster, their qualifications for each aircraft type must be added. This is done
using Crew Roster – Type Ratings. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>This table contains the aircraft types a crew
member is qualified in as well as the positions they are qualified in.  The
supported  entries include : PIC – Pilot in Command; SIC - Second in Command;
Attendant; Engineer; Instructor; Check Airman; Check Attendant.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The “qualified in type” is further defined based
on FAR rules 91, 121, 125 and 135. A specific Crew member’s flight experience
can be updated. </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>For a new crew member, you can enter their
beginning Flight experience for the aircraft Type and position. Update existing
experience in a specific type updating and Update all Flight experience can be
entered as well.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040861"><span
style='font-family:"Calibri","sans-serif"'>How can I add Type Ratings for a
Crew Member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Crew member to have Type Rating added.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Type Rating tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Rating button. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Aircraft Type from the Aircraft Type table.
This is the only required field to save a record in the Type Rating sub table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Check the Qualified In Type check box that
corresponds to the crew members qualifications. A crew member can be qualified
as both PIC and SIC and can also be qualified in FAR 91 and other rules </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter / update the Beginning Flight Experience –
As of Date. Future dates are not allowed</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Total Hours – In type, Day, Night, Instrument
– numeric, can’t be negative </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter PIC Hours – In type, Day, Night,
Instrument – numeric, can’t be negative value</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter  SIC Hours – In type, Day, Night,
Instrument – numeric, can’t be negative value</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Other  Hours – Simulator, Flt. Engineer,
Flt. Instructor, Flt Attendant – numeric, can’t be negative value </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Total Time in; Total Day, Total Night and Total
Instrument values for the aircraft type will be updated </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<p class=MsoListParagraphCxSpLast style='margin:0in;margin-bottom:.0001pt;
text-align:justify;line-height:150%'><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Crew Passport
/ Visa Records</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Crew members may work on flights that require
Passports and Visa.  Passport and Visa information is not required to add a
crew member. It is possible for a crew member to have multiple passports. 
Flight Scheduling Software allows you to designate which Passport should be used as a default by
marking a passport record as “Choice”. You may also mark a Passport / Visa as
Inactive.   </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040862"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Passport for a crew
member?</span></a><span style='font-family:"Calibri","sans-serif"'> </span></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member you want to add a
passport</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passports  / Visas tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Passport  button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Passport Number. This is a required
field to add a passport record.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If this passport is to be marked as Choice,
check the Check box</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Expiration date. This is a required
field  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issuing City</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter issue date </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the passport. This is a
required field.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040863"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Visa for a crew
member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member you want to add a visa
record</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passports / Visas tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Visa   button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the Visa. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Visa Number. This is a required field.
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter an Expiration Date. This is a required
field.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issue date.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>&nbsp;</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040864"><span
style='font-family:"Calibri","sans-serif"'>How can I add Additional Information
for a Crew member?</span></a></h3>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Flight Scheduling Software allows you to capture information about a
crew member in a couple of ways. A simple way to do this is to add Notes to a
Crew member record.  A more structured way is to use the Additional Info
feature. The Additional Info feature allows you the option to select a list of
predefined Information, apply it to a new or existing crew member and then add
crew member specific data for each code you assign. The specific information is
not required but may be needed. Example - identify a crew members “Hertz Gold
Card “to his Crew member record.  You would select the Hertz Gold Card code
from the Crew Additional Info table and then enter the crew members Gold Card
Number in the Information field.   </p>

<p class=MsoNormalCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;line-height:150%'>&nbsp;</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Crew member to have Additional
Information added from the Crew Roster grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Additional Information tab </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Crew Additional Info table will display any
existing Add Info codes that have been assigned to the Crew member</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the desired code and description exists you
can enter the specific information in the Additional Information field   </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button  </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'><b>&nbsp;</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'><b>Crew Currency</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Crew Currency is a sub table within the Crew Roster
where the crew “currency” – i.e. flight hours, take off and landings and other
metrics are displayed. Crew currency information is capture when Post-flight
logs are entered / completed. Variances are displayed by comparing the
“actuals” to the Currency rules defined in Company Profile – Preflight parameter
settings. When selecting crew members for a trip in Preflight, the system
validates the crew’s actuals to the currency rules and will display exceptions
if a Crew member has exceeded the defined maximums or has less than the
minimums required to fly a specific Trip. Crew currency data is read only and
can’t be modified in the Crew Roster – Crew Currency table. Crew Currency will
also allow you to view a summary of the crew members check list and status of
each check list item.</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040865"><span
style='font-family:"Calibri","sans-serif"'>How can I View a Crew members “Crew
currency” and “Crew Check List”</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member whose Crew Currency you
want to view </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Crew Currency tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Crew Currency display will appear showing you
the following information </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Aircraft type codes </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Day and  Night Take offs – based on the “days range” defined in
company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Day and Night Landings – based on the “days range” defined in
company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Appr and Instrument – based on the days range defined in company
profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Flight and rest hours – based on days / months / quarters  range
defined in company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If you want to see the Crew Check List, select the
Check List view within  Crew Currency table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The display will change to show the crew members
check list items and related info as follows:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Check list item description</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Due date, Alert Period and Grace period </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Aircraft Type and Description</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Current Flight Hours </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Total Required Flight hours </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If a check list item is past due it will be
highlighted in red</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:.75in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If a check list item is within its Grace period
it will be highlighted in yellow</span></p>

<p class=MsoNormal>&nbsp;</p>

</div><div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%'>Crew
Roster is used to add and maintain all information related to a specific crew
member. The Crew Roster contains personal information about a crew member such
as name, address, home, business and mobile /cell phone numbers, home base.
Crew Roster also contains sub tables that contain other information for a
specific crew member. These sub tables include additional crew information,
crew check list, type ratings information and crew currency and any aircraft
each crewmember is assigned to. </span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040859"><span
style='font-family:"Calibri","sans-serif"'>How can I access the Crew Roster
Table?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the People  tab</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Crew Roster link</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040860"><span
style='font-family:"Calibri","sans-serif"'>How can I Add a Crew Member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Crew Roster table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Crew member Last name. This is a
required field.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Crew member first name. This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Home base for the Crew member. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button, the system will assign
a Crew Code to the crew member based on the Crew code rules defined in the
Company Profile – Database tab. Crew codes can’t be edited </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>&nbsp;</p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;margin-bottom:.0001pt;text-align:justify;line-height:150%'>The
rest of the fields on the Crew Maintenance display are optional.</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:56.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>These optional fields include address and phone
numbers</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Gender code. F – Female or M – Male </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Address 1, Address 2, Address 3</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>City</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>State / Province</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Postal Code</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country. Country must be valid in the Country table </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Personal, Business, and Mobile/ Cell Phone numbers</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Pager, Fax Numbers</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:92.25pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Business and Other E-mail address</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;line-height:150%'>&nbsp;</p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>There are fields with citizenship and related
data. This information may be used as part of APIS filing or if Visas are
required. These fields include </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Date of Birth. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Social Security No.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>City of Birth.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>State / Province of Birth.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country of Birth code. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Country of Citizenship code. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Hire Date. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Termination Date - This date would be entered if the crew member is
no longer employed. Terminated crew members will not display when you access
the Crew Roster from Pre-flight. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Crew member can also be configured with
filters. When these filters are set, the Crew member may only be visible in the
Crew Roster when those filters are set. These filters include </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Client Code</span></b><span lang=EN-GB> - If the Crew member is
assigned to a Client Code, if that Client code is selected in Pre-flight Trip,
when available crew are displayed, only those crew members assigned to the
Client code selected will display as default. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Active Crew</span></b><span lang=EN-GB> - This indicate the crew
member is active. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Fixed Wing and Rotary Wing</span></b><span lang=EN-GB> - If a Pre-flight
Trip is configured with an aircraft type of fixed or rotary, only crew members
checked as fixed or rotary will display in crew availability as default.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>No Calendar Display</span></b><span lang=EN-GB> - If selected, the
crew member will not appear on scheduling calendars. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Department Code</span></b><span lang=EN-GB> - Enter a valid
department code from the Department Table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The crew members pilots’ license and / or
additional license information  can be entered in the following fields</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>License No, Expiry Date, City, Country and License type</span></b><span
lang=EN-GB>.   Country must be a valid country in the Country Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Additional License No, Expiry Date Country and License type</span></b><span
lang=EN-GB>.   Country must be a valid country in the Country Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Crew Type</span></b><span lang=EN-GB> - This is a description field
that can be used to define crew. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
line-height:150%'><span lang=EN-GB> </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Notes can be entered specific to that crew
member. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Images specific to a crewmember can be attached.
</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>                                </p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Crew Type
Rating </b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>After a Crew member has been added to the Crew
Roster, their qualifications for each aircraft type must be added. This is done
using Crew Roster – Type Ratings. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>This table contains the aircraft types a crew
member is qualified in as well as the positions they are qualified in.  The
supported  entries include : PIC – Pilot in Command; SIC - Second in Command;
Attendant; Engineer; Instructor; Check Airman; Check Attendant.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The “qualified in type” is further defined based
on FAR rules 91, 121, 125 and 135. A specific Crew member’s flight experience
can be updated. </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>For a new crew member, you can enter their
beginning Flight experience for the aircraft Type and position. Update existing
experience in a specific type updating and Update all Flight experience can be
entered as well.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040861"><span
style='font-family:"Calibri","sans-serif"'>How can I add Type Ratings for a
Crew Member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Crew member to have Type Rating added.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Type Rating tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Rating button. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Aircraft Type from the Aircraft Type table.
This is the only required field to save a record in the Type Rating sub table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Check the Qualified In Type check box that
corresponds to the crew members qualifications. A crew member can be qualified
as both PIC and SIC and can also be qualified in FAR 91 and other rules </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter / update the Beginning Flight Experience –
As of Date. Future dates are not allowed</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Total Hours – In type, Day, Night, Instrument
– numeric, can’t be negative </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter PIC Hours – In type, Day, Night,
Instrument – numeric, can’t be negative value</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter  SIC Hours – In type, Day, Night,
Instrument – numeric, can’t be negative value</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Other  Hours – Simulator, Flt. Engineer,
Flt. Instructor, Flt Attendant – numeric, can’t be negative value </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Total Time in; Total Day, Total Night and Total
Instrument values for the aircraft type will be updated </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<p class=MsoListParagraphCxSpLast style='margin:0in;margin-bottom:.0001pt;
text-align:justify;line-height:150%'><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Crew Passport
/ Visa Records</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Crew members may work on flights that require
Passports and Visa.  Passport and Visa information is not required to add a
crew member. It is possible for a crew member to have multiple passports. 
Flight Scheduling Software allows you to designate which Passport should be used as a default by
marking a passport record as “Choice”. You may also mark a Passport / Visa as
Inactive.   </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040862"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Passport for a crew
member?</span></a><span style='font-family:"Calibri","sans-serif"'> </span></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member you want to add a
passport</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passports  / Visas tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Passport  button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Passport Number. This is a required
field to add a passport record.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If this passport is to be marked as Choice,
check the Check box</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Expiration date. This is a required
field  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issuing City</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter issue date </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the passport. This is a
required field.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040863"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Visa for a crew
member?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member you want to add a visa
record</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passports / Visas tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Visa   button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the Visa. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Visa Number. This is a required field.
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter an Expiration Date. This is a required
field.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issue date.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>&nbsp;</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040864"><span
style='font-family:"Calibri","sans-serif"'>How can I add Additional Information
for a Crew member?</span></a></h3>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Flight Scheduling Software allows you to capture information about a
crew member in a couple of ways. A simple way to do this is to add Notes to a
Crew member record.  A more structured way is to use the Additional Info
feature. The Additional Info feature allows you the option to select a list of
predefined Information, apply it to a new or existing crew member and then add
crew member specific data for each code you assign. The specific information is
not required but may be needed. Example - identify a crew members “Hertz Gold
Card “to his Crew member record.  You would select the Hertz Gold Card code
from the Crew Additional Info table and then enter the crew members Gold Card
Number in the Information field.   </p>

<p class=MsoNormalCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;line-height:150%'>&nbsp;</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Crew member to have Additional
Information added from the Crew Roster grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Additional Information tab </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Crew Additional Info table will display any
existing Add Info codes that have been assigned to the Crew member</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the desired code and description exists you
can enter the specific information in the Additional Information field   </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button  </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'><b>&nbsp;</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'><b>Crew Currency</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Crew Currency is a sub table within the Crew Roster
where the crew “currency” – i.e. flight hours, take off and landings and other
metrics are displayed. Crew currency information is capture when Post-flight
logs are entered / completed. Variances are displayed by comparing the
“actuals” to the Currency rules defined in Company Profile – Preflight parameter
settings. When selecting crew members for a trip in Preflight, the system
validates the crew’s actuals to the currency rules and will display exceptions
if a Crew member has exceeded the defined maximums or has less than the
minimums required to fly a specific Trip. Crew currency data is read only and
can’t be modified in the Crew Roster – Crew Currency table. Crew Currency will
also allow you to view a summary of the crew members check list and status of
each check list item.</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040865"><span
style='font-family:"Calibri","sans-serif"'>How can I View a Crew members “Crew
currency” and “Crew Check List”</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the crew member whose Crew Currency you
want to view </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Crew Currency tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Crew Currency display will appear showing you
the following information </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Aircraft type codes </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Day and  Night Take offs – based on the “days range” defined in
company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Day and Night Landings – based on the “days range” defined in
company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Appr and Instrument – based on the days range defined in company
profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Flight and rest hours – based on days / months / quarters  range
defined in company profile </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If you want to see the Crew Check List, select the
Check List view within  Crew Currency table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The display will change to show the crew members
check list items and related info as follows:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Check list item description</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Due date, Alert Period and Grace period </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Aircraft Type and Description</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Current Flight Hours </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Total Required Flight hours </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If a check list item is past due it will be
highlighted in red</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:.75in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If a check list item is within its Grace period
it will be highlighted in yellow</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>
             
            </td>
        </tr>
    </table>

</asp:Content>
