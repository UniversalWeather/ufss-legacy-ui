﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="PassengerHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.PassengerHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passengers/Requestors</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='line-height: 20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Passengers/Requestors
                        Table is designed for keeping records of your passengers and trip requestors.
                    </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 12.0pt; line-height: 20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are key features:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                            Code is system assigned based on the PAX Code configuration in the Company Profile
                            – Database Table. .</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        PAX Name</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                            Passenger’s name information. First, Middle and Last Name.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Address </span></b></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Business and Personal Cell and other Phone numbers —</span></b><span lang="EN-US"
                            style='font-family: "Arial","sans-serif"'> Passenger’s phone number.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Date of birth —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Passenger’s date of birth.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Department/Authorization —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Allows you to set trip authorization parameters by department and designate corresponding
                            codes and descriptions for each. See <a href="<%=ResolveClientUrl("~/Views/Help/Company/DepartmentAuthorizationHelp.aspx?Screen=DeptAuthorizationHelp") %>"
                                style="font: 12px arial;">Department/Authorization</a> for more details.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Std Billing -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This data field can hold billing information such as charge codes and credit card
                            numbers. It is intended for use in billing reports and DataExport.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Active —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Select
                            this check box to designate this passenger as active. If you want to remove a passenger
                            from the reports, simply leave the check box clear to retain their history in the
                            system.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Client Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Use this field to assign a <a href="<%=ResolveClientUrl("~/Views/Help/People/ClientCodeHelp.aspx?Screen=ClientHelp") %>" style="font:12px arial;" >Client Code</a> to this
                            passenger or requestor. To choose a code from the Client Table, simply click the
                            button to the right of the data line.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The passenger’s Home Base ICAO. Click the button to the right of the data line to
                            choose an ICAO from the <a href="<%=ResolveClientUrl("~/Views/Help/Logistics/AirportHelp.aspx?Screen=AirportHelp") %>" style="font:12px arial;" >Airport Table</a> or key it
                            in.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Flight Purposes —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Assign a purpose for this passenger’s flight. If there is a default flight purpose
                            set in the Flight Purpose database, that data will automatically post to this field.
                            When this passenger is assigned to a trip in the Passenger Manifest, this flight
                            purpose will post for all legs. Click the button to the right of the data line to
                            choose a purpose code from the <a href="<%=ResolveClientUrl("~/Views/Help/People/FlightPurposeHelp.aspx?Screen=FlightPurposeHelp") %>" style="font:12px arial;" >Flight Purposes Table</a>.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Type —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Select
                            one of the three radio buttons to identify your passenger’s travel status. This
                            is required for passengers and their guests. Choose control, non-control and guest
                            designations as defined by SIFL regulations. </span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Associated With —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This relates directly to the SIFL reporting function. Whenever you have a guest,
                            this box will be activated and you can assign the passenger he or she is associated
                            with.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        SSN —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Passenger’s
                            social security number to be printed on customs forms and exported to Travel$ense.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Title —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Passenger’s
                            position or function name to be exported to Travel$ense.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Salary Level —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Numerical or alphabetical salary range representation to be exported to Travel$ense.</span></li>
                    <li class="MsoNormal" style='line-height: 20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Notes -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Here
                            is where you can store passenger-specific preferences such as dietary requirements
                            that might prove helpful when planning in-flight, ground, or related travel services</span></li>
                </ul>
                <p class="MsoNormal" style='margin-left: 18.0pt; line-height: 20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Other sub tables are also
                        connected to passport, visa and additional info to each passenger</span><span lang="EN-US"
                            style='font-family: "Arial","sans-serif"'> </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='margin-left: 54.0pt; text-indent: -18.0pt;
                    line-height: 20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Passport
                        – Contains Passport Number, Nation, Expiration Date, Issue Date, Issue City / Authority.
                        Since a Passenger can have more than one passport, a “Choice” can also be set on
                        specific Passport which will be used for trips as the default passport</span></p>
                <p class="MsoListParagraphCxSpLast" style='margin-left: 54.0pt; text-indent: -18.0pt;
                    line-height: 20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Visa – Contains
                        <a name="_GoBack"></a>Country, Visa Number, Expiration Date, Issuing City / Authority,
                        Issue Date and Notes </span>
                </p>
            </td>
        </tr>
    </table>
</asp:Content>
