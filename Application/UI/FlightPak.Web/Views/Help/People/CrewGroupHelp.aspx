﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewGroupHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewGroupHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Group</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Crew Group table allows
                        you to group your crew into specific categories (i.e. type of aircraft they are
                        qualified in Turbojets, helicopter, or by type of crew – pilots, attendants, etc).
                    </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Each Crew group can be
                        assigned an alpha or numeric <b>Code</b> and a corresponding <b>Description</b>.
                    </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <a name="_GoBack"></a><span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Crew members can be assigned
                        to a group by “dragging” them from the available grid to the selected grid. Crew
                        members can be unassigned from a crew group by “dragging” selected crew from the
                        assigned grid to the unassigned grid. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> This is a four-character
                        identifier for your crew group. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> Name for your crew group
                        that corresponds with the assigned code. </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
