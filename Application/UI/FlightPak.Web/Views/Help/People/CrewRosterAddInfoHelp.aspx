﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewRosterAddInfoHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewRosterAddInfoHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Additional Info Codes</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>This table consists of
                        records describing the various types of additional information about an individual
                        crewmember. Each record of the Crew Additional Info table represents a particular
                        item about each crewmember that you would like to track. <a name="_GoBack"></a>
                    </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Each desired
                            item to be tracked by crew is assigned a code of no more than three characters.
                            It is strongly recommend that you code Crew Add Info Codes so that they are grouped
                            together logically. For example, begin by using code 100, then index each subsequent
                            entry by at least 10 or more (e.g. 110 for Home Fax, 120 for E-mail, etc. This will
                            allow flexibility should you decide to add codes in the future.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Each information item is explained on this line.</span></li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
