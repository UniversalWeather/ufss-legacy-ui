﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="PassengerManagement.aspx.cs" Inherits="FlightPak.Web.Views.Help.PassengerManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Passenger Management</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Passenger
information is entered and maintained in the Passenger/Requestor table.
Passenger data includes name, address and contact information. Personal
information such as birthdate, nationality and gender can be entered. Many of
the data fields are required if your company uses the APIS module or if your
passengers use corporate aircraft for personal travel and are required to
calculate SIFL.</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040867"><span
style='font-family:"Calibri","sans-serif"'>How can I access the
Passenger/Requestors Table?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the People  tab</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passenger/Requestors link</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040868"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Passenger record?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Passenger/Requestors table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click the Add button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Last Name and First Name for the
Passenger. These are required fields.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Middle Name. This is optional and may be
entered as an initial or name. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Update Home base. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. The system will assign
a PAX code to the new passenger based on the rules defined in the Company
Profile – Database table.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Address and
contact information for the Passenger can be entered. This information is
optional but many of the fields are required by APIS. </p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The following is a summary of the passenger
contact information you can enter</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Company. This is the company the passenger works
or is employed by.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Employee ID. This field may be the passengers
employee number or other identifier </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Passenger Business  Phone No</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Passenger Home Phone No</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Primary Mobile / Cell </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Secondary  Mobile / Cell </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Other Phone</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Business Fax </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Home Fax</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Business E-mail </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Personal E-mail</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Other E-mail </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Nationality. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Country of Residence. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Address Line 1, Address Line 2 and Address Line 3
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>City and State / Province </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Postal Code </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Personal information can also be entered.
Personal information includes the following </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Gender – F – Female; M – Male </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>PIN number. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Date of Birth. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Passenger Requestor records also have filters
that can be set for each passenger. When the Passenger requestor table is
accessed, these filters will determine if the passenger will display </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Home base</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Active checkbox</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Flight Purpose. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Client Code.       </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Mark passenger as requestor Check Box. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Upload Passenger Photo or any other document desired. This field is
used to locate photos and images of passenger documents, such as a driver’s
license or passport photo. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Billing and Tax Information can be entered for a
Passenger. These data elements are optional  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Department</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Authorization</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Std Billing. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Account No. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Tax information can be entered for a Passenger.
Tax info is used when Flight Scheduling Software SIFL calculations are generated. All these
fields are optional.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Control non-Control, Guest. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>SIFL security    </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Associated With </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Travel$ense Information is used to generate
export files for use with NBAA – Travel$ense. All this information is Optional </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Travel$ense ID. This field can be used to store social security
number or other user defined ID</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Title: Passenger’s position or function name </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Salary Level : Numerical or alphabetical salary range representation</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>PAX Passport
/ Visa Records</b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Passengers may travel
on flights that require Passports and Visa.  Passport and Visa information is
not required to add a Passenger. It is possible for a Passenger to have
multiple passports.  Flight Pak allows you to designate which Passport should
be used as a default by marking a passport record as “Choice”. You may also
mark a Passport / Visa as Inactive.   </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040869"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Passport for a Passenger?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the passenger you want to add a passport</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Passport tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Passport  button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Passport Number. This is a required
field to add a passport record.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If this passport is to be marked as Choice,
check the Check box</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Expiration date. This is a required
field  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issuing City</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter issue date </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the passport. This is a
required field.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040870"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Visa for a Passenger?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the passenger you want to add a visa
record</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Visa tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Edit button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add Visa   button </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country Code for the Visa. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Visa Number. This is a required field.
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter an Expiration Date. This is a required
field.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Issue date.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button.</span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040871"><span
style='font-family:"Calibri","sans-serif"'>How can I add Additional Information
for a Passenger?</span></a></h3>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>Flight Scheduling Software allows you to capture information about a
passenger in a couple of ways. A simple way to do this is to add Notes to a
passenger record.  A more structured way is to use the Additional Info feature.
The Additional Info feature allows you the option to select a list of
predefined Information, apply it to a new or existing passenger and then add
passenger specific data for each code you assign. The specific information is
not required but may be needed. Example - identify a passengers “Hertz Gold Card
“to his passenger record.  You would select the Hertz Gold Card code from the
passenger Additional Info table and then enter the passengers Gold Card Number
in the Information field.   </p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Passenger to have Additional
Information added.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Additional Information tab </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Additional Information table will display
any existing Add Info codes that have been assigned to the Passenger</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the desired code and description exists you
can enter the specific information in the Additional Information field   </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button  </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%'>&nbsp;</p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Flight
Purpose</b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>The Flight
Purpose codes represent reasons why passengers are traveling on a particular
leg. Codes must be set up correctly in the catalog to ensure that SIFL
calculations and reporting are accurate. A default Purpose auto-populates the
Flight Purpose code when adding a new passenger in the Passenger/Requestor
table. Flight Purposes specifically used for personal travel must be marked
accordingly. </p>

<h3 style='text-align:justify;line-height:150%'><span style='font-family:"Calibri","sans-serif"'> <a
name="_Toc335040872">How can I add a Flight Purpose record?</a></span></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the Fleet Tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Flight Purposes link.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the<b> Flight Purpose Code. </b>This is a
required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Flight Purpose <b>Description</b>.
This is a required field. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select a Default Purpose Check Box. This is an
optional setting. If a default flight purpose is set, all passengers assigned
to Pre-flight Trip / Legs will show the default flight purpose. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select Personal Travel Check Box. This is an
optional setting.  In Pre-flight – Leg, if a passenger has a flight purpose
code marked as personal, that passenger record will be included in SIFL
calculations. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter a Client Code. This is an optional field. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select Inactive Check Box – This is an optional
field. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save Button. </span></p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>
            </td>
        </tr>
    </table>

</asp:Content>
