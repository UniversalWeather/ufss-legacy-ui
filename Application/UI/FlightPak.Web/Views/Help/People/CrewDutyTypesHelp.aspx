﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CrewDutyTypesHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.People.CrewDutyTypesHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Duty Types</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Crew Duty Types Table
                        contains records that represent the type of activities each crewmember could be
                        engaged in. <a name="_GoBack"></a></span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Flight Scheduling Software requires that
                        there be pre-defined Crew Duty Types records in this database for the system to
                        work correctly. </span></i>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"'>The codes and descriptions
                        for these Duty Types are as follows:</span></i></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><i><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        F — Flight</span></i></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><i><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        R — Rest Overnight</span></i></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><i><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        G — Ground time</span></i></b></li>
                </ul>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Other examples of Duty
                        Types could be military duty, training, school, commercial travel, and any other
                        type of duty or time off that your crewmembers are often engaged in. This table
                        is a critical database for both management reporting and crew scheduling. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are some of the
                        key features: </span>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> A one
                            or two-character code must be assigned to each Duty Type description. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Textual explanation of the type of duty you are describing. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Value Points:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            You may put a positive or negative number in this field. This is a numerical value
                            that indicates the relative worth of each type of duty. This value may be helpful
                            in calculating a weighted average workload balance per crewmember. This is user
                            defined and these values are calculated on the workload index report that comes
                            from the section of the software. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Weekend Value Points:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Separate point system you develop for weighing weekend work. This is user defined
                            and these values are calculated on the workload index report. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Workload Index:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select the check box to have the specified Duty Type displayed on the Workload Index
                            report form. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Office Duty:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select this check box to count this duty type as Office Duty in the Workload Index
                            reports. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Color:</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Assign
                            a color to a specific Duty Type by clicking the arrow to the right and selecting
                            one of the colors from the drop down box. These will be displayed in the </span>
                        <span lang="EN-US" style='font-family: "Arial","sans-serif"'><a href="mk:@MSITStore:c:\fpk11\help\fpkhelp.chm::/_06M0K4DU8.htm">
                            <span style='color: windowtext; text-decoration: none'>Monthly Crew Calendar</span></a></span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
