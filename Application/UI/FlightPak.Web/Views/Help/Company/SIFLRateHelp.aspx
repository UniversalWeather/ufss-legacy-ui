﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="SIFLRateHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.SIFLRateHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">SIFL Rate</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Standard Industry Fare
                        Level (SIFL) rates for specific date ranges are maintained here. Designed as a cost
                        tracking tool, these rates are used in Preflight and Postflight to calculate SIFL
                        Charges They are also used in <a name="_GoBack"></a>Postflight Reports to calculate
                        personal travel charges. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Start/End Date</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'>: Each entry must have a
                        beginning and ending date for which a particular rate is effective. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Fare Levels</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'>: These are the rates for
                        statute miles traveled within a specific mile range. The three mile ranges are 0-500,
                        501-1,500 and all miles over 1,500. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Terminal Charge</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'>: This is the rate to be
                        charged at the final destination.</span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
