﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="DepartmentGroupHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.DepartmentGroupHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Department Group</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Department Group table
                        allows you to group your departments into groups (i.e. Marketing, Sales, Flight
                        Operations etc) Each group can be assigned an alpha or numeric <b>Code</b> and a
                        corresponding <b>Description</b>. The Table allows you to ‘’drag” various departments
                        under the selected Group Code. Departments can be ungrouped by “dragging” the specific
                        department back to the unassigned display. <a name="_GoBack"></a></span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> This is a four-character
                        identifier for your fleet group. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> Name for your fleet group
                        that corresponds with the assigned code.</span><span lang="EN-US" style='font-size: 8.0pt;
                            font-family: "Times New Roman","serif"'> </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US">&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
