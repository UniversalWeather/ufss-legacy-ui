﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="PaymentTypesHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.PaymentTypesHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Payment Types</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Payment Type Table
                        is designed for recording the various payment types, such as Visa, Master and American
                        Express, used by your flight department. Assign an alpha or numeric two-character
                        code and corresponding description to each type. Table contains two fields </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code – Abbreviation
                        used to identify a payment type – ie VC, MC, AM, UV etc<a name="_GoBack"></a></span></p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description
                        – Description of the payment type code – ie Visa, Master Card, American express</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
