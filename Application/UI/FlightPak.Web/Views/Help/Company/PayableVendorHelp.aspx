﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="PayableVendorHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.PayableVendorHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Payable Vendor</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Payables Vendor Table
                        allows you to create an unlimited number of vendor records for those you conduct
                        business with. This information will be used when recording expenses in the cost-tracking
                        module. Vendor records can be created using any information important to your business
                        needs. Notes are captured in the <b>Notes</b> section, and selecting <b>Contacts</b>
                        allows you to create and maintain detailed information on the various contacts at
                        each vendor location. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        <br>
                        Following are key features:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Alpha
                            or numeric vendor identification. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Name —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Vendor’s
                            name. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Terms —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Terms
                            under which account is paid (i.e. 15, 30, 45 days). </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Tax ID —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The
                            vendor's tax identification number. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The vendor’s Home Base ICAO. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Active —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Select
                            this check box to indicate the Payable Vendor as active. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Closest ICAO —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Designate vendor’s nearest airport. Click the button to the right of the data line
                            to choose an ICAO from the <a href="<%=ResolveClientUrl("~/Views/Help/Logistics/AirportHelp.aspx?Screen=AirportHelp") %>" style="font:12px arial;" >
Airport Table</a>. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Billing information -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            All the information you need to correspond with the Payable Vendor – includes addresses,
                            postal codes, phone numbers, E-mail addresses and fax numbers can be stored here.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Main Contact - Here’s where you can store all information about your primary contact
                        at this particular Payable Vendor<a name="_GoBack"></a>. </span></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Notes - </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Here’s
                            where you can store any additional notes you may have about this particular vendor.
                            For example: Always provide 12-hour advance notice, etc.</span></li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
