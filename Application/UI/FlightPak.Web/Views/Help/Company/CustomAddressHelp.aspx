﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CustomAddressHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.CustomAddressHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Custom Address</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Custom Address Table
                        allows you to place additional locations on your Locator map (i.e. other corporate
                        offices and special attractions). </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are the tables
                        date requirements / features: </span>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> — Alpha
                            or numeric identifier for each record in your database. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            ICAO for your home base operations. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Closest ICAO —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Airport near the item described. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Name Address and Phone numbers </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            associated with the Custom Address </span><a name="_GoBack"></a></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Metro —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The
                            metro code table is intended to provide a 'hook' for airports, hotels, etc. so that
                            searches can be executed (in the Universal Locator) for metro city areas. For example,
                            a user may want to locate all hotels that are located in the 'Houston metro area'
                            but may not be associated with the city name. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Custom symbol —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            When you map this custom address, this symbol, which is in a bitmap format, will
                            appear on your map. For example, if you have an image of a golf club, this bitmap
                            image will appear wherever plotted on your map. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Latitude/Longitude —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Allows you to plot points and identify locations on the map most accurately.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Notes -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> This
                            area is reserved for storing notes or information you would like to save specific
                            to this custom address</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
