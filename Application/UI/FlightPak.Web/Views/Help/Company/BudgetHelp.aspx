﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="BudgetHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.BudgetHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Budget</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <a name="_GoBack"></a><span lang="EN-US" style='font-family: "Arial","sans-serif"'>The
                        Budget Table is used in the cost tracking function. Here is where you store your
                        monthly cost projections for the calendar year. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are key features:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Account number —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Accounting code used by your aviation department. This originates in your <a href="<%=ResolveClientUrl("~/Views/Help/Company/AccountsHelp.aspx?Screen=AccountsHelp") %>" style="font:12px arial;" >

                                Accounts Table</a> and is selected when defining your budget. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Name of the specific account number you select such as fuel and hangar use.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Year —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The budget
                            year you intend to view. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Tail number —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The aircraft’s identification number used for sorting and dividing specific charges.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Budget months —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Months 1-12 per specified year.</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
