﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="DepartmentAuthorizationHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.DepartmentAuthorizationHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Department/Authorization</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:20px;'><span lang=EN-US style='font-family:"Arial","sans-serif"'>The
Department/Authorization Table allows you to set trip authorization parameters
by department and designate corresponding codes and descriptions for each. It
consists of two main tabular areas - <b>Department Maintenance</b> and <b>Authorization
Maintenance</b>. Since these databases are critical for management reporting,
they should not be altered without consultation with your users and management.
</span></p>

<p class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
style='font-family:"Arial","sans-serif"'>Department Maintenance</span></b><span
lang=EN-US style='font-family:"Arial","sans-serif"'> </span></p>

<p class=MsoNormal style='line-height:20px;'><span lang=EN-US
style='font-family:"Arial","sans-serif"'>Each department record represents a
corporate group serviced by your flight department. A client code can be
associated with each department. The Department Maintenance menu features the
following:</span></p>

<p class=MsoListParagraph style='margin-left:72.0pt;line-height:20px;'><span
lang=EN-US style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<ul type=disc>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Client Code —</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> This alpha or numeric
     identifier should be no more than five characters. When implemented, it
     allows an additional level of “filtering” in all areas of the system. For
     example, a user can be assigned a client code and in turn will only see
     aircraft, tripsheet, etc. that are assigned to that same client code. By
     keeping your descriptions logical and brief, you can easily input this information
     and retrieve it from the drop-down menus throughout the system. </span></li>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Department Code –</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> This is the
     identifier for the Department. </span></li>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Description/Department -</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> Required. This is a
     descriptive label. This field should not be changed without consultation
     with users. </span></li>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Inactive –</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> When the check box is
     selected the selected <a name="_GoBack"></a>Department is inactive and may
     not display when table is displayed in Preflight, Postflight and other
     transactions </span></li>
</ul>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US style='font-family:"Arial","sans-serif"'><br>
<b>Authorization Maintenance</b> </span></p>

<p class=MsoNormal style='line-height:20px;'><span lang=EN-US
style='font-family:"Arial","sans-serif"'>Each Authorization Maintenance record
represents a corporate cost center within a department or a sub-unit of the
department field. Each authorization code is found within the department code.
For example, within the admin department you have various admin groups, such as
ADM100, 200, etc., and accompanying authorization codes.</span></p>

<ul type=disc>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Authorization Code —</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> This is the
     identifier for this particular subgroup. This is the abbreviation by which
     reports are sorted. </span></li>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Authorization Description —</span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'> This is a descriptive
     label for this particular subgroup, and it will print on reports. This
     field should not be changed without authorization with the selected
     department.</span></li>
 <li class=MsoNormal style='line-height:20px;'><b><span lang=EN-US
     style='font-family:"Arial","sans-serif"'>Inactive  - </span></b><span
     lang=EN-US style='font-family:"Arial","sans-serif"'>When the check box is
     selected the selected Authorization code is inactive and may not display
     when table is displayed in Preflight, Postflight and other transactions</span></li>
</ul>

<p class=MsoNormal><span lang=EN-US style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
