﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="ConfiguringCompanyProfile.aspx.cs" Inherits="FlightPak.Web.Views.Help.ConfiguringCompanyProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Configuring Company Profile</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:20px;text-autospace:none'>Company Profile Table contains
the settings and parameters that control default settings for Preflight, Post-flight,
Reports, Database, Corporate Request and Charter Quote modules. All Company
Profile settings are set for a single home base unless specifically noted.</p>

<h3 style='line-height:150%'><a name="_Toc335040823"><span style='font-family:
"Calibri","sans-serif"; color:#3366CC;'>How can I access Company Profile?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Database button on the global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Company tab on the left menu.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Company Profile link.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040824"><span style='font-family:
"Calibri","sans-serif"; color:#3366CC;'>How can I setup and configure a new Company Profile /
Home Base?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Once
you are in the Company Profile table, click on the <b>Add</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the Company Information tab and setup as necessary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter your ICAO in the <b>Main
Base</b> field. If you would like to look at all the ICAO’s available, click on
the <b>lookup</b> button next to the Main Base field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Base Description</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Description of the Main
Base.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Company Name</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Name of the Company
Name. Company Name specified here will be displayed in all system reports
header section.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Account Format</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Defines the Account
number format in the Account table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Corporate Account Format</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Defines the Corporate
Account number format in the Account table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Federal Tax Account</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Value in this field
stores the account number for Federal Tax Account. Account must be set up in
Account table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>State Tax Account</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Value in this field
stores the account number for State Tax Account. Account must be set up in
Account table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Sales Tax Account</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Value in this field
stores the account number for Sales Taxes. Account must be set up in Account
table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Currency</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> – Currency code / symbol to
display on reports associated with the Home base.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enable TSA NO-Fly Auto
Check in Preflight</span></b><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'> – When selected, Preflight will check the TSA No-Fly list
for passengers / crew that should not fly.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> <b>Enable TSA NO-Fly Auto
Check in PAX / Requestor Table</b> – When selected, Pax / Requestor table is
checked against the TSA NO-Fly list when the PAX / Requestor Table is accessed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Note Display - </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, information
in the General Notes section will display on login for all users. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Auto Dispatch Number – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, Dispatch
number in Pre-flight will be auto generated by system.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Inactive – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, the Main
Base will be marked as Inactive.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Default Date Format – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select the default date
format for the home base from the drop down. All users assigned to this Home
Base that access Database Tables, Pre-flight, Post-flight and Reports will view
dates in this format. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Conflict Checking – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, system will
check for conflicts in trip planning for aircraft scheduling, crew scheduling,
runway length, crew checklist warnings and aircraft capacity.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Text Auto-tab – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected the cursor
will automatically move to next data field once the current field reaches the
maximum length of input.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Canpass Permit – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, the Canpass
Permit No. field is activated for user to enter Canpass Permit number.<b>  </b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the Pre-flight tab and setup as necessary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Flt Cal Timer – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Value entered in seconds
to control the auto refreshing of Scheduling Calendar display. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Boeing Winds – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Selected default % (50% or
75% or 85%) determines the default setting for all calculations using Wind
Reliability.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Ground Time –</span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'> If Auto Calc Leg Times
parameter is Turned ON, the value entered here will be added as a fixed amount
of Ground Time to the Arrival Time to determine next leg Departure Time for
newly added trip legs. Entry format (minutes or tenths) is determined by the
Time Display parameter setting either in minutes or tenths.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Time Display – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Default display of Time
and Flight/Block Hour value, can be set to Tenths or Minutes. “Minutes” is
simple clock time in HH:MM format. “Tenths” is the conversion of HH:MM value to
its decimal equivalent.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>ETE Round - </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>This setting determines
the “minutes rounding rules” for ETE values that are calculated in the trip
manager<b>. </b>Selecting<b> None </b>will display the actual ETE<b> </b>and
selecting<b> 5M or 10M or 15M or 30M or 1 Hour </b>will round the calculated
ETE to the nearest selected minute or hour</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Default Crew Duty Rules - </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>This is looked up from
Crew Duty Rule table. The selected Crew Duty Rule will be set as the default
crew duty rule for new trips. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Default FAR – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Selected FAR rule will be
set as default FAR for new trips.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Deactivate Home Base
Filter – </span></b><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
selected, when the user accesses any module with a Home Base filter, the
default Home Base Filter is turned “OFF” otherwise the Home Base Filter is
turned “ON&quot;<b>. </b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Auto Calc Leg Times – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, the Flight
time is calculated when legs are added or edited. Otherwise the system will not
calculate the flight time when legs are added or edited.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Use Checklist Warnings – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>If selected, when the user
edits the date, time or ICAO on a trip, and saves the trip, the system will
show a warning message <b>&quot;Reset Logistics Checklist?&quot;</b> with Yes
and No Buttons.  If user selects “Yes”, then all the related checklist items
for each leg will be reset. If this parameter is not selected, the warning
message does not display. Checklist Items are:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Crew Hotel</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Maintenance Crew Hotel</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Crew Transportation at arrival ICAO</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Crew Transportation at Departure ICAO</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Depart Catering</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Arrive Catering </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>PAX Hotel</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Arrive PAX Transportation</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:50px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:Wingdings'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>Depart PAX Transportation</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Assign All Checklist Items
to New Crew Members – </span></b><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>When selected, system will assign all Crew Roster checklists
items to a crew member when added to a Trip.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Do Not Update Fuel Brand –
</span></b><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
selected, Logistics Update routine will not update Fuel Brand in Airport – FBO
table.<b> </b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Assign Dept/Auth during
Requestor Selection - </span></b><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>If selected, the system will assign requestor’s Department
Code and Authorization Code when a new trip is created in Pre-flight.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Include Non-Logged
Tripsheets in Crew History - </span></b><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>If selected, the system will include trips that are not
logged in Post Flight in Pre-flight Crew History. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Auto Validate Preflight
– </span></b><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
selected, the system will check for aircraft scheduling and crew scheduling conflicts
when the trip is saved. Exceptions are raised if there are any conflicts in
scheduling. If not selected, the Preflight will not raise exceptions.<b> </b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Auto Create Crew Availability Info</span></b><span lang=EN-GB> - If
selected, Pre-flight Crew Availability grid will display all crew available for
assignment.  If not selected, the Crew availability grid will not auto populate.
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Show Requestors Only</span></b><span lang=EN-GB> - If selected, only
PAX / Requestor records in Pax Requestor table marked as Requestor will display
when looking up the requestor for a trip in Pre-flight. If not selected, all
the records in the passenger/requestor table will display.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Highlight Aircraft Having Notes</span></b><span lang=EN-GB> – If
selected, when a trip is assigned an aircraft, Tail Number will appear in <b>RED</b>
if there are any notes for that aircraft in Fleet Profile. Placing cursor over the
tail number will display the note. If not selected, Tail number will not be
highlighted. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Highlight Crew Having Notes and/or Addl Notes</span></b><span
lang=EN-GB> – If selected, when a crew is added to trip, crew code and name
will appear in <b>RED</b> if there are any notes or additional notes for that
crew in Crew Roster. Placing cursor over the crew code will display the note.  If
not selected, crew code and name will not be highlighted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Highlight Passenger Having Notes and/or PAX Alerts</span></b><span
lang=EN-GB> – If selected, when a passenger is added to a trip, PAX code and
name will appear in <b>RED</b> if there are any notes or alerts for that PAX
code in Passengers/Requestors catalog. If not selected, PAX code and name will
not be highlighted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Preflight Search Days</span></b><span lang=EN-GB> <b>Back -</b>
Value entered represents the number of days from system date to use as Starting
Depart Date in Search criteria for Pre-flight.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Manager Date Warnings</span></b><span lang=EN-GB> - Defines maximum
days allowed between two consecutive legs to be planned in a trip. If the value
is exceeded, an exception will be raised for the trip in Pre-flight. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Default Flight Category</span></b><span lang=EN-GB> – The selected
Flight Category will auto populate in Pre-flight when a trip is created.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Default Check List Group</span></b><span lang=EN-GB> – The selected
Check List Group will auto populate when a trip leg is added.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Preflight Status</span></b><span lang=EN-GB>- The selected Trip
Status will be the default status set by the system when a trip is created.  Valid
statuses are: T - Tripsheet, W- Worksheet, U –Unfulfilled, C – Canceled and H
– Hold.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Default Tripsheet Report Writer Group</span></b><span lang=EN-GB> –
Tripsheet Report Writer can have multiple report groups. Value entered here
will display as default when Tripsheet Report Writer is accessed. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>GA Desk Id</span></b><span lang=EN-GB> – Text field entry is used
with Fleet group for GA Desk Report. GA Desk Id is assigned by Federal Aviation
Administration.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Check Crew Qualification Against Leg FAR</span></b><span lang=EN-GB>
– When selected, the system will check for Type Rating for each crew assigned
against Leg’s FAR and raise an exception if a crew member is not qualified to
fly the aircraft.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Check 24-hr Trip Overlap for Crew – </span></b><span lang=EN-GB>If
selected, the system will check for other trips scheduled within 24 hours
before and after each leg to check if a crew member’s duty is conflicting with
duty hours. If there are conflicts, the system will raise an exception. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Check Passenger Overlap</span></b><span lang=EN-GB> – If selected,
the system will check if a PAX code is assigned to other trips that have trip
leg times conflicting with current trip legs times If there are conflicts, the
system will raise an exception.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Check Tail Inspection Due</span></b><span lang=EN-GB> –If selected,
the system will check for total trip flight hours for the tail number against
Inspection due/warning details in Fleet Profile and raises exception in case
the aircraft has passed flight hours set for inspection due check. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:25px;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-size:10.0pt;
line-height:20px;font-family:"Courier New";color:black'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Company Logo</span></b><span lang=EN-GB> – The uploaded company logo
used to display on reports.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Logo Position</span></b><span lang=EN-GB> – Defines the print
location of the company logo on reports. If None is selected, the company logo
will not be printed on reports.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Crew Currency –</span></b><span lang=EN-GB> Used to define the Crew
Duty Rules, Minimum and Maximum ranges and labels for crew currency.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Privacy Settings List is shown –</span></b><span lang=EN-GB> User
can turn on / off the display of items to restrict what information is
displayed on screens and reports.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Custom Flight Log – </span></b><span lang=EN-GB>User can turn on /
off<b> </b>options that are used on Pre-flight Tripsheet Report Writer - Flight
Log Report.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the Post-flight tab and setup as necessary.</span></p>

<p class=Default style='margin-left:25px;text-align:justify;text-indent:-.25in;
line-height:150%'><span style='font-size:11.0pt;line-height:20px;font-family:
"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
style='font-size:11.0pt;line-height:20px;font-family:"Calibri","sans-serif"'>Log
Fixed Wing</span></b><span style='font-size:11.0pt;line-height:20px;font-family:
"Calibri","sans-serif"'> </span><span style='font-family:"Calibri","sans-serif"'>–
</span><span style='font-size:11.0pt;line-height:20px;font-family:"Calibri","sans-serif"'>Controls
the scheduled time either HOME or UTC that is sent over to Post-flight Flight
Logs from</span><span style='font-size:11.0pt;line-height:20px;font-family:
"Calibri","sans-serif";color:windowtext'> </span><span style='font-size:11.0pt;
line-height:20px;font-family:"Calibri","sans-serif"'>Preflight.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Log Rotary Wing</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
Controls </span><span lang=EN-GB>the scheduled time either<span
style='color:black'> Local or UTC that is sent over to Post-flight Flight Logs
from Pre-flight.</span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Auto Populate Leg Times</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–If
selected, system will populate Legs Departure and Arrival times automatically
for Scheduled Date-time, Out/Off/On/In and Date/Time. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Taxi Time</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>– The
value entered here will be added to Post-flight Legs Off/In fields. Taxi time
is the time anticipated for an aircraft to come out of wheel chocks and wait on
the runway before actual take off time and the time to return to its wheel
chocks after landing. Auto Populate Leg Times must be enabled to use this
function. Format is in HH:MM.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Fuel Purchase Units</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
Default measurement unit for fuel purchased either in U.S. Gallons, Litres,
Kilos, Pounds, Imp. Gallons.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Fuel Burn</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
Default measurement unit for fuel consumption either in Pounds or Kilos.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Tenths-Min Conversions</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>-
Tenths-Min conversion offers 3 settings to convert Hours and Minutes to Decimal
equivalent when the Time Display is set to Tenths. You can specify your own
conversion by choosing “Other”.  Hover cursor over each option to view the
conversion details.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Fiscal Year</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
Your Company’s Fiscal year Start and End month.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Remove End Duty marker on
last leg when adding a new leg</span></b><span lang=EN-GB style='color:black'>-
If selected, the </span><span lang=EN-GB style='color:black'>system will
automatically mark the last leg for Duty End. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Prompt to calculate SIFL
while saving a Flight Log</span></b><span lang=EN-GB style='color:black'> </span><span
lang=EN-GB style='color:black'>– If selected, the system will display a message
to calculate SIFL if any of the legs have passengers with personal travel
flight purpose. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Auto Fill Engine Airframe
Hours and Landings</span></b><span lang=EN-GB style='color:black'> – Determines
whether the S</span><span lang=EN-GB style='color:black'>ystem will update
Fleet Profile Engine/Airframe Catalog for an aircraft with block/flight hours
or adjusted hours-landings when user marks the flight log as completed.</span></p>

<p class=Default style='margin-left:25px;text-align:justify;text-indent:-.25in;
line-height:150%'><span style='font-size:11.0pt;line-height:20px;font-family:
"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
style='font-size:11.0pt;line-height:20px;font-family:"Calibri","sans-serif"'>Post-flight
Crew Duty Basis</span></b><span style='font-size:11.0pt;line-height:20px;
font-family:"Calibri","sans-serif"'>- D</span><span style='font-size:11.0pt;
line-height:20px;font-family:"Calibri","sans-serif"'>etermines if crew duty
hours begin with scheduled departure date-time or Block Out date-time in Post-flight.
</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Post-flight Aircraft
Basis</span></b><span lang=EN-GB style='color:black'> </span><span lang=EN-GB
style='color:black'>– Aircraft basis can be set to block time or flight time. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Use Log sheet Warnings</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
If selected, the system will display warning messages in the Post-flight
Exceptions tab if there are any warnings in the flight log. </span></p>

<p class=Default style='margin-left:25px;text-align:justify;text-indent:-.25in;
line-height:150%'><span style='font-size:11.0pt;line-height:20px;font-family:
"Courier New";color:windowtext'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span style='font-size:11.0pt;line-height:20px;font-family:
"Calibri","sans-serif"'>Log Blocked Seats</span></b><span style='font-size:
11.0pt;line-height:20px;font-family:"Calibri","sans-serif"'> </span><span
style='font-size:11.0pt;line-height:20px;font-family:"Calibri","sans-serif"'>–
If selected, the system will carry blocked seats entry from Preflight to Post-flight
when a trip is logged. </span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Default Blocked Seats
Flight Purpose</span></b><span lang=EN-GB style='color:black'> </span><span
lang=EN-GB style='color:black'>– The selected Flight Purpose code will be
assigned to the blocked seats entry in Post-flight Passengers tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>SIFL Layover Hours</span></b><span
lang=EN-GB style='color:black'> </span><span lang=EN-GB style='color:black'>–
Allows the user to set a value for SIFL Layover Hours.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-family:"Courier New";color:black'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><b><span lang=EN-GB style='color:black'>Flight Log Search Days
Back</span></b><span lang=EN-GB style='color:black'> </span><span lang=EN-GB
style='color:black'>– </span><span lang=EN-GB>Value entered represents the
number of days from system date to use as Starting Depart Date in Search
criteria for Post-flight.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the Database tab and setup as necessary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>PAX Code Setting</span></b><span lang=EN-GB> – This is a <b>company-wide
setting</b> and controls the system assigned format of the Passenger code when
a passenger record is added to the Passenger / Requestor Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Crew Code Setting</span></b><span lang=EN-GB> - This is a <b>company-wide
setting</b> and controls the system assigned format of the Crew code when a
crew member is added to the Crew Roster Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the Reports tab and setup as necessary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:20px;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:20px;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the APIS tab and setup as necessary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enable APIS Interface – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>When selected, APIS
buttons will be active in Pre-flight.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Alert To Resubmit Trips
when APIS Data is edited – </span></b><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>When selected, system will prompt to resubmit a Trip if any
APIS information is edited.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Sender ID – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>ID provided by Customs and
Border Patrol to submit APIS transactions electronically. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:20px;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:20px;font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>CBP User Password – </span></b><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Password provided by
Customs and Border Patrol to submit APIS transactions electronically.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040825"><span style='font-family:
"Calibri","sans-serif"; color:#3366CC;'>How can I define my Passenger and Crew Codes?</span></a></h3>

<p class=MsoNormal style='line-height:150%'>Passenger and Crew codes are
automatically assigned by the system. The codes are based on a combination of
Initials from the Passenger or Crews’ First, Middle and Last names. The
Passenger and Crew codes have separated combinations. If a passenger or crew
has the same initial combination as an existing passenger or crew, the system
will automatically assign a numeric code in the last position of the code.</p>

<p class=MsoNormal style='line-height:150%'><b><i>Note - Once the Passenger and
Crew Code combinations are defined, they can’t be edited.</i></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the <b>Database</b> tab</span><span lang=EN-GB> in Company Profile.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Under <b>PAX Name Elements</b>, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Define the first, middle and last name sequence using the up, down
arrows or drag and drop option. This determines which Name sequence will be
used.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Click on the Save Button – The PAX Code sequence is set and you can
now add passenger records to the PAX / Requestor Table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Under <b>PAX Name Elements</b> repeat the
process for the Crew Code Setting </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Define the first, middle and last name sequence using the up, down
arrows. This determines which Name sequence will be used.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:25px;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Click on the <b>Save</b> Button – The Crew code sequence is set and
you can add Crew records to the Crew Roster Table.</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>
            </td>
        </tr>
    </table>

</asp:Content>
