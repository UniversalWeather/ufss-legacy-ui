﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="AccountsHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.AccountsHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Accounts</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Information about each
                        of your aviation department’s expenses is stored in the Accounts Table. This account
                        number will automatically post to the appropriate account number field throughout
                        the system. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>You will also have a corporate
                        account number that will serve as a link between the aviation department’s accounting
                        system and the corporate accounting system. If you wish, both numbers may be the
                        same depending on the accounting scheme within the individual companies. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The system performs a “masking”
                        function which is set in the <a href="<%=ResolveClientUrl("~/Views/Help/Company/ConfiguringCompanyProfile.aspx?Screen=ConfiguringCompanyProfile") %>" style="font:12px arial;" >Company Profile Table</a>.
                        This masking automatically pre-defines place holders for numbers and punctuation
                        and it is applied in the Accounts Table. For example, your pre-defined format for
                        each account number may be four digits followed by a decimal point and three more
                        digits. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are the data
                        fields found on this table:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Account Number —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is your aviation account number. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The name of the particular account (i.e. federal tax, fuel, and hangar lease).
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Corporate Account number —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is an account number that may be the same as your aviation department’s number
                            or may be different. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The home base associated with a specific account. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Fixed Cost --</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The cost associated with the specific account selected or created, at a fixed rate.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Include in Billing —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select the check box to have this account number included in the billing reports.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Inactive – </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Select
                            the check box to have this account number marked as inactive </span><a name="_GoBack">
                            </a></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Budget —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Links
                            to the <a href="<%=ResolveClientUrl("~/Views/Help/Company/BudgetHelp.aspx?Screen=BudgetHelp") %>" style="font:12px arial;" >
Budget Table</a> which lists actual costs projected
                            for a particular expense.</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
