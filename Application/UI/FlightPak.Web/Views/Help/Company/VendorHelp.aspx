﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="VendorHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Company.VendorHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Vendor</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Vendor Table contain
                        all information needed to track transactions with your vendors. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Using this table, you can
                        make changes to and/or maintain Vendor records. The information tracked for a Vendor
                        includes:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Active —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Select
                            this check box to indicate that this vendor is active. The display for this can
                            be turned off on the search criteria tab. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Alpha
                            or numeric code of no more than four characters that corresponds with a vendor name.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Name -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The vendor’s
                            name. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The airport where the vendor is based. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Closest ICAO —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is a geographical closest ICAO location in the event this table contains vendors
                            other than aircraft vendors, such as suppliers, etc. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Additional Insured —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select this check box to indicate that the vendor has additional insurance.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Credit —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Identifies
                            your vendor’s credit status. There’s either a record on file or credit has been
                            approved. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Part 135 Ops —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Identifies whether or not this pilot has been approved as a Part 135 operator.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Drug Test Approval —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select this check box to indicate that the vendor has been drug test approved.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Ins. Cert. —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select this check box to indicate that the vendor’s insurance certificate has been
                            verified. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Billing Information —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            All the information you need to bill your clients like addresses, postal codes,
                            phone, E-mail addresses and fax numbers can be stored here.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Notes -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Here’s
                            where you can store any additional information specific to your vendor necessary
                            for maintaining a good working relationship.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Main Contact</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            - Here’s where you can store all information about your primary contact at this
                            particular vendor location. </span></li>
                </ul>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Contacts Button</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Click this button to be
                        linked Contact for the selected Vendor. The Vendor is where information specific
                        to all the people you interact with at a particular vendor location can be stored.
                    </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Typical information for
                        the contact will include </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Name, Phone
                        Numbers, Addresses, E-mail addresses, Main Contact Check Box </span>
                </p>
                
            </td>
        </tr>
    </table>
</asp:Content>
