﻿<%@ Page Title="Help" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="ViewHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.ViewHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    
    <table width="100%" cellpadding="0" cellspacing="0" style="display: none;">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:Label ID="lblTitle" runat="server" /></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>
            <td>
                <div id="divHelpContent" runat="server" class="dgpExternalForm">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
