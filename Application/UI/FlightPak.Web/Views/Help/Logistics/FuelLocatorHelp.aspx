﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FuelLocatorHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.FuelLocatorHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fuel Locators</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <a name="_GoBack"></a><span lang="EN-US" style='font-family: "Arial","sans-serif"'>The
                        Fuel Locators Table is used to maintain codes and descriptions for each of the locations
                        your aviation department has made arrangements for fuel services. It is also used
                        to sort your fuel expenses and sort these by fuel locator. In addition, you have
                        the option of designating a client code for each of your descriptions. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                            This four-character code will be used in sorting reports. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. This is a descriptive label that corresponds with the code and prints
                            on reports. For example, home base fuel, domestic and international fuel and UVAir
                            fuel card purchases are common descriptions. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Client Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            <b><i>Four</i></b>-character code that identifies the client for this particular
                            fuel location. You can select a code from the Client Table by clicking the button
                            to the right of the data.</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
