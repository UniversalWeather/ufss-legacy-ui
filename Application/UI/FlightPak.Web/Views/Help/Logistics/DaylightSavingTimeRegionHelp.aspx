﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="DaylightSavingTimeRegionHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.DaylightSavingTimeRegionHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">DST Region</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <a name="_GoBack"></a><span lang="EN-US" style='font-family: "Arial","sans-serif"'>The
                        Daylight Savings Time Region Table is the key to controlling all your daylight savings
                        times (DST) calculations for your airports. Universal will maintain and supply this
                        data. It consists of a code and a description of the region; the start and end days
                        of the daylight savings time; the UTC off-set time (the time difference from Greenwich
                        Mean Time); and a city list. For example, you will have an Eastern Time entry with
                        the code US1 and description US Eastern. These codes and descriptions contain all
                        of the necessary daylight savings time information. The Airport Table will reference
                        the code US1 region and automatically import all the start and off-set times into
                        that code.</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> An abbreviation
                            for the region </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Detailed explanation of the code. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Start/End —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            These are the days and times that Daylight Savings Time goes into effect and ends.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        UTC offset —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This figure represents the number of hours your start and end times differ from
                            UTC time. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        City List —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This lists all the cities this entry effects</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
