﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="AirportHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.AirportHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Airport</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Airport Table contains
                        a listing of airports around the world, and it allows you to search for individual
                        airports by ICAO, city, state, country and airport name. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>UWA maintains all the information
                        relevant to each airport including elevation, longest runway, radio frequency, notes,
                        and alerts. FBO, Hotel, Transportation and Catering records for each ICAO are stored
                        in separate tables that can be accessed from the Airport Table once you select an
                        Airport record. Some data fields are merely informational while others are necessary
                        for calculations required during scheduling. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The following are some
                        of those fields: </span>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        I.C.A.O. —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> International
                            Civil Aviation Organization-A specialized agency of the United Nations whose objective
                            is to develop the principles and techniques of international air navigation and
                            to foster planning and development of international civil air transport. In other
                            words, ICAO is an international identifier of airports. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        IATA -</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> International
                            Air Transportation Association identifier for the selected airport</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        EU-E</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>TS – European
                            Union – Emissions Tracking System checkbox – indicates if the ICAO is part of the
                            EU ETS program</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        U.S. Tax –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Checkbox
                            – indicates if airport is a U.S. taxable airport. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Rural – </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Checkbox
                            – indicates if airport is d</span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>eclared
                                a rural airport within the US. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Entry Port –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Checkbox – indicates if airport is a </span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                                US Customs entry port. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Heliport – </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Checkbox
                            – indicates if the airport is a Heliport</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Latitude/Longitude —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is where you place latitude and longitude coordinates for this airport. These
                            two calculations are extremely critical to FlighPak’s calculations.. </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        UTC/Summer Time Checkbox and Start/Finish Dates / Time —</span></b><span lang="EN-US"
                            style='font-family: "Arial","sans-serif"'> I</span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>nformation
                                is maintained by UWA. This is where your daylight savings start and finish times
                                as designated. These dates / times are extremely critical to calculating the timing
                                of your trips correctly. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Take-off and Landing Bias —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is in addition to the take-off and landing biases in the Aircraft database
                            which adds up to give you your block time. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        UTC Region Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is where you will establish your DST region. You can click the button to the
                            right of the data line to choose from the DST Region Table.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Airport Landing Fees –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            These fields are used to store Landing fees and are maintained by the user. Landing
                            Fee values are used in the Charter Quote model to calculate Charter Quotes. Airport
                            Landing Fees are maintained by the User</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Runway Information —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Four Runway records are displayed – longest to shortest. If an ICAO has more than
                            4 runways, the complete list of runways with ILS and similar information is contained
                            in the Runways sub table. The aircraft you are attempting to schedule for this airport
                            will be checked for runway compliance.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        FBO of Choice –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            If you have designated an FBO record as choice for this airport, the information
                            will display in the FBO of choice display </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Custom Information –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Displays the Customs Hours of Operation, contact phone numbers, type of Entry etc
                            for this Airport. This information is maintained by UWA </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Frequency and Phone Numbers</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            – Displays phone numbers and radio frequencies for the Tower, Ground , Departure
                            etc. This information is maintained by UWA</span></li>
                </ul>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-size: 10.0pt; font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Notes</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> - Airport Table Notes can
                        be any information about a particular airport or service provider for one particular
                        location. You may want to use this area to keep special data on record as a reference
                        when scheduling trips. If there are notes posted to this section, the word “Notes”
                        in the tab will be colored red. </span>
                </p>
                <p class="MsoListParagraphCxSpMiddle" style='text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-size: 10.0pt; font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Alerts</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> - Airport Table Alerts,
                        such as landing restrictions and special permit requirements, are critically important
                        warning data about an airport. This database will be very important to you as you
                        schedule trips. If there are alerts posted to this section, the word “Alerts” in
                        the tab will be colored red.</span></p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-size: 10.0pt; font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Runway
                    </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>– Lists all
                        Runways located at the selected ICAO along with length, width, surface, lights,
                        ILS details, Navigation Aids<a name="_GoBack"></a></span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
