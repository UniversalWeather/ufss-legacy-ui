﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="TripManagerCheckListGroupHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.TripManagerCheckListGroupHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Trip Checklist Group</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Checklist Group Table
                        is where groupings for individual checklist items are created. Some examples are
                        domestic and international flights. In this database, you assign a code and description
                        for each group. To select items for each group, refer to the Trip Checklist table.</span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Trip Checklist Group
                        Table includes the following fields:<a name="_GoBack"></a></span></p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Checklist
                        Group Code</span></p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
