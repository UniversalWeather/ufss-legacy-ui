﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="ExchangeRatesHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.ExchangeRatesHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Exchange Rate</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Exchange Rate table
                        contains a list of Exchange Rate codes. There is currently no functionality within
                        the application that uses exchange rates to convert values to a defined currency.
                        The Exchange rate table is used only as a lookup to assign exchange rates to a home
                        base in the company profile </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Exchange Rate table includes
                        the following information: </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Exchange
                        rate code – identifies the country the exchange rate is used for example AUS could
                        be used to identify Australian dollars</span></p>
                <p class="MsoListParagraphCxSpMiddle" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description
                        - User defined – example Australian Dollars, Canadian Dollars, US Dollars</span></p>
                <p class="MsoListParagraphCxSpMiddle" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>From and
                        To Dates: defined the period when the exchange rate is valid </span>
                </p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Exchange
                        rate – the rate entered by the user<a name="_GoBack"></a>. </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
