﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="TripManagerCheckListHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.TripManagerCheckListHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Trip Checklist</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Trip Checklist table
                        contains a list of checklist items specific to trip planning in. The Trip Checklist
                        items are associated with a <a href="<%=ResolveClientUrl("~/Views/Help/Logistics/TripManagerCheckListGroupHelp.aspx?Screen=TripMgrChkLstGrpHelp") %>" style="font:12px arial;" >Trip Checklist Group</a>.
                        The user can add new Checklist <a name="_GoBack"></a>items to the table, edit descriptions
                        and groups assigned. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The fields in the Trip
                        Checklist table include </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='margin-bottom: 0cm; margin-bottom: .0001pt;
                    text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Checklist
                        Code </span>
                </p>
                <p class="MsoListParagraphCxSpMiddle" style='margin-bottom: 0cm; margin-bottom: .0001pt;
                    text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description</span></p>
                <p class="MsoListParagraphCxSpLast" style='margin-bottom: 0cm; margin-bottom: .0001pt;
                    text-indent: -18.0pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Group – a
                        “lookup” to the Trip Checklist Group Table is provided </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
