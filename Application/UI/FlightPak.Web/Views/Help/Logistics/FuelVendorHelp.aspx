﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FuelVendorHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.FuelVendorHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fuel Vendor</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Fuel Vendor Table contains
                        a list of fuel vendors that provide you with fuel pricing files. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Table displays a code
                        associated with each vendor and the vendor name. The table also allows the user
                        to browse for the location of the file provided by the vendor and file format –
                        which must be CSV.</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The table will also display
                        the last date when a selected vendor file was uploaded</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; color: red'>UWA provides
                        a “realtime” feed of their fuel prices via a web service and their Fuel Vendor code
                        <a name="_GoBack"></a>does not appear in the Fuel Vendor table </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"; color: red'>&nbsp;</span></i></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'></span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Fuel Vendor Table information
                        includes:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Fuel Vendor Code — </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            required</span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Name —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Required.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        File Name –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Name of the actual file received from your Fuel Vendor</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        File Type </span></b></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
