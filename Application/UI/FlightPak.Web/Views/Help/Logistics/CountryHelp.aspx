﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="CountryHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.CountryHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Country</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Country Table provides
                        two-letter codes and an associated country description that will be used throughout
                        the system including the Locator module. This table is supplied and maintained by
                        Universal.</span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Displayed fields in the
                        country table are:</span></p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Country Code</span></p>
                <p class="MsoListParagraphCxSpMiddle" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>ISO Code<a
                        name="_GoBack"></a></span></p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
