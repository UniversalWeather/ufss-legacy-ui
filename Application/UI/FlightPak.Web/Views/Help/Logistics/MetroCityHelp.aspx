﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="MetroCityHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Logistics.MetroCityHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Metro City</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Metro City Table is
                        used with the mapping utility. A default list of Metro City codes <a name="_GoBack">
                        </a>is provided by Universal. This data is used to indicate a metro area or an area
                        around a particular region. City descriptions and corresponding codes can be assigned
                        and maintained here. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Associated
                            with each item mapped to the Universal Locator. The metro code table is intended
                            to provide a 'hook' for airports, hotels, etc. so that searches are executed (in
                            the Universal Locator) for 'metro areas.' For example, a user may want to locate
                            all hotels that are located in the 'Houston metro area' but that may not be associated
                            with the city name. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Corresponds with the code.</span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
