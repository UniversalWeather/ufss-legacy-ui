﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Admin;
using FlightPak.Web.AdminService;
namespace FlightPak.Web.Views.Help
{
    public partial class ViewHelp : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string ModuleNameConstant = "View Help";
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            BindContent();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        private void BindContent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Screen"]))
                {
                    List<GetHelpContent> GetHelpContentList = new List<GetHelpContent>();
                    HelpContent oGetHelpContent = new HelpContent();
                    oGetHelpContent.Identifier = Request.QueryString["Screen"].ToString();
                    oGetHelpContent.IsDeleted = false;
                    using (AdminServiceClient AdminService = new AdminServiceClient())
                    {
                        var HelpContentInfo = AdminService.GetHelpContent(oGetHelpContent);
                        if (HelpContentInfo.ReturnFlag == true)
                        {
                            GetHelpContentList = HelpContentInfo.EntityList;
                        }
                        if (GetHelpContentList.Count > 0)
                        {
                            lblTitle.Text = System.Web.HttpUtility.HtmlEncode(GetHelpContentList[0].Topic);
                            divHelpContent.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(GetHelpContentList[0].Content));
                        }
                    }
                }
            }
        }

        
    }
}