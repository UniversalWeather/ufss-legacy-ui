﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="Glossary.aspx.cs" Inherits="FlightPak.Web.Views.Help.Glossary.Glossary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Glossary</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="5" cellspacing="1" border="1px">
        <tr>
            <td align="center" width="20%">
                <b>Term</b>
            </td>
            <td align="center" width="80%">
             <b>Definition</b>
            </td>
        </tr>
        <tr>
            <td>
                Alien Registration Card (Green card)
            </td>
            <td>
                United States permanent residents have an identification card known as the “Permanent
                Residency Card,” which is also known as the “Green Card” or “Alien Registration
                Card.” Permanent residents are not United States citizens, and they are granted
                permission to reside and work in the United States on a permanent basis, depending
                on the expiration date provided. In the past, these cards utilized the term “Alien
                Registration Card” and may have been issued at a time when no expiration dates were
                listed on them; however, they are now officially known as the “Permanent Residency
                Card.”
            </td>
        </tr>
        <tr>
            <td>
                ASOS (Automated Surface Observing Systems)
            </td>
            <td>
                Automated Surface Observing Systems (ASOS), a joint effort of the National Weather
                Service (NWS), the Federal Aviation Administration (FAA), and the Department of
                Defense (DOD), serves as the nation's primary surface weather observing network.
                ASOS works non-stop, updating observations every minute, 24 hours a day.
            </td>
        </tr>
        <tr>
            <td>
                ATIS (Automatic Terminal Information Service)
            </td>
            <td>
                Automatic Terminal Information Service (ATIS) is a continuous broadcast of non-control
                aeronautical information provided via hourly (or more) recordings, available via
                VHF frequency or phone for control areas such as airports. ATIS broadcasts contain
                essential information including weather information, which runways are active, available
                approaches, and any other information required by pilots such as important NOTAMs.
            </td>
        </tr>
        <tr>
            <td>
                AWOS (Automated Weather Observing Systems)
            </td>
            <td>
                Automated Weather Observing Systems (AWOS) is a voice- synthesized weather report
                that can be transmitted via radio, NDB, or VOR, ensuring that pilots on approach
                have up-to-date airport weather conditions. This voice data report is also available
                by telephone.
            </td>
        </tr>
        <tr>
            <td>
                Bias (Take-off & Landing)
            </td>
            <td>
                Adds specific amount of time to the system-provided ETE calculation based on user-entered
                additional time at the departure airport and/or the arrival airport.
            </td>
        </tr>
        <tr>
            <td>
                Block Time
            </td>
            <td>
                Time calculation from the time the aircraft departs the parking area (or the chocks)
                at the departure airport until the aircraft arrives in the parking area (or the
                chocks) at the arrival airport.
            </td>
        </tr>
        <tr>
            <td>
                Boeing Winds
            </td>
            <td>
                Series of wind calculations published by Boeing between two airports, based on altitude
                of flight, wind probability (percentage), and season of flight.
            </td>
        </tr>
        <tr>
            <td>
                CANPASS Corporate Air
            </td>
            <td>
                If your company is a CANPASS – Corporate Aircraft member, your corporate aircraft
                can have the following privileges:
                <ul type="disc">
                    <li>It can land at any airport of entry (AOE) in Canada </li>
                    <li>It can land at an AOE any time the airport is open for landing, regardless of the
                        hours of business of the local Canada Border Services Agency (CBSA) office </li>
                    <li>It can land at a CANPASS-only airport, which may be nearer to your destination
                    </li>
                    <li>It can transport up to four non-members who are integral to your business operation
                    </li>
                    <li>It receives expedited clearance </li>
                    <li>It can proceed to the final destination if there is no CBSA officer waiting for
                        the aircraft at the reported time of arrival, without the pilot having to make a
                        second call to the CBSA after landing </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                CANPASS Private Aircraft
            </td>
            <td>
                If your company is a CANPASS – Corporate Aircraft member, your corporate aircraft
                can have the following privileges:
                <ul type="disc">
                    <li>It can land at any airport of entry (AOE) in Canada </li>
                    <li>It can land at an AOE any time the airport is open for landing, regardless of the
                        hours of business of the local Canada Border Services Agency (CBSA) office </li>
                    <li>It can land at a CANPASS-only airport, which may be nearer to your destination
                    </li>
                    <li>It can transport up to four non-members who are integral to your business operation
                    </li>
                    <li>It receives expedited clearance </li>
                    <li>It can proceed to the final destination if there is no CBSA officer waiting for
                        the aircraft at the reported time of arrival, without the pilot having to make a
                        second call to the CBSA after landing </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                NON CANPASS Members
            </td>
            <td>
                Non CANPASS members must utilize the Telephone Reporting Centre (TRC). When general
                aviation aircraft enter Canada, they report to the Canada Border Services Agency
                (CBSA) by telephone. Travelers on private, company-owned, or charter aircraft carrying
                no more than 15 people (including the crew) must call the TRC to get authorization
                from the CBSA to enter or return to Canada. General aviation aircraft must land
                at an approved airport of entry (AOE) during the CBSA's hours of business. If the
                aircraft is carrying more than 15 people, the pilot has to contact the CBSA office
                at the proposed AOE in advance to arrange for customs clearance on arrival.
            </td>
        </tr>
        <tr>
            <td>
                CBP
            </td>
            <td>
                Customs and Border Protection
            </td>
        </tr>
        <tr>
            <td>
                Clr Del 1
            </td>
            <td>
                Clearance Delivery Frequency
            </td>
        </tr>
        <tr>
            <td>
                Clr Del 2
            </td>
            <td>
                Secondary (or alternate) Clearance Delivery Frequency
            </td>
        </tr>
        <tr>
            <td>
                Constant TAS
            </td>
            <td>
                True Air Speed (TAS) is the true measure of aircraft performance in cruise, thus
                listed in aircraft specs, manuals, performance comparisons, pilot reports, and every
                situation when actual performance needs to be measured. It is the speed normally
                listed on the flight plan, also used in flight planning, before considering the
                effects of wind.
            </td>
        </tr>
        <tr>
            <td>
                DHS
            </td>
            <td>
                Department of Homeland Security - US Customs along with other agencies such as TSA,
                Border Patrol, etc. fall under the Department of Homeland Security.
            </td>
        </tr>
        <tr>
            <td>
                EU-ETS
            </td>
            <td>
                EU-ETS, the European Union Emissions Trading Scheme, is a regulatory legislation
                requiring all non-commercial operators who travel into, out of, and between applicable
                EU Member States, EEA Countries, and Outlying Territories to monitor, report, and
                offset their carbon emissions (Co2) on an annual basis.
            </td>
        </tr>
        <tr>
            <td>
                FAR
            </td>
            <td>
                Federal Aviation Regulations - a set of rules prescribed by the FAA governing all
                aviation activities in the United States.
            </td>
        </tr>
        <tr>
            <td>
                FBO
            </td>
            <td>
                Fixed Base Operator - The terminal at airports that private aircraft operate to/from.
            </td>
        </tr>
        <tr>
            <td>
                Flight Time
            </td>
            <td>
                Time calculation from the time the aircraft departs the runway at the departure
                airport until the aircraft lands on the runway at the arrival airport.
            </td>
        </tr>
        <tr>
            <td>
                GA Desk ID
            </td>
            <td>
                ID number assigned by the GA desk to a company/client. The GA Desk has front-line
                access to real-time information at the FAA’s ATC System Command Center, such as
                NAS delays, anticipated en route and terminal weather constraints, reroute advisories
                and affected flights, and numerous other ATC issues.
            </td>
        </tr>
        <tr>
            <td>
                HSC (High Speed Cruise)
            </td>
            <td>
                Cruise at either the limiting speed for the aircraft or the maximum speed attainable
                at Maximum Cruise Thrust.
            </td>
        </tr>
        <tr>
            <td>
                IATA (International Air Transport Association)
            </td>
            <td>
                International Air Transport Association (IATA), an international trade organization
                representing the airline industry. It has developed commercial standards for fuel
                invoicing, among others, to simplify the process for efficiency. The IATA airport
                code uses a 3-letter code designating each airport around the world based on phonics.
                (For example, Charles de Gaulle’s IATA Code CDG.)
            </td>
        </tr>
        <tr>
            <td>
                ICAO (International Civil Aviation Organization)
            </td>
            <td>
                International Civil Aviation Organization (ICAO), an agency of the United Nations,
                codifies the principles and techniques of international air navigation. The ICAO
                airport code uses a 4-letter code designating each airport around the world based
                on location. (For example, Charles de Gaulle’s ICAO Code LFPG.)
            </td>
        </tr>
        <tr>
            <td>
                ILS
            </td>
            <td>
                Instrument Landing System - A ground-based instrument approach system that provides
                precision guidance to an aircraft approaching and landing on a runway, using a combination
                of radio signals and, in many cases, high-intensity lighting arrays to enable a
                safe landing during instrument meteorological conditions, such as low ceilings or
                reduced visibility due to fog, rain, or blowing snow.
            </td>
        </tr>
        <tr>
            <td>
                Lat/Long
            </td>
            <td>
                Latitude/Longitude - Angles that uniquely define points on the earth. Together,
                the angles comprise a coordinate scheme that can locate or identify exact geographic
                positions on the surfaces of the earth.
            </td>
        </tr>
        <tr>
            <td>
                LRC (Long Range Cruise)
            </td>
            <td>
                The speed that will provide the furthest distance traveled for a given amount of
                fuel burned and the minimum fuel burned for a given cruise distance.
            </td>
        </tr>
        <tr>
            <td>
                OOOI
            </td>
            <td>
                Out/Off/On/In - Provides the basic times required to calculate block and flight
                times. These times are used to track the status of aircraft and crews.
            </td>
        </tr>
        <tr>
            <td>
                Part 121 operator
            </td>
            <td>
                Operators flying under FAR 121 regulations - Domestic, Flag, and Supplemental Operations
                (generally known as "the airlines").
            </td>
        </tr>
        <tr>
            <td>
                Part 125 operator
            </td>
            <td>
                Operators flying under FAR 125 regulations - Airplanes having a seating capacity
                of 20 or more passengers, or a payload capacity of 6,000 pounds or more (generally
                applies to private operators utilizing airline type equipment, such as BBJ, ACJ
                etc.).
            </td>
        </tr>
        <tr>
            <td>
                SELCAL
            </td>
            <td>
                A selective-calling radio system that can alert an aircraft's crew that a ground
                radio station wishes to communicate with the aircraft, without the crew having to
                continuously monitor a potentially noisy and high-voice-traffic frequency. Aircraft
                having SELCAL abilities are assigned a 4-letter code (address) by the SELCAL Call
                Registrar, Aviation Spectrum Resources.
            </td>
        </tr>
        <tr>
            <td>
                SIFL
            </td>
            <td>
                Standard Industry Fare Level - A charge, developed by the IRS, imputed to the employee’s
                income for personal use of an employer-provided aircraft.
            </td>
        </tr>
        <tr>
            <td>
                SIFL Multiples
            </td>
            <td>
                The Standard Industry Fare Level (SIFL) charge varies depending on the maximum certified
                gross take-off weight of the aircraft, whether the employee is a "control employee"
                or not, and whether or not the employee is considered eligible for the "Security"
                charge. The SIFL Multiple is dependent on a combination of these categories.
            </td>
        </tr>
        <tr>
            <td>
                TAS
            </td>
            <td>
                True Air Speed - the speed of the aircraft relative to the air mass in which it
                is flying.
            </td>
        </tr>
        <tr>
            <td>
                Travel$ense
            </td>
            <td>
                A software program developed by the NBAA that will automatically calculate the actual
                business hours, productivity, trip expenses, and family time saved by traveling
                via business aviation aircraft instead of scheduled airline alternatives.
            </td>
        </tr>
        <tr>
            <td>
                Unicom
            </td>
            <td>
                An air-ground communication facility (radio) operated by a non-air traffic control
                private agency (such as an FBO or the operations office at a municipally run airport)
                to provide advisory service at uncontrolled airports and various non-flight services,
                such as requesting a taxi, quick fuel turnaround, etc., even at airports that have
                a tower.
            </td>
        </tr>
        <tr>
            <td>
                URL
            </td>
            <td>
                Uniform Resource Locator - Also known as a Web address.
            </td>
        </tr>
        <tr>
            <td>
                UTC (aka GMT or Z[Zulu])
            </td>
            <td>
                Coordinated Universal Time (UTC) is the primary world time standard. It is a constant,
                and is used within the aviation community for the filing of flight plans, ATC clearances,
                and weather forecasts.
            </td>
        </tr>
        <tr>
            <td>
                Wind Factor
            </td>
            <td>
                The headwind or tailwind component (speed) for a route between two points.
            </td>
        </tr>
        <tr>
            <td>
                Wind Reliability
            </td>
            <td>
                A reliability factor of the wind speed expressed in percentages for a given route.
                Boeing provides factors of 50, 75, and 85%.
            </td>
        </tr>
    </table>
</asp:Content>
