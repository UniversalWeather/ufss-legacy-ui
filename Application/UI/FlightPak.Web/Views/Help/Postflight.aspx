﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="Postflight.aspx.cs" Inherits="FlightPak.Web.Views.Help.Postflight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Postflight</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="WordSection1">
                    <p class="MsoNormal" style='text-align: justify; line-height: 150%'>
                        <span style='font-size: 10.0pt; line-height: 150%'>Once the trip you built in Preflight
                            has been completed (or flown), your crewmembers will supply you with the information
                            needed to create a Flight Log and track the actual trip data. Post-flight module
                            is where you maintain your actual trip information such as flight times, crew currency,
                            expense tracking, calculating SIFL, etc.</span></p>
                    <p class="MsoNormal" style='text-align: justify; line-height: 150%'>
                        <span style='font-size: 10.0pt; line-height: 150%'>Before proceeding, the following
                            should be set in the Company Profile, Post-flight Tab.</span></p>
                    <p class="MsoListParagraphCxSpFirst" style='margin-bottom: 0in; margin-bottom: .0001pt;
                        text-indent: -.25in; line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: Symbol'>
                            ·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span></span><b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>Post-flight
                                Crew Duty Basis</span></b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>
                                    - Choose the crew duty clock starting basis by setting the radio button to either
                                    <b>Scheduled Dep</b>., if you want your Post-flight crew hours to be calculated
                                    from Scheduled. <b>Departure or Block Out, to use actual block out time.</b></span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-bottom: 0in; margin-bottom: .0001pt;
                        text-indent: -.25in; line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: Symbol'>
                            ·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span></span><b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>Post-flight
                                Aircraft Basis - </span></b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>
                                    Choose the aircraft flight time clock basis by setting the radio button to <b>Flight
                                        Time</b> or <b>Block Time</b>, as appropriate to your business.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-bottom: 0in; margin-bottom: .0001pt;
                        text-indent: -.25in; line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: Symbol'>
                            ·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span></span><b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>Tenths
                                – Min Conversion - </span></b><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>
                                    Choose the radio button to either <b>Standard</b> or <b>Flight Scheduling Software</b> or <b>Other</b>,
                                    depending on how your company chooses to calculate the flight time in the flight
                                    logs. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0in; margin-right: 0in;
                        margin-bottom: 0in; margin-left: 1.0in; margin-bottom: .0001pt; text-indent: -.25in;
                        line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: "Courier New"'>
                            o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>The <b>Standard</b>
                            option converts hours and minutes exactly 6 minutes per tenth. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0in; margin-right: 0in;
                        margin-bottom: 0in; margin-left: 1.0in; margin-bottom: .0001pt; text-indent: -.25in;
                        line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: "Courier New"'>
                            o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>The <b>Flight Scheduling Software</b>
                            option converts hours and minutes per the system default. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0in; margin-right: 0in;
                        margin-bottom: 0in; margin-left: 1.0in; margin-bottom: .0001pt; text-indent: -.25in;
                        line-height: 150%; text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: "Courier New"'>
                            o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>The <b>Other</b> option
                            allows you to customize your calculations. </span>
                    </p>
                    <p class="MsoListParagraphCxSpLast" style='margin-top: 0in; margin-right: 0in; margin-bottom: 0in;
                        margin-left: 1.0in; margin-bottom: .0001pt; text-indent: -.25in; line-height: 150%;
                        text-autospace: none'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: "Courier New"'>
                            o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>Once the choice has
                            been selected, it <b>should not be changed </b>because it affects all reports with
                            the application and your flight log calculations.</span></p>
                    <h3 style='line-height: 150%'>
                        <a name="_Toc335040895"><span style='font-family: "Calibri","sans-serif"; color:#3366CC;'>How can I
                            log a completed Trip?</span></a></h3>
                    <p class="MsoListParagraphCxSpFirst" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the <b>Post-flight</b> button on the global
                            menu.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the <b>Retrieve</b> button. Doing so will
                            open the Select Tripsheet screen.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Enter your filter criteria and click on the Search
                            button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The grid will display all Trips that matched your Search
                            criteria.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">You can use sort and / or scroll features to locate
                            the Trip that you want to log.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Trip record that you want to log on the
                            grid.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the <b>OK</b> button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='line-height: 150%'>
                        <span lang="EN-GB">Note </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><i><span lang="EN-GB">If the Trip has already been logged you would receive
                            an alert message <b>“The selected Tripsheet is already logged in Postflight. Selecting YES will remove all data previously entered per leg in this log”.</b></span></i></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><i><span lang="EN-GB">Click on the <b>OK</b> button to update the existing
                            log or on the <b>Cancel</b> button to cancel the action. </span></i>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><i><span lang="EN-GB">If you wish to update the existing log, all existing
                            flight log information including expenses will be replaced with latest details and
                            a new log number will be generated.</span></i></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The system will transfer the trip to Post-flight and
                            assign a Flight Log number.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The system will also open the newly created log in
                            read only mode and if you wish to make any changes, click on the <b>Edit</b> button
                            above the Post-flight tab.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Select the Pre-flight tab; make any necessary changes
                            to any data fields on this tab.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Select the Legs tab and you will be now able to enter
                            the actual flight/block times for the trip.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='line-height: 150%'>
                        <i><span lang="EN-GB">Example: OUT = taxi begin, OFF = take off, ON = landing, IN =
                            engine shutdown</span></i></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">To post additional legs, highlight the appropriate
                            leg and follow the previous step until all leg times have been posted.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='line-height: 150%'>
                        <b><i><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>Note - It is not
                            necessary, at this time, to click on the Save button, however, it is highly recommended
                            to avoid unnecessary loss of data. To continue working on the flight log, you will
                            need to click on the Edit Log button.</span></i></b></p>
                    <p class="MsoListParagraphCxSpLast" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Once you've completed all legs, select the Crew tab;
                            highlight Leg 1 and you will now able to enter the crew currency (i.e. POS(ition),
                            T/D (Take Off Day), T/N (Take Off Night), etc. and make any adjustments to duty,
                            augmented crew or RON. You can adjust more than 1 crewmember’s duty, augmented crew
                            or RON at the same time by selecting all the applicable crewmembers and then make
                            the necessary time adjustments.</span></p>
                    <p class="MsoNormal" style='margin-left: .5in; line-height: 150%'>
                        <i>Note - Out/Off/On/In times must be posted to the Flight Log Legs before crew currency
                            can be posted. To make any adjustments to duty, augmented crew or RON, you must
                            select the applicable crewmember first.</i></p>
                    <p class="MsoListParagraphCxSpFirst" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Once you have entered the Crew currency for one leg,
                            you can navigate to other crew legs by highlighting applicable leg above the Crew
                            grid.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the <b>Save</b> button or go on to the Passenger
                            tab and make any changes that may be required.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">If the flight log qualifies for personal travel and
                            requires SIFL (Standard Industry Fare Level) charges, you can move to the SIFL tab
                            and proceed with those calculations.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click the <b>Recalc</b> SIFL button and the system
                            will calculate the charges for you based on.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">If required you can insert additional legs by clicking
                            the Add SIFL Leg button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">You may also post any expense invoices that are applicable
                            to this log at this time. If so, click the Expenses tab, highlight Leg 1 and click
                            the <b>Add Expense</b> button. You are now ready to enter the data from your invoice
                            in the applicable fields.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">You can now enter the data from your invoice in the
                            applicable fields.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">When you have added all the information you desire,
                            click on the Save button.</span></p>
                    <h3 style='line-height: 150%'>
                        <a name="_Toc335040896"><span style='font-family: "Calibri","sans-serif"; color:#3366CC;'>How can I
                            search for an existing Log?</span></a></h3>
                    <p class="MsoListParagraphCxSpFirst" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>If you
                            know the Log number, enter the Log number in the search box under the Post-flight
                            tab and click on <b>search icon</b> </span><span style='font-size: 10.0pt; line-height: 150%'>
                                <img width="25" height="20" id="Picture 114" src="10_files/image001.jpg"></span><span
                                    lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">If the Log exists, the system will open the Log.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">If the Log doesn’t exist, you will receive an error
                            message indicating the invalid log number.</span></p>
                    <h3 style='line-height: 150%'>
                        <a name="_Toc335040897"><span style='font-family: "Calibri","sans-serif"; color:#3366cc;'>What if I
                            don’t know the Log No.?</span></a></h3>
                    <p class="MsoListParagraphCxSpFirst" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>If you
                            don’t know your Log number, you can search by various filter criteria by clicking
                            on “See All” link.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on <b>“See All”</b> link next to the Log No.
                            Search box.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Enter your filter criteria and click on the <b>Search</b>
                            button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The grid will display all Logs that matched your Search
                            criteria.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">You can use sort and / or scroll features to locate
                            the Log that you want to work.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Log record that you want to open on the
                            grid.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on <b>OK</b> button to open the Log.</span></p>
                    <h3 style='line-height: 150%'>
                        <a name="_Toc335040898"><span style='font-family: "Calibri","sans-serif"; color:#3366CC;'>How can I
                            edit an existing Log?</span></a></h3>
                    <p class="MsoListParagraph" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%; font-family: Symbol'>
                            ·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span></span><span lang="EN-GB" style='font-size: 10.0pt; line-height: 150%'>By default
                                the Log you selected would open in the read only mode and you can click on various
                                tabs on the Log to view the details. To edit the selected Log, click on <b>Edit Log</b>
                                button right above the Post-flight tab.</span></p>
                    <h3>
                        <a name="_Toc335040899" style="color:#3366CC;">How can I Add / Edit an Expense Log </a>
                    </h3>
                    <p class="MsoNormal">
                        &nbsp;</p>
                    <p class="MsoListParagraph" style='text-align: justify; text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Post-flight Expense has 3 functions: Search, Expense,
                            Expense Notes</span></p>
                    <p class="MsoNormal" style='margin-left: .25in; text-align: justify; line-height: 150%'>
                        <b>Post-flight Search All Expenses </b>
                    </p>
                    <p class="MsoListParagraphCxSpFirst" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">From the Expense table select the See All button. The
                            Post-flight Search all Expense Grid will display. This display lists all expenses
                            from flight log manager. User selects the necessary filter information and searches
                            for a specific expense record. </span>
                    </p>
                    <p class="MsoListParagraphCxSpLast" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Users can select a specific entry, after selecting
                            the <b>OK</b> button; they can copy, edit or delete the record.</span></p>
                    <p class="MsoNormal" style='margin-left: .25in; text-align: justify; line-height: 150%'>
                        <b>Expense </b>
                    </p>
                    <p class="MsoListParagraphCxSpFirst" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">This is similar to Post-flight Expense tab.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Account number is required to add an expense record
                        </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Fuel details (quantity, locator, prices and taxes)
                            can be entered when an expense is marked as Fuel Entry. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click the Save Button to save the Expense record. Record
                            will display in the Search All Expenses grid </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">User can save expense and create a slip number when
                            mandate account number information is added. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The lower grid displays the legs from where legs are
                            selected and new expense can be added.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">To Edit an Expense Slip, select the Slip No. from the
                            Search all grid and then select the Ok button.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The selected expense grid will display. Select Edit
                            button and update the necessary field, then Save. </span>
                    </p>
                    <p class="MsoNormal" style='margin-left: .25in; text-align: justify; line-height: 150%'>
                        <b>Expense Notes</b></p>
                    <p class="MsoListParagraphCxSpFirst" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Expense notes for an expense are displayed in the Expense
                            Notes Tab. The Expense Note Tab can be expanded and collapsed by clicking the direction
                            arrow on the Tab. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The user selects an Expense Slip No. They select the
                            Expense Notes Tab and enter / edit or delete text information related to the expense
                        </span>
                    </p>
                    <p class="MsoListParagraphCxSpLast" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Expense notes can be added and saved by clicking save
                            button.</span></p>
                    <h3 style='line-height: 150%'>
                        <span style='font-family: "Calibri","sans-serif"; color:#3366CC;'>How can I maintain Other Crew Duty
                            Logs?</span></h3>
                    <p class="MsoNormal" style='text-align: justify'>
                        Other Crew Duty Logs are used to enter activities that are not Flight specific.
                        Typical Other Crew Duty logs could include Office Time, Simulator or other activities
                        defined in the Crew Duty Type table. Other Crew Duty Logs can also be created from
                        the Scheduling Calendar. Crew Calendar entries can be updated using Other Crew Duty
                        Log.</p>
                    <p class="MsoListParagraphCxSpFirst" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Post-flight button on the global menu.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Other Crew Duty Log button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Add button.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Select the Crew Code. This is a required field.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Enter Start Date and Start Time, This is a required
                            field.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Enter End Date and End Time, This is a required field.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Enter the following optional duty related information.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-align: justify;
                        text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Approaches – Precision and Non-Precision.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-align: justify;
                        text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Take Off – Day and Night.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-align: justify;
                        text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Landings – Day and Night.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='margin-left: 1.0in; text-align: justify;
                        text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: "Courier New"'>o<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Hours – Instrument, Night, Flight and Duty.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-align: justify; text-indent: -.25in;
                        line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Click on the Save Button. </span>
                    </p>
                    <h3 style='line-height: 150%'>
                        <a name="_Toc334468198"><span style='font-family: "Calibri","sans-serif"; color:#3366CC;'>How can I
                            use the Post-flight SIFL</span></a><span style='font-family: "Calibri","sans-serif"'>?</span></h3>
                    <p class="MsoListParagraphCxSpFirst" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Post-flight SIFL is accessed from Post-flight Log -
                            PAX tab. The SIFL tab will display below the PAX Detail grid. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Post-flight SIFL is used calculate the SIFL Total charges
                            per PAX for the appropriate mile range for when the Flight Purpose is designated
                            as Personal (Flight Purpose Table).</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The SIFL leg grid will populate automatically when
                            passenger leg information is entered and there is a SIFL qualified leg in the Post-flight
                            log. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">SIFL Total grid will update if the user selects the
                            Recalc SIFL button. The SIFL grid will also update when the SIFL display when changes
                            are made to the SIFL grid and the user selects the Save Button. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">SIFL rates and Terminal Charges are retrieved from
                            SIFL Rates Table for corresponding time period.</span></p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">The Top grid displays the legs from the selected Flight
                            log that qualify for SIFL. </span>
                    </p>
                    <p class="MsoListParagraphCxSpMiddle" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">Users can Add/Recalc/Delete SIFL for the PAX. Non-database
                            PAX do not qualify for SIFL entries.</span></p>
                    <p class="MsoListParagraphCxSpLast" style='text-indent: -.25in; line-height: 150%'>
                        <span lang="EN-GB" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span></span><span lang="EN-GB">User can log expenses, if they click on the Expenses
                            tab which is next to PAX tab.</span></p>
                    <p class="MsoNormal" style='line-height: 150%'>
                        <span style='font-size: 10.0pt; line-height: 150%'>&nbsp;</span></p>
                    <p class="MsoNormal">
                        &nbsp;</p>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
