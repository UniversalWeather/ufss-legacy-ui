﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="CorporateRequest.aspx.cs" Inherits="FlightPak.Web.Views.Help.CorporateRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Corporate Request</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>Corporate is used to plan trip request. The
key information captured for the request include, </span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Request
information (Aircraft, Department/Authorization, TravelCoordinator etc.)</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Legs</span></p>



<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Assigned
passengers</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Logistics</span></p>



<h3 style='line-height:150%'><a name="_Toc335040878"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access Request module?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Corporate Request</b> button on the
global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>New Request</b> button, to create a new
Trip Request</span></p>



<h3 style='line-height:150%'><a name="_Toc335040879"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I search for an existing trip request?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If you
know the Trip Request number, enter the Trip reuest number in the search box under the Corporate-Request
tab and click on <b>search icon</b> </span><span style='font-size:10.0pt;
line-height:150%'><img width=25 height=20 id="Picture 114"
src="8_files/image001.jpg"></span><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the Trip Request exists, the system will open the
Trip Request.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the Trip Request doesn’t exist, you will receive an
error message indicating the Request No. doesn’t exist.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040880"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>What if I don’t know the Request No.?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If you
don’t know your Trip Request number, you can search by various filter criteria by
clicking on “See All Request” link.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>“See All Request”</b> link next to the Request
No. Search box.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter your filter criteria and click on the <b>Search</b>
button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The grid will display all Requests that matched
your Search criteria.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You can use sort and / or scroll features to
locate the Request that you want to find.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Request record that you want to open.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>OK</b> button to open the Request.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040881"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I edit an existing Request?</span></a></h3>

<p class=MsoListParagraph style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>By
default the Request you selected would open in the read only mode and you can
click on various tabs on the Request to view the details. To edit the selected
Request, click on <b>Edit Request</b> button right above the Corporate-Request tab.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040882"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I create a New Request?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Corporate Request</b> button on the
global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>New Request</b> button.</span></p>



<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b>Request Main
Details</b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the <b>Departure Date.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Tail No.</b> lookup to view the Fleet Profile table. Select the Tail
No. of the request by clicking the desired fleet and click the <b>OK</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Requestor lookup</b> to view the Passenger/Requestor table. Select
the Requestor of the request by clicking the desired name and click the <b>OK</b>
button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Account No. lookup to view the Accounts table. Select the Account by
clicking the desired Account and click on the OK button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
Department and Authorization.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter the
<b>Request Purpose.</b></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Next</b> button at the bottom of the page or click on the <b>Legs</b>
tab to proceed with building legs for your request.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Building Legs</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>By
default, your aircraft’s home base will be displayed in the <b>departure ICAO</b>
field. You can change the departure ICAO, if required.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the desired <b>arrival ICAO</b> code in the Arrives field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your desired <b>Departure or Arrival Time</b> either in Local or UTC or Home
Base times.  The system will calculate other times automatically.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
this is the only (or last) leg of the day, select the <b>End of Duty Day</b>
checkbox next to the crew duty rules section.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><i><span
style='font-size:10.0pt;line-height:150%'>Note – At this point, you have
entered all the required data for the trip to be saved and a Request No. to be
assigned by the system if you click the Save button.</span></i></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
there are more legs to be added to this request, click on the Add Leg button to
begin planning your next leg.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
the Arrives ICAO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter
your desired <b>Departure or Arrival Date and Time </b>either in Local or UTC
or Home Base times.  The system will calculate other dates and times
automatically.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If
this is the last leg of the day, select the <b>End of Duty Day</b> checkbox
next to the crew duty rules section.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>When
you have completed adding your flight leg data, click on the <b>Crew tab</b> or
the <b>Next button</b> at the bottom of the page, to begin adding crewmembers
to each leg of the trip.</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><i><span
style='font-size:10.0pt;line-height:150%'>Note - It is not necessary, at this
time, to click on the Save button, however, it is highly recommended to avoid
unnecessary loss of data.  To begin working in the request again, you will need to
click on the Edit Request button.</span></i></b></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Adding Passengers</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>From
the Passenger tab, click on the <b>PAX Catalog</b> link to open the Passenger /
Requestor table</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
<b>one or multiple Passenger</b> and click on the <b>OK</b> button to place all
selected Passengers on the trip.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The selected
passenger will be assigned to all legs.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
default flight purpose will also be assigned to all passengers / all legs. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
Flight Purpose code can be customized for each passenger by leg</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
remove a passenger from a particular leg, simply remove the Flight Purpose code
(i.e. EB, GP, etc.) displayed in the legs grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
change the Flight Purpose code for a passenger, select the desired code from
the drop down.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
change the Flight Purpose code in all legs of a specific passenger, highlight
the passenger record and then select the desired Flight Purpose in the All Legs
drop down.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the Logistics tab or the Next button to begin adding logistics to your trip
(e.g. FBO/Handler, Catering, Leg Notes, etc.).</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:150%'><b><i><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Note - It is not
necessary, at this time, to click on the Save button, however, it is highly
recommended to avoid unnecessary loss of data.  To begin working in the trip
again, you will need to click on the Edit Trip button.</span></i></b></p>

<p class=MsoListParagraphCxSpLast style='line-height:150%'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;line-height:150%'><b><span
style='font-size:10.0pt;line-height:150%'>Adding Logistics</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the desired section, FBO/Handler, Catering and Fuel to begin adding/managing
the logistics that will be associated with this trip.</span></p>



<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
move from leg to leg, highlight the leg number tab you are working</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>
            </td>
        </tr>
    </table>

</asp:Content>
