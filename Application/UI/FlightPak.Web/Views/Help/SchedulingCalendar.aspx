﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="SchedulingCalendar.aspx.cs" Inherits="FlightPak.Web.Views.Help.SchedulingCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Scheduling Calendar</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>There are 6 Calendar views available
in Flight Scheduling Software namely Planner, Weekly, Monthly, Day, Business Week, and Corporate
views. Weekly view has 4 additional views – Main, Detail, Fleet and Crew. Each
Calendar has a variety of filters and settings that allow you to customize the
display. Some calendar views allow access to Preflight to create New Trips,
or to view and / or edit Trip details like Crew, Passenger, and Logistics. Some
calendar views also allow the user to create Crew and Fleet calendar entries.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040884"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access Scheduling Calendar?</span></a></h3>

<p class=MsoListParagraph style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Calendar icon</b> at the top of the screen</span></p>

<h3 style='line-height:150%'><a name="_Toc335040885"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I Maneuver around the Calendar?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The <b>initial
date display</b> will always be the current date </span><span style='font-size:
10.0pt;line-height:150%'></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Depending
on the Calendar view, clicking on the <b>single arrow</b> left or right will
move the date by one day or one week.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Clicking
on the <b>double arrow</b> left or right will move the date one month or one
week.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%;text-autospace:
none'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:
Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
navigate to a specific date, place the cursor in the date display filed and
select the desired date.</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-size:10.0pt;line-height:150%;
font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
navigate back to the current date, click on the <b>Today</b> button and the
calendar display will change back to the current date.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040886"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>Can I set a default Calendar view?</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;line-height:150%'>Yes, </span><span style='font-size:
10.0pt;line-height:150%'>you can set a default Calendar view and this will be
the calendar view you will see as the initial display whenever you access the
calendar.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the calendar view that you want to use as default and click on the <b>Default
View</b> check box. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>If you
don’ have the default view setup, the system will display the Weekly Fleet view
whenever you accesses the calendar.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040887"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>What is Color Legend?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>Calendar views will display the color setting for tail
numbers, fleet and crew duty rules. To view the color settings, click on the <b>Color
Legend</b> button and a display will appear listing the color settings for Type
of Duty.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040888"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access and use Filters?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>Depending on the specific Calendar you can apply various
filters to narrow down what you see on the Calendar. In order to access the
Filters, </span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the<b> Collapse/expand the right pane</b> icon </span><span
style='font-size:10.0pt;line-height:150%'></span><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'> at the right end of your Calendar view.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
filter on Fleet, expand the <b>Fleet</b> tab, <b>select/unselect</b> from the
available Fleet list and click on <b>Apply</b> button to refresh the display of
information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
filter on Crew, expand the <b>Crew</b> tab, <b>select/unselect</b> from the
available Crew list and click on <b>Apply</b> button to refresh the display of
information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Several
additional filter options are available under the <b>Filter Criteria</b> tab
that you can <b>select/unselect </b>and<b> </b>click on<b> Apply </b>button<b> </b>to
refresh the display of information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
save your <b>Filter Criteria as Default, </b>click on<b> </b>the<b> Save
Defaults </b>button under the Filter Criteria tab and click on the <b>OK</b>
button when the system prompts for confirmation.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
can reset to system default filter criteria by clicking the <b>Reset To System
Defaults</b> button under the Filter Criteria tab and click on the <b>OK</b>
button when the system prompts for confirmation.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040889"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I access and configure my Display Options?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>You can customize the display of data for each individual
calendar view. </span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the<b> Collapse/expand the right pane</b> icon </span><span
style='font-size:10.0pt;line-height:150%'><img width=19 height=20 id="Picture 4"
src="9_files/image002.png"></span><span lang=EN-GB style='font-size:10.0pt;
line-height:150%'> at the right end of your Calendar view.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Display Options</b> button at the right bottom of the screen.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the specific Calendar view on the popup window.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Set
your desired display options </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To set
the revised display options just for your current session, click on the <b>Save
for Current Session</b> button and click on the <b>Apply</b> button to refresh
the display of information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To set
the revised display options permanently (so that you will see this display
option every time you access the particular Calendar view), click on the <b>Save
for Current Session</b> button and click on the <b>Apply</b> button to refresh
the display of information.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
reset the display options to initial system default values, click on the <b>Reset
to System Defaults</b> button and click on the <b>Apply</b> button to refresh
the display of information.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040890"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I Enter and / or Edit Crew Calendar Entry?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>Crew Calendar entries can be created from any Calendar Views.
To access a Crew Calendar entry,</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired cell in the calendar and click your <b>right-mouse </b>button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>In the
drop down display, select Crew Calendar entry.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>A new
popup window will open with top section displaying the existing Crew Calendar
entries and the bottom section displaying the details of selected </span><span
lang=EN-GB style='font-size:10.0pt;line-height:150%'>Crew Calendar entry.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
can search for a specific Crew Calendar entry using the filter, search and sort
options.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
create a <b>new </b>Crew Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Add</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Crew
Code</b> or lookup to select one or multiple Crew from the Crew Roster table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Duty
Type</b> or lookup to select a Duty Type from the Crew Duty Type table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>ICAO</b>
or lookup to select an Airport from the Airport table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Start
Date and Time.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>End
Date and Time.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Home
Base</b> or lookup to select a Home Base from the Company Profile table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Optionally
you can enter the <b>Comments.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Optionally
you can enter the <b>Tail No.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Save</b> button to create the new Crew Calendar entry.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'> To <b>edit</b>
an existing Crew Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired Crew Calendar entry record and click on the <b>Edit</b> button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Edit
one or multiple desired fields.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Save</b> button to save the changes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To <b>delete</b>
an existing Crew Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired Crew Calendar entry record and click on the <b>Delete</b> button.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>OK</b> button when the system prompts for a confirmation.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040891"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I Enter and / or Edit Fleet Calendar Entry?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>Fleet Calendar entries can be created from any Calendar
Views. To access a Fleet Calendar entry,</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired cell in the calendar and click your <b>right-mouse </b>button.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>In the
drop down display, select Fleet Calendar entry.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>A new
popup window will open with top section displaying the existing Fleet Calendar
entries.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>You
can search for a specific Fleet Calendar entry using the filter, search and
sort options.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
create a new Fleet Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Add</b> button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Tail
No</b>. or lookup and select a Tail No. from the Fleet profile table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Duty
Type</b> or lookup and select a Duty Type from the Aircraft Duty Types table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>ICAO</b>
or lookup and select an Airport from the Airport table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Start
Date and Time</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>End
Date and Time</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Enter <b>Home
Base</b> or lookup and select a Home Base from the Company Profile table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Optionally
you can enter <b>Comments</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Save</b> button to create the new Fleet Calendar entry</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'> To
edit an existing Fleet Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired Fleet Calendar entry record and click on the <b>Edit</b> button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Edit
one or multiple desired fields</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the <b>Save</b> button to save the changes</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
delete an existing Fleet Calendar entry, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Select
the desired Fleet Calendar entry record and click on the <b>Delete</b> button</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%;text-autospace:none'><span lang=EN-GB style='font-size:10.0pt;
line-height:150%;font-family:"Courier New"'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
on the OK button when the system prompts for a confirmation.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040892"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I Build a Trip from the Scheduling Calendars?</span></a></h3>

<p class=MsoNormal style='line-height:150%'><span style='font-size:10.0pt;
line-height:150%'>Right clicking in a cell allows you to build trips from all
of the Scheduling Calendar views. Most of what is covered here can also be
applied to the other calendar views (i.e. Planner, Weekly Fleet, Weekly Crew,
Monthly, Weekly, Business Week, Day and Corporate).  It is up to you to
determine which calendar you prefer to work with. In the instructions below,
we’ve chosen the Weekly Fleet calendar view.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Navigate
to the <b>Weekly Fleet</b> calendar view</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Once
you determine which day and aircraft you want to build your trip on, click your
<b>right-mouse button</b> into the cell where the day and aircraft intersect. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Right
clicking will produce the drop down menu and click on the <b>Add New Trip Using
Preflight</b>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>The
system will take you to the Pre-flight screen and open a new Trip in edit mode
with departure date and tail number already filled in.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>To
proceed further with building the Trip, follow the instructions <b>“How can I
create a New Trip?”</b> under Pre-flight section.</span></p>

<h3 style='line-height:150%'><a name="_Toc335040893"><span style='font-family:
"Calibri","sans-serif"; color:#3366cc;'>How can I Edit a trip from the Scheduling Calendars?</span></a></h3>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%;text-autospace:none'><span style='font-size:10.0pt;line-height:150%'>Right
clicking in a cell allows you to edit trips from all of the Scheduling
Calendar. </span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Navigate
to the desired calendar view.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-indent:-.25in;line-height:150%;text-autospace:none'><span
lang=EN-GB style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>Click
your <b>right-mouse button</b> on the Trip No. that you would like to edit and
select either Preflight or Crew or Passengers or Logistics or Outbound
Instructions or Logistics to directly access different areas of the Trip.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-indent:-.25in;line-height:150%;text-autospace:none'><span lang=EN-GB
style='font-size:10.0pt;line-height:150%;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB style='font-size:10.0pt;line-height:150%'>When
selecting Logistics, you will be presented with a secondary drop down menu to
choose a specific logistics either FBO or Crew Hotel or Crew Transport or PAX
Hotel or PAX Transport or Catering.</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>
            </td>
        </tr>
    </table>

</asp:Content>
