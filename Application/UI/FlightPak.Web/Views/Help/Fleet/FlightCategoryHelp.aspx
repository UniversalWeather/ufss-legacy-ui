﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FlightCategoryHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.FlightCategoryHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Flight Categories</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Flight Category Table
                        contains records that represent a type of leg which an aircraft can fly. Examples
                        of types of legs are deadhead, live leg, maintenance, training and charter.
                    </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"; color: #C00000'>Flight
                        Categories are the reason why the aircraft is flying, not the reason why a passenger
                        is on board. The reason a passenger is on a flight is defined in the Flight Purpose
                        table </span></i>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Table features include:</span></p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code:</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> Required. The category code
                        is an abbreviation for different Flight Categories by which various reports are
                        sorted. The coding scheme should be as logical as possible so that you can easily
                        implement it throughout the system. For example: live leg can logically be LL.
                    </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description:</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> Required. This is a descriptive
                        label that prints on reports. This field should not be altered without consultation
                        with all users of this data. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Client Code:</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> This four-character code
                        is a client identifier. Choose your code by clicking the button to the right of
                        the data line or typing a new code to associate the client code with the flight
                        category. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Inactive Checkbox – This
                        checkbox is used to mark a Flight Catagory that is <a name="_GoBack"></a>no longer
                        used.</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
