﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="AircraftTypeHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.AircraftTypeHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Aircraft Type</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Each record of the Aircraft
                        Type Table represents an aircraft type. Many common aircraft types have been preset
                        for you. This database should only be maintained to add new aircraft types or to
                        fine-tune the performance settings to make scheduling more accurate.<br>
                        <br>
                        Aircraft profile information, such as air speeds, take-off and landing biases, and
                        speed and range descriptions are stored here. From this table, you can add, edit
                        or delete this kind of profile data. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"; color: #C00000'>It is
                        recommended that you DO NOT delete aircraft types. If you do not have one of these
                        aircraft in your fleet nor do you expect to add them in the future, you should retain
                        these records anyway because certain future functions and examples may prove very
                        useful for those types of aircraft. </span></i>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Here are this menu’s features:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Type Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. The Type Code is an abbreviation by which various reports are sorted and
                            aircraft types are selected for data entry fields in Tripsheets, Flight Logs, Airport
                            Pairs and Type Comparisons. Once the record is created, the user cannot change it.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This descriptive label explains the aircraft type and prints out on reports. This
                            field should not be altered without consultation with all users of this data.
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Associated Type Code – </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Allows you to associate a new type code with an existing type code </span>
                    </li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Default Power Setting —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Default air speed setting. This default setting will be imported into the section
                            when you are planning a trip for that aircraft. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Power Setting —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. This field controls all power settings and will be used on the Planning
                            screen. These are preformatted by the system and serve simply as field markers at
                            this point. The data for this setting is controlled by True Air Speed, Hour Range,
                            and Landing Bias fields. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Max Wind Altitude –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Used to define the Maximum Wind Altitude the aircraft type operates at</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Fixed and Rotary Toggles –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Used to indicate if the aircraft type is fixed wing or Rotary</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Charge Rate –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            used to define the rate the aircraft is charged out </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Charge Unit –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Hourly, Nautical Mile, Statute – used to determine the Unit the Charge rate is based
                            on to determine flight / trip cost.<a name="_GoBack"></a></span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        True Air Speed —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. True airspeed of the aircraft type at the particular throttle setting
                            in nautical miles per hour. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Hour Range —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Required. Number of flight time hours for the aircraft type at a particular throttle
                            setting before you would like to be warned that you are exceeding the aircraft’s
                            range. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        T/O Bias —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> A
                            default take-off bias that is added during departure. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Lnd Bias —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> A
                            default landing bias that is added upon landing. These biases combined with airport
                            take-off and landing biases generate flight time. These two biases are added to
                            the flight time to create your block time. </span></li>
                </ul>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
