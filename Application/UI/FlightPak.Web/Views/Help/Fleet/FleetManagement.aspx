﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master" AutoEventWireup="true" CodeBehind="FleetManagement.aspx.cs" Inherits="FlightPak.Web.Views.Help.FleetManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
 <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Management</span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
             <div class=WordSection1>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Fleet Profile
contains all the aircraft in your company’s inventory. Based on the settings
and information entered for each aircraft, Flight Scheduling Software will use the information
in Preflight and Post-flight. When combined with an Aircraft Type code, Flight
Pak will calculate Flight Hours and ETA (Estimated Time of Arrival) for each
trip leg. </p>

<h3 style='line-height:150%'><a name="_Toc335040841"><span style='font-family:
"Calibri","sans-serif"'>Aircraft Type</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>The first step
to loading a new aircraft to the fleet profile is to add the Aircraft Type Code
to the Aircraft Type Code table. The Aircraft Type Code contains the settings
for a specific type of aircraft. These settings include </p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Fixed Wing or Rotary.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Default power setting for Constant Mach, Long
Range Cruise and High Speed Cruise.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>True Air Speed</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Hour Range</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Take Off / Landing Bias </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Maximum Wind Altitude. </span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Charge Rate and Charge Unit.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><i>Note that
your version of Flight Scheduling Software may include a preloaded set of Aircraft Types but you
can load your own Aircraft Types.  </i></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040842"><span
style='font-family:"Calibri","sans-serif"'>How can I add an Aircraft Type Code?</span></a><span
style='font-family:"Calibri","sans-serif"'> </span></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the Fleet tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Aircraft Types link </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button at the bottom of the
grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Aircraft Type Code</b>. This is a
required field </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Description</b> for the Aircraft Type
Code. This is a required field </span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><u><span
 style='text-decoration:none'>&nbsp;</span></u></b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>The rest of the
fields are optional but Flight Scheduling Software uses the information in these fields to
calculate arrival times and flight hours and should be entered to improve the
accuracy of these calculations.</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Set the <b>Fixed</b> or <b>Rotary</b> radio
button. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Default</b> <b>Power Setting</b>;
values = “1”, “2” or “3”.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Max <b>Wind Altitude</b>. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Charge Rate.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select <b>Charge Unit</b>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter True Air Speed for each power setting;
Constant Mach, Long Range Cruise, High Speed Cruise.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Hour Range</b> for each power setting.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Take Off Bias</b> for each power
setting.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Landing Bias</b> for each power setting.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Save</b> button. The Aircraft
Type will be add to the Aircraft type table and will display in the Aircraft
Type grid.  </span></p>

<h3 style='line-height:150%'><a name="_Toc335040843"><span style='font-family:
"Calibri","sans-serif"'>Fleet Profile</span></a><span style='font-family:"Calibri","sans-serif"'>
</span></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>This table
contains detailed information for each Aircraft in your fleet. Information that
can be added for each aircraft includes Tail Number, Serial No., the maximum
number of passengers the aircraft can carry, minimum runway required for the
aircraft and maximum flight level. There are also a number of sub-tables
associated with each tail number that contain International information, Engine
/ Airframe, Components, and Additional Information.                </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040844"><span
style='font-family:"Calibri","sans-serif"'>How can I add a new Aircraft (Tail No.)
to the Fleet Profile table?</span></a><span style='font-family:"Calibri","sans-serif"'>
</span></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Database</b> button on the
global menu.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the <b>Fleet</b> tab.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Fleet Profile</b> link </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Add</b> button at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter an <b>Aircraft code</b> for the new
Aircraft. This is a required field, must be unique and is user defined.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB> Enter the <b>Tail Number</b> and the new
aircraft’s <b>Serial Number</b>. These are required fields.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select an Aircraft <b>Type Code</b>. This must
be valid Aircraft Type code in the Aircraft Type table and is a required field.
 </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter an Aircraft <b>Description.</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select a <b>Home Base</b> where the Tail number
will be based. The home base will default from the user’s home base but can be
updated.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Primary Dom Flight Phone</b> and <b>Primary
Intl Flight Phone</b>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select a <b>Client Code</b>. This must be valid Client
Code in the Client table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter <b>Max PAX</b>. This is the maximum number
of passengers the aircraft can carry.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select a <b>Vendor Code</b>. If a code is
entered, it must be valid in the Vendor Table. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>  Enter <b>Min Runway</b>. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select <b>Maintenance Crew code</b>. The code
must be a valid code in the  Crew Roster table</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select an<b> Aircraft Class</b> from the drop
down. The Aircraft Class assigned to the Tail number will control some of the
required entries in Pre-flight – Trip leg. Valid Classes are </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB> 1 Single Engine Land</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>2 Multi Engine Land</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>3 Single Engine Sea</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>4 Multi Engine Sea</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>5 Turbo Jet </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>6 Helicopter</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select <b>Aircraft Type</b> from any one of
&quot;Small&quot;, &quot;Medium&quot; or &quot;Large'' </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select <b>Emergency Contact</b>. The code must
be a valid code in the Emergency Contact table.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Set the <b>FAR</b> either FAR 91 or FAR 135.
This setting defines the default rules the aircraft normally operates under.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the Aircraft is <b>Inactive</b>, the check
box should be set. Inactive Aircraft will not display when selecting Tail
numbers for use in Pre-flight, unless the user turns the Inactive filter off.
Inactive Aircraft will also not display in the scheduling calendars.  </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the following Inspection History details</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Last Inspection Date</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Hours to Next</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Hours to Warning</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the following Flight Plan defaults</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Cruise speed. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Max Flight Level</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the following Aircraft parameters</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Max Fuel, Min Fuel and Taxi Fuel</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Max Takeoff Wt, Max Landing Wt, Max Zero fuel Wt, Basic Operating Wt</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter SIFL multiples.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Control</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Non-control</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-GB>Sec. Control</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Set the Foreground Color and Back Color for the Tail
number. These colors will display on scheduling calendars (based on Scheduling
Calendar setting).</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If images are stored for the Tail number, these
can be browsed and used on reports, trip sheets etc. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter any Notes for the Aircraft in the Notes
tab.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.75in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Clicks on the Save button. </span></p>

<h3 style='line-height:150%'><a name="_Toc335040845"><span style='font-family:
"Calibri","sans-serif"'>Fleet Profile - International</span></a><span
style='font-family:"Calibri","sans-serif"'>  </span></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>After the Tail
Number / Aircraft has been added you can add more information about the
aircraft.  International data contains many fields required for APIS filings. </p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
style='color:#4F81BD'>How can I Add Intl data to a Tail Number?</span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have International
Data added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Intl. link</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the aircraft’s  Decal Number</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the  Aircraft Make</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the  Aircraft Model and Year</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the Aircraft’s Colors and Trim Colors</span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;line-height:150%'>&nbsp;</p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;line-height:150%'>Enter
the following Owner/Lessee Details. </p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Company Name or Last, First and Middle
Name</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Address fields</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter City </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter State  / Province</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Postal Code</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Home Phone number</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Business Fax number </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Business E-mail address </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;line-height:150%'><b><u><span lang=EN-GB><span
 style='text-decoration:none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;line-height:150%'>Enter
the following Operator Details. </p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Company Name or Last, First and Middle
Name</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Address fields</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter City </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter State  / Province</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Postal Code</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Country</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Home Phone number</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Business Fax number </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Business E-mail address </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040846"><span
style='font-family:"Calibri","sans-serif"'>How can I add Engine / Airframe
information?</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>If Company
Profile – “Post-flight – Fill Engine Airframe“ parameter is set, the hours and
cycles will be updated when a Post-flight Log is completed. Otherwise you can
manually enter values in the Engine / Airframe sub table.</p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have Engine / Airframe
data added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Engine / Airframe link</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter  Airframe Hours, Airframe Cycles and
Airframe As Of (date) for each engine</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Engine 1 Hours, Engine 1 Cycles, and
Engine 1 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Engine 2 Hours, Engine 2 Cycles, and
Engine 2 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Engine 3 Hours, Engine 3 Cycles, and
Engine 3 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Engine 4 Hours, Engine  4 Cycles, and
Engine 4 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Reverser  1 Hours, Reverser 1 Cycles, and
Reverser 1 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Reverser  2 Hours, Reverser 2 Cycles, and
Reverser 2 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Reverser  3 Hours, Reverser 3 Cycles, and
Reverser 3 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Reverser  4 Hours, Reverser 4 Cycles, and
Reverser 4 As Of (date)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Constant Mach 1st Hr,2nd Hr,3rd Hr,4th
Hr,5th Hr and so on till 20'th Hr</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Long Range Cruise 1st Hr,2nd Hr,3rd Hr,4th
Hr,5th Hr and so on till 20'th Hr</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter High Speed Cruise  1st Hr,2nd Hr,3rd
Hr,4th Hr,5th Hr and so on till 20'th Hr</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040847"><span
style='font-family:"Calibri","sans-serif"'>How can I add Fleet Component
Information for a Tail Number?</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>In addition to engines
and Airframes, you may need to track other parts of the aircraft. These might
include landing gear, stairs, avionics, oxygen systems etc. The component sub
table allows various components to be entered along with inspection dates,
warning dates etc. This sub table is optional – no information is required. </p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have Fleet Component
data added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Fleet Component link</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter a component description </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Last Inspection date</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Next Inspection </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Warning value and Unit (Hours, Cycles,
Days)</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter a serial number if the component has a
serial number</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<h3 style='line-height:150%'><a name="_Toc335040848"><span style='font-family:
"Calibri","sans-serif"'>Aircraft Charge rates</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Charge rates are
used to estimate the cost of operation an aircraft. Key entry fields include a
Charge rate and a Charge Unit. Charge Units are Hours and Distance. If “Hours”
radio button is selected, the system will use Company Profile settings, Flight Hours
or Block Hours parameter and the corresponding information from each trip leg
to do the calculation.  If Distance is selected, you can select the one setting
(radio button) for Nautical Miles, Statute Miles or Kilometers and charges will
be based on the charge rate times the distance button you have set. Charge
rates use Begin and End dates. Charges will calculate using the rates
appropriate for the trip legs / dates </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040849"><span
style='font-family:"Calibri","sans-serif"'>How can I add Charge Rates and
Charge Units?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have Charge Rates
added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Charge Rates link</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter a Charge Rate</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Charge Unit either Hours or Distance
(Nautical Miles, Statute Miles or Kilometres. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Begin and End Dates. These dates represent
the date range when entered Charge rate and charge unit is valid</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button </span></p>

<h3 style='line-height:150%'><a name="_Toc335040850"><span style='font-family:
"Calibri","sans-serif"'>Forecast</span></a></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'>A Forecast of
cost for each Tail number can be entered. If this sub table has data entered,
it can be displayed on reports comparing actual flight hours to forecast.
Multi-year forecast can be entered. Use of the Forecast sub table is optional.</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040851"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Forecast to a Tail
Number?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have Forecast data
added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Forecast link</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Add button</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter a Forecast Year. This is a required field.
</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Forecast Flight hours for each month in
the Forecast Year</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><u><span
 style='text-decoration:none'>&nbsp;</span></u></b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Additional
Information</b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Flight Scheduling Software allows
you to capture information about a tail number in a number of ways. A simple
way to do this is to add Notes to a Tail No. record.  A more structured way to
add information to a Tail number is to use the Additional Information feature.
Additional Information feature allows you to select additional information
codes and description from the Fleet Profile Additional Info table. Once you
select a code from the Fleet Profile Additional Info table, you can enter
specific information associated with the selected code. Example might include
selection of On board Fax Machine, and the actual Fax Machine serial number.
You could also use the Additional Info table to a list of common features found
in aircraft and then assign specific information i.e. serial number or cabin
location for each specific item on the selected aircraft. </p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040852"><span
style='font-family:"Calibri","sans-serif"'>How can I link specific Aircraft
information to an Additional Information Code?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Tail number to have Additional
Information added from the Fleet Profile grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Navigate to the Additional Information tab </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>The Tail Number Additional Info table will
display any existing Add Info codes that have been assigned to the Tail Number</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>If the desired code and description exists you
can enter the specific information in the Additional Information field   </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=EN-GB style='font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button  </span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:1.0in;text-align:justify;
line-height:150%'><b><span lang=EN-GB>&nbsp;</span></b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Aircraft Duty
Type</b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
150%'>An Aircraft Duty Type record is used to identify the specific type of
duty for an aircraft. Aircraft Duty Types are used in Fleet Calendars. There
are three predefined Aircraft Duty Type Code records, F –&gt; Flight; G –&gt;
Ground; R –&gt; Remain Overnight are included in the table. New Aircraft Duty
Types can be added. Start time and End time can be assigned to each Aircraft
Duty Type. </p>

<h3 style='line-height:150%'><span style='font-family:"Calibri","sans-serif"'> <a
name="_Toc335040853">How can I access Aircraft Duty Type Table?</a></span></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand Fleet tab</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on Aircraft Duty Type link</span></p>

<h3 style='line-height:150%'><a name="_Toc335040854"><span style='font-family:
"Calibri","sans-serif"'>How can I add a new Aircraft Duty Type?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Aircraft Duty Type table,
click on the Add button at the bottom of the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Aircraft Duty Code</b> - Code
associated with each Aircraft Duty Type. This is a required field and must be
unique. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter the <b>Description</b> - </span><span
lang=EN-GB>A description associated with the Aircraft Duty Type code (i.e. M -
Maintenance).</span><span lang=EN-GB> This is a required field.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Optionally you can enter the following fields,</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Start Time </span></b><span lang=EN-GB>– The time the Aircraft Duty
will begin in HH:MM format</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>End Time</span></b><span lang=EN-GB> – The time the Aircraft Duty
will end in HH:MM format</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Foreground Color</span></b><span lang=EN-GB> </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:1.0in;margin-bottom:.0001pt;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Background Color </span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Aircraft Standby</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Quick Calendar Entry</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button to save the newly
created record</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Cancel button to not to save the
new record</span></p>

<h3 style='line-height:150%'><a name="_Toc335040855"><span style='font-family:
"Calibri","sans-serif"'>How can I edit an existing Aircraft Duty Type record?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Note – Duty Type F, G and R are predefined and
you can’t edit these code or description. But you can define your own Start
Time and End Time for these predefined records.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Aircraft Duty Type table,
click on the Aircraft Duty Type record that you want to edit on the grid. You
can use sort or search or scroll functionalities to locate the record that want
to edit.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Edit</b> button at the bottom of
the grid</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You can edit one or more of  the following
fields,</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Description</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Start Time </span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>End Time </span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Aircraft Standby</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:1.0in;text-indent:-.25in;
line-height:150%'><span lang=EN-GB style='font-family:"Courier New"'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><b><span
lang=EN-GB>Quick Calendar Entry</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button to save all the changes</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Cancel  button to not to save any
changes</span></p>

<h3 style='line-height:150%'><a name="_Toc335040856"><span style='font-family:
"Calibri","sans-serif"'>How can I delete an existing Aircraft Duty Type?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Note – Duty Type F, G and R are predefined and
you can’t delete these records </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Once you are in the Aircraft Duty Type table,
click on the Aircraft Duty Type record that you want to delete on the grid. You
can use sort or search or scroll functionalities to locate the record that want
to delete.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the <b>Delete</b> icon at the bottom of
the grid.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>You will receive a warning message <b>“Are you
sure you want to delete this record?”</b> </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>Yes</b> button to delete the record</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on <b>No </b>button<b> </b>to not to
delete the record</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b>Flight
Category </b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'>Flight
Categories define the various reasons why an aircraft is flying.  Flight
Categories are assigned to each leg of a trip in Preflight.  Flight categories
will display on calendars and Post-flight Flight Logs.</p>

<h3 style='text-align:justify;line-height:150%'><a name="_Toc335040857"><span
style='font-family:"Calibri","sans-serif"'>How can I add a Flight Category?</span></a></h3>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Database button on the global menu</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Expand the Fleet Tab</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on Flight Category </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Flight Category Code. This is a required
field and must be unique</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Flight Category Code description. This is
a required field </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select the Fore and Background colors. These
settings define the colors that will be used in the scheduling calendars when
this flight category is display. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Select Inactive Check Box – If selected, the
flight category can’t be used for new trips</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:justify;text-indent:-.25in;line-height:150%'><span
lang=EN-GB style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Enter Client Code.  If a client code is assigned
to a specific flight category and that client code is selected in Pre-flight -
Trip, only the assigned flight categories can be used for that trip.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:justify;text-indent:-.25in;line-height:150%'><span lang=EN-GB
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Click on the Save button. </span></p>

<p class=MsoNormal>&nbsp;</p>

</div>
            </td>
        </tr>
    </table>

</asp:Content>
