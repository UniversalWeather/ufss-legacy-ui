﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="DelayTypeHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.DelayTypeHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Delay Type</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Delay Type Table is
                        used to define each type of aircraft delay incurred during a leg . The most common
                        examples are weather, late passengers, air traffic control and maintenance. You
                        can edit, delete or create new entries according to your business needs.</span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The codes can be applied
                        in Postflight to each leg when a delay occurs.<a name="_GoBack"></a></span></p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Fields in this table include
                    </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code – Abbreviation
                        used to define the type of delay </span>
                </p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description
                        – Description of the Delay Code </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
