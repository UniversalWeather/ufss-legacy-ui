﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FleetProfileAdditionalInfoHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.FleetProfileAdditionalInfoHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Profile Additional Info</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>The Fleet Profile Additional
                        Information Table is a place to set up individual fields of information. This database
                        allows you to track information about your fleet beyond what is provided in the
                        system. It is strongly recommend that you code so that they are grouped together
                        logically. For example, begin by using code 100, then index each subsequent entry
                        by at least 10 or more. This will allow flexibility should you decide to add codes
                        in the future. </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Fleet Additional Information
                        Table contains the following fields </span>
                </p>
                <p class="MsoListParagraphCxSpFirst" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code – Valid
                        code from the Additional Info Table </span>
                </p>
                <p class="MsoListParagraphCxSpMiddle" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description
                        of Additional Info – not editable </span>
                </p>
                <p class="MsoListParagraphCxSpLast" style='text-indent: -18.0pt'>
                    <span lang="EN-US" style='font-family: Symbol'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span></span><span lang="EN-US" style='font-family: "Arial","sans-serif"; line-height:20px;'>Additional
                        Info – User enters description, serial number or other information – ie Video Conference
                        number for aircraft, fax machine serial number, DVD serial number etc<a name="_GoBack"></a></span></p>
            </td>
        </tr>
    </table>
</asp:Content>
