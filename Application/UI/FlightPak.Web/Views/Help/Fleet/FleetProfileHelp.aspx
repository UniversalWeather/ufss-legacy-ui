﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FleetProfileHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.FleetProfileHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Profile</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Fleet Profile table
                        allows you to maintain descriptive data such as tail numbers, aircraft description,
                        aircraft engine and airframe hours, component information, forecast of aircraft
                        hours and charter specifications (CHARTER QUOTE USERS ONLY) for each of the aircraft
                        in your fleet. Each record of the Fleet Profile record represents aircraft that
                        are present in your fleet or your vendor’s fleet. This table consists of three tabular
                        areas - <b>Search</b>, <b>Maintenance</b> and <b>Notes</b>. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Following are key features:</span></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Numeric
                            code assigned to this particular aircraft by the user. Must be separate from the
                            vendor code. It is recommended that you space these as follows: 100. 120, 140, etc.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Home Base —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This is the aircraft’s Home Base.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Client Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This aircraft is associated with a single client.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Tail number —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            The identification number that appears on the aircraft.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Serial Number –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Manufacturers serial number for the aircraft</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Type Code —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Abbreviation for type of aircraft. Since this originates from the <a href="<%=ResolveClientUrl("~/Views/Help/Fleet/AircraftTypeHelp.aspx?Screen=AircraftTypeHelp") %>" style="font:12px arial;" >

                                Aircraft Types</a> database, this is where your type code is selected.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Type description —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Describes the aircraft type. Since this originates from the <a href="<%=ResolveClientUrl("~/Views/Help/Fleet/AircraftTypeHelp.aspx?Screen=AircraftTypeHelp") %>" style="font:12px arial;" >
                                Aircraft Types</a> database, it will automatically post from the type code.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Primary and Secondary Domestic Flight Phone —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            These fields contain the aircraft’s domestic phone numbers.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Primary and Secondary Intl Flight Phone —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            These fields contain the aircraft’s Intl phone numbers.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Max PAX —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Maximum
                            number of passengers this specific aircraft can carry. If the user exceeds the maximum
                            number of passengers, a warning is displayed.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Emergency Contact –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            used to associate an emergency contact to the aircraft – must be valid in the Emergency
                            Contact Table </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Vendor </span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>used
                            to associate a Vendor to the aircraft – must be valid in the Vendor Table</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Default FAR rule Check box for the Aircraft – FAR 91, FAR 135</span></b></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Vendor —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> The
                            Vendor that this aircraft is associated with. If the aircraft is not a vendor’s
                            aircraft, leave this field blank.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Max Reservations —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Number of reservations this aircraft can hold. For example, if you have an aircraft
                            that seats 15 and always carries an attendant, your maximum reservations would be
                            14 (attendants are excluded from the reservation count). ( No longer functional)
                        </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Min Runway —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This field defines the minimum runway on which this aircraft can land. When you
                            plan a trip with this aircraft, the <a href="<%=ResolveClientUrl("~/Views/Help/Logistics/AirportHelp.aspx?Screen=AirportHelp") %>" style="font:12px arial;" >Airport Database</a>
                            will confirm that this value is higher than the maximum runway. If it is not, you
                            will receive a warning in. </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Aircraft Class —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            There are 6 predefined values; 1. Single Engine Land; 2 Multi Engine Land; 3 – Single
                            Engine Sea; 4 – Multi Engine Sea; 5 – Turbo Jet; 6 – Helicoptor </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        field is exported into Travel$ense for cost comparison purposes.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Aircraft Type –</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Used to define the type of aircraft to calculate landing fees ie Small, Medium and
                            Large </span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Inactive Checkbox—</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Select this check box if the aircraft is no longer in service. When selected, it
                            will not appear on reports or calendars. </span></li>
                </ul>
                <p class="MsoNormal" style='margin-left:20px; margin-right:20px; text-align: left; line-height:20px;'>
                    <i><span lang="EN-US" style='font-family: "Arial","sans-serif"; color: #C00000'>It is
                        recommended that you place an aircraft into inactive status instead of deleting
                        it completely from your database for history and reporting purposes.</span></i></p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Inspection History —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            This sub table allows you to project into the future dates when this aircraft will
                            need inspection. <i>Last Hourly</i>, <i>Hours to Next</i>, and <i>Hours to Warning</i>
                            are your choices.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Aircraft parameters —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Here is where you will input the maximum and minimum fuel requirements along with
                            basic operating weight (BOW) values.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        SIFL Multipliers —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Input the value for the Control Non-control employees and Sec employees. These values
                            are defined by the IRS.</span></li>
                </ul>
                <p class="MsoNormal" style='margin-left: 18.0pt; line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>There are a number of
                        sub tables associate with each record in the Fleet Table </span></b>
                </p>
                <ul type="disc">
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        International —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>T</span><span
                            lang="EN-US" style='font-family: "Arial","sans-serif"'>able maintains Decal Number,
                            Owner/Lessee and address information necessary for international operations. This
                            information will be printed onto customs forms.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Engine/Airframe —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Tables
                            contains parameters for </span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                                fuel burn, Airframe, Engine and Reverser hours for tracking and reporting purposes.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Components —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Table </span><span lang="EN-US" style='font-family: "Arial","sans-serif"'>tracks component
                                status and maintenance history information such as last inspection date, next inspection
                                date and warnings for aircraft components, such as landing gear. You will also have
                                the serial number for that inspection and an option to track it in hours, days or
                                cycles. These are generated within Flight Scheduling Software reports.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Additional Information —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Itemized list of equipment or components aboard aircraft as specified in the <a href="<%=ResolveClientUrl("~/Views/Help/Fleet/FleetProfileHelp.aspx?Screen=FleetProfileHelp") %>" style="font:12px arial;" >Fleet Profile Additional Information</a> database. Includes
                            such equipment as onboard fax machines and computers.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Forecast —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'> Table
                            contains forecast usage by year for the aircraft. For this year, you will document
                            how many hours each month you expect to operate a particular aircraft. This is directly
                            related to your budgeting reports.</span></li>
                    <li class="MsoNormal" style='line-height:20px;'><b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                        Chrg. Rates —</span></b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>
                            Here you will have a begin and end date, charge rate and unit whether it is hourly,
                            nautical miles or statute miles. For example, when you run billing reports, they
                            will be produced from this rate’s table as reflected in the records. It is very
                            important to populate these unit fields so there is not a void in time and so that
                            these rates are projected into the future.<br>
                            <b>Charter Rate. —</b> FOR CHARTER QUOTE USERS ONLY. Links to Charter Module<a name="_GoBack"></a>.
                            Here is where you set charge parameters for your charter operations.</span></li>
                </ul>
                <p class="MsoNormal" style='line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
