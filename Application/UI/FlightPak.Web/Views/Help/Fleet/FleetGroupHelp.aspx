﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Help.Master"
    AutoEventWireup="true" CodeBehind="FleetGroupHelp.aspx.cs" Inherits="FlightPak.Web.Views.Help.Fleet.FleetGroupHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Fleet Group</span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>The Fleet Group table allows
                        you to group your fleet into specific categories (i.e. Fixed, Rotary, or by manufacturer
                        ie Lear, Gulfstream etc .) for the purpose of ordering aircraft in the calendar
                        displays. Each group can be assigned an alpha or numeric <b>Code</b> and a corresponding
                        <b>Description</b>. </span>
                </p>
                <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; line-height:20px;'>
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>Aircraft can be added to
                        a fleet group by “dragging” the selected aircraft from the available grid to the
                        selected grid. Aircraft can be removed from a selected group by “dragging “ selected
                        aircraft from the selected grid to the Available grid.<a name="_GoBack"></a></span></p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Code —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> This is a four-character
                        identifier for your fleet group. </span>
                </p>
                <p class="MsoNormal" style='line-height:20px;'>
                    <b><span lang="EN-US" style='font-family: "Arial","sans-serif"'>Description —</span></b><span
                        lang="EN-US" style='font-family: "Arial","sans-serif"'> Name for your fleet group
                        that corresponds with the assigned code. </span>
                </p>
                <p class="MsoNormal">
                    <span lang="EN-US" style='font-family: "Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>
</asp:Content>
