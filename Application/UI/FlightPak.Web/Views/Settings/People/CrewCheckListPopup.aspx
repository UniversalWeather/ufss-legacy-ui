﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewCheckListPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewCheckListPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crew CheckList</title>
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>    
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
     <style type="text/css">
        .box1 tr {
            height: 0 !important;
            vertical-align: top !important;
           }
        .RadWindow {
            height: 445px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            $(document).ready(function () {
                var checklistId;
                $("#recordAdd").click(function () {
                    var dgCheckList = "<%= dgCrewCheckList.ClientID %>";
                    var widthDoc = $(document).width();
                    popupwindow("/Views/Settings/People/CrewCheckListCatalog.aspx?IsPopup=Add", "Check List Catalog", widthDoc + 200, 650, dgCheckList);

                    return false;
                });
                $("#recordEdit").click(function () {
                    var grid = $find("<%= dgCrewCheckList.ClientID %>").get_masterTableView();
                    var selectedRows = grid.get_selectedItems(); 
                    var widthDoc = $(document).width();
                    var dgCheckList = "<%= dgCrewCheckList.ClientID %>";
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell = grid.getCellByColumnUniqueName(row, "CrewCheckID");
                        checklistId = cell.innerHTML;
                    }
                    if (checklistId != undefined && checklistId != '' && checklistId != 0) {
                        popupwindow("/Views/Settings/People/CrewCheckListCatalog.aspx?IsPopup=&CrewChecklistID=" + checklistId, "Check List Catalog", widthDoc + 200, 650, dgCheckList, 'radCrewCheckListMasterPopupAdd');
                    }
                    else {
                        showMessageBox('Please select an Check list.', popupTitle);
                    }
                });
                $("#recordDelete").click(function () {
                    debugger;
                    var grid = $find("<%= dgCrewCheckList.ClientID %>").get_masterTableView();
                    var selectedRows = grid.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell = grid.getCellByColumnUniqueName(row, "CrewCheckID");
                        checklistId = cell.innerHTML;
                    }
                    if (checklistId != undefined && checklistId != '' && checklistId != 0) {
                        showConfirmPopup("Warning, Deleting this crew check list code will delete the code from all crew members. Continue Deleting?", confirmCallBackFn, 'Confirmation');
                    } else {
                        showMessageBox('Please select an Check list.', popupTitle);
                    }
                });
                function confirmCallBackFn(arg) {
                    if (arg) {
                        $.ajax({
                            async: true,
                            type: 'Get',
                            url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=CrewCheckList&crewCheckListId=' +  checklistId,
                            contentType: 'text/html',
                            success: function (data) {
                                verifyReturnedResultForJqgrid(data);
                                var message = "This record is already in use. Deletion is not allowed.";
                                if (data.indexOf("PreconditionFailed") == 0 || data.indexOf("NotFound") == 0 || data.indexOf("Conflict") == 0 || data == 0) {
                                    showMessageBox(message, popupTitle);
                                } else if (data.indexOf("Failed to execute api") == 0) {
                                    showMessageBox(data, popupTitle);
                                } else {
                                    var masterTable = $find("<%= dgCrewCheckList.ClientID %>").get_masterTableView();
                                    masterTable.rebind();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                var content = jqXHR.responseText;
                                if (content.indexOf('404'))
                                    showMessageBox("This record is already in use. Deletion is not allowed.", popupTitle);
                            }
                        });
                    }
                }
            });
            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewCheckList.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewCheckCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "CrewChecklistDescription");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "IsScheduleCheck");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "CrewCheckID");
                }

                if (selectedRows.length > 0) {
                    oArg.CrewCheckCD = cell1.innerHTML;
                    oArg.CrewChecklistDescription = cell2.innerHTML;
                    oArg.IsScheduleCheck = cell3.innerHTML;
                    oArg.CrewCheckID = cell4.innerHTML;
                }
                else {
                    oArg.CrewCheckCD = "";
                    oArg.CrewChecklistDescription = "";
                    oArg.IsScheduleCheck = "";
                    oArg.CrewCheckID = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }
            function RowDblClick() {
                var masterTable = $find("<%= dgCrewCheckList.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgCrewCheckList" runat="server" AllowMultiRowSelection="true"
                        AllowSorting="true" OnNeedDataSource="CrewCheckList_BindData" OnItemCommand="CrewCheckList_ItemCommand"
                        OnInsertCommand="CrewCheckList_InsertCommand" AutoGenerateColumns="false" Height="330px"
                        PageSize="10" AllowPaging="true" Width="500px" PagerStyle-AlwaysVisible="true" OnPreRender="dgCrewCheckList_PreRender">
                        <MasterTableView DataKeyNames="CrewCheckID,CrewCheckCD,CrewChecklistDescription,IsScheduleCheck"
                            CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CrewCheckCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CrewChecklistDescription" HeaderText="Crew Checklist"
                                    CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                    HeaderStyle-Width="280px" FilterControlWidth="260px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsScheduleCheck" HeaderText="Schedule Check"
                                    CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                    HeaderStyle-Width="100px" FilterControlWidth="80px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="CrewCheckID" HeaderText="Type Code" CurrentFilterFunction="EqualTo"
                                    Display="false" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <a class="add-icon-grid" title="Add" href="javascript:void(0);" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="javascript:void(0);return false;"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="javascript:void(0)"></a>
                                <div style="padding: 5px 5px; text-align: right;">
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>
