﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewChecklistGroupPopup : System.Web.UI.Page
    {
        private string CheckGroupCD;
        protected void Page_Load(object sender, EventArgs e)
        {
            
                if (!string.IsNullOrEmpty(Request.QueryString["CheckGroupCD"]))
                {
                    CheckGroupCD = Request.QueryString["CheckGroupCD"].ToUpper().Trim();
                }
            
     
        }

        protected void CrewChecklistGroup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ObjRetVal = ObjService.GetTripManagerChecklistGroupList();
                if (ObjRetVal.ReturnFlag == true)
                {
                    dgCrewChecklistGroup.DataSource = ObjRetVal.EntityList;
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgCrewChecklistGroup;
            SelectItem();
        }

        private void SelectItem()
        {
            if (!string.IsNullOrEmpty(CheckGroupCD))
            {
                foreach (GridDataItem item in dgCrewChecklistGroup.MasterTableView.Items)
                {
                    if (item["CheckGroupCD"].Text.Trim().ToUpper() == CheckGroupCD)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (dgCrewChecklistGroup.MasterTableView.Items.Count > 0)
                {
                    dgCrewChecklistGroup.SelectedIndexes.Add(0);

                }
            }
        }

        protected void dgCrewChecklistGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgCrewChecklistGroup_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewChecklistGroup, Page.Session);
        }
    }
}