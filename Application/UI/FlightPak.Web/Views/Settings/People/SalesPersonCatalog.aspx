﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="SalesPersonCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.SalesPersonCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function OnClientClick(strPanelToExpand) {

                var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
                var PanelBar2 = $find("<%= pnlMaintenance.ClientID %>");
                PanelBar1.get_items().getItem(0).set_expanded(false);
                PanelBar2.get_items().getItem(0).set_expanded(false);
                if (strPanelToExpand == "Notes") {
                    PanelBar1.get_items().getItem(0).set_expanded(true);
                }
                return false;
            }


            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                    }
                }
            }


            function openHomeBase() {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CompanyMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radCompanyPopup");
                oWnd.add_close(OnClientCloseAirportMasterPopup);
            }

            // To Store Image
            function File_onchange() {
                if (validate())
                    __doPostBack('__Page', 'LoadImage');
            }

            function validate() {
                var uploadcontrol = document.getElementById('<%=fileUL.ClientID%>').value;
                //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
                var reg = /([^\s]+(?=.(jpg|gif|bmp|jpeg)).\2)/gm;
                if (uploadcontrol.length > 0) {
                    if (reg.test(uploadcontrol)) {
                        return true;
                    }
                    else {
                        //alert("Only .bmp files are allowed!");
                        return false;
                    }
                }
            }

            function CheckName() {
                var nam = document.getElementById('<%=tbImgName.ClientID%>').value;

                if (nam != "") {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                }
            }

            // this function is related to Image 
            function OpenRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                oWnd.show();
            }

            // this function is related to Image 
            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }
            function openCountry() {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountry.ClientID%>").value, "RadCountryMasterPopup");

            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCountry.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = "";
                        document.getElementById("<%=cvCountry.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }


            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                //alert(new String("Width:" + bounds.width + " " + "Height: " + bounds.height));
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Salesperson");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }

            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });

            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
        <script type="text/javascript">
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Country" OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Company" AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Company" OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Salesperson</span> <span class="tab-nav-icons">
                        <!--<a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a>-->
                        <a href="../../Help/Company/SalesPersonHelp.aspx?Screen=SalesPersonHelp" class="help-icon"
                            target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                AutoPostBack="true" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgSalesPerson" runat="server" AllowSorting="true" OnItemCreated="dgSalesPerson_ItemCreated"
            Visible="true" OnNeedDataSource="dgSalesPerson_BindData" OnItemCommand="dgSalesPerson_ItemCommand"
            OnPageIndexChanged="dgSalesPerson_PageIndexChanged" OnUpdateCommand="dgSalesPerson_UpdateCommand"
            OnInsertCommand="dgSalesPerson_InsertCommand" OnPreRender="dgSalesPerson_PreRender"
            OnDeleteCommand="dgSalesPerson_DeleteCommand" AutoGenerateColumns="false" Height="341px"
            PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgSalesPerson_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView DataKeyNames="CustomerID,SalesPersonCD,CountryID,HomeBaseID,IcaoID,SalesPersonID,Name,FirstName,LastName,MiddleName,PhoneNum,Address1,Address2,Address3,CityName,StateName,PostalZipCD,NationalityCD,FaxNum,PersonalFaxNum,CellPhoneNum,EmailAddress,PersonalEmailID,Logo,LogoPosition,
                                           Notes,IsInActive,LastUpdUID,LastUpdTS,IsDeleted" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="SalesPersonCD" HeaderText="Salesperson Code"
                        FilterDelay="500" HeaderStyle-Width="120px" FilterControlWidth="100px" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Name" HeaderText="Description" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="240px"
                        FilterDelay="500" FilterControlWidth="220px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="E-mail" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="220px"
                        FilterDelay="500" FilterControlWidth="200px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IcaoID" HeaderText="Home Base" HeaderStyle-Width="100px"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SalesPersonID" HeaderText="SalesPersonID" Display="false"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false">
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddSalesPersonCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditSalesPersonCatalog)%>' ToolTip="Edit"
                            CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeleteSalesPersonCatalog)%>' runat="server"
                            CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel80">
                                                            <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <span class="mnd_text">Code</span>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumericAndChar(this, event,'-')"
                                                                            onBlur="return RemoveSpecialChars(this);" AutoPostBack="true" OnTextChanged="Code_TextChanged"
                                                                            CssClass="text40" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Salesperson Code is Required"
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Salesperson Code is Required"
                                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbName" runat="server" Text="First Name"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbFirstName" runat="server" MaxLength="40" CssClass="text110"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel80" valign="top">
                                                            <asp:Label ID="Label1" runat="server" Text="Middle Name"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbMiddleName" runat="server" MaxLength="40" CssClass="text110"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel80" valign="top">
                                                            <asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbLastName" runat="server" MaxLength="40" CssClass="text110"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbAddress1" runat="server" Text="Address 1"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbAddr1" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbAddr2" runat="server" Text="Address 2"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbAddr2" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbAddr3" runat="server" Text="Address 3"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbAddr3" runat="server" MaxLength="100" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCity" runat="server" Text="City"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbCity" runat="server" MaxLength="30" CssClass="text200"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbState" runat="server" Text="State/Province"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbCountry" runat="server" Text="Country"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCountry" runat="server" CssClass="text40" OnTextChanged="Country_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" ValidationGroup="save"
                                                                            MaxLength="3"></asp:TextBox>
                                                                        <asp:Button ID="btnCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                            ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbPostal" runat="server" Text="Postal"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbPhone" runat="server" Text="Business Phone"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum" runat="server" MaxLength="25" CssClass="text200"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbEmail" runat="server" Text="Business E-mail"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbEmail" runat="server" MaxLength="250" CssClass="text200" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                            ControlToValidate="tbEmail" CssClass="alert-text" ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                        </asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPersonalEmailID" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regPersonalEmail" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPersonalEmailID" CssClass="alert-text"
                                                                            ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                                        </asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:Label ID="lbFax" runat="server" Text="Business Fax"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFax" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPersonalFaxNum" runat="server" MaxLength="25" CssClass="text200"
                                                                            ValidationGroup="save" onKeyPress="return fnAllowPhoneFormat(this,event)">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150" valign="top">
                                                            Home Base
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbHomeBase" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="HomeBase_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                        </asp:TextBox>
                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomeBase();return false;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                            ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Image" CssClass="PanelHeaderStyle">
                        <ContentTemplate>
                            <table width="100%" class="box1">
                                <tr style="display: none;">
                                    <td class="tdLabel100">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel370">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="Static"
                                                        onchange="javascript:return File_onchange();" />
                                                    <asp:Label ID="lblError" runat="server" CssClass="alert-text" Style="float: left;">File types must be .jpg, .gif or .bmp</asp:Label>
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="rexpfileUL" runat="server" ControlToValidate="fileUL"
                                                        ValidationGroup="Save" CssClass="alert-text" ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([jJ][pP][eE][gG])$)"
                                                        Display="Static"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                Logo Position
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="radlstLogoPosition" runat="server" RepeatDirection="Horizontal"
                                                                    CellPadding="0" CellSpacing="0">
                                                                    <asp:ListItem Value="0">None</asp:ListItem>
                                                                    <asp:ListItem Value="1">Center</asp:ListItem>
                                                                    <asp:ListItem Value="2">Left </asp:ListItem>
                                                                    <asp:ListItem Value="3">Right </asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">
                                        <table cellpadding="0px" cellspacing="0px">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                        OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                                        ClientIDMode="Static">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <td class="input_no_bg" style="text-align:justify !important;">
                                                <asp:Literal ID="litCQLogoMsg" runat="server"></asp:Literal>
                                            </td>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                            ClientIDMode="Static" />
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                            OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db ">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnCountryID" runat="server" />
                        <asp:HiddenField ID="hdnSalesPersonID" runat="server" />
                        <asp:HiddenField ID="hdnBrowserName" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
