﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class TravelCoordinatorAssignedAircraft : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Grid Control could be ajaxified when the page is initially loaded.
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgClientCode, dgClientCode, RadAjaxLoadingPanel1);
            // Store the clientID of the grid to reference it later on the client
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgClientCode.ClientID));
        }
        protected void dgClientCode_ItemCreated(object sender, GridItemEventArgs e)
        {
            /*if (e.Item is GridCommandItem)
            {
                //Added based on UWA requirement.
                LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
            }*/
        }

        /// <summary>
        /// Bind Delay Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            /* FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();
             if (ObjService.GetDelayTypeList().ReturnFlag == true)
             {
                 dgClientCode.DataSource = ObjService.GetDelayTypeList().EntityList;
             }*/
            DataTable dt = new DataTable();
            dt.Columns.Add("CODE", typeof(string));
            dt.Columns.Add("DESC", typeof(string));
            
            dt.Columns.Add("LASTUSER", typeof(string));
            dt.Columns.Add("LASTUPDT", typeof(DateTime));

            dt.Rows.Add("UNIV ", "UNIVERSAL WEATHER & AVIATION",  "Sujitha", DateTime.Now);
           

            dgClientCode.DataSource = dt;
        }

        /// <summary>
        /// Item Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_ItemCommand(object sender, GridCommandEventArgs e)
        {
            /*switch (e.CommandName)
            {
                case RadGrid.EditCommandName:
                    e.Canceled = true;
                    e.Item.Selected = true;
                    GridDataItem item = (GridDataItem)Session["SelectedItem"];
                    tbCode.Text = (item).GetDataKeyValue("Delay_Type_Code").ToString();
                    if ((item).GetDataKeyValue("Delay_Type_Description") != null)
                    {
                        tbDescription.Text = (item).GetDataKeyValue("Delay_Type_Description").ToString();
                    }
                    else
                    {
                        tbDescription.Text = string.Empty;
                    }

                    tbCode.ReadOnly = true;
                    tbCode.BackColor = System.Drawing.Color.LightGray;
                    pnlExternalForm.Visible = true;
                    btnSaveChanges.Text = "Update";
                    break;
                case RadGrid.InitInsertCommandName:
                    e.Canceled = true;
                    tbCode.BackColor = System.Drawing.Color.White;
                    dgClientCode.SelectedIndexes.Clear();
                    tbCode.ReadOnly = false;
                    DisplayInsertForm();
                    break;
                case "Filter":
                    foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                    {
                        //column.CurrentFilterValue = string.Empty;
                        //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                    }
                    break;
                default:
                    break;*/
            
        }

        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_UpdateCommand(object source, GridCommandEventArgs e)
        {
           //e.Canceled = true;

           // try
           // {
           //     if ((Session["SelectedItem"] != null))
           //     {
           //         GridDataItem item = (GridDataItem)Session["SelectedItem"];
           //         FlightPakMasterService.MasterCatalogServiceClient ObjDelaytypeService = new FlightPakMasterService.MasterCatalogServiceClient();
           //         FlightPakMasterService.Delay_Type_Master objDelaytype = new FlightPakMasterService.Delay_Type_Master();
           //         objDelaytype.Delay_Type_Code = tbCode.Text;
           //         objDelaytype.Delay_Type_Description = tbDescription.Text;
           //         objDelaytype.EXCL_SIFL = false;
           //         objDelaytype.CLIENT = "";
           //         ObjDelaytypeService.UpdateDelayType(objDelaytype);
           //         e.Item.OwnerTableView.Rebind();
           //         e.Item.Selected = true;
           //         pnlExternalForm.Visible = false;
           //     }
           // }
           // catch (System.NullReferenceException exc)
           // {
           //     e.Canceled = true;
           //     throw exc;
           // }
           // catch (System.ArgumentOutOfRangeException exc)
           // {
           //     e.Canceled = true;
           //     throw exc;
           // }
           // catch (System.InvalidCastException exc)
           // {
           //     e.Canceled = true;
           //     throw exc;
           // }
        }

        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_InsertCommand(object source, GridCommandEventArgs e)
        {
            //try
            //{
            //    e.Canceled = true;

            //    FlightPakMasterService.MasterCatalogServiceClient ObjDelaytypeService = new FlightPakMasterService.MasterCatalogServiceClient();
            //    FlightPakMasterService.Delay_Type_Master objDelaytype = new FlightPakMasterService.Delay_Type_Master();

            //    objDelaytype.Delay_Type_Code = tbCode.Text;
            //    objDelaytype.Delay_Type_Description = tbDescription.Text;
            //    objDelaytype.EXCL_SIFL = false;
            //    objDelaytype.CLIENT = "";

            //    ObjDelaytypeService.AddDelayType(objDelaytype);
            //    pnlExternalForm.Visible = false;
            //    dgClientCode.Rebind();
            //}
            //catch (System.NullReferenceException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}
            //catch (System.ArgumentOutOfRangeException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}
            //catch (System.InvalidCastException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgClientCode_DeleteCommand(object source, GridCommandEventArgs e)
        {
            /*var Code = ((GridDataItem)dgClientCode.SelectedItems[0]).GetDataKeyValue("Delay_Type_Code");
            FlightPakMasterService.MasterCatalogServiceClient ObjDelaytypeService = new FlightPakMasterService.MasterCatalogServiceClient();
            FlightPakMasterService.Delay_Type_Master objDelaytype = new FlightPakMasterService.Delay_Type_Master();
            objDelaytype.DELETED = true;
            ObjDelaytypeService.DeleteDelayType(objDelaytype);

            Session["SelectedItem"] = null;
            pnlExternalForm.Visible = false;*/
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgClientCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["SelectedItem"] = (GridDataItem)dgClientCode.SelectedItems[0];
            //DateTime LastUpdatedTime;
            //string LastUpdatedUser;
            //if ((Session["SelectedItem"] != null))
            //{
            //    GridDataItem item = (GridDataItem)Session["SelectedItem"];               
            //    LastUpdatedTime = Convert.ToDateTime((item).GetDataKeyValue("LASTUPDT"));
            //    LastUpdatedUser =  (item).GetDataKeyValue("LASTUSER").ToString();

            //    Label lbLastUpdatedUser = (dgClientCode.Controls.FindControl("lbLastUpdatedUser") as Label);
            //    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last updated by " + LastUpdatedUser + " on " + LastUpdatedTime + " UMT");
            //}
        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
           /* pnlExternalForm.Visible = true;
            btnSaveChanges.Text = "Save";

            tbCode.Text = string.Empty;
            tbDescription.Text = string.Empty;*/
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {/*
            if (btnSaveChanges.Text == "Update")
            {
                (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
            }
            else
            {
                (dgClientCode.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
            }*/
        }

        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
           /* pnlExternalForm.Visible = false;
            Session["SelectedItem"] = null;*/
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            if ((e.Initiator.ID.IndexOf("btnSaveChanges") > -1))
            {
                e.Updated = dgClientCode;
            }
        }
    }
}