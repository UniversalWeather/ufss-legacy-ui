﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CrewCheckListCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.CrewCheckList"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>


<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
    <script type="text/javascript">
        function Clicking(sender, args) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var Description = document.getElementById("<%=tbDescription.ClientID%>").value;
            if (Description == "") {
                ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'), true);
                if (txtCode != "") {
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 100);
                }
                return false;
            }

            
            var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit) {
                    if ((txtCode != "")) {
                        if (document.getElementById("<%=tbCodeId.ClientID%>").value == "") {
                            document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
                            this.click();
                        }
                        else {
                            document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
                            this.click();
                        }
                    }
                    else {
                        document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
                        this.click();
                    }
                }
                else {
                    if (document.getElementById("<%=tbCodeId.ClientID%>").value == "") {
                        document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                        this.click();
                    }
                }
            });

            var callBackFunctionAlert = Function.createDelegate(sender, function (shouldSubmit) {
                document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                this.click();
            });

            var updateCallBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit) {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                    this.click();
                }
            });

            if (document.getElementById("<%=tbCodeId.ClientID%>").value) {
                if (document.getElementById("<%=isChkAllCrew.ClientID%>").value == "True") {
                    var text = "Do you want to save changes to the current record?";
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(text, callBackFunction, 300, 100, null, "FlightPak System");
                    args.set_cancel(true);
                }
                else {
                    var text = "Do you want to save changes to the current record?";
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(text, updateCallBackFunction, 300, 100, null, "FlightPak System");
                    args.set_cancel(true);
                }

            }
            else {
                if (document.getElementById("<%=isChkAllCrew.ClientID%>").value == "True") {
                    var text = "Add New Crew Checklist Code To All Crew Members?";

                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(text, callBackFunction, 300, 100, null, "FlightPak System");
                    args.set_cancel(true);
                }
                else {
                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
                }
            }
        }


        function CheckTxtBox(state) {
            ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'), state);
        }
        
        function openWin(radWin) {
            var url = '';

            if (radWin == "RadExportData") {
                url = "../../Reports/ExportReportInformation.aspx?Report=RptDBCrewChecklistExport&UserCD=UC";
            }

            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, radWin);
        }


        //        function CheckPassportChoice() {
        //            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;

        //            var Description = document.getElementById("<%=tbDescription.ClientID%>").value;
        //            if (Description == "") {
        //                ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'), true);
        //                if (txtCode != "") {
        //                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 100);
        //                }
        //                return false;
        //            }

        //            if ((txtCode != "")) {
        //                Msg = "Add New Crew Checklist Code To All Crew Members?";
        //                var IsResult = confirm(Msg, "Crew");

        //                if (IsResult == true) {
        //                    document.getElementById('<%=hdnValue.ClientID%>').value = "Yes";
        //                }
        //                else {
        //                    document.getElementById('<%=hdnValue.ClientID%>').value = "No";
        //                }
        //            }

        //        }

        function ProcessDeletewarn(customMsg) {
            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
            var grid = $find(window['gridId']);

            var msg = 'Are you sure you want to delete this record?';

            if (customMsg != null) {
                msg = customMsg;
            }

            if (grid.get_masterTableView().get_selectedItems().length > 0) {
                if (confirm(msg)) {
                    //                    app.alert(msg, 2, 2, Error);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select a record from the above table');
                return false;
            }
        }
        function GetDimensions(sender, args) {
            var bounds = sender.getWindowBounds();
            return;
        }
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
        function CheckTxtBox(control) {
            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }


            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);

                } return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            $(document).ready(function UpdateConfirm() {
                $('#art-contentLayout_popup').css("width", "600px");
                $('#art-sheet').css("width", "620px");
                $('#art-sheet-body').css("width", "620px");
                $('#art-sheet-body').css("position", "fixed");
                $('#art-sheet-body').css("padding", "0px 0px 0px 5px");
                
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

        </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        ClientIDMode="AutoID">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewCheckList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCrewCheckList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCode" />
                    <telerik:AjaxUpdatedControl ControlID="cvCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" />
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Crew Checklist Codes</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBCrewChecklist');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:ShowReports('','EXPORT','RptDBCrewChecklistExport');return false;"
                            OnClick="btnShowReports_OnClick" class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=CrewChecklistHelp" target="_blank" title="Help"
                            class="help-icon"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td class="tdLabel100" align="left">
                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                     <td align="left">
                        <asp:CheckBox ID="chkIsScheduleCheck" runat="server" Text="Scheduled Check Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                            AutoPostBack="true" />
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="dgCrewCheckList" runat="server" AllowSorting="true" OnItemCreated="CrewCheckList_ItemCreated"
                OnNeedDataSource="CrewCheckList_BindData" OnItemCommand="CrewCheckList_ItemCommand"
                OnPreRender="CrewCheckList_PreRender" OnUpdateCommand="CrewCheckList_UpdateCommand"
                OnInsertCommand="CrewCheckList_InsertCommand" OnPageIndexChanged="CrewChecklist_PageIndexChanged"
                OnDeleteCommand="CrewCheckList_DeleteCommand" AutoGenerateColumns="false" Height="341px"
                PageSize="10" AllowPaging="true" OnSelectedIndexChanged="CrewCheckList_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView DataKeyNames="CustomerID,CrewCheckID,CrewCheckCD,CrewChecklistDescription,IsScheduleCheck,LastUpdUID,LastUpdTS,IsCrewCurrencyPlanner,IsDeleted,IsInActive"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CrewCheckCD" AutoPostBackOnFilter="false" HeaderText="Checklist Code"
                            CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="100px"
                            FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CrewChecklistDescription" AutoPostBackOnFilter="false"
                            HeaderText="Description" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                            HeaderStyle-Width="460px" FilterControlWidth="440px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsScheduleCheck" AutoPostBackOnFilter="true"
                            HeaderStyle-Width="100px" HeaderText="Scheduled Check" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="CrewCheckID" AutoPostBackOnFilter="false" HeaderText="Checklist Code"
                            Display="false" CurrentFilterFunction="EqualTo" ShowFilterIcon="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="80px">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddCrewCheckListCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditCrewCheckListCatalog)%>' ToolTip="Edit"
                                CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteCrewCheckListCatalog)%>'
                                runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130">
                                    <asp:CheckBox ID="chkScheduled" runat="server" Text="Scheduled Check" Checked="true" />
                                </td>
                                <td valign="top">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                                <td align="left">
                                    <%--<asp:CheckBox ID="chkCrewCurrencyPlanner" runat="server" Text="Crew Currency Planner" />--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel100" valign="top">
                                    <span class="mnd_text">Checklist Code</span>
                                </td>
                                <td align="left" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" MaxLength="3" CssClass="text40" ValidationGroup="save"
                                                    OnTextChanged="Code_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Checklist Code is Required"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="save" ControlToValidate="tbCode"
                                                    Text="Checklist Code is Required." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel100" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" runat="server" MaxLength="25" CssClass="text225"></asp:TextBox>
                                                <asp:HiddenField ID="tbCodeId" runat="server" />
                                                <asp:HiddenField ID="isChkAllCrew" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="save"
                                                    ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                                    ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="pad-top-btm" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellpadding="0">
                            <tr>
                                <td class="custom_radbutton">
                                    <telerik:RadButton ID="btnSaveChanges" runat="server" Text="Save" ValidationGroup="save"
                                        OnClientClicking="Clicking" OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnValue" runat="server" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
