﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignAircraftPopUp.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.AssignAircraftPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select Available Aircraft</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgAssignRequestors.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;

                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function ConfirmWindowClose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }

            function ConfirmWindowClose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAssignRequestors.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var cell, cell1,cell2;
                var AircraftCD = ""
                var FleetID = ""
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];

                    cell = MasterTable.getCellByColumnUniqueName(row, "AircraftCD");
                    cell1 = MasterTable.getCellByColumnUniqueName(row, "FleetID");
                    cell2 = MasterTable.getCellByColumnUniqueName(row, "TailNum");
                    if (i == (selectedRows.length - 1)) {
                        AircraftCD = AircraftCD + cell.innerHTML;
                        FleetID = FleetID + cell1.innerHTML;
                        TailNum = TailNum + cell2.innerHTML;
                    }
                    else {
                        AircraftCD = AircraftCD + cell.innerHTML + ",";
                        FleetID = FleetID + cell1.innerHTML + ",";
                        TailNum = TailNum + cell2.innerHTML + ",";
                    }
                }
                oArg.AircraftCD = AircraftCD;
                oArg.FleetID = FleetID;
                oArg.TailNum = TailNum;
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
                else {
                    alert("Please fill both fields");
                }
            }
            function RowDblClick() {
                var masterTable = $find("<%= dgAssignRequestors.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }

            </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
       <%-- <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgAssignRequestors">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAssignRequestors" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAssignRequestors" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadWindow1" runat="server" AutoSize="true" Modal="true" Behaviors="None">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>--%>
        
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgAssignRequestors" runat="server"  OnInsertCommand="dgAssignRequestors_InsertCommand"
            AllowSorting="true" OnNeedDataSource="dgAssignRequestors_BindData" AllowMultiRowSelection="true" 
            AutoGenerateColumns="false" Height="341px" AllowPaging="false" Width="600px" OnItemCommand="dgAssignRequestors_ItemCommand"
            PagerStyle-AlwaysVisible="true" OnPreRender="dgAssignRequestors_PreRender">
            <MasterTableView DataKeyNames="FleetID,AircraftCD,TailNum,TypeDescription" CommandItemDisplay="None" AllowPaging="false">
                <Columns>
                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" DataTextField="AircraftCD"
                        HeaderStyle-Width="80px" FilterControlWidth="60px" />
                    <%--<telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn" ShowFilterIcon="false" >
                        <ItemTemplate>
                            <asp:CheckBox ID="chkMain" runat="server"  />
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                    </telerik:GridTemplateColumn>--%>
                    <telerik:GridBoundColumn DataField="FleetID"  HeaderText="FleetID" ShowFilterIcon="false" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." ShowFilterIcon="false"
                        CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="TypeDescription" AutoPostBackOnFilter="false" FilterDelay="500"
                         ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderText="Type Description"
                         HeaderStyle-Width="400px" FilterControlWidth="380px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AircraftCD"  HeaderText="Aircraft CD" ShowFilterIcon="false"
                    CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="false" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>                    
                </Columns>
                <CommandItemTemplate>
                   <div style="padding: 5px 5px; text-align: right; clear: both;">
                       
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                        <!--<button id="Cancel" onclick="ConfirmWindowClose(); return false;">
                            Cancel</button>-->
                    </div>
                       
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "RowDblClick" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        
        <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>
