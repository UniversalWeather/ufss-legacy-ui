﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewMemberMultipleSelectionPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewMemberMultipleSelectionPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Crew Member</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgCrewMember.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {

                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%=dgCrewMember.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var cell;
                var CrewCD = ""
                var CrewID = ""
                var HomeBaseCD = ""
                var PhoneNum = ""
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "HomeBaseCD");
                    var cell3 = MasterTable.get_selectedItems()[i].findElement("lbLastName").innerHTML + MasterTable.get_selectedItems()[i].findElement("Label1").innerHTML + MasterTable.get_selectedItems()[i].findElement("lbFirstName").innerHTML + MasterTable.get_selectedItems()[i].findElement("lbInitial").innerHTML;

                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "PhoneNum");
                    var cell5 = MasterTable.getCellByColumnUniqueName(row, "CrewCD");

                    cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewID");
                    cell2 = MasterTable.getCellByColumnUniqueName(row, "HomeBaseCD");
                    cell3 = MasterTable.getCellByColumnUniqueName(row, "PhoneNum");
                    
                    if (i == (selectedRows.length - 1)) {
                        CrewID = CrewID + cell1.innerHTML;
                        CrewCD = CrewCD + cell5.innerHTML;
                    }
                    else {
                        CrewID = CrewID + cell1.innerHTML + ",";
                        CrewCD = CrewCD + cell5.innerHTML + ",";
                    }
                }

                oArg.CrewCD = CrewCD;
                oArg.CrewID = CrewID;

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
                else {
                    alert("Please fill both fields");
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgCrewMember">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewMember" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewMember" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgCrewMember" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="CrewMember_BindData" AutoGenerateColumns="false" Height="330px" 
            PageSize="2000" AllowPaging="false" Width="550px" PagerStyle-AlwaysVisible="false" 
            AllowFilteringByColumn="true">
            <MasterTableView DataKeyNames="CrewID,PhoneNum,HomeBaseID,HomeBaseCD,PhoneNum,LastName,FirstName,MiddleInitial,CrewCD"
                CommandItemDisplay="Bottom" AllowPaging="false" >
                <Columns>
                  <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" DataTextField="CrewCD"
                      HeaderStyle-Width="40px" />

                    <telerik:GridBoundColumn DataField="CrewID" HeaderText="Code" AutoPostBackOnFilter="true"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="CrewCD" HeaderText="CrewCode" AutoPostBackOnFilter="true"
                         ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                         FilterControlWidth="60px">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                        ShowFilterIcon="false" HeaderText="Crew Name" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="180px" FilterControlWidth="160px">
                        <ItemTemplate>
                            <asp:Label ID="lbLastName" runat="server" Text='<%#Eval("LastName")%>'></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text=", "></asp:Label>
                            <asp:Label ID="lbFirstName" runat="server" Text='<%#Eval("FirstName")%>'></asp:Label>
                            <asp:Label ID="lbInitial" runat="server" Text='<%#Eval("MiddleInitial")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="true"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="80px"
                        FilterControlWidth="60px">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone No." AutoPostBackOnFilter="true"
                         ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                         FilterControlWidth="80px">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                </CommandItemTemplate>
                
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
            
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
