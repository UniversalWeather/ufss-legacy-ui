﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="TravelCoordinatorAssignedAircraft.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.TravelCoordinatorAssignedAircraft" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgClientCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <table style="width: 100%;">
        <tr>
            <td class="headerText">
                Assigned Aircraft
            </td>
            <td width="10%" align="center" valign="bottom">
                <asp:Image ID="imghelp" runat="server" ImageUrl="~/App_Themes/Default/images/help.png"
                    Height="16px" Width="16px" />
            </td>
        </tr>
    </table>
    <hr />
    <telerik:RadGrid ID="dgClientCode" runat="server" AllowSorting="true" OnItemCreated="dgClientCode_ItemCreated"
        Visible="true" OnNeedDataSource="dgClientCode_BindData" OnItemCommand="dgClientCode_ItemCommand"
        OnUpdateCommand="dgClientCode_UpdateCommand" OnInsertCommand="dgClientCode_InsertCommand"
        OnDeleteCommand="dgClientCode_DeleteCommand" AutoGenerateColumns="false" Height="330px"
        PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgClientCode_SelectedIndexChanged"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
        <MasterTableView DataKeyNames="Code" CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="CODE" HeaderText="Code" CurrentFilterFunction="Contains">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DESC" HeaderText="Description" CurrentFilterFunction="Contains">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table class="tablebox">
                <tr>
                    <td class="tdLabel100">
                        <b>
                            <asp:Label ID="lbClientCode" runat="server" Text="Code"></asp:Label></b>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        CssClass="text40"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbClientCode"
                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Unique Code Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>
                            <asp:Label ID="lbDescription" runat="server" Text="Description"></asp:Label></b>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="30" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        CssClass="text225"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                        ErrorMessage="Description Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="Save" />
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click"
                            CssClass="button" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
