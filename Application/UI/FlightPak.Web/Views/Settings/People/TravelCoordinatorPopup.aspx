﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TravelCoordinatorPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.TravelCoordinatorPopup" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Associated With PAX</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgTravelCoordinator.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgTravelCoordinator.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "TravelCoordCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "TravelCoordinatorID");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "FirstName");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "BusinessPhone");
                }

                if (selectedRows.length > 0) {
                    oArg.TravelCoordCD = cell1.innerHTML;
                    oArg.TravelCoordID = cell2.innerHTML;
                    oArg.FirstName = cell3.innerHTML;
                    oArg.PhoneNum = cell4.innerHTML;
                }
                else {
                    oArg.TravelCoordCD = "";
                    oArg.TravelCoordID = "";
                    oArg.FirstName = "";
                    oArg.PhoneNum = "";
                }


                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgTravelCoordinator">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTravelCoordinator" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTravelCoordinator" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkHomeBase" runat="server" Text="Home Base Only" />
                                    </span>
                                    <span>
                                    <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgTravelCoordinator" runat="server" AllowSorting="true" Visible="true"
                        Width="800px" OnNeedDataSource="dgTravelCoordinator_BindData" AutoGenerateColumns="false"
                        PageSize="10" AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
                        Height="341px" OnItemCommand="dgTravelCoordinator_ItemCommand" OnPreRender="dgTravelCoordinator_PreRender">
                        <MasterTableView DataKeyNames="TravelCoordinatorID,CustomerID,TravelCoordCD,FirstName,MiddleName,LastName,PhoneNum,FaxNum,PagerNum,CellPhoneNum,EmailID
            ,Notes,HomebaseID,LastUpdUID,LastUpdTS,IsDeleted,HomebaseCD,BaseDescription" CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="TravelCoordinatorID" HeaderText="TravelCoordinatorID"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    Display="false" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TravelCoordCD" HeaderText="Travel Coordinator Code"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FirstName" HeaderText="Description" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="240px"
                                    FilterControlWidth="220px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Phone" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="90px"
                                    FilterControlWidth="70px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FaxNum" HeaderText="Fax" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="90px"
                                    FilterControlWidth="70px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BusinessEmail" HeaderText="Business<br/>E-mail"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="160px" FilterControlWidth="140px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="80px"
                                    FilterControlWidth="60px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents OnRowDblClick="returnToParent"  />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>           
        </table>
    </div>
    </form>
</body>
</html>
