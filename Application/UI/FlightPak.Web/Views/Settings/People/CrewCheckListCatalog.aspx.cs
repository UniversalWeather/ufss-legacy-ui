﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewCheckList : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private List<string> lstCrewCheckListCodes = new List<string>();
        #region constants
        private const string ConstIsScheduleCheck = "IsScheduleCheck";
        private const string ConstIsCrewCurrencyPlanner = "IsCrewCurrencyPlanner";
        #endregion

        private ExceptionManager exManager;
        private bool CrewChecklistPageNavigated = false;
        private string strCrewChecklistID = "";
        private string strCrewChecklistCD = "";
        private bool _selectLastModified = false;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewCheckList, dgCrewCheckList, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewCheckList.ClientID));
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewCrewCheckListCatalogReport);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCrewCheckListCatalog);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCrewCheckList.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                            if (IsPopUp)
                            {
                                if (Request.QueryString["CrewChecklistID"] != null)
                                {
                                    Session["SelectedCrewChecklistID"] = Convert.ToInt64(Request.QueryString["CrewChecklistID"]);
                                }
                                SetPopUpControls();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }


        }

        private void SetPopUpControls()
        {
            dgCrewCheckList.Visible = false;
            chkSearchActiveOnly.Visible = false;
            chkIsScheduleCheck.Visible = false;
            lbtnReports.Visible = false;
            lbtnSaveReports.Visible = false;
            btnShowReports.Visible = false;
            if (IsAdd)
            {
                (dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
            }
            else
            {
                (dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
            }
        }

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (BindDataSwitch)
                {
                    dgCrewCheckList.Rebind();
                }
                if (dgCrewCheckList.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["SelectedCrewChecklistID"] = null;
                    //}
                    if (Session["SelectedCrewChecklistID"] == null)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        Session["SelectedCrewChecklistID"] = dgCrewCheckList.Items[0].GetDataKeyValue("CrewCheckID").ToString();
                    }

                    if (dgCrewCheckList.SelectedIndexes.Count == 0)
                        dgCrewCheckList.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                }
                GridEnable(true, true, true);
            }
        }

        private void SelectItem()
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewChecklistID"] != null)
                    {
                        string ID = Session["SelectedCrewChecklistID"].ToString();
                        foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                        {
                            if (Item["CrewCheckID"].Text.Trim() == ID)
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }


        protected void CrewCheckList_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewChecklistPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewCheckList, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }


        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCrewCheckList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        /// <summary>
        /// Bind Crew Check List Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCrewChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objVen = objCrewChecklistService.GetCrewChecklistList();
                            List<FlightPakMasterService.CrewCheckList> CrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                            if (objVen.ReturnFlag == true)
                            {
                                CrewCheckList = objVen.EntityList;
                            }
                            dgCrewCheckList.DataSource = CrewCheckList;
                            Session["CrewCheckLists"] = CrewCheckList;
                            //Session.Remove("CrewChecklistCodes");
                            lstCrewCheckListCodes = new List<string>();

                            Session["CrewChecklistCodes"] = lstCrewCheckListCodes;
                            if (((chkSearchActiveOnly.Checked == true)) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Item Command for Crew Check List Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedCrewChecklistID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewCheckList, Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewCheckList);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        ChklistItemsToNewCrewMembers();
                                        GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCrewCheckList.SelectedIndexes.Clear();
                               
                                DisplayInsertForm();
                                ChklistItemsToNewCrewMembers();
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCrewChecklistID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objCrewChecklistService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewCheckList objCrewCheckList = new FlightPakMasterService.CrewCheckList();
                                objCrewCheckList.CrewCheckID = Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString());
                                objCrewCheckList.CrewCheckCD = tbCode.Text;
                                if (!string.IsNullOrEmpty(tbDescription.Text))
                                {
                                    objCrewCheckList.CrewChecklistDescription = tbDescription.Text;
                                }
                                else
                                {
                                    objCrewCheckList.CrewChecklistDescription = string.Empty;
                                }                                
                                objCrewCheckList.IsScheduleCheck = chkScheduled.Checked;
                                //objCrewCheckList.IsCrewCurrencyPlanner = chkCrewCurrencyPlanner.Checked;
                                objCrewCheckList.IsInActive = chkInactive.Checked;
                                objCrewCheckList.IsDeleted = false;

                                using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                                {
                                    if (hdnValue.Value == "No")
                                    {
                                         var objRetVal = objService.UpdateCrewChecklist(objCrewCheckList);
                                         if (objRetVal.ReturnFlag == true)
                                         {
                                             e.Item.OwnerTableView.Rebind();
                                             e.Item.Selected = true;
                                             ///////Update Method UnLock
                                             using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                             {
                                                 var returnValue = CommonService.UnLock(EntitySet.Database.CrewCheckList, Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString().Trim()));
                                                 GridEnable(true, true, true);
                                                 SelectItem();
                                                 ReadOnlyForm();
                                             }

                                             ShowSuccessMessage();
                                             _selectLastModified = true;
                                         }
                                         else
                                         {
                                             //For Data Anotation
                                             ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewCheckList);
                                         }
                                    }
                                    else
                                    {
                                        var objRetVal = objService.UpdateChecklistforAllCrewMembers(objCrewCheckList);
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;
                                            ///////Update Method UnLock
                                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                            {
                                                var returnValue = CommonService.UnLock(EntitySet.Database.CrewCheckList, Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString().Trim()));
                                                GridEnable(true, true, true);
                                                SelectItem();
                                                ReadOnlyForm();
                                            }

                                            ShowSuccessMessage();
                                            _selectLastModified = true;
                                        }
                                        else
                                        {
                                            //For Data Anotation
                                            ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewCheckList);
                                        }
                                    }
                                }
                                
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }


        protected void CrewChecklist_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewCheckList.ClientSettings.Scrolling.ScrollTop = "0";
                        CrewChecklistPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        /// <summary>
        /// To check whether the Already the Passenger exists
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    lstCrewCheckListCodes = (List<string>)Session["CrewChecklistCodes"];
                    if (lstCrewCheckListCodes != null && lstCrewCheckListCodes.Equals(tbCode.Text.ToString().Trim()))
                        returnVal = true;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }

        /// <summary>
        /// Update Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                e.Canceled = true;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckAlreadyExist())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Unique Code is Required', 360, 50, 'Crew Checklist');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else
                        {
                            using (MasterCatalogServiceClient CrewCheckListService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CrewCheckList objCrewCheckList = new FlightPakMasterService.CrewCheckList();
                                objCrewCheckList.CrewCheckCD = tbCode.Text;
                                if (!string.IsNullOrEmpty(tbDescription.Text))
                                {
                                    objCrewCheckList.CrewChecklistDescription = tbDescription.Text;
                                }
                                else
                                {
                                    objCrewCheckList.CrewChecklistDescription = string.Empty;
                                }
                                objCrewCheckList.IsScheduleCheck = chkScheduled.Checked;
                                //objCrewCheckList.IsCrewCurrencyPlanner = chkCrewCurrencyPlanner.Checked;
                                objCrewCheckList.IsInActive = chkInactive.Checked;
                                objCrewCheckList.IsDeleted = false;

                                using (MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                                {
                                    if (hdnValue.Value == "No")
                                    {
                                        var objRetVal = objService.AddCrewChecklist(objCrewCheckList);
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            DefaultSelection(false);
                                            dgCrewCheckList.Rebind();

                                            ShowSuccessMessage();
                                            _selectLastModified = true;
                                        }
                                        else
                                        {
                                            //For Data Anotation
                                            ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewCheckList);
                                        }
                                    }
                                    else
                                    {
                                        var objRetVal = objService.AddChecklistforAllCrewMembers(objCrewCheckList);
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            DefaultSelection(false);
                                            dgCrewCheckList.Rebind();

                                            ShowSuccessMessage();
                                            _selectLastModified = true;
                                        }
                                        else
                                        {
                                            //For Data Anotation
                                            ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewCheckList);
                                        }
                                    }
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                e.Canceled = true;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCrewChecklistID"] != null)
                        {
                            if (Session["SelectedCrewChecklistID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CrewCheckListService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.CrewCheckList CrewCheckList = new FlightPakMasterService.CrewCheckList();
                                    string Code = Session["SelectedCrewChecklistID"].ToString();
                                    strCrewChecklistCD = "";
                                    strCrewChecklistID = "";
                                    foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                                    {
                                        if (Item["CrewCheckID"].Text.Trim() == Code.Trim())
                                        {
                                            strCrewChecklistCD = Item["CrewCheckCD"].Text.Trim();
                                            strCrewChecklistID = Item["CrewCheckID"].Text.Trim();
                                            CrewCheckList.CrewChecklistDescription = Item.GetDataKeyValue("CrewChecklistDescription").ToString();
                                            CrewCheckList.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                            break;
                                        }
                                    }
                                    CrewCheckList.CrewCheckCD = strCrewChecklistCD;
                                    CrewCheckList.CrewCheckID = Convert.ToInt64(strCrewChecklistID);
                                    CrewCheckList.IsDeleted = true;
                                    //Lock will happen from UI
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewCheckList, Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            DefaultSelection(false);
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewCheckList);
                                            return;
                                        }
                                    }
                                    CrewCheckListService.DeleteCrewChecklist(CrewCheckList);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgCrewCheckList.Rebind();
                                    DefaultSelection(false);
                                    GridEnable(true, true, true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("CrewCheckID") != null)
                                {
                                    Session["SelectedCrewChecklistID"] = Item.GetDataKeyValue("CrewCheckID").ToString();
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                    }
                }
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            if (Session["CheckListSetting"] != null)
                            {
                                Session.Remove("CheckListSetting");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedCrewChecklistID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CrewCheckList, Convert.ToInt64(Session["SelectedCrewChecklistID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                        DefaultSelection(false);
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                   
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddCrewCheckListCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCrewCheckListCatalog))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete('Warning, Deleting this crew check list code will delete the code from all crew members. Continue Deleting?');";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCrewCheckListCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            if (CheckAlreadyExist())
                            {
                                cvCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbDescription.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }

        }

        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetCrewChecklistList().EntityList.Where(x => x.CrewCheckCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            returnVal = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }

        /// <summary>
        /// Read only form
        /// </summary>

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                   
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewChecklistID"] != null)
                    {
                        strCrewChecklistID = "";
                        foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                        {
                            if (Item["CrewCheckID"].Text.Trim() == Session["SelectedCrewChecklistID"].ToString().Trim())
                            {
                                tbCodeId.Value = Item.GetDataKeyValue("CrewCheckID").ToString();
                                tbCode.Text = Item.GetDataKeyValue("CrewCheckCD").ToString();
                                if (Item.GetDataKeyValue("CrewChecklistDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("CrewChecklistDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue(ConstIsScheduleCheck) != null)
                                {
                                    chkScheduled.Checked = Convert.ToBoolean(Item.GetDataKeyValue(ConstIsScheduleCheck).ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkScheduled.Checked = false;
                                }
                                //if (Item.GetDataKeyValue(ConstIsCrewCurrencyPlanner) != null)
                                //{
                                //    chkCrewCurrencyPlanner.Checked = Convert.ToBoolean(Item.GetDataKeyValue(ConstIsCrewCurrencyPlanner).ToString(), CultureInfo.CurrentCulture);
                                //}
                                //else
                                //{
                                //    chkCrewCurrencyPlanner.Checked = false;
                                //}
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                Label lbLastUpdatedUser = (Label)dgCrewCheckList.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                lbColumnName1.Text = "Checklist Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["CrewCheckCD"].Text;
                                lbColumnValue2.Text = Item["CrewChecklistDescription"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }


        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewCheckList);
                }
            }
        }
        #endregion

        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbCodeId.Value = string.Empty;
                    //chkCrewCurrencyPlanner.Checked = false;
                    chkScheduled.Checked = false;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (enable == false)
                    //{
                    //    tbCode.ReadOnly = false;
                    //    tbCode.BackColor = System.Drawing.Color.White;
                    //}
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    //chkCrewCurrencyPlanner.Enabled = enable;
                    chkScheduled.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;                    
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.CrewCheckList> lstCrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                if (Session["CrewCheckLists"] != null)
                {
                    lstCrewCheckList = (List<FlightPakMasterService.CrewCheckList>)Session["CrewCheckLists"];
                }
                if (lstCrewCheckList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCrewCheckList = lstCrewCheckList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.CrewCheckList>(); }
                    if (chkIsScheduleCheck.Checked == true) { lstCrewCheckList = lstCrewCheckList.Where(x => x.IsScheduleCheck == true).ToList<FlightPakMasterService.CrewCheckList>(); }
                    
                    dgCrewCheckList.DataSource = lstCrewCheckList;
                    if (IsDataBind)
                    {
                        dgCrewCheckList.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCrewCheckList.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var CrewChecklistValue = FPKMstService.GetCrewChecklistList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CrewChecklistValue);
            List<FlightPakMasterService.CrewCheckList> filteredList = GetFilteredList(CrewChecklistValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CrewCheckID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCrewCheckList.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewCheckList.CurrentPageIndex = PageNumber;
            dgCrewCheckList.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfCrewCheckList CrewChecklistValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCrewChecklistID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = CrewChecklistValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().CrewCheckID;
                Session["SelectedCrewChecklistID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.CrewCheckList> GetFilteredList(ReturnValueOfCrewCheckList CrewChecklistValue)
        {
            List<FlightPakMasterService.CrewCheckList> filteredList = new List<FlightPakMasterService.CrewCheckList>();

            if (CrewChecklistValue.ReturnFlag)
            {
                filteredList = CrewChecklistValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.CrewCheckList>(); }
                    if (chkIsScheduleCheck.Checked) { filteredList = filteredList.Where(x => x.IsScheduleCheck == true).ToList<FlightPakMasterService.CrewCheckList>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewCheckList.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCrewCheckList.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        private void ChklistItemsToNewCrewMembers()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Int64 Code = 0;
                        Code = Convert.ToInt64(UserPrincipal.Identity._homeBaseId);
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(Code).EntityList.ToList();
                            if (CompanyList.Count > 0)
                            {
                                GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                isChkAllCrew.Value = Convert.ToString(CompanyCatalogEntity.IsChecklistAssign.ToString().Trim(), CultureInfo.CurrentCulture);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
    }
}