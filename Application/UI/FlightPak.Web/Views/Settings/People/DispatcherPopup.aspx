﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Dispatcher</title>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <style type="text/css">
        body {
            width: auto;
            height: auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="jqgrid dispatcher_popup">
            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <table id="gridDispacherPopup" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pager_gridDispacherPopup"></div>   
                                <span class="Span">Page Size:</span>
                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize($(jqgridTableId), $('#rowNum')); return false;" />
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </form>

    <script type="text/javascript">
        var oArg = new Object();
        var jqgridTableId = '#gridDispacherPopup';
        var selectedUserName;
        $(document).ready(function () {
            selectedUserName = $.trim(unescape(getQuerystring("UserName", "")));

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sidx: "sort"
                }
            });

            $("#btnSubmit").click(function (e) {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr); 
                if (selr == null) {
                    showMessageBox('Please select a dispatcher.', popupTitle);
                }
                else {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData);
                }
                return false;
            });

            callGridFillMethod();

        });

        function returnToParent(rowData) {
            var oArg = new Object();
            if (rowData != undefined) {
                 oArg.UserName = rowData.UserName;
                 oArg.FullName = rowData.FirstName;
                 oArg.PhoneNum = rowData.PhoneNum;
            }
            else {
                 oArg.UserName = "";
                 oArg.FullName = "";
                 oArg.PhoneNum = "";
            }
            var oWnd = GetRadWindow();
            if (!oArg.UserName)
                oArg.UserName = '';
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        function callGridFillMethod() {
            jQuery("#gridDispacherPopup").jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.sendCustomerId = true;
                    postData.apiType = 'fss';
                    postData.method = 'UserMasterList';
                    return postData;
                },
                height: 400,
                width: 600,
                viewrecords: true,
                rowNum: 20,
                multiselect: false,
                loadonce: true,
                ignoreCase: true,
                pager: "#pager_gridDispacherPopup",
                colNames: ['', 'Username', 'Full Name', '', '', '', '', ''],
                colModel: [
                     {
                         name: 'FirstName', index: 'FirstName', align: "left", sortable: false, hidden: true
                     },
                    {
                        name: 'UserName', index: 'UserName', width: 70, align: "left", key: true
                    },
                    {
                        name: 'FullName', index: 'FullName', align: "left"
                    },

                     {
                         name: 'MiddleName', index: 'MiddleName', align: "left", sortable: false, hidden: true
                     },
                      {
                          name: 'LastName', index: 'LastName', align: "left", sortable: false, hidden: true
                      },
                      {
                          name: 'PhoneNum', index: 'PhoneNum', align: "left", sortable: false, hidden: true
                      },
                      {
                          name: 'IsDispatcher', index: 'IsDispatcher', align: "left", sortable: false, hidden: true
                      },
                    {
                        name: 'CrewDutyRulesID', index: 'CrewDutyRulesID', hidden: true
                    }
                ],

                ondblClickRow: function (rowid) {
                    var rowData = $(jqgridTableId).jqGrid("getRowData", rowid);
                    returnToParent(rowData);
                    return;
                },
                onSelectRow: function (rowid, status, e) {
                    var rowData = $(jqgridTableId).jqGrid("getRowData", rowid);
                    var lastSel = rowData['UserName'];//replace name with any column
                    if (rowid !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $('#results_table').jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = rowid;
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    var mname = rowelem.MiddleName == null ? "" : rowelem.MiddleName;
                    var val = rowelem.LastName + " " + rowelem.FirstName + " " + mname;
                    $('#gridDispacherPopup').setCell(rowid, 'FullName', val, '');
                },
                loadComplete: function () {
                    

                    var i, count, $grid = $("#gridDispacherPopup");
                    var rowArray = $("#gridDispacherPopup").jqGrid('getDataIDs');
                    for (i = 0, count = rowArray.length; i < count; i += 1) {
                        if ($.trim(rowArray[i]) == $.trim(selectedUserName)) {
                            $grid.jqGrid('setSelection', rowArray[i], true);
                            break;
                        }
                    }
                }
            });

            
            $("#gridDispacherPopup").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        }
    </script>
</body>
</html>
