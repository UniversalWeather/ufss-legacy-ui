﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using System.Data;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.People
{
    public partial class TravelCoordinator : BaseSecuredPage    // System.Web.UI.Page
    {
        private bool IsEmptyCheck = true;
        ArrayList PaxGroupArray = new ArrayList();
        Hashtable htSelectedList;
        private ExceptionManager exManager;
        private bool TravelCoordinatorPageNavigated = false;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTravelCoordinator, dgTravelCoordinator, RadAjaxLoadingPanel1);
                        //// Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgTravelCoordinator.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewTravelCoordinator);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgTravelCoordinator.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkHomeBase.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgTravelCoordinator.Rebind();
                    }
                    if (dgTravelCoordinator.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["TravelCoordinatorID"] = null;
                        //}
                        if (Session["TravelCoordinatorID"] == null)
                        {
                            dgTravelCoordinator.SelectedIndexes.Add(0);
                            Session["TravelCoordinatorID"] = dgTravelCoordinator.Items[0].GetDataKeyValue("TravelCoordinatorID").ToString();
                        }

                        if (dgTravelCoordinator.SelectedIndexes.Count == 0)
                            dgTravelCoordinator.SelectedIndexes.Add(0);

                        SelectItem();
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["TravelCoordinatorID"] != null)
                    {
                        string ID = Session["TravelCoordinatorID"].ToString();
                        foreach (GridDataItem Item in dgTravelCoordinator.MasterTableView.Items)
                        {
                            if (Item["TravelCoordinatorID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl = (LinkButton)dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton delCtl = (LinkButton)dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton editCtl = (LinkButton)dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddTravelCoordinator))
                    {
                        insertCtl.Visible = true;
                        if (Add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteTravelCoordinator))
                    {
                        delCtl.Visible = true;
                        if (Delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditTravelCoordinator))
                    {
                        editCtl.Visible = true;
                        if (Edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    //To select Default value of Homebase. 
                    if (UserPrincipal.Identity._homeBaseId != null)
                    {
                        tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd;
                        hdnHomeBaseID.Value = UserPrincipal.Identity._homeBaseId.ToString();
                    }
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>  
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    LoadControlData();
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbLastName.Enabled = enable;
                    tbPhone.Enabled = enable;
                    tbPager.Enabled = enable;
                    tbMobile.Enabled = enable;
                    tbFax.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    lstRequestorAvailable.Enabled = enable;
                    lstAircraftAvailable.Enabled = enable;
                    tbNotes.Enabled = enable;
                    tbBusinessPhone.Enabled = enable;
                    tbHomeFax.Enabled = enable;
                    tbCellPhoneNum2.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbBusinessEmail.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherEmail.Enabled = enable;
                    Button1.Enabled = enable;
                    Button2.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel1.Visible = enable;
                    btnSaveChanges1.Visible = enable;
                    //chkHomeBase.Enabled = !(enable);
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnTravelCoordinatorID.Value = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbPager.Text = string.Empty;
                    tbMobile.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBaseID.Value = string.Empty;
                    tbNotes.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    lstRequestorAvailable.Items.Clear();
                    lstAircraftAvailable.Items.Clear();
                    lstRequestorAvailable.Enabled = true;
                    lstAircraftAvailable.Enabled = true;
                    Session.Remove("lstPax");
                    Session.Remove("lstFleet");
                    Session.Remove("SelectedPassengerList");
                    Session.Remove("SelectedFleetList");
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TravelCoordinatorID"] != null)
                        {
                            foreach (GridDataItem Item in dgTravelCoordinator.MasterTableView.Items)
                            {
                                if (Item["TravelCoordinatorID"].Text.Trim() == Session["TravelCoordinatorID"].ToString().Trim())
                                {
                                    hdnTravelCoordinatorID.Value = Item.GetDataKeyValue("TravelCoordinatorID").ToString().Trim();
                                    tbCode.Text = Item.GetDataKeyValue("TravelCoordCD").ToString().Trim();
                                    if (Item.GetDataKeyValue("FirstName") != null)
                                    {
                                        tbFirstName.Text = Item.GetDataKeyValue("FirstName").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbFirstName.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("MiddleName") != null)
                                    {
                                        tbMiddleName.Text = Item.GetDataKeyValue("MiddleName").ToString();
                                    }
                                    else
                                    {
                                        tbMiddleName.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("LastName") != null)
                                    {
                                        tbLastName.Text = Item.GetDataKeyValue("LastName").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbLastName.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("PhoneNum") != null)
                                    {
                                        tbPhone.Text = Item.GetDataKeyValue("PhoneNum").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbPhone.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("FaxNum") != null)
                                    {
                                        tbFax.Text = Item.GetDataKeyValue("FaxNum").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbFax.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("PagerNum") != null)
                                    {
                                        tbPager.Text = Item.GetDataKeyValue("PagerNum").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbPager.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                    {
                                        tbMobile.Text = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbMobile.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("EmailID") != null)
                                    {
                                        tbEmail.Text = Item.GetDataKeyValue("EmailID").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbEmail.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("HomebaseID") != null)
                                    {
                                        tbHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString();
                                        hdnHomeBaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString();
                                    }
                                    else
                                    {
                                        tbHomeBase.Text = string.Empty;
                                        hdnHomeBaseID.Value = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("Notes") != null)
                                    {
                                        tbNotes.Text = Item.GetDataKeyValue("Notes").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbNotes.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("BusinessPhone") != null)
                                    {
                                        tbBusinessPhone.Text = Item.GetDataKeyValue("BusinessPhone").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbBusinessPhone.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("HomeFax") != null)
                                    {
                                        tbHomeFax.Text = Item.GetDataKeyValue("HomeFax").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbHomeFax.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("CellPhoneNum2") != null)
                                    {
                                        tbCellPhoneNum2.Text = Item.GetDataKeyValue("CellPhoneNum2").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbCellPhoneNum2.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("OtherPhone") != null)
                                    {
                                        tbOtherPhone.Text = Item.GetDataKeyValue("OtherPhone").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbOtherPhone.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("BusinessEmail") != null)
                                    {
                                        tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbBusinessEmail.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("PersonalEmail") != null)
                                    {
                                        tbPersonalEmail.Text = Item.GetDataKeyValue("PersonalEmail").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbPersonalEmail.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("OtherEmail") != null)
                                    {
                                        tbOtherEmail.Text = Item.GetDataKeyValue("OtherEmail").ToString().Trim();
                                    }
                                    else
                                    {
                                        tbOtherEmail.Text = string.Empty;
                                    }
                                    Label lbLastUpdatedUser;
                                    lbLastUpdatedUser = (Label)dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                                    {
                                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                    }
                                    else
                                    {
                                        lbLastUpdatedUser.Text = string.Empty;
                                    }
                                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                                    {
                                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));// item.GetDataKeyValue("LastUpdTS").ToString();
                                    }
                                    if (Item.GetDataKeyValue("IsInActive") != null)
                                    {
                                        chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                    }
                                    else
                                    {
                                        chkInactive.Checked = false;
                                    }
                                    lbColumnName1.Text = "Travel Coordinator Code";
                                    lbColumnName2.Text = "Name";
                                    lbColumnValue1.Text = Item["TravelCoordCD"].Text;
                                    lbColumnValue2.Text = Item["DisplayName"].Text;
                                    break;
                                }
                            }
                        }
                        LoadListFleet();
                        LoadListRequester();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.TravelCoordinator GetItems(FlightPakMasterService.TravelCoordinator objTravelCoordinator)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objTravelCoordinator))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        objTravelCoordinator.TravelCoordinatorID = Convert.ToInt64(hdnTravelCoordinatorID.Value);
                    }
                    objTravelCoordinator.TravelCoordCD = tbCode.Text.Trim();
                    objTravelCoordinator.FirstName = tbFirstName.Text.Trim();
                    if (!string.IsNullOrEmpty(tbMiddleName.Text))
                    {
                        objTravelCoordinator.MiddleName = tbMiddleName.Text.Trim();
                    }
                    objTravelCoordinator.LastName = tbLastName.Text.Trim();
                    int PhoneCount = tbPhone.Text.IndexOf("+");
                    if (!string.IsNullOrEmpty(tbPhone.Text))
                    {
                        objTravelCoordinator.PhoneNum = tbPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbMobile.Text))
                    {
                        objTravelCoordinator.CellPhoneNum = tbMobile.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFax.Text))
                    {
                        objTravelCoordinator.FaxNum = tbFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFax.Text))
                    {
                        objTravelCoordinator.PagerNum = tbPager.Text.Trim();
                    }
                    if (PhoneCount >= 0)
                    {
                        if (tbPhone.Text != null)
                        {
                            string PhoneNumber = tbPhone.Text.Substring(0, tbPhone.Text.IndexOf("+")) + tbPhone.Text;
                            objTravelCoordinator.PhoneNum = PhoneNumber;
                        }
                        else
                        {
                            objTravelCoordinator.PhoneNum = string.Empty;
                        }
                    }
                    int MobileCount = tbMobile.Text.IndexOf("+");
                    if (MobileCount >= 0)
                    {
                        if (tbMobile.Text != null)
                        {
                            string mobileNumber = tbMobile.Text.Substring(0, tbMobile.Text.IndexOf("+")) + tbMobile.Text;
                            objTravelCoordinator.CellPhoneNum = mobileNumber;
                        }
                        else
                        {
                            objTravelCoordinator.CellPhoneNum = string.Empty;
                        }
                    }
                    int FaxCount = tbFax.Text.IndexOf("+");
                    if (FaxCount >= 0)
                    {
                        if (tbFax.Text != null)
                        {
                            string faxNumber = tbFax.Text.Substring(0, tbFax.Text.IndexOf("+")) + tbFax.Text;
                            objTravelCoordinator.FaxNum = faxNumber;
                        }
                        else
                        {
                            objTravelCoordinator.FaxNum = string.Empty;
                        }
                    }
                    int PagerCount = tbPager.Text.IndexOf("+");
                    if (PagerCount >= 0)
                    {
                        if (tbPager.Text != null)
                        {
                            string PagerNumber = tbPager.Text.Substring(0, tbPager.Text.IndexOf("+")) + tbPager.Text;
                            objTravelCoordinator.PagerNum = PagerNumber;
                        }
                        else
                        {
                            objTravelCoordinator.PagerNum = string.Empty;
                        }
                    }
                    if (!string.IsNullOrEmpty(tbEmail.Text))
                    {
                        objTravelCoordinator.EmailID = tbEmail.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        objTravelCoordinator.Notes = tbNotes.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(hdnHomeBaseID.Value))
                    {
                        objTravelCoordinator.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value); //tbHomeBase.Text;
                    }
                    objTravelCoordinator.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbBusinessPhone.Text))
                    {
                        objTravelCoordinator.BusinessPhone = tbBusinessPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbHomeFax.Text))
                    {
                        objTravelCoordinator.HomeFax = tbHomeFax.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum2.Text))
                    {
                        objTravelCoordinator.CellPhoneNum2 = tbCellPhoneNum2.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                    {
                        objTravelCoordinator.OtherPhone = tbOtherPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBusinessEmail.Text))
                    {
                        objTravelCoordinator.BusinessEmail = tbBusinessEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                    {
                        objTravelCoordinator.PersonalEmail = tbPersonalEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                    {
                        objTravelCoordinator.OtherEmail = tbOtherEmail.Text;
                    }
                    objTravelCoordinator.IsInActive = chkInactive.Checked;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objTravelCoordinator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void LoadListFleet()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Int64 travelCoordID = Convert.ToInt64(hdnTravelCoordinatorID.Value);
                    List<FlightPakMasterService.TravelCoordinatorFleet> fleetAvailableList = GetTravelFleetSelectedList(travelCoordID);
                    List<object> TempAvlList = new List<object>();
                    htSelectedList = new Hashtable();
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPakMasterService.Fleet> lstFleetInfo = FleetService.GetFleetProfileList().EntityList;
                        List<FlightPakMasterService.Fleet> lstFleet = new List<FlightPakMasterService.Fleet>();
                        lstFleet = (from AvailableList in fleetAvailableList
                                    from fleetList in lstFleetInfo
                                    where fleetList.FleetID == AvailableList.FleetID && AvailableList.IsDeleted == false
                                    orderby fleetList.TailNum ascending
                                    select fleetList).ToList<FlightPakMasterService.Fleet>();
                        lstAircraftAvailable.Items.Clear();
                        RadListBoxItem lstItem;
                        foreach (FlightPakMasterService.Fleet FleetAvlList in lstFleet)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = FleetAvlList.TailNum.ToString();
                            lstItem.Value = FleetAvlList.FleetID.ToString();
                            lstAircraftAvailable.Items.Add(lstItem);
                        }
                        if (lstAircraftAvailable.Items.Count != 0)
                        {
                            foreach (FlightPakMasterService.TravelCoordinatorFleet FleetAvlList in fleetAvailableList)
                            {
                                htSelectedList.Add(FleetAvlList.FleetID.ToString(), FleetAvlList.TravelCoordinatorFleetID.ToString());
                                PaxGroupArray.Add(FleetAvlList.FleetID);
                                TempAvlList.Add(FleetAvlList.FleetID.ToString().Trim());
                                if (Convert.ToBoolean(FleetAvlList.IsChoice) == true)
                                {
                                    lstAircraftAvailable.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(FleetAvlList.FleetID.ToString().Trim())).Selected = true;
                                }
                            }
                        }
                        Session["SelectedFleetList"] = htSelectedList;
                        FleetSession();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void LoadListRequester()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Int64 TravelCoordinatorID = Convert.ToInt64(hdnTravelCoordinatorID.Value);
                    List<FlightPakMasterService.TravelCoordinatorRequestor> RequestorAvailableList = GetTravelPaxAvailableListByCode(TravelCoordinatorID);
                    List<object> TempAvlList = new List<object>();
                    htSelectedList = new Hashtable();
                    using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPakMasterService.Passenger> lstPassengerInfo = PaxService.GetPassengerRequestorList().EntityList;
                        List<FlightPakMasterService.Passenger> lstPassenger = new List<FlightPakMasterService.Passenger>();
                        lstPassenger = (from AvailableList in RequestorAvailableList
                                        from PaxList in lstPassengerInfo
                                        where PaxList.PassengerRequestorID == AvailableList.PassengerRequestorID && AvailableList.IsDeleted == false
                                        orderby PaxList.PassengerRequestorCD ascending
                                        select PaxList).ToList<FlightPakMasterService.Passenger>();
                        lstRequestorAvailable.Items.Clear();
                        RadListBoxItem lstItem;
                        foreach (FlightPakMasterService.Passenger RequestorAvlList in lstPassenger)
                        {
                            lstItem = new RadListBoxItem();
                            lstItem.Text = RequestorAvlList.PassengerRequestorCD.ToString();
                            lstItem.Value = RequestorAvlList.PassengerRequestorID.ToString();
                            lstRequestorAvailable.Items.Add(lstItem);
                        }
                        if (lstRequestorAvailable.Items.Count != 0)
                        {
                            foreach (FlightPakMasterService.TravelCoordinatorRequestor RequestorAvlList in RequestorAvailableList)
                            {
                                htSelectedList.Add(RequestorAvlList.PassengerRequestorID.ToString(), RequestorAvlList.TravelCoordinatorRequestorID.ToString());
                                PaxGroupArray.Add(RequestorAvlList.PassengerRequestorID);
                                TempAvlList.Add(RequestorAvlList.PassengerRequestorID.ToString().Trim());
                                if (Convert.ToBoolean(RequestorAvlList.IsChoice) == true && lstRequestorAvailable.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(RequestorAvlList.PassengerRequestorID.ToString())) != null)
                                {
                                    lstRequestorAvailable.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(RequestorAvlList.PassengerRequestorID.ToString().Trim())).Selected = true;
                                }
                            }
                        }
                        Session["SelectedPassengerList"] = htSelectedList;
                        RequestorSession();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "Grid events"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Bind Delay Type Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ReturnValue = TravelCoordinatorService.GetTravelCoordinatorList();
                            List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                            if (ReturnValue.ReturnFlag == true)
                            {
                                TravelCoordinatorList = ReturnValue.EntityList;
                            }
                            dgTravelCoordinator.DataSource = TravelCoordinatorList;
                            Session["TravelCoordinatorList"] = TravelCoordinatorList;

                            if (!IsPopUp)
                                DoSearchFilter();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Item Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                // Lock the record
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.TravelCoordinator, Convert.ToInt64(Session["TravelCoordinatorID"]));
                                    Session["IsEditLockTravelCoordinator"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TravelCoordinator);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    DisableLinks();
                                    RadAjaxManager1.FocusControl(tbFirstName.ClientID); //tbFirstName.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgTravelCoordinator.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                DisableLinks();
                                break;
                            case "UpdateEdited":
                                dgTravelCoordinator_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["TravelCoordinatorID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.TravelCoordinator objTravelCoordinator = new FlightPakMasterService.TravelCoordinator();
                                objTravelCoordinator = GetItems(objTravelCoordinator);
                                objTravelCoordinator = GetRequestorItems(objTravelCoordinator);
                                objTravelCoordinator = GetFleetItems(objTravelCoordinator);
                                var Result = TravelCoordinatorService.UpdateTravelCoordinatorMaster(objTravelCoordinator);
                                if (Result.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.TravelCoordinator, Convert.ToInt64(Session["TravelCoordinatorID"]));
                                        Session["IsEditLockTravelCoordinator"] = "False";
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        dgTravelCoordinator.Rebind();
                                        SelectItem();
                                        ReadOnlyForm();
                                    }

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.TravelCoordinator);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Travel Coordinator Code Must Be Unique', 360, 50, 'TravelCoordinator');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            cvAircraftCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.TravelCoordinator objTravelCoordinator = new FlightPakMasterService.TravelCoordinator();
                                objTravelCoordinator = GetItems(objTravelCoordinator);
                                objTravelCoordinator = GetRequestorItems(objTravelCoordinator);
                                objTravelCoordinator = GetFleetItems(objTravelCoordinator);
                                var Result = TravelCoordinatorService.AddTravelCoordinatorMaster(objTravelCoordinator);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    dgTravelCoordinator.Rebind();
                                    GridEnable(true, true, true);
                                    DefaultSelection(false);
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.TravelCoordinator);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (Session["TravelCoordinatorID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.TravelCoordinator TravelCoordinatorService = new FlightPakMasterService.TravelCoordinator();
                                    FlightPakMasterService.TravelCoordinatorFleet TravelCoordinatorFleetService = new FlightPakMasterService.TravelCoordinatorFleet();
                                    FlightPakMasterService.TravelCoordinatorRequestor TravelCoordinatorRequestorService = new FlightPakMasterService.TravelCoordinatorRequestor();
                                    GridDataItem Item = dgTravelCoordinator.SelectedItems[0] as GridDataItem;
                                    if (Item.GetDataKeyValue("TravelCoordinatorID") != null)
                                    {
                                        TravelCoordinatorService.TravelCoordinatorID = Convert.ToInt64(Item.GetDataKeyValue("TravelCoordinatorID").ToString().Trim());
                                        TravelCoordinatorFleetService.TravelCoordinatorID = Convert.ToInt64(Item.GetDataKeyValue("TravelCoordinatorID").ToString().Trim());
                                        TravelCoordinatorRequestorService.TravelCoordinatorID = Convert.ToInt64(Item.GetDataKeyValue("TravelCoordinatorID").ToString().Trim());
                                    }
                                    if (Item.GetDataKeyValue("TravelCoordCD") != null)
                                    {
                                        TravelCoordinatorService.TravelCoordCD = Item.GetDataKeyValue("TravelCoordCD").ToString().Trim();
                                        //TravelCoordinatorFleetService.TravelCoordCD = Item.GetDataKeyValue("TravelCoordCD").ToString().Trim();
                                        //TravelCoordinatorRequestorService.TravelCoordCD = Item.GetDataKeyValue("TravelCoordCD").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("FirstName") != null)
                                    {
                                        TravelCoordinatorService.FirstName = Item.GetDataKeyValue("FirstName").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("MiddleName") != null)
                                    {
                                        TravelCoordinatorService.MiddleName = Item.GetDataKeyValue("MiddleName").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("LastName") != null)
                                    {
                                        TravelCoordinatorService.LastName = Item.GetDataKeyValue("LastName").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("PhoneNum") != null)
                                    {
                                        TravelCoordinatorService.PhoneNum = Item.GetDataKeyValue("PhoneNum").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("FaxNum") != null)
                                    {
                                        TravelCoordinatorService.FaxNum = Item.GetDataKeyValue("FaxNum").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("PagerNum") != null)
                                    {
                                        TravelCoordinatorService.PagerNum = Item.GetDataKeyValue("PagerNum").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                    {
                                        TravelCoordinatorService.CellPhoneNum = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("EmailID") != null)
                                    {
                                        TravelCoordinatorService.EmailID = Item.GetDataKeyValue("EmailID").ToString().Trim();
                                    }
                                    TravelCoordinatorService.IsDeleted = true;
                                    TravelCoordinatorFleetService.IsDeleted = true;
                                    TravelCoordinatorRequestorService.IsDeleted = true;
                                    //TravelCoordinatorRequestorService.PassengerRequestorID = Convert.ToInt64(lstRequestorAvailable.SelectedItem.Value);
                                    //TravelCoordinatorFleetService.FleetID = Convert.ToInt64(lstAircraftAvailable.SelectedItem.Value);
                                    //Lock the record
                                    var returnValue = CommonService.Lock(EntitySet.Database.TravelCoordinator, Convert.ToInt64(Session["TravelCoordinatorID"]));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.TravelCoordinator);
                                        return;
                                    }
                                    MasterService.DeleteTravelCoordinatorMaster(TravelCoordinatorService);
                                    MasterService.DeleteTravelCoordinatorFleet(TravelCoordinatorFleetService);
                                    MasterService.DeleteTravelCoordinatorRequestor(TravelCoordinatorRequestorService);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.TravelCoordinator, Convert.ToInt64(Session["TravelCoordinatorID"]));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTravelCoordinator_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if ((btnSaveChanges.Visible == false) && (btnSaveChanges1.Visible == false))
                            {
                                if (Session["TravelCoordinatorID"] != null)
                                {
                                    GridDataItem Item = dgTravelCoordinator.SelectedItems[0] as GridDataItem;
                                    Session["TravelCoordinatorID"] = Item["TravelCoordinatorID"].Text;
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                    }
                }
            }
        }
        protected void dgTravelCoordinator_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (TravelCoordinatorPageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTravelCoordinator, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerInformation);
                }
            }
        }
        protected void dgTravelCoordinator_OnPageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgTravelCoordinator.ClientSettings.Scrolling.ScrollTop = "0";
                        TravelCoordinatorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        #endregion
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgTravelCoordinator;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info
                                if (Session["newPax"] != null)
                                {
                                    // Declaring Lists
                                    List<Passenger> FinalList = new List<Passenger>();
                                    List<Passenger> RetainList = new List<Passenger>();
                                    // Retain Grid Items and bind into List and add into Final List
                                    lstRequestorAvailable.Items.Clear(); //clearing list for refreshing the selected items
                                    RetainList = (RetainRequestorList(lstRequestorAvailable));
                                    FinalList.AddRange(RetainList);
                                    List<Passenger> NewList = (List<Passenger>)Session["newPax"];
                                    FinalList.AddRange(NewList);
                                    Session["lstPax"] = FinalList;
                                    for (int ListItem = 0; ListItem < NewList.Count; ListItem++)
                                    {
                                        lstRequestorAvailable.Items.Add(new RadListBoxItem(NewList[ListItem].PassengerRequestorCD, NewList[ListItem].PassengerRequestorID.ToString()));
                                    }
                                    Session.Remove("newPax");
                                }
                                if (Session["newfleet"] != null)
                                {
                                    List<FlightPakMasterService.Fleet> FinalList = new List<FlightPakMasterService.Fleet>();
                                    List<FlightPakMasterService.Fleet> RetainList = new List<FlightPakMasterService.Fleet>();
                                    List<FlightPakMasterService.Fleet> NewList = new List<FlightPakMasterService.Fleet>();
                                    lstAircraftAvailable.Items.Clear(); //clearing list for refreshing the selected items
                                    RetainList = (RetainFleetList(lstAircraftAvailable));
                                    FinalList.AddRange(RetainList);
                                    NewList = (List<FlightPakMasterService.Fleet>)Session["newfleet"];
                                    FinalList.AddRange(NewList);
                                    Session["lstFleet"] = FinalList;
                                    for (int ListItem = 0; ListItem < NewList.Count; ListItem++)
                                    {
                                        lstAircraftAvailable.Items.Add(new RadListBoxItem(NewList[ListItem].TailNum, NewList[ListItem].FleetID.ToString()));
                                    }
                                    Session.Remove("newfleet");
                                }
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Delay Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgTravelCoordinator.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Cancel Delay Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.TravelCoordinator, Convert.ToInt64(Session["TravelCoordinatorID"]));
                            //Session.Remove("TravelCoordinatorID");
                            DefaultSelection(false);
                            EnableLinks();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void list_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(o, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ReturnValue = TravelCoordinatorService.GetTravelCoordinatorMaster();
                            if (ReturnValue.ReturnFlag == true)
                            {
                                ((RadComboBox)o).DataSource = (from TM in ReturnValue.EntityList
                                                               where ((((RadComboBox)o).DataTextField == "TravelCoordCD" && TM.TravelCoordCD.Contains(e.Text))
                                                               ||
                                                               (((RadComboBox)o).DataTextField == "FirstName" && TM.FirstName.ToLower().Contains(e.Text.ToLower()))
                                                               ||
                                                               (((RadComboBox)o).DataTextField == "PhoneNum" && TM.PhoneNum.Contains(e.Text))
                                                               ||
                                                               (((RadComboBox)o).DataTextField == "FaxNum" && TM.FaxNum.Contains(e.Text)))
                                                               select TM).ToList();
                                ((RadComboBox)o).DataBind();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>        
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAllReadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (tbCode.Text != string.Empty)
                    {
                        List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                        TravelCoordinatorList = ((List<GetTravelCoordinator>)Session["TravelCoordinatorList"]).Where(x => x.TravelCoordCD.Trim().ToUpper().ToString() == tbCode.Text.Trim().ToUpper().ToString()).ToList<GetTravelCoordinator>();
                        if (TravelCoordinatorList.Count != 0)
                        {
                            cvAircraftCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            ReturnValue = true;
                        }
                        else
                        {
                            cvAircraftCode.IsValid = true;
                            RadAjaxManager1.FocusControl(tbHomeBase.ClientID); //tbHomeBase.Focus();
                            ReturnValue = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="travelCoordCD"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.TravelCoordinatorRequestor> GetTravelPaxAvailableListByCode(Int64 TravelCoordinatorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinatorID))
            {
                List<FlightPakMasterService.TravelCoordinatorRequestor> TravelCoordinatorRequestorList = new List<TravelCoordinatorRequestor>();
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = PaxService.GetTravelCoordinatorRequestorSelectedList(TravelCoordinatorID);
                            TravelCoordinatorRequestorList = ObjRetVal.EntityList.ToList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
                return TravelCoordinatorRequestorList;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="travelCoordCD"></param>
        /// <returns></returns>
        public List<FlightPakMasterService.TravelCoordinatorFleet> GetTravelFleetSelectedList(Int64 TravelCoordinatorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinatorID))
            {
                List<FlightPakMasterService.TravelCoordinatorFleet> TravelCoordinatorFleetList = new List<TravelCoordinatorFleet>();
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = PaxService.GetTravelCoordinatorFleetSelectedList(TravelCoordinatorID);
                            TravelCoordinatorFleetList = ObjRetVal.EntityList.ToList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
                return TravelCoordinatorFleetList;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnclrFilters_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (GridColumn Column in dgTravelCoordinator.MasterTableView.Columns)
                        {
                            //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            //column.CurrentFilterValue = string.Empty;
                        }
                        dgTravelCoordinator.MasterTableView.FilterExpression = string.Empty;
                        dgTravelCoordinator.MasterTableView.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TailNum"></param>
        /// <param name="Choice"></param>
        /// <returns></returns>
        private FlightPakMasterService.TravelCoordinator GetFleetItems(FlightPakMasterService.TravelCoordinator objTravelCoordinator)//string TailNum, bool Choice)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objTravelCoordinator))
            {
                FlightPakMasterService.TravelCoordinatorFleet objTravelCoordinatorFleet;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objTravelCoordinator.TravelCoordinatorFleet = new List<FlightPakMasterService.TravelCoordinatorFleet>();
                    htSelectedList = (Hashtable)Session["SelectedFleetList"];
                    Int64 Identity = 0;
                    for (int AvailableItems = 0; AvailableItems < lstAircraftAvailable.Items.Count; AvailableItems++)
                    {
                        objTravelCoordinatorFleet = new FlightPakMasterService.TravelCoordinatorFleet();
                        objTravelCoordinatorFleet.TravelCoordinatorID = objTravelCoordinator.TravelCoordinatorID;// Convert.ToInt64(hdnTravelCoordinatorID.Value); //tbCode.Text.Trim();
                        objTravelCoordinatorFleet.CustomerID = objTravelCoordinator.CustomerID;
                        objTravelCoordinatorFleet.FleetID = Convert.ToInt64(lstAircraftAvailable.Items[AvailableItems].Value.Trim());
                        if ((hdnSave.Value == "Update") && (Convert.ToInt64(htSelectedList[objTravelCoordinatorFleet.FleetID.Value.ToString()]) != 0))
                        {
                            objTravelCoordinatorFleet.TravelCoordinatorFleetID = Convert.ToInt64(htSelectedList[objTravelCoordinatorFleet.FleetID.Value.ToString()]);
                        }
                        else
                        {
                            Identity = Identity - 1;
                            objTravelCoordinatorFleet.TravelCoordinatorFleetID = Identity;
                        }
                        if (lstAircraftAvailable.Items[AvailableItems].Selected)
                        {
                            objTravelCoordinatorFleet.IsChoice = true;
                        }
                        objTravelCoordinatorFleet.IsDeleted = false;
                        objTravelCoordinator.TravelCoordinatorFleet.Add(objTravelCoordinatorFleet);
                    }

                    //code for deleting removed items
                    htSelectedList = new Hashtable();
                    bool IsAvailable = false;
                    int TIndex = 0;
                    if (Session["SelectedFleetList"] != null)
                    {
                        htSelectedList = (Hashtable)Session["SelectedFleetList"];
                    }
                    foreach (DictionaryEntry hsl in htSelectedList) // (int DIndex = 0; DIndex < htSelectedList.Count; DIndex++)
                    {
                        IsAvailable = false;
                        TIndex = 0;
                        for (int Index = 0; Index < objTravelCoordinator.TravelCoordinatorFleet.Count; Index++)
                        {
                            TIndex = TIndex + 1;
                            if (Convert.ToInt64(hsl.Key) == objTravelCoordinator.TravelCoordinatorFleet[Index].FleetID.Value)
                            {
                                IsAvailable = true;
                                break;
                            }
                            else
                            {
                                IsAvailable = false;
                            }
                        }
                        if (IsAvailable == false)
                        {
                            objTravelCoordinatorFleet = new FlightPakMasterService.TravelCoordinatorFleet();
                            objTravelCoordinatorFleet.TravelCoordinatorID = objTravelCoordinator.TravelCoordinatorID;
                            objTravelCoordinatorFleet.CustomerID = objTravelCoordinator.CustomerID;
                            objTravelCoordinatorFleet.FleetID = Convert.ToInt64(hsl.Key);
                            objTravelCoordinatorFleet.TravelCoordinatorFleetID = Convert.ToInt64(hsl.Value);
                            objTravelCoordinatorFleet.IsChoice = false;
                            objTravelCoordinatorFleet.IsDeleted = true;
                            objTravelCoordinator.TravelCoordinatorFleet.Add(objTravelCoordinatorFleet);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objTravelCoordinator;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PassengerCD"></param>
        /// <param name="Choice"></param>
        /// <returns></returns>
        private FlightPakMasterService.TravelCoordinator GetRequestorItems(FlightPakMasterService.TravelCoordinator objTravelCoordinator)//string PassengerCD, bool Choice)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objTravelCoordinator))
            {
                FlightPakMasterService.TravelCoordinatorRequestor objTravelCoordinatorRequestor;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objTravelCoordinator.TravelCoordinatorRequestor = new List<FlightPakMasterService.TravelCoordinatorRequestor>();
                    htSelectedList = (Hashtable)Session["SelectedPassengerList"];
                    Int64 Identity = 0;
                    for (int AvailableItems = 0; AvailableItems < lstRequestorAvailable.Items.Count; AvailableItems++)
                    {
                        objTravelCoordinatorRequestor = new FlightPakMasterService.TravelCoordinatorRequestor();
                        objTravelCoordinatorRequestor.TravelCoordinatorID = objTravelCoordinator.TravelCoordinatorID; //Convert.ToInt64(hdnTravelCoordinatorID.Value); //tbCode.Text.Trim();
                        objTravelCoordinatorRequestor.CustomerID = objTravelCoordinator.CustomerID;
                        objTravelCoordinatorRequestor.PassengerRequestorID = Convert.ToInt64(lstRequestorAvailable.Items[AvailableItems].Value.Trim());
                        if ((hdnSave.Value == "Update") && (Convert.ToInt64(htSelectedList.Contains(objTravelCoordinatorRequestor.PassengerRequestorID.Value.ToString())) != 0))
                        {
                            objTravelCoordinatorRequestor.TravelCoordinatorRequestorID = Convert.ToInt64(htSelectedList[objTravelCoordinatorRequestor.PassengerRequestorID.Value.ToString()]);
                        }
                        else
                        {
                            Identity = Identity - 1;
                            objTravelCoordinatorRequestor.TravelCoordinatorRequestorID = Identity;
                        }
                        if (lstRequestorAvailable.Items[AvailableItems].Selected)
                        {
                            objTravelCoordinatorRequestor.IsChoice = true;
                        }
                        objTravelCoordinatorRequestor.IsDeleted = false;
                        objTravelCoordinator.TravelCoordinatorRequestor.Add(objTravelCoordinatorRequestor);
                    }

                    //code for deleting removed items
                    htSelectedList = new Hashtable();
                    bool IsAvailable = false;
                    int TIndex = 0;
                    if (Session["SelectedPassengerList"] != null)
                    {
                        htSelectedList = (Hashtable)Session["SelectedPassengerList"];
                    }
                    foreach (DictionaryEntry hsl in htSelectedList) // (int DIndex = 0; DIndex < htSelectedList.Count; DIndex++)
                    {
                        IsAvailable = false;
                        TIndex = 0;
                        for (int Index = 0; Index < objTravelCoordinator.TravelCoordinatorRequestor.Count; Index++)
                        {
                            TIndex = TIndex + 1;
                            if (Convert.ToInt64(hsl.Key) == objTravelCoordinator.TravelCoordinatorRequestor[Index].PassengerRequestorID.Value)
                            {
                                IsAvailable = true;
                                break;
                            }
                            else
                            {
                                IsAvailable = false;
                            }
                        }
                        if (IsAvailable == false)
                        {
                            objTravelCoordinatorRequestor = new FlightPakMasterService.TravelCoordinatorRequestor();
                            objTravelCoordinatorRequestor.TravelCoordinatorID = objTravelCoordinator.TravelCoordinatorID;
                            objTravelCoordinatorRequestor.CustomerID = objTravelCoordinator.CustomerID;
                            objTravelCoordinatorRequestor.PassengerRequestorID = Convert.ToInt64(hsl.Key);
                            objTravelCoordinatorRequestor.TravelCoordinatorRequestorID = Convert.ToInt64(hsl.Value);
                            objTravelCoordinatorRequestor.IsChoice = false;
                            objTravelCoordinatorRequestor.IsDeleted = true;
                            objTravelCoordinator.TravelCoordinatorRequestor.Add(objTravelCoordinatorRequestor);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objTravelCoordinator;
            }
        }
        /// <summary>
        /// Define the method of IPostBackEventHandler that raises change events.
        /// </summary>
        /// <param name="sourceControl"></param>
        /// <param name="eventArgument"></param>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RequestorList"></param>
        /// <returns></returns>
        private List<FlightPakMasterService.Fleet> RetainFleetList(RadListBox RequestorList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestorList))
            {
                List<FlightPakMasterService.Fleet> FleetItems = new List<FlightPakMasterService.Fleet>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (lstAircraftAvailable.Items.Count > 0)
                    {
                        for (int ListItem = 0; ListItem < RequestorList.Items.Count; ListItem++)
                        {
                            FleetItems.Add(new FlightPakMasterService.Fleet
                            {
                                TailNum = RequestorList.Items[ListItem].Text.Trim(),
                                FleetID = Convert.ToInt64(RequestorList.Items[ListItem].Value.Trim())
                            });
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return FleetItems;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RequestorList"></param>
        /// <returns></returns>
        private List<Passenger> RetainRequestorList(RadListBox RequestorList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestorList))
            {
                List<Passenger> Requestors = new List<Passenger>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (lstRequestorAvailable.Items.Count > 0)
                    {
                        for (int ListItem = 0; ListItem < RequestorList.Items.Count; ListItem++)
                        {
                            Requestors.Add(new Passenger
                            {
                                PassengerRequestorCD = RequestorList.Items[ListItem].Text.Trim(),
                                PassengerRequestorID = Convert.ToInt64(RequestorList.Items[ListItem].Value.Trim())
                            });
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return Requestors;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void RequestorSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<Passenger> FinalList = new List<Passenger>();//karthik
                    List<Passenger> RetainList = new List<Passenger>();
                    // Retain Grid Items and bind into List and add into Final List
                    RetainList = (RetainRequestorList(lstRequestorAvailable));
                    FinalList.AddRange(RetainList);
                    Session["lstPax"] = FinalList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void FleetSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.Fleet> FinalList = new List<FlightPakMasterService.Fleet>();
                    List<FlightPakMasterService.Fleet> RetainList = new List<FlightPakMasterService.Fleet>();
                    RetainList = (RetainFleetList(lstAircraftAvailable));
                    FinalList.AddRange(RetainList);
                    Session["lstFleet"] = FinalList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbHomeBase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckhomebaseExist(tbHomeBase);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
                }
            }
        }
        private bool CheckhomebaseExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(txtbx.Text.ToString().ToUpper().Trim()));
                            if (objRetVal.Count() > 0 && objRetVal != null)
                            {
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                                hdnHomeBaseID.Value = CompanyList[0].HomebaseID.ToString();
                                txtbx.Text = CompanyList[0].HomebaseCD;
                            }
                            else
                            {
                                cvHomeBase.IsValid = false;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = false;
                            }
                        }
                    }
                    else
                    {
                        hdnHomeBaseID.Value = string.Empty;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        //protected void chkHomeBase_OnCheckedChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                DoSearchFilter();
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.TravelCoordinator);
        //        }
        //    }
        //}

        protected void DoSearchFilter()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objTravelCoordinatorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                if (Session["TravelCoordinatorList"] != null)
                {
                    TravelCoordinatorList = (List<FlightPakMasterService.GetTravelCoordinator>)Session["TravelCoordinatorList"];
                }
                if (TravelCoordinatorList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { TravelCoordinatorList = TravelCoordinatorList.Where(x => x.IsInactive == false).ToList<GetTravelCoordinator>(); }
                    if (chkHomeBase.Checked == true) { TravelCoordinatorList = TravelCoordinatorList.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetTravelCoordinator>(); }
                    dgTravelCoordinator.DataSource = TravelCoordinatorList.OrderBy(x => x.TravelCoordCD);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter();
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        #endregion
        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
            chkHomeBase.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
            chkHomeBase.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgTravelCoordinator.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var TravelCoordinatorValue = FPKMstService.GetTravelCoordinatorList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, TravelCoordinatorValue);
            List<FlightPakMasterService.GetTravelCoordinator> filteredList = GetFilteredList(TravelCoordinatorValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.TravelCoordinatorID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgTravelCoordinator.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgTravelCoordinator.CurrentPageIndex = PageNumber;
            dgTravelCoordinator.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetTravelCoordinator TravelCoordinatorValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["TravelCoordinatorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = TravelCoordinatorValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().TravelCoordinatorID;
                Session["TravelCoordinatorID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetTravelCoordinator> GetFilteredList(ReturnValueOfGetTravelCoordinator TravelCoordinatorValue)
        {
            List<FlightPakMasterService.GetTravelCoordinator> filteredList = new List<FlightPakMasterService.GetTravelCoordinator>();

            if (TravelCoordinatorValue.ReturnFlag)
            {
                filteredList = TravelCoordinatorValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInactive == false).ToList<GetTravelCoordinator>(); }
                    if (chkHomeBase.Checked) { filteredList = filteredList.Where(x => x.HomebaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetTravelCoordinator>(); }
                }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgTravelCoordinator.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgTravelCoordinator.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}