﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class AssignRequestorsPopUp : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //string OtherSelected = Request.QueryString["PassengerID"].ToString();
                        //string[] strarr;
                        //dgAssignRequestors.Rebind();
                        //foreach (GridDataItem item in dgAssignRequestors.MasterTableView.Items)
                        //{
                        //    strarr = item["PassengerRequestorID"].Text.Split(',');
                        //    if (OtherSelected != null)
                        //    {
                        //        if (OtherSelected.IndexOf(strarr[0].Trim() + ",") > -1 && OtherSelected.ToString().Trim() != "")
                        //            item.Selected = true;
                        //    }
                        //}

                        if (!IsPostBack)
                        {
                            string ItemSelected = string.Empty;
                            string[] strarr = new string[] { };
                            if (Request.QueryString["PassengerID"] != null)
                            {
                                ItemSelected = Request.QueryString["PassengerID"].ToString();
                            }
                            if (ItemSelected.Contains(","))
                            {
                                strarr = ItemSelected.Split(',');
                            }
                            dgAssignRequestors.Rebind();
                            if (strarr.Length != 0)
                            {
                                foreach (GridDataItem item in dgAssignRequestors.MasterTableView.Items)
                                {
                                    for (int Index = 0; Index < strarr.Length; Index++)
                                    {
                                        if ((!string.IsNullOrEmpty(strarr[Index])) && (strarr[Index].ToString() == item["PassengerRequestorID"].Text))
                                        {
                                            item.Selected = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                dgAssignRequestors.SelectedIndexes.Add(0);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<Passenger> requesterlist = new List<Passenger>();
                    if (dgAssignRequestors.SelectedItems.Count > 0)
                    {                       
                        GridDataItem Item;
                        for (int i = 0; i < dgAssignRequestors.SelectedItems.Count; i++)
                        {
                            Item = (GridDataItem)dgAssignRequestors.SelectedItems[i];
                            //if (CheckIfExists(Item.GetDataKeyValue("PassengerRequestorCD").ToString()))
                            //{
                            //    lbMessage.Text = "Warning, PassengerRequestorCD Already Exists.";
                            //    lbMessage.ForeColor = System.Drawing.Color.Red;
                            //}
                            //else
                            {
                                requesterlist.Add(new Passenger
                                {
                                    PassengerRequestorCD = Item.GetDataKeyValue("PassengerRequestorCD").ToString(),
                                    PassengerRequestorID = Convert.ToInt64(Item.GetDataKeyValue("PassengerRequestorID").ToString())
                                });                                
                            }
                        }                        
                    }
                    Session["newPax"] = requesterlist;
                    Session.Remove("newfleet");
                    InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                    //Session["CrewAddlInfo"] = null;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckIfExists(string InfoCode)
        {
            bool ReturnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InfoCode))
            {

                if (Session["lstPax"] != null)
                {
                    List<Passenger> CrewDefinitionList = (List<Passenger>)Session["lstPax"];
                    var CrewAdditionalInfoValue = (from crew in CrewDefinitionList
                                                   where crew.PassengerRequestorCD.ToUpper().Trim() == InfoCode.ToUpper().Trim()
                                                   select crew);
                    if (CrewAdditionalInfoValue.Count() > 0)
                    { ReturnValue = true; }
                }
            }
            return ReturnValue;
        }
        protected void dgAssignRequestors_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgAssignRequestors_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }
        protected void dgAssignRequestors_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = PaxService.GetPassengerList().EntityList.Where(x => x.IsRequestor == Convert.ToBoolean(1)).ToList();
                            dgAssignRequestors.DataSource = ObjRetVal;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        protected void dgAssignRequestors_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAssignRequestors, Page.Session);
        }
    }
}