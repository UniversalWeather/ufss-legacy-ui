﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewRosterAddInfoPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.People.CrewRosterAddInfoPopup" ClientIDMode="AutoID"  MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crew Additional Information</title>
      <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgCrewRosterAddInfo.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCrewRosterAddInfo.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "CrewInfoCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "CrewInformationDescription")
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "CrewInfoID")
                }

                if (selectedRows.length > 0) {
                    oArg.CrewInfoCD = cell1.innerHTML;
                    oArg.CrewInfoDesc = cell2.innerHTML;
                    oArg.CrewInfoID = cell3.innerHTML;
                }
                else {
                    oArg.CrewInfoCD = "";
                    oArg.CrewInfoDesc = "";
                    oArg.CrewInfoID = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }

            function RowDblClick() {
                var masterTable = $find("<%= dgCrewRosterAddInfo.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function OpenAddWindow(arg) {
                var url = "";
                var oManager = $find("<%= RadWindowManager1.ClientID %>");              
                if (arg == "add")
                    url = "../People/CrewRosterAddInfoCatalog.aspx?IsPopup=Add";
                else if (arg == "edit") {
                    oArg = new Object();
                    grid = $find("<%= dgCrewRosterAddInfo.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    if (selectedRows.length > 0)
                        url = "../People/CrewRosterAddInfoCatalog.aspx?IsPopup=&CrewInfoId=" + MasterTable.getCellByColumnUniqueName(selectedRows[0], "CrewInfoID").innerHTML;
                    else {
                        oManager.radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                        return false;
                    }
                }
                else if (arg == "delete") {
                    oArg = new Object();
                    grid = $find("<%= dgCrewRosterAddInfo.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    if (selectedRows.length > 0) {
                       // oManager.radconfirm('This Will Delete The Code From All Associated Crew Members. Continue Delete?',DeleteRecord, 330, 100, "Delete", "Delete");
                       // oManager.radconfirm('This Will Delete The Code From All Associated Crew Members. Continue Delete?');
                        return ProcessDelete('This Will Delete The Code From All Associated Crew Members. Continue Delete?');
                    }
                    else {
                        oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                        return false;
                    }

                }
                oManager.open(url, "RadAEAddlInfo");
                oManager.add_close(OnPageClosed);
               
                return false;
            }
        
            function OnPageClosed(sender, args) {
                rebindgrid();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRosterAddInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="LinkButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgCrewRosterAddInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="lbtnInitEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
         <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadAEAddlInfo" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnPageClosed" ShowContentDuringLoad="false"
                AutoSize="false" Width="430px" Height="200px" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            </Windows>
         </telerik:RadWindowManager>
        <telerik:RadGrid ID="dgCrewRosterAddInfo" runat="server" AllowSorting="true" Visible="true"
            OnNeedDataSource="CrewRosterAddInfo_BindData" OnItemCommand="CrewRosterAddInfo_ItemCommand"
            OnInsertCommand="CrewRosterAddInfo_InsertCommand" PageSize="10" Width="500px" OnItemCreated="dgCrewRosterAddInfo_ItemCreated"
            AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
            AutoGenerateColumns="false" OnDeleteCommand="dgCrewRosterAddInfo_DeleteCommand">
            <MasterTableView DataKeyNames="CrewInfoID,CrewInfoCD,CustomerID,CrewInformationDescription,LastUpdUID,LastUpdTS,IsDeleted" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="CrewInfoCD" HeaderText="Code" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewInformationDescription" HeaderText="Crew Additional Info"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        HeaderStyle-Width="400px" FilterControlWidth="380px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewInfoID" HeaderText="Code" AutoPostBackOnFilter="false" Display="false"
                        ShowFilterIcon="false" CurrentFilterFunction="EqualTo" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                     <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="LinkButton1" runat="server" ToolTip="Add" OnClientClick="javascript:return OpenAddWindow('add'); return false;"
                        Visible='<%# IsAuthorized(Permission.Database.AddCrewRosterAddInfoCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return OpenAddWindow('edit'); return false;"
                        Visible='<%# IsAuthorized(Permission.Database.EditCrewRosterAddInfoCatalog)%>'
                        ToolTip="Edit" ><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                 <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete('This Will Delete The Code From All Associated Crew Members. Continue Delete?');"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteCrewRosterAddInfoCatalog)%>'
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                  </div>
                    <div style="padding: 5px 5px; text-align: right;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "RowDblClick" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Label ID="InjectScript" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <div id="DivExternalForm" runat="server" class="dgpExternalForm"></div>
    </div>
    </form>
</body>
</html>
