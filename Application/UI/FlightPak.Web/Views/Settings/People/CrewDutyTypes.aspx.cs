﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class CrewDutyTypes : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool CrewDutyTypePageNavigated = false;
        private List<string> listCrewCodes = new List<string>();
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewCrewDutyTypesReport);

                        if (!IsPostBack)
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewDutyTypes, dgCrewDutyTypes, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewDutyTypes.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewCrewDutyTypes);
                             // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgCrewDutyTypes.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgCrewDutyTypes.Visible = false;
                chkSearchActiveOnly.Visible = false;
                if (IsAdd)
                {
                    (dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCrewDutyTypes.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCrewDutyTypes.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            RadAjaxManager1.FocusControl(target.ClientID); //target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                        {
                            e.Updated = dgCrewDutyTypes;
                        }
                        if (hdnSave.Value == "Delete" || hdnGridEnable.Value == "Save")
                        {
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CrewTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.CrewDutyType> CrewDutyTypeList = new List<CrewDutyType>();
                            var objCrewTypeVal = CrewTypeService.GetCrewDutyTypeList();
                            if (objCrewTypeVal.ReturnFlag == true)
                            {
                                CrewDutyTypeList = objCrewTypeVal.EntityList;
                            }
                            dgCrewDutyTypes.DataSource = CrewDutyTypeList;
                            Session["CrewDutyType"] = CrewDutyTypeList;
                            if (((chkSearchActiveOnly.Checked == true)) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Command for Aircraft Type Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedDutyTypeID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"].ToString().Trim()));
                                        Session["IsCrewDutyTypeEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyType);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyType);
                                            dgCrewDutyTypes.Rebind();
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        RadAjaxManager1.FocusControl(tbDescription.ClientID); //tbDescription.Focus();
                                        SelectItem();
                                        tbCode.Enabled = false;
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCrewDutyTypes.SelectedIndexes.Clear();
                                GridEnable(true, false, false);
                                DisplayInsertForm();
                                RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }

                                Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;

                            case "RowClick":
                                DefaultSelection(true);
                                break;

                            //case "RowClick":
                            //    GridDataItem item = dgCrewDutyTypes.SelectedItems[0] as GridDataItem;
                            //    Session["SelectedDutyTypeID"] = item.GetDataKeyValue("DutyTypeID").ToString();
                            //    var key = item.ItemIndex;
                            //    dgCrewDutyTypes.Rebind();
                            //    dgCrewDutyTypes.SelectedIndexes.Add(key);
                            //    GridEnable(true, true, true);
                            //    ReadOnlyForm();
                            //    break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Update Command for Aircraft Duty Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        string startTime = string.IsNullOrEmpty(rmtbStartTime.Text) ? "0000" : rmtbStartTime.Text;
                        string endtime = string.IsNullOrEmpty(rmtbEndTime.Text) ? "0000" : rmtbEndTime.Text;
                        if (Session["SelectedDutyTypeID"] != null)
                        {
                            if (Convert.ToDouble(startTime, CultureInfo.CurrentCulture) > Convert.ToDouble(endtime, CultureInfo.CurrentCulture))
                            {
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Start time should be lesser than end time', 360, 50, 'Crew Duty Type ');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                            else
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = CrewDutyTypeService.UpdateCrewDutyType(GetItems());
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"].ToString().Trim()));
                                        }
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true);
                                        SelectItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();
                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewDutyType);
                                    }
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            Session.Remove("SelectedDutyTypeID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCrewDutyTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private CrewDutyType GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.CrewDutyType CrewDutyType = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewDutyType = new FlightPakMasterService.CrewDutyType();
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedDutyTypeID"] != null)
                        {
                            CrewDutyType.DutyTypeID = Convert.ToInt64(Session["SelectedDutyTypeID"].ToString().Trim());
                        }
                    }
                    else
                    {
                        CrewDutyType.DutyTypeID = 0;
                    }
                    CrewDutyType.DutyTypeCD = tbCode.Text;
                    CrewDutyType.DutyTypesDescription = tbDescription.Text;
                    if (tbValuePoints.Text != string.Empty)
                    {
                        CrewDutyType.ValuePTS = Convert.ToDecimal(tbValuePoints.Text);
                    }
                    if (tbWeekendValuePoints.Text != string.Empty)
                    {
                        CrewDutyType.WeekendPTS = Convert.ToDecimal(tbWeekendValuePoints.Text);
                    }
                    if (tbHolidayValuePoints.Text != string.Empty)
                    {
                        CrewDutyType.HolidayPTS = Convert.ToDecimal(tbHolidayValuePoints.Text);
                    }
                    CrewDutyType.BackgroundCustomColor = hdnBackColor.Value;
                    CrewDutyType.ForeGrndCustomColor = hdnForeColor.Value;
                    CrewDutyType.DutyStartTM = rmtbStartTime.Text;
                    CrewDutyType.DutyEndTM = rmtbEndTime.Text;
                    //CrewDutyType.IsCrewCurrency = chkCrewCurrencyPlanner.Checked;
                    CrewDutyType.CalendarEntry = chkQuickCalendarEntry.Checked;
                    CrewDutyType.IsCrewDuty = chkCrewDuty.Checked;
                    CrewDutyType.IsOfficeDuty = chkOfficeDuty.Checked;
                    CrewDutyType.IsStandby = chkCrewStandBy.Checked;
                    CrewDutyType.IsWorkLoadIndex = chkWorkLoadIndex.Checked;
                    CrewDutyType.IsInActive = chkInactive.Checked;
                    CrewDutyType.IsDeleted = false;                    
                    return CrewDutyType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewDutyType;
            }
        }
        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string startTime = string.IsNullOrEmpty(rmtbStartTime.Text) ? "0000" : rmtbStartTime.Text;
                        string endtime = string.IsNullOrEmpty(rmtbEndTime.Text) ? "0000" : rmtbEndTime.Text;
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                        }
                        else if (Convert.ToDouble(startTime, CultureInfo.CurrentCulture) > Convert.ToDouble(endtime, CultureInfo.CurrentCulture))
                        {
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('Start time should be lesser than end time', 360, 50, 'Crew Duty Type ');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else if (CheckForReservedCode())
                        {
                            string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                + @" oManager.radalert('R, F, G and SM Codes Are Reserved For Internal Use by FSS.', 400, 50, 'Crew Duty Type');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objCrewDutyTypeService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = objCrewDutyTypeService.AddCrewDutyType(GetItems());
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgCrewDutyTypes.Rebind();
                                    DefaultSelection(true);
                                    hdnGridEnable.Value = "Save";

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.CrewDutyType);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCrewDutyTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_DeleteCommand(object source, GridCommandEventArgs e)
        {

            string Code = tbCode.Text.Trim();
            string strCrewGroupTypeID = "";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string dutyCode = string.Empty;
                string dutyId = string.Empty;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDutyTypeID"] != null)
                        {
                            foreach (GridDataItem Item in dgCrewDutyTypes.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("DutyTypeCD").ToString().Trim() == Code.Trim())
                                {
                                    dutyCode = Item.GetDataKeyValue("DutyTypeCD").ToString().ToUpper().Trim();
                                    dutyId = Item.GetDataKeyValue("DutyTypeID").ToString().ToUpper().Trim();
                                    break;
                                }
                            }
                            if (dutyCode == "F" || dutyCode == "G" || dutyCode == "R" || dutyCode == "SM")
                            {
                                string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('R, F, G and SM Codes Are Reserved For Internal Use by FSS.', 400, 50, 'Crew Duty Type');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            }
                            else
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CrewDutyTypeTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.CrewDutyType CrewDutyType = new FlightPakMasterService.CrewDutyType();


                                    foreach (GridDataItem Item in dgCrewDutyTypes.MasterTableView.Items)
                                    {
                                        if (Item.GetDataKeyValue("DutyTypeID").ToString().Trim() == dutyId.Trim())
                                        {
                                            CrewDutyType.DutyTypeID = Convert.ToInt64(Item.GetDataKeyValue("DutyTypeID").ToString());
                                            CrewDutyType.DutyTypeCD = Item.GetDataKeyValue("DutyTypeCD").ToString();
                                            CrewDutyType.IsDeleted = true;
                                            strCrewGroupTypeID = Item.GetDataKeyValue("DutyTypeID").ToString().Trim();
                                            break;
                                        }
                                    }
                                    //Lock will happen from UI
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"]));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            e.Item.Selected = true;
                                            DefaultSelection(false);
                                            dgCrewDutyTypes.Rebind();
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyType);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.CrewDutyType);
                                            return;
                                        }
                                    }
                                    CrewDutyTypeTypeService.DeleteCrewDutyType(CrewDutyType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                    hdnSave.Value = "Delete";
                                }
                            }

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"]));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgCrewDutyTypes.SelectedItems[0] as GridDataItem;
                                Session["SelectedDutyTypeID"] = item.GetDataKeyValue("DutyTypeID").ToString().Trim();
                                //var key = item.ItemIndex;
                                //dgCrewDutyTypes.Rebind();
                                //dgCrewDutyTypes.SelectedIndexes.Add(key);
                                ReadOnlyForm();
                                GridEnable(true, true, true);

                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                    }
                }
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgCrewDutyTypes.Rebind();
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                    tbValuePoints.Enabled = false;
                    tbWeekendValuePoints.Enabled = false;
                    tbHolidayValuePoints.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To display edit form
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    dgCrewDutyTypes.Rebind();
                    EnableForm(true);
                    if (chkWorkLoadIndex.Checked == false)
                    {
                        tbValuePoints.Enabled = false;
                        tbWeekendValuePoints.Enabled = false;
                        tbHolidayValuePoints.Enabled = false;
                    }
                    else
                    {
                        tbValuePoints.Enabled = true;
                        tbWeekendValuePoints.Enabled = true;
                        tbHolidayValuePoints.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Aircraft Duty Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Cancel Aircraft Duty Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedDutyTypeID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.CrewDutyType, Convert.ToInt64(Session["SelectedDutyTypeID"].ToString().Trim()));
                                }
                            }
                        }
                        //Session.Remove("SelectedDutyTypeID");
                        DefaultSelection(true);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCrewDutyTypePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewDutyTypes.Rebind();
                    }
                    if (dgCrewDutyTypes.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedDutyTypeID"] = null;
                        //}
                        if (Session["SelectedDutyTypeID"] == null)
                        {
                            dgCrewDutyTypes.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["SelectedDutyTypeID"] = dgCrewDutyTypes.Items[0].GetDataKeyValue("DutyTypeID").ToString();
                        }

                        if (dgCrewDutyTypes.SelectedIndexes.Count == 0)
                            dgCrewDutyTypes.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDutyTypeID"] != null)
                    {
                        string ID = Session["SelectedDutyTypeID"].ToString();
                        foreach (GridDataItem item in dgCrewDutyTypes.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("DutyTypeID").ToString().ToUpper().Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            dataItem.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
                            dataItem.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// Function to Check if Aircraft Duty Type Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            bool returnVal = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewDutyType"] != null)
                    {
                        List<CrewDutyType> CrewDutyTypeList = new List<CrewDutyType>();
                        CrewDutyTypeList = ((List<CrewDutyType>)Session["CrewDutyType"]).Where(x => x.DutyTypeCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<CrewDutyType>();
                        if (CrewDutyTypeList.Count != 0)
                        {
                            cvCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                            returnVal = true;
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
                    lbtninsertCtl = (LinkButton)dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lbtndelCtl = (LinkButton)dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    lbtneditCtl = (LinkButton)dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddCrewDutyTypes))
                    {
                        lbtninsertCtl.Visible = true;
                        if (add)
                        {
                            lbtninsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtninsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtninsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCrewDutyTypes))
                    {
                        lbtndelCtl.Visible = true;
                        if (delete)
                        {
                            lbtndelCtl.Enabled = true;
                            lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtndelCtl.Enabled = false;
                            lbtndelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtndelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCrewDutyTypes))
                    {
                        lbtneditCtl.Visible = true;
                        if (edit)
                        {
                            lbtneditCtl.Enabled = true;
                            lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtneditCtl.Enabled = false;
                            lbtneditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtneditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To display in read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgCrewDutyTypes.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Load Selected Item Data into the Form Fields
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDutyTypeID"] != null)
                    {

                        foreach (GridDataItem Item in dgCrewDutyTypes.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("DutyTypeID").ToString().Trim() == Session["SelectedDutyTypeID"].ToString().Trim())
                            {
                                hdnCDTID.Value = Convert.ToString(Item.GetDataKeyValue("DutyTypeID"));//.ToString();
                                tbCode.Text = Convert.ToString(Item.GetDataKeyValue("DutyTypeCD"));//.ToString();
                                if (Item.GetDataKeyValue("DutyTypesDescription") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("DutyTypesDescription").ToString();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ValuePTS") != null)
                                {
                                    tbValuePoints.Text = Item.GetDataKeyValue("ValuePTS").ToString();
                                }
                                else
                                {
                                    tbValuePoints.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("WeekendPTS") != null)
                                {
                                    tbWeekendValuePoints.Text = Item.GetDataKeyValue("WeekendPTS").ToString();
                                }
                                else
                                {
                                    tbWeekendValuePoints.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HolidayPTS") != null)
                                {
                                    tbHolidayValuePoints.Text = Item.GetDataKeyValue("HolidayPTS").ToString();
                                }
                                else
                                {
                                    tbHolidayValuePoints.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ForeGrndCustomColor") != null)
                                {
                                    System.Drawing.Color foreColor;
                                    tbForeColor.Text = Item.GetDataKeyValue("ForeGrndCustomColor").ToString();
                                    hdnForeColor.Value = Item.GetDataKeyValue("ForeGrndCustomColor").ToString();
                                    foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
                                    rcpForeColor.SelectedColor = foreColor;
                                }
                                else
                                {
                                    tbForeColor.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BackgroundCustomColor") != null)
                                {
                                    System.Drawing.Color backColor;
                                    tbBackColor.Text = Item.GetDataKeyValue("BackgroundCustomColor").ToString();
                                    hdnBackColor.Value = Item.GetDataKeyValue("BackgroundCustomColor").ToString();
                                    backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
                                    rcpBackColor.SelectedColor = backColor;
                                }
                                else
                                {
                                    tbBackColor.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CalendarEntry") != null)
                                {
                                    chkQuickCalendarEntry.Checked = Convert.ToBoolean(Item.GetDataKeyValue("CalendarEntry").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("CalendarEntry").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkQuickCalendarEntry.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsWorkLoadIndex") != null)
                                {
                                    chkWorkLoadIndex.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsWorkLoadIndex").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsWorkLoadIndex").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkWorkLoadIndex.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsOfficeDuty") != null)
                                {
                                    chkOfficeDuty.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsOfficeDuty").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsOfficeDuty").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkOfficeDuty.Checked = false;
                                }
                                //if (Item.GetDataKeyValue("IsCrewCurrency") != null)
                                //{
                                //    chkCrewCurrencyPlanner.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsCrewCurrency").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsCrewCurrency").ToString(), CultureInfo.CurrentCulture);
                                //}
                                //else
                                //{
                                //    chkCrewCurrencyPlanner.Checked = false;
                                //}
                                if (Item.GetDataKeyValue("IsCrewDuty") != null)
                                {
                                    chkCrewDuty.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsCrewDuty").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsCrewDuty").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkCrewDuty.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsStandby") != null)
                                {
                                    chkCrewStandBy.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsStandby").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsStandby").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkCrewStandBy.Checked = false;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(Item.GetDataKeyValue("DutyStartTM"))))
                                {
                                    rmtbStartTime.Text = Convert.ToString(Item.GetDataKeyValue("DutyStartTM")).Insert(2, ":");
                                }
                                else
                                {
                                    rmtbStartTime.Text = "00:00";
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(Item.GetDataKeyValue("DutyEndTM"))))
                                {
                                    rmtbEndTime.Text = Convert.ToString(Item.GetDataKeyValue("DutyEndTM")).Insert(2, ":");
                                }
                                else
                                {
                                    rmtbEndTime.Text = "00:00";
                                }
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgCrewDutyTypes.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString() == "&nbsp;" ? "False" : Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                lbColumnName1.Text = "Duty Type Code";
                                lbColumnName2.Text = "Description";
                                lbColumnValue1.Text = Item["DutyTypeCD"].Text;
                                lbColumnValue2.Text = Item["DutyTypesDescription"].Text;
                                break;
                            }
                        }
                    }


                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Function to Enable / Diable the form fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    if (((enable == true) && (hdnSave.Value == "Update")) && (tbCode.Text.ToUpper().Trim() == "F" || tbCode.Text.ToUpper().Trim() == "G" || tbCode.Text.ToUpper().Trim() == "SM"))
                    {
                        tbCode.Enabled = false;
                        tbValuePoints.Enabled = false;
                        tbWeekendValuePoints.Enabled = false;
                        tbHolidayValuePoints.Enabled = false;
                        tbDescription.Enabled = false;

                    }
                    else
                    {
                        tbCode.Enabled = enable;
                        tbValuePoints.Enabled = enable;
                        tbWeekendValuePoints.Enabled = enable;
                        tbHolidayValuePoints.Enabled = enable;
                        tbDescription.Enabled = enable;

                    }
                    tbCode.Enabled = enable;

                    rmtbStartTime.Enabled = enable;
                    rmtbEndTime.Enabled = enable;
                    chkQuickCalendarEntry.Enabled = enable;

                    //chkCrewCurrencyPlanner.Enabled = enable;
                    chkWorkLoadIndex.Enabled = enable;
                    chkOfficeDuty.Enabled = enable;
                    chkCrewDuty.Enabled = enable;
                    chkCrewStandBy.Enabled = enable;
                    tbForeColor.Enabled = enable;
                    tbBackColor.Enabled = enable;
                    rcpBackColor.Enabled = enable;
                    rcpForeColor.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    chkInactive.Enabled = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbHolidayValuePoints.Text = string.Empty;
                    tbValuePoints.Text = string.Empty;
                    tbWeekendValuePoints.Text = string.Empty;
                    tbCode.Text = string.Empty;
                    hdnCDTID.Value = string.Empty;
                    tbDescription.Text = string.Empty;
                    rmtbStartTime.Text = string.Empty;
                    rmtbEndTime.Text = string.Empty;
                    chkQuickCalendarEntry.Checked = false;
                    //chkCrewCurrencyPlanner.Checked = false;
                    chkCrewDuty.Checked = false;
                    chkCrewStandBy.Checked = false;
                    chkOfficeDuty.Checked = false;
                    chkWorkLoadIndex.Checked = false;
                    tbForeColor.Text = string.Empty;
                    hdnForeColor.Value = string.Empty;
                    tbBackColor.Text = string.Empty;
                    hdnBackColor.Value = string.Empty;
                    rcpBackColor.SelectedColor = System.Drawing.Color.White;
                    rcpForeColor.SelectedColor = System.Drawing.Color.White;
                    chkInactive.Checked = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.Trim().ToUpper() == (tbCode.Text.Trim().ToUpper()));
                                if (objRetVal.Count() > 0)
                                {
                                    cvCode.IsValid = false;
                                    RadAjaxManager1.FocusControl(tbCode.ClientID); //tbCode.Focus();
                                }
                                if (CheckForReservedCode())
                                {
                                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('R, F, G and SM Codes Are Reserved For Internal Use by FSS.', 400, 50, 'Crew Duty Type');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// To enable and disable the workload index
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkWorkLoadIndex_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkWorkLoadIndex.Checked == true)
                        {
                            tbValuePoints.Enabled = true;
                            tbHolidayValuePoints.Enabled = true;
                            tbWeekendValuePoints.Enabled = true;
                        }
                        else
                        {
                            tbValuePoints.Enabled = false;
                            tbHolidayValuePoints.Enabled = false;
                            tbWeekendValuePoints.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// To enable and disable ValuePoints
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkOfficeDuty_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkOfficeDuty.Checked == true)
                        {
                            tbValuePoints.Enabled = false;
                            tbHolidayValuePoints.Enabled = false;
                            tbWeekendValuePoints.Enabled = false;
                            tbValuePoints.Text = "0";
                            tbHolidayValuePoints.Text = "0";
                            tbWeekendValuePoints.Text = "0";
                        }
                        else
                        {
                            tbValuePoints.Enabled = true;
                            tbHolidayValuePoints.Enabled = true;
                            tbWeekendValuePoints.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewDutyTypes.ClientSettings.Scrolling.ScrollTop = "0";
                        CrewDutyTypePageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        #region "Reports"
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        #endregion
        /// <summary>
        /// Prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCrewDutyTypes_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewDutyTypePageNavigated)
                        {
                            SelectItem();
                        }

                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCrewDutyTypes, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.CrewDutyType);
                }
            }
        }
        /// <summary>
        /// CheckForReservedCode
        /// </summary>
        /// <returns></returns>
        private bool CheckForReservedCode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string dutyCode = tbCode.Text.ToUpper().Trim();
                    if (dutyCode.Trim() == "F" || dutyCode.Trim() == "G" || dutyCode.Trim() == "R" || dutyCode.Trim() == "SM")
                    {

                        returnVal = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.CrewDutyType> lstCrewDutyType = new List<FlightPakMasterService.CrewDutyType>();
                if (Session["CrewDutyType"] != null)
                {
                    lstCrewDutyType = (List<FlightPakMasterService.CrewDutyType>)Session["CrewDutyType"];
                }
                if (lstCrewDutyType.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstCrewDutyType = lstCrewDutyType.Where(x => x.IsInActive == false).ToList<CrewDutyType>(); }
                    if (chkIsCrewDuty.Checked == true) { lstCrewDutyType = lstCrewDutyType.Where(x => x.IsCrewDuty == true).ToList<CrewDutyType>(); }
                    if (chkIsWorkLoadIndex.Checked == true) { lstCrewDutyType = lstCrewDutyType.Where(x => x.IsWorkLoadIndex == true).ToList<CrewDutyType>(); }
                    dgCrewDutyTypes.DataSource = lstCrewDutyType;
                    if (IsDataBind)
                    {
                        dgCrewDutyTypes.DataBind();
                    }
                }
                LoadControlData();
                return false;
            }
        }
        #endregion

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            chkSearchActiveOnly.Enabled = true;
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCrewDutyTypes.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var DutyTypeValue = FPKMstService.GetCrewDutyTypeList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, DutyTypeValue);
            List<FlightPakMasterService.CrewDutyType> filteredList = GetFilteredList(DutyTypeValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.DutyTypeID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCrewDutyTypes.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCrewDutyTypes.CurrentPageIndex = PageNumber;
            dgCrewDutyTypes.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfCrewDutyType DutyTypeValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedDutyTypeID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = DutyTypeValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().DutyTypeID;
                Session["SelectedDutyTypeID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.CrewDutyType> GetFilteredList(ReturnValueOfCrewDutyType DutyTypeValue)
        {
            List<FlightPakMasterService.CrewDutyType> filteredList = new List<FlightPakMasterService.CrewDutyType>();

            if (DutyTypeValue.ReturnFlag)
            {
                filteredList = DutyTypeValue.EntityList;
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<CrewDutyType>(); }
                    if (chkIsCrewDuty.Checked) { filteredList = filteredList.Where(x => x.IsCrewDuty == true).ToList<CrewDutyType>(); }
                }
            }

            return filteredList;
        }
    }
}
